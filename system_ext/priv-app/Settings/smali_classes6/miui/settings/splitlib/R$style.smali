.class public final Lmiui/settings/splitlib/R$style;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/settings/splitlib/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final AccentColorHighlightBorderlessButton:I = 0x7f130000

.field public static final AccessibilityDialog:I = 0x7f130001

.field public static final AccessibilityDialogButton:I = 0x7f130002

.field public static final AccessibilityDialogButtonBarSpace:I = 0x7f130003

.field public static final AccessibilityDialogButtonList:I = 0x7f130004

.field public static final AccessibilityDialogDescription:I = 0x7f130005

.field public static final AccessibilityDialogIcon:I = 0x7f130006

.field public static final AccessibilityDialogPermissionDescription:I = 0x7f130007

.field public static final AccessibilityDialogPermissionTitle:I = 0x7f130008

.field public static final AccessibilityDialogServiceIcon:I = 0x7f130009

.field public static final AccessibilityDialogTitle:I = 0x7f13000a

.field public static final AccessibilityTextReadingPreviewTitle:I = 0x7f13000b

.field public static final ActionBarTitleTextPadding:I = 0x7f130015

.field public static final ActionBar_Contributors:I = 0x7f13000c

.field public static final ActionBar_FreeWifi:I = 0x7f13000d

.field public static final ActionBar_Haptic_Show_NoTitle:I = 0x7f13000e

.field public static final ActionBar_ShowTitle:I = 0x7f130011

.field public static final ActionBar_ShowTitleAndBack:I = 0x7f130013

.field public static final ActionBar_ShowTitle_Transparent:I = 0x7f130012

.field public static final ActionBar_Show_Custom:I = 0x7f13000f

.field public static final ActionBar_Show_NoTitle:I = 0x7f130010

.field public static final ActionBar_TabText:I = 0x7f130014

.field public static final ActionPrimaryButton:I = 0x7f130016

.field public static final ActionSecondaryButton:I = 0x7f130017

.field public static final AlertDialog:I = 0x7f130018

.field public static final AlertDialog_AppCompat:I = 0x7f130019

.field public static final AlertDialog_AppCompat_Light:I = 0x7f13001a

.field public static final AlertDialog_Theme:I = 0x7f13001b

.field public static final AlertDialog_Theme_Dark:I = 0x7f13001c

.field public static final AlertDialog_Theme_DayNight:I = 0x7f13001d

.field public static final AlertDialog_Theme_Light:I = 0x7f13001e

.field public static final AlertDialog_Widget_ProgressBar_Dark:I = 0x7f13001f

.field public static final AlertDialog_Widget_ProgressBar_Light:I = 0x7f130020

.field public static final AlertDialog_Widget_StateEditText_Dark:I = 0x7f130021

.field public static final AlertDialog_Widget_StateEditText_Light:I = 0x7f130022

.field public static final AndroidThemeColorAccentYellow:I = 0x7f130023

.field public static final Animation:I = 0x7f130024

.field public static final Animation_AppCompat_Dialog:I = 0x7f130025

.field public static final Animation_AppCompat_DropDownUp:I = 0x7f130026

.field public static final Animation_AppCompat_Tooltip:I = 0x7f130027

.field public static final Animation_Design_BottomSheetDialog:I = 0x7f130028

.field public static final Animation_Dialog:I = 0x7f130029

.field public static final Animation_Dialog_Center:I = 0x7f13002a

.field public static final Animation_Dialog_NoAnimation:I = 0x7f13002b

.field public static final Animation_MaterialComponents_BottomSheetDialog:I = 0x7f13002c

.field public static final Animation_PopupWindow:I = 0x7f13002d

.field public static final Animation_PopupWindow_DropDown:I = 0x7f13002e

.field public static final Animation_PopupWindow_ImmersionMenu:I = 0x7f13002f

.field public static final Animation_SudWindowAnimation:I = 0x7f130030

.field public static final ApnPreference:I = 0x7f130031

.field public static final AppAuthenticationExpander:I = 0x7f130032

.field public static final AppAuthenticationPolicyIcon:I = 0x7f130033

.field public static final AppAuthenticationPolicyItem:I = 0x7f130034

.field public static final AppAuthenticationPolicyNumberOfUrisText:I = 0x7f130035

.field public static final AppAuthenticationPolicyText:I = 0x7f130036

.field public static final AppEntitiesHeader_Text:I = 0x7f130037

.field public static final AppEntitiesHeader_Text_HeaderTitle:I = 0x7f130038

.field public static final AppEntitiesHeader_Text_Summary:I = 0x7f130039

.field public static final AppEntitiesHeader_Text_Title:I = 0x7f13003a

.field public static final AppTheme:I = 0x7f13003b

.field public static final AppUriAuthenticationPolicyText:I = 0x7f13003c

.field public static final AppUsageTimeRemindText:I = 0x7f13003d

.field public static final Banner_ButtonText_SettingsLib:I = 0x7f13003e

.field public static final Banner_Dismiss_SettingsLib:I = 0x7f13003f

.field public static final Banner_Preference_SettingsLib:I = 0x7f130040

.field public static final Banner_Subtitle_SettingsLib:I = 0x7f130041

.field public static final Banner_Summary_SettingsLib:I = 0x7f130042

.field public static final Banner_Text_Summary:I = 0x7f130043

.field public static final Banner_Text_Title:I = 0x7f130044

.field public static final Banner_Title_SettingsLib:I = 0x7f130045

.field public static final BarChart_Text:I = 0x7f130046

.field public static final BarChart_Text_HeaderTitle:I = 0x7f130047

.field public static final BarChart_Text_Summary:I = 0x7f130048

.field public static final BarChart_Text_Title:I = 0x7f130049

.field public static final BasePreferenceThemeOverlay:I = 0x7f13014f

.field public static final Base_AlertDialog_AppCompat:I = 0x7f13004a

.field public static final Base_AlertDialog_AppCompat_Light:I = 0x7f13004b

.field public static final Base_Animation_AppCompat_Dialog:I = 0x7f13004c

.field public static final Base_Animation_AppCompat_DropDownUp:I = 0x7f13004d

.field public static final Base_Animation_AppCompat_Tooltip:I = 0x7f13004e

.field public static final Base_CardView:I = 0x7f13004f

.field public static final Base_DialogWindowTitleBackground_AppCompat:I = 0x7f130051

.field public static final Base_DialogWindowTitle_AppCompat:I = 0x7f130050

.field public static final Base_MaterialAlertDialog_MaterialComponents_Title_Icon:I = 0x7f130052

.field public static final Base_MaterialAlertDialog_MaterialComponents_Title_Panel:I = 0x7f130053

.field public static final Base_MaterialAlertDialog_MaterialComponents_Title_Text:I = 0x7f130054

.field public static final Base_TextAppearance_AppCompat:I = 0x7f130055

.field public static final Base_TextAppearance_AppCompat_Body1:I = 0x7f130056

.field public static final Base_TextAppearance_AppCompat_Body2:I = 0x7f130057

.field public static final Base_TextAppearance_AppCompat_Button:I = 0x7f130058

.field public static final Base_TextAppearance_AppCompat_Caption:I = 0x7f130059

.field public static final Base_TextAppearance_AppCompat_Display1:I = 0x7f13005a

.field public static final Base_TextAppearance_AppCompat_Display2:I = 0x7f13005b

.field public static final Base_TextAppearance_AppCompat_Display3:I = 0x7f13005c

.field public static final Base_TextAppearance_AppCompat_Display4:I = 0x7f13005d

.field public static final Base_TextAppearance_AppCompat_Headline:I = 0x7f13005e

.field public static final Base_TextAppearance_AppCompat_Inverse:I = 0x7f13005f

.field public static final Base_TextAppearance_AppCompat_Large:I = 0x7f130060

.field public static final Base_TextAppearance_AppCompat_Large_Inverse:I = 0x7f130061

.field public static final Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Large:I = 0x7f130062

.field public static final Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Small:I = 0x7f130063

.field public static final Base_TextAppearance_AppCompat_Medium:I = 0x7f130064

.field public static final Base_TextAppearance_AppCompat_Medium_Inverse:I = 0x7f130065

.field public static final Base_TextAppearance_AppCompat_Menu:I = 0x7f130066

.field public static final Base_TextAppearance_AppCompat_SearchResult:I = 0x7f130067

.field public static final Base_TextAppearance_AppCompat_SearchResult_Subtitle:I = 0x7f130068

.field public static final Base_TextAppearance_AppCompat_SearchResult_Title:I = 0x7f130069

.field public static final Base_TextAppearance_AppCompat_Small:I = 0x7f13006a

.field public static final Base_TextAppearance_AppCompat_Small_Inverse:I = 0x7f13006b

.field public static final Base_TextAppearance_AppCompat_Subhead:I = 0x7f13006c

.field public static final Base_TextAppearance_AppCompat_Subhead_Inverse:I = 0x7f13006d

.field public static final Base_TextAppearance_AppCompat_Title:I = 0x7f13006e

.field public static final Base_TextAppearance_AppCompat_Title_Inverse:I = 0x7f13006f

.field public static final Base_TextAppearance_AppCompat_Tooltip:I = 0x7f130070

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Menu:I = 0x7f130071

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle:I = 0x7f130072

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse:I = 0x7f130073

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Title:I = 0x7f130074

.field public static final Base_TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse:I = 0x7f130075

.field public static final Base_TextAppearance_AppCompat_Widget_ActionMode_Subtitle:I = 0x7f130076

.field public static final Base_TextAppearance_AppCompat_Widget_ActionMode_Title:I = 0x7f130077

.field public static final Base_TextAppearance_AppCompat_Widget_Button:I = 0x7f130078

.field public static final Base_TextAppearance_AppCompat_Widget_Button_Borderless_Colored:I = 0x7f130079

.field public static final Base_TextAppearance_AppCompat_Widget_Button_Colored:I = 0x7f13007a

.field public static final Base_TextAppearance_AppCompat_Widget_Button_Inverse:I = 0x7f13007b

.field public static final Base_TextAppearance_AppCompat_Widget_DropDownItem:I = 0x7f13007c

.field public static final Base_TextAppearance_AppCompat_Widget_PopupMenu_Header:I = 0x7f13007d

.field public static final Base_TextAppearance_AppCompat_Widget_PopupMenu_Large:I = 0x7f13007e

.field public static final Base_TextAppearance_AppCompat_Widget_PopupMenu_Small:I = 0x7f13007f

.field public static final Base_TextAppearance_AppCompat_Widget_Switch:I = 0x7f130080

.field public static final Base_TextAppearance_AppCompat_Widget_TextView_SpinnerItem:I = 0x7f130081

.field public static final Base_TextAppearance_MaterialComponents_Badge:I = 0x7f130082

.field public static final Base_TextAppearance_MaterialComponents_Button:I = 0x7f130083

.field public static final Base_TextAppearance_MaterialComponents_Headline6:I = 0x7f130084

.field public static final Base_TextAppearance_MaterialComponents_Subtitle2:I = 0x7f130085

.field public static final Base_TextAppearance_Widget_AppCompat_ExpandedMenu_Item:I = 0x7f130086

.field public static final Base_TextAppearance_Widget_AppCompat_Toolbar_Subtitle:I = 0x7f130087

.field public static final Base_TextAppearance_Widget_AppCompat_Toolbar_Title:I = 0x7f130088

.field public static final Base_ThemeOverlay_AppCompat:I = 0x7f1300b0

.field public static final Base_ThemeOverlay_AppCompat_ActionBar:I = 0x7f1300b1

.field public static final Base_ThemeOverlay_AppCompat_Dark:I = 0x7f1300b2

.field public static final Base_ThemeOverlay_AppCompat_Dark_ActionBar:I = 0x7f1300b3

.field public static final Base_ThemeOverlay_AppCompat_Dialog:I = 0x7f1300b4

.field public static final Base_ThemeOverlay_AppCompat_Dialog_Alert:I = 0x7f1300b5

.field public static final Base_ThemeOverlay_AppCompat_Light:I = 0x7f1300b6

.field public static final Base_ThemeOverlay_Material3_AutoCompleteTextView:I = 0x7f1300b7

.field public static final Base_ThemeOverlay_Material3_BottomSheetDialog:I = 0x7f1300b8

.field public static final Base_ThemeOverlay_Material3_Dialog:I = 0x7f1300b9

.field public static final Base_ThemeOverlay_Material3_TextInputEditText:I = 0x7f1300ba

.field public static final Base_ThemeOverlay_MaterialComponents_Dialog:I = 0x7f1300bb

.field public static final Base_ThemeOverlay_MaterialComponents_Dialog_Alert:I = 0x7f1300bc

.field public static final Base_ThemeOverlay_MaterialComponents_Dialog_Alert_Framework:I = 0x7f1300bd

.field public static final Base_ThemeOverlay_MaterialComponents_Light_Dialog_Alert_Framework:I = 0x7f1300be

.field public static final Base_ThemeOverlay_MaterialComponents_MaterialAlertDialog:I = 0x7f1300bf

.field public static final Base_Theme_AppCompat:I = 0x7f130089

.field public static final Base_Theme_AppCompat_CompactMenu:I = 0x7f13008a

.field public static final Base_Theme_AppCompat_Dialog:I = 0x7f13008b

.field public static final Base_Theme_AppCompat_DialogWhenLarge:I = 0x7f13008f

.field public static final Base_Theme_AppCompat_Dialog_Alert:I = 0x7f13008c

.field public static final Base_Theme_AppCompat_Dialog_FixedSize:I = 0x7f13008d

.field public static final Base_Theme_AppCompat_Dialog_MinWidth:I = 0x7f13008e

.field public static final Base_Theme_AppCompat_Light:I = 0x7f130090

.field public static final Base_Theme_AppCompat_Light_DarkActionBar:I = 0x7f130091

.field public static final Base_Theme_AppCompat_Light_Dialog:I = 0x7f130092

.field public static final Base_Theme_AppCompat_Light_DialogWhenLarge:I = 0x7f130096

.field public static final Base_Theme_AppCompat_Light_Dialog_Alert:I = 0x7f130093

.field public static final Base_Theme_AppCompat_Light_Dialog_FixedSize:I = 0x7f130094

.field public static final Base_Theme_AppCompat_Light_Dialog_MinWidth:I = 0x7f130095

.field public static final Base_Theme_Material3_Dark:I = 0x7f130097

.field public static final Base_Theme_Material3_Dark_BottomSheetDialog:I = 0x7f130098

.field public static final Base_Theme_Material3_Dark_Dialog:I = 0x7f130099

.field public static final Base_Theme_Material3_Light:I = 0x7f13009a

.field public static final Base_Theme_Material3_Light_BottomSheetDialog:I = 0x7f13009b

.field public static final Base_Theme_Material3_Light_Dialog:I = 0x7f13009c

.field public static final Base_Theme_MaterialComponents:I = 0x7f13009d

.field public static final Base_Theme_MaterialComponents_Bridge:I = 0x7f13009e

.field public static final Base_Theme_MaterialComponents_CompactMenu:I = 0x7f13009f

.field public static final Base_Theme_MaterialComponents_Dialog:I = 0x7f1300a0

.field public static final Base_Theme_MaterialComponents_DialogWhenLarge:I = 0x7f1300a5

.field public static final Base_Theme_MaterialComponents_Dialog_Alert:I = 0x7f1300a1

.field public static final Base_Theme_MaterialComponents_Dialog_Bridge:I = 0x7f1300a2

.field public static final Base_Theme_MaterialComponents_Dialog_FixedSize:I = 0x7f1300a3

.field public static final Base_Theme_MaterialComponents_Dialog_MinWidth:I = 0x7f1300a4

.field public static final Base_Theme_MaterialComponents_Light:I = 0x7f1300a6

.field public static final Base_Theme_MaterialComponents_Light_Bridge:I = 0x7f1300a7

.field public static final Base_Theme_MaterialComponents_Light_DarkActionBar:I = 0x7f1300a8

.field public static final Base_Theme_MaterialComponents_Light_DarkActionBar_Bridge:I = 0x7f1300a9

.field public static final Base_Theme_MaterialComponents_Light_Dialog:I = 0x7f1300aa

.field public static final Base_Theme_MaterialComponents_Light_DialogWhenLarge:I = 0x7f1300af

.field public static final Base_Theme_MaterialComponents_Light_Dialog_Alert:I = 0x7f1300ab

.field public static final Base_Theme_MaterialComponents_Light_Dialog_Bridge:I = 0x7f1300ac

.field public static final Base_Theme_MaterialComponents_Light_Dialog_FixedSize:I = 0x7f1300ad

.field public static final Base_Theme_MaterialComponents_Light_Dialog_MinWidth:I = 0x7f1300ae

.field public static final Base_V14_ThemeOverlay_Material3_BottomSheetDialog:I = 0x7f1300cf

.field public static final Base_V14_ThemeOverlay_MaterialComponents_BottomSheetDialog:I = 0x7f1300d0

.field public static final Base_V14_ThemeOverlay_MaterialComponents_Dialog:I = 0x7f1300d1

.field public static final Base_V14_ThemeOverlay_MaterialComponents_Dialog_Alert:I = 0x7f1300d2

.field public static final Base_V14_ThemeOverlay_MaterialComponents_MaterialAlertDialog:I = 0x7f1300d3

.field public static final Base_V14_Theme_Material3_Dark:I = 0x7f1300c0

.field public static final Base_V14_Theme_Material3_Dark_BottomSheetDialog:I = 0x7f1300c1

.field public static final Base_V14_Theme_Material3_Dark_Dialog:I = 0x7f1300c2

.field public static final Base_V14_Theme_Material3_Light:I = 0x7f1300c3

.field public static final Base_V14_Theme_Material3_Light_BottomSheetDialog:I = 0x7f1300c4

.field public static final Base_V14_Theme_Material3_Light_Dialog:I = 0x7f1300c5

.field public static final Base_V14_Theme_MaterialComponents:I = 0x7f1300c6

.field public static final Base_V14_Theme_MaterialComponents_Bridge:I = 0x7f1300c7

.field public static final Base_V14_Theme_MaterialComponents_Dialog:I = 0x7f1300c8

.field public static final Base_V14_Theme_MaterialComponents_Dialog_Bridge:I = 0x7f1300c9

.field public static final Base_V14_Theme_MaterialComponents_Light:I = 0x7f1300ca

.field public static final Base_V14_Theme_MaterialComponents_Light_Bridge:I = 0x7f1300cb

.field public static final Base_V14_Theme_MaterialComponents_Light_DarkActionBar_Bridge:I = 0x7f1300cc

.field public static final Base_V14_Theme_MaterialComponents_Light_Dialog:I = 0x7f1300cd

.field public static final Base_V14_Theme_MaterialComponents_Light_Dialog_Bridge:I = 0x7f1300ce

.field public static final Base_V21_ThemeOverlay_AppCompat_Dialog:I = 0x7f1300dc

.field public static final Base_V21_ThemeOverlay_Material3_BottomSheetDialog:I = 0x7f1300dd

.field public static final Base_V21_ThemeOverlay_MaterialComponents_BottomSheetDialog:I = 0x7f1300de

.field public static final Base_V21_Theme_AppCompat:I = 0x7f1300d4

.field public static final Base_V21_Theme_AppCompat_Dialog:I = 0x7f1300d5

.field public static final Base_V21_Theme_AppCompat_Light:I = 0x7f1300d6

.field public static final Base_V21_Theme_AppCompat_Light_Dialog:I = 0x7f1300d7

.field public static final Base_V21_Theme_MaterialComponents:I = 0x7f1300d8

.field public static final Base_V21_Theme_MaterialComponents_Dialog:I = 0x7f1300d9

.field public static final Base_V21_Theme_MaterialComponents_Light:I = 0x7f1300da

.field public static final Base_V21_Theme_MaterialComponents_Light_Dialog:I = 0x7f1300db

.field public static final Base_V22_Theme_AppCompat:I = 0x7f1300df

.field public static final Base_V22_Theme_AppCompat_Light:I = 0x7f1300e0

.field public static final Base_V23_Theme_AppCompat:I = 0x7f1300e1

.field public static final Base_V23_Theme_AppCompat_Light:I = 0x7f1300e2

.field public static final Base_V24_Theme_Material3_Dark:I = 0x7f1300e3

.field public static final Base_V24_Theme_Material3_Dark_Dialog:I = 0x7f1300e4

.field public static final Base_V24_Theme_Material3_Light:I = 0x7f1300e5

.field public static final Base_V24_Theme_Material3_Light_Dialog:I = 0x7f1300e6

.field public static final Base_V26_Theme_AppCompat:I = 0x7f1300e7

.field public static final Base_V26_Theme_AppCompat_Light:I = 0x7f1300e8

.field public static final Base_V26_Widget_AppCompat_Toolbar:I = 0x7f1300e9

.field public static final Base_V28_Theme_AppCompat:I = 0x7f1300ea

.field public static final Base_V28_Theme_AppCompat_Light:I = 0x7f1300eb

.field public static final Base_V7_ThemeOverlay_AppCompat_Dialog:I = 0x7f1300f0

.field public static final Base_V7_Theme_AppCompat:I = 0x7f1300ec

.field public static final Base_V7_Theme_AppCompat_Dialog:I = 0x7f1300ed

.field public static final Base_V7_Theme_AppCompat_Light:I = 0x7f1300ee

.field public static final Base_V7_Theme_AppCompat_Light_Dialog:I = 0x7f1300ef

.field public static final Base_V7_Widget_AppCompat_AutoCompleteTextView:I = 0x7f1300f1

.field public static final Base_V7_Widget_AppCompat_EditText:I = 0x7f1300f2

.field public static final Base_V7_Widget_AppCompat_Toolbar:I = 0x7f1300f3

.field public static final Base_Widget_AppCompat_ActionBar:I = 0x7f1300f4

.field public static final Base_Widget_AppCompat_ActionBar_Solid:I = 0x7f1300f5

.field public static final Base_Widget_AppCompat_ActionBar_TabBar:I = 0x7f1300f6

.field public static final Base_Widget_AppCompat_ActionBar_TabText:I = 0x7f1300f7

.field public static final Base_Widget_AppCompat_ActionBar_TabView:I = 0x7f1300f8

.field public static final Base_Widget_AppCompat_ActionButton:I = 0x7f1300f9

.field public static final Base_Widget_AppCompat_ActionButton_CloseMode:I = 0x7f1300fa

.field public static final Base_Widget_AppCompat_ActionButton_Overflow:I = 0x7f1300fb

.field public static final Base_Widget_AppCompat_ActionMode:I = 0x7f1300fc

.field public static final Base_Widget_AppCompat_ActivityChooserView:I = 0x7f1300fd

.field public static final Base_Widget_AppCompat_AutoCompleteTextView:I = 0x7f1300fe

.field public static final Base_Widget_AppCompat_Button:I = 0x7f1300ff

.field public static final Base_Widget_AppCompat_ButtonBar:I = 0x7f130105

.field public static final Base_Widget_AppCompat_ButtonBar_AlertDialog:I = 0x7f130106

.field public static final Base_Widget_AppCompat_Button_Borderless:I = 0x7f130100

.field public static final Base_Widget_AppCompat_Button_Borderless_Colored:I = 0x7f130101

.field public static final Base_Widget_AppCompat_Button_ButtonBar_AlertDialog:I = 0x7f130102

.field public static final Base_Widget_AppCompat_Button_Colored:I = 0x7f130103

.field public static final Base_Widget_AppCompat_Button_Small:I = 0x7f130104

.field public static final Base_Widget_AppCompat_CompoundButton_CheckBox:I = 0x7f130107

.field public static final Base_Widget_AppCompat_CompoundButton_RadioButton:I = 0x7f130108

.field public static final Base_Widget_AppCompat_CompoundButton_Switch:I = 0x7f130109

.field public static final Base_Widget_AppCompat_DrawerArrowToggle:I = 0x7f13010a

.field public static final Base_Widget_AppCompat_DrawerArrowToggle_Common:I = 0x7f13010b

.field public static final Base_Widget_AppCompat_DropDownItem_Spinner:I = 0x7f13010c

.field public static final Base_Widget_AppCompat_EditText:I = 0x7f13010d

.field public static final Base_Widget_AppCompat_ImageButton:I = 0x7f13010e

.field public static final Base_Widget_AppCompat_Light_ActionBar:I = 0x7f13010f

.field public static final Base_Widget_AppCompat_Light_ActionBar_Solid:I = 0x7f130110

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabBar:I = 0x7f130111

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabText:I = 0x7f130112

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabText_Inverse:I = 0x7f130113

.field public static final Base_Widget_AppCompat_Light_ActionBar_TabView:I = 0x7f130114

.field public static final Base_Widget_AppCompat_Light_PopupMenu:I = 0x7f130115

.field public static final Base_Widget_AppCompat_Light_PopupMenu_Overflow:I = 0x7f130116

.field public static final Base_Widget_AppCompat_ListMenuView:I = 0x7f130117

.field public static final Base_Widget_AppCompat_ListPopupWindow:I = 0x7f130118

.field public static final Base_Widget_AppCompat_ListView:I = 0x7f130119

.field public static final Base_Widget_AppCompat_ListView_DropDown:I = 0x7f13011a

.field public static final Base_Widget_AppCompat_ListView_Menu:I = 0x7f13011b

.field public static final Base_Widget_AppCompat_PopupMenu:I = 0x7f13011c

.field public static final Base_Widget_AppCompat_PopupMenu_Overflow:I = 0x7f13011d

.field public static final Base_Widget_AppCompat_PopupWindow:I = 0x7f13011e

.field public static final Base_Widget_AppCompat_ProgressBar:I = 0x7f13011f

.field public static final Base_Widget_AppCompat_ProgressBar_Horizontal:I = 0x7f130120

.field public static final Base_Widget_AppCompat_RatingBar:I = 0x7f130121

.field public static final Base_Widget_AppCompat_RatingBar_Indicator:I = 0x7f130122

.field public static final Base_Widget_AppCompat_RatingBar_Small:I = 0x7f130123

.field public static final Base_Widget_AppCompat_SearchView:I = 0x7f130124

.field public static final Base_Widget_AppCompat_SearchView_ActionBar:I = 0x7f130125

.field public static final Base_Widget_AppCompat_SeekBar:I = 0x7f130126

.field public static final Base_Widget_AppCompat_SeekBar_Discrete:I = 0x7f130127

.field public static final Base_Widget_AppCompat_Spinner:I = 0x7f130128

.field public static final Base_Widget_AppCompat_Spinner_Underlined:I = 0x7f130129

.field public static final Base_Widget_AppCompat_TextView:I = 0x7f13012a

.field public static final Base_Widget_AppCompat_TextView_SpinnerItem:I = 0x7f13012b

.field public static final Base_Widget_AppCompat_Toolbar:I = 0x7f13012c

.field public static final Base_Widget_AppCompat_Toolbar_Button_Navigation:I = 0x7f13012d

.field public static final Base_Widget_Design_TabLayout:I = 0x7f13012e

.field public static final Base_Widget_Material3_ActionBar_Solid:I = 0x7f13012f

.field public static final Base_Widget_Material3_ActionMode:I = 0x7f130130

.field public static final Base_Widget_Material3_CardView:I = 0x7f130131

.field public static final Base_Widget_Material3_Chip:I = 0x7f130132

.field public static final Base_Widget_Material3_CollapsingToolbar:I = 0x7f130133

.field public static final Base_Widget_Material3_CompoundButton_CheckBox:I = 0x7f130134

.field public static final Base_Widget_Material3_CompoundButton_RadioButton:I = 0x7f130135

.field public static final Base_Widget_Material3_CompoundButton_Switch:I = 0x7f130136

.field public static final Base_Widget_Material3_ExtendedFloatingActionButton:I = 0x7f130137

.field public static final Base_Widget_Material3_ExtendedFloatingActionButton_Icon:I = 0x7f130138

.field public static final Base_Widget_Material3_FloatingActionButton:I = 0x7f130139

.field public static final Base_Widget_Material3_FloatingActionButton_Large:I = 0x7f13013a

.field public static final Base_Widget_Material3_Light_ActionBar_Solid:I = 0x7f13013b

.field public static final Base_Widget_Material3_MaterialCalendar_NavigationButton:I = 0x7f13013c

.field public static final Base_Widget_Material3_Snackbar:I = 0x7f13013d

.field public static final Base_Widget_Material3_TabLayout:I = 0x7f13013e

.field public static final Base_Widget_Material3_TabLayout_OnSurface:I = 0x7f13013f

.field public static final Base_Widget_Material3_TabLayout_Secondary:I = 0x7f130140

.field public static final Base_Widget_MaterialComponents_AutoCompleteTextView:I = 0x7f130141

.field public static final Base_Widget_MaterialComponents_CheckedTextView:I = 0x7f130142

.field public static final Base_Widget_MaterialComponents_Chip:I = 0x7f130143

.field public static final Base_Widget_MaterialComponents_MaterialCalendar_HeaderToggleButton:I = 0x7f130144

.field public static final Base_Widget_MaterialComponents_MaterialCalendar_NavigationButton:I = 0x7f130145

.field public static final Base_Widget_MaterialComponents_PopupMenu:I = 0x7f130146

.field public static final Base_Widget_MaterialComponents_PopupMenu_ContextMenu:I = 0x7f130147

.field public static final Base_Widget_MaterialComponents_PopupMenu_ListPopupWindow:I = 0x7f130148

.field public static final Base_Widget_MaterialComponents_PopupMenu_Overflow:I = 0x7f130149

.field public static final Base_Widget_MaterialComponents_Slider:I = 0x7f13014a

.field public static final Base_Widget_MaterialComponents_Snackbar:I = 0x7f13014b

.field public static final Base_Widget_MaterialComponents_TextInputEditText:I = 0x7f13014c

.field public static final Base_Widget_MaterialComponents_TextInputLayout:I = 0x7f13014d

.field public static final Base_Widget_MaterialComponents_TextView:I = 0x7f13014e

.field public static final BiometricEnrollIntroMessage:I = 0x7f130150

.field public static final BiometricEnrollIntroTitle:I = 0x7f130151

.field public static final BiometricHeaderStyle:I = 0x7f130152

.field public static final BorderlessButton:I = 0x7f130153

.field public static final BroadcastActionButton:I = 0x7f130154

.field public static final BroadcastDialogBodyStyle:I = 0x7f130155

.field public static final BroadcastDialogButtonStyle:I = 0x7f130156

.field public static final BroadcastDialogTitleStyle:I = 0x7f130157

.field public static final CardInstructionsText:I = 0x7f130158

.field public static final CardPreference:I = 0x7f130159

.field public static final CardView:I = 0x7f13015a

.field public static final CardView_Dark:I = 0x7f13015b

.field public static final CardView_Light:I = 0x7f13015c

.field public static final CheckWidgetDrawable:I = 0x7f13015d

.field public static final CheckWidgetDrawable_CheckBox:I = 0x7f13015e

.field public static final CheckWidgetDrawable_RadioButton:I = 0x7f13015f

.field public static final CollapseSubtitleStyle:I = 0x7f130160

.field public static final CollapseTitleStyle:I = 0x7f130161

.field public static final CollapsingToolbarTitle_Collapsed:I = 0x7f130162

.field public static final CollapsingToolbarTitle_Expanded:I = 0x7f130163

.field public static final ConditionCardBorderlessButton:I = 0x7f130164

.field public static final ConditionFullCardBorderlessButton:I = 0x7f130165

.field public static final ConditionHalfCardBorderlessButton:I = 0x7f130166

.field public static final ConfirmDeviceCredentialsAnimationStyle:I = 0x7f130167

.field public static final ContextualCardDismissalButton:I = 0x7f130168

.field public static final ContextualCardStyle:I = 0x7f130169

.field public static final CrossProfileConsentDialogDescription:I = 0x7f13016a

.field public static final CrossProfileConsentDialogIcon:I = 0x7f13016b

.field public static final CrossProfileConsentDialogSubDescription:I = 0x7f13016c

.field public static final CrossProfileConsentDialogSubTitle:I = 0x7f13016d

.field public static final CrossProfileConsentDialogTitle:I = 0x7f13016e

.field public static final CrossProfileEntityHeaderIcon:I = 0x7f13016f

.field public static final CrossProfileEntityHeaderTitle:I = 0x7f130170

.field public static final CrossProfileSwapHorizIcon:I = 0x7f130171

.field public static final CryptKeeperBlankTheme:I = 0x7f130172

.field public static final DeviceDisplayImage:I = 0x7f130173

.field public static final DialogPanelStyle:I = 0x7f130174

.field public static final DialogTheme:I = 0x7f130175

.field public static final DisclaimerNegativeButton:I = 0x7f130176

.field public static final DisclaimerPositiveButton:I = 0x7f130177

.field public static final Divider:I = 0x7f130178

.field public static final Divider_Settings:I = 0x7f130179

.field public static final DolbyEqualizerActionBar:I = 0x7f13017a

.field public static final DreamCardStyle:I = 0x7f13017b

.field public static final EQSeekBar:I = 0x7f13017c

.field public static final EditDialog:I = 0x7f13017d

.field public static final EditorActivityTheme:I = 0x7f13017e

.field public static final EmptyTheme:I = 0x7f13017f

.field public static final EntityHeader:I = 0x7f130180

.field public static final ExpandSubtitleStyle:I = 0x7f130181

.field public static final ExpandTitleStyle:I = 0x7f130182

.field public static final FaceLayoutTheme:I = 0x7f130183

.field public static final FallbackHome:I = 0x7f130184

.field public static final FallbackHome_SetupWizard:I = 0x7f130185

.field public static final FingerprintLayoutTheme:I = 0x7f130186

.field public static final Fod_Dialog_Fullscreen:I = 0x7f130187

.field public static final ForgetPasswordButtonStyle:I = 0x7f130188

.field public static final GlifTheme:I = 0x7f130189

.field public static final GlifTheme_DayNight:I = 0x7f13018a

.field public static final GlifTheme_Light:I = 0x7f13018b

.field public static final GlifV2Theme:I = 0x7f13018c

.field public static final GlifV2ThemeAlertDialog:I = 0x7f130192

.field public static final GlifV2ThemeAlertDialog_Light:I = 0x7f130193

.field public static final GlifV2Theme_DayNight:I = 0x7f13018d

.field public static final GlifV2Theme_DayNight_Transparent:I = 0x7f13018e

.field public static final GlifV2Theme_Light:I = 0x7f13018f

.field public static final GlifV2Theme_Light_Transparent:I = 0x7f130190

.field public static final GlifV2Theme_Transparent:I = 0x7f130191

.field public static final GlifV3Theme:I = 0x7f130194

.field public static final GlifV3Theme_DayNight:I = 0x7f130195

.field public static final GlifV3Theme_DayNight_NoActionBar:I = 0x7f130196

.field public static final GlifV3Theme_DayNight_Transparent:I = 0x7f130197

.field public static final GlifV3Theme_Footer:I = 0x7f130198

.field public static final GlifV3Theme_Light:I = 0x7f130199

.field public static final GlifV3Theme_Light_NoActionBar:I = 0x7f13019a

.field public static final GlifV3Theme_Light_Transparent:I = 0x7f13019b

.field public static final GlifV3Theme_NoActionBar:I = 0x7f13019c

.field public static final GlifV3Theme_Transparent:I = 0x7f13019d

.field public static final GlifV4Theme:I = 0x7f13019e

.field public static final GlifV4Theme_DayNight:I = 0x7f13019f

.field public static final GlifV4Theme_Light:I = 0x7f1301a0

.field public static final GroupOptionsButton:I = 0x7f1301a1

.field public static final HapticDetailTheme:I = 0x7f1301a2

.field public static final HapticDetailTheme_ActionBar_TabText:I = 0x7f1301a3

.field public static final HapticDetailTheme_ActionBar_TabText_Expand:I = 0x7f1301a4

.field public static final HomePage:I = 0x7f1301a5

.field public static final HomepageTitleText:I = 0x7f1301a6

.field public static final HorizontalProgressBar_SettingsLib:I = 0x7f1301a7

.field public static final InstructionsText:I = 0x7f1301a8

.field public static final InterestingTheme:I = 0x7f1301a9

.field public static final Internal_MenuWindow_Theme:I = 0x7f1301aa

.field public static final Internal_MenuWindow_Theme_Dark:I = 0x7f1301ab

.field public static final Internal_MenuWindow_Theme_Light:I = 0x7f1301ac

.field public static final KeyguardForgetPasswordButtonStyle:I = 0x7f1301ae

.field public static final Keyguard_Widget_ButtonBar:I = 0x7f1301ad

.field public static final LanguageCheckboxAndLabel:I = 0x7f1301af

.field public static final LargeEditTextStyle:I = 0x7f1301b0

.field public static final LightTheme_SettingsBase_SetupWizard:I = 0x7f1301b1

.field public static final ListView_WithDivider:I = 0x7f1301b2

.field public static final LocaleSearchTheme:I = 0x7f1301b3

.field public static final LockPatternContainerStyle:I = 0x7f1301b4

.field public static final LockPatternStyle:I = 0x7f1301b5

.field public static final MainPreferenceFragment:I = 0x7f1301b6

.field public static final MainSwitchText_Settingslib:I = 0x7f1301b7

.field public static final MaterialAlertDialog_Material3:I = 0x7f1301b8

.field public static final MaterialAlertDialog_Material3_Body_Text:I = 0x7f1301b9

.field public static final MaterialAlertDialog_Material3_Body_Text_CenterStacked:I = 0x7f1301ba

.field public static final MaterialAlertDialog_Material3_Title_Icon:I = 0x7f1301bb

.field public static final MaterialAlertDialog_Material3_Title_Icon_CenterStacked:I = 0x7f1301bc

.field public static final MaterialAlertDialog_Material3_Title_Panel:I = 0x7f1301bd

.field public static final MaterialAlertDialog_Material3_Title_Panel_CenterStacked:I = 0x7f1301be

.field public static final MaterialAlertDialog_Material3_Title_Text:I = 0x7f1301bf

.field public static final MaterialAlertDialog_Material3_Title_Text_CenterStacked:I = 0x7f1301c0

.field public static final MaterialAlertDialog_MaterialComponents:I = 0x7f1301c1

.field public static final MaterialAlertDialog_MaterialComponents_Body_Text:I = 0x7f1301c2

.field public static final MaterialAlertDialog_MaterialComponents_Picker_Date_Calendar:I = 0x7f1301c3

.field public static final MaterialAlertDialog_MaterialComponents_Picker_Date_Spinner:I = 0x7f1301c4

.field public static final MaterialAlertDialog_MaterialComponents_Title_Icon:I = 0x7f1301c5

.field public static final MaterialAlertDialog_MaterialComponents_Title_Icon_CenterStacked:I = 0x7f1301c6

.field public static final MaterialAlertDialog_MaterialComponents_Title_Panel:I = 0x7f1301c7

.field public static final MaterialAlertDialog_MaterialComponents_Title_Panel_CenterStacked:I = 0x7f1301c8

.field public static final MaterialAlertDialog_MaterialComponents_Title_Text:I = 0x7f1301c9

.field public static final MaterialAlertDialog_MaterialComponents_Title_Text_CenterStacked:I = 0x7f1301ca

.field public static final Miui:I = 0x7f1301cb

.field public static final MiuiAccessibility:I = 0x7f1301ce

.field public static final MiuiAccessibility_ActionBar_TabText:I = 0x7f1301cf

.field public static final MiuiAccessibility_ActionBar_TabText_Expand:I = 0x7f1301d0

.field public static final MiuiColumnSplitterStyle:I = 0x7f1301d1

.field public static final MiuiDragShadowStyle:I = 0x7f1301d2

.field public static final Miui_InformationItemTitleStyle:I = 0x7f1301cc

.field public static final Miui_InformationTextStyle:I = 0x7f1301cd

.field public static final Miuix_AppCompat_CheckBoxStyle:I = 0x7f1301d3

.field public static final Miuix_AppCompat_RadioButtonStyle:I = 0x7f1301d4

.field public static final Miuix_AppCompat_TextAppearance:I = 0x7f1301d5

.field public static final Miuix_AppCompat_TextAppearance_AlertDialogListItem:I = 0x7f1301d6

.field public static final Miuix_AppCompat_TextAppearance_AlertDialogListItem_MultiChoice:I = 0x7f1301d7

.field public static final Miuix_AppCompat_TextAppearance_AlertDialogListItem_SingleChoice:I = 0x7f1301d8

.field public static final Miuix_AppCompat_TextAppearance_BlankPage:I = 0x7f1301d9

.field public static final Miuix_AppCompat_TextAppearance_Comment:I = 0x7f1301da

.field public static final Miuix_AppCompat_TextAppearance_Comment_Dark:I = 0x7f1301db

.field public static final Miuix_AppCompat_TextAppearance_Comment_Light:I = 0x7f1301dc

.field public static final Miuix_AppCompat_TextAppearance_DialogTitle:I = 0x7f1301dd

.field public static final Miuix_AppCompat_TextAppearance_EditItemLabel:I = 0x7f1301de

.field public static final Miuix_AppCompat_TextAppearance_Large:I = 0x7f1301df

.field public static final Miuix_AppCompat_TextAppearance_List:I = 0x7f1301e0

.field public static final Miuix_AppCompat_TextAppearance_List_GroupHeader:I = 0x7f1301e1

.field public static final Miuix_AppCompat_TextAppearance_List_Primary:I = 0x7f1301e2

.field public static final Miuix_AppCompat_TextAppearance_List_Secondary:I = 0x7f1301e3

.field public static final Miuix_AppCompat_TextAppearance_List_Secondary_Preference:I = 0x7f1301e4

.field public static final Miuix_AppCompat_TextAppearance_Medium:I = 0x7f1301e5

.field public static final Miuix_AppCompat_TextAppearance_Medium_Dialog:I = 0x7f1301e6

.field public static final Miuix_AppCompat_TextAppearance_Medium_Dialog_Dark:I = 0x7f1301e7

.field public static final Miuix_AppCompat_TextAppearance_Medium_Dialog_Light:I = 0x7f1301e8

.field public static final Miuix_AppCompat_TextAppearance_Medium_MenuList:I = 0x7f1301e9

.field public static final Miuix_AppCompat_TextAppearance_NavigationPreference:I = 0x7f1301ea

.field public static final Miuix_AppCompat_TextAppearance_NavigationPreference_Second:I = 0x7f1301eb

.field public static final Miuix_AppCompat_TextAppearance_PreferenceCategory:I = 0x7f1301ec

.field public static final Miuix_AppCompat_TextAppearance_PreferenceList:I = 0x7f1301ed

.field public static final Miuix_AppCompat_TextAppearance_PreferenceRight:I = 0x7f1301ee

.field public static final Miuix_AppCompat_TextAppearance_Small:I = 0x7f1301ef

.field public static final Miuix_AppCompat_TextAppearance_Small_MenuList:I = 0x7f1301f0

.field public static final Miuix_AppCompat_TextAppearance_Spinner:I = 0x7f1301f1

.field public static final Miuix_AppCompat_TextAppearance_Spinner_Integrated:I = 0x7f1301f2

.field public static final Miuix_AppCompat_TextAppearance_Widget:I = 0x7f1301f3

.field public static final Miuix_AppCompat_TextAppearance_Widget_ActionButton:I = 0x7f1301f4

.field public static final Miuix_AppCompat_TextAppearance_Widget_ActionMode:I = 0x7f1301f5

.field public static final Miuix_AppCompat_TextAppearance_Widget_ActionMode_Button:I = 0x7f1301f6

.field public static final Miuix_AppCompat_TextAppearance_Widget_ActionMode_Title:I = 0x7f1301f7

.field public static final Miuix_AppCompat_TextAppearance_Widget_ActionMode_Title_Button:I = 0x7f1301f8

.field public static final Miuix_AppCompat_TextAppearance_Widget_Button:I = 0x7f1301f9

.field public static final Miuix_AppCompat_TextAppearance_Widget_EditText:I = 0x7f1301fa

.field public static final Miuix_AppCompat_TextAppearance_Widget_EditText_Search:I = 0x7f1301fb

.field public static final Miuix_AppCompat_TextAppearance_Widget_List:I = 0x7f1301fc

.field public static final Miuix_AppCompat_TextAppearance_Widget_List_Secondary:I = 0x7f1301fd

.field public static final Miuix_AppCompat_TextAppearance_WindowTitle:I = 0x7f1301fe

.field public static final Miuix_AppCompat_TextAppearance_WindowTitle_Expand:I = 0x7f1301ff

.field public static final Miuix_AppCompat_TextAppearance_WindowTitle_Large:I = 0x7f130200

.field public static final Miuix_AppCompat_TextAppearance_WindowTitle_Large_Subtitle:I = 0x7f130201

.field public static final Miuix_AppCompat_TextAppearance_WindowTitle_Subtitle:I = 0x7f130202

.field public static final Miuix_AppCompat_TextAppearance_WindowTitle_Subtitle_Expand:I = 0x7f130203

.field public static final Miuix_DropDownPopupList:I = 0x7f130204

.field public static final Miuix_DropDownPopupList_Dark:I = 0x7f130205

.field public static final Miuix_DropDownPopupList_DayNight:I = 0x7f130206

.field public static final Miuix_DropdownItem:I = 0x7f130207

.field public static final Miuix_DropdownItem_Dark:I = 0x7f130208

.field public static final Miuix_DropdownItem_DayNight:I = 0x7f130209

.field public static final Miuix_GuidePopupText:I = 0x7f13020a

.field public static final Miuix_GuidePopupText_Dark:I = 0x7f13020b

.field public static final Miuix_GuidePopupText_DayNight:I = 0x7f13020c

.field public static final Miuix_Preference:I = 0x7f13020d

.field public static final Miuix_PreferenceFragment:I = 0x7f13021d

.field public static final Miuix_Preference_Category:I = 0x7f13020e

.field public static final Miuix_Preference_Category_Checkable:I = 0x7f13020f

.field public static final Miuix_Preference_Category_Radio:I = 0x7f130210

.field public static final Miuix_Preference_CheckBoxPreference:I = 0x7f130211

.field public static final Miuix_Preference_DialogPreference:I = 0x7f130212

.field public static final Miuix_Preference_DialogPreference_EditTextPreference:I = 0x7f130213

.field public static final Miuix_Preference_DropDown:I = 0x7f130214

.field public static final Miuix_Preference_PreferenceScreen:I = 0x7f130215

.field public static final Miuix_Preference_RadioButtonPreference:I = 0x7f130216

.field public static final Miuix_Preference_SeekBarPreference:I = 0x7f130217

.field public static final Miuix_Preference_StretchableWidgetPreference:I = 0x7f130218

.field public static final Miuix_Preference_StretchableWidgetPreference_DateTimePicker:I = 0x7f130219

.field public static final Miuix_Preference_SwitchPreference:I = 0x7f13021a

.field public static final Miuix_Preference_SwitchPreferenceCompat:I = 0x7f13021b

.field public static final Miuix_Preference_TextPreference:I = 0x7f13021c

.field public static final Miuix_Spring_TextAppearance:I = 0x7f13021e

.field public static final Miuix_Spring_TextAppearance_indicator:I = 0x7f13021f

.field public static final Miuix_Spring_TextAppearance_trigger:I = 0x7f130220

.field public static final NavigationLayout:I = 0x7f130221

.field public static final NotificationCard:I = 0x7f130222

.field public static final NotificationCardText:I = 0x7f130223

.field public static final PanelOptionRoundedOutlinedButton:I = 0x7f130224

.field public static final PanelOptionRoundedSolidButton:I = 0x7f130225

.field public static final Passport_Widget_ProgressBar:I = 0x7f130226

.field public static final PickerDialogTheme_Settings:I = 0x7f130227

.field public static final Platform_AppCompat:I = 0x7f130228

.field public static final Platform_AppCompat_Light:I = 0x7f130229

.field public static final Platform_MaterialComponents:I = 0x7f13022a

.field public static final Platform_MaterialComponents_Dialog:I = 0x7f13022b

.field public static final Platform_MaterialComponents_Light:I = 0x7f13022c

.field public static final Platform_MaterialComponents_Light_Dialog:I = 0x7f13022d

.field public static final Platform_ThemeOverlay_AppCompat:I = 0x7f13022e

.field public static final Platform_ThemeOverlay_AppCompat_Dark:I = 0x7f13022f

.field public static final Platform_ThemeOverlay_AppCompat_Light:I = 0x7f130230

.field public static final Platform_V21_AppCompat:I = 0x7f130231

.field public static final Platform_V21_AppCompat_Light:I = 0x7f130232

.field public static final Platform_V25_AppCompat:I = 0x7f130233

.field public static final Platform_V25_AppCompat_Light:I = 0x7f130234

.field public static final Platform_Widget_AppCompat_Spinner:I = 0x7f130235

.field public static final PopupMenu_Style_Dark:I = 0x7f130236

.field public static final PopupMenu_Style_DayNight:I = 0x7f130237

.field public static final PopupMenu_Style_Light:I = 0x7f130238

.field public static final Preference:I = 0x7f130239

.field public static final PreferenceCategoryTitleTextStyle:I = 0x7f130250

.field public static final PreferenceFragment:I = 0x7f130251

.field public static final PreferenceFragmentList:I = 0x7f130253

.field public static final PreferenceFragmentListSinglePane:I = 0x7f130255

.field public static final PreferenceFragmentList_Material:I = 0x7f130254

.field public static final PreferenceFragment_Material:I = 0x7f130252

.field public static final PreferenceSummaryTextStyle:I = 0x7f130256

.field public static final PreferenceThemeOverlay:I = 0x7f130259

.field public static final PreferenceThemeOverlay_SettingsBase:I = 0x7f13025a

.field public static final PreferenceThemeOverlay_v14:I = 0x7f13025b

.field public static final PreferenceThemeOverlay_v14_Material:I = 0x7f13025c

.field public static final PreferenceTheme_SettingsLib:I = 0x7f130257

.field public static final PreferenceTheme_SetupWizard:I = 0x7f130258

.field public static final Preference_Category:I = 0x7f13023a

.field public static final Preference_Category_Material:I = 0x7f13023b

.field public static final Preference_CheckBoxPreference:I = 0x7f13023c

.field public static final Preference_CheckBoxPreference_Material:I = 0x7f13023d

.field public static final Preference_DialogPreference:I = 0x7f13023e

.field public static final Preference_DialogPreference_EditTextPreference:I = 0x7f13023f

.field public static final Preference_DialogPreference_EditTextPreference_Material:I = 0x7f130240

.field public static final Preference_DialogPreference_Material:I = 0x7f130241

.field public static final Preference_DropDown:I = 0x7f130242

.field public static final Preference_DropDown_Material:I = 0x7f130243

.field public static final Preference_Information:I = 0x7f130244

.field public static final Preference_Information_Material:I = 0x7f130245

.field public static final Preference_Material:I = 0x7f130246

.field public static final Preference_PreferenceScreen:I = 0x7f130247

.field public static final Preference_PreferenceScreen_Material:I = 0x7f130248

.field public static final Preference_RadioButtonPreference_Wifi:I = 0x7f130249

.field public static final Preference_SeekBarPreference:I = 0x7f13024a

.field public static final Preference_SeekBarPreference_Material:I = 0x7f13024b

.field public static final Preference_SwitchPreference:I = 0x7f13024c

.field public static final Preference_SwitchPreferenceCompat:I = 0x7f13024e

.field public static final Preference_SwitchPreferenceCompat_Material:I = 0x7f13024f

.field public static final Preference_SwitchPreference_Material:I = 0x7f13024d

.field public static final PreviewPagerPageIndicator:I = 0x7f13025d

.field public static final ProgressBarTextStyle:I = 0x7f13025e

.field public static final ProgressBarTextStyle_Large:I = 0x7f13025f

.field public static final ProgressBarTextStyle_Large_Dark:I = 0x7f130260

.field public static final ProgressBarTextStyle_Large_Light:I = 0x7f130261

.field public static final ProgressBarTextStyle_Small:I = 0x7f130262

.field public static final ProgressBarTextStyle_Small_Dark:I = 0x7f130263

.field public static final ProgressBarTextStyle_Small_Light:I = 0x7f130264

.field public static final Provision:I = 0x7f130265

.field public static final ProvisionBottomText:I = 0x7f130268

.field public static final ProvisionExplainTextStyle:I = 0x7f130269

.field public static final ProvisionPageSubTitleTextStyle:I = 0x7f13026a

.field public static final ProvisionPageTitleTextStyle:I = 0x7f13026b

.field public static final ProvisionStepSikpTextStyle:I = 0x7f13026c

.field public static final ProvisionStepTextStyle:I = 0x7f13026d

.field public static final Provision_InputTextView:I = 0x7f130266

.field public static final Provision_Preference:I = 0x7f130267

.field public static final QrCodeScanner:I = 0x7f13026e

.field public static final RecommendItem:I = 0x7f13026f

.field public static final RequestManageCredentialsAllowButton:I = 0x7f130270

.field public static final RequestManageCredentialsButtonPanel:I = 0x7f130271

.field public static final RequestManageCredentialsDescription:I = 0x7f130272

.field public static final RequestManageCredentialsDontAllowButton:I = 0x7f130273

.field public static final RequestManageCredentialsFab:I = 0x7f130274

.field public static final RequestManageCredentialsHeader:I = 0x7f130275

.field public static final RequestManageCredentialsHeaderLandscape:I = 0x7f130276

.field public static final RequestManageCredentialsTitle:I = 0x7f130277

.field public static final RingProgressBarStyle:I = 0x7f130278

.field public static final RingTonePreferenceStyle:I = 0x7f130279

.field public static final RoundedCornerButtonTheme:I = 0x7f13027a

.field public static final RoundedCornerThemeOverlay:I = 0x7f13027b

.field public static final RtlOverlay_DialogWindowTitle_AppCompat:I = 0x7f13027c

.field public static final RtlOverlay_Widget_AppCompat_ActionBar_TitleItem:I = 0x7f13027d

.field public static final RtlOverlay_Widget_AppCompat_DialogTitle_Icon:I = 0x7f13027e

.field public static final RtlOverlay_Widget_AppCompat_PopupMenuItem:I = 0x7f13027f

.field public static final RtlOverlay_Widget_AppCompat_PopupMenuItem_InternalGroup:I = 0x7f130280

.field public static final RtlOverlay_Widget_AppCompat_PopupMenuItem_Shortcut:I = 0x7f130281

.field public static final RtlOverlay_Widget_AppCompat_PopupMenuItem_SubmenuArrow:I = 0x7f130282

.field public static final RtlOverlay_Widget_AppCompat_PopupMenuItem_Text:I = 0x7f130283

.field public static final RtlOverlay_Widget_AppCompat_PopupMenuItem_Title:I = 0x7f130284

.field public static final RtlOverlay_Widget_AppCompat_SearchView_MagIcon:I = 0x7f13028a

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown:I = 0x7f130285

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Icon1:I = 0x7f130286

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Icon2:I = 0x7f130287

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Query:I = 0x7f130288

.field public static final RtlOverlay_Widget_AppCompat_Search_DropDown_Text:I = 0x7f130289

.field public static final RtlUnderlay_Widget_AppCompat_ActionButton:I = 0x7f13028b

.field public static final RtlUnderlay_Widget_AppCompat_ActionButton_Overflow:I = 0x7f13028c

.field public static final SearchActionBarStyle:I = 0x7f13028d

.field public static final SearchBarStyle:I = 0x7f13028e

.field public static final SecurityLockPatternView:I = 0x7f13028f

.field public static final SecurityPreferenceButton:I = 0x7f130290

.field public static final SecurityPreferenceButtonContainer:I = 0x7f130291

.field public static final SettingsBarChart:I = 0x7f130292

.field public static final SettingsBarChartBar:I = 0x7f130293

.field public static final SettingsBarChartBarIcon:I = 0x7f130294

.field public static final SettingsBarChartBarSummary:I = 0x7f130295

.field public static final SettingsBarChartBarTitle:I = 0x7f130296

.field public static final SettingsBarChartDetailsButton:I = 0x7f130297

.field public static final SettingsBarChartEmptyText:I = 0x7f130298

.field public static final SettingsBarChartTitle:I = 0x7f130299

.field public static final SettingsBarViewStyle:I = 0x7f13029a

.field public static final SettingsCategoryPreference_SettingsLib:I = 0x7f13029b

.field public static final SettingsCheckBoxPreference_SettingsLib:I = 0x7f13029c

.field public static final SettingsDropdownPreference_SettingsLib:I = 0x7f13029d

.field public static final SettingsEditTextPreference_SettingsLib:I = 0x7f13029e

.field public static final SettingsLibActionButton:I = 0x7f13029f

.field public static final SettingsLibButtonStyle:I = 0x7f1302a0

.field public static final SettingsLibRoundedCornerThemeOverlay:I = 0x7f1302a1

.field public static final SettingsLibTabsStyle:I = 0x7f1302a2

.field public static final SettingsLibTabsTextAppearance:I = 0x7f1302a3

.field public static final SettingsMultiSelectListPreference:I = 0x7f1302a4

.field public static final SettingsPreferenceFragmentStyle:I = 0x7f1302a6

.field public static final SettingsPreferenceTheme:I = 0x7f1302a7

.field public static final SettingsPreferenceTheme_SetupWizard:I = 0x7f1302a8

.field public static final SettingsPreference_SettingsLib:I = 0x7f1302a5

.field public static final SettingsSeekBarPreference:I = 0x7f1302a9

.field public static final SettingsSeekbarPreference_SettingsLib:I = 0x7f1302aa

.field public static final SettingsSpinnerDropdown:I = 0x7f1302ab

.field public static final SettingsSpinnerTitleBar:I = 0x7f1302ac

.field public static final SettingsSwitchPreference_SettingsLib:I = 0x7f1302ad

.field public static final SetupWizardAccessibilityTheme:I = 0x7f1302ae

.field public static final SetupWizardButton_Negative:I = 0x7f1302af

.field public static final SetupWizardButton_Positive:I = 0x7f1302b0

.field public static final SetupWizardPartnerResource:I = 0x7f1302b1

.field public static final SetupWizardPreferenceFragmentStyle:I = 0x7f1302b2

.field public static final SetupWizardTheme_DayNight_Transparent:I = 0x7f1302b3

.field public static final SetupWizardTheme_Light_Transparent:I = 0x7f1302b4

.field public static final SetupWizardTheme_Transparent:I = 0x7f1302b5

.field public static final Shadow_TextAppearance:I = 0x7f1302b6

.field public static final Shadow_TextAppearance_Small:I = 0x7f1302b7

.field public static final ShapeAppearanceOverlay:I = 0x7f1302d1

.field public static final ShapeAppearanceOverlay_BottomLeftDifferentCornerSize:I = 0x7f1302d2

.field public static final ShapeAppearanceOverlay_BottomRightCut:I = 0x7f1302d3

.field public static final ShapeAppearanceOverlay_Cut:I = 0x7f1302d4

.field public static final ShapeAppearanceOverlay_DifferentCornerSize:I = 0x7f1302d5

.field public static final ShapeAppearanceOverlay_Material3_Button:I = 0x7f1302d6

.field public static final ShapeAppearanceOverlay_Material3_Chip:I = 0x7f1302d7

.field public static final ShapeAppearanceOverlay_Material3_FloatingActionButton:I = 0x7f1302d8

.field public static final ShapeAppearanceOverlay_Material3_NavigationView_Item:I = 0x7f1302d9

.field public static final ShapeAppearanceOverlay_Material3_TextField_Filled:I = 0x7f1302da

.field public static final ShapeAppearanceOverlay_MaterialAlertDialog_Material3:I = 0x7f1302db

.field public static final ShapeAppearanceOverlay_MaterialComponents_BottomSheet:I = 0x7f1302dc

.field public static final ShapeAppearanceOverlay_MaterialComponents_Chip:I = 0x7f1302dd

.field public static final ShapeAppearanceOverlay_MaterialComponents_ExtendedFloatingActionButton:I = 0x7f1302de

.field public static final ShapeAppearanceOverlay_MaterialComponents_FloatingActionButton:I = 0x7f1302df

.field public static final ShapeAppearanceOverlay_MaterialComponents_MaterialCalendar_Day:I = 0x7f1302e0

.field public static final ShapeAppearanceOverlay_MaterialComponents_MaterialCalendar_Window_Fullscreen:I = 0x7f1302e1

.field public static final ShapeAppearanceOverlay_MaterialComponents_MaterialCalendar_Year:I = 0x7f1302e2

.field public static final ShapeAppearanceOverlay_MaterialComponents_TextInputLayout_FilledBox:I = 0x7f1302e3

.field public static final ShapeAppearanceOverlay_TopLeftCut:I = 0x7f1302e4

.field public static final ShapeAppearanceOverlay_TopRightDifferentCornerSize:I = 0x7f1302e5

.field public static final ShapeAppearance_M3_Sys_Shape_Corner_ExtraLarge:I = 0x7f1302b8

.field public static final ShapeAppearance_M3_Sys_Shape_Corner_ExtraLarge_Top:I = 0x7f1302b9

.field public static final ShapeAppearance_M3_Sys_Shape_Corner_ExtraSmall:I = 0x7f1302ba

.field public static final ShapeAppearance_M3_Sys_Shape_Corner_ExtraSmall_Top:I = 0x7f1302bb

.field public static final ShapeAppearance_M3_Sys_Shape_Corner_Full:I = 0x7f1302bc

.field public static final ShapeAppearance_M3_Sys_Shape_Corner_Large:I = 0x7f1302bd

.field public static final ShapeAppearance_M3_Sys_Shape_Corner_Large_End:I = 0x7f1302be

.field public static final ShapeAppearance_M3_Sys_Shape_Corner_Large_Top:I = 0x7f1302bf

.field public static final ShapeAppearance_M3_Sys_Shape_Corner_Medium:I = 0x7f1302c0

.field public static final ShapeAppearance_M3_Sys_Shape_Corner_None:I = 0x7f1302c1

.field public static final ShapeAppearance_M3_Sys_Shape_Corner_Small:I = 0x7f1302c2

.field public static final ShapeAppearance_M3_Sys_Shape_Large:I = 0x7f1302c3

.field public static final ShapeAppearance_M3_Sys_Shape_Medium:I = 0x7f1302c4

.field public static final ShapeAppearance_M3_Sys_Shape_Small:I = 0x7f1302c5

.field public static final ShapeAppearance_Material3_LargeComponent:I = 0x7f1302c6

.field public static final ShapeAppearance_Material3_MediumComponent:I = 0x7f1302c7

.field public static final ShapeAppearance_Material3_NavigationBarView_ActiveIndicator:I = 0x7f1302c8

.field public static final ShapeAppearance_Material3_SmallComponent:I = 0x7f1302c9

.field public static final ShapeAppearance_Material3_Tooltip:I = 0x7f1302ca

.field public static final ShapeAppearance_MaterialComponents:I = 0x7f1302cb

.field public static final ShapeAppearance_MaterialComponents_LargeComponent:I = 0x7f1302cc

.field public static final ShapeAppearance_MaterialComponents_MediumComponent:I = 0x7f1302cd

.field public static final ShapeAppearance_MaterialComponents_SmallComponent:I = 0x7f1302ce

.field public static final ShapeAppearance_MaterialComponents_Test:I = 0x7f1302cf

.field public static final ShapeAppearance_MaterialComponents_Tooltip:I = 0x7f1302d0

.field public static final ShowTitleTheme:I = 0x7f1302e6

.field public static final SimConfirmDialog_ButtonBarStyle:I = 0x7f1302e7

.field public static final SimConfirmDialog_OutlineButton:I = 0x7f1302e8

.field public static final SlicePreference:I = 0x7f1302e9

.field public static final SliceRow:I = 0x7f1302ea

.field public static final SliceRow_Settings:I = 0x7f1302eb

.field public static final SliceRow_Slider:I = 0x7f1302ec

.field public static final SliceRow_Slider_LargeIcon:I = 0x7f1302ed

.field public static final SpinnerDropDownItem_SettingsLib:I = 0x7f1302ef

.field public static final SpinnerItem_SettingsLib:I = 0x7f1302f0

.field public static final Spinner_SettingsLib:I = 0x7f1302ee

.field public static final StateEditTextPasswordStyle:I = 0x7f1302f1

.field public static final StorageButtonStyle:I = 0x7f1302f2

.field public static final StorageImageStyle1:I = 0x7f1302f3

.field public static final StorageImageStyle2:I = 0x7f1302f4

.field public static final StorageTextStyle1:I = 0x7f1302f5

.field public static final StorageTextStyle2:I = 0x7f1302f6

.field public static final StorageTextStyle3:I = 0x7f1302f7

.field public static final SucPartnerCustomizationButtonBar:I = 0x7f1302fa

.field public static final SucPartnerCustomizationButtonBar_Stackable:I = 0x7f1302fb

.field public static final SucPartnerCustomizationButton_Primary:I = 0x7f1302f8

.field public static final SucPartnerCustomizationButton_Secondary:I = 0x7f1302f9

.field public static final SudAlertDialogTheme:I = 0x7f1302fc

.field public static final SudAlertDialogThemeCompat:I = 0x7f1302fe

.field public static final SudAlertDialogThemeCompat_Light:I = 0x7f1302ff

.field public static final SudAlertDialogTheme_Light:I = 0x7f1302fd

.field public static final SudAppCompatButtonButtonBarAlertDialog:I = 0x7f130300

.field public static final SudAppCompatButtonButtonBarAlertDialog_Light:I = 0x7f130301

.field public static final SudBaseCardTitle:I = 0x7f130303

.field public static final SudBaseHeaderTitle:I = 0x7f130304

.field public static final SudBaseThemeGlif:I = 0x7f130305

.field public static final SudBaseThemeGlifV3:I = 0x7f130307

.field public static final SudBaseThemeGlifV3_Light:I = 0x7f130308

.field public static final SudBaseThemeGlif_Light:I = 0x7f130306

.field public static final SudBase_ProgressBarLarge:I = 0x7f130302

.field public static final SudButtonItem:I = 0x7f130309

.field public static final SudButtonItem_Colored:I = 0x7f13030a

.field public static final SudCardTitle:I = 0x7f13030b

.field public static final SudCheckBox:I = 0x7f13030c

.field public static final SudCheckBox_Multiline:I = 0x7f13030d

.field public static final SudContentFrame:I = 0x7f13030f

.field public static final SudContentIllustration:I = 0x7f130310

.field public static final SudContent_Glif:I = 0x7f13030e

.field public static final SudDateTimePickerDialogTheme:I = 0x7f130311

.field public static final SudDateTimePickerDialogTheme_Light:I = 0x7f130312

.field public static final SudDescription:I = 0x7f130313

.field public static final SudDescription_Glif:I = 0x7f130314

.field public static final SudDeviceDefaultButtonBarButtonStyle:I = 0x7f130315

.field public static final SudDeviceDefaultButtonBarButtonStyle_Light:I = 0x7f130316

.field public static final SudDeviceDefaultWindowTitleTextAppearance:I = 0x7f130317

.field public static final SudDynamicColorAlertDialogThemeCompat:I = 0x7f130318

.field public static final SudDynamicColorAlertDialogThemeCompat_Light:I = 0x7f130319

.field public static final SudDynamicColorBaseTheme:I = 0x7f13031a

.field public static final SudDynamicColorBaseTheme_Light:I = 0x7f13031b

.field public static final SudDynamicColorDateTimePickerDialogTheme:I = 0x7f13031c

.field public static final SudDynamicColorDateTimePickerDialogTheme_Light:I = 0x7f13031d

.field public static final SudDynamicColorTheme:I = 0x7f13031e

.field public static final SudDynamicColorThemeGlifV3:I = 0x7f130321

.field public static final SudDynamicColorThemeGlifV3_DayNight:I = 0x7f130322

.field public static final SudDynamicColorThemeGlifV3_Light:I = 0x7f130323

.field public static final SudDynamicColorTheme_DayNight:I = 0x7f13031f

.field public static final SudDynamicColorTheme_Light:I = 0x7f130320

.field public static final SudEditBoxTheme:I = 0x7f130324

.field public static final SudEditText:I = 0x7f130325

.field public static final SudFillContentLayout:I = 0x7f130326

.field public static final SudFourColorIndeterminateProgressBar:I = 0x7f130327

.field public static final SudFullDynamicColorAlertDialogThemeCompat:I = 0x7f130328

.field public static final SudFullDynamicColorAlertDialogThemeCompat_Light:I = 0x7f130329

.field public static final SudFullDynamicColorTheme:I = 0x7f13032a

.field public static final SudFullDynamicColorThemeGlifV3:I = 0x7f13032d

.field public static final SudFullDynamicColorThemeGlifV3_DayNight:I = 0x7f13032e

.field public static final SudFullDynamicColorThemeGlifV3_Light:I = 0x7f13032f

.field public static final SudFullDynamicColorTheme_DayNight:I = 0x7f13032b

.field public static final SudFullDynamicColorTheme_Light:I = 0x7f13032c

.field public static final SudGlifButtonBar:I = 0x7f130334

.field public static final SudGlifButtonBar_Stackable:I = 0x7f130335

.field public static final SudGlifButton_BaseTertiary:I = 0x7f130330

.field public static final SudGlifButton_Primary:I = 0x7f130331

.field public static final SudGlifButton_Secondary:I = 0x7f130332

.field public static final SudGlifButton_Tertiary:I = 0x7f130333

.field public static final SudGlifCardBackground:I = 0x7f130336

.field public static final SudGlifCardContainer:I = 0x7f130337

.field public static final SudGlifDescription:I = 0x7f130338

.field public static final SudGlifDescriptionMaterialYou:I = 0x7f130339

.field public static final SudGlifHeaderContainer:I = 0x7f13033a

.field public static final SudGlifHeaderTitle:I = 0x7f13033b

.field public static final SudGlifHeaderTitleMaterialYou:I = 0x7f13033c

.field public static final SudGlifIcon:I = 0x7f13033d

.field public static final SudGlifIconContainer:I = 0x7f13033e

.field public static final SudHeaderTitle:I = 0x7f13033f

.field public static final SudInfoContainer:I = 0x7f130340

.field public static final SudInfoDescription:I = 0x7f130341

.field public static final SudItemContainer:I = 0x7f130342

.field public static final SudItemContainerMaterialYou:I = 0x7f130346

.field public static final SudItemContainerMaterialYou_Description:I = 0x7f130347

.field public static final SudItemContainer_Description:I = 0x7f130343

.field public static final SudItemContainer_Description_Glif:I = 0x7f130344

.field public static final SudItemContainer_Verbose:I = 0x7f130345

.field public static final SudItemIconContainer:I = 0x7f130348

.field public static final SudItemSummary:I = 0x7f130349

.field public static final SudItemSummaryGlif:I = 0x7f13034a

.field public static final SudItemSummaryMaterialYou:I = 0x7f13034b

.field public static final SudItemTitle:I = 0x7f13034c

.field public static final SudItemTitleMaterialYou:I = 0x7f130350

.field public static final SudItemTitle_GlifDescription:I = 0x7f13034d

.field public static final SudItemTitle_SectionHeader:I = 0x7f13034e

.field public static final SudItemTitle_Verbose:I = 0x7f13034f

.field public static final SudLandContentContianerStyle:I = 0x7f130351

.field public static final SudLoadingContentFrame:I = 0x7f130352

.field public static final SudMaterialYouAlertDialogTheme:I = 0x7f130353

.field public static final SudMaterialYouAlertDialogThemeCompat:I = 0x7f130355

.field public static final SudMaterialYouAlertDialogThemeCompat_Light:I = 0x7f130356

.field public static final SudMaterialYouAlertDialogTheme_Light:I = 0x7f130354

.field public static final SudMaterialYouAlertDialogTitleStyle:I = 0x7f130357

.field public static final SudMaterialYouDeviceDefaultWindowTitleTextAppearance:I = 0x7f130358

.field public static final SudMaterialYouItemTitle_Verbose:I = 0x7f130359

.field public static final SudMaterialYouWindowTitleStyle:I = 0x7f13035a

.field public static final SudNavBarButtonStyle:I = 0x7f13035b

.field public static final SudNavBarTheme:I = 0x7f13035c

.field public static final SudNavBarThemeDark:I = 0x7f13035d

.field public static final SudNavBarThemeLight:I = 0x7f13035e

.field public static final SudRadioButton:I = 0x7f13035f

.field public static final SudSwitchBarStyle:I = 0x7f130360

.field public static final SudSwitchStyle:I = 0x7f130361

.field public static final SudSwitchStyle_Divided:I = 0x7f130362

.field public static final SudTextAppearanceDeviceDefaultMedium:I = 0x7f130363

.field public static final SudTextAppearanceDeviceDefaultMedium_Light:I = 0x7f130364

.field public static final SudTextFieldAutoFilledTheme:I = 0x7f130365

.field public static final SudThemeGlif:I = 0x7f130366

.field public static final SudThemeGlifV2:I = 0x7f130369

.field public static final SudThemeGlifV2_DayNight:I = 0x7f13036a

.field public static final SudThemeGlifV2_Light:I = 0x7f13036b

.field public static final SudThemeGlifV3:I = 0x7f13036c

.field public static final SudThemeGlifV3_DayNight:I = 0x7f13036d

.field public static final SudThemeGlifV3_Light:I = 0x7f13036e

.field public static final SudThemeGlifV4:I = 0x7f13036f

.field public static final SudThemeGlifV4_DayNight:I = 0x7f130370

.field public static final SudThemeGlifV4_Light:I = 0x7f130371

.field public static final SudThemeGlif_DayNight:I = 0x7f130367

.field public static final SudThemeGlif_Light:I = 0x7f130368

.field public static final SudThemeMaterial:I = 0x7f130372

.field public static final SudThemeMaterial_DayNight:I = 0x7f130373

.field public static final SudThemeMaterial_Light:I = 0x7f130374

.field public static final SudWindowTitleStyle:I = 0x7f130375

.field public static final SuggestionCardIcon:I = 0x7f130376

.field public static final SuggestionCardText:I = 0x7f130377

.field public static final SuwAlertDialogThemeCompat:I = 0x7f130378

.field public static final SuwAlertDialogThemeCompat_DayNight:I = 0x7f130379

.field public static final SuwAlertDialogThemeCompat_Light:I = 0x7f13037a

.field public static final SwitchBar_Switch_Settingslib:I = 0x7f13037c

.field public static final Switch_SettingsLib:I = 0x7f13037b

.field public static final SyncSwitchPreference:I = 0x7f13037d

.field public static final TabletProvisionBottomText:I = 0x7f13037e

.field public static final TestStyleWithLineHeight:I = 0x7f130384

.field public static final TestStyleWithLineHeightAppearance:I = 0x7f130385

.field public static final TestStyleWithThemeLineHeightAttribute:I = 0x7f130386

.field public static final TestStyleWithoutLineHeight:I = 0x7f130387

.field public static final TestThemeWithLineHeight:I = 0x7f130388

.field public static final TestThemeWithLineHeightDisabled:I = 0x7f130389

.field public static final Test_ShapeAppearanceOverlay_MaterialComponents_MaterialCalendar_Day:I = 0x7f13037f

.field public static final Test_Theme_MaterialComponents_MaterialCalendar:I = 0x7f130380

.field public static final Test_Widget_MaterialComponents_MaterialCalendar:I = 0x7f130381

.field public static final Test_Widget_MaterialComponents_MaterialCalendar_Day:I = 0x7f130382

.field public static final Test_Widget_MaterialComponents_MaterialCalendar_Day_Selected:I = 0x7f130383

.field public static final TextAppearance:I = 0x7f13038a

.field public static final TextAppearanceBroadcastDialogButton:I = 0x7f130452

.field public static final TextAppearanceBroadcastDialogSubTitle:I = 0x7f130453

.field public static final TextAppearanceBroadcastDialogTitle:I = 0x7f130454

.field public static final TextAppearanceInfo:I = 0x7f130455

.field public static final TextAppearanceMedium:I = 0x7f130456

.field public static final TextAppearanceSmall:I = 0x7f130457

.field public static final TextAppearance_AdminDialogMessage:I = 0x7f13038b

.field public static final TextAppearance_AdminDialogTitle:I = 0x7f13038c

.field public static final TextAppearance_AppCompat:I = 0x7f13038d

.field public static final TextAppearance_AppCompat_Body1:I = 0x7f13038e

.field public static final TextAppearance_AppCompat_Body2:I = 0x7f13038f

.field public static final TextAppearance_AppCompat_Button:I = 0x7f130390

.field public static final TextAppearance_AppCompat_Caption:I = 0x7f130391

.field public static final TextAppearance_AppCompat_Display1:I = 0x7f130392

.field public static final TextAppearance_AppCompat_Display2:I = 0x7f130393

.field public static final TextAppearance_AppCompat_Display3:I = 0x7f130394

.field public static final TextAppearance_AppCompat_Display4:I = 0x7f130395

.field public static final TextAppearance_AppCompat_Headline:I = 0x7f130396

.field public static final TextAppearance_AppCompat_Inverse:I = 0x7f130397

.field public static final TextAppearance_AppCompat_Large:I = 0x7f130398

.field public static final TextAppearance_AppCompat_Large_Inverse:I = 0x7f130399

.field public static final TextAppearance_AppCompat_Light_SearchResult_Subtitle:I = 0x7f13039a

.field public static final TextAppearance_AppCompat_Light_SearchResult_Title:I = 0x7f13039b

.field public static final TextAppearance_AppCompat_Light_Widget_PopupMenu_Large:I = 0x7f13039c

.field public static final TextAppearance_AppCompat_Light_Widget_PopupMenu_Small:I = 0x7f13039d

.field public static final TextAppearance_AppCompat_Medium:I = 0x7f13039e

.field public static final TextAppearance_AppCompat_Medium_Inverse:I = 0x7f13039f

.field public static final TextAppearance_AppCompat_Menu:I = 0x7f1303a0

.field public static final TextAppearance_AppCompat_SearchResult_Subtitle:I = 0x7f1303a1

.field public static final TextAppearance_AppCompat_SearchResult_Title:I = 0x7f1303a2

.field public static final TextAppearance_AppCompat_Small:I = 0x7f1303a3

.field public static final TextAppearance_AppCompat_Small_Inverse:I = 0x7f1303a4

.field public static final TextAppearance_AppCompat_Subhead:I = 0x7f1303a5

.field public static final TextAppearance_AppCompat_Subhead_Inverse:I = 0x7f1303a6

.field public static final TextAppearance_AppCompat_Title:I = 0x7f1303a7

.field public static final TextAppearance_AppCompat_Title_Inverse:I = 0x7f1303a8

.field public static final TextAppearance_AppCompat_Tooltip:I = 0x7f1303a9

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Menu:I = 0x7f1303aa

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Subtitle:I = 0x7f1303ab

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse:I = 0x7f1303ac

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Title:I = 0x7f1303ad

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse:I = 0x7f1303ae

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Subtitle:I = 0x7f1303af

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Subtitle_Inverse:I = 0x7f1303b0

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Title:I = 0x7f1303b1

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Title_Inverse:I = 0x7f1303b2

.field public static final TextAppearance_AppCompat_Widget_Button:I = 0x7f1303b3

.field public static final TextAppearance_AppCompat_Widget_Button_Borderless_Colored:I = 0x7f1303b4

.field public static final TextAppearance_AppCompat_Widget_Button_Colored:I = 0x7f1303b5

.field public static final TextAppearance_AppCompat_Widget_Button_Inverse:I = 0x7f1303b6

.field public static final TextAppearance_AppCompat_Widget_DropDownItem:I = 0x7f1303b7

.field public static final TextAppearance_AppCompat_Widget_PopupMenu_Header:I = 0x7f1303b8

.field public static final TextAppearance_AppCompat_Widget_PopupMenu_Large:I = 0x7f1303b9

.field public static final TextAppearance_AppCompat_Widget_PopupMenu_Small:I = 0x7f1303ba

.field public static final TextAppearance_AppCompat_Widget_Switch:I = 0x7f1303bb

.field public static final TextAppearance_AppCompat_Widget_TextView_SpinnerItem:I = 0x7f1303bc

.field public static final TextAppearance_AppInfo_Medium:I = 0x7f1303bd

.field public static final TextAppearance_AppInfo_Small:I = 0x7f1303be

.field public static final TextAppearance_CategoryTitle:I = 0x7f1303bf

.field public static final TextAppearance_CategoryTitle_SettingsLib:I = 0x7f1303c0

.field public static final TextAppearance_Compat_Notification:I = 0x7f1303c1

.field public static final TextAppearance_Compat_Notification_Info:I = 0x7f1303c2

.field public static final TextAppearance_Compat_Notification_Info_Media:I = 0x7f1303c3

.field public static final TextAppearance_Compat_Notification_Line2:I = 0x7f1303c4

.field public static final TextAppearance_Compat_Notification_Line2_Media:I = 0x7f1303c5

.field public static final TextAppearance_Compat_Notification_Media:I = 0x7f1303c6

.field public static final TextAppearance_Compat_Notification_Time:I = 0x7f1303c7

.field public static final TextAppearance_Compat_Notification_Time_Media:I = 0x7f1303c8

.field public static final TextAppearance_Compat_Notification_Title:I = 0x7f1303c9

.field public static final TextAppearance_Compat_Notification_Title_Media:I = 0x7f1303ca

.field public static final TextAppearance_ConditionCardSummary:I = 0x7f1303cb

.field public static final TextAppearance_ContextualCardDismissalText:I = 0x7f1303cc

.field public static final TextAppearance_Contributors:I = 0x7f1303cd

.field public static final TextAppearance_Design_CollapsingToolbar_Expanded:I = 0x7f1303ce

.field public static final TextAppearance_Design_Counter:I = 0x7f1303cf

.field public static final TextAppearance_Design_Counter_Overflow:I = 0x7f1303d0

.field public static final TextAppearance_Design_Error:I = 0x7f1303d1

.field public static final TextAppearance_Design_HelperText:I = 0x7f1303d2

.field public static final TextAppearance_Design_Hint:I = 0x7f1303d3

.field public static final TextAppearance_Design_Placeholder:I = 0x7f1303d4

.field public static final TextAppearance_Design_Prefix:I = 0x7f1303d5

.field public static final TextAppearance_Design_Snackbar_Message:I = 0x7f1303d6

.field public static final TextAppearance_Design_Suffix:I = 0x7f1303d7

.field public static final TextAppearance_Design_Tab:I = 0x7f1303d8

.field public static final TextAppearance_DialogMessage:I = 0x7f1303d9

.field public static final TextAppearance_DropDownPopupListItem:I = 0x7f1303da

.field public static final TextAppearance_DropDownPopupListItem_SingleChoice:I = 0x7f1303db

.field public static final TextAppearance_DropDownPopupListItem_SingleChoice_Dark:I = 0x7f1303dc

.field public static final TextAppearance_EntityHeaderSummary:I = 0x7f1303dd

.field public static final TextAppearance_EntityHeaderTitle:I = 0x7f1303de

.field public static final TextAppearance_ErrorText:I = 0x7f1303df

.field public static final TextAppearance_Footer_Title_SettingsLib:I = 0x7f1303e0

.field public static final TextAppearance_FreeWifi:I = 0x7f1303e1

.field public static final TextAppearance_GuidePopupItem:I = 0x7f1303e2

.field public static final TextAppearance_GuidePopupItem_Dark:I = 0x7f1303e3

.field public static final TextAppearance_HeadLineFontFamily:I = 0x7f1303e4

.field public static final TextAppearance_HeadLineFontFamily_Subhead:I = 0x7f1303e5

.field public static final TextAppearance_HomepageCardTitle:I = 0x7f1303e6

.field public static final TextAppearance_M3_Sys_Typescale_BodyLarge:I = 0x7f1303e7

.field public static final TextAppearance_M3_Sys_Typescale_BodyMedium:I = 0x7f1303e8

.field public static final TextAppearance_M3_Sys_Typescale_BodySmall:I = 0x7f1303e9

.field public static final TextAppearance_M3_Sys_Typescale_DisplayLarge:I = 0x7f1303ea

.field public static final TextAppearance_M3_Sys_Typescale_DisplayMedium:I = 0x7f1303eb

.field public static final TextAppearance_M3_Sys_Typescale_DisplaySmall:I = 0x7f1303ec

.field public static final TextAppearance_M3_Sys_Typescale_HeadlineLarge:I = 0x7f1303ed

.field public static final TextAppearance_M3_Sys_Typescale_HeadlineMedium:I = 0x7f1303ee

.field public static final TextAppearance_M3_Sys_Typescale_HeadlineSmall:I = 0x7f1303ef

.field public static final TextAppearance_M3_Sys_Typescale_LabelLarge:I = 0x7f1303f0

.field public static final TextAppearance_M3_Sys_Typescale_LabelMedium:I = 0x7f1303f1

.field public static final TextAppearance_M3_Sys_Typescale_LabelSmall:I = 0x7f1303f2

.field public static final TextAppearance_M3_Sys_Typescale_TitleLarge:I = 0x7f1303f3

.field public static final TextAppearance_M3_Sys_Typescale_TitleMedium:I = 0x7f1303f4

.field public static final TextAppearance_M3_Sys_Typescale_TitleSmall:I = 0x7f1303f5

.field public static final TextAppearance_Material3_ActionBar_Subtitle:I = 0x7f1303f6

.field public static final TextAppearance_Material3_ActionBar_Title:I = 0x7f1303f7

.field public static final TextAppearance_Material3_BodyLarge:I = 0x7f1303f8

.field public static final TextAppearance_Material3_BodyMedium:I = 0x7f1303f9

.field public static final TextAppearance_Material3_BodySmall:I = 0x7f1303fa

.field public static final TextAppearance_Material3_DisplayLarge:I = 0x7f1303fb

.field public static final TextAppearance_Material3_DisplayMedium:I = 0x7f1303fc

.field public static final TextAppearance_Material3_DisplaySmall:I = 0x7f1303fd

.field public static final TextAppearance_Material3_HeadlineLarge:I = 0x7f1303fe

.field public static final TextAppearance_Material3_HeadlineMedium:I = 0x7f1303ff

.field public static final TextAppearance_Material3_HeadlineSmall:I = 0x7f130400

.field public static final TextAppearance_Material3_LabelLarge:I = 0x7f130401

.field public static final TextAppearance_Material3_LabelMedium:I = 0x7f130402

.field public static final TextAppearance_Material3_LabelSmall:I = 0x7f130403

.field public static final TextAppearance_Material3_MaterialTimePicker_Title:I = 0x7f130404

.field public static final TextAppearance_Material3_TitleLarge:I = 0x7f130405

.field public static final TextAppearance_Material3_TitleMedium:I = 0x7f130406

.field public static final TextAppearance_Material3_TitleSmall:I = 0x7f130407

.field public static final TextAppearance_MaterialComponents_Badge:I = 0x7f130408

.field public static final TextAppearance_MaterialComponents_Body1:I = 0x7f130409

.field public static final TextAppearance_MaterialComponents_Body2:I = 0x7f13040a

.field public static final TextAppearance_MaterialComponents_Button:I = 0x7f13040b

.field public static final TextAppearance_MaterialComponents_Caption:I = 0x7f13040c

.field public static final TextAppearance_MaterialComponents_Chip:I = 0x7f13040d

.field public static final TextAppearance_MaterialComponents_Headline1:I = 0x7f13040e

.field public static final TextAppearance_MaterialComponents_Headline2:I = 0x7f13040f

.field public static final TextAppearance_MaterialComponents_Headline3:I = 0x7f130410

.field public static final TextAppearance_MaterialComponents_Headline4:I = 0x7f130411

.field public static final TextAppearance_MaterialComponents_Headline5:I = 0x7f130412

.field public static final TextAppearance_MaterialComponents_Headline6:I = 0x7f130413

.field public static final TextAppearance_MaterialComponents_Overline:I = 0x7f130414

.field public static final TextAppearance_MaterialComponents_Subtitle1:I = 0x7f130415

.field public static final TextAppearance_MaterialComponents_Subtitle2:I = 0x7f130416

.field public static final TextAppearance_MaterialComponents_TimePicker_Title:I = 0x7f130417

.field public static final TextAppearance_MaterialComponents_Tooltip:I = 0x7f130418

.field public static final TextAppearance_MediaRouter_Dynamic_Body:I = 0x7f130419

.field public static final TextAppearance_MediaRouter_Dynamic_Body_Light:I = 0x7f13041a

.field public static final TextAppearance_MediaRouter_Dynamic_Header:I = 0x7f13041b

.field public static final TextAppearance_MediaRouter_Dynamic_Header_Light:I = 0x7f13041c

.field public static final TextAppearance_MediaRouter_Dynamic_Metadata_PrimaryText:I = 0x7f13041d

.field public static final TextAppearance_MediaRouter_Dynamic_Metadata_SecondaryText:I = 0x7f13041e

.field public static final TextAppearance_MediaRouter_PrimaryText:I = 0x7f13041f

.field public static final TextAppearance_MediaRouter_SecondaryText:I = 0x7f130420

.field public static final TextAppearance_MediaRouter_Title:I = 0x7f130421

.field public static final TextAppearance_Medium:I = 0x7f130422

.field public static final TextAppearance_Medium_Dialog_Light:I = 0x7f130423

.field public static final TextAppearance_NotificationHistory:I = 0x7f130424

.field public static final TextAppearance_NotificationHistory_AppName:I = 0x7f130425

.field public static final TextAppearance_NotificationImportanceButton:I = 0x7f130426

.field public static final TextAppearance_NotificationImportanceButton_Selected:I = 0x7f130427

.field public static final TextAppearance_NotificationImportanceButton_Unselected:I = 0x7f130428

.field public static final TextAppearance_NotificationImportanceDetail:I = 0x7f130429

.field public static final TextAppearance_PasswordEntry:I = 0x7f13042a

.field public static final TextAppearance_PreferenceSummary_SettingsLib:I = 0x7f13042b

.field public static final TextAppearance_PreferenceTitle_SettingsLib:I = 0x7f13042c

.field public static final TextAppearance_Primary:I = 0x7f13042d

.field public static final TextAppearance_RemoveDialogContent:I = 0x7f13042e

.field public static final TextAppearance_SearchBar:I = 0x7f13042f

.field public static final TextAppearance_Secondary:I = 0x7f130430

.field public static final TextAppearance_SimConfirmDialogList:I = 0x7f130431

.field public static final TextAppearance_SimConfirmDialogList_Summary:I = 0x7f130432

.field public static final TextAppearance_Small:I = 0x7f130433

.field public static final TextAppearance_Small_SwitchBar:I = 0x7f130434

.field public static final TextAppearance_SubTitle:I = 0x7f130435

.field public static final TextAppearance_SudCardTitle:I = 0x7f130436

.field public static final TextAppearance_SudDescription:I = 0x7f130437

.field public static final TextAppearance_SudDescription_Secondary:I = 0x7f130438

.field public static final TextAppearance_SudGlifBody:I = 0x7f130439

.field public static final TextAppearance_SudGlifItemSummary:I = 0x7f13043a

.field public static final TextAppearance_SudGlifItemTitle:I = 0x7f13043b

.field public static final TextAppearance_SudItemSummary:I = 0x7f13043c

.field public static final TextAppearance_SudMaterialYouDescription:I = 0x7f13043d

.field public static final TextAppearance_SudMaterialYouItemSummary:I = 0x7f13043e

.field public static final TextAppearance_SudMaterialYouItemTitle:I = 0x7f13043f

.field public static final TextAppearance_SuggestionSummary:I = 0x7f130440

.field public static final TextAppearance_SuggestionTitle:I = 0x7f130441

.field public static final TextAppearance_Switch:I = 0x7f130442

.field public static final TextAppearance_Tab:I = 0x7f130443

.field public static final TextAppearance_Test_NoTextSize:I = 0x7f130444

.field public static final TextAppearance_Test_UsesDp:I = 0x7f130445

.field public static final TextAppearance_Test_UsesSP:I = 0x7f130446

.field public static final TextAppearance_Title_Big:I = 0x7f130447

.field public static final TextAppearance_Title_Medium:I = 0x7f130448

.field public static final TextAppearance_Title_Small:I = 0x7f130449

.field public static final TextAppearance_TopIntroText:I = 0x7f13044a

.field public static final TextAppearance_Widget_AppCompat_ExpandedMenu_Item:I = 0x7f13044b

.field public static final TextAppearance_Widget_AppCompat_Toolbar_Subtitle:I = 0x7f13044c

.field public static final TextAppearance_Widget_AppCompat_Toolbar_Title:I = 0x7f13044d

.field public static final TextAppearance_ZenOnboardingButton:I = 0x7f13044e

.field public static final TextAppearance_info_label:I = 0x7f13044f

.field public static final TextAppearance_info_small:I = 0x7f130450

.field public static final TextAppearance_info_value:I = 0x7f130451

.field public static final Theme:I = 0x7f130458

.field public static final ThemeCameraNaviBar:I = 0x7f130539

.field public static final ThemeMiuiSettings_Main:I = 0x7f13053a

.field public static final ThemeMiuiSettings_Main_NoActionBar:I = 0x7f13053b

.field public static final ThemeMiuiSettings_Main_NoTitle:I = 0x7f13053c

.field public static final ThemeMiuiSettings_MiuiSettings:I = 0x7f13053d

.field public static final ThemeOverlayColorAccentRed:I = 0x7f1305a5

.field public static final ThemeOverlay_AlertDialog:I = 0x7f13053e

.field public static final ThemeOverlay_AppCompat:I = 0x7f13053f

.field public static final ThemeOverlay_AppCompat_ActionBar:I = 0x7f130540

.field public static final ThemeOverlay_AppCompat_Dark:I = 0x7f130541

.field public static final ThemeOverlay_AppCompat_Dark_ActionBar:I = 0x7f130542

.field public static final ThemeOverlay_AppCompat_DayNight:I = 0x7f130543

.field public static final ThemeOverlay_AppCompat_DayNight_ActionBar:I = 0x7f130544

.field public static final ThemeOverlay_AppCompat_Dialog:I = 0x7f130545

.field public static final ThemeOverlay_AppCompat_Dialog_Alert:I = 0x7f130546

.field public static final ThemeOverlay_AppCompat_Light:I = 0x7f130547

.field public static final ThemeOverlay_Design_TextInputEditText:I = 0x7f130548

.field public static final ThemeOverlay_Material3:I = 0x7f130549

.field public static final ThemeOverlay_Material3_ActionBar:I = 0x7f13054a

.field public static final ThemeOverlay_Material3_AutoCompleteTextView:I = 0x7f13054b

.field public static final ThemeOverlay_Material3_AutoCompleteTextView_FilledBox:I = 0x7f13054c

.field public static final ThemeOverlay_Material3_AutoCompleteTextView_FilledBox_Dense:I = 0x7f13054d

.field public static final ThemeOverlay_Material3_AutoCompleteTextView_OutlinedBox:I = 0x7f13054e

.field public static final ThemeOverlay_Material3_AutoCompleteTextView_OutlinedBox_Dense:I = 0x7f13054f

.field public static final ThemeOverlay_Material3_BottomAppBar:I = 0x7f130550

.field public static final ThemeOverlay_Material3_BottomSheetDialog:I = 0x7f130551

.field public static final ThemeOverlay_Material3_Button:I = 0x7f130552

.field public static final ThemeOverlay_Material3_Button_ElevatedButton:I = 0x7f130553

.field public static final ThemeOverlay_Material3_Button_TextButton:I = 0x7f130554

.field public static final ThemeOverlay_Material3_Button_TextButton_Snackbar:I = 0x7f130555

.field public static final ThemeOverlay_Material3_Button_TonalButton:I = 0x7f130556

.field public static final ThemeOverlay_Material3_Chip:I = 0x7f130557

.field public static final ThemeOverlay_Material3_Chip_Assist:I = 0x7f130558

.field public static final ThemeOverlay_Material3_Dark:I = 0x7f130559

.field public static final ThemeOverlay_Material3_Dark_ActionBar:I = 0x7f13055a

.field public static final ThemeOverlay_Material3_DayNight_BottomSheetDialog:I = 0x7f13055b

.field public static final ThemeOverlay_Material3_Dialog:I = 0x7f13055c

.field public static final ThemeOverlay_Material3_Dialog_Alert:I = 0x7f13055d

.field public static final ThemeOverlay_Material3_Dialog_Alert_Framework:I = 0x7f13055e

.field public static final ThemeOverlay_Material3_DynamicColors_Dark:I = 0x7f13055f

.field public static final ThemeOverlay_Material3_DynamicColors_DayNight:I = 0x7f130560

.field public static final ThemeOverlay_Material3_DynamicColors_Light:I = 0x7f130561

.field public static final ThemeOverlay_Material3_FloatingActionButton_Primary:I = 0x7f130562

.field public static final ThemeOverlay_Material3_FloatingActionButton_Secondary:I = 0x7f130563

.field public static final ThemeOverlay_Material3_FloatingActionButton_Surface:I = 0x7f130564

.field public static final ThemeOverlay_Material3_FloatingActionButton_Tertiary:I = 0x7f130565

.field public static final ThemeOverlay_Material3_HarmonizedColors:I = 0x7f130566

.field public static final ThemeOverlay_Material3_Light:I = 0x7f130567

.field public static final ThemeOverlay_Material3_Light_Dialog_Alert_Framework:I = 0x7f130568

.field public static final ThemeOverlay_Material3_MaterialAlertDialog:I = 0x7f130569

.field public static final ThemeOverlay_Material3_MaterialAlertDialog_Centered:I = 0x7f13056a

.field public static final ThemeOverlay_Material3_MaterialCalendar:I = 0x7f13056b

.field public static final ThemeOverlay_Material3_MaterialCalendar_Fullscreen:I = 0x7f13056c

.field public static final ThemeOverlay_Material3_MaterialCalendar_HeaderCancelButton:I = 0x7f13056d

.field public static final ThemeOverlay_Material3_MaterialTimePicker:I = 0x7f13056e

.field public static final ThemeOverlay_Material3_MaterialTimePicker_Display_TextInputEditText:I = 0x7f13056f

.field public static final ThemeOverlay_Material3_NavigationView:I = 0x7f130570

.field public static final ThemeOverlay_Material3_Snackbar:I = 0x7f130571

.field public static final ThemeOverlay_Material3_TextInputEditText:I = 0x7f130572

.field public static final ThemeOverlay_Material3_TextInputEditText_FilledBox:I = 0x7f130573

.field public static final ThemeOverlay_Material3_TextInputEditText_FilledBox_Dense:I = 0x7f130574

.field public static final ThemeOverlay_Material3_TextInputEditText_OutlinedBox:I = 0x7f130575

.field public static final ThemeOverlay_Material3_TextInputEditText_OutlinedBox_Dense:I = 0x7f130576

.field public static final ThemeOverlay_Material3_Toolbar_Surface:I = 0x7f130577

.field public static final ThemeOverlay_MaterialAlertDialog_Material3_Title_Icon:I = 0x7f130578

.field public static final ThemeOverlay_MaterialComponents:I = 0x7f130579

.field public static final ThemeOverlay_MaterialComponents_ActionBar:I = 0x7f13057a

.field public static final ThemeOverlay_MaterialComponents_ActionBar_Primary:I = 0x7f13057b

.field public static final ThemeOverlay_MaterialComponents_ActionBar_Surface:I = 0x7f13057c

.field public static final ThemeOverlay_MaterialComponents_AutoCompleteTextView:I = 0x7f13057d

.field public static final ThemeOverlay_MaterialComponents_AutoCompleteTextView_FilledBox:I = 0x7f13057e

.field public static final ThemeOverlay_MaterialComponents_AutoCompleteTextView_FilledBox_Dense:I = 0x7f13057f

.field public static final ThemeOverlay_MaterialComponents_AutoCompleteTextView_OutlinedBox:I = 0x7f130580

.field public static final ThemeOverlay_MaterialComponents_AutoCompleteTextView_OutlinedBox_Dense:I = 0x7f130581

.field public static final ThemeOverlay_MaterialComponents_BottomAppBar_Primary:I = 0x7f130582

.field public static final ThemeOverlay_MaterialComponents_BottomAppBar_Surface:I = 0x7f130583

.field public static final ThemeOverlay_MaterialComponents_BottomSheetDialog:I = 0x7f130584

.field public static final ThemeOverlay_MaterialComponents_Dark:I = 0x7f130585

.field public static final ThemeOverlay_MaterialComponents_Dark_ActionBar:I = 0x7f130586

.field public static final ThemeOverlay_MaterialComponents_DayNight_BottomSheetDialog:I = 0x7f130587

.field public static final ThemeOverlay_MaterialComponents_Dialog:I = 0x7f130588

.field public static final ThemeOverlay_MaterialComponents_Dialog_Alert:I = 0x7f130589

.field public static final ThemeOverlay_MaterialComponents_Dialog_Alert_Framework:I = 0x7f13058a

.field public static final ThemeOverlay_MaterialComponents_Light:I = 0x7f13058b

.field public static final ThemeOverlay_MaterialComponents_Light_Dialog_Alert_Framework:I = 0x7f13058c

.field public static final ThemeOverlay_MaterialComponents_MaterialAlertDialog:I = 0x7f13058d

.field public static final ThemeOverlay_MaterialComponents_MaterialAlertDialog_Centered:I = 0x7f13058e

.field public static final ThemeOverlay_MaterialComponents_MaterialAlertDialog_Picker_Date:I = 0x7f13058f

.field public static final ThemeOverlay_MaterialComponents_MaterialAlertDialog_Picker_Date_Calendar:I = 0x7f130590

.field public static final ThemeOverlay_MaterialComponents_MaterialAlertDialog_Picker_Date_Header_Text:I = 0x7f130591

.field public static final ThemeOverlay_MaterialComponents_MaterialAlertDialog_Picker_Date_Header_Text_Day:I = 0x7f130592

.field public static final ThemeOverlay_MaterialComponents_MaterialAlertDialog_Picker_Date_Spinner:I = 0x7f130593

.field public static final ThemeOverlay_MaterialComponents_MaterialCalendar:I = 0x7f130594

.field public static final ThemeOverlay_MaterialComponents_MaterialCalendar_Fullscreen:I = 0x7f130595

.field public static final ThemeOverlay_MaterialComponents_TextInputEditText:I = 0x7f130596

.field public static final ThemeOverlay_MaterialComponents_TextInputEditText_FilledBox:I = 0x7f130597

.field public static final ThemeOverlay_MaterialComponents_TextInputEditText_FilledBox_Dense:I = 0x7f130598

.field public static final ThemeOverlay_MaterialComponents_TextInputEditText_OutlinedBox:I = 0x7f130599

.field public static final ThemeOverlay_MaterialComponents_TextInputEditText_OutlinedBox_Dense:I = 0x7f13059a

.field public static final ThemeOverlay_MaterialComponents_TimePicker:I = 0x7f13059b

.field public static final ThemeOverlay_MaterialComponents_TimePicker_Display:I = 0x7f13059c

.field public static final ThemeOverlay_MaterialComponents_TimePicker_Display_TextInputEditText:I = 0x7f13059d

.field public static final ThemeOverlay_MaterialComponents_Toolbar_Popup_Primary:I = 0x7f13059e

.field public static final ThemeOverlay_MaterialComponents_Toolbar_Primary:I = 0x7f13059f

.field public static final ThemeOverlay_MaterialComponents_Toolbar_Surface:I = 0x7f1305a0

.field public static final ThemeOverlay_MediaRouter_Dark:I = 0x7f1305a1

.field public static final ThemeOverlay_MediaRouter_Light:I = 0x7f1305a2

.field public static final ThemeOverlay_SwitchBar_Settings:I = 0x7f1305a3

.field public static final ThemeOverlay_SwitchBar_Settings_Base:I = 0x7f1305a4

.field public static final Theme_AccessibilityTutorialActivity:I = 0x7f130459

.field public static final Theme_ActionBarOverlay:I = 0x7f13045a

.field public static final Theme_AlertActivity:I = 0x7f13045b

.field public static final Theme_AlertDialog:I = 0x7f13045c

.field public static final Theme_AlertDialog_Base:I = 0x7f13045d

.field public static final Theme_AlertDialog_SettingsLib:I = 0x7f13045e

.field public static final Theme_AlertDialog_SimConfirmDialog:I = 0x7f13045f

.field public static final Theme_AppCompat:I = 0x7f130460

.field public static final Theme_AppCompat_CompactMenu:I = 0x7f130461

.field public static final Theme_AppCompat_DayNight:I = 0x7f130462

.field public static final Theme_AppCompat_DayNight_DarkActionBar:I = 0x7f130463

.field public static final Theme_AppCompat_DayNight_Dialog:I = 0x7f130464

.field public static final Theme_AppCompat_DayNight_DialogWhenLarge:I = 0x7f130467

.field public static final Theme_AppCompat_DayNight_Dialog_Alert:I = 0x7f130465

.field public static final Theme_AppCompat_DayNight_Dialog_MinWidth:I = 0x7f130466

.field public static final Theme_AppCompat_DayNight_NoActionBar:I = 0x7f130468

.field public static final Theme_AppCompat_Dialog:I = 0x7f130469

.field public static final Theme_AppCompat_DialogWhenLarge:I = 0x7f13046c

.field public static final Theme_AppCompat_Dialog_Alert:I = 0x7f13046a

.field public static final Theme_AppCompat_Dialog_MinWidth:I = 0x7f13046b

.field public static final Theme_AppCompat_Empty:I = 0x7f13046d

.field public static final Theme_AppCompat_Light:I = 0x7f13046e

.field public static final Theme_AppCompat_Light_DarkActionBar:I = 0x7f13046f

.field public static final Theme_AppCompat_Light_Dialog:I = 0x7f130470

.field public static final Theme_AppCompat_Light_DialogWhenLarge:I = 0x7f130473

.field public static final Theme_AppCompat_Light_Dialog_Alert:I = 0x7f130471

.field public static final Theme_AppCompat_Light_Dialog_MinWidth:I = 0x7f130472

.field public static final Theme_AppCompat_Light_NoActionBar:I = 0x7f130474

.field public static final Theme_AppCompat_NoActionBar:I = 0x7f130475

.field public static final Theme_AppLaunchSettings:I = 0x7f130476

.field public static final Theme_BluetoothPermission:I = 0x7f130477

.field public static final Theme_CollapsingToolbar_Settings:I = 0x7f130478

.field public static final Theme_ConfirmDeviceCredentials:I = 0x7f130479

.field public static final Theme_ConfirmDeviceCredentialsDark:I = 0x7f13047a

.field public static final Theme_ConfirmDeviceCredentialsWork:I = 0x7f13047b

.field public static final Theme_Contributors:I = 0x7f13047c

.field public static final Theme_CreateShortCut:I = 0x7f13047d

.field public static final Theme_Dark:I = 0x7f13047e

.field public static final Theme_Dark_ActionBar_NoTitle:I = 0x7f13047f

.field public static final Theme_Dark_Dialog:I = 0x7f130480

.field public static final Theme_Dark_Dialog_Alert:I = 0x7f130481

.field public static final Theme_Dark_Dialog_Edit:I = 0x7f130482

.field public static final Theme_Dark_Dialog_Edit_Default:I = 0x7f130483

.field public static final Theme_Dark_Dialog_FixedSize:I = 0x7f130484

.field public static final Theme_Dark_Dialog_FixedSize_Small:I = 0x7f130485

.field public static final Theme_Dark_Dialog_NoTitle:I = 0x7f130486

.field public static final Theme_Dark_FloatingWindow:I = 0x7f130487

.field public static final Theme_Dark_Navigation:I = 0x7f130488

.field public static final Theme_Dark_NoTitle:I = 0x7f130489

.field public static final Theme_Dark_Settings:I = 0x7f13048a

.field public static final Theme_Dark_Settings_NoTitle:I = 0x7f13048b

.field public static final Theme_DayNight:I = 0x7f13048c

.field public static final Theme_DayNight_ActionBar_NoTitle:I = 0x7f13048d

.field public static final Theme_DayNight_ApplicationLockImmersion:I = 0x7f13048e

.field public static final Theme_DayNight_Dialog:I = 0x7f13048f

.field public static final Theme_DayNight_Dialog_Alert:I = 0x7f130490

.field public static final Theme_DayNight_Dialog_Edit:I = 0x7f130491

.field public static final Theme_DayNight_Dialog_Edit_Default:I = 0x7f130492

.field public static final Theme_DayNight_Dialog_FixedSize:I = 0x7f130493

.field public static final Theme_DayNight_Dialog_FixedSize_Small:I = 0x7f130494

.field public static final Theme_DayNight_Dialog_NoTitle:I = 0x7f130495

.field public static final Theme_DayNight_FloatingWindow:I = 0x7f130496

.field public static final Theme_DayNight_Main:I = 0x7f130497

.field public static final Theme_DayNight_Navigation:I = 0x7f130498

.field public static final Theme_DayNight_NoTitile_Transparent:I = 0x7f130499

.field public static final Theme_DayNight_NoTitile_Transparent_Dialog:I = 0x7f13049a

.field public static final Theme_DayNight_NoTitle:I = 0x7f13049b

.field public static final Theme_DayNight_NoTitle_Translucent:I = 0x7f13049c

.field public static final Theme_DayNight_Normal:I = 0x7f13049d

.field public static final Theme_DayNight_Panel:I = 0x7f13049e

.field public static final Theme_DayNight_Settings:I = 0x7f13049f

.field public static final Theme_DayNight_Settings_DisablePreview:I = 0x7f1304a0

.field public static final Theme_DayNight_Settings_NoTitle:I = 0x7f1304a1

.field public static final Theme_DayNight_Settings_NoTitle_DisablePreview:I = 0x7f1304a2

.field public static final Theme_DayNight_Show_NoTitle:I = 0x7f1304a3

.field public static final Theme_DayNight_Show_NoTitle_NoBackground:I = 0x7f1304a4

.field public static final Theme_DayNight_Show_Tab:I = 0x7f1304a5

.field public static final Theme_DayNight_Show_Tab_ShowTitle:I = 0x7f1304a6

.field public static final Theme_Design:I = 0x7f1304a7

.field public static final Theme_Design_BottomSheetDialog:I = 0x7f1304a8

.field public static final Theme_Design_Light:I = 0x7f1304a9

.field public static final Theme_Design_Light_BottomSheetDialog:I = 0x7f1304aa

.field public static final Theme_Design_Light_NoActionBar:I = 0x7f1304ab

.field public static final Theme_Design_NoActionBar:I = 0x7f1304ac

.field public static final Theme_EdgeModeSetting:I = 0x7f1304ad

.field public static final Theme_EditModeTitle:I = 0x7f1304ae

.field public static final Theme_FreeWifi_Login:I = 0x7f1304af

.field public static final Theme_Light:I = 0x7f1304b0

.field public static final Theme_Light_ActionBar_NoTitle:I = 0x7f1304b1

.field public static final Theme_Light_Dialog:I = 0x7f1304b2

.field public static final Theme_Light_Dialog_Alert:I = 0x7f1304b3

.field public static final Theme_Light_Dialog_Edit:I = 0x7f1304b4

.field public static final Theme_Light_Dialog_Edit_Default:I = 0x7f1304b5

.field public static final Theme_Light_Dialog_FixedSize:I = 0x7f1304b6

.field public static final Theme_Light_Dialog_FixedSize_Small:I = 0x7f1304b7

.field public static final Theme_Light_Dialog_NoTitle:I = 0x7f1304b8

.field public static final Theme_Light_FloatingWindow:I = 0x7f1304b9

.field public static final Theme_Light_Navigation:I = 0x7f1304ba

.field public static final Theme_Light_NoTitile_Transparent_Dialog:I = 0x7f1304bb

.field public static final Theme_Light_NoTitle:I = 0x7f1304bc

.field public static final Theme_Light_NoTitle_Translucent:I = 0x7f1304bd

.field public static final Theme_Light_Settings:I = 0x7f1304be

.field public static final Theme_Light_Settings_NoTitle:I = 0x7f1304bf

.field public static final Theme_ListView_WithDivider:I = 0x7f1304c0

.field public static final Theme_LocalePickerWithRegionActivity:I = 0x7f1304c1

.field public static final Theme_MTP:I = 0x7f1304c2

.field public static final Theme_MTP_Picker:I = 0x7f1304c3

.field public static final Theme_Material3_Dark:I = 0x7f1304c4

.field public static final Theme_Material3_Dark_BottomSheetDialog:I = 0x7f1304c5

.field public static final Theme_Material3_Dark_Dialog:I = 0x7f1304c6

.field public static final Theme_Material3_Dark_DialogWhenLarge:I = 0x7f1304c9

.field public static final Theme_Material3_Dark_Dialog_Alert:I = 0x7f1304c7

.field public static final Theme_Material3_Dark_Dialog_MinWidth:I = 0x7f1304c8

.field public static final Theme_Material3_Dark_NoActionBar:I = 0x7f1304ca

.field public static final Theme_Material3_DayNight:I = 0x7f1304cb

.field public static final Theme_Material3_DayNight_BottomSheetDialog:I = 0x7f1304cc

.field public static final Theme_Material3_DayNight_Dialog:I = 0x7f1304cd

.field public static final Theme_Material3_DayNight_DialogWhenLarge:I = 0x7f1304d0

.field public static final Theme_Material3_DayNight_Dialog_Alert:I = 0x7f1304ce

.field public static final Theme_Material3_DayNight_Dialog_MinWidth:I = 0x7f1304cf

.field public static final Theme_Material3_DayNight_NoActionBar:I = 0x7f1304d1

.field public static final Theme_Material3_DynamicColors_Dark:I = 0x7f1304d2

.field public static final Theme_Material3_DynamicColors_DayNight:I = 0x7f1304d3

.field public static final Theme_Material3_DynamicColors_Light:I = 0x7f1304d4

.field public static final Theme_Material3_Light:I = 0x7f1304d5

.field public static final Theme_Material3_Light_BottomSheetDialog:I = 0x7f1304d6

.field public static final Theme_Material3_Light_Dialog:I = 0x7f1304d7

.field public static final Theme_Material3_Light_DialogWhenLarge:I = 0x7f1304da

.field public static final Theme_Material3_Light_Dialog_Alert:I = 0x7f1304d8

.field public static final Theme_Material3_Light_Dialog_MinWidth:I = 0x7f1304d9

.field public static final Theme_Material3_Light_NoActionBar:I = 0x7f1304db

.field public static final Theme_MaterialComponents:I = 0x7f1304dc

.field public static final Theme_MaterialComponents_BottomSheetDialog:I = 0x7f1304dd

.field public static final Theme_MaterialComponents_Bridge:I = 0x7f1304de

.field public static final Theme_MaterialComponents_CompactMenu:I = 0x7f1304df

.field public static final Theme_MaterialComponents_DayNight:I = 0x7f1304e0

.field public static final Theme_MaterialComponents_DayNight_BottomSheetDialog:I = 0x7f1304e1

.field public static final Theme_MaterialComponents_DayNight_Bridge:I = 0x7f1304e2

.field public static final Theme_MaterialComponents_DayNight_DarkActionBar:I = 0x7f1304e3

.field public static final Theme_MaterialComponents_DayNight_DarkActionBar_Bridge:I = 0x7f1304e4

.field public static final Theme_MaterialComponents_DayNight_Dialog:I = 0x7f1304e5

.field public static final Theme_MaterialComponents_DayNight_DialogWhenLarge:I = 0x7f1304ed

.field public static final Theme_MaterialComponents_DayNight_Dialog_Alert:I = 0x7f1304e6

.field public static final Theme_MaterialComponents_DayNight_Dialog_Alert_Bridge:I = 0x7f1304e7

.field public static final Theme_MaterialComponents_DayNight_Dialog_Bridge:I = 0x7f1304e8

.field public static final Theme_MaterialComponents_DayNight_Dialog_FixedSize:I = 0x7f1304e9

.field public static final Theme_MaterialComponents_DayNight_Dialog_FixedSize_Bridge:I = 0x7f1304ea

.field public static final Theme_MaterialComponents_DayNight_Dialog_MinWidth:I = 0x7f1304eb

.field public static final Theme_MaterialComponents_DayNight_Dialog_MinWidth_Bridge:I = 0x7f1304ec

.field public static final Theme_MaterialComponents_DayNight_NoActionBar:I = 0x7f1304ee

.field public static final Theme_MaterialComponents_DayNight_NoActionBar_Bridge:I = 0x7f1304ef

.field public static final Theme_MaterialComponents_Dialog:I = 0x7f1304f0

.field public static final Theme_MaterialComponents_DialogWhenLarge:I = 0x7f1304f8

.field public static final Theme_MaterialComponents_Dialog_Alert:I = 0x7f1304f1

.field public static final Theme_MaterialComponents_Dialog_Alert_Bridge:I = 0x7f1304f2

.field public static final Theme_MaterialComponents_Dialog_Bridge:I = 0x7f1304f3

.field public static final Theme_MaterialComponents_Dialog_FixedSize:I = 0x7f1304f4

.field public static final Theme_MaterialComponents_Dialog_FixedSize_Bridge:I = 0x7f1304f5

.field public static final Theme_MaterialComponents_Dialog_MinWidth:I = 0x7f1304f6

.field public static final Theme_MaterialComponents_Dialog_MinWidth_Bridge:I = 0x7f1304f7

.field public static final Theme_MaterialComponents_Light:I = 0x7f1304f9

.field public static final Theme_MaterialComponents_Light_BarSize:I = 0x7f1304fa

.field public static final Theme_MaterialComponents_Light_BottomSheetDialog:I = 0x7f1304fb

.field public static final Theme_MaterialComponents_Light_Bridge:I = 0x7f1304fc

.field public static final Theme_MaterialComponents_Light_DarkActionBar:I = 0x7f1304fd

.field public static final Theme_MaterialComponents_Light_DarkActionBar_Bridge:I = 0x7f1304fe

.field public static final Theme_MaterialComponents_Light_Dialog:I = 0x7f1304ff

.field public static final Theme_MaterialComponents_Light_DialogWhenLarge:I = 0x7f130507

.field public static final Theme_MaterialComponents_Light_Dialog_Alert:I = 0x7f130500

.field public static final Theme_MaterialComponents_Light_Dialog_Alert_Bridge:I = 0x7f130501

.field public static final Theme_MaterialComponents_Light_Dialog_Bridge:I = 0x7f130502

.field public static final Theme_MaterialComponents_Light_Dialog_FixedSize:I = 0x7f130503

.field public static final Theme_MaterialComponents_Light_Dialog_FixedSize_Bridge:I = 0x7f130504

.field public static final Theme_MaterialComponents_Light_Dialog_MinWidth:I = 0x7f130505

.field public static final Theme_MaterialComponents_Light_Dialog_MinWidth_Bridge:I = 0x7f130506

.field public static final Theme_MaterialComponents_Light_LargeTouch:I = 0x7f130508

.field public static final Theme_MaterialComponents_Light_NoActionBar:I = 0x7f130509

.field public static final Theme_MaterialComponents_Light_NoActionBar_Bridge:I = 0x7f13050a

.field public static final Theme_MaterialComponents_NoActionBar:I = 0x7f13050b

.field public static final Theme_MaterialComponents_NoActionBar_Bridge:I = 0x7f13050c

.field public static final Theme_MediaRouter:I = 0x7f13050d

.field public static final Theme_MediaRouter_Light:I = 0x7f13050e

.field public static final Theme_MediaRouter_LightControlPanel:I = 0x7f130510

.field public static final Theme_MediaRouter_Light_DarkControlPanel:I = 0x7f13050f

.field public static final Theme_MiuiSettings_NoTitle_Transparent:I = 0x7f130511

.field public static final Theme_MiuiSettings_StorageWizard:I = 0x7f130512

.field public static final Theme_Navigation:I = 0x7f130513

.field public static final Theme_NoDisplay:I = 0x7f130514

.field public static final Theme_NoStartingWindow:I = 0x7f130515

.field public static final Theme_Panel:I = 0x7f130516

.field public static final Theme_PreferenceOverlay_Dark:I = 0x7f130517

.field public static final Theme_PreferenceOverlay_DayNight:I = 0x7f130518

.field public static final Theme_PreferenceOverlay_Floating_Dark:I = 0x7f130519

.field public static final Theme_PreferenceOverlay_Floating_DayNight:I = 0x7f13051a

.field public static final Theme_PreferenceOverlay_Floating_Light:I = 0x7f13051b

.field public static final Theme_PreferenceOverlay_Light:I = 0x7f13051c

.field public static final Theme_Provision_Notitle:I = 0x7f13051d

.field public static final Theme_Provision_Notitle_WifiSettings:I = 0x7f13051e

.field public static final Theme_ProvisioningActivity:I = 0x7f13051f

.field public static final Theme_Radio:I = 0x7f130520

.field public static final Theme_ScreenEffect:I = 0x7f130521

.field public static final Theme_Settings:I = 0x7f130522

.field public static final Theme_SettingsBase:I = 0x7f130528

.field public static final Theme_SettingsBase_v31:I = 0x7f130529

.field public static final Theme_Settings_ContextualCard:I = 0x7f130523

.field public static final Theme_Settings_Home:I = 0x7f130524

.field public static final Theme_Settings_HomeBase:I = 0x7f130526

.field public static final Theme_Settings_Home_NoAnimation:I = 0x7f130525

.field public static final Theme_Settings_NoActionBar:I = 0x7f130527

.field public static final Theme_Sos_Alert_Dialog:I = 0x7f13052a

.field public static final Theme_StorageManagerActivity:I = 0x7f13052b

.field public static final Theme_SubSettings:I = 0x7f13052c

.field public static final Theme_SubSettingsBase:I = 0x7f13052e

.field public static final Theme_SubSettings_Base:I = 0x7f13052d

.field public static final Theme_TabTheme:I = 0x7f13052f

.field public static final Theme_Translucent:I = 0x7f130530

.field public static final Theme_Translucent_NoTitleBar:I = 0x7f130531

.field public static final Theme_Translucent_Settings:I = 0x7f130532

.field public static final Theme_Translucent_Settings_NoTitle:I = 0x7f130533

.field public static final Theme_USB_AlertActivity:I = 0x7f130534

.field public static final Theme_WifiDialog:I = 0x7f130535

.field public static final Theme_WifiSettings:I = 0x7f130536

.field public static final Theme_WifiSettings_showTitle:I = 0x7f130537

.field public static final Theme_finddevice_wifi_dialog:I = 0x7f130538

.field public static final TimeLabel:I = 0x7f1305a6

.field public static final TimezoneSearchAlphabetIndexerStyle:I = 0x7f1305a7

.field public static final TimezoneSearchTheme:I = 0x7f1305a8

.field public static final Transparent:I = 0x7f1305a9

.field public static final TrimmedHorizontalProgressBar:I = 0x7f1305aa

.field public static final TrustedCredentialsList:I = 0x7f1305ab

.field public static final TwoStateButtonPreference:I = 0x7f1305ac

.field public static final V12_PreferenceThemeOverlay:I = 0x7f1305ad

.field public static final V12_PreferenceThemeOverlay_Dark:I = 0x7f1305ae

.field public static final V12_PreferenceThemeOverlay_Floating_Dark:I = 0x7f1305af

.field public static final V12_PreferenceThemeOverlay_Floating_Light:I = 0x7f1305b0

.field public static final V12_PreferenceThemeOverlay_Light:I = 0x7f1305b1

.field public static final Widget:I = 0x7f1305b2

.field public static final WidgetEditText:I = 0x7f1307d5

.field public static final WidgetStateEditText:I = 0x7f1307d6

.field public static final Widget_ActionBar:I = 0x7f1305b3

.field public static final Widget_ActionBarButtonIconStyle:I = 0x7f1305cc

.field public static final Widget_ActionBarButtonTextStyle:I = 0x7f1305cd

.field public static final Widget_ActionBarButtonTextStyle_Dark:I = 0x7f1305ce

.field public static final Widget_ActionBarButtonTextStyle_Light:I = 0x7f1305cf

.field public static final Widget_ActionBarMovableLayout:I = 0x7f1305d0

.field public static final Widget_ActionBar_Base:I = 0x7f1305b4

.field public static final Widget_ActionBar_EditModeTitle:I = 0x7f1305b5

.field public static final Widget_ActionBar_NoTitle:I = 0x7f1305b6

.field public static final Widget_ActionBar_Settings:I = 0x7f1305b7

.field public static final Widget_ActionBar_Split:I = 0x7f1305b8

.field public static final Widget_ActionBar_SubSettings:I = 0x7f1305b9

.field public static final Widget_ActionBar_TabBar:I = 0x7f1305ba

.field public static final Widget_ActionBar_TabBar_Expand:I = 0x7f1305bb

.field public static final Widget_ActionBar_TabBar_Secondary:I = 0x7f1305bc

.field public static final Widget_ActionBar_TabText:I = 0x7f1305bd

.field public static final Widget_ActionBar_TabText_Dark:I = 0x7f1305be

.field public static final Widget_ActionBar_TabText_Expand:I = 0x7f1305bf

.field public static final Widget_ActionBar_TabText_Expand_Dark:I = 0x7f1305c0

.field public static final Widget_ActionBar_TabText_Expand_Light:I = 0x7f1305c1

.field public static final Widget_ActionBar_TabText_Light:I = 0x7f1305c2

.field public static final Widget_ActionBar_TabText_Secondary_Collapse:I = 0x7f1305c3

.field public static final Widget_ActionBar_TabText_Secondary_Collapse_Dark:I = 0x7f1305c4

.field public static final Widget_ActionBar_TabText_Secondary_Collapse_Light:I = 0x7f1305c5

.field public static final Widget_ActionBar_TabText_Secondary_Expand:I = 0x7f1305c6

.field public static final Widget_ActionBar_TabText_Secondary_Expand_Dark:I = 0x7f1305c7

.field public static final Widget_ActionBar_TabText_Secondary_Expand_Light:I = 0x7f1305c8

.field public static final Widget_ActionBar_TabView:I = 0x7f1305c9

.field public static final Widget_ActionBar_TabView_Expand:I = 0x7f1305ca

.field public static final Widget_ActionBar_TabView_Secondary:I = 0x7f1305cb

.field public static final Widget_ActionButton:I = 0x7f1305d1

.field public static final Widget_ActionButton_Dark:I = 0x7f1305d2

.field public static final Widget_ActionButton_Light:I = 0x7f1305d3

.field public static final Widget_ActionButton_Overflow_Dark:I = 0x7f1305d4

.field public static final Widget_ActionButton_Overflow_Light:I = 0x7f1305d5

.field public static final Widget_ActionMenu:I = 0x7f1305d6

.field public static final Widget_ActionMode:I = 0x7f1305d7

.field public static final Widget_ActionMode_ActionButton:I = 0x7f1305d8

.field public static final Widget_ActionMode_ActionButton_Dark:I = 0x7f1305d9

.field public static final Widget_ActionMode_ActionButton_Light:I = 0x7f1305da

.field public static final Widget_ActionMode_ActionButton_Overflow:I = 0x7f1305db

.field public static final Widget_ActionMode_ActionButton_Overflow_Dark:I = 0x7f1305dc

.field public static final Widget_ActionMode_ActionButton_Overflow_Light:I = 0x7f1305dd

.field public static final Widget_ActionMode_Button:I = 0x7f1305de

.field public static final Widget_ActionMode_Button_Dark:I = 0x7f1305df

.field public static final Widget_ActionMode_Button_Default:I = 0x7f1305e0

.field public static final Widget_ActionMode_Button_Default_Dark:I = 0x7f1305e1

.field public static final Widget_ActionMode_Button_Default_Light:I = 0x7f1305e2

.field public static final Widget_ActionMode_Button_Light:I = 0x7f1305e3

.field public static final Widget_AlertDialogListItem:I = 0x7f1305e4

.field public static final Widget_AlertDialogListItem_MultiChoice:I = 0x7f1305e5

.field public static final Widget_AlertDialogListItem_SingleChoice:I = 0x7f1305e6

.field public static final Widget_AlphabetIndexer:I = 0x7f1305e7

.field public static final Widget_AlphabetIndexer_Dark:I = 0x7f1305e8

.field public static final Widget_AlphabetIndexer_Dark_Starred:I = 0x7f1305e9

.field public static final Widget_AlphabetIndexer_DayNight:I = 0x7f1305ea

.field public static final Widget_AlphabetIndexer_Starred:I = 0x7f1305eb

.field public static final Widget_AlphabetIndexer_Starred_DayNight:I = 0x7f1305ec

.field public static final Widget_AppCompat_ActionBar:I = 0x7f1305ed

.field public static final Widget_AppCompat_ActionBar_Solid:I = 0x7f1305ee

.field public static final Widget_AppCompat_ActionBar_TabBar:I = 0x7f1305ef

.field public static final Widget_AppCompat_ActionBar_TabText:I = 0x7f1305f0

.field public static final Widget_AppCompat_ActionBar_TabView:I = 0x7f1305f1

.field public static final Widget_AppCompat_ActionButton:I = 0x7f1305f2

.field public static final Widget_AppCompat_ActionButton_CloseMode:I = 0x7f1305f3

.field public static final Widget_AppCompat_ActionButton_Overflow:I = 0x7f1305f4

.field public static final Widget_AppCompat_ActionMode:I = 0x7f1305f5

.field public static final Widget_AppCompat_ActivityChooserView:I = 0x7f1305f6

.field public static final Widget_AppCompat_AutoCompleteTextView:I = 0x7f1305f7

.field public static final Widget_AppCompat_Button:I = 0x7f1305f8

.field public static final Widget_AppCompat_ButtonBar:I = 0x7f1305fe

.field public static final Widget_AppCompat_ButtonBar_AlertDialog:I = 0x7f1305ff

.field public static final Widget_AppCompat_Button_Borderless:I = 0x7f1305f9

.field public static final Widget_AppCompat_Button_Borderless_Colored:I = 0x7f1305fa

.field public static final Widget_AppCompat_Button_ButtonBar_AlertDialog:I = 0x7f1305fb

.field public static final Widget_AppCompat_Button_Colored:I = 0x7f1305fc

.field public static final Widget_AppCompat_Button_Small:I = 0x7f1305fd

.field public static final Widget_AppCompat_CompoundButton_CheckBox:I = 0x7f130600

.field public static final Widget_AppCompat_CompoundButton_RadioButton:I = 0x7f130601

.field public static final Widget_AppCompat_CompoundButton_Switch:I = 0x7f130602

.field public static final Widget_AppCompat_DrawerArrowToggle:I = 0x7f130603

.field public static final Widget_AppCompat_DropDownItem_Spinner:I = 0x7f130604

.field public static final Widget_AppCompat_EditText:I = 0x7f130605

.field public static final Widget_AppCompat_ImageButton:I = 0x7f130606

.field public static final Widget_AppCompat_Light_ActionBar:I = 0x7f130607

.field public static final Widget_AppCompat_Light_ActionBar_Solid:I = 0x7f130608

.field public static final Widget_AppCompat_Light_ActionBar_Solid_Inverse:I = 0x7f130609

.field public static final Widget_AppCompat_Light_ActionBar_TabBar:I = 0x7f13060a

.field public static final Widget_AppCompat_Light_ActionBar_TabBar_Inverse:I = 0x7f13060b

.field public static final Widget_AppCompat_Light_ActionBar_TabText:I = 0x7f13060c

.field public static final Widget_AppCompat_Light_ActionBar_TabText_Inverse:I = 0x7f13060d

.field public static final Widget_AppCompat_Light_ActionBar_TabView:I = 0x7f13060e

.field public static final Widget_AppCompat_Light_ActionBar_TabView_Inverse:I = 0x7f13060f

.field public static final Widget_AppCompat_Light_ActionButton:I = 0x7f130610

.field public static final Widget_AppCompat_Light_ActionButton_CloseMode:I = 0x7f130611

.field public static final Widget_AppCompat_Light_ActionButton_Overflow:I = 0x7f130612

.field public static final Widget_AppCompat_Light_ActionMode_Inverse:I = 0x7f130613

.field public static final Widget_AppCompat_Light_ActivityChooserView:I = 0x7f130614

.field public static final Widget_AppCompat_Light_AutoCompleteTextView:I = 0x7f130615

.field public static final Widget_AppCompat_Light_DropDownItem_Spinner:I = 0x7f130616

.field public static final Widget_AppCompat_Light_ListPopupWindow:I = 0x7f130617

.field public static final Widget_AppCompat_Light_ListView_DropDown:I = 0x7f130618

.field public static final Widget_AppCompat_Light_PopupMenu:I = 0x7f130619

.field public static final Widget_AppCompat_Light_PopupMenu_Overflow:I = 0x7f13061a

.field public static final Widget_AppCompat_Light_SearchView:I = 0x7f13061b

.field public static final Widget_AppCompat_Light_Spinner_DropDown_ActionBar:I = 0x7f13061c

.field public static final Widget_AppCompat_ListMenuView:I = 0x7f13061d

.field public static final Widget_AppCompat_ListPopupWindow:I = 0x7f13061e

.field public static final Widget_AppCompat_ListView:I = 0x7f13061f

.field public static final Widget_AppCompat_ListView_DropDown:I = 0x7f130620

.field public static final Widget_AppCompat_ListView_Menu:I = 0x7f130621

.field public static final Widget_AppCompat_PopupMenu:I = 0x7f130622

.field public static final Widget_AppCompat_PopupMenu_Overflow:I = 0x7f130623

.field public static final Widget_AppCompat_PopupWindow:I = 0x7f130624

.field public static final Widget_AppCompat_ProgressBar:I = 0x7f130625

.field public static final Widget_AppCompat_ProgressBar_Horizontal:I = 0x7f130626

.field public static final Widget_AppCompat_ProgressBar_Inline:I = 0x7f130627

.field public static final Widget_AppCompat_RatingBar:I = 0x7f130628

.field public static final Widget_AppCompat_RatingBar_Indicator:I = 0x7f130629

.field public static final Widget_AppCompat_RatingBar_Small:I = 0x7f13062a

.field public static final Widget_AppCompat_SearchView:I = 0x7f13062b

.field public static final Widget_AppCompat_SearchView_ActionBar:I = 0x7f13062c

.field public static final Widget_AppCompat_SeekBar:I = 0x7f13062d

.field public static final Widget_AppCompat_SeekBar_Discrete:I = 0x7f13062e

.field public static final Widget_AppCompat_SeekBar_Inline:I = 0x7f13062f

.field public static final Widget_AppCompat_Spinner:I = 0x7f130630

.field public static final Widget_AppCompat_Spinner_DropDown:I = 0x7f130631

.field public static final Widget_AppCompat_Spinner_DropDown_ActionBar:I = 0x7f130632

.field public static final Widget_AppCompat_Spinner_Underlined:I = 0x7f130633

.field public static final Widget_AppCompat_TextView:I = 0x7f130634

.field public static final Widget_AppCompat_TextView_SpinnerItem:I = 0x7f130635

.field public static final Widget_AppCompat_Toolbar:I = 0x7f130636

.field public static final Widget_AppCompat_Toolbar_Button_Navigation:I = 0x7f130637

.field public static final Widget_ArrowPopupView:I = 0x7f130638

.field public static final Widget_ArrowPopupView_Dark:I = 0x7f130639

.field public static final Widget_ArrowPopupView_DayNight:I = 0x7f13063a

.field public static final Widget_AutoCompleteTextView:I = 0x7f13063b

.field public static final Widget_AutoCompleteTextView_Search:I = 0x7f13063c

.field public static final Widget_Button:I = 0x7f13063d

.field public static final Widget_ButtonBar:I = 0x7f13064d

.field public static final Widget_Button_Dialog:I = 0x7f13063e

.field public static final Widget_Button_Dialog_Default:I = 0x7f13063f

.field public static final Widget_Button_Inline:I = 0x7f130640

.field public static final Widget_Button_Inline_Delete:I = 0x7f130641

.field public static final Widget_Button_Inline_Detail:I = 0x7f130642

.field public static final Widget_Button_Inline_Expand:I = 0x7f130643

.field public static final Widget_Button_Inline_Shrink:I = 0x7f130644

.field public static final Widget_Button_Negative:I = 0x7f130645

.field public static final Widget_Button_Positive:I = 0x7f130646

.field public static final Widget_Button_Primary:I = 0x7f130647

.field public static final Widget_Button_Rect:I = 0x7f130648

.field public static final Widget_Button_Rect_BlankPage:I = 0x7f130649

.field public static final Widget_Button_Rect_BlankPage_Dark:I = 0x7f13064a

.field public static final Widget_Button_Rect_BlankPage_Light:I = 0x7f13064b

.field public static final Widget_Button_Warning:I = 0x7f13064c

.field public static final Widget_Compat_NotificationActionContainer:I = 0x7f13064e

.field public static final Widget_Compat_NotificationActionText:I = 0x7f13064f

.field public static final Widget_CompoundButton:I = 0x7f130650

.field public static final Widget_CompoundButton_CheckBox:I = 0x7f130651

.field public static final Widget_CompoundButton_CheckBox_Dialog:I = 0x7f130652

.field public static final Widget_CompoundButton_CheckBox_Dialog_Dark:I = 0x7f130653

.field public static final Widget_CompoundButton_CheckBox_Dialog_Light:I = 0x7f130654

.field public static final Widget_CompoundButton_CheckBox_Expand:I = 0x7f130655

.field public static final Widget_CompoundButton_RadioButton:I = 0x7f130656

.field public static final Widget_CompoundButton_RadioButton_Circle:I = 0x7f130657

.field public static final Widget_Dark:I = 0x7f130658

.field public static final Widget_Dark_AutoCompleteTextView_Search:I = 0x7f130659

.field public static final Widget_DatePicker:I = 0x7f13065a

.field public static final Widget_DateTimePicker:I = 0x7f13065b

.field public static final Widget_DateTimePicker_Big_Dark:I = 0x7f13065c

.field public static final Widget_DateTimePicker_Big_DayNight:I = 0x7f13065d

.field public static final Widget_DateTimePicker_Big_Light:I = 0x7f13065e

.field public static final Widget_Design_AppBarLayout:I = 0x7f13065f

.field public static final Widget_Design_BottomNavigationView:I = 0x7f130660

.field public static final Widget_Design_BottomSheet_Modal:I = 0x7f130661

.field public static final Widget_Design_CollapsingToolbar:I = 0x7f130662

.field public static final Widget_Design_FloatingActionButton:I = 0x7f130663

.field public static final Widget_Design_NavigationView:I = 0x7f130664

.field public static final Widget_Design_ScrimInsetsFrameLayout:I = 0x7f130665

.field public static final Widget_Design_Snackbar:I = 0x7f130666

.field public static final Widget_Design_TabLayout:I = 0x7f130667

.field public static final Widget_Design_TextInputEditText:I = 0x7f130668

.field public static final Widget_Design_TextInputLayout:I = 0x7f130669

.field public static final Widget_DialogPrimaryContent:I = 0x7f13066a

.field public static final Widget_DialogTitle:I = 0x7f13066b

.field public static final Widget_DropDownItem:I = 0x7f13066c

.field public static final Widget_DropDownItem_Spinner:I = 0x7f13066d

.field public static final Widget_DropdownSelector:I = 0x7f13066e

.field public static final Widget_DropdownSelector_Spinner:I = 0x7f13066f

.field public static final Widget_DropdownSelector_Spinner_Integrated:I = 0x7f130670

.field public static final Widget_EditText:I = 0x7f130671

.field public static final Widget_EditText_Clearable:I = 0x7f130672

.field public static final Widget_EditText_Dark:I = 0x7f130673

.field public static final Widget_EditText_Dark_Clearable:I = 0x7f130674

.field public static final Widget_EditText_DayNight:I = 0x7f130675

.field public static final Widget_EditText_Dialog:I = 0x7f130676

.field public static final Widget_EditText_Search:I = 0x7f130677

.field public static final Widget_FilterSortTabView:I = 0x7f130678

.field public static final Widget_FilterSortTabView_Dark:I = 0x7f130679

.field public static final Widget_FilterSortTabView_DayNight:I = 0x7f13067a

.field public static final Widget_FilterSortView:I = 0x7f13067b

.field public static final Widget_FilterSortView_Dark:I = 0x7f13067c

.field public static final Widget_FilterSortView_DayNight:I = 0x7f13067d

.field public static final Widget_FloatingActionButton:I = 0x7f13067e

.field public static final Widget_FloatingActionButton_Dark:I = 0x7f13067f

.field public static final Widget_GuidePopupView:I = 0x7f130680

.field public static final Widget_GuidePopupView_Dark:I = 0x7f130681

.field public static final Widget_GuidePopupView_DayNight:I = 0x7f130682

.field public static final Widget_ListMenuItem_Dark:I = 0x7f130683

.field public static final Widget_ListMenuItem_Light:I = 0x7f130684

.field public static final Widget_ListPopupWindow:I = 0x7f130685

.field public static final Widget_ListView:I = 0x7f130686

.field public static final Widget_ListView_Item:I = 0x7f130687

.field public static final Widget_ListView_Item_DoubleLine:I = 0x7f130688

.field public static final Widget_ListView_Item_DoubleLine_SmallPadding:I = 0x7f130689

.field public static final Widget_ListView_Item_GroupHeader:I = 0x7f13068a

.field public static final Widget_ListView_Item_Immersion:I = 0x7f13068b

.field public static final Widget_ListView_Item_SingleLine:I = 0x7f13068c

.field public static final Widget_ListView_Item_SingleLine_SmallPadding:I = 0x7f13068d

.field public static final Widget_ListView_Item_TripleLine:I = 0x7f13068e

.field public static final Widget_ListView_Item_TripleLine_SmallPadding:I = 0x7f13068f

.field public static final Widget_Material3_ActionBar_Solid:I = 0x7f130690

.field public static final Widget_Material3_ActionMode:I = 0x7f130691

.field public static final Widget_Material3_AppBarLayout:I = 0x7f130692

.field public static final Widget_Material3_AutoCompleteTextView_FilledBox:I = 0x7f130693

.field public static final Widget_Material3_AutoCompleteTextView_FilledBox_Dense:I = 0x7f130694

.field public static final Widget_Material3_AutoCompleteTextView_OutlinedBox:I = 0x7f130695

.field public static final Widget_Material3_AutoCompleteTextView_OutlinedBox_Dense:I = 0x7f130696

.field public static final Widget_Material3_Badge:I = 0x7f130697

.field public static final Widget_Material3_BottomAppBar:I = 0x7f130698

.field public static final Widget_Material3_BottomNavigationView:I = 0x7f130699

.field public static final Widget_Material3_BottomNavigationView_ActiveIndicator:I = 0x7f13069a

.field public static final Widget_Material3_BottomSheet:I = 0x7f13069b

.field public static final Widget_Material3_BottomSheet_Modal:I = 0x7f13069c

.field public static final Widget_Material3_Button:I = 0x7f13069d

.field public static final Widget_Material3_Button_ElevatedButton:I = 0x7f13069e

.field public static final Widget_Material3_Button_ElevatedButton_Icon:I = 0x7f13069f

.field public static final Widget_Material3_Button_Icon:I = 0x7f1306a0

.field public static final Widget_Material3_Button_IconButton:I = 0x7f1306a1

.field public static final Widget_Material3_Button_OutlinedButton:I = 0x7f1306a2

.field public static final Widget_Material3_Button_OutlinedButton_Icon:I = 0x7f1306a3

.field public static final Widget_Material3_Button_TextButton:I = 0x7f1306a4

.field public static final Widget_Material3_Button_TextButton_Dialog:I = 0x7f1306a5

.field public static final Widget_Material3_Button_TextButton_Dialog_Flush:I = 0x7f1306a6

.field public static final Widget_Material3_Button_TextButton_Dialog_Icon:I = 0x7f1306a7

.field public static final Widget_Material3_Button_TextButton_Icon:I = 0x7f1306a8

.field public static final Widget_Material3_Button_TextButton_Snackbar:I = 0x7f1306a9

.field public static final Widget_Material3_Button_TonalButton:I = 0x7f1306aa

.field public static final Widget_Material3_Button_TonalButton_Icon:I = 0x7f1306ab

.field public static final Widget_Material3_Button_UnelevatedButton:I = 0x7f1306ac

.field public static final Widget_Material3_CardView_Elevated:I = 0x7f1306ad

.field public static final Widget_Material3_CardView_Filled:I = 0x7f1306ae

.field public static final Widget_Material3_CardView_Outlined:I = 0x7f1306af

.field public static final Widget_Material3_CheckedTextView:I = 0x7f1306b0

.field public static final Widget_Material3_ChipGroup:I = 0x7f1306bb

.field public static final Widget_Material3_Chip_Assist:I = 0x7f1306b1

.field public static final Widget_Material3_Chip_Assist_Elevated:I = 0x7f1306b2

.field public static final Widget_Material3_Chip_Filter:I = 0x7f1306b3

.field public static final Widget_Material3_Chip_Filter_Elevated:I = 0x7f1306b4

.field public static final Widget_Material3_Chip_Input:I = 0x7f1306b5

.field public static final Widget_Material3_Chip_Input_Elevated:I = 0x7f1306b6

.field public static final Widget_Material3_Chip_Input_Icon:I = 0x7f1306b7

.field public static final Widget_Material3_Chip_Input_Icon_Elevated:I = 0x7f1306b8

.field public static final Widget_Material3_Chip_Suggestion:I = 0x7f1306b9

.field public static final Widget_Material3_Chip_Suggestion_Elevated:I = 0x7f1306ba

.field public static final Widget_Material3_CircularProgressIndicator:I = 0x7f1306bc

.field public static final Widget_Material3_CircularProgressIndicator_ExtraSmall:I = 0x7f1306bd

.field public static final Widget_Material3_CircularProgressIndicator_Medium:I = 0x7f1306be

.field public static final Widget_Material3_CircularProgressIndicator_Small:I = 0x7f1306bf

.field public static final Widget_Material3_CollapsingToolbar:I = 0x7f1306c0

.field public static final Widget_Material3_CollapsingToolbar_Large:I = 0x7f1306c1

.field public static final Widget_Material3_CollapsingToolbar_Medium:I = 0x7f1306c2

.field public static final Widget_Material3_CompoundButton_CheckBox:I = 0x7f1306c3

.field public static final Widget_Material3_CompoundButton_RadioButton:I = 0x7f1306c4

.field public static final Widget_Material3_CompoundButton_Switch:I = 0x7f1306c5

.field public static final Widget_Material3_DrawerLayout:I = 0x7f1306c6

.field public static final Widget_Material3_ExtendedFloatingActionButton_Icon_Primary:I = 0x7f1306c7

.field public static final Widget_Material3_ExtendedFloatingActionButton_Icon_Secondary:I = 0x7f1306c8

.field public static final Widget_Material3_ExtendedFloatingActionButton_Icon_Surface:I = 0x7f1306c9

.field public static final Widget_Material3_ExtendedFloatingActionButton_Icon_Tertiary:I = 0x7f1306ca

.field public static final Widget_Material3_ExtendedFloatingActionButton_Primary:I = 0x7f1306cb

.field public static final Widget_Material3_ExtendedFloatingActionButton_Secondary:I = 0x7f1306cc

.field public static final Widget_Material3_ExtendedFloatingActionButton_Surface:I = 0x7f1306cd

.field public static final Widget_Material3_ExtendedFloatingActionButton_Tertiary:I = 0x7f1306ce

.field public static final Widget_Material3_FloatingActionButton_Large_Primary:I = 0x7f1306cf

.field public static final Widget_Material3_FloatingActionButton_Large_Secondary:I = 0x7f1306d0

.field public static final Widget_Material3_FloatingActionButton_Large_Surface:I = 0x7f1306d1

.field public static final Widget_Material3_FloatingActionButton_Large_Tertiary:I = 0x7f1306d2

.field public static final Widget_Material3_FloatingActionButton_Primary:I = 0x7f1306d3

.field public static final Widget_Material3_FloatingActionButton_Secondary:I = 0x7f1306d4

.field public static final Widget_Material3_FloatingActionButton_Surface:I = 0x7f1306d5

.field public static final Widget_Material3_FloatingActionButton_Tertiary:I = 0x7f1306d6

.field public static final Widget_Material3_Light_ActionBar_Solid:I = 0x7f1306d7

.field public static final Widget_Material3_LinearProgressIndicator:I = 0x7f1306d8

.field public static final Widget_Material3_MaterialCalendar:I = 0x7f1306d9

.field public static final Widget_Material3_MaterialCalendar_Day:I = 0x7f1306da

.field public static final Widget_Material3_MaterialCalendar_DayOfWeekLabel:I = 0x7f1306de

.field public static final Widget_Material3_MaterialCalendar_DayTextView:I = 0x7f1306df

.field public static final Widget_Material3_MaterialCalendar_Day_Invalid:I = 0x7f1306db

.field public static final Widget_Material3_MaterialCalendar_Day_Selected:I = 0x7f1306dc

.field public static final Widget_Material3_MaterialCalendar_Day_Today:I = 0x7f1306dd

.field public static final Widget_Material3_MaterialCalendar_Fullscreen:I = 0x7f1306e0

.field public static final Widget_Material3_MaterialCalendar_HeaderCancelButton:I = 0x7f1306e1

.field public static final Widget_Material3_MaterialCalendar_HeaderDivider:I = 0x7f1306e2

.field public static final Widget_Material3_MaterialCalendar_HeaderLayout:I = 0x7f1306e3

.field public static final Widget_Material3_MaterialCalendar_HeaderSelection:I = 0x7f1306e4

.field public static final Widget_Material3_MaterialCalendar_HeaderSelection_Fullscreen:I = 0x7f1306e5

.field public static final Widget_Material3_MaterialCalendar_HeaderTitle:I = 0x7f1306e6

.field public static final Widget_Material3_MaterialCalendar_HeaderToggleButton:I = 0x7f1306e7

.field public static final Widget_Material3_MaterialCalendar_Item:I = 0x7f1306e8

.field public static final Widget_Material3_MaterialCalendar_MonthNavigationButton:I = 0x7f1306e9

.field public static final Widget_Material3_MaterialCalendar_MonthTextView:I = 0x7f1306ea

.field public static final Widget_Material3_MaterialCalendar_Year:I = 0x7f1306eb

.field public static final Widget_Material3_MaterialCalendar_YearNavigationButton:I = 0x7f1306ee

.field public static final Widget_Material3_MaterialCalendar_Year_Selected:I = 0x7f1306ec

.field public static final Widget_Material3_MaterialCalendar_Year_Today:I = 0x7f1306ed

.field public static final Widget_Material3_MaterialDivider:I = 0x7f1306ef

.field public static final Widget_Material3_MaterialDivider_Heavy:I = 0x7f1306f0

.field public static final Widget_Material3_MaterialTimePicker:I = 0x7f1306f1

.field public static final Widget_Material3_MaterialTimePicker_Button:I = 0x7f1306f2

.field public static final Widget_Material3_MaterialTimePicker_Clock:I = 0x7f1306f3

.field public static final Widget_Material3_MaterialTimePicker_Display:I = 0x7f1306f4

.field public static final Widget_Material3_MaterialTimePicker_Display_Divider:I = 0x7f1306f5

.field public static final Widget_Material3_MaterialTimePicker_Display_HelperText:I = 0x7f1306f6

.field public static final Widget_Material3_MaterialTimePicker_Display_TextInputEditText:I = 0x7f1306f7

.field public static final Widget_Material3_MaterialTimePicker_Display_TextInputLayout:I = 0x7f1306f8

.field public static final Widget_Material3_MaterialTimePicker_ImageButton:I = 0x7f1306f9

.field public static final Widget_Material3_NavigationRailView:I = 0x7f1306fa

.field public static final Widget_Material3_NavigationRailView_ActiveIndicator:I = 0x7f1306fb

.field public static final Widget_Material3_NavigationView:I = 0x7f1306fc

.field public static final Widget_Material3_PopupMenu:I = 0x7f1306fd

.field public static final Widget_Material3_PopupMenu_ContextMenu:I = 0x7f1306fe

.field public static final Widget_Material3_PopupMenu_ListPopupWindow:I = 0x7f1306ff

.field public static final Widget_Material3_PopupMenu_Overflow:I = 0x7f130700

.field public static final Widget_Material3_Slider:I = 0x7f130701

.field public static final Widget_Material3_Snackbar:I = 0x7f130702

.field public static final Widget_Material3_Snackbar_FullWidth:I = 0x7f130703

.field public static final Widget_Material3_Snackbar_TextView:I = 0x7f130704

.field public static final Widget_Material3_TabLayout:I = 0x7f130705

.field public static final Widget_Material3_TabLayout_OnSurface:I = 0x7f130706

.field public static final Widget_Material3_TabLayout_Secondary:I = 0x7f130707

.field public static final Widget_Material3_TextInputEditText_FilledBox:I = 0x7f130708

.field public static final Widget_Material3_TextInputEditText_FilledBox_Dense:I = 0x7f130709

.field public static final Widget_Material3_TextInputEditText_OutlinedBox:I = 0x7f13070a

.field public static final Widget_Material3_TextInputEditText_OutlinedBox_Dense:I = 0x7f13070b

.field public static final Widget_Material3_TextInputLayout_FilledBox:I = 0x7f13070c

.field public static final Widget_Material3_TextInputLayout_FilledBox_Dense:I = 0x7f13070d

.field public static final Widget_Material3_TextInputLayout_FilledBox_Dense_ExposedDropdownMenu:I = 0x7f13070e

.field public static final Widget_Material3_TextInputLayout_FilledBox_ExposedDropdownMenu:I = 0x7f13070f

.field public static final Widget_Material3_TextInputLayout_OutlinedBox:I = 0x7f130710

.field public static final Widget_Material3_TextInputLayout_OutlinedBox_Dense:I = 0x7f130711

.field public static final Widget_Material3_TextInputLayout_OutlinedBox_Dense_ExposedDropdownMenu:I = 0x7f130712

.field public static final Widget_Material3_TextInputLayout_OutlinedBox_ExposedDropdownMenu:I = 0x7f130713

.field public static final Widget_Material3_Toolbar:I = 0x7f130714

.field public static final Widget_Material3_Toolbar_OnSurface:I = 0x7f130715

.field public static final Widget_Material3_Toolbar_Surface:I = 0x7f130716

.field public static final Widget_Material3_Tooltip:I = 0x7f130717

.field public static final Widget_MaterialComponents_ActionBar_Primary:I = 0x7f130718

.field public static final Widget_MaterialComponents_ActionBar_PrimarySurface:I = 0x7f130719

.field public static final Widget_MaterialComponents_ActionBar_Solid:I = 0x7f13071a

.field public static final Widget_MaterialComponents_ActionBar_Surface:I = 0x7f13071b

.field public static final Widget_MaterialComponents_ActionMode:I = 0x7f13071c

.field public static final Widget_MaterialComponents_AppBarLayout_Primary:I = 0x7f13071d

.field public static final Widget_MaterialComponents_AppBarLayout_PrimarySurface:I = 0x7f13071e

.field public static final Widget_MaterialComponents_AppBarLayout_Surface:I = 0x7f13071f

.field public static final Widget_MaterialComponents_AutoCompleteTextView_FilledBox:I = 0x7f130720

.field public static final Widget_MaterialComponents_AutoCompleteTextView_FilledBox_Dense:I = 0x7f130721

.field public static final Widget_MaterialComponents_AutoCompleteTextView_OutlinedBox:I = 0x7f130722

.field public static final Widget_MaterialComponents_AutoCompleteTextView_OutlinedBox_Dense:I = 0x7f130723

.field public static final Widget_MaterialComponents_Badge:I = 0x7f130724

.field public static final Widget_MaterialComponents_BottomAppBar:I = 0x7f130725

.field public static final Widget_MaterialComponents_BottomAppBar_Colored:I = 0x7f130726

.field public static final Widget_MaterialComponents_BottomAppBar_PrimarySurface:I = 0x7f130727

.field public static final Widget_MaterialComponents_BottomNavigationView:I = 0x7f130728

.field public static final Widget_MaterialComponents_BottomNavigationView_Colored:I = 0x7f130729

.field public static final Widget_MaterialComponents_BottomNavigationView_PrimarySurface:I = 0x7f13072a

.field public static final Widget_MaterialComponents_BottomSheet:I = 0x7f13072b

.field public static final Widget_MaterialComponents_BottomSheet_Modal:I = 0x7f13072c

.field public static final Widget_MaterialComponents_Button:I = 0x7f13072d

.field public static final Widget_MaterialComponents_Button_Icon:I = 0x7f13072e

.field public static final Widget_MaterialComponents_Button_OutlinedButton:I = 0x7f13072f

.field public static final Widget_MaterialComponents_Button_OutlinedButton_Icon:I = 0x7f130730

.field public static final Widget_MaterialComponents_Button_TextButton:I = 0x7f130731

.field public static final Widget_MaterialComponents_Button_TextButton_Dialog:I = 0x7f130732

.field public static final Widget_MaterialComponents_Button_TextButton_Dialog_Flush:I = 0x7f130733

.field public static final Widget_MaterialComponents_Button_TextButton_Dialog_Icon:I = 0x7f130734

.field public static final Widget_MaterialComponents_Button_TextButton_Icon:I = 0x7f130735

.field public static final Widget_MaterialComponents_Button_TextButton_Snackbar:I = 0x7f130736

.field public static final Widget_MaterialComponents_Button_UnelevatedButton:I = 0x7f130737

.field public static final Widget_MaterialComponents_Button_UnelevatedButton_Icon:I = 0x7f130738

.field public static final Widget_MaterialComponents_CardView:I = 0x7f130739

.field public static final Widget_MaterialComponents_CheckedTextView:I = 0x7f13073a

.field public static final Widget_MaterialComponents_ChipGroup:I = 0x7f13073f

.field public static final Widget_MaterialComponents_Chip_Action:I = 0x7f13073b

.field public static final Widget_MaterialComponents_Chip_Choice:I = 0x7f13073c

.field public static final Widget_MaterialComponents_Chip_Entry:I = 0x7f13073d

.field public static final Widget_MaterialComponents_Chip_Filter:I = 0x7f13073e

.field public static final Widget_MaterialComponents_CircularProgressIndicator:I = 0x7f130740

.field public static final Widget_MaterialComponents_CircularProgressIndicator_ExtraSmall:I = 0x7f130741

.field public static final Widget_MaterialComponents_CircularProgressIndicator_Medium:I = 0x7f130742

.field public static final Widget_MaterialComponents_CircularProgressIndicator_Small:I = 0x7f130743

.field public static final Widget_MaterialComponents_CollapsingToolbar:I = 0x7f130744

.field public static final Widget_MaterialComponents_CompoundButton_CheckBox:I = 0x7f130745

.field public static final Widget_MaterialComponents_CompoundButton_RadioButton:I = 0x7f130746

.field public static final Widget_MaterialComponents_CompoundButton_Switch:I = 0x7f130747

.field public static final Widget_MaterialComponents_ExtendedFloatingActionButton:I = 0x7f130748

.field public static final Widget_MaterialComponents_ExtendedFloatingActionButton_Icon:I = 0x7f130749

.field public static final Widget_MaterialComponents_FloatingActionButton:I = 0x7f13074a

.field public static final Widget_MaterialComponents_Light_ActionBar_Solid:I = 0x7f13074b

.field public static final Widget_MaterialComponents_LinearProgressIndicator:I = 0x7f13074c

.field public static final Widget_MaterialComponents_MaterialButtonToggleGroup:I = 0x7f13074d

.field public static final Widget_MaterialComponents_MaterialCalendar:I = 0x7f13074e

.field public static final Widget_MaterialComponents_MaterialCalendar_Day:I = 0x7f13074f

.field public static final Widget_MaterialComponents_MaterialCalendar_DayOfWeekLabel:I = 0x7f130753

.field public static final Widget_MaterialComponents_MaterialCalendar_DayTextView:I = 0x7f130754

.field public static final Widget_MaterialComponents_MaterialCalendar_Day_Invalid:I = 0x7f130750

.field public static final Widget_MaterialComponents_MaterialCalendar_Day_Selected:I = 0x7f130751

.field public static final Widget_MaterialComponents_MaterialCalendar_Day_Today:I = 0x7f130752

.field public static final Widget_MaterialComponents_MaterialCalendar_Fullscreen:I = 0x7f130755

.field public static final Widget_MaterialComponents_MaterialCalendar_HeaderCancelButton:I = 0x7f130756

.field public static final Widget_MaterialComponents_MaterialCalendar_HeaderConfirmButton:I = 0x7f130757

.field public static final Widget_MaterialComponents_MaterialCalendar_HeaderDivider:I = 0x7f130758

.field public static final Widget_MaterialComponents_MaterialCalendar_HeaderLayout:I = 0x7f130759

.field public static final Widget_MaterialComponents_MaterialCalendar_HeaderSelection:I = 0x7f13075a

.field public static final Widget_MaterialComponents_MaterialCalendar_HeaderSelection_Fullscreen:I = 0x7f13075b

.field public static final Widget_MaterialComponents_MaterialCalendar_HeaderTitle:I = 0x7f13075c

.field public static final Widget_MaterialComponents_MaterialCalendar_HeaderToggleButton:I = 0x7f13075d

.field public static final Widget_MaterialComponents_MaterialCalendar_Item:I = 0x7f13075e

.field public static final Widget_MaterialComponents_MaterialCalendar_MonthNavigationButton:I = 0x7f13075f

.field public static final Widget_MaterialComponents_MaterialCalendar_MonthTextView:I = 0x7f130760

.field public static final Widget_MaterialComponents_MaterialCalendar_Year:I = 0x7f130761

.field public static final Widget_MaterialComponents_MaterialCalendar_YearNavigationButton:I = 0x7f130764

.field public static final Widget_MaterialComponents_MaterialCalendar_Year_Selected:I = 0x7f130762

.field public static final Widget_MaterialComponents_MaterialCalendar_Year_Today:I = 0x7f130763

.field public static final Widget_MaterialComponents_MaterialDivider:I = 0x7f130765

.field public static final Widget_MaterialComponents_NavigationRailView:I = 0x7f130766

.field public static final Widget_MaterialComponents_NavigationRailView_Colored:I = 0x7f130767

.field public static final Widget_MaterialComponents_NavigationRailView_Colored_Compact:I = 0x7f130768

.field public static final Widget_MaterialComponents_NavigationRailView_Compact:I = 0x7f130769

.field public static final Widget_MaterialComponents_NavigationRailView_PrimarySurface:I = 0x7f13076a

.field public static final Widget_MaterialComponents_NavigationView:I = 0x7f13076b

.field public static final Widget_MaterialComponents_PopupMenu:I = 0x7f13076c

.field public static final Widget_MaterialComponents_PopupMenu_ContextMenu:I = 0x7f13076d

.field public static final Widget_MaterialComponents_PopupMenu_ListPopupWindow:I = 0x7f13076e

.field public static final Widget_MaterialComponents_PopupMenu_Overflow:I = 0x7f13076f

.field public static final Widget_MaterialComponents_ProgressIndicator:I = 0x7f130770

.field public static final Widget_MaterialComponents_ShapeableImageView:I = 0x7f130771

.field public static final Widget_MaterialComponents_Slider:I = 0x7f130772

.field public static final Widget_MaterialComponents_Snackbar:I = 0x7f130773

.field public static final Widget_MaterialComponents_Snackbar_FullWidth:I = 0x7f130774

.field public static final Widget_MaterialComponents_Snackbar_TextView:I = 0x7f130775

.field public static final Widget_MaterialComponents_TabLayout:I = 0x7f130776

.field public static final Widget_MaterialComponents_TabLayout_Colored:I = 0x7f130777

.field public static final Widget_MaterialComponents_TabLayout_PrimarySurface:I = 0x7f130778

.field public static final Widget_MaterialComponents_TextInputEditText_FilledBox:I = 0x7f130779

.field public static final Widget_MaterialComponents_TextInputEditText_FilledBox_Dense:I = 0x7f13077a

.field public static final Widget_MaterialComponents_TextInputEditText_OutlinedBox:I = 0x7f13077b

.field public static final Widget_MaterialComponents_TextInputEditText_OutlinedBox_Dense:I = 0x7f13077c

.field public static final Widget_MaterialComponents_TextInputLayout_FilledBox:I = 0x7f13077d

.field public static final Widget_MaterialComponents_TextInputLayout_FilledBox_Dense:I = 0x7f13077e

.field public static final Widget_MaterialComponents_TextInputLayout_FilledBox_Dense_ExposedDropdownMenu:I = 0x7f13077f

.field public static final Widget_MaterialComponents_TextInputLayout_FilledBox_ExposedDropdownMenu:I = 0x7f130780

.field public static final Widget_MaterialComponents_TextInputLayout_OutlinedBox:I = 0x7f130781

.field public static final Widget_MaterialComponents_TextInputLayout_OutlinedBox_Dense:I = 0x7f130782

.field public static final Widget_MaterialComponents_TextInputLayout_OutlinedBox_Dense_ExposedDropdownMenu:I = 0x7f130783

.field public static final Widget_MaterialComponents_TextInputLayout_OutlinedBox_ExposedDropdownMenu:I = 0x7f130784

.field public static final Widget_MaterialComponents_TextView:I = 0x7f130785

.field public static final Widget_MaterialComponents_TimePicker:I = 0x7f130786

.field public static final Widget_MaterialComponents_TimePicker_Button:I = 0x7f130787

.field public static final Widget_MaterialComponents_TimePicker_Clock:I = 0x7f130788

.field public static final Widget_MaterialComponents_TimePicker_Display:I = 0x7f130789

.field public static final Widget_MaterialComponents_TimePicker_Display_Divider:I = 0x7f13078a

.field public static final Widget_MaterialComponents_TimePicker_Display_HelperText:I = 0x7f13078b

.field public static final Widget_MaterialComponents_TimePicker_Display_TextInputEditText:I = 0x7f13078c

.field public static final Widget_MaterialComponents_TimePicker_Display_TextInputLayout:I = 0x7f13078d

.field public static final Widget_MaterialComponents_TimePicker_ImageButton:I = 0x7f13078e

.field public static final Widget_MaterialComponents_TimePicker_ImageButton_ShapeAppearance:I = 0x7f13078f

.field public static final Widget_MaterialComponents_Toolbar:I = 0x7f130790

.field public static final Widget_MaterialComponents_Toolbar_Primary:I = 0x7f130791

.field public static final Widget_MaterialComponents_Toolbar_PrimarySurface:I = 0x7f130792

.field public static final Widget_MaterialComponents_Toolbar_Surface:I = 0x7f130793

.field public static final Widget_MaterialComponents_Tooltip:I = 0x7f130794

.field public static final Widget_MediaRouter_Light_MediaRouteButton:I = 0x7f130795

.field public static final Widget_MediaRouter_MediaRouteButton:I = 0x7f130796

.field public static final Widget_MessageView:I = 0x7f130797

.field public static final Widget_MessageView_Error:I = 0x7f130798

.field public static final Widget_MessageView_Error_Dark:I = 0x7f130799

.field public static final Widget_MessageView_Error_DayNight:I = 0x7f13079a

.field public static final Widget_MessageView_Guide:I = 0x7f13079b

.field public static final Widget_MessageView_Guide_Dark:I = 0x7f13079c

.field public static final Widget_MessageView_Guide_DayNight:I = 0x7f13079d

.field public static final Widget_MessageView_Warning:I = 0x7f13079e

.field public static final Widget_MessageView_Warning_Dark:I = 0x7f13079f

.field public static final Widget_MessageView_Warning_DayNight:I = 0x7f1307a0

.field public static final Widget_NestedHeaderLayout:I = 0x7f1307a1

.field public static final Widget_NumberPicker:I = 0x7f1307a2

.field public static final Widget_NumberPicker_Dark:I = 0x7f1307a3

.field public static final Widget_NumberPicker_DayNight:I = 0x7f1307a4

.field public static final Widget_NumberPicker_Large_Dark:I = 0x7f1307a5

.field public static final Widget_NumberPicker_Large_Light:I = 0x7f1307a6

.field public static final Widget_NumberPicker_Medium_Dark:I = 0x7f1307a7

.field public static final Widget_NumberPicker_Medium_Light:I = 0x7f1307a8

.field public static final Widget_PopupWindow:I = 0x7f1307a9

.field public static final Widget_PopupWindow_Settings:I = 0x7f1307aa

.field public static final Widget_PreferenceIcon:I = 0x7f1307ab

.field public static final Widget_PreferenceItem:I = 0x7f1307ac

.field public static final Widget_PreferenceItem_Navigation:I = 0x7f1307ad

.field public static final Widget_PreferenceItem_Radio:I = 0x7f1307ae

.field public static final Widget_ProgressBar:I = 0x7f1307af

.field public static final Widget_ProgressBar_Dark:I = 0x7f1307b0

.field public static final Widget_ProgressBar_Horizontal:I = 0x7f1307b1

.field public static final Widget_ProgressBar_Horizontal_Dark:I = 0x7f1307b2

.field public static final Widget_ProgressBar_Horizontal_DayNight:I = 0x7f1307b3

.field public static final Widget_ProgressBar_Light:I = 0x7f1307b4

.field public static final Widget_ProgressBar_Small:I = 0x7f1307b5

.field public static final Widget_ProgressBar_Small_Dark:I = 0x7f1307b6

.field public static final Widget_ProgressBar_Small_Light:I = 0x7f1307b7

.field public static final Widget_RatingBar:I = 0x7f1307b8

.field public static final Widget_RatingBar_Indicator:I = 0x7f1307b9

.field public static final Widget_RatingBar_Large:I = 0x7f1307ba

.field public static final Widget_RatingBar_Small:I = 0x7f1307bb

.field public static final Widget_SearchActionMode:I = 0x7f1307bc

.field public static final Widget_SearchActionMode_Input:I = 0x7f1307bd

.field public static final Widget_SeekBar:I = 0x7f1307be

.field public static final Widget_SeekBar_Dark:I = 0x7f1307bf

.field public static final Widget_SeekBar_DayNight:I = 0x7f1307c0

.field public static final Widget_SeekBar_Light:I = 0x7f1307c1

.field public static final Widget_SliceView:I = 0x7f1307c2

.field public static final Widget_SliceView_Bluetooth:I = 0x7f1307c3

.field public static final Widget_SliceView_ContextualCard:I = 0x7f1307c4

.field public static final Widget_SliceView_Panel:I = 0x7f1307c5

.field public static final Widget_SliceView_Panel_Slider:I = 0x7f1307c6

.field public static final Widget_SliceView_Panel_Slider_LargeIcon:I = 0x7f1307c7

.field public static final Widget_SliceView_Settings:I = 0x7f1307c8

.field public static final Widget_SlidingButton:I = 0x7f1307c9

.field public static final Widget_SlidingButton_Dark:I = 0x7f1307ca

.field public static final Widget_SlidingButton_DayNight:I = 0x7f1307cb

.field public static final Widget_Spinner:I = 0x7f1307cc

.field public static final Widget_StateEditText:I = 0x7f1307cd

.field public static final Widget_StateEditText_Dark:I = 0x7f1307ce

.field public static final Widget_StateEditText_DayNight:I = 0x7f1307cf

.field public static final Widget_StateEditText_Light:I = 0x7f1307d0

.field public static final Widget_Support_CoordinatorLayout:I = 0x7f1307d1

.field public static final Widget_SwitchBar_Switch:I = 0x7f1307d2

.field public static final Widget_TabBar:I = 0x7f1307d3

.field public static final Widget_TextAppearance_AlphabetIndexer_Overlay:I = 0x7f1307d4

.field public static final adb_wireless_item:I = 0x7f1307d7

.field public static final adb_wireless_item_content:I = 0x7f1307d8

.field public static final adb_wireless_item_label:I = 0x7f1307d9

.field public static final adb_wireless_item_progress_text:I = 0x7f1307da

.field public static final adb_wireless_section:I = 0x7f1307db

.field public static final blank_screen_no_device:I = 0x7f1307dc

.field public static final bt_item:I = 0x7f1307dd

.field public static final bt_item_edit_content:I = 0x7f1307de

.field public static final bt_item_label:I = 0x7f1307df

.field public static final certSpinnerDropDownItemStyle:I = 0x7f1307e0

.field public static final device_info_dialog_label:I = 0x7f1307e1

.field public static final device_info_dialog_value:I = 0x7f1307e2

.field public static final dialog_btn_vertical_group:I = 0x7f1307e3

.field public static final dialog_suggest_vertical_btn:I = 0x7f1307e4

.field public static final dialog_vertical_btn:I = 0x7f1307e5

.field public static final entry_layout:I = 0x7f1307e6

.field public static final eventInfoListItemStyle:I = 0x7f1307e7

.field public static final floationActivityAnimator:I = 0x7f1307e8

.field public static final form_value:I = 0x7f1307e9

.field public static final guideTheme:I = 0x7f1307ea

.field public static final info_label:I = 0x7f1307eb

.field public static final info_layout:I = 0x7f1307ec

.field public static final info_small:I = 0x7f1307ed

.field public static final info_value:I = 0x7f1307ee

.field public static final input_capsule_content:I = 0x7f1307ef

.field public static final input_capsule_horizontal:I = 0x7f1307f0

.field public static final input_capsule_horizontal_constant:I = 0x7f1307f1

.field public static final input_capsule_label:I = 0x7f1307f2

.field public static final input_category:I = 0x7f1307f3

.field public static final input_item_content:I = 0x7f1307f4

.field public static final input_item_horizontal:I = 0x7f1307f5

.field public static final input_item_label:I = 0x7f1307f6

.field public static final keyguard_edittext_style:I = 0x7f1307f7

.field public static final miui_wifi_item:I = 0x7f1307f8

.field public static final pluginStyle:I = 0x7f1307f9

.field public static final pluginStyleV2:I = 0x7f1307fa

.field public static final settingsCollapseSubtitleStyle:I = 0x7f1307fb

.field public static final settingsCollapseTitleStyle:I = 0x7f1307fc

.field public static final settingsExpandSubtitleStyle:I = 0x7f1307fd

.field public static final settingsExpandTitleStyle:I = 0x7f1307fe

.field public static final stretchableWidgetStyle:I = 0x7f1307ff

.field public static final stylus_battery_anim:I = 0x7f130800

.field public static final stylus_window_anim:I = 0x7f130801

.field public static final update_style:I = 0x7f130802

.field public static final vpn_app_management_version_summary:I = 0x7f130803

.field public static final vpn_app_management_version_title:I = 0x7f130804

.field public static final vpn_checkbox:I = 0x7f130805

.field public static final vpn_input_category:I = 0x7f130806

.field public static final vpn_label:I = 0x7f130807

.field public static final vpn_spinner_value:I = 0x7f130808

.field public static final vpn_value:I = 0x7f130809

.field public static final vpn_warning:I = 0x7f13080a

.field public static final wifi_advanced_toggle:I = 0x7f13080b

.field public static final wifi_ap_dialog_spinner_item_title:I = 0x7f13080c

.field public static final wifi_ap_dialog_spinner_item_tv:I = 0x7f13080d

.field public static final wifi_checkbox_item_content:I = 0x7f13080e

.field public static final wifi_detail_spinner_item_title:I = 0x7f13080f

.field public static final wifi_divider_view:I = 0x7f130810

.field public static final wifi_item:I = 0x7f130811

.field public static final wifi_item_content:I = 0x7f130812

.field public static final wifi_item_edit_content:I = 0x7f130813

.field public static final wifi_item_label:I = 0x7f130814

.field public static final wifi_item_spinner:I = 0x7f130815

.field public static final wifi_item_warning:I = 0x7f130816

.field public static final wifi_reconnect:I = 0x7f130817

.field public static final wifi_section:I = 0x7f130818

.field public static final wifi_spinner:I = 0x7f130819

.field public static final wifi_spinner_item:I = 0x7f13081a

.field public static final wifi_spinner_item_content:I = 0x7f13081b

.field public static final wifi_spinner_item_label:I = 0x7f13081c

.field public static final wifi_spinner_item_title:I = 0x7f13081d

.field public static final wifi_spinner_item_tv:I = 0x7f13081e

.field public static final wifi_spinner_items:I = 0x7f13081f

.field public static final wifi_spinner_layout:I = 0x7f130820

.field public static final wifi_spinner_layout_constant:I = 0x7f130821

.field public static final wifi_spinner_title:I = 0x7f130822


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
