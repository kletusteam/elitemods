.class public final Lmiui/settings/splitlib/R$dimen;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/settings/splitlib/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final a11ymenu_grid_layout_margin:I = 0x7f070000

.field public static final a11ymenu_layout_margin:I = 0x7f070001

.field public static final a11ymenu_main_background_curve_radius:I = 0x7f070002

.field public static final abc_action_bar_content_inset_material:I = 0x7f070003

.field public static final abc_action_bar_content_inset_with_nav:I = 0x7f070004

.field public static final abc_action_bar_default_height_material:I = 0x7f070005

.field public static final abc_action_bar_default_padding_end_material:I = 0x7f070006

.field public static final abc_action_bar_default_padding_start_material:I = 0x7f070007

.field public static final abc_action_bar_elevation_material:I = 0x7f070008

.field public static final abc_action_bar_icon_vertical_padding_material:I = 0x7f070009

.field public static final abc_action_bar_overflow_padding_end_material:I = 0x7f07000a

.field public static final abc_action_bar_overflow_padding_start_material:I = 0x7f07000b

.field public static final abc_action_bar_stacked_max_height:I = 0x7f07000c

.field public static final abc_action_bar_stacked_tab_max_width:I = 0x7f07000d

.field public static final abc_action_bar_subtitle_bottom_margin_material:I = 0x7f07000e

.field public static final abc_action_bar_subtitle_top_margin_material:I = 0x7f07000f

.field public static final abc_action_button_min_height_material:I = 0x7f070010

.field public static final abc_action_button_min_width_material:I = 0x7f070011

.field public static final abc_action_button_min_width_overflow_material:I = 0x7f070012

.field public static final abc_alert_dialog_button_bar_height:I = 0x7f070013

.field public static final abc_alert_dialog_button_dimen:I = 0x7f070014

.field public static final abc_button_inset_horizontal_material:I = 0x7f070015

.field public static final abc_button_inset_vertical_material:I = 0x7f070016

.field public static final abc_button_padding_horizontal_material:I = 0x7f070017

.field public static final abc_button_padding_vertical_material:I = 0x7f070018

.field public static final abc_cascading_menus_min_smallest_width:I = 0x7f070019

.field public static final abc_config_prefDialogWidth:I = 0x7f07001a

.field public static final abc_control_corner_material:I = 0x7f07001b

.field public static final abc_control_inset_material:I = 0x7f07001c

.field public static final abc_control_padding_material:I = 0x7f07001d

.field public static final abc_dialog_corner_radius_material:I = 0x7f07001e

.field public static final abc_dialog_fixed_height_major:I = 0x7f07001f

.field public static final abc_dialog_fixed_height_minor:I = 0x7f070020

.field public static final abc_dialog_fixed_width_major:I = 0x7f070021

.field public static final abc_dialog_fixed_width_minor:I = 0x7f070022

.field public static final abc_dialog_list_padding_bottom_no_buttons:I = 0x7f070023

.field public static final abc_dialog_list_padding_top_no_title:I = 0x7f070024

.field public static final abc_dialog_min_width_major:I = 0x7f070025

.field public static final abc_dialog_min_width_minor:I = 0x7f070026

.field public static final abc_dialog_padding_material:I = 0x7f070027

.field public static final abc_dialog_padding_top_material:I = 0x7f070028

.field public static final abc_dialog_title_divider_material:I = 0x7f070029

.field public static final abc_disabled_alpha_material_dark:I = 0x7f07002a

.field public static final abc_disabled_alpha_material_light:I = 0x7f07002b

.field public static final abc_dropdownitem_icon_width:I = 0x7f07002c

.field public static final abc_dropdownitem_text_padding_left:I = 0x7f07002d

.field public static final abc_dropdownitem_text_padding_right:I = 0x7f07002e

.field public static final abc_edit_text_inset_bottom_material:I = 0x7f07002f

.field public static final abc_edit_text_inset_horizontal_material:I = 0x7f070030

.field public static final abc_edit_text_inset_top_material:I = 0x7f070031

.field public static final abc_floating_window_z:I = 0x7f070032

.field public static final abc_list_item_height_large_material:I = 0x7f070033

.field public static final abc_list_item_height_material:I = 0x7f070034

.field public static final abc_list_item_height_small_material:I = 0x7f070035

.field public static final abc_list_item_padding_horizontal_material:I = 0x7f070036

.field public static final abc_panel_menu_list_width:I = 0x7f070037

.field public static final abc_progress_bar_height_material:I = 0x7f070038

.field public static final abc_search_view_preferred_height:I = 0x7f070039

.field public static final abc_search_view_preferred_width:I = 0x7f07003a

.field public static final abc_seekbar_track_background_height_material:I = 0x7f07003b

.field public static final abc_seekbar_track_progress_height_material:I = 0x7f07003c

.field public static final abc_select_dialog_padding_start_material:I = 0x7f07003d

.field public static final abc_slice_action_row_height:I = 0x7f07003e

.field public static final abc_slice_big_pic_max_height:I = 0x7f07003f

.field public static final abc_slice_big_pic_min_height:I = 0x7f070040

.field public static final abc_slice_grid_gutter:I = 0x7f070041

.field public static final abc_slice_grid_image_min_width:I = 0x7f070042

.field public static final abc_slice_grid_image_only_height:I = 0x7f070043

.field public static final abc_slice_grid_image_text_height:I = 0x7f070044

.field public static final abc_slice_grid_max_height:I = 0x7f070045

.field public static final abc_slice_grid_min_height:I = 0x7f070046

.field public static final abc_slice_grid_raw_image_text_offset:I = 0x7f070047

.field public static final abc_slice_grid_small_image_text_height:I = 0x7f070048

.field public static final abc_slice_grid_text_inner_padding:I = 0x7f070049

.field public static final abc_slice_grid_text_padding:I = 0x7f07004a

.field public static final abc_slice_icon_size:I = 0x7f07004b

.field public static final abc_slice_large_height:I = 0x7f07004c

.field public static final abc_slice_padding:I = 0x7f07004d

.field public static final abc_slice_row_active_input_height:I = 0x7f07004e

.field public static final abc_slice_row_max_height:I = 0x7f07004f

.field public static final abc_slice_row_min_height:I = 0x7f070050

.field public static final abc_slice_row_range_height:I = 0x7f070051

.field public static final abc_slice_row_range_inline_height:I = 0x7f070052

.field public static final abc_slice_row_range_multi_text_height:I = 0x7f070053

.field public static final abc_slice_row_range_single_text_height:I = 0x7f070054

.field public static final abc_slice_row_selection_height:I = 0x7f070055

.field public static final abc_slice_row_selection_multi_text_height:I = 0x7f070056

.field public static final abc_slice_row_selection_single_text_height:I = 0x7f070057

.field public static final abc_slice_shortcut_size:I = 0x7f070058

.field public static final abc_slice_small_height:I = 0x7f070059

.field public static final abc_slice_small_image_size:I = 0x7f07005a

.field public static final abc_star_big:I = 0x7f07005b

.field public static final abc_star_medium:I = 0x7f07005c

.field public static final abc_star_small:I = 0x7f07005d

.field public static final abc_switch_padding:I = 0x7f07005e

.field public static final abc_text_size_body_1_material:I = 0x7f07005f

.field public static final abc_text_size_body_2_material:I = 0x7f070060

.field public static final abc_text_size_button_material:I = 0x7f070061

.field public static final abc_text_size_caption_material:I = 0x7f070062

.field public static final abc_text_size_display_1_material:I = 0x7f070063

.field public static final abc_text_size_display_2_material:I = 0x7f070064

.field public static final abc_text_size_display_3_material:I = 0x7f070065

.field public static final abc_text_size_display_4_material:I = 0x7f070066

.field public static final abc_text_size_headline_material:I = 0x7f070067

.field public static final abc_text_size_large_material:I = 0x7f070068

.field public static final abc_text_size_medium_material:I = 0x7f070069

.field public static final abc_text_size_menu_header_material:I = 0x7f07006a

.field public static final abc_text_size_menu_material:I = 0x7f07006b

.field public static final abc_text_size_small_material:I = 0x7f07006c

.field public static final abc_text_size_subhead_material:I = 0x7f07006d

.field public static final abc_text_size_subtitle_material_toolbar:I = 0x7f07006e

.field public static final abc_text_size_title_material:I = 0x7f07006f

.field public static final abc_text_size_title_material_toolbar:I = 0x7f070070

.field public static final accessibility_animated_margin_bottom_pad_horizontal:I = 0x7f070071

.field public static final accessibility_animated_margin_bottom_pad_vertical:I = 0x7f070072

.field public static final accessibility_animated_margin_top_pad_horizontal:I = 0x7f070073

.field public static final accessibility_animated_margin_top_pad_vertical:I = 0x7f070074

.field public static final accessibility_animated_max_height_pad_horizontal:I = 0x7f070075

.field public static final accessibility_animated_max_height_pad_vertical:I = 0x7f070076

.field public static final accessibility_button_preference_padding_top_bottom:I = 0x7f070077

.field public static final accessibility_button_preview_height:I = 0x7f070078

.field public static final accessibility_icon_foreground_size:I = 0x7f070079

.field public static final accessibility_icon_size:I = 0x7f07007a

.field public static final accessibility_illustration_view_radius:I = 0x7f07007b

.field public static final accessibility_imageview_size:I = 0x7f07007c

.field public static final accessibility_layout_margin_start_end:I = 0x7f07007d

.field public static final accessibility_qs_tooltip_margin:I = 0x7f07007e

.field public static final accessibility_qs_tooltip_margin_top:I = 0x7f07007f

.field public static final accessibility_textview_layout_margin_bottom:I = 0x7f070080

.field public static final accesspoint_summary_size:I = 0x7f070081

.field public static final accesspoint_title_size:I = 0x7f070082

.field public static final account_icon_margin_top:I = 0x7f070083

.field public static final action_bar_default_height:I = 0x7f070084

.field public static final action_bar_size:I = 0x7f070085

.field public static final action_bar_subtitle_bottom_margin:I = 0x7f070086

.field public static final action_bar_subtitle_top_margin:I = 0x7f070087

.field public static final action_bar_switch_padding:I = 0x7f070088

.field public static final action_bar_tab_text_size:I = 0x7f070089

.field public static final action_button_drawable_padding:I = 0x7f07008a

.field public static final action_mode_title_button_width:I = 0x7f07008b

.field public static final actionbar_contentInsetStart:I = 0x7f07008c

.field public static final actionbar_icon_size:I = 0x7f07008d

.field public static final actionbar_size:I = 0x7f07008e

.field public static final actionbar_subsettings_contentInsetStart:I = 0x7f07008f

.field public static final actionbar_svg_icon_size:I = 0x7f070090

.field public static final adaptive_outline_stroke:I = 0x7f070091

.field public static final add_a_photo_circled_padding:I = 0x7f070092

.field public static final add_a_photo_icon_size_in_user_info_dialog:I = 0x7f070093

.field public static final add_fingerprint_msg_text_size:I = 0x7f070094

.field public static final add_fingerprint_page_back_height:I = 0x7f070095

.field public static final add_fingerprint_page_back_left:I = 0x7f070096

.field public static final add_fingerprint_page_back_width:I = 0x7f070097

.field public static final add_fingerprint_page_bottom_padding:I = 0x7f070098

.field public static final add_fingerprint_page_layout_margin:I = 0x7f070099

.field public static final add_fingerprint_page_layout_margin_bottom:I = 0x7f07009a

.field public static final add_fingerprint_page_layout_margin_top:I = 0x7f07009b

.field public static final add_fingerprint_page_main_content_height:I = 0x7f07009c

.field public static final add_fingerprint_page_title_top_margin:I = 0x7f07009d

.field public static final add_fingerprint_title_text_size:I = 0x7f07009e

.field public static final admin_details_dialog_icon_size:I = 0x7f07009f

.field public static final admin_details_dialog_learn_more_button_minWidth:I = 0x7f0700a0

.field public static final admin_details_dialog_learn_more_button_padding:I = 0x7f0700a1

.field public static final admin_details_dialog_learn_more_button_top_margin:I = 0x7f0700a2

.field public static final admin_details_dialog_link_padding_top:I = 0x7f0700a3

.field public static final admin_details_dialog_padding:I = 0x7f0700a4

.field public static final admin_details_dialog_padding_bottom:I = 0x7f0700a5

.field public static final admin_details_dialog_title_bottom_padding:I = 0x7f0700a6

.field public static final advanced_bluetooth_battery_height:I = 0x7f0700a7

.field public static final advanced_bluetooth_battery_meter_height:I = 0x7f0700a8

.field public static final advanced_bluetooth_battery_meter_width:I = 0x7f0700a9

.field public static final advanced_bluetooth_battery_right_margin:I = 0x7f0700aa

.field public static final advanced_bluetooth_battery_width:I = 0x7f0700ab

.field public static final advanced_bluetooth_header_title_text_size:I = 0x7f0700ac

.field public static final advanced_dashboard_tile_foreground_image_inset:I = 0x7f0700ad

.field public static final advanced_icon_size:I = 0x7f0700ae

.field public static final ai_settings_description_padding_left:I = 0x7f0700af

.field public static final ai_settings_divider_size:I = 0x7f0700b0

.field public static final ai_settings_long_press_height:I = 0x7f0700b1

.field public static final ai_settings_long_press_margin_bottom:I = 0x7f0700b2

.field public static final ai_settings_long_press_margin_top:I = 0x7f0700b3

.field public static final ai_settings_long_press_width:I = 0x7f0700b4

.field public static final ai_settings_main_margin_bottom:I = 0x7f0700b5

.field public static final ai_settings_main_margin_top:I = 0x7f0700b6

.field public static final ai_settings_main_text_margin_bottom:I = 0x7f0700b7

.field public static final ai_settings_main_text_size:I = 0x7f0700b8

.field public static final ai_settings_main_title_height:I = 0x7f0700b9

.field public static final ai_settings_sub_back:I = 0x7f0700ba

.field public static final ai_settings_sub_op_image_height:I = 0x7f0700bb

.field public static final ai_settings_sub_op_item_height:I = 0x7f0700bc

.field public static final ai_settings_sub_op_item_margin_left:I = 0x7f0700bd

.field public static final ai_settings_sub_status_bar_height:I = 0x7f0700be

.field public static final ai_settings_sub_text_size:I = 0x7f0700bf

.field public static final ai_settings_sub_title_margin_left:I = 0x7f0700c0

.field public static final ai_settings_sub_title_margin_top:I = 0x7f0700c1

.field public static final ai_settings_sub_title_text_size:I = 0x7f0700c2

.field public static final ai_settings_summary:I = 0x7f0700c3

.field public static final ai_settings_title:I = 0x7f0700c4

.field public static final ai_settings_title_height:I = 0x7f0700c5

.field public static final ai_settings_title_width:I = 0x7f0700c6

.field public static final airplane_mode_message_margin_vertical:I = 0x7f0700c7

.field public static final all_specs_card_margin_top:I = 0x7f0700c8

.field public static final android_navigation_bar_size:I = 0x7f0700c9

.field public static final android_version_logo_height:I = 0x7f0700ca

.field public static final android_version_logo_marginStart:I = 0x7f0700cb

.field public static final android_version_logo_topMargin:I = 0x7f0700cc

.field public static final android_version_logo_width:I = 0x7f0700cd

.field public static final ap_battery_image_high:I = 0x7f0700ce

.field public static final ap_battery_image_width:I = 0x7f0700cf

.field public static final app_icon_min_width:I = 0x7f0700d0

.field public static final app_icon_size:I = 0x7f0700d1

.field public static final app_preference_padding_start:I = 0x7f0700d2

.field public static final appcompat_dialog_background_inset:I = 0x7f0700d3

.field public static final appear_y_translation_start:I = 0x7f0700d4

.field public static final application_icon_size:I = 0x7f0700d5

.field public static final appwidget_height:I = 0x7f0700d6

.field public static final appwidget_width:I = 0x7f0700d7

.field public static final auto_rule_first_item_padding_top:I = 0x7f0700d8

.field public static final auto_rule_first_margin_top:I = 0x7f0700d9

.field public static final auto_rule_last_item_padding_bottom:I = 0x7f0700da

.field public static final auto_rule_last_margin_bottom:I = 0x7f0700db

.field public static final auto_rule_margin_left:I = 0x7f0700dc

.field public static final auto_rule_padding_left:I = 0x7f0700dd

.field public static final auto_rule_padding_right:I = 0x7f0700de

.field public static final autoclick_preview_height:I = 0x7f0700df

.field public static final avatar_length:I = 0x7f0700e0

.field public static final avatar_margin_end:I = 0x7f0700e1

.field public static final avatar_margin_top:I = 0x7f0700e2

.field public static final avatar_picker_icon_inset:I = 0x7f0700e3

.field public static final avatar_picker_margin:I = 0x7f0700e4

.field public static final avatar_picker_padding:I = 0x7f0700e5

.field public static final avatar_size_in_picker:I = 0x7f0700e6

.field public static final back_button_alight_top:I = 0x7f0700e7

.field public static final back_button_alight_top_cetus:I = 0x7f0700e8

.field public static final back_button_margin_start:I = 0x7f0700e9

.field public static final back_gesture_indicator_width:I = 0x7f0700ea

.field public static final back_image_exit_start:I = 0x7f0700eb

.field public static final back_image_exit_top:I = 0x7f0700ec

.field public static final back_image_layout_top:I = 0x7f0700ed

.field public static final back_image_width_height:I = 0x7f0700ee

.field public static final background_application_manage_item_operation_width:I = 0x7f0700ef

.field public static final backup_image_bottom:I = 0x7f0700f0

.field public static final backup_image_height:I = 0x7f0700f1

.field public static final backup_image_start:I = 0x7f0700f2

.field public static final backup_image_top:I = 0x7f0700f3

.field public static final backup_image_width:I = 0x7f0700f4

.field public static final backup_item_icon_margin_right:I = 0x7f0700f5

.field public static final badge_size:I = 0x7f0700f6

.field public static final balance_seekbar_center_marker_height:I = 0x7f0700f7

.field public static final balance_seekbar_center_marker_width:I = 0x7f0700f8

.field public static final base_card_item_card_value_margin:I = 0x7f0700f9

.field public static final base_card_item_img_margin:I = 0x7f0700fa

.field public static final base_card_item_img_width:I = 0x7f0700fb

.field public static final battery_height:I = 0x7f0700fc

.field public static final battery_history_chart_height:I = 0x7f0700fd

.field public static final battery_history_duration_padding_top:I = 0x7f0700fe

.field public static final battery_history_hardware_padding_top:I = 0x7f0700ff

.field public static final battery_history_title_padding_bottom:I = 0x7f070100

.field public static final battery_meter_height:I = 0x7f070101

.field public static final battery_meter_width:I = 0x7f070102

.field public static final battery_powersave_outline_thickness:I = 0x7f070103

.field public static final battery_width:I = 0x7f070104

.field public static final big_add_account_margin_start:I = 0x7f070105

.field public static final big_add_account_margin_top:I = 0x7f070106

.field public static final big_add_account_title_size:I = 0x7f070107

.field public static final bind_app_widget_dialog_checkbox_bottom_padding:I = 0x7f070108

.field public static final blank_screen_height:I = 0x7f070109

.field public static final blank_screen_icon_height:I = 0x7f07010a

.field public static final blank_screen_icon_width:I = 0x7f07010b

.field public static final blank_screen_text_size:I = 0x7f07010c

.field public static final bluetooth_checkbox_padding:I = 0x7f07010d

.field public static final bluetooth_companion_app_widget:I = 0x7f07010e

.field public static final bluetooth_dialog_padding:I = 0x7f07010f

.field public static final bluetooth_dialog_padding_bottom:I = 0x7f070110

.field public static final bluetooth_dialog_padding_top:I = 0x7f070111

.field public static final bluetooth_find_broadcast_button_one_size:I = 0x7f070112

.field public static final bluetooth_find_broadcast_button_start_margin:I = 0x7f070113

.field public static final bluetooth_find_broadcast_button_two_size:I = 0x7f070114

.field public static final bluetooth_find_broadcast_header_top_margin:I = 0x7f070115

.field public static final bluetooth_find_broadcast_progress_height:I = 0x7f070116

.field public static final bluetooth_find_broadcast_progress_margin_top:I = 0x7f070117

.field public static final bluetooth_find_broadcast_progress_width:I = 0x7f070118

.field public static final bluetooth_pair_padding:I = 0x7f070119

.field public static final bluetooth_pairing_edittext_padding:I = 0x7f07011a

.field public static final bluetooth_pairing_padding:I = 0x7f07011b

.field public static final board_layout_padding_top_bottom:I = 0x7f07011c

.field public static final borderlayout_bg_corner_radius:I = 0x7f07011d

.field public static final borderlayout_bg_stroke_width:I = 0x7f07011e

.field public static final bottom_svg_icon_size:I = 0x7f07011f

.field public static final brightness_seekbar_margin_bottom:I = 0x7f070120

.field public static final broadcast_dialog_btn_margin_bottom:I = 0x7f070121

.field public static final broadcast_dialog_btn_minHeight:I = 0x7f070122

.field public static final broadcast_dialog_btn_radius:I = 0x7f070123

.field public static final broadcast_dialog_btn_text_size:I = 0x7f070124

.field public static final broadcast_dialog_icon_margin_top:I = 0x7f070125

.field public static final broadcast_dialog_icon_size:I = 0x7f070126

.field public static final broadcast_dialog_margin:I = 0x7f070127

.field public static final broadcast_dialog_subtitle_text_size:I = 0x7f070128

.field public static final broadcast_dialog_title_img_margin_top:I = 0x7f070129

.field public static final broadcast_dialog_title_text_margin:I = 0x7f07012a

.field public static final broadcast_dialog_title_text_margin_top:I = 0x7f07012b

.field public static final broadcast_dialog_title_text_size:I = 0x7f07012c

.field public static final bt_battery_padding:I = 0x7f07012d

.field public static final bt_btn_refresh_size:I = 0x7f07012e

.field public static final bt_custom_preference_height:I = 0x7f07012f

.field public static final bt_custom_preference_title_margin:I = 0x7f070130

.field public static final bt_custom_preference_title_size:I = 0x7f070131

.field public static final bt_custom_widget_background_width:I = 0x7f070132

.field public static final bt_icon_elevation:I = 0x7f070133

.field public static final bt_nearby_icon_size:I = 0x7f070134

.field public static final bt_pairing_pin_size:I = 0x7f070135

.field public static final bt_preference_icon_size:I = 0x7f070136

.field public static final bt_preference_title_size:I = 0x7f070137

.field public static final btn_inline_size:I = 0x7f070138

.field public static final btn_radius:I = 0x7f070139

.field public static final button_background_corner_radius:I = 0x7f07013a

.field public static final button_height:I = 0x7f07013b

.field public static final button_highlight_margin_start:I = 0x7f07013c

.field public static final button_layout_padding_top:I = 0x7f07013d

.field public static final button_normal_margin_end:I = 0x7f07013e

.field public static final button_normal_margin_start:I = 0x7f07013f

.field public static final button_vertical_padding:I = 0x7f070140

.field public static final camera_item_margin_bottom:I = 0x7f070141

.field public static final caption_preview_text_size:I = 0x7f070142

.field public static final captioning_preview_height:I = 0x7f070143

.field public static final card_background_radius:I = 0x7f070144

.field public static final card_icon_size:I = 0x7f070145

.field public static final card_instructions_text_size:I = 0x7f070146

.field public static final card_item_bottom:I = 0x7f070147

.field public static final card_key_words_text_size:I = 0x7f070148

.field public static final card_margin_actionbar:I = 0x7f070149

.field public static final card_margin_actionbar_thirteen:I = 0x7f07014a

.field public static final card_margin_edge:I = 0x7f07014b

.field public static final card_margin_edge_interval:I = 0x7f07014c

.field public static final card_margin_edge_start:I = 0x7f07014d

.field public static final card_margin_edge_thirteen:I = 0x7f07014e

.field public static final card_margin_top:I = 0x7f07014f

.field public static final card_padding_start:I = 0x7f070150

.field public static final card_padding_top:I = 0x7f070151

.field public static final card_ringtone_image_size:I = 0x7f070152

.field public static final card_ringtone_margin_bottom:I = 0x7f070153

.field public static final card_ringtone_margin_top:I = 0x7f070154

.field public static final card_ringtone_title_margin_start:I = 0x7f070155

.field public static final card_rv_padding_start:I = 0x7f070156

.field public static final card_stroke_width:I = 0x7f070157

.field public static final cardview_compat_inset_shadow:I = 0x7f070158

.field public static final cardview_default_elevation:I = 0x7f070159

.field public static final cardview_default_radius:I = 0x7f07015a

.field public static final category_text_size:I = 0x7f07015b

.field public static final chartview_divider_height:I = 0x7f07015c

.field public static final chartview_divider_width:I = 0x7f07015d

.field public static final chartview_text_padding:I = 0x7f07015e

.field public static final chartview_trapezoid_margin_bottom:I = 0x7f07015f

.field public static final chartview_trapezoid_margin_start:I = 0x7f070160

.field public static final chartview_trapezoid_radius:I = 0x7f070161

.field public static final checkbox_layout_padding:I = 0x7f070162

.field public static final checkbox_radius:I = 0x7f070163

.field public static final checkbox_widget_min_width:I = 0x7f070164

.field public static final checkbox_width:I = 0x7f070165

.field public static final choose_lock_password_top_margin:I = 0x7f070166

.field public static final choose_lock_pattern_header_text_size:I = 0x7f070167

.field public static final choose_lock_pattern_sub_header_margin_bottom:I = 0x7f070168

.field public static final choose_lock_pattern_tip_margin_top:I = 0x7f070169

.field public static final choose_lockscreen_min_height:I = 0x7f07016a

.field public static final choose_other_unlock_text_size:I = 0x7f07016b

.field public static final choose_other_unlock_view_width:I = 0x7f07016c

.field public static final choose_unlock_below_list:I = 0x7f07016d

.field public static final choose_unlock_bg_radius:I = 0x7f07016e

.field public static final choose_unlock_hint_text_size:I = 0x7f07016f

.field public static final choose_unlock_hint_text_top_margin:I = 0x7f070170

.field public static final choose_unlock_item_bottom_margin:I = 0x7f070171

.field public static final choose_unlock_item_height:I = 0x7f070172

.field public static final choose_unlock_item_icon_left_padding:I = 0x7f070173

.field public static final choose_unlock_item_icon_padding:I = 0x7f070174

.field public static final choose_unlock_item_icon_right_padding:I = 0x7f070175

.field public static final choose_unlock_item_margin:I = 0x7f070176

.field public static final choose_unlock_item_top_margin:I = 0x7f070177

.field public static final choose_unlock_password_footer_bottom_margin:I = 0x7f070178

.field public static final choose_unlock_password_footer_divider_width:I = 0x7f070179

.field public static final choose_unlock_password_footer_margin:I = 0x7f07017a

.field public static final choose_unlock_password_footer_margin_bottom:I = 0x7f07017b

.field public static final choose_unlock_password_footer_margin_cetus:I = 0x7f07017c

.field public static final choose_unlock_password_footer_rigit_margin:I = 0x7f07017d

.field public static final choose_unlock_text_left_margin:I = 0x7f07017e

.field public static final choose_unlock_text_top_margin:I = 0x7f07017f

.field public static final clock_face_margin_start:I = 0x7f070180

.field public static final close_lid_display_setting_video_height:I = 0x7f070181

.field public static final close_lid_display_setting_video_width:I = 0x7f070182

.field public static final close_lid_display_video_marginTop:I = 0x7f070183

.field public static final color_bitmap_point_size:I = 0x7f070184

.field public static final color_mode_preview_height:I = 0x7f070185

.field public static final color_swatch_size:I = 0x7f070186

.field public static final color_swatch_stroke_width:I = 0x7f070187

.field public static final column_split_divider_width:I = 0x7f070188

.field public static final column_split_navigation_width_landscape:I = 0x7f070189

.field public static final column_split_navigation_width_portrait:I = 0x7f07018a

.field public static final common_issu_actionbar_size:I = 0x7f07018b

.field public static final compat_button_inset_horizontal_material:I = 0x7f07018c

.field public static final compat_button_inset_vertical_material:I = 0x7f07018d

.field public static final compat_button_padding_horizontal_material:I = 0x7f07018e

.field public static final compat_button_padding_vertical_material:I = 0x7f07018f

.field public static final compat_control_corner_material:I = 0x7f070190

.field public static final compat_notification_large_icon_max_height:I = 0x7f070191

.field public static final compat_notification_large_icon_max_width:I = 0x7f070192

.field public static final config_activity_embed_split_ratio:I = 0x7f070193

.field public static final confirm_credentials_layout_width:I = 0x7f070194

.field public static final confirm_credentials_security_method_margin:I = 0x7f070195

.field public static final confirm_credentials_side_margin:I = 0x7f070196

.field public static final confirm_credentials_top_margin:I = 0x7f070197

.field public static final confirm_credentials_top_padding:I = 0x7f070198

.field public static final confirm_fingerprint_image_bottom_margin:I = 0x7f070199

.field public static final confirm_fingerprint_image_top_margin:I = 0x7f07019a

.field public static final confirm_password_entry_margin:I = 0x7f07019b

.field public static final confirm_password_entry_top_margin:I = 0x7f07019c

.field public static final confirm_password_header_text_bottom_margin:I = 0x7f07019d

.field public static final confirm_password_header_text_size:I = 0x7f07019e

.field public static final confirm_password_header_text_top_margin:I = 0x7f07019f

.field public static final confirm_password_password_entry_text_size:I = 0x7f0701a0

.field public static final confirm_pattern_bottom_margin:I = 0x7f0701a1

.field public static final confirm_pattern_layout_margin_top:I = 0x7f0701a2

.field public static final confirm_pattern_top_margin:I = 0x7f0701a3

.field public static final contact_photo_width:I = 0x7f0701a4

.field public static final container_margin_bottom:I = 0x7f0701a5

.field public static final container_padding:I = 0x7f0701a6

.field public static final content_margin_left:I = 0x7f0701a7

.field public static final contextual_card_corner_radius:I = 0x7f0701a8

.field public static final contextual_card_dismissal_button_margin_end:I = 0x7f0701a9

.field public static final contextual_card_dismissal_button_margin_start:I = 0x7f0701aa

.field public static final contextual_card_dismissal_margin_top:I = 0x7f0701ab

.field public static final contextual_card_dismissal_side_margin:I = 0x7f0701ac

.field public static final contextual_card_icon_padding_start:I = 0x7f0701ad

.field public static final contextual_card_icon_size:I = 0x7f0701ae

.field public static final contextual_card_padding_end:I = 0x7f0701af

.field public static final contextual_card_side_margin:I = 0x7f0701b0

.field public static final contextual_card_text_padding_start:I = 0x7f0701b1

.field public static final contextual_card_vertical_margin:I = 0x7f0701b2

.field public static final contextual_condition_card_title_margin_bottom:I = 0x7f0701b3

.field public static final contextual_condition_footer_height:I = 0x7f0701b4

.field public static final contextual_condition_footer_padding_end:I = 0x7f0701b5

.field public static final contextual_condition_footer_padding_top:I = 0x7f0701b6

.field public static final contextual_condition_full_card_padding_bottom:I = 0x7f0701b7

.field public static final contextual_condition_full_card_padding_end:I = 0x7f0701b8

.field public static final contextual_condition_full_card_padding_start:I = 0x7f0701b9

.field public static final contextual_condition_full_card_padding_top:I = 0x7f0701ba

.field public static final contextual_condition_half_card_padding_top:I = 0x7f0701bb

.field public static final contextual_condition_half_card_summary_margin_bottom:I = 0x7f0701bc

.field public static final contextual_condition_half_card_title_margin_top:I = 0x7f0701bd

.field public static final contextual_condition_header_icon_margin_end:I = 0x7f0701be

.field public static final contextual_condition_header_icon_width_height:I = 0x7f0701bf

.field public static final contextual_condition_header_icons_margin_start:I = 0x7f0701c0

.field public static final contextual_condition_header_indicator_padding_end:I = 0x7f0701c1

.field public static final contextual_condition_header_indicator_padding_start:I = 0x7f0701c2

.field public static final contextual_condition_header_indicator_padding_top:I = 0x7f0701c3

.field public static final contextual_condition_header_padding_bottom:I = 0x7f0701c4

.field public static final contextual_condition_header_padding_top:I = 0x7f0701c5

.field public static final contextual_full_card_padding_end:I = 0x7f0701c6

.field public static final contextual_half_card_padding_bottom:I = 0x7f0701c7

.field public static final contextual_half_card_padding_top:I = 0x7f0701c8

.field public static final contextual_half_card_title_margin_top:I = 0x7f0701c9

.field public static final control_center_preference_border_padding_inside:I = 0x7f0701ca

.field public static final control_center_preference_checkbox_borderlayout_width:I = 0x7f0701cb

.field public static final control_center_preference_checkbox_content_height:I = 0x7f0701cc

.field public static final control_center_preference_checkbox_content_width:I = 0x7f0701cd

.field public static final control_center_preference_checkbox_corner_radius:I = 0x7f0701ce

.field public static final control_center_preference_checkbox_desc_line_height:I = 0x7f0701cf

.field public static final control_center_preference_checkbox_desc_margin_top:I = 0x7f0701d0

.field public static final control_center_preference_checkbox_desc_text_size:I = 0x7f0701d1

.field public static final control_center_preference_checkbox_text_max_width:I = 0x7f0701d2

.field public static final control_center_preference_checkbox_title_line_height:I = 0x7f0701d3

.field public static final control_center_preference_checkbox_title_margin_top:I = 0x7f0701d4

.field public static final control_center_preference_checkbox_title_text_size:I = 0x7f0701d5

.field public static final control_center_preference_margin_vertical:I = 0x7f0701d6

.field public static final control_center_preference_vertical_margin:I = 0x7f0701d7

.field public static final conversation_bubble_width_snap:I = 0x7f0701d8

.field public static final conversation_icon_size:I = 0x7f0701d9

.field public static final conversation_message_contact_icon_text_size:I = 0x7f0701da

.field public static final conversation_message_list_padding:I = 0x7f0701db

.field public static final conversation_message_text_size:I = 0x7f0701dc

.field public static final conversation_status_text_size:I = 0x7f0701dd

.field public static final count_time_bar_margin_left:I = 0x7f0701de

.field public static final count_time_bar_margin_right:I = 0x7f0701df

.field public static final crypt_keeper_edit_text_ime_padding:I = 0x7f0701e0

.field public static final crypt_keeper_password_top_margin:I = 0x7f0701e1

.field public static final crypt_keeper_password_width:I = 0x7f0701e2

.field public static final crypt_keeper_status_margin_top:I = 0x7f0701e3

.field public static final crypt_keeper_status_text_size:I = 0x7f0701e4

.field public static final crypt_keeper_title_text_size:I = 0x7f0701e5

.field public static final custom_carrier_page_margin_end:I = 0x7f0701e6

.field public static final custom_carrier_page_margin_start:I = 0x7f0701e7

.field public static final custom_checkbox_text_margin_start:I = 0x7f0701e8

.field public static final custom_checkbox_text_size:I = 0x7f0701e9

.field public static final custom_input_bottom:I = 0x7f0701ea

.field public static final custom_input_left:I = 0x7f0701eb

.field public static final custom_input_right:I = 0x7f0701ec

.field public static final custom_input_top:I = 0x7f0701ed

.field public static final custom_match_parent:I = 0x7f0701ee

.field public static final custom_sound_background_height:I = 0x7f0701ef

.field public static final custom_sound_checkbox_height:I = 0x7f0701f0

.field public static final custom_sound_checkbox_marginEnd:I = 0x7f0701f1

.field public static final dark_mode_background_margin:I = 0x7f0701f2

.field public static final dark_mode_outer_view_height:I = 0x7f0701f3

.field public static final dark_mode_outer_view_padding:I = 0x7f0701f4

.field public static final dark_mode_outer_view_width:I = 0x7f0701f5

.field public static final dark_mode_textSize:I = 0x7f0701f6

.field public static final dark_mode_view_height:I = 0x7f0701f7

.field public static final dark_mode_view_width:I = 0x7f0701f8

.field public static final dashboard_tile_foreground_image_inset:I = 0x7f0701f9

.field public static final dashboard_tile_foreground_image_size:I = 0x7f0701fa

.field public static final dashboard_tile_image_size:I = 0x7f0701fb

.field public static final data_usage_chart_height:I = 0x7f0701fc

.field public static final data_usage_chart_optimalWidth:I = 0x7f0701fd

.field public static final date_picker_checkbox_padding_end:I = 0x7f0701fe

.field public static final date_picker_checkbox_padding_start:I = 0x7f0701ff

.field public static final date_picker_layout_padding:I = 0x7f070200

.field public static final date_picker_lunar_text_size:I = 0x7f070201

.field public static final datetime_margin_bottom:I = 0x7f070202

.field public static final datetime_margin_top:I = 0x7f070203

.field public static final def_drawer_elevation:I = 0x7f070204

.field public static final default_dimension:I = 0x7f070205

.field public static final default_icon_bitmap_size:I = 0x7f070206

.field public static final delete_profile_dialog_padding_left_right:I = 0x7f070207

.field public static final delete_profile_dialog_padding_top:I = 0x7f070208

.field public static final demo_button_height:I = 0x7f070209

.field public static final demo_button_margin_bottom:I = 0x7f07020a

.field public static final demo_container_height:I = 0x7f07020b

.field public static final demo_container_width:I = 0x7f07020c

.field public static final demo_height:I = 0x7f07020d

.field public static final demo_margin_end:I = 0x7f07020e

.field public static final demo_margin_start:I = 0x7f07020f

.field public static final demo_width:I = 0x7f070210

.field public static final description_margin_sides:I = 0x7f070211

.field public static final description_margin_top:I = 0x7f070212

.field public static final design_appbar_elevation:I = 0x7f070213

.field public static final design_bottom_navigation_active_item_max_width:I = 0x7f070214

.field public static final design_bottom_navigation_active_item_min_width:I = 0x7f070215

.field public static final design_bottom_navigation_active_text_size:I = 0x7f070216

.field public static final design_bottom_navigation_elevation:I = 0x7f070217

.field public static final design_bottom_navigation_height:I = 0x7f070218

.field public static final design_bottom_navigation_icon_size:I = 0x7f070219

.field public static final design_bottom_navigation_item_max_width:I = 0x7f07021a

.field public static final design_bottom_navigation_item_min_width:I = 0x7f07021b

.field public static final design_bottom_navigation_label_padding:I = 0x7f07021c

.field public static final design_bottom_navigation_margin:I = 0x7f07021d

.field public static final design_bottom_navigation_shadow_height:I = 0x7f07021e

.field public static final design_bottom_navigation_text_size:I = 0x7f07021f

.field public static final design_bottom_sheet_elevation:I = 0x7f070220

.field public static final design_bottom_sheet_modal_elevation:I = 0x7f070221

.field public static final design_bottom_sheet_peek_height_min:I = 0x7f070222

.field public static final design_fab_border_width:I = 0x7f070223

.field public static final design_fab_elevation:I = 0x7f070224

.field public static final design_fab_image_size:I = 0x7f070225

.field public static final design_fab_size_mini:I = 0x7f070226

.field public static final design_fab_size_normal:I = 0x7f070227

.field public static final design_fab_translation_z_hovered_focused:I = 0x7f070228

.field public static final design_fab_translation_z_pressed:I = 0x7f070229

.field public static final design_navigation_elevation:I = 0x7f07022a

.field public static final design_navigation_icon_padding:I = 0x7f07022b

.field public static final design_navigation_icon_size:I = 0x7f07022c

.field public static final design_navigation_item_horizontal_padding:I = 0x7f07022d

.field public static final design_navigation_item_icon_padding:I = 0x7f07022e

.field public static final design_navigation_item_vertical_padding:I = 0x7f07022f

.field public static final design_navigation_max_width:I = 0x7f070230

.field public static final design_navigation_padding_bottom:I = 0x7f070231

.field public static final design_navigation_separator_vertical_padding:I = 0x7f070232

.field public static final design_snackbar_action_inline_max_width:I = 0x7f070233

.field public static final design_snackbar_action_text_color_alpha:I = 0x7f070234

.field public static final design_snackbar_background_corner_radius:I = 0x7f070235

.field public static final design_snackbar_elevation:I = 0x7f070236

.field public static final design_snackbar_extra_spacing_horizontal:I = 0x7f070237

.field public static final design_snackbar_max_width:I = 0x7f070238

.field public static final design_snackbar_min_width:I = 0x7f070239

.field public static final design_snackbar_padding_horizontal:I = 0x7f07023a

.field public static final design_snackbar_padding_vertical:I = 0x7f07023b

.field public static final design_snackbar_padding_vertical_2lines:I = 0x7f07023c

.field public static final design_snackbar_text_size:I = 0x7f07023d

.field public static final design_tab_max_width:I = 0x7f07023e

.field public static final design_tab_scrollable_min_width:I = 0x7f07023f

.field public static final design_tab_text_size:I = 0x7f070240

.field public static final design_tab_text_size_2line:I = 0x7f070241

.field public static final design_textinput_caption_translate_y:I = 0x7f070242

.field public static final detail_layout_padding:I = 0x7f070243

.field public static final developer_option_dialog_margin_start:I = 0x7f070244

.field public static final developer_option_dialog_margin_top:I = 0x7f070245

.field public static final developer_option_dialog_min_height:I = 0x7f070246

.field public static final developer_option_dialog_padding_start:I = 0x7f070247

.field public static final device_admin_add_margin_bottom:I = 0x7f070248

.field public static final device_admin_add_margin_top:I = 0x7f070249

.field public static final device_card_margin_start:I = 0x7f07024a

.field public static final device_description_icon_size:I = 0x7f07024b

.field public static final device_layout_margin_top:I = 0x7f07024c

.field public static final device_memory_usage_button_height:I = 0x7f07024d

.field public static final device_memory_usage_button_width:I = 0x7f07024e

.field public static final device_name_card_height:I = 0x7f07024f

.field public static final device_params_padding_top:I = 0x7f070250

.field public static final devices_params_grid_item_title_size:I = 0x7f070251

.field public static final devices_params_grid_item_value_size:I = 0x7f070252

.field public static final dialog_btn_bg_corner:I = 0x7f070253

.field public static final dialog_btn_height_size:I = 0x7f070254

.field public static final dialog_btn_vertical_group_top_margin:I = 0x7f070255

.field public static final dialog_btn_vertical_top_margin:I = 0x7f070256

.field public static final dialog_content_diff_margin:I = 0x7f070257

.field public static final dialog_message_text_size:I = 0x7f070258

.field public static final dialog_title_horizontal_padding:I = 0x7f070259

.field public static final dialog_title_vertical_padding_bottom:I = 0x7f07025a

.field public static final dialog_title_vertical_padding_top:I = 0x7f07025b

.field public static final disabled_alpha_material_dark:I = 0x7f07025c

.field public static final disabled_alpha_material_light:I = 0x7f07025d

.field public static final disappear_y_translation:I = 0x7f07025e

.field public static final divider_height:I = 0x7f07025f

.field public static final divider_margin_bottom:I = 0x7f070260

.field public static final divider_margin_left_right:I = 0x7f070261

.field public static final divider_margin_top:I = 0x7f070262

.field public static final divider_margin_top_bottom:I = 0x7f070263

.field public static final divider_width:I = 0x7f070264

.field public static final dns_edittext_padding:I = 0x7f070265

.field public static final dot_end:I = 0x7f070266

.field public static final dot_view_top:I = 0x7f070267

.field public static final dot_width_height:I = 0x7f070268

.field public static final dpp_border_thickness:I = 0x7f070269

.field public static final dpp_corner_length:I = 0x7f07026a

.field public static final dpp_corner_thickness:I = 0x7f07026b

.field public static final dpp_guideline_thickness:I = 0x7f07026c

.field public static final dream_item_content_padding:I = 0x7f07026d

.field public static final dream_item_corner_radius:I = 0x7f07026e

.field public static final dream_item_icon_padding:I = 0x7f07026f

.field public static final dream_item_icon_size:I = 0x7f070270

.field public static final dream_item_min_column_width:I = 0x7f070271

.field public static final dream_item_summary_text_size:I = 0x7f070272

.field public static final dream_item_title_margin_bottom:I = 0x7f070273

.field public static final dream_item_title_margin_horizontal:I = 0x7f070274

.field public static final dream_item_title_margin_top:I = 0x7f070275

.field public static final dream_item_title_text_size:I = 0x7f070276

.field public static final dream_picker_margin_horizontal:I = 0x7f070277

.field public static final dream_preference_card_padding:I = 0x7f070278

.field public static final dream_preference_margin_bottom:I = 0x7f070279

.field public static final dream_preview_placeholder_width:I = 0x7f07027a

.field public static final drop_down_menu_shadow_alpha:I = 0x7f07027b

.field public static final edit_button_margin_top:I = 0x7f07027c

.field public static final edit_event_title_height:I = 0x7f07027d

.field public static final edit_text_clear_icon_padding:I = 0x7f07027e

.field public static final edit_text_margin:I = 0x7f07027f

.field public static final edit_text_padding:I = 0x7f070280

.field public static final elevation:I = 0x7f070281

.field public static final empty_text_layout_height:I = 0x7f070282

.field public static final empty_text_padding:I = 0x7f070283

.field public static final end_frame_layout_height:I = 0x7f070284

.field public static final exit_layout_width_height:I = 0x7f070285

.field public static final exit_linearlayout_height:I = 0x7f070286

.field public static final exit_linearlayout_top:I = 0x7f070287

.field public static final exit_top_bottom_start_end:I = 0x7f070288

.field public static final expanded_title_margin_end:I = 0x7f070289

.field public static final expanded_title_margin_start:I = 0x7f07028a

.field public static final expert_hue_thumb_height:I = 0x7f07028b

.field public static final external_ram_notification_size:I = 0x7f07028c

.field public static final face_detail_image_top_margin:I = 0x7f07028d

.field public static final face_detail_name_edittext_bottom_margin:I = 0x7f07028e

.field public static final face_detail_name_edittext_horizontal_margin:I = 0x7f07028f

.field public static final face_detail_name_edittext_text_size:I = 0x7f070290

.field public static final face_detail_name_text_bottom_margin:I = 0x7f070291

.field public static final face_detail_name_text_left_margin:I = 0x7f070292

.field public static final face_detail_name_text_size:I = 0x7f070293

.field public static final face_detail_name_text_top_margin:I = 0x7f070294

.field public static final face_preview_scale:I = 0x7f070295

.field public static final face_preview_translate_x:I = 0x7f070296

.field public static final face_preview_translate_y:I = 0x7f070297

.field public static final fake_landscape_screen_minor_size:I = 0x7f070298

.field public static final fallback_layout_margin_bottom:I = 0x7f070299

.field public static final fallback_progressbar_margin_bottom:I = 0x7f07029a

.field public static final fallback_progressbar_min_width:I = 0x7f07029b

.field public static final fallback_progressbar_text_size:I = 0x7f07029c

.field public static final fastscroll_default_thickness:I = 0x7f07029d

.field public static final fastscroll_margin:I = 0x7f07029e

.field public static final fastscroll_minimum_range:I = 0x7f07029f

.field public static final fhd_text_summary_view_margin_left:I = 0x7f0702a0

.field public static final fhd_text_view_margin_left:I = 0x7f0702a1

.field public static final finger_detail_name_text_bottom_margin:I = 0x7f0702a2

.field public static final finger_detail_name_text_size:I = 0x7f0702a3

.field public static final finger_face_detail_name_edittext_text_size:I = 0x7f0702a4

.field public static final finger_face_detail_name_text_left_margin:I = 0x7f0702a5

.field public static final finger_face_detail_name_text_top_margin:I = 0x7f0702a6

.field public static final fingerprint_animation_size:I = 0x7f0702a7

.field public static final fingerprint_decor_padding_top:I = 0x7f0702a8

.field public static final fingerprint_detail_button_top_bottom:I = 0x7f0702a9

.field public static final fingerprint_detail_button_top_margin:I = 0x7f0702aa

.field public static final fingerprint_detail_edittext_horizontal_margin:I = 0x7f0702ab

.field public static final fingerprint_detail_page_layout_margin_top:I = 0x7f0702ac

.field public static final fingerprint_dot_radius:I = 0x7f0702ad

.field public static final fingerprint_enrolling_content_margin_vertical:I = 0x7f0702ae

.field public static final fingerprint_error_text_appear_distance:I = 0x7f0702af

.field public static final fingerprint_error_text_disappear_distance:I = 0x7f0702b0

.field public static final fingerprint_face_detail_button_bottom:I = 0x7f0702b1

.field public static final fingerprint_face_detail_button_horizontal_margin:I = 0x7f0702b2

.field public static final fingerprint_face_detail_edittext_top_margin:I = 0x7f0702b3

.field public static final fingerprint_face_detail_image_top_margin:I = 0x7f0702b4

.field public static final fingerprint_find_sensor_graphic_size:I = 0x7f0702b5

.field public static final fingerprint_finish_max_size:I = 0x7f0702b6

.field public static final fingerprint_illustration_aspect_ratio:I = 0x7f0702b7

.field public static final fingerprint_progress_bar_max_size:I = 0x7f0702b8

.field public static final fingerprint_progress_bar_min_size:I = 0x7f0702b9

.field public static final fingerprint_pulse_radius:I = 0x7f0702ba

.field public static final firewall_button_group_width:I = 0x7f0702bb

.field public static final firewall_button_group_width_half:I = 0x7f0702bc

.field public static final first_row_video_margin_bottom:I = 0x7f0702bd

.field public static final first_row_video_margin_top:I = 0x7f0702be

.field public static final flat_screen_status_bar_height:I = 0x7f0702bf

.field public static final fod_authentication_dialog_shift_button:I = 0x7f0702c0

.field public static final fod_authentication_dialog_subtitle:I = 0x7f0702c1

.field public static final fod_authentication_dialog_title:I = 0x7f0702c2

.field public static final fold_description_text_size:I = 0x7f0702c3

.field public static final font_bubble_left_width:I = 0x7f0702c4

.field public static final font_bubble_margin:I = 0x7f0702c5

.field public static final font_bubble_marginTop:I = 0x7f0702c6

.field public static final font_bubble_padding:I = 0x7f0702c7

.field public static final font_bubble_radius:I = 0x7f0702c8

.field public static final font_bubble_right_width:I = 0x7f0702c9

.field public static final font_bubble_size:I = 0x7f0702ca

.field public static final font_checkbox_radius:I = 0x7f0702cb

.field public static final font_hint_view_layout_height:I = 0x7f0702cc

.field public static final font_hint_view_margin_bottom:I = 0x7f0702cd

.field public static final font_hint_view_margin_top:I = 0x7f0702ce

.field public static final font_recommend_bottom:I = 0x7f0702cf

.field public static final font_recommend_height:I = 0x7f0702d0

.field public static final font_recommend_size:I = 0x7f0702d1

.field public static final font_scroll_view_height:I = 0x7f0702d2

.field public static final font_settings_bottom_card_height:I = 0x7f0702d3

.field public static final font_settings_zoom_icon_size_land:I = 0x7f0702d4

.field public static final font_settings_zoom_icon_size_port:I = 0x7f0702d5

.field public static final font_size_huge_font_tips:I = 0x7f0702d6

.field public static final font_size_preview_padding_start:I = 0x7f0702d7

.field public static final font_size_view_big_font_size:I = 0x7f0702d8

.field public static final font_size_view_big_radius:I = 0x7f0702d9

.field public static final font_size_view_height:I = 0x7f0702da

.field public static final font_size_view_hit_margin_bottom:I = 0x7f0702db

.field public static final font_size_view_hit_margin_top:I = 0x7f0702dc

.field public static final font_size_view_hit_view_height:I = 0x7f0702dd

.field public static final font_size_view_margin_bottom:I = 0x7f0702de

.field public static final font_size_view_small_font_size:I = 0x7f0702df

.field public static final font_size_view_small_radius:I = 0x7f0702e0

.field public static final font_size_view_width:I = 0x7f0702e1

.field public static final font_view_margin_bottom:I = 0x7f0702e2

.field public static final font_weight_seekbar_bottom_margin:I = 0x7f0702e3

.field public static final font_weight_view_margin_top:I = 0x7f0702e4

.field public static final fop_password_text_margin_bottom:I = 0x7f0702e5

.field public static final forget_password_button_bottom_margin:I = 0x7f0702e6

.field public static final forget_password_button_top_margin:I = 0x7f0702e7

.field public static final forget_password_dialog_btn_margin_left_right:I = 0x7f0702e8

.field public static final forget_password_dialog_text_margin_left_right:I = 0x7f0702e9

.field public static final forget_password_text_size:I = 0x7f0702ea

.field public static final frame_207_height:I = 0x7f0702eb

.field public static final frame_207_margin_top:I = 0x7f0702ec

.field public static final freeform_flashback_item_height:I = 0x7f0702ed

.field public static final freeform_guide_item_padding:I = 0x7f0702ee

.field public static final freeform_settings_preference_bottom_padding_inner:I = 0x7f0702ef

.field public static final freeform_settings_preference_item_padding_inner:I = 0x7f0702f0

.field public static final freeform_text_margin_left:I = 0x7f0702f1

.field public static final freeform_text_margin_right:I = 0x7f0702f2

.field public static final freeform_vedio_view_height:I = 0x7f0702f3

.field public static final freeform_vedio_view_width:I = 0x7f0702f4

.field public static final full_image_height_default:I = 0x7f0702f5

.field public static final full_image_margin_bottom:I = 0x7f0702f6

.field public static final full_image_margin_top:I = 0x7f0702f7

.field public static final full_image_width_default:I = 0x7f0702f8

.field public static final general_item_margin_start:I = 0x7f0702f9

.field public static final general_item_margin_top:I = 0x7f0702fa

.field public static final gesture_img_checkbox_height:I = 0x7f0702fb

.field public static final gesture_img_checkbox_width:I = 0x7f0702fc

.field public static final gestures_play_button_size:I = 0x7f0702fd

.field public static final gestures_settings_padding_top_bottom:I = 0x7f0702fe

.field public static final godzillaLeftPadding:I = 0x7f0702ff

.field public static final grid_item_btn_view_height:I = 0x7f070300

.field public static final grid_item_padding:I = 0x7f070301

.field public static final guide_button_bottom:I = 0x7f070302

.field public static final guide_button_height:I = 0x7f070303

.field public static final guide_button_text_size:I = 0x7f070304

.field public static final guide_button_top:I = 0x7f070305

.field public static final guide_button_width:I = 0x7f070306

.field public static final guide_text_height:I = 0x7f070307

.field public static final guide_text_margin_top:I = 0x7f070308

.field public static final guide_text_size:I = 0x7f070309

.field public static final guide_title_margin_top:I = 0x7f07030a

.field public static final guide_title_text_size:I = 0x7f07030b

.field public static final gxzw_anim_select_view_gesture_mode_margin_bottom:I = 0x7f07030c

.field public static final gxzw_anim_select_view_margin_bottom:I = 0x7f07030d

.field public static final gxzw_new_fingerprint_indicate_margin:I = 0x7f07030e

.field public static final gxzw_new_fingerprint_step_height:I = 0x7f07030f

.field public static final gxzw_new_fingerprint_step_width:I = 0x7f070310

.field public static final gxzw_new_fingerprint_top_margin:I = 0x7f070311

.field public static final gxzw_new_fingerprint_video_margin:I = 0x7f070312

.field public static final handy_mode_guide_circle_move_distance:I = 0x7f070313

.field public static final handy_mode_guide_circle_radius_large:I = 0x7f070314

.field public static final handy_mode_guide_circle_radius_small:I = 0x7f070315

.field public static final handy_mode_guide_circle_vertical_center:I = 0x7f070316

.field public static final handy_mode_guide_margin_vertical:I = 0x7f070317

.field public static final handy_mode_guide_screen_height:I = 0x7f070318

.field public static final handy_mode_guide_screen_left:I = 0x7f070319

.field public static final handy_mode_guide_screen_top:I = 0x7f07031a

.field public static final handy_mode_guide_screen_width:I = 0x7f07031b

.field public static final haptic_detail_text_view_padding_start_end:I = 0x7f07031c

.field public static final haptic_help_text_view_margin_end:I = 0x7f07031d

.field public static final haptic_help_text_view_margin_start:I = 0x7f07031e

.field public static final haptic_help_text_view_start_view_margin_start:I = 0x7f07031f

.field public static final haptic_help_textview_internal_margin_top_bottom:I = 0x7f070320

.field public static final haptic_preference_container_padding:I = 0x7f070321

.field public static final haptic_preview_height:I = 0x7f070322

.field public static final haptic_preview_margin_bottom:I = 0x7f070323

.field public static final haptic_preview_margin_top:I = 0x7f070324

.field public static final haptic_preview_text_margin_bottom:I = 0x7f070325

.field public static final haptic_preview_text_size:I = 0x7f070326

.field public static final haptic_text_size:I = 0x7f070327

.field public static final header_icon_size:I = 0x7f070328

.field public static final header_icon_width:I = 0x7f070329

.field public static final header_icon_xiaomi_account_size:I = 0x7f07032a

.field public static final headset_anc_high:I = 0x7f07032b

.field public static final headset_anc_image_high:I = 0x7f07032c

.field public static final headset_anc_layout_hight:I = 0x7f07032d

.field public static final headset_anc_layout_marginTop:I = 0x7f07032e

.field public static final headset_anc_layout_width:I = 0x7f07032f

.field public static final headset_anc_level_Text_height:I = 0x7f070330

.field public static final headset_anc_level_Text_marginBottom:I = 0x7f070331

.field public static final headset_anc_level_Text_marginLeft:I = 0x7f070332

.field public static final headset_anc_level_Text_marginRight:I = 0x7f070333

.field public static final headset_anc_level_Text_marginTop:I = 0x7f070334

.field public static final headset_anc_level_layout_height:I = 0x7f070335

.field public static final headset_anc_level_layout_marginLeft:I = 0x7f070336

.field public static final headset_anc_level_layout_marginRight:I = 0x7f070337

.field public static final headset_anc_level_layout_marginTop:I = 0x7f070338

.field public static final headset_anc_margintop:I = 0x7f070339

.field public static final headset_anc_mode_text_size:I = 0x7f07033a

.field public static final headset_anc_text_marginBottom:I = 0x7f07033b

.field public static final headset_anc_text_marginTop:I = 0x7f07033c

.field public static final headset_anc_text_marginhigh:I = 0x7f07033d

.field public static final headset_anc_text_size:I = 0x7f07033e

.field public static final headset_battery_diverleft:I = 0x7f07033f

.field public static final headset_battery_image:I = 0x7f070340

.field public static final headset_battery_image_high:I = 0x7f070341

.field public static final headset_battery_image_wedth:I = 0x7f070342

.field public static final headset_battery_info_margin:I = 0x7f070343

.field public static final headset_battery_info_wedth:I = 0x7f070344

.field public static final headset_battery_layout_hight:I = 0x7f070345

.field public static final headset_battery_marginLeft:I = 0x7f070346

.field public static final headset_battery_margin_size:I = 0x7f070347

.field public static final headset_battery_second_text_hight:I = 0x7f070348

.field public static final headset_battery_second_text_marginLeft:I = 0x7f070349

.field public static final headset_battery_second_text_size:I = 0x7f07034a

.field public static final headset_battery_text_height:I = 0x7f07034b

.field public static final headset_battery_text_marginLeft:I = 0x7f07034c

.field public static final headset_battery_text_marginTop:I = 0x7f07034d

.field public static final headset_battery_text_size:I = 0x7f07034e

.field public static final headset_blank_hight:I = 0x7f07034f

.field public static final headset_divider_layout_marginBottom:I = 0x7f070350

.field public static final headset_divider_layout_marginLeft:I = 0x7f070351

.field public static final headset_divider_layout_marginTop:I = 0x7f070352

.field public static final headset_find_device_bottom_tip_width:I = 0x7f070353

.field public static final headset_find_device_bottom_tips_textSize:I = 0x7f070354

.field public static final headset_find_device_card_content_marginStart:I = 0x7f070355

.field public static final headset_find_device_card_family_margin_end:I = 0x7f070356

.field public static final headset_find_device_card_family_margin_start:I = 0x7f070357

.field public static final headset_find_device_card_height:I = 0x7f070358

.field public static final headset_find_device_card_linear_height:I = 0x7f070359

.field public static final headset_find_device_card_linear_image_height:I = 0x7f07035a

.field public static final headset_find_device_card_linear_image_marginTop:I = 0x7f07035b

.field public static final headset_find_device_card_linear_image_width:I = 0x7f07035c

.field public static final headset_find_device_card_linear_text2_marginTop:I = 0x7f07035d

.field public static final headset_find_device_card_linear_text2_textSize:I = 0x7f07035e

.field public static final headset_find_device_card_linear_text_marginTop:I = 0x7f07035f

.field public static final headset_find_device_card_linear_text_textSize:I = 0x7f070360

.field public static final headset_find_device_card_marginStart:I = 0x7f070361

.field public static final headset_find_device_card_marginStart_list:I = 0x7f070362

.field public static final headset_find_device_card_width:I = 0x7f070363

.field public static final headset_find_device_image_connect_state_height:I = 0x7f070364

.field public static final headset_find_device_image_connect_state_width:I = 0x7f070365

.field public static final headset_find_device_image_device_icon_height:I = 0x7f070366

.field public static final headset_find_device_image_device_icon_width:I = 0x7f070367

.field public static final headset_find_device_linear_attention_marginStart:I = 0x7f070368

.field public static final headset_find_device_linear_attention_marginTop:I = 0x7f070369

.field public static final headset_find_device_linear_bottom_tip_margin:I = 0x7f07036a

.field public static final headset_find_device_linear_bottom_tip_marginTop:I = 0x7f07036b

.field public static final headset_find_device_linear_device_icon_marginStart:I = 0x7f07036c

.field public static final headset_find_device_linear_device_icon_marginTop:I = 0x7f07036d

.field public static final headset_find_device_linear_find_module_height:I = 0x7f07036e

.field public static final headset_find_device_linear_find_module_marginTop:I = 0x7f07036f

.field public static final headset_find_device_linear_layout_marginTop:I = 0x7f070370

.field public static final headset_find_device_linear_tips_height:I = 0x7f070371

.field public static final headset_find_device_linear_tips_marginEnd:I = 0x7f070372

.field public static final headset_find_device_linear_tips_marginStart:I = 0x7f070373

.field public static final headset_find_device_tex_device_name_layout_marginTop:I = 0x7f070374

.field public static final headset_find_device_tex_device_name_textSize:I = 0x7f070375

.field public static final headset_find_device_tex_device_name_width:I = 0x7f070376

.field public static final headset_find_device_text_attention_textSize:I = 0x7f070377

.field public static final headset_find_device_text_connect_state_marginStart:I = 0x7f070378

.field public static final headset_find_device_text_connect_state_textSize:I = 0x7f070379

.field public static final headset_find_device_text_tips_marginTop:I = 0x7f07037a

.field public static final headset_find_device_text_tips_textSize:I = 0x7f07037b

.field public static final headset_find_device_tv_state_title_width:I = 0x7f07037c

.field public static final headset_fitness_checking_icon_size_pad:I = 0x7f07037d

.field public static final headset_fitness_checking_text_size:I = 0x7f07037e

.field public static final headset_fitness_icon_height:I = 0x7f07037f

.field public static final headset_fitness_icon_margin_top:I = 0x7f070380

.field public static final headset_fitness_icon_width:I = 0x7f070381

.field public static final headset_fitness_l_r_width:I = 0x7f070382

.field public static final headset_fitness_layout_margin:I = 0x7f070383

.field public static final headset_fitness_result_l_r_width:I = 0x7f070384

.field public static final headset_fitness_result_text_size:I = 0x7f070385

.field public static final headset_fitness_summary_margin:I = 0x7f070386

.field public static final headset_fitness_summary_margin_top:I = 0x7f070387

.field public static final headset_fitness_summary_text_size:I = 0x7f070388

.field public static final headset_fitness_title_margin_top:I = 0x7f070389

.field public static final headset_fitness_title_text_size:I = 0x7f07038a

.field public static final headset_fitness_view_width:I = 0x7f07038b

.field public static final headset_ignore_layout_hight:I = 0x7f07038c

.field public static final headset_ignore_layout_marginTop:I = 0x7f07038d

.field public static final headset_ignore_marginLeft:I = 0x7f07038e

.field public static final headset_ignore_marginTop:I = 0x7f07038f

.field public static final headset_image_size:I = 0x7f070390

.field public static final headset_pad_fitness_summary_margin:I = 0x7f070391

.field public static final headset_padding:I = 0x7f070392

.field public static final headset_press_key_anc_list_bottom:I = 0x7f070393

.field public static final headset_press_key_anc_list_margin:I = 0x7f070394

.field public static final headset_press_key_anc_list_text_margin:I = 0x7f070395

.field public static final headset_press_key_anc_list_title_bottom:I = 0x7f070396

.field public static final headset_press_key_anc_list_title_start:I = 0x7f070397

.field public static final headset_press_key_anc_list_title_top:I = 0x7f070398

.field public static final headset_press_key_anc_list_top:I = 0x7f070399

.field public static final headset_press_key_check_list_padding:I = 0x7f07039a

.field public static final headset_press_key_icon_size:I = 0x7f07039b

.field public static final headset_transparent_level_Text_marginLeft:I = 0x7f07039c

.field public static final headset_transparent_level_Text_marginRight:I = 0x7f07039d

.field public static final headset_version_layout_layout_hight:I = 0x7f07039e

.field public static final headset_version_layout_layout_width:I = 0x7f07039f

.field public static final headset_version_layout_marginTop:I = 0x7f0703a0

.field public static final headset_version_name_layout_width:I = 0x7f0703a1

.field public static final headset_version_renameLayout_checkversion_red_margin:I = 0x7f0703a2

.field public static final headset_version_renameLayout_checkversion_width:I = 0x7f0703a3

.field public static final headset_version_renameLayout_first_text_hight:I = 0x7f0703a4

.field public static final headset_version_renameLayout_first_text_size:I = 0x7f0703a5

.field public static final headset_version_renameLayout_first_text_width:I = 0x7f0703a6

.field public static final headset_version_renameLayout_marginLeft:I = 0x7f0703a7

.field public static final headset_version_renameLayout_marginTop:I = 0x7f0703a8

.field public static final headset_version_renameLayout_second_text_height:I = 0x7f0703a9

.field public static final headset_version_renameLayout_second_text_marginTop:I = 0x7f0703aa

.field public static final headset_version_renameLayout_second_text_size:I = 0x7f0703ab

.field public static final headset_version_renameLayout_second_text_width:I = 0x7f0703ac

.field public static final headset_version_renameLayout_tofirst_marginLeft:I = 0x7f0703ad

.field public static final help_category_margin_top:I = 0x7f0703ae

.field public static final help_category_padding_start_end:I = 0x7f0703af

.field public static final highlight_alpha_material_colored:I = 0x7f0703b0

.field public static final highlight_alpha_material_dark:I = 0x7f0703b1

.field public static final highlight_alpha_material_light:I = 0x7f0703b2

.field public static final highlight_list_margin_bottom:I = 0x7f0703b3

.field public static final highlight_list_radius:I = 0x7f0703b4

.field public static final highlight_side_left_margin:I = 0x7f0703b5

.field public static final highlight_side_right_margin:I = 0x7f0703b6

.field public static final highlight_top_margin:I = 0x7f0703b7

.field public static final hint_alpha_material_dark:I = 0x7f0703b8

.field public static final hint_alpha_material_light:I = 0x7f0703b9

.field public static final hint_pressed_alpha_material_dark:I = 0x7f0703ba

.field public static final hint_pressed_alpha_material_light:I = 0x7f0703bb

.field public static final hint_size:I = 0x7f0703bc

.field public static final homepage_app_bar_corner_radius:I = 0x7f0703bd

.field public static final homepage_app_bar_margin_bottom_two_pane:I = 0x7f0703be

.field public static final homepage_app_bar_margin_horizontal_two_pane:I = 0x7f0703bf

.field public static final homepage_app_bar_padding_two_pane:I = 0x7f0703c0

.field public static final homepage_padding_horizontal_two_pane:I = 0x7f0703c1

.field public static final homepage_preference_corner_radius:I = 0x7f0703c2

.field public static final homepage_preference_icon_padding_start:I = 0x7f0703c3

.field public static final homepage_preference_icon_padding_start_two_pane:I = 0x7f0703c4

.field public static final homepage_preference_min_height:I = 0x7f0703c5

.field public static final homepage_preference_text_padding_start:I = 0x7f0703c6

.field public static final homepage_preference_text_padding_start_two_pane:I = 0x7f0703c7

.field public static final homepage_title_margin_bottom:I = 0x7f0703c8

.field public static final homepage_title_margin_horizontal:I = 0x7f0703c9

.field public static final horizontal_divider_height:I = 0x7f0703ca

.field public static final ic_dpp_qr_code_size:I = 0x7f0703cb

.field public static final ic_head_margin_top:I = 0x7f0703cc

.field public static final ic_head_size:I = 0x7f0703cd

.field public static final ic_sharing_wifi_loading_size:I = 0x7f0703ce

.field public static final ic_time_height:I = 0x7f0703cf

.field public static final ic_time_width:I = 0x7f0703d0

.field public static final ic_union_height:I = 0x7f0703d1

.field public static final ic_union_width:I = 0x7f0703d2

.field public static final icon_size:I = 0x7f0703d3

.field public static final image_button_height:I = 0x7f0703d4

.field public static final image_button_width:I = 0x7f0703d5

.field public static final img_interesting_margin_end:I = 0x7f0703d6

.field public static final img_interesting_margin_start:I = 0x7f0703d7

.field public static final img_limit_margin_end:I = 0x7f0703d8

.field public static final img_limit_margin_start:I = 0x7f0703d9

.field public static final input_margin_start_end:I = 0x7f0703da

.field public static final input_method_clipboard_settings_bg_image_height:I = 0x7f0703db

.field public static final input_method_clipboard_settings_bg_image_margin_bottom:I = 0x7f0703dc

.field public static final input_method_clipboard_settings_bg_image_width:I = 0x7f0703dd

.field public static final input_method_clipboard_settings_image_margin_top:I = 0x7f0703de

.field public static final input_method_clipboard_settings_list_margin_end:I = 0x7f0703df

.field public static final input_method_clipboard_settings_tips_margin_bottom:I = 0x7f0703e0

.field public static final input_method_clipboard_settings_tips_margin_start:I = 0x7f0703e1

.field public static final input_method_clipboard_settings_tips_text_size:I = 0x7f0703e2

.field public static final input_method_manage_item_bg_radius:I = 0x7f0703e3

.field public static final input_method_manage_item_corner_radius:I = 0x7f0703e4

.field public static final input_method_manage_item_enable_text_margin_start_or_end:I = 0x7f0703e5

.field public static final input_method_manage_item_enable_text_margin_top_or_bottom:I = 0x7f0703e6

.field public static final input_method_manage_item_enable_text_size:I = 0x7f0703e7

.field public static final input_method_manage_item_icon_height_or_width:I = 0x7f0703e8

.field public static final input_method_manage_item_icon_margin_start:I = 0x7f0703e9

.field public static final input_method_manage_item_line_margin_top_or_bottom:I = 0x7f0703ea

.field public static final input_method_manage_item_margin_start_or_end:I = 0x7f0703eb

.field public static final input_method_manage_item_margin_top_or_bottom:I = 0x7f0703ec

.field public static final input_method_manage_item_min_height:I = 0x7f0703ed

.field public static final input_method_manage_item_summary_text_size:I = 0x7f0703ee

.field public static final input_method_manage_item_text_margin_start:I = 0x7f0703ef

.field public static final input_method_manage_item_title_text_size:I = 0x7f0703f0

.field public static final input_method_select_cloud_show_type_item_height:I = 0x7f0703f1

.field public static final input_method_select_function_des_text_size:I = 0x7f0703f2

.field public static final input_method_select_function_icon_size:I = 0x7f0703f3

.field public static final input_method_select_function_image_margin_start:I = 0x7f0703f4

.field public static final input_method_select_function_image_width:I = 0x7f0703f5

.field public static final input_method_select_function_item_height:I = 0x7f0703f6

.field public static final input_method_select_function_text_margin_start:I = 0x7f0703f7

.field public static final input_method_select_function_title_text_size:I = 0x7f0703f8

.field public static final input_method_select_picker_height:I = 0x7f0703f9

.field public static final input_method_select_picker_width:I = 0x7f0703fa

.field public static final installed_app_details_bullet_offset:I = 0x7f0703fb

.field public static final instruction_text_size:I = 0x7f0703fc

.field public static final instructions_text_size:I = 0x7f0703fd

.field public static final item_touch_helper_max_drag_scroll_per_frame:I = 0x7f0703fe

.field public static final item_touch_helper_swipe_escape_max_velocity:I = 0x7f0703ff

.field public static final item_touch_helper_swipe_escape_velocity:I = 0x7f070400

.field public static final iv_play_start_margin_start:I = 0x7f070401

.field public static final iv_play_start_margin_top:I = 0x7f070402

.field public static final iv_play_start_width_height:I = 0x7f070403

.field public static final iv_play_stop_margin_start:I = 0x7f070404

.field public static final iv_play_stop_margin_top:I = 0x7f070405

.field public static final iv_play_stop_width_height:I = 0x7f070406

.field public static final key_settings_click_back_marginLeft:I = 0x7f070407

.field public static final key_settings_click_bottom_height:I = 0x7f070408

.field public static final key_settings_click_bottom_marginTop:I = 0x7f070409

.field public static final key_settings_click_bottom_width:I = 0x7f07040a

.field public static final key_settings_click_home_marginLeft:I = 0x7f07040b

.field public static final key_settings_click_menu_marginLeft:I = 0x7f07040c

.field public static final key_settings_power_click_marginLeft:I = 0x7f07040d

.field public static final key_settings_power_click_marginTop:I = 0x7f07040e

.field public static final key_settings_power_double_click_height:I = 0x7f07040f

.field public static final key_settings_power_double_click_width:I = 0x7f070410

.field public static final key_settings_three_gesture_height:I = 0x7f070411

.field public static final key_settings_three_gesture_marginLeft:I = 0x7f070412

.field public static final key_settings_three_gesture_marginTop:I = 0x7f070413

.field public static final key_settings_three_gesture_width:I = 0x7f070414

.field public static final key_words_text_size:I = 0x7f070415

.field public static final keyboard_demo_width_and_height_ratio:I = 0x7f070416

.field public static final keyboard_demo_x_ratio:I = 0x7f070417

.field public static final keyboard_demo_y_ratio:I = 0x7f070418

.field public static final keyboard_key_text_sp:I = 0x7f070419

.field public static final keyguard_edittext_horizontal_margin:I = 0x7f07041a

.field public static final knock_text_padding_left:I = 0x7f07041b

.field public static final knock_text_padding_right:I = 0x7f07041c

.field public static final knock_title_text_size:I = 0x7f07041d

.field public static final large_screen_keyboard_icon_height:I = 0x7f07041e

.field public static final large_screen_keyboard_icon_width:I = 0x7f07041f

.field public static final le_bluetooth_battery_start_margin:I = 0x7f070420

.field public static final le_bluetooth_battery_top_margin:I = 0x7f070421

.field public static final le_bluetooth_summary_drawable_padding:I = 0x7f070422

.field public static final le_bluetooth_summary_min_width:I = 0x7f070423

.field public static final le_bluetooth_summary_padding:I = 0x7f070424

.field public static final le_bluetooth_summary_start_margin:I = 0x7f070425

.field public static final led_color_preview_height:I = 0x7f070426

.field public static final led_color_preview_width:I = 0x7f070427

.field public static final left_video_margin_end:I = 0x7f070428

.field public static final left_video_margin_start:I = 0x7f070429

.field public static final line_height_single:I = 0x7f07042a

.field public static final link_turbo_padding:I = 0x7f07042b

.field public static final link_turbo_title_padding:I = 0x7f07042c

.field public static final list_common_padding:I = 0x7f07042d

.field public static final list_item_height:I = 0x7f07042e

.field public static final list_preference_right_padding:I = 0x7f07042f

.field public static final list_preferred_item_padding:I = 0x7f070430

.field public static final locale_search_item_button_height:I = 0x7f070431

.field public static final locale_search_item_button_width:I = 0x7f070432

.field public static final locale_search_item_end_padding:I = 0x7f070433

.field public static final locale_search_item_start_padding:I = 0x7f070434

.field public static final location_icon_size:I = 0x7f070435

.field public static final lock_password_edit_text_bottom_margin:I = 0x7f070436

.field public static final lock_password_edit_text_height:I = 0x7f070437

.field public static final lock_password_edit_text_top_margin:I = 0x7f070438

.field public static final lock_password_edit_text_width:I = 0x7f070439

.field public static final lock_password_header_text_top_margin:I = 0x7f07043a

.field public static final lock_screen_numeric_keyboard_alphabet_text_size:I = 0x7f07043b

.field public static final lock_screen_numeric_keyboard_item_height:I = 0x7f07043c

.field public static final lock_screen_numeric_keyboard_number_text_size:I = 0x7f07043d

.field public static final lock_secure_after_timeout_text_top_padding:I = 0x7f07043e

.field public static final lock_view_size:I = 0x7f07043f

.field public static final lottie_view_height:I = 0x7f070440

.field public static final lottie_view_margin_top:I = 0x7f070441

.field public static final lottie_view_width:I = 0x7f070442

.field public static final m3_alert_dialog_action_bottom_padding:I = 0x7f070443

.field public static final m3_alert_dialog_action_top_padding:I = 0x7f070444

.field public static final m3_alert_dialog_corner_size:I = 0x7f070445

.field public static final m3_alert_dialog_elevation:I = 0x7f070446

.field public static final m3_alert_dialog_icon_margin:I = 0x7f070447

.field public static final m3_alert_dialog_icon_size:I = 0x7f070448

.field public static final m3_alert_dialog_title_bottom_margin:I = 0x7f070449

.field public static final m3_appbar_expanded_title_margin_bottom:I = 0x7f07044a

.field public static final m3_appbar_expanded_title_margin_horizontal:I = 0x7f07044b

.field public static final m3_appbar_scrim_height_trigger:I = 0x7f07044c

.field public static final m3_appbar_scrim_height_trigger_large:I = 0x7f07044d

.field public static final m3_appbar_scrim_height_trigger_medium:I = 0x7f07044e

.field public static final m3_appbar_size_compact:I = 0x7f07044f

.field public static final m3_appbar_size_large:I = 0x7f070450

.field public static final m3_appbar_size_medium:I = 0x7f070451

.field public static final m3_badge_horizontal_offset:I = 0x7f070452

.field public static final m3_badge_radius:I = 0x7f070453

.field public static final m3_badge_vertical_offset:I = 0x7f070454

.field public static final m3_badge_with_text_horizontal_offset:I = 0x7f070455

.field public static final m3_badge_with_text_radius:I = 0x7f070456

.field public static final m3_badge_with_text_vertical_offset:I = 0x7f070457

.field public static final m3_bottom_nav_item_active_indicator_height:I = 0x7f070458

.field public static final m3_bottom_nav_item_active_indicator_margin_horizontal:I = 0x7f070459

.field public static final m3_bottom_nav_item_active_indicator_width:I = 0x7f07045a

.field public static final m3_bottom_nav_item_padding_bottom:I = 0x7f07045b

.field public static final m3_bottom_nav_item_padding_top:I = 0x7f07045c

.field public static final m3_bottom_nav_min_height:I = 0x7f07045d

.field public static final m3_bottom_sheet_elevation:I = 0x7f07045e

.field public static final m3_bottom_sheet_modal_elevation:I = 0x7f07045f

.field public static final m3_bottomappbar_fab_cradle_margin:I = 0x7f070460

.field public static final m3_bottomappbar_fab_cradle_rounded_corner_radius:I = 0x7f070461

.field public static final m3_bottomappbar_fab_cradle_vertical_offset:I = 0x7f070462

.field public static final m3_btn_dialog_btn_min_width:I = 0x7f070463

.field public static final m3_btn_dialog_btn_spacing:I = 0x7f070464

.field public static final m3_btn_disabled_elevation:I = 0x7f070465

.field public static final m3_btn_disabled_translation_z:I = 0x7f070466

.field public static final m3_btn_elevated_btn_elevation:I = 0x7f070467

.field public static final m3_btn_elevation:I = 0x7f070468

.field public static final m3_btn_icon_btn_padding_left:I = 0x7f070469

.field public static final m3_btn_icon_btn_padding_right:I = 0x7f07046a

.field public static final m3_btn_icon_only_default_padding:I = 0x7f07046b

.field public static final m3_btn_icon_only_default_size:I = 0x7f07046c

.field public static final m3_btn_icon_only_icon_padding:I = 0x7f07046d

.field public static final m3_btn_icon_only_min_width:I = 0x7f07046e

.field public static final m3_btn_inset:I = 0x7f07046f

.field public static final m3_btn_max_width:I = 0x7f070470

.field public static final m3_btn_padding_bottom:I = 0x7f070471

.field public static final m3_btn_padding_left:I = 0x7f070472

.field public static final m3_btn_padding_right:I = 0x7f070473

.field public static final m3_btn_padding_top:I = 0x7f070474

.field public static final m3_btn_stroke_size:I = 0x7f070475

.field public static final m3_btn_text_btn_icon_padding_left:I = 0x7f070476

.field public static final m3_btn_text_btn_icon_padding_right:I = 0x7f070477

.field public static final m3_btn_text_btn_padding_left:I = 0x7f070478

.field public static final m3_btn_text_btn_padding_right:I = 0x7f070479

.field public static final m3_btn_translation_z_base:I = 0x7f07047a

.field public static final m3_btn_translation_z_hovered:I = 0x7f07047b

.field public static final m3_card_dragged_z:I = 0x7f07047c

.field public static final m3_card_elevated_dragged_z:I = 0x7f07047d

.field public static final m3_card_elevated_elevation:I = 0x7f07047e

.field public static final m3_card_elevated_hovered_z:I = 0x7f07047f

.field public static final m3_card_elevation:I = 0x7f070480

.field public static final m3_card_hovered_z:I = 0x7f070481

.field public static final m3_card_stroke_width:I = 0x7f070482

.field public static final m3_chip_checked_hovered_translation_z:I = 0x7f070483

.field public static final m3_chip_corner_size:I = 0x7f070484

.field public static final m3_chip_disabled_translation_z:I = 0x7f070485

.field public static final m3_chip_dragged_translation_z:I = 0x7f070486

.field public static final m3_chip_elevated_elevation:I = 0x7f070487

.field public static final m3_chip_hovered_translation_z:I = 0x7f070488

.field public static final m3_chip_icon_size:I = 0x7f070489

.field public static final m3_datepicker_elevation:I = 0x7f07048a

.field public static final m3_divider_heavy_thickness:I = 0x7f07048b

.field public static final m3_extended_fab_bottom_padding:I = 0x7f07048c

.field public static final m3_extended_fab_end_padding:I = 0x7f07048d

.field public static final m3_extended_fab_icon_padding:I = 0x7f07048e

.field public static final m3_extended_fab_min_height:I = 0x7f07048f

.field public static final m3_extended_fab_start_padding:I = 0x7f070490

.field public static final m3_extended_fab_top_padding:I = 0x7f070491

.field public static final m3_fab_border_width:I = 0x7f070492

.field public static final m3_fab_corner_size:I = 0x7f070493

.field public static final m3_fab_translation_z_hovered_focused:I = 0x7f070494

.field public static final m3_fab_translation_z_pressed:I = 0x7f070495

.field public static final m3_large_fab_max_image_size:I = 0x7f070496

.field public static final m3_large_fab_size:I = 0x7f070497

.field public static final m3_menu_elevation:I = 0x7f070498

.field public static final m3_navigation_drawer_layout_corner_size:I = 0x7f070499

.field public static final m3_navigation_item_horizontal_padding:I = 0x7f07049a

.field public static final m3_navigation_item_icon_padding:I = 0x7f07049b

.field public static final m3_navigation_item_shape_inset_bottom:I = 0x7f07049c

.field public static final m3_navigation_item_shape_inset_end:I = 0x7f07049d

.field public static final m3_navigation_item_shape_inset_start:I = 0x7f07049e

.field public static final m3_navigation_item_shape_inset_top:I = 0x7f07049f

.field public static final m3_navigation_item_vertical_padding:I = 0x7f0704a0

.field public static final m3_navigation_menu_divider_horizontal_padding:I = 0x7f0704a1

.field public static final m3_navigation_menu_headline_horizontal_padding:I = 0x7f0704a2

.field public static final m3_navigation_rail_default_width:I = 0x7f0704a3

.field public static final m3_navigation_rail_item_active_indicator_height:I = 0x7f0704a4

.field public static final m3_navigation_rail_item_active_indicator_margin_horizontal:I = 0x7f0704a5

.field public static final m3_navigation_rail_item_active_indicator_width:I = 0x7f0704a6

.field public static final m3_navigation_rail_item_min_height:I = 0x7f0704a7

.field public static final m3_navigation_rail_item_padding_bottom:I = 0x7f0704a8

.field public static final m3_navigation_rail_item_padding_top:I = 0x7f0704a9

.field public static final m3_ripple_default_alpha:I = 0x7f0704aa

.field public static final m3_ripple_focused_alpha:I = 0x7f0704ab

.field public static final m3_ripple_hovered_alpha:I = 0x7f0704ac

.field public static final m3_ripple_pressed_alpha:I = 0x7f0704ad

.field public static final m3_ripple_selectable_pressed_alpha:I = 0x7f0704ae

.field public static final m3_slider_thumb_elevation:I = 0x7f0704af

.field public static final m3_snackbar_action_text_color_alpha:I = 0x7f0704b0

.field public static final m3_snackbar_margin:I = 0x7f0704b1

.field public static final m3_sys_elevation_level0:I = 0x7f0704b2

.field public static final m3_sys_elevation_level1:I = 0x7f0704b3

.field public static final m3_sys_elevation_level2:I = 0x7f0704b4

.field public static final m3_sys_elevation_level3:I = 0x7f0704b5

.field public static final m3_sys_elevation_level4:I = 0x7f0704b6

.field public static final m3_sys_elevation_level5:I = 0x7f0704b7

.field public static final m3_sys_state_dragged_state_layer_opacity:I = 0x7f0704b8

.field public static final m3_sys_state_focus_state_layer_opacity:I = 0x7f0704b9

.field public static final m3_sys_state_hover_state_layer_opacity:I = 0x7f0704ba

.field public static final m3_sys_state_pressed_state_layer_opacity:I = 0x7f0704bb

.field public static final m3_timepicker_display_stroke_width:I = 0x7f0704bc

.field public static final m3_timepicker_window_elevation:I = 0x7f0704bd

.field public static final main_video_bg_radius:I = 0x7f0704be

.field public static final main_video_height:I = 0x7f0704bf

.field public static final main_video_margin_bottom:I = 0x7f0704c0

.field public static final main_video_margin_start_end:I = 0x7f0704c1

.field public static final main_video_margin_top:I = 0x7f0704c2

.field public static final mall_card_icon_arrow_right_height:I = 0x7f0704c3

.field public static final mall_card_icon_arrow_right_width:I = 0x7f0704c4

.field public static final mall_card_padding_end:I = 0x7f0704c5

.field public static final mall_card_padding_start:I = 0x7f0704c6

.field public static final mall_card_padding_top:I = 0x7f0704c7

.field public static final master_clear_warning_info_text_size:I = 0x7f0704c8

.field public static final match_parent:I = 0x7f0704c9

.field public static final material_bottom_sheet_max_width:I = 0x7f0704ca

.field public static final material_clock_display_padding:I = 0x7f0704cb

.field public static final material_clock_face_margin_top:I = 0x7f0704cc

.field public static final material_clock_hand_center_dot_radius:I = 0x7f0704cd

.field public static final material_clock_hand_padding:I = 0x7f0704ce

.field public static final material_clock_hand_stroke_width:I = 0x7f0704cf

.field public static final material_clock_number_text_padding:I = 0x7f0704d0

.field public static final material_clock_number_text_size:I = 0x7f0704d1

.field public static final material_clock_period_toggle_height:I = 0x7f0704d2

.field public static final material_clock_period_toggle_margin_left:I = 0x7f0704d3

.field public static final material_clock_period_toggle_width:I = 0x7f0704d4

.field public static final material_clock_size:I = 0x7f0704d5

.field public static final material_cursor_inset_bottom:I = 0x7f0704d6

.field public static final material_cursor_inset_top:I = 0x7f0704d7

.field public static final material_cursor_width:I = 0x7f0704d8

.field public static final material_divider_thickness:I = 0x7f0704d9

.field public static final material_emphasis_disabled:I = 0x7f0704da

.field public static final material_emphasis_disabled_background:I = 0x7f0704db

.field public static final material_emphasis_high_type:I = 0x7f0704dc

.field public static final material_emphasis_medium:I = 0x7f0704dd

.field public static final material_filled_edittext_font_1_3_padding_bottom:I = 0x7f0704de

.field public static final material_filled_edittext_font_1_3_padding_top:I = 0x7f0704df

.field public static final material_filled_edittext_font_2_0_padding_bottom:I = 0x7f0704e0

.field public static final material_filled_edittext_font_2_0_padding_top:I = 0x7f0704e1

.field public static final material_font_1_3_box_collapsed_padding_top:I = 0x7f0704e2

.field public static final material_font_2_0_box_collapsed_padding_top:I = 0x7f0704e3

.field public static final material_helper_text_default_padding_top:I = 0x7f0704e4

.field public static final material_helper_text_font_1_3_padding_horizontal:I = 0x7f0704e5

.field public static final material_helper_text_font_1_3_padding_top:I = 0x7f0704e6

.field public static final material_input_text_to_prefix_suffix_padding:I = 0x7f0704e7

.field public static final material_text_size_dp:I = 0x7f0704e8

.field public static final material_text_size_sp:I = 0x7f0704e9

.field public static final material_text_view_test_line_height:I = 0x7f0704ea

.field public static final material_text_view_test_line_height_override:I = 0x7f0704eb

.field public static final material_textinput_default_width:I = 0x7f0704ec

.field public static final material_textinput_max_width:I = 0x7f0704ed

.field public static final material_textinput_min_width:I = 0x7f0704ee

.field public static final material_time_picker_minimum_screen_height:I = 0x7f0704ef

.field public static final material_time_picker_minimum_screen_width:I = 0x7f0704f0

.field public static final material_timepicker_dialog_buttons_margin_top:I = 0x7f0704f1

.field public static final mdm_app_icon_width_height:I = 0x7f0704f2

.field public static final mdm_app_info_height:I = 0x7f0704f3

.field public static final mdm_app_info_padding_top_bottom:I = 0x7f0704f4

.field public static final mdm_app_name_padding_left:I = 0x7f0704f5

.field public static final media_checkbox_delimiter_height:I = 0x7f0704f6

.field public static final media_checkbox_delimiter_margin:I = 0x7f0704f7

.field public static final media_checkbox_delimiter_width:I = 0x7f0704f8

.field public static final media_checkbox_image_height:I = 0x7f0704f9

.field public static final media_checkbox_image_margin_start:I = 0x7f0704fa

.field public static final media_checkbox_image_width:I = 0x7f0704fb

.field public static final media_checkbox_margin_vertical:I = 0x7f0704fc

.field public static final media_checkbox_text_margin_start:I = 0x7f0704fd

.field public static final mediarouter_chooser_list_item_padding_bottom:I = 0x7f0704fe

.field public static final mediarouter_chooser_list_item_padding_end:I = 0x7f0704ff

.field public static final mediarouter_chooser_list_item_padding_start:I = 0x7f070500

.field public static final mediarouter_chooser_list_item_padding_top:I = 0x7f070501

.field public static final memory_card_height:I = 0x7f070502

.field public static final memory_card_margin_top:I = 0x7f070503

.field public static final memory_card_padding:I = 0x7f070504

.field public static final memory_icon_radius:I = 0x7f070505

.field public static final memory_icon_size:I = 0x7f070506

.field public static final menu_background_radius:I = 0x7f070507

.field public static final menu_elevation:I = 0x7f070508

.field public static final menu_icon_margin_end:I = 0x7f070509

.field public static final menu_item_height_double:I = 0x7f07050a

.field public static final menu_item_height_multi:I = 0x7f07050b

.field public static final menu_item_height_one:I = 0x7f07050c

.field public static final menu_item_padding_left:I = 0x7f07050d

.field public static final menu_item_padding_right:I = 0x7f07050e

.field public static final menu_item_width:I = 0x7f07050f

.field public static final menu_text_size:I = 0x7f070510

.field public static final message_bubble_left_right_padding:I = 0x7f070511

.field public static final message_icon_inset:I = 0x7f070512

.field public static final message_metadata_top_padding:I = 0x7f070513

.field public static final message_padding_default:I = 0x7f070514

.field public static final message_text_bottom_padding:I = 0x7f070515

.field public static final message_text_left_right_padding:I = 0x7f070516

.field public static final message_text_top_padding:I = 0x7f070517

.field public static final min_tap_target_size:I = 0x7f070518

.field public static final miui_common_unlock_screen_tip_shake_distance:I = 0x7f070519

.field public static final miui_face_detail_button_delete_bottom:I = 0x7f07051a

.field public static final miui_face_detail_button_horizontal:I = 0x7f07051b

.field public static final miui_face_detect_view_margin_end:I = 0x7f07051c

.field public static final miui_face_detect_view_margin_top:I = 0x7f07051d

.field public static final miui_face_edittext_height:I = 0x7f07051e

.field public static final miui_face_edittext_width:I = 0x7f07051f

.field public static final miui_face_enroll_back_image_height:I = 0x7f070520

.field public static final miui_face_enroll_back_image_start:I = 0x7f070521

.field public static final miui_face_enroll_back_image_width:I = 0x7f070522

.field public static final miui_face_enroll_back_linear_height:I = 0x7f070523

.field public static final miui_face_enroll_back_linear_width:I = 0x7f070524

.field public static final miui_face_enroll_button_horizontal:I = 0x7f070525

.field public static final miui_face_enroll_button_text_size:I = 0x7f070526

.field public static final miui_face_enroll_button_vertical_padding:I = 0x7f070527

.field public static final miui_face_enroll_camera_preview_extra_circles:I = 0x7f070528

.field public static final miui_face_enroll_camera_preview_height:I = 0x7f070529

.field public static final miui_face_enroll_camera_preview_iner_top:I = 0x7f07052a

.field public static final miui_face_enroll_camera_preview_top:I = 0x7f07052b

.field public static final miui_face_enroll_camera_preview_width:I = 0x7f07052c

.field public static final miui_face_enroll_circle_radius:I = 0x7f07052d

.field public static final miui_face_enroll_cover_offset:I = 0x7f07052e

.field public static final miui_face_enroll_detect_image_end:I = 0x7f07052f

.field public static final miui_face_enroll_detect_image_height:I = 0x7f070530

.field public static final miui_face_enroll_detect_image_repeta_end:I = 0x7f070531

.field public static final miui_face_enroll_detect_image_top:I = 0x7f070532

.field public static final miui_face_enroll_detect_image_width:I = 0x7f070533

.field public static final miui_face_enroll_detect_initX:I = 0x7f070534

.field public static final miui_face_enroll_detect_initY:I = 0x7f070535

.field public static final miui_face_enroll_detect_size:I = 0x7f070536

.field public static final miui_face_enroll_detect_width:I = 0x7f070537

.field public static final miui_face_enroll_first_suggestion_horizontal:I = 0x7f070538

.field public static final miui_face_enroll_first_suggestion_text_size:I = 0x7f070539

.field public static final miui_face_enroll_first_suggestion_top:I = 0x7f07053a

.field public static final miui_face_enroll_introduction_btn_bottom:I = 0x7f07053b

.field public static final miui_face_enroll_introduction_message_horizontal:I = 0x7f07053c

.field public static final miui_face_enroll_introduction_message_text_size:I = 0x7f07053d

.field public static final miui_face_enroll_introduction_message_top:I = 0x7f07053e

.field public static final miui_face_enroll_introduction_title_text_size:I = 0x7f07053f

.field public static final miui_face_enroll_introduction_title_top:I = 0x7f070540

.field public static final miui_face_enroll_introduction_video_top:I = 0x7f070541

.field public static final miui_face_enroll_line_distance:I = 0x7f070542

.field public static final miui_face_enroll_line_end_xposition:I = 0x7f070543

.field public static final miui_face_enroll_line_end_yposition:I = 0x7f070544

.field public static final miui_face_enroll_line_start_xposition:I = 0x7f070545

.field public static final miui_face_enroll_line_start_yposition:I = 0x7f070546

.field public static final miui_face_enroll_name_edittext_bottom:I = 0x7f070547

.field public static final miui_face_enroll_name_edittext_horizontal:I = 0x7f070548

.field public static final miui_face_enroll_name_edittext_padding:I = 0x7f070549

.field public static final miui_face_enroll_name_edittext_text_size:I = 0x7f07054a

.field public static final miui_face_enroll_name_edittext_top:I = 0x7f07054b

.field public static final miui_face_enroll_name_text_left:I = 0x7f07054c

.field public static final miui_face_enroll_name_text_left_offset:I = 0x7f07054d

.field public static final miui_face_enroll_name_text_size:I = 0x7f07054e

.field public static final miui_face_enroll_name_text_top:I = 0x7f07054f

.field public static final miui_face_enroll_next_button_bottom:I = 0x7f070550

.field public static final miui_face_enroll_next_button_horizontal:I = 0x7f070551

.field public static final miui_face_enroll_next_button_width:I = 0x7f070552

.field public static final miui_face_enroll_ok_button_bottom:I = 0x7f070553

.field public static final miui_face_enroll_progress_circle_left:I = 0x7f070554

.field public static final miui_face_enroll_progress_circle_top:I = 0x7f070555

.field public static final miui_face_enroll_progress_circle_width:I = 0x7f070556

.field public static final miui_face_enroll_prompt_video_top:I = 0x7f070557

.field public static final miui_face_enroll_prompt_video_top_landscape:I = 0x7f070558

.field public static final miui_face_enroll_risk_warning_icon_marginbottom:I = 0x7f070559

.field public static final miui_face_enroll_risk_warning_title_textsize:I = 0x7f07055a

.field public static final miui_face_enroll_second_suggestion_horizontal:I = 0x7f07055b

.field public static final miui_face_enroll_second_suggestion_text_size:I = 0x7f07055c

.field public static final miui_face_enroll_second_suggestion_top:I = 0x7f07055d

.field public static final miui_face_enroll_success_message_text_size:I = 0x7f07055e

.field public static final miui_face_enroll_success_message_top:I = 0x7f07055f

.field public static final miui_face_enroll_success_title_start:I = 0x7f070560

.field public static final miui_face_enroll_success_title_text_size:I = 0x7f070561

.field public static final miui_face_enroll_success_title_top:I = 0x7f070562

.field public static final miui_face_enroll_success_video_height:I = 0x7f070563

.field public static final miui_face_enroll_success_video_top:I = 0x7f070564

.field public static final miui_face_enroll_success_video_width:I = 0x7f070565

.field public static final miui_face_enroll_suggesiton_detect_image_end:I = 0x7f070566

.field public static final miui_face_enroll_suggesiton_detect_image_repeta_end:I = 0x7f070567

.field public static final miui_face_enroll_suggestion_video_height:I = 0x7f070568

.field public static final miui_face_enroll_suggestion_video_top:I = 0x7f070569

.field public static final miui_face_enroll_suggestion_video_width:I = 0x7f07056a

.field public static final miui_face_enroll_title_height:I = 0x7f07056b

.field public static final miui_face_enroll_title_text_maxtextsize:I = 0x7f07056c

.field public static final miui_face_enroll_title_text_mintextsize:I = 0x7f07056d

.field public static final miui_face_enroll_title_text_size:I = 0x7f07056e

.field public static final miui_face_enroll_title_top:I = 0x7f07056f

.field public static final miui_face_input_cameraview_height:I = 0x7f070570

.field public static final miui_face_input_cameraview_width:I = 0x7f070571

.field public static final miui_face_input_first_suggestion_height:I = 0x7f070572

.field public static final miui_face_input_first_suggestion_maxtextsize:I = 0x7f070573

.field public static final miui_face_input_first_suggestion_mintextsize:I = 0x7f070574

.field public static final miui_face_next_success_btn_height:I = 0x7f070575

.field public static final miui_face_prompt_video_height:I = 0x7f070576

.field public static final miui_face_prompt_video_width:I = 0x7f070577

.field public static final miui_face_rotate_margin_top:I = 0x7f070578

.field public static final miui_finger_enroll_back_image_start:I = 0x7f070579

.field public static final miui_finger_enroll_back_linear_height:I = 0x7f07057a

.field public static final miui_finger_enroll_back_linear_top:I = 0x7f07057b

.field public static final miui_finger_enroll_button_text_size:I = 0x7f07057c

.field public static final miui_finger_enroll_margin_top:I = 0x7f07057d

.field public static final miui_finger_enroll_margin_top_pad:I = 0x7f07057e

.field public static final miui_finger_enroll_name_edittext_padding:I = 0x7f07057f

.field public static final miui_finger_enroll_name_edittext_text_size:I = 0x7f070580

.field public static final miui_finger_enroll_name_edittext_top:I = 0x7f070581

.field public static final miui_finger_enroll_name_edittext_width:I = 0x7f070582

.field public static final miui_finger_enroll_name_text_left:I = 0x7f070583

.field public static final miui_finger_enroll_name_text_size:I = 0x7f070584

.field public static final miui_finger_enroll_name_text_top:I = 0x7f070585

.field public static final miui_finger_enroll_next_button_bottom:I = 0x7f070586

.field public static final miui_finger_enroll_next_button_horizontal:I = 0x7f070587

.field public static final miui_finger_enroll_next_button_margin_top:I = 0x7f070588

.field public static final miui_finger_enroll_suggest_bottom_pad:I = 0x7f070589

.field public static final miui_finger_enroll_suggest_title_bottom:I = 0x7f07058a

.field public static final miui_finger_enroll_title_message_size:I = 0x7f07058b

.field public static final miui_finger_enroll_title_message_top:I = 0x7f07058c

.field public static final miui_finger_enroll_title_text_size:I = 0x7f07058d

.field public static final miui_finger_enroll_title_top:I = 0x7f07058e

.field public static final miui_finger_enroll_video_height:I = 0x7f07058f

.field public static final miui_finger_face_enroll_name_edittext_height:I = 0x7f070590

.field public static final miui_lab_divider_height:I = 0x7f070591

.field public static final miui_lab_list_marginTop:I = 0x7f070592

.field public static final miui_lab_logo_marginTop:I = 0x7f070593

.field public static final miui_lab_logo_size:I = 0x7f070594

.field public static final miui_lab_marginBottom:I = 0x7f070595

.field public static final miui_lab_marginTop:I = 0x7f070596

.field public static final miui_lab_slogan_marginTop:I = 0x7f070597

.field public static final miui_lab_slogan_size:I = 0x7f070598

.field public static final miui_logo_view_height:I = 0x7f070599

.field public static final miui_logo_view_margintop:I = 0x7f07059a

.field public static final miui_logo_view_width:I = 0x7f07059b

.field public static final miui_nfc_preference_image_margin_start_and_end:I = 0x7f07059c

.field public static final miui_nfc_preference_text_margin:I = 0x7f07059d

.field public static final miui_nfc_preference_text_size:I = 0x7f07059e

.field public static final miui_pattern_lock_pattern_view_horizontal_margin:I = 0x7f07059f

.field public static final miui_restricted_summary_marginLeft:I = 0x7f0705a0

.field public static final miui_restricted_summary_marginTop:I = 0x7f0705a1

.field public static final miui_restricted_summary_size:I = 0x7f0705a2

.field public static final miui_restricted_summary_width:I = 0x7f0705a3

.field public static final miui_secure_keyboard_height:I = 0x7f0705a4

.field public static final miui_unlock_pattern_cell_view_size:I = 0x7f0705a5

.field public static final miui_version_margin_top:I = 0x7f0705a6

.field public static final miuix_appcompat_action_bar_default_height:I = 0x7f0705a7

.field public static final miuix_appcompat_action_bar_floating_height:I = 0x7f0705a8

.field public static final miuix_appcompat_action_bar_horizontal_padding_end:I = 0x7f0705a9

.field public static final miuix_appcompat_action_bar_horizontal_padding_start:I = 0x7f0705aa

.field public static final miuix_appcompat_action_bar_max_height:I = 0x7f0705ab

.field public static final miuix_appcompat_action_bar_stacked_tab_max_width:I = 0x7f0705ac

.field public static final miuix_appcompat_action_bar_subtitle_bg_divider_height:I = 0x7f0705ad

.field public static final miuix_appcompat_action_bar_subtitle_bg_divider_width:I = 0x7f0705ae

.field public static final miuix_appcompat_action_bar_subtitle_bg_end_margin:I = 0x7f0705af

.field public static final miuix_appcompat_action_bar_subtitle_bg_start_margin:I = 0x7f0705b0

.field public static final miuix_appcompat_action_bar_subtitle_bottom_padding:I = 0x7f0705b1

.field public static final miuix_appcompat_action_bar_subtitle_collapse_padding_vertical:I = 0x7f0705b2

.field public static final miuix_appcompat_action_bar_subtitle_start_margin:I = 0x7f0705b3

.field public static final miuix_appcompat_action_bar_tab_bg_height:I = 0x7f0705b4

.field public static final miuix_appcompat_action_bar_tab_bg_padding_horizontal:I = 0x7f0705b5

.field public static final miuix_appcompat_action_bar_tab_expand_margin:I = 0x7f0705b6

.field public static final miuix_appcompat_action_bar_tab_expand_text_size:I = 0x7f0705b7

.field public static final miuix_appcompat_action_bar_tab_expand_text_size_1:I = 0x7f0705b8

.field public static final miuix_appcompat_action_bar_tab_expand_text_size_2:I = 0x7f0705b9

.field public static final miuix_appcompat_action_bar_tab_secondary_collapse_text_size:I = 0x7f0705ba

.field public static final miuix_appcompat_action_bar_tab_secondary_expand_text_size:I = 0x7f0705bb

.field public static final miuix_appcompat_action_bar_tab_secondary_margin:I = 0x7f0705bc

.field public static final miuix_appcompat_action_bar_tab_text_size:I = 0x7f0705bd

.field public static final miuix_appcompat_action_bar_title_bottom_padding:I = 0x7f0705be

.field public static final miuix_appcompat_action_bar_title_collapse_padding_vertical:I = 0x7f0705bf

.field public static final miuix_appcompat_action_bar_title_expand_item_padding_end:I = 0x7f0705c0

.field public static final miuix_appcompat_action_bar_title_horizontal_padding:I = 0x7f0705c1

.field public static final miuix_appcompat_action_bar_title_tab_collapse_bottom_padding:I = 0x7f0705c2

.field public static final miuix_appcompat_action_bar_title_tab_collapse_top_padding:I = 0x7f0705c3

.field public static final miuix_appcompat_action_bar_title_tab_horizontal_padding:I = 0x7f0705c4

.field public static final miuix_appcompat_action_bar_title_top_padding:I = 0x7f0705c5

.field public static final miuix_appcompat_action_button_bg_bottom_padding:I = 0x7f0705c6

.field public static final miuix_appcompat_action_button_bg_top_padding:I = 0x7f0705c7

.field public static final miuix_appcompat_action_button_gap:I = 0x7f0705c8

.field public static final miuix_appcompat_action_button_gap_big_wide:I = 0x7f0705c9

.field public static final miuix_appcompat_action_button_gap_normal_wide:I = 0x7f0705ca

.field public static final miuix_appcompat_action_button_gap_small_wide:I = 0x7f0705cb

.field public static final miuix_appcompat_action_button_gap_tiny_wide:I = 0x7f0705cc

.field public static final miuix_appcompat_action_button_height:I = 0x7f0705cd

.field public static final miuix_appcompat_action_button_main_height:I = 0x7f0705ce

.field public static final miuix_appcompat_action_button_main_width:I = 0x7f0705cf

.field public static final miuix_appcompat_action_button_max_width:I = 0x7f0705d0

.field public static final miuix_appcompat_action_button_min_width:I = 0x7f0705d1

.field public static final miuix_appcompat_action_button_width:I = 0x7f0705d2

.field public static final miuix_appcompat_action_menu_item_view_padding_horizontal:I = 0x7f0705d3

.field public static final miuix_appcompat_action_mode_immersion_more_margin_right:I = 0x7f0705d4

.field public static final miuix_appcompat_action_mode_title_button_height:I = 0x7f0705d5

.field public static final miuix_appcompat_action_mode_title_button_size:I = 0x7f0705d6

.field public static final miuix_appcompat_action_mode_title_button_width:I = 0x7f0705d7

.field public static final miuix_appcompat_action_mode_title_margin_bottom:I = 0x7f0705d8

.field public static final miuix_appcompat_action_tab_badge_height:I = 0x7f0705d9

.field public static final miuix_appcompat_action_tab_badge_width:I = 0x7f0705da

.field public static final miuix_appcompat_actionbar_progressbar_horizontal_padding:I = 0x7f0705db

.field public static final miuix_appcompat_alert_dialog_title_min_height:I = 0x7f0705dc

.field public static final miuix_appcompat_alphabet_indexer_bg_corners:I = 0x7f0705dd

.field public static final miuix_appcompat_alphabet_indexer_item_height:I = 0x7f0705de

.field public static final miuix_appcompat_alphabet_indexer_item_margin:I = 0x7f0705df

.field public static final miuix_appcompat_alphabet_indexer_margin_end:I = 0x7f0705e0

.field public static final miuix_appcompat_alphabet_indexer_min_item_margin:I = 0x7f0705e1

.field public static final miuix_appcompat_alphabet_indexer_min_width:I = 0x7f0705e2

.field public static final miuix_appcompat_alphabet_indexer_omit_item_height:I = 0x7f0705e3

.field public static final miuix_appcompat_alphabet_indexer_overlay_offset:I = 0x7f0705e4

.field public static final miuix_appcompat_alphabet_indexer_overlay_padding_top:I = 0x7f0705e5

.field public static final miuix_appcompat_alphabet_indexer_overlay_text_size:I = 0x7f0705e6

.field public static final miuix_appcompat_alphabet_indexer_padding_vertical:I = 0x7f0705e7

.field public static final miuix_appcompat_alphabet_indexer_text_highlight_size:I = 0x7f0705e8

.field public static final miuix_appcompat_alphabet_indexer_text_size:I = 0x7f0705e9

.field public static final miuix_appcompat_alphabet_overlay_height:I = 0x7f0705ea

.field public static final miuix_appcompat_alphabet_overlay_width:I = 0x7f0705eb

.field public static final miuix_appcompat_arrow_popup_triangle_bottom_w:I = 0x7f0705ec

.field public static final miuix_appcompat_arrow_popup_triangle_corners:I = 0x7f0705ed

.field public static final miuix_appcompat_arrow_popup_triangle_h:I = 0x7f0705ee

.field public static final miuix_appcompat_arrow_popup_triangle_middle_h:I = 0x7f0705ef

.field public static final miuix_appcompat_arrow_popup_triangle_middle_w:I = 0x7f0705f0

.field public static final miuix_appcompat_arrow_popup_triangle_padding:I = 0x7f0705f1

.field public static final miuix_appcompat_arrow_popup_triangle_padding_adjust:I = 0x7f0705f2

.field public static final miuix_appcompat_arrow_popup_triangle_top_w:I = 0x7f0705f3

.field public static final miuix_appcompat_arrow_popup_view_min_height:I = 0x7f0705f4

.field public static final miuix_appcompat_arrow_popup_view_paddingBottom:I = 0x7f0705f5

.field public static final miuix_appcompat_arrow_popup_view_paddingEnd:I = 0x7f0705f6

.field public static final miuix_appcompat_arrow_popup_view_paddingStart:I = 0x7f0705f7

.field public static final miuix_appcompat_arrow_popup_view_paddingTop:I = 0x7f0705f8

.field public static final miuix_appcompat_arrow_popup_view_round_corners:I = 0x7f0705f9

.field public static final miuix_appcompat_arrow_popup_window_elevation:I = 0x7f0705fa

.field public static final miuix_appcompat_arrow_popup_window_list_max_height:I = 0x7f0705fb

.field public static final miuix_appcompat_arrow_popup_window_min_border:I = 0x7f0705fc

.field public static final miuix_appcompat_btn_inline_size:I = 0x7f0705fd

.field public static final miuix_appcompat_btn_radio_height:I = 0x7f0705fe

.field public static final miuix_appcompat_btn_radio_width:I = 0x7f0705ff

.field public static final miuix_appcompat_button_background_corner_radius:I = 0x7f070600

.field public static final miuix_appcompat_button_bg_corner_radius:I = 0x7f070601

.field public static final miuix_appcompat_button_content_safe_padding:I = 0x7f070602

.field public static final miuix_appcompat_button_height:I = 0x7f070603

.field public static final miuix_appcompat_button_right_widget_height:I = 0x7f070604

.field public static final miuix_appcompat_button_right_widget_width:I = 0x7f070605

.field public static final miuix_appcompat_button_text_size:I = 0x7f070606

.field public static final miuix_appcompat_check_widget_height:I = 0x7f070607

.field public static final miuix_appcompat_check_widget_width:I = 0x7f070608

.field public static final miuix_appcompat_checked_text_view_addition_margin:I = 0x7f070609

.field public static final miuix_appcompat_clearable_edit_text_size:I = 0x7f07060a

.field public static final miuix_appcompat_config_prefDialogWidth:I = 0x7f07060b

.field public static final miuix_appcompat_context_menu_separate_item_horizontal_padding:I = 0x7f07060c

.field public static final miuix_appcompat_context_menu_separate_item_margin_top:I = 0x7f07060d

.field public static final miuix_appcompat_context_menu_separate_item_padding_bottom:I = 0x7f07060e

.field public static final miuix_appcompat_context_menu_separate_item_padding_top:I = 0x7f07060f

.field public static final miuix_appcompat_context_menu_window_margin_screen:I = 0x7f070610

.field public static final miuix_appcompat_context_menu_window_margin_statusbar:I = 0x7f070611

.field public static final miuix_appcompat_date_picker_checkbox_padding_bottom:I = 0x7f070612

.field public static final miuix_appcompat_date_picker_checkbox_padding_top:I = 0x7f070613

.field public static final miuix_appcompat_date_picker_dialog_margin_horizontal:I = 0x7f070614

.field public static final miuix_appcompat_date_picker_dialog_padding_horizontal:I = 0x7f070615

.field public static final miuix_appcompat_date_picker_lunar_text_size:I = 0x7f070616

.field public static final miuix_appcompat_date_picker_text_size:I = 0x7f070617

.field public static final miuix_appcompat_dialog_bg_corner_radius:I = 0x7f070618

.field public static final miuix_appcompat_dialog_bg_corner_radius_bottom:I = 0x7f070619

.field public static final miuix_appcompat_dialog_border_padding:I = 0x7f07061a

.field public static final miuix_appcompat_dialog_bottom_margin:I = 0x7f07061b

.field public static final miuix_appcompat_dialog_btn_margin_horizontal:I = 0x7f07061c

.field public static final miuix_appcompat_dialog_btn_margin_vertical:I = 0x7f07061d

.field public static final miuix_appcompat_dialog_button_bar_bottom_padding:I = 0x7f07061e

.field public static final miuix_appcompat_dialog_button_horizontal_margin:I = 0x7f07061f

.field public static final miuix_appcompat_dialog_button_panel_horizontal_margin:I = 0x7f070620

.field public static final miuix_appcompat_dialog_button_vertical_margin:I = 0x7f070621

.field public static final miuix_appcompat_dialog_checkbox_horizontal_margin:I = 0x7f070622

.field public static final miuix_appcompat_dialog_checkbox_horizontal_padding:I = 0x7f070623

.field public static final miuix_appcompat_dialog_checkbox_inner_padding:I = 0x7f070624

.field public static final miuix_appcompat_dialog_checkbox_padding:I = 0x7f070625

.field public static final miuix_appcompat_dialog_checkbox_text_size:I = 0x7f070626

.field public static final miuix_appcompat_dialog_checkbox_vertical_margin:I = 0x7f070627

.field public static final miuix_appcompat_dialog_checkbox_vertical_padding:I = 0x7f070628

.field public static final miuix_appcompat_dialog_comment_text_size:I = 0x7f070629

.field public static final miuix_appcompat_dialog_content_margin_bottom:I = 0x7f07062a

.field public static final miuix_appcompat_dialog_edit_text_border_radius:I = 0x7f07062b

.field public static final miuix_appcompat_dialog_edit_text_height:I = 0x7f07062c

.field public static final miuix_appcompat_dialog_edit_text_horizontal_padding:I = 0x7f07062d

.field public static final miuix_appcompat_dialog_edit_text_radius:I = 0x7f07062e

.field public static final miuix_appcompat_dialog_edit_text_size:I = 0x7f07062f

.field public static final miuix_appcompat_dialog_fixed_height_major:I = 0x7f070630

.field public static final miuix_appcompat_dialog_fixed_height_major_small:I = 0x7f070631

.field public static final miuix_appcompat_dialog_fixed_height_minor:I = 0x7f070632

.field public static final miuix_appcompat_dialog_fixed_height_minor_small:I = 0x7f070633

.field public static final miuix_appcompat_dialog_fixed_width_major:I = 0x7f070634

.field public static final miuix_appcompat_dialog_fixed_width_major_small:I = 0x7f070635

.field public static final miuix_appcompat_dialog_fixed_width_minor:I = 0x7f070636

.field public static final miuix_appcompat_dialog_fixed_width_minor_small:I = 0x7f070637

.field public static final miuix_appcompat_dialog_icon_drawable_height:I = 0x7f070638

.field public static final miuix_appcompat_dialog_icon_drawable_margin:I = 0x7f070639

.field public static final miuix_appcompat_dialog_icon_drawable_width:I = 0x7f07063a

.field public static final miuix_appcompat_dialog_ime_margin:I = 0x7f07063b

.field public static final miuix_appcompat_dialog_list_item_padding_end:I = 0x7f07063c

.field public static final miuix_appcompat_dialog_list_item_padding_start:I = 0x7f07063d

.field public static final miuix_appcompat_dialog_list_item_padding_vertical:I = 0x7f07063e

.field public static final miuix_appcompat_dialog_list_padding_bottom_no_buttons:I = 0x7f07063f

.field public static final miuix_appcompat_dialog_list_padding_top_no_title:I = 0x7f070640

.field public static final miuix_appcompat_dialog_list_preferred_item_height:I = 0x7f070641

.field public static final miuix_appcompat_dialog_list_preferred_item_height_large:I = 0x7f070642

.field public static final miuix_appcompat_dialog_list_preferred_item_height_small:I = 0x7f070643

.field public static final miuix_appcompat_dialog_max_height_major:I = 0x7f070644

.field public static final miuix_appcompat_dialog_max_height_minor:I = 0x7f070645

.field public static final miuix_appcompat_dialog_max_width:I = 0x7f070646

.field public static final miuix_appcompat_dialog_max_width_land:I = 0x7f070647

.field public static final miuix_appcompat_dialog_max_width_major:I = 0x7f070648

.field public static final miuix_appcompat_dialog_max_width_minor:I = 0x7f070649

.field public static final miuix_appcompat_dialog_message_bottom_padding:I = 0x7f07064a

.field public static final miuix_appcompat_dialog_message_line_height:I = 0x7f07064b

.field public static final miuix_appcompat_dialog_message_text_size:I = 0x7f07064c

.field public static final miuix_appcompat_dialog_message_vertical_padding:I = 0x7f07064d

.field public static final miuix_appcompat_dialog_min_width_major:I = 0x7f07064e

.field public static final miuix_appcompat_dialog_min_width_minor:I = 0x7f07064f

.field public static final miuix_appcompat_dialog_panel_horizontal_padding:I = 0x7f070650

.field public static final miuix_appcompat_dialog_panel_vertical_padding:I = 0x7f070651

.field public static final miuix_appcompat_dialog_progress_horizontal_margin:I = 0x7f070652

.field public static final miuix_appcompat_dialog_progress_horizontal_width:I = 0x7f070653

.field public static final miuix_appcompat_dialog_progress_message_padding_vertical:I = 0x7f070654

.field public static final miuix_appcompat_dialog_progress_message_text_size:I = 0x7f070655

.field public static final miuix_appcompat_dialog_progress_title_vertical_padding_bottom:I = 0x7f070656

.field public static final miuix_appcompat_dialog_progress_title_vertical_padding_top:I = 0x7f070657

.field public static final miuix_appcompat_dialog_progress_vertical_margin:I = 0x7f070658

.field public static final miuix_appcompat_dialog_progressbar_size:I = 0x7f070659

.field public static final miuix_appcompat_dialog_title_bg_height:I = 0x7f07065a

.field public static final miuix_appcompat_dialog_title_horizontal_padding:I = 0x7f07065b

.field public static final miuix_appcompat_dialog_title_text_size:I = 0x7f07065c

.field public static final miuix_appcompat_dialog_title_vertical_margin_bottom:I = 0x7f07065d

.field public static final miuix_appcompat_dialog_title_vertical_padding:I = 0x7f07065e

.field public static final miuix_appcompat_dialog_width_margin:I = 0x7f07065f

.field public static final miuix_appcompat_drop_down_item_text_size:I = 0x7f070660

.field public static final miuix_appcompat_drop_down_menu_elevation:I = 0x7f070661

.field public static final miuix_appcompat_drop_down_menu_min_width:I = 0x7f070662

.field public static final miuix_appcompat_drop_down_menu_padding_large:I = 0x7f070663

.field public static final miuix_appcompat_drop_down_menu_padding_single_item:I = 0x7f070664

.field public static final miuix_appcompat_drop_down_menu_padding_small:I = 0x7f070665

.field public static final miuix_appcompat_drop_down_menu_radius:I = 0x7f070666

.field public static final miuix_appcompat_dropdown_item_padding_horizontal:I = 0x7f070667

.field public static final miuix_appcompat_edit_text_border_radius_size:I = 0x7f070668

.field public static final miuix_appcompat_edit_text_border_size:I = 0x7f070669

.field public static final miuix_appcompat_edit_text_font_size:I = 0x7f07066a

.field public static final miuix_appcompat_edit_text_padding_horizontal:I = 0x7f07066b

.field public static final miuix_appcompat_edit_text_padding_vertical:I = 0x7f07066c

.field public static final miuix_appcompat_edit_text_radius_size:I = 0x7f07066d

.field public static final miuix_appcompat_edit_text_search_bg_height:I = 0x7f07066e

.field public static final miuix_appcompat_edit_text_search_bg_icon_padding_end:I = 0x7f07066f

.field public static final miuix_appcompat_edit_text_search_bg_icon_padding_start:I = 0x7f070670

.field public static final miuix_appcompat_edit_text_search_bg_padding_start:I = 0x7f070671

.field public static final miuix_appcompat_edit_text_search_bg_radius:I = 0x7f070672

.field public static final miuix_appcompat_edit_text_search_clear_btn_height:I = 0x7f070673

.field public static final miuix_appcompat_edit_text_search_clear_btn_width:I = 0x7f070674

.field public static final miuix_appcompat_edit_text_search_icon_height:I = 0x7f070675

.field public static final miuix_appcompat_edit_text_search_icon_width:I = 0x7f070676

.field public static final miuix_appcompat_expand_subtitle_text_size:I = 0x7f070677

.field public static final miuix_appcompat_expand_title_text_size:I = 0x7f070678

.field public static final miuix_appcompat_filter_sort_arrow_view_height:I = 0x7f070679

.field public static final miuix_appcompat_filter_sort_arrow_view_width:I = 0x7f07067a

.field public static final miuix_appcompat_filter_sort_tab_view_corner:I = 0x7f07067b

.field public static final miuix_appcompat_filter_sort_tab_view_height:I = 0x7f07067c

.field public static final miuix_appcompat_filter_sort_tab_view_image_margin:I = 0x7f07067d

.field public static final miuix_appcompat_filter_sort_tab_view_text_size:I = 0x7f07067e

.field public static final miuix_appcompat_filter_sort_view_corner:I = 0x7f07067f

.field public static final miuix_appcompat_filter_sort_view_padding:I = 0x7f070680

.field public static final miuix_appcompat_floating_action_button_main_margin_bottom_no_navigationbar:I = 0x7f070681

.field public static final miuix_appcompat_floating_action_button_main_margin_bottom_with_navigationbar:I = 0x7f070682

.field public static final miuix_appcompat_floating_action_button_main_margin_left:I = 0x7f070683

.field public static final miuix_appcompat_floating_action_button_main_margin_right:I = 0x7f070684

.field public static final miuix_appcompat_floating_action_button_main_margin_top:I = 0x7f070685

.field public static final miuix_appcompat_floating_window_background_border_width:I = 0x7f070686

.field public static final miuix_appcompat_floating_window_background_radius:I = 0x7f070687

.field public static final miuix_appcompat_floating_window_background_radius_0dp:I = 0x7f070688

.field public static final miuix_appcompat_floating_window_drag_handle_height:I = 0x7f070689

.field public static final miuix_appcompat_floating_window_fixed_height_major:I = 0x7f07068a

.field public static final miuix_appcompat_floating_window_fixed_height_major_0dp:I = 0x7f07068b

.field public static final miuix_appcompat_floating_window_fixed_height_minor:I = 0x7f07068c

.field public static final miuix_appcompat_floating_window_fixed_width_major:I = 0x7f07068d

.field public static final miuix_appcompat_floating_window_fixed_width_major_0dp:I = 0x7f07068e

.field public static final miuix_appcompat_floating_window_fixed_width_minor:I = 0x7f07068f

.field public static final miuix_appcompat_floating_window_round_corner_radius:I = 0x7f070690

.field public static final miuix_appcompat_floating_window_top_offset:I = 0x7f070691

.field public static final miuix_appcompat_guide_popup_horizontal_padding:I = 0x7f070692

.field public static final miuix_appcompat_guide_popup_line_length:I = 0x7f070693

.field public static final miuix_appcompat_guide_popup_start_point_radius:I = 0x7f070694

.field public static final miuix_appcompat_guide_popup_text_radius:I = 0x7f070695

.field public static final miuix_appcompat_guide_popup_text_size:I = 0x7f070696

.field public static final miuix_appcompat_guide_popup_vertical_padding:I = 0x7f070697

.field public static final miuix_appcompat_guide_popup_window_min_height:I = 0x7f070698

.field public static final miuix_appcompat_guide_popup_window_min_width:I = 0x7f070699

.field public static final miuix_appcompat_guide_popup_window_padding:I = 0x7f07069a

.field public static final miuix_appcompat_ic_clear_height:I = 0x7f07069b

.field public static final miuix_appcompat_ic_clear_width:I = 0x7f07069c

.field public static final miuix_appcompat_ic_visible_height:I = 0x7f07069d

.field public static final miuix_appcompat_ic_visible_width:I = 0x7f07069e

.field public static final miuix_appcompat_immersion_menu_background_radius:I = 0x7f07069f

.field public static final miuix_appcompat_immersion_menu_icon_margin_end:I = 0x7f0706a0

.field public static final miuix_appcompat_large_text_size:I = 0x7f0706a1

.field public static final miuix_appcompat_list_item_dialog_padding_horizontal:I = 0x7f0706a2

.field public static final miuix_appcompat_list_item_group_header_height:I = 0x7f0706a3

.field public static final miuix_appcompat_list_menu_bg_header_height:I = 0x7f0706a4

.field public static final miuix_appcompat_list_menu_bg_header_radius:I = 0x7f0706a5

.field public static final miuix_appcompat_list_menu_dialog_maximum_height:I = 0x7f0706a6

.field public static final miuix_appcompat_list_menu_dialog_maximum_width:I = 0x7f0706a7

.field public static final miuix_appcompat_list_menu_dialog_minimum_width:I = 0x7f0706a8

.field public static final miuix_appcompat_list_menu_item_padding_large:I = 0x7f0706a9

.field public static final miuix_appcompat_list_menu_item_padding_small:I = 0x7f0706aa

.field public static final miuix_appcompat_list_preferred_item_height:I = 0x7f0706ab

.field public static final miuix_appcompat_list_preferred_item_height_large:I = 0x7f0706ac

.field public static final miuix_appcompat_list_preferred_item_height_small:I = 0x7f0706ad

.field public static final miuix_appcompat_list_preferred_item_padding_left:I = 0x7f0706ae

.field public static final miuix_appcompat_list_preferred_item_padding_right:I = 0x7f0706af

.field public static final miuix_appcompat_list_preferred_item_width_small:I = 0x7f0706b0

.field public static final miuix_appcompat_menu_popup_max_height:I = 0x7f0706b1

.field public static final miuix_appcompat_message_view_bg_height:I = 0x7f0706b2

.field public static final miuix_appcompat_message_view_bg_radius:I = 0x7f0706b3

.field public static final miuix_appcompat_message_view_icon_height:I = 0x7f0706b4

.field public static final miuix_appcompat_message_view_icon_width:I = 0x7f0706b5

.field public static final miuix_appcompat_message_view_margin_end:I = 0x7f0706b6

.field public static final miuix_appcompat_message_view_margin_start:I = 0x7f0706b7

.field public static final miuix_appcompat_message_view_padding_end:I = 0x7f0706b8

.field public static final miuix_appcompat_message_view_padding_start:I = 0x7f0706b9

.field public static final miuix_appcompat_message_view_padding_vertical:I = 0x7f0706ba

.field public static final miuix_appcompat_message_view_text_margin_right:I = 0x7f0706bb

.field public static final miuix_appcompat_message_view_text_padding_start:I = 0x7f0706bc

.field public static final miuix_appcompat_message_view_text_size:I = 0x7f0706bd

.field public static final miuix_appcompat_normal_text_size:I = 0x7f0706be

.field public static final miuix_appcompat_number_picker_bg_corners:I = 0x7f0706bf

.field public static final miuix_appcompat_number_picker_bg_height:I = 0x7f0706c0

.field public static final miuix_appcompat_number_picker_bg_width:I = 0x7f0706c1

.field public static final miuix_appcompat_number_picker_big_bg_width:I = 0x7f0706c2

.field public static final miuix_appcompat_number_picker_highlight_region_height:I = 0x7f0706c3

.field public static final miuix_appcompat_number_picker_label_margin_left:I = 0x7f0706c4

.field public static final miuix_appcompat_number_picker_label_margin_top:I = 0x7f0706c5

.field public static final miuix_appcompat_number_picker_label_padding:I = 0x7f0706c6

.field public static final miuix_appcompat_number_picker_label_text_size:I = 0x7f0706c7

.field public static final miuix_appcompat_number_picker_text_size_highlight_large:I = 0x7f0706c8

.field public static final miuix_appcompat_number_picker_text_size_highlight_medium:I = 0x7f0706c9

.field public static final miuix_appcompat_number_picker_text_size_highlight_normal:I = 0x7f0706ca

.field public static final miuix_appcompat_number_picker_text_size_hint_large:I = 0x7f0706cb

.field public static final miuix_appcompat_number_picker_text_size_hint_medium:I = 0x7f0706cc

.field public static final miuix_appcompat_number_picker_text_size_hint_normal:I = 0x7f0706cd

.field public static final miuix_appcompat_overflow_popup_menu_item_min_width:I = 0x7f0706ce

.field public static final miuix_appcompat_overflow_popup_menu_item_padding_horizontal:I = 0x7f0706cf

.field public static final miuix_appcompat_picker_horizontal_padding:I = 0x7f0706d0

.field public static final miuix_appcompat_picker_vertical_padding_bottom:I = 0x7f0706d1

.field public static final miuix_appcompat_picker_vertical_padding_top:I = 0x7f0706d2

.field public static final miuix_appcompat_popup_menu_item_min_width:I = 0x7f0706d3

.field public static final miuix_appcompat_popup_menu_item_padding_horizontal:I = 0x7f0706d4

.field public static final miuix_appcompat_progress_bar_height:I = 0x7f0706d5

.field public static final miuix_appcompat_progress_bar_line_height:I = 0x7f0706d6

.field public static final miuix_appcompat_progress_bar_line_radius:I = 0x7f0706d7

.field public static final miuix_appcompat_progress_indeterminate_size:I = 0x7f0706d8

.field public static final miuix_appcompat_progressbar_large_icon_text_margin:I = 0x7f0706d9

.field public static final miuix_appcompat_progressbar_large_text_bottom_padding:I = 0x7f0706da

.field public static final miuix_appcompat_progressbar_large_text_size:I = 0x7f0706db

.field public static final miuix_appcompat_progressbar_large_text_top_padding:I = 0x7f0706dc

.field public static final miuix_appcompat_progressbar_size:I = 0x7f0706dd

.field public static final miuix_appcompat_progressbar_size_small:I = 0x7f0706de

.field public static final miuix_appcompat_progressbar_small_icon_text_margin:I = 0x7f0706df

.field public static final miuix_appcompat_progressbar_small_text_bottom_padding:I = 0x7f0706e0

.field public static final miuix_appcompat_progressbar_small_text_size:I = 0x7f0706e1

.field public static final miuix_appcompat_progressbar_small_text_top_padding:I = 0x7f0706e2

.field public static final miuix_appcompat_radio_button_drawable_padding:I = 0x7f0706e3

.field public static final miuix_appcompat_radio_button_height:I = 0x7f0706e4

.field public static final miuix_appcompat_radio_button_width:I = 0x7f0706e5

.field public static final miuix_appcompat_ratingbar_icon_size_large:I = 0x7f0706e6

.field public static final miuix_appcompat_ratingbar_icon_size_small:I = 0x7f0706e7

.field public static final miuix_appcompat_ratingbar_size:I = 0x7f0706e8

.field public static final miuix_appcompat_ratingbar_size_indicator:I = 0x7f0706e9

.field public static final miuix_appcompat_ratingbar_size_large:I = 0x7f0706ea

.field public static final miuix_appcompat_ratingbar_size_small:I = 0x7f0706eb

.field public static final miuix_appcompat_round_corner_radius:I = 0x7f0706ec

.field public static final miuix_appcompat_scrollbar_thumb_vertical_padding:I = 0x7f0706ed

.field public static final miuix_appcompat_scrollbar_thumb_vertical_radius:I = 0x7f0706ee

.field public static final miuix_appcompat_scrollbar_thumb_vertical_width:I = 0x7f0706ef

.field public static final miuix_appcompat_search_action_mode_cancel_text_padding_end:I = 0x7f0706f0

.field public static final miuix_appcompat_search_action_mode_cancel_text_padding_start:I = 0x7f0706f1

.field public static final miuix_appcompat_search_action_mode_cancel_text_size:I = 0x7f0706f2

.field public static final miuix_appcompat_search_action_mode_cancel_width:I = 0x7f0706f3

.field public static final miuix_appcompat_search_edit_text_size:I = 0x7f0706f4

.field public static final miuix_appcompat_search_mode_bg_padding:I = 0x7f0706f5

.field public static final miuix_appcompat_search_mode_bg_padding_bottom:I = 0x7f0706f6

.field public static final miuix_appcompat_search_mode_bg_padding_top:I = 0x7f0706f7

.field public static final miuix_appcompat_search_mode_bg_radius:I = 0x7f0706f8

.field public static final miuix_appcompat_search_mode_bg_size:I = 0x7f0706f9

.field public static final miuix_appcompat_search_view_default_height:I = 0x7f0706fa

.field public static final miuix_appcompat_searchbar_bg_height:I = 0x7f0706fb

.field public static final miuix_appcompat_secondary_text_size:I = 0x7f0706fc

.field public static final miuix_appcompat_seekbar_height:I = 0x7f0706fd

.field public static final miuix_appcompat_seekbar_icon_size:I = 0x7f0706fe

.field public static final miuix_appcompat_seekbar_progress_bg_radius:I = 0x7f0706ff

.field public static final miuix_appcompat_select_dialog_item_padding_horizontal:I = 0x7f070700

.field public static final miuix_appcompat_sliding_button_frame_corner_radius:I = 0x7f070701

.field public static final miuix_appcompat_sliding_button_frame_horizontal_padding:I = 0x7f070702

.field public static final miuix_appcompat_sliding_button_frame_vertical_padding:I = 0x7f070703

.field public static final miuix_appcompat_sliding_button_height:I = 0x7f070704

.field public static final miuix_appcompat_sliding_button_mask_horizontal_padding:I = 0x7f070705

.field public static final miuix_appcompat_sliding_button_mask_vertical_padding:I = 0x7f070706

.field public static final miuix_appcompat_sliding_button_slider_horizontal_padding:I = 0x7f070707

.field public static final miuix_appcompat_sliding_button_slider_max_offset:I = 0x7f070708

.field public static final miuix_appcompat_sliding_button_slider_size:I = 0x7f070709

.field public static final miuix_appcompat_sliding_button_slider_vertical_padding:I = 0x7f07070a

.field public static final miuix_appcompat_sliding_button_width:I = 0x7f07070b

.field public static final miuix_appcompat_small_text_size:I = 0x7f07070c

.field public static final miuix_appcompat_spinner_bg_padding:I = 0x7f07070d

.field public static final miuix_appcompat_spinner_double_line_summary_size:I = 0x7f07070e

.field public static final miuix_appcompat_spinner_double_line_title_size:I = 0x7f07070f

.field public static final miuix_appcompat_spinner_dropdown_item_horizontal_padding:I = 0x7f070710

.field public static final miuix_appcompat_spinner_dropdown_item_icon_margin:I = 0x7f070711

.field public static final miuix_appcompat_spinner_dropdown_item_padding_end:I = 0x7f070712

.field public static final miuix_appcompat_spinner_dropdown_item_padding_start:I = 0x7f070713

.field public static final miuix_appcompat_spinner_dropdown_item_text_margin:I = 0x7f070714

.field public static final miuix_appcompat_spinner_dropdown_maximum_width:I = 0x7f070715

.field public static final miuix_appcompat_spinner_dropdown_minimum_width:I = 0x7f070716

.field public static final miuix_appcompat_spinner_dropdown_selector_padding:I = 0x7f070717

.field public static final miuix_appcompat_spinner_dropdown_selector_padding_bottom:I = 0x7f070718

.field public static final miuix_appcompat_spinner_dropdown_selector_padding_top:I = 0x7f070719

.field public static final miuix_appcompat_spinner_icon_padding:I = 0x7f07071a

.field public static final miuix_appcompat_spinner_icon_padding_integrated:I = 0x7f07071b

.field public static final miuix_appcompat_spinner_magin_screen_horizontal:I = 0x7f07071c

.field public static final miuix_appcompat_spinner_magin_screen_vertical:I = 0x7f07071d

.field public static final miuix_appcompat_spinner_max_height:I = 0x7f07071e

.field public static final miuix_appcompat_spinner_max_width:I = 0x7f07071f

.field public static final miuix_appcompat_spinner_popup_item_bg_padding_vertical:I = 0x7f070720

.field public static final miuix_appcompat_spinner_right_widget_height:I = 0x7f070721

.field public static final miuix_appcompat_spinner_right_widget_width:I = 0x7f070722

.field public static final miuix_appcompat_spinner_round_corner_radius:I = 0x7f070723

.field public static final miuix_appcompat_spinner_text_max_width_integrated:I = 0x7f070724

.field public static final miuix_appcompat_spinner_text_size:I = 0x7f070725

.field public static final miuix_appcompat_spinner_text_size_integrated:I = 0x7f070726

.field public static final miuix_appcompat_split_action_bar_default_height:I = 0x7f070727

.field public static final miuix_appcompat_state_edit_widget_padding:I = 0x7f070728

.field public static final miuix_appcompat_subtitle_text_size:I = 0x7f070729

.field public static final miuix_appcompat_title_text_size:I = 0x7f07072a

.field public static final miuix_appcompat_triangle_arrow_size:I = 0x7f07072b

.field public static final miuix_appcompat_two_state_extra_padding_horizontal_huge:I = 0x7f07072c

.field public static final miuix_appcompat_two_state_extra_padding_horizontal_large:I = 0x7f07072d

.field public static final miuix_appcompat_two_state_extra_padding_horizontal_small:I = 0x7f07072e

.field public static final miuix_appcompat_window_dialog_radius:I = 0x7f07072f

.field public static final miuix_appcompat_window_extra_padding_horizontal_huge:I = 0x7f070730

.field public static final miuix_appcompat_window_extra_padding_horizontal_large:I = 0x7f070731

.field public static final miuix_appcompat_window_extra_padding_horizontal_small:I = 0x7f070732

.field public static final miuix_divider_height:I = 0x7f070733

.field public static final miuix_floating_preference_checkable_item_bg_padding_end:I = 0x7f070734

.field public static final miuix_floating_preference_checkable_item_mask_padding_end:I = 0x7f070735

.field public static final miuix_floating_preference_checkable_item_mask_padding_start:I = 0x7f070736

.field public static final miuix_floating_preference_item_padding_end:I = 0x7f070737

.field public static final miuix_floating_preference_item_padding_start:I = 0x7f070738

.field public static final miuix_floating_preference_item_radio_two_state_bg_padding_end:I = 0x7f070739

.field public static final miuix_floating_preference_item_radio_two_state_bg_padding_start:I = 0x7f07073a

.field public static final miuix_floating_preference_radio_set_item_padding_start:I = 0x7f07073b

.field public static final miuix_floating_preference_radio_set_radio_item_padding_start:I = 0x7f07073c

.field public static final miuix_floating_preference_seekbar_icon_margin_start:I = 0x7f07073d

.field public static final miuix_floating_preference_seekbar_padding_end:I = 0x7f07073e

.field public static final miuix_floating_preference_seekbar_padding_start:I = 0x7f07073f

.field public static final miuix_label_text_size_small:I = 0x7f070740

.field public static final miuix_nested_header_layout_content_min_height:I = 0x7f070741

.field public static final miuix_picker_datetime_bottom_margin:I = 0x7f070742

.field public static final miuix_picker_datetime_end_margin:I = 0x7f070743

.field public static final miuix_picker_datetime_start_padding:I = 0x7f070744

.field public static final miuix_picker_datetime_text_size:I = 0x7f070745

.field public static final miuix_picker_datetime_top_margin:I = 0x7f070746

.field public static final miuix_picker_icon_end_margin:I = 0x7f070747

.field public static final miuix_picker_lunar_start_margin:I = 0x7f070748

.field public static final miuix_picker_lunar_text_size:I = 0x7f070749

.field public static final miuix_picker_start_text_size:I = 0x7f07074a

.field public static final miuix_picker_start_text_start_margin:I = 0x7f07074b

.field public static final miuix_picker_start_text_top_margin:I = 0x7f07074c

.field public static final miuix_picker_state_icon_start_margin:I = 0x7f07074d

.field public static final miuix_picker_state_icon_top_margin:I = 0x7f07074e

.field public static final miuix_picker_switch_vertical_margin:I = 0x7f07074f

.field public static final miuix_picker_view_start_margin:I = 0x7f070750

.field public static final miuix_popup_dropdown_item_min_width:I = 0x7f070751

.field public static final miuix_popup_guide_text_view_max_width:I = 0x7f070752

.field public static final miuix_preference_btn_radio_height:I = 0x7f070753

.field public static final miuix_preference_btn_radio_width:I = 0x7f070754

.field public static final miuix_preference_category_divider_gap_height:I = 0x7f070755

.field public static final miuix_preference_category_divider_height:I = 0x7f070756

.field public static final miuix_preference_category_gap_height:I = 0x7f070757

.field public static final miuix_preference_category_height:I = 0x7f070758

.field public static final miuix_preference_category_no_title_height:I = 0x7f070759

.field public static final miuix_preference_category_text_size:I = 0x7f07075a

.field public static final miuix_preference_category_title_height:I = 0x7f07075b

.field public static final miuix_preference_category_vertical_padding:I = 0x7f07075c

.field public static final miuix_preference_checkable_item_bg_padding_end:I = 0x7f07075d

.field public static final miuix_preference_checkable_item_mask_padding_bottom:I = 0x7f07075e

.field public static final miuix_preference_checkable_item_mask_padding_end:I = 0x7f07075f

.field public static final miuix_preference_checkable_item_mask_padding_start:I = 0x7f070760

.field public static final miuix_preference_checkable_item_mask_padding_top:I = 0x7f070761

.field public static final miuix_preference_checkable_item_mask_radius:I = 0x7f070762

.field public static final miuix_preference_connect_detail_height:I = 0x7f070763

.field public static final miuix_preference_connect_detail_padding_horizontal:I = 0x7f070764

.field public static final miuix_preference_connect_detail_width:I = 0x7f070765

.field public static final miuix_preference_connect_translation_height:I = 0x7f070766

.field public static final miuix_preference_connect_translation_width:I = 0x7f070767

.field public static final miuix_preference_dialog_edittext_margin:I = 0x7f070768

.field public static final miuix_preference_dialog_edittext_margin_horizontal:I = 0x7f070769

.field public static final miuix_preference_dialog_edittext_padding_top:I = 0x7f07076a

.field public static final miuix_preference_high_light_radius:I = 0x7f07076b

.field public static final miuix_preference_horizontal_extra_padding:I = 0x7f07076c

.field public static final miuix_preference_icon_arrow_right_height:I = 0x7f07076d

.field public static final miuix_preference_icon_arrow_right_width:I = 0x7f07076e

.field public static final miuix_preference_icon_max_height:I = 0x7f07076f

.field public static final miuix_preference_icon_min_width:I = 0x7f070770

.field public static final miuix_preference_icon_padding_end:I = 0x7f070771

.field public static final miuix_preference_item_icon_margin_end:I = 0x7f070772

.field public static final miuix_preference_item_min_height:I = 0x7f070773

.field public static final miuix_preference_item_padding_bottom:I = 0x7f070774

.field public static final miuix_preference_item_padding_end:I = 0x7f070775

.field public static final miuix_preference_item_padding_inner:I = 0x7f070776

.field public static final miuix_preference_item_padding_side_zero:I = 0x7f070777

.field public static final miuix_preference_item_padding_start:I = 0x7f070778

.field public static final miuix_preference_item_padding_top:I = 0x7f070779

.field public static final miuix_preference_item_radio_padding_bottom:I = 0x7f07077a

.field public static final miuix_preference_item_radio_padding_end:I = 0x7f07077b

.field public static final miuix_preference_item_radio_padding_start:I = 0x7f07077c

.field public static final miuix_preference_item_radio_padding_top:I = 0x7f07077d

.field public static final miuix_preference_item_radio_two_state_bg_padding_end:I = 0x7f07077e

.field public static final miuix_preference_item_radio_two_state_bg_padding_start:I = 0x7f07077f

.field public static final miuix_preference_item_seekbar_icon_height:I = 0x7f070780

.field public static final miuix_preference_item_seekbar_icon_margin_end:I = 0x7f070781

.field public static final miuix_preference_item_seekbar_icon_margin_start:I = 0x7f070782

.field public static final miuix_preference_item_seekbar_icon_width:I = 0x7f070783

.field public static final miuix_preference_item_seekbar_padding_bottom:I = 0x7f070784

.field public static final miuix_preference_item_seekbar_padding_top:I = 0x7f070785

.field public static final miuix_preference_item_seekbar_parent_padding_bottom:I = 0x7f070786

.field public static final miuix_preference_item_seekbar_parent_padding_top:I = 0x7f070787

.field public static final miuix_preference_main_icon_min_width:I = 0x7f070788

.field public static final miuix_preference_main_icon_padding_end:I = 0x7f070789

.field public static final miuix_preference_main_margin_end:I = 0x7f07078a

.field public static final miuix_preference_main_margin_start:I = 0x7f07078b

.field public static final miuix_preference_navigation_item_content_padding_end:I = 0x7f07078c

.field public static final miuix_preference_navigation_item_content_padding_start:I = 0x7f07078d

.field public static final miuix_preference_navigation_item_mask_padding_horizontal:I = 0x7f07078e

.field public static final miuix_preference_navigation_item_mask_padding_vertical:I = 0x7f07078f

.field public static final miuix_preference_navigation_item_min_height:I = 0x7f070790

.field public static final miuix_preference_navigation_item_padding_vertical:I = 0x7f070791

.field public static final miuix_preference_navigation_text_max_width:I = 0x7f070792

.field public static final miuix_preference_normal_text_size:I = 0x7f070793

.field public static final miuix_preference_radio_item_min_height:I = 0x7f070794

.field public static final miuix_preference_radio_set_item_padding_start:I = 0x7f070795

.field public static final miuix_preference_radio_set_radio_item_padding_start:I = 0x7f070796

.field public static final miuix_preference_right_text_size:I = 0x7f070797

.field public static final miuix_preference_rv_bottom_padding:I = 0x7f070798

.field public static final miuix_preference_rv_top_padding:I = 0x7f070799

.field public static final miuix_preference_secondary_text_size:I = 0x7f07079a

.field public static final miuix_preference_seekbar_padding_end:I = 0x7f07079b

.field public static final miuix_preference_seekbar_padding_start:I = 0x7f07079c

.field public static final miuix_preference_summary_margin_top:I = 0x7f07079d

.field public static final miuix_preference_text_max_width:I = 0x7f07079e

.field public static final miuix_preference_text_padding_bottom:I = 0x7f07079f

.field public static final miuix_preference_text_padding_top:I = 0x7f0707a0

.field public static final miuix_preference_widget_width:I = 0x7f0707a1

.field public static final miuix_sbl_action_indeterminate_distance:I = 0x7f0707a2

.field public static final miuix_sbl_action_simple_enter:I = 0x7f0707a3

.field public static final miuix_sbl_action_simple_trigger:I = 0x7f0707a4

.field public static final miuix_sbl_action_upindeterminate_distance:I = 0x7f0707a5

.field public static final miuix_sbl_indicator_locked_body_height:I = 0x7f0707a6

.field public static final miuix_sbl_indicator_locked_body_margintop:I = 0x7f0707a7

.field public static final miuix_sbl_indicator_locked_body_width:I = 0x7f0707a8

.field public static final miuix_sbl_indicator_locked_header_height:I = 0x7f0707a9

.field public static final miuix_sbl_indicator_locked_header_width:I = 0x7f0707aa

.field public static final miuix_sbl_indicator_locked_labe_margintop:I = 0x7f0707ab

.field public static final miuix_sbl_indicator_locked_labe_textsize:I = 0x7f0707ac

.field public static final miuix_sbl_indicator_locked_layout_padding_bottom:I = 0x7f0707ad

.field public static final miuix_sbl_tracking_progress_bg_height:I = 0x7f0707ae

.field public static final miuix_sbl_tracking_progress_bg_marginbottom:I = 0x7f0707af

.field public static final miuix_sbl_tracking_progress_bg_marginleft:I = 0x7f0707b0

.field public static final miuix_sbl_tracking_progress_bg_marginright:I = 0x7f0707b1

.field public static final miuix_sbl_tracking_progress_bg_margintop:I = 0x7f0707b2

.field public static final miuix_sbl_tracking_progress_bg_radius_width:I = 0x7f0707b3

.field public static final miuix_sbl_tracking_progress_bg_stroke_width:I = 0x7f0707b4

.field public static final miuix_sbl_tracking_progress_bg_width:I = 0x7f0707b5

.field public static final miuix_sbl_tracking_progress_labe_marginbottom:I = 0x7f0707b6

.field public static final miuix_sbl_tracking_progress_labe_margintop:I = 0x7f0707b7

.field public static final miuix_sbl_tracking_progress_labe_textsize:I = 0x7f0707b8

.field public static final miuix_sbl_tracking_uo_progress_labe_margintleft:I = 0x7f0707b9

.field public static final miuix_sbl_tracking_up_progress_bg_marginbottom:I = 0x7f0707ba

.field public static final miuix_sbl_tracking_up_progress_bg_margintop:I = 0x7f0707bb

.field public static final miuix_stretchable_state_icon_heigth:I = 0x7f0707bc

.field public static final miuix_stretchable_state_icon_width:I = 0x7f0707bd

.field public static final miuix_stretchable_view_heigth:I = 0x7f0707be

.field public static final miuix_text_size_small:I = 0x7f0707bf

.field public static final miuix_waterbox_round_corner_radius:I = 0x7f0707c0

.field public static final mixed_password_emergency_call_text_size:I = 0x7f0707c1

.field public static final mixed_password_unlock_screen_edittext_top_margin:I = 0x7f0707c2

.field public static final mixed_password_unlock_screen_top_blank:I = 0x7f0707c3

.field public static final more_settings_icon_size:I = 0x7f0707c4

.field public static final more_settings_icon_top_margin:I = 0x7f0707c5

.field public static final mr_cast_group_volume_seekbar_height:I = 0x7f0707c6

.field public static final mr_cast_meta_art_size:I = 0x7f0707c7

.field public static final mr_cast_meta_subtitle_text_size:I = 0x7f0707c8

.field public static final mr_cast_route_volume_seekbar_height:I = 0x7f0707c9

.field public static final mr_cast_seekbar_thumb_size:I = 0x7f0707ca

.field public static final mr_controller_volume_group_list_item_height:I = 0x7f0707cb

.field public static final mr_controller_volume_group_list_item_icon_size:I = 0x7f0707cc

.field public static final mr_controller_volume_group_list_max_height:I = 0x7f0707cd

.field public static final mr_controller_volume_group_list_padding_top:I = 0x7f0707ce

.field public static final mr_dialog_fixed_width_major:I = 0x7f0707cf

.field public static final mr_dialog_fixed_width_minor:I = 0x7f0707d0

.field public static final mr_dynamic_dialog_header_text_size:I = 0x7f0707d1

.field public static final mr_dynamic_dialog_route_text_size:I = 0x7f0707d2

.field public static final mr_dynamic_dialog_row_height:I = 0x7f0707d3

.field public static final mr_dynamic_volume_group_list_item_height:I = 0x7f0707d4

.field public static final msd_vista_magnifier_margin_left:I = 0x7f0707d5

.field public static final msd_vista_magnifier_margin_top:I = 0x7f0707d6

.field public static final msd_vista_model_margin_left:I = 0x7f0707d7

.field public static final msd_vista_model_margin_top:I = 0x7f0707d8

.field public static final msd_xp_magnifier_margin_left:I = 0x7f0707d9

.field public static final msd_xp_magnifier_margin_top:I = 0x7f0707da

.field public static final msd_xp_model_margin_left:I = 0x7f0707db

.field public static final msd_xp_model_margin_top:I = 0x7f0707dc

.field public static final mtrl_alert_dialog_background_inset_bottom:I = 0x7f0707dd

.field public static final mtrl_alert_dialog_background_inset_end:I = 0x7f0707de

.field public static final mtrl_alert_dialog_background_inset_start:I = 0x7f0707df

.field public static final mtrl_alert_dialog_background_inset_top:I = 0x7f0707e0

.field public static final mtrl_alert_dialog_picker_background_inset:I = 0x7f0707e1

.field public static final mtrl_badge_horizontal_edge_offset:I = 0x7f0707e2

.field public static final mtrl_badge_long_text_horizontal_padding:I = 0x7f0707e3

.field public static final mtrl_badge_radius:I = 0x7f0707e4

.field public static final mtrl_badge_text_horizontal_edge_offset:I = 0x7f0707e5

.field public static final mtrl_badge_text_size:I = 0x7f0707e6

.field public static final mtrl_badge_toolbar_action_menu_item_horizontal_offset:I = 0x7f0707e7

.field public static final mtrl_badge_toolbar_action_menu_item_vertical_offset:I = 0x7f0707e8

.field public static final mtrl_badge_with_text_radius:I = 0x7f0707e9

.field public static final mtrl_bottomappbar_fabOffsetEndMode:I = 0x7f0707ea

.field public static final mtrl_bottomappbar_fab_bottom_margin:I = 0x7f0707eb

.field public static final mtrl_bottomappbar_fab_cradle_margin:I = 0x7f0707ec

.field public static final mtrl_bottomappbar_fab_cradle_rounded_corner_radius:I = 0x7f0707ed

.field public static final mtrl_bottomappbar_fab_cradle_vertical_offset:I = 0x7f0707ee

.field public static final mtrl_bottomappbar_height:I = 0x7f0707ef

.field public static final mtrl_btn_corner_radius:I = 0x7f0707f0

.field public static final mtrl_btn_dialog_btn_min_width:I = 0x7f0707f1

.field public static final mtrl_btn_disabled_elevation:I = 0x7f0707f2

.field public static final mtrl_btn_disabled_z:I = 0x7f0707f3

.field public static final mtrl_btn_elevation:I = 0x7f0707f4

.field public static final mtrl_btn_focused_z:I = 0x7f0707f5

.field public static final mtrl_btn_hovered_z:I = 0x7f0707f6

.field public static final mtrl_btn_icon_btn_padding_left:I = 0x7f0707f7

.field public static final mtrl_btn_icon_padding:I = 0x7f0707f8

.field public static final mtrl_btn_inset:I = 0x7f0707f9

.field public static final mtrl_btn_letter_spacing:I = 0x7f0707fa

.field public static final mtrl_btn_max_width:I = 0x7f0707fb

.field public static final mtrl_btn_padding_bottom:I = 0x7f0707fc

.field public static final mtrl_btn_padding_left:I = 0x7f0707fd

.field public static final mtrl_btn_padding_right:I = 0x7f0707fe

.field public static final mtrl_btn_padding_top:I = 0x7f0707ff

.field public static final mtrl_btn_pressed_z:I = 0x7f070800

.field public static final mtrl_btn_snackbar_margin_horizontal:I = 0x7f070801

.field public static final mtrl_btn_stroke_size:I = 0x7f070802

.field public static final mtrl_btn_text_btn_icon_padding:I = 0x7f070803

.field public static final mtrl_btn_text_btn_padding_left:I = 0x7f070804

.field public static final mtrl_btn_text_btn_padding_right:I = 0x7f070805

.field public static final mtrl_btn_text_size:I = 0x7f070806

.field public static final mtrl_btn_z:I = 0x7f070807

.field public static final mtrl_calendar_action_confirm_button_min_width:I = 0x7f070808

.field public static final mtrl_calendar_action_height:I = 0x7f070809

.field public static final mtrl_calendar_action_padding:I = 0x7f07080a

.field public static final mtrl_calendar_bottom_padding:I = 0x7f07080b

.field public static final mtrl_calendar_content_padding:I = 0x7f07080c

.field public static final mtrl_calendar_day_corner:I = 0x7f07080d

.field public static final mtrl_calendar_day_height:I = 0x7f07080e

.field public static final mtrl_calendar_day_horizontal_padding:I = 0x7f07080f

.field public static final mtrl_calendar_day_today_stroke:I = 0x7f070810

.field public static final mtrl_calendar_day_vertical_padding:I = 0x7f070811

.field public static final mtrl_calendar_day_width:I = 0x7f070812

.field public static final mtrl_calendar_days_of_week_height:I = 0x7f070813

.field public static final mtrl_calendar_dialog_background_inset:I = 0x7f070814

.field public static final mtrl_calendar_header_content_padding:I = 0x7f070815

.field public static final mtrl_calendar_header_content_padding_fullscreen:I = 0x7f070816

.field public static final mtrl_calendar_header_divider_thickness:I = 0x7f070817

.field public static final mtrl_calendar_header_height:I = 0x7f070818

.field public static final mtrl_calendar_header_height_fullscreen:I = 0x7f070819

.field public static final mtrl_calendar_header_selection_line_height:I = 0x7f07081a

.field public static final mtrl_calendar_header_text_padding:I = 0x7f07081b

.field public static final mtrl_calendar_header_toggle_margin_bottom:I = 0x7f07081c

.field public static final mtrl_calendar_header_toggle_margin_top:I = 0x7f07081d

.field public static final mtrl_calendar_landscape_header_width:I = 0x7f07081e

.field public static final mtrl_calendar_maximum_default_fullscreen_minor_axis:I = 0x7f07081f

.field public static final mtrl_calendar_month_horizontal_padding:I = 0x7f070820

.field public static final mtrl_calendar_month_vertical_padding:I = 0x7f070821

.field public static final mtrl_calendar_navigation_bottom_padding:I = 0x7f070822

.field public static final mtrl_calendar_navigation_height:I = 0x7f070823

.field public static final mtrl_calendar_navigation_top_padding:I = 0x7f070824

.field public static final mtrl_calendar_pre_l_text_clip_padding:I = 0x7f070825

.field public static final mtrl_calendar_selection_baseline_to_top_fullscreen:I = 0x7f070826

.field public static final mtrl_calendar_selection_text_baseline_to_bottom:I = 0x7f070827

.field public static final mtrl_calendar_selection_text_baseline_to_bottom_fullscreen:I = 0x7f070828

.field public static final mtrl_calendar_selection_text_baseline_to_top:I = 0x7f070829

.field public static final mtrl_calendar_text_input_padding_top:I = 0x7f07082a

.field public static final mtrl_calendar_title_baseline_to_top:I = 0x7f07082b

.field public static final mtrl_calendar_title_baseline_to_top_fullscreen:I = 0x7f07082c

.field public static final mtrl_calendar_year_corner:I = 0x7f07082d

.field public static final mtrl_calendar_year_height:I = 0x7f07082e

.field public static final mtrl_calendar_year_horizontal_padding:I = 0x7f07082f

.field public static final mtrl_calendar_year_vertical_padding:I = 0x7f070830

.field public static final mtrl_calendar_year_width:I = 0x7f070831

.field public static final mtrl_card_checked_icon_margin:I = 0x7f070832

.field public static final mtrl_card_checked_icon_size:I = 0x7f070833

.field public static final mtrl_card_corner_radius:I = 0x7f070834

.field public static final mtrl_card_dragged_z:I = 0x7f070835

.field public static final mtrl_card_elevation:I = 0x7f070836

.field public static final mtrl_card_spacing:I = 0x7f070837

.field public static final mtrl_chip_pressed_translation_z:I = 0x7f070838

.field public static final mtrl_chip_text_size:I = 0x7f070839

.field public static final mtrl_edittext_rectangle_top_offset:I = 0x7f07083a

.field public static final mtrl_exposed_dropdown_menu_popup_elevation:I = 0x7f07083b

.field public static final mtrl_exposed_dropdown_menu_popup_vertical_offset:I = 0x7f07083c

.field public static final mtrl_exposed_dropdown_menu_popup_vertical_padding:I = 0x7f07083d

.field public static final mtrl_extended_fab_bottom_padding:I = 0x7f07083e

.field public static final mtrl_extended_fab_corner_radius:I = 0x7f07083f

.field public static final mtrl_extended_fab_disabled_elevation:I = 0x7f070840

.field public static final mtrl_extended_fab_disabled_translation_z:I = 0x7f070841

.field public static final mtrl_extended_fab_elevation:I = 0x7f070842

.field public static final mtrl_extended_fab_end_padding:I = 0x7f070843

.field public static final mtrl_extended_fab_end_padding_icon:I = 0x7f070844

.field public static final mtrl_extended_fab_icon_size:I = 0x7f070845

.field public static final mtrl_extended_fab_icon_text_spacing:I = 0x7f070846

.field public static final mtrl_extended_fab_min_height:I = 0x7f070847

.field public static final mtrl_extended_fab_min_width:I = 0x7f070848

.field public static final mtrl_extended_fab_start_padding:I = 0x7f070849

.field public static final mtrl_extended_fab_start_padding_icon:I = 0x7f07084a

.field public static final mtrl_extended_fab_top_padding:I = 0x7f07084b

.field public static final mtrl_extended_fab_translation_z_base:I = 0x7f07084c

.field public static final mtrl_extended_fab_translation_z_hovered_focused:I = 0x7f07084d

.field public static final mtrl_extended_fab_translation_z_pressed:I = 0x7f07084e

.field public static final mtrl_fab_elevation:I = 0x7f07084f

.field public static final mtrl_fab_min_touch_target:I = 0x7f070850

.field public static final mtrl_fab_translation_z_hovered_focused:I = 0x7f070851

.field public static final mtrl_fab_translation_z_pressed:I = 0x7f070852

.field public static final mtrl_high_ripple_default_alpha:I = 0x7f070853

.field public static final mtrl_high_ripple_focused_alpha:I = 0x7f070854

.field public static final mtrl_high_ripple_hovered_alpha:I = 0x7f070855

.field public static final mtrl_high_ripple_pressed_alpha:I = 0x7f070856

.field public static final mtrl_large_touch_target:I = 0x7f070857

.field public static final mtrl_low_ripple_default_alpha:I = 0x7f070858

.field public static final mtrl_low_ripple_focused_alpha:I = 0x7f070859

.field public static final mtrl_low_ripple_hovered_alpha:I = 0x7f07085a

.field public static final mtrl_low_ripple_pressed_alpha:I = 0x7f07085b

.field public static final mtrl_min_touch_target_size:I = 0x7f07085c

.field public static final mtrl_navigation_bar_item_default_icon_size:I = 0x7f07085d

.field public static final mtrl_navigation_bar_item_default_margin:I = 0x7f07085e

.field public static final mtrl_navigation_elevation:I = 0x7f07085f

.field public static final mtrl_navigation_item_horizontal_padding:I = 0x7f070860

.field public static final mtrl_navigation_item_icon_padding:I = 0x7f070861

.field public static final mtrl_navigation_item_icon_size:I = 0x7f070862

.field public static final mtrl_navigation_item_shape_horizontal_margin:I = 0x7f070863

.field public static final mtrl_navigation_item_shape_vertical_margin:I = 0x7f070864

.field public static final mtrl_navigation_rail_active_text_size:I = 0x7f070865

.field public static final mtrl_navigation_rail_compact_width:I = 0x7f070866

.field public static final mtrl_navigation_rail_default_width:I = 0x7f070867

.field public static final mtrl_navigation_rail_elevation:I = 0x7f070868

.field public static final mtrl_navigation_rail_icon_margin:I = 0x7f070869

.field public static final mtrl_navigation_rail_icon_size:I = 0x7f07086a

.field public static final mtrl_navigation_rail_margin:I = 0x7f07086b

.field public static final mtrl_navigation_rail_text_bottom_margin:I = 0x7f07086c

.field public static final mtrl_navigation_rail_text_size:I = 0x7f07086d

.field public static final mtrl_progress_circular_inset:I = 0x7f07086e

.field public static final mtrl_progress_circular_inset_extra_small:I = 0x7f07086f

.field public static final mtrl_progress_circular_inset_medium:I = 0x7f070870

.field public static final mtrl_progress_circular_inset_small:I = 0x7f070871

.field public static final mtrl_progress_circular_radius:I = 0x7f070872

.field public static final mtrl_progress_circular_size:I = 0x7f070873

.field public static final mtrl_progress_circular_size_extra_small:I = 0x7f070874

.field public static final mtrl_progress_circular_size_medium:I = 0x7f070875

.field public static final mtrl_progress_circular_size_small:I = 0x7f070876

.field public static final mtrl_progress_circular_track_thickness_extra_small:I = 0x7f070877

.field public static final mtrl_progress_circular_track_thickness_medium:I = 0x7f070878

.field public static final mtrl_progress_circular_track_thickness_small:I = 0x7f070879

.field public static final mtrl_progress_indicator_full_rounded_corner_radius:I = 0x7f07087a

.field public static final mtrl_progress_track_thickness:I = 0x7f07087b

.field public static final mtrl_shape_corner_size_large_component:I = 0x7f07087c

.field public static final mtrl_shape_corner_size_medium_component:I = 0x7f07087d

.field public static final mtrl_shape_corner_size_small_component:I = 0x7f07087e

.field public static final mtrl_slider_halo_radius:I = 0x7f07087f

.field public static final mtrl_slider_label_padding:I = 0x7f070880

.field public static final mtrl_slider_label_radius:I = 0x7f070881

.field public static final mtrl_slider_label_square_side:I = 0x7f070882

.field public static final mtrl_slider_thumb_elevation:I = 0x7f070883

.field public static final mtrl_slider_thumb_radius:I = 0x7f070884

.field public static final mtrl_slider_track_height:I = 0x7f070885

.field public static final mtrl_slider_track_side_padding:I = 0x7f070886

.field public static final mtrl_slider_track_top:I = 0x7f070887

.field public static final mtrl_slider_widget_height:I = 0x7f070888

.field public static final mtrl_snackbar_action_text_color_alpha:I = 0x7f070889

.field public static final mtrl_snackbar_background_corner_radius:I = 0x7f07088a

.field public static final mtrl_snackbar_background_overlay_color_alpha:I = 0x7f07088b

.field public static final mtrl_snackbar_margin:I = 0x7f07088c

.field public static final mtrl_snackbar_message_margin_horizontal:I = 0x7f07088d

.field public static final mtrl_snackbar_padding_horizontal:I = 0x7f07088e

.field public static final mtrl_switch_thumb_elevation:I = 0x7f07088f

.field public static final mtrl_textinput_box_corner_radius_medium:I = 0x7f070890

.field public static final mtrl_textinput_box_corner_radius_small:I = 0x7f070891

.field public static final mtrl_textinput_box_label_cutout_padding:I = 0x7f070892

.field public static final mtrl_textinput_box_stroke_width_default:I = 0x7f070893

.field public static final mtrl_textinput_box_stroke_width_focused:I = 0x7f070894

.field public static final mtrl_textinput_counter_margin_start:I = 0x7f070895

.field public static final mtrl_textinput_end_icon_margin_start:I = 0x7f070896

.field public static final mtrl_textinput_outline_box_expanded_padding:I = 0x7f070897

.field public static final mtrl_textinput_start_icon_margin_end:I = 0x7f070898

.field public static final mtrl_toolbar_default_height:I = 0x7f070899

.field public static final mtrl_tooltip_arrowSize:I = 0x7f07089a

.field public static final mtrl_tooltip_cornerSize:I = 0x7f07089b

.field public static final mtrl_tooltip_minHeight:I = 0x7f07089c

.field public static final mtrl_tooltip_minWidth:I = 0x7f07089d

.field public static final mtrl_tooltip_padding:I = 0x7f07089e

.field public static final mtrl_transition_shared_axis_slide_distance:I = 0x7f07089f

.field public static final multiple_users_avatar_size:I = 0x7f0708a0

.field public static final my_device_card_margin_actionbar_thirteen:I = 0x7f0708a1

.field public static final my_device_card_margin_edge:I = 0x7f0708a2

.field public static final my_device_card_padding_start:I = 0x7f0708a3

.field public static final my_device_item_margin_edge:I = 0x7f0708a4

.field public static final navigation_gesture_line_height:I = 0x7f0708a5

.field public static final navigation_gesture_line_margin_bottom:I = 0x7f0708a6

.field public static final navigation_gesture_line_radius:I = 0x7f0708a7

.field public static final navigation_gesture_line_width:I = 0x7f0708a8

.field public static final navigation_type_margin_bottom:I = 0x7f0708a9

.field public static final navigation_type_margin_top:I = 0x7f0708aa

.field public static final navigation_type_radio_button_margin_top:I = 0x7f0708ab

.field public static final navigation_type_radio_button_text_size:I = 0x7f0708ac

.field public static final navigation_type_radio_margin_top:I = 0x7f0708ad

.field public static final navigation_type_radio_text_margin_start:I = 0x7f0708ae

.field public static final navigation_type_title_margin_start:I = 0x7f0708af

.field public static final navigation_type_video_height:I = 0x7f0708b0

.field public static final navigation_type_video_margin_start:I = 0x7f0708b1

.field public static final navigation_type_video_width:I = 0x7f0708b2

.field public static final new_fingerprint_image_top:I = 0x7f0708b3

.field public static final new_fingerprint_image_top_pad:I = 0x7f0708b4

.field public static final new_fingerprint_instruction_img_top:I = 0x7f0708b5

.field public static final new_fingerprint_step_video_height:I = 0x7f0708b6

.field public static final new_fingerprint_step_video_width:I = 0x7f0708b7

.field public static final new_fingerprint_top_text_height:I = 0x7f0708b8

.field public static final new_fingerprint_top_text_maxtextsize:I = 0x7f0708b9

.field public static final new_fingerprint_top_text_mintextsize:I = 0x7f0708ba

.field public static final new_fingerprint_top_title_height:I = 0x7f0708bb

.field public static final new_fingerprint_top_title_horizontal_margin:I = 0x7f0708bc

.field public static final new_fingerprint_top_title_maxtextsize:I = 0x7f0708bd

.field public static final new_fingerprint_top_title_mintextsize:I = 0x7f0708be

.field public static final new_versin_button_paddingLeft:I = 0x7f0708bf

.field public static final new_versin_button_paddingTop:I = 0x7f0708c0

.field public static final new_version_button_corner:I = 0x7f0708c1

.field public static final new_version_text_size:I = 0x7f0708c2

.field public static final nfc_detection_point_height:I = 0x7f0708c3

.field public static final no_action_height:I = 0x7f0708c4

.field public static final no_action_radius:I = 0x7f0708c5

.field public static final normal_list_radius:I = 0x7f0708c6

.field public static final notch_style_image_margin_bottom:I = 0x7f0708c7

.field public static final notch_style_margin_top:I = 0x7f0708c8

.field public static final notch_style_radio_text_size:I = 0x7f0708c9

.field public static final notification_action_icon_size:I = 0x7f0708ca

.field public static final notification_action_text_size:I = 0x7f0708cb

.field public static final notification_app_icon_badge_margin:I = 0x7f0708cc

.field public static final notification_app_icon_badge_size:I = 0x7f0708cd

.field public static final notification_app_icon_size:I = 0x7f0708ce

.field public static final notification_big_circle_margin:I = 0x7f0708cf

.field public static final notification_card_bottom_padding:I = 0x7f0708d0

.field public static final notification_card_horizontal_margin:I = 0x7f0708d1

.field public static final notification_card_horizontal_padding:I = 0x7f0708d2

.field public static final notification_card_icon_height:I = 0x7f0708d3

.field public static final notification_card_icon_width:I = 0x7f0708d4

.field public static final notification_card_text_margin_top:I = 0x7f0708d5

.field public static final notification_card_text_size:I = 0x7f0708d6

.field public static final notification_card_top_padding:I = 0x7f0708d7

.field public static final notification_content_margin_start:I = 0x7f0708d8

.field public static final notification_history_header_drawable_start:I = 0x7f0708d9

.field public static final notification_importance_button_horiz_padding:I = 0x7f0708da

.field public static final notification_importance_button_padding:I = 0x7f0708db

.field public static final notification_importance_button_separation:I = 0x7f0708dc

.field public static final notification_importance_button_text:I = 0x7f0708dd

.field public static final notification_importance_button_width:I = 0x7f0708de

.field public static final notification_importance_description_padding:I = 0x7f0708df

.field public static final notification_importance_description_text:I = 0x7f0708e0

.field public static final notification_importance_drawable_padding:I = 0x7f0708e1

.field public static final notification_importance_text_marginTop:I = 0x7f0708e2

.field public static final notification_importance_toggle_marginBottom:I = 0x7f0708e3

.field public static final notification_importance_toggle_marginTop:I = 0x7f0708e4

.field public static final notification_importance_toggle_size:I = 0x7f0708e5

.field public static final notification_large_icon_height:I = 0x7f0708e6

.field public static final notification_large_icon_width:I = 0x7f0708e7

.field public static final notification_main_column_padding_top:I = 0x7f0708e8

.field public static final notification_media_narrow_margin:I = 0x7f0708e9

.field public static final notification_right_icon_size:I = 0x7f0708ea

.field public static final notification_right_side_padding_top:I = 0x7f0708eb

.field public static final notification_small_icon_background_padding:I = 0x7f0708ec

.field public static final notification_small_icon_size_as_large:I = 0x7f0708ed

.field public static final notification_subtext_size:I = 0x7f0708ee

.field public static final notification_top_pad:I = 0x7f0708ef

.field public static final notification_top_pad_large_text:I = 0x7f0708f0

.field public static final numeric_encrypt_dot_size:I = 0x7f0708f1

.field public static final numeric_password_screen_dots_padding:I = 0x7f0708f2

.field public static final numeric_screen_dots_top_margin:I = 0x7f0708f3

.field public static final numeric_unlock_screen_bottom_margin:I = 0x7f0708f4

.field public static final numeric_unlock_screen_input_pad_horizontal_margin:I = 0x7f0708f5

.field public static final opacity_disabled:I = 0x7f0708f6

.field public static final opacity_enabled:I = 0x7f0708f7

.field public static final output_switcher_panel_icon_corner_radius:I = 0x7f0708f8

.field public static final output_switcher_panel_icon_size:I = 0x7f0708f9

.field public static final output_switcher_slice_max_height:I = 0x7f0708fa

.field public static final output_switcher_slice_padding_top:I = 0x7f0708fb

.field public static final pad_custom_carrier_page_margin_end:I = 0x7f0708fc

.field public static final pad_custom_carrier_page_margin_start:I = 0x7f0708fd

.field public static final padding_huge_font_tips:I = 0x7f0708fe

.field public static final page_content_horizontal:I = 0x7f0708ff

.field public static final page_content_horizontal_end:I = 0x7f070900

.field public static final page_layout_extral_small_size:I = 0x7f070901

.field public static final page_layout_extral_small_summary_size:I = 0x7f070902

.field public static final page_layout_extral_small_title_size:I = 0x7f070903

.field public static final page_layout_godzilla_actionbar_title_size:I = 0x7f070904

.field public static final page_layout_godzilla_size:I = 0x7f070905

.field public static final page_layout_godzilla_summary_size:I = 0x7f070906

.field public static final page_layout_godzilla_title_size:I = 0x7f070907

.field public static final page_layout_huge_actionbar_title_size:I = 0x7f070908

.field public static final page_layout_huge_size:I = 0x7f070909

.field public static final page_layout_huge_summary_size:I = 0x7f07090a

.field public static final page_layout_huge_title_size:I = 0x7f07090b

.field public static final page_layout_large_actionbar_title_size:I = 0x7f07090c

.field public static final page_layout_large_size:I = 0x7f07090d

.field public static final page_layout_large_summary_size:I = 0x7f07090e

.field public static final page_layout_large_title_size:I = 0x7f07090f

.field public static final page_layout_level_left_margin1:I = 0x7f070910

.field public static final page_layout_level_left_margin2:I = 0x7f070911

.field public static final page_layout_level_text_size:I = 0x7f070912

.field public static final page_layout_lyt_page_2_paddings_left:I = 0x7f070913

.field public static final page_layout_medium_actionbar_title_size:I = 0x7f070914

.field public static final page_layout_medium_size:I = 0x7f070915

.field public static final page_layout_medium_summary_size:I = 0x7f070916

.field public static final page_layout_medium_title_size:I = 0x7f070917

.field public static final page_layout_normal_actionbar_title_size:I = 0x7f070918

.field public static final page_layout_normal_size:I = 0x7f070919

.field public static final page_layout_normal_summary_size:I = 0x7f07091a

.field public static final page_layout_normal_title_size:I = 0x7f07091b

.field public static final page_layout_panel_padding_left:I = 0x7f07091c

.field public static final page_layout_panel_padding_top:I = 0x7f07091d

.field public static final page_layout_small_actionbar_title_size:I = 0x7f07091e

.field public static final page_layout_small_size:I = 0x7f07091f

.field public static final page_layout_small_summary_size:I = 0x7f070920

.field public static final page_layout_small_title_size:I = 0x7f070921

.field public static final page_layout_space:I = 0x7f070922

.field public static final pager_tabs_padding:I = 0x7f070923

.field public static final pager_tabs_selected_indicator_height:I = 0x7f070924

.field public static final pager_tabs_title_padding:I = 0x7f070925

.field public static final panel_fixed_width_minor:I = 0x7f070926

.field public static final panel_slice_Horizontal_padding:I = 0x7f070927

.field public static final panel_slice_vertical_padding:I = 0x7f070928

.field public static final paper_mode_view_padding:I = 0x7f070929

.field public static final params_card_height:I = 0x7f07092a

.field public static final passport_progressbar_size:I = 0x7f07092b

.field public static final password_guard_title:I = 0x7f07092c

.field public static final password_guard_title_large:I = 0x7f07092d

.field public static final password_input_text_size:I = 0x7f07092e

.field public static final password_requirement_textsize:I = 0x7f07092f

.field public static final password_settings_edittext_width:I = 0x7f070930

.field public static final pattern_layout_margin_top:I = 0x7f070931

.field public static final pattern_settings_header_text_size:I = 0x7f070932

.field public static final pattern_settings_lock_pattern_view_size:I = 0x7f070933

.field public static final pattern_settings_lock_pattern_view_size_half:I = 0x7f070934

.field public static final pattern_settings_lock_pattern_view_size_more_than_half:I = 0x7f070935

.field public static final pattern_unlock_screen_bottom_margin:I = 0x7f070936

.field public static final pattern_unlock_screen_emergency_text_size:I = 0x7f070937

.field public static final perference_screen_padding_top:I = 0x7f070938

.field public static final picker_margin_end:I = 0x7f070939

.field public static final picker_radius:I = 0x7f07093a

.field public static final picker_start_end:I = 0x7f07093b

.field public static final picker_width:I = 0x7f07093c

.field public static final play_button_margin_bottom:I = 0x7f07093d

.field public static final play_button_margin_end:I = 0x7f07093e

.field public static final play_button_width_height:I = 0x7f07093f

.field public static final power_guide_card_height:I = 0x7f070940

.field public static final power_guide_div_left_margin:I = 0x7f070941

.field public static final power_guide_div_right_margin:I = 0x7f070942

.field public static final power_guide_icon:I = 0x7f070943

.field public static final power_guide_out_margin:I = 0x7f070944

.field public static final power_guide_text_icon_margin:I = 0x7f070945

.field public static final power_guide_text_margin_top:I = 0x7f070946

.field public static final power_guide_title_margin_bottom:I = 0x7f070947

.field public static final power_guide_title_margin_top:I = 0x7f070948

.field public static final power_guide_top_card_bottom_space_height:I = 0x7f070949

.field public static final power_guide_txt_line_margin_horizontal:I = 0x7f07094a

.field public static final power_guide_txt_line_margin_vertical:I = 0x7f07094b

.field public static final pre_install_app_icon_item_height:I = 0x7f07094c

.field public static final pre_install_app_icon_margin_end:I = 0x7f07094d

.field public static final pre_install_app_icon_width:I = 0x7f07094e

.field public static final pre_install_app_list_margin_top:I = 0x7f07094f

.field public static final preference_account_avatar_size:I = 0x7f070950

.field public static final preference_account_padding_left:I = 0x7f070951

.field public static final preference_account_padding_right:I = 0x7f070952

.field public static final preference_bt_custom_margin_bottom:I = 0x7f070953

.field public static final preference_bt_custom_margin_end:I = 0x7f070954

.field public static final preference_bt_custom_margin_start:I = 0x7f070955

.field public static final preference_bt_custom_margin_top:I = 0x7f070956

.field public static final preference_bt_custom_misc_title_padding:I = 0x7f070957

.field public static final preference_bt_custom_padding_start:I = 0x7f070958

.field public static final preference_bt_custom_widget_margin_left:I = 0x7f070959

.field public static final preference_bt_custom_widget_margin_right:I = 0x7f07095a

.field public static final preference_bt_custom_widget_width:I = 0x7f07095b

.field public static final preference_category_item_padding_side:I = 0x7f07095c

.field public static final preference_dropdown_padding_start:I = 0x7f07095d

.field public static final preference_fragment_padding_bottom:I = 0x7f07095e

.field public static final preference_fragment_padding_side:I = 0x7f07095f

.field public static final preference_icon_foreground_image_inset:I = 0x7f070960

.field public static final preference_icon_minWidth:I = 0x7f070961

.field public static final preference_icon_padding_end:I = 0x7f070962

.field public static final preference_icon_size:I = 0x7f070963

.field public static final preference_image_margin_bottom:I = 0x7f070964

.field public static final preference_image_margin_top:I = 0x7f070965

.field public static final preference_image_second_title_margin_top:I = 0x7f070966

.field public static final preference_image_summary_margin_top:I = 0x7f070967

.field public static final preference_item_padding:I = 0x7f070968

.field public static final preference_item_padding_inner:I = 0x7f070969

.field public static final preference_item_padding_left:I = 0x7f07096a

.field public static final preference_item_padding_right:I = 0x7f07096b

.field public static final preference_item_padding_side:I = 0x7f07096c

.field public static final preference_no_icon_padding_start:I = 0x7f07096d

.field public static final preference_screen_padding_bottom:I = 0x7f07096e

.field public static final preference_seekbar_padding_horizontal:I = 0x7f07096f

.field public static final preference_seekbar_padding_vertical:I = 0x7f070970

.field public static final preference_seekbar_value_minWidth:I = 0x7f070971

.field public static final preference_text_padding_top_bottom:I = 0x7f070972

.field public static final preference_text_right_max_width:I = 0x7f070973

.field public static final preference_text_size:I = 0x7f070974

.field public static final preference_value_aod_style_icon_height:I = 0x7f070975

.field public static final preference_value_aod_style_icon_width:I = 0x7f070976

.field public static final preference_value_max_width:I = 0x7f070977

.field public static final preference_value_padding_bottom:I = 0x7f070978

.field public static final preference_value_padding_top:I = 0x7f070979

.field public static final preference_wifi_custom_widget_width:I = 0x7f07097a

.field public static final presentation_clock_width:I = 0x7f07097b

.field public static final preview_img_margin:I = 0x7f07097c

.field public static final preview_pager_padding:I = 0x7f07097d

.field public static final preview_pref_height:I = 0x7f07097e

.field public static final privacy_choose_icon_height:I = 0x7f07097f

.field public static final privacy_choose_icon_margin_top:I = 0x7f070980

.field public static final privacy_choose_icon_width:I = 0x7f070981

.field public static final privacy_choose_redraw_verify_button_hight:I = 0x7f070982

.field public static final privacy_choose_unlock_password_footer_margin:I = 0x7f070983

.field public static final privacy_confirm_forget_password_bottom_margin:I = 0x7f070984

.field public static final privacy_confirm_forget_password_bottom_margin_cetus:I = 0x7f070985

.field public static final privacy_confirm_icon_margin_top:I = 0x7f070986

.field public static final privacy_confirm_or_choose_relative_layout_hight:I = 0x7f070987

.field public static final privacy_funciotn_icon_top_margin:I = 0x7f070988

.field public static final privacy_function_button_bottom_margin:I = 0x7f070989

.field public static final privacy_icon_margin_top:I = 0x7f07098a

.field public static final privacy_list_header_footer_padding_start_end:I = 0x7f07098b

.field public static final privacy_list_header_line_height:I = 0x7f07098c

.field public static final privacy_list_header_line_margin_top:I = 0x7f07098d

.field public static final privacy_list_header_padding_start:I = 0x7f07098e

.field public static final privacy_list_header_padding_top:I = 0x7f07098f

.field public static final privacy_list_header_summary_margin_top:I = 0x7f070990

.field public static final privacy_list_header_textsize:I = 0x7f070991

.field public static final privacy_lockpattern_margin_bottom:I = 0x7f070992

.field public static final privacy_password_footer_text_margin_top:I = 0x7f070993

.field public static final privacy_password_footer_text_size:I = 0x7f070994

.field public static final privacy_password_forget_pattern_height:I = 0x7f070995

.field public static final privacy_password_forget_pattern_text_size:I = 0x7f070996

.field public static final privacy_password_header_margin_top:I = 0x7f070997

.field public static final privacy_password_header_text_margin_top:I = 0x7f070998

.field public static final privacy_password_header_text_size:I = 0x7f070999

.field public static final privacy_password_header_text_text_size:I = 0x7f07099a

.field public static final privacy_password_lockpattern_view_margin:I = 0x7f07099b

.field public static final privacy_password_more_margin_end:I = 0x7f07099c

.field public static final privacy_password_setting_margin_top:I = 0x7f07099d

.field public static final privacy_password_setting_size:I = 0x7f07099e

.field public static final privacy_password_top_layout_margin_top_pc:I = 0x7f07099f

.field public static final profile_badge_size:I = 0x7f0709a0

.field public static final progress_height:I = 0x7f0709a1

.field public static final progress_margin_top:I = 0x7f0709a2

.field public static final provision_accesspoint_summary_size:I = 0x7f0709a3

.field public static final provision_accesspoint_title_size:I = 0x7f0709a4

.field public static final provision_actionbar_height:I = 0x7f0709a5

.field public static final provision_anim_floating_window_height_edge:I = 0x7f0709a6

.field public static final provision_anim_floating_window_width_edge:I = 0x7f0709a7

.field public static final provision_anim_margin_end:I = 0x7f0709a8

.field public static final provision_anim_margin_start:I = 0x7f0709a9

.field public static final provision_back_left_padding:I = 0x7f0709aa

.field public static final provision_back_margin_top:I = 0x7f0709ab

.field public static final provision_btn_height:I = 0x7f0709ac

.field public static final provision_btn_width:I = 0x7f0709ad

.field public static final provision_button_left_padding:I = 0x7f0709ae

.field public static final provision_button_right_padding:I = 0x7f0709af

.field public static final provision_button_text_size:I = 0x7f0709b0

.field public static final provision_choose_password_top_margin:I = 0x7f0709b1

.field public static final provision_choose_pattern_top_margin:I = 0x7f0709b2

.field public static final provision_choose_unlock_top_margin:I = 0x7f0709b3

.field public static final provision_container_margin_top:I = 0x7f0709b4

.field public static final provision_content_bottom_padding:I = 0x7f0709b5

.field public static final provision_content_margin_top:I = 0x7f0709b6

.field public static final provision_content_padding_horizontal:I = 0x7f0709b7

.field public static final provision_content_top_padding:I = 0x7f0709b8

.field public static final provision_context_left_margin:I = 0x7f0709b9

.field public static final provision_context_right_margin:I = 0x7f0709ba

.field public static final provision_description_margin_top:I = 0x7f0709bb

.field public static final provision_description_text_size:I = 0x7f0709bc

.field public static final provision_disc_padding_top:I = 0x7f0709bd

.field public static final provision_highlight_side_left_margin:I = 0x7f0709be

.field public static final provision_highlight_side_right_margin:I = 0x7f0709bf

.field public static final provision_highlight_top_margin:I = 0x7f0709c0

.field public static final provision_line_left_padding:I = 0x7f0709c1

.field public static final provision_line_right_padding:I = 0x7f0709c2

.field public static final provision_list_desc_margin_bottom:I = 0x7f0709c3

.field public static final provision_list_item_bg_radius_size:I = 0x7f0709c4

.field public static final provision_list_left_padding:I = 0x7f0709c5

.field public static final provision_list_padding_bottom:I = 0x7f0709c6

.field public static final provision_list_padding_top:I = 0x7f0709c7

.field public static final provision_list_right_padding:I = 0x7f0709c8

.field public static final provision_logo_title_size:I = 0x7f0709c9

.field public static final provision_margin_bottom:I = 0x7f0709ca

.field public static final provision_margin_end:I = 0x7f0709cb

.field public static final provision_margin_start:I = 0x7f0709cc

.field public static final provision_margin_top:I = 0x7f0709cd

.field public static final provision_navigation_height:I = 0x7f0709ce

.field public static final provision_next_bottom_margin:I = 0x7f0709cf

.field public static final provision_next_button_padding_bottom_vertical:I = 0x7f0709d0

.field public static final provision_next_button_padding_top_vertical:I = 0x7f0709d1

.field public static final provision_next_button_padding_vertical:I = 0x7f0709d2

.field public static final provision_next_button_text_size:I = 0x7f0709d3

.field public static final provision_other_ap_list_item_bg_radius_size:I = 0x7f0709d4

.field public static final provision_padding_top:I = 0x7f0709d5

.field public static final provision_page_padding_horizontal:I = 0x7f0709d6

.field public static final provision_preference_height:I = 0x7f0709d7

.field public static final provision_preference_high_light_radius:I = 0x7f0709d8

.field public static final provision_preference_margin_bottom:I = 0x7f0709d9

.field public static final provision_preference_signal_margin_top:I = 0x7f0709da

.field public static final provision_preference_title_margin_top:I = 0x7f0709db

.field public static final provision_preview_height:I = 0x7f0709dc

.field public static final provision_preview_width:I = 0x7f0709dd

.field public static final provision_setup_preference_start_margin:I = 0x7f0709de

.field public static final provision_signal_left_padding:I = 0x7f0709df

.field public static final provision_step_text_maxwidth:I = 0x7f0709e0

.field public static final provision_step_text_size:I = 0x7f0709e1

.field public static final provision_text_padding:I = 0x7f0709e2

.field public static final provision_text_padding_bottom:I = 0x7f0709e3

.field public static final provision_text_padding_edge_new:I = 0x7f0709e4

.field public static final provision_text_padding_end:I = 0x7f0709e5

.field public static final provision_text_padding_skip:I = 0x7f0709e6

.field public static final provision_text_padding_start:I = 0x7f0709e7

.field public static final provision_text_padding_top:I = 0x7f0709e8

.field public static final provision_text_size_explain:I = 0x7f0709e9

.field public static final provision_text_size_subtitle:I = 0x7f0709ea

.field public static final provision_text_size_title:I = 0x7f0709eb

.field public static final provision_text_size_title_margin:I = 0x7f0709ec

.field public static final provision_text_size_title_small:I = 0x7f0709ed

.field public static final provision_title_height:I = 0x7f0709ee

.field public static final provision_title_layout_height:I = 0x7f0709ef

.field public static final provision_title_padding:I = 0x7f0709f0

.field public static final provision_title_text_size:I = 0x7f0709f1

.field public static final provision_title_top_padding:I = 0x7f0709f2

.field public static final provision_titlewithsub_add_padding:I = 0x7f0709f3

.field public static final provision_wifilist_right_margin:I = 0x7f0709f4

.field public static final provisionlib_btn_margin_left_bound:I = 0x7f0709f5

.field public static final provisionlib_btn_margin_right_bound:I = 0x7f0709f6

.field public static final pvc_account_text_margin_top:I = 0x7f0709f7

.field public static final pvc_account_text_size:I = 0x7f0709f8

.field public static final pvc_add_account_info_margin_top:I = 0x7f0709f9

.field public static final pvc_exit_account_info_margin_top:I = 0x7f0709fa

.field public static final pvc_exit_account_info_text_size:I = 0x7f0709fb

.field public static final px_160:I = 0x7f0709fc

.field public static final px_189:I = 0x7f0709fd

.field public static final px_40:I = 0x7f0709fe

.field public static final px_80:I = 0x7f0709ff

.field public static final px_fod_margin:I = 0x7f070a00

.field public static final qhd_text_summary_view_margin_left:I = 0x7f070a01

.field public static final qhd_text_view_margin_left:I = 0x7f070a02

.field public static final qrcode_icon_size:I = 0x7f070a03

.field public static final qrcode_preview_margin:I = 0x7f070a04

.field public static final qrcode_preview_radius:I = 0x7f070a05

.field public static final qrcode_preview_size:I = 0x7f070a06

.field public static final qrcode_size:I = 0x7f070a07

.field public static final raise_bottom_height_of_keyboard_bg_corners:I = 0x7f070a08

.field public static final raise_bottom_height_of_keyboard_bg_padding:I = 0x7f070a09

.field public static final raise_bottom_height_of_keyboard_bg_width:I = 0x7f070a0a

.field public static final raise_bottom_height_of_keyboard_des_text_margin_top:I = 0x7f070a0b

.field public static final raise_bottom_height_of_keyboard_des_text_size:I = 0x7f070a0c

.field public static final raise_bottom_height_of_keyboard_image_view_height:I = 0x7f070a0d

.field public static final raise_bottom_height_of_keyboard_image_view_margin_top:I = 0x7f070a0e

.field public static final raise_bottom_height_of_keyboard_image_view_width:I = 0x7f070a0f

.field public static final raise_bottom_height_of_keyboard_sub_title_margin_top:I = 0x7f070a10

.field public static final raise_bottom_height_of_keyboard_sub_title_text_size:I = 0x7f070a11

.field public static final raise_bottom_height_of_keyboard_text_layout_width:I = 0x7f070a12

.field public static final raise_bottom_height_of_keyboard_title_margin_top:I = 0x7f070a13

.field public static final raise_bottom_height_of_keyboard_title_text_size:I = 0x7f070a14

.field public static final raise_bottom_height_of_keyboard_view_margin:I = 0x7f070a15

.field public static final recommend_item_vertical_margin:I = 0x7f070a16

.field public static final recommend_region_margin:I = 0x7f070a17

.field public static final recommend_region_padding_bottom:I = 0x7f070a18

.field public static final recommend_title_margin_bottom:I = 0x7f070a19

.field public static final recommend_title_margin_start:I = 0x7f070a1a

.field public static final recommend_title_margin_top:I = 0x7f070a1b

.field public static final rect_button_radius:I = 0x7f070a1c

.field public static final redaction_padding_start:I = 0x7f070a1d

.field public static final redaction_vertical_margins:I = 0x7f070a1e

.field public static final reset_button_margin_end:I = 0x7f070a1f

.field public static final reset_checkbox_padding_end:I = 0x7f070a20

.field public static final reset_checkbox_summary_padding_top:I = 0x7f070a21

.field public static final reset_checkbox_summary_text_size:I = 0x7f070a22

.field public static final reset_checkbox_title_padding_top:I = 0x7f070a23

.field public static final reset_checkbox_title_text_size:I = 0x7f070a24

.field public static final reset_internet_ring_progress_right_margin:I = 0x7f070a25

.field public static final reset_legacy_password_settings_instruction_text_size:I = 0x7f070a26

.field public static final reset_network_margin_end:I = 0x7f070a27

.field public static final reset_network_margin_start:I = 0x7f070a28

.field public static final resolution_selection_image_view_height:I = 0x7f070a29

.field public static final resolution_selection_image_view_padding_bottom:I = 0x7f070a2a

.field public static final resolution_selection_image_view_padding_left:I = 0x7f070a2b

.field public static final resolution_selection_image_view_padding_right:I = 0x7f070a2c

.field public static final resolution_selection_image_view_padding_top:I = 0x7f070a2d

.field public static final resolution_selection_image_view_width:I = 0x7f070a2e

.field public static final resolution_selection_margin_bottom:I = 0x7f070a2f

.field public static final resolution_selection_margin_top:I = 0x7f070a30

.field public static final resolution_text_size:I = 0x7f070a31

.field public static final resolution_text_summary_fhd_size:I = 0x7f070a32

.field public static final resolution_text_summary_qhd_size:I = 0x7f070a33

.field public static final resolution_text_summary_view_height:I = 0x7f070a34

.field public static final resolution_text_summary_view_margin_top:I = 0x7f070a35

.field public static final resolution_text_summary_view_width:I = 0x7f070a36

.field public static final resolution_text_view_fhd_width:I = 0x7f070a37

.field public static final resolution_text_view_height:I = 0x7f070a38

.field public static final resolution_text_view_margin_top:I = 0x7f070a39

.field public static final resolution_text_view_qhd_width:I = 0x7f070a3a

.field public static final resolution_text_view_summary_fhd_width:I = 0x7f070a3b

.field public static final resolution_text_view_summary_margin_top:I = 0x7f070a3c

.field public static final resolution_text_view_summary_qhd_width:I = 0x7f070a3d

.field public static final restricted_icon_margin_end:I = 0x7f070a3e

.field public static final restricted_icon_padding:I = 0x7f070a3f

.field public static final restricted_icon_size:I = 0x7f070a40

.field public static final restricted_screen_timeout_subtitle_textsize:I = 0x7f070a41

.field public static final restricted_screen_timeout_title_textsize:I = 0x7f070a42

.field public static final right_video_margin_end:I = 0x7f070a43

.field public static final right_video_margin_start:I = 0x7f070a44

.field public static final ring_progress_bar_thickness:I = 0x7f070a45

.field public static final ringtone_card_height:I = 0x7f070a46

.field public static final ringtone_card_margin:I = 0x7f070a47

.field public static final ringtone_card_margin_edge:I = 0x7f070a48

.field public static final ringtone_card_margin_top:I = 0x7f070a49

.field public static final ringtone_card_noti_img_margin:I = 0x7f070a4a

.field public static final ringtone_card_noti_img_width:I = 0x7f070a4b

.field public static final ringtone_card_noti_padding:I = 0x7f070a4c

.field public static final ringtone_card_noti_title_margin:I = 0x7f070a4d

.field public static final ringtone_card_phone_padding:I = 0x7f070a4e

.field public static final ringtone_card_phone_padding_start:I = 0x7f070a4f

.field public static final ringtone_item_icon_margin_start:I = 0x7f070a50

.field public static final ringtone_item_icon_size:I = 0x7f070a51

.field public static final ringtone_item_icon_title_padding:I = 0x7f070a52

.field public static final ringtone_item_icon_wh:I = 0x7f070a53

.field public static final ringtone_item_padding_bottom:I = 0x7f070a54

.field public static final ringtone_item_padding_top:I = 0x7f070a55

.field public static final ringtone_settings_card_height:I = 0x7f070a56

.field public static final ringtone_settings_card_height_no_call:I = 0x7f070a57

.field public static final round_radius:I = 0x7f070a58

.field public static final round_width:I = 0x7f070a59

.field public static final row_width:I = 0x7f070a5a

.field public static final scanner_back_size:I = 0x7f070a5b

.field public static final scanner_filled_height:I = 0x7f070a5c

.field public static final scanner_filled_width:I = 0x7f070a5d

.field public static final scanner_frame_size:I = 0x7f070a5e

.field public static final scanner_summary_min_width:I = 0x7f070a5f

.field public static final screen_color_margin_bottom:I = 0x7f070a60

.field public static final screen_color_margin_gap:I = 0x7f070a61

.field public static final screen_color_preview_diameter:I = 0x7f070a62

.field public static final screen_color_preview_diameter_new:I = 0x7f070a63

.field public static final screen_color_preview_margin_top:I = 0x7f070a64

.field public static final screen_color_preview_offset:I = 0x7f070a65

.field public static final screen_color_preview_offset_new:I = 0x7f070a66

.field public static final screen_color_text_height:I = 0x7f070a67

.field public static final screen_color_text_margin_top:I = 0x7f070a68

.field public static final screen_color_text_size:I = 0x7f070a69

.field public static final screen_color_text_width:I = 0x7f070a6a

.field public static final screen_effect_actionbar_height:I = 0x7f070a6b

.field public static final screen_effect_btn_maxwidth:I = 0x7f070a6c

.field public static final screen_effect_btn_padding_horizontal:I = 0x7f070a6d

.field public static final screen_effect_btn_radius:I = 0x7f070a6e

.field public static final screen_effect_btn_stroke_width:I = 0x7f070a6f

.field public static final screen_margin_bottom:I = 0x7f070a70

.field public static final screen_margin_sides:I = 0x7f070a71

.field public static final screen_margin_top:I = 0x7f070a72

.field public static final screen_padding_top:I = 0x7f070a73

.field public static final screen_pinning_padding_end:I = 0x7f070a74

.field public static final screen_pinning_padding_start:I = 0x7f070a75

.field public static final screen_zoom_preview_height:I = 0x7f070a76

.field public static final scroll_actionbar_over_distance:I = 0x7f070a77

.field public static final scroll_actionbar_range:I = 0x7f070a78

.field public static final search_bar_content_inset:I = 0x7f070a79

.field public static final search_bar_corner_radius:I = 0x7f070a7a

.field public static final search_bar_height:I = 0x7f070a7b

.field public static final search_bar_margin:I = 0x7f070a7c

.field public static final search_bar_padding_end:I = 0x7f070a7d

.field public static final search_bar_padding_end_two_pane:I = 0x7f070a7e

.field public static final search_bar_padding_start:I = 0x7f070a7f

.field public static final search_bar_padding_start_two_pane:I = 0x7f070a80

.field public static final search_bar_text_size:I = 0x7f070a81

.field public static final search_bar_title_padding_start:I = 0x7f070a82

.field public static final search_bar_title_padding_start_regular_two_pane:I = 0x7f070a83

.field public static final search_description_text_size:I = 0x7f070a84

.field public static final search_empty_view_margin_element:I = 0x7f070a85

.field public static final search_empty_view_margin_top:I = 0x7f070a86

.field public static final search_history_clear_margin_bottom:I = 0x7f070a87

.field public static final search_history_clear_margin_top:I = 0x7f070a88

.field public static final search_history_ly_margin_end:I = 0x7f070a89

.field public static final search_history_ly_margin_start:I = 0x7f070a8a

.field public static final search_history_text_size:I = 0x7f070a8b

.field public static final search_history_tv_bg_radius:I = 0x7f070a8c

.field public static final search_history_tv_margin_end:I = 0x7f070a8d

.field public static final search_history_tv_max_width:I = 0x7f070a8e

.field public static final search_item_padding_end:I = 0x7f070a8f

.field public static final search_item_padding_start:I = 0x7f070a90

.field public static final search_result_item_layout_height:I = 0x7f070a91

.field public static final search_tv_margin_top_and_bottom:I = 0x7f070a92

.field public static final search_tv_padding_start_and_end:I = 0x7f070a93

.field public static final search_tv_padding_top_and_bottom:I = 0x7f070a94

.field public static final secondary_app_icon_size:I = 0x7f070a95

.field public static final secure_card_img_width:I = 0x7f070a96

.field public static final secure_card_margin_actionbar:I = 0x7f070a97

.field public static final secure_card_margin_category:I = 0x7f070a98

.field public static final secure_card_margin_title:I = 0x7f070a99

.field public static final secure_card_padding_bottom:I = 0x7f070a9a

.field public static final secure_card_padding_start:I = 0x7f070a9b

.field public static final secure_card_padding_top:I = 0x7f070a9c

.field public static final security_card_instructions_text_size:I = 0x7f070a9d

.field public static final security_card_key_words_text_size:I = 0x7f070a9e

.field public static final security_ime_order_icon_height:I = 0x7f070a9f

.field public static final security_ime_order_icon_width:I = 0x7f070aa0

.field public static final seekbar_title_left_margin:I = 0x7f070aa1

.field public static final seekbar_width:I = 0x7f070aa2

.field public static final select_dialog_item_margin_start:I = 0x7f070aa3

.field public static final select_dialog_padding_start:I = 0x7f070aa4

.field public static final select_dialog_summary_padding_bottom:I = 0x7f070aa5

.field public static final setting_item_padding_bottom:I = 0x7f070aa6

.field public static final setting_item_padding_top:I = 0x7f070aa7

.field public static final setting_margin_left:I = 0x7f070aa8

.field public static final setting_margin_right:I = 0x7f070aa9

.field public static final settings_bar_view_icon_size:I = 0x7f070aaa

.field public static final settings_bar_view_max_height:I = 0x7f070aab

.field public static final settings_header_wifi_textview_width:I = 0x7f070aac

.field public static final settings_panel_button_group_height:I = 0x7f070aad

.field public static final settings_panel_corner_radius:I = 0x7f070aae

.field public static final settings_panel_default_height:I = 0x7f070aaf

.field public static final settings_panel_title_margin:I = 0x7f070ab0

.field public static final settings_panel_title_margin_bottom:I = 0x7f070ab1

.field public static final settings_panel_width:I = 0x7f070ab2

.field public static final settings_search_margin:I = 0x7f070ab3

.field public static final settings_side_margin:I = 0x7f070ab4

.field public static final settingslib_dialogCornerRadius:I = 0x7f070ab5

.field public static final settingslib_highlight_alpha_material_dark:I = 0x7f070ab6

.field public static final settingslib_highlight_alpha_material_light:I = 0x7f070ab7

.field public static final settingslib_illustration_height:I = 0x7f070ab8

.field public static final settingslib_illustration_padding:I = 0x7f070ab9

.field public static final settingslib_illustration_width:I = 0x7f070aba

.field public static final settingslib_listPreferredItemPaddingEnd:I = 0x7f070abb

.field public static final settingslib_listPreferredItemPaddingStart:I = 0x7f070abc

.field public static final settingslib_min_switch_bar_height:I = 0x7f070abd

.field public static final settingslib_min_switch_width:I = 0x7f070abe

.field public static final settingslib_preferenceIconSize:I = 0x7f070abf

.field public static final settingslib_preferred_minimum_touch_target:I = 0x7f070ac0

.field public static final settingslib_restricted_icon_margin_end:I = 0x7f070ac1

.field public static final settingslib_restricted_icon_size:I = 0x7f070ac2

.field public static final settingslib_scrim_visible_height_trigger:I = 0x7f070ac3

.field public static final settingslib_spinner_height:I = 0x7f070ac4

.field public static final settingslib_switch_bar_radius:I = 0x7f070ac5

.field public static final settingslib_switch_title_margin:I = 0x7f070ac6

.field public static final settingslib_switchbar_margin:I = 0x7f070ac7

.field public static final settingslib_switchbar_padding_left:I = 0x7f070ac8

.field public static final settingslib_switchbar_padding_right:I = 0x7f070ac9

.field public static final settingslib_switchbar_subsettings_margin_end:I = 0x7f070aca

.field public static final settingslib_switchbar_subsettings_margin_start:I = 0x7f070acb

.field public static final settingslib_toolbar_layout_height:I = 0x7f070acc

.field public static final setup_choose_password_msg_size:I = 0x7f070acd

.field public static final setup_choose_password_title_size:I = 0x7f070ace

.field public static final setup_footer_bottom_margin:I = 0x7f070acf

.field public static final setup_footer_start_margin:I = 0x7f070ad0

.field public static final setup_list_item_radius_size:I = 0x7f070ad1

.field public static final setup_preference_start_margin:I = 0x7f070ad2

.field public static final setup_title_start_margin:I = 0x7f070ad3

.field public static final setup_title_top_margin:I = 0x7f070ad4

.field public static final shortcut_icon_size:I = 0x7f070ad5

.field public static final shortcut_size:I = 0x7f070ad6

.field public static final shortcut_size_maskable:I = 0x7f070ad7

.field public static final shoulderkey_guide_image_view_height:I = 0x7f070ad8

.field public static final shoulderkey_guide_image_view_vertical_margin:I = 0x7f070ad9

.field public static final shoulderkey_guide_image_view_width:I = 0x7f070ada

.field public static final shoulderkey_sound_selection_image_view_height:I = 0x7f070adb

.field public static final shoulderkey_sound_selection_image_view_padding_bottom:I = 0x7f070adc

.field public static final shoulderkey_sound_selection_image_view_padding_left:I = 0x7f070add

.field public static final shoulderkey_sound_selection_image_view_padding_right:I = 0x7f070ade

.field public static final shoulderkey_sound_selection_image_view_padding_top:I = 0x7f070adf

.field public static final shoulderkey_sound_selection_image_view_width:I = 0x7f070ae0

.field public static final shoulderkey_sound_selection_margin_top:I = 0x7f070ae1

.field public static final shoulderkey_sound_selection_text_view_margin_bottom:I = 0x7f070ae2

.field public static final shoulderkey_sound_selection_text_view_margin_left:I = 0x7f070ae3

.field public static final signal_icon_size:I = 0x7f070ae4

.field public static final signal_strength_icon_size:I = 0x7f070ae5

.field public static final silent_zen_mode_button_marginStart:I = 0x7f070ae6

.field public static final silent_zen_mode_button_padding:I = 0x7f070ae7

.field public static final sim_color_spinner_padding:I = 0x7f070ae8

.field public static final sim_content_padding:I = 0x7f070ae9

.field public static final sim_dialog_margin_bottom:I = 0x7f070aea

.field public static final sim_dialog_margin_top:I = 0x7f070aeb

.field public static final sim_dialog_padding:I = 0x7f070aec

.field public static final sim_label_padding:I = 0x7f070aed

.field public static final sim_red_dot_size:I = 0x7f070aee

.field public static final sims_select_margin_bottom:I = 0x7f070aef

.field public static final sims_select_margin_top:I = 0x7f070af0

.field public static final sliding_button_border:I = 0x7f070af1

.field public static final smart_cover_item_min_height:I = 0x7f070af2

.field public static final sos_anim_height:I = 0x7f070af3

.field public static final sos_anim_margin_start:I = 0x7f070af4

.field public static final sos_anim_width:I = 0x7f070af5

.field public static final sos_bottom_native_layout_stroke_width:I = 0x7f070af6

.field public static final sos_bottom_native_padding_bottom:I = 0x7f070af7

.field public static final sos_bottom_native_padding_left:I = 0x7f070af8

.field public static final sos_bottom_native_padding_right:I = 0x7f070af9

.field public static final sos_bottom_native_padding_top:I = 0x7f070afa

.field public static final sos_bottom_native_radius:I = 0x7f070afb

.field public static final sos_btn_min_width:I = 0x7f070afc

.field public static final sos_cn_gp_call_margin_end:I = 0x7f070afd

.field public static final sos_cn_gp_call_margin_start:I = 0x7f070afe

.field public static final sos_cn_gp_call_margin_top:I = 0x7f070aff

.field public static final sos_cn_gp_emergency_margin_end:I = 0x7f070b00

.field public static final sos_cn_imageview_emergency_height:I = 0x7f070b01

.field public static final sos_cn_imageview_emergency_margin_top:I = 0x7f070b02

.field public static final sos_cn_imageview_emergency_width:I = 0x7f070b03

.field public static final sos_cn_textview_emergency_margin_bottom:I = 0x7f070b04

.field public static final sos_cn_textview_emergency_margin_top:I = 0x7f070b05

.field public static final sos_common_padding:I = 0x7f070b06

.field public static final sos_common_radius:I = 0x7f070b07

.field public static final sos_contact_gp_margin_bottom:I = 0x7f070b08

.field public static final sos_contact_gp_margin_end:I = 0x7f070b09

.field public static final sos_contact_gp_margin_start:I = 0x7f070b0a

.field public static final sos_desc_margin_bottom:I = 0x7f070b0b

.field public static final sos_desc_margin_end:I = 0x7f070b0c

.field public static final sos_desc_margin_start:I = 0x7f070b0d

.field public static final sos_desc_margin_top:I = 0x7f070b0e

.field public static final sos_direction_back_height:I = 0x7f070b0f

.field public static final sos_direction_back_width:I = 0x7f070b10

.field public static final sos_direction_marginBottom:I = 0x7f070b11

.field public static final sos_direction_marginTop:I = 0x7f070b12

.field public static final sos_el_icon_height:I = 0x7f070b13

.field public static final sos_el_icon_margin_start:I = 0x7f070b14

.field public static final sos_el_icon_width:I = 0x7f070b15

.field public static final sos_el_right_part_margin_start:I = 0x7f070b16

.field public static final sos_emergency_name_max_width:I = 0x7f070b17

.field public static final sos_exit_button_height:I = 0x7f070b18

.field public static final sos_exit_button_width:I = 0x7f070b19

.field public static final sos_gp_contact_first_margin_bottom:I = 0x7f070b1a

.field public static final sos_gp_contact_first_margin_end:I = 0x7f070b1b

.field public static final sos_gp_contact_first_margin_start:I = 0x7f070b1c

.field public static final sos_gp_contact_first_margin_top:I = 0x7f070b1d

.field public static final sos_gp_contact_title_margin_start:I = 0x7f070b1e

.field public static final sos_gp_contact_title_margin_top:I = 0x7f070b1f

.field public static final sos_gp_desc_height:I = 0x7f070b20

.field public static final sos_gp_desc_margin_bottom:I = 0x7f070b21

.field public static final sos_gp_desc_margin_end:I = 0x7f070b22

.field public static final sos_gp_desc_margin_start:I = 0x7f070b23

.field public static final sos_gp_desc_margin_top:I = 0x7f070b24

.field public static final sos_gp_desc_minHeight:I = 0x7f070b25

.field public static final sos_gp_margin_end:I = 0x7f070b26

.field public static final sos_gp_margin_start:I = 0x7f070b27

.field public static final sos_group_cn_sos_el_height:I = 0x7f070b28

.field public static final sos_group_cn_sos_el_margin_end:I = 0x7f070b29

.field public static final sos_group_cn_sos_el_margin_start:I = 0x7f070b2a

.field public static final sos_group_cn_sos_el_margin_top:I = 0x7f070b2b

.field public static final sos_imageview_height:I = 0x7f070b2c

.field public static final sos_imageview_width:I = 0x7f070b2d

.field public static final sos_linear_margin_end:I = 0x7f070b2e

.field public static final sos_linear_margin_start:I = 0x7f070b2f

.field public static final sos_linear_total_height:I = 0x7f070b30

.field public static final sos_location_bg_height:I = 0x7f070b31

.field public static final sos_location_bg_margin_end:I = 0x7f070b32

.field public static final sos_lottie_marginLeft:I = 0x7f070b33

.field public static final sos_lottie_marginTop:I = 0x7f070b34

.field public static final sos_medical_summary_margin_end:I = 0x7f070b35

.field public static final sos_player_icon_length:I = 0x7f070b36

.field public static final sos_player_icon_margin_end:I = 0x7f070b37

.field public static final sos_player_linear_height:I = 0x7f070b38

.field public static final sos_player_text_margin_start:I = 0x7f070b39

.field public static final sos_player_text_size:I = 0x7f070b3a

.field public static final sos_recycleview_margin_top:I = 0x7f070b3b

.field public static final sos_statement_margin_top:I = 0x7f070b3c

.field public static final sos_textsize_12dp:I = 0x7f070b3d

.field public static final sos_textsize_13dp:I = 0x7f070b3e

.field public static final sos_textsize_14dp:I = 0x7f070b3f

.field public static final sos_textsize_16dp:I = 0x7f070b40

.field public static final sos_textsize_18dp:I = 0x7f070b41

.field public static final sos_textsize_22dp:I = 0x7f070b42

.field public static final sos_top_layout_height:I = 0x7f070b43

.field public static final sos_toplayout_margin_end:I = 0x7f070b44

.field public static final sos_toplayout_margin_start:I = 0x7f070b45

.field public static final sos_wifi_height:I = 0x7f070b46

.field public static final sos_wifi_width:I = 0x7f070b47

.field public static final sound_icon_margin_start:I = 0x7f070b48

.field public static final sound_icon_margin_top:I = 0x7f070b49

.field public static final sound_speaker_desc_text_size:I = 0x7f070b4a

.field public static final sound_text_margin_top:I = 0x7f070b4b

.field public static final spinner_dropdown_height:I = 0x7f070b4c

.field public static final spinner_height:I = 0x7f070b4d

.field public static final spinner_left_right_margin:I = 0x7f070b4e

.field public static final spinner_padding_top_or_bottom:I = 0x7f070b4f

.field public static final spinner_preference_style_margin:I = 0x7f070b50

.field public static final split_action_bar_overlay_height:I = 0x7f070b51

.field public static final state_edit_left_right_margin:I = 0x7f070b52

.field public static final state_edit_left_right_padding:I = 0x7f070b53

.field public static final status_bar_app_item_icon_max_height:I = 0x7f070b54

.field public static final status_bar_emergency_contacts_height:I = 0x7f070b55

.field public static final status_bar_emergency_contacts_padding:I = 0x7f070b56

.field public static final status_bar_height:I = 0x7f070b57

.field public static final status_bar_height_default:I = 0x7f070b58

.field public static final status_bar_small_view_height:I = 0x7f070b59

.field public static final status_bar_small_view_width:I = 0x7f070b5a

.field public static final status_bar_sos_view_height:I = 0x7f070b5b

.field public static final status_bar_sos_view_notch_start_padding:I = 0x7f070b5c

.field public static final status_bar_sos_view_ridus:I = 0x7f070b5d

.field public static final status_bar_sos_view_start_padding:I = 0x7f070b5e

.field public static final structure_keyguard_face_detect_height:I = 0x7f070b5f

.field public static final structure_keyguard_face_detect_left:I = 0x7f070b60

.field public static final structure_keyguard_face_detect_top:I = 0x7f070b61

.field public static final structure_keyguard_face_preview_detect_height:I = 0x7f070b62

.field public static final structure_keyguard_face_preview_detect_left:I = 0x7f070b63

.field public static final structure_keyguard_face_preview_detect_top:I = 0x7f070b64

.field public static final structure_keyguard_face_preview_height:I = 0x7f070b65

.field public static final stylus_arrow_margin_end_inner:I = 0x7f070b66

.field public static final stylus_battery_level_margin_top:I = 0x7f070b67

.field public static final stylus_battery_level_view_height:I = 0x7f070b68

.field public static final stylus_battery_level_view_width:I = 0x7f070b69

.field public static final stylus_battery_text_margin_top:I = 0x7f070b6a

.field public static final stylus_battery_window_height:I = 0x7f070b6b

.field public static final stylus_battery_window_width:I = 0x7f070b6c

.field public static final stylus_demo_blue_point_y_ratio:I = 0x7f070b6d

.field public static final stylus_demo_first_x_ratio:I = 0x7f070b6e

.field public static final stylus_demo_image_height_ratio:I = 0x7f070b6f

.field public static final stylus_demo_image_width_ratio:I = 0x7f070b70

.field public static final stylus_demo_image_x_ratio:I = 0x7f070b71

.field public static final stylus_demo_image_y_ratio:I = 0x7f070b72

.field public static final stylus_demo_second_x_ratio:I = 0x7f070b73

.field public static final stylus_demo_text_y_ratio:I = 0x7f070b74

.field public static final stylus_demo_width_and_height_ratio:I = 0x7f070b75

.field public static final stylus_edit_main_point_ms:I = 0x7f070b76

.field public static final stylus_edit_main_text_ms:I = 0x7f070b77

.field public static final stylus_edit_pen_height:I = 0x7f070b78

.field public static final stylus_edit_pen_mt:I = 0x7f070b79

.field public static final stylus_edit_pen_width:I = 0x7f070b7a

.field public static final stylus_edit_point_mt:I = 0x7f070b7b

.field public static final stylus_edit_point_wh:I = 0x7f070b7c

.field public static final stylus_edit_sec_point_ms:I = 0x7f070b7d

.field public static final stylus_edit_sec_text_ms:I = 0x7f070b7e

.field public static final stylus_edit_show_bg_height:I = 0x7f070b7f

.field public static final stylus_edit_show_bg_mb:I = 0x7f070b80

.field public static final stylus_edit_show_bg_ms:I = 0x7f070b81

.field public static final stylus_edit_show_bg_mt:I = 0x7f070b82

.field public static final stylus_edit_show_bg_width:I = 0x7f070b83

.field public static final stylus_edit_text_mt:I = 0x7f070b84

.field public static final stylus_function_arrow_left:I = 0x7f070b85

.field public static final stylus_function_arrow_width:I = 0x7f070b86

.field public static final stylus_icon_height:I = 0x7f070b87

.field public static final stylus_icon_width:I = 0x7f070b88

.field public static final stylus_item_height:I = 0x7f070b89

.field public static final stylus_key_text_sp:I = 0x7f070b8a

.field public static final stylus_margin_inner:I = 0x7f070b8b

.field public static final stylus_margin_start_inner:I = 0x7f070b8c

.field public static final stylus_not_connect_message_bottom_margin:I = 0x7f070b8d

.field public static final stylus_not_connect_message_height:I = 0x7f070b8e

.field public static final stylus_not_connect_message_start_margin:I = 0x7f070b8f

.field public static final stylus_not_connect_message_width:I = 0x7f070b90

.field public static final stylus_rotation_pen_height:I = 0x7f070b91

.field public static final stylus_rotation_pen_width:I = 0x7f070b92

.field public static final stylus_shortcut_height:I = 0x7f070b93

.field public static final stylus_summary_text_height:I = 0x7f070b94

.field public static final stylus_summary_text_mt:I = 0x7f070b95

.field public static final stylus_summary_text_size:I = 0x7f070b96

.field public static final stylus_summary_text_width:I = 0x7f070b97

.field public static final stylus_text_margin_start_inner:I = 0x7f070b98

.field public static final stylus_title_text_height:I = 0x7f070b99

.field public static final stylus_title_text_size:I = 0x7f070b9a

.field public static final stylus_title_text_width:I = 0x7f070b9b

.field public static final stylus_update_padding_hor:I = 0x7f070b9c

.field public static final stylus_update_padding_ver:I = 0x7f070b9d

.field public static final stylus_window_expand_height:I = 0x7f070b9e

.field public static final stylus_window_height:I = 0x7f070b9f

.field public static final stylus_window_indicator_height:I = 0x7f070ba0

.field public static final stylus_window_indicator_mb:I = 0x7f070ba1

.field public static final stylus_window_indicator_width:I = 0x7f070ba2

.field public static final stylus_window_shortcut_mb:I = 0x7f070ba3

.field public static final stylus_window_skip_learn_pse:I = 0x7f070ba4

.field public static final stylus_window_skip_learn_ptb:I = 0x7f070ba5

.field public static final stylus_window_video_mt:I = 0x7f070ba6

.field public static final stylus_window_welcome_mt:I = 0x7f070ba7

.field public static final stylus_window_width:I = 0x7f070ba8

.field public static final stylus_window_y:I = 0x7f070ba9

.field public static final subtitle_bottom_padding:I = 0x7f070baa

.field public static final sud_alert_dialog_button_bar_button_text_size:I = 0x7f070bab

.field public static final sud_alert_dialog_button_bar_height:I = 0x7f070bac

.field public static final sud_alert_dialog_button_bar_width:I = 0x7f070bad

.field public static final sud_alert_dialog_title_text_size:I = 0x7f070bae

.field public static final sud_alert_dialog_title_text_size_material_you:I = 0x7f070baf

.field public static final sud_card_corner_radius:I = 0x7f070bb0

.field public static final sud_card_elevation:I = 0x7f070bb1

.field public static final sud_card_land_header_text_margin_top:I = 0x7f070bb2

.field public static final sud_card_port_margin_sides:I = 0x7f070bb3

.field public static final sud_card_title_padding_bottom:I = 0x7f070bb4

.field public static final sud_card_title_padding_end:I = 0x7f070bb5

.field public static final sud_card_title_padding_start:I = 0x7f070bb6

.field public static final sud_card_title_padding_top:I = 0x7f070bb7

.field public static final sud_check_box_line_spacing_extra:I = 0x7f070bb8

.field public static final sud_check_box_margin_bottom:I = 0x7f070bb9

.field public static final sud_check_box_margin_start:I = 0x7f070bba

.field public static final sud_check_box_margin_top:I = 0x7f070bbb

.field public static final sud_check_box_padding_start:I = 0x7f070bbc

.field public static final sud_content_frame_padding_bottom:I = 0x7f070bbd

.field public static final sud_content_frame_padding_top:I = 0x7f070bbe

.field public static final sud_content_glif_margin_bottom:I = 0x7f070bbf

.field public static final sud_content_glif_margin_top:I = 0x7f070bc0

.field public static final sud_content_illustration_max_height:I = 0x7f070bc1

.field public static final sud_content_illustration_max_width:I = 0x7f070bc2

.field public static final sud_content_illustration_min_height:I = 0x7f070bc3

.field public static final sud_content_illustration_min_width:I = 0x7f070bc4

.field public static final sud_content_illustration_padding_vertical:I = 0x7f070bc5

.field public static final sud_content_info_icon_margin_end:I = 0x7f070bc6

.field public static final sud_content_info_icon_margin_end_material_you:I = 0x7f070bc7

.field public static final sud_content_info_icon_size:I = 0x7f070bc8

.field public static final sud_content_info_icon_size_material_you:I = 0x7f070bc9

.field public static final sud_content_info_line_spacing_extra:I = 0x7f070bca

.field public static final sud_content_info_line_spacing_extra_material_you:I = 0x7f070bcb

.field public static final sud_content_info_padding_bottom:I = 0x7f070bcc

.field public static final sud_content_info_padding_bottom_material_you:I = 0x7f070bcd

.field public static final sud_content_info_padding_top:I = 0x7f070bce

.field public static final sud_content_info_padding_top_material_you:I = 0x7f070bcf

.field public static final sud_content_info_text_size:I = 0x7f070bd0

.field public static final sud_content_info_text_size_material_you:I = 0x7f070bd1

.field public static final sud_content_loading_frame_padding_bottom:I = 0x7f070bd2

.field public static final sud_content_loading_frame_padding_end:I = 0x7f070bd3

.field public static final sud_content_loading_frame_padding_start:I = 0x7f070bd4

.field public static final sud_content_loading_frame_padding_top:I = 0x7f070bd5

.field public static final sud_content_text_size_material_you:I = 0x7f070bd6

.field public static final sud_decor_padding_top:I = 0x7f070bd7

.field public static final sud_description_glif_margin_bottom_lists:I = 0x7f070bd8

.field public static final sud_description_glif_margin_top:I = 0x7f070bd9

.field public static final sud_description_line_spacing_extra:I = 0x7f070bda

.field public static final sud_description_margin_bottom:I = 0x7f070bdb

.field public static final sud_description_margin_bottom_extra:I = 0x7f070bdc

.field public static final sud_description_margin_bottom_lists:I = 0x7f070bdd

.field public static final sud_description_margin_top:I = 0x7f070bde

.field public static final sud_description_margin_top_extra:I = 0x7f070bdf

.field public static final sud_description_text_size:I = 0x7f070be0

.field public static final sud_edit_text_corner_radius:I = 0x7f070be1

.field public static final sud_edit_text_min_height:I = 0x7f070be2

.field public static final sud_edit_text_padding_horizontal:I = 0x7f070be3

.field public static final sud_expand_arrow_drawable_padding:I = 0x7f070be4

.field public static final sud_footer_bar_button_radius_material_you:I = 0x7f070be5

.field public static final sud_glif_alert_dialog_corner_radius:I = 0x7f070be6

.field public static final sud_glif_button_corner_radius:I = 0x7f070be7

.field public static final sud_glif_button_margin_end:I = 0x7f070be8

.field public static final sud_glif_button_margin_start:I = 0x7f070be9

.field public static final sud_glif_button_min_height:I = 0x7f070bea

.field public static final sud_glif_button_min_height_material_you:I = 0x7f070beb

.field public static final sud_glif_button_padding:I = 0x7f070bec

.field public static final sud_glif_card_elevation:I = 0x7f070bed

.field public static final sud_glif_card_height:I = 0x7f070bee

.field public static final sud_glif_card_width:I = 0x7f070bef

.field public static final sud_glif_content_padding_top:I = 0x7f070bf0

.field public static final sud_glif_content_padding_top_material_you:I = 0x7f070bf1

.field public static final sud_glif_description_margin_bottom:I = 0x7f070bf2

.field public static final sud_glif_description_margin_bottom_material_you:I = 0x7f070bf3

.field public static final sud_glif_description_margin_top:I = 0x7f070bf4

.field public static final sud_glif_description_margin_top_material_you:I = 0x7f070bf5

.field public static final sud_glif_description_text_size_material_you:I = 0x7f070bf6

.field public static final sud_glif_device_default_dialog_corner_radius:I = 0x7f070bf7

.field public static final sud_glif_footer_bar_min_height:I = 0x7f070bf8

.field public static final sud_glif_footer_bar_min_height_material_you:I = 0x7f070bf9

.field public static final sud_glif_footer_bar_padding_end_material_you:I = 0x7f070bfa

.field public static final sud_glif_footer_bar_padding_start_material_you:I = 0x7f070bfb

.field public static final sud_glif_footer_bar_padding_vertical_material_you:I = 0x7f070bfc

.field public static final sud_glif_footer_button_text_size:I = 0x7f070bfd

.field public static final sud_glif_footer_button_text_size_material_you:I = 0x7f070bfe

.field public static final sud_glif_footer_padding_end:I = 0x7f070bff

.field public static final sud_glif_footer_padding_start:I = 0x7f070c00

.field public static final sud_glif_footer_padding_vertical:I = 0x7f070c01

.field public static final sud_glif_header_title_margin_bottom:I = 0x7f070c02

.field public static final sud_glif_header_title_margin_bottom_material_you:I = 0x7f070c03

.field public static final sud_glif_header_title_margin_top:I = 0x7f070c04

.field public static final sud_glif_header_title_margin_top_material_you:I = 0x7f070c05

.field public static final sud_glif_header_title_size_material_you:I = 0x7f070c06

.field public static final sud_glif_icon_margin_top:I = 0x7f070c07

.field public static final sud_glif_icon_margin_top_material_you:I = 0x7f070c08

.field public static final sud_glif_icon_max_height:I = 0x7f070c09

.field public static final sud_glif_icon_max_height_material_you:I = 0x7f070c0a

.field public static final sud_glif_land_content_area_weight:I = 0x7f070c0b

.field public static final sud_glif_land_header_area_weight:I = 0x7f070c0c

.field public static final sud_glif_land_middle_horizontal_spacing:I = 0x7f070c0d

.field public static final sud_glif_land_middle_horizontal_spacing_material_you:I = 0x7f070c0e

.field public static final sud_glif_margin_end:I = 0x7f070c0f

.field public static final sud_glif_margin_end_material_you:I = 0x7f070c10

.field public static final sud_glif_margin_start:I = 0x7f070c11

.field public static final sud_glif_margin_start_material_you:I = 0x7f070c12

.field public static final sud_glif_negative_button_padding:I = 0x7f070c13

.field public static final sud_glif_primary_button_button_margin_start:I = 0x7f070c14

.field public static final sud_glif_primary_button_button_margin_start_material_you:I = 0x7f070c15

.field public static final sud_glif_progress_bar_margin_vertical:I = 0x7f070c16

.field public static final sud_glif_progress_bar_padding:I = 0x7f070c17

.field public static final sud_glif_secondary_button_button_margin_start:I = 0x7f070c18

.field public static final sud_glif_v3_button_corner_radius:I = 0x7f070c19

.field public static final sud_header_container_margin_bottom:I = 0x7f070c1a

.field public static final sud_header_container_margin_bottom_material_you:I = 0x7f070c1b

.field public static final sud_header_elevation_hack:I = 0x7f070c1c

.field public static final sud_header_title_line_spacing_extra:I = 0x7f070c1d

.field public static final sud_header_title_line_spacing_extra_material_you:I = 0x7f070c1e

.field public static final sud_header_title_margin_bottom:I = 0x7f070c1f

.field public static final sud_header_title_max_size_material_you:I = 0x7f070c20

.field public static final sud_header_title_min_size_material_you:I = 0x7f070c21

.field public static final sud_header_title_padding_bottom:I = 0x7f070c22

.field public static final sud_header_title_padding_top:I = 0x7f070c23

.field public static final sud_header_title_size:I = 0x7f070c24

.field public static final sud_horizontal_icon_height:I = 0x7f070c25

.field public static final sud_icon_uniformity_elevation:I = 0x7f070c26

.field public static final sud_illustration_aspect_ratio:I = 0x7f070c27

.field public static final sud_items_glif_icon_divider_inset:I = 0x7f070c28

.field public static final sud_items_glif_text_divider_inset:I = 0x7f070c29

.field public static final sud_items_icon_container_width:I = 0x7f070c2a

.field public static final sud_items_icon_divider_inset:I = 0x7f070c2b

.field public static final sud_items_min_height_material_you:I = 0x7f070c2c

.field public static final sud_items_padding_bottom:I = 0x7f070c2d

.field public static final sud_items_padding_bottom_extra:I = 0x7f070c2e

.field public static final sud_items_padding_bottom_material_you:I = 0x7f070c2f

.field public static final sud_items_padding_top:I = 0x7f070c30

.field public static final sud_items_padding_top_material_you:I = 0x7f070c31

.field public static final sud_items_padding_vertical:I = 0x7f070c32

.field public static final sud_items_preferred_height:I = 0x7f070c33

.field public static final sud_items_summary_margin_top:I = 0x7f070c34

.field public static final sud_items_summary_margin_top_material_you:I = 0x7f070c35

.field public static final sud_items_summary_text_size:I = 0x7f070c36

.field public static final sud_items_summary_text_size_material_you:I = 0x7f070c37

.field public static final sud_items_text_divider_inset:I = 0x7f070c38

.field public static final sud_items_title_text_size:I = 0x7f070c39

.field public static final sud_items_title_text_size_material_you:I = 0x7f070c3a

.field public static final sud_items_verbose_padding_bottom_extra:I = 0x7f070c3b

.field public static final sud_items_verbose_padding_vertical:I = 0x7f070c3c

.field public static final sud_layout_margin_sides:I = 0x7f070c3d

.field public static final sud_loading_header_height:I = 0x7f070c3e

.field public static final sud_navbar_button_drawable_padding:I = 0x7f070c3f

.field public static final sud_navbar_button_padding_sides:I = 0x7f070c40

.field public static final sud_navbar_height:I = 0x7f070c41

.field public static final sud_navbar_ic_intrinsic_size:I = 0x7f070c42

.field public static final sud_navbar_padding_sides:I = 0x7f070c43

.field public static final sud_navbar_text_size:I = 0x7f070c44

.field public static final sud_progress_bar_margin_bottom:I = 0x7f070c45

.field public static final sud_progress_bar_margin_bottom_material_you:I = 0x7f070c46

.field public static final sud_progress_bar_margin_top:I = 0x7f070c47

.field public static final sud_progress_bar_margin_top_material_you:I = 0x7f070c48

.field public static final sud_progress_bar_margin_vertical:I = 0x7f070c49

.field public static final sud_radio_button_line_spacing_extra:I = 0x7f070c4a

.field public static final sud_radio_button_margin_bottom:I = 0x7f070c4b

.field public static final sud_radio_button_margin_start:I = 0x7f070c4c

.field public static final sud_radio_button_margin_top:I = 0x7f070c4d

.field public static final sud_radio_button_padding_start:I = 0x7f070c4e

.field public static final sud_switch_content_padding_end:I = 0x7f070c4f

.field public static final sud_switch_divider_height:I = 0x7f070c50

.field public static final sud_switch_divider_padding_top:I = 0x7f070c51

.field public static final sud_switch_min_width:I = 0x7f070c52

.field public static final sud_switch_padding_end:I = 0x7f070c53

.field public static final sud_switch_padding_start:I = 0x7f070c54

.field public static final sud_switch_padding_top:I = 0x7f070c55

.field public static final sud_switch_thumb_margin:I = 0x7f070c56

.field public static final sud_switch_thumb_size:I = 0x7f070c57

.field public static final sud_switch_track_height:I = 0x7f070c58

.field public static final sud_switch_track_radius:I = 0x7f070c59

.field public static final sud_switch_track_width:I = 0x7f070c5a

.field public static final sud_title_area_elevation:I = 0x7f070c5b

.field public static final suggestion_card_padding_bottom_one_card:I = 0x7f070c5c

.field public static final support_escalation_card_padding_end:I = 0x7f070c5d

.field public static final support_escalation_card_padding_start:I = 0x7f070c5e

.field public static final switchbar_margin_end:I = 0x7f070c5f

.field public static final switchbar_margin_start:I = 0x7f070c60

.field public static final switchbar_subsettings_margin_end:I = 0x7f070c61

.field public static final switchbar_subsettings_margin_start:I = 0x7f070c62

.field public static final system_app_text_view_padding_start:I = 0x7f070c63

.field public static final system_navigation_illustration_height:I = 0x7f070c64

.field public static final tab_margin_end:I = 0x7f070c65

.field public static final tab_radius:I = 0x7f070c66

.field public static final table_margin_top:I = 0x7f070c67

.field public static final tablet_preview_pref_height:I = 0x7f070c68

.field public static final tablet_provision_back_left_padding:I = 0x7f070c69

.field public static final tablet_provision_btn_height:I = 0x7f070c6a

.field public static final tablet_provision_btn_width:I = 0x7f070c6b

.field public static final tablet_provision_button_left_padding:I = 0x7f070c6c

.field public static final tablet_provision_button_right_padding:I = 0x7f070c6d

.field public static final tablet_provision_button_text_size:I = 0x7f070c6e

.field public static final tablet_provision_content_padding_horizontal:I = 0x7f070c6f

.field public static final tablet_provision_content_top_padding:I = 0x7f070c70

.field public static final tablet_provision_line_left_padding:I = 0x7f070c71

.field public static final tablet_provision_line_right_padding:I = 0x7f070c72

.field public static final tablet_provision_list_left_padding:I = 0x7f070c73

.field public static final tablet_provision_list_right_padding:I = 0x7f070c74

.field public static final tablet_provision_next_button_padding_bottom_vertical:I = 0x7f070c75

.field public static final tablet_provision_next_button_padding_top_vertical:I = 0x7f070c76

.field public static final tablet_provision_text_padding:I = 0x7f070c77

.field public static final tablet_provision_title_text_size:I = 0x7f070c78

.field public static final tablet_provision_title_top_padding:I = 0x7f070c79

.field public static final temperature_icon_size:I = 0x7f070c7a

.field public static final test_dimen:I = 0x7f070c7b

.field public static final test_mtrl_calendar_day_cornerSize:I = 0x7f070c7c

.field public static final test_navigation_bar_active_item_max_width:I = 0x7f070c7d

.field public static final test_navigation_bar_active_item_min_width:I = 0x7f070c7e

.field public static final test_navigation_bar_active_text_size:I = 0x7f070c7f

.field public static final test_navigation_bar_elevation:I = 0x7f070c80

.field public static final test_navigation_bar_height:I = 0x7f070c81

.field public static final test_navigation_bar_icon_size:I = 0x7f070c82

.field public static final test_navigation_bar_item_max_width:I = 0x7f070c83

.field public static final test_navigation_bar_item_min_width:I = 0x7f070c84

.field public static final test_navigation_bar_label_padding:I = 0x7f070c85

.field public static final test_navigation_bar_shadow_height:I = 0x7f070c86

.field public static final test_navigation_bar_text_size:I = 0x7f070c87

.field public static final tether_no_devices_text_padding_top:I = 0x7f070c88

.field public static final tether_no_devices_text_size:I = 0x7f070c89

.field public static final text_dimen_36:I = 0x7f070c8a

.field public static final text_dimen_50:I = 0x7f070c8b

.field public static final text_font_size_51:I = 0x7f070c8c

.field public static final text_max_size:I = 0x7f070c8d

.field public static final text_summary_size:I = 0x7f070c8e

.field public static final text_title_size:I = 0x7f070c8f

.field public static final thumbnail_border_round_oval:I = 0x7f070c90

.field public static final thumbnail_border_width:I = 0x7f070c91

.field public static final thumbnail_height:I = 0x7f070c92

.field public static final thumbnail_max_width:I = 0x7f070c93

.field public static final thumbnail_min_width:I = 0x7f070c94

.field public static final thumbnail_shadow_layer_radius:I = 0x7f070c95

.field public static final thumbnail_shadow_layer_x:I = 0x7f070c96

.field public static final thumbnail_shadow_layer_y:I = 0x7f070c97

.field public static final time_container_margin_top_and_start:I = 0x7f070c98

.field public static final time_label_height:I = 0x7f070c99

.field public static final time_label_margin_buttom:I = 0x7f070c9a

.field public static final time_label_margin_top:I = 0x7f070c9b

.field public static final title_layout_bottom_margin:I = 0x7f070c9c

.field public static final title_layout_padding_left:I = 0x7f070c9d

.field public static final title_layout_top_margin:I = 0x7f070c9e

.field public static final title_padding_start_end:I = 0x7f070c9f

.field public static final tooltip_corner_radius:I = 0x7f070ca0

.field public static final tooltip_horizontal_padding:I = 0x7f070ca1

.field public static final tooltip_margin:I = 0x7f070ca2

.field public static final tooltip_precise_anchor_extra_offset:I = 0x7f070ca3

.field public static final tooltip_precise_anchor_threshold:I = 0x7f070ca4

.field public static final tooltip_vertical_padding:I = 0x7f070ca5

.field public static final tooltip_y_offset_non_touch:I = 0x7f070ca6

.field public static final tooltip_y_offset_touch:I = 0x7f070ca7

.field public static final top_account_actionBar:I = 0x7f070ca8

.field public static final top_image_layout_height:I = 0x7f070ca9

.field public static final total_memory_card_text_size:I = 0x7f070caa

.field public static final traffic_mark_max_width:I = 0x7f070cab

.field public static final traffic_mark_min_width:I = 0x7f070cac

.field public static final tts_bottom_button_divider:I = 0x7f070cad

.field public static final tts_bottom_button_marginBottom:I = 0x7f070cae

.field public static final tts_bottom_button_textSize:I = 0x7f070caf

.field public static final tts_bottom_button_text_paddingTop:I = 0x7f070cb0

.field public static final turn_off_password_height:I = 0x7f070cb1

.field public static final two_target_min_width:I = 0x7f070cb2

.field public static final two_target_pref_medium_icon_size:I = 0x7f070cb3

.field public static final two_target_pref_small_icon_size:I = 0x7f070cb4

.field public static final txt_dpp_qr_code_summary_size:I = 0x7f070cb5

.field public static final txt_sharing_wifi:I = 0x7f070cb6

.field public static final udfps_lottie_translate_y:I = 0x7f070cb7

.field public static final unlock_pattern_view_diameter_factor:I = 0x7f070cb8

.field public static final update_user_photo_popup_min_width:I = 0x7f070cb9

.field public static final usage_graph_area_height:I = 0x7f070cba

.field public static final usage_graph_divider_size:I = 0x7f070cbb

.field public static final usage_graph_dot_interval:I = 0x7f070cbc

.field public static final usage_graph_dot_size:I = 0x7f070cbd

.field public static final usage_graph_labels_padding:I = 0x7f070cbe

.field public static final usage_graph_labels_width:I = 0x7f070cbf

.field public static final usage_graph_line_corner_radius:I = 0x7f070cc0

.field public static final usage_graph_line_width:I = 0x7f070cc1

.field public static final usage_graph_margin_top_bottom:I = 0x7f070cc2

.field public static final usage_number_text_size:I = 0x7f070cc3

.field public static final usage_state_app_usage_bar_view_height:I = 0x7f070cc4

.field public static final usage_state_app_usage_bar_view_margin_bottom:I = 0x7f070cc5

.field public static final usage_state_app_usage_bar_view_margin_top:I = 0x7f070cc6

.field public static final usage_state_app_usage_explain_text_size:I = 0x7f070cc7

.field public static final usage_state_app_usage_margin_top:I = 0x7f070cc8

.field public static final usage_state_app_usage_max_bar_height:I = 0x7f070cc9

.field public static final usage_state_app_usage_padding_left:I = 0x7f070cca

.field public static final usage_state_app_usage_padding_right:I = 0x7f070ccb

.field public static final usage_state_bar_width:I = 0x7f070ccc

.field public static final usage_state_channel_height:I = 0x7f070ccd

.field public static final usage_state_channel_margin:I = 0x7f070cce

.field public static final usage_state_channel_radius:I = 0x7f070ccf

.field public static final usage_state_channel_text_size:I = 0x7f070cd0

.field public static final usage_state_channel_width:I = 0x7f070cd1

.field public static final usage_state_coord_text_size:I = 0x7f070cd2

.field public static final usage_state_device_bar_height:I = 0x7f070cd3

.field public static final usage_state_device_rect_bar_height:I = 0x7f070cd4

.field public static final usage_state_device_usage_bar_view_margin_bottom:I = 0x7f070cd5

.field public static final usage_state_device_usage_explain_text_size:I = 0x7f070cd6

.field public static final usage_state_having_line:I = 0x7f070cd7

.field public static final usage_state_item_app_name_text_size:I = 0x7f070cd8

.field public static final usage_state_item_arrow_height:I = 0x7f070cd9

.field public static final usage_state_item_arrow_margin_left:I = 0x7f070cda

.field public static final usage_state_item_arrow_width:I = 0x7f070cdb

.field public static final usage_state_item_height:I = 0x7f070cdc

.field public static final usage_state_item_image_margin_right:I = 0x7f070cdd

.field public static final usage_state_item_image_wh:I = 0x7f070cde

.field public static final usage_state_item_seek_bar_height:I = 0x7f070cdf

.field public static final usage_state_item_time_text_size:I = 0x7f070ce0

.field public static final usage_state_line_text_size:I = 0x7f070ce1

.field public static final usage_state_margin:I = 0x7f070ce2

.field public static final usage_state_margin_rever:I = 0x7f070ce3

.field public static final usage_state_notification_list_margin_top:I = 0x7f070ce4

.field public static final usage_state_padding:I = 0x7f070ce5

.field public static final usage_state_rect_height:I = 0x7f070ce6

.field public static final usage_state_rect_round_radius:I = 0x7f070ce7

.field public static final usage_state_rect_width:I = 0x7f070ce8

.field public static final usage_state_remind_item_arrow_height:I = 0x7f070ce9

.field public static final usage_state_remind_item_arrow_margin_left:I = 0x7f070cea

.field public static final usage_state_remind_item_arrow_width:I = 0x7f070ceb

.field public static final usage_state_remind_item_height:I = 0x7f070cec

.field public static final usage_state_remind_item_padding_bottom:I = 0x7f070ced

.field public static final usage_state_remind_item_padding_top:I = 0x7f070cee

.field public static final usage_state_remind_title_text_size:I = 0x7f070cef

.field public static final usage_state_remind_value_text_size:I = 0x7f070cf0

.field public static final usage_state_show_tip_height:I = 0x7f070cf1

.field public static final usage_state_show_tip_title_text_size:I = 0x7f070cf2

.field public static final usage_state_show_tip_value_text_size:I = 0x7f070cf3

.field public static final usage_state_show_tip_width:I = 0x7f070cf4

.field public static final usage_state_show_tip_width2:I = 0x7f070cf5

.field public static final usage_state_text_size60:I = 0x7f070cf6

.field public static final usage_state_text_size70:I = 0x7f070cf7

.field public static final usage_state_text_size75:I = 0x7f070cf8

.field public static final usage_state_tip_left_margin:I = 0x7f070cf9

.field public static final usage_state_tip_margin:I = 0x7f070cfa

.field public static final usage_state_tip_text_margin:I = 0x7f070cfb

.field public static final usage_state_tip_title_margin_top:I = 0x7f070cfc

.field public static final usage_state_tip_value_margin_bottom:I = 0x7f070cfd

.field public static final usage_state_week_bar_width:I = 0x7f070cfe

.field public static final usage_stats_app_list_padding_end:I = 0x7f070cff

.field public static final usage_stats_app_list_padding_start:I = 0x7f070d00

.field public static final usage_stats_app_usage_list_title_margin_bottom:I = 0x7f070d01

.field public static final usage_stats_app_usage_list_title_margin_top:I = 0x7f070d02

.field public static final usage_stats_app_usage_list_title_text_size:I = 0x7f070d03

.field public static final usage_stats_detail_bar_margin_end:I = 0x7f070d04

.field public static final usage_stats_detail_bar_max_height:I = 0x7f070d05

.field public static final usage_stats_detail_bar_padding_bottom:I = 0x7f070d06

.field public static final usage_stats_detail_bar_view_height:I = 0x7f070d07

.field public static final usage_stats_detail_iv_margin_top:I = 0x7f070d08

.field public static final usage_stats_detail_iv_wh:I = 0x7f070d09

.field public static final usage_stats_detail_name_margin_top:I = 0x7f070d0a

.field public static final usage_stats_detail_name_text_size:I = 0x7f070d0b

.field public static final usage_stats_divide_height:I = 0x7f070d0c

.field public static final usage_stats_divide_line_height:I = 0x7f070d0d

.field public static final usage_stats_divide_margin_height:I = 0x7f070d0e

.field public static final usage_stats_go_more_height:I = 0x7f070d0f

.field public static final usage_stats_go_more_text_size:I = 0x7f070d10

.field public static final usage_stats_tab_bar_height:I = 0x7f070d11

.field public static final usage_stats_tab_bar_text_size:I = 0x7f070d12

.field public static final usage_stats_tab_margin:I = 0x7f070d13

.field public static final used_memory_card_text_size:I = 0x7f070d14

.field public static final user_icon_view_height:I = 0x7f070d15

.field public static final user_name_height_in_user_info_dialog:I = 0x7f070d16

.field public static final user_photo_size_in_user_info_dialog:I = 0x7f070d17

.field public static final user_spinner_height:I = 0x7f070d18

.field public static final user_spinner_item_height:I = 0x7f070d19

.field public static final user_spinner_padding:I = 0x7f070d1a

.field public static final user_spinner_padding_sides:I = 0x7f070d1b

.field public static final valuepreference_icon_height:I = 0x7f070d1c

.field public static final valuepreference_icon_padding:I = 0x7f070d1d

.field public static final valuepreference_icon_width:I = 0x7f070d1e

.field public static final valuepreference_image_padding:I = 0x7f070d1f

.field public static final valuepreference_row_height:I = 0x7f070d20

.field public static final version_card_height:I = 0x7f070d21

.field public static final version_card_img_height:I = 0x7f070d22

.field public static final version_card_img_margin_top:I = 0x7f070d23

.field public static final version_card_img_width:I = 0x7f070d24

.field public static final version_card_margin_actionbar:I = 0x7f070d25

.field public static final version_card_txt_margin_top:I = 0x7f070d26

.field public static final vertical_divider_width:I = 0x7f070d27

.field public static final video_checkbox_height:I = 0x7f070d28

.field public static final video_checkbox_width:I = 0x7f070d29

.field public static final video_height:I = 0x7f070d2a

.field public static final video_radius:I = 0x7f070d2b

.field public static final video_text_margin_bottom:I = 0x7f070d2c

.field public static final video_text_margin_start_end:I = 0x7f070d2d

.field public static final view_dimen_102:I = 0x7f070d2e

.field public static final view_dimen_105:I = 0x7f070d2f

.field public static final view_dimen_109:I = 0x7f070d30

.field public static final view_dimen_14:I = 0x7f070d31

.field public static final view_dimen_156:I = 0x7f070d32

.field public static final view_dimen_160:I = 0x7f070d33

.field public static final view_dimen_200:I = 0x7f070d34

.field public static final view_dimen_27:I = 0x7f070d35

.field public static final view_dimen_30:I = 0x7f070d36

.field public static final view_dimen_354:I = 0x7f070d37

.field public static final view_dimen_380:I = 0x7f070d38

.field public static final view_dimen_43:I = 0x7f070d39

.field public static final view_dimen_46:I = 0x7f070d3a

.field public static final view_dimen_47:I = 0x7f070d3b

.field public static final view_dimen_48:I = 0x7f070d3c

.field public static final view_dimen_54:I = 0x7f070d3d

.field public static final view_dimen_64:I = 0x7f070d3e

.field public static final view_dimen_72:I = 0x7f070d3f

.field public static final visible_vertical_space_below_password:I = 0x7f070d40

.field public static final visual_border_height:I = 0x7f070d41

.field public static final visual_border_width:I = 0x7f070d42

.field public static final visual_border_width_big:I = 0x7f070d43

.field public static final visual_box_border_radius:I = 0x7f070d44

.field public static final visual_box_borderlayout_bg_stroke_width:I = 0x7f070d45

.field public static final visual_box_margin:I = 0x7f070d46

.field public static final visual_box_margin_left_right:I = 0x7f070d47

.field public static final visual_box_reyclerview_height:I = 0x7f070d48

.field public static final visual_check_box_border:I = 0x7f070d49

.field public static final visual_check_box_border_padding:I = 0x7f070d4a

.field public static final visual_check_box_spacing:I = 0x7f070d4b

.field public static final visual_checkbox_padding:I = 0x7f070d4c

.field public static final visual_layout_margin:I = 0x7f070d4d

.field public static final visual_text_font_size:I = 0x7f070d4e

.field public static final visual_text_height:I = 0x7f070d4f

.field public static final visual_text_margin:I = 0x7f070d50

.field public static final visual_text_radius:I = 0x7f070d51

.field public static final visual_text_width:I = 0x7f070d52

.field public static final visual_text_width_big:I = 0x7f070d53

.field public static final visual_text_width_small:I = 0x7f070d54

.field public static final voice_access_preference_container_padding:I = 0x7f070d55

.field public static final volume_decription_size:I = 0x7f070d56

.field public static final volume_icon_height:I = 0x7f070d57

.field public static final volume_icon_margin_bottom:I = 0x7f070d58

.field public static final volume_icon_margin_top:I = 0x7f070d59

.field public static final volume_icon_width:I = 0x7f070d5a

.field public static final volume_seekbar_margin:I = 0x7f070d5b

.field public static final volume_seekbar_margin_bottom:I = 0x7f070d5c

.field public static final volume_seekbar_padding:I = 0x7f070d5d

.field public static final volume_seekbar_side_margin:I = 0x7f070d5e

.field public static final volumes_settings_panel_height:I = 0x7f070d5f

.field public static final volumn_horizontal_edge:I = 0x7f070d60

.field public static final vpn_input_margin_top:I = 0x7f070d61

.field public static final vpn_type_margin_right:I = 0x7f070d62

.field public static final water_box_padding_top:I = 0x7f070d63

.field public static final widget_frame_end_margin:I = 0x7f070d64

.field public static final wifi_24G_icon_width:I = 0x7f070d65

.field public static final wifi_add_network_security_margin_top:I = 0x7f070d66

.field public static final wifi_add_network_spinner_title_margin_end:I = 0x7f070d67

.field public static final wifi_add_network_title_size:I = 0x7f070d68

.field public static final wifi_ap_band_checkbox_padding:I = 0x7f070d69

.field public static final wifi_ap_dialog_spinner_title_margin_end:I = 0x7f070d6a

.field public static final wifi_ap_dialog_spinner_tv_margin_start:I = 0x7f070d6b

.field public static final wifi_assistant_checkbox_top_margin:I = 0x7f070d6c

.field public static final wifi_assistant_description_margin:I = 0x7f070d6d

.field public static final wifi_assistant_height:I = 0x7f070d6e

.field public static final wifi_assistant_horizontal_margin:I = 0x7f070d6f

.field public static final wifi_assistant_image_start:I = 0x7f070d70

.field public static final wifi_assistant_image_top:I = 0x7f070d71

.field public static final wifi_assistant_padding:I = 0x7f070d72

.field public static final wifi_assistant_padding_start_end:I = 0x7f070d73

.field public static final wifi_assistant_padding_top_bottom:I = 0x7f070d74

.field public static final wifi_assistant_text_padding:I = 0x7f070d75

.field public static final wifi_assistant_top_margin:I = 0x7f070d76

.field public static final wifi_auto_connect_margin:I = 0x7f070d77

.field public static final wifi_btn_refresh_size:I = 0x7f070d78

.field public static final wifi_ca_cert_spinner_start_margin:I = 0x7f070d79

.field public static final wifi_custom_preference_title_margin:I = 0x7f070d7a

.field public static final wifi_custom_preference_title_size:I = 0x7f070d7b

.field public static final wifi_custom_widget_background_width:I = 0x7f070d7c

.field public static final wifi_detail_bottom_ll_margin_top:I = 0x7f070d7d

.field public static final wifi_detail_bottom_text_margin_top:I = 0x7f070d7e

.field public static final wifi_detail_bottom_text_size:I = 0x7f070d7f

.field public static final wifi_detail_grid__summary_bottom:I = 0x7f070d80

.field public static final wifi_detail_grid__summary_size:I = 0x7f070d81

.field public static final wifi_detail_grid__summary_top:I = 0x7f070d82

.field public static final wifi_detail_grid__title_size:I = 0x7f070d83

.field public static final wifi_detail_grid__title_top:I = 0x7f070d84

.field public static final wifi_detail_grid_height:I = 0x7f070d85

.field public static final wifi_detail_grid_horspace:I = 0x7f070d86

.field public static final wifi_detail_grid_padding:I = 0x7f070d87

.field public static final wifi_detail_grid_top:I = 0x7f070d88

.field public static final wifi_detail_page_header_image_size:I = 0x7f070d89

.field public static final wifi_detail_spinner_title_margin_end:I = 0x7f070d8a

.field public static final wifi_dialog_padding_bottom:I = 0x7f070d8b

.field public static final wifi_direct_connection:I = 0x7f070d8c

.field public static final wifi_divider_height:I = 0x7f070d8d

.field public static final wifi_dpp_fragment_icon_width_height:I = 0x7f070d8e

.field public static final wifi_edittexts_between_margin:I = 0x7f070d8f

.field public static final wifi_icon_size:I = 0x7f070d90

.field public static final wifi_marginEnd:I = 0x7f070d91

.field public static final wifi_nearby_padding__end:I = 0x7f070d92

.field public static final wifi_nearby_padding_start:I = 0x7f070d93

.field public static final wifi_nearby_padding_top:I = 0x7f070d94

.field public static final wifi_preference_badge_padding:I = 0x7f070d95

.field public static final wifi_preference_item_padding_side:I = 0x7f070d96

.field public static final wifi_privacy_tv_margin_start:I = 0x7f070d97

.field public static final wifi_refresh_end_margin:I = 0x7f070d98

.field public static final wifi_saved_preference_margin:I = 0x7f070d99

.field public static final wifi_settings_panel_height:I = 0x7f070d9a

.field public static final wifi_spinner_end_margin:I = 0x7f070d9b

.field public static final wifi_spinner_height:I = 0x7f070d9c

.field public static final wifi_spinner_margin_top:I = 0x7f070d9d

.field public static final wifi_spinner_silde_margin:I = 0x7f070d9e

.field public static final wifi_spinner_start_margin:I = 0x7f070d9f

.field public static final wifi_spinner_title_margin:I = 0x7f070da0

.field public static final wifi_spinner_title_margin_constant:I = 0x7f070da1

.field public static final wifi_spinner_title_margin_end:I = 0x7f070da2

.field public static final wifi_spinner_top_margin:I = 0x7f070da3

.field public static final wifi_spinner_tv_margin:I = 0x7f070da4

.field public static final wifi_spinner_tv_margin_start:I = 0x7f070da5

.field public static final wifi_state_edit_left_right_margin:I = 0x7f070da6

.field public static final wifi_state_edittext_margin:I = 0x7f070da7

.field public static final wifi_title_compound_padding:I = 0x7f070da8

.field public static final word_photo_border_size:I = 0x7f070da9

.field public static final word_photo_size:I = 0x7f070daa

.field public static final zen_conversations_icon_offset:I = 0x7f070dab

.field public static final zen_conversations_icon_size:I = 0x7f070dac

.field public static final zen_conversations_image_margin_vertical:I = 0x7f070dad

.field public static final zen_mode_condition_detail_bottom_padding:I = 0x7f070dae

.field public static final zen_mode_condition_detail_button_padding:I = 0x7f070daf

.field public static final zen_mode_condition_detail_item_interline_spacing:I = 0x7f070db0

.field public static final zen_mode_condition_detail_item_spacing:I = 0x7f070db1

.field public static final zen_mode_icon_margin:I = 0x7f070db2

.field public static final zen_mode_icon_size:I = 0x7f070db3

.field public static final zen_mode_settings_button_margin_vertical:I = 0x7f070db4

.field public static final zen_schedule_day_margin:I = 0x7f070db5

.field public static final zen_schedule_rule_checkbox_padding:I = 0x7f070db6

.field public static final zen_senders_image_margin_vertical:I = 0x7f070db7


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
