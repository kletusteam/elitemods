.class public final Lmiui/settings/splitlib/R$array;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/settings/splitlib/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "array"
.end annotation


# static fields
.field public static final BLE_MMA_FEATURE_VERSION:I = 0x7f030000

.field public static final IS_SUPPORT_SPLIT:I = 0x7f030001

.field public static final LC3_CG_FEATURE_VERSION:I = 0x7f030002

.field public static final TIME_STAMP:I = 0x7f030003

.field public static final accessibility_autoclick_control_selector_keys:I = 0x7f030004

.field public static final accessibility_autoclick_selector_values:I = 0x7f030005

.field public static final accessibility_button_gesture_selector_titles:I = 0x7f030006

.field public static final accessibility_button_gesture_selector_values:I = 0x7f030007

.field public static final accessibility_button_location_selector_titles:I = 0x7f030008

.field public static final accessibility_button_location_selector_values:I = 0x7f030009

.field public static final accessibility_button_size_selector_titles:I = 0x7f03000a

.field public static final accessibility_button_size_selector_values:I = 0x7f03000b

.field public static final accessibility_timeout_control_selector_keys:I = 0x7f03000c

.field public static final accessibility_timeout_selector_values:I = 0x7f03000d

.field public static final accessibility_timeout_summaries:I = 0x7f03000e

.field public static final add_wifi_security:I = 0x7f03000f

.field public static final add_wifi_security_with_sae:I = 0x7f030010

.field public static final agps_apn_entries:I = 0x7f030011

.field public static final agps_apn_values:I = 0x7f030012

.field public static final agps_network_entries:I = 0x7f030013

.field public static final agps_network_values:I = 0x7f030014

.field public static final agps_reset_type_entries:I = 0x7f030015

.field public static final agps_reset_type_values:I = 0x7f030016

.field public static final agps_si_mode_entries:I = 0x7f030017

.field public static final agps_si_mode_values:I = 0x7f030018

.field public static final ai_item_action_array:I = 0x7f030019

.field public static final ai_key_item_name:I = 0x7f03001a

.field public static final alarm_repeat_type:I = 0x7f03001b

.field public static final alarm_repeat_type_no_workdays:I = 0x7f03001c

.field public static final alarm_repeat_type_no_workdays_values:I = 0x7f03001d

.field public static final alarm_repeat_type_values:I = 0x7f03001e

.field public static final allowlist_hide_summary_in_battery_usage:I = 0x7f03001f

.field public static final alphabet_sub_table_with_starred:I = 0x7f030020

.field public static final alphabet_table:I = 0x7f030021

.field public static final alphabet_table_with_starred:I = 0x7f030022

.field public static final always_enabled_app_list:I = 0x7f030023

.field public static final am_pms:I = 0x7f030024

.field public static final animator_duration_scale_entries:I = 0x7f030025

.field public static final animator_duration_scale_values:I = 0x7f030026

.field public static final aod_notification_status_entries:I = 0x7f030027

.field public static final aod_show_style_values:I = 0x7f030028

.field public static final ap_identify:I = 0x7f030029

.field public static final apn_auth_entries:I = 0x7f03002a

.field public static final apn_auth_values:I = 0x7f03002b

.field public static final apn_not_allowed_entries:I = 0x7f03002c

.field public static final apn_protocol_entries:I = 0x7f03002d

.field public static final apn_protocol_values:I = 0x7f03002e

.field public static final app_install_location_entries:I = 0x7f03002f

.field public static final app_install_location_values:I = 0x7f030030

.field public static final app_ops_categories:I = 0x7f030031

.field public static final app_ops_labels:I = 0x7f030032

.field public static final app_ops_summaries:I = 0x7f030033

.field public static final app_process_limit_entries:I = 0x7f030034

.field public static final app_process_limit_values:I = 0x7f030035

.field public static final audio_mode_entries:I = 0x7f030036

.field public static final audio_mode_values:I = 0x7f030037

.field public static final autofill_logging_level_entries:I = 0x7f030038

.field public static final autofill_logging_level_values:I = 0x7f030039

.field public static final automatic_storage_management_days:I = 0x7f03003a

.field public static final automatic_storage_management_days_values:I = 0x7f03003b

.field public static final avatar_image_descriptions:I = 0x7f03003c

.field public static final avatar_images:I = 0x7f03003d

.field public static final ba_disable_timeout:I = 0x7f03003e

.field public static final ba_disable_timeout_value:I = 0x7f03003f

.field public static final ba_security_settings_entries:I = 0x7f030040

.field public static final ba_security_settings_summaries:I = 0x7f030041

.field public static final ba_security_settings_values:I = 0x7f030042

.field public static final background_style1:I = 0x7f030043

.field public static final background_style2:I = 0x7f030044

.field public static final background_style3:I = 0x7f030045

.field public static final background_style4:I = 0x7f030046

.field public static final battery_indicator_style_entries:I = 0x7f030047

.field public static final battery_indicator_style_icons:I = 0x7f030048

.field public static final battery_indicator_style_values:I = 0x7f030049

.field public static final battery_labels:I = 0x7f03004a

.field public static final battery_saver_trigger_values:I = 0x7f03004b

.field public static final batterymeter_bolt_points:I = 0x7f03004c

.field public static final batterymeter_color_levels:I = 0x7f03004d

.field public static final batterymeter_color_values:I = 0x7f03004e

.field public static final batterymeter_plus_points:I = 0x7f03004f

.field public static final bcast_channel_selection:I = 0x7f030050

.field public static final bearer_entries:I = 0x7f030051

.field public static final bearer_values:I = 0x7f030052

.field public static final bluetooth_a2dp_codec_aptxadaptive_mode_summaries:I = 0x7f030053

.field public static final bluetooth_a2dp_codec_aptxadaptive_mode_titles:I = 0x7f030054

.field public static final bluetooth_a2dp_codec_aptxadaptive_mode_values:I = 0x7f030055

.field public static final bluetooth_a2dp_codec_bits_per_sample_summaries:I = 0x7f030056

.field public static final bluetooth_a2dp_codec_bits_per_sample_titles:I = 0x7f030057

.field public static final bluetooth_a2dp_codec_bits_per_sample_values:I = 0x7f030058

.field public static final bluetooth_a2dp_codec_channel_mode_summaries:I = 0x7f030059

.field public static final bluetooth_a2dp_codec_channel_mode_titles:I = 0x7f03005a

.field public static final bluetooth_a2dp_codec_channel_mode_values:I = 0x7f03005b

.field public static final bluetooth_a2dp_codec_ldac_playback_quality_summaries:I = 0x7f03005c

.field public static final bluetooth_a2dp_codec_ldac_playback_quality_titles:I = 0x7f03005d

.field public static final bluetooth_a2dp_codec_ldac_playback_quality_values:I = 0x7f03005e

.field public static final bluetooth_a2dp_codec_lhdc_latency_summaries:I = 0x7f03005f

.field public static final bluetooth_a2dp_codec_lhdc_latency_titles:I = 0x7f030060

.field public static final bluetooth_a2dp_codec_lhdc_latency_values:I = 0x7f030061

.field public static final bluetooth_a2dp_codec_lhdc_playback_quality_summaries:I = 0x7f030062

.field public static final bluetooth_a2dp_codec_lhdc_playback_quality_titles:I = 0x7f030063

.field public static final bluetooth_a2dp_codec_lhdc_playback_quality_values:I = 0x7f030064

.field public static final bluetooth_a2dp_codec_sample_rate_summaries:I = 0x7f030065

.field public static final bluetooth_a2dp_codec_sample_rate_titles:I = 0x7f030066

.field public static final bluetooth_a2dp_codec_sample_rate_values:I = 0x7f030067

.field public static final bluetooth_a2dp_codec_summaries:I = 0x7f030068

.field public static final bluetooth_a2dp_codec_titles:I = 0x7f030069

.field public static final bluetooth_a2dp_codec_values:I = 0x7f03006a

.field public static final bluetooth_audio_active_device_summaries:I = 0x7f03006b

.field public static final bluetooth_avrcp_version_values:I = 0x7f03006c

.field public static final bluetooth_avrcp_versions:I = 0x7f03006d

.field public static final bluetooth_broadcast_pin_config_titles:I = 0x7f03006e

.field public static final bluetooth_broadcast_pin_config_values:I = 0x7f03006f

.field public static final bluetooth_map_version_values:I = 0x7f030070

.field public static final bluetooth_map_versions:I = 0x7f030071

.field public static final bluetooth_max_connected_audio_devices:I = 0x7f030072

.field public static final bluetooth_max_connected_audio_devices_values:I = 0x7f030073

.field public static final bluetooth_visibility_timeout_entries:I = 0x7f030074

.field public static final broadcast_locales:I = 0x7f030075

.field public static final bt_hci_snoop_log_entries:I = 0x7f030076

.field public static final bt_hci_snoop_log_values:I = 0x7f030077

.field public static final bt_hci_snoop_log_values_enhanced:I = 0x7f030078

.field public static final bt_icon_bg_colors:I = 0x7f030079

.field public static final bt_icon_fg_colors:I = 0x7f03007a

.field public static final button_light_timeout_entries:I = 0x7f03007b

.field public static final button_light_timeout_value:I = 0x7f03007c

.field public static final bytes_picker_sizes:I = 0x7f03007d

.field public static final cached_apps_freezer_entries:I = 0x7f03007e

.field public static final cached_apps_freezer_values:I = 0x7f03007f

.field public static final captioning_color_selector_titles:I = 0x7f030080

.field public static final captioning_color_selector_values:I = 0x7f030081

.field public static final captioning_edge_type_selector_titles:I = 0x7f030082

.field public static final captioning_edge_type_selector_values:I = 0x7f030083

.field public static final captioning_font_size_selector_summaries:I = 0x7f030084

.field public static final captioning_font_size_selector_titles:I = 0x7f030085

.field public static final captioning_font_size_selector_values:I = 0x7f030086

.field public static final captioning_opacity_selector_titles:I = 0x7f030087

.field public static final captioning_opacity_selector_values:I = 0x7f030088

.field public static final captioning_preset_selector_titles:I = 0x7f030089

.field public static final captioning_preset_selector_values:I = 0x7f03008a

.field public static final captioning_typeface_selector_titles:I = 0x7f03008b

.field public static final captioning_typeface_selector_values:I = 0x7f03008c

.field public static final cdma_subscription_choices:I = 0x7f03008d

.field public static final cdma_subscription_values:I = 0x7f03008e

.field public static final cdma_system_select_choices:I = 0x7f03008f

.field public static final cdma_system_select_values:I = 0x7f030090

.field public static final change_security_lock_picker:I = 0x7f030091

.field public static final change_security_lock_picker_key:I = 0x7f030092

.field public static final chinese_days:I = 0x7f030093

.field public static final chinese_digits:I = 0x7f030094

.field public static final chinese_leap_months:I = 0x7f030095

.field public static final chinese_months:I = 0x7f030096

.field public static final chinese_symbol_animals:I = 0x7f030097

.field public static final choose_dns_mode:I = 0x7f030098

.field public static final city_name:I = 0x7f030099

.field public static final city_timezone:I = 0x7f03009a

.field public static final click_bottom:I = 0x7f03009b

.field public static final code_change_entries:I = 0x7f03009c

.field public static final code_change_values:I = 0x7f03009d

.field public static final color_game_led_freq_values:I = 0x7f03009e

.field public static final color_mode_descriptions:I = 0x7f03009f

.field public static final color_mode_ids:I = 0x7f0300a0

.field public static final color_mode_names:I = 0x7f0300a1

.field public static final color_picker:I = 0x7f0300a2

.field public static final common_password_business_clickable_default:I = 0x7f0300a3

.field public static final common_password_business_keys:I = 0x7f0300a4

.field public static final common_password_business_names:I = 0x7f0300a5

.field public static final config_color_mode_options_strings:I = 0x7f0300a6

.field public static final config_color_mode_options_values:I = 0x7f0300a7

.field public static final config_device_lock_need:I = 0x7f0300a8

.field public static final config_disallowed_app_localeChange_packages:I = 0x7f0300a9

.field public static final config_downloaded_services:I = 0x7f0300aa

.field public static final config_order_audio_services:I = 0x7f0300ab

.field public static final config_order_captions_services:I = 0x7f0300ac

.field public static final config_order_display_services:I = 0x7f0300ad

.field public static final config_order_interaction_control_services:I = 0x7f0300ae

.field public static final config_order_screen_reader_services:I = 0x7f0300af

.field public static final config_panel_keep_observe_uri:I = 0x7f0300b0

.field public static final config_preinstalled_audio_services:I = 0x7f0300b1

.field public static final config_preinstalled_captions_services:I = 0x7f0300b2

.field public static final config_preinstalled_display_services:I = 0x7f0300b3

.field public static final config_preinstalled_interaction_control_services:I = 0x7f0300b4

.field public static final config_preinstalled_screen_reader_services:I = 0x7f0300b5

.field public static final config_screen_resolution_options_strings:I = 0x7f0300b6

.field public static final config_screen_resolution_summaries_strings:I = 0x7f0300b7

.field public static final config_settableAutoRotationDeviceStatesDescriptions:I = 0x7f0300b8

.field public static final config_settings_slices_accessibility_components:I = 0x7f0300b9

.field public static final config_skip_reset_apps_package_name:I = 0x7f0300ba

.field public static final config_suppress_injected_tile_keys:I = 0x7f0300bb

.field public static final connect_mode_entries:I = 0x7f0300bc

.field public static final connect_mode_values:I = 0x7f0300bd

.field public static final coolsound_area_mash_up:I = 0x7f0300be

.field public static final cutout_mode_entries:I = 0x7f0300bf

.field public static final daltonizer_mode_keys:I = 0x7f0300c0

.field public static final daltonizer_mode_summaries:I = 0x7f0300c1

.field public static final daltonizer_type_values:I = 0x7f0300c2

.field public static final dark_ui_mode_entries:I = 0x7f0300c3

.field public static final dark_ui_mode_values:I = 0x7f0300c4

.field public static final dark_ui_scheduler_preference_titles:I = 0x7f0300c5

.field public static final dark_ui_scheduler_with_bedtime_preference_titles:I = 0x7f0300c6

.field public static final data_network_type_name_default:I = 0x7f0300c7

.field public static final data_type_name_cus_reg_key:I = 0x7f0300c8

.field public static final data_type_name_cus_reg_value:I = 0x7f0300c9

.field public static final data_type_name_mcc_key:I = 0x7f0300ca

.field public static final data_type_name_mcc_mnc_key:I = 0x7f0300cb

.field public static final data_type_name_mcc_mnc_value:I = 0x7f0300cc

.field public static final data_type_name_mcc_value:I = 0x7f0300cd

.field public static final data_type_name_miui_mcc_key:I = 0x7f0300ce

.field public static final data_type_name_miui_mcc_value:I = 0x7f0300cf

.field public static final data_usage_data_range:I = 0x7f0300d0

.field public static final date_of_app_all:I = 0x7f0300d1

.field public static final date_of_traffic:I = 0x7f0300d2

.field public static final date_of_traffic_prefix:I = 0x7f0300d3

.field public static final debug_hw_overdraw_entries:I = 0x7f0300d4

.field public static final debug_hw_overdraw_values:I = 0x7f0300d5

.field public static final defalut_browser_package:I = 0x7f0300d6

.field public static final default_camera_package:I = 0x7f0300d7

.field public static final default_dialer_package:I = 0x7f0300d8

.field public static final default_email_package:I = 0x7f0300d9

.field public static final default_gallery_package:I = 0x7f0300da

.field public static final default_mms_package:I = 0x7f0300db

.field public static final default_music_package:I = 0x7f0300dc

.field public static final default_video_package:I = 0x7f0300dd

.field public static final detailed_am_pms:I = 0x7f0300de

.field public static final device_support_pickup_by_MTK:I = 0x7f0300df

.field public static final disable_apptitmer_device_list:I = 0x7f0300e0

.field public static final dndm_auto_time_trun_on_off:I = 0x7f0300e1

.field public static final dndm_vip_list_group_array:I = 0x7f0300e2

.field public static final double_click_mode_entries:I = 0x7f0300e3

.field public static final double_key_mode_values:I = 0x7f0300e4

.field public static final double_tone_entries:I = 0x7f0300e5

.field public static final double_tone_entries_left_TWS01:I = 0x7f0300e6

.field public static final double_tone_entries_right_TWS01:I = 0x7f0300e7

.field public static final double_tone_entry_values:I = 0x7f0300e8

.field public static final double_tone_entry_values_TWS01:I = 0x7f0300e9

.field public static final dream_timeout_entries:I = 0x7f0300ea

.field public static final dream_timeout_values:I = 0x7f0300eb

.field public static final eap_method_without_sim_auth:I = 0x7f0300ec

.field public static final eap_ocsp_type:I = 0x7f0300ed

.field public static final earthly_branches:I = 0x7f0300ee

.field public static final enable_opengl_traces_entries:I = 0x7f0300ef

.field public static final enable_opengl_traces_values:I = 0x7f0300f0

.field public static final enabled_networks_4g_choices:I = 0x7f0300f1

.field public static final enabled_networks_cdma_choices:I = 0x7f0300f2

.field public static final enabled_networks_cdma_no_lte_choices:I = 0x7f0300f3

.field public static final enabled_networks_cdma_no_lte_values:I = 0x7f0300f4

.field public static final enabled_networks_cdma_only_lte_choices:I = 0x7f0300f5

.field public static final enabled_networks_cdma_only_lte_values:I = 0x7f0300f6

.field public static final enabled_networks_cdma_values:I = 0x7f0300f7

.field public static final enabled_networks_choices:I = 0x7f0300f8

.field public static final enabled_networks_except_gsm_4g_choices:I = 0x7f0300f9

.field public static final enabled_networks_except_gsm_choices:I = 0x7f0300fa

.field public static final enabled_networks_except_gsm_lte_choices:I = 0x7f0300fb

.field public static final enabled_networks_except_gsm_lte_values:I = 0x7f0300fc

.field public static final enabled_networks_except_gsm_values:I = 0x7f0300fd

.field public static final enabled_networks_except_lte_choices:I = 0x7f0300fe

.field public static final enabled_networks_except_lte_values:I = 0x7f0300ff

.field public static final enabled_networks_gsm_wcdma_choices:I = 0x7f030100

.field public static final enabled_networks_gsm_wcdma_values:I = 0x7f030101

.field public static final enabled_networks_subsidy_locked_choices:I = 0x7f030102

.field public static final enabled_networks_subsidy_locked_values:I = 0x7f030103

.field public static final enabled_networks_tdscdma_choices:I = 0x7f030104

.field public static final enabled_networks_tdscdma_values:I = 0x7f030105

.field public static final enabled_networks_values:I = 0x7f030106

.field public static final enhanced_4g_lte_mode_sumary_variant:I = 0x7f030107

.field public static final enhanced_4g_lte_mode_title_variant:I = 0x7f030108

.field public static final entries_color_calibration:I = 0x7f030109

.field public static final entries_font_size:I = 0x7f03010a

.field public static final entryvalues_font_size:I = 0x7f03010b

.field public static final eras:I = 0x7f03010c

.field public static final fingerprint_unlock_type_entries:I = 0x7f03010d

.field public static final fingerprint_unlock_type_values:I = 0x7f03010e

.field public static final fluency_mode_confirms:I = 0x7f03010f

.field public static final fluency_mode_entries:I = 0x7f030110

.field public static final fluency_mode_summaries:I = 0x7f030111

.field public static final font_size_preference_preview_size:I = 0x7f030112

.field public static final font_size_title:I = 0x7f030113

.field public static final fps_list_entries:I = 0x7f030114

.field public static final fps_list_values:I = 0x7f030115

.field public static final gesture_prevent_ringing_entries:I = 0x7f030116

.field public static final gesture_prevent_ringing_values:I = 0x7f030117

.field public static final gradient_colors:I = 0x7f030118

.field public static final gradient_positions:I = 0x7f030119

.field public static final graphics_driver_all_apps_preference_values:I = 0x7f03011a

.field public static final graphics_driver_app_preference_values:I = 0x7f03011b

.field public static final handy_mode_size_text_entries:I = 0x7f03011c

.field public static final handy_mode_size_value_entries:I = 0x7f03011d

.field public static final haptic_feedback_level_texts:I = 0x7f03011e

.field public static final haptic_feedback_level_values:I = 0x7f03011f

.field public static final hdcp_checking_summaries:I = 0x7f030120

.field public static final hdcp_checking_titles:I = 0x7f030121

.field public static final hdcp_checking_values:I = 0x7f030122

.field public static final heavenly_stems:I = 0x7f030123

.field public static final hide_account_list:I = 0x7f030124

.field public static final ingress_rate_limit_entries:I = 0x7f030125

.field public static final ingress_rate_limit_values:I = 0x7f030126

.field public static final input_method_function_des:I = 0x7f030127

.field public static final input_method_function_icons:I = 0x7f030128

.field public static final input_method_function_title:I = 0x7f030129

.field public static final input_method_middle_function_des:I = 0x7f03012a

.field public static final input_method_middle_function_title:I = 0x7f03012b

.field public static final input_method_selector_titles:I = 0x7f03012c

.field public static final input_method_selector_values:I = 0x7f03012d

.field public static final inputdebuglog_select_level_entries:I = 0x7f03012e

.field public static final inputdebuglog_select_level_entryValues:I = 0x7f03012f

.field public static final key_and_gesture_shortcut_action:I = 0x7f030130

.field public static final key_popup_voice_choice:I = 0x7f030131

.field public static final key_slider_voice_choice:I = 0x7f030132

.field public static final keyguard_notification_status_entries:I = 0x7f030133

.field public static final keyguard_notification_status_values:I = 0x7f030134

.field public static final kg_wrong_puk_code_message_list:I = 0x7f030135

.field public static final led_color_entries:I = 0x7f030136

.field public static final led_color_values:I = 0x7f030137

.field public static final led_freq_entries:I = 0x7f030138

.field public static final led_freq_values:I = 0x7f030139

.field public static final left_tone_entries:I = 0x7f03013a

.field public static final left_tone_entry_values:I = 0x7f03013b

.field public static final locale_carrier_names:I = 0x7f03013c

.field public static final lock_after_timeout_entries:I = 0x7f03013d

.field public static final lock_after_timeout_values:I = 0x7f03013e

.field public static final lock_screen_secure_after_timeout_entries:I = 0x7f03013f

.field public static final lock_screen_secure_after_timeout_values:I = 0x7f030140

.field public static final long_press:I = 0x7f030141

.field public static final long_press_entries_left_TWS_K77s:I = 0x7f030142

.field public static final long_press_entries_right_TWS_K77s:I = 0x7f030143

.field public static final long_press_timeout_selector_list_titles:I = 0x7f030144

.field public static final long_press_timeout_selector_titles:I = 0x7f030145

.field public static final long_press_timeout_selector_values:I = 0x7f030146

.field public static final long_press_volume_down_action:I = 0x7f030147

.field public static final long_press_volume_down_action_value:I = 0x7f030148

.field public static final magnification_mode_summaries:I = 0x7f030149

.field public static final magnification_mode_values:I = 0x7f03014a

.field public static final max_number_titles:I = 0x7f03014b

.field public static final max_number_values:I = 0x7f03014c

.field public static final media_feedback_level_entries:I = 0x7f03014d

.field public static final media_feedback_level_values:I = 0x7f03014e

.field public static final mi1_led_color_entries:I = 0x7f03014f

.field public static final mi1_led_color_values:I = 0x7f030150

.field public static final mi2_wifi_security:I = 0x7f030151

.field public static final miui_entries_font_size:I = 0x7f030152

.field public static final miui_entryvalues_font_size:I = 0x7f030153

.field public static final modem_perm_action_items:I = 0x7f030154

.field public static final months:I = 0x7f030155

.field public static final months_short:I = 0x7f030156

.field public static final months_shortest:I = 0x7f030157

.field public static final msd_pc_system:I = 0x7f030158

.field public static final msd_pc_system_download_summary:I = 0x7f030159

.field public static final msd_pc_system_install_finish_summary:I = 0x7f03015a

.field public static final msd_pc_system_install_summary:I = 0x7f03015b

.field public static final msd_pc_system_retry_linux:I = 0x7f03015c

.field public static final msd_pc_system_retry_mac:I = 0x7f03015d

.field public static final msd_pc_system_retry_vista:I = 0x7f03015e

.field public static final msd_pc_system_retry_xp:I = 0x7f03015f

.field public static final mvno_type_entries:I = 0x7f030160

.field public static final mvno_type_values:I = 0x7f030161

.field public static final ncc_locales:I = 0x7f030162

.field public static final network_speed_slow_summary:I = 0x7f030163

.field public static final new_dndm_vip_list_group_array:I = 0x7f030164

.field public static final nfc_payment_favor:I = 0x7f030165

.field public static final nfc_payment_favor_values:I = 0x7f030166

.field public static final nfc_se_route_values:I = 0x7f030167

.field public static final notch_battery_indicator_style_entries:I = 0x7f030168

.field public static final notch_battery_indicator_style_icons:I = 0x7f030169

.field public static final notch_battery_indicator_style_values:I = 0x7f03016a

.field public static final notif_types_titles:I = 0x7f03016b

.field public static final notif_types_values:I = 0x7f03016c

.field public static final notifi_importance_entries:I = 0x7f03016d

.field public static final notifi_importance_summaries:I = 0x7f03016e

.field public static final notifi_importance_values:I = 0x7f03016f

.field public static final notification_shade_shortcut_entries:I = 0x7f030170

.field public static final notification_shade_shortcut_values:I = 0x7f030171

.field public static final notification_style_entries:I = 0x7f030172

.field public static final notification_style_values:I = 0x7f030173

.field public static final one_handed_timeout_values:I = 0x7f030174

.field public static final organized_locales:I = 0x7f030175

.field public static final origin_carrier_names:I = 0x7f030176

.field public static final overlay_display_devices_entries:I = 0x7f030177

.field public static final overlay_display_devices_values:I = 0x7f030178

.field public static final page_layout_margins_godzilla:I = 0x7f030179

.field public static final page_layout_margins_godzilla_global:I = 0x7f03017a

.field public static final page_layout_margins_huge:I = 0x7f03017b

.field public static final page_layout_margins_huge_global:I = 0x7f03017c

.field public static final page_layout_margins_large:I = 0x7f03017d

.field public static final page_layout_margins_large_global:I = 0x7f03017e

.field public static final page_layout_margins_medium:I = 0x7f03017f

.field public static final page_layout_margins_medium_global:I = 0x7f030180

.field public static final page_layout_margins_normal:I = 0x7f030181

.field public static final page_layout_margins_normal_global:I = 0x7f030182

.field public static final page_layout_margins_small:I = 0x7f030183

.field public static final page_layout_margins_small_global:I = 0x7f030184

.field public static final paper_color_index:I = 0x7f030185

.field public static final paper_color_values:I = 0x7f030186

.field public static final perm_hips_action:I = 0x7f030187

.field public static final permission_risk_privacy:I = 0x7f030188

.field public static final permission_risk_security:I = 0x7f030189

.field public static final perms_hips_values:I = 0x7f03018a

.field public static final phone_call_noise_suppression_title:I = 0x7f03018b

.field public static final phone_call_noise_suppression_values:I = 0x7f03018c

.field public static final poco_device_list:I = 0x7f03018d

.field public static final power_click:I = 0x7f03018e

.field public static final power_double_click:I = 0x7f03018f

.field public static final power_long_press:I = 0x7f030190

.field public static final power_mode_entries:I = 0x7f030191

.field public static final preferred_app_entries:I = 0x7f030192

.field public static final preferred_network_mode_choices:I = 0x7f030193

.field public static final preferred_network_mode_choices_world_mode:I = 0x7f030194

.field public static final preferred_network_mode_gsm_wcdma_choices:I = 0x7f030195

.field public static final preferred_network_mode_gsm_wcdma_values:I = 0x7f030196

.field public static final preferred_network_mode_values:I = 0x7f030197

.field public static final preferred_network_mode_values_world_mode:I = 0x7f030198

.field public static final press_key_entries:I = 0x7f030199

.field public static final press_key_entry_values:I = 0x7f03019a

.field public static final priority_storage_entries:I = 0x7f03019b

.field public static final priority_storage_value:I = 0x7f03019c

.field public static final privacy_password_function_specfication_keys:I = 0x7f03019d

.field public static final privacy_password_function_specfication_names:I = 0x7f03019e

.field public static final proc_stats_memory_states:I = 0x7f03019f

.field public static final proc_stats_process_states:I = 0x7f0301a0

.field public static final ram_states:I = 0x7f0301a1

.field public static final remove_oldman_mode_device_list:I = 0x7f0301a2

.field public static final right_tone_entries:I = 0x7f0301a3

.field public static final right_tone_entry_values:I = 0x7f0301a4

.field public static final rtt_setting_mode:I = 0x7f0301a5

.field public static final screen_button_style:I = 0x7f0301a6

.field public static final screen_color_title:I = 0x7f0301a7

.field public static final screen_key_long_press_action_value:I = 0x7f0301a8

.field public static final screen_key_long_press_timeout:I = 0x7f0301a9

.field public static final screen_key_long_press_timeout_value:I = 0x7f0301aa

.field public static final screen_key_position_action:I = 0x7f0301ab

.field public static final screen_key_position_action_value:I = 0x7f0301ac

.field public static final screen_key_press_action:I = 0x7f0301ad

.field public static final screen_key_press_action_value:I = 0x7f0301ae

.field public static final screen_optimize_summary:I = 0x7f0301af

.field public static final screen_optimize_title:I = 0x7f0301b0

.field public static final screen_resolution:I = 0x7f0301b1

.field public static final screen_resolution_format:I = 0x7f0301b2

.field public static final screen_resolution_text:I = 0x7f0301b3

.field public static final screen_saturation_title:I = 0x7f0301b4

.field public static final screen_timeout_entries:I = 0x7f0301b5

.field public static final screen_timeout_values:I = 0x7f0301b6

.field public static final screen_zoom_level:I = 0x7f0301b7

.field public static final search_keywords_title_com_miui_securitycenter:I = 0x7f0301b8

.field public static final security_settings_premium_sms_values:I = 0x7f0301b9

.field public static final select_logd_size_lowram_titles:I = 0x7f0301ba

.field public static final select_logd_size_summaries:I = 0x7f0301bb

.field public static final select_logd_size_titles:I = 0x7f0301bc

.field public static final select_logd_size_values:I = 0x7f0301bd

.field public static final select_logd_system_levels:I = 0x7f0301be

.field public static final select_logpersist_summaries:I = 0x7f0301bf

.field public static final select_logpersist_titles:I = 0x7f0301c0

.field public static final select_logpersist_values:I = 0x7f0301c1

.field public static final setting_palette_colors:I = 0x7f0301c2

.field public static final setting_palette_data:I = 0x7f0301c3

.field public static final show_non_rect_clip_entries:I = 0x7f0301c4

.field public static final show_non_rect_clip_values:I = 0x7f0301c5

.field public static final show_verification_device_list:I = 0x7f0301c6

.field public static final silent_entries:I = 0x7f0301c7

.field public static final silent_mode_duration_text:I = 0x7f0301c8

.field public static final silent_mode_duration_value:I = 0x7f0301c9

.field public static final silent_values:I = 0x7f0301ca

.field public static final sim_color_light:I = 0x7f0301cb

.field public static final simulate_color_space_entries:I = 0x7f0301cc

.field public static final simulate_color_space_values:I = 0x7f0301cd

.field public static final slice_allowlist_package_names:I = 0x7f0301ce

.field public static final solar_terms:I = 0x7f0301cf

.field public static final special_locale_codes_global:I = 0x7f0301d0

.field public static final special_locale_names_global:I = 0x7f0301d1

.field public static final startup_hips_action:I = 0x7f0301d2

.field public static final startup_hips_values:I = 0x7f0301d3

.field public static final support_accessibility_haptic_device_list:I = 0x7f0301d4

.field public static final support_settings_haptic_device_list:I = 0x7f0301d5

.field public static final support_speaker_auto_clean_device_list:I = 0x7f0301d6

.field public static final suppot_lite_font_page_device:I = 0x7f0301d7

.field public static final swipe_direction_titles:I = 0x7f0301d8

.field public static final swipe_direction_values:I = 0x7f0301d9

.field public static final switch_to_user_zero_when_docked_timeout_entries:I = 0x7f0301da

.field public static final switch_to_user_zero_when_docked_timeout_values:I = 0x7f0301db

.field public static final tabs_taptic_detail:I = 0x7f0301dc

.field public static final tabs_taptic_detail_m2:I = 0x7f0301dd

.field public static final tare_alarm_manager_actions:I = 0x7f0301de

.field public static final tare_app_balance_subfactors:I = 0x7f0301df

.field public static final tare_consumption_limit_subfactors:I = 0x7f0301e0

.field public static final tare_job_scheduler_actions:I = 0x7f0301e1

.field public static final tare_modifiers_subfactors:I = 0x7f0301e2

.field public static final tare_policies:I = 0x7f0301e3

.field public static final tare_rewards_subfactors:I = 0x7f0301e4

.field public static final tare_units:I = 0x7f0301e5

.field public static final three_drop:I = 0x7f0301e6

.field public static final three_long_press:I = 0x7f0301e7

.field public static final time_format_entries:I = 0x7f0301e8

.field public static final time_format_values:I = 0x7f0301e9

.field public static final timezone_filters:I = 0x7f0301ea

.field public static final track_frame_time_entries:I = 0x7f0301eb

.field public static final track_frame_time_values:I = 0x7f0301ec

.field public static final traffic_balance_entries:I = 0x7f0301ed

.field public static final traffic_balance_values:I = 0x7f0301ee

.field public static final traffic_type:I = 0x7f0301ef

.field public static final transition_animation_scale_entries:I = 0x7f0301f0

.field public static final transition_animation_scale_values:I = 0x7f0301f1

.field public static final triple_click_mode_entries:I = 0x7f0301f2

.field public static final triple_key_mode_values:I = 0x7f0301f3

.field public static final triple_tone_entries:I = 0x7f0301f4

.field public static final triple_tone_entries_left_TWS01:I = 0x7f0301f5

.field public static final triple_tone_entriess_right_TWS01:I = 0x7f0301f6

.field public static final triple_tone_entry_values:I = 0x7f0301f7

.field public static final triple_tone_entry_values_TWS01:I = 0x7f0301f8

.field public static final true_color_screen_optimize_summary:I = 0x7f0301f9

.field public static final true_color_screen_optimize_title:I = 0x7f0301fa

.field public static final tts_demo_string_langs:I = 0x7f0301fb

.field public static final tts_demo_strings:I = 0x7f0301fc

.field public static final tts_rate_entries:I = 0x7f0301fd

.field public static final tts_rate_values:I = 0x7f0301fe

.field public static final tts_rate_xunfei_entries:I = 0x7f0301ff

.field public static final tts_rate_xunfei_values:I = 0x7f030200

.field public static final typing_style_entries:I = 0x7f030201

.field public static final typing_style_values:I = 0x7f030202

.field public static final uplmn_cu_mcc_mnc_values:I = 0x7f030203

.field public static final uplmn_prefer_network_mode_td_choices:I = 0x7f030204

.field public static final uplmn_prefer_network_mode_values:I = 0x7f030205

.field public static final uplmn_prefer_network_mode_w_choices:I = 0x7f030206

.field public static final usage_stats_display_order_types:I = 0x7f030207

.field public static final usb_configuration_titles:I = 0x7f030208

.field public static final usb_configuration_values:I = 0x7f030209

.field public static final usb_connection_mode_entries:I = 0x7f03020a

.field public static final usb_connection_mode_values:I = 0x7f03020b

.field public static final user_content_ratings_entries:I = 0x7f03020c

.field public static final user_content_ratings_values:I = 0x7f03020d

.field public static final values_color_calibration:I = 0x7f03020e

.field public static final vip_mode_text:I = 0x7f03020f

.field public static final vip_mode_value:I = 0x7f030210

.field public static final vpn_proxy_settings:I = 0x7f030211

.field public static final vpn_states:I = 0x7f030212

.field public static final vpn_types:I = 0x7f030213

.field public static final vpn_types_long:I = 0x7f030214

.field public static final wapi_psk_type:I = 0x7f030215

.field public static final week_days:I = 0x7f030216

.field public static final week_days_short:I = 0x7f030217

.field public static final week_days_shortest:I = 0x7f030218

.field public static final wfd_status:I = 0x7f030219

.field public static final when_to_start_screensaver_entries:I = 0x7f03021a

.field public static final when_to_start_screensaver_values:I = 0x7f03021b

.field public static final whetstone_level_entries:I = 0x7f03021c

.field public static final whetstone_level_values:I = 0x7f03021d

.field public static final wide_notch_battery_indicator_style_entries:I = 0x7f03021e

.field public static final wifi_ap_band:I = 0x7f03021f

.field public static final wifi_ap_band_config_2G_5G:I = 0x7f030220

.field public static final wifi_ap_band_config_2G_only:I = 0x7f030221

.field public static final wifi_ap_band_config_full:I = 0x7f030222

.field public static final wifi_ap_band_summary:I = 0x7f030223

.field public static final wifi_ap_hidden_ssid_config:I = 0x7f030224

.field public static final wifi_ap_security:I = 0x7f030225

.field public static final wifi_ap_security_with_sae:I = 0x7f030226

.field public static final wifi_calling_mode_choices:I = 0x7f030227

.field public static final wifi_calling_mode_choices_v2:I = 0x7f030228

.field public static final wifi_calling_mode_choices_v2_with_ims_preferred:I = 0x7f030229

.field public static final wifi_calling_mode_choices_v2_without_wifi_only:I = 0x7f03022a

.field public static final wifi_calling_mode_choices_v2_without_wifi_only_with_ims_preferred:I = 0x7f03022b

.field public static final wifi_calling_mode_choices_with_ims_preferred:I = 0x7f03022c

.field public static final wifi_calling_mode_choices_without_wifi_only:I = 0x7f03022d

.field public static final wifi_calling_mode_choices_without_wifi_only_with_ims_preferred:I = 0x7f03022e

.field public static final wifi_calling_mode_summaries:I = 0x7f03022f

.field public static final wifi_calling_mode_summaries_with_ims_preferred:I = 0x7f030230

.field public static final wifi_calling_mode_summaries_without_wifi_only:I = 0x7f030231

.field public static final wifi_calling_mode_summaries_without_wifi_only_with_ims_preferred:I = 0x7f030232

.field public static final wifi_calling_mode_values:I = 0x7f030233

.field public static final wifi_calling_mode_values_with_ims_preferred:I = 0x7f030234

.field public static final wifi_calling_mode_values_without_wifi_only:I = 0x7f030235

.field public static final wifi_calling_mode_values_without_wifi_only_with_ims_preferred:I = 0x7f030236

.field public static final wifi_connect_type_entries:I = 0x7f030237

.field public static final wifi_connect_type_values:I = 0x7f030238

.field public static final wifi_eap_entries:I = 0x7f030239

.field public static final wifi_eap_method:I = 0x7f03023a

.field public static final wifi_eap_method_add_sim:I = 0x7f03023b

.field public static final wifi_eap_method_add_sim_for_cmcc:I = 0x7f03023c

.field public static final wifi_eap_method_target_strings:I = 0x7f03023d

.field public static final wifi_eap_method_tts_strings:I = 0x7f03023e

.field public static final wifi_frequency_band_entries:I = 0x7f03023f

.field public static final wifi_frequency_band_values:I = 0x7f030240

.field public static final wifi_hidden_entries:I = 0x7f030241

.field public static final wifi_hidden_gbk_entries:I = 0x7f030242

.field public static final wifi_ip_settings:I = 0x7f030243

.field public static final wifi_link_turbo_mode_entries:I = 0x7f030244

.field public static final wifi_link_turbo_mode_summary_entries:I = 0x7f030245

.field public static final wifi_link_turbo_mode_values:I = 0x7f030246

.field public static final wifi_metered_entries:I = 0x7f030247

.field public static final wifi_metered_values:I = 0x7f030248

.field public static final wifi_p2p_status:I = 0x7f030249

.field public static final wifi_p2p_wps_setup:I = 0x7f03024a

.field public static final wifi_peap_phase2_entries:I = 0x7f03024b

.field public static final wifi_peap_phase2_entries_with_sim_auth:I = 0x7f03024c

.field public static final wifi_privacy_entries:I = 0x7f03024d

.field public static final wifi_privacy_values:I = 0x7f03024e

.field public static final wifi_proxy_settings:I = 0x7f03024f

.field public static final wifi_security:I = 0x7f030250

.field public static final wifi_security_no_eap:I = 0x7f030251

.field public static final wifi_security_only_wapi_cert_entries:I = 0x7f030252

.field public static final wifi_signal:I = 0x7f030253

.field public static final wifi_ssid_type_entries:I = 0x7f030254

.field public static final wifi_ssid_type_values:I = 0x7f030255

.field public static final wifi_status:I = 0x7f030256

.field public static final wifi_status_with_ssid:I = 0x7f030257

.field public static final wifi_tether_security:I = 0x7f030258

.field public static final wifi_tether_security_values:I = 0x7f030259

.field public static final wifi_ttls_phase2_entries:I = 0x7f03025a

.field public static final wifi_wapi_key_type:I = 0x7f03025b

.field public static final wifitrackerlib_wifi_status:I = 0x7f03025c

.field public static final window_animation_scale_entries:I = 0x7f03025d

.field public static final window_animation_scale_values:I = 0x7f03025e

.field public static final zen_mode_contacts_calls_entries:I = 0x7f03025f

.field public static final zen_mode_contacts_messages_entries:I = 0x7f030260

.field public static final zen_mode_contacts_values:I = 0x7f030261

.field public static final zen_mode_conversations_entries:I = 0x7f030262

.field public static final zen_mode_conversations_values:I = 0x7f030263


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
