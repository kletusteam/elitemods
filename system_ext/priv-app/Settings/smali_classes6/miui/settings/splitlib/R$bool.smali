.class public final Lmiui/settings/splitlib/R$bool;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/settings/splitlib/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "bool"
.end annotation


# static fields
.field public static final abc_action_bar_embed_tabs:I = 0x7f050000

.field public static final abc_action_bar_expanded_action_views_exclusive:I = 0x7f050001

.field public static final abc_allow_stacked_button_bar:I = 0x7f050002

.field public static final abc_config_actionMenuItemAllCaps:I = 0x7f050003

.field public static final abc_config_showMenuShortcutsWhenKeyboardPresent:I = 0x7f050004

.field public static final abc_split_action_bar_is_narrow:I = 0x7f050005

.field public static final action_bar_embed_tabs:I = 0x7f050006

.field public static final action_bar_tight_title:I = 0x7f050007

.field public static final auto_confirm_bluetooth_activation_dialog:I = 0x7f050008

.field public static final check_widget_anim_enable:I = 0x7f050009

.field public static final config_additional_system_update_setting_enable:I = 0x7f05000a

.field public static final config_agps_enabled:I = 0x7f05000b

.field public static final config_allow_edit_carrier_enabled:I = 0x7f05000c

.field public static final config_alwaysShowTypeIcon:I = 0x7f05000d

.field public static final config_battery_combine_system_components:I = 0x7f05000e

.field public static final config_device_is_drip_type:I = 0x7f05000f

.field public static final config_device_is_hole_type:I = 0x7f050010

.field public static final config_device_is_narrow_notch_type:I = 0x7f050011

.field public static final config_disable_dun_apn:I = 0x7f050012

.field public static final config_disable_uninstall_update:I = 0x7f050013

.field public static final config_display_recent_apps:I = 0x7f050014

.field public static final config_display_volte:I = 0x7f050015

.field public static final config_display_vowifi:I = 0x7f050016

.field public static final config_enableColorTemperature:I = 0x7f050017

.field public static final config_enable_extra_screen_zoom_preview:I = 0x7f050018

.field public static final config_face_education_use_lottie:I = 0x7f050019

.field public static final config_face_intro_show_less_secure:I = 0x7f05001a

.field public static final config_face_intro_show_require_eyes:I = 0x7f05001b

.field public static final config_force_rounded_icon_TopLevelSettings:I = 0x7f05001c

.field public static final config_gesture_settings_enabled:I = 0x7f05001d

.field public static final config_global_support_slm:I = 0x7f05001e

.field public static final config_handle_sim_slot_change:I = 0x7f05001f

.field public static final config_has_aikey:I = 0x7f050020

.field public static final config_has_help:I = 0x7f050021

.field public static final config_hideNoInternetState:I = 0x7f050022

.field public static final config_hide_ims_apns:I = 0x7f050023

.field public static final config_hide_none_security_option:I = 0x7f050024

.field public static final config_hide_swipe_security_option:I = 0x7f050025

.field public static final config_hspa_data_distinguishable:I = 0x7f050026

.field public static final config_keep_contextual_card_dismissal_timestamp:I = 0x7f050027

.field public static final config_location_mode_available:I = 0x7f050028

.field public static final config_lock_pattern_minimal_ui:I = 0x7f050029

.field public static final config_materialPreferenceIconSpaceReserved:I = 0x7f05002a

.field public static final config_msid_enable:I = 0x7f05002b

.field public static final config_network_selection_list_aggregation_enabled:I = 0x7f05002c

.field public static final config_nfc_detection_point:I = 0x7f05002d

.field public static final config_night_light_suggestion_enabled:I = 0x7f05002e

.field public static final config_offer_restricted_profiles:I = 0x7f05002f

.field public static final config_securitycenter_enable:I = 0x7f050030

.field public static final config_showEmergencyButton:I = 0x7f050031

.field public static final config_showMin3G:I = 0x7f050032

.field public static final config_showRsrpSignalLevelforLTE:I = 0x7f050033

.field public static final config_show_adaptive_connectivity:I = 0x7f050034

.field public static final config_show_alarm_volume:I = 0x7f050035

.field public static final config_show_app_info_settings_battery:I = 0x7f050036

.field public static final config_show_app_info_settings_memory:I = 0x7f050037

.field public static final config_show_assist_and_voice_input:I = 0x7f050038

.field public static final config_show_avatar_in_homepage:I = 0x7f050039

.field public static final config_show_branded_account_in_device_info:I = 0x7f05003a

.field public static final config_show_call_volume:I = 0x7f05003b

.field public static final config_show_camera_laser_sensor:I = 0x7f05003c

.field public static final config_show_charging_sounds:I = 0x7f05003d

.field public static final config_show_connect_tone_ui:I = 0x7f05003e

.field public static final config_show_customize_carrier_name:I = 0x7f05003f

.field public static final config_show_data_saver:I = 0x7f050040

.field public static final config_show_default_home:I = 0x7f050041

.field public static final config_show_device_administrators:I = 0x7f050042

.field public static final config_show_device_header_in_device_info:I = 0x7f050043

.field public static final config_show_device_model:I = 0x7f050044

.field public static final config_show_device_name:I = 0x7f050045

.field public static final config_show_emergency_gesture_settings:I = 0x7f050046

.field public static final config_show_emergency_info_in_device_info:I = 0x7f050047

.field public static final config_show_emergency_settings:I = 0x7f050048

.field public static final config_show_enabled_vr_listeners:I = 0x7f050049

.field public static final config_show_encryption_and_credentials_encryption_status:I = 0x7f05004a

.field public static final config_show_enhanced_handover_swith:I = 0x7f05004b

.field public static final config_show_high_power_apps:I = 0x7f05004c

.field public static final config_show_location_scanning:I = 0x7f05004d

.field public static final config_show_manage_device_admin:I = 0x7f05004e

.field public static final config_show_manage_trust_agents:I = 0x7f05004f

.field public static final config_show_manual:I = 0x7f050050

.field public static final config_show_media_volume:I = 0x7f050051

.field public static final config_show_mobile_plan:I = 0x7f050052

.field public static final config_show_notification_ringtone:I = 0x7f050053

.field public static final config_show_notification_volume:I = 0x7f050054

.field public static final config_show_phone_language:I = 0x7f050055

.field public static final config_show_physical_keyboard_pref:I = 0x7f050056

.field public static final config_show_pointer_speed:I = 0x7f050057

.field public static final config_show_premium_sms:I = 0x7f050058

.field public static final config_show_private_dns_settings:I = 0x7f050059

.field public static final config_show_regulatory_info:I = 0x7f05005a

.field public static final config_show_reset_dashboard:I = 0x7f05005b

.field public static final config_show_screen_locking_sounds:I = 0x7f05005c

.field public static final config_show_screen_pinning_settings:I = 0x7f05005d

.field public static final config_show_show_password:I = 0x7f05005e

.field public static final config_show_smart_sim:I = 0x7f05005f

.field public static final config_show_smart_storage_toggle:I = 0x7f050060

.field public static final config_show_smooth_display:I = 0x7f050061

.field public static final config_show_softap_wifi6:I = 0x7f050062

.field public static final config_show_spellcheckers_settings:I = 0x7f050063

.field public static final config_show_system_update_settings:I = 0x7f050064

.field public static final config_show_toggle_airplane:I = 0x7f050065

.field public static final config_show_top_level_accessibility:I = 0x7f050066

.field public static final config_show_top_level_battery:I = 0x7f050067

.field public static final config_show_top_level_connected_devices:I = 0x7f050068

.field public static final config_show_top_level_display:I = 0x7f050069

.field public static final config_show_touch_sounds:I = 0x7f05006a

.field public static final config_show_trust_agent_click_intent:I = 0x7f05006b

.field public static final config_show_tts_settings_summary:I = 0x7f05006c

.field public static final config_show_ucar_projection_screen:I = 0x7f05006d

.field public static final config_show_unlock_set_or_change:I = 0x7f05006e

.field public static final config_show_vibrate_input_devices:I = 0x7f05006f

.field public static final config_show_virtual_keyboard_pref:I = 0x7f050070

.field public static final config_show_wallpaper_attribution:I = 0x7f050071

.field public static final config_show_whitelist_roaming_swith:I = 0x7f050072

.field public static final config_show_wifi_ip_address:I = 0x7f050073

.field public static final config_show_wifi_mac_address:I = 0x7f050074

.field public static final config_show_wifi_settings:I = 0x7f050075

.field public static final config_sim_deletion_confirmation_default_on:I = 0x7f050076

.field public static final config_storage_manager_settings_enabled:I = 0x7f050077

.field public static final config_suc_use_partner_resource:I = 0x7f050078

.field public static final config_support_CT_PA:I = 0x7f050079

.field public static final config_support_enabled:I = 0x7f05007a

.field public static final config_supported_large_screen:I = 0x7f05007b

.field public static final config_use_compact_battery_status:I = 0x7f05007c

.field public static final config_use_legacy_suggestion:I = 0x7f05007d

.field public static final dream_setup_supported:I = 0x7f05007e

.field public static final enable_pbap_pce_profile:I = 0x7f05007f

.field public static final has_boot_sounds:I = 0x7f050080

.field public static final has_dock_settings:I = 0x7f050081

.field public static final has_powercontrol_widget:I = 0x7f050082

.field public static final has_silent_mode:I = 0x7f050083

.field public static final has_translation_contributors:I = 0x7f050084

.field public static final is_black_theme:I = 0x7f050085

.field public static final is_pad:I = 0x7f050086

.field public static final is_phone:I = 0x7f050087

.field public static final is_tablet:I = 0x7f050088

.field public static final kg_hide_emgcy_btn_when_oos:I = 0x7f050089

.field public static final miui_font_scale_switch:I = 0x7f05008a

.field public static final miuix_appcompat_floating_window_is_translucent:I = 0x7f05008b

.field public static final miuix_appcompat_window_floating:I = 0x7f05008c

.field public static final mtrl_btn_textappearance_all_caps:I = 0x7f05008d

.field public static final preference_item_bg_enable_variablePadding:I = 0x7f05008e

.field public static final settingslib_config_allow_divider:I = 0x7f05008f

.field public static final settingslib_config_icon_space_reserved:I = 0x7f050090

.field public static final simple_cache_enable_im_memory:I = 0x7f050091

.field public static final spinner_popup_item_bg_enable_variablePadding:I = 0x7f050092

.field public static final sudUseTabletLayout:I = 0x7f050093

.field public static final support_night_mode:I = 0x7f050094

.field public static final treat_as_land:I = 0x7f050095

.field public static final wifi_settings_only_support_portrait:I = 0x7f050096


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
