.class public final Lmiui/settings/splitlib/R$id;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/settings/splitlib/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final ALT:I = 0x7f0a0000

.field public static final BOTTOM_END:I = 0x7f0a0001

.field public static final BOTTOM_START:I = 0x7f0a0002

.field public static final CTRL:I = 0x7f0a0003

.field public static final FUNCTION:I = 0x7f0a0004

.field public static final META:I = 0x7f0a0005

.field public static final NO_DEBUG:I = 0x7f0a0006

.field public static final SHIFT:I = 0x7f0a0007

.field public static final SHOW_ALL:I = 0x7f0a0008

.field public static final SHOW_PATH:I = 0x7f0a0009

.field public static final SHOW_PROGRESS:I = 0x7f0a000a

.field public static final SYM:I = 0x7f0a000b

.field public static final TOP_END:I = 0x7f0a000c

.field public static final TOP_START:I = 0x7f0a000d

.field public static final abc_icon_frame:I = 0x7f0a000e

.field public static final about_settings:I = 0x7f0a000f

.field public static final accelerate:I = 0x7f0a0010

.field public static final accelerometer_settings:I = 0x7f0a0011

.field public static final accept:I = 0x7f0a0012

.field public static final accessibility_action_clickable_span:I = 0x7f0a0013

.field public static final accessibility_button:I = 0x7f0a0014

.field public static final accessibility_custom_action_0:I = 0x7f0a0015

.field public static final accessibility_custom_action_1:I = 0x7f0a0016

.field public static final accessibility_custom_action_10:I = 0x7f0a0017

.field public static final accessibility_custom_action_11:I = 0x7f0a0018

.field public static final accessibility_custom_action_12:I = 0x7f0a0019

.field public static final accessibility_custom_action_13:I = 0x7f0a001a

.field public static final accessibility_custom_action_14:I = 0x7f0a001b

.field public static final accessibility_custom_action_15:I = 0x7f0a001c

.field public static final accessibility_custom_action_16:I = 0x7f0a001d

.field public static final accessibility_custom_action_17:I = 0x7f0a001e

.field public static final accessibility_custom_action_18:I = 0x7f0a001f

.field public static final accessibility_custom_action_19:I = 0x7f0a0020

.field public static final accessibility_custom_action_2:I = 0x7f0a0021

.field public static final accessibility_custom_action_20:I = 0x7f0a0022

.field public static final accessibility_custom_action_21:I = 0x7f0a0023

.field public static final accessibility_custom_action_22:I = 0x7f0a0024

.field public static final accessibility_custom_action_23:I = 0x7f0a0025

.field public static final accessibility_custom_action_24:I = 0x7f0a0026

.field public static final accessibility_custom_action_25:I = 0x7f0a0027

.field public static final accessibility_custom_action_26:I = 0x7f0a0028

.field public static final accessibility_custom_action_27:I = 0x7f0a0029

.field public static final accessibility_custom_action_28:I = 0x7f0a002a

.field public static final accessibility_custom_action_29:I = 0x7f0a002b

.field public static final accessibility_custom_action_3:I = 0x7f0a002c

.field public static final accessibility_custom_action_30:I = 0x7f0a002d

.field public static final accessibility_custom_action_31:I = 0x7f0a002e

.field public static final accessibility_custom_action_4:I = 0x7f0a002f

.field public static final accessibility_custom_action_5:I = 0x7f0a0030

.field public static final accessibility_custom_action_6:I = 0x7f0a0031

.field public static final accessibility_custom_action_7:I = 0x7f0a0032

.field public static final accessibility_custom_action_8:I = 0x7f0a0033

.field public static final accessibility_custom_action_9:I = 0x7f0a0034

.field public static final account:I = 0x7f0a0035

.field public static final account_add:I = 0x7f0a0036

.field public static final account_avatar:I = 0x7f0a0037

.field public static final account_avatar_two_pane_version:I = 0x7f0a0038

.field public static final account_icon:I = 0x7f0a0039

.field public static final account_id:I = 0x7f0a003a

.field public static final account_list:I = 0x7f0a003b

.field public static final account_settings:I = 0x7f0a003c

.field public static final account_settings_menu_auto_sync:I = 0x7f0a003d

.field public static final account_settings_menu_auto_sync_personal:I = 0x7f0a003e

.field public static final account_settings_menu_auto_sync_work:I = 0x7f0a003f

.field public static final accounts:I = 0x7f0a0040

.field public static final accounts_label:I = 0x7f0a0041

.field public static final action:I = 0x7f0a0042

.field public static final action0:I = 0x7f0a0043

.field public static final action_back:I = 0x7f0a0044

.field public static final action_bar:I = 0x7f0a0045

.field public static final action_bar_activity_content:I = 0x7f0a0046

.field public static final action_bar_back:I = 0x7f0a0047

.field public static final action_bar_collapse_container:I = 0x7f0a0048

.field public static final action_bar_collapse_tab_container:I = 0x7f0a0049

.field public static final action_bar_container:I = 0x7f0a004a

.field public static final action_bar_expand_container:I = 0x7f0a004b

.field public static final action_bar_movable_container:I = 0x7f0a004c

.field public static final action_bar_movable_tab_container:I = 0x7f0a004d

.field public static final action_bar_overlay_bg:I = 0x7f0a004e

.field public static final action_bar_overlay_floating_root:I = 0x7f0a004f

.field public static final action_bar_overlay_layout:I = 0x7f0a0050

.field public static final action_bar_root:I = 0x7f0a0051

.field public static final action_bar_spinner:I = 0x7f0a0052

.field public static final action_bar_subtitle:I = 0x7f0a0053

.field public static final action_bar_subtitle_expand:I = 0x7f0a0054

.field public static final action_bar_title:I = 0x7f0a0055

.field public static final action_bar_title_expand:I = 0x7f0a0056

.field public static final action_btn:I = 0x7f0a0057

.field public static final action_button:I = 0x7f0a0058

.field public static final action_container:I = 0x7f0a0059

.field public static final action_context_bar:I = 0x7f0a005a

.field public static final action_context_bar_vs:I = 0x7f0a005b

.field public static final action_delete:I = 0x7f0a005c

.field public static final action_details:I = 0x7f0a005d

.field public static final action_divider:I = 0x7f0a005e

.field public static final action_drag_move_bottom:I = 0x7f0a005f

.field public static final action_drag_move_down:I = 0x7f0a0060

.field public static final action_drag_move_top:I = 0x7f0a0061

.field public static final action_drag_move_up:I = 0x7f0a0062

.field public static final action_drag_remove:I = 0x7f0a0063

.field public static final action_image:I = 0x7f0a0064

.field public static final action_menu_divider:I = 0x7f0a0065

.field public static final action_menu_item_child_icon:I = 0x7f0a0066

.field public static final action_menu_item_child_text:I = 0x7f0a0067

.field public static final action_menu_presenter:I = 0x7f0a0068

.field public static final action_mode_bar:I = 0x7f0a0069

.field public static final action_mode_bar_stub:I = 0x7f0a006a

.field public static final action_mode_close_button:I = 0x7f0a006b

.field public static final action_select_all:I = 0x7f0a006c

.field public static final action_sent_indicator:I = 0x7f0a006d

.field public static final action_text:I = 0x7f0a006e

.field public static final actionbar_layout:I = 0x7f0a006f

.field public static final actionbar_parent:I = 0x7f0a0070

.field public static final actionbar_title:I = 0x7f0a0071

.field public static final actions:I = 0x7f0a0072

.field public static final active_layout:I = 0x7f0a0073

.field public static final activity_chooser_view_content:I = 0x7f0a0074

.field public static final ad_service_instructions_link_text_view:I = 0x7f0a0075

.field public static final add:I = 0x7f0a0076

.field public static final add_a_photo_icon:I = 0x7f0a0077

.field public static final add_action_button:I = 0x7f0a0078

.field public static final add_another:I = 0x7f0a0079

.field public static final add_language:I = 0x7f0a007a

.field public static final add_msg:I = 0x7f0a007b

.field public static final add_msg_expander:I = 0x7f0a007c

.field public static final add_msg_simplified:I = 0x7f0a007d

.field public static final add_new_service:I = 0x7f0a007e

.field public static final add_preference_widget:I = 0x7f0a007f

.field public static final add_to_blackfile_button:I = 0x7f0a0080

.field public static final additional_summary:I = 0x7f0a0081

.field public static final adjacent:I = 0x7f0a0082

.field public static final admin_description:I = 0x7f0a0083

.field public static final admin_disabled_other_options:I = 0x7f0a0084

.field public static final admin_disabled_other_options_text:I = 0x7f0a0085

.field public static final admin_icon:I = 0x7f0a0086

.field public static final admin_more_details_link:I = 0x7f0a0087

.field public static final admin_name:I = 0x7f0a0088

.field public static final admin_policies:I = 0x7f0a0089

.field public static final admin_support_dialog_title:I = 0x7f0a008a

.field public static final admin_support_icon:I = 0x7f0a008b

.field public static final admin_support_message:I = 0x7f0a008c

.field public static final admin_support_msg:I = 0x7f0a008d

.field public static final admin_warning:I = 0x7f0a008e

.field public static final admin_warning_simplified:I = 0x7f0a008f

.field public static final advanced:I = 0x7f0a0090

.field public static final advanced_shortcut:I = 0x7f0a0091

.field public static final agree_button:I = 0x7f0a0092

.field public static final ai_double_click:I = 0x7f0a0093

.field public static final ai_long_press:I = 0x7f0a0094

.field public static final ai_settings_back:I = 0x7f0a0095

.field public static final ai_settings_sub_title:I = 0x7f0a0096

.field public static final ai_single_click:I = 0x7f0a0097

.field public static final airplane_mode_settings:I = 0x7f0a0098

.field public static final alarm_section:I = 0x7f0a0099

.field public static final alarmmanager:I = 0x7f0a009a

.field public static final alert:I = 0x7f0a009b

.field public static final alertTitle:I = 0x7f0a009c

.field public static final alert_icon:I = 0x7f0a009d

.field public static final alert_label:I = 0x7f0a009e

.field public static final alert_summary:I = 0x7f0a009f

.field public static final alerted_icon:I = 0x7f0a00a0

.field public static final alias:I = 0x7f0a00a1

.field public static final aligned:I = 0x7f0a00a2

.field public static final all:I = 0x7f0a00a3

.field public static final all_details:I = 0x7f0a00a4

.field public static final allow_button:I = 0x7f0a00a5

.field public static final alphanumeric_pin:I = 0x7f0a00a6

.field public static final also_erases_esim:I = 0x7f0a00a7

.field public static final also_erases_external:I = 0x7f0a00a8

.field public static final always:I = 0x7f0a00a9

.field public static final alwaysUse:I = 0x7f0a00aa

.field public static final always_on_invalid_reason:I = 0x7f0a00ab

.field public static final always_on_vpn:I = 0x7f0a00ac

.field public static final amPm:I = 0x7f0a00ad

.field public static final ancAdapterText:I = 0x7f0a00ae

.field public static final ancAdjust:I = 0x7f0a00af

.field public static final ancAdjustText:I = 0x7f0a00b0

.field public static final ancAdjustView:I = 0x7f0a00b1

.field public static final ancAdjustView2:I = 0x7f0a00b2

.field public static final ancCard:I = 0x7f0a00b3

.field public static final ancHighText:I = 0x7f0a00b4

.field public static final ancLayoutInfo:I = 0x7f0a00b5

.field public static final ancLowText:I = 0x7f0a00b6

.field public static final ancMediumText:I = 0x7f0a00b7

.field public static final ancText:I = 0x7f0a00b8

.field public static final anclayout:I = 0x7f0a00b9

.field public static final android_beam_explained:I = 0x7f0a00ba

.field public static final android_beam_image:I = 0x7f0a00bb

.field public static final android_beam_settings:I = 0x7f0a00bc

.field public static final android_beam_switch:I = 0x7f0a00bd

.field public static final android_beam_tips:I = 0x7f0a00be

.field public static final android_radio:I = 0x7f0a00bf

.field public static final androidx_window_activity_scope:I = 0x7f0a00c0

.field public static final angle:I = 0x7f0a00c1

.field public static final anim_preference_connecting:I = 0x7f0a00c2

.field public static final animateToEnd:I = 0x7f0a00c3

.field public static final animateToStart:I = 0x7f0a00c4

.field public static final animated_img:I = 0x7f0a00c5

.field public static final anonymous:I = 0x7f0a00c6

.field public static final anonymous_icon:I = 0x7f0a00c7

.field public static final anonymous_summary:I = 0x7f0a00c8

.field public static final anonymous_title:I = 0x7f0a00c9

.field public static final answer:I = 0x7f0a00ca

.field public static final aod_style_icon:I = 0x7f0a00cb

.field public static final apn_radiobutton:I = 0x7f0a00cc

.field public static final app1_view:I = 0x7f0a00cd

.field public static final app2_view:I = 0x7f0a00ce

.field public static final app3_view:I = 0x7f0a00cf

.field public static final app_background:I = 0x7f0a00d0

.field public static final app_bar:I = 0x7f0a00d1

.field public static final app_bar_container:I = 0x7f0a00d2

.field public static final app_bar_content:I = 0x7f0a00d3

.field public static final app_data_content:I = 0x7f0a00d4

.field public static final app_data_icon:I = 0x7f0a00d5

.field public static final app_data_summary:I = 0x7f0a00d6

.field public static final app_data_text:I = 0x7f0a00d7

.field public static final app_data_title:I = 0x7f0a00d8

.field public static final app_detail:I = 0x7f0a00d9

.field public static final app_detail_icon:I = 0x7f0a00da

.field public static final app_detail_summary:I = 0x7f0a00db

.field public static final app_detail_title:I = 0x7f0a00dc

.field public static final app_details:I = 0x7f0a00dd

.field public static final app_disabled:I = 0x7f0a00de

.field public static final app_entities_header:I = 0x7f0a00df

.field public static final app_foreground:I = 0x7f0a00e0

.field public static final app_head:I = 0x7f0a00e1

.field public static final app_header:I = 0x7f0a00e2

.field public static final app_icon:I = 0x7f0a00e3

.field public static final app_launch_scrollview:I = 0x7f0a00e4

.field public static final app_limit_switch:I = 0x7f0a00e5

.field public static final app_name:I = 0x7f0a00e6

.field public static final app_name_textView:I = 0x7f0a00e7

.field public static final app_on_sdcard:I = 0x7f0a00e8

.field public static final app_restrictions_pref:I = 0x7f0a00e9

.field public static final app_restrictions_settings:I = 0x7f0a00ea

.field public static final app_screen_time:I = 0x7f0a00eb

.field public static final app_settings:I = 0x7f0a00ec

.field public static final app_size:I = 0x7f0a00ed

.field public static final app_summary:I = 0x7f0a00ee

.field public static final app_switches:I = 0x7f0a00ef

.field public static final app_timer:I = 0x7f0a00f0

.field public static final app_title:I = 0x7f0a00f1

.field public static final app_titles:I = 0x7f0a00f2

.field public static final app_usage_bar_view:I = 0x7f0a00f3

.field public static final app_views_container:I = 0x7f0a00f4

.field public static final append:I = 0x7f0a00f5

.field public static final appendix:I = 0x7f0a00f6

.field public static final application_list:I = 0x7f0a00f7

.field public static final application_settings:I = 0x7f0a00f8

.field public static final appops_content:I = 0x7f0a00f9

.field public static final apps:I = 0x7f0a00fa

.field public static final appsSize:I = 0x7f0a00fb

.field public static final appsSizePrefix:I = 0x7f0a00fc

.field public static final apps_list:I = 0x7f0a00fd

.field public static final apps_top_intro_text:I = 0x7f0a00fe

.field public static final arc:I = 0x7f0a00ff

.field public static final arrow:I = 0x7f0a0100

.field public static final arrow_image_view:I = 0x7f0a0101

.field public static final arrow_next:I = 0x7f0a0102

.field public static final arrow_previous:I = 0x7f0a0103

.field public static final arrow_right:I = 0x7f0a0104

.field public static final arrow_right_2:I = 0x7f0a0105

.field public static final asConfigured:I = 0x7f0a0106

.field public static final assetsFolder:I = 0x7f0a0107

.field public static final async:I = 0x7f0a0108

.field public static final attention_text:I = 0x7f0a0109

.field public static final auto:I = 0x7f0a010a

.field public static final autoComplete:I = 0x7f0a010b

.field public static final autoCompleteToEnd:I = 0x7f0a010c

.field public static final autoCompleteToStart:I = 0x7f0a010d

.field public static final auto_connect:I = 0x7f0a010e

.field public static final auto_connect_slidingButton:I = 0x7f0a010f

.field public static final auto_launch:I = 0x7f0a0110

.field public static final autoclick_delay:I = 0x7f0a0111

.field public static final automatic:I = 0x7f0a0112

.field public static final avatar_grid:I = 0x7f0a0113

.field public static final avatar_image:I = 0x7f0a0114

.field public static final back:I = 0x7f0a0115

.field public static final back_button:I = 0x7f0a0116

.field public static final back_finger_print:I = 0x7f0a0117

.field public static final back_image:I = 0x7f0a0118

.field public static final back_image_exit:I = 0x7f0a0119

.field public static final back_linearlayout:I = 0x7f0a011a

.field public static final back_sensitivity_seekbar:I = 0x7f0a011b

.field public static final background:I = 0x7f0a011c

.field public static final background_image:I = 0x7f0a011d

.field public static final background_text:I = 0x7f0a011e

.field public static final background_view:I = 0x7f0a011f

.field public static final backgroundlayout:I = 0x7f0a0120

.field public static final backtap_guide:I = 0x7f0a0121

.field public static final backtap_guide_lottie_view:I = 0x7f0a0122

.field public static final backup_image_view:I = 0x7f0a0123

.field public static final backup_pw_cancel_button:I = 0x7f0a0124

.field public static final backup_pw_set_button:I = 0x7f0a0125

.field public static final band:I = 0x7f0a0126

.field public static final banner_dismiss_btn:I = 0x7f0a0127

.field public static final banner_icon:I = 0x7f0a0128

.field public static final banner_negative_btn:I = 0x7f0a0129

.field public static final banner_positive_btn:I = 0x7f0a012a

.field public static final banner_subtitle:I = 0x7f0a012b

.field public static final banner_summary:I = 0x7f0a012c

.field public static final banner_title:I = 0x7f0a012d

.field public static final bar_app_usage_detail:I = 0x7f0a012e

.field public static final bar_chart_details:I = 0x7f0a012f

.field public static final bar_chart_title:I = 0x7f0a0130

.field public static final bar_summary:I = 0x7f0a0131

.field public static final bar_title:I = 0x7f0a0132

.field public static final bar_view:I = 0x7f0a0133

.field public static final bar_view1:I = 0x7f0a0134

.field public static final bar_view2:I = 0x7f0a0135

.field public static final bar_view3:I = 0x7f0a0136

.field public static final bar_view4:I = 0x7f0a0137

.field public static final bar_views_container:I = 0x7f0a0138

.field public static final barrier:I = 0x7f0a0139

.field public static final base_card_item:I = 0x7f0a013a

.field public static final base_main_layout:I = 0x7f0a013b

.field public static final base_settings_card_view:I = 0x7f0a013c

.field public static final baseline:I = 0x7f0a013d

.field public static final batteryBoxInfo:I = 0x7f0a013e

.field public static final batteryCard:I = 0x7f0a013f

.field public static final batteryLeftInfo:I = 0x7f0a0140

.field public static final batteryRightInfo:I = 0x7f0a0141

.field public static final battery_active:I = 0x7f0a0142

.field public static final battery_chart:I = 0x7f0a0143

.field public static final battery_entity_header:I = 0x7f0a0144

.field public static final battery_header_icon:I = 0x7f0a0145

.field public static final battery_icon:I = 0x7f0a0146

.field public static final battery_indicator_settings:I = 0x7f0a0147

.field public static final battery_info_layout:I = 0x7f0a0148

.field public static final battery_percent:I = 0x7f0a0149

.field public static final battery_settings_new:I = 0x7f0a014a

.field public static final battery_usage:I = 0x7f0a014b

.field public static final batterybox:I = 0x7f0a014c

.field public static final batteryleft:I = 0x7f0a014d

.field public static final batteryright:I = 0x7f0a014e

.field public static final beginOnFirstDraw:I = 0x7f0a014f

.field public static final beginning:I = 0x7f0a0150

.field public static final bg_image:I = 0x7f0a0151

.field public static final big_title:I = 0x7f0a0152

.field public static final biometric_header_description:I = 0x7f0a0153

.field public static final blank:I = 0x7f0a0154

.field public static final blank_screen:I = 0x7f0a0155

.field public static final blank_screen_icon:I = 0x7f0a0156

.field public static final blank_screen_no_device:I = 0x7f0a0157

.field public static final bleAudioPasswordDialog:I = 0x7f0a0158

.field public static final bleAudioSourceInfo:I = 0x7f0a0159

.field public static final ble_audio_password:I = 0x7f0a015a

.field public static final blob_expiry:I = 0x7f0a015b

.field public static final blob_id:I = 0x7f0a015c

.field public static final blob_label:I = 0x7f0a015d

.field public static final blob_size:I = 0x7f0a015e

.field public static final blocking:I = 0x7f0a015f

.field public static final bluetoothShareBroadcast:I = 0x7f0a0160

.field public static final bluetooth_audio_bit_per_sample_16:I = 0x7f0a0161

.field public static final bluetooth_audio_bit_per_sample_24:I = 0x7f0a0162

.field public static final bluetooth_audio_bit_per_sample_32:I = 0x7f0a0163

.field public static final bluetooth_audio_bit_per_sample_default:I = 0x7f0a0164

.field public static final bluetooth_audio_bit_per_sample_radio_group:I = 0x7f0a0165

.field public static final bluetooth_audio_channel_mode_default:I = 0x7f0a0166

.field public static final bluetooth_audio_channel_mode_mono:I = 0x7f0a0167

.field public static final bluetooth_audio_channel_mode_radio_group:I = 0x7f0a0168

.field public static final bluetooth_audio_channel_mode_stereo:I = 0x7f0a0169

.field public static final bluetooth_audio_codec_aac:I = 0x7f0a016a

.field public static final bluetooth_audio_codec_aptx:I = 0x7f0a016b

.field public static final bluetooth_audio_codec_aptx_adaptive:I = 0x7f0a016c

.field public static final bluetooth_audio_codec_aptx_hd:I = 0x7f0a016d

.field public static final bluetooth_audio_codec_aptx_twsp:I = 0x7f0a016e

.field public static final bluetooth_audio_codec_default:I = 0x7f0a016f

.field public static final bluetooth_audio_codec_help_info:I = 0x7f0a0170

.field public static final bluetooth_audio_codec_lc3:I = 0x7f0a0171

.field public static final bluetooth_audio_codec_ldac:I = 0x7f0a0172

.field public static final bluetooth_audio_codec_radio_group:I = 0x7f0a0173

.field public static final bluetooth_audio_codec_sbc:I = 0x7f0a0174

.field public static final bluetooth_audio_quality_best_effort:I = 0x7f0a0175

.field public static final bluetooth_audio_quality_default:I = 0x7f0a0176

.field public static final bluetooth_audio_quality_optimized_connection:I = 0x7f0a0177

.field public static final bluetooth_audio_quality_optimized_quality:I = 0x7f0a0178

.field public static final bluetooth_audio_quality_radio_group:I = 0x7f0a0179

.field public static final bluetooth_audio_sample_rate_441:I = 0x7f0a017a

.field public static final bluetooth_audio_sample_rate_480:I = 0x7f0a017b

.field public static final bluetooth_audio_sample_rate_882:I = 0x7f0a017c

.field public static final bluetooth_audio_sample_rate_960:I = 0x7f0a017d

.field public static final bluetooth_audio_sample_rate_default:I = 0x7f0a017e

.field public static final bluetooth_audio_sample_rate_radio_group:I = 0x7f0a017f

.field public static final bluetooth_broadcast_current_pin:I = 0x7f0a0180

.field public static final bluetooth_broadcast_pin_16:I = 0x7f0a0181

.field public static final bluetooth_broadcast_pin_4:I = 0x7f0a0182

.field public static final bluetooth_broadcast_pin_config_radio_group:I = 0x7f0a0183

.field public static final bluetooth_broadcast_pin_unencrypted:I = 0x7f0a0184

.field public static final bluetooth_device_confirmed:I = 0x7f0a0185

.field public static final bluetooth_device_default:I = 0x7f0a0186

.field public static final bluetooth_device_light:I = 0x7f0a0187

.field public static final bluetooth_fragment_settings:I = 0x7f0a0188

.field public static final bluetooth_section:I = 0x7f0a0189

.field public static final bluetooth_settings:I = 0x7f0a018a

.field public static final bluetooth_text_white_list:I = 0x7f0a018b

.field public static final board_layout:I = 0x7f0a018c

.field public static final body:I = 0x7f0a018d

.field public static final border_negative:I = 0x7f0a018e

.field public static final border_positive:I = 0x7f0a018f

.field public static final both:I = 0x7f0a0190

.field public static final bottom:I = 0x7f0a0191

.field public static final bottom_divider:I = 0x7f0a0192

.field public static final bottom_label_group:I = 0x7f0a0193

.field public static final bottom_label_space:I = 0x7f0a0194

.field public static final bottom_scroll_view:I = 0x7f0a0195

.field public static final bottom_summary:I = 0x7f0a0196

.field public static final bounce:I = 0x7f0a0197

.field public static final boxBattery:I = 0x7f0a0198

.field public static final box_2g:I = 0x7f0a0199

.field public static final box_5g:I = 0x7f0a019a

.field public static final brightness_settings:I = 0x7f0a019b

.field public static final broadcastPINcode:I = 0x7f0a019c

.field public static final broadcast_edit_text:I = 0x7f0a019d

.field public static final broadcast_error_message:I = 0x7f0a019e

.field public static final broadcast_name_text:I = 0x7f0a019f

.field public static final bssid:I = 0x7f0a01a0

.field public static final bt_battery_case:I = 0x7f0a01a1

.field public static final bt_battery_case_summary:I = 0x7f0a01a2

.field public static final bt_battery_icon:I = 0x7f0a01a3

.field public static final bt_battery_left:I = 0x7f0a01a4

.field public static final bt_battery_left_summary:I = 0x7f0a01a5

.field public static final bt_battery_prediction:I = 0x7f0a01a6

.field public static final bt_battery_right:I = 0x7f0a01a7

.field public static final bt_battery_right_summary:I = 0x7f0a01a8

.field public static final bt_battery_summary:I = 0x7f0a01a9

.field public static final bt_stylus_connect:I = 0x7f0a01aa

.field public static final bt_title:I = 0x7f0a01ab

.field public static final btn:I = 0x7f0a01ac

.field public static final btn_back:I = 0x7f0a01ad

.field public static final btn_back_global:I = 0x7f0a01ae

.field public static final btn_continue:I = 0x7f0a01af

.field public static final btn_delete:I = 0x7f0a01b0

.field public static final btn_exit_sos:I = 0x7f0a01b1

.field public static final btn_hide_sos:I = 0x7f0a01b2

.field public static final btn_learnToUse:I = 0x7f0a01b3

.field public static final btn_next:I = 0x7f0a01b4

.field public static final btn_screen_zoom_bigger:I = 0x7f0a01b5

.field public static final btn_screen_zoom_smaller:I = 0x7f0a01b6

.field public static final btn_select:I = 0x7f0a01b7

.field public static final btn_skip:I = 0x7f0a01b8

.field public static final btn_today:I = 0x7f0a01b9

.field public static final btn_week:I = 0x7f0a01ba

.field public static final bubble_all:I = 0x7f0a01bb

.field public static final bubble_all_icon:I = 0x7f0a01bc

.field public static final bubble_all_label:I = 0x7f0a01bd

.field public static final bubble_image:I = 0x7f0a01be

.field public static final bubble_none:I = 0x7f0a01bf

.field public static final bubble_none_icon:I = 0x7f0a01c0

.field public static final bubble_none_label:I = 0x7f0a01c1

.field public static final bubble_selected:I = 0x7f0a01c2

.field public static final bubble_selected_icon:I = 0x7f0a01c3

.field public static final bubble_selected_label:I = 0x7f0a01c4

.field public static final bugreport_option_full_summary:I = 0x7f0a01c5

.field public static final bugreport_option_full_title:I = 0x7f0a01c6

.field public static final bugreport_option_interactive_summary:I = 0x7f0a01c7

.field public static final bugreport_option_interactive_title:I = 0x7f0a01c8

.field public static final button:I = 0x7f0a01c9

.field public static final button1:I = 0x7f0a01ca

.field public static final button2:I = 0x7f0a01cb

.field public static final button3:I = 0x7f0a01cc

.field public static final button4:I = 0x7f0a01cd

.field public static final buttonContainer:I = 0x7f0a01ce

.field public static final buttonGroup:I = 0x7f0a01cf

.field public static final buttonPanel:I = 0x7f0a01d0

.field public static final button_bar:I = 0x7f0a01d1

.field public static final button_broadcast_layout:I = 0x7f0a01d2

.field public static final button_delete:I = 0x7f0a01d3

.field public static final button_disconnect:I = 0x7f0a01d4

.field public static final button_find_broadcast:I = 0x7f0a01d5

.field public static final button_group:I = 0x7f0a01d6

.field public static final button_highlight_container:I = 0x7f0a01d7

.field public static final button_icon:I = 0x7f0a01d8

.field public static final button_layout:I = 0x7f0a01d9

.field public static final button_leave_broadcast:I = 0x7f0a01da

.field public static final button_line:I = 0x7f0a01db

.field public static final button_modify:I = 0x7f0a01dc

.field public static final button_normal_container:I = 0x7f0a01dd

.field public static final button_panel:I = 0x7f0a01de

.field public static final button_scan_qr_code:I = 0x7f0a01df

.field public static final button_tutorial_image:I = 0x7f0a01e0

.field public static final button_tutorial_message:I = 0x7f0a01e1

.field public static final button_tutorial_title:I = 0x7f0a01e2

.field public static final button_update:I = 0x7f0a01e3

.field public static final buttons:I = 0x7f0a01e4

.field public static final buttons_spacer_left:I = 0x7f0a01e5

.field public static final bytes:I = 0x7f0a01e6

.field public static final ca_cert:I = 0x7f0a01e7

.field public static final cache_measures:I = 0x7f0a01e8

.field public static final calculating_title:I = 0x7f0a01e9

.field public static final calculating_view:I = 0x7f0a01ea

.field public static final call_arrow:I = 0x7f0a01eb

.field public static final call_detail:I = 0x7f0a01ec

.field public static final call_title:I = 0x7f0a01ed

.field public static final camera_group:I = 0x7f0a01ee

.field public static final camera_layout:I = 0x7f0a01ef

.field public static final cancel:I = 0x7f0a01f0

.field public static final cancelButton:I = 0x7f0a01f1

.field public static final cancel_action:I = 0x7f0a01f2

.field public static final cancel_backup_pw:I = 0x7f0a01f3

.field public static final cancel_btn:I = 0x7f0a01f4

.field public static final cancel_button:I = 0x7f0a01f5

.field public static final cancel_finger_authenticate:I = 0x7f0a01f6

.field public static final cancel_view:I = 0x7f0a01f7

.field public static final card:I = 0x7f0a01f8

.field public static final card_container:I = 0x7f0a01f9

.field public static final card_icon:I = 0x7f0a01fa

.field public static final card_title:I = 0x7f0a01fb

.field public static final card_value:I = 0x7f0a01fc

.field public static final card_value_layout:I = 0x7f0a01fd

.field public static final cardview:I = 0x7f0a01fe

.field public static final carrier_and_update:I = 0x7f0a01ff

.field public static final carrier_list:I = 0x7f0a0200

.field public static final carriers_container:I = 0x7f0a0201

.field public static final cb_trusted_credential_status:I = 0x7f0a0202

.field public static final cdma_settings:I = 0x7f0a0203

.field public static final cell_network_group:I = 0x7f0a0204

.field public static final center:I = 0x7f0a0205

.field public static final centerCrop:I = 0x7f0a0206

.field public static final centerInside:I = 0x7f0a0207

.field public static final center_btn:I = 0x7f0a0208

.field public static final center_horizontal:I = 0x7f0a0209

.field public static final center_vertical:I = 0x7f0a020a

.field public static final cert_list:I = 0x7f0a020b

.field public static final cert_remove_button:I = 0x7f0a020c

.field public static final cfg_wapi_key_type_spinner:I = 0x7f0a020d

.field public static final chain:I = 0x7f0a020e

.field public static final chains:I = 0x7f0a020f

.field public static final channel_fields:I = 0x7f0a0210

.field public static final charge:I = 0x7f0a0211

.field public static final charging_group:I = 0x7f0a0212

.field public static final chart_summary:I = 0x7f0a0213

.field public static final check_button:I = 0x7f0a0214

.field public static final check_group:I = 0x7f0a0215

.field public static final check_password_container:I = 0x7f0a0216

.field public static final check_text:I = 0x7f0a0217

.field public static final checkbox:I = 0x7f0a0218

.field public static final checkbox3points_borderlayout:I = 0x7f0a0219

.field public static final checkboxPanel:I = 0x7f0a021a

.field public static final checkboxWidget:I = 0x7f0a021b

.field public static final checkbox_3points:I = 0x7f0a021c

.field public static final checkbox_borderlayout:I = 0x7f0a021d

.field public static final checkbox_container:I = 0x7f0a021e

.field public static final checkbox_frame:I = 0x7f0a021f

.field public static final checkbox_negative:I = 0x7f0a0220

.field public static final checkbox_positive:I = 0x7f0a0221

.field public static final checkbox_state_checked:I = 0x7f0a0222

.field public static final checkbox_state_normal:I = 0x7f0a0223

.field public static final checkbox_stub:I = 0x7f0a0224

.field public static final checked:I = 0x7f0a0225

.field public static final checked_fl:I = 0x7f0a0226

.field public static final checkgroup:I = 0x7f0a0227

.field public static final chip:I = 0x7f0a0228

.field public static final chip1:I = 0x7f0a0229

.field public static final chip2:I = 0x7f0a022a

.field public static final chip3:I = 0x7f0a022b

.field public static final chip_group:I = 0x7f0a022c

.field public static final choose__lockPattern:I = 0x7f0a022d

.field public static final choose_channel:I = 0x7f0a022e

.field public static final choose_different_network:I = 0x7f0a022f

.field public static final choose_relative_view:I = 0x7f0a0230

.field public static final chronometer:I = 0x7f0a0231

.field public static final circle_center:I = 0x7f0a0232

.field public static final circle_view:I = 0x7f0a0233

.field public static final clear:I = 0x7f0a0234

.field public static final clear_action_button:I = 0x7f0a0235

.field public static final clear_activities_button:I = 0x7f0a0236

.field public static final clear_all_data_text:I = 0x7f0a0237

.field public static final clear_button:I = 0x7f0a0238

.field public static final clear_data:I = 0x7f0a0239

.field public static final clear_list:I = 0x7f0a023a

.field public static final clear_list_category:I = 0x7f0a023b

.field public static final clear_list_layout:I = 0x7f0a023c

.field public static final clear_text:I = 0x7f0a023d

.field public static final clickable:I = 0x7f0a023e

.field public static final clip_horizontal:I = 0x7f0a023f

.field public static final clip_vertical:I = 0x7f0a0240

.field public static final clock_hour:I = 0x7f0a0241

.field public static final clock_minute:I = 0x7f0a0242

.field public static final clockwise:I = 0x7f0a0243

.field public static final close:I = 0x7f0a0244

.field public static final closeAnc:I = 0x7f0a0245

.field public static final closeAncImage:I = 0x7f0a0246

.field public static final closeAncText:I = 0x7f0a0247

.field public static final close_button:I = 0x7f0a0248

.field public static final collapse:I = 0x7f0a0249

.field public static final collapseActionView:I = 0x7f0a024a

.field public static final collapse_button:I = 0x7f0a024b

.field public static final collapsing_toolbar:I = 0x7f0a024c

.field public static final color_bar:I = 0x7f0a024d

.field public static final color_bit_point:I = 0x7f0a024e

.field public static final color_icon:I = 0x7f0a024f

.field public static final color_label:I = 0x7f0a0250

.field public static final color_preview:I = 0x7f0a0251

.field public static final color_spinner:I = 0x7f0a0252

.field public static final color_swatch:I = 0x7f0a0253

.field public static final color_text:I = 0x7f0a0254

.field public static final comment:I = 0x7f0a0255

.field public static final comp_description:I = 0x7f0a0256

.field public static final companion_text:I = 0x7f0a0257

.field public static final compress:I = 0x7f0a0258

.field public static final config_list:I = 0x7f0a0259

.field public static final confirm:I = 0x7f0a025a

.field public static final confirm_backup_pw:I = 0x7f0a025b

.field public static final confirm_button:I = 0x7f0a025c

.field public static final confirm_fingerprint_dialog_title:I = 0x7f0a025d

.field public static final confirm_fingerprint_view_msg:I = 0x7f0a025e

.field public static final confirm_fingerprint_view_title:I = 0x7f0a025f

.field public static final confirm_new_backup_pw:I = 0x7f0a0260

.field public static final confirm_view:I = 0x7f0a0261

.field public static final connect:I = 0x7f0a0262

.field public static final connect_fail:I = 0x7f0a0263

.field public static final connect_status:I = 0x7f0a0264

.field public static final connecting:I = 0x7f0a0265

.field public static final connection_indicator:I = 0x7f0a0266

.field public static final contacts_list:I = 0x7f0a0267

.field public static final container:I = 0x7f0a0268

.field public static final container_layout:I = 0x7f0a0269

.field public static final container_material:I = 0x7f0a026a

.field public static final content:I = 0x7f0a026b

.field public static final contentPanel:I = 0x7f0a026c

.field public static final contentView:I = 0x7f0a026d

.field public static final content_frame:I = 0x7f0a026e

.field public static final content_header_container:I = 0x7f0a026f

.field public static final content_layout:I = 0x7f0a0270

.field public static final content_mask:I = 0x7f0a0271

.field public static final content_mask_vs:I = 0x7f0a0272

.field public static final content_parent:I = 0x7f0a0273

.field public static final content_wrapper:I = 0x7f0a0274

.field public static final contents:I = 0x7f0a0275

.field public static final contents_cacrt:I = 0x7f0a0276

.field public static final contents_title:I = 0x7f0a0277

.field public static final contents_usercrt:I = 0x7f0a0278

.field public static final contents_userkey:I = 0x7f0a0279

.field public static final contextual_cards_content:I = 0x7f0a027a

.field public static final contiguous:I = 0x7f0a027b

.field public static final controlScreen_description:I = 0x7f0a027c

.field public static final controlScreen_icon:I = 0x7f0a027d

.field public static final controlScreen_title:I = 0x7f0a027e

.field public static final control_buttons_panel:I = 0x7f0a027f

.field public static final control_center_style_delete_guide:I = 0x7f0a0280

.field public static final control_center_style_icon:I = 0x7f0a0281

.field public static final conversation_icon:I = 0x7f0a0282

.field public static final conversation_settings_clear_recents:I = 0x7f0a0283

.field public static final cool_color:I = 0x7f0a0284

.field public static final coordinator:I = 0x7f0a0285

.field public static final cos:I = 0x7f0a0286

.field public static final count:I = 0x7f0a0287

.field public static final counterclockwise:I = 0x7f0a0288

.field public static final counting_down:I = 0x7f0a0289

.field public static final cpu_group:I = 0x7f0a028a

.field public static final createSupervisedUser:I = 0x7f0a028b

.field public static final credential_container:I = 0x7f0a028c

.field public static final credential_management_app_description:I = 0x7f0a028d

.field public static final credential_management_app_icon:I = 0x7f0a028e

.field public static final credential_management_app_title:I = 0x7f0a028f

.field public static final credentials_image:I = 0x7f0a0290

.field public static final cts_icon:I = 0x7f0a0291

.field public static final current_backup_pw:I = 0x7f0a0292

.field public static final current_backup_pw_layout:I = 0x7f0a0293

.field public static final current_label:I = 0x7f0a0294

.field public static final current_time:I = 0x7f0a0295

.field public static final custom:I = 0x7f0a0296

.field public static final customPanel:I = 0x7f0a0297

.field public static final custom_color:I = 0x7f0a0298

.field public static final custom_content:I = 0x7f0a0299

.field public static final customize_button:I = 0x7f0a029a

.field public static final customize_container:I = 0x7f0a029b

.field public static final cut:I = 0x7f0a029c

.field public static final cycle_day:I = 0x7f0a029d

.field public static final cycle_left_time:I = 0x7f0a029e

.field public static final cycle_progress:I = 0x7f0a029f

.field public static final cycles_spinner:I = 0x7f0a02a0

.field public static final dark_mode_apps:I = 0x7f0a02a1

.field public static final dark_mode_apps_line:I = 0x7f0a02a2

.field public static final dark_mode_apps_title:I = 0x7f0a02a3

.field public static final dark_mode_enable:I = 0x7f0a02a4

.field public static final dark_mode_outer_view:I = 0x7f0a02a5

.field public static final dark_mode_parent:I = 0x7f0a02a6

.field public static final dark_mode_text:I = 0x7f0a02a7

.field public static final dark_mode_view:I = 0x7f0a02a8

.field public static final dark_mode_wallpaper:I = 0x7f0a02a9

.field public static final data:I = 0x7f0a02aa

.field public static final data_limits:I = 0x7f0a02ab

.field public static final data_network_type_label:I = 0x7f0a02ac

.field public static final data_network_type_value:I = 0x7f0a02ad

.field public static final data_remaining_view:I = 0x7f0a02ae

.field public static final data_state_label:I = 0x7f0a02af

.field public static final data_state_value:I = 0x7f0a02b0

.field public static final data_usage:I = 0x7f0a02b1

.field public static final data_usage_menu_metered:I = 0x7f0a02b2

.field public static final data_usage_slidingButton:I = 0x7f0a02b3

.field public static final data_usage_view:I = 0x7f0a02b4

.field public static final datePicker:I = 0x7f0a02b5

.field public static final datePickerContainer:I = 0x7f0a02b6

.field public static final datePickerLunar:I = 0x7f0a02b7

.field public static final dateTimePicker:I = 0x7f0a02b8

.field public static final dateTimePickerContainer:I = 0x7f0a02b9

.field public static final date_picker_actions:I = 0x7f0a02ba

.field public static final date_time_settings:I = 0x7f0a02bb

.field public static final datetime_picker:I = 0x7f0a02bc

.field public static final day:I = 0x7f0a02bd

.field public static final day_data_traffic:I = 0x7f0a02be

.field public static final day_total:I = 0x7f0a02bf

.field public static final decelerate:I = 0x7f0a02c0

.field public static final decelerateAndComplete:I = 0x7f0a02c1

.field public static final decor_content_parent:I = 0x7f0a02c2

.field public static final decorate_view:I = 0x7f0a02c3

.field public static final defaultView:I = 0x7f0a02c4

.field public static final default_activity_button:I = 0x7f0a02c5

.field public static final default_content_container:I = 0x7f0a02c6

.field public static final default_imgview:I = 0x7f0a02c7

.field public static final default_keyboard:I = 0x7f0a02c8

.field public static final default_label:I = 0x7f0a02c9

.field public static final default_textview:I = 0x7f0a02ca

.field public static final deferred_setup_arrow:I = 0x7f0a02cb

.field public static final deferred_setup_title:I = 0x7f0a02cc

.field public static final delete:I = 0x7f0a02cd

.field public static final delete_btn:I = 0x7f0a02ce

.field public static final delete_button:I = 0x7f0a02cf

.field public static final delete_managed_profile_closing_paragraph:I = 0x7f0a02d0

.field public static final delete_managed_profile_device_manager_name:I = 0x7f0a02d1

.field public static final delete_managed_profile_mdm_icon_view:I = 0x7f0a02d2

.field public static final delete_managed_profile_opening_paragraph:I = 0x7f0a02d3

.field public static final delete_this_device:I = 0x7f0a02d4

.field public static final delimiter:I = 0x7f0a02d5

.field public static final delimiter_storage:I = 0x7f0a02d6

.field public static final deltaRelative:I = 0x7f0a02d7

.field public static final deny_button:I = 0x7f0a02d8

.field public static final dependency_ordering:I = 0x7f0a02d9

.field public static final des:I = 0x7f0a02da

.field public static final description:I = 0x7f0a02db

.field public static final description_grid:I = 0x7f0a02dc

.field public static final description_text:I = 0x7f0a02dd

.field public static final design_bottom_sheet:I = 0x7f0a02de

.field public static final design_menu_item_action_area:I = 0x7f0a02df

.field public static final design_menu_item_action_area_stub:I = 0x7f0a02e0

.field public static final design_menu_item_text:I = 0x7f0a02e1

.field public static final design_navigation_view:I = 0x7f0a02e2

.field public static final detail_arrow:I = 0x7f0a02e3

.field public static final detail_description:I = 0x7f0a02e4

.field public static final detail_msg_text:I = 0x7f0a02e5

.field public static final detail_title:I = 0x7f0a02e6

.field public static final determinateBar:I = 0x7f0a02e7

.field public static final deviceDetails:I = 0x7f0a02e8

.field public static final deviceName:I = 0x7f0a02e9

.field public static final device_description_image:I = 0x7f0a02ea

.field public static final device_icon:I = 0x7f0a02eb

.field public static final device_memory_card_view:I = 0x7f0a02ec

.field public static final device_more_parameter:I = 0x7f0a02ed

.field public static final device_more_parameter_divider:I = 0x7f0a02ee

.field public static final device_name:I = 0x7f0a02ef

.field public static final device_name_card_layout:I = 0x7f0a02f0

.field public static final device_name_card_view:I = 0x7f0a02f1

.field public static final device_name_key_id:I = 0x7f0a02f2

.field public static final device_name_value_id:I = 0x7f0a02f3

.field public static final device_params:I = 0x7f0a02f4

.field public static final device_status:I = 0x7f0a02f5

.field public static final device_usage_bar_view:I = 0x7f0a02f6

.field public static final devicestats_notification:I = 0x7f0a02f7

.field public static final devicestats_unlock:I = 0x7f0a02f8

.field public static final dex:I = 0x7f0a02f9

.field public static final dialog:I = 0x7f0a02fa

.field public static final dialog_alert_subtitle:I = 0x7f0a02fb

.field public static final dialog_bg:I = 0x7f0a02fc

.field public static final dialog_button:I = 0x7f0a02fd

.field public static final dialog_container:I = 0x7f0a02fe

.field public static final dialog_dim_bg:I = 0x7f0a02ff

.field public static final dialog_divider:I = 0x7f0a0300

.field public static final dialog_icon:I = 0x7f0a0301

.field public static final dialog_layout:I = 0x7f0a0302

.field public static final dialog_message:I = 0x7f0a0303

.field public static final dialog_root_view:I = 0x7f0a0304

.field public static final dialog_scrollview:I = 0x7f0a0305

.field public static final dialog_subtitle:I = 0x7f0a0306

.field public static final dialog_switch_hint:I = 0x7f0a0307

.field public static final dialog_title:I = 0x7f0a0308

.field public static final dimensions:I = 0x7f0a0309

.field public static final direct:I = 0x7f0a030a

.field public static final disableHome:I = 0x7f0a030b

.field public static final disablePostScroll:I = 0x7f0a030c

.field public static final disableScroll:I = 0x7f0a030d

.field public static final disagree_button:I = 0x7f0a030e

.field public static final disclaimer:I = 0x7f0a030f

.field public static final disclaimer_desc:I = 0x7f0a0310

.field public static final disclaimer_item_list:I = 0x7f0a0311

.field public static final disclaimer_title:I = 0x7f0a0312

.field public static final disjoint:I = 0x7f0a0313

.field public static final dismissal_icon_end:I = 0x7f0a0314

.field public static final dismissal_icon_start:I = 0x7f0a0315

.field public static final dismissal_swipe_background:I = 0x7f0a0316

.field public static final dismissal_view:I = 0x7f0a0317

.field public static final display_settings:I = 0x7f0a0318

.field public static final diverleft:I = 0x7f0a0319

.field public static final diverright:I = 0x7f0a031a

.field public static final divider:I = 0x7f0a031b

.field public static final divider1:I = 0x7f0a031c

.field public static final divider2:I = 0x7f0a031d

.field public static final divider3:I = 0x7f0a031e

.field public static final divider_line:I = 0x7f0a031f

.field public static final dns1:I = 0x7f0a0320

.field public static final dns2:I = 0x7f0a0321

.field public static final dns_servers:I = 0x7f0a0322

.field public static final dock_audio_media_enable_cb:I = 0x7f0a0323

.field public static final domain:I = 0x7f0a0324

.field public static final domain_name:I = 0x7f0a0325

.field public static final done:I = 0x7f0a0326

.field public static final dont_allow_button:I = 0x7f0a0327

.field public static final dot_one:I = 0x7f0a0328

.field public static final dot_three:I = 0x7f0a0329

.field public static final dot_two:I = 0x7f0a032a

.field public static final dot_view:I = 0x7f0a032b

.field public static final double_click_summary:I = 0x7f0a032c

.field public static final double_click_title:I = 0x7f0a032d

.field public static final download_progress:I = 0x7f0a032e

.field public static final dpp_qrCode_summary:I = 0x7f0a032f

.field public static final dpp_qrcode_generator:I = 0x7f0a0330

.field public static final dpp_sharing_wifi:I = 0x7f0a0331

.field public static final drag:I = 0x7f0a0332

.field public static final dragDown:I = 0x7f0a0333

.field public static final dragEnd:I = 0x7f0a0334

.field public static final dragHandle:I = 0x7f0a0335

.field public static final dragLeft:I = 0x7f0a0336

.field public static final dragList:I = 0x7f0a0337

.field public static final dragRight:I = 0x7f0a0338

.field public static final dragStart:I = 0x7f0a0339

.field public static final dragUp:I = 0x7f0a033a

.field public static final drag_btn:I = 0x7f0a033b

.field public static final drawable:I = 0x7f0a033c

.field public static final dream_list:I = 0x7f0a033d

.field public static final dream_preview_button:I = 0x7f0a033e

.field public static final dropdown:I = 0x7f0a033f

.field public static final dropdown_menu:I = 0x7f0a0340

.field public static final dynamic_item:I = 0x7f0a0341

.field public static final eap:I = 0x7f0a0342

.field public static final easeIn:I = 0x7f0a0343

.field public static final easeInOut:I = 0x7f0a0344

.field public static final easeOut:I = 0x7f0a0345

.field public static final edge_mode:I = 0x7f0a0346

.field public static final edit_query:I = 0x7f0a0347

.field public static final edit_sim_name:I = 0x7f0a0348

.field public static final edit_text:I = 0x7f0a0349

.field public static final editor:I = 0x7f0a034a

.field public static final edittext:I = 0x7f0a034b

.field public static final edittext_container:I = 0x7f0a034c

.field public static final eight:I = 0x7f0a034d

.field public static final eighthours:I = 0x7f0a034e

.field public static final either:I = 0x7f0a034f

.field public static final elastic:I = 0x7f0a0350

.field public static final emergencyCall:I = 0x7f0a0351

.field public static final emergencyCallButton:I = 0x7f0a0352

.field public static final emergency_contacts_edit_cancel:I = 0x7f0a0353

.field public static final emergency_contacts_edit_confirm:I = 0x7f0a0354

.field public static final emergency_gesture_number_override:I = 0x7f0a0355

.field public static final empty:I = 0x7f0a0356

.field public static final empty_body:I = 0x7f0a0357

.field public static final empty_header:I = 0x7f0a0358

.field public static final empty_img:I = 0x7f0a0359

.field public static final empty_print_state:I = 0x7f0a035a

.field public static final empty_printers_list_service_enabled:I = 0x7f0a035b

.field public static final empty_text:I = 0x7f0a035c

.field public static final empty_view:I = 0x7f0a035d

.field public static final enable:I = 0x7f0a035e

.field public static final enable_backup_text:I = 0x7f0a035f

.field public static final enable_mode:I = 0x7f0a0360

.field public static final enable_text:I = 0x7f0a0361

.field public static final enabled:I = 0x7f0a0362

.field public static final enalbe_identify_iPhone:I = 0x7f0a0363

.field public static final encroid:I = 0x7f0a0364

.field public static final encrypt_dont_require_password:I = 0x7f0a0365

.field public static final encrypt_require_password:I = 0x7f0a0366

.field public static final encryption:I = 0x7f0a0367

.field public static final encryption_message:I = 0x7f0a0368

.field public static final end:I = 0x7f0a0369

.field public static final endToStart:I = 0x7f0a036a

.field public static final end_framelayout:I = 0x7f0a036b

.field public static final end_padder:I = 0x7f0a036c

.field public static final enhanceVoiceText:I = 0x7f0a036d

.field public static final enterAlways:I = 0x7f0a036e

.field public static final enterAlwaysCollapsed:I = 0x7f0a036f

.field public static final entity_header:I = 0x7f0a0370

.field public static final entity_header_content:I = 0x7f0a0371

.field public static final entity_header_icon:I = 0x7f0a0372

.field public static final entity_header_icon_personal:I = 0x7f0a0373

.field public static final entity_header_icon_work:I = 0x7f0a0374

.field public static final entity_header_second_summary:I = 0x7f0a0375

.field public static final entity_header_summary:I = 0x7f0a0376

.field public static final entity_header_swap_horiz:I = 0x7f0a0377

.field public static final entity_header_title:I = 0x7f0a0378

.field public static final erase_esim:I = 0x7f0a0379

.field public static final erase_esim_container:I = 0x7f0a037a

.field public static final erase_external:I = 0x7f0a037b

.field public static final erase_external_container:I = 0x7f0a037c

.field public static final erase_external_option_text:I = 0x7f0a037d

.field public static final errorText:I = 0x7f0a037e

.field public static final error_item_text:I = 0x7f0a037f

.field public static final error_message:I = 0x7f0a0380

.field public static final error_text:I = 0x7f0a0381

.field public static final esim_id_label:I = 0x7f0a0382

.field public static final esim_id_value:I = 0x7f0a0383

.field public static final estimation:I = 0x7f0a0384

.field public static final exclusionlist:I = 0x7f0a0385

.field public static final execute_reset_network:I = 0x7f0a0386

.field public static final exit:I = 0x7f0a0387

.field public static final exitUntilCollapsed:I = 0x7f0a0388

.field public static final exit_layout:I = 0x7f0a0389

.field public static final expand:I = 0x7f0a038a

.field public static final expand_activities_button:I = 0x7f0a038b

.field public static final expand_content:I = 0x7f0a038c

.field public static final expand_icon:I = 0x7f0a038d

.field public static final expand_indicator:I = 0x7f0a038e

.field public static final expand_title:I = 0x7f0a038f

.field public static final expanded_menu:I = 0x7f0a0390

.field public static final expert_arrow:I = 0x7f0a0391

.field public static final extended_fab:I = 0x7f0a0392

.field public static final extra:I = 0x7f0a0393

.field public static final face_delete:I = 0x7f0a0394

.field public static final face_detail_image:I = 0x7f0a0395

.field public static final face_detail_info_text:I = 0x7f0a0396

.field public static final face_title_edit_text:I = 0x7f0a0397

.field public static final factor:I = 0x7f0a0398

.field public static final factor_list:I = 0x7f0a0399

.field public static final factor_number:I = 0x7f0a039a

.field public static final factory_reset:I = 0x7f0a039b

.field public static final fade:I = 0x7f0a039c

.field public static final feedback_btn:I = 0x7f0a039d

.field public static final feedback_services_settings:I = 0x7f0a039e

.field public static final fhd_img_view:I = 0x7f0a039f

.field public static final fhd_text_view:I = 0x7f0a03a0

.field public static final fhd_text_view_summary:I = 0x7f0a03a1

.field public static final fields:I = 0x7f0a03a2

.field public static final fill:I = 0x7f0a03a3

.field public static final fill_horizontal:I = 0x7f0a03a4

.field public static final fill_vertical:I = 0x7f0a03a5

.field public static final filled:I = 0x7f0a03a6

.field public static final filter_settings:I = 0x7f0a03a7

.field public static final filter_sort_view:I = 0x7f0a03a8

.field public static final filter_spinner:I = 0x7f0a03a9

.field public static final find_module:I = 0x7f0a03aa

.field public static final fingerprintIcon:I = 0x7f0a03ab

.field public static final fingerprint_animation:I = 0x7f0a03ac

.field public static final fingerprint_background:I = 0x7f0a03ad

.field public static final fingerprint_delete:I = 0x7f0a03ae

.field public static final fingerprint_detail_image:I = 0x7f0a03af

.field public static final fingerprint_detail_info_text:I = 0x7f0a03b0

.field public static final fingerprint_hint:I = 0x7f0a03b1

.field public static final fingerprint_hint_more:I = 0x7f0a03b2

.field public static final fingerprint_in_app_indicator:I = 0x7f0a03b3

.field public static final fingerprint_layout:I = 0x7f0a03b4

.field public static final fingerprint_name_edit_text:I = 0x7f0a03b5

.field public static final fingerprint_name_suggest_title:I = 0x7f0a03b6

.field public static final fingerprint_progress_bar:I = 0x7f0a03b7

.field public static final fingerprint_rename_field:I = 0x7f0a03b8

.field public static final fingerprint_sensor_location:I = 0x7f0a03b9

.field public static final fingerprint_sensor_location_animation:I = 0x7f0a03ba

.field public static final fingerprint_step_image:I = 0x7f0a03bb

.field public static final fingerprint_title_edit_text:I = 0x7f0a03bc

.field public static final finish_button:I = 0x7f0a03bd

.field public static final first_action:I = 0x7f0a03be

.field public static final first_rank:I = 0x7f0a03bf

.field public static final fitCenter:I = 0x7f0a03c0

.field public static final fitEnd:I = 0x7f0a03c1

.field public static final fitStart:I = 0x7f0a03c2

.field public static final fitToContents:I = 0x7f0a03c3

.field public static final fitXY:I = 0x7f0a03c4

.field public static final fitness_btn_done:I = 0x7f0a03c5

.field public static final fitness_btn_lauout:I = 0x7f0a03c6

.field public static final fitness_btn_restart:I = 0x7f0a03c7

.field public static final fitness_button_frame:I = 0x7f0a03c8

.field public static final fitness_checking_progress:I = 0x7f0a03c9

.field public static final fitness_checking_tv:I = 0x7f0a03ca

.field public static final fitness_icon:I = 0x7f0a03cb

.field public static final fitness_icon_l:I = 0x7f0a03cc

.field public static final fitness_icon_r:I = 0x7f0a03cd

.field public static final fitness_progress_ll:I = 0x7f0a03ce

.field public static final fitness_result_l:I = 0x7f0a03cf

.field public static final fitness_result_r:I = 0x7f0a03d0

.field public static final fitness_start:I = 0x7f0a03d1

.field public static final fitness_summary:I = 0x7f0a03d2

.field public static final fitness_title:I = 0x7f0a03d3

.field public static final fitness_title_height:I = 0x7f0a03d4

.field public static final five:I = 0x7f0a03d5

.field public static final fixed:I = 0x7f0a03d6

.field public static final fl_content:I = 0x7f0a03d7

.field public static final fl_limit_container:I = 0x7f0a03d8

.field public static final flashlight_group:I = 0x7f0a03d9

.field public static final flip:I = 0x7f0a03da

.field public static final float_notification_card:I = 0x7f0a03db

.field public static final floating:I = 0x7f0a03dc

.field public static final fold_screen_settings:I = 0x7f0a03dd

.field public static final folder:I = 0x7f0a03de

.field public static final followGuide:I = 0x7f0a03df

.field public static final font_bubble_left:I = 0x7f0a03e0

.field public static final font_bubble_left_layout:I = 0x7f0a03e1

.field public static final font_bubble_right:I = 0x7f0a03e2

.field public static final font_bubble_right_layout:I = 0x7f0a03e3

.field public static final font_hint_view:I = 0x7f0a03e4

.field public static final font_hint_view_layout:I = 0x7f0a03e5

.field public static final font_recycler_view:I = 0x7f0a03e6

.field public static final font_scroll_view:I = 0x7f0a03e7

.field public static final font_settings:I = 0x7f0a03e8

.field public static final font_size_preview_text_group:I = 0x7f0a03e9

.field public static final font_size_settings_layout:I = 0x7f0a03ea

.field public static final font_view:I = 0x7f0a03eb

.field public static final fontweight_view:I = 0x7f0a03ec

.field public static final footer:I = 0x7f0a03ed

.field public static final footerLayout:I = 0x7f0a03ee

.field public static final footerLeftButton:I = 0x7f0a03ef

.field public static final footerRightButton:I = 0x7f0a03f0

.field public static final footerText:I = 0x7f0a03f1

.field public static final footer_divider:I = 0x7f0a03f2

.field public static final footer_message_2:I = 0x7f0a03f3

.field public static final footer_message_3:I = 0x7f0a03f4

.field public static final footer_message_4:I = 0x7f0a03f5

.field public static final footer_message_5:I = 0x7f0a03f6

.field public static final footer_message_6:I = 0x7f0a03f7

.field public static final footer_title_1:I = 0x7f0a03f8

.field public static final footer_title_2:I = 0x7f0a03f9

.field public static final footerlayout:I = 0x7f0a03fa

.field public static final fore:I = 0x7f0a03fb

.field public static final forever:I = 0x7f0a03fc

.field public static final forgetPassword:I = 0x7f0a03fd

.field public static final forgetPattern:I = 0x7f0a03fe

.field public static final forget_button:I = 0x7f0a03ff

.field public static final forget_password_dialog_content_four:I = 0x7f0a0400

.field public static final forget_password_dialog_content_one:I = 0x7f0a0401

.field public static final forget_password_dialog_content_three:I = 0x7f0a0402

.field public static final forget_password_dialog_content_two:I = 0x7f0a0403

.field public static final forget_password_dialog_title:I = 0x7f0a0404

.field public static final forgotButton:I = 0x7f0a0405

.field public static final forgot_password_text:I = 0x7f0a0406

.field public static final four:I = 0x7f0a0407

.field public static final fourhours:I = 0x7f0a0408

.field public static final fragment_container:I = 0x7f0a0409

.field public static final fragment_container_view_tag:I = 0x7f0a040a

.field public static final fragment_content:I = 0x7f0a040b

.field public static final fragment_other:I = 0x7f0a040c

.field public static final fragment_settings:I = 0x7f0a040d

.field public static final frame:I = 0x7f0a040e

.field public static final frame_layout:I = 0x7f0a040f

.field public static final freeSize:I = 0x7f0a0410

.field public static final freeSizePrefix:I = 0x7f0a0411

.field public static final free_wifi_confirm:I = 0x7f0a0412

.field public static final free_wifi_info:I = 0x7f0a0413

.field public static final free_wifi_login_again:I = 0x7f0a0414

.field public static final free_wifi_online_users_num:I = 0x7f0a0415

.field public static final free_wifi_signal:I = 0x7f0a0416

.field public static final free_wifi_speed_slow:I = 0x7f0a0417

.field public static final free_wifi_status:I = 0x7f0a0418

.field public static final free_wifi_title:I = 0x7f0a0419

.field public static final freeform_vedio_text_description:I = 0x7f0a041a

.field public static final friction_icon:I = 0x7f0a041b

.field public static final from:I = 0x7f0a041c

.field public static final frpSkipPassword:I = 0x7f0a041d

.field public static final full_down:I = 0x7f0a041e

.field public static final full_image:I = 0x7f0a041f

.field public static final full_screen_container:I = 0x7f0a0420

.field public static final function_key_text:I = 0x7f0a0421

.field public static final function_specific:I = 0x7f0a0422

.field public static final function_specific_icon:I = 0x7f0a0423

.field public static final gateway:I = 0x7f0a0424

.field public static final gesture_animation_view:I = 0x7f0a0425

.field public static final gesture_image:I = 0x7f0a0426

.field public static final gesture_img:I = 0x7f0a0427

.field public static final gesture_tutorial_message:I = 0x7f0a0428

.field public static final gesture_tutorial_title:I = 0x7f0a0429

.field public static final ghost_view:I = 0x7f0a042a

.field public static final ghost_view_holder:I = 0x7f0a042b

.field public static final glif_layout:I = 0x7f0a042c

.field public static final global_feedback_category:I = 0x7f0a042d

.field public static final go_to_location_setting:I = 0x7f0a042e

.field public static final gone:I = 0x7f0a042f

.field public static final gp_contact_first:I = 0x7f0a0430

.field public static final gp_contact_second:I = 0x7f0a0431

.field public static final gp_contact_third:I = 0x7f0a0432

.field public static final gps_group:I = 0x7f0a0433

.field public static final graph:I = 0x7f0a0434

.field public static final graph_label_group:I = 0x7f0a0435

.field public static final graph_wrap:I = 0x7f0a0436

.field public static final grid_view:I = 0x7f0a0437

.field public static final gridview:I = 0x7f0a0438

.field public static final groupConnectSuccess:I = 0x7f0a0439

.field public static final group_cn_110_call:I = 0x7f0a043a

.field public static final group_cn_119_call:I = 0x7f0a043b

.field public static final group_cn_120_call:I = 0x7f0a043c

.field public static final group_cn_sos_common_call:I = 0x7f0a043d

.field public static final group_cn_sos_el:I = 0x7f0a043e

.field public static final group_divider:I = 0x7f0a043f

.field public static final group_indicator:I = 0x7f0a0440

.field public static final grouping:I = 0x7f0a0441

.field public static final groups:I = 0x7f0a0442

.field public static final gsm_settings:I = 0x7f0a0443

.field public static final guide_button:I = 0x7f0a0444

.field public static final guide_text:I = 0x7f0a0445

.field public static final guide_title:I = 0x7f0a0446

.field public static final guideline:I = 0x7f0a0447

.field public static final gxzw_anim_preview:I = 0x7f0a0448

.field public static final gxzw_anim_select_view:I = 0x7f0a0449

.field public static final gxzw_new_fingerprint_indicate:I = 0x7f0a044a

.field public static final gxzw_new_fingerprint_ok:I = 0x7f0a044b

.field public static final gxzw_quick_open_fast_container:I = 0x7f0a044c

.field public static final gxzw_quick_open_fast_summary:I = 0x7f0a044d

.field public static final gxzw_quick_open_fast_title:I = 0x7f0a044e

.field public static final gxzw_quick_open_navigation:I = 0x7f0a044f

.field public static final gxzw_quick_open_preview:I = 0x7f0a0450

.field public static final gxzw_quick_open_slow_container:I = 0x7f0a0451

.field public static final gxzw_quick_open_slow_summary:I = 0x7f0a0452

.field public static final gxzw_quick_open_slow_title:I = 0x7f0a0453

.field public static final gxzw_recognizing_anim_container:I = 0x7f0a0454

.field public static final gxzw_recognizing_anim_frame:I = 0x7f0a0455

.field public static final gxzw_recognizing_anim_icon:I = 0x7f0a0456

.field public static final gxzw_recognizing_anim_name:I = 0x7f0a0457

.field public static final handle_tip:I = 0x7f0a0458

.field public static final handymode_guide:I = 0x7f0a0459

.field public static final handymode_guide_lottie_view:I = 0x7f0a045a

.field public static final haptic_text:I = 0x7f0a045b

.field public static final hardware:I = 0x7f0a045c

.field public static final hardware_info:I = 0x7f0a045d

.field public static final hardware_info_list:I = 0x7f0a045e

.field public static final hardware_shortcut:I = 0x7f0a045f

.field public static final head:I = 0x7f0a0460

.field public static final header:I = 0x7f0a0461

.field public static final headerMessage:I = 0x7f0a0462

.field public static final headerText:I = 0x7f0a0463

.field public static final header_content_container:I = 0x7f0a0464

.field public static final header_content_view:I = 0x7f0a0465

.field public static final header_details:I = 0x7f0a0466

.field public static final header_divider:I = 0x7f0a0467

.field public static final header_google_settings:I = 0x7f0a0468

.field public static final header_icon:I = 0x7f0a0469

.field public static final header_icons_container:I = 0x7f0a046a

.field public static final header_layout:I = 0x7f0a046b

.field public static final header_subtitle:I = 0x7f0a046c

.field public static final header_title:I = 0x7f0a046d

.field public static final header_view:I = 0x7f0a046e

.field public static final headsetAntiLostLayout:I = 0x7f0a046f

.field public static final headsetBatteryInfo:I = 0x7f0a0470

.field public static final headsetTransport:I = 0x7f0a0471

.field public static final health:I = 0x7f0a0472

.field public static final hidden_gbk_fields:I = 0x7f0a0473

.field public static final hidden_gbk_settings:I = 0x7f0a0474

.field public static final hidden_gbk_title:I = 0x7f0a0475

.field public static final hidden_settings:I = 0x7f0a0476

.field public static final hidden_settings_field:I = 0x7f0a0477

.field public static final hidden_settings_fields:I = 0x7f0a0478

.field public static final hidden_settings_title:I = 0x7f0a0479

.field public static final hidden_settings_warning:I = 0x7f0a047a

.field public static final hidden_ssid:I = 0x7f0a047b

.field public static final hide_all:I = 0x7f0a047c

.field public static final hide_split_action_bar:I = 0x7f0a047d

.field public static final hide_system:I = 0x7f0a047e

.field public static final hideable:I = 0x7f0a047f

.field public static final high:I = 0x7f0a0480

.field public static final high_imgview:I = 0x7f0a0481

.field public static final high_keyboard:I = 0x7f0a0482

.field public static final high_textview:I = 0x7f0a0483

.field public static final high_tick:I = 0x7f0a0484

.field public static final highlight_list_container:I = 0x7f0a0485

.field public static final highlight_text:I = 0x7f0a0486

.field public static final hint_bar:I = 0x7f0a0487

.field public static final hint_layout:I = 0x7f0a0488

.field public static final hint_text:I = 0x7f0a0489

.field public static final history_image:I = 0x7f0a048a

.field public static final history_image_on:I = 0x7f0a048b

.field public static final history_off:I = 0x7f0a048c

.field public static final history_off_summary:I = 0x7f0a048d

.field public static final history_off_title:I = 0x7f0a048e

.field public static final history_on:I = 0x7f0a048f

.field public static final history_on_empty:I = 0x7f0a0490

.field public static final history_toggled_on_summary:I = 0x7f0a0491

.field public static final history_toggled_on_title:I = 0x7f0a0492

.field public static final home:I = 0x7f0a0493

.field public static final homeAsUp:I = 0x7f0a0494

.field public static final home_app_pref:I = 0x7f0a0495

.field public static final home_app_uninstall:I = 0x7f0a0496

.field public static final home_divider:I = 0x7f0a0497

.field public static final home_radio:I = 0x7f0a0498

.field public static final homepage_all_settings:I = 0x7f0a0499

.field public static final homepage_app_bar_regular_phone_view:I = 0x7f0a049a

.field public static final homepage_app_bar_two_pane_view:I = 0x7f0a049b

.field public static final homepage_container:I = 0x7f0a049c

.field public static final homepage_personal_settings:I = 0x7f0a049d

.field public static final homepage_title:I = 0x7f0a049e

.field public static final honorRequest:I = 0x7f0a049f

.field public static final horizontal:I = 0x7f0a04a0

.field public static final horizontal_dot:I = 0x7f0a04a1

.field public static final horizontal_hour:I = 0x7f0a04a2

.field public static final horizontal_min:I = 0x7f0a04a3

.field public static final hostname:I = 0x7f0a04a4

.field public static final hour:I = 0x7f0a04a5

.field public static final how_message:I = 0x7f0a04a6

.field public static final httpClientTest:I = 0x7f0a04a7

.field public static final huge:I = 0x7f0a04a8

.field public static final icc_id_label:I = 0x7f0a04a9

.field public static final icc_id_value:I = 0x7f0a04aa

.field public static final icon:I = 0x7f0a04ab

.field public static final icon_button:I = 0x7f0a04ac

.field public static final icon_container:I = 0x7f0a04ad

.field public static final icon_device_locked:I = 0x7f0a04ae

.field public static final icon_dpp_qrcode:I = 0x7f0a04af

.field public static final icon_end:I = 0x7f0a04b0

.field public static final icon_end_frame:I = 0x7f0a04b1

.field public static final icon_fingerprint:I = 0x7f0a04b2

.field public static final icon_frame:I = 0x7f0a04b3

.field public static final icon_glasses:I = 0x7f0a04b4

.field public static final icon_grand_parent:I = 0x7f0a04b5

.field public static final icon_group:I = 0x7f0a04b6

.field public static final icon_imageView:I = 0x7f0a04b7

.field public static final icon_info:I = 0x7f0a04b8

.field public static final icon_label_container:I = 0x7f0a04b9

.field public static final icon_layout:I = 0x7f0a04ba

.field public static final icon_less_secure:I = 0x7f0a04bb

.field public static final icon_link:I = 0x7f0a04bc

.field public static final icon_looking:I = 0x7f0a04bd

.field public static final icon_require_eyes:I = 0x7f0a04be

.field public static final icon_sharing:I = 0x7f0a04bf

.field public static final icon_shield:I = 0x7f0a04c0

.field public static final icon_start:I = 0x7f0a04c1

.field public static final icon_start_frame:I = 0x7f0a04c2

.field public static final icon_text:I = 0x7f0a04c3

.field public static final icon_title:I = 0x7f0a04c4

.field public static final icon_trash_can:I = 0x7f0a04c5

.field public static final icon_view:I = 0x7f0a04c6

.field public static final id_btn_connect:I = 0x7f0a04c7

.field public static final id_btn_disconnect:I = 0x7f0a04c8

.field public static final id_btn_forget:I = 0x7f0a04c9

.field public static final id_btn_group_add_source:I = 0x7f0a04ca

.field public static final id_btn_refresh:I = 0x7f0a04cb

.field public static final id_btn_refresh_cancel:I = 0x7f0a04cc

.field public static final id_progress_group_scan:I = 0x7f0a04cd

.field public static final id_tv_groupid:I = 0x7f0a04ce

.field public static final id_tv_status:I = 0x7f0a04cf

.field public static final identity:I = 0x7f0a04d0

.field public static final identy:I = 0x7f0a04d1

.field public static final ifRoom:I = 0x7f0a04d2

.field public static final ignore:I = 0x7f0a04d3

.field public static final ignoreRequest:I = 0x7f0a04d4

.field public static final ignore_off:I = 0x7f0a04d5

.field public static final ignore_on:I = 0x7f0a04d6

.field public static final illustration_accessibility:I = 0x7f0a04d7

.field public static final illustration_default:I = 0x7f0a04d8

.field public static final illustration_frame:I = 0x7f0a04d9

.field public static final illustration_lottie:I = 0x7f0a04da

.field public static final illustration_lottie_portrait:I = 0x7f0a04db

.field public static final image:I = 0x7f0a04dc

.field public static final image1:I = 0x7f0a04dd

.field public static final image2:I = 0x7f0a04de

.field public static final image3:I = 0x7f0a04df

.field public static final imageBoxBattery:I = 0x7f0a04e0

.field public static final imageCloseAnc:I = 0x7f0a04e1

.field public static final imageLeftBattery:I = 0x7f0a04e2

.field public static final imageRightBattery:I = 0x7f0a04e3

.field public static final imageTransport:I = 0x7f0a04e4

.field public static final image_background:I = 0x7f0a04e5

.field public static final image_bullet:I = 0x7f0a04e6

.field public static final image_classic:I = 0x7f0a04e7

.field public static final image_current:I = 0x7f0a04e8

.field public static final image_layout:I = 0x7f0a04e9

.field public static final image_preference:I = 0x7f0a04ea

.field public static final image_view:I = 0x7f0a04eb

.field public static final image_viewstub_checkbox:I = 0x7f0a04ec

.field public static final image_wind:I = 0x7f0a04ed

.field public static final imageopenAnc:I = 0x7f0a04ee

.field public static final imei_label:I = 0x7f0a04ef

.field public static final imei_sv_label:I = 0x7f0a04f0

.field public static final imei_sv_value:I = 0x7f0a04f1

.field public static final imei_value:I = 0x7f0a04f2

.field public static final img_holder:I = 0x7f0a04f3

.field public static final img_interesting:I = 0x7f0a04f4

.field public static final img_item:I = 0x7f0a04f5

.field public static final img_limit:I = 0x7f0a04f6

.field public static final img_long_press:I = 0x7f0a04f7

.field public static final img_long_press_place_holder:I = 0x7f0a04f8

.field public static final img_sim_icon:I = 0x7f0a04f9

.field public static final ims_reg_state_label:I = 0x7f0a04fa

.field public static final ims_reg_state_value:I = 0x7f0a04fb

.field public static final indicator:I = 0x7f0a04fc

.field public static final indicator_container:I = 0x7f0a04fd

.field public static final indicator_left:I = 0x7f0a04fe

.field public static final indicator_locked_body:I = 0x7f0a04ff

.field public static final indicator_locked_header:I = 0x7f0a0500

.field public static final indicator_right:I = 0x7f0a0501

.field public static final indictor:I = 0x7f0a0502

.field public static final info:I = 0x7f0a0503

.field public static final info_button:I = 0x7f0a0504

.field public static final info_message_glasses:I = 0x7f0a0505

.field public static final info_message_less_secure:I = 0x7f0a0506

.field public static final info_message_looking:I = 0x7f0a0507

.field public static final info_message_require_eyes:I = 0x7f0a0508

.field public static final info_outline:I = 0x7f0a0509

.field public static final info_outline_layout:I = 0x7f0a050a

.field public static final info_reconnect:I = 0x7f0a050b

.field public static final info_row_less_secure:I = 0x7f0a050c

.field public static final info_row_require_eyes:I = 0x7f0a050d

.field public static final initiate_encrypt:I = 0x7f0a050e

.field public static final initiate_reset_network:I = 0x7f0a050f

.field public static final input_container:I = 0x7f0a0510

.field public static final input_method_function_list:I = 0x7f0a0511

.field public static final input_name:I = 0x7f0a0512

.field public static final inputmethod_pref:I = 0x7f0a0513

.field public static final inputmethod_settings:I = 0x7f0a0514

.field public static final inquiry:I = 0x7f0a0515

.field public static final install:I = 0x7f0a0516

.field public static final install_type:I = 0x7f0a0517

.field public static final installed:I = 0x7f0a0518

.field public static final instant_app_button_container:I = 0x7f0a0519

.field public static final instruction:I = 0x7f0a051a

.field public static final instruction_text:I = 0x7f0a051b

.field public static final intent:I = 0x7f0a051c

.field public static final interact_across_profiles_consent_dialog_summary:I = 0x7f0a051d

.field public static final interact_across_profiles_consent_dialog_title:I = 0x7f0a051e

.field public static final interact_across_profiles_dialog:I = 0x7f0a051f

.field public static final interact_across_profiles_dialog_layout:I = 0x7f0a0520

.field public static final invalid:I = 0x7f0a0521

.field public static final invisible:I = 0x7f0a0522

.field public static final inward:I = 0x7f0a0523

.field public static final ip_addr:I = 0x7f0a0524

.field public static final ip_fields:I = 0x7f0a0525

.field public static final ip_settings:I = 0x7f0a0526

.field public static final ipaddr:I = 0x7f0a0527

.field public static final ipaddress:I = 0x7f0a0528

.field public static final ipsec_ca_cert:I = 0x7f0a0529

.field public static final ipsec_identifier:I = 0x7f0a052a

.field public static final ipsec_identifier_bg:I = 0x7f0a052b

.field public static final ipsec_peer:I = 0x7f0a052c

.field public static final ipsec_psk:I = 0x7f0a052d

.field public static final ipsec_secret:I = 0x7f0a052e

.field public static final ipsec_secret_bg:I = 0x7f0a052f

.field public static final ipsec_server_cert:I = 0x7f0a0530

.field public static final ipsec_user:I = 0x7f0a0531

.field public static final ipsec_user_cert:I = 0x7f0a0532

.field public static final italic:I = 0x7f0a0533

.field public static final item:I = 0x7f0a0534

.field public static final item_both:I = 0x7f0a0535

.field public static final item_icon:I = 0x7f0a0536

.field public static final item_layout:I = 0x7f0a0537

.field public static final item_left:I = 0x7f0a0538

.field public static final item_name:I = 0x7f0a0539

.field public static final item_page_2_1:I = 0x7f0a053a

.field public static final item_page_2_2:I = 0x7f0a053b

.field public static final item_page_2_3:I = 0x7f0a053c

.field public static final item_right:I = 0x7f0a053d

.field public static final item_textview:I = 0x7f0a053e

.field public static final item_touch_helper_previous_elevation:I = 0x7f0a053f

.field public static final item_view:I = 0x7f0a0540

.field public static final iv_app_icon:I = 0x7f0a0541

.field public static final iv_arrow:I = 0x7f0a0542

.field public static final iv_codec:I = 0x7f0a0543

.field public static final iv_connect_state:I = 0x7f0a0544

.field public static final iv_detail_icon:I = 0x7f0a0545

.field public static final iv_device_icon:I = 0x7f0a0546

.field public static final iv_font_weight_heavy:I = 0x7f0a0547

.field public static final iv_font_weight_light:I = 0x7f0a0548

.field public static final iv_item:I = 0x7f0a0549

.field public static final iv_sos_contact_call_first:I = 0x7f0a054a

.field public static final iv_sos_contact_call_second:I = 0x7f0a054b

.field public static final iv_sos_contact_call_third:I = 0x7f0a054c

.field public static final iv_sos_el:I = 0x7f0a054d

.field public static final iv_state_icon_both:I = 0x7f0a054e

.field public static final iv_state_icon_left:I = 0x7f0a054f

.field public static final iv_state_icon_right:I = 0x7f0a0550

.field public static final jobscheduler:I = 0x7f0a0551

.field public static final jumpToEnd:I = 0x7f0a0552

.field public static final jumpToStart:I = 0x7f0a0553

.field public static final keep:I = 0x7f0a0554

.field public static final key_click_bottom:I = 0x7f0a0555

.field public static final key_long_press_bottom:I = 0x7f0a0556

.field public static final key_power_click:I = 0x7f0a0557

.field public static final key_power_double_click:I = 0x7f0a0558

.field public static final key_settings:I = 0x7f0a0559

.field public static final key_settings_preview_action:I = 0x7f0a055a

.field public static final key_settings_preview_background:I = 0x7f0a055b

.field public static final key_settings_preview_scrollview:I = 0x7f0a055c

.field public static final key_three_gesture:I = 0x7f0a055d

.field public static final key_three_long_press:I = 0x7f0a055e

.field public static final keyboard:I = 0x7f0a055f

.field public static final knock_gesture_v:I = 0x7f0a0560

.field public static final l10nWarn:I = 0x7f0a0561

.field public static final l2tp:I = 0x7f0a0562

.field public static final l2tp_secret:I = 0x7f0a0563

.field public static final l_adbwirelessdialog:I = 0x7f0a0564

.field public static final l_anonymous:I = 0x7f0a0565

.field public static final l_apinfo:I = 0x7f0a0566

.field public static final l_ca_cert:I = 0x7f0a0567

.field public static final l_domain:I = 0x7f0a0568

.field public static final l_encryption:I = 0x7f0a0569

.field public static final l_highlight:I = 0x7f0a056a

.field public static final l_identity:I = 0x7f0a056b

.field public static final l_info_reconnect:I = 0x7f0a056c

.field public static final l_method:I = 0x7f0a056d

.field public static final l_ocsp:I = 0x7f0a056e

.field public static final l_pairing_failed:I = 0x7f0a056f

.field public static final l_pairing_six_digit:I = 0x7f0a0570

.field public static final l_password:I = 0x7f0a0571

.field public static final l_password_layout:I = 0x7f0a0572

.field public static final l_password_text:I = 0x7f0a0573

.field public static final l_phase2:I = 0x7f0a0574

.field public static final l_privacy_settings:I = 0x7f0a0575

.field public static final l_privacy_settings_fields:I = 0x7f0a0576

.field public static final l_qrcode_pairing_failed:I = 0x7f0a0577

.field public static final l_security:I = 0x7f0a0578

.field public static final l_show_password_img:I = 0x7f0a0579

.field public static final l_signal:I = 0x7f0a057a

.field public static final l_sim:I = 0x7f0a057b

.field public static final l_sim_card:I = 0x7f0a057c

.field public static final l_title:I = 0x7f0a057d

.field public static final l_user_cert:I = 0x7f0a057e

.field public static final l_wifidialog:I = 0x7f0a057f

.field public static final label:I = 0x7f0a0580

.field public static final label2:I = 0x7f0a0581

.field public static final label_bar:I = 0x7f0a0582

.field public static final label_bottom:I = 0x7f0a0583

.field public static final label_end:I = 0x7f0a0584

.field public static final label_frame:I = 0x7f0a0585

.field public static final label_group:I = 0x7f0a0586

.field public static final label_layout:I = 0x7f0a0587

.field public static final label_middle:I = 0x7f0a0588

.field public static final label_start:I = 0x7f0a0589

.field public static final label_top:I = 0x7f0a058a

.field public static final labeled:I = 0x7f0a058b

.field public static final language_settings:I = 0x7f0a058c

.field public static final large:I = 0x7f0a058d

.field public static final large_icon:I = 0x7f0a058e

.field public static final larger:I = 0x7f0a058f

.field public static final last_rank:I = 0x7f0a0590

.field public static final last_time_used:I = 0x7f0a0591

.field public static final last_updated:I = 0x7f0a0592

.field public static final latest_area_info_label:I = 0x7f0a0593

.field public static final latest_area_info_value:I = 0x7f0a0594

.field public static final launch:I = 0x7f0a0595

.field public static final launch_mdp_app_button:I = 0x7f0a0596

.field public static final launcher_settings:I = 0x7f0a0597

.field public static final layout:I = 0x7f0a0598

.field public static final layoutDivider:I = 0x7f0a0599

.field public static final layoutOut:I = 0x7f0a059a

.field public static final layout_app_locale_details:I = 0x7f0a059b

.field public static final layout_content:I = 0x7f0a059c

.field public static final layout_icon:I = 0x7f0a059d

.field public static final layout_l_r:I = 0x7f0a059e

.field public static final layout_left:I = 0x7f0a059f

.field public static final layout_middle:I = 0x7f0a05a0

.field public static final layout_result:I = 0x7f0a05a1

.field public static final layout_right:I = 0x7f0a05a2

.field public static final layout_wifi_share_qrcode:I = 0x7f0a05a3

.field public static final lease_desc:I = 0x7f0a05a4

.field public static final lease_expiry:I = 0x7f0a05a5

.field public static final lease_package:I = 0x7f0a05a6

.field public static final led_color_image:I = 0x7f0a05a7

.field public static final led_settings:I = 0x7f0a05a8

.field public static final left:I = 0x7f0a05a9

.field public static final leftBattery:I = 0x7f0a05aa

.field public static final leftSpacer:I = 0x7f0a05ab

.field public static final leftToRight:I = 0x7f0a05ac

.field public static final left_button:I = 0x7f0a05ad

.field public static final left_shoulder_key:I = 0x7f0a05ae

.field public static final left_text:I = 0x7f0a05af

.field public static final level:I = 0x7f0a05b0

.field public static final level_layout:I = 0x7f0a05b1

.field public static final level_text:I = 0x7f0a05b2

.field public static final lib:I = 0x7f0a05b3

.field public static final light_mode_enable:I = 0x7f0a05b4

.field public static final light_mode_outer_view:I = 0x7f0a05b5

.field public static final light_mode_parent:I = 0x7f0a05b6

.field public static final light_mode_text:I = 0x7f0a05b7

.field public static final light_mode_view:I = 0x7f0a05b8

.field public static final line:I = 0x7f0a05b9

.field public static final line1:I = 0x7f0a05ba

.field public static final line2:I = 0x7f0a05bb

.field public static final line3:I = 0x7f0a05bc

.field public static final line_layout:I = 0x7f0a05bd

.field public static final linear:I = 0x7f0a05be

.field public static final linear_layout:I = 0x7f0a05bf

.field public static final link_text:I = 0x7f0a05c0

.field public static final link_turbo_app:I = 0x7f0a05c1

.field public static final list:I = 0x7f0a05c2

.field public static final listContainer:I = 0x7f0a05c3

.field public static final listHeader:I = 0x7f0a05c4

.field public static final listMode:I = 0x7f0a05c5

.field public static final list_container:I = 0x7f0a05c6

.field public static final list_item:I = 0x7f0a05c7

.field public static final list_view:I = 0x7f0a05c8

.field public static final listview:I = 0x7f0a05c9

.field public static final ll_app_notification_list_container:I = 0x7f0a05ca

.field public static final ll_app_usage_list_container:I = 0x7f0a05cb

.field public static final ll_font_weight:I = 0x7f0a05cc

.field public static final ll_normal_container:I = 0x7f0a05cd

.field public static final ll_normal_day_time_set:I = 0x7f0a05ce

.field public static final ll_switch:I = 0x7f0a05cf

.field public static final ll_week_container:I = 0x7f0a05d0

.field public static final ll_week_day_time_set:I = 0x7f0a05d1

.field public static final ll_weekday:I = 0x7f0a05d2

.field public static final ll_weekend:I = 0x7f0a05d3

.field public static final loading_container:I = 0x7f0a05d4

.field public static final loading_progress:I = 0x7f0a05d5

.field public static final loading_progress_up:I = 0x7f0a05d6

.field public static final loading_text:I = 0x7f0a05d7

.field public static final locale:I = 0x7f0a05d8

.field public static final locale_picker_fragment:I = 0x7f0a05d9

.field public static final location_settings:I = 0x7f0a05da

.field public static final lockPattern:I = 0x7f0a05db

.field public static final lock_none:I = 0x7f0a05dc

.field public static final lock_password:I = 0x7f0a05dd

.field public static final lock_pin:I = 0x7f0a05de

.field public static final lock_screen_notification_card:I = 0x7f0a05df

.field public static final lockpattern_framelayout:I = 0x7f0a05e0

.field public static final lockscreen_remote_input:I = 0x7f0a05e1

.field public static final login:I = 0x7f0a05e2

.field public static final login_account:I = 0x7f0a05e3

.field public static final logo:I = 0x7f0a05e4

.field public static final long_press_summary:I = 0x7f0a05e5

.field public static final long_press_title:I = 0x7f0a05e6

.field public static final longer:I = 0x7f0a05e7

.field public static final lottie_layer_name:I = 0x7f0a05e8

.field public static final lottie_recycler:I = 0x7f0a05e9

.field public static final lottie_video:I = 0x7f0a05ea

.field public static final lottie_view:I = 0x7f0a05eb

.field public static final lottie_viewstub_checkbox:I = 0x7f0a05ec

.field public static final low:I = 0x7f0a05ed

.field public static final low_tick:I = 0x7f0a05ee

.field public static final ltr:I = 0x7f0a05ef

.field public static final lunarModePanel:I = 0x7f0a05f0

.field public static final lunarText:I = 0x7f0a05f1

.field public static final lunar_button:I = 0x7f0a05f2

.field public static final lunar_layout:I = 0x7f0a05f3

.field public static final lunar_text:I = 0x7f0a05f4

.field public static final lyt_btn_back:I = 0x7f0a05f5

.field public static final lyt_btn_next:I = 0x7f0a05f6

.field public static final macaddr:I = 0x7f0a05f7

.field public static final main_blue_point:I = 0x7f0a05f8

.field public static final main_clear_container:I = 0x7f0a05f9

.field public static final main_clear_scrollview:I = 0x7f0a05fa

.field public static final main_content:I = 0x7f0a05fb

.field public static final main_content_scrollable_container:I = 0x7f0a05fc

.field public static final main_frame:I = 0x7f0a05fd

.field public static final main_switch_bar:I = 0x7f0a05fe

.field public static final main_text:I = 0x7f0a05ff

.field public static final mainlayout:I = 0x7f0a0600

.field public static final mall_card:I = 0x7f0a0601

.field public static final mall_card_title:I = 0x7f0a0602

.field public static final manage_xiaomi_router:I = 0x7f0a0603

.field public static final manufacturer_settings:I = 0x7f0a0604

.field public static final mask:I = 0x7f0a0605

.field public static final masked:I = 0x7f0a0606

.field public static final material_clock_display:I = 0x7f0a0607

.field public static final material_clock_face:I = 0x7f0a0608

.field public static final material_clock_hand:I = 0x7f0a0609

.field public static final material_clock_period_am_button:I = 0x7f0a060a

.field public static final material_clock_period_pm_button:I = 0x7f0a060b

.field public static final material_clock_period_toggle:I = 0x7f0a060c

.field public static final material_hour_text_input:I = 0x7f0a060d

.field public static final material_hour_tv:I = 0x7f0a060e

.field public static final material_label:I = 0x7f0a060f

.field public static final material_minute_text_input:I = 0x7f0a0610

.field public static final material_minute_tv:I = 0x7f0a0611

.field public static final material_textinput_timepicker:I = 0x7f0a0612

.field public static final material_timepicker_cancel_button:I = 0x7f0a0613

.field public static final material_timepicker_container:I = 0x7f0a0614

.field public static final material_timepicker_edit_text:I = 0x7f0a0615

.field public static final material_timepicker_mode_button:I = 0x7f0a0616

.field public static final material_timepicker_ok_button:I = 0x7f0a0617

.field public static final material_timepicker_view:I = 0x7f0a0618

.field public static final material_value_index:I = 0x7f0a0619

.field public static final matrix:I = 0x7f0a061a

.field public static final media_actions:I = 0x7f0a061b

.field public static final media_controller_compat_view_tag:I = 0x7f0a061c

.field public static final media_section:I = 0x7f0a061d

.field public static final medium:I = 0x7f0a061e

.field public static final meid_number_label:I = 0x7f0a061f

.field public static final meid_number_value:I = 0x7f0a0620

.field public static final meid_settings:I = 0x7f0a0621

.field public static final memory:I = 0x7f0a0622

.field public static final menu_point:I = 0x7f0a0623

.field public static final message:I = 0x7f0a0624

.field public static final message_below_pin:I = 0x7f0a0625

.field public static final message_container:I = 0x7f0a0626

.field public static final message_content:I = 0x7f0a0627

.field public static final message_in_control:I = 0x7f0a0628

.field public static final message_status:I = 0x7f0a0629

.field public static final message_text:I = 0x7f0a062a

.field public static final message_text_and_info:I = 0x7f0a062b

.field public static final metered_settings:I = 0x7f0a062c

.field public static final metered_settings_fields:I = 0x7f0a062d

.field public static final metered_settings_title:I = 0x7f0a062e

.field public static final method:I = 0x7f0a062f

.field public static final mi2_security:I = 0x7f0a0630

.field public static final mi_account_settings:I = 0x7f0a0631

.field public static final mi_wallet_payment:I = 0x7f0a0632

.field public static final micloud_settings:I = 0x7f0a0633

.field public static final middle:I = 0x7f0a0634

.field public static final middleground_layout:I = 0x7f0a0635

.field public static final miheadset_checklist:I = 0x7f0a0636

.field public static final miheadset_key_close:I = 0x7f0a0637

.field public static final miheadset_key_close_checkbox:I = 0x7f0a0638

.field public static final miheadset_key_close_icon:I = 0x7f0a0639

.field public static final miheadset_key_close_summary:I = 0x7f0a063a

.field public static final miheadset_key_close_title:I = 0x7f0a063b

.field public static final miheadset_key_openAnc:I = 0x7f0a063c

.field public static final miheadset_key_openAnc_checkbox:I = 0x7f0a063d

.field public static final miheadset_key_openAnc_icon:I = 0x7f0a063e

.field public static final miheadset_key_openAnc_summary:I = 0x7f0a063f

.field public static final miheadset_key_openAnc_title:I = 0x7f0a0640

.field public static final miheadset_key_transparent:I = 0x7f0a0641

.field public static final miheadset_key_transparent_checkbox:I = 0x7f0a0642

.field public static final miheadset_key_transparent_icon:I = 0x7f0a0643

.field public static final miheadset_key_transparent_summary:I = 0x7f0a0644

.field public static final miheadset_key_transparent_title:I = 0x7f0a0645

.field public static final mimoney_settings:I = 0x7f0a0646

.field public static final min_number_label:I = 0x7f0a0647

.field public static final min_number_value:I = 0x7f0a0648

.field public static final mini:I = 0x7f0a0649

.field public static final miniLabel:I = 0x7f0a064a

.field public static final minute:I = 0x7f0a064b

.field public static final mirrored_text_group:I = 0x7f0a064c

.field public static final miui_band:I = 0x7f0a064d

.field public static final miui_card_view:I = 0x7f0a064e

.field public static final miui_face_input_camera_preview:I = 0x7f0a064f

.field public static final miui_face_input_camera_preview_framelayout:I = 0x7f0a0650

.field public static final miui_face_input_camera_preview_second_coverd:I = 0x7f0a0651

.field public static final miui_face_input_detect:I = 0x7f0a0652

.field public static final miui_face_input_first_suggestion:I = 0x7f0a0653

.field public static final miui_face_input_grid:I = 0x7f0a0654

.field public static final miui_face_input_introduction_title:I = 0x7f0a0655

.field public static final miui_face_input_introduction_video:I = 0x7f0a0656

.field public static final miui_face_input_nextorsuccess_button:I = 0x7f0a0657

.field public static final miui_face_input_progress_circle:I = 0x7f0a0658

.field public static final miui_face_input_prompt_title:I = 0x7f0a0659

.field public static final miui_face_input_prompt_video:I = 0x7f0a065a

.field public static final miui_face_input_success_image:I = 0x7f0a065b

.field public static final miui_face_input_success_message:I = 0x7f0a065c

.field public static final miui_face_input_success_title:I = 0x7f0a065d

.field public static final miui_face_input_success_video:I = 0x7f0a065e

.field public static final miui_face_input_suggestion_video:I = 0x7f0a065f

.field public static final miui_face_input_suggestion_video_image:I = 0x7f0a0660

.field public static final miui_face_input_title:I = 0x7f0a0661

.field public static final miui_face_recoginition_intorduction_next:I = 0x7f0a0662

.field public static final miui_lab:I = 0x7f0a0663

.field public static final miui_lab_sv:I = 0x7f0a0664

.field public static final miui_logo_view:I = 0x7f0a0665

.field public static final miui_version_card_view:I = 0x7f0a0666

.field public static final miui_version_text:I = 0x7f0a0667

.field public static final miuix_animation_tag_foreground_color:I = 0x7f0a0668

.field public static final miuix_animation_tag_init_layout:I = 0x7f0a0669

.field public static final miuix_animation_tag_is_dragging:I = 0x7f0a066a

.field public static final miuix_animation_tag_listview_pos:I = 0x7f0a066b

.field public static final miuix_animation_tag_set_height:I = 0x7f0a066c

.field public static final miuix_animation_tag_set_width:I = 0x7f0a066d

.field public static final miuix_animation_tag_touch_listener:I = 0x7f0a066e

.field public static final miuix_animation_tag_view_hover_corners:I = 0x7f0a066f

.field public static final miuix_animation_tag_view_touch_corners:I = 0x7f0a0670

.field public static final miuix_animation_tag_view_touch_padding_rect:I = 0x7f0a0671

.field public static final miuix_animation_tag_view_touch_rect:I = 0x7f0a0672

.field public static final miuix_animation_tag_view_touch_rect_gravity:I = 0x7f0a0673

.field public static final miuix_animation_tag_view_touch_rect_location_mode:I = 0x7f0a0674

.field public static final miuix_animation_tag_view_touch_rect_offset_x:I = 0x7f0a0675

.field public static final miuix_animation_tag_view_touch_rect_offset_y:I = 0x7f0a0676

.field public static final miuix_appcompat_floating_window_index:I = 0x7f0a0677

.field public static final miuix_appcompat_is_floating_window_anim_over:I = 0x7f0a0678

.field public static final mobile_network_settings:I = 0x7f0a0679

.field public static final modify_backup_pw:I = 0x7f0a067a

.field public static final month:I = 0x7f0a067b

.field public static final month_data_traffic:I = 0x7f0a067c

.field public static final month_grid:I = 0x7f0a067d

.field public static final month_navigation_bar:I = 0x7f0a067e

.field public static final month_navigation_fragment_toggle:I = 0x7f0a067f

.field public static final month_navigation_next:I = 0x7f0a0680

.field public static final month_navigation_previous:I = 0x7f0a0681

.field public static final month_title:I = 0x7f0a0682

.field public static final month_total:I = 0x7f0a0683

.field public static final more:I = 0x7f0a0684

.field public static final more_info:I = 0x7f0a0685

.field public static final motion_base:I = 0x7f0a0686

.field public static final mppe:I = 0x7f0a0687

.field public static final mr_art:I = 0x7f0a0688

.field public static final mr_cast_checkbox:I = 0x7f0a0689

.field public static final mr_cast_close_button:I = 0x7f0a068a

.field public static final mr_cast_divider:I = 0x7f0a068b

.field public static final mr_cast_group_icon:I = 0x7f0a068c

.field public static final mr_cast_group_name:I = 0x7f0a068d

.field public static final mr_cast_group_progress_bar:I = 0x7f0a068e

.field public static final mr_cast_header_name:I = 0x7f0a068f

.field public static final mr_cast_list:I = 0x7f0a0690

.field public static final mr_cast_meta_art:I = 0x7f0a0691

.field public static final mr_cast_meta_background:I = 0x7f0a0692

.field public static final mr_cast_meta_black_scrim:I = 0x7f0a0693

.field public static final mr_cast_meta_subtitle:I = 0x7f0a0694

.field public static final mr_cast_meta_title:I = 0x7f0a0695

.field public static final mr_cast_mute_button:I = 0x7f0a0696

.field public static final mr_cast_route_icon:I = 0x7f0a0697

.field public static final mr_cast_route_name:I = 0x7f0a0698

.field public static final mr_cast_route_progress_bar:I = 0x7f0a0699

.field public static final mr_cast_stop_button:I = 0x7f0a069a

.field public static final mr_cast_volume_layout:I = 0x7f0a069b

.field public static final mr_cast_volume_slider:I = 0x7f0a069c

.field public static final mr_chooser_list:I = 0x7f0a069d

.field public static final mr_chooser_route_desc:I = 0x7f0a069e

.field public static final mr_chooser_route_icon:I = 0x7f0a069f

.field public static final mr_chooser_route_name:I = 0x7f0a06a0

.field public static final mr_chooser_route_progress_bar:I = 0x7f0a06a1

.field public static final mr_chooser_title:I = 0x7f0a06a2

.field public static final mr_close:I = 0x7f0a06a3

.field public static final mr_control_divider:I = 0x7f0a06a4

.field public static final mr_control_playback_ctrl:I = 0x7f0a06a5

.field public static final mr_control_subtitle:I = 0x7f0a06a6

.field public static final mr_control_title:I = 0x7f0a06a7

.field public static final mr_control_title_container:I = 0x7f0a06a8

.field public static final mr_custom_control:I = 0x7f0a06a9

.field public static final mr_default_control:I = 0x7f0a06aa

.field public static final mr_dialog_area:I = 0x7f0a06ab

.field public static final mr_expandable_area:I = 0x7f0a06ac

.field public static final mr_group_expand_collapse:I = 0x7f0a06ad

.field public static final mr_group_volume_route_name:I = 0x7f0a06ae

.field public static final mr_media_main_control:I = 0x7f0a06af

.field public static final mr_name:I = 0x7f0a06b0

.field public static final mr_picker_close_button:I = 0x7f0a06b1

.field public static final mr_picker_header_name:I = 0x7f0a06b2

.field public static final mr_picker_list:I = 0x7f0a06b3

.field public static final mr_picker_route_icon:I = 0x7f0a06b4

.field public static final mr_picker_route_name:I = 0x7f0a06b5

.field public static final mr_picker_route_progress_bar:I = 0x7f0a06b6

.field public static final mr_playback_control:I = 0x7f0a06b7

.field public static final mr_title_bar:I = 0x7f0a06b8

.field public static final mr_volume_control:I = 0x7f0a06b9

.field public static final mr_volume_group_list:I = 0x7f0a06ba

.field public static final mr_volume_item_icon:I = 0x7f0a06bb

.field public static final mr_volume_slider:I = 0x7f0a06bc

.field public static final msd_install_cancel:I = 0x7f0a06bd

.field public static final msd_install_download_summary:I = 0x7f0a06be

.field public static final msd_install_error_title:I = 0x7f0a06bf

.field public static final msd_install_icon:I = 0x7f0a06c0

.field public static final msd_install_icon_1:I = 0x7f0a06c1

.field public static final msd_install_magnifier:I = 0x7f0a06c2

.field public static final msd_install_model:I = 0x7f0a06c3

.field public static final msd_install_next:I = 0x7f0a06c4

.field public static final msd_install_retry:I = 0x7f0a06c5

.field public static final msd_install_succeed:I = 0x7f0a06c6

.field public static final msd_install_summary:I = 0x7f0a06c7

.field public static final msd_install_summary_1:I = 0x7f0a06c8

.field public static final msg:I = 0x7f0a06c9

.field public static final msim_settings:I = 0x7f0a06ca

.field public static final mtrl_anchor_parent:I = 0x7f0a06cb

.field public static final mtrl_calendar_day_selector_frame:I = 0x7f0a06cc

.field public static final mtrl_calendar_days_of_week:I = 0x7f0a06cd

.field public static final mtrl_calendar_frame:I = 0x7f0a06ce

.field public static final mtrl_calendar_main_pane:I = 0x7f0a06cf

.field public static final mtrl_calendar_months:I = 0x7f0a06d0

.field public static final mtrl_calendar_selection_frame:I = 0x7f0a06d1

.field public static final mtrl_calendar_text_input_frame:I = 0x7f0a06d2

.field public static final mtrl_calendar_year_selector_frame:I = 0x7f0a06d3

.field public static final mtrl_card_checked_layer_id:I = 0x7f0a06d4

.field public static final mtrl_child_content_container:I = 0x7f0a06d5

.field public static final mtrl_internal_children_alpha_tag:I = 0x7f0a06d6

.field public static final mtrl_motion_snapshot_view:I = 0x7f0a06d7

.field public static final mtrl_picker_fullscreen:I = 0x7f0a06d8

.field public static final mtrl_picker_header:I = 0x7f0a06d9

.field public static final mtrl_picker_header_selection_text:I = 0x7f0a06da

.field public static final mtrl_picker_header_title_and_selection:I = 0x7f0a06db

.field public static final mtrl_picker_header_toggle:I = 0x7f0a06dc

.field public static final mtrl_picker_text_input_date:I = 0x7f0a06dd

.field public static final mtrl_picker_text_input_range_end:I = 0x7f0a06de

.field public static final mtrl_picker_text_input_range_start:I = 0x7f0a06df

.field public static final mtrl_picker_title_text:I = 0x7f0a06e0

.field public static final mtrl_view_tag_bottom_padding:I = 0x7f0a06e1

.field public static final multi_face_name_edit:I = 0x7f0a06e2

.field public static final multi_face_name_text:I = 0x7f0a06e3

.field public static final multiple_networks:I = 0x7f0a06e4

.field public static final multiply:I = 0x7f0a06e5

.field public static final mute_button:I = 0x7f0a06e6

.field public static final my_device:I = 0x7f0a06e7

.field public static final my_device_name:I = 0x7f0a06e8

.field public static final my_progressBar:I = 0x7f0a06e9

.field public static final name:I = 0x7f0a06ea

.field public static final name_edittext:I = 0x7f0a06eb

.field public static final name_label:I = 0x7f0a06ec

.field public static final nature_color:I = 0x7f0a06ed

.field public static final navigation_bar_item_active_indicator_view:I = 0x7f0a06ee

.field public static final navigation_bar_item_icon_container:I = 0x7f0a06ef

.field public static final navigation_bar_item_icon_view:I = 0x7f0a06f0

.field public static final navigation_bar_item_labels_group:I = 0x7f0a06f1

.field public static final navigation_bar_item_large_label_view:I = 0x7f0a06f2

.field public static final navigation_bar_item_small_label_view:I = 0x7f0a06f3

.field public static final navigation_guide:I = 0x7f0a06f4

.field public static final navigation_handle:I = 0x7f0a06f5

.field public static final navigation_header_container:I = 0x7f0a06f6

.field public static final negative_btn:I = 0x7f0a06f7

.field public static final nestedheaderlayout:I = 0x7f0a06f8

.field public static final network_description_grid_linearlayout:I = 0x7f0a06f9

.field public static final network_options:I = 0x7f0a06fa

.field public static final network_prefix_length:I = 0x7f0a06fb

.field public static final network_request_summary_text:I = 0x7f0a06fc

.field public static final network_request_title_progress:I = 0x7f0a06fd

.field public static final network_request_title_text:I = 0x7f0a06fe

.field public static final network_state:I = 0x7f0a06ff

.field public static final networkid:I = 0x7f0a0700

.field public static final neutral_btn:I = 0x7f0a0701

.field public static final never:I = 0x7f0a0702

.field public static final new_backup_pw:I = 0x7f0a0703

.field public static final new_fingerprint_button:I = 0x7f0a0704

.field public static final new_fingerprint_cancel:I = 0x7f0a0705

.field public static final new_fingerprint_instruction_img:I = 0x7f0a0706

.field public static final new_fingerprint_ok:I = 0x7f0a0707

.field public static final new_fingerprint_step_video:I = 0x7f0a0708

.field public static final new_fingerprint_top_text:I = 0x7f0a0709

.field public static final new_fingerprint_top_title:I = 0x7f0a070a

.field public static final next:I = 0x7f0a070b

.field public static final next_button:I = 0x7f0a070c

.field public static final next_global:I = 0x7f0a070d

.field public static final nfc_how_it_works_button:I = 0x7f0a070e

.field public static final nfc_how_it_works_content:I = 0x7f0a070f

.field public static final nfc_how_it_works_image:I = 0x7f0a0710

.field public static final nfc_how_it_works_image_text:I = 0x7f0a0711

.field public static final nfc_how_it_works_title:I = 0x7f0a0712

.field public static final nfc_payment_empty_text:I = 0x7f0a0713

.field public static final nfc_payment_pref:I = 0x7f0a0714

.field public static final nfc_payment_tap_image:I = 0x7f0a0715

.field public static final nine:I = 0x7f0a0716

.field public static final noScroll:I = 0x7f0a0717

.field public static final no_action_container:I = 0x7f0a0718

.field public static final no_ca_cert_warning:I = 0x7f0a0719

.field public static final no_cancel_mobile_plan:I = 0x7f0a071a

.field public static final no_domain_warning:I = 0x7f0a071b

.field public static final no_user_cert_warning:I = 0x7f0a071c

.field public static final none:I = 0x7f0a071d

.field public static final normal:I = 0x7f0a071e

.field public static final normal_list_container:I = 0x7f0a071f

.field public static final notch_style_show:I = 0x7f0a0720

.field public static final notch_style_show_container:I = 0x7f0a0721

.field public static final notch_style_show_image:I = 0x7f0a0722

.field public static final notch_style_status_bar_inside:I = 0x7f0a0723

.field public static final notch_style_status_bar_inside_container:I = 0x7f0a0724

.field public static final notch_style_status_bar_inside_image:I = 0x7f0a0725

.field public static final notification_background:I = 0x7f0a0726

.field public static final notification_button:I = 0x7f0a0727

.field public static final notification_center:I = 0x7f0a0728

.field public static final notification_extra:I = 0x7f0a0729

.field public static final notification_icon:I = 0x7f0a072a

.field public static final notification_list:I = 0x7f0a072b

.field public static final notification_list_wrapper:I = 0x7f0a072c

.field public static final notification_main_column:I = 0x7f0a072d

.field public static final notification_main_column_container:I = 0x7f0a072e

.field public static final notification_section:I = 0x7f0a072f

.field public static final notification_summary:I = 0x7f0a0730

.field public static final notification_text:I = 0x7f0a0731

.field public static final number:I = 0x7f0a0732

.field public static final number_label:I = 0x7f0a0733

.field public static final number_of_uris:I = 0x7f0a0734

.field public static final number_picker_input:I = 0x7f0a0735

.field public static final number_value:I = 0x7f0a0736

.field public static final ocsp:I = 0x7f0a0737

.field public static final off:I = 0x7f0a0738

.field public static final ok:I = 0x7f0a0739

.field public static final ok_account:I = 0x7f0a073a

.field public static final ok_button:I = 0x7f0a073b

.field public static final on:I = 0x7f0a073c

.field public static final on_switch:I = 0x7f0a073d

.field public static final one:I = 0x7f0a073e

.field public static final onehour:I = 0x7f0a073f

.field public static final op_icon:I = 0x7f0a0740

.field public static final op_name:I = 0x7f0a0741

.field public static final op_switch:I = 0x7f0a0742

.field public static final op_time:I = 0x7f0a0743

.field public static final opaque:I = 0x7f0a0744

.field public static final openAnc:I = 0x7f0a0745

.field public static final openAncImage:I = 0x7f0a0746

.field public static final operator_name_label:I = 0x7f0a0747

.field public static final operator_name_value:I = 0x7f0a0748

.field public static final operator_settings:I = 0x7f0a0749

.field public static final opt_in:I = 0x7f0a074a

.field public static final optional:I = 0x7f0a074b

.field public static final options:I = 0x7f0a074c

.field public static final options_ipsec_identity:I = 0x7f0a074d

.field public static final otaCard:I = 0x7f0a074e

.field public static final otaLayout:I = 0x7f0a074f

.field public static final ota_btn:I = 0x7f0a0750

.field public static final ota_remote_version:I = 0x7f0a0751

.field public static final other:I = 0x7f0a0752

.field public static final other_advanced_settings:I = 0x7f0a0753

.field public static final other_users_present:I = 0x7f0a0754

.field public static final outer_image_view:I = 0x7f0a0755

.field public static final outline:I = 0x7f0a0756

.field public static final outward:I = 0x7f0a0757

.field public static final overlay_see_more:I = 0x7f0a0758

.field public static final owner_info:I = 0x7f0a0759

.field public static final owner_info_edit_text:I = 0x7f0a075a

.field public static final owner_info_line_layout:I = 0x7f0a075b

.field public static final owner_info_nickname:I = 0x7f0a075c

.field public static final package_name:I = 0x7f0a075d

.field public static final packed:I = 0x7f0a075e

.field public static final page_indicator:I = 0x7f0a075f

.field public static final page_layout_scrollview:I = 0x7f0a0760

.field public static final pairing_accept_button:I = 0x7f0a0761

.field public static final pairing_caption:I = 0x7f0a0762

.field public static final pairing_code:I = 0x7f0a0763

.field public static final pairing_code_message:I = 0x7f0a0764

.field public static final pairing_decline_button:I = 0x7f0a0765

.field public static final pairing_failed_label:I = 0x7f0a0766

.field public static final pairing_group_message:I = 0x7f0a0767

.field public static final pairing_subhead:I = 0x7f0a0768

.field public static final palette_view:I = 0x7f0a0769

.field public static final panel_container:I = 0x7f0a076a

.field public static final panel_header:I = 0x7f0a076b

.field public static final panel_parent_layout:I = 0x7f0a076c

.field public static final panel_title:I = 0x7f0a076d

.field public static final parallax:I = 0x7f0a076e

.field public static final parent:I = 0x7f0a076f

.field public static final parentPanel:I = 0x7f0a0770

.field public static final parentRelative:I = 0x7f0a0771

.field public static final parent_matrix:I = 0x7f0a0772

.field public static final password:I = 0x7f0a0773

.field public static final passwordEntry:I = 0x7f0a0774

.field public static final password_account:I = 0x7f0a0775

.field public static final password_container:I = 0x7f0a0776

.field public static final password_entry:I = 0x7f0a0777

.field public static final password_label:I = 0x7f0a0778

.field public static final password_layout:I = 0x7f0a0779

.field public static final password_requirements_view:I = 0x7f0a077a

.field public static final password_scanner_button:I = 0x7f0a077b

.field public static final password_text:I = 0x7f0a077c

.field public static final password_toggle:I = 0x7f0a077d

.field public static final path:I = 0x7f0a077e

.field public static final pathRelative:I = 0x7f0a077f

.field public static final pattern_layout:I = 0x7f0a0780

.field public static final pb_progress:I = 0x7f0a0781

.field public static final peekHeight:I = 0x7f0a0782

.field public static final pen:I = 0x7f0a0783

.field public static final pen_image:I = 0x7f0a0784

.field public static final percent:I = 0x7f0a0785

.field public static final percentage_bar_chart:I = 0x7f0a0786

.field public static final performAction_description:I = 0x7f0a0787

.field public static final performAction_icon:I = 0x7f0a0788

.field public static final performAction_title:I = 0x7f0a0789

.field public static final perm_icon:I = 0x7f0a078a

.field public static final permissionDialog_description:I = 0x7f0a078b

.field public static final permissionDialog_disable_message:I = 0x7f0a078c

.field public static final permissionDialog_disable_title:I = 0x7f0a078d

.field public static final permissionDialog_icon:I = 0x7f0a078e

.field public static final permissionDialog_title:I = 0x7f0a078f

.field public static final permission_disable_cancel_button:I = 0x7f0a0790

.field public static final permission_disable_stop_button:I = 0x7f0a0791

.field public static final permission_enable_allow_button:I = 0x7f0a0792

.field public static final permission_enable_deny_button:I = 0x7f0a0793

.field public static final permission_enable_uninstall_button:I = 0x7f0a0794

.field public static final permission_group:I = 0x7f0a0795

.field public static final permission_list:I = 0x7f0a0796

.field public static final permissions_content:I = 0x7f0a0797

.field public static final permissions_icon:I = 0x7f0a0798

.field public static final permissions_summary:I = 0x7f0a0799

.field public static final permissions_text:I = 0x7f0a079a

.field public static final permissions_title:I = 0x7f0a079b

.field public static final personalize_title:I = 0x7f0a079c

.field public static final phase2:I = 0x7f0a079d

.field public static final phone_led_color:I = 0x7f0a079e

.field public static final phone_model:I = 0x7f0a079f

.field public static final phonebook_sharing_message_confirm_pin:I = 0x7f0a07a0

.field public static final phonebook_sharing_message_entry_pin:I = 0x7f0a07a1

.field public static final picAnimation:I = 0x7f0a07a2

.field public static final picker_container:I = 0x7f0a07a3

.field public static final pickers:I = 0x7f0a07a4

.field public static final pin:I = 0x7f0a07a5

.field public static final pin_values_hint:I = 0x7f0a07a6

.field public static final pingHostname:I = 0x7f0a07a7

.field public static final ping_test:I = 0x7f0a07a8

.field public static final pinned_header:I = 0x7f0a07a9

.field public static final pkg_list:I = 0x7f0a07aa

.field public static final pkgname:I = 0x7f0a07ab

.field public static final play_button:I = 0x7f0a07ac

.field public static final play_tips:I = 0x7f0a07ad

.field public static final popup_arrow:I = 0x7f0a07ae

.field public static final popup_gridview:I = 0x7f0a07af

.field public static final popup_led_gridview:I = 0x7f0a07b0

.field public static final popup_led_title_container:I = 0x7f0a07b1

.field public static final popup_title_container:I = 0x7f0a07b2

.field public static final port:I = 0x7f0a07b3

.field public static final position:I = 0x7f0a07b4

.field public static final positive_btn:I = 0x7f0a07b5

.field public static final postLayout:I = 0x7f0a07b6

.field public static final power:I = 0x7f0a07b7

.field public static final power_guide:I = 0x7f0a07b8

.field public static final power_mode_settings:I = 0x7f0a07b9

.field public static final pre_install_app_layout:I = 0x7f0a07ba

.field public static final pre_install_app_list:I = 0x7f0a07bb

.field public static final pre_install_category:I = 0x7f0a07bc

.field public static final pref_all:I = 0x7f0a07bd

.field public static final pref_left_button:I = 0x7f0a07be

.field public static final pref_radio:I = 0x7f0a07bf

.field public static final pref_right_button:I = 0x7f0a07c0

.field public static final pref_right_button1:I = 0x7f0a07c1

.field public static final preferD:I = 0x7f0a07c2

.field public static final preference_button:I = 0x7f0a07c3

.field public static final preference_container:I = 0x7f0a07c4

.field public static final preference_detail:I = 0x7f0a07c5

.field public static final preference_highlighted:I = 0x7f0a07c6

.field public static final preference_image:I = 0x7f0a07c7

.field public static final preference_title:I = 0x7f0a07c8

.field public static final prefs_container:I = 0x7f0a07c9

.field public static final prepend:I = 0x7f0a07ca

.field public static final preview:I = 0x7f0a07cb

.field public static final preview_image:I = 0x7f0a07cc

.field public static final preview_label:I = 0x7f0a07cd

.field public static final preview_pager:I = 0x7f0a07ce

.field public static final preview_placeholder:I = 0x7f0a07cf

.field public static final preview_text:I = 0x7f0a07d0

.field public static final preview_view:I = 0x7f0a07d1

.field public static final preview_viewport:I = 0x7f0a07d2

.field public static final preview_window:I = 0x7f0a07d3

.field public static final print_enable:I = 0x7f0a07d4

.field public static final print_enable_container:I = 0x7f0a07d5

.field public static final print_menu_item_add_printer:I = 0x7f0a07d6

.field public static final print_menu_item_search:I = 0x7f0a07d7

.field public static final print_menu_item_settings:I = 0x7f0a07d8

.field public static final priority_group:I = 0x7f0a07d9

.field public static final privacy_choose_icon:I = 0x7f0a07da

.field public static final privacy_password_choose_access_control_back:I = 0x7f0a07db

.field public static final privacy_password_choose_access_control_back_text:I = 0x7f0a07dc

.field public static final privacy_password_confirm_back:I = 0x7f0a07dd

.field public static final privacy_password_confirm_back_title:I = 0x7f0a07de

.field public static final privacy_password_footer_text:I = 0x7f0a07df

.field public static final privacy_password_forget_pattern:I = 0x7f0a07e0

.field public static final privacy_password_header_text:I = 0x7f0a07e1

.field public static final privacy_password_icon:I = 0x7f0a07e2

.field public static final privacy_password_icon_container:I = 0x7f0a07e3

.field public static final privacy_password_lockpatternView:I = 0x7f0a07e4

.field public static final privacy_password_more:I = 0x7f0a07e5

.field public static final privacy_password_setting:I = 0x7f0a07e6

.field public static final privacy_passwordheaderText:I = 0x7f0a07e7

.field public static final privacy_protection_settings:I = 0x7f0a07e8

.field public static final privacy_settings:I = 0x7f0a07e9

.field public static final privacy_settings_fields:I = 0x7f0a07ea

.field public static final privacy_settings_title:I = 0x7f0a07eb

.field public static final privacypassword:I = 0x7f0a07ec

.field public static final private_dns_help_info:I = 0x7f0a07ed

.field public static final private_dns_mode_off:I = 0x7f0a07ee

.field public static final private_dns_mode_opportunistic:I = 0x7f0a07ef

.field public static final private_dns_mode_provider:I = 0x7f0a07f0

.field public static final private_dns_mode_provider_hostname:I = 0x7f0a07f1

.field public static final private_dns_radio_group:I = 0x7f0a07f2

.field public static final prl_version_label:I = 0x7f0a07f3

.field public static final prl_version_value:I = 0x7f0a07f4

.field public static final process:I = 0x7f0a07f5

.field public static final profile_badge:I = 0x7f0a07f6

.field public static final profile_owner_warning:I = 0x7f0a07f7

.field public static final profile_spinner:I = 0x7f0a07f8

.field public static final profiles_label:I = 0x7f0a07f9

.field public static final profiles_section:I = 0x7f0a07fa

.field public static final progress:I = 0x7f0a07fb

.field public static final progressBar:I = 0x7f0a07fc

.field public static final progress_bar:I = 0x7f0a07fd

.field public static final progress_bar_animation:I = 0x7f0a07fe

.field public static final progress_bar_background:I = 0x7f0a07ff

.field public static final progress_circle:I = 0x7f0a0800

.field public static final progress_circle_container:I = 0x7f0a0801

.field public static final progress_circular:I = 0x7f0a0802

.field public static final progress_horizontal:I = 0x7f0a0803

.field public static final progress_info:I = 0x7f0a0804

.field public static final progress_percent:I = 0x7f0a0805

.field public static final prompt:I = 0x7f0a0806

.field public static final prompt1:I = 0x7f0a0807

.field public static final prompt2:I = 0x7f0a0808

.field public static final prompt3:I = 0x7f0a0809

.field public static final properPaddingViewGroup:I = 0x7f0a080a

.field public static final provision_activation:I = 0x7f0a080b

.field public static final provision_arrow_back:I = 0x7f0a080c

.field public static final provision_arrow_next:I = 0x7f0a080d

.field public static final provision_back_btn:I = 0x7f0a080e

.field public static final provision_bar:I = 0x7f0a080f

.field public static final provision_btn_back:I = 0x7f0a0810

.field public static final provision_btn_extra:I = 0x7f0a0811

.field public static final provision_container:I = 0x7f0a0812

.field public static final provision_global_back_btn:I = 0x7f0a0813

.field public static final provision_global_next_btn:I = 0x7f0a0814

.field public static final provision_lyt_btn:I = 0x7f0a0815

.field public static final provision_lyt_btn_back:I = 0x7f0a0816

.field public static final provision_lyt_btn_next:I = 0x7f0a0817

.field public static final provision_lyt_content:I = 0x7f0a0818

.field public static final provision_lyt_title:I = 0x7f0a0819

.field public static final provision_next:I = 0x7f0a081a

.field public static final provision_next_btn:I = 0x7f0a081b

.field public static final provision_page_layout:I = 0x7f0a081c

.field public static final provision_preview_img:I = 0x7f0a081d

.field public static final provision_preview_layout:I = 0x7f0a081e

.field public static final provision_skip_btn:I = 0x7f0a081f

.field public static final provision_sub_title:I = 0x7f0a0820

.field public static final provision_title:I = 0x7f0a0821

.field public static final provision_title_space:I = 0x7f0a0822

.field public static final proxy_exclusionlist:I = 0x7f0a0823

.field public static final proxy_fields:I = 0x7f0a0824

.field public static final proxy_hostname:I = 0x7f0a0825

.field public static final proxy_pac:I = 0x7f0a0826

.field public static final proxy_pac_field:I = 0x7f0a0827

.field public static final proxy_port:I = 0x7f0a0828

.field public static final proxy_settings:I = 0x7f0a0829

.field public static final proxy_settings_fields:I = 0x7f0a082a

.field public static final proxy_settings_title:I = 0x7f0a082b

.field public static final proxy_warning_limited_support:I = 0x7f0a082c

.field public static final purpose:I = 0x7f0a082d

.field public static final pvc_account_text:I = 0x7f0a082e

.field public static final pvc_add_account_info:I = 0x7f0a082f

.field public static final pvc_add_account_title:I = 0x7f0a0830

.field public static final pvc_add_account_title_content:I = 0x7f0a0831

.field public static final pvc_add_account_top_layout:I = 0x7f0a0832

.field public static final pvc_content:I = 0x7f0a0833

.field public static final pvc_exit_account_info:I = 0x7f0a0834

.field public static final pvc_icon:I = 0x7f0a0835

.field public static final qhd_img_view:I = 0x7f0a0836

.field public static final qhd_text_view:I = 0x7f0a0837

.field public static final qhd_text_view_summary:I = 0x7f0a0838

.field public static final qrcode:I = 0x7f0a0839

.field public static final qrcode_view:I = 0x7f0a083a

.field public static final qs_content:I = 0x7f0a083b

.field public static final qs_illustration:I = 0x7f0a083c

.field public static final radio:I = 0x7f0a083d

.field public static final radioButton:I = 0x7f0a083e

.field public static final radio_button_full_screen:I = 0x7f0a083f

.field public static final radio_button_image:I = 0x7f0a0840

.field public static final radio_button_virtual_keys:I = 0x7f0a0841

.field public static final radio_extra_widget:I = 0x7f0a0842

.field public static final radio_extra_widget_container:I = 0x7f0a0843

.field public static final radio_extra_widget_divider:I = 0x7f0a0844

.field public static final radio_group:I = 0x7f0a0845

.field public static final radio_preference_layout:I = 0x7f0a0846

.field public static final rank_name:I = 0x7f0a0847

.field public static final ranking_extra:I = 0x7f0a0848

.field public static final ratio:I = 0x7f0a0849

.field public static final rcv_custom_op:I = 0x7f0a084a

.field public static final re_download:I = 0x7f0a084b

.field public static final read_description:I = 0x7f0a084c

.field public static final read_icon:I = 0x7f0a084d

.field public static final read_title:I = 0x7f0a084e

.field public static final recently_dismissed_list:I = 0x7f0a084f

.field public static final recommend_layout:I = 0x7f0a0850

.field public static final recommend_tip:I = 0x7f0a0851

.field public static final recommend_title_tv:I = 0x7f0a0852

.field public static final recommend_view:I = 0x7f0a0853

.field public static final rectangles:I = 0x7f0a0854

.field public static final recyclerView:I = 0x7f0a0855

.field public static final recycler_view:I = 0x7f0a0856

.field public static final recyclerview:I = 0x7f0a0857

.field public static final red_dot:I = 0x7f0a0858

.field public static final red_point:I = 0x7f0a0859

.field public static final redact_sensitive:I = 0x7f0a085a

.field public static final refresh_anim:I = 0x7f0a085b

.field public static final refresh_anim_bg:I = 0x7f0a085c

.field public static final refresh_frame:I = 0x7f0a085d

.field public static final regulatoryInfo:I = 0x7f0a085e

.field public static final reject:I = 0x7f0a085f

.field public static final relativeBackground:I = 0x7f0a0860

.field public static final remain_time:I = 0x7f0a0861

.field public static final remember:I = 0x7f0a0862

.field public static final remote_input:I = 0x7f0a0863

.field public static final remote_input_progress:I = 0x7f0a0864

.field public static final remote_input_send:I = 0x7f0a0865

.field public static final remote_input_text:I = 0x7f0a0866

.field public static final remove:I = 0x7f0a0867

.field public static final remove_action_button:I = 0x7f0a0868

.field public static final remove_button:I = 0x7f0a0869

.field public static final rename:I = 0x7f0a086a

.field public static final renameCard:I = 0x7f0a086b

.field public static final renameEdit:I = 0x7f0a086c

.field public static final renameLayout:I = 0x7f0a086d

.field public static final reply_description:I = 0x7f0a086e

.field public static final reply_icon:I = 0x7f0a086f

.field public static final reply_title:I = 0x7f0a0870

.field public static final reply_view:I = 0x7f0a0871

.field public static final request_background_location_permission:I = 0x7f0a0872

.field public static final res:I = 0x7f0a0873

.field public static final reset_app_preferences:I = 0x7f0a0874

.field public static final reset_backup_pw_layout:I = 0x7f0a0875

.field public static final reset_button:I = 0x7f0a0876

.field public static final reset_confirm_directory_text:I = 0x7f0a0877

.field public static final reset_importance_button:I = 0x7f0a0878

.field public static final reset_network_confirm:I = 0x7f0a0879

.field public static final reset_network_subscription:I = 0x7f0a087a

.field public static final resolution_fhd:I = 0x7f0a087b

.field public static final resolution_qhd:I = 0x7f0a087c

.field public static final resource:I = 0x7f0a087d

.field public static final restart:I = 0x7f0a087e

.field public static final restricted_action:I = 0x7f0a087f

.field public static final restricted_icon:I = 0x7f0a0880

.field public static final restricted_lock_icon:I = 0x7f0a0881

.field public static final restricted_lock_icon_remote_input:I = 0x7f0a0882

.field public static final restricted_lock_root:I = 0x7f0a0883

.field public static final retry_btn:I = 0x7f0a0884

.field public static final reverse:I = 0x7f0a0885

.field public static final reverseSawtooth:I = 0x7f0a0886

.field public static final revert_button:I = 0x7f0a0887

.field public static final right:I = 0x7f0a0888

.field public static final rightBattery:I = 0x7f0a0889

.field public static final rightSpacer:I = 0x7f0a088a

.field public static final rightToLeft:I = 0x7f0a088b

.field public static final right_arrow:I = 0x7f0a088c

.field public static final right_button:I = 0x7f0a088d

.field public static final right_icon:I = 0x7f0a088e

.field public static final right_shoulder_key:I = 0x7f0a088f

.field public static final right_side:I = 0x7f0a0890

.field public static final right_spinner:I = 0x7f0a0891

.field public static final right_text:I = 0x7f0a0892

.field public static final ringer_section:I = 0x7f0a0893

.field public static final ringer_volume_settings:I = 0x7f0a0894

.field public static final ringtone_alarm:I = 0x7f0a0895

.field public static final ringtone_call:I = 0x7f0a0896

.field public static final ringtone_grid:I = 0x7f0a0897

.field public static final ringtone_grid_view:I = 0x7f0a0898

.field public static final ringtone_icon:I = 0x7f0a0899

.field public static final ringtone_notification:I = 0x7f0a089a

.field public static final ringtone_settings:I = 0x7f0a089b

.field public static final ringtone_settings_card:I = 0x7f0a089c

.field public static final ringtone_title:I = 0x7f0a089d

.field public static final ringtone_value:I = 0x7f0a089e

.field public static final rl_bg:I = 0x7f0a089f

.field public static final rl_container:I = 0x7f0a08a0

.field public static final rl_switch_container:I = 0x7f0a08a1

.field public static final roaming_state_label:I = 0x7f0a08a2

.field public static final roaming_state_value:I = 0x7f0a08a3

.field public static final root:I = 0x7f0a08a4

.field public static final rounded:I = 0x7f0a08a5

.field public static final routes:I = 0x7f0a08a6

.field public static final row_divider:I = 0x7f0a08a7

.field public static final row_index_key:I = 0x7f0a08a8

.field public static final row_view:I = 0x7f0a08a9

.field public static final rssi:I = 0x7f0a08aa

.field public static final rtl:I = 0x7f0a08ab

.field public static final rule_container:I = 0x7f0a08ac

.field public static final rule_item_ll:I = 0x7f0a08ad

.field public static final ruleitem:I = 0x7f0a08ae

.field public static final rulesummary:I = 0x7f0a08af

.field public static final ruletitle:I = 0x7f0a08b0

.field public static final running_processes:I = 0x7f0a08b1

.field public static final rx_link_speed:I = 0x7f0a08b2

.field public static final safety_emergency_settings:I = 0x7f0a08b3

.field public static final save:I = 0x7f0a08b4

.field public static final save_login:I = 0x7f0a08b5

.field public static final save_non_transition_alpha:I = 0x7f0a08b6

.field public static final save_overlay_view:I = 0x7f0a08b7

.field public static final sawtooth:I = 0x7f0a08b8

.field public static final scale:I = 0x7f0a08b9

.field public static final scan_links_progressbar:I = 0x7f0a08ba

.field public static final scan_list:I = 0x7f0a08bb

.field public static final scanning_progress:I = 0x7f0a08bc

.field public static final screen:I = 0x7f0a08bd

.field public static final screen_color_image:I = 0x7f0a08be

.field public static final screen_color_image_bg:I = 0x7f0a08bf

.field public static final screen_color_image_layout:I = 0x7f0a08c0

.field public static final screen_enhance_engine_top_summary:I = 0x7f0a08c1

.field public static final screen_enhance_engine_top_view:I = 0x7f0a08c2

.field public static final screen_lock_options:I = 0x7f0a08c3

.field public static final screen_on_group:I = 0x7f0a08c4

.field public static final screen_scrollview:I = 0x7f0a08c5

.field public static final screen_settings:I = 0x7f0a08c6

.field public static final screen_timeout_settings:I = 0x7f0a08c7

.field public static final screen_zoom_scrollview:I = 0x7f0a08c8

.field public static final scroll:I = 0x7f0a08c9

.field public static final scrollIndicatorDown:I = 0x7f0a08ca

.field public static final scrollIndicatorUp:I = 0x7f0a08cb

.field public static final scrollView:I = 0x7f0a08cc

.field public static final scroll_headers:I = 0x7f0a08cd

.field public static final scroll_layout:I = 0x7f0a08ce

.field public static final scroll_view:I = 0x7f0a08cf

.field public static final scrollable:I = 0x7f0a08d0

.field public static final scrollview:I = 0x7f0a08d1

.field public static final scrollviewaaa:I = 0x7f0a08d2

.field public static final searchActionMode_customFrameLayout:I = 0x7f0a08d3

.field public static final search_action_bar:I = 0x7f0a08d4

.field public static final search_action_bar_title:I = 0x7f0a08d5

.field public static final search_action_bar_two_pane:I = 0x7f0a08d6

.field public static final search_badge:I = 0x7f0a08d7

.field public static final search_bar:I = 0x7f0a08d8

.field public static final search_bar_title:I = 0x7f0a08d9

.field public static final search_button:I = 0x7f0a08da

.field public static final search_close_btn:I = 0x7f0a08db

.field public static final search_container:I = 0x7f0a08dc

.field public static final search_domains:I = 0x7f0a08dd

.field public static final search_edit_frame:I = 0x7f0a08de

.field public static final search_empty:I = 0x7f0a08df

.field public static final search_go_btn:I = 0x7f0a08e0

.field public static final search_history:I = 0x7f0a08e1

.field public static final search_history_clear_tv:I = 0x7f0a08e2

.field public static final search_history_fl:I = 0x7f0a08e3

.field public static final search_history_tv:I = 0x7f0a08e4

.field public static final search_loading:I = 0x7f0a08e5

.field public static final search_mag_icon:I = 0x7f0a08e6

.field public static final search_mask:I = 0x7f0a08e7

.field public static final search_mask_vs:I = 0x7f0a08e8

.field public static final search_panel:I = 0x7f0a08e9

.field public static final search_plate:I = 0x7f0a08ea

.field public static final search_result:I = 0x7f0a08eb

.field public static final search_result_ll:I = 0x7f0a08ec

.field public static final search_src_text:I = 0x7f0a08ed

.field public static final search_text_cancel:I = 0x7f0a08ee

.field public static final search_view:I = 0x7f0a08ef

.field public static final search_voice_btn:I = 0x7f0a08f0

.field public static final second_blue_point:I = 0x7f0a08f1

.field public static final second_text:I = 0x7f0a08f2

.field public static final second_title:I = 0x7f0a08f3

.field public static final second_title_container:I = 0x7f0a08f4

.field public static final security:I = 0x7f0a08f5

.field public static final security_center_settings:I = 0x7f0a08f6

.field public static final security_fields:I = 0x7f0a08f7

.field public static final security_filed:I = 0x7f0a08f8

.field public static final security_settings:I = 0x7f0a08f9

.field public static final security_settings_face_settings_enroll_button:I = 0x7f0a08fa

.field public static final security_settings_face_settings_remove_button:I = 0x7f0a08fb

.field public static final security_status:I = 0x7f0a08fc

.field public static final see_more:I = 0x7f0a08fd

.field public static final seek_bar:I = 0x7f0a08fe

.field public static final seekbar:I = 0x7f0a08ff

.field public static final seekbar_app_usage_time:I = 0x7f0a0900

.field public static final seekbar_background_semicircle:I = 0x7f0a0901

.field public static final seekbar_container:I = 0x7f0a0902

.field public static final seekbar_frame:I = 0x7f0a0903

.field public static final seekbar_value:I = 0x7f0a0904

.field public static final select:I = 0x7f0a0905

.field public static final select_cb:I = 0x7f0a0906

.field public static final select_dialog_listview:I = 0x7f0a0907

.field public static final selected:I = 0x7f0a0908

.field public static final selected_image:I = 0x7f0a0909

.field public static final selection_type:I = 0x7f0a090a

.field public static final selector_extra_widget:I = 0x7f0a090b

.field public static final selector_extra_widget_container:I = 0x7f0a090c

.field public static final sendbroadcast:I = 0x7f0a090d

.field public static final server:I = 0x7f0a090e

.field public static final service:I = 0x7f0a090f

.field public static final service_state_label:I = 0x7f0a0910

.field public static final service_state_value:I = 0x7f0a0911

.field public static final set_action_button_icon:I = 0x7f0a0912

.field public static final set_action_button_visibility:I = 0x7f0a0913

.field public static final set_time:I = 0x7f0a0914

.field public static final settings:I = 0x7f0a0915

.field public static final settings_button:I = 0x7f0a0916

.field public static final settings_button_no_background:I = 0x7f0a0917

.field public static final settings_card:I = 0x7f0a0918

.field public static final settings_description:I = 0x7f0a0919

.field public static final settings_divider:I = 0x7f0a091a

.field public static final settings_homepage_container:I = 0x7f0a091b

.field public static final settings_icon:I = 0x7f0a091c

.field public static final settings_search_item_image:I = 0x7f0a091d

.field public static final settings_search_item_name:I = 0x7f0a091e

.field public static final settings_search_item_path:I = 0x7f0a091f

.field public static final settings_title:I = 0x7f0a0920

.field public static final settings_title_divider:I = 0x7f0a0921

.field public static final settings_title_panel:I = 0x7f0a0922

.field public static final settings_title_template:I = 0x7f0a0923

.field public static final settingslib_button:I = 0x7f0a0924

.field public static final settingslib_learn_more:I = 0x7f0a0925

.field public static final settingslib_main_switch_bar:I = 0x7f0a0926

.field public static final setup_choose_unlock_msg:I = 0x7f0a0927

.field public static final setup_choose_unlock_password_msg:I = 0x7f0a0928

.field public static final setup_choose_unlock_password_title:I = 0x7f0a0929

.field public static final setup_choose_unlock_title:I = 0x7f0a092a

.field public static final setup_footer_layout:I = 0x7f0a092b

.field public static final setup_lockPattern:I = 0x7f0a092c

.field public static final setup_new_fingerprint_instruction_img:I = 0x7f0a092d

.field public static final setup_new_fingerprint_step_video:I = 0x7f0a092e

.field public static final setup_new_fingerprint_top_text:I = 0x7f0a092f

.field public static final setup_new_fingerprint_top_title:I = 0x7f0a0930

.field public static final setup_new_fingerprint_view:I = 0x7f0a0931

.field public static final setup_password_entry:I = 0x7f0a0932

.field public static final setup_subHeaderText:I = 0x7f0a0933

.field public static final setup_topLayout:I = 0x7f0a0934

.field public static final setup_wizard_layout:I = 0x7f0a0935

.field public static final seven:I = 0x7f0a0936

.field public static final shape_preference_connected:I = 0x7f0a0937

.field public static final shape_preference_disconnected:I = 0x7f0a0938

.field public static final share_button:I = 0x7f0a0939

.field public static final share_text:I = 0x7f0a093a

.field public static final share_this_wifi:I = 0x7f0a093b

.field public static final share_wifi:I = 0x7f0a093c

.field public static final shared:I = 0x7f0a093d

.field public static final sharing_anim_bg:I = 0x7f0a093e

.field public static final shortcut:I = 0x7f0a093f

.field public static final shortcutIconBtn:I = 0x7f0a0940

.field public static final shortcutLabel:I = 0x7f0a0941

.field public static final shorter:I = 0x7f0a0942

.field public static final shoulderkey_guide_image:I = 0x7f0a0943

.field public static final showCustom:I = 0x7f0a0944

.field public static final showHome:I = 0x7f0a0945

.field public static final showTitle:I = 0x7f0a0946

.field public static final show_all:I = 0x7f0a0947

.field public static final show_app_badge_card:I = 0x7f0a0948

.field public static final show_options:I = 0x7f0a0949

.field public static final show_owner_info_on_lockscreen_checkbox:I = 0x7f0a094a

.field public static final show_page_position_layout:I = 0x7f0a094b

.field public static final show_password:I = 0x7f0a094c

.field public static final show_password_img:I = 0x7f0a094d

.field public static final show_password_layout:I = 0x7f0a094e

.field public static final show_split_action_bar:I = 0x7f0a094f

.field public static final show_system:I = 0x7f0a0950

.field public static final signal:I = 0x7f0a0951

.field public static final signal_strength:I = 0x7f0a0952

.field public static final signal_strength_label:I = 0x7f0a0953

.field public static final signal_strength_value:I = 0x7f0a0954

.field public static final signin_button:I = 0x7f0a0955

.field public static final silence:I = 0x7f0a0956

.field public static final silence_icon:I = 0x7f0a0957

.field public static final silence_label:I = 0x7f0a0958

.field public static final silence_summary:I = 0x7f0a0959

.field public static final silent_mode_count_layout:I = 0x7f0a095a

.field public static final sim:I = 0x7f0a095b

.field public static final sim_card:I = 0x7f0a095c

.field public static final sin:I = 0x7f0a095d

.field public static final single_click_summary:I = 0x7f0a095e

.field public static final single_click_title:I = 0x7f0a095f

.field public static final single_network:I = 0x7f0a0960

.field public static final single_ssid:I = 0x7f0a0961

.field public static final single_status:I = 0x7f0a0962

.field public static final six:I = 0x7f0a0963

.field public static final size_spinner:I = 0x7f0a0964

.field public static final skip:I = 0x7f0a0965

.field public static final skipCollapsed:I = 0x7f0a0966

.field public static final skip_button:I = 0x7f0a0967

.field public static final slice_slider_layout:I = 0x7f0a0968

.field public static final slice_view:I = 0x7f0a0969

.field public static final slide:I = 0x7f0a096a

.field public static final slide_link_turbo:I = 0x7f0a096b

.field public static final slider_title_container:I = 0x7f0a096c

.field public static final slidingButton:I = 0x7f0a096d

.field public static final sliding_button:I = 0x7f0a096e

.field public static final sliding_button_background:I = 0x7f0a096f

.field public static final sliding_button_wallpaper:I = 0x7f0a0970

.field public static final sliding_drawer_handle:I = 0x7f0a0971

.field public static final sliding_tab_selected_indicator:I = 0x7f0a0972

.field public static final sliding_tabs:I = 0x7f0a0973

.field public static final small:I = 0x7f0a0974

.field public static final smaller:I = 0x7f0a0975

.field public static final sms_received_settings:I = 0x7f0a0976

.field public static final snackbar_action:I = 0x7f0a0977

.field public static final snackbar_text:I = 0x7f0a0978

.field public static final snap:I = 0x7f0a0979

.field public static final snapMargins:I = 0x7f0a097a

.field public static final snippet:I = 0x7f0a097b

.field public static final snoozed_list:I = 0x7f0a097c

.field public static final software:I = 0x7f0a097d

.field public static final software_shortcut:I = 0x7f0a097e

.field public static final sort_by_alphabet:I = 0x7f0a097f

.field public static final sort_by_timezone:I = 0x7f0a0980

.field public static final sort_order_alpha:I = 0x7f0a0981

.field public static final sort_order_frequent_notification:I = 0x7f0a0982

.field public static final sort_order_recent_notification:I = 0x7f0a0983

.field public static final sort_order_size:I = 0x7f0a0984

.field public static final sos_cancel:I = 0x7f0a0985

.field public static final sos_dec:I = 0x7f0a0986

.field public static final sos_desc_grop:I = 0x7f0a0987

.field public static final sos_dialog_layout:I = 0x7f0a0988

.field public static final sos_icon:I = 0x7f0a0989

.field public static final sos_json_anim:I = 0x7f0a098a

.field public static final sos_play_icon:I = 0x7f0a098b

.field public static final sos_progressbar:I = 0x7f0a098c

.field public static final sos_summary:I = 0x7f0a098d

.field public static final sos_title:I = 0x7f0a098e

.field public static final sound_bullet:I = 0x7f0a098f

.field public static final sound_bullet_text:I = 0x7f0a0990

.field public static final sound_classic:I = 0x7f0a0991

.field public static final sound_classic_text:I = 0x7f0a0992

.field public static final sound_current:I = 0x7f0a0993

.field public static final sound_current_text:I = 0x7f0a0994

.field public static final sound_settings:I = 0x7f0a0995

.field public static final sound_wind:I = 0x7f0a0996

.field public static final sound_wind_text:I = 0x7f0a0997

.field public static final space:I = 0x7f0a0998

.field public static final space1:I = 0x7f0a0999

.field public static final space2:I = 0x7f0a099a

.field public static final spacer:I = 0x7f0a099b

.field public static final speak_content:I = 0x7f0a099c

.field public static final speaker_harman:I = 0x7f0a099d

.field public static final speaker_icon:I = 0x7f0a099e

.field public static final speaker_summary:I = 0x7f0a099f

.field public static final special_effects_controller_view_tag:I = 0x7f0a09a0

.field public static final speed_slow_summary:I = 0x7f0a09a1

.field public static final spinner:I = 0x7f0a09a2

.field public static final spinner_dropdown_container:I = 0x7f0a09a3

.field public static final spinner_layout:I = 0x7f0a09a4

.field public static final spinner_title:I = 0x7f0a09a5

.field public static final spline:I = 0x7f0a09a6

.field public static final split_action_bar:I = 0x7f0a09a7

.field public static final split_action_bar_vs:I = 0x7f0a09a8

.field public static final split_screen_layout:I = 0x7f0a09a9

.field public static final spread:I = 0x7f0a09aa

.field public static final spread_inside:I = 0x7f0a09ab

.field public static final springBackView:I = 0x7f0a09ac

.field public static final springbackView:I = 0x7f0a09ad

.field public static final springback_layout:I = 0x7f0a09ae

.field public static final springbacklayout:I = 0x7f0a09af

.field public static final sprintback_layout:I = 0x7f0a09b0

.field public static final square:I = 0x7f0a09b1

.field public static final src_atop:I = 0x7f0a09b2

.field public static final src_in:I = 0x7f0a09b3

.field public static final src_over:I = 0x7f0a09b4

.field public static final ssid:I = 0x7f0a09b5

.field public static final ssid_scanner_button:I = 0x7f0a09b6

.field public static final ssid_too_long_warning:I = 0x7f0a09b7

.field public static final standard:I = 0x7f0a09b8

.field public static final start:I = 0x7f0a09b9

.field public static final startHorizontal:I = 0x7f0a09ba

.field public static final startToEnd:I = 0x7f0a09bb

.field public static final startVertical:I = 0x7f0a09bc

.field public static final start_enjoy:I = 0x7f0a09bd

.field public static final start_text:I = 0x7f0a09be

.field public static final startactivity:I = 0x7f0a09bf

.field public static final startup_count:I = 0x7f0a09c0

.field public static final state_1:I = 0x7f0a09c1

.field public static final state_2:I = 0x7f0a09c2

.field public static final state_3:I = 0x7f0a09c3

.field public static final state_image:I = 0x7f0a09c4

.field public static final state_normal:I = 0x7f0a09c5

.field public static final state_off_button:I = 0x7f0a09c6

.field public static final state_on_button:I = 0x7f0a09c7

.field public static final staticLayout:I = 0x7f0a09c8

.field public static final staticPostLayout:I = 0x7f0a09c9

.field public static final staticip:I = 0x7f0a09ca

.field public static final status:I = 0x7f0a09cb

.field public static final status_bar_latest_event_content:I = 0x7f0a09cc

.field public static final status_bar_view:I = 0x7f0a09cd

.field public static final stop:I = 0x7f0a09ce

.field public static final storage_back_button:I = 0x7f0a09cf

.field public static final storage_forget:I = 0x7f0a09d0

.field public static final storage_format:I = 0x7f0a09d1

.field public static final storage_format_as_internal:I = 0x7f0a09d2

.field public static final storage_format_as_portable:I = 0x7f0a09d3

.field public static final storage_free:I = 0x7f0a09d4

.field public static final storage_migrate:I = 0x7f0a09d5

.field public static final storage_mount:I = 0x7f0a09d6

.field public static final storage_next_button:I = 0x7f0a09d7

.field public static final storage_rename:I = 0x7f0a09d8

.field public static final storage_size_bg:I = 0x7f0a09d9

.field public static final storage_unmount:I = 0x7f0a09da

.field public static final storage_use_title:I = 0x7f0a09db

.field public static final storage_view:I = 0x7f0a09dc

.field public static final storage_wizard_aux:I = 0x7f0a09dd

.field public static final storage_wizard_body:I = 0x7f0a09de

.field public static final storage_wizard_init_external:I = 0x7f0a09df

.field public static final storage_wizard_init_internal:I = 0x7f0a09e0

.field public static final storage_wizard_migrate_v2_checklist_media:I = 0x7f0a09e1

.field public static final storage_wizard_progress:I = 0x7f0a09e2

.field public static final storage_wizard_progress_summary:I = 0x7f0a09e3

.field public static final storage_wizard_second_body:I = 0x7f0a09e4

.field public static final storage_wizard_third_body:I = 0x7f0a09e5

.field public static final stretch:I = 0x7f0a09e6

.field public static final stylus_and_keyboard_settings:I = 0x7f0a09e7

.field public static final stylus_battery:I = 0x7f0a09e8

.field public static final stylus_battery_percent:I = 0x7f0a09e9

.field public static final stylus_in_charging:I = 0x7f0a09ea

.field public static final stylus_info_battery:I = 0x7f0a09eb

.field public static final stylus_info_connect_fail:I = 0x7f0a09ec

.field public static final stylus_info_connecting:I = 0x7f0a09ed

.field public static final stylus_info_press_connect:I = 0x7f0a09ee

.field public static final stylus_pager_image:I = 0x7f0a09ef

.field public static final stylus_shortcut_text:I = 0x7f0a09f0

.field public static final subHeaderText:I = 0x7f0a09f1

.field public static final subcontent:I = 0x7f0a09f2

.field public static final subject:I = 0x7f0a09f3

.field public static final submenuarrow:I = 0x7f0a09f4

.field public static final submit_area:I = 0x7f0a09f5

.field public static final subtitle:I = 0x7f0a09f6

.field public static final suc_customization_original_weight:I = 0x7f0a09f7

.field public static final suc_footer_button_bar:I = 0x7f0a09f8

.field public static final suc_layout_content:I = 0x7f0a09f9

.field public static final suc_layout_footer:I = 0x7f0a09fa

.field public static final suc_layout_status:I = 0x7f0a09fb

.field public static final suc_layout_title:I = 0x7f0a09fc

.field public static final sud_bottom_scroll_view:I = 0x7f0a09fd

.field public static final sud_content_info_container:I = 0x7f0a09fe

.field public static final sud_content_info_description:I = 0x7f0a09ff

.field public static final sud_content_info_icon:I = 0x7f0a0a00

.field public static final sud_content_info_icon_container:I = 0x7f0a0a01

.field public static final sud_glif_progress_bar:I = 0x7f0a0a02

.field public static final sud_header_scroll_view:I = 0x7f0a0a03

.field public static final sud_illustration_video_view:I = 0x7f0a0a04

.field public static final sud_items_expandable_switch_content:I = 0x7f0a0a05

.field public static final sud_items_icon:I = 0x7f0a0a06

.field public static final sud_items_icon_container:I = 0x7f0a0a07

.field public static final sud_items_summary:I = 0x7f0a0a08

.field public static final sud_items_switch:I = 0x7f0a0a09

.field public static final sud_items_switch_divider:I = 0x7f0a0a0a

.field public static final sud_items_title:I = 0x7f0a0a0b

.field public static final sud_landscape_content_area:I = 0x7f0a0a0c

.field public static final sud_landscape_header_area:I = 0x7f0a0a0d

.field public static final sud_large_progress_bar:I = 0x7f0a0a0e

.field public static final sud_layout_content:I = 0x7f0a0a0f

.field public static final sud_layout_decor:I = 0x7f0a0a10

.field public static final sud_layout_description:I = 0x7f0a0a11

.field public static final sud_layout_header:I = 0x7f0a0a12

.field public static final sud_layout_icon:I = 0x7f0a0a13

.field public static final sud_layout_icon_container:I = 0x7f0a0a14

.field public static final sud_layout_illustration_progress_stub:I = 0x7f0a0a15

.field public static final sud_layout_navigation_bar:I = 0x7f0a0a16

.field public static final sud_layout_progress:I = 0x7f0a0a17

.field public static final sud_layout_progress_illustration:I = 0x7f0a0a18

.field public static final sud_layout_progress_stub:I = 0x7f0a0a19

.field public static final sud_layout_sticky_header:I = 0x7f0a0a1a

.field public static final sud_layout_subtitle:I = 0x7f0a0a1b

.field public static final sud_layout_template_content:I = 0x7f0a0a1c

.field public static final sud_layout_title:I = 0x7f0a0a1d

.field public static final sud_navbar_back:I = 0x7f0a0a1e

.field public static final sud_navbar_more:I = 0x7f0a0a1f

.field public static final sud_navbar_next:I = 0x7f0a0a20

.field public static final sud_progress_bar:I = 0x7f0a0a21

.field public static final sud_progress_illustration:I = 0x7f0a0a22

.field public static final sud_recycler_view:I = 0x7f0a0a23

.field public static final sud_scroll_view:I = 0x7f0a0a24

.field public static final suggestion_card:I = 0x7f0a0a25

.field public static final suggestion_container_two_pane:I = 0x7f0a0a26

.field public static final suggestion_content:I = 0x7f0a0a27

.field public static final summary:I = 0x7f0a0a28

.field public static final summary1:I = 0x7f0a0a29

.field public static final summary_container:I = 0x7f0a0a2a

.field public static final summary_frame:I = 0x7f0a0a2b

.field public static final summary_text:I = 0x7f0a0a2c

.field public static final supplicant_state:I = 0x7f0a0a2d

.field public static final support:I = 0x7f0a0a2e

.field public static final support_action_bar:I = 0x7f0a0a2f

.field public static final suppression_text:I = 0x7f0a0a30

.field public static final surface_view:I = 0x7f0a0a31

.field public static final switchWidget:I = 0x7f0a0a32

.field public static final switch_bar:I = 0x7f0a0a33

.field public static final switch_ime_button:I = 0x7f0a0a34

.field public static final switch_text:I = 0x7f0a0a35

.field public static final switch_widget:I = 0x7f0a0a36

.field public static final sync_active:I = 0x7f0a0a37

.field public static final sync_failed:I = 0x7f0a0a38

.field public static final sync_img:I = 0x7f0a0a39

.field public static final sync_settings:I = 0x7f0a0a3a

.field public static final sync_settings_error_info:I = 0x7f0a0a3b

.field public static final systemSize:I = 0x7f0a0a3c

.field public static final systemSizePrefix:I = 0x7f0a0a3d

.field public static final system_apps_updater:I = 0x7f0a0a3e

.field public static final system_default_label:I = 0x7f0a0a3f

.field public static final system_label:I = 0x7f0a0a40

.field public static final tabMode:I = 0x7f0a0a41

.field public static final tab_container:I = 0x7f0a0a42

.field public static final tablayout:I = 0x7f0a0a43

.field public static final tablet_screen_settings:I = 0x7f0a0a44

.field public static final tabs:I = 0x7f0a0a45

.field public static final tabs_container:I = 0x7f0a0a46

.field public static final tag_accessibility_actions:I = 0x7f0a0a47

.field public static final tag_accessibility_clickable_spans:I = 0x7f0a0a48

.field public static final tag_accessibility_heading:I = 0x7f0a0a49

.field public static final tag_accessibility_pane_title:I = 0x7f0a0a4a

.field public static final tag_alphabet_indexer_item:I = 0x7f0a0a4b

.field public static final tag_mirror_menu_query_listener:I = 0x7f0a0a4c

.field public static final tag_on_apply_window_listener:I = 0x7f0a0a4d

.field public static final tag_on_receive_content_listener:I = 0x7f0a0a4e

.field public static final tag_on_receive_content_mime_types:I = 0x7f0a0a4f

.field public static final tag_popup_menu_item:I = 0x7f0a0a50

.field public static final tag_screen_reader_focusable:I = 0x7f0a0a51

.field public static final tag_spinner_dropdown_view:I = 0x7f0a0a52

.field public static final tag_spinner_dropdown_view_double_line:I = 0x7f0a0a53

.field public static final tag_state_description:I = 0x7f0a0a54

.field public static final tag_transition_group:I = 0x7f0a0a55

.field public static final tag_unhandled_key_event_manager:I = 0x7f0a0a56

.field public static final tag_unhandled_key_listeners:I = 0x7f0a0a57

.field public static final tag_window_insets_animation_callback:I = 0x7f0a0a58

.field public static final technology:I = 0x7f0a0a59

.field public static final temperature:I = 0x7f0a0a5a

.field public static final test_checkbox_android_button_tint:I = 0x7f0a0a5b

.field public static final test_checkbox_app_button_tint:I = 0x7f0a0a5c

.field public static final test_radiobutton_android_button_tint:I = 0x7f0a0a5d

.field public static final test_radiobutton_app_button_tint:I = 0x7f0a0a5e

.field public static final tether_no_device:I = 0x7f0a0a5f

.field public static final tether_settings:I = 0x7f0a0a60

.field public static final text:I = 0x7f0a0a61

.field public static final text1:I = 0x7f0a0a62

.field public static final text2:I = 0x7f0a0a63

.field public static final textAlign:I = 0x7f0a0a64

.field public static final textEnd:I = 0x7f0a0a65

.field public static final textSpacerNoButtons:I = 0x7f0a0a66

.field public static final textSpacerNoTitle:I = 0x7f0a0a67

.field public static final textStart:I = 0x7f0a0a68

.field public static final textTop:I = 0x7f0a0a69

.field public static final textViewDeviceName:I = 0x7f0a0a6a

.field public static final textViewHeadset:I = 0x7f0a0a6b

.field public static final text_dark:I = 0x7f0a0a6c

.field public static final text_frame:I = 0x7f0a0a6d

.field public static final text_group:I = 0x7f0a0a6e

.field public static final text_input_end_icon:I = 0x7f0a0a6f

.field public static final text_input_error_icon:I = 0x7f0a0a70

.field public static final text_input_start_icon:I = 0x7f0a0a71

.field public static final text_layout:I = 0x7f0a0a72

.field public static final text_light:I = 0x7f0a0a73

.field public static final text_overlay:I = 0x7f0a0a74

.field public static final text_right:I = 0x7f0a0a75

.field public static final text_see_more:I = 0x7f0a0a76

.field public static final text_see_more_count:I = 0x7f0a0a77

.field public static final text_title:I = 0x7f0a0a78

.field public static final text_total_time:I = 0x7f0a0a79

.field public static final text_view:I = 0x7f0a0a7a

.field public static final textinput_counter:I = 0x7f0a0a7b

.field public static final textinput_error:I = 0x7f0a0a7c

.field public static final textinput_helper_text:I = 0x7f0a0a7d

.field public static final textinput_placeholder:I = 0x7f0a0a7e

.field public static final textinput_prefix_text:I = 0x7f0a0a7f

.field public static final textinput_suffix_text:I = 0x7f0a0a80

.field public static final theme_settings:I = 0x7f0a0a81

.field public static final three:I = 0x7f0a0a82

.field public static final tic:I = 0x7f0a0a83

.field public static final tile_item:I = 0x7f0a0a84

.field public static final time:I = 0x7f0a0a85

.field public static final timePicker:I = 0x7f0a0a86

.field public static final timePickerLayout:I = 0x7f0a0a87

.field public static final time_container:I = 0x7f0a0a88

.field public static final time_count_seekbar:I = 0x7f0a0a89

.field public static final time_label:I = 0x7f0a0a8a

.field public static final time_zone_search_menu:I = 0x7f0a0a8b

.field public static final timestamp:I = 0x7f0a0a8c

.field public static final tint_overlay:I = 0x7f0a0a8d

.field public static final title:I = 0x7f0a0a8e

.field public static final titleDivider:I = 0x7f0a0a8f

.field public static final titleDividerNoCustom:I = 0x7f0a0a90

.field public static final title_container:I = 0x7f0a0a91

.field public static final title_group:I = 0x7f0a0a92

.field public static final title_icon:I = 0x7f0a0a93

.field public static final title_in_control:I = 0x7f0a0a94

.field public static final title_layout:I = 0x7f0a0a95

.field public static final title_template:I = 0x7f0a0a96

.field public static final title_text:I = 0x7f0a0a97

.field public static final title_view:I = 0x7f0a0a98

.field public static final today_list:I = 0x7f0a0a99

.field public static final toggle:I = 0x7f0a0a9a

.field public static final toggle_diversity:I = 0x7f0a0a9b

.field public static final tool_bar:I = 0x7f0a0a9c

.field public static final top:I = 0x7f0a0a9d

.field public static final topLayout:I = 0x7f0a0a9e

.field public static final topPanel:I = 0x7f0a0a9f

.field public static final top_actionBar:I = 0x7f0a0aa0

.field public static final top_blank:I = 0x7f0a0aa1

.field public static final top_line:I = 0x7f0a0aa2

.field public static final top_logo:I = 0x7f0a0aa3

.field public static final top_row:I = 0x7f0a0aa4

.field public static final top_screen:I = 0x7f0a0aa5

.field public static final top_view:I = 0x7f0a0aa6

.field public static final tos_content:I = 0x7f0a0aa7

.field public static final total_storage:I = 0x7f0a0aa8

.field public static final total_summary:I = 0x7f0a0aa9

.field public static final touch_outside:I = 0x7f0a0aaa

.field public static final tracking_progress:I = 0x7f0a0aab

.field public static final tracking_progress_label:I = 0x7f0a0aac

.field public static final tracking_progress_up:I = 0x7f0a0aad

.field public static final tracking_progress_up_container:I = 0x7f0a0aae

.field public static final tracking_progress_up_label:I = 0x7f0a0aaf

.field public static final transitionToEnd:I = 0x7f0a0ab0

.field public static final transitionToStart:I = 0x7f0a0ab1

.field public static final transition_current_scene:I = 0x7f0a0ab2

.field public static final transition_layout_save:I = 0x7f0a0ab3

.field public static final transition_position:I = 0x7f0a0ab4

.field public static final transition_scene_layoutid_cache:I = 0x7f0a0ab5

.field public static final transition_transform:I = 0x7f0a0ab6

.field public static final transparentAdjust:I = 0x7f0a0ab7

.field public static final transparentAdjustText:I = 0x7f0a0ab8

.field public static final transparentAdjustView:I = 0x7f0a0ab9

.field public static final transparentDark:I = 0x7f0a0aba

.field public static final transparentLight:I = 0x7f0a0abb

.field public static final transparentModeText:I = 0x7f0a0abc

.field public static final transparents:I = 0x7f0a0abd

.field public static final transport:I = 0x7f0a0abe

.field public static final triangle:I = 0x7f0a0abf

.field public static final trigger_content_view:I = 0x7f0a0ac0

.field public static final trigger_view:I = 0x7f0a0ac1

.field public static final triple_tap_shortcut:I = 0x7f0a0ac2

.field public static final trusted_credential_status:I = 0x7f0a0ac3

.field public static final trusted_credential_subject_primary:I = 0x7f0a0ac4

.field public static final trusted_credential_subject_secondary:I = 0x7f0a0ac5

.field public static final trusted_devices_prompt_layout:I = 0x7f0a0ac6

.field public static final tts_button_lyt:I = 0x7f0a0ac7

.field public static final tts_engine_pref:I = 0x7f0a0ac8

.field public static final tts_engine_pref_text:I = 0x7f0a0ac9

.field public static final tts_engine_radiobutton:I = 0x7f0a0aca

.field public static final tts_engine_settings:I = 0x7f0a0acb

.field public static final tts_play:I = 0x7f0a0acc

.field public static final tts_reset:I = 0x7f0a0acd

.field public static final ttv_limit_summary:I = 0x7f0a0ace

.field public static final turn_off_password:I = 0x7f0a0acf

.field public static final tv:I = 0x7f0a0ad0

.field public static final tv_1:I = 0x7f0a0ad1

.field public static final tv_2:I = 0x7f0a0ad2

.field public static final tv_app_name:I = 0x7f0a0ad3

.field public static final tv_app_usage_hour:I = 0x7f0a0ad4

.field public static final tv_app_usage_hour_text:I = 0x7f0a0ad5

.field public static final tv_app_usage_miunte:I = 0x7f0a0ad6

.field public static final tv_app_usage_miunte_text:I = 0x7f0a0ad7

.field public static final tv_app_usage_mouth_channel:I = 0x7f0a0ad8

.field public static final tv_app_usage_time:I = 0x7f0a0ad9

.field public static final tv_connect_state:I = 0x7f0a0ada

.field public static final tv_device_name:I = 0x7f0a0adb

.field public static final tv_get_it:I = 0x7f0a0adc

.field public static final tv_godzilla_tips:I = 0x7f0a0add

.field public static final tv_item:I = 0x7f0a0ade

.field public static final tv_limit_summary:I = 0x7f0a0adf

.field public static final tv_limit_title:I = 0x7f0a0ae0

.field public static final tv_negative_desc:I = 0x7f0a0ae1

.field public static final tv_negative_title:I = 0x7f0a0ae2

.field public static final tv_normal:I = 0x7f0a0ae3

.field public static final tv_normal_time:I = 0x7f0a0ae4

.field public static final tv_normal_time_title:I = 0x7f0a0ae5

.field public static final tv_positive_desc:I = 0x7f0a0ae6

.field public static final tv_positive_title:I = 0x7f0a0ae7

.field public static final tv_search_feedback:I = 0x7f0a0ae8

.field public static final tv_secspace_fod_finger_result:I = 0x7f0a0ae9

.field public static final tv_secspace_fod_finger_title:I = 0x7f0a0aea

.field public static final tv_set_message:I = 0x7f0a0aeb

.field public static final tv_set_time:I = 0x7f0a0aec

.field public static final tv_set_title:I = 0x7f0a0aed

.field public static final tv_sos_contact_name_first:I = 0x7f0a0aee

.field public static final tv_sos_contact_name_second:I = 0x7f0a0aef

.field public static final tv_sos_contact_name_third:I = 0x7f0a0af0

.field public static final tv_sos_contact_phone_first:I = 0x7f0a0af1

.field public static final tv_sos_contact_phone_second:I = 0x7f0a0af2

.field public static final tv_sos_contact_phone_third:I = 0x7f0a0af3

.field public static final tv_state_description_both:I = 0x7f0a0af4

.field public static final tv_state_description_left:I = 0x7f0a0af5

.field public static final tv_state_description_right:I = 0x7f0a0af6

.field public static final tv_state_title_both:I = 0x7f0a0af7

.field public static final tv_state_title_left:I = 0x7f0a0af8

.field public static final tv_state_title_right:I = 0x7f0a0af9

.field public static final tv_sysapp_summary:I = 0x7f0a0afa

.field public static final tv_text:I = 0x7f0a0afb

.field public static final tv_title:I = 0x7f0a0afc

.field public static final tv_usage_time:I = 0x7f0a0afd

.field public static final tv_use_password:I = 0x7f0a0afe

.field public static final tv_week_day:I = 0x7f0a0aff

.field public static final tv_week_day_time:I = 0x7f0a0b00

.field public static final tv_week_day_time_title:I = 0x7f0a0b01

.field public static final tv_weekday_time:I = 0x7f0a0b02

.field public static final tv_weekday_title:I = 0x7f0a0b03

.field public static final tv_weekend_time:I = 0x7f0a0b04

.field public static final tv_weekend_title:I = 0x7f0a0b05

.field public static final tv_welcome:I = 0x7f0a0b06

.field public static final tv_work_day:I = 0x7f0a0b07

.field public static final two:I = 0x7f0a0b08

.field public static final two_pane_suggestion_content:I = 0x7f0a0b09

.field public static final two_target_divider:I = 0x7f0a0b0a

.field public static final twohours:I = 0x7f0a0b0b

.field public static final tx_link_speed:I = 0x7f0a0b0c

.field public static final txt_choice:I = 0x7f0a0b0d

.field public static final txt_fluency_content:I = 0x7f0a0b0e

.field public static final txt_fluency_summary:I = 0x7f0a0b0f

.field public static final txt_sim_displayname:I = 0x7f0a0b10

.field public static final txt_sim_number:I = 0x7f0a0b11

.field public static final txt_sim_summary:I = 0x7f0a0b12

.field public static final type:I = 0x7f0a0b13

.field public static final typeSpinner:I = 0x7f0a0b14

.field public static final tz_item_details:I = 0x7f0a0b15

.field public static final tz_item_dst:I = 0x7f0a0b16

.field public static final tz_item_name:I = 0x7f0a0b17

.field public static final tz_item_time:I = 0x7f0a0b18

.field public static final unchecked:I = 0x7f0a0b19

.field public static final uniform:I = 0x7f0a0b1a

.field public static final uninstall_button:I = 0x7f0a0b1b

.field public static final unlabeled:I = 0x7f0a0b1c

.field public static final unlock_set_settings:I = 0x7f0a0b1d

.field public static final up:I = 0x7f0a0b1e

.field public static final update:I = 0x7f0a0b1f

.field public static final updateTextView:I = 0x7f0a0b20

.field public static final updateTextViewParent:I = 0x7f0a0b21

.field public static final update_hint:I = 0x7f0a0b22

.field public static final update_hint_text:I = 0x7f0a0b23

.field public static final update_icon:I = 0x7f0a0b24

.field public static final update_summary:I = 0x7f0a0b25

.field public static final update_title:I = 0x7f0a0b26

.field public static final update_value_right:I = 0x7f0a0b27

.field public static final upgrade_prompt:I = 0x7f0a0b28

.field public static final uptime:I = 0x7f0a0b29

.field public static final uri_name:I = 0x7f0a0b2a

.field public static final uris:I = 0x7f0a0b2b

.field public static final usage_date:I = 0x7f0a0b2c

.field public static final usage_graph:I = 0x7f0a0b2d

.field public static final usage_layout:I = 0x7f0a0b2e

.field public static final usage_log:I = 0x7f0a0b2f

.field public static final usage_summary:I = 0x7f0a0b30

.field public static final usage_time:I = 0x7f0a0b31

.field public static final usage_title:I = 0x7f0a0b32

.field public static final usagestats_app_list:I = 0x7f0a0b33

.field public static final usagestats_list:I = 0x7f0a0b34

.field public static final usagestats_list_item:I = 0x7f0a0b35

.field public static final useFingerprint:I = 0x7f0a0b36

.field public static final useLogo:I = 0x7f0a0b37

.field public static final use_privacy_password_immediate:I = 0x7f0a0b38

.field public static final used_storage:I = 0x7f0a0b39

.field public static final user_cert:I = 0x7f0a0b3a

.field public static final user_dict_settings_add_dialog_top:I = 0x7f0a0b3b

.field public static final user_dictionary_add_shortcut:I = 0x7f0a0b3c

.field public static final user_dictionary_add_shortcut_label:I = 0x7f0a0b3d

.field public static final user_dictionary_add_word_grid:I = 0x7f0a0b3e

.field public static final user_dictionary_add_word_text:I = 0x7f0a0b3f

.field public static final user_name:I = 0x7f0a0b40

.field public static final user_photo:I = 0x7f0a0b41

.field public static final user_settings:I = 0x7f0a0b42

.field public static final username:I = 0x7f0a0b43

.field public static final userpass:I = 0x7f0a0b44

.field public static final value:I = 0x7f0a0b45

.field public static final value_right:I = 0x7f0a0b46

.field public static final verified_links_widget:I = 0x7f0a0b47

.field public static final verify_ProgressBar:I = 0x7f0a0b48

.field public static final verify_webView:I = 0x7f0a0b49

.field public static final verifying_layout:I = 0x7f0a0b4a

.field public static final verifying_progress:I = 0x7f0a0b4b

.field public static final verifying_textview:I = 0x7f0a0b4c

.field public static final verison:I = 0x7f0a0b4d

.field public static final verison_info:I = 0x7f0a0b4e

.field public static final versionName:I = 0x7f0a0b4f

.field public static final version_layout:I = 0x7f0a0b50

.field public static final vertical:I = 0x7f0a0b51

.field public static final video:I = 0x7f0a0b52

.field public static final video_card_view:I = 0x7f0a0b53

.field public static final video_check_box:I = 0x7f0a0b54

.field public static final video_container:I = 0x7f0a0b55

.field public static final video_display_surfaceview:I = 0x7f0a0b56

.field public static final video_loop_view:I = 0x7f0a0b57

.field public static final video_play_button:I = 0x7f0a0b58

.field public static final video_preview_image:I = 0x7f0a0b59

.field public static final video_texture_view:I = 0x7f0a0b5a

.field public static final video_view:I = 0x7f0a0b5b

.field public static final video_view_freeform:I = 0x7f0a0b5c

.field public static final video_view_full_screen:I = 0x7f0a0b5d

.field public static final video_view_notes:I = 0x7f0a0b5e

.field public static final video_view_rotation:I = 0x7f0a0b5f

.field public static final video_view_screenshot:I = 0x7f0a0b60

.field public static final video_view_virtual_keys:I = 0x7f0a0b61

.field public static final view:I = 0x7f0a0b62

.field public static final viewDiver:I = 0x7f0a0b63

.field public static final viewGroup:I = 0x7f0a0b64

.field public static final viewPager:I = 0x7f0a0b65

.field public static final view_corner:I = 0x7f0a0b66

.field public static final view_corner_both:I = 0x7f0a0b67

.field public static final view_corner_left:I = 0x7f0a0b68

.field public static final view_corner_right:I = 0x7f0a0b69

.field public static final view_custom:I = 0x7f0a0b6a

.field public static final view_filpper:I = 0x7f0a0b6b

.field public static final view_flipper:I = 0x7f0a0b6c

.field public static final view_high_light_root:I = 0x7f0a0b6d

.field public static final view_offset_helper:I = 0x7f0a0b6e

.field public static final view_pager:I = 0x7f0a0b6f

.field public static final view_tree_lifecycle_owner:I = 0x7f0a0b70

.field public static final view_tree_saved_state_registry_owner:I = 0x7f0a0b71

.field public static final view_tree_view_model_store_owner:I = 0x7f0a0b72

.field public static final viewpager:I = 0x7f0a0b73

.field public static final vip_service_settings:I = 0x7f0a0b74

.field public static final virtual_keys_container:I = 0x7f0a0b75

.field public static final visible:I = 0x7f0a0b76

.field public static final visible_pattern_button:I = 0x7f0a0b77

.field public static final visible_pattern_layout:I = 0x7f0a0b78

.field public static final visible_pattern_title:I = 0x7f0a0b79

.field public static final visible_removing_fragment_view_tag:I = 0x7f0a0b7a

.field public static final visual_box_reyclerview_height:I = 0x7f0a0b7b

.field public static final voice_assist:I = 0x7f0a0b7c

.field public static final voice_network_type_label:I = 0x7f0a0b7d

.field public static final voice_network_type_value:I = 0x7f0a0b7e

.field public static final voice_section:I = 0x7f0a0b7f

.field public static final voltage:I = 0x7f0a0b80

.field public static final volume_item_container:I = 0x7f0a0b81

.field public static final volume_seekbar:I = 0x7f0a0b82

.field public static final vpn:I = 0x7f0a0b83

.field public static final vpn_create:I = 0x7f0a0b84

.field public static final vpn_proxy_fields:I = 0x7f0a0b85

.field public static final vpn_proxy_host:I = 0x7f0a0b86

.field public static final vpn_proxy_port:I = 0x7f0a0b87

.field public static final vpn_proxy_settings:I = 0x7f0a0b88

.field public static final vpn_proxy_settings_title:I = 0x7f0a0b89

.field public static final vpn_settings:I = 0x7f0a0b8a

.field public static final vpn_settings_multiple:I = 0x7f0a0b8b

.field public static final vv_item:I = 0x7f0a0b8c

.field public static final wallpaper:I = 0x7f0a0b8d

.field public static final wallpaper_settings:I = 0x7f0a0b8e

.field public static final wallpaper_text:I = 0x7f0a0b8f

.field public static final wapi_as_cert:I = 0x7f0a0b90

.field public static final wapi_as_cert_edit:I = 0x7f0a0b91

.field public static final wapi_as_cert_text:I = 0x7f0a0b92

.field public static final wapi_cert:I = 0x7f0a0b93

.field public static final wapi_cert_create_subdir_edit:I = 0x7f0a0b94

.field public static final wapi_cert_create_subdir_text:I = 0x7f0a0b95

.field public static final wapi_cert_select:I = 0x7f0a0b96

.field public static final wapi_psk:I = 0x7f0a0b97

.field public static final wapi_psk_type:I = 0x7f0a0b98

.field public static final wapi_user_cert:I = 0x7f0a0b99

.field public static final wapi_user_cert_edit:I = 0x7f0a0b9a

.field public static final wapi_user_cert_text:I = 0x7f0a0b9b

.field public static final wapi_user_certificate_text:I = 0x7f0a0b9c

.field public static final warm_color:I = 0x7f0a0b9d

.field public static final warning_button:I = 0x7f0a0b9e

.field public static final warning_info:I = 0x7f0a0b9f

.field public static final warning_text:I = 0x7f0a0ba0

.field public static final water_box_view:I = 0x7f0a0ba1

.field public static final web_uri:I = 0x7f0a0ba2

.field public static final widget_summary:I = 0x7f0a0ba3

.field public static final widget_summary1:I = 0x7f0a0ba4

.field public static final widget_summary2:I = 0x7f0a0ba5

.field public static final wifi6_animation_1:I = 0x7f0a0ba6

.field public static final wifi6_animation_2:I = 0x7f0a0ba7

.field public static final wifi6_animation_3:I = 0x7f0a0ba8

.field public static final wifi6_animation_4:I = 0x7f0a0ba9

.field public static final wifi_advanced_fields:I = 0x7f0a0baa

.field public static final wifi_advanced_fields_divider:I = 0x7f0a0bab

.field public static final wifi_advanced_toggle:I = 0x7f0a0bac

.field public static final wifi_advanced_togglebox:I = 0x7f0a0bad

.field public static final wifi_animation_1:I = 0x7f0a0bae

.field public static final wifi_animation_2:I = 0x7f0a0baf

.field public static final wifi_animation_3:I = 0x7f0a0bb0

.field public static final wifi_animation_4:I = 0x7f0a0bb1

.field public static final wifi_ap_hidden_ssid:I = 0x7f0a0bb2

.field public static final wifi_ap_picture_view:I = 0x7f0a0bb3

.field public static final wifi_band:I = 0x7f0a0bb4

.field public static final wifi_candidate:I = 0x7f0a0bb5

.field public static final wifi_channel:I = 0x7f0a0bb6

.field public static final wifi_detail_description_image:I = 0x7f0a0bb7

.field public static final wifi_dpp_layout:I = 0x7f0a0bb8

.field public static final wifi_frequency:I = 0x7f0a0bb9

.field public static final wifi_group:I = 0x7f0a0bba

.field public static final wifi_negative:I = 0x7f0a0bbb

.field public static final wifi_network_list_container:I = 0x7f0a0bbc

.field public static final wifi_positive:I = 0x7f0a0bbd

.field public static final wifi_settings:I = 0x7f0a0bbe

.field public static final wifi_share_action:I = 0x7f0a0bbf

.field public static final wifi_share_action_empty:I = 0x7f0a0bc0

.field public static final wifi_share_add_summary:I = 0x7f0a0bc1

.field public static final wifi_share_count:I = 0x7f0a0bc2

.field public static final wifi_share_qrcode:I = 0x7f0a0bc3

.field public static final wifi_state:I = 0x7f0a0bc4

.field public static final wifi_tether_settings:I = 0x7f0a0bc5

.field public static final wifi_wapi_cert_delet_subdir_spinner:I = 0x7f0a0bc6

.field public static final wifi_wapi_cert_delet_subdir_text:I = 0x7f0a0bc7

.field public static final wifi_wps:I = 0x7f0a0bc8

.field public static final wireless_settings:I = 0x7f0a0bc9

.field public static final withText:I = 0x7f0a0bca

.field public static final withinBounds:I = 0x7f0a0bcb

.field public static final words_title:I = 0x7f0a0bcc

.field public static final wps_progress_bar:I = 0x7f0a0bcd

.field public static final wps_timeout_bar:I = 0x7f0a0bce

.field public static final wps_txt:I = 0x7f0a0bcf

.field public static final wrap:I = 0x7f0a0bd0

.field public static final wrap_content:I = 0x7f0a0bd1

.field public static final year:I = 0x7f0a0bd2

.field public static final zen_alarm_warning:I = 0x7f0a0bd3

.field public static final zen_automatic_rule_widget:I = 0x7f0a0bd4

.field public static final zen_conditions:I = 0x7f0a0bd5

.field public static final zen_custom_settings_dialog_alarms:I = 0x7f0a0bd6

.field public static final zen_custom_settings_dialog_alarms_allow:I = 0x7f0a0bd7

.field public static final zen_custom_settings_dialog_calls:I = 0x7f0a0bd8

.field public static final zen_custom_settings_dialog_calls_allow:I = 0x7f0a0bd9

.field public static final zen_custom_settings_dialog_events:I = 0x7f0a0bda

.field public static final zen_custom_settings_dialog_events_allow:I = 0x7f0a0bdb

.field public static final zen_custom_settings_dialog_media:I = 0x7f0a0bdc

.field public static final zen_custom_settings_dialog_media_allow:I = 0x7f0a0bdd

.field public static final zen_custom_settings_dialog_messages:I = 0x7f0a0bde

.field public static final zen_custom_settings_dialog_messages_allow:I = 0x7f0a0bdf

.field public static final zen_custom_settings_dialog_notifications:I = 0x7f0a0be0

.field public static final zen_custom_settings_dialog_reminders:I = 0x7f0a0be1

.field public static final zen_custom_settings_dialog_reminders_allow:I = 0x7f0a0be2

.field public static final zen_custom_settings_dialog_show_notifications:I = 0x7f0a0be3

.field public static final zen_custom_settings_dialog_system:I = 0x7f0a0be4

.field public static final zen_custom_settings_dialog_system_allow:I = 0x7f0a0be5

.field public static final zen_duration_conditions:I = 0x7f0a0be6

.field public static final zen_duration_container:I = 0x7f0a0be7

.field public static final zen_duration_dialog_container:I = 0x7f0a0be8

.field public static final zen_mode_button:I = 0x7f0a0be9

.field public static final zen_mode_rule_name:I = 0x7f0a0bea

.field public static final zen_mode_rule_name_warning:I = 0x7f0a0beb

.field public static final zen_mode_settings_senders_image:I = 0x7f0a0bec

.field public static final zen_mode_settings_senders_overlay_view:I = 0x7f0a0bed

.field public static final zen_mode_settings_turn_off_button:I = 0x7f0a0bee

.field public static final zen_mode_settings_turn_on_button:I = 0x7f0a0bef

.field public static final zen_onboarding_choices:I = 0x7f0a0bf0

.field public static final zen_onboarding_current_setting:I = 0x7f0a0bf1

.field public static final zen_onboarding_current_setting_button:I = 0x7f0a0bf2

.field public static final zen_onboarding_current_setting_summary:I = 0x7f0a0bf3

.field public static final zen_onboarding_current_setting_title:I = 0x7f0a0bf4

.field public static final zen_onboarding_new_setting:I = 0x7f0a0bf5

.field public static final zen_onboarding_new_setting_button:I = 0x7f0a0bf6

.field public static final zen_onboarding_new_setting_summary:I = 0x7f0a0bf7

.field public static final zen_onboarding_new_setting_title:I = 0x7f0a0bf8

.field public static final zen_radio_buttons:I = 0x7f0a0bf9

.field public static final zen_radio_buttons_content:I = 0x7f0a0bfa

.field public static final zen_rule_name:I = 0x7f0a0bfb

.field public static final zero:I = 0x7f0a0bfc

.field public static final zero_corner_chip:I = 0x7f0a0bfd

.field public static final zip:I = 0x7f0a0bfe


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
