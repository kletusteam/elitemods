.class public interface abstract Lmiui/bluetooth/ble/IBluetoothMiBleCallback;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/bluetooth/ble/IBluetoothMiBleCallback$_Parcel;,
        Lmiui/bluetooth/ble/IBluetoothMiBleCallback$Stub;,
        Lmiui/bluetooth/ble/IBluetoothMiBleCallback$Default;
    }
.end annotation


# static fields
.field public static final DESCRIPTOR:Ljava/lang/String; = "miui.bluetooth.ble.IBluetoothMiBleCallback"


# virtual methods
.method public abstract onConnectionState(Landroid/os/ParcelUuid;I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
