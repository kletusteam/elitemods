.class Lmiui/bluetooth/ble/MiBleDeviceManager$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/bluetooth/ble/MiBleDeviceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/bluetooth/ble/MiBleDeviceManager;


# direct methods
.method constructor <init>(Lmiui/bluetooth/ble/MiBleDeviceManager;)V
    .locals 0

    iput-object p1, p0, Lmiui/bluetooth/ble/MiBleDeviceManager$1;->this$0:Lmiui/bluetooth/ble/MiBleDeviceManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 0

    iget-object p1, p0, Lmiui/bluetooth/ble/MiBleDeviceManager$1;->this$0:Lmiui/bluetooth/ble/MiBleDeviceManager;

    invoke-static {p2}, Lmiui/bluetooth/ble/IMiBleDeviceManager$Stub;->asInterface(Landroid/os/IBinder;)Lmiui/bluetooth/ble/IMiBleDeviceManager;

    move-result-object p2

    invoke-static {p1, p2}, Lmiui/bluetooth/ble/MiBleDeviceManager;->access$000(Lmiui/bluetooth/ble/MiBleDeviceManager;Lmiui/bluetooth/ble/IMiBleDeviceManager;)V

    iget-object p1, p0, Lmiui/bluetooth/ble/MiBleDeviceManager$1;->this$0:Lmiui/bluetooth/ble/MiBleDeviceManager;

    invoke-static {p1}, Lmiui/bluetooth/ble/MiBleDeviceManager;->access$100(Lmiui/bluetooth/ble/MiBleDeviceManager;)Lmiui/bluetooth/ble/MiBleDeviceManager$MiBleDeviceManagerListener;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lmiui/bluetooth/ble/MiBleDeviceManager$1;->this$0:Lmiui/bluetooth/ble/MiBleDeviceManager;

    invoke-static {p1}, Lmiui/bluetooth/ble/MiBleDeviceManager;->access$100(Lmiui/bluetooth/ble/MiBleDeviceManager;)Lmiui/bluetooth/ble/MiBleDeviceManager$MiBleDeviceManagerListener;

    move-result-object p1

    iget-object p0, p0, Lmiui/bluetooth/ble/MiBleDeviceManager$1;->this$0:Lmiui/bluetooth/ble/MiBleDeviceManager;

    invoke-interface {p1, p0}, Lmiui/bluetooth/ble/MiBleDeviceManager$MiBleDeviceManagerListener;->onInit(Lmiui/bluetooth/ble/MiBleDeviceManager;)V

    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1

    iget-object p1, p0, Lmiui/bluetooth/ble/MiBleDeviceManager$1;->this$0:Lmiui/bluetooth/ble/MiBleDeviceManager;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lmiui/bluetooth/ble/MiBleDeviceManager;->access$000(Lmiui/bluetooth/ble/MiBleDeviceManager;Lmiui/bluetooth/ble/IMiBleDeviceManager;)V

    iget-object p1, p0, Lmiui/bluetooth/ble/MiBleDeviceManager$1;->this$0:Lmiui/bluetooth/ble/MiBleDeviceManager;

    invoke-static {p1}, Lmiui/bluetooth/ble/MiBleDeviceManager;->access$100(Lmiui/bluetooth/ble/MiBleDeviceManager;)Lmiui/bluetooth/ble/MiBleDeviceManager$MiBleDeviceManagerListener;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p0, p0, Lmiui/bluetooth/ble/MiBleDeviceManager$1;->this$0:Lmiui/bluetooth/ble/MiBleDeviceManager;

    invoke-static {p0}, Lmiui/bluetooth/ble/MiBleDeviceManager;->access$100(Lmiui/bluetooth/ble/MiBleDeviceManager;)Lmiui/bluetooth/ble/MiBleDeviceManager$MiBleDeviceManagerListener;

    move-result-object p0

    invoke-interface {p0}, Lmiui/bluetooth/ble/MiBleDeviceManager$MiBleDeviceManagerListener;->onDestroy()V

    :cond_0
    return-void
.end method
