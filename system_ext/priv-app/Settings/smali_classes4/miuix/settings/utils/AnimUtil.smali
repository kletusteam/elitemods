.class public Lmiuix/settings/utils/AnimUtil;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static animButtonClick(Landroid/view/View;Ljava/lang/Runnable;)V
    .locals 6

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    const/4 v1, 0x1

    new-array v2, v1, [Landroid/view/View;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v2}, Lmiui/animation/Folme;->useAt([Landroid/view/View;)Lmiui/animation/IFolme;

    move-result-object v2

    invoke-interface {v2}, Lmiui/animation/IFolme;->touch()Lmiui/animation/ITouchStyle;

    move-result-object v2

    new-array v4, v1, [Lmiui/animation/ITouchStyle$TouchType;

    sget-object v5, Lmiui/animation/ITouchStyle$TouchType;->DOWN:Lmiui/animation/ITouchStyle$TouchType;

    aput-object v5, v4, v3

    const v5, 0x3f666666    # 0.9f

    invoke-interface {v2, v5, v4}, Lmiui/animation/ITouchStyle;->setScale(F[Lmiui/animation/ITouchStyle$TouchType;)Lmiui/animation/ITouchStyle;

    move-result-object v2

    new-array v1, v1, [Lmiui/animation/base/AnimConfig;

    new-instance v4, Lmiui/animation/base/AnimConfig;

    invoke-direct {v4}, Lmiui/animation/base/AnimConfig;-><init>()V

    aput-object v4, v1, v3

    invoke-interface {v2, v1}, Lmiui/animation/ITouchStyle;->touchDown([Lmiui/animation/base/AnimConfig;)V

    new-instance v1, Lmiuix/settings/utils/-$$Lambda$AnimUtil$dUT22XWIIFvB83ddxcjO3rT-tmI;

    invoke-direct {v1, p0, v0, p1}, Lmiuix/settings/utils/-$$Lambda$AnimUtil$dUT22XWIIFvB83ddxcjO3rT-tmI;-><init>(Landroid/view/View;Landroid/os/Handler;Ljava/lang/Runnable;)V

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public static animButtonTouch(Landroid/view/View;Ljava/lang/Runnable;)V
    .locals 2

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lmiuix/settings/utils/-$$Lambda$AnimUtil$WvS4S2Nx3KGeH07oP_O0F3dfT5Q;

    invoke-direct {v1, v0, p1}, Lmiuix/settings/utils/-$$Lambda$AnimUtil$WvS4S2Nx3KGeH07oP_O0F3dfT5Q;-><init>(Landroid/os/Handler;Ljava/lang/Runnable;)V

    invoke-virtual {p0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method static synthetic lambda$animButtonClick$0(Landroid/view/View;Landroid/os/Handler;Ljava/lang/Runnable;)V
    .locals 5

    const/4 v0, 0x1

    new-array v1, v0, [Landroid/view/View;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v1}, Lmiui/animation/Folme;->useAt([Landroid/view/View;)Lmiui/animation/IFolme;

    move-result-object v1

    invoke-interface {v1}, Lmiui/animation/IFolme;->touch()Lmiui/animation/ITouchStyle;

    move-result-object v1

    new-array v3, v0, [Lmiui/animation/ITouchStyle$TouchType;

    sget-object v4, Lmiui/animation/ITouchStyle$TouchType;->UP:Lmiui/animation/ITouchStyle$TouchType;

    aput-object v4, v3, v2

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v1, v4, v3}, Lmiui/animation/ITouchStyle;->setScale(F[Lmiui/animation/ITouchStyle$TouchType;)Lmiui/animation/ITouchStyle;

    move-result-object v1

    new-array v0, v0, [Lmiui/animation/base/AnimConfig;

    new-instance v3, Lmiui/animation/base/AnimConfig;

    invoke-direct {v3}, Lmiui/animation/base/AnimConfig;-><init>()V

    aput-object v3, v0, v2

    invoke-interface {v1, v0}, Lmiui/animation/ITouchStyle;->touchUp([Lmiui/animation/base/AnimConfig;)V

    const-wide/16 v0, 0x96

    invoke-virtual {p1, p2, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method static synthetic lambda$animButtonTouch$2(Landroid/os/Handler;Ljava/lang/Runnable;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x3

    if-eq v0, v3, :cond_2

    if-eqz v0, :cond_1

    if-eq v0, v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance v1, Lmiuix/settings/utils/-$$Lambda$AnimUtil$doX-upXlyin_vgpzAF7eKuSJa2M;

    invoke-direct {v1, p2, p0, p1}, Lmiuix/settings/utils/-$$Lambda$AnimUtil$doX-upXlyin_vgpzAF7eKuSJa2M;-><init>(Landroid/view/View;Landroid/os/Handler;Ljava/lang/Runnable;)V

    const-wide/16 v3, 0x96

    invoke-virtual {p0, v1, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_1
    new-array v3, v2, [Landroid/view/View;

    aput-object p2, v3, v1

    invoke-static {v3}, Lmiui/animation/Folme;->useAt([Landroid/view/View;)Lmiui/animation/IFolme;

    move-result-object v3

    invoke-interface {v3}, Lmiui/animation/IFolme;->touch()Lmiui/animation/ITouchStyle;

    move-result-object v3

    const v4, 0x3f666666    # 0.9f

    new-array v5, v2, [Lmiui/animation/ITouchStyle$TouchType;

    sget-object v6, Lmiui/animation/ITouchStyle$TouchType;->DOWN:Lmiui/animation/ITouchStyle$TouchType;

    aput-object v6, v5, v1

    invoke-interface {v3, v4, v5}, Lmiui/animation/ITouchStyle;->setScale(F[Lmiui/animation/ITouchStyle$TouchType;)Lmiui/animation/ITouchStyle;

    move-result-object v3

    new-array v4, v2, [Lmiui/animation/base/AnimConfig;

    new-instance v5, Lmiui/animation/base/AnimConfig;

    invoke-direct {v5}, Lmiui/animation/base/AnimConfig;-><init>()V

    aput-object v5, v4, v1

    invoke-interface {v3, v4}, Lmiui/animation/ITouchStyle;->touchDown([Lmiui/animation/base/AnimConfig;)V

    nop

    :goto_0
    goto :goto_1

    :cond_2
    new-array v3, v2, [Landroid/view/View;

    aput-object p2, v3, v1

    invoke-static {v3}, Lmiui/animation/Folme;->useAt([Landroid/view/View;)Lmiui/animation/IFolme;

    move-result-object v3

    invoke-interface {v3}, Lmiui/animation/IFolme;->touch()Lmiui/animation/ITouchStyle;

    move-result-object v3

    new-array v4, v2, [Lmiui/animation/base/AnimConfig;

    new-instance v5, Lmiui/animation/base/AnimConfig;

    invoke-direct {v5}, Lmiui/animation/base/AnimConfig;-><init>()V

    aput-object v5, v4, v1

    invoke-interface {v3, v4}, Lmiui/animation/ITouchStyle;->touchUp([Lmiui/animation/base/AnimConfig;)V

    :goto_1
    return v2
.end method

.method static synthetic lambda$null$1(Landroid/view/View;Landroid/os/Handler;Ljava/lang/Runnable;)V
    .locals 5

    const/4 v0, 0x1

    new-array v1, v0, [Landroid/view/View;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v1}, Lmiui/animation/Folme;->useAt([Landroid/view/View;)Lmiui/animation/IFolme;

    move-result-object v1

    invoke-interface {v1}, Lmiui/animation/IFolme;->touch()Lmiui/animation/ITouchStyle;

    move-result-object v1

    new-array v3, v0, [Lmiui/animation/ITouchStyle$TouchType;

    sget-object v4, Lmiui/animation/ITouchStyle$TouchType;->UP:Lmiui/animation/ITouchStyle$TouchType;

    aput-object v4, v3, v2

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v1, v4, v3}, Lmiui/animation/ITouchStyle;->setScale(F[Lmiui/animation/ITouchStyle$TouchType;)Lmiui/animation/ITouchStyle;

    move-result-object v1

    new-array v0, v0, [Lmiui/animation/base/AnimConfig;

    new-instance v3, Lmiui/animation/base/AnimConfig;

    invoke-direct {v3}, Lmiui/animation/base/AnimConfig;-><init>()V

    aput-object v3, v0, v2

    invoke-interface {v1, v0}, Lmiui/animation/ITouchStyle;->touchUp([Lmiui/animation/base/AnimConfig;)V

    const-wide/16 v0, 0x96

    invoke-virtual {p1, p2, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
