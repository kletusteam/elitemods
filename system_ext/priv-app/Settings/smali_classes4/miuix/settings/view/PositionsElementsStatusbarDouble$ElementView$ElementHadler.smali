.class Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ElementHadler"
.end annotation


# instance fields
.field final synthetic this$1:Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;


# direct methods
.method private constructor <init>(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;)V
    .locals 0

    iput-object p1, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;->this$1:Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;Lmiuix/settings/view/PositionsElementsStatusbarDouble$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;-><init>(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->moveToPosition(Z)V

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;->this$1:Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;

    invoke-static {v0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->access$400(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;->this$1:Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;

    invoke-static {v0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->access$2600(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;)Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;

    move-result-object v0

    const/16 v1, 0xcb

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;->this$1:Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;

    iget-object v0, v0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$2700(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->access$502(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;Z)Z

    goto :goto_0

    :cond_1
    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;->this$1:Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;

    invoke-static {v0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->access$2500(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;)V

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;->this$1:Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->moveToPosition(Z)V

    goto :goto_1

    :pswitch_4
    iget-object v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;->this$1:Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;

    invoke-static {v0, v1}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->access$2400(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;)V

    nop

    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0xc8
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
