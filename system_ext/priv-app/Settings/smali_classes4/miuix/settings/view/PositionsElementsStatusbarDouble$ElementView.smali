.class Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;
.super Landroid/widget/ImageView;

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/settings/view/PositionsElementsStatusbarDouble;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ElementView"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;,
        Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;
    }
.end annotation


# static fields
.field private static final eventUp:I = 0xca

.field private static final falseTouch:I = 0xcb

.field private static final moveToPosition:I = 0xc9

.field private static final moveWithAnim:I = 0xc8

.field private static final otherElementmoveToPosition:I = 0xcc


# instance fields
.field private centerEnable:Z

.field private final elementHadler:Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;

.field private isCenter:Z

.field private mCenterZone:Z

.field private final mCoordinate:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;",
            ">;"
        }
    .end annotation
.end field

.field private mFlag:Z

.field private mName:Ljava/lang/String;

.field private mPosition:I

.field private mTouch:Z

.field private rX:F

.field private rY:F

.field final synthetic this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;


# direct methods
.method constructor <init>(Lmiuix/settings/view/PositionsElementsStatusbarDouble;Landroid/content/Context;)V
    .locals 1

    iput-object p1, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-direct {p0, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->isCenter:Z

    iput-boolean p1, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->centerEnable:Z

    iput-boolean p1, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mCenterZone:Z

    iput-boolean p1, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mTouch:Z

    iput-boolean p1, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mFlag:Z

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mCoordinate:Ljava/util/ArrayList;

    new-instance p1, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;

    const/4 v0, 0x0

    invoke-direct {p1, p0, v0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;-><init>(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;Lmiuix/settings/view/PositionsElementsStatusbarDouble$1;)V

    iput-object p1, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->elementHadler:Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;

    invoke-virtual {p0, p0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method static synthetic access$100(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$202(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->centerEnable:Z

    return p1
.end method

.method static synthetic access$2400(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->moveWithAnim(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;)V

    return-void
.end method

.method static synthetic access$2500(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;)V
    .locals 0

    invoke-direct {p0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->eventUp()V

    return-void
.end method

.method static synthetic access$2600(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;)Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;
    .locals 1

    iget-object v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->elementHadler:Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;

    return-object v0
.end method

.method static synthetic access$300(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;)I
    .locals 1

    iget v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    return v0
.end method

.method static synthetic access$308(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;)I
    .locals 2

    iget v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    return v0
.end method

.method static synthetic access$310(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;)I
    .locals 2

    iget v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    return v0
.end method

.method static synthetic access$400(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;)Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mFlag:Z

    return v0
.end method

.method static synthetic access$500(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;)Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mTouch:Z

    return v0
.end method

.method static synthetic access$502(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mTouch:Z

    return p1
.end method

.method static synthetic access$600(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;FFZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->onMovePosition(FFZ)V

    return-void
.end method

.method static synthetic access$700(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;)Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->isCenter:Z

    return v0
.end method

.method private eventUp()V
    .locals 10

    iget-object v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1300(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)F

    move-result v0

    iget-object v1, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v1}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1200(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p0, v2}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->setAlpha(F)V

    iget-boolean v2, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mFlag:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mCoordinate:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->getX()F

    move-result v2

    invoke-virtual {p0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->getY()F

    move-result v3

    invoke-virtual {p0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    iget-boolean v4, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->isCenter:Z

    if-eqz v4, :cond_2

    iget-boolean v6, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->centerEnable:Z

    if-eqz v6, :cond_2

    const/high16 v6, 0x40000000    # 2.0f

    div-float v7, v1, v6

    sub-float/2addr v7, v0

    const/high16 v8, 0x40400000    # 3.0f

    div-float v9, v0, v8

    sub-float/2addr v7, v9

    cmpl-float v7, v2, v7

    if-lez v7, :cond_2

    div-float v6, v1, v6

    div-float v7, v0, v8

    add-float/2addr v6, v7

    cmpg-float v6, v2, v6

    if-gez v6, :cond_2

    iget-object v4, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v4, v5}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1600(Lmiuix/settings/view/PositionsElementsStatusbarDouble;I)Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v6, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    iget-boolean v7, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->isCenter:Z

    invoke-static {v6, v2, v7}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1700(Lmiuix/settings/view/PositionsElementsStatusbarDouble;FZ)I

    move-result v6

    iput v6, v4, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    iget-object v6, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->elementHadler:Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;

    const/16 v7, 0xcc

    invoke-virtual {v6, v7, v4}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v7

    const-wide/16 v8, 0x32

    invoke-virtual {v6, v7, v8, v9}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_1
    iput v5, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v5, v2, v4}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1700(Lmiuix/settings/view/PositionsElementsStatusbarDouble;FZ)I

    move-result v4

    iput v4, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    :goto_0
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->moveToPosition(Z)V

    iget-object v4, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v4}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1800(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)V

    iget-object v4, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->elementHadler:Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;

    const/16 v5, 0xcb

    const-wide/16 v6, 0x64

    invoke-virtual {v4, v5, v6, v7}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_2

    :cond_3
    :goto_1
    iget-object v2, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->elementHadler:Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;

    const/16 v3, 0xca

    invoke-virtual {v2, v3}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;->sendMessage(Landroid/os/Message;)Z

    :goto_2
    return-void
.end method

.method private moveWithAnim(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;)V
    .locals 14

    iget-object v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$2100(Lmiuix/settings/view/PositionsElementsStatusbarDouble;Z)Z

    move-result v0

    const/16 v2, 0xc8

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mTouch:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->elementHadler:Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;

    invoke-virtual {v0, v2, p1}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_9

    :cond_0
    invoke-virtual {p0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->getX()F

    move-result v0

    invoke-virtual {p0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->getY()F

    move-result v3

    iget v4, p1, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;->mX:F

    sub-float/2addr v4, v0

    iget v5, p1, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;->mY:F

    sub-float/2addr v5, v3

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v6

    const/high16 v7, 0x41700000    # 15.0f

    div-float/2addr v6, v7

    const/high16 v7, 0x3f800000    # 1.0f

    cmpl-float v6, v6, v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    if-lez v6, :cond_2

    iget-boolean v6, p1, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;->needX:Z

    if-eqz v6, :cond_2

    cmpl-float v6, v4, v8

    if-lez v6, :cond_1

    const/16 v6, 0xf

    goto :goto_0

    :cond_1
    const/16 v6, -0xf

    :goto_0
    int-to-float v6, v6

    add-float/2addr v0, v6

    invoke-virtual {p0, v0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->setTranslationX(F)V

    goto :goto_1

    :cond_2
    iget-boolean v6, p1, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;->needX:Z

    if-eqz v6, :cond_3

    add-float/2addr v0, v4

    invoke-virtual {p0, v0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->setTranslationX(F)V

    iput-boolean v9, p1, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;->needX:Z

    :cond_3
    :goto_1
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v6

    const/high16 v10, 0x41200000    # 10.0f

    div-float/2addr v6, v10

    cmpl-float v6, v6, v7

    if-lez v6, :cond_5

    iget-boolean v6, p1, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;->needY:Z

    if-eqz v6, :cond_5

    cmpl-float v6, v5, v8

    if-lez v6, :cond_4

    const/16 v6, 0xa

    goto :goto_2

    :cond_4
    const/16 v6, -0xa

    :goto_2
    int-to-float v6, v6

    add-float/2addr v3, v6

    invoke-virtual {p0, v3}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->setTranslationY(F)V

    goto :goto_3

    :cond_5
    iget-boolean v6, p1, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;->needY:Z

    if-eqz v6, :cond_6

    add-float/2addr v3, v5

    invoke-virtual {p0, v3}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->setTranslationY(F)V

    iput-boolean v9, p1, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;->needY:Z

    :cond_6
    :goto_3
    iget-object v6, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v6}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1400(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)F

    move-result v6

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    iget-object v10, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v10}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1500(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)F

    move-result v10

    div-float/2addr v10, v7

    sub-float/2addr v6, v10

    cmpl-float v6, v3, v6

    if-lez v6, :cond_7

    move v6, v1

    goto :goto_4

    :cond_7
    move v6, v9

    :goto_4
    iget-boolean v10, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->isCenter:Z

    if-eq v10, v6, :cond_8

    iput-boolean v6, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->isCenter:Z

    const/4 v6, 0x1

    goto :goto_6

    :cond_8
    const/4 v6, 0x0

    if-eqz v10, :cond_9

    iget-object v10, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v10}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1300(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)F

    move-result v10

    const/high16 v11, 0x40400000    # 3.0f

    div-float/2addr v10, v11

    add-float/2addr v10, v0

    iget-object v12, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v12}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1200(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)F

    move-result v12

    div-float/2addr v12, v7

    iget-object v13, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v13}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1300(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)F

    move-result v13

    sub-float/2addr v12, v13

    cmpl-float v10, v10, v12

    if-lez v10, :cond_9

    iget-object v10, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v10}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1300(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)F

    move-result v10

    mul-float/2addr v10, v7

    div-float/2addr v10, v11

    add-float/2addr v10, v0

    iget-object v11, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v11}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1200(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)F

    move-result v11

    div-float/2addr v11, v7

    iget-object v7, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v7}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1300(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)F

    move-result v7

    add-float/2addr v11, v7

    cmpg-float v7, v10, v11

    if-gez v7, :cond_9

    move v7, v1

    goto :goto_5

    :cond_9
    move v7, v9

    :goto_5
    iget-boolean v10, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mCenterZone:Z

    if-eq v7, v10, :cond_a

    iput-boolean v7, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mCenterZone:Z

    iget-object v10, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    iget-boolean v11, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->isCenter:Z

    invoke-static {v10, v0, v11}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$2200(Lmiuix/settings/view/PositionsElementsStatusbarDouble;FZ)V

    :cond_a
    :goto_6
    iget-object v7, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    cmpg-float v8, v4, v8

    if-gez v8, :cond_b

    goto :goto_7

    :cond_b
    move v1, v9

    :goto_7
    invoke-static {v7, v0, v3, v1, v6}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$2300(Lmiuix/settings/view/PositionsElementsStatusbarDouble;FFZZ)V

    iget-boolean v1, p1, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;->needY:Z

    if-nez v1, :cond_d

    iget-boolean v1, p1, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;->needX:Z

    if-eqz v1, :cond_c

    goto :goto_8

    :cond_c
    invoke-virtual {p0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->moveElement()V

    goto :goto_9

    :cond_d
    :goto_8
    iget-object v1, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->elementHadler:Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;

    invoke-virtual {v1, v2, p1}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;->sendMessage(Landroid/os/Message;)Z

    :goto_9
    return-void
.end method

.method private onMovePosition(FFZ)V
    .locals 11

    iget-object v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1200(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)F

    move-result v0

    iget-object v1, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v1}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1300(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)F

    move-result v1

    invoke-virtual {p0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->getX()F

    move-result v2

    iget v3, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    iget-boolean v4, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mTouch:Z

    if-nez v4, :cond_e

    iget-boolean v4, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mFlag:Z

    if-nez v4, :cond_e

    iget v4, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    if-eqz v4, :cond_e

    iget-boolean v4, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->isCenter:Z

    iget-object v5, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v5}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1400(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)F

    move-result v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    iget-object v7, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v7}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1500(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)F

    move-result v7

    div-float/2addr v7, v6

    sub-float/2addr v5, v7

    cmpl-float v5, p2, v5

    const/4 v7, 0x0

    const/4 v8, 0x1

    if-lez v5, :cond_0

    move v5, v8

    goto :goto_0

    :cond_0
    move v5, v7

    :goto_0
    if-ne v4, v5, :cond_e

    iget-object v4, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v4}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1900(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)Z

    move-result v4

    const/4 v5, -0x1

    const/high16 v9, 0x40400000    # 3.0f

    if-eqz v4, :cond_6

    if-eqz p3, :cond_2

    mul-float v4, v1, v6

    div-float/2addr v4, v9

    add-float/2addr v4, v2

    cmpg-float v4, p1, v4

    if-gez v4, :cond_4

    cmpl-float v4, p1, v2

    if-lez v4, :cond_4

    iget v4, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    div-float v6, v0, v6

    cmpg-float v6, p1, v6

    if-ltz v6, :cond_1

    goto :goto_1

    :cond_1
    move v5, v8

    :goto_1
    add-int/2addr v4, v5

    iput v4, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    goto :goto_2

    :cond_2
    add-float v4, p1, v1

    mul-float v7, v1, v6

    div-float/2addr v7, v9

    add-float/2addr v7, v2

    cmpl-float v4, v4, v7

    if-lez v4, :cond_4

    cmpg-float v4, p1, v2

    if-gez v4, :cond_4

    iget v4, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    div-float v6, v0, v6

    cmpg-float v6, p1, v6

    if-ltz v6, :cond_3

    move v5, v8

    :cond_3
    add-int/2addr v4, v5

    iput v4, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    :cond_4
    :goto_2
    iget v4, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    if-eq v4, v3, :cond_e

    rem-int/lit8 v5, v4, 0xa

    if-eqz v5, :cond_5

    invoke-virtual {p0, v8}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->moveToPosition(Z)V

    goto/16 :goto_5

    :cond_5
    add-int/2addr v4, v8

    iput v4, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    iget-object v4, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    iget-boolean v5, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->isCenter:Z

    invoke-static {v4, v2, v5}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$2000(Lmiuix/settings/view/PositionsElementsStatusbarDouble;FZ)V

    goto :goto_5

    :cond_6
    if-eqz p3, :cond_9

    mul-float v4, v1, v6

    div-float/2addr v4, v9

    add-float/2addr v4, v2

    cmpg-float v4, p1, v4

    if-gez v4, :cond_c

    cmpl-float v4, p1, v2

    if-lez v4, :cond_c

    iget v4, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    div-float v6, v0, v6

    cmpl-float v6, p1, v6

    if-lez v6, :cond_7

    move v7, v8

    :cond_7
    iget-boolean v6, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->isCenter:Z

    xor-int/2addr v6, v7

    if-eqz v6, :cond_8

    goto :goto_3

    :cond_8
    move v5, v8

    :goto_3
    add-int/2addr v4, v5

    iput v4, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    goto :goto_4

    :cond_9
    add-float v4, p1, v1

    mul-float v10, v1, v6

    div-float/2addr v10, v9

    add-float/2addr v10, v2

    cmpl-float v4, v4, v10

    if-lez v4, :cond_c

    cmpg-float v4, p1, v2

    if-gez v4, :cond_c

    iget v4, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    div-float v6, v0, v6

    cmpl-float v6, p1, v6

    if-lez v6, :cond_a

    move v7, v8

    :cond_a
    iget-boolean v6, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->isCenter:Z

    xor-int/2addr v6, v7

    if-eqz v6, :cond_b

    move v5, v8

    :cond_b
    add-int/2addr v4, v5

    iput v4, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    :cond_c
    :goto_4
    iget v4, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    if-eq v4, v3, :cond_e

    rem-int/lit8 v5, v4, 0xa

    if-eqz v5, :cond_d

    invoke-virtual {p0, v8}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->moveToPosition(Z)V

    goto :goto_5

    :cond_d
    add-int/2addr v4, v8

    iput v4, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    iget-object v4, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    iget-boolean v5, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->isCenter:Z

    invoke-static {v4, v2, v5}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$2000(Lmiuix/settings/view/PositionsElementsStatusbarDouble;FZ)V

    :cond_e
    :goto_5
    return-void
.end method


# virtual methods
.method public moveElement()V
    .locals 3

    iget-object v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mCoordinate:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mFlag:Z

    iget-object v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mCoordinate:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;

    iget-object v2, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mCoordinate:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    invoke-direct {p0, v0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->moveWithAnim(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;)V

    goto :goto_0

    :cond_0
    iput-boolean v1, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mFlag:Z

    :goto_0
    return-void
.end method

.method public moveToPosition(Z)V
    .locals 7

    iget-boolean v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mFlag:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->elementHadler:Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;

    const/16 v1, 0xc9

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$ElementHadler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_2

    :cond_0
    iget-object v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1200(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)F

    move-result v0

    iget-object v1, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v1}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1300(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)F

    move-result v1

    iget-boolean v2, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->isCenter:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v2}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1400(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)F

    move-result v2

    iget-object v3, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v3}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1500(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)F

    move-result v3

    sub-float/2addr v2, v3

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    iget-object v3, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v3}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1900(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)Z

    move-result v3

    const/16 v4, 0x14

    const/16 v5, 0x1e

    const/high16 v6, 0x40000000    # 2.0f

    if-eqz v3, :cond_6

    iget v3, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    if-le v3, v5, :cond_2

    sub-int/2addr v3, v5

    int-to-float v3, v3

    mul-float/2addr v3, v1

    sub-float v3, v0, v3

    goto :goto_1

    :cond_2
    if-le v3, v4, :cond_3

    add-int/lit8 v3, v3, -0x15

    int-to-float v3, v3

    mul-float/2addr v3, v1

    goto :goto_1

    :cond_3
    add-int/lit8 v4, v3, -0xa

    if-lez v4, :cond_4

    add-int/lit8 v3, v3, -0xa

    int-to-float v3, v3

    mul-float/2addr v3, v1

    sub-float v3, v0, v3

    goto :goto_1

    :cond_4
    if-nez v3, :cond_5

    div-float v3, v0, v6

    div-float v4, v1, v6

    sub-float/2addr v3, v4

    goto :goto_1

    :cond_5
    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    mul-float/2addr v3, v1

    goto :goto_1

    :cond_6
    iget v3, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    if-le v3, v5, :cond_7

    iget-object v3, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    iget v3, v3, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->mRightCenterZoneStart:F

    iget v4, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    add-int/lit8 v4, v4, -0x1f

    int-to-float v4, v4

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    goto :goto_1

    :cond_7
    if-le v3, v4, :cond_8

    iget-object v3, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    iget v3, v3, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->mLeftCenterZoneEnd:F

    iget v5, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    sub-int/2addr v5, v4

    int-to-float v4, v5

    mul-float/2addr v4, v1

    sub-float/2addr v3, v4

    goto :goto_1

    :cond_8
    add-int/lit8 v4, v3, -0xa

    if-lez v4, :cond_9

    add-int/lit8 v3, v3, -0xa

    int-to-float v3, v3

    mul-float/2addr v3, v1

    sub-float v3, v0, v3

    goto :goto_1

    :cond_9
    if-nez v3, :cond_a

    div-float v3, v0, v6

    div-float v4, v1, v6

    sub-float/2addr v3, v4

    goto :goto_1

    :cond_a
    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    mul-float/2addr v3, v1

    :goto_1
    if-eqz p1, :cond_c

    new-instance v4, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;-><init>(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;Lmiuix/settings/view/PositionsElementsStatusbarDouble$1;)V

    iput v3, v4, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;->mX:F

    iput v2, v4, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;->mY:F

    iget-object v5, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mCoordinate:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-boolean v5, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mFlag:Z

    if-nez v5, :cond_b

    invoke-virtual {p0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->moveElement()V

    :cond_b
    goto :goto_2

    :cond_c
    invoke-virtual {p0, v3}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->setTranslationX(F)V

    invoke-virtual {p0, v2}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->setTranslationY(F)V

    :goto_2
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9

    iget-object v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$900(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)Z

    move-result v0

    iget-boolean v1, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mTouch:Z

    const/4 v2, 0x1

    if-ne v0, v1, :cond_5

    const/4 v0, 0x2

    new-array v1, v0, [I

    iget-object v3, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-virtual {v3, v1}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->getLocationOnScreen([I)V

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    iget-object v4, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v4}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1000(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)F

    move-result v4

    sub-float/2addr v3, v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    aget v5, v1, v2

    int-to-float v5, v5

    sub-float/2addr v4, v5

    invoke-virtual {p0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->bringToFront()V

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    and-int/lit16 v5, v5, 0xff

    if-eqz v5, :cond_4

    if-eq v5, v2, :cond_3

    if-eq v5, v0, :cond_0

    const/4 v0, 0x3

    if-eq v5, v0, :cond_3

    goto/16 :goto_1

    :cond_0
    new-instance v0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;

    const/4 v5, 0x0

    invoke-direct {v0, p0, v5}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;-><init>(Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;Lmiuix/settings/view/PositionsElementsStatusbarDouble$1;)V

    iget v5, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->rX:F

    cmpl-float v6, v3, v5

    const/4 v7, 0x0

    if-ltz v6, :cond_1

    sub-float v5, v3, v5

    iget-object v6, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v6}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1200(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)F

    move-result v6

    iget-object v8, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v8}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1300(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)F

    move-result v8

    sub-float/2addr v6, v8

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_1

    move v5, v2

    goto :goto_0

    :cond_1
    move v5, v7

    :goto_0
    iput-boolean v5, v0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;->needX:Z

    iget v5, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->rX:F

    sub-float v5, v3, v5

    iput v5, v0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;->mX:F

    iget v5, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->rY:F

    cmpl-float v6, v4, v5

    if-ltz v6, :cond_2

    sub-float v5, v4, v5

    iget-object v6, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v6}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1400(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)F

    move-result v6

    iget-object v8, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->this$0:Lmiuix/settings/view/PositionsElementsStatusbarDouble;

    invoke-static {v8}, Lmiuix/settings/view/PositionsElementsStatusbarDouble;->access$1500(Lmiuix/settings/view/PositionsElementsStatusbarDouble;)F

    move-result v8

    sub-float/2addr v6, v8

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_2

    move v7, v2

    :cond_2
    iput-boolean v7, v0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;->needY:Z

    iget v5, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->rY:F

    sub-float v5, v4, v5

    iput v5, v0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView$Coordinate;->mY:F

    iget-object v5, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mCoordinate:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-boolean v5, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mFlag:Z

    if-nez v5, :cond_5

    invoke-virtual {p0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->moveElement()V

    goto :goto_1

    :cond_3
    invoke-direct {p0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->eventUp()V

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    iput-boolean v2, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mTouch:Z

    const/high16 v0, 0x3f000000    # 0.5f

    invoke-virtual {p0, v0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->setAlpha(F)V

    const/16 v0, 0x33

    iput v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    invoke-virtual {p0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->getX()F

    move-result v0

    sub-float v0, v3, v0

    iput v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->rX:F

    invoke-virtual {p0}, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->getY()F

    move-result v0

    sub-float v0, v4, v0

    iput v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->rY:F

    :cond_5
    :goto_1
    return v2
.end method

.method public setPosition(I)V
    .locals 3

    iput p1, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mPosition:I

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    move v2, v0

    goto :goto_0

    :cond_0
    move v2, v1

    :goto_0
    iput-boolean v2, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->mCenterZone:Z

    if-nez v2, :cond_2

    const/16 v2, 0x14

    if-le p1, v2, :cond_1

    goto :goto_1

    :cond_1
    move v0, v1

    :cond_2
    :goto_1
    iput-boolean v0, p0, Lmiuix/settings/view/PositionsElementsStatusbarDouble$ElementView;->isCenter:Z

    return-void
.end method
