.class public Lmiuix/settings/cardview/CardsGridView$GredAdapter;
.super Landroid/widget/BaseAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/settings/cardview/CardsGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GredAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/settings/cardview/CardsGridView$GredAdapter$Tile;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmiuix/settings/cardview/CardsGridView$GredData;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lmiuix/settings/cardview/CardsGridView;


# direct methods
.method constructor <init>(Lmiuix/settings/cardview/CardsGridView;Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lmiuix/settings/cardview/CardsGridView$GredData;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lmiuix/settings/cardview/CardsGridView$GredAdapter;->this$0:Lmiuix/settings/cardview/CardsGridView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p2, p0, Lmiuix/settings/cardview/CardsGridView$GredAdapter;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lmiuix/settings/cardview/CardsGridView$GredAdapter;->mList:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lmiuix/settings/cardview/CardsGridView$GredAdapter;->mList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmiuix/settings/cardview/CardsGridView$GredAdapter;->mList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    const v6, 0x3e99999a    # 0.3f

    if-nez p2, :cond_1

    iget-object v3, p0, Lmiuix/settings/cardview/CardsGridView$GredAdapter;->this$0:Lmiuix/settings/cardview/CardsGridView;

    invoke-virtual {v3}, Lmiuix/settings/cardview/CardsGridView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v3, p0, Lmiuix/settings/cardview/CardsGridView$GredAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const-string v3, "card_item_layout"

    invoke-static {v0, v3}, Landroid/Utils/Utils;->LayoutToID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    const/4 v3, 0x0

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v4, v5, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    new-instance v2, Lmiuix/settings/cardview/CardsGridView$GredAdapter$Tile;

    invoke-direct {v2, p0}, Lmiuix/settings/cardview/CardsGridView$GredAdapter$Tile;-><init>(Lmiuix/settings/cardview/CardsGridView$GredAdapter;)V

    const-string v3, "icon"

    invoke-static {v0, v3}, Landroid/Utils/Utils;->IDtoID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v2, Lmiuix/settings/cardview/CardsGridView$GredAdapter$Tile;->icon:Landroid/widget/ImageView;

    const-string/jumbo v3, "title"

    invoke-static {v0, v3}, Landroid/Utils/Utils;->IDtoID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v2, Lmiuix/settings/cardview/CardsGridView$GredAdapter$Tile;->title:Landroid/widget/TextView;

    const-string/jumbo v3, "summary"

    invoke-static {v0, v3}, Landroid/Utils/Utils;->IDtoID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v2, Lmiuix/settings/cardview/CardsGridView$GredAdapter$Tile;->summary:Landroid/widget/TextView;

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v3, p0, Lmiuix/settings/cardview/CardsGridView$GredAdapter;->mList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/settings/cardview/CardsGridView$GredData;

    if-eqz v1, :cond_0

    iget-object v3, v2, Lmiuix/settings/cardview/CardsGridView$GredAdapter$Tile;->icon:Landroid/widget/ImageView;

    invoke-virtual {v1}, Lmiuix/settings/cardview/CardsGridView$GredData;->getIconIds()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v3, v2, Lmiuix/settings/cardview/CardsGridView$GredAdapter$Tile;->title:Landroid/widget/TextView;

    invoke-virtual {v1}, Lmiuix/settings/cardview/CardsGridView$GredData;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, v2, Lmiuix/settings/cardview/CardsGridView$GredAdapter$Tile;->summary:Landroid/widget/TextView;

    invoke-virtual {v1}, Lmiuix/settings/cardview/CardsGridView$GredData;->getSummary()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Lmiuix/settings/cardview/CardsGridView$GredData;->isDisable()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v2, Lmiuix/settings/cardview/CardsGridView$GredAdapter$Tile;->icon:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setAlpha(F)V

    iget-object v3, v2, Lmiuix/settings/cardview/CardsGridView$GredAdapter$Tile;->title:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setAlpha(F)V

    iget-object v3, v2, Lmiuix/settings/cardview/CardsGridView$GredAdapter$Tile;->summary:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setAlpha(F)V

    :cond_0
    new-instance v3, Lmiuix/settings/cardview/CardsGridView$GredAdapter$1;

    invoke-direct {v3, p0, v1}, Lmiuix/settings/cardview/CardsGridView$GredAdapter$1;-><init>(Lmiuix/settings/cardview/CardsGridView$GredAdapter;Lmiuix/settings/cardview/CardsGridView$GredData;)V

    invoke-static {p2, v3}, Lmiuix/settings/utils/AnimUtil;->animButtonTouch(Landroid/view/View;Ljava/lang/Runnable;)V

    return-object p2

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/settings/cardview/CardsGridView$GredAdapter$Tile;

    goto :goto_0
.end method
