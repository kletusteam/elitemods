.class public Lmiuix/settings/cardview/CardsGridView;
.super Landroid/widget/RelativeLayout;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/settings/cardview/CardsGridView$GredAdapter;,
        Lmiuix/settings/cardview/CardsGridView$GredData;
    }
.end annotation


# instance fields
.field protected cardGridView:Lmiuix/settings/cardview/CardGridView;

.field protected mAdapter:Landroid/widget/BaseAdapter;

.field protected mContext:Landroid/content/Context;

.field protected mData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lmiuix/settings/cardview/CardsGridView$GredData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmiuix/settings/cardview/CardsGridView;->mAdapter:Landroid/widget/BaseAdapter;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/settings/cardview/CardsGridView;->mData:Ljava/util/ArrayList;

    iput-object p1, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmiuix/settings/cardview/CardsGridView;->mAdapter:Landroid/widget/BaseAdapter;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/settings/cardview/CardsGridView;->mData:Ljava/util/ArrayList;

    iput-object p1, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    return-void
.end method

.method private getDataToCount(I)Lmiuix/settings/cardview/CardsGridView$GredData;
    .locals 3

    iget-object v1, p0, Lmiuix/settings/cardview/CardsGridView;->mData:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/settings/cardview/CardsGridView$GredData;

    invoke-virtual {v0}, Lmiuix/settings/cardview/CardsGridView$GredData;->getCount()I

    move-result v2

    if-ne v2, p1, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected init()V
    .locals 3

    iget-object v0, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    const-string v2, "card_settings_layout"

    invoke-static {v1, v2}, Landroid/Utils/Utils;->LayoutToID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    iget-object v0, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    const-string v1, "grid_view"

    invoke-static {v0, v1}, Landroid/Utils/Utils;->IDtoID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lmiuix/settings/cardview/CardsGridView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/settings/cardview/CardGridView;

    iput-object v0, p0, Lmiuix/settings/cardview/CardsGridView;->cardGridView:Lmiuix/settings/cardview/CardGridView;

    return-void
.end method

.method public initData()V
    .locals 12

    invoke-virtual {p0}, Lmiuix/settings/cardview/CardsGridView;->init()V

    new-instance v6, Lmiuix/settings/cardview/CardsGridView$GredData;

    const/4 v7, 0x0

    iget-object v8, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    const-string v9, "elem_clock_card"

    invoke-static {v8, v9}, Landroid/Utils/Utils;->DrawableToID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v8

    iget-object v9, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    iget-object v10, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    const-string v11, "clock_card_title"

    invoke-static {v10, v11}, Landroid/Utils/Utils;->StringToID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, p0, v7, v8, v9}, Lmiuix/settings/cardview/CardsGridView$GredData;-><init>(Lmiuix/settings/cardview/CardsGridView;IILjava/lang/String;)V

    iget-object v7, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    const-string v9, "clock_card_summary"

    invoke-static {v8, v9}, Landroid/Utils/Utils;->StringToID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lmiuix/settings/cardview/CardsGridView$GredData;->setSummary(Ljava/lang/String;)Lmiuix/settings/cardview/CardsGridView$GredData;

    move-result-object v0

    new-instance v6, Lmiuix/settings/cardview/CardsGridView$GredData;

    const/4 v7, 0x1

    iget-object v8, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    const-string v9, "elem_bat_card"

    invoke-static {v8, v9}, Landroid/Utils/Utils;->DrawableToID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v8

    iget-object v9, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    iget-object v10, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    const-string v11, "bat_card_title"

    invoke-static {v10, v11}, Landroid/Utils/Utils;->StringToID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, p0, v7, v8, v9}, Lmiuix/settings/cardview/CardsGridView$GredData;-><init>(Lmiuix/settings/cardview/CardsGridView;IILjava/lang/String;)V

    iget-object v7, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    const-string v9, "bat_card_summary"

    invoke-static {v8, v9}, Landroid/Utils/Utils;->StringToID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lmiuix/settings/cardview/CardsGridView$GredData;->setSummary(Ljava/lang/String;)Lmiuix/settings/cardview/CardsGridView$GredData;

    move-result-object v1

    new-instance v6, Lmiuix/settings/cardview/CardsGridView$GredData;

    const/4 v7, 0x2

    iget-object v8, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    const-string v9, "elem_net_card"

    invoke-static {v8, v9}, Landroid/Utils/Utils;->DrawableToID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v8

    iget-object v9, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    iget-object v10, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    const-string v11, "net_card_title"

    invoke-static {v10, v11}, Landroid/Utils/Utils;->StringToID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, p0, v7, v8, v9}, Lmiuix/settings/cardview/CardsGridView$GredData;-><init>(Lmiuix/settings/cardview/CardsGridView;IILjava/lang/String;)V

    iget-object v7, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    const-string v9, "net_card_summary"

    invoke-static {v8, v9}, Landroid/Utils/Utils;->StringToID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lmiuix/settings/cardview/CardsGridView$GredData;->setSummary(Ljava/lang/String;)Lmiuix/settings/cardview/CardsGridView$GredData;

    move-result-object v2

    new-instance v6, Lmiuix/settings/cardview/CardsGridView$GredData;

    const/4 v7, 0x3

    iget-object v8, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    const-string v9, "elem_speed_card"

    invoke-static {v8, v9}, Landroid/Utils/Utils;->DrawableToID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v8

    iget-object v9, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    iget-object v10, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    const-string/jumbo v11, "speed_card_title"

    invoke-static {v10, v11}, Landroid/Utils/Utils;->StringToID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, p0, v7, v8, v9}, Lmiuix/settings/cardview/CardsGridView$GredData;-><init>(Lmiuix/settings/cardview/CardsGridView;IILjava/lang/String;)V

    iget-object v7, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    const-string/jumbo v9, "speed_card_summary"

    invoke-static {v8, v9}, Landroid/Utils/Utils;->StringToID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lmiuix/settings/cardview/CardsGridView$GredData;->setSummary(Ljava/lang/String;)Lmiuix/settings/cardview/CardsGridView$GredData;

    move-result-object v3

    new-instance v6, Lmiuix/settings/cardview/CardsGridView$GredData;

    const/4 v7, 0x4

    iget-object v8, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    const-string v9, "elem_notif_card"

    invoke-static {v8, v9}, Landroid/Utils/Utils;->DrawableToID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v8

    iget-object v9, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    iget-object v10, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    const-string v11, "notif_card_title"

    invoke-static {v10, v11}, Landroid/Utils/Utils;->StringToID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, p0, v7, v8, v9}, Lmiuix/settings/cardview/CardsGridView$GredData;-><init>(Lmiuix/settings/cardview/CardsGridView;IILjava/lang/String;)V

    iget-object v7, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    const-string v9, "notif_card_summary"

    invoke-static {v8, v9}, Landroid/Utils/Utils;->StringToID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lmiuix/settings/cardview/CardsGridView$GredData;->setSummary(Ljava/lang/String;)Lmiuix/settings/cardview/CardsGridView$GredData;

    move-result-object v4

    new-instance v6, Lmiuix/settings/cardview/CardsGridView$GredData;

    const/4 v7, 0x5

    iget-object v8, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    const-string v9, "elem_status_card"

    invoke-static {v8, v9}, Landroid/Utils/Utils;->DrawableToID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v8

    iget-object v9, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    iget-object v10, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    const-string/jumbo v11, "status_card_title"

    invoke-static {v10, v11}, Landroid/Utils/Utils;->StringToID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, p0, v7, v8, v9}, Lmiuix/settings/cardview/CardsGridView$GredData;-><init>(Lmiuix/settings/cardview/CardsGridView;IILjava/lang/String;)V

    iget-object v7, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    const-string/jumbo v9, "status_card_summary"

    invoke-static {v8, v9}, Landroid/Utils/Utils;->StringToID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lmiuix/settings/cardview/CardsGridView$GredData;->setSummary(Ljava/lang/String;)Lmiuix/settings/cardview/CardsGridView$GredData;

    move-result-object v5

    iget-object v6, p0, Lmiuix/settings/cardview/CardsGridView;->mData:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v6, p0, Lmiuix/settings/cardview/CardsGridView;->mData:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v6, p0, Lmiuix/settings/cardview/CardsGridView;->mData:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v6, p0, Lmiuix/settings/cardview/CardsGridView;->mData:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v6, p0, Lmiuix/settings/cardview/CardsGridView;->mData:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v6, p0, Lmiuix/settings/cardview/CardsGridView;->mData:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v6, Lmiuix/settings/cardview/CardsGridView$GredAdapter;

    iget-object v7, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lmiuix/settings/cardview/CardsGridView;->mData:Ljava/util/ArrayList;

    invoke-direct {v6, p0, v7, v8}, Lmiuix/settings/cardview/CardsGridView$GredAdapter;-><init>(Lmiuix/settings/cardview/CardsGridView;Landroid/content/Context;Ljava/util/List;)V

    iput-object v6, p0, Lmiuix/settings/cardview/CardsGridView;->mAdapter:Landroid/widget/BaseAdapter;

    iget-object v6, p0, Lmiuix/settings/cardview/CardsGridView;->cardGridView:Lmiuix/settings/cardview/CardGridView;

    iget-object v7, p0, Lmiuix/settings/cardview/CardsGridView;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v6, v7}, Lmiuix/settings/cardview/CardGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public isDisableToCount(I)Z
    .locals 2

    invoke-direct {p0, p1}, Lmiuix/settings/cardview/CardsGridView;->getDataToCount(I)Lmiuix/settings/cardview/CardsGridView$GredData;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/settings/cardview/CardsGridView$GredData;->isDisable()Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    invoke-virtual {p0}, Lmiuix/settings/cardview/CardsGridView;->initData()V

    return-void
.end method

.method public setDisable(I)V
    .locals 3

    invoke-direct {p0, p1}, Lmiuix/settings/cardview/CardsGridView;->getDataToCount(I)Lmiuix/settings/cardview/CardsGridView$GredData;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lmiuix/settings/cardview/CardsGridView$GredData;->setDisable(Z)V

    iget-object v0, p0, Lmiuix/settings/cardview/CardsGridView;->mAdapter:Landroid/widget/BaseAdapter;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public setSummaryToCount(ILjava/lang/CharSequence;)V
    .locals 3

    invoke-direct {p0, p1}, Lmiuix/settings/cardview/CardsGridView;->getDataToCount(I)Lmiuix/settings/cardview/CardsGridView$GredData;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiuix/settings/cardview/CardsGridView$GredData;->setSummary(Ljava/lang/String;)Lmiuix/settings/cardview/CardsGridView$GredData;

    iget-object v0, p0, Lmiuix/settings/cardview/CardsGridView;->mAdapter:Landroid/widget/BaseAdapter;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method protected startActivityofItem(I)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    const-class v2, Lcom/android/settings/wifi/WifiConfigInfo;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "miui.intent.action.Elements"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "value"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lmiuix/settings/cardview/CardsGridView;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
