.class public Lmiuix/settings/cardview/CardsGridView$GredData;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/settings/cardview/CardsGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GredData"
.end annotation


# instance fields
.field private count:I

.field private disable:Z

.field private iconIds:I

.field private summary:Ljava/lang/String;

.field final synthetic this$0:Lmiuix/settings/cardview/CardsGridView;

.field private title:Ljava/lang/String;


# direct methods
.method constructor <init>(Lmiuix/settings/cardview/CardsGridView;IILjava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lmiuix/settings/cardview/CardsGridView$GredData;->this$0:Lmiuix/settings/cardview/CardsGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/settings/cardview/CardsGridView$GredData;->disable:Z

    iput p2, p0, Lmiuix/settings/cardview/CardsGridView$GredData;->count:I

    iput p3, p0, Lmiuix/settings/cardview/CardsGridView$GredData;->iconIds:I

    iput-object p4, p0, Lmiuix/settings/cardview/CardsGridView$GredData;->title:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget v0, p0, Lmiuix/settings/cardview/CardsGridView$GredData;->count:I

    return v0
.end method

.method public getIconIds()I
    .locals 1

    iget v0, p0, Lmiuix/settings/cardview/CardsGridView$GredData;->iconIds:I

    return v0
.end method

.method public getSummary()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmiuix/settings/cardview/CardsGridView$GredData;->summary:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmiuix/settings/cardview/CardsGridView$GredData;->title:Ljava/lang/String;

    return-object v0
.end method

.method isDisable()Z
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    iget-boolean v0, p0, Lmiuix/settings/cardview/CardsGridView$GredData;->disable:Z

    goto/32 :goto_0

    nop
.end method

.method public setDisable(Z)V
    .locals 0

    iput-boolean p1, p0, Lmiuix/settings/cardview/CardsGridView$GredData;->disable:Z

    return-void
.end method

.method public setSummary(Ljava/lang/String;)Lmiuix/settings/cardview/CardsGridView$GredData;
    .locals 0

    iput-object p1, p0, Lmiuix/settings/cardview/CardsGridView$GredData;->summary:Ljava/lang/String;

    return-object p0
.end method
