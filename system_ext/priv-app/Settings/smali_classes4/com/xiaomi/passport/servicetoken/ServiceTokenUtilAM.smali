.class public Lcom/xiaomi/passport/servicetoken/ServiceTokenUtilAM;
.super Lcom/xiaomi/passport/servicetoken/ServiceTokenUtilImplBase;


# instance fields
.field private final amUtil:Lcom/xiaomi/passport/servicetoken/IAMUtil;


# direct methods
.method public constructor <init>(Lcom/xiaomi/passport/servicetoken/IAMUtil;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/passport/servicetoken/ServiceTokenUtilImplBase;-><init>()V

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/xiaomi/passport/servicetoken/ServiceTokenUtilAM;->amUtil:Lcom/xiaomi/passport/servicetoken/IAMUtil;

    return-void

    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "amUtil == null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method static checkAndGet(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    if-eqz p0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, ","

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    array-length v1, p1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 p0, 0x1

    aget-object v0, p1, p0

    :cond_1
    :goto_0
    return-object v0
.end method

.method private getServiceTokenImpl(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/passport/servicetoken/ServiceTokenUtilAM;->amUtil:Lcom/xiaomi/passport/servicetoken/IAMUtil;

    invoke-interface {v0, p1}, Lcom/xiaomi/passport/servicetoken/IAMUtil;->getXiaomiAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-direct {p0, p2}, Lcom/xiaomi/passport/servicetoken/ServiceTokenUtilAM;->noAccountErrorResult(Ljava/lang/String;)Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;

    move-result-object p0

    return-object p0

    :cond_0
    iget-object v1, p0, Lcom/xiaomi/passport/servicetoken/ServiceTokenUtilAM;->amUtil:Lcom/xiaomi/passport/servicetoken/IAMUtil;

    invoke-interface {v1, p1, p2, v0}, Lcom/xiaomi/passport/servicetoken/IAMUtil;->peekAuthToken(Landroid/content/Context;Ljava/lang/String;Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 p3, 0x0

    const/4 v2, 0x1

    invoke-static {p3, p2, v1, v2}, Lcom/xiaomi/passport/servicetoken/AMAuthTokenConverter;->parseAMAuthToken(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Z)Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;

    move-result-object p2

    invoke-virtual {p0, p1, v0, p2}, Lcom/xiaomi/passport/servicetoken/ServiceTokenUtilAM;->addAccountAdditionalInfo(Landroid/content/Context;Landroid/accounts/Account;Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;)Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;

    move-result-object p0

    return-object p0

    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/xiaomi/passport/servicetoken/ServiceTokenUtilAM;->amUtil:Lcom/xiaomi/passport/servicetoken/IAMUtil;

    invoke-interface {v1, p1, p2, v0, p3}, Lcom/xiaomi/passport/servicetoken/IAMUtil;->getAuthToken(Landroid/content/Context;Ljava/lang/String;Landroid/accounts/Account;Landroid/os/Bundle;)Landroid/accounts/AccountManagerFuture;

    move-result-object p3

    invoke-interface {p3}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    invoke-static {p3, p2}, Lcom/xiaomi/passport/servicetoken/AMAuthTokenConverter;->fromAMBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;

    move-result-object p2

    invoke-virtual {p0, p1, v0, p2}, Lcom/xiaomi/passport/servicetoken/ServiceTokenUtilAM;->addAccountAdditionalInfo(Landroid/content/Context;Landroid/accounts/Account;Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;)Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;

    move-result-object p0

    return-object p0

    :catch_0
    move-exception p0

    invoke-static {p2, p0}, Lcom/xiaomi/passport/servicetoken/AMAuthTokenConverter;->fromAMException(Ljava/lang/String;Ljava/lang/Exception;)Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;

    move-result-object p0

    return-object p0
.end method

.method private noAccountErrorResult(Ljava/lang/String;)Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;
    .locals 0

    new-instance p0, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;

    invoke-direct {p0, p1}, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;-><init>(Ljava/lang/String;)V

    sget-object p1, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$ErrorCode;->ERROR_NO_ACCOUNT:Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$ErrorCode;

    invoke-virtual {p0, p1}, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;->errorCode(Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$ErrorCode;)Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;->build()Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method final addAccountAdditionalInfo(Landroid/content/Context;Landroid/accounts/Account;Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;)Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;
    .locals 4

    goto/32 :goto_1e

    nop

    :goto_0
    invoke-virtual {p1, v0}, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;->errorCode(Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$ErrorCode;)Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;

    move-result-object p1

    goto/32 :goto_14

    nop

    :goto_1
    iget-object v0, p3, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;->serviceToken:Ljava/lang/String;

    goto/32 :goto_c

    nop

    :goto_2
    if-eqz v0, :cond_0

    goto/32 :goto_21

    :cond_0
    goto/32 :goto_1

    nop

    :goto_3
    invoke-virtual {p0, p1}, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;->userId(Ljava/lang/String;)Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;

    move-result-object p0

    goto/32 :goto_1b

    nop

    :goto_4
    invoke-virtual {p1, p0}, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;->ph(Ljava/lang/String;)Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;

    move-result-object p0

    goto/32 :goto_2d

    nop

    :goto_5
    invoke-interface {v1, p1, p2}, Lcom/xiaomi/passport/servicetoken/IAMUtil;->getCUserId(Landroid/content/Context;Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_e

    nop

    :goto_6
    invoke-virtual {p1, v0}, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;->stsCookies(Ljava/lang/String;)Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;

    move-result-object p1

    goto/32 :goto_2c

    nop

    :goto_7
    invoke-static {v0, v2}, Lcom/xiaomi/passport/servicetoken/ServiceTokenUtilAM;->checkAndGet(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_23

    nop

    :goto_8
    iget-object v0, p3, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;->sid:Ljava/lang/String;

    goto/32 :goto_16

    nop

    :goto_9
    goto/16 :goto_21

    :goto_a
    goto/32 :goto_25

    nop

    :goto_b
    sget-object v1, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$ErrorCode;->ERROR_NONE:Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$ErrorCode;

    goto/32 :goto_12

    nop

    :goto_c
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    goto/32 :goto_28

    nop

    :goto_d
    invoke-virtual {p1, p3}, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;->peeked(Z)Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;

    move-result-object p1

    goto/32 :goto_18

    nop

    :goto_e
    iget-object v2, p0, Lcom/xiaomi/passport/servicetoken/ServiceTokenUtilAM;->amUtil:Lcom/xiaomi/passport/servicetoken/IAMUtil;

    goto/32 :goto_2b

    nop

    :goto_f
    iget-object v0, p3, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;->sid:Ljava/lang/String;

    goto/32 :goto_1c

    nop

    :goto_10
    invoke-virtual {p1, v0}, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;->errorStackTrace(Ljava/lang/String;)Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;

    move-result-object p1

    goto/32 :goto_22

    nop

    :goto_11
    invoke-virtual {p1, v0}, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;->security(Ljava/lang/String;)Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;

    move-result-object p1

    goto/32 :goto_26

    nop

    :goto_12
    if-eq v0, v1, :cond_1

    goto/32 :goto_21

    :cond_1
    goto/32 :goto_8

    nop

    :goto_13
    new-instance p1, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;

    goto/32 :goto_f

    nop

    :goto_14
    iget-object v0, p3, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;->errorMessage:Ljava/lang/String;

    goto/32 :goto_15

    nop

    :goto_15
    invoke-virtual {p1, v0}, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;->errorMessage(Ljava/lang/String;)Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;

    move-result-object p1

    goto/32 :goto_29

    nop

    :goto_16
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    goto/32 :goto_2

    nop

    :goto_17
    iget-object v1, p0, Lcom/xiaomi/passport/servicetoken/ServiceTokenUtilAM;->amUtil:Lcom/xiaomi/passport/servicetoken/IAMUtil;

    goto/32 :goto_5

    nop

    :goto_18
    invoke-virtual {p1, v1}, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;->cUserId(Ljava/lang/String;)Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;

    move-result-object p1

    goto/32 :goto_1a

    nop

    :goto_19
    iget-object v0, p3, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;->stsCookies:Ljava/lang/String;

    goto/32 :goto_6

    nop

    :goto_1a
    invoke-virtual {p1, v2}, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;->slh(Ljava/lang/String;)Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;

    move-result-object p1

    goto/32 :goto_4

    nop

    :goto_1b
    invoke-virtual {p0}, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;->build()Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;

    move-result-object p0

    goto/32 :goto_20

    nop

    :goto_1c
    invoke-direct {p1, v0}, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;-><init>(Ljava/lang/String;)V

    goto/32 :goto_30

    nop

    :goto_1d
    return-object p3

    :goto_1e
    iget-object v0, p3, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;->errorCode:Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$ErrorCode;

    goto/32 :goto_b

    nop

    :goto_1f
    invoke-virtual {p1, v0}, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;->serviceToken(Ljava/lang/String;)Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;

    move-result-object p1

    goto/32 :goto_19

    nop

    :goto_20
    return-object p0

    :goto_21
    goto/32 :goto_1d

    nop

    :goto_22
    iget-boolean p3, p3, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;->peeked:Z

    goto/32 :goto_d

    nop

    :goto_23
    iget-object p0, p0, Lcom/xiaomi/passport/servicetoken/ServiceTokenUtilAM;->amUtil:Lcom/xiaomi/passport/servicetoken/IAMUtil;

    goto/32 :goto_24

    nop

    :goto_24
    iget-object v3, p3, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;->sid:Ljava/lang/String;

    goto/32 :goto_2e

    nop

    :goto_25
    iget-object v0, p3, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;->serviceToken:Ljava/lang/String;

    goto/32 :goto_2a

    nop

    :goto_26
    iget-object v0, p3, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;->errorCode:Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$ErrorCode;

    goto/32 :goto_0

    nop

    :goto_27
    invoke-static {v0, p0}, Lcom/xiaomi/passport/servicetoken/ServiceTokenUtilAM;->checkAndGet(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_13

    nop

    :goto_28
    if-nez v0, :cond_2

    goto/32 :goto_a

    :cond_2
    goto/32 :goto_9

    nop

    :goto_29
    iget-object v0, p3, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;->errorStackTrace:Ljava/lang/String;

    goto/32 :goto_10

    nop

    :goto_2a
    invoke-static {v0}, Lcom/xiaomi/accountsdk/utils/Coder;->getMd5DigestUpperCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_17

    nop

    :goto_2b
    iget-object v3, p3, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;->sid:Ljava/lang/String;

    goto/32 :goto_2f

    nop

    :goto_2c
    iget-object v0, p3, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;->security:Ljava/lang/String;

    goto/32 :goto_11

    nop

    :goto_2d
    iget-object p1, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto/32 :goto_3

    nop

    :goto_2e
    invoke-interface {p0, p1, v3, p2}, Lcom/xiaomi/passport/servicetoken/IAMUtil;->getPh(Landroid/content/Context;Ljava/lang/String;Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_27

    nop

    :goto_2f
    invoke-interface {v2, p1, v3, p2}, Lcom/xiaomi/passport/servicetoken/IAMUtil;->getSlh(Landroid/content/Context;Ljava/lang/String;Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_7

    nop

    :goto_30
    iget-object v0, p3, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;->serviceToken:Ljava/lang/String;

    goto/32 :goto_1f

    nop
.end method

.method public final getServiceTokenImpl(Landroid/content/Context;Ljava/lang/String;)Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/xiaomi/passport/servicetoken/ServiceTokenUtilAM;->getServiceTokenImpl(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;

    move-result-object p0

    return-object p0
.end method

.method protected final invalidateServiceTokenImpl(Landroid/content/Context;Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;)Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/passport/servicetoken/ServiceTokenUtilAM;->amUtil:Lcom/xiaomi/passport/servicetoken/IAMUtil;

    invoke-interface {v0, p1}, Lcom/xiaomi/passport/servicetoken/IAMUtil;->getXiaomiAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object p1, p2, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;->sid:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/xiaomi/passport/servicetoken/ServiceTokenUtilAM;->noAccountErrorResult(Ljava/lang/String;)Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-static {p2}, Lcom/xiaomi/passport/servicetoken/AMAuthTokenConverter;->buildAMAuthToken(Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;)Ljava/lang/String;

    move-result-object v0

    iget-object p0, p0, Lcom/xiaomi/passport/servicetoken/ServiceTokenUtilAM;->amUtil:Lcom/xiaomi/passport/servicetoken/IAMUtil;

    invoke-interface {p0, p1, v0}, Lcom/xiaomi/passport/servicetoken/IAMUtil;->invalidateAuthToken(Landroid/content/Context;Ljava/lang/String;)V

    new-instance p0, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;

    iget-object p1, p2, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;->sid:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/xiaomi/passport/servicetoken/ServiceTokenResult$Builder;->build()Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;

    move-result-object p0

    return-object p0
.end method
