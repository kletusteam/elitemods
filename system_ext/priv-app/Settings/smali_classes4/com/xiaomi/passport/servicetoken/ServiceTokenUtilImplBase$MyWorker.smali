.class abstract Lcom/xiaomi/passport/servicetoken/ServiceTokenUtilImplBase$MyWorker;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/passport/servicetoken/ServiceTokenUtilImplBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "MyWorker"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract realWork()Lcom/xiaomi/passport/servicetoken/ServiceTokenResult;
.end method

.method work()Lcom/xiaomi/passport/servicetoken/ServiceTokenFuture;
    .locals 3

    goto/32 :goto_2

    nop

    :goto_0
    new-instance v2, Lcom/xiaomi/passport/servicetoken/ServiceTokenUtilImplBase$MyWorker$1;

    goto/32 :goto_4

    nop

    :goto_1
    invoke-static {}, Lcom/xiaomi/passport/servicetoken/ServiceTokenUtilImplBase;->access$000()Ljava/util/concurrent/Executor;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_2
    new-instance v0, Lcom/xiaomi/passport/servicetoken/ServiceTokenFuture;

    goto/32 :goto_6

    nop

    :goto_3
    invoke-direct {v0, v1}, Lcom/xiaomi/passport/servicetoken/ServiceTokenFuture;-><init>(Lcom/xiaomi/accountsdk/futureservice/ClientFuture$ClientCallback;)V

    goto/32 :goto_1

    nop

    :goto_4
    invoke-direct {v2, p0, v0}, Lcom/xiaomi/passport/servicetoken/ServiceTokenUtilImplBase$MyWorker$1;-><init>(Lcom/xiaomi/passport/servicetoken/ServiceTokenUtilImplBase$MyWorker;Lcom/xiaomi/passport/servicetoken/ServiceTokenFuture;)V

    goto/32 :goto_5

    nop

    :goto_5
    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto/32 :goto_7

    nop

    :goto_6
    const/4 v1, 0x0

    goto/32 :goto_3

    nop

    :goto_7
    return-object v0
.end method
