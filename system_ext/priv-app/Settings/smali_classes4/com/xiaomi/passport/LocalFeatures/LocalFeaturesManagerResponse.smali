.class public Lcom/xiaomi/passport/LocalFeatures/LocalFeaturesManagerResponse;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/xiaomi/passport/LocalFeatures/LocalFeaturesManagerResponse;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mLocalFeatureManagerResponse:Lcom/xiaomi/accounts/ILocalFeatureManagerResponse;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/xiaomi/passport/LocalFeatures/LocalFeaturesManagerResponse$1;

    invoke-direct {v0}, Lcom/xiaomi/passport/LocalFeatures/LocalFeaturesManagerResponse$1;-><init>()V

    sput-object v0, Lcom/xiaomi/passport/LocalFeatures/LocalFeaturesManagerResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object p1

    invoke-static {p1}, Lcom/xiaomi/accounts/ILocalFeatureManagerResponse$Stub;->asInterface(Landroid/os/IBinder;)Lcom/xiaomi/accounts/ILocalFeatureManagerResponse;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/passport/LocalFeatures/LocalFeaturesManagerResponse;->mLocalFeatureManagerResponse:Lcom/xiaomi/accounts/ILocalFeatureManagerResponse;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/passport/LocalFeatures/LocalFeaturesManagerResponse;->mLocalFeatureManagerResponse:Lcom/xiaomi/accounts/ILocalFeatureManagerResponse;

    invoke-interface {p0}, Landroid/os/IInterface;->asBinder()Landroid/os/IBinder;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    return-void
.end method
