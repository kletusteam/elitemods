.class Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebViewClient;
.super Landroid/webkit/WebViewClient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/passport/jsb/PassportJsbWebView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PassportJsbWebViewClient"
.end annotation


# instance fields
.field private mUrlLoadListener:Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadListener;

.field private mUrlStrategies:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/passport/deeplink/IUrlStrategy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebViewClient;->mUrlStrategies:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lcom/xiaomi/passport/jsb/PassportJsbWebView$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebViewClient;->mUrlLoadListener:Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadListener;

    if-eqz p0, :cond_0

    check-cast p1, Lcom/xiaomi/passport/jsb/PassportJsbWebView;

    invoke-interface {p0, p1, p2}, Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadListener;->onLoadMainFrameFinish(Lcom/xiaomi/passport/jsb/PassportJsbWebView;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebViewClient;->mUrlLoadListener:Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadListener;

    if-eqz p0, :cond_0

    check-cast p1, Lcom/xiaomi/passport/jsb/PassportJsbWebView;

    invoke-interface {p0, p1, p2, p3}, Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadListener;->onLoadMainFrameStart(Lcom/xiaomi/passport/jsb/PassportJsbWebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceError;)V
    .locals 0

    iget-object p3, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebViewClient;->mUrlLoadListener:Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadListener;

    if-nez p3, :cond_0

    return-void

    :cond_0
    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->isForMainFrame()Z

    move-result p3

    if-eqz p3, :cond_1

    iget-object p0, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebViewClient;->mUrlLoadListener:Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadListener;

    check-cast p1, Lcom/xiaomi/passport/jsb/PassportJsbWebView;

    invoke-interface {p0, p1, p2}, Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadListener;->onLoadMainFrameError(Lcom/xiaomi/passport/jsb/PassportJsbWebView;Landroid/webkit/WebResourceRequest;)V

    goto :goto_0

    :cond_1
    iget-object p0, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebViewClient;->mUrlLoadListener:Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadListener;

    check-cast p1, Lcom/xiaomi/passport/jsb/PassportJsbWebView;

    invoke-interface {p0, p1, p2}, Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadListener;->onLoadResourceError(Lcom/xiaomi/passport/jsb/PassportJsbWebView;Landroid/webkit/WebResourceRequest;)V

    :goto_0
    return-void
.end method

.method public onReceivedHttpError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceResponse;)V
    .locals 0

    iget-object p3, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebViewClient;->mUrlLoadListener:Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadListener;

    if-nez p3, :cond_0

    return-void

    :cond_0
    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->isForMainFrame()Z

    move-result p3

    if-eqz p3, :cond_1

    iget-object p0, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebViewClient;->mUrlLoadListener:Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadListener;

    check-cast p1, Lcom/xiaomi/passport/jsb/PassportJsbWebView;

    invoke-interface {p0, p1, p2}, Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadListener;->onLoadMainFrameError(Lcom/xiaomi/passport/jsb/PassportJsbWebView;Landroid/webkit/WebResourceRequest;)V

    goto :goto_0

    :cond_1
    iget-object p0, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebViewClient;->mUrlLoadListener:Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadListener;

    check-cast p1, Lcom/xiaomi/passport/jsb/PassportJsbWebView;

    invoke-interface {p0, p1, p2}, Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadListener;->onLoadResourceError(Lcom/xiaomi/passport/jsb/PassportJsbWebView;Landroid/webkit/WebResourceRequest;)V

    :goto_0
    return-void
.end method

.method public release()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebViewClient;->mUrlStrategies:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/passport/deeplink/IUrlStrategy;

    invoke-interface {v1}, Lcom/xiaomi/passport/deeplink/IUrlStrategy;->release()V

    goto :goto_0

    :cond_0
    iget-object p0, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebViewClient;->mUrlStrategies:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public setUrlLoadListener(Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadListener;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebViewClient;->mUrlLoadListener:Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadListener;

    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;)Z
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebViewClient;->mUrlStrategies:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/passport/deeplink/IUrlStrategy;

    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->getUrl()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/xiaomi/passport/deeplink/IUrlStrategy;->shouldIntercept(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;)Z

    move-result p0

    return p0
.end method
