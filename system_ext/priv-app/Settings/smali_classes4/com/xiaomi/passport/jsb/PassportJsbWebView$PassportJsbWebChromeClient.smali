.class Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebChromeClient;
.super Landroid/webkit/WebChromeClient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/passport/jsb/PassportJsbWebView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PassportJsbWebChromeClient"
.end annotation


# instance fields
.field private mUrlLoadListener:Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadListener;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/xiaomi/passport/jsb/PassportJsbWebView$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebChromeClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/webkit/WebView;I)V
    .locals 0

    return-void
.end method

.method public onReceivedTitle(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebChromeClient;->mUrlLoadListener:Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadListener;

    if-eqz p0, :cond_0

    check-cast p1, Lcom/xiaomi/passport/jsb/PassportJsbWebView;

    invoke-interface {p0, p1, p2}, Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadListener;->onReceiveUrlTitle(Lcom/xiaomi/passport/jsb/PassportJsbWebView;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setUrlLoadListener(Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadListener;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebChromeClient;->mUrlLoadListener:Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadListener;

    return-void
.end method
