.class public Lcom/xiaomi/passport/jsb/PassportJsbMethodInvoker;
.super Ljava/lang/Object;


# instance fields
.field private final mHostWebView:Lcom/xiaomi/passport/jsb/PassportJsbWebView;

.field private final mJsbMethods:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/xiaomi/passport/jsb/PassportJsbMethod;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/xiaomi/passport/jsb/PassportJsbWebView;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/xiaomi/passport/jsb/PassportJsbWebView;",
            "Ljava/util/List<",
            "Lcom/xiaomi/passport/jsb/PassportJsbMethod;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/passport/jsb/PassportJsbMethodInvoker;->mHostWebView:Lcom/xiaomi/passport/jsb/PassportJsbWebView;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/passport/jsb/PassportJsbMethodInvoker;->mJsbMethods:Ljava/util/Map;

    new-instance p1, Lcom/xiaomi/passport/jsb/method_impl/PassportJsbMethodIsInDarkMode;

    invoke-direct {p1}, Lcom/xiaomi/passport/jsb/method_impl/PassportJsbMethodIsInDarkMode;-><init>()V

    invoke-virtual {p0, p1}, Lcom/xiaomi/passport/jsb/PassportJsbMethodInvoker;->addJsbMethod(Lcom/xiaomi/passport/jsb/PassportJsbMethod;)V

    new-instance p1, Lcom/xiaomi/passport/jsb/method_impl/PassportJsbMethodIsInTalkBackMode;

    invoke-direct {p1}, Lcom/xiaomi/passport/jsb/method_impl/PassportJsbMethodIsInTalkBackMode;-><init>()V

    invoke-virtual {p0, p1}, Lcom/xiaomi/passport/jsb/PassportJsbMethodInvoker;->addJsbMethod(Lcom/xiaomi/passport/jsb/PassportJsbMethod;)V

    new-instance p1, Lcom/xiaomi/passport/jsb/method_impl/PassportJsbMethodStartPage;

    invoke-direct {p1}, Lcom/xiaomi/passport/jsb/method_impl/PassportJsbMethodStartPage;-><init>()V

    invoke-virtual {p0, p1}, Lcom/xiaomi/passport/jsb/PassportJsbMethodInvoker;->addJsbMethod(Lcom/xiaomi/passport/jsb/PassportJsbMethod;)V

    new-instance p1, Lcom/xiaomi/passport/jsb/method_impl/PassportJsbMethodClosePage;

    invoke-direct {p1}, Lcom/xiaomi/passport/jsb/method_impl/PassportJsbMethodClosePage;-><init>()V

    invoke-virtual {p0, p1}, Lcom/xiaomi/passport/jsb/PassportJsbMethodInvoker;->addJsbMethod(Lcom/xiaomi/passport/jsb/PassportJsbMethod;)V

    new-instance p1, Lcom/xiaomi/passport/jsb/method_impl/PassportJsbMethodShowDialog;

    invoke-direct {p1}, Lcom/xiaomi/passport/jsb/method_impl/PassportJsbMethodShowDialog;-><init>()V

    invoke-virtual {p0, p1}, Lcom/xiaomi/passport/jsb/PassportJsbMethodInvoker;->addJsbMethod(Lcom/xiaomi/passport/jsb/PassportJsbMethod;)V

    new-instance p1, Lcom/xiaomi/passport/jsb/method_impl/PassportJsbMethodShowToast;

    invoke-direct {p1}, Lcom/xiaomi/passport/jsb/method_impl/PassportJsbMethodShowToast;-><init>()V

    invoke-virtual {p0, p1}, Lcom/xiaomi/passport/jsb/PassportJsbMethodInvoker;->addJsbMethod(Lcom/xiaomi/passport/jsb/PassportJsbMethod;)V

    new-instance p1, Lcom/xiaomi/passport/jsb/method_impl/PassportJsbMethodListenInboxSms;

    invoke-direct {p1}, Lcom/xiaomi/passport/jsb/method_impl/PassportJsbMethodListenInboxSms;-><init>()V

    invoke-virtual {p0, p1}, Lcom/xiaomi/passport/jsb/PassportJsbMethodInvoker;->addJsbMethod(Lcom/xiaomi/passport/jsb/PassportJsbMethod;)V

    new-instance p1, Lcom/xiaomi/passport/jsb/method_impl/PassportJsbMethodGetConfig;

    invoke-direct {p1}, Lcom/xiaomi/passport/jsb/method_impl/PassportJsbMethodGetConfig;-><init>()V

    invoke-virtual {p0, p1}, Lcom/xiaomi/passport/jsb/PassportJsbMethodInvoker;->addJsbMethod(Lcom/xiaomi/passport/jsb/PassportJsbMethod;)V

    new-instance p1, Lcom/xiaomi/passport/jsb/method_impl/PassportJsbMethodGetLoadHistory;

    invoke-direct {p1}, Lcom/xiaomi/passport/jsb/method_impl/PassportJsbMethodGetLoadHistory;-><init>()V

    invoke-virtual {p0, p1}, Lcom/xiaomi/passport/jsb/PassportJsbMethodInvoker;->addJsbMethod(Lcom/xiaomi/passport/jsb/PassportJsbMethod;)V

    new-instance p1, Lcom/xiaomi/passport/jsb/method_impl/PassportJsbMethodGoBack;

    invoke-direct {p1}, Lcom/xiaomi/passport/jsb/method_impl/PassportJsbMethodGoBack;-><init>()V

    invoke-virtual {p0, p1}, Lcom/xiaomi/passport/jsb/PassportJsbMethodInvoker;->addJsbMethod(Lcom/xiaomi/passport/jsb/PassportJsbMethod;)V

    new-instance p1, Lcom/xiaomi/passport/jsb/method_impl/PassportJsbMethodGoForward;

    invoke-direct {p1}, Lcom/xiaomi/passport/jsb/method_impl/PassportJsbMethodGoForward;-><init>()V

    invoke-virtual {p0, p1}, Lcom/xiaomi/passport/jsb/PassportJsbMethodInvoker;->addJsbMethod(Lcom/xiaomi/passport/jsb/PassportJsbMethod;)V

    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/xiaomi/passport/jsb/PassportJsbMethod;

    invoke-virtual {p0, p2}, Lcom/xiaomi/passport/jsb/PassportJsbMethodInvoker;->addJsbMethod(Lcom/xiaomi/passport/jsb/PassportJsbMethod;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public addJsbMethod(Lcom/xiaomi/passport/jsb/PassportJsbMethod;)V
    .locals 1

    iget-object p0, p0, Lcom/xiaomi/passport/jsb/PassportJsbMethodInvoker;->mJsbMethods:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/xiaomi/passport/jsb/PassportJsbMethod;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public release()V
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/passport/jsb/PassportJsbMethodInvoker;->mJsbMethods:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/xiaomi/passport/jsb/PassportJsbMethod;

    iget-object v2, p0, Lcom/xiaomi/passport/jsb/PassportJsbMethodInvoker;->mHostWebView:Lcom/xiaomi/passport/jsb/PassportJsbWebView;

    invoke-virtual {v1, v2}, Lcom/xiaomi/passport/jsb/PassportJsbMethod;->release(Lcom/xiaomi/passport/jsb/PassportJsbWebView;)V

    goto :goto_0

    :cond_0
    iget-object p0, p0, Lcom/xiaomi/passport/jsb/PassportJsbMethodInvoker;->mJsbMethods:Ljava/util/Map;

    invoke-interface {p0}, Ljava/util/Map;->clear()V

    return-void
.end method
