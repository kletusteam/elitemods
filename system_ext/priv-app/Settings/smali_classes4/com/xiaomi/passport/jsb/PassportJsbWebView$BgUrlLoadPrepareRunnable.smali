.class Lcom/xiaomi/passport/jsb/PassportJsbWebView$BgUrlLoadPrepareRunnable;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/xiaomi/passport/task/BgTask$DoInBackgroundRunnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/passport/jsb/PassportJsbWebView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BgUrlLoadPrepareRunnable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/xiaomi/passport/task/BgTask$DoInBackgroundRunnable<",
        "Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadPrepareResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAppContext:Landroid/content/Context;

.field private final mUrlLoadPrepareRunnables:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadPrepareRunnable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadPrepareRunnable;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$BgUrlLoadPrepareRunnable;->mAppContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$BgUrlLoadPrepareRunnable;->mUrlLoadPrepareRunnables:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public run()Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadPrepareResult;
    .locals 5

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iget-object v2, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$BgUrlLoadPrepareRunnable;->mUrlLoadPrepareRunnables:Ljava/util/List;

    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    :cond_0
    iget-object v2, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$BgUrlLoadPrepareRunnable;->mUrlLoadPrepareRunnables:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadPrepareRunnable;

    iget-object v4, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$BgUrlLoadPrepareRunnable;->mAppContext:Landroid/content/Context;

    invoke-interface {v3, v4, v0, v1}, Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadPrepareRunnable;->run(Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;)V

    goto :goto_0

    :cond_1
    new-instance p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadPrepareResult;

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadPrepareResult;-><init>(Ljava/util/Map;Ljava/util/Map;)V

    return-object p0

    :cond_2
    :goto_1
    new-instance p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadPrepareResult;

    invoke-direct {p0, v0, v1}, Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadPrepareResult;-><init>(Ljava/util/Map;Ljava/util/Map;)V

    return-object p0
.end method

.method public bridge synthetic run()Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0}, Lcom/xiaomi/passport/jsb/PassportJsbWebView$BgUrlLoadPrepareRunnable;->run()Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadPrepareResult;

    move-result-object p0

    return-object p0
.end method
