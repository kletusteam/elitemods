.class public Lcom/xiaomi/passport/jsb/PassportJsbWebView;
.super Landroid/webkit/WebView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/passport/jsb/PassportJsbWebView$BgUrlLoadPrepareRunnable;,
        Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadPrepareResult;,
        Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadPrepareRunnable;,
        Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadListener;,
        Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebChromeClient;,
        Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebViewClient;
    }
.end annotation


# instance fields
.field private mJsbMethodInvoker:Lcom/xiaomi/passport/jsb/PassportJsbMethodInvoker;

.field private mUrlLoadPrepareTask:Lcom/xiaomi/passport/task/BgTask;

.field private mViewPostRunnables:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private mWebChromeClient:Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebChromeClient;

.field private mWebViewClient:Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebViewClient;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/xiaomi/passport/jsb/PassportJsbWebView;-><init>(Landroid/content/Context;Ljava/util/List;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/xiaomi/passport/jsb/PassportJsbMethod;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-static {v0}, Landroid/webkit/WebView;->setWebContentsDebuggingEnabled(Z)V

    :cond_0
    new-instance v0, Lcom/xiaomi/passport/jsb/PassportJsbMethodInvoker;

    invoke-direct {v0, p0, p2}, Lcom/xiaomi/passport/jsb/PassportJsbMethodInvoker;-><init>(Lcom/xiaomi/passport/jsb/PassportJsbWebView;Ljava/util/List;)V

    iput-object v0, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView;->mJsbMethodInvoker:Lcom/xiaomi/passport/jsb/PassportJsbMethodInvoker;

    const-string p2, "PASSPORT_JSB_METHOD_INVOKER"

    invoke-virtual {p0, v0, p2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView;->mViewPostRunnables:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/xiaomi/passport/jsb/PassportJsbWebView;->initSettings(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/passport/jsb/PassportJsbWebView;->initStyle(Landroid/content/Context;)V

    new-instance p1, Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebViewClient;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebViewClient;-><init>(Lcom/xiaomi/passport/jsb/PassportJsbWebView$1;)V

    iput-object p1, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView;->mWebViewClient:Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebViewClient;

    invoke-super {p0, p1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    new-instance p1, Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebChromeClient;

    invoke-direct {p1, p2}, Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebChromeClient;-><init>(Lcom/xiaomi/passport/jsb/PassportJsbWebView$1;)V

    iput-object p1, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView;->mWebChromeClient:Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebChromeClient;

    invoke-super {p0, p1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    return-void
.end method

.method static synthetic access$201(Lcom/xiaomi/passport/jsb/PassportJsbWebView;Ljava/lang/String;Ljava/util/Map;)V
    .locals 0

    invoke-super {p0, p1, p2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method private initSettings(Landroid/content/Context;)V
    .locals 4

    invoke-virtual {p0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setAppCachePath(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setDatabaseEnabled(Z)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setAllowContentAccess(Z)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0, p1}, Lcom/xiaomi/accountsdk/account/XMPassportUserAgent;->getWebViewUserAgent(Landroid/webkit/WebView;Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, " WebViewType/PassportJSBWebView"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    return-void
.end method

.method private initStyle(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/webkit/WebView;->setScrollBarSize(I)V

    invoke-virtual {p0}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/xiaomi/accountsdk/utils/UIUtils;->isSystemNightMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/webkit/WebView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x106000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/webkit/WebView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x106000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    invoke-static {p1, p0}, Lcom/xiaomi/accountsdk/utils/UIUtils;->adaptForceDarkInApi29(Landroid/content/Context;Landroid/webkit/WebView;)V

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView;->mWebViewClient:Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebViewClient;

    invoke-virtual {v0}, Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebViewClient;->release()V

    iget-object v0, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView;->mJsbMethodInvoker:Lcom/xiaomi/passport/jsb/PassportJsbMethodInvoker;

    invoke-virtual {v0}, Lcom/xiaomi/passport/jsb/PassportJsbMethodInvoker;->release()V

    iget-object v0, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView;->mViewPostRunnables:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Landroid/webkit/WebView;->removeCallbacks(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView;->mViewPostRunnables:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView;->mUrlLoadPrepareTask:Lcom/xiaomi/passport/task/BgTask;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/xiaomi/passport/task/BgTask;->cancelAndRelease()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView;->mUrlLoadPrepareTask:Lcom/xiaomi/passport/task/BgTask;

    :cond_1
    invoke-super {p0}, Landroid/webkit/WebView;->destroy()V

    return-void
.end method

.method public loadUrl(Ljava/lang/String;)V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/passport/jsb/PassportJsbWebView;->loadUrl(Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method public loadUrl(Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, p1, p2, v0}, Lcom/xiaomi/passport/jsb/PassportJsbWebView;->loadUrl(Ljava/lang/String;Ljava/util/Map;Ljava/util/List;)V

    return-void
.end method

.method public loadUrl(Ljava/lang/String;Ljava/util/Map;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadPrepareRunnable;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView;->mUrlLoadPrepareTask:Lcom/xiaomi/passport/task/BgTask;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/xiaomi/passport/task/BgTask;->cancelAndRelease()V

    :cond_1
    new-instance v0, Lcom/xiaomi/passport/task/BgTask;

    new-instance v1, Lcom/xiaomi/passport/jsb/PassportJsbWebView$BgUrlLoadPrepareRunnable;

    invoke-virtual {p0}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p3}, Lcom/xiaomi/passport/jsb/PassportJsbWebView$BgUrlLoadPrepareRunnable;-><init>(Landroid/content/Context;Ljava/util/List;)V

    new-instance p3, Lcom/xiaomi/passport/jsb/PassportJsbWebView$1;

    invoke-direct {p3, p0, p2, p1}, Lcom/xiaomi/passport/jsb/PassportJsbWebView$1;-><init>(Lcom/xiaomi/passport/jsb/PassportJsbWebView;Ljava/util/Map;Ljava/lang/String;)V

    invoke-direct {v0, v1, p3}, Lcom/xiaomi/passport/task/BgTask;-><init>(Lcom/xiaomi/passport/task/BgTask$DoInBackgroundRunnable;Lcom/xiaomi/passport/task/BgTask$OnPostExecuteRunnable;)V

    iput-object v0, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView;->mUrlLoadPrepareTask:Lcom/xiaomi/passport/task/BgTask;

    invoke-virtual {v0}, Lcom/xiaomi/passport/task/BgTask;->execute()V

    :goto_0
    return-void
.end method

.method public post(Ljava/lang/Runnable;)Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView;->mViewPostRunnables:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-super {p0, p1}, Landroid/webkit/WebView;->post(Ljava/lang/Runnable;)Z

    move-result p0

    return p0
.end method

.method public postDelayed(Ljava/lang/Runnable;J)Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView;->mViewPostRunnables:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebView;->postDelayed(Ljava/lang/Runnable;J)Z

    move-result p0

    return p0
.end method

.method public setUrlLoadListener(Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadListener;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView;->mWebViewClient:Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebViewClient;

    invoke-virtual {v0, p1}, Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebViewClient;->setUrlLoadListener(Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadListener;)V

    iget-object p0, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView;->mWebChromeClient:Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebChromeClient;

    invoke-virtual {p0, p1}, Lcom/xiaomi/passport/jsb/PassportJsbWebView$PassportJsbWebChromeClient;->setUrlLoadListener(Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadListener;)V

    return-void
.end method
