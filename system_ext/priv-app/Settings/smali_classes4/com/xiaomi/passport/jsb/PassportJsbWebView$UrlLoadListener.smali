.class public interface abstract Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadListener;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/passport/jsb/PassportJsbWebView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "UrlLoadListener"
.end annotation


# virtual methods
.method public abstract onLoadMainFrameError(Lcom/xiaomi/passport/jsb/PassportJsbWebView;Landroid/webkit/WebResourceRequest;)V
.end method

.method public abstract onLoadMainFrameFinish(Lcom/xiaomi/passport/jsb/PassportJsbWebView;Ljava/lang/String;)V
.end method

.method public abstract onLoadMainFrameStart(Lcom/xiaomi/passport/jsb/PassportJsbWebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
.end method

.method public abstract onLoadResourceError(Lcom/xiaomi/passport/jsb/PassportJsbWebView;Landroid/webkit/WebResourceRequest;)V
.end method

.method public abstract onReceiveUrlTitle(Lcom/xiaomi/passport/jsb/PassportJsbWebView;Ljava/lang/String;)V
.end method
