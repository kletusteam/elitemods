.class Lcom/xiaomi/passport/jsb/PassportJsbWebView$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/xiaomi/passport/task/BgTask$OnPostExecuteRunnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/xiaomi/passport/jsb/PassportJsbWebView;->loadUrl(Ljava/lang/String;Ljava/util/Map;Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/xiaomi/passport/task/BgTask$OnPostExecuteRunnable<",
        "Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadPrepareResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/xiaomi/passport/jsb/PassportJsbWebView;

.field final synthetic val$additionalHttpHeaders:Ljava/util/Map;

.field final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/xiaomi/passport/jsb/PassportJsbWebView;Ljava/util/Map;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$1;->this$0:Lcom/xiaomi/passport/jsb/PassportJsbWebView;

    iput-object p2, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$1;->val$additionalHttpHeaders:Ljava/util/Map;

    iput-object p3, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$1;->val$url:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadPrepareResult;)V
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$1;->val$additionalHttpHeaders:Ljava/util/Map;

    iget-object v1, p1, Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadPrepareResult;->headers:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    iget-object v0, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$1;->this$0:Lcom/xiaomi/passport/jsb/PassportJsbWebView;

    iget-object v1, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$1;->val$url:Ljava/lang/String;

    iget-object p1, p1, Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadPrepareResult;->urlParams:Ljava/util/Map;

    invoke-static {v1, p1}, Lcom/xiaomi/passport/utils/HttpUrl;->make(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object p1

    iget-object p0, p0, Lcom/xiaomi/passport/jsb/PassportJsbWebView$1;->val$additionalHttpHeaders:Ljava/util/Map;

    invoke-static {v0, p1, p0}, Lcom/xiaomi/passport/jsb/PassportJsbWebView;->access$201(Lcom/xiaomi/passport/jsb/PassportJsbWebView;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method public bridge synthetic run(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadPrepareResult;

    invoke-virtual {p0, p1}, Lcom/xiaomi/passport/jsb/PassportJsbWebView$1;->run(Lcom/xiaomi/passport/jsb/PassportJsbWebView$UrlLoadPrepareResult;)V

    return-void
.end method
