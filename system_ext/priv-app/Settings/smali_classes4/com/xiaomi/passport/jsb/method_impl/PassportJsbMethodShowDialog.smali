.class public Lcom/xiaomi/passport/jsb/method_impl/PassportJsbMethodShowDialog;
.super Lcom/xiaomi/passport/jsb/PassportJsbMethod;


# instance fields
.field private mDialog:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/passport/jsb/PassportJsbMethod;-><init>()V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 0

    const-string p0, "showDialog"

    return-object p0
.end method

.method public release(Lcom/xiaomi/passport/jsb/PassportJsbWebView;)V
    .locals 0

    iget-object p1, p0, Lcom/xiaomi/passport/jsb/method_impl/PassportJsbMethodShowDialog;->mDialog:Landroid/app/AlertDialog;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/AlertDialog;->dismiss()V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/xiaomi/passport/jsb/method_impl/PassportJsbMethodShowDialog;->mDialog:Landroid/app/AlertDialog;

    :cond_0
    return-void
.end method
