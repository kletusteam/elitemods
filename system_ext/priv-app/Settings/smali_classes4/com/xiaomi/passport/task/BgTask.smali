.class public Lcom/xiaomi/passport/task/BgTask;
.super Landroid/os/AsyncTask;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/passport/task/BgTask$OnPostExecuteRunnable;,
        Lcom/xiaomi/passport/task/BgTask$DoInBackgroundRunnable;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final mDoInBackgroundRunnableRef:Lcom/xiaomi/accountsdk/account/utils/ReferenceHolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/xiaomi/accountsdk/account/utils/ReferenceHolder<",
            "Lcom/xiaomi/passport/task/BgTask$DoInBackgroundRunnable<",
            "TT;>;>;"
        }
    .end annotation
.end field

.field private final mOnPostExecuteRunnableRef:Lcom/xiaomi/accountsdk/account/utils/ReferenceHolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/xiaomi/accountsdk/account/utils/ReferenceHolder<",
            "Lcom/xiaomi/passport/task/BgTask$OnPostExecuteRunnable<",
            "TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/xiaomi/passport/task/BgTask$DoInBackgroundRunnable;Lcom/xiaomi/passport/task/BgTask$OnPostExecuteRunnable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/xiaomi/passport/task/BgTask$DoInBackgroundRunnable<",
            "TT;>;",
            "Lcom/xiaomi/passport/task/BgTask$OnPostExecuteRunnable<",
            "TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Lcom/xiaomi/accountsdk/account/utils/ReferenceHolder;

    invoke-direct {v0, p1}, Lcom/xiaomi/accountsdk/account/utils/ReferenceHolder;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/xiaomi/passport/task/BgTask;->mDoInBackgroundRunnableRef:Lcom/xiaomi/accountsdk/account/utils/ReferenceHolder;

    new-instance p1, Lcom/xiaomi/accountsdk/account/utils/ReferenceHolder;

    invoke-direct {p1, p2}, Lcom/xiaomi/accountsdk/account/utils/ReferenceHolder;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/xiaomi/passport/task/BgTask;->mOnPostExecuteRunnableRef:Lcom/xiaomi/accountsdk/account/utils/ReferenceHolder;

    return-void
.end method


# virtual methods
.method public cancelAndRelease()V
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/os/AsyncTask;->cancel(Z)Z

    iget-object v0, p0, Lcom/xiaomi/passport/task/BgTask;->mDoInBackgroundRunnableRef:Lcom/xiaomi/accountsdk/account/utils/ReferenceHolder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/xiaomi/accountsdk/account/utils/ReferenceHolder;->set(Ljava/lang/Object;)V

    iget-object p0, p0, Lcom/xiaomi/passport/task/BgTask;->mOnPostExecuteRunnableRef:Lcom/xiaomi/accountsdk/account/utils/ReferenceHolder;

    invoke-virtual {p0, v1}, Lcom/xiaomi/accountsdk/account/utils/ReferenceHolder;->set(Ljava/lang/Object;)V

    return-void
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/xiaomi/passport/task/BgTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")TT;"
        }
    .end annotation

    iget-object p0, p0, Lcom/xiaomi/passport/task/BgTask;->mDoInBackgroundRunnableRef:Lcom/xiaomi/accountsdk/account/utils/ReferenceHolder;

    invoke-virtual {p0}, Lcom/xiaomi/accountsdk/account/utils/ReferenceHolder;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/xiaomi/passport/task/BgTask$DoInBackgroundRunnable;

    if-eqz p0, :cond_0

    invoke-interface {p0}, Lcom/xiaomi/passport/task/BgTask$DoInBackgroundRunnable;->run()Ljava/lang/Object;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public execute()V
    .locals 2

    invoke-static {}, Lcom/xiaomi/passport/utils/XiaomiPassportExecutor;->getSingleton()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {p0, v0, v1}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object p0, p0, Lcom/xiaomi/passport/task/BgTask;->mOnPostExecuteRunnableRef:Lcom/xiaomi/accountsdk/account/utils/ReferenceHolder;

    invoke-virtual {p0}, Lcom/xiaomi/accountsdk/account/utils/ReferenceHolder;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/xiaomi/passport/task/BgTask$OnPostExecuteRunnable;

    if-eqz p0, :cond_0

    invoke-interface {p0, p1}, Lcom/xiaomi/passport/task/BgTask$OnPostExecuteRunnable;->run(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
