.class public Lcom/xiaomi/passport/uicontroller/NotificationWebView;
.super Lcom/xiaomi/passport/uicontroller/PassportBaseWebView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/passport/uicontroller/NotificationWebView$ExternalParams;
    }
.end annotation


# instance fields
.field private final needRemoveAllCookie:Z

.field private final serverTimeAlignedListener:Lcom/xiaomi/accountsdk/utils/ServerTimeUtil$ServerTimeAlignedListener;


# direct methods
.method public static putExtraForNotificationWebView(Landroid/content/Intent;Lcom/xiaomi/passport/uicontroller/NotificationWebView$ExternalParams;)V
    .locals 2

    iget-object v0, p1, Lcom/xiaomi/passport/uicontroller/NotificationWebView$ExternalParams;->notificationUrl:Ljava/lang/String;

    const-string v1, "notification_url"

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-boolean p1, p1, Lcom/xiaomi/passport/uicontroller/NotificationWebView$ExternalParams;->needRemoveAllCookies:Z

    const-string v0, "need_remove_all_cookies"

    invoke-virtual {p0, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/webkit/WebView;->onAttachedToWindow()V

    iget-object p0, p0, Lcom/xiaomi/passport/uicontroller/NotificationWebView;->serverTimeAlignedListener:Lcom/xiaomi/accountsdk/utils/ServerTimeUtil$ServerTimeAlignedListener;

    invoke-static {p0}, Lcom/xiaomi/accountsdk/utils/ServerTimeUtil;->addServerTimeChangedListener(Lcom/xiaomi/accountsdk/utils/ServerTimeUtil$ServerTimeAlignedListener;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/passport/uicontroller/NotificationWebView;->serverTimeAlignedListener:Lcom/xiaomi/accountsdk/utils/ServerTimeUtil$ServerTimeAlignedListener;

    invoke-static {v0}, Lcom/xiaomi/accountsdk/utils/ServerTimeUtil;->removeServerTimeChangedListener(Lcom/xiaomi/accountsdk/utils/ServerTimeUtil$ServerTimeAlignedListener;)V

    iget-boolean v0, p0, Lcom/xiaomi/passport/uicontroller/NotificationWebView;->needRemoveAllCookie:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    :cond_0
    invoke-super {p0}, Landroid/webkit/WebView;->onDetachedFromWindow()V

    return-void
.end method
