.class public Lcom/xiaomi/passport/deeplink/AccountHybridViewClient;
.super Landroid/webkit/WebViewClient;


# instance fields
.field private mUrlStrategy:Lcom/xiaomi/passport/deeplink/IUrlStrategy;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    new-instance v0, Lcom/xiaomi/passport/deeplink/DeeplinkUrlStrategy;

    invoke-direct {v0}, Lcom/xiaomi/passport/deeplink/DeeplinkUrlStrategy;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/passport/deeplink/AccountHybridViewClient;->mUrlStrategy:Lcom/xiaomi/passport/deeplink/IUrlStrategy;

    return-void
.end method


# virtual methods
.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;)Z
    .locals 0

    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;)Z

    move-result p0

    return p0
.end method
