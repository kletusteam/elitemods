.class public Lcom/xiaomi/passport/widget/PassportHybridView$AccountWebViewClient;
.super Lcom/xiaomi/passport/deeplink/AccountHybridViewClient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/passport/widget/PassportHybridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AccountWebViewClient"
.end annotation


# instance fields
.field private final webView:Lcom/xiaomi/passport/widget/PassportHybridView;


# direct methods
.method public constructor <init>(Lcom/xiaomi/passport/widget/PassportHybridView;)V
    .locals 1

    invoke-direct {p0}, Lcom/xiaomi/passport/deeplink/AccountHybridViewClient;-><init>()V

    const-string v0, "PassportWebView should not be null"

    invoke-static {p1, v0}, Lcom/xiaomi/passport/uicontroller/PreConditions;->checkArgumentNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/xiaomi/passport/widget/PassportHybridView$AccountWebViewClient;->webView:Lcom/xiaomi/passport/widget/PassportHybridView;

    return-void
.end method


# virtual methods
.method public onPageError(Landroid/webkit/WebView;)V
    .locals 0

    iget-object p0, p0, Lcom/xiaomi/passport/widget/PassportHybridView$AccountWebViewClient;->webView:Lcom/xiaomi/passport/widget/PassportHybridView;

    invoke-virtual {p0, p1}, Lcom/xiaomi/passport/widget/PassportHybridView;->onPageError(Landroid/webkit/WebView;)V

    return-void
.end method

.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/passport/widget/PassportHybridView$AccountWebViewClient;->webView:Lcom/xiaomi/passport/widget/PassportHybridView;

    invoke-virtual {v0, p1, p2}, Lcom/xiaomi/passport/widget/PassportHybridView;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/passport/widget/PassportHybridView$AccountWebViewClient;->webView:Lcom/xiaomi/passport/widget/PassportHybridView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/xiaomi/passport/widget/PassportHybridView;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceError;)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceError;)V

    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->isForMainFrame()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-virtual {p0, p1}, Lcom/xiaomi/passport/widget/PassportHybridView$AccountWebViewClient;->onPageError(Landroid/webkit/WebView;)V

    :cond_0
    return-void
.end method

.method public onReceivedHttpError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceResponse;)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onReceivedHttpError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceResponse;)V

    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->isForMainFrame()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-virtual {p0, p1}, Lcom/xiaomi/passport/widget/PassportHybridView$AccountWebViewClient;->onPageError(Landroid/webkit/WebView;)V

    :cond_0
    return-void
.end method

.method public onReceivedLoginRequest(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/passport/widget/PassportHybridView$AccountWebViewClient;->webView:Lcom/xiaomi/passport/widget/PassportHybridView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/xiaomi/passport/widget/PassportHybridView;->onReceivedLoginRequest(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedLoginRequest(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V

    invoke-virtual {p0, p1}, Lcom/xiaomi/passport/widget/PassportHybridView$AccountWebViewClient;->onPageError(Landroid/webkit/WebView;)V

    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/passport/widget/PassportHybridView$AccountWebViewClient;->webView:Lcom/xiaomi/passport/widget/PassportHybridView;

    invoke-virtual {v0, p1, p2}, Lcom/xiaomi/passport/widget/PassportHybridView;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    iget-object p0, p0, Lcom/xiaomi/passport/widget/PassportHybridView$AccountWebViewClient;->webView:Lcom/xiaomi/passport/widget/PassportHybridView;

    invoke-static {p0, p1, p2}, Lcom/xiaomi/passport/widget/PassportHybridView;->access$000(Lcom/xiaomi/passport/widget/PassportHybridView;Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result p0

    return p0
.end method
