.class public Lcom/xiaomi/passport/widget/PassportHybridView;
.super Lcom/xiaomi/passport/uicontroller/PassportBaseWebView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/passport/widget/PassportHybridView$AccountWebViewClient;
    }
.end annotation


# instance fields
.field private mUrlStrategy:Lcom/xiaomi/passport/deeplink/IUrlStrategy;

.field private passportUrl:Ljava/lang/String;

.field private final serverTimeAlignedListener:Lcom/xiaomi/accountsdk/utils/ServerTimeUtil$ServerTimeAlignedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/xiaomi/passport/uicontroller/PassportBaseWebView;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/xiaomi/passport/deeplink/DeeplinkUrlStrategy;

    invoke-direct {v0}, Lcom/xiaomi/passport/deeplink/DeeplinkUrlStrategy;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/passport/widget/PassportHybridView;->mUrlStrategy:Lcom/xiaomi/passport/deeplink/IUrlStrategy;

    new-instance v0, Lcom/xiaomi/accountsdk/utils/WebViewFidNonceUtil$ServerTimeAlignedListenerImpl;

    invoke-direct {v0, p0}, Lcom/xiaomi/accountsdk/utils/WebViewFidNonceUtil$ServerTimeAlignedListenerImpl;-><init>(Landroid/webkit/WebView;)V

    iput-object v0, p0, Lcom/xiaomi/passport/widget/PassportHybridView;->serverTimeAlignedListener:Lcom/xiaomi/accountsdk/utils/ServerTimeUtil$ServerTimeAlignedListener;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object p0

    iget p0, p0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 p0, p0, 0x2

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    invoke-static {p0}, Landroid/webkit/WebView;->setWebContentsDebuggingEnabled(Z)V

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/xiaomi/passport/widget/PassportHybridView;Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/xiaomi/passport/widget/PassportHybridView;->loadPassportUrl(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method private addWebCookies(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/xiaomi/accountsdk/utils/WebViewDeviceIdUtil;

    invoke-direct {v0}, Lcom/xiaomi/accountsdk/utils/WebViewDeviceIdUtil;-><init>()V

    invoke-virtual {v0, p0}, Lcom/xiaomi/accountsdk/utils/WebViewDeviceIdUtil;->setupDeviceIdForAccountWeb(Landroid/webkit/WebView;)V

    new-instance v0, Lcom/xiaomi/accountsdk/utils/WebViewFidNonceUtil;

    invoke-direct {v0}, Lcom/xiaomi/accountsdk/utils/WebViewFidNonceUtil;-><init>()V

    invoke-virtual {v0, p0}, Lcom/xiaomi/accountsdk/utils/WebViewFidNonceUtil;->setupFidNonceForAccountWeb(Landroid/webkit/WebView;)V

    new-instance v0, Lcom/xiaomi/accountsdk/utils/WebViewUserSpaceIdUtil;

    invoke-direct {v0}, Lcom/xiaomi/accountsdk/utils/WebViewUserSpaceIdUtil;-><init>()V

    invoke-virtual {v0, p0}, Lcom/xiaomi/accountsdk/utils/WebViewUserSpaceIdUtil;->setupUserSpaceIdForAccountWeb(Landroid/webkit/WebView;)V

    new-instance v0, Lcom/xiaomi/accountsdk/utils/WebViewNativeUserAgentUtil;

    invoke-direct {v0}, Lcom/xiaomi/accountsdk/utils/WebViewNativeUserAgentUtil;-><init>()V

    invoke-virtual {v0, p0}, Lcom/xiaomi/accountsdk/utils/WebViewNativeUserAgentUtil;->setupUserAgentForAccountWeb(Landroid/webkit/WebView;)V

    new-instance v0, Lcom/xiaomi/accountsdk/utils/WebViewCookieUtil;

    invoke-direct {v0}, Lcom/xiaomi/accountsdk/utils/WebViewCookieUtil;-><init>()V

    invoke-virtual {v0, p0, p1}, Lcom/xiaomi/accountsdk/utils/WebViewCookieUtil;->setupCookiesForAccountWeb(Landroid/webkit/WebView;Ljava/util/Map;)V

    return-void
.end method

.method private loadPassportUrl(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 4

    iget-object v0, p0, Lcom/xiaomi/passport/widget/PassportHybridView;->passportUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/passport/widget/PassportHybridView;->passportUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/webkit/CookieManager;->getCookie(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "passInfo"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "need-relogin"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/xiaomi/passport/widget/PassportHybridView;->onNeedReLogin()Z

    move-result v1

    if-eqz v1, :cond_0

    return v2

    :cond_0
    const-string v1, "login-end"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v0}, Lcom/xiaomi/accountsdk/utils/XMPassportUtil;->extractPasstokenFromNotificationLoginEndCookie(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/xiaomi/accountsdk/utils/XMPassportUtil;->extractUserIdFromNotificationLoginEndCookie(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, v1}, Lcom/xiaomi/passport/widget/PassportHybridView;->onLoginEnd(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    return v2

    :cond_1
    const-string v1, "auth-end"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0, p2}, Lcom/xiaomi/passport/widget/PassportHybridView;->onAuthEnd(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    return v2

    :cond_2
    const-string v1, "bindph-end"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/xiaomi/passport/widget/PassportHybridView;->onBindPHEnd()Z

    move-result v0

    if-eqz v0, :cond_3

    return v2

    :cond_3
    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/passport/widget/PassportHybridView;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method private setWebSettings()V
    .locals 4

    invoke-virtual {p0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object p0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    invoke-virtual {p0, v0}, Landroid/webkit/WebSettings;->setDatabaseEnabled(Z)V

    invoke-virtual {p0, v0}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    invoke-virtual {p0, v0}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/webkit/WebSettings;->setAllowContentAccess(Z)V

    invoke-virtual {p0, v1}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    invoke-virtual {p0}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v1

    invoke-static {}, Lcom/xiaomi/accountsdk/utils/VersionUtils;->getVersion()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    const-string v0, "%s PassportSDK/PassportHybridView/%s XiaoMi/HybridView/"

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public extraAccountWebCookies()Ljava/util/Map;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 p0, 0x0

    return-object p0
.end method

.method public loadUrl(Ljava/lang/String;)V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/passport/widget/PassportHybridView;->loadUrl(Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method public loadUrl(Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/xiaomi/passport/widget/PassportHybridView;->passportUrl:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/xiaomi/passport/widget/PassportHybridView;->needRemoveAllCookies()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    :cond_0
    invoke-direct {p0}, Lcom/xiaomi/passport/widget/PassportHybridView;->setWebSettings()V

    invoke-virtual {p0}, Lcom/xiaomi/passport/widget/PassportHybridView;->extraAccountWebCookies()Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/xiaomi/passport/widget/PassportHybridView;->addWebCookies(Ljava/util/Map;)V

    new-instance v0, Lcom/xiaomi/passport/widget/PassportHybridView$AccountWebViewClient;

    invoke-direct {v0, p0}, Lcom/xiaomi/passport/widget/PassportHybridView$AccountWebViewClient;-><init>(Lcom/xiaomi/passport/widget/PassportHybridView;)V

    invoke-virtual {p0, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    invoke-super {p0, p1, p2}, Lcom/xiaomi/passport/uicontroller/PassportBaseWebView;->loadUrl(Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method public needRemoveAllCookies()Z
    .locals 0

    const/4 p0, 0x1

    return p0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/webkit/WebView;->onAttachedToWindow()V

    iget-object p0, p0, Lcom/xiaomi/passport/widget/PassportHybridView;->serverTimeAlignedListener:Lcom/xiaomi/accountsdk/utils/ServerTimeUtil$ServerTimeAlignedListener;

    invoke-static {p0}, Lcom/xiaomi/accountsdk/utils/ServerTimeUtil;->addServerTimeChangedListener(Lcom/xiaomi/accountsdk/utils/ServerTimeUtil$ServerTimeAlignedListener;)V

    return-void
.end method

.method public onAuthEnd(Ljava/lang/String;)Z
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

.method public onBindPHEnd()Z
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    iget-object v0, p0, Lcom/xiaomi/passport/widget/PassportHybridView;->serverTimeAlignedListener:Lcom/xiaomi/accountsdk/utils/ServerTimeUtil$ServerTimeAlignedListener;

    invoke-static {v0}, Lcom/xiaomi/accountsdk/utils/ServerTimeUtil;->removeServerTimeChangedListener(Lcom/xiaomi/accountsdk/utils/ServerTimeUtil$ServerTimeAlignedListener;)V

    invoke-super {p0}, Landroid/webkit/WebView;->onDetachedFromWindow()V

    return-void
.end method

.method public onLoginEnd(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

.method public onNeedReLogin()Z
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

.method public onPageError(Landroid/webkit/WebView;)V
    .locals 0

    return-void
.end method

.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)Z
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

.method public onReceivedLoginRequest(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

.method public restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;
    .locals 1

    const-string v0, "bundle_key_passport_url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/xiaomi/passport/widget/PassportHybridView;->passportUrl:Ljava/lang/String;

    new-instance v0, Lcom/xiaomi/passport/widget/PassportHybridView$AccountWebViewClient;

    invoke-direct {v0, p0}, Lcom/xiaomi/passport/widget/PassportHybridView$AccountWebViewClient;-><init>(Lcom/xiaomi/passport/widget/PassportHybridView;)V

    invoke-virtual {p0, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    invoke-direct {p0}, Lcom/xiaomi/passport/widget/PassportHybridView;->setWebSettings()V

    invoke-virtual {p0}, Lcom/xiaomi/passport/widget/PassportHybridView;->extraAccountWebCookies()Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/xiaomi/passport/widget/PassportHybridView;->addWebCookies(Ljava/util/Map;)V

    invoke-super {p0, p1}, Landroid/webkit/WebView;->restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    move-result-object p0

    return-object p0
.end method

.method public saveState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;
    .locals 2

    iget-object v0, p0, Lcom/xiaomi/passport/widget/PassportHybridView;->passportUrl:Ljava/lang/String;

    const-string v1, "bundle_key_passport_url"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/webkit/WebView;->saveState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    move-result-object p0

    return-object p0
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 0

    iget-object p1, p0, Lcom/xiaomi/passport/widget/PassportHybridView;->mUrlStrategy:Lcom/xiaomi/passport/deeplink/IUrlStrategy;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-interface {p1, p0, p2}, Lcom/xiaomi/passport/deeplink/IUrlStrategy;->shouldIntercept(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method
