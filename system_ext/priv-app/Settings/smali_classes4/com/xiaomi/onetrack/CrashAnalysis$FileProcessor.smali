.class Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/onetrack/CrashAnalysis;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FileProcessor"
.end annotation


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field final b:Ljava/lang/String;

.field final c:Ljava/lang/String;

.field final synthetic d:Lcom/xiaomi/onetrack/CrashAnalysis;


# direct methods
.method constructor <init>(Lcom/xiaomi/onetrack/CrashAnalysis;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->d:Lcom/xiaomi/onetrack/CrashAnalysis;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->a:Ljava/util/List;

    iput-object p2, p0, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->c:Ljava/lang/String;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ".xcrash"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->b:Ljava/lang/String;

    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    const-string p0, "__"

    invoke-virtual {p1, p0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    array-length p1, p0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    aget-object p0, p0, p1

    const-string p1, "_"

    invoke-virtual {p0, p1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    array-length p1, p0

    const/4 v1, 0x3

    if-ne p1, v1, :cond_0

    aget-object p0, p0, v0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method


# virtual methods
.method a()V
    .locals 11

    goto/32 :goto_3d

    nop

    :goto_0
    invoke-static {v10, v1}, Lcom/xiaomi/onetrack/util/p;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    goto/32 :goto_a

    nop

    :goto_2
    iget-object v2, p0, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->d:Lcom/xiaomi/onetrack/CrashAnalysis;

    goto/32 :goto_39

    nop

    :goto_3
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1e

    nop

    :goto_4
    invoke-static {v3, v2}, Lcom/xiaomi/onetrack/CrashAnalysis;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_3c

    nop

    :goto_5
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    goto/32 :goto_25

    nop

    :goto_6
    iget-object v2, p0, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->d:Lcom/xiaomi/onetrack/CrashAnalysis;

    goto/32 :goto_31

    nop

    :goto_7
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_15

    nop

    :goto_8
    iget-object v2, p0, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->c:Ljava/lang/String;

    goto/32 :goto_4

    nop

    :goto_9
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_32

    nop

    :goto_a
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_3f

    nop

    :goto_b
    invoke-static {v10, v2}, Lcom/xiaomi/onetrack/util/p;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_2

    nop

    :goto_c
    const-string v1, "remove reported crash file"

    goto/32 :goto_0

    nop

    :goto_d
    iget-object v1, p0, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->a:Ljava/util/List;

    goto/32 :goto_20

    nop

    :goto_e
    const-string v5, "fileName: "

    goto/32 :goto_37

    nop

    :goto_f
    invoke-static {v10, v2}, Lcom/xiaomi/onetrack/util/p;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_34

    nop

    :goto_10
    invoke-static {v1, v2}, Lcom/xiaomi/onetrack/util/k;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_5

    nop

    :goto_11
    if-lt v0, v1, :cond_0

    goto/32 :goto_40

    :cond_0
    goto/32 :goto_d

    nop

    :goto_12
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_33

    nop

    :goto_13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_2c

    nop

    :goto_14
    invoke-virtual/range {v2 .. v9}, Lcom/xiaomi/onetrack/api/h;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    goto/32 :goto_1f

    nop

    :goto_15
    const-string v10, "CrashAnalysis"

    goto/32 :goto_2f

    nop

    :goto_16
    invoke-static {v10, v2}, Lcom/xiaomi/onetrack/util/p;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_2b

    nop

    :goto_17
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_7

    nop

    :goto_18
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_16

    nop

    :goto_19
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    goto/32 :goto_11

    nop

    :goto_1a
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_1b

    nop

    :goto_1b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_26

    nop

    :goto_1c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_2a

    nop

    :goto_1d
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_36

    nop

    :goto_1e
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_30

    nop

    :goto_1f
    new-instance v2, Ljava/io/File;

    goto/32 :goto_29

    nop

    :goto_20
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_23

    nop

    :goto_21
    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    goto/32 :goto_9

    nop

    :goto_22
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_2d

    nop

    :goto_23
    check-cast v1, Ljava/io/File;

    goto/32 :goto_21

    nop

    :goto_24
    if-nez v2, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_3b

    nop

    :goto_25
    if-eqz v2, :cond_2

    goto/32 :goto_1

    :cond_2
    goto/32 :goto_6

    nop

    :goto_26
    const-string v5, "feature id: "

    goto/32 :goto_22

    nop

    :goto_27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_b

    nop

    :goto_28
    invoke-static {v2}, Lcom/xiaomi/onetrack/util/k;->a(Ljava/io/File;)V

    goto/32 :goto_c

    nop

    :goto_29
    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto/32 :goto_28

    nop

    :goto_2a
    const-string v5, "crashTimeStamp: "

    goto/32 :goto_1d

    nop

    :goto_2b
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_13

    nop

    :goto_2c
    const-string v5, "error: "

    goto/32 :goto_3

    nop

    :goto_2d
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_18

    nop

    :goto_2e
    iget-object v5, p0, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->c:Ljava/lang/String;

    goto/32 :goto_14

    nop

    :goto_2f
    invoke-static {v10, v2}, Lcom/xiaomi/onetrack/util/p;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_1a

    nop

    :goto_30
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_f

    nop

    :goto_31
    invoke-static {v2}, Lcom/xiaomi/onetrack/CrashAnalysis;->c(Lcom/xiaomi/onetrack/CrashAnalysis;)Lcom/xiaomi/onetrack/api/h;

    move-result-object v2

    goto/32 :goto_24

    nop

    :goto_32
    invoke-direct {p0, v1}, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto/32 :goto_35

    nop

    :goto_33
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_e

    nop

    :goto_34
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_1c

    nop

    :goto_35
    const v2, 0x19000

    goto/32 :goto_10

    nop

    :goto_36
    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto/32 :goto_27

    nop

    :goto_37
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_17

    nop

    :goto_38
    invoke-static {v3, v2}, Lcom/xiaomi/onetrack/CrashAnalysis;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto/32 :goto_8

    nop

    :goto_39
    invoke-static {v2}, Lcom/xiaomi/onetrack/CrashAnalysis;->c(Lcom/xiaomi/onetrack/CrashAnalysis;)Lcom/xiaomi/onetrack/api/h;

    move-result-object v2

    goto/32 :goto_2e

    nop

    :goto_3a
    iget-object v1, p0, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->a:Ljava/util/List;

    goto/32 :goto_19

    nop

    :goto_3b
    iget-object v2, p0, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->c:Ljava/lang/String;

    goto/32 :goto_38

    nop

    :goto_3c
    invoke-static {v3}, Lcom/xiaomi/onetrack/CrashAnalysis;->a(Ljava/lang/String;)J

    move-result-wide v8

    goto/32 :goto_12

    nop

    :goto_3d
    const/4 v0, 0x0

    :goto_3e
    goto/32 :goto_3a

    nop

    :goto_3f
    goto/16 :goto_3e

    :goto_40
    goto/32 :goto_41

    nop

    :goto_41
    return-void
.end method

.method a(Ljava/io/File;)Z
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    return p0

    :goto_1
    const/4 p0, 0x1

    goto/32 :goto_4

    nop

    :goto_2
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_3
    iget-object p0, p0, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->a:Ljava/util/List;

    goto/32 :goto_a

    nop

    :goto_4
    return p0

    :goto_5
    goto/32 :goto_9

    nop

    :goto_6
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    goto/32 :goto_8

    nop

    :goto_7
    iget-object v1, p0, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->b:Ljava/lang/String;

    goto/32 :goto_6

    nop

    :goto_8
    if-nez v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_3

    nop

    :goto_9
    const/4 p0, 0x0

    goto/32 :goto_0

    nop

    :goto_a
    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_1

    nop
.end method
