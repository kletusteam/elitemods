.class public Lcom/xiaomi/onetrack/util/oaid/helpers/b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/onetrack/util/oaid/helpers/b$a;
    }
.end annotation


# static fields
.field static a:Ljava/lang/String; = "b"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static c()Ljava/lang/String;
    .locals 1

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    :try_start_0
    invoke-static {}, Lcom/xiaomi/onetrack/util/oaid/helpers/b;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/xiaomi/onetrack/util/oaid/helpers/b;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    sget-object p1, Lcom/xiaomi/onetrack/util/oaid/helpers/b;->a:Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-static {p1, p0}, Lcom/xiaomi/onetrack/util/p;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string p0, ""

    :goto_0
    return-object p0
.end method

.method a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    goto/32 :goto_3f

    nop

    :goto_0
    aget p0, p0, p2

    packed-switch p0, :pswitch_data_0

    goto/32 :goto_29

    nop

    :goto_1
    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    goto/32 :goto_0

    nop

    :goto_2
    new-array p1, p1, [Ljava/lang/Object;

    goto/32 :goto_12

    nop

    :goto_3
    new-instance p0, Lcom/xiaomi/onetrack/util/oaid/helpers/h;

    goto/32 :goto_d

    nop

    :goto_4
    invoke-direct {p0}, Lcom/xiaomi/onetrack/util/oaid/helpers/f;-><init>()V

    goto/32 :goto_18

    nop

    :goto_5
    invoke-virtual {p0}, Lcom/xiaomi/onetrack/util/oaid/helpers/b;->a()Z

    move-result v1

    goto/32 :goto_25

    nop

    :goto_6
    goto :goto_8

    :pswitch_0
    goto/32 :goto_2e

    nop

    :goto_7
    invoke-virtual {p0, p1}, Lcom/xiaomi/onetrack/util/oaid/helpers/a;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    :goto_8
    goto/32 :goto_26

    nop

    :goto_9
    goto :goto_8

    :pswitch_1
    goto/32 :goto_3a

    nop

    :goto_a
    sget-object v0, Lcom/xiaomi/onetrack/util/oaid/helpers/b$a;->p:Lcom/xiaomi/onetrack/util/oaid/helpers/b$a;

    :goto_b
    goto/32 :goto_3c

    nop

    :goto_c
    invoke-direct {p0}, Lcom/xiaomi/onetrack/util/oaid/helpers/j;-><init>()V

    goto/32 :goto_28

    nop

    :goto_d
    invoke-direct {p0}, Lcom/xiaomi/onetrack/util/oaid/helpers/h;-><init>()V

    goto/32 :goto_37

    nop

    :goto_e
    goto :goto_8

    :pswitch_2
    goto/32 :goto_2a

    nop

    :goto_f
    goto :goto_8

    :pswitch_3
    goto/32 :goto_24

    nop

    :goto_10
    invoke-virtual {p0, p1}, Lcom/xiaomi/onetrack/util/oaid/helpers/i;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_e

    nop

    :goto_11
    invoke-static {p2, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_22

    nop

    :goto_12
    const/4 v0, 0x0

    goto/32 :goto_34

    nop

    :goto_13
    new-instance p0, Ljava/lang/Exception;

    goto/32 :goto_43

    nop

    :goto_14
    goto :goto_8

    :pswitch_4
    goto/32 :goto_3e

    nop

    :goto_15
    invoke-direct {p0}, Lcom/xiaomi/onetrack/util/oaid/helpers/m;-><init>()V

    goto/32 :goto_39

    nop

    :goto_16
    new-instance p0, Lcom/xiaomi/onetrack/util/oaid/helpers/d;

    goto/32 :goto_1d

    nop

    :goto_17
    invoke-direct {p0}, Lcom/xiaomi/onetrack/util/oaid/helpers/e;-><init>()V

    goto/32 :goto_41

    nop

    :goto_18
    invoke-virtual {p0, p1}, Lcom/xiaomi/onetrack/util/oaid/helpers/f;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_f

    nop

    :goto_19
    sget-object v0, Lcom/xiaomi/onetrack/util/oaid/helpers/b$a;->o:Lcom/xiaomi/onetrack/util/oaid/helpers/b$a;

    :goto_1a
    goto/32 :goto_30

    nop

    :goto_1b
    new-instance p0, Lcom/xiaomi/onetrack/util/oaid/helpers/m;

    goto/32 :goto_15

    nop

    :goto_1c
    goto :goto_8

    :pswitch_5
    goto/32 :goto_35

    nop

    :goto_1d
    invoke-direct {p0}, Lcom/xiaomi/onetrack/util/oaid/helpers/d;-><init>()V

    goto/32 :goto_1f

    nop

    :goto_1e
    invoke-direct {p0}, Lcom/xiaomi/onetrack/util/oaid/helpers/k;-><init>()V

    goto/32 :goto_3d

    nop

    :goto_1f
    invoke-virtual {p0, p1}, Lcom/xiaomi/onetrack/util/oaid/helpers/d;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_33

    nop

    :goto_20
    new-instance p0, Lcom/xiaomi/onetrack/util/oaid/helpers/k;

    goto/32 :goto_1e

    nop

    :goto_21
    goto/16 :goto_8

    :pswitch_6
    goto/32 :goto_20

    nop

    :goto_22
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    goto/32 :goto_42

    nop

    :goto_23
    goto/16 :goto_8

    :pswitch_7
    goto/32 :goto_3

    nop

    :goto_24
    new-instance p0, Lcom/xiaomi/onetrack/util/oaid/helpers/e;

    goto/32 :goto_17

    nop

    :goto_25
    if-nez v1, :cond_0

    goto/32 :goto_1a

    :cond_0
    goto/32 :goto_19

    nop

    :goto_26
    return-object p0

    :goto_27
    goto/32 :goto_13

    nop

    :goto_28
    invoke-virtual {p0, p1}, Lcom/xiaomi/onetrack/util/oaid/helpers/j;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_9

    nop

    :goto_29
    const-string p0, ""

    goto/32 :goto_2b

    nop

    :goto_2a
    invoke-static {p1}, Lcom/xiaomi/onetrack/util/n;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_14

    nop

    :goto_2b
    goto/16 :goto_8

    :pswitch_8
    goto/32 :goto_1b

    nop

    :goto_2c
    invoke-direct {p0}, Lcom/xiaomi/onetrack/util/oaid/helpers/l;-><init>()V

    goto/32 :goto_2f

    nop

    :goto_2d
    if-nez p0, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_a

    nop

    :goto_2e
    new-instance p0, Lcom/xiaomi/onetrack/util/oaid/helpers/i;

    goto/32 :goto_3b

    nop

    :goto_2f
    invoke-virtual {p0, p1}, Lcom/xiaomi/onetrack/util/oaid/helpers/l;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_23

    nop

    :goto_30
    invoke-virtual {p0}, Lcom/xiaomi/onetrack/util/oaid/helpers/b;->b()Z

    move-result p0

    goto/32 :goto_2d

    nop

    :goto_31
    goto/16 :goto_8

    :pswitch_9
    goto/32 :goto_16

    nop

    :goto_32
    invoke-direct {p0}, Lcom/xiaomi/onetrack/util/oaid/helpers/a;-><init>()V

    goto/32 :goto_7

    nop

    :goto_33
    goto/16 :goto_8

    :pswitch_a
    goto/32 :goto_36

    nop

    :goto_34
    aput-object p2, p1, v0

    goto/32 :goto_38

    nop

    :goto_35
    new-instance p0, Lcom/xiaomi/onetrack/util/oaid/helpers/j;

    goto/32 :goto_c

    nop

    :goto_36
    new-instance p0, Lcom/xiaomi/onetrack/util/oaid/helpers/a;

    goto/32 :goto_32

    nop

    :goto_37
    invoke-virtual {p0, p1}, Lcom/xiaomi/onetrack/util/oaid/helpers/h;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_21

    nop

    :goto_38
    const-string p2, "undefined oaid method of manufacturer %s"

    goto/32 :goto_11

    nop

    :goto_39
    invoke-virtual {p0, p1}, Lcom/xiaomi/onetrack/util/oaid/helpers/m;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_6

    nop

    :goto_3a
    new-instance p0, Lcom/xiaomi/onetrack/util/oaid/helpers/f;

    goto/32 :goto_4

    nop

    :goto_3b
    invoke-direct {p0}, Lcom/xiaomi/onetrack/util/oaid/helpers/i;-><init>()V

    goto/32 :goto_10

    nop

    :goto_3c
    if-nez v0, :cond_2

    goto/32 :goto_27

    :cond_2
    goto/32 :goto_40

    nop

    :goto_3d
    invoke-virtual {p0, p1}, Lcom/xiaomi/onetrack/util/oaid/helpers/k;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_1c

    nop

    :goto_3e
    new-instance p0, Lcom/xiaomi/onetrack/util/oaid/helpers/l;

    goto/32 :goto_2c

    nop

    :goto_3f
    invoke-static {p2}, Lcom/xiaomi/onetrack/util/oaid/helpers/b$a;->b(Ljava/lang/String;)Lcom/xiaomi/onetrack/util/oaid/helpers/b$a;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_40
    sget-object p0, Lcom/xiaomi/onetrack/util/oaid/helpers/c;->a:[I

    goto/32 :goto_1

    nop

    :goto_41
    invoke-virtual {p0, p1}, Lcom/xiaomi/onetrack/util/oaid/helpers/e;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_31

    nop

    :goto_42
    throw p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch

    :goto_43
    const/4 p1, 0x1

    goto/32 :goto_2

    nop
.end method

.method public a()Z
    .locals 1

    const-string p0, "ro.build.freeme.label"

    invoke-static {p0}, Lcom/xiaomi/onetrack/util/ab;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "FREEMEOS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public b()Z
    .locals 1

    const-string p0, "ro.ssui.product"

    invoke-static {p0}, Lcom/xiaomi/onetrack/util/ab;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "unknown"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method
