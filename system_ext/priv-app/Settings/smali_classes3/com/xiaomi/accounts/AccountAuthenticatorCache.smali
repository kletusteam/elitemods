.class public Lcom/xiaomi/accounts/AccountAuthenticatorCache;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/accounts/AccountAuthenticatorCache$ServiceInfo;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mInterfaceName:Ljava/lang/String;

.field private mServiceInfo:Lcom/xiaomi/accounts/AccountAuthenticatorCache$ServiceInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/xiaomi/accounts/AccountAuthenticatorCache$ServiceInfo<",
            "Landroid/accounts/AuthenticatorDescription;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/accounts/AccountAuthenticatorCache;->mContext:Landroid/content/Context;

    const-string p1, "com.xiaomi.accounts.AccountAuthenticator"

    iput-object p1, p0, Lcom/xiaomi/accounts/AccountAuthenticatorCache;->mInterfaceName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/xiaomi/accounts/AccountAuthenticatorCache;->generateServicesMap()V

    return-void
.end method

.method private parseServiceInfo(Landroid/content/pm/ResolveInfo;)Lcom/xiaomi/accounts/AccountAuthenticatorCache$ServiceInfo;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/ResolveInfo;",
            ")",
            "Lcom/xiaomi/accounts/AccountAuthenticatorCache$ServiceInfo<",
            "Landroid/accounts/AuthenticatorDescription;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    iget-object p0, p1, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object p1, p0, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    iget-object v0, p0, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    iget-object p0, p0, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/xiaomi/accounts/AccountAuthenticatorCache;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p1

    iget-object p0, p0, Lcom/xiaomi/accounts/AccountAuthenticatorCache;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object p0

    const-string v0, "com.xiaomi.passport.accountmanager.MiAuthenticatorService"

    :goto_0
    move-object v2, p1

    new-instance p1, Landroid/content/ComponentName;

    invoke-direct {p1, v2, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget v7, p0, Landroid/content/pm/ApplicationInfo;->uid:I

    new-instance v8, Landroid/accounts/AuthenticatorDescription;

    iget v3, p0, Landroid/content/pm/ApplicationInfo;->labelRes:I

    iget v5, p0, Landroid/content/pm/ApplicationInfo;->icon:I

    const/4 v6, -0x1

    const-string v1, "com.xiaomi"

    move-object v0, v8

    move v4, v5

    invoke-direct/range {v0 .. v6}, Landroid/accounts/AuthenticatorDescription;-><init>(Ljava/lang/String;Ljava/lang/String;IIII)V

    new-instance p0, Lcom/xiaomi/accounts/AccountAuthenticatorCache$ServiceInfo;

    invoke-direct {p0, v8, p1, v7}, Lcom/xiaomi/accounts/AccountAuthenticatorCache$ServiceInfo;-><init>(Ljava/lang/Object;Landroid/content/ComponentName;I)V

    return-object p0
.end method


# virtual methods
.method generateServicesMap()V
    .locals 3

    goto/32 :goto_8

    nop

    :goto_0
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_3

    nop

    :goto_1
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto/32 :goto_9

    nop

    :goto_2
    iget-object v1, p0, Lcom/xiaomi/accounts/AccountAuthenticatorCache;->mInterfaceName:Ljava/lang/String;

    goto/32 :goto_1

    nop

    :goto_3
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    goto/32 :goto_6

    nop

    :goto_4
    invoke-direct {p0, v0}, Lcom/xiaomi/accounts/AccountAuthenticatorCache;->parseServiceInfo(Landroid/content/pm/ResolveInfo;)Lcom/xiaomi/accounts/AccountAuthenticatorCache$ServiceInfo;

    move-result-object v0

    goto/32 :goto_c

    nop

    :goto_5
    const/4 v2, 0x0

    goto/32 :goto_7

    nop

    :goto_6
    iget-object v1, p0, Lcom/xiaomi/accounts/AccountAuthenticatorCache;->mContext:Landroid/content/Context;

    goto/32 :goto_a

    nop

    :goto_7
    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->resolveService(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_8
    new-instance v0, Landroid/content/Intent;

    goto/32 :goto_2

    nop

    :goto_9
    iget-object v1, p0, Lcom/xiaomi/accounts/AccountAuthenticatorCache;->mContext:Landroid/content/Context;

    goto/32 :goto_0

    nop

    :goto_a
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    goto/32 :goto_5

    nop

    :goto_b
    return-void

    :goto_c
    iput-object v0, p0, Lcom/xiaomi/accounts/AccountAuthenticatorCache;->mServiceInfo:Lcom/xiaomi/accounts/AccountAuthenticatorCache$ServiceInfo;

    goto/32 :goto_b

    nop
.end method

.method public getServiceInfo(Landroid/accounts/AuthenticatorDescription;)Lcom/xiaomi/accounts/AccountAuthenticatorCache$ServiceInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AuthenticatorDescription;",
            ")",
            "Lcom/xiaomi/accounts/AccountAuthenticatorCache$ServiceInfo<",
            "Landroid/accounts/AuthenticatorDescription;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_1

    iget-object p1, p1, Landroid/accounts/AuthenticatorDescription;->type:Ljava/lang/String;

    const-string v0, "com.xiaomi"

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p0, p0, Lcom/xiaomi/accounts/AccountAuthenticatorCache;->mServiceInfo:Lcom/xiaomi/accounts/AccountAuthenticatorCache$ServiceInfo;

    return-object p0

    :cond_1
    :goto_0
    const-string p0, "Account"

    const-string p1, "no xiaomi account type"

    invoke-static {p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p0, 0x0

    return-object p0
.end method
