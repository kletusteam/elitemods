.class public Lcom/xiaomi/accounts/secure/KeyStoreRSA;
.super Ljava/lang/Object;


# instance fields
.field private final alias:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/accounts/secure/KeyStoreRSA;->alias:Ljava/lang/String;

    return-void
.end method

.method private generate_api18_22(Landroid/content/Context;ILjava/lang/String;Ljava/util/Date;Ljava/util/Date;IZ)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    new-instance v0, Ljavax/security/auth/x500/X500Principal;

    invoke-direct {v0, p3}, Ljavax/security/auth/x500/X500Principal;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/xiaomi/accounts/secure/KeyStoreRSA;->obtainKeyStore()Ljava/security/KeyStore;

    move-result-object p3

    new-instance v1, Landroid/security/KeyPairGeneratorSpec$Builder;

    invoke-direct {v1, p1}, Landroid/security/KeyPairGeneratorSpec$Builder;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/xiaomi/accounts/secure/KeyStoreRSA;->alias:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/security/KeyPairGeneratorSpec$Builder;->setAlias(Ljava/lang/String;)Landroid/security/KeyPairGeneratorSpec$Builder;

    invoke-virtual {v1, p4}, Landroid/security/KeyPairGeneratorSpec$Builder;->setStartDate(Ljava/util/Date;)Landroid/security/KeyPairGeneratorSpec$Builder;

    invoke-virtual {v1, p5}, Landroid/security/KeyPairGeneratorSpec$Builder;->setEndDate(Ljava/util/Date;)Landroid/security/KeyPairGeneratorSpec$Builder;

    int-to-long p4, p6

    invoke-static {p4, p5}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object p4

    invoke-virtual {v1, p4}, Landroid/security/KeyPairGeneratorSpec$Builder;->setSerialNumber(Ljava/math/BigInteger;)Landroid/security/KeyPairGeneratorSpec$Builder;

    invoke-virtual {v1, v0}, Landroid/security/KeyPairGeneratorSpec$Builder;->setSubject(Ljavax/security/auth/x500/X500Principal;)Landroid/security/KeyPairGeneratorSpec$Builder;

    if-eqz p7, :cond_0

    invoke-virtual {v1}, Landroid/security/KeyPairGeneratorSpec$Builder;->setEncryptionRequired()Landroid/security/KeyPairGeneratorSpec$Builder;

    :cond_0
    invoke-virtual {v1, p2}, Landroid/security/KeyPairGeneratorSpec$Builder;->setKeySize(I)Landroid/security/KeyPairGeneratorSpec$Builder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p2

    sget-object p4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-static {p1, p4}, Lcom/xiaomi/accounts/secure/KeyStoreRSA;->setLocale(Landroid/content/Context;Ljava/util/Locale;)V

    const-string p4, "RSA"

    const-string p5, "AndroidKeyStore"

    invoke-static {p4, p5}, Ljava/security/KeyPairGenerator;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyPairGenerator;

    move-result-object p4

    invoke-virtual {v1}, Landroid/security/KeyPairGeneratorSpec$Builder;->build()Landroid/security/KeyPairGeneratorSpec;

    move-result-object p5

    invoke-virtual {p4, p5}, Ljava/security/KeyPairGenerator;->initialize(Ljava/security/spec/AlgorithmParameterSpec;)V

    :try_start_0
    invoke-virtual {p4}, Ljava/security/KeyPairGenerator;->generateKeyPair()Ljava/security/KeyPair;
    :try_end_0
    .catch Ljava/security/ProviderException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {p1, p2}, Lcom/xiaomi/accounts/secure/KeyStoreRSA;->setLocale(Landroid/content/Context;Ljava/util/Locale;)V

    :try_start_1
    iget-object p1, p0, Lcom/xiaomi/accounts/secure/KeyStoreRSA;->alias:Ljava/lang/String;

    const/4 p2, 0x0

    invoke-virtual {p3, p1, p2}, Ljava/security/KeyStore;->getKey(Ljava/lang/String;[C)Ljava/security/Key;

    move-result-object p1

    check-cast p1, Ljava/security/PrivateKey;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    const-string p2, "Failed to obtain private key from a generated key pair"

    if-eqz p1, :cond_2

    iget-object p0, p0, Lcom/xiaomi/accounts/secure/KeyStoreRSA;->alias:Ljava/lang/String;

    invoke-virtual {p3, p0}, Ljava/security/KeyStore;->getCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;

    move-result-object p0

    invoke-virtual {p0}, Ljava/security/cert/Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object p0

    if-eqz p0, :cond_1

    return-void

    :cond_1
    new-instance p0, Ljava/security/GeneralSecurityException;

    invoke-direct {p0, p2}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_2
    :try_start_2
    new-instance p0, Ljava/security/GeneralSecurityException;

    invoke-direct {p0, p2}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception p0

    new-instance p1, Ljava/security/GeneralSecurityException;

    invoke-direct {p1, p0}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/Throwable;)V

    throw p1

    :catchall_0
    move-exception p0

    goto :goto_0

    :catch_1
    move-exception p0

    :try_start_3
    new-instance p3, Ljava/security/GeneralSecurityException;

    invoke-direct {p3, p0}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/Throwable;)V

    throw p3

    :catch_2
    move-exception p0

    new-instance p3, Ljava/security/GeneralSecurityException;

    invoke-direct {p3, p0}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/Throwable;)V

    throw p3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_0
    invoke-static {p1, p2}, Lcom/xiaomi/accounts/secure/KeyStoreRSA;->setLocale(Landroid/content/Context;Ljava/util/Locale;)V

    throw p0
.end method

.method private obtainKeyStore()Ljava/security/KeyStore;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    const-string p0, "AndroidKeyStore"

    invoke-static {p0}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Ljava/security/KeyStore;->load(Ljava/security/KeyStore$LoadStoreParameter;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    new-instance v0, Ljava/security/KeyStoreException;

    const-string v1, "Key store error"

    invoke-direct {v0, v1, p0}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method private static setLocale(Landroid/content/Context;Ljava/util/Locale;)V
    .locals 1

    invoke-static {p1}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iput-object p1, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    return-void
.end method


# virtual methods
.method derive(Ljava/lang/String;I)[B
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    invoke-direct {p0}, Lcom/xiaomi/accounts/secure/KeyStoreRSA;->obtainKeyStore()Ljava/security/KeyStore;

    move-result-object v0

    :try_start_0
    iget-object p0, p0, Lcom/xiaomi/accounts/secure/KeyStoreRSA;->alias:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Ljava/security/KeyStore;->getKey(Ljava/lang/String;[C)Ljava/security/Key;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_d

    nop

    :goto_1
    const-string v0, "out length must be maximal 255 * hash-length; requested: "

    goto/32 :goto_49

    nop

    :goto_2
    check-cast p0, Ljava/security/PrivateKey;

    goto/32 :goto_11

    nop

    :goto_3
    invoke-virtual {v1}, Ljavax/crypto/Mac;->getMacLength()I

    move-result v1

    goto/32 :goto_31

    nop

    :goto_4
    invoke-virtual {v0}, Ljava/security/Signature;->sign()[B

    move-result-object p0

    goto/32 :goto_37

    nop

    :goto_5
    new-array v3, v3, [B

    goto/32 :goto_16

    nop

    :goto_6
    invoke-virtual {v0, p0}, Ljava/security/Signature;->update([B)V

    goto/32 :goto_4

    nop

    :goto_7
    if-le v1, v4, :cond_0

    goto/32 :goto_42

    :cond_0
    goto/32 :goto_13

    nop

    :goto_8
    int-to-double v4, p2

    goto/32 :goto_3

    nop

    :goto_9
    move v5, v3

    :goto_a
    goto/32 :goto_15

    nop

    :goto_b
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_1d

    nop

    :goto_c
    invoke-direct {p1, p0}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/Throwable;)V

    goto/32 :goto_43

    nop

    :goto_d
    if-nez p0, :cond_1

    goto/32 :goto_47

    :cond_1
    goto/32 :goto_2

    nop

    :goto_e
    invoke-virtual {v6, p0}, Ljavax/crypto/Mac;->update(B)V

    goto/32 :goto_12

    nop

    :goto_f
    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    goto/32 :goto_1e

    nop

    :goto_10
    int-to-byte p0, v5

    goto/32 :goto_e

    nop

    :goto_11
    const-string v0, "SHA256withRSA"

    goto/32 :goto_32

    nop

    :goto_12
    invoke-virtual {v6}, Ljavax/crypto/Mac;->doFinal()[B

    move-result-object p0

    goto/32 :goto_35

    nop

    :goto_13
    invoke-static {p2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    goto/32 :goto_9

    nop

    :goto_14
    invoke-static {p2, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    goto/32 :goto_21

    nop

    :goto_15
    if-lt v5, v1, :cond_2

    goto/32 :goto_2f

    :cond_2
    goto/32 :goto_30

    nop

    :goto_16
    invoke-direct {v2, v3, v0}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    goto/32 :goto_2a

    nop

    :goto_17
    invoke-virtual {v0, p0}, Ljava/security/Signature;->initSign(Ljava/security/PrivateKey;)V

    goto/32 :goto_44

    nop

    :goto_18
    sub-int/2addr p2, v6

    goto/32 :goto_2e

    nop

    :goto_19
    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    goto/32 :goto_24

    nop

    :goto_1a
    invoke-static {v0}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v1

    goto/32 :goto_19

    nop

    :goto_1b
    invoke-virtual {v1, p0}, Ljavax/crypto/Mac;->doFinal([B)[B

    move-result-object v3

    goto/32 :goto_2d

    nop

    :goto_1c
    invoke-virtual {v6, p0}, Ljavax/crypto/Mac;->update([B)V

    goto/32 :goto_29

    nop

    :goto_1d
    const-string p2, " bytes"

    goto/32 :goto_3f

    nop

    :goto_1e
    double-to-int v1, v4

    goto/32 :goto_40

    nop

    :goto_1f
    move v4, v3

    :goto_20
    goto/32 :goto_3c

    nop

    :goto_21
    invoke-virtual {v4, p0, v3, v6}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    goto/32 :goto_18

    nop

    :goto_22
    add-int/lit8 v4, v4, 0x1

    goto/32 :goto_39

    nop

    :goto_23
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p0

    goto/32 :goto_1c

    nop

    :goto_24
    invoke-virtual {v1}, Ljavax/crypto/Mac;->getMacLength()I

    move-result v3

    goto/32 :goto_5

    nop

    :goto_25
    new-instance p0, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_3d

    nop

    :goto_26
    aput-byte v5, p0, v4

    goto/32 :goto_22

    nop

    :goto_27
    invoke-virtual {v6, v2}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    goto/32 :goto_4a

    nop

    :goto_28
    new-array p0, v3, [B

    goto/32 :goto_8

    nop

    :goto_29
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_10

    nop

    :goto_2a
    invoke-virtual {v1, v2}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    goto/32 :goto_3e

    nop

    :goto_2b
    div-double/2addr v4, v6

    goto/32 :goto_f

    nop

    :goto_2c
    const/16 v5, 0x77

    goto/32 :goto_26

    nop

    :goto_2d
    invoke-direct {v2, v3, v0}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    goto/32 :goto_36

    nop

    :goto_2e
    goto/16 :goto_a

    :goto_2f
    goto/32 :goto_3b

    nop

    :goto_30
    invoke-static {v0}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v6

    goto/32 :goto_27

    nop

    :goto_31
    int-to-double v6, v1

    goto/32 :goto_2b

    nop

    :goto_32
    invoke-static {v0}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v0

    goto/32 :goto_17

    nop

    :goto_33
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_45

    nop

    :goto_34
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1

    nop

    :goto_35
    array-length v6, p0

    goto/32 :goto_14

    nop

    :goto_36
    const/4 v3, 0x0

    goto/32 :goto_1f

    nop

    :goto_37
    const-string v0, "HmacSHA256"

    goto/32 :goto_1a

    nop

    :goto_38
    if-lt v4, v5, :cond_3

    goto/32 :goto_3a

    :cond_3
    goto/32 :goto_2c

    nop

    :goto_39
    goto/16 :goto_20

    :goto_3a
    goto/32 :goto_28

    nop

    :goto_3b
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object p0

    goto/32 :goto_41

    nop

    :goto_3c
    array-length v5, p0

    goto/32 :goto_38

    nop

    :goto_3d
    new-instance p1, Ljava/lang/StringBuilder;

    goto/32 :goto_34

    nop

    :goto_3e
    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    goto/32 :goto_1b

    nop

    :goto_3f
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_33

    nop

    :goto_40
    const/16 v4, 0xff

    goto/32 :goto_7

    nop

    :goto_41
    return-object p0

    :goto_42
    goto/32 :goto_25

    nop

    :goto_43
    throw p1

    :goto_44
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p0

    goto/32 :goto_6

    nop

    :goto_45
    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_46

    nop

    :goto_46
    throw p0

    :goto_47
    :try_start_1
    new-instance p0, Ljava/security/GeneralSecurityException;

    const-string p1, "Failed to obtain private key from a generated key pair"

    invoke-direct {p0, p1}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p0

    goto/32 :goto_48

    nop

    :goto_48
    new-instance p1, Ljava/security/GeneralSecurityException;

    goto/32 :goto_c

    nop

    :goto_49
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_b

    nop

    :goto_4a
    invoke-virtual {v6, p0}, Ljavax/crypto/Mac;->update([B)V

    goto/32 :goto_23

    nop
.end method

.method discard()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    goto/32 :goto_3

    nop

    :goto_0
    iget-object p0, p0, Lcom/xiaomi/accounts/secure/KeyStoreRSA;->alias:Ljava/lang/String;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {v0, p0}, Ljava/security/KeyStore;->deleteEntry(Ljava/lang/String;)V

    goto/32 :goto_2

    nop

    :goto_2
    return-void

    :goto_3
    invoke-direct {p0}, Lcom/xiaomi/accounts/secure/KeyStoreRSA;->obtainKeyStore()Ljava/security/KeyStore;

    move-result-object v0

    goto/32 :goto_0

    nop
.end method

.method exists()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    invoke-direct {p0}, Lcom/xiaomi/accounts/secure/KeyStoreRSA;->obtainKeyStore()Ljava/security/KeyStore;

    move-result-object v0

    :try_start_0
    iget-object p0, p0, Lcom/xiaomi/accounts/secure/KeyStoreRSA;->alias:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Ljava/security/KeyStore;->getKey(Ljava/lang/String;[C)Ljava/security/Key;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_5

    nop

    :goto_1
    return p0

    :goto_2
    :try_start_1
    new-instance p0, Ljava/security/GeneralSecurityException;

    const-string v0, "Failed to obtain private key from a generated key pair"

    invoke-direct {p0, v0}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p0

    goto/32 :goto_7

    nop

    :goto_3
    invoke-direct {v0, p0}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/Throwable;)V

    goto/32 :goto_4

    nop

    :goto_4
    throw v0

    :goto_5
    if-nez p0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_6

    nop

    :goto_6
    instance-of p0, p0, Ljava/security/PrivateKey;

    goto/32 :goto_1

    nop

    :goto_7
    new-instance v0, Ljava/security/GeneralSecurityException;

    goto/32 :goto_3

    nop
.end method

.method generate(Landroid/content/Context;ILjava/lang/String;Ljava/util/Date;Ljava/util/Date;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    invoke-direct/range {p0 .. p7}, Lcom/xiaomi/accounts/secure/KeyStoreRSA;->generate_api18_22(Landroid/content/Context;ILjava/lang/String;Ljava/util/Date;Ljava/util/Date;IZ)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method
