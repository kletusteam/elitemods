.class public Lcom/xiaomi/accountsdk/utils/FidNonce$Builder;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/accountsdk/utils/FidNonce;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build(Lcom/xiaomi/accountsdk/utils/FidNonce$Type;)Lcom/xiaomi/accountsdk/utils/FidNonce;
    .locals 2

    invoke-static {}, Lcom/xiaomi/accountsdk/utils/ServerTimeUtil;->getComputer()Lcom/xiaomi/accountsdk/utils/ServerTimeUtil$IServerTimeComputer;

    move-result-object p0

    invoke-static {}, Lcom/xiaomi/accountsdk/utils/FidSigningUtil;->getFidSigner()Lcom/xiaomi/accountsdk/utils/FidSigningUtil$IFidSigner;

    move-result-object v0

    new-instance v1, Lcom/xiaomi/accountsdk/utils/FidNonce$Builder;

    invoke-direct {v1}, Lcom/xiaomi/accountsdk/utils/FidNonce$Builder;-><init>()V

    invoke-virtual {v1, p1, p0, v0}, Lcom/xiaomi/accountsdk/utils/FidNonce$Builder;->build(Lcom/xiaomi/accountsdk/utils/FidNonce$Type;Lcom/xiaomi/accountsdk/utils/ServerTimeUtil$IServerTimeComputer;Lcom/xiaomi/accountsdk/utils/FidSigningUtil$IFidSigner;)Lcom/xiaomi/accountsdk/utils/FidNonce;

    move-result-object p0

    return-object p0
.end method

.method build(Lcom/xiaomi/accountsdk/utils/FidNonce$Type;Lcom/xiaomi/accountsdk/utils/ServerTimeUtil$IServerTimeComputer;Lcom/xiaomi/accountsdk/utils/FidSigningUtil$IFidSigner;)Lcom/xiaomi/accountsdk/utils/FidNonce;
    .locals 5

    goto/32 :goto_17

    nop

    :goto_0
    if-eqz v3, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_6

    nop

    :goto_1
    return-object v2

    :catch_0
    move-exception p0

    goto/32 :goto_25

    nop

    :goto_2
    return-object v2

    :goto_3
    :try_start_0
    invoke-interface {p3}, Lcom/xiaomi/accountsdk/utils/FidSigningUtil$IFidSigner;->canSign()Z

    move-result v3
    :try_end_0
    .catch Lcom/xiaomi/accountsdk/utils/FidSigningUtil$FidSignException; {:try_start_0 .. :try_end_0} :catch_4

    goto/32 :goto_0

    nop

    :goto_4
    throw p0

    :goto_5
    const/4 v2, 0x0

    goto/32 :goto_2b

    nop

    :goto_6
    return-object v2

    :goto_7
    goto/32 :goto_22

    nop

    :goto_8
    return-object v2

    :catch_1
    move-exception p0

    goto/32 :goto_a

    nop

    :goto_9
    invoke-static {p0, p2}, Landroid/util/Base64;->encode([BI)[B

    move-result-object p0

    :try_start_1
    new-instance p2, Ljava/lang/String;

    invoke-direct {p2, p0, v0}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_2

    goto/32 :goto_27

    nop

    :goto_a
    invoke-static {v1, p0}, Lcom/xiaomi/accountsdk/utils/AccountLog;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/32 :goto_1

    nop

    :goto_b
    invoke-static {v1, p0}, Lcom/xiaomi/accountsdk/utils/AccountLog;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/32 :goto_8

    nop

    :goto_c
    return-object p0

    :catch_2
    move-exception p0

    goto/32 :goto_1a

    nop

    :goto_d
    if-eqz p0, :cond_1

    goto/32 :goto_24

    :cond_1
    goto/32 :goto_23

    nop

    :goto_e
    return-object v2

    :catch_3
    move-exception p0

    goto/32 :goto_b

    nop

    :goto_f
    new-instance p0, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_14

    nop

    :goto_10
    invoke-virtual {p0, p1, p2, v3}, Lcom/xiaomi/accountsdk/utils/FidNonce$Builder;->buildPlain(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    :try_start_2
    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object p1

    const/16 p2, 0xa

    invoke-static {p1, p2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object p1
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_0

    :try_start_3
    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object p0

    invoke-interface {p3, p0}, Lcom/xiaomi/accountsdk/utils/FidSigningUtil$IFidSigner;->sign([B)[B

    move-result-object p0
    :try_end_3
    .catch Lcom/xiaomi/accountsdk/utils/FidSigningUtil$FidSignException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3 .. :try_end_3} :catch_3

    goto/32 :goto_d

    nop

    :goto_11
    return-object v2

    :catch_4
    move-exception p0

    goto/32 :goto_1f

    nop

    :goto_12
    const-string p1, "n"

    goto/32 :goto_1b

    nop

    :goto_13
    if-eqz p3, :cond_2

    goto/32 :goto_3

    :cond_2
    goto/32 :goto_2

    nop

    :goto_14
    const-string p1, "type == null"

    goto/32 :goto_16

    nop

    :goto_15
    invoke-direct {p0, p1, p2}, Lcom/xiaomi/accountsdk/utils/FidNonce;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_c

    nop

    :goto_16
    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_4

    nop

    :goto_17
    const-string v0, "UTF-8"

    goto/32 :goto_18

    nop

    :goto_18
    const-string v1, "FidNonce"

    goto/32 :goto_19

    nop

    :goto_19
    if-nez p1, :cond_3

    goto/32 :goto_2a

    :cond_3
    goto/32 :goto_5

    nop

    :goto_1a
    invoke-static {v1, p0}, Lcom/xiaomi/accountsdk/utils/AccountLog;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/32 :goto_e

    nop

    :goto_1b
    goto :goto_1e

    :goto_1c
    goto/32 :goto_1d

    nop

    :goto_1d
    const-string p1, "wb"

    :goto_1e
    goto/32 :goto_2d

    nop

    :goto_1f
    invoke-static {v1, p0}, Lcom/xiaomi/accountsdk/utils/AccountLog;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/32 :goto_29

    nop

    :goto_20
    return-object v2

    :goto_21
    goto/32 :goto_13

    nop

    :goto_22
    sget-object v3, Lcom/xiaomi/accountsdk/utils/FidNonce$Type;->NATIVE:Lcom/xiaomi/accountsdk/utils/FidNonce$Type;

    goto/32 :goto_2c

    nop

    :goto_23
    return-object v2

    :goto_24
    goto/32 :goto_9

    nop

    :goto_25
    invoke-static {v1, p0}, Lcom/xiaomi/accountsdk/utils/AccountLog;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/32 :goto_11

    nop

    :goto_26
    invoke-virtual {p0}, Lcom/xiaomi/accountsdk/utils/FidNonce$Builder;->getVersion()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_10

    nop

    :goto_27
    new-instance p0, Lcom/xiaomi/accountsdk/utils/FidNonce;

    goto/32 :goto_15

    nop

    :goto_28
    invoke-virtual {p0, v3, v4}, Lcom/xiaomi/accountsdk/utils/FidNonce$Builder;->generateNonce(J)Ljava/lang/String;

    move-result-object p2

    goto/32 :goto_26

    nop

    :goto_29
    return-object v2

    :goto_2a
    goto/32 :goto_f

    nop

    :goto_2b
    if-eqz p2, :cond_4

    goto/32 :goto_21

    :cond_4
    goto/32 :goto_20

    nop

    :goto_2c
    if-eq p1, v3, :cond_5

    goto/32 :goto_1c

    :cond_5
    goto/32 :goto_12

    nop

    :goto_2d
    invoke-interface {p2}, Lcom/xiaomi/accountsdk/utils/ServerTimeUtil$IServerTimeComputer;->computeServerTime()J

    move-result-wide v3

    goto/32 :goto_28

    nop
.end method

.method buildPlain(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    :try_start_0
    new-instance p0, Lorg/json/JSONObject;

    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    const-string v0, "tp"

    invoke-virtual {p0, v0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "nonce"

    invoke-virtual {p0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "v"

    invoke-virtual {p0, p1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {p0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_1

    nop

    :goto_0
    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_4

    nop

    :goto_1
    return-object p0

    :catch_0
    goto/32 :goto_2

    nop

    :goto_2
    new-instance p0, Ljava/lang/IllegalStateException;

    goto/32 :goto_3

    nop

    :goto_3
    const-string p1, "should not happen"

    goto/32 :goto_0

    nop

    :goto_4
    throw p0
.end method

.method generateNonce(J)Ljava/lang/String;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-static {p1, p2}, Lcom/xiaomi/accountsdk/utils/NonceCoder;->generateNonce(J)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_1

    nop

    :goto_1
    return-object p0
.end method

.method getVersion()Ljava/lang/String;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-static {}, Lcom/xiaomi/accountsdk/utils/VersionUtils;->getVersion()Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_1

    nop

    :goto_1
    return-object p0
.end method
