.class public abstract Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<IServiceType:",
        "Ljava/lang/Object;",
        "ServerDataType:",
        "Ljava/lang/Object;",
        "ClientDataType:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/content/ServiceConnection;"
    }
.end annotation


# static fields
.field private static final mThreadPool:Ljava/util/concurrent/ExecutorService;


# instance fields
.field private final bindFlag:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mActionName:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private volatile mFuture:Lcom/xiaomi/accountsdk/futureservice/ClientFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/xiaomi/accountsdk/futureservice/ClientFuture<",
            "TServerDataType;TClientDataType;>;"
        }
    .end annotation
.end field

.field private mIService:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TIServiceType;"
        }
    .end annotation
.end field

.field private final mServicePackageName:Ljava/lang/String;

.field private final unbindFlag:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/xiaomi/accountsdk/futureservice/ClientFuture;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/xiaomi/accountsdk/futureservice/ClientFuture<",
            "TServerDataType;TClientDataType;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->bindFlag:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->unbindFlag:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->mActionName:Ljava/lang/String;

    iput-object p3, p0, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->mServicePackageName:Ljava/lang/String;

    iput-object p4, p0, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->mFuture:Lcom/xiaomi/accountsdk/futureservice/ClientFuture;

    return-void
.end method

.method static synthetic access$000(Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->doWork()V

    return-void
.end method

.method static checkFirstTimeCall(Ljava/util/concurrent/atomic/AtomicBoolean;)Z
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result p0

    return p0
.end method

.method private clearFields()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->mIService:Ljava/lang/Object;

    iput-object v0, p0, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->mFuture:Lcom/xiaomi/accountsdk/futureservice/ClientFuture;

    return-void
.end method

.method private doWork()V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->callServiceWork()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->mFuture:Lcom/xiaomi/accountsdk/futureservice/ClientFuture;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->mFuture:Lcom/xiaomi/accountsdk/futureservice/ClientFuture;

    invoke-virtual {v1, v0}, Lcom/xiaomi/accountsdk/futureservice/ClientFuture;->setServerData(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->mFuture:Lcom/xiaomi/accountsdk/futureservice/ClientFuture;

    if-eqz v1, :cond_0

    iget-object p0, p0, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->mFuture:Lcom/xiaomi/accountsdk/futureservice/ClientFuture;

    invoke-virtual {p0, v0}, Lcom/xiaomi/accountsdk/futureservice/ClientFuture;->setServerSideThrowable(Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    return-void
.end method


# virtual methods
.method public final bind()Z
    .locals 3

    iget-object v0, p0, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->bindFlag:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-static {v0}, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->checkFirstTimeCall(Ljava/util/concurrent/atomic/AtomicBoolean;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->mActionName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->mServicePackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->mContext:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, p0, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v1, Landroid/os/RemoteException;

    const-string v2, "failed to bind to service"

    invoke-direct {v1, v2}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->mFuture:Lcom/xiaomi/accountsdk/futureservice/ClientFuture;

    invoke-virtual {v2, v1}, Lcom/xiaomi/accountsdk/futureservice/ClientFuture;->setServerSideThrowable(Ljava/lang/Throwable;)V

    invoke-virtual {p0}, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->unbind()V

    :cond_0
    return v0

    :cond_1
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "should only bind for one time"

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method protected abstract binderToServiceType(Landroid/os/IBinder;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/IBinder;",
            ")TIServiceType;"
        }
    .end annotation
.end method

.method protected abstract callServiceWork()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TServerDataType;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method protected final getIService()Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TIServiceType;"
        }
    .end annotation

    iget-object p0, p0, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->mIService:Ljava/lang/Object;

    return-object p0
.end method

.method public onBindingDied(Landroid/content/ComponentName;)V
    .locals 1

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onBindingDied>>>name:"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "ServerServiceConnector"

    invoke-static {p1, p0}, Lcom/xiaomi/accountsdk/utils/AccountLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onNullBinding(Landroid/content/ComponentName;)V
    .locals 1

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onNullBinding>>>name:"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "ServerServiceConnector"

    invoke-static {p1, p0}, Lcom/xiaomi/accountsdk/utils/AccountLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 0

    invoke-virtual {p0, p2}, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->binderToServiceType(Landroid/os/IBinder;)Ljava/lang/Object;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->mIService:Ljava/lang/Object;

    sget-object p1, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    new-instance p2, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector$1;

    invoke-direct {p2, p0}, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector$1;-><init>(Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;)V

    invoke-interface {p1, p2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0

    invoke-direct {p0}, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->clearFields()V

    return-void
.end method

.method final unbind()V
    .locals 1

    goto/32 :goto_8

    nop

    :goto_0
    invoke-direct {p0}, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->clearFields()V

    goto/32 :goto_9

    nop

    :goto_1
    invoke-static {v0}, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->checkFirstTimeCall(Ljava/util/concurrent/atomic/AtomicBoolean;)Z

    move-result v0

    goto/32 :goto_5

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_6

    nop

    :goto_3
    return-void

    :goto_4
    goto/32 :goto_a

    nop

    :goto_5
    if-eqz v0, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_3

    nop

    :goto_6
    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    :goto_7
    goto/32 :goto_0

    nop

    :goto_8
    iget-object v0, p0, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->unbindFlag:Ljava/util/concurrent/atomic/AtomicBoolean;

    goto/32 :goto_1

    nop

    :goto_9
    return-void

    :goto_a
    iget-object v0, p0, Lcom/xiaomi/accountsdk/futureservice/ServerServiceConnector;->mContext:Landroid/content/Context;

    goto/32 :goto_2

    nop
.end method
