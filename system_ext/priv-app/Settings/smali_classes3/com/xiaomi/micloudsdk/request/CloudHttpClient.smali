.class public Lcom/xiaomi/micloudsdk/request/CloudHttpClient;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/apache/http/client/HttpClient;


# instance fields
.field private mProxy:Lorg/apache/http/client/HttpClient;


# direct methods
.method protected constructor <init>(Lorg/apache/http/client/HttpClient;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/micloudsdk/request/CloudHttpClient;->mProxy:Lorg/apache/http/client/HttpClient;

    return-void
.end method

.method protected static initClient()Lorg/apache/http/impl/client/DefaultHttpClient;
    .locals 3

    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    invoke-virtual {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v1

    invoke-static {}, Lcom/xiaomi/micloudsdk/request/utils/RequestContext;->getUserAgent()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lorg/apache/http/params/HttpProtocolParams;->setUserAgent(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    return-object v0
.end method

.method public static newInstance()Lcom/xiaomi/micloudsdk/request/CloudHttpClient;
    .locals 2

    new-instance v0, Lcom/xiaomi/micloudsdk/request/CloudHttpClient;

    invoke-static {}, Lcom/xiaomi/micloudsdk/request/CloudHttpClient;->initClient()Lorg/apache/http/impl/client/DefaultHttpClient;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/xiaomi/micloudsdk/request/CloudHttpClient;-><init>(Lorg/apache/http/client/HttpClient;)V

    return-object v0
.end method
