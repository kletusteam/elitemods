.class Lcom/miui/maml/elements/MusicControlScreenElement$AlbumInfo;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/maml/elements/MusicControlScreenElement;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AlbumInfo"
.end annotation


# instance fields
.field album:Ljava/lang/String;

.field artist:Ljava/lang/String;

.field title:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/maml/elements/MusicControlScreenElement$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/maml/elements/MusicControlScreenElement$AlbumInfo;-><init>()V

    return-void
.end method


# virtual methods
.method update(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    goto/32 :goto_b

    nop

    :goto_0
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto/32 :goto_1a

    nop

    :goto_1
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto/32 :goto_d

    nop

    :goto_2
    iput-object p1, p0, Lcom/miui/maml/elements/MusicControlScreenElement$AlbumInfo;->title:Ljava/lang/String;

    goto/32 :goto_14

    nop

    :goto_3
    iput-object p3, p0, Lcom/miui/maml/elements/MusicControlScreenElement$AlbumInfo;->album:Ljava/lang/String;

    :goto_4
    goto/32 :goto_19

    nop

    :goto_5
    if-eqz v0, :cond_0

    goto/32 :goto_18

    :cond_0
    goto/32 :goto_17

    nop

    :goto_6
    if-nez p3, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_9

    nop

    :goto_7
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    :goto_8
    goto/32 :goto_1d

    nop

    :goto_9
    invoke-virtual {p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p3

    :goto_a
    goto/32 :goto_12

    nop

    :goto_b
    if-nez p1, :cond_2

    goto/32 :goto_8

    :cond_2
    goto/32 :goto_7

    nop

    :goto_c
    const/4 v0, 0x0

    goto/32 :goto_15

    nop

    :goto_d
    if-nez v0, :cond_3

    goto/32 :goto_16

    :cond_3
    goto/32 :goto_13

    nop

    :goto_e
    invoke-static {p3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto/32 :goto_5

    nop

    :goto_f
    const/4 v0, 0x1

    :goto_10
    goto/32 :goto_1e

    nop

    :goto_11
    iget-object v0, p0, Lcom/miui/maml/elements/MusicControlScreenElement$AlbumInfo;->album:Ljava/lang/String;

    goto/32 :goto_e

    nop

    :goto_12
    iget-object v0, p0, Lcom/miui/maml/elements/MusicControlScreenElement$AlbumInfo;->title:Ljava/lang/String;

    goto/32 :goto_1

    nop

    :goto_13
    iget-object v0, p0, Lcom/miui/maml/elements/MusicControlScreenElement$AlbumInfo;->artist:Ljava/lang/String;

    goto/32 :goto_0

    nop

    :goto_14
    iput-object p2, p0, Lcom/miui/maml/elements/MusicControlScreenElement$AlbumInfo;->artist:Ljava/lang/String;

    goto/32 :goto_3

    nop

    :goto_15
    goto :goto_10

    :goto_16
    goto/32 :goto_f

    nop

    :goto_17
    goto :goto_16

    :goto_18
    goto/32 :goto_c

    nop

    :goto_19
    return v0

    :goto_1a
    if-nez v0, :cond_4

    goto/32 :goto_16

    :cond_4
    goto/32 :goto_11

    nop

    :goto_1b
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p2

    :goto_1c
    goto/32 :goto_6

    nop

    :goto_1d
    if-nez p2, :cond_5

    goto/32 :goto_1c

    :cond_5
    goto/32 :goto_1b

    nop

    :goto_1e
    if-nez v0, :cond_6

    goto/32 :goto_4

    :cond_6
    goto/32 :goto_2

    nop
.end method
