.class public Lcom/miui/maml/data/FunctionsLoader;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static load()V
    .locals 0

    invoke-static {}, Lcom/miui/maml/data/BaseFunctions;->load()V

    invoke-static {}, Lcom/miui/maml/data/StringFunctions;->load()V

    invoke-static {}, Lcom/miui/maml/data/FormatFunctions;->load()V

    invoke-static {}, Lcom/miui/maml/data/DateFunctions;->load()V

    invoke-static {}, Lcom/miui/maml/data/JsonFunctions;->load()V

    return-void
.end method
