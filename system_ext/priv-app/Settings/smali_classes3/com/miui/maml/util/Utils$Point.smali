.class public Lcom/miui/maml/util/Utils$Point;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/maml/util/Utils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Point"
.end annotation


# instance fields
.field public x:D

.field public y:D


# direct methods
.method public constructor <init>(DD)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/miui/maml/util/Utils$Point;->x:D

    iput-wide p3, p0, Lcom/miui/maml/util/Utils$Point;->y:D

    return-void
.end method


# virtual methods
.method public Offset(Lcom/miui/maml/util/Utils$Point;)V
    .locals 4

    iget-wide v0, p0, Lcom/miui/maml/util/Utils$Point;->x:D

    iget-wide v2, p1, Lcom/miui/maml/util/Utils$Point;->x:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Lcom/miui/maml/util/Utils$Point;->x:D

    iget-wide v0, p0, Lcom/miui/maml/util/Utils$Point;->y:D

    iget-wide v2, p1, Lcom/miui/maml/util/Utils$Point;->y:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Lcom/miui/maml/util/Utils$Point;->y:D

    return-void
.end method

.method minus(Lcom/miui/maml/util/Utils$Point;)Lcom/miui/maml/util/Utils$Point;
    .locals 5

    goto/32 :goto_2

    nop

    :goto_0
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/miui/maml/util/Utils$Point;-><init>(DD)V

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0

    :goto_2
    new-instance v0, Lcom/miui/maml/util/Utils$Point;

    goto/32 :goto_5

    nop

    :goto_3
    iget-wide v3, p1, Lcom/miui/maml/util/Utils$Point;->x:D

    goto/32 :goto_6

    nop

    :goto_4
    sub-double/2addr v3, p0

    goto/32 :goto_0

    nop

    :goto_5
    iget-wide v1, p0, Lcom/miui/maml/util/Utils$Point;->x:D

    goto/32 :goto_3

    nop

    :goto_6
    sub-double/2addr v1, v3

    goto/32 :goto_8

    nop

    :goto_7
    iget-wide p0, p1, Lcom/miui/maml/util/Utils$Point;->y:D

    goto/32 :goto_4

    nop

    :goto_8
    iget-wide v3, p0, Lcom/miui/maml/util/Utils$Point;->y:D

    goto/32 :goto_7

    nop
.end method
