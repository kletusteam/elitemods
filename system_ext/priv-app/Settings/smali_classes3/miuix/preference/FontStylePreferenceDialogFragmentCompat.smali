.class public Lmiuix/preference/FontStylePreferenceDialogFragmentCompat;
.super Landroidx/preference/FontStylePreferenceDialogFragmentCompat;


# instance fields
.field private final mDelegate:Lmiuix/preference/PreferenceDialogFragmentCompatDelegate;

.field private mImpl:Lmiuix/preference/IPreferenceDialogFragment;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;-><init>()V

    new-instance v0, Lmiuix/preference/FontStylePreferenceDialogFragmentCompat$1;

    invoke-direct {v0, p0}, Lmiuix/preference/FontStylePreferenceDialogFragmentCompat$1;-><init>(Lmiuix/preference/FontStylePreferenceDialogFragmentCompat;)V

    iput-object v0, p0, Lmiuix/preference/FontStylePreferenceDialogFragmentCompat;->mImpl:Lmiuix/preference/IPreferenceDialogFragment;

    new-instance v0, Lmiuix/preference/PreferenceDialogFragmentCompatDelegate;

    iget-object v1, p0, Lmiuix/preference/FontStylePreferenceDialogFragmentCompat;->mImpl:Lmiuix/preference/IPreferenceDialogFragment;

    invoke-direct {v0, v1, p0}, Lmiuix/preference/PreferenceDialogFragmentCompatDelegate;-><init>(Lmiuix/preference/IPreferenceDialogFragment;Landroidx/preference/PreferenceDialogFragmentCompat;)V

    iput-object v0, p0, Lmiuix/preference/FontStylePreferenceDialogFragmentCompat;->mDelegate:Lmiuix/preference/PreferenceDialogFragmentCompatDelegate;

    return-void
.end method

.method static synthetic access$000(Lmiuix/preference/FontStylePreferenceDialogFragmentCompat;Landroid/view/View;)V
    .locals 0

    invoke-virtual {p0, p1}, Lmiuix/preference/FontStylePreferenceDialogFragmentCompat;->onBindDialogView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$100(Lmiuix/preference/FontStylePreferenceDialogFragmentCompat;Landroid/content/Context;)Landroid/view/View;
    .locals 1

    invoke-virtual {p0, p1}, Lmiuix/preference/FontStylePreferenceDialogFragmentCompat;->onCreateDialogView(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;)Lmiuix/preference/FontStylePreferenceDialogFragmentCompat;
    .locals 3

    new-instance v1, Lmiuix/preference/FontStylePreferenceDialogFragmentCompat;

    invoke-direct {v1}, Lmiuix/preference/FontStylePreferenceDialogFragmentCompat;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Landroid/os/Bundle;-><init>(I)V

    const-string v2, "key"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lmiuix/preference/FontStylePreferenceDialogFragmentCompat;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1

    iget-object v0, p0, Lmiuix/preference/FontStylePreferenceDialogFragmentCompat;->mDelegate:Lmiuix/preference/PreferenceDialogFragmentCompatDelegate;

    invoke-virtual {v0, p1}, Lmiuix/preference/PreferenceDialogFragmentCompatDelegate;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onPrepareDialogBuilder(Lmiuix/appcompat/app/AlertDialog$Builder;)V
    .locals 2

    new-instance v0, Lmiuix/preference/BuilderDelegate;

    invoke-virtual {p0}, Lmiuix/preference/FontStylePreferenceDialogFragmentCompat;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lmiuix/preference/BuilderDelegate;-><init>(Landroid/content/Context;Lmiuix/appcompat/app/AlertDialog$Builder;)V

    invoke-super {p0, v0}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->onPrepareDialogBuilder(Landroidx/appcompat/app/AlertDialog$Builder;)V

    return-void
.end method
