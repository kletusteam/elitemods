.class final enum Lmiui/io/ResettableInputStream$Type;
.super Ljava/lang/Enum;
.source "ResettableInputStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/io/ResettableInputStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lmiui/io/ResettableInputStream$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lmiui/io/ResettableInputStream$Type;

.field public static final enum Asset:Lmiui/io/ResettableInputStream$Type;

.field public static final enum ByteArray:Lmiui/io/ResettableInputStream$Type;

.field public static final enum File:Lmiui/io/ResettableInputStream$Type;

.field public static final enum Uri:Lmiui/io/ResettableInputStream$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    new-instance v0, Lmiui/io/ResettableInputStream$Type;

    const-string v1, "File"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lmiui/io/ResettableInputStream$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmiui/io/ResettableInputStream$Type;->File:Lmiui/io/ResettableInputStream$Type;

    new-instance v1, Lmiui/io/ResettableInputStream$Type;

    const-string v3, "Uri"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lmiui/io/ResettableInputStream$Type;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lmiui/io/ResettableInputStream$Type;->Uri:Lmiui/io/ResettableInputStream$Type;

    new-instance v3, Lmiui/io/ResettableInputStream$Type;

    const-string v5, "Asset"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lmiui/io/ResettableInputStream$Type;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lmiui/io/ResettableInputStream$Type;->Asset:Lmiui/io/ResettableInputStream$Type;

    new-instance v5, Lmiui/io/ResettableInputStream$Type;

    const-string v7, "ByteArray"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lmiui/io/ResettableInputStream$Type;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lmiui/io/ResettableInputStream$Type;->ByteArray:Lmiui/io/ResettableInputStream$Type;

    const/4 v7, 0x4

    new-array v7, v7, [Lmiui/io/ResettableInputStream$Type;

    aput-object v0, v7, v2

    aput-object v1, v7, v4

    aput-object v3, v7, v6

    aput-object v5, v7, v8

    sput-object v7, Lmiui/io/ResettableInputStream$Type;->$VALUES:[Lmiui/io/ResettableInputStream$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmiui/io/ResettableInputStream$Type;
    .locals 1

    const-class v0, Lmiui/io/ResettableInputStream$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmiui/io/ResettableInputStream$Type;

    return-object v0
.end method

.method public static values()[Lmiui/io/ResettableInputStream$Type;
    .locals 1

    sget-object v0, Lmiui/io/ResettableInputStream$Type;->$VALUES:[Lmiui/io/ResettableInputStream$Type;

    invoke-virtual {v0}, [Lmiui/io/ResettableInputStream$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmiui/io/ResettableInputStream$Type;

    return-object v0
.end method
