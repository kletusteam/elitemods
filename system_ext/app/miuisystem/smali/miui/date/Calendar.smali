.class public Lmiui/date/Calendar;
.super Ljava/lang/Object;
.source "Calendar.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;",
        "Ljava/lang/Comparable<",
        "Lmiui/date/Calendar;",
        ">;"
    }
.end annotation


# static fields
.field public static final AD:I = 0x1

.field public static final AFTERNOON:I = 0x4

.field public static final AM:I = 0x0

.field public static final AM_PM:I = 0x11

.field public static final APRIL:I = 0x3

.field public static final AUGUST:I = 0x7

.field public static final AUTUMN_BEGINS:I = 0xf

.field public static final AUTUMN_EQUINOX:I = 0x12

.field public static final BC:I = 0x0

.field public static final CHICKEN:I = 0x9

.field public static final CHINESE_ERA_DAY:I = 0xb

.field public static final CHINESE_ERA_HOUR:I = 0x13

.field public static final CHINESE_ERA_MONTH:I = 0x7

.field public static final CHINESE_ERA_YEAR:I = 0x4

.field public static final CHINESE_MONTH:I = 0x6

.field public static final CHINESE_MONTH_IS_LEAP:I = 0x8

.field public static final CHINESE_YEAR:I = 0x2

.field private static final CHINESE_YEAR_INFO:[I

.field public static final CHINESE_YEAR_SYMBOL_ANIMAL:I = 0x3

.field public static final CLEAR_AND_BRIGHT:I = 0x7

.field public static final COLD_DEWS:I = 0x13

.field public static final COW:I = 0x1

.field private static final DAYS_19000131_TO_19700101:I = -0x63c1

.field private static final DAYS_IN_MONTH:[B

.field private static final DAYS_IN_YEAR:[I

.field private static final DAY_CHINESE_YEAR_FROM_19000101:[I

.field public static final DAY_OF_CHINESE_MONTH:I = 0xa

.field public static final DAY_OF_CHINESE_YEAR:I = 0xd

.field public static final DAY_OF_MONTH:I = 0x9

.field public static final DAY_OF_WEEK:I = 0xe

.field public static final DAY_OF_YEAR:I = 0xc

.field public static final DECEMBER:I = 0xb

.field public static final DETAIL_AM_PM:I = 0x10

.field public static final DOG:I = 0xa

.field public static final DRAGON:I = 0x4

.field public static final DST_OFFSET:I = 0x18

.field public static final EARLY_MORNING:I = 0x1

.field public static final ERA:I = 0x0

.field public static final EVENING:I = 0x5

.field public static final FEBRUARY:I = 0x1

.field public static final FIELD_COUNT:I = 0x19

.field private static final FIELD_NAMES:[Ljava/lang/String;

.field private static final FORMAT_CHARACTERS:[I

.field public static final FRIDAY:I = 0x6

.field public static final GRAIN_BUDS:I = 0xa

.field public static final GRAIN_IN_EAR:I = 0xb

.field public static final GRAIN_RAIN:I = 0x8

.field public static final GREAT_COLD:I = 0x2

.field public static final GREAT_HEAT:I = 0xe

.field public static final HEAVY_SNOW:I = 0x17

.field public static final HOAR_FROST_FALLS:I = 0x14

.field public static final HORSE:I = 0x6

.field public static final HOUR:I = 0x12

.field public static final INSECTS_AWAKEN:I = 0x5

.field public static final IS_CHINESE_LEAP_MONTH:I = 0x1

.field public static final JANUARY:I = 0x0

.field public static final JULY:I = 0x6

.field public static final JUNE:I = 0x5

.field public static final LIGHT_SNOW:I = 0x16

.field public static final MARCH:I = 0x2

.field private static final MAX_CHINESE_CALENDAR_MILLISECOND:J = 0x3c314a71400L

.field private static final MAX_CHINESE_YEAR:I = 0x834

.field public static final MAY:I = 0x4

.field public static final MIDNIGHT:I = 0x0

.field public static final MILLISECOND:I = 0x16

.field public static final MILLISECOND_OF_DAY:I = 0x5265c00

.field public static final MILLISECOND_OF_HOUR:I = 0x36ee80

.field public static final MILLISECOND_OF_MINUTE:I = 0xea60

.field public static final MILLISECOND_OF_SECOND:I = 0x3e8

.field public static final MINUTE:I = 0x14

.field private static final MIN_CHINESE_CALENDAR_MILLISECOND:J = -0x201b77f5c00L

.field private static final MIN_CHINESE_YEAR:I = 0x76c

.field public static final MONDAY:I = 0x2

.field public static final MONKEY:I = 0x8

.field public static final MONTH:I = 0x5

.field public static final MORNING:I = 0x2

.field public static final MOUSE:I = 0x0

.field public static final NIGHT:I = 0x6

.field public static final NOON:I = 0x3

.field public static final NOT_CHINESE_LEAP_MONTH:I = 0x0

.field public static final NOVEMBER:I = 0xa

.field public static final NO_SOLAR_TERM:I = 0x0

.field public static final OCTOBER:I = 0x9

.field public static final PIG:I = 0xb

.field public static final PM:I = 0x1

.field public static final RABBIT:I = 0x3

.field public static final SATURDAY:I = 0x7

.field public static final SECOND:I = 0x15

.field public static final SEPTEMBER:I = 0x8

.field public static final SHEEP:I = 0x7

.field public static final SLIGHT_COLD:I = 0x1

.field public static final SLIGHT_HEAT:I = 0xd

.field public static final SNAKE:I = 0x5

.field public static final SOLAR_TERM:I = 0xf

.field private static final SOLAR_TERM_BASE:[I

.field private static final SOLAR_TERM_INDEX:[B

.field private static final SOLAR_TERM_OS:[B

.field public static final SPRING_BEGINS:I = 0x3

.field public static final STOPPING_THE_HEAT:I = 0x10

.field public static final SUMMER_BEGINS:I = 0x9

.field public static final SUMMER_SOLSTICE:I = 0xc

.field public static final SUNDAY:I = 0x1

.field public static final THE_RAINS:I = 0x4

.field public static final THURSDAY:I = 0x5

.field public static final TIGER:I = 0x2

.field public static final TUESDAY:I = 0x3

.field public static final VERNAL_EQUINOX:I = 0x6

.field public static final WEDNESDAY:I = 0x4

.field public static final WHITE_DEWS:I = 0x11

.field public static final WINTER_BEGINS:I = 0x15

.field public static final WINTER_SOLSTICE:I = 0x18

.field public static final YEAR:I = 0x1

.field public static final ZONE_OFFSET:I = 0x17

.field private static final defaultGregorianCutover:J = -0xb1d069b5400L

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private transient changeYear:I

.field private transient currentYearSkew:I

.field private transient gregorianCutover:J

.field private transient julianError:I

.field private transient julianSkew:I

.field private transient lastYearSkew:I

.field private mFields:[I

.field private mMillisecond:J

.field private mTimeZone:Ljava/util/TimeZone;


# direct methods
.method static constructor <clinit>()V
    .locals 26

    const/16 v0, 0xc

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lmiui/date/Calendar;->DAYS_IN_MONTH:[B

    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lmiui/date/Calendar;->DAYS_IN_YEAR:[I

    const-string v1, "ERA"

    const-string v2, "YEAR"

    const-string v3, "CHINESE_YEAR"

    const-string v4, "CHINESE_YEAR_SYMBOL_ANIMAL"

    const-string v5, "CHINESE_ERA_YEAR"

    const-string v6, "MONTH"

    const-string v7, "CHINESE_MONTH"

    const-string v8, "CHINESE_ERA_MONTH"

    const-string v9, "CHINESE_MONTH_IS_LEAP"

    const-string v10, "DAY_OF_MONTH"

    const-string v11, "DAY_OF_CHINESE_MONTH"

    const-string v12, "CHINESE_ERA_DAY"

    const-string v13, "DAY_OF_YEAR"

    const-string v14, "DAY_OF_CHINESE_YEAR"

    const-string v15, "DAY_OF_WEEK"

    const-string v16, "SOLAR_TERM"

    const-string v17, "DETAIL_AM_PM"

    const-string v18, "AM_PM"

    const-string v19, "HOUR"

    const-string v20, "CHINESE_ERA_HOUR"

    const-string v21, "MINUTE"

    const-string v22, "SECOND"

    const-string v23, "MILLISECOND"

    const-string v24, "ZONE_OFFSET"

    const-string v25, "DST_OFFSET"

    filled-new-array/range {v1 .. v25}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lmiui/date/Calendar;->FIELD_NAMES:[Ljava/lang/String;

    const/16 v0, 0x3a

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lmiui/date/Calendar;->FORMAT_CHARACTERS:[I

    const/16 v0, 0xca

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lmiui/date/Calendar;->DAY_CHINESE_YEAR_FROM_19000101:[I

    const/16 v0, 0xc9

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    sput-object v0, Lmiui/date/Calendar;->CHINESE_YEAR_INFO:[I

    const/16 v0, 0x18

    new-array v0, v0, [I

    fill-array-data v0, :array_5

    sput-object v0, Lmiui/date/Calendar;->SOLAR_TERM_BASE:[I

    nop

    const-string v0, "0123415341536789:;<9:=<>:=1>?012@015@015@015AB78CDE8CD=1FD01GH01GH01IH01IJ0KLMN;LMBEOPDQRST0RUH0RVH0RWH0RWM0XYMNZ[MB\\]PT^_ST`_WH`_WH`_WM`_WM`aYMbc[Mde]Sfe]gfh_gih_Wih_WjhaWjka[jkl[jmn]ope]qph_qrh_sth_W"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lmiui/date/Calendar;->SOLAR_TERM_INDEX:[B

    nop

    const-string v0, "211122112122112121222211221122122222212222222221222122222232222222222222222233223232223232222222322222112122112121222211222122222222222222222222322222112122112121222111211122122222212221222221221122122222222222222222222223222232222232222222222222112122112121122111211122122122212221222221221122122222222222222221211122112122212221222211222122222232222232222222222222112122112121111111222222112121112121111111222222111121112121111111211122112122112121122111222212111121111121111111111122112122112121122111211122112122212221222221222211111121111121111111222111111121111111111111111122112121112121111111222111111111111111111111111122111121112121111111221122122222212221222221222111011111111111111111111122111121111121111111211122112122112121122211221111011111101111111111111112111121111121111111211122112122112221222211221111011111101111111110111111111121111111111111111122112121112121122111111011111121111111111111111011111111112111111111111011111111111111111111221111011111101110111110111011011111111111111111221111011011101110111110111011011111101111111111211111001011101110111110110011011111101111111111211111001011001010111110110011011111101111111110211111001011001010111100110011011011101110111110211111001011001010011100110011001011101110111110211111001010001010011000100011001011001010111110111111001010001010011000111111111111111111111111100011001011001010111100111111001010001010000000111111000010000010000000100011001011001010011100110011001011001110111110100011001010001010011000110011001011001010111110111100000010000000000000000011001010001010011000111100000000000000000000000011001010001010000000111000000000000000000000000011001010000010000000"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lmiui/date/Calendar;->SOLAR_TERM_OS:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x1ft
        0x1ct
        0x1ft
        0x1et
        0x1ft
        0x1et
        0x1ft
        0x1ft
        0x1et
        0x1ft
        0x1et
        0x1ft
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x1f
        0x3b
        0x5a
        0x78
        0x97
        0xb5
        0xd4
        0xf3
        0x111
        0x130
        0x14e
        0x16d
    .end array-data

    :array_2
    .array-data 4
        0x3
        -0x1
        -0x1
        0xc
        0xe
        0x1a
        0x0
        0x12
        0x13
        -0x1
        0x12
        0x5
        0x5
        0x7
        -0x1
        -0x1
        -0x1
        -0x1
        0x16
        -0x1
        -0x1
        -0x1
        0x1a
        -0x1
        0x4
        0x19
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        0x10
        -0x1
        0xe
        0x9
        0x7
        -0x1
        -0x1
        0x12
        -0x1
        -0x1
        0x12
        -0x1
        0x14
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        0x15
        0xf
        -0x1
        -0x1
        0x1a
        -0x1
        0x1
        0x19
    .end array-data

    :array_3
    .array-data 4
        0x0
        0x180
        0x2e2
        0x445
        0x5c4
        0x726
        0x889
        0xa09
        0xb6b
        0xcce
        0xe4e
        0xfb0
        0x1130
        0x1292
        0x13f4
        0x1574
        0x16d6
        0x1839
        0x19b9
        0x1b1c
        0x1c9c
        0x1dfe
        0x1f60
        0x20e0
        0x2242
        0x23a4
        0x2525
        0x2687
        0x27ea
        0x296a
        0x2acc
        0x2c4b
        0x2dad
        0x2f10
        0x3090
        0x31f3
        0x3355
        0x34d5
        0x3637
        0x37b7
        0x3919
        0x3a7b
        0x3bfb
        0x3d5e
        0x3ec0
        0x4041
        0x41a3
        0x4305
        0x4485
        0x45e7
        0x4767
        0x48c9
        0x4a2c
        0x4bac
        0x4d0e
        0x4e71
        0x4ff1
        0x5153
        0x52d2
        0x5435
        0x5597
        0x5717
        0x587a
        0x59dc
        0x5b5c
        0x5cbf
        0x5e20
        0x5fa0
        0x6103
        0x6283
        0x63e5
        0x6548
        0x66c8
        0x682a
        0x698c
        0x6b0c
        0x6c6e
        0x6dee
        0x6f50
        0x70b3
        0x7233
        0x7396
        0x74f8
        0x7678
        0x77da
        0x795a
        0x7abc
        0x7c1e
        0x7d9e
        0x7f01
        0x8064
        0x81e4
        0x8346
        0x84a8
        0x8627
        0x878a
        0x890a
        0x8a6c
        0x8bcf
        0x8d4f
        0x8eb1
        0x9013
        0x9193
        0x92f5
        0x9458
        0x95d8
        0x973a
        0x98bb
        0x9a1d
        0x9b7f
        0x9cff
        0x9e61
        0x9fc3
        0xa143
        0xa2a6
        0xa426
        0xa588
        0xa6eb
        0xa86b
        0xa9cd
        0xab2f
        0xacaf
        0xae11
        0xaf74
        0xb0f4
        0xb256
        0xb3d6
        0xb538
        0xb69a
        0xb81a
        0xb97d
        0xbadf
        0xbc5f
        0xbdc2
        0xbf42
        0xc0a4
        0xc206
        0xc386
        0xc4e8
        0xc64a
        0xc7ca
        0xc92d
        0xca90
        0xcc10
        0xcd72
        0xcef2
        0xd054
        0xd1b6
        0xd336
        0xd498
        0xd5fb
        0xd77b
        0xd8de
        0xda5e
        0xdbc0
        0xdd22
        0xdea1
        0xe004
        0xe166
        0xe2e6
        0xe449
        0xe5ab
        0xe72b
        0xe88d
        0xea0d
        0xeb6f
        0xecd2
        0xee52
        0xefb4
        0xf117
        0xf297
        0xf3f9
        0xf579
        0xf6db
        0xf83d
        0xf9bd
        0xfb20
        0xfc82
        0xfe02
        0xff65
        0x100c7
        0x10247
        0x103a9
        0x10529
        0x1068b
        0x107ee
        0x1096e
        0x10ad0
        0x10c33
        0x10db2
        0x10f14
        0x11094
        0x111f6
        0x11359
        0x114d9
        0x1163c
        0x1179e
        0x1191e
        0x11a80
        0x11be2
        0x11d62
        0x11ec4
    .end array-data

    :array_4
    .array-data 4
        0x4bd8
        0x4ae0
        0xa570
        0x54d5
        0xd260
        0xd950
        0x5554
        0x56af
        0x9ad0
        0x55d2
        0x4ae0
        0xa5b6
        0xa4d0
        0xd250
        0xd295
        0xb54f
        0xd6a0
        0xada2
        0x95b0
        0x4977
        0x497f
        0xa4b0
        0xb4b5
        0x6a50
        0x6d40
        0xab54
        0x2b6f
        0x9570
        0x52f2
        0x4970
        0x6566
        0xd4a0
        0xea50
        0x6a95
        0x5adf
        0x2b60
        0x86e3
        0x92ef
        0xc8d7
        0xc95f
        0xd4a0
        0xd8a6
        0xb55f
        0x56a0
        0xa5b4
        0x25df
        0x92d0
        0xd2b2
        0xa950
        0xb557
        0x6ca0
        0xb550
        0x5355
        0x4daf
        0xa5b0
        0x4573
        0x52bf
        0xa9a8
        0xe950
        0x6aa0
        0xaea6
        0xab50
        0x4b60
        0xaae4
        0xa570
        0x5260
        0xf263
        0xd950
        0x5b57
        0x56a0
        0x96d0
        0x4dd5
        0x4ad0
        0xa4d0
        0xd4d4
        0xd250
        0xd558
        0xb540
        0xb6a0
        0x95a6
        0x95bf
        0x49b0
        0xa974
        0xa4b0
        0xb27a
        0x6a50
        0x6d40
        0xaf46
        0xab60
        0x9570
        0x4af5
        0x4970
        0x64b0
        0x74a3
        0xea50
        0x6b58
        0x5ac0
        0xab60
        0x96d5
        0x92e0
        0xc960
        0xd954
        0xd4a0
        0xda50
        0x7552
        0x56a0
        0xabb7
        0x25d0
        0x92d0
        0xcab5
        0xa950
        0xb4a0
        0xbaa4
        0xad50
        0x55d9
        0x4ba0
        0xa5b0
        0x5176
        0x52bf
        0xa930
        0x7954
        0x6aa0
        0xad50
        0x5b52
        0x4b60
        0xa6e6
        0xa4e0
        0xd260
        0xea65
        0xd530
        0x5aa0
        0x76a3
        0x96d0
        0x4afb
        0x4ad0
        0xa4d0
        0xd0b6
        0xd25f
        0xd520
        0xdd45
        0xb5a0
        0x56d0
        0x55b2
        0x49b0
        0xa577
        0xa4b0
        0xaa50
        0xb255
        0x6d2f
        0xada0
        0x4b63
        0x937f
        0x49f8
        0x4970
        0x64b0
        0x68a6
        0xea5f
        0x6b20
        0xa6c4
        0xaaef
        0x92e0
        0xd2e3
        0xc960
        0xd557
        0xd4a0
        0xda50
        0x5d55
        0x56a0
        0xa6d0
        0x55d4
        0x52d0
        0xa9b8
        0xa950
        0xb4a0
        0xb6a6
        0xad50
        0x55a0
        0xaba4
        0xa5b0
        0x52b0
        0xb273
        0x6930
        0x7337
        0x6aa0
        0xad50
        0x4b55
        0x4b6f
        0xa570
        0x54e4
        0xd260
        0xe968
        0xd520
        0xdaa0
        0x6aa6
        0x56df
        0x4ae0
        0xa9d4
        0xa4d0
        0xd150
        0xf252
        0xd520
    .end array-data

    :array_5
    .array-data 4
        0x4
        0x13
        0x3
        0x12
        0x4
        0x13
        0x4
        0x13
        0x4
        0x14
        0x4
        0x14
        0x6
        0x16
        0x6
        0x16
        0x6
        0x16
        0x7
        0x16
        0x6
        0x15
        0x6
        0x15
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lmiui/date/Calendar;-><init>(Ljava/util/TimeZone;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/TimeZone;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x19

    new-array v0, v0, [I

    iput-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    const-wide v0, -0xb1d069b5400L

    iput-wide v0, p0, Lmiui/date/Calendar;->gregorianCutover:J

    const/16 v0, 0x62e

    iput v0, p0, Lmiui/date/Calendar;->changeYear:I

    const/16 v1, 0x62e

    div-int/lit8 v1, v1, 0x64

    const/16 v2, 0x62e

    div-int/lit16 v2, v2, 0x190

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x2

    iput v1, p0, Lmiui/date/Calendar;->julianError:I

    const/16 v2, 0x7d0

    rsub-int v3, v2, 0x62e

    div-int/lit16 v3, v3, 0x190

    add-int/2addr v3, v1

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x64

    sub-int/2addr v3, v0

    iput v3, p0, Lmiui/date/Calendar;->julianSkew:I

    const/16 v0, 0xa

    iput v0, p0, Lmiui/date/Calendar;->currentYearSkew:I

    const/4 v0, 0x0

    iput v0, p0, Lmiui/date/Calendar;->lastYearSkew:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lmiui/date/Calendar;->mMillisecond:J

    invoke-virtual {p0, p1}, Lmiui/date/Calendar;->setTimeZone(Ljava/util/TimeZone;)Lmiui/date/Calendar;

    return-void
.end method

.method private appendNumericTimeZone(Ljava/lang/StringBuilder;ZZ)V
    .locals 5

    const/16 v0, 0x17

    invoke-virtual {p0, v0}, Lmiui/date/Calendar;->get(I)I

    move-result v0

    const/16 v1, 0x18

    invoke-virtual {p0, v1}, Lmiui/date/Calendar;->get(I)I

    move-result v1

    add-int/2addr v0, v1

    const/16 v1, 0x2b

    if-gez v0, :cond_0

    const/16 v1, 0x2d

    neg-int v0, v0

    :cond_0
    if-eqz p2, :cond_1

    const-string v2, "GMT"

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const v2, 0x36ee80

    div-int v3, v0, v2

    const/4 v4, 0x2

    invoke-static {p1, v4, v3}, Lmiui/date/Calendar;->appendNumericValue(Ljava/lang/StringBuilder;II)V

    if-eqz p3, :cond_2

    const/16 v3, 0x3a

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_2
    rem-int v2, v0, v2

    const v3, 0xea60

    div-int/2addr v2, v3

    invoke-static {p1, v4, v2}, Lmiui/date/Calendar;->appendNumericValue(Ljava/lang/StringBuilder;II)V

    return-void
.end method

.method private static appendNumericValue(Ljava/lang/StringBuilder;II)V
    .locals 1

    invoke-static {p1, p2}, Lmiui/util/SimpleNumberFormatter;->format(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method private appendTimeZone(Ljava/lang/StringBuilder;Lmiui/date/CalendarFormatSymbols;I)V
    .locals 5

    iget-object v0, p0, Lmiui/date/Calendar;->mTimeZone:Ljava/util/TimeZone;

    iget-object v1, p0, Lmiui/date/Calendar;->mFields:[I

    const/16 v2, 0x18

    aget v1, v1, v2

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    move v1, v3

    :goto_0
    const/4 v4, 0x4

    if-eq p3, v4, :cond_1

    move v2, v3

    :cond_1
    invoke-virtual {p2}, Lmiui/date/CalendarFormatSymbols;->getLocale()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v4}, Ljava/util/TimeZone;->getDisplayName(ZILjava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_2
    invoke-direct {p0, p1, v3, v3}, Lmiui/date/Calendar;->appendNumericTimeZone(Ljava/lang/StringBuilder;ZZ)V

    :goto_1
    return-void
.end method

.method private appendValue(Ljava/lang/StringBuilder;Lmiui/date/CalendarFormatSymbols;CII)V
    .locals 9

    const/4 v0, 0x3

    const/16 v1, 0x12

    const/4 v2, 0x0

    const/4 v3, 0x4

    const/16 v4, 0xa

    const/4 v5, 0x5

    const/16 v6, 0xc

    const/4 v7, 0x2

    const/4 v8, 0x1

    sparse-switch p3, :sswitch_data_0

    goto/16 :goto_2

    :sswitch_0
    invoke-direct {p0, p1, p2, p4}, Lmiui/date/Calendar;->appendTimeZone(Ljava/lang/StringBuilder;Lmiui/date/CalendarFormatSymbols;I)V

    goto/16 :goto_2

    :sswitch_1
    if-ne p4, v7, :cond_0

    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    aget v0, v0, v8

    rem-int/lit8 v0, v0, 0x64

    invoke-static {p1, p4, v0}, Lmiui/date/Calendar;->appendNumericValue(Ljava/lang/StringBuilder;II)V

    goto/16 :goto_2

    :cond_0
    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    aget v0, v0, v8

    invoke-static {p1, p4, v0}, Lmiui/date/Calendar;->appendNumericValue(Ljava/lang/StringBuilder;II)V

    goto/16 :goto_2

    :sswitch_2
    invoke-virtual {p2}, Lmiui/date/CalendarFormatSymbols;->getSolarTerms()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lmiui/date/Calendar;->mFields:[I

    const/16 v2, 0xf

    aget v1, v1, v2

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :sswitch_3
    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    aget v0, v0, v1

    invoke-static {p1, p4, v0}, Lmiui/date/Calendar;->appendNumericValue(Ljava/lang/StringBuilder;II)V

    goto/16 :goto_2

    :sswitch_4
    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    aget v0, v0, v1

    rem-int/2addr v0, v6

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    move v6, v0

    :goto_0
    invoke-static {p1, p4, v6}, Lmiui/date/Calendar;->appendNumericValue(Ljava/lang/StringBuilder;II)V

    goto/16 :goto_2

    :sswitch_5
    if-eq p4, v7, :cond_2

    invoke-virtual {p2}, Lmiui/date/CalendarFormatSymbols;->getChineseDays()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lmiui/date/Calendar;->mFields:[I

    aget v1, v1, v4

    sub-int/2addr v1, v8

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_2
    invoke-virtual {p2}, Lmiui/date/CalendarFormatSymbols;->getHeavenlyStems()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lmiui/date/Calendar;->mFields:[I

    const/16 v2, 0xb

    aget v1, v1, v2

    rem-int/2addr v1, v4

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lmiui/date/CalendarFormatSymbols;->getEarthlyBranches()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lmiui/date/Calendar;->mFields:[I

    aget v1, v1, v2

    rem-int/2addr v1, v6

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :sswitch_6
    if-eq p4, v7, :cond_3

    invoke-virtual {p2}, Lmiui/date/CalendarFormatSymbols;->getAmPms()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lmiui/date/Calendar;->mFields:[I

    const/16 v2, 0x11

    aget v1, v1, v2

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_3
    invoke-virtual {p2}, Lmiui/date/CalendarFormatSymbols;->getDetailedAmPms()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lmiui/date/Calendar;->mFields:[I

    const/16 v2, 0x10

    aget v1, v1, v2

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :sswitch_7
    if-ne p4, v3, :cond_4

    invoke-direct {p0, p1, v8, v8}, Lmiui/date/Calendar;->appendNumericTimeZone(Ljava/lang/StringBuilder;ZZ)V

    goto/16 :goto_2

    :cond_4
    if-ne p4, v5, :cond_5

    invoke-direct {p0, p1, v2, v8}, Lmiui/date/Calendar;->appendNumericTimeZone(Ljava/lang/StringBuilder;ZZ)V

    goto/16 :goto_2

    :cond_5
    invoke-direct {p0, p1, v2, v2}, Lmiui/date/Calendar;->appendNumericTimeZone(Ljava/lang/StringBuilder;ZZ)V

    goto/16 :goto_2

    :sswitch_8
    if-eq p4, v7, :cond_7

    invoke-virtual {p2}, Lmiui/date/CalendarFormatSymbols;->getChineseDigits()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lmiui/date/Calendar;->mFields:[I

    aget v1, v1, v7

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    :goto_1
    if-lez v1, :cond_6

    rem-int/lit8 v3, v1, 0xa

    div-int/lit8 v1, v1, 0xa

    aget-object v4, v0, v3

    invoke-virtual {p1, v2, v4}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_6
    goto/16 :goto_2

    :cond_7
    invoke-virtual {p2}, Lmiui/date/CalendarFormatSymbols;->getHeavenlyStems()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lmiui/date/Calendar;->mFields:[I

    aget v1, v1, v3

    rem-int/2addr v1, v4

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lmiui/date/CalendarFormatSymbols;->getEarthlyBranches()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lmiui/date/Calendar;->mFields:[I

    aget v1, v1, v3

    rem-int/2addr v1, v6

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :sswitch_9
    if-eq p4, v7, :cond_8

    invoke-virtual {p2}, Lmiui/date/CalendarFormatSymbols;->getChineseLeapMonths()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lmiui/date/Calendar;->mFields:[I

    const/16 v2, 0x8

    aget v1, v1, v2

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lmiui/date/CalendarFormatSymbols;->getChineseMonths()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lmiui/date/Calendar;->mFields:[I

    const/4 v2, 0x6

    aget v1, v1, v2

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_8
    invoke-virtual {p2}, Lmiui/date/CalendarFormatSymbols;->getHeavenlyStems()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lmiui/date/Calendar;->mFields:[I

    const/4 v2, 0x7

    aget v1, v1, v2

    rem-int/2addr v1, v4

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lmiui/date/CalendarFormatSymbols;->getEarthlyBranches()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lmiui/date/Calendar;->mFields:[I

    aget v1, v1, v2

    rem-int/2addr v1, v6

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :sswitch_a
    if-ge p4, v0, :cond_9

    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    aget v0, v0, v5

    add-int/2addr v0, v8

    invoke-static {p1, p4, v0}, Lmiui/date/Calendar;->appendNumericValue(Ljava/lang/StringBuilder;II)V

    goto/16 :goto_2

    :cond_9
    if-ne p4, v3, :cond_a

    invoke-virtual {p2}, Lmiui/date/CalendarFormatSymbols;->getMonths()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lmiui/date/Calendar;->mFields:[I

    aget v1, v1, v5

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_a
    if-ne p4, v5, :cond_b

    invoke-virtual {p2}, Lmiui/date/CalendarFormatSymbols;->getShortestMonths()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lmiui/date/Calendar;->mFields:[I

    aget v1, v1, v5

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_b
    invoke-virtual {p2}, Lmiui/date/CalendarFormatSymbols;->getShortMonths()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lmiui/date/Calendar;->mFields:[I

    aget v1, v1, v5

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :sswitch_b
    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    aget v0, v0, v1

    rem-int/2addr v0, v6

    invoke-static {p1, p4, v0}, Lmiui/date/Calendar;->appendNumericValue(Ljava/lang/StringBuilder;II)V

    goto/16 :goto_2

    :sswitch_c
    const/16 v0, 0x13

    if-ne p4, v7, :cond_c

    invoke-virtual {p2}, Lmiui/date/CalendarFormatSymbols;->getHeavenlyStems()[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lmiui/date/Calendar;->mFields:[I

    aget v2, v2, v0

    rem-int/2addr v2, v4

    aget-object v1, v1, v2

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_c
    invoke-virtual {p2}, Lmiui/date/CalendarFormatSymbols;->getEarthlyBranches()[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lmiui/date/Calendar;->mFields:[I

    aget v0, v2, v0

    rem-int/2addr v0, v6

    aget-object v0, v1, v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :sswitch_d
    invoke-virtual {p2}, Lmiui/date/CalendarFormatSymbols;->getEras()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lmiui/date/Calendar;->mFields:[I

    aget v1, v1, v2

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :sswitch_e
    goto :goto_2

    :sswitch_f
    const/16 v0, 0xe

    if-ne p4, v3, :cond_d

    invoke-virtual {p2}, Lmiui/date/CalendarFormatSymbols;->getWeekDays()[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lmiui/date/Calendar;->mFields:[I

    aget v0, v2, v0

    sub-int/2addr v0, v8

    aget-object v0, v1, v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_d
    if-ne p4, v5, :cond_e

    invoke-virtual {p2}, Lmiui/date/CalendarFormatSymbols;->getShortestWeekDays()[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lmiui/date/Calendar;->mFields:[I

    aget v0, v2, v0

    sub-int/2addr v0, v8

    aget-object v0, v1, v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_e
    invoke-virtual {p2}, Lmiui/date/CalendarFormatSymbols;->getShortWeekDays()[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lmiui/date/Calendar;->mFields:[I

    aget v0, v2, v0

    sub-int/2addr v0, v8

    aget-object v0, v1, v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :sswitch_10
    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    aget v0, v0, p5

    invoke-static {p1, p4, v0}, Lmiui/date/Calendar;->appendNumericValue(Ljava/lang/StringBuilder;II)V

    goto :goto_2

    :sswitch_11
    invoke-virtual {p2}, Lmiui/date/CalendarFormatSymbols;->getChineseSymbolAnimals()[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lmiui/date/Calendar;->mFields:[I

    aget v0, v2, v0

    aget-object v0, v1, v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    nop

    :goto_2
    return-void

    :sswitch_data_0
    .sparse-switch
        0x41 -> :sswitch_11
        0x44 -> :sswitch_10
        0x45 -> :sswitch_f
        0x46 -> :sswitch_e
        0x47 -> :sswitch_d
        0x48 -> :sswitch_10
        0x49 -> :sswitch_c
        0x4b -> :sswitch_b
        0x4c -> :sswitch_a
        0x4d -> :sswitch_a
        0x4e -> :sswitch_9
        0x53 -> :sswitch_10
        0x57 -> :sswitch_e
        0x59 -> :sswitch_8
        0x5a -> :sswitch_7
        0x61 -> :sswitch_6
        0x63 -> :sswitch_f
        0x64 -> :sswitch_10
        0x65 -> :sswitch_5
        0x68 -> :sswitch_4
        0x6b -> :sswitch_3
        0x6d -> :sswitch_10
        0x73 -> :sswitch_10
        0x74 -> :sswitch_2
        0x77 -> :sswitch_e
        0x79 -> :sswitch_1
        0x7a -> :sswitch_0
    .end sparse-switch
.end method

.method private compute()V
    .locals 3

    invoke-direct {p0}, Lmiui/date/Calendar;->computeDateTime()J

    move-result-wide v0

    invoke-virtual {p0}, Lmiui/date/Calendar;->outOfChineseCalendarRange()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0, v0, v1}, Lmiui/date/Calendar;->computeChineseDate(J)V

    invoke-direct {p0}, Lmiui/date/Calendar;->computeSolarTerm()V

    invoke-direct {p0, v0, v1}, Lmiui/date/Calendar;->computeChineseEraNames(J)V

    :cond_0
    return-void
.end method

.method private computeChineseDate(J)V
    .locals 9

    const-wide/16 v0, -0x63c1

    sub-long v0, p1, v0

    long-to-int v0, v0

    iget-object v1, p0, Lmiui/date/Calendar;->mFields:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    const/16 v3, 0x834

    if-lt v1, v3, :cond_0

    goto :goto_0

    :cond_0
    add-int/lit8 v3, v1, 0x1

    :goto_0
    move v1, v3

    sget-object v2, Lmiui/date/Calendar;->DAY_CHINESE_YEAR_FROM_19000101:[I

    add-int/lit16 v3, v1, -0x76c

    aget v2, v2, v3

    sub-int/2addr v0, v2

    if-gez v0, :cond_1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Lmiui/date/Calendar;->daysInChineseYear(I)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    if-gez v0, :cond_2

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Lmiui/date/Calendar;->daysInChineseYear(I)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget-object v2, p0, Lmiui/date/Calendar;->mFields:[I

    const/4 v3, 0x2

    aput v1, v2, v3

    const/16 v3, 0xd

    add-int/lit8 v4, v0, 0x1

    aput v4, v2, v3

    invoke-static {v1}, Lmiui/date/Calendar;->leapChineseMonth(I)I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_1
    const/16 v6, 0xc

    if-ge v5, v6, :cond_5

    if-lez v0, :cond_5

    if-ltz v2, :cond_3

    add-int/lit8 v6, v2, 0x1

    if-ne v5, v6, :cond_3

    if-nez v3, :cond_3

    add-int/lit8 v5, v5, -0x1

    const/4 v3, 0x1

    invoke-static {v1}, Lmiui/date/Calendar;->leapDaysInChineseYear(I)I

    move-result v4

    goto :goto_2

    :cond_3
    invoke-static {v1, v5}, Lmiui/date/Calendar;->daysInChineseMonth(II)I

    move-result v4

    :goto_2
    if-eqz v3, :cond_4

    add-int/lit8 v6, v2, 0x1

    if-ne v5, v6, :cond_4

    const/4 v3, 0x0

    :cond_4
    sub-int/2addr v0, v4

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_5
    if-nez v0, :cond_7

    if-lez v2, :cond_7

    add-int/lit8 v6, v2, 0x1

    if-ne v5, v6, :cond_7

    if-eqz v3, :cond_6

    const/4 v3, 0x0

    goto :goto_3

    :cond_6
    const/4 v3, 0x1

    add-int/lit8 v5, v5, -0x1

    :cond_7
    :goto_3
    if-gez v0, :cond_8

    add-int/2addr v0, v4

    add-int/lit8 v5, v5, -0x1

    :cond_8
    iget-object v6, p0, Lmiui/date/Calendar;->mFields:[I

    const/16 v7, 0x8

    aput v3, v6, v7

    const/4 v7, 0x6

    aput v5, v6, v7

    const/16 v7, 0xa

    add-int/lit8 v8, v0, 0x1

    aput v8, v6, v7

    return-void
.end method

.method private computeChineseEraNames(J)V
    .locals 11

    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    const/4 v1, 0x1

    aget v2, v0, v1

    const/4 v3, 0x5

    aget v4, v0, v3

    const/4 v5, 0x2

    if-ge v4, v5, :cond_1

    const/4 v4, 0x6

    aget v4, v0, v4

    if-ge v4, v5, :cond_0

    const/16 v4, 0xd

    aget v4, v0, v4

    if-ge v4, v1, :cond_1

    :cond_0
    add-int/lit8 v2, v2, -0x1

    :cond_1
    const/4 v4, 0x3

    add-int/lit16 v6, v2, -0x76c

    const/16 v7, 0xc

    add-int/2addr v6, v7

    int-to-long v8, v6

    invoke-static {v8, v9, v7}, Lmiui/date/Calendar;->mod(JI)I

    move-result v6

    aput v6, v0, v4

    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    const/4 v4, 0x4

    add-int/lit16 v6, v2, -0x76c

    add-int/lit8 v6, v6, 0x24

    int-to-long v8, v6

    const/16 v6, 0x3c

    invoke-static {v8, v9, v6}, Lmiui/date/Calendar;->mod(JI)I

    move-result v8

    aput v8, v0, v4

    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    aget v4, v0, v1

    aget v0, v0, v3

    invoke-static {v4, v0}, Lmiui/date/Calendar;->solarTermDaysOfMonth(II)I

    move-result v0

    shr-int/lit8 v0, v0, 0x8

    iget-object v4, p0, Lmiui/date/Calendar;->mFields:[I

    aget v8, v4, v1

    add-int/lit16 v8, v8, -0x76c

    mul-int/2addr v8, v7

    aget v3, v4, v3

    add-int/2addr v8, v3

    const/16 v2, 0x9

    aget v2, v4, v2

    if-lt v2, v0, :cond_2

    add-int/lit8 v8, v8, 0x1

    :cond_2
    const/4 v2, 0x7

    add-int/lit8 v3, v8, 0xc

    int-to-long v9, v3

    invoke-static {v9, v10, v6}, Lmiui/date/Calendar;->mod(JI)I

    move-result v3

    aput v3, v4, v2

    const-wide/16 v2, -0x63c1

    sub-long v2, p1, v2

    long-to-int v2, v2

    iget-object v3, p0, Lmiui/date/Calendar;->mFields:[I

    const/16 v4, 0xb

    add-int/lit8 v7, v2, 0x28

    int-to-long v7, v7

    invoke-static {v7, v8, v6}, Lmiui/date/Calendar;->mod(JI)I

    move-result v7

    aput v7, v3, v4

    iget-object v3, p0, Lmiui/date/Calendar;->mFields:[I

    const/16 v4, 0x13

    mul-int/lit8 v7, v2, 0xc

    const/16 v8, 0x12

    aget v8, v3, v8

    add-int/2addr v8, v1

    rem-int/lit8 v8, v8, 0x18

    div-int/2addr v8, v5

    add-int/2addr v7, v8

    int-to-long v7, v7

    invoke-static {v7, v8, v6}, Lmiui/date/Calendar;->mod(JI)I

    move-result v1

    aput v1, v3, v4

    return-void
.end method

.method private computeDate(JJ)V
    .locals 10

    invoke-direct {p0, p1, p2, p3, p4}, Lmiui/date/Calendar;->computeYearAndDay(JJ)I

    move-result v0

    iget-object v1, p0, Lmiui/date/Calendar;->mFields:[I

    const/16 v2, 0xc

    aput v0, v1, v2

    const/4 v2, 0x1

    aget v1, v1, v2

    iget v3, p0, Lmiui/date/Calendar;->changeYear:I

    if-ne v1, v3, :cond_0

    iget-wide v3, p0, Lmiui/date/Calendar;->gregorianCutover:J

    cmp-long v3, v3, p3

    if-gtz v3, :cond_0

    iget v3, p0, Lmiui/date/Calendar;->currentYearSkew:I

    add-int/2addr v0, v3

    :cond_0
    div-int/lit8 v3, v0, 0x20

    invoke-virtual {p0, v1}, Lmiui/date/Calendar;->isLeapYear(I)Z

    move-result v1

    invoke-static {v1, v3}, Lmiui/date/Calendar;->daysInYear(ZI)I

    move-result v4

    sub-int v4, v0, v4

    invoke-static {v1, v3}, Lmiui/date/Calendar;->daysInMonth(ZI)I

    move-result v5

    if-le v4, v5, :cond_1

    invoke-static {v1, v3}, Lmiui/date/Calendar;->daysInMonth(ZI)I

    move-result v5

    sub-int/2addr v4, v5

    add-int/lit8 v3, v3, 0x1

    :cond_1
    iget-object v5, p0, Lmiui/date/Calendar;->mFields:[I

    const/4 v6, 0x5

    aput v3, v5, v6

    const/16 v6, 0x9

    aput v4, v5, v6

    const/16 v6, 0xe

    const-wide/16 v7, 0x3

    sub-long v7, p1, v7

    const/4 v9, 0x7

    invoke-static {v7, v8, v9}, Lmiui/date/Calendar;->mod(JI)I

    move-result v7

    add-int/2addr v7, v2

    aput v7, v5, v6

    return-void
.end method

.method private computeDateTime()J
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lmiui/date/Calendar;->mFields:[I

    iget-object v2, v0, Lmiui/date/Calendar;->mTimeZone:Ljava/util/TimeZone;

    invoke-virtual {v2}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v2

    const/16 v3, 0x17

    aput v2, v1, v3

    iget-wide v1, v0, Lmiui/date/Calendar;->mMillisecond:J

    const-wide/32 v4, 0x5265c00

    div-long v6, v1, v4

    rem-long/2addr v1, v4

    long-to-int v1, v1

    const-wide/16 v4, 0x1

    const v2, 0x5265c00

    if-gez v1, :cond_0

    add-int/2addr v1, v2

    sub-long/2addr v6, v4

    :cond_0
    iget-object v8, v0, Lmiui/date/Calendar;->mFields:[I

    aget v8, v8, v3

    add-int/2addr v1, v8

    :goto_0
    if-gez v1, :cond_1

    add-int/2addr v1, v2

    sub-long/2addr v6, v4

    goto :goto_0

    :cond_1
    :goto_1
    if-lt v1, v2, :cond_2

    sub-int/2addr v1, v2

    add-long/2addr v6, v4

    goto :goto_1

    :cond_2
    iget-object v8, v0, Lmiui/date/Calendar;->mFields:[I

    aget v8, v8, v3

    iget-wide v9, v0, Lmiui/date/Calendar;->mMillisecond:J

    int-to-long v11, v8

    add-long/2addr v11, v9

    const-wide/16 v13, 0x0

    cmp-long v15, v9, v13

    if-lez v15, :cond_3

    cmp-long v15, v11, v13

    if-gez v15, :cond_3

    if-lez v8, :cond_3

    const-wide v11, 0x7fffffffffffffffL

    goto :goto_2

    :cond_3
    cmp-long v9, v9, v13

    if-gez v9, :cond_4

    cmp-long v9, v11, v13

    if-lez v9, :cond_4

    if-gez v8, :cond_4

    const-wide/high16 v11, -0x8000000000000000L

    :cond_4
    :goto_2
    invoke-direct {v0, v6, v7, v11, v12}, Lmiui/date/Calendar;->computeDate(JJ)V

    invoke-direct {v0, v1}, Lmiui/date/Calendar;->getDstOffset(I)I

    move-result v9

    iget-object v10, v0, Lmiui/date/Calendar;->mFields:[I

    const/16 v15, 0x18

    aput v9, v10, v15

    if-eqz v9, :cond_9

    move-wide v15, v6

    add-int/2addr v1, v9

    if-gez v1, :cond_5

    add-int/2addr v1, v2

    sub-long/2addr v6, v4

    goto :goto_3

    :cond_5
    if-lt v1, v2, :cond_6

    sub-int/2addr v1, v2

    add-long/2addr v6, v4

    :cond_6
    :goto_3
    cmp-long v2, v15, v6

    if-eqz v2, :cond_9

    aget v2, v10, v3

    sub-int v8, v9, v2

    iget-wide v2, v0, Lmiui/date/Calendar;->mMillisecond:J

    int-to-long v4, v8

    add-long/2addr v4, v2

    cmp-long v10, v2, v13

    if-lez v10, :cond_7

    cmp-long v10, v4, v13

    if-gez v10, :cond_7

    if-lez v8, :cond_7

    const-wide v2, 0x7fffffffffffffffL

    move-wide v11, v2

    goto :goto_4

    :cond_7
    cmp-long v2, v2, v13

    if-gez v2, :cond_8

    cmp-long v2, v4, v13

    if-lez v2, :cond_8

    if-gez v8, :cond_8

    const-wide/high16 v2, -0x8000000000000000L

    move-wide v11, v2

    goto :goto_4

    :cond_8
    move-wide v11, v4

    :goto_4
    invoke-direct {v0, v6, v7, v11, v12}, Lmiui/date/Calendar;->computeDate(JJ)V

    :cond_9
    iget-object v2, v0, Lmiui/date/Calendar;->mFields:[I

    const/4 v3, 0x1

    aget v4, v2, v3

    const/4 v5, 0x0

    if-gtz v4, :cond_a

    aput v5, v2, v5

    rsub-int/lit8 v4, v4, 0x1

    aput v4, v2, v3

    goto :goto_5

    :cond_a
    aput v3, v2, v5

    :goto_5
    const/16 v4, 0x16

    rem-int/lit16 v10, v1, 0x3e8

    aput v10, v2, v4

    div-int/lit16 v1, v1, 0x3e8

    const/16 v4, 0x15

    rem-int/lit8 v10, v1, 0x3c

    aput v10, v2, v4

    div-int/lit8 v1, v1, 0x3c

    const/16 v4, 0x14

    rem-int/lit8 v10, v1, 0x3c

    aput v10, v2, v4

    div-int/lit8 v1, v1, 0x3c

    const/16 v4, 0x12

    rem-int/lit8 v10, v1, 0x18

    aput v10, v2, v4

    const/16 v4, 0x11

    const/16 v13, 0xb

    if-le v10, v13, :cond_b

    move v13, v3

    goto :goto_6

    :cond_b
    move v13, v5

    :goto_6
    aput v13, v2, v4

    const/16 v4, 0x10

    packed-switch v10, :pswitch_data_0

    goto :goto_7

    :pswitch_0
    const/4 v3, 0x6

    aput v3, v2, v4

    goto :goto_7

    :pswitch_1
    const/4 v3, 0x5

    aput v3, v2, v4

    goto :goto_7

    :pswitch_2
    const/4 v3, 0x4

    aput v3, v2, v4

    goto :goto_7

    :pswitch_3
    const/4 v3, 0x3

    aput v3, v2, v4

    goto :goto_7

    :pswitch_4
    const/4 v3, 0x2

    aput v3, v2, v4

    goto :goto_7

    :pswitch_5
    aput v3, v2, v4

    goto :goto_7

    :pswitch_6
    aput v5, v2, v4

    nop

    :goto_7
    return-wide v6

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private computeSolarTerm()V
    .locals 7

    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    const/4 v1, 0x1

    aget v2, v0, v1

    const/4 v3, 0x5

    aget v0, v0, v3

    invoke-static {v2, v0}, Lmiui/date/Calendar;->solarTermDaysOfMonth(II)I

    move-result v0

    iget-object v2, p0, Lmiui/date/Calendar;->mFields:[I

    const/16 v4, 0x9

    aget v4, v2, v4

    shr-int/lit8 v5, v0, 0x8

    const/16 v6, 0xf

    if-ne v4, v5, :cond_0

    aget v3, v2, v3

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v1

    aput v3, v2, v6

    goto :goto_0

    :cond_0
    and-int/lit16 v1, v0, 0xff

    if-ne v4, v1, :cond_1

    aget v1, v2, v3

    mul-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x2

    aput v1, v2, v6

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    aput v1, v2, v6

    :goto_0
    return-void
.end method

.method private computeYearAndDay(JJ)I
    .locals 7

    const/16 v0, 0x7b2

    move-wide v1, p1

    iget-wide v3, p0, Lmiui/date/Calendar;->gregorianCutover:J

    cmp-long v3, p3, v3

    if-gez v3, :cond_0

    iget v3, p0, Lmiui/date/Calendar;->julianSkew:I

    int-to-long v3, v3

    sub-long/2addr v1, v3

    :cond_0
    :goto_0
    const-wide/16 v3, 0x16d

    div-long v3, v1, v3

    long-to-int v3, v3

    move v4, v3

    if-eqz v3, :cond_1

    add-int/2addr v0, v4

    int-to-long v5, v0

    invoke-direct {p0, v5, v6}, Lmiui/date/Calendar;->daysFromBaseYear(J)J

    move-result-wide v5

    sub-long v1, p1, v5

    goto :goto_0

    :cond_1
    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-gez v3, :cond_2

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lmiui/date/Calendar;->daysInYear(I)I

    move-result v3

    int-to-long v5, v3

    add-long/2addr v1, v5

    :cond_2
    iget-object v3, p0, Lmiui/date/Calendar;->mFields:[I

    const/4 v5, 0x1

    aput v0, v3, v5

    long-to-int v3, v1

    add-int/2addr v3, v5

    return v3
.end method

.method private daysFromBaseYear(J)J
    .locals 13

    const-wide/16 v0, 0x7b2

    cmp-long v2, p1, v0

    const-wide/16 v3, 0x190

    const-wide/16 v5, 0x64

    const-wide/16 v7, 0x4

    const-wide/16 v9, 0x16d

    if-ltz v2, :cond_3

    sub-long v0, p1, v0

    mul-long/2addr v0, v9

    const-wide/16 v9, 0x7b1

    sub-long v9, p1, v9

    div-long/2addr v9, v7

    add-long/2addr v0, v9

    iget v2, p0, Lmiui/date/Calendar;->changeYear:I

    int-to-long v7, v2

    cmp-long v7, p1, v7

    if-lez v7, :cond_0

    const-wide/16 v7, 0x76d

    sub-long v7, p1, v7

    div-long/2addr v7, v5

    const-wide/16 v5, 0x641

    sub-long v5, p1, v5

    div-long/2addr v5, v3

    sub-long/2addr v7, v5

    sub-long/2addr v0, v7

    goto :goto_0

    :cond_0
    int-to-long v3, v2

    cmp-long v3, p1, v3

    if-nez v3, :cond_1

    iget v2, p0, Lmiui/date/Calendar;->currentYearSkew:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    goto :goto_0

    :cond_1
    add-int/lit8 v2, v2, -0x1

    int-to-long v2, v2

    cmp-long v2, p1, v2

    if-nez v2, :cond_2

    iget v2, p0, Lmiui/date/Calendar;->lastYearSkew:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    goto :goto_0

    :cond_2
    iget v2, p0, Lmiui/date/Calendar;->julianSkew:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    :goto_0
    return-wide v0

    :cond_3
    iget v2, p0, Lmiui/date/Calendar;->changeYear:I

    int-to-long v11, v2

    cmp-long v2, p1, v11

    const-wide/16 v11, 0x7b4

    if-gtz v2, :cond_4

    sub-long v0, p1, v0

    mul-long/2addr v0, v9

    sub-long v2, p1, v11

    div-long/2addr v2, v7

    add-long/2addr v0, v2

    iget v2, p0, Lmiui/date/Calendar;->julianSkew:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0

    :cond_4
    sub-long v0, p1, v0

    mul-long/2addr v0, v9

    sub-long v9, p1, v11

    div-long/2addr v9, v7

    add-long/2addr v0, v9

    const-wide/16 v7, 0x7d0

    sub-long v9, p1, v7

    div-long/2addr v9, v5

    sub-long/2addr v0, v9

    sub-long v5, p1, v7

    div-long/2addr v5, v3

    add-long/2addr v0, v5

    return-wide v0
.end method

.method static daysInChineseMonth(II)I
    .locals 3

    sget-object v0, Lmiui/date/Calendar;->CHINESE_YEAR_INFO:[I

    add-int/lit16 v1, p0, -0x76c

    aget v0, v0, v1

    add-int/lit8 v1, p1, 0x1

    const/high16 v2, 0x10000

    shr-int v1, v2, v1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/16 v0, 0x1e

    goto :goto_0

    :cond_0
    const/16 v0, 0x1d

    :goto_0
    return v0
.end method

.method static daysInChineseYear(I)I
    .locals 3

    sget-object v0, Lmiui/date/Calendar;->DAY_CHINESE_YEAR_FROM_19000101:[I

    add-int/lit16 v1, p0, -0x76c

    add-int/lit8 v1, v1, 0x1

    aget v1, v0, v1

    add-int/lit16 v2, p0, -0x76c

    aget v0, v0, v2

    sub-int/2addr v1, v0

    return v1
.end method

.method private static daysInMonth(ZI)I
    .locals 2

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    sget-object v1, Lmiui/date/Calendar;->DAYS_IN_MONTH:[B

    aget-byte v1, v1, p1

    add-int/2addr v1, v0

    return v1

    :cond_0
    sget-object v0, Lmiui/date/Calendar;->DAYS_IN_MONTH:[B

    aget-byte v0, v0, p1

    return v0
.end method

.method private daysInYear(I)I
    .locals 3

    invoke-virtual {p0, p1}, Lmiui/date/Calendar;->isLeapYear(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x16e

    goto :goto_0

    :cond_0
    const/16 v0, 0x16d

    :goto_0
    iget v1, p0, Lmiui/date/Calendar;->changeYear:I

    if-ne p1, v1, :cond_1

    iget v2, p0, Lmiui/date/Calendar;->currentYearSkew:I

    sub-int/2addr v0, v2

    :cond_1
    add-int/lit8 v1, v1, -0x1

    if-ne p1, v1, :cond_2

    iget v1, p0, Lmiui/date/Calendar;->lastYearSkew:I

    sub-int/2addr v0, v1

    :cond_2
    return v0
.end method

.method private static daysInYear(ZI)I
    .locals 2

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    if-le p1, v0, :cond_0

    sget-object v1, Lmiui/date/Calendar;->DAYS_IN_YEAR:[I

    aget v1, v1, p1

    add-int/2addr v1, v0

    return v1

    :cond_0
    sget-object v0, Lmiui/date/Calendar;->DAYS_IN_YEAR:[I

    aget v0, v0, p1

    return v0
.end method

.method private getDstOffset(I)I
    .locals 10

    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    const/4 v1, 0x1

    aget v4, v0, v1

    const/4 v9, 0x0

    if-gtz v4, :cond_0

    move v0, v9

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lmiui/date/Calendar;->mTimeZone:Ljava/util/TimeZone;

    const/4 v3, 0x1

    const/4 v5, 0x5

    aget v5, v0, v5

    const/16 v6, 0x9

    aget v6, v0, v6

    const/16 v7, 0xe

    aget v7, v0, v7

    move v8, p1

    invoke-virtual/range {v2 .. v8}, Ljava/util/TimeZone;->getOffset(IIIIII)I

    move-result v0

    :goto_0
    iget-object v2, p0, Lmiui/date/Calendar;->mFields:[I

    aget v1, v2, v1

    if-lez v1, :cond_1

    const/16 v1, 0x17

    aget v1, v2, v1

    sub-int v1, v0, v1

    return v1

    :cond_1
    return v9
.end method

.method private static leapChineseMonth(I)I
    .locals 2

    sget-object v0, Lmiui/date/Calendar;->CHINESE_YEAR_INFO:[I

    add-int/lit16 v1, p0, -0x76c

    aget v0, v0, v1

    const/16 v1, 0xf

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v1, -0x1

    goto :goto_0

    :cond_0
    add-int/lit8 v1, v0, -0x1

    :goto_0
    return v1
.end method

.method static leapDaysInChineseYear(I)I
    .locals 2

    invoke-static {p0}, Lmiui/date/Calendar;->leapChineseMonth(I)I

    move-result v0

    if-ltz v0, :cond_1

    sget-object v0, Lmiui/date/Calendar;->CHINESE_YEAR_INFO:[I

    add-int/lit16 v1, p0, -0x76c

    add-int/lit8 v1, v1, 0x1

    aget v0, v0, v1

    const/16 v1, 0xf

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/16 v0, 0x1e

    goto :goto_0

    :cond_0
    const/16 v0, 0x1d

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private static mod(JI)I
    .locals 3

    int-to-long v0, p2

    rem-long v0, p0, v0

    long-to-int v0, v0

    const-wide/16 v1, 0x0

    cmp-long v1, p0, v1

    if-gez v1, :cond_0

    if-gez v0, :cond_0

    add-int v1, v0, p2

    return v1

    :cond_0
    return v0
.end method

.method private onChineseDateChange()V
    .locals 8

    const-wide/16 v0, -0x63c1

    sget-object v2, Lmiui/date/Calendar;->DAY_CHINESE_YEAR_FROM_19000101:[I

    iget-object v3, p0, Lmiui/date/Calendar;->mFields:[I

    const/4 v4, 0x2

    aget v3, v3, v4

    add-int/lit16 v3, v3, -0x76c

    aget v2, v2, v3

    int-to-long v2, v2

    add-long/2addr v0, v2

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p0, Lmiui/date/Calendar;->mFields:[I

    const/4 v5, 0x6

    aget v6, v3, v5

    if-ge v2, v6, :cond_0

    aget v3, v3, v4

    invoke-static {v3, v2}, Lmiui/date/Calendar;->daysInChineseMonth(II)I

    move-result v3

    int-to-long v5, v3

    add-long/2addr v0, v5

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const/16 v2, 0x8

    aget v2, v3, v2

    const/4 v7, 0x1

    if-ne v2, v7, :cond_1

    aget v2, v3, v4

    invoke-static {v2, v6}, Lmiui/date/Calendar;->daysInChineseMonth(II)I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    goto :goto_1

    :cond_1
    aget v2, v3, v4

    invoke-static {v2}, Lmiui/date/Calendar;->leapChineseMonth(I)I

    move-result v2

    if-ltz v2, :cond_2

    iget-object v3, p0, Lmiui/date/Calendar;->mFields:[I

    aget v5, v3, v5

    if-ge v2, v5, :cond_2

    aget v3, v3, v4

    invoke-static {v3}, Lmiui/date/Calendar;->leapDaysInChineseYear(I)I

    move-result v3

    int-to-long v3, v3

    add-long/2addr v0, v3

    :cond_2
    :goto_1
    iget-object v2, p0, Lmiui/date/Calendar;->mFields:[I

    const/16 v3, 0xa

    aget v2, v2, v3

    sub-int/2addr v2, v7

    int-to-long v2, v2

    add-long/2addr v0, v2

    const-wide/16 v2, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lmiui/date/Calendar;->computeDate(JJ)V

    invoke-direct {p0}, Lmiui/date/Calendar;->onSolarDateChange()V

    return-void
.end method

.method private onSolarDateChange()V
    .locals 15

    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    const/4 v1, 0x1

    aget v9, v0, v1

    const/4 v2, 0x5

    aget v10, v0, v2

    const/16 v2, 0x9

    aget v0, v0, v2

    int-to-long v2, v9

    invoke-direct {p0, v2, v3}, Lmiui/date/Calendar;->daysFromBaseYear(J)J

    move-result-wide v2

    invoke-virtual {p0, v9}, Lmiui/date/Calendar;->isLeapYear(I)Z

    move-result v4

    invoke-static {v4, v10}, Lmiui/date/Calendar;->daysInYear(ZI)I

    move-result v4

    add-int/2addr v4, v0

    sub-int/2addr v4, v1

    int-to-long v4, v4

    add-long v11, v2, v4

    iget-object v2, p0, Lmiui/date/Calendar;->mFields:[I

    const-wide/16 v3, 0x3

    sub-long v3, v11, v3

    const/4 v5, 0x7

    invoke-static {v3, v4, v5}, Lmiui/date/Calendar;->mod(JI)I

    move-result v3

    add-int/lit8 v7, v3, 0x1

    const/16 v1, 0xe

    aput v7, v2, v1

    iget-object v1, p0, Lmiui/date/Calendar;->mFields:[I

    const/16 v2, 0x12

    aget v2, v1, v2

    const v3, 0x36ee80

    mul-int/2addr v2, v3

    const/16 v3, 0x14

    aget v3, v1, v3

    const v4, 0xea60

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    const/16 v3, 0x15

    aget v3, v1, v3

    mul-int/lit16 v3, v3, 0x3e8

    add-int/2addr v2, v3

    const/16 v3, 0x16

    aget v1, v1, v3

    add-int/2addr v1, v2

    const-wide/32 v2, 0x5265c00

    mul-long/2addr v2, v11

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lmiui/date/Calendar;->mMillisecond:J

    iget-object v2, p0, Lmiui/date/Calendar;->mTimeZone:Ljava/util/TimeZone;

    const/4 v3, 0x1

    move v4, v9

    move v5, v10

    move v6, v0

    move v8, v1

    invoke-virtual/range {v2 .. v8}, Ljava/util/TimeZone;->getOffset(IIIIII)I

    move-result v2

    int-to-long v2, v2

    new-instance v4, Ljava/util/Date;

    iget-wide v5, p0, Lmiui/date/Calendar;->mMillisecond:J

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    iget-object v5, p0, Lmiui/date/Calendar;->mTimeZone:Ljava/util/TimeZone;

    invoke-virtual {v5, v4}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-direct {p0, v1}, Lmiui/date/Calendar;->getDstOffset(I)I

    move-result v5

    int-to-long v5, v5

    sub-long/2addr v2, v5

    :cond_0
    iget-wide v5, p0, Lmiui/date/Calendar;->mMillisecond:J

    if-gtz v9, :cond_1

    const-wide/16 v13, 0x0

    goto :goto_0

    :cond_1
    move-wide v13, v2

    :goto_0
    sub-long/2addr v5, v13

    iput-wide v5, p0, Lmiui/date/Calendar;->mMillisecond:J

    invoke-direct {p0}, Lmiui/date/Calendar;->compute()V

    return-void
.end method

.method static solarTermDaysOfMonth(II)I
    .locals 8

    const/16 v0, 0x834

    if-le p0, v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    sget-object v0, Lmiui/date/Calendar;->SOLAR_TERM_INDEX:[B

    add-int/lit16 v1, p0, -0x76c

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, -0x30

    mul-int/lit8 v1, p1, 0x2

    mul-int/lit8 v2, v0, 0x18

    add-int/2addr v2, v1

    add-int/lit8 v3, v2, 0x1

    sget-object v4, Lmiui/date/Calendar;->SOLAR_TERM_OS:[B

    aget-byte v5, v4, v2

    add-int/lit8 v5, v5, -0x30

    sget-object v6, Lmiui/date/Calendar;->SOLAR_TERM_BASE:[I

    aget v7, v6, v1

    add-int/2addr v5, v7

    aget-byte v2, v4, v3

    add-int/lit8 v2, v2, -0x30

    add-int/lit8 v4, v1, 0x1

    aget v4, v6, v4

    add-int/2addr v2, v4

    shl-int/lit8 v3, v5, 0x8

    add-int/2addr v3, v2

    return v3
.end method


# virtual methods
.method public add(II)Lmiui/date/Calendar;
    .locals 17

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    if-ltz v1, :cond_26

    const/16 v3, 0x19

    if-ge v1, v3, :cond_26

    const/16 v3, 0x834

    const/16 v4, 0x76c

    const/16 v5, 0xa

    const-string v6, "out of range of Chinese Lunar Year"

    const/16 v7, 0x9

    const/4 v8, 0x5

    const-string v9, "out of range"

    const/16 v10, 0x8

    const/4 v11, 0x2

    const/4 v12, 0x6

    const/4 v13, 0x0

    const/4 v14, 0x1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "unsupported set field:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lmiui/date/Calendar;->FIELD_NAMES:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :pswitch_1
    if-eqz v2, :cond_25

    iget-wide v3, v0, Lmiui/date/Calendar;->mMillisecond:J

    int-to-long v5, v2

    add-long/2addr v5, v3

    if-lez v2, :cond_0

    move v7, v14

    goto :goto_0

    :cond_0
    move v7, v13

    :goto_0
    cmp-long v3, v5, v3

    if-lez v3, :cond_1

    move v13, v14

    :cond_1
    if-ne v7, v13, :cond_2

    iput-wide v5, v0, Lmiui/date/Calendar;->mMillisecond:J

    invoke-direct/range {p0 .. p0}, Lmiui/date/Calendar;->compute()V

    goto/16 :goto_d

    :cond_2
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :pswitch_2
    if-eqz v2, :cond_25

    iget-wide v3, v0, Lmiui/date/Calendar;->mMillisecond:J

    int-to-long v5, v2

    const-wide/16 v7, 0x3e8

    mul-long/2addr v5, v7

    add-long/2addr v5, v3

    if-lez v2, :cond_3

    move v7, v14

    goto :goto_1

    :cond_3
    move v7, v13

    :goto_1
    cmp-long v3, v5, v3

    if-lez v3, :cond_4

    move v13, v14

    :cond_4
    if-ne v7, v13, :cond_5

    iput-wide v5, v0, Lmiui/date/Calendar;->mMillisecond:J

    invoke-direct/range {p0 .. p0}, Lmiui/date/Calendar;->compute()V

    goto/16 :goto_d

    :cond_5
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :pswitch_3
    if-eqz v2, :cond_25

    iget-wide v3, v0, Lmiui/date/Calendar;->mMillisecond:J

    int-to-long v5, v2

    const-wide/32 v7, 0xea60

    mul-long/2addr v5, v7

    add-long/2addr v5, v3

    if-lez v2, :cond_6

    move v7, v14

    goto :goto_2

    :cond_6
    move v7, v13

    :goto_2
    cmp-long v3, v5, v3

    if-lez v3, :cond_7

    move v13, v14

    :cond_7
    if-ne v7, v13, :cond_8

    iput-wide v5, v0, Lmiui/date/Calendar;->mMillisecond:J

    invoke-direct/range {p0 .. p0}, Lmiui/date/Calendar;->compute()V

    goto/16 :goto_d

    :cond_8
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :pswitch_4
    if-eqz v2, :cond_25

    iget-wide v3, v0, Lmiui/date/Calendar;->mMillisecond:J

    int-to-long v5, v2

    const-wide/32 v7, 0x36ee80

    mul-long/2addr v5, v7

    add-long/2addr v5, v3

    if-lez v2, :cond_9

    move v7, v14

    goto :goto_3

    :cond_9
    move v7, v13

    :goto_3
    cmp-long v3, v5, v3

    if-lez v3, :cond_a

    move v13, v14

    :cond_a
    if-ne v7, v13, :cond_b

    iput-wide v5, v0, Lmiui/date/Calendar;->mMillisecond:J

    invoke-direct/range {p0 .. p0}, Lmiui/date/Calendar;->compute()V

    goto/16 :goto_d

    :cond_b
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :pswitch_5
    if-eqz v2, :cond_25

    iget-wide v3, v0, Lmiui/date/Calendar;->mMillisecond:J

    int-to-long v5, v2

    const-wide/32 v7, 0x5265c00

    mul-long/2addr v5, v7

    add-long/2addr v5, v3

    if-lez v2, :cond_c

    move v7, v14

    goto :goto_4

    :cond_c
    move v7, v13

    :goto_4
    cmp-long v3, v5, v3

    if-lez v3, :cond_d

    move v13, v14

    :cond_d
    if-ne v7, v13, :cond_e

    iput-wide v5, v0, Lmiui/date/Calendar;->mMillisecond:J

    invoke-direct/range {p0 .. p0}, Lmiui/date/Calendar;->compute()V

    goto/16 :goto_d

    :cond_e
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :pswitch_6
    if-eqz v2, :cond_25

    invoke-virtual/range {p0 .. p0}, Lmiui/date/Calendar;->outOfChineseCalendarRange()Z

    move-result v7

    if-nez v7, :cond_1a

    iget-object v7, v0, Lmiui/date/Calendar;->mFields:[I

    aget v7, v7, v11

    invoke-static {v7}, Lmiui/date/Calendar;->leapChineseMonth(I)I

    move-result v7

    :goto_5
    const/16 v8, 0xb

    if-lez v2, :cond_12

    iget-object v9, v0, Lmiui/date/Calendar;->mFields:[I

    aget v15, v9, v12

    if-ne v15, v7, :cond_f

    aget v16, v9, v10

    if-nez v16, :cond_f

    aput v14, v9, v10

    goto :goto_6

    :cond_f
    add-int/lit8 v15, v15, 0x1

    aput v15, v9, v12

    aput v13, v9, v10

    if-le v15, v8, :cond_11

    aput v13, v9, v12

    aget v8, v9, v11

    add-int/2addr v8, v14

    aput v8, v9, v11

    if-gt v8, v3, :cond_10

    invoke-static {v8}, Lmiui/date/Calendar;->leapChineseMonth(I)I

    move-result v7

    goto :goto_6

    :cond_10
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_11
    :goto_6
    add-int/lit8 v2, v2, -0x1

    goto :goto_5

    :cond_12
    :goto_7
    if-gez v2, :cond_17

    iget-object v3, v0, Lmiui/date/Calendar;->mFields:[I

    aget v9, v3, v12

    if-ne v9, v7, :cond_13

    aget v15, v3, v10

    if-ne v15, v14, :cond_13

    aput v13, v3, v10

    goto :goto_9

    :cond_13
    add-int/lit8 v9, v9, -0x1

    aput v9, v3, v12

    if-gez v9, :cond_15

    aput v8, v3, v12

    rsub-int/lit8 v9, v14, 0xb

    aput v9, v3, v12

    aget v9, v3, v11

    if-lt v9, v4, :cond_14

    aget v3, v3, v14

    invoke-static {v3}, Lmiui/date/Calendar;->leapChineseMonth(I)I

    move-result v7

    goto :goto_8

    :cond_14
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_15
    :goto_8
    iget-object v3, v0, Lmiui/date/Calendar;->mFields:[I

    aget v9, v3, v12

    if-ne v9, v7, :cond_16

    aput v14, v3, v10

    :cond_16
    :goto_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    :cond_17
    iget-object v3, v0, Lmiui/date/Calendar;->mFields:[I

    aget v4, v3, v10

    if-ne v4, v14, :cond_18

    aget v3, v3, v11

    invoke-static {v3}, Lmiui/date/Calendar;->leapDaysInChineseYear(I)I

    move-result v3

    goto :goto_a

    :cond_18
    aget v4, v3, v11

    aget v3, v3, v12

    invoke-static {v4, v3}, Lmiui/date/Calendar;->daysInChineseMonth(II)I

    move-result v3

    :goto_a
    nop

    iget-object v4, v0, Lmiui/date/Calendar;->mFields:[I

    aget v6, v4, v5

    if-le v6, v3, :cond_19

    aput v3, v4, v5

    :cond_19
    invoke-direct/range {p0 .. p0}, Lmiui/date/Calendar;->onChineseDateChange()V

    goto/16 :goto_d

    :cond_1a
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :pswitch_7
    if-eqz v2, :cond_25

    iget-object v3, v0, Lmiui/date/Calendar;->mFields:[I

    aget v4, v3, v8

    add-int/2addr v2, v4

    div-int/lit8 v4, v2, 0xc

    rem-int/lit8 v2, v2, 0xc

    if-gez v2, :cond_1b

    add-int/lit8 v2, v2, 0xc

    add-int/lit8 v4, v4, -0x1

    :cond_1b
    aput v2, v3, v8

    if-nez v4, :cond_1e

    aget v5, v3, v13

    if-nez v5, :cond_1c

    aget v5, v3, v14

    rsub-int/lit8 v5, v5, -0x1

    aput v5, v3, v14

    :cond_1c
    aget v3, v3, v14

    invoke-virtual {v0, v3}, Lmiui/date/Calendar;->isLeapYear(I)Z

    move-result v3

    iget-object v5, v0, Lmiui/date/Calendar;->mFields:[I

    aget v5, v5, v8

    invoke-static {v3, v5}, Lmiui/date/Calendar;->daysInMonth(ZI)I

    move-result v3

    iget-object v5, v0, Lmiui/date/Calendar;->mFields:[I

    aget v6, v5, v7

    if-le v6, v3, :cond_1d

    aput v3, v5, v7

    :cond_1d
    invoke-direct/range {p0 .. p0}, Lmiui/date/Calendar;->onSolarDateChange()V

    goto :goto_b

    :cond_1e
    invoke-virtual {v0, v14, v4}, Lmiui/date/Calendar;->add(II)Lmiui/date/Calendar;

    :goto_b
    goto/16 :goto_d

    :pswitch_8
    if-eqz v2, :cond_25

    iget-object v7, v0, Lmiui/date/Calendar;->mFields:[I

    aget v7, v7, v11

    add-int/2addr v2, v7

    invoke-virtual/range {p0 .. p0}, Lmiui/date/Calendar;->outOfChineseCalendarRange()Z

    move-result v7

    if-nez v7, :cond_22

    if-lt v2, v4, :cond_22

    if-gt v2, v3, :cond_22

    iget-object v3, v0, Lmiui/date/Calendar;->mFields:[I

    aput v2, v3, v11

    aget v4, v3, v10

    if-ne v4, v14, :cond_1f

    aget v3, v3, v12

    invoke-static {v2}, Lmiui/date/Calendar;->leapChineseMonth(I)I

    move-result v4

    if-ne v3, v4, :cond_1f

    iget-object v3, v0, Lmiui/date/Calendar;->mFields:[I

    aput v13, v3, v10

    :cond_1f
    iget-object v3, v0, Lmiui/date/Calendar;->mFields:[I

    aget v4, v3, v10

    if-ne v4, v14, :cond_20

    aget v3, v3, v11

    invoke-static {v3}, Lmiui/date/Calendar;->leapDaysInChineseYear(I)I

    move-result v3

    goto :goto_c

    :cond_20
    aget v4, v3, v11

    aget v3, v3, v12

    invoke-static {v4, v3}, Lmiui/date/Calendar;->daysInChineseMonth(II)I

    move-result v3

    :goto_c
    nop

    iget-object v4, v0, Lmiui/date/Calendar;->mFields:[I

    aget v6, v4, v5

    if-le v6, v3, :cond_21

    aput v3, v4, v5

    :cond_21
    invoke-direct/range {p0 .. p0}, Lmiui/date/Calendar;->onChineseDateChange()V

    goto :goto_d

    :cond_22
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :pswitch_9
    if-eqz v2, :cond_25

    iget-object v3, v0, Lmiui/date/Calendar;->mFields:[I

    aget v4, v3, v13

    if-nez v4, :cond_23

    aget v4, v3, v14

    rsub-int/lit8 v4, v4, -0x1

    aput v4, v3, v14

    :cond_23
    aget v4, v3, v14

    add-int/2addr v4, v2

    aput v4, v3, v14

    invoke-virtual {v0, v4}, Lmiui/date/Calendar;->isLeapYear(I)Z

    move-result v3

    iget-object v4, v0, Lmiui/date/Calendar;->mFields:[I

    aget v4, v4, v8

    invoke-static {v3, v4}, Lmiui/date/Calendar;->daysInMonth(ZI)I

    move-result v3

    iget-object v4, v0, Lmiui/date/Calendar;->mFields:[I

    aget v5, v4, v7

    if-le v5, v3, :cond_24

    aput v3, v4, v7

    :cond_24
    invoke-direct/range {p0 .. p0}, Lmiui/date/Calendar;->onSolarDateChange()V

    goto :goto_d

    :pswitch_a
    if-eqz v2, :cond_25

    iget-object v3, v0, Lmiui/date/Calendar;->mFields:[I

    aget v4, v3, v13

    add-int v5, v4, v2

    rem-int/lit8 v2, v5, 0x2

    if-eq v4, v2, :cond_25

    if-nez v4, :cond_25

    aget v4, v3, v14

    rsub-int/lit8 v4, v4, -0x1

    aput v4, v3, v14

    invoke-direct/range {p0 .. p0}, Lmiui/date/Calendar;->onSolarDateChange()V

    :cond_25
    :goto_d
    return-object v0

    :cond_26
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Field out of range [0-25]: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_5
        :pswitch_0
        :pswitch_5
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public after(Lmiui/date/Calendar;)Z
    .locals 4

    invoke-virtual {p0}, Lmiui/date/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-virtual {p1}, Lmiui/date/Calendar;->getTimeInMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public before(Lmiui/date/Calendar;)Z
    .locals 4

    invoke-virtual {p0}, Lmiui/date/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-virtual {p1}, Lmiui/date/Calendar;->getTimeInMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final clone()Ljava/lang/Object;
    .locals 2

    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/date/Calendar;

    iget-object v1, p0, Lmiui/date/Calendar;->mFields:[I

    invoke-virtual {v1}, [I->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    iput-object v1, v0, Lmiui/date/Calendar;->mFields:[I

    iget-object v1, p0, Lmiui/date/Calendar;->mTimeZone:Ljava/util/TimeZone;

    invoke-virtual {v1}, Ljava/util/TimeZone;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/TimeZone;

    iput-object v1, v0, Lmiui/date/Calendar;->mTimeZone:Ljava/util/TimeZone;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lmiui/date/Calendar;

    invoke-virtual {p0, p1}, Lmiui/date/Calendar;->compareTo(Lmiui/date/Calendar;)I

    move-result p1

    return p1
.end method

.method public compareTo(Lmiui/date/Calendar;)I
    .locals 5

    iget-wide v0, p0, Lmiui/date/Calendar;->mMillisecond:J

    iget-wide v2, p1, Lmiui/date/Calendar;->mMillisecond:J

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    const/4 v0, -0x1

    goto :goto_0

    :cond_0
    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    if-eq p1, p0, :cond_1

    instance-of v0, p1, Lmiui/date/Calendar;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lmiui/date/Calendar;->mMillisecond:J

    move-object v2, p1

    check-cast v2, Lmiui/date/Calendar;

    iget-wide v2, v2, Lmiui/date/Calendar;->mMillisecond:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public format(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lmiui/date/Calendar;->format(Ljava/lang/CharSequence;Lmiui/date/CalendarFormatSymbols;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public format(Ljava/lang/CharSequence;Lmiui/date/CalendarFormatSymbols;)Ljava/lang/String;
    .locals 3

    invoke-static {}, Lmiui/util/Pools;->getStringBuilderPool()Lmiui/util/Pools$Pool;

    move-result-object v0

    invoke-interface {v0}, Lmiui/util/Pools$Pool;->acquire()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v0, p1, p2}, Lmiui/date/Calendar;->format(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;Lmiui/date/CalendarFormatSymbols;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lmiui/util/Pools;->getStringBuilderPool()Lmiui/util/Pools$Pool;

    move-result-object v2

    invoke-interface {v2, v0}, Lmiui/util/Pools$Pool;->release(Ljava/lang/Object;)V

    return-object v1
.end method

.method public format(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lmiui/date/Calendar;->format(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;Lmiui/date/CalendarFormatSymbols;)Ljava/lang/StringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public format(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;Lmiui/date/CalendarFormatSymbols;)Ljava/lang/StringBuilder;
    .locals 11

    if-nez p3, :cond_0

    invoke-static {}, Lmiui/date/CalendarFormatSymbols;->getDefault()Lmiui/date/CalendarFormatSymbols;

    move-result-object p3

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v6

    move v7, v0

    :goto_0
    if-ge v1, v6, :cond_8

    invoke-interface {p2, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v8

    const/16 v0, 0x27

    if-eqz v7, :cond_3

    if-ne v8, v0, :cond_2

    add-int/lit8 v0, v1, 0x1

    if-ge v0, v6, :cond_1

    add-int/lit8 v0, v1, 0x1

    invoke-interface {p2, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-ne v0, v8, :cond_1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_1
    const/4 v0, 0x0

    move v7, v0

    goto :goto_2

    :cond_2
    invoke-virtual {p1, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_3
    if-ne v8, v0, :cond_5

    add-int/lit8 v0, v1, 0x1

    if-ge v0, v6, :cond_4

    add-int/lit8 v0, v1, 0x1

    invoke-interface {p2, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-ne v0, v8, :cond_4

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_4
    const/4 v0, 0x1

    move v7, v0

    goto :goto_2

    :cond_5
    const/16 v0, 0x41

    if-lt v8, v0, :cond_7

    const/16 v0, 0x7a

    if-gt v8, v0, :cond_7

    sget-object v0, Lmiui/date/Calendar;->FORMAT_CHARACTERS:[I

    add-int/lit8 v2, v8, -0x41

    aget v0, v0, v2

    if-ltz v0, :cond_7

    const/4 v0, 0x1

    move v9, v0

    move v10, v1

    :goto_1
    add-int/lit8 v0, v10, 0x1

    if-ge v0, v6, :cond_6

    add-int/lit8 v0, v10, 0x1

    invoke-interface {p2, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-ne v0, v8, :cond_6

    add-int/lit8 v10, v10, 0x1

    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_6
    sget-object v0, Lmiui/date/Calendar;->FORMAT_CHARACTERS:[I

    add-int/lit8 v1, v8, -0x41

    aget v5, v0, v1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move v3, v8

    move v4, v9

    invoke-direct/range {v0 .. v5}, Lmiui/date/Calendar;->appendValue(Ljava/lang/StringBuilder;Lmiui/date/CalendarFormatSymbols;CII)V

    move v1, v10

    goto :goto_2

    :cond_7
    invoke-virtual {p1, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_8
    return-object p1
.end method

.method public get(I)I
    .locals 3

    if-ltz p1, :cond_0

    const/16 v0, 0x19

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    aget v0, v0, p1

    return v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Field out of range [0-25]: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getActualMaximum(I)I
    .locals 6

    if-ltz p1, :cond_4

    const/16 v0, 0x19

    if-ge p1, v0, :cond_4

    const/4 v0, 0x6

    const/16 v1, 0xb

    const/4 v2, 0x2

    const/16 v3, 0x3b

    const/4 v4, 0x0

    const/4 v5, 0x1

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unsupported field: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lmiui/date/Calendar;->FIELD_NAMES:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/16 v0, 0x3e7

    return v0

    :pswitch_1
    return v3

    :pswitch_2
    return v3

    :pswitch_3
    const/16 v0, 0x17

    return v0

    :pswitch_4
    return v5

    :pswitch_5
    return v0

    :pswitch_6
    const/16 v0, 0x18

    return v0

    :pswitch_7
    const/4 v0, 0x7

    return v0

    :pswitch_8
    invoke-virtual {p0}, Lmiui/date/Calendar;->outOfChineseCalendarRange()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    aget v0, v0, v2

    invoke-static {v0}, Lmiui/date/Calendar;->daysInChineseYear(I)I

    move-result v4

    :goto_0
    return v4

    :pswitch_9
    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    aget v0, v0, v5

    invoke-direct {p0, v0}, Lmiui/date/Calendar;->daysInYear(I)I

    move-result v0

    return v0

    :pswitch_a
    invoke-virtual {p0}, Lmiui/date/Calendar;->outOfChineseCalendarRange()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lmiui/date/Calendar;->isChineseLeapMonth()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    aget v0, v0, v2

    invoke-static {v0}, Lmiui/date/Calendar;->leapDaysInChineseYear(I)I

    move-result v4

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lmiui/date/Calendar;->mFields:[I

    aget v2, v1, v2

    aget v0, v1, v0

    invoke-static {v2, v0}, Lmiui/date/Calendar;->daysInChineseMonth(II)I

    move-result v4

    :goto_1
    return v4

    :pswitch_b
    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    aget v0, v0, v5

    invoke-virtual {p0, v0}, Lmiui/date/Calendar;->isLeapYear(I)Z

    move-result v0

    iget-object v1, p0, Lmiui/date/Calendar;->mFields:[I

    const/4 v2, 0x5

    aget v1, v1, v2

    invoke-static {v0, v1}, Lmiui/date/Calendar;->daysInMonth(ZI)I

    move-result v0

    return v0

    :pswitch_c
    return v5

    :pswitch_d
    return v1

    :pswitch_e
    return v3

    :pswitch_f
    return v1

    :pswitch_10
    const/16 v0, 0x834

    return v0

    :pswitch_11
    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    aget v0, v0, v4

    if-ne v0, v5, :cond_3

    const v0, 0x116bd2d2

    goto :goto_2

    :cond_3
    const v0, 0x116babff

    :goto_2
    return v0

    :pswitch_12
    return v5

    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Field out of range [0-25]: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_d
        :pswitch_e
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_e
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_e
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getActualMinimum(I)I
    .locals 3

    if-ltz p1, :cond_0

    const/16 v0, 0x19

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unsupported field: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lmiui/date/Calendar;->FIELD_NAMES:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    return v1

    :pswitch_1
    return v1

    :pswitch_2
    return v1

    :pswitch_3
    return v1

    :pswitch_4
    return v1

    :pswitch_5
    return v1

    :pswitch_6
    return v1

    :pswitch_7
    return v0

    :pswitch_8
    invoke-virtual {p0}, Lmiui/date/Calendar;->outOfChineseCalendarRange()Z

    move-result v1

    xor-int/2addr v0, v1

    return v0

    :pswitch_9
    return v0

    :pswitch_a
    invoke-virtual {p0}, Lmiui/date/Calendar;->outOfChineseCalendarRange()Z

    move-result v1

    xor-int/2addr v0, v1

    return v0

    :pswitch_b
    return v0

    :pswitch_c
    return v1

    :pswitch_d
    return v1

    :pswitch_e
    return v1

    :pswitch_f
    return v1

    :pswitch_10
    const/16 v0, 0x76c

    return v0

    :pswitch_11
    return v0

    :pswitch_12
    return v1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Field out of range [0-25]: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_d
        :pswitch_e
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_e
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_e
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getChineseLeapMonth()I
    .locals 2

    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    const/4 v1, 0x2

    aget v0, v0, v1

    invoke-static {v0}, Lmiui/date/Calendar;->leapChineseMonth(I)I

    move-result v0

    return v0
.end method

.method public getTimeInMillis()J
    .locals 2

    iget-wide v0, p0, Lmiui/date/Calendar;->mMillisecond:J

    return-wide v0
.end method

.method public getTimeZone()Ljava/util/TimeZone;
    .locals 1

    iget-object v0, p0, Lmiui/date/Calendar;->mTimeZone:Ljava/util/TimeZone;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-wide v0, p0, Lmiui/date/Calendar;->mMillisecond:J

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public isChineseLeapMonth()Z
    .locals 2

    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    const/16 v1, 0x8

    aget v0, v0, v1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public isLeapYear(I)Z
    .locals 3

    iget v0, p0, Lmiui/date/Calendar;->changeYear:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-le p1, v0, :cond_2

    rem-int/lit8 v0, p1, 0x4

    if-nez v0, :cond_1

    rem-int/lit8 v0, p1, 0x64

    if-nez v0, :cond_0

    rem-int/lit16 v0, p1, 0x190

    if-nez v0, :cond_1

    :cond_0
    goto :goto_0

    :cond_1
    move v1, v2

    :goto_0
    return v1

    :cond_2
    rem-int/lit8 v0, p1, 0x4

    if-nez v0, :cond_3

    goto :goto_1

    :cond_3
    move v1, v2

    :goto_1
    return v1
.end method

.method public outOfChineseCalendarRange()Z
    .locals 8

    iget-wide v0, p0, Lmiui/date/Calendar;->mMillisecond:J

    iget-object v2, p0, Lmiui/date/Calendar;->mFields:[I

    const/16 v3, 0x17

    aget v3, v2, v3

    int-to-long v4, v3

    const-wide v6, -0x201b77f5c00L

    sub-long/2addr v6, v4

    const/16 v4, 0x18

    aget v2, v2, v4

    int-to-long v4, v2

    sub-long/2addr v6, v4

    cmp-long v4, v0, v6

    if-ltz v4, :cond_1

    const-wide v4, 0x3c314a71400L

    int-to-long v6, v3

    sub-long/2addr v4, v6

    int-to-long v2, v2

    sub-long/2addr v4, v2

    cmp-long v0, v0, v4

    if-ltz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public set(II)Lmiui/date/Calendar;
    .locals 8

    const-string v0, "]: "

    const-string v1, "-"

    const-string v2, "value is out of date range ["

    const/4 v3, 0x6

    if-ne p1, v3, :cond_6

    const/4 v4, 0x1

    const/16 v5, 0x8

    const/4 v6, 0x2

    if-gez p2, :cond_1

    neg-int p2, p2

    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    aget v0, v0, v6

    invoke-static {v0}, Lmiui/date/Calendar;->leapChineseMonth(I)I

    move-result v0

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    aput v4, v0, v5

    goto :goto_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "year "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lmiui/date/Calendar;->mFields:[I

    aget v2, v2, v6

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has no such leap month:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p0, p1}, Lmiui/date/Calendar;->getActualMinimum(I)I

    move-result v7

    if-ge p2, v7, :cond_3

    invoke-virtual {p0, p1}, Lmiui/date/Calendar;->getActualMaximum(I)I

    move-result v7

    if-gt p2, v7, :cond_2

    goto :goto_0

    :cond_2
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, p1}, Lmiui/date/Calendar;->getActualMinimum(I)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, p1}, Lmiui/date/Calendar;->getActualMaximum(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_3
    :goto_0
    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    const/4 v1, 0x0

    aput v1, v0, v5

    :goto_1
    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    aput p2, v0, v3

    aget v1, v0, v5

    if-ne v1, v4, :cond_4

    aget v0, v0, v6

    invoke-static {v0}, Lmiui/date/Calendar;->leapDaysInChineseYear(I)I

    move-result v0

    goto :goto_2

    :cond_4
    aget v0, v0, v6

    invoke-static {v0, p2}, Lmiui/date/Calendar;->daysInChineseMonth(II)I

    move-result v0

    :goto_2
    nop

    iget-object v1, p0, Lmiui/date/Calendar;->mFields:[I

    const/16 v2, 0xa

    aget v3, v1, v2

    if-le v3, v0, :cond_5

    aput v0, v1, v2

    :cond_5
    invoke-direct {p0}, Lmiui/date/Calendar;->onChineseDateChange()V

    return-object p0

    :cond_6
    invoke-virtual {p0, p1}, Lmiui/date/Calendar;->getActualMinimum(I)I

    move-result v3

    if-ge p2, v3, :cond_8

    invoke-virtual {p0, p1}, Lmiui/date/Calendar;->getActualMaximum(I)I

    move-result v3

    if-gt p2, v3, :cond_7

    goto :goto_3

    :cond_7
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, p1}, Lmiui/date/Calendar;->getActualMinimum(I)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, p1}, Lmiui/date/Calendar;->getActualMaximum(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_8
    :goto_3
    :try_start_0
    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    aget v0, v0, p1

    sub-int v0, p2, v0

    invoke-virtual {p0, p1, v0}, Lmiui/date/Calendar;->add(II)Lmiui/date/Calendar;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    nop

    return-object p0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unsupported set field:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lmiui/date/Calendar;->FIELD_NAMES:[Ljava/lang/String;

    aget-object v3, v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public set(IIIIIII)Lmiui/date/Calendar;
    .locals 3

    const-string v0, "Year "

    if-ltz p2, :cond_5

    const/16 v1, 0xb

    if-gt p2, v1, :cond_5

    if-lez p3, :cond_4

    invoke-virtual {p0, p1}, Lmiui/date/Calendar;->isLeapYear(I)Z

    move-result v1

    invoke-static {v1, p2}, Lmiui/date/Calendar;->daysInMonth(ZI)I

    move-result v1

    if-gt p3, v1, :cond_4

    if-ltz p4, :cond_3

    const/16 v0, 0x17

    if-gt p4, v0, :cond_3

    if-ltz p5, :cond_2

    const/16 v0, 0x3b

    if-gt p5, v0, :cond_2

    if-ltz p6, :cond_1

    if-gt p6, v0, :cond_1

    if-ltz p7, :cond_0

    const/16 v0, 0x3e7

    if-gt p7, v0, :cond_0

    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    const/4 v1, 0x1

    aput p1, v0, v1

    const/4 v1, 0x5

    aput p2, v0, v1

    const/16 v1, 0x9

    aput p3, v0, v1

    const/16 v1, 0x12

    aput p4, v0, v1

    const/16 v1, 0x14

    aput p5, v0, v1

    const/16 v1, 0x15

    aput p6, v0, v1

    const/16 v1, 0x16

    aput p7, v0, v1

    invoke-direct {p0}, Lmiui/date/Calendar;->onSolarDateChange()V

    return-object p0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid millisecond "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid second "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid minute "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid hour "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " month "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " has no day "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " has no month "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setChineseTime(IIIZIIII)Lmiui/date/Calendar;
    .locals 5

    const/16 v0, 0x76c

    if-lt p1, v0, :cond_a

    const/16 v0, 0x834

    if-gt p1, v0, :cond_a

    const-string v0, "Year "

    if-ltz p2, :cond_9

    const/16 v1, 0xb

    if-gt p2, v1, :cond_9

    if-eqz p4, :cond_1

    invoke-static {p1}, Lmiui/date/Calendar;->leapChineseMonth(I)I

    move-result v1

    if-ne v1, p2, :cond_0

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " has no leap month "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    :goto_0
    const-string v1, " has no day "

    const-string v2, " month "

    if-eqz p4, :cond_3

    if-lez p3, :cond_2

    invoke-static {p1}, Lmiui/date/Calendar;->leapDaysInChineseYear(I)I

    move-result v3

    if-gt p3, v3, :cond_2

    goto :goto_1

    :cond_2
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_3
    if-lez p3, :cond_8

    invoke-static {p1, p2}, Lmiui/date/Calendar;->daysInChineseMonth(II)I

    move-result v3

    if-gt p3, v3, :cond_8

    :goto_1
    if-ltz p5, :cond_7

    const/16 v0, 0x17

    if-gt p5, v0, :cond_7

    if-ltz p6, :cond_6

    const/16 v0, 0x3b

    if-gt p6, v0, :cond_6

    if-ltz p7, :cond_5

    if-gt p7, v0, :cond_5

    if-ltz p8, :cond_4

    const/16 v0, 0x3e8

    if-gt p8, v0, :cond_4

    iget-object v0, p0, Lmiui/date/Calendar;->mFields:[I

    const/4 v1, 0x2

    aput p1, v0, v1

    const/4 v1, 0x6

    aput p2, v0, v1

    const/16 v1, 0xa

    aput p3, v0, v1

    const/16 v1, 0x8

    aput p4, v0, v1

    const/16 v1, 0x12

    aput p5, v0, v1

    const/16 v1, 0x14

    aput p6, v0, v1

    const/16 v1, 0x15

    aput p7, v0, v1

    const/16 v1, 0x16

    aput p8, v0, v1

    invoke-direct {p0}, Lmiui/date/Calendar;->onChineseDateChange()V

    return-object p0

    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid millisecond "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid second "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid minute "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid hour "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_9
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " has no month "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Date out of range [1900 - 2100] expected, but year is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setGregorianChange(J)V
    .locals 6

    iput-wide p1, p0, Lmiui/date/Calendar;->gregorianCutover:J

    new-instance v0, Lmiui/date/Calendar;

    const-string v1, "GMT"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-direct {v0, v1}, Lmiui/date/Calendar;-><init>(Ljava/util/TimeZone;)V

    invoke-virtual {v0, p1, p2}, Lmiui/date/Calendar;->setTimeInMillis(J)Lmiui/date/Calendar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiui/date/Calendar;->get(I)I

    move-result v2

    iput v2, p0, Lmiui/date/Calendar;->changeYear:I

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lmiui/date/Calendar;->get(I)I

    move-result v3

    if-nez v3, :cond_0

    iget v3, p0, Lmiui/date/Calendar;->changeYear:I

    rsub-int/lit8 v3, v3, 0x1

    iput v3, p0, Lmiui/date/Calendar;->changeYear:I

    :cond_0
    iget v3, p0, Lmiui/date/Calendar;->changeYear:I

    div-int/lit8 v4, v3, 0x64

    div-int/lit16 v5, v3, 0x190

    sub-int/2addr v4, v5

    add-int/lit8 v4, v4, -0x2

    iput v4, p0, Lmiui/date/Calendar;->julianError:I

    add-int/lit16 v5, v3, -0x7d0

    div-int/lit16 v5, v5, 0x190

    add-int/2addr v5, v4

    add-int/lit16 v3, v3, -0x7d0

    div-int/lit8 v3, v3, 0x64

    sub-int/2addr v5, v3

    iput v5, p0, Lmiui/date/Calendar;->julianSkew:I

    const/16 v3, 0xc

    invoke-virtual {v0, v3}, Lmiui/date/Calendar;->get(I)I

    move-result v3

    iget v4, p0, Lmiui/date/Calendar;->julianSkew:I

    if-ge v3, v4, :cond_1

    add-int/lit8 v2, v3, -0x1

    iput v2, p0, Lmiui/date/Calendar;->currentYearSkew:I

    sub-int/2addr v4, v3

    add-int/2addr v4, v1

    iput v4, p0, Lmiui/date/Calendar;->lastYearSkew:I

    goto :goto_0

    :cond_1
    iput v2, p0, Lmiui/date/Calendar;->lastYearSkew:I

    iput v4, p0, Lmiui/date/Calendar;->currentYearSkew:I

    :goto_0
    return-void
.end method

.method public setTimeInMillis(J)Lmiui/date/Calendar;
    .locals 0

    iput-wide p1, p0, Lmiui/date/Calendar;->mMillisecond:J

    invoke-direct {p0}, Lmiui/date/Calendar;->compute()V

    return-object p0
.end method

.method public setTimeZone(Ljava/util/TimeZone;)Lmiui/date/Calendar;
    .locals 2

    if-nez p1, :cond_0

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object p1

    :cond_0
    iget-object v0, p0, Lmiui/date/Calendar;->mTimeZone:Ljava/util/TimeZone;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iput-object p1, p0, Lmiui/date/Calendar;->mTimeZone:Ljava/util/TimeZone;

    invoke-direct {p0}, Lmiui/date/Calendar;->compute()V

    :cond_2
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-static {}, Lmiui/util/Pools;->getStringBuilderPool()Lmiui/util/Pools$Pool;

    move-result-object v0

    invoke-interface {v0}, Lmiui/util/Pools$Pool;->acquire()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "[time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lmiui/date/Calendar;->mMillisecond:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ",zone="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lmiui/date/Calendar;->mTimeZone:Ljava/util/TimeZone;

    invoke-virtual {v1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    :goto_0
    const/16 v2, 0x19

    if-ge v1, v2, :cond_0

    const/16 v2, 0x2c

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    sget-object v2, Lmiui/date/Calendar;->FIELD_NAMES:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x3d

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lmiui/date/Calendar;->mFields:[I

    aget v2, v2, v1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lmiui/util/Pools;->getStringBuilderPool()Lmiui/util/Pools$Pool;

    move-result-object v2

    invoke-interface {v2, v0}, Lmiui/util/Pools$Pool;->release(Ljava/lang/Object;)V

    return-object v1
.end method
