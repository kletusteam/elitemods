.class public Lcom/miui/internal/app/SystemApplication;
.super Landroid/app/Application;
.source "SystemApplication.java"


# static fields
.field public static sInitializingApplication:Landroid/app/Application;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    sput-object p0, Lcom/miui/internal/app/SystemApplication;->sInitializingApplication:Landroid/app/Application;

    return-void
.end method

.method public static getCurrentApplication()Landroid/app/Application;
    .locals 1

    sget-object v0, Lcom/miui/internal/app/SystemApplication;->sInitializingApplication:Landroid/app/Application;

    return-object v0
.end method


# virtual methods
.method public onCreate()V
    .locals 0

    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    return-void
.end method
