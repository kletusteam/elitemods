.class public final Lcom/miui/system/internal/R$array;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/system/internal/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "array"
.end annotation


# static fields
.field public static am_pms:I

.field public static chinese_days:I

.field public static chinese_digits:I

.field public static chinese_leap_months:I

.field public static chinese_months:I

.field public static chinese_symbol_animals:I

.field public static cta_permission_names:I

.field public static cta_permissions:I

.field public static detailed_am_pms:I

.field public static di_zhi:I

.field public static earthly_branches:I

.field public static eras:I

.field public static eu_regions:I

.field public static heavenly_stems:I

.field public static jia_zi:I

.field public static miui_nature_sound_array:I

.field public static months:I

.field public static months_short:I

.field public static months_shortest:I

.field public static religious_languages:I

.field public static religious_regions:I

.field public static solar_terms:I

.field public static tian_gan:I

.field public static week_days:I

.field public static week_days_short:I

.field public static week_days_shortest:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/high16 v0, 0x12050000

    sput v0, Lcom/miui/system/internal/R$array;->am_pms:I

    const v0, 0x12050001

    sput v0, Lcom/miui/system/internal/R$array;->chinese_days:I

    const v0, 0x12050002

    sput v0, Lcom/miui/system/internal/R$array;->chinese_digits:I

    const v0, 0x12050004

    sput v0, Lcom/miui/system/internal/R$array;->chinese_leap_months:I

    const v0, 0x12050005

    sput v0, Lcom/miui/system/internal/R$array;->chinese_months:I

    const v0, 0x12050006

    sput v0, Lcom/miui/system/internal/R$array;->chinese_symbol_animals:I

    const v0, 0x12050007

    sput v0, Lcom/miui/system/internal/R$array;->cta_permission_names:I

    const v0, 0x12050008

    sput v0, Lcom/miui/system/internal/R$array;->cta_permissions:I

    const v0, 0x12050009

    sput v0, Lcom/miui/system/internal/R$array;->detailed_am_pms:I

    const v0, 0x1205000a

    sput v0, Lcom/miui/system/internal/R$array;->di_zhi:I

    const v0, 0x1205000b

    sput v0, Lcom/miui/system/internal/R$array;->earthly_branches:I

    const v0, 0x1205000c

    sput v0, Lcom/miui/system/internal/R$array;->eras:I

    const v0, 0x1205000d

    sput v0, Lcom/miui/system/internal/R$array;->eu_regions:I

    const v0, 0x1205000e

    sput v0, Lcom/miui/system/internal/R$array;->heavenly_stems:I

    const v0, 0x1205000f

    sput v0, Lcom/miui/system/internal/R$array;->jia_zi:I

    const v0, 0x12050003

    sput v0, Lcom/miui/system/internal/R$array;->miui_nature_sound_array:I

    const v0, 0x12050011

    sput v0, Lcom/miui/system/internal/R$array;->months:I

    const v0, 0x12050012

    sput v0, Lcom/miui/system/internal/R$array;->months_short:I

    const v0, 0x12050013

    sput v0, Lcom/miui/system/internal/R$array;->months_shortest:I

    const v0, 0x12050014

    sput v0, Lcom/miui/system/internal/R$array;->religious_languages:I

    const v0, 0x12050015

    sput v0, Lcom/miui/system/internal/R$array;->religious_regions:I

    const v0, 0x12050016

    sput v0, Lcom/miui/system/internal/R$array;->solar_terms:I

    const v0, 0x12050017

    sput v0, Lcom/miui/system/internal/R$array;->tian_gan:I

    const v0, 0x12050018

    sput v0, Lcom/miui/system/internal/R$array;->week_days:I

    const v0, 0x12050019

    sput v0, Lcom/miui/system/internal/R$array;->week_days_short:I

    const v0, 0x1205001a

    sput v0, Lcom/miui/system/internal/R$array;->week_days_shortest:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
