.class public final Lcom/miui/system/internal/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/system/internal/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static afternoon:I

.field public static android_ringtone_default_with_actual:I

.field public static android_ringtone_silent:I

.field public static android_ringtone_unknown:I

.field public static anniversary_of_lifting_martial_law:I

.field public static anti_aggression_day:I

.field public static arbor_day:I

.field public static armed_forces_day:I

.field public static autumn_begins:I

.field public static autumn_equinox:I

.field public static childrens_day:I

.field public static chinese_youth_day:I

.field public static christmas_day:I

.field public static clear_and_bright:I

.field public static cloud_state_disabled:I

.field public static cloud_state_finished:I

.field public static cloud_state_syncing:I

.field public static cold_dews:I

.field public static cta_button_continue:I

.field public static cta_button_quit:I

.field public static cta_check_reminder:I

.field public static cta_message_license:I

.field public static cta_message_permission:I

.field public static cta_permission_and:I

.field public static cta_permission_delimiter:I

.field public static cta_title:I

.field public static def_alarm_alert:I

.field public static def_notification_sound:I

.field public static def_ringtone:I

.field public static def_ringtone_slot_1:I

.field public static def_ringtone_slot_2:I

.field public static def_sms_delivered_sound:I

.field public static def_sms_delivered_sound_slot_1:I

.field public static def_sms_delivered_sound_slot_2:I

.field public static def_sms_received_sound:I

.field public static def_sms_received_sound_slot_1:I

.field public static def_sms_received_sound_slot_2:I

.field public static device_hongmi:I

.field public static device_pad:I

.field public static device_poco:I

.field public static device_poco_global:I

.field public static device_poco_india:I

.field public static device_redmi:I

.field public static device_xiaomi:I

.field public static double_seventh_day:I

.field public static early_morning:I

.field public static easter:I

.field public static empty:I

.field public static evening:I

.field public static fmt_chinese_date:I

.field public static fmt_date:I

.field public static fmt_date_day:I

.field public static fmt_date_long_month:I

.field public static fmt_date_long_month_day:I

.field public static fmt_date_long_year_month:I

.field public static fmt_date_long_year_month_day:I

.field public static fmt_date_numeric_day:I

.field public static fmt_date_numeric_month:I

.field public static fmt_date_numeric_month_day:I

.field public static fmt_date_numeric_year:I

.field public static fmt_date_numeric_year_month:I

.field public static fmt_date_numeric_year_month_day:I

.field public static fmt_date_short_month:I

.field public static fmt_date_short_month_day:I

.field public static fmt_date_short_year_month:I

.field public static fmt_date_short_year_month_day:I

.field public static fmt_date_time:I

.field public static fmt_date_time_timezone:I

.field public static fmt_date_timezone:I

.field public static fmt_date_year:I

.field public static fmt_time:I

.field public static fmt_time_12hour:I

.field public static fmt_time_12hour_minute:I

.field public static fmt_time_12hour_minute_pm:I

.field public static fmt_time_12hour_minute_second:I

.field public static fmt_time_12hour_minute_second_millis:I

.field public static fmt_time_12hour_minute_second_millis_pm:I

.field public static fmt_time_12hour_minute_second_pm:I

.field public static fmt_time_12hour_pm:I

.field public static fmt_time_24hour:I

.field public static fmt_time_24hour_minute:I

.field public static fmt_time_24hour_minute_second:I

.field public static fmt_time_24hour_minute_second_millis:I

.field public static fmt_time_millis:I

.field public static fmt_time_minute:I

.field public static fmt_time_minute_second:I

.field public static fmt_time_minute_second_millis:I

.field public static fmt_time_second:I

.field public static fmt_time_second_millis:I

.field public static fmt_time_timezone:I

.field public static fmt_timezone:I

.field public static fmt_weekday:I

.field public static fmt_weekday_date:I

.field public static fmt_weekday_date_time:I

.field public static fmt_weekday_date_time_timezone:I

.field public static fmt_weekday_date_timezone:I

.field public static fmt_weekday_long:I

.field public static fmt_weekday_short:I

.field public static fmt_weekday_time:I

.field public static fmt_weekday_time_timezone:I

.field public static fmt_weekday_timezone:I

.field public static fools_day:I

.field public static grain_buds:I

.field public static grain_in_ear:I

.field public static grain_rain:I

.field public static great_cold:I

.field public static great_heat:I

.field public static group_name_contacts:I

.field public static group_name_coworkers:I

.field public static group_name_family:I

.field public static group_name_friends:I

.field public static heavy_snow:I

.field public static hksar_establishment_day:I

.field public static hoar_frost_falls:I

.field public static insects_awaken:I

.field public static international_womens_day:I

.field public static labour_day:I

.field public static lantern_festival:I

.field public static light_snow:I

.field public static lunar_ba:I

.field public static lunar_chu:I

.field public static lunar_chu_shi:I

.field public static lunar_er:I

.field public static lunar_er_shi:I

.field public static lunar_jiu:I

.field public static lunar_leap:I

.field public static lunar_ling:I

.field public static lunar_liu:I

.field public static lunar_nian:I

.field public static lunar_qi:I

.field public static lunar_san:I

.field public static lunar_san_shi:I

.field public static lunar_shi:I

.field public static lunar_shi_er:I

.field public static lunar_shi_yi:I

.field public static lunar_si:I

.field public static lunar_wu:I

.field public static lunar_year:I

.field public static lunar_yi:I

.field public static lunar_yue:I

.field public static lunar_zheng:I

.field public static midnight:I

.field public static miui_clock_city_name_local:I

.field public static miui_clock_city_name_second:I

.field public static miui_clock_fancy_colon:I

.field public static miui_device_name:I

.field public static miui_digital_clock_dot:I

.field public static miui_lock_screen_date:I

.field public static miui_lock_screen_date_12:I

.field public static miui_lock_screen_date_two_lines:I

.field public static miui_lock_screen_date_two_lines_12:I

.field public static miui_lock_screen_large_date:I

.field public static miui_lock_screen_large_date_12:I

.field public static miui_vertical_time_format_12:I

.field public static miui_vertical_time_format_24:I

.field public static morning:I

.field public static national_day:I

.field public static national_father_day:I

.field public static new_years_day:I

.field public static night:I

.field public static noon:I

.field public static notice_audition:I

.field public static partys_day:I

.field public static peace_day:I

.field public static presentation_payphone:I

.field public static presentation_private:I

.field public static presentation_unknown:I

.field public static retrocession_day:I

.field public static slight_cold:I

.field public static slight_heat:I

.field public static spring_begins:I

.field public static stopping_the_heat:I

.field public static summer_begins:I

.field public static summer_solstice:I

.field public static teachers_day:I

.field public static the_armys_day:I

.field public static the_double_ninth_festival:I

.field public static the_dragon_boat_festival:I

.field public static the_laba_rice_porridge_festival:I

.field public static the_mid_autumn_festival:I

.field public static the_rains:I

.field public static the_spring_festival:I

.field public static today:I

.field public static tomorrow:I

.field public static tw_childrens_day:I

.field public static tw_youth_day:I

.field public static united_nations_day:I

.field public static valentines_day:I

.field public static vernal_equinox:I

.field public static white_dews:I

.field public static winter_begins:I

.field public static winter_solstice:I

.field public static yellow_page_module_sms_sent:I

.field public static yellow_page_sms_disabled_in_privacy_mode:I

.field public static yellow_page_sms_send_failed:I

.field public static yellow_page_sms_sending:I

.field public static yellow_page_unable_send_sms:I

.field public static yesterday:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const v0, 0x1207001b

    sput v0, Lcom/miui/system/internal/R$string;->afternoon:I

    const v0, 0x1207001e

    sput v0, Lcom/miui/system/internal/R$string;->android_ringtone_default_with_actual:I

    const/high16 v0, 0x12070000

    sput v0, Lcom/miui/system/internal/R$string;->android_ringtone_silent:I

    const v0, 0x12070001

    sput v0, Lcom/miui/system/internal/R$string;->android_ringtone_unknown:I

    const v0, 0x1207001f

    sput v0, Lcom/miui/system/internal/R$string;->anniversary_of_lifting_martial_law:I

    const v0, 0x1207002a

    sput v0, Lcom/miui/system/internal/R$string;->anti_aggression_day:I

    const v0, 0x1207002b

    sput v0, Lcom/miui/system/internal/R$string;->arbor_day:I

    const v0, 0x1207002c

    sput v0, Lcom/miui/system/internal/R$string;->armed_forces_day:I

    const v0, 0x1207002f

    sput v0, Lcom/miui/system/internal/R$string;->autumn_begins:I

    const v0, 0x12070032

    sput v0, Lcom/miui/system/internal/R$string;->autumn_equinox:I

    const v0, 0x12070041

    sput v0, Lcom/miui/system/internal/R$string;->childrens_day:I

    const v0, 0x12070089

    sput v0, Lcom/miui/system/internal/R$string;->chinese_youth_day:I

    const v0, 0x1207008c

    sput v0, Lcom/miui/system/internal/R$string;->christmas_day:I

    const v0, 0x1207008f

    sput v0, Lcom/miui/system/internal/R$string;->clear_and_bright:I

    const v0, 0x12070090

    sput v0, Lcom/miui/system/internal/R$string;->cloud_state_disabled:I

    const v0, 0x12070091

    sput v0, Lcom/miui/system/internal/R$string;->cloud_state_finished:I

    const v0, 0x12070092

    sput v0, Lcom/miui/system/internal/R$string;->cloud_state_syncing:I

    const v0, 0x12070093

    sput v0, Lcom/miui/system/internal/R$string;->cold_dews:I

    const v0, 0x12070098

    sput v0, Lcom/miui/system/internal/R$string;->cta_button_continue:I

    const v0, 0x12070099

    sput v0, Lcom/miui/system/internal/R$string;->cta_button_quit:I

    const v0, 0x1207009a

    sput v0, Lcom/miui/system/internal/R$string;->cta_check_reminder:I

    const v0, 0x1207009b

    sput v0, Lcom/miui/system/internal/R$string;->cta_message_license:I

    const v0, 0x1207009c

    sput v0, Lcom/miui/system/internal/R$string;->cta_message_permission:I

    const v0, 0x1207009d

    sput v0, Lcom/miui/system/internal/R$string;->cta_permission_and:I

    const v0, 0x1207009e

    sput v0, Lcom/miui/system/internal/R$string;->cta_permission_delimiter:I

    const v0, 0x1207009f

    sput v0, Lcom/miui/system/internal/R$string;->cta_title:I

    const v0, 0x12070002

    sput v0, Lcom/miui/system/internal/R$string;->def_alarm_alert:I

    const v0, 0x12070003

    sput v0, Lcom/miui/system/internal/R$string;->def_notification_sound:I

    const v0, 0x12070004

    sput v0, Lcom/miui/system/internal/R$string;->def_ringtone:I

    const v0, 0x12070005

    sput v0, Lcom/miui/system/internal/R$string;->def_ringtone_slot_1:I

    const v0, 0x12070006

    sput v0, Lcom/miui/system/internal/R$string;->def_ringtone_slot_2:I

    const v0, 0x12070007

    sput v0, Lcom/miui/system/internal/R$string;->def_sms_delivered_sound:I

    const v0, 0x12070008

    sput v0, Lcom/miui/system/internal/R$string;->def_sms_delivered_sound_slot_1:I

    const v0, 0x12070009

    sput v0, Lcom/miui/system/internal/R$string;->def_sms_delivered_sound_slot_2:I

    const v0, 0x1207000a

    sput v0, Lcom/miui/system/internal/R$string;->def_sms_received_sound:I

    const v0, 0x1207000b

    sput v0, Lcom/miui/system/internal/R$string;->def_sms_received_sound_slot_1:I

    const v0, 0x1207000c

    sput v0, Lcom/miui/system/internal/R$string;->def_sms_received_sound_slot_2:I

    const v0, 0x12070015

    sput v0, Lcom/miui/system/internal/R$string;->device_hongmi:I

    const v0, 0x12070016

    sput v0, Lcom/miui/system/internal/R$string;->device_pad:I

    const v0, 0x120700a3

    sput v0, Lcom/miui/system/internal/R$string;->device_poco:I

    const v0, 0x120700a4

    sput v0, Lcom/miui/system/internal/R$string;->device_poco_global:I

    const v0, 0x120700a5

    sput v0, Lcom/miui/system/internal/R$string;->device_poco_india:I

    const v0, 0x120700a6

    sput v0, Lcom/miui/system/internal/R$string;->device_redmi:I

    const v0, 0x12070017

    sput v0, Lcom/miui/system/internal/R$string;->device_xiaomi:I

    const v0, 0x120700a9

    sput v0, Lcom/miui/system/internal/R$string;->double_seventh_day:I

    const v0, 0x120700aa

    sput v0, Lcom/miui/system/internal/R$string;->early_morning:I

    const v0, 0x120700b7

    sput v0, Lcom/miui/system/internal/R$string;->easter:I

    const v0, 0x120700b9

    sput v0, Lcom/miui/system/internal/R$string;->empty:I

    const v0, 0x120700bc

    sput v0, Lcom/miui/system/internal/R$string;->evening:I

    const v0, 0x120700c5

    sput v0, Lcom/miui/system/internal/R$string;->fmt_chinese_date:I

    const v0, 0x120700c6

    sput v0, Lcom/miui/system/internal/R$string;->fmt_date:I

    const v0, 0x120700c7

    sput v0, Lcom/miui/system/internal/R$string;->fmt_date_day:I

    const v0, 0x120700c8

    sput v0, Lcom/miui/system/internal/R$string;->fmt_date_long_month:I

    const v0, 0x120700c9

    sput v0, Lcom/miui/system/internal/R$string;->fmt_date_long_month_day:I

    const v0, 0x120700ca

    sput v0, Lcom/miui/system/internal/R$string;->fmt_date_long_year_month:I

    const v0, 0x120700cb

    sput v0, Lcom/miui/system/internal/R$string;->fmt_date_long_year_month_day:I

    const v0, 0x120700cc

    sput v0, Lcom/miui/system/internal/R$string;->fmt_date_numeric_day:I

    const v0, 0x120700cd

    sput v0, Lcom/miui/system/internal/R$string;->fmt_date_numeric_month:I

    const v0, 0x120700ce

    sput v0, Lcom/miui/system/internal/R$string;->fmt_date_numeric_month_day:I

    const v0, 0x120700cf

    sput v0, Lcom/miui/system/internal/R$string;->fmt_date_numeric_year:I

    const v0, 0x120700d0

    sput v0, Lcom/miui/system/internal/R$string;->fmt_date_numeric_year_month:I

    const v0, 0x120700d1

    sput v0, Lcom/miui/system/internal/R$string;->fmt_date_numeric_year_month_day:I

    const v0, 0x120700d2

    sput v0, Lcom/miui/system/internal/R$string;->fmt_date_short_month:I

    const v0, 0x120700d3

    sput v0, Lcom/miui/system/internal/R$string;->fmt_date_short_month_day:I

    const v0, 0x120700d4

    sput v0, Lcom/miui/system/internal/R$string;->fmt_date_short_year_month:I

    const v0, 0x120700d5

    sput v0, Lcom/miui/system/internal/R$string;->fmt_date_short_year_month_day:I

    const v0, 0x120700d6

    sput v0, Lcom/miui/system/internal/R$string;->fmt_date_time:I

    const v0, 0x120700d7

    sput v0, Lcom/miui/system/internal/R$string;->fmt_date_time_timezone:I

    const v0, 0x120700d8

    sput v0, Lcom/miui/system/internal/R$string;->fmt_date_timezone:I

    const v0, 0x120700d9

    sput v0, Lcom/miui/system/internal/R$string;->fmt_date_year:I

    const v0, 0x120700da

    sput v0, Lcom/miui/system/internal/R$string;->fmt_time:I

    const v0, 0x120700db

    sput v0, Lcom/miui/system/internal/R$string;->fmt_time_12hour:I

    const v0, 0x120700dc

    sput v0, Lcom/miui/system/internal/R$string;->fmt_time_12hour_minute:I

    const v0, 0x120700dd

    sput v0, Lcom/miui/system/internal/R$string;->fmt_time_12hour_minute_pm:I

    const v0, 0x120700de

    sput v0, Lcom/miui/system/internal/R$string;->fmt_time_12hour_minute_second:I

    const v0, 0x120700df

    sput v0, Lcom/miui/system/internal/R$string;->fmt_time_12hour_minute_second_millis:I

    const v0, 0x120700e0

    sput v0, Lcom/miui/system/internal/R$string;->fmt_time_12hour_minute_second_millis_pm:I

    const v0, 0x120700e1

    sput v0, Lcom/miui/system/internal/R$string;->fmt_time_12hour_minute_second_pm:I

    const v0, 0x120700e2

    sput v0, Lcom/miui/system/internal/R$string;->fmt_time_12hour_pm:I

    const v0, 0x120700e3

    sput v0, Lcom/miui/system/internal/R$string;->fmt_time_24hour:I

    const v0, 0x120700e4

    sput v0, Lcom/miui/system/internal/R$string;->fmt_time_24hour_minute:I

    const v0, 0x120700e5

    sput v0, Lcom/miui/system/internal/R$string;->fmt_time_24hour_minute_second:I

    const v0, 0x120700e6

    sput v0, Lcom/miui/system/internal/R$string;->fmt_time_24hour_minute_second_millis:I

    const v0, 0x120700e7

    sput v0, Lcom/miui/system/internal/R$string;->fmt_time_millis:I

    const v0, 0x120700e8

    sput v0, Lcom/miui/system/internal/R$string;->fmt_time_minute:I

    const v0, 0x120700e9

    sput v0, Lcom/miui/system/internal/R$string;->fmt_time_minute_second:I

    const v0, 0x120700ea

    sput v0, Lcom/miui/system/internal/R$string;->fmt_time_minute_second_millis:I

    const v0, 0x120700eb

    sput v0, Lcom/miui/system/internal/R$string;->fmt_time_second:I

    const v0, 0x120700ec

    sput v0, Lcom/miui/system/internal/R$string;->fmt_time_second_millis:I

    const v0, 0x120700ed

    sput v0, Lcom/miui/system/internal/R$string;->fmt_time_timezone:I

    const v0, 0x120700ee

    sput v0, Lcom/miui/system/internal/R$string;->fmt_timezone:I

    const v0, 0x120700ef

    sput v0, Lcom/miui/system/internal/R$string;->fmt_weekday:I

    const v0, 0x120700f0

    sput v0, Lcom/miui/system/internal/R$string;->fmt_weekday_date:I

    const v0, 0x120700f1

    sput v0, Lcom/miui/system/internal/R$string;->fmt_weekday_date_time:I

    const v0, 0x120700f2

    sput v0, Lcom/miui/system/internal/R$string;->fmt_weekday_date_time_timezone:I

    const v0, 0x120700f3

    sput v0, Lcom/miui/system/internal/R$string;->fmt_weekday_date_timezone:I

    const v0, 0x120700f4

    sput v0, Lcom/miui/system/internal/R$string;->fmt_weekday_long:I

    const v0, 0x120700f5

    sput v0, Lcom/miui/system/internal/R$string;->fmt_weekday_short:I

    const v0, 0x120700f6

    sput v0, Lcom/miui/system/internal/R$string;->fmt_weekday_time:I

    const v0, 0x120700f7

    sput v0, Lcom/miui/system/internal/R$string;->fmt_weekday_time_timezone:I

    const v0, 0x120700f8

    sput v0, Lcom/miui/system/internal/R$string;->fmt_weekday_timezone:I

    const v0, 0x120700f9

    sput v0, Lcom/miui/system/internal/R$string;->fools_day:I

    const v0, 0x12070105

    sput v0, Lcom/miui/system/internal/R$string;->grain_buds:I

    const v0, 0x12070106

    sput v0, Lcom/miui/system/internal/R$string;->grain_in_ear:I

    const v0, 0x12070107

    sput v0, Lcom/miui/system/internal/R$string;->grain_rain:I

    const v0, 0x12070108

    sput v0, Lcom/miui/system/internal/R$string;->great_cold:I

    const v0, 0x12070109

    sput v0, Lcom/miui/system/internal/R$string;->great_heat:I

    const v0, 0x1207010b

    sput v0, Lcom/miui/system/internal/R$string;->group_name_contacts:I

    const v0, 0x1207010c

    sput v0, Lcom/miui/system/internal/R$string;->group_name_coworkers:I

    const v0, 0x1207010d

    sput v0, Lcom/miui/system/internal/R$string;->group_name_family:I

    const v0, 0x1207010e

    sput v0, Lcom/miui/system/internal/R$string;->group_name_friends:I

    const v0, 0x1207011d

    sput v0, Lcom/miui/system/internal/R$string;->heavy_snow:I

    const v0, 0x1207011e

    sput v0, Lcom/miui/system/internal/R$string;->hksar_establishment_day:I

    const v0, 0x1207011f

    sput v0, Lcom/miui/system/internal/R$string;->hoar_frost_falls:I

    const v0, 0x12070122

    sput v0, Lcom/miui/system/internal/R$string;->insects_awaken:I

    const v0, 0x12070123

    sput v0, Lcom/miui/system/internal/R$string;->international_womens_day:I

    const v0, 0x12070125

    sput v0, Lcom/miui/system/internal/R$string;->labour_day:I

    const v0, 0x12070126    # 4.2599904E-28f

    sput v0, Lcom/miui/system/internal/R$string;->lantern_festival:I

    const v0, 0x12070128    # 4.2599914E-28f

    sput v0, Lcom/miui/system/internal/R$string;->light_snow:I

    const v0, 0x1207012a    # 4.2599924E-28f

    sput v0, Lcom/miui/system/internal/R$string;->lunar_ba:I

    const v0, 0x1207012c    # 4.2599933E-28f

    sput v0, Lcom/miui/system/internal/R$string;->lunar_chu:I

    const v0, 0x1207012d    # 4.259994E-28f

    sput v0, Lcom/miui/system/internal/R$string;->lunar_chu_shi:I

    const v0, 0x1207012e    # 4.2599943E-28f

    sput v0, Lcom/miui/system/internal/R$string;->lunar_er:I

    const v0, 0x1207012f    # 4.259995E-28f

    sput v0, Lcom/miui/system/internal/R$string;->lunar_er_shi:I

    const v0, 0x12070130    # 4.2599953E-28f

    sput v0, Lcom/miui/system/internal/R$string;->lunar_jiu:I

    const v0, 0x12070131    # 4.2599957E-28f

    sput v0, Lcom/miui/system/internal/R$string;->lunar_leap:I

    const v0, 0x12070132    # 4.259996E-28f

    sput v0, Lcom/miui/system/internal/R$string;->lunar_ling:I

    const v0, 0x12070133    # 4.2599967E-28f

    sput v0, Lcom/miui/system/internal/R$string;->lunar_liu:I

    const v0, 0x12070134    # 4.259997E-28f

    sput v0, Lcom/miui/system/internal/R$string;->lunar_nian:I

    const v0, 0x12070135    # 4.2599977E-28f

    sput v0, Lcom/miui/system/internal/R$string;->lunar_qi:I

    const v0, 0x12070136    # 4.259998E-28f

    sput v0, Lcom/miui/system/internal/R$string;->lunar_san:I

    const v0, 0x12070137    # 4.2599986E-28f

    sput v0, Lcom/miui/system/internal/R$string;->lunar_san_shi:I

    const v0, 0x12070138    # 4.259999E-28f

    sput v0, Lcom/miui/system/internal/R$string;->lunar_shi:I

    const v0, 0x12070139    # 4.2599996E-28f

    sput v0, Lcom/miui/system/internal/R$string;->lunar_shi_er:I

    const v0, 0x1207013a    # 4.26E-28f

    sput v0, Lcom/miui/system/internal/R$string;->lunar_shi_yi:I

    const v0, 0x1207013b    # 4.2600006E-28f

    sput v0, Lcom/miui/system/internal/R$string;->lunar_si:I

    const v0, 0x1207013c    # 4.260001E-28f

    sput v0, Lcom/miui/system/internal/R$string;->lunar_wu:I

    const v0, 0x1207013d    # 4.2600015E-28f

    sput v0, Lcom/miui/system/internal/R$string;->lunar_year:I

    const v0, 0x1207013e    # 4.260002E-28f

    sput v0, Lcom/miui/system/internal/R$string;->lunar_yi:I

    const v0, 0x1207013f    # 4.2600025E-28f

    sput v0, Lcom/miui/system/internal/R$string;->lunar_yue:I

    const v0, 0x12070140    # 4.260003E-28f

    sput v0, Lcom/miui/system/internal/R$string;->lunar_zheng:I

    const v0, 0x12070145    # 4.2600054E-28f

    sput v0, Lcom/miui/system/internal/R$string;->midnight:I

    const v0, 0x12070029

    sput v0, Lcom/miui/system/internal/R$string;->miui_clock_city_name_local:I

    const v0, 0x12070031

    sput v0, Lcom/miui/system/internal/R$string;->miui_clock_city_name_second:I

    const v0, 0x12070030

    sput v0, Lcom/miui/system/internal/R$string;->miui_clock_fancy_colon:I

    const v0, 0x12070018

    sput v0, Lcom/miui/system/internal/R$string;->miui_device_name:I

    const v0, 0x12070028

    sput v0, Lcom/miui/system/internal/R$string;->miui_digital_clock_dot:I

    const v0, 0x12070020

    sput v0, Lcom/miui/system/internal/R$string;->miui_lock_screen_date:I

    const v0, 0x12070021

    sput v0, Lcom/miui/system/internal/R$string;->miui_lock_screen_date_12:I

    const v0, 0x12070024

    sput v0, Lcom/miui/system/internal/R$string;->miui_lock_screen_date_two_lines:I

    const v0, 0x12070025

    sput v0, Lcom/miui/system/internal/R$string;->miui_lock_screen_date_two_lines_12:I

    const v0, 0x12070022

    sput v0, Lcom/miui/system/internal/R$string;->miui_lock_screen_large_date:I

    const v0, 0x12070023

    sput v0, Lcom/miui/system/internal/R$string;->miui_lock_screen_large_date_12:I

    const v0, 0x12070027

    sput v0, Lcom/miui/system/internal/R$string;->miui_vertical_time_format_12:I

    const v0, 0x12070026

    sput v0, Lcom/miui/system/internal/R$string;->miui_vertical_time_format_24:I

    const v0, 0x12070173

    sput v0, Lcom/miui/system/internal/R$string;->morning:I

    const v0, 0x12070175

    sput v0, Lcom/miui/system/internal/R$string;->national_day:I

    const v0, 0x12070176

    sput v0, Lcom/miui/system/internal/R$string;->national_father_day:I

    const v0, 0x12070177

    sput v0, Lcom/miui/system/internal/R$string;->new_years_day:I

    const v0, 0x12070178

    sput v0, Lcom/miui/system/internal/R$string;->night:I

    const v0, 0x1207017a

    sput v0, Lcom/miui/system/internal/R$string;->noon:I

    const v0, 0x12070019

    sput v0, Lcom/miui/system/internal/R$string;->notice_audition:I

    const v0, 0x1207017d

    sput v0, Lcom/miui/system/internal/R$string;->partys_day:I

    const v0, 0x1207017f

    sput v0, Lcom/miui/system/internal/R$string;->peace_day:I

    const v0, 0x1207000d

    sput v0, Lcom/miui/system/internal/R$string;->presentation_payphone:I

    const v0, 0x1207000e

    sput v0, Lcom/miui/system/internal/R$string;->presentation_private:I

    const v0, 0x1207000f

    sput v0, Lcom/miui/system/internal/R$string;->presentation_unknown:I

    const v0, 0x12070184

    sput v0, Lcom/miui/system/internal/R$string;->retrocession_day:I

    const v0, 0x1207018b

    sput v0, Lcom/miui/system/internal/R$string;->slight_cold:I

    const v0, 0x1207018c

    sput v0, Lcom/miui/system/internal/R$string;->slight_heat:I

    const v0, 0x120701a8

    sput v0, Lcom/miui/system/internal/R$string;->spring_begins:I

    const v0, 0x120701a9

    sput v0, Lcom/miui/system/internal/R$string;->stopping_the_heat:I

    const v0, 0x120701aa

    sput v0, Lcom/miui/system/internal/R$string;->summer_begins:I

    const v0, 0x120701ab

    sput v0, Lcom/miui/system/internal/R$string;->summer_solstice:I

    const v0, 0x120701b2

    sput v0, Lcom/miui/system/internal/R$string;->teachers_day:I

    const v0, 0x120701b3

    sput v0, Lcom/miui/system/internal/R$string;->the_armys_day:I

    const v0, 0x120701b4

    sput v0, Lcom/miui/system/internal/R$string;->the_double_ninth_festival:I

    const v0, 0x120701b5

    sput v0, Lcom/miui/system/internal/R$string;->the_dragon_boat_festival:I

    const v0, 0x120701b7

    sput v0, Lcom/miui/system/internal/R$string;->the_laba_rice_porridge_festival:I

    const v0, 0x120701b8

    sput v0, Lcom/miui/system/internal/R$string;->the_mid_autumn_festival:I

    const v0, 0x120701b9

    sput v0, Lcom/miui/system/internal/R$string;->the_rains:I

    const v0, 0x120701ba

    sput v0, Lcom/miui/system/internal/R$string;->the_spring_festival:I

    const v0, 0x120701be

    sput v0, Lcom/miui/system/internal/R$string;->today:I

    const v0, 0x120701bf

    sput v0, Lcom/miui/system/internal/R$string;->tomorrow:I

    const v0, 0x120701c4

    sput v0, Lcom/miui/system/internal/R$string;->tw_childrens_day:I

    const v0, 0x120701c5

    sput v0, Lcom/miui/system/internal/R$string;->tw_youth_day:I

    const v0, 0x120701c6

    sput v0, Lcom/miui/system/internal/R$string;->united_nations_day:I

    const v0, 0x120701c7

    sput v0, Lcom/miui/system/internal/R$string;->valentines_day:I

    const v0, 0x120701c8

    sput v0, Lcom/miui/system/internal/R$string;->vernal_equinox:I

    const v0, 0x120701de

    sput v0, Lcom/miui/system/internal/R$string;->white_dews:I

    const v0, 0x120701e0

    sput v0, Lcom/miui/system/internal/R$string;->winter_begins:I

    const v0, 0x120701e1

    sput v0, Lcom/miui/system/internal/R$string;->winter_solstice:I

    const v0, 0x12070010

    sput v0, Lcom/miui/system/internal/R$string;->yellow_page_module_sms_sent:I

    const v0, 0x12070011

    sput v0, Lcom/miui/system/internal/R$string;->yellow_page_sms_disabled_in_privacy_mode:I

    const v0, 0x12070012

    sput v0, Lcom/miui/system/internal/R$string;->yellow_page_sms_send_failed:I

    const v0, 0x12070013

    sput v0, Lcom/miui/system/internal/R$string;->yellow_page_sms_sending:I

    const v0, 0x12070014

    sput v0, Lcom/miui/system/internal/R$string;->yellow_page_unable_send_sms:I

    const v0, 0x120701e2

    sput v0, Lcom/miui/system/internal/R$string;->yesterday:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
