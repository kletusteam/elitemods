.class public final Lcom/miui/system/internal/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/system/internal/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static action:I

.field public static app_info:I

.field public static clear_button:I

.field public static content:I

.field public static current_date:I

.field public static current_date_large:I

.field public static current_time:I

.field public static left_top_date_container:I

.field public static list:I

.field public static local_city_name:I

.field public static local_date:I

.field public static local_time:I

.field public static no_notification_tips:I

.field public static resident_city_name:I

.field public static resident_date:I

.field public static resident_time:I

.field public static resident_time_layout:I

.field public static scroll:I

.field public static title:I

.field public static unlock_screen_lunar_calendar_info:I

.field public static unlock_screen_owner_info:I

.field public static veto:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const v0, 0x1204000c

    sput v0, Lcom/miui/system/internal/R$id;->action:I

    const v0, 0x1204000d

    sput v0, Lcom/miui/system/internal/R$id;->app_info:I

    const v0, 0x1204000e

    sput v0, Lcom/miui/system/internal/R$id;->clear_button:I

    const v0, 0x12040010

    sput v0, Lcom/miui/system/internal/R$id;->content:I

    const v0, 0x12040011

    sput v0, Lcom/miui/system/internal/R$id;->current_date:I

    const v0, 0x12040012

    sput v0, Lcom/miui/system/internal/R$id;->current_date_large:I

    const v0, 0x12040013

    sput v0, Lcom/miui/system/internal/R$id;->current_time:I

    const v0, 0x12040015

    sput v0, Lcom/miui/system/internal/R$id;->left_top_date_container:I

    const v0, 0x12040016

    sput v0, Lcom/miui/system/internal/R$id;->list:I

    const v0, 0x12040017

    sput v0, Lcom/miui/system/internal/R$id;->local_city_name:I

    const v0, 0x12040018

    sput v0, Lcom/miui/system/internal/R$id;->local_date:I

    const v0, 0x12040019

    sput v0, Lcom/miui/system/internal/R$id;->local_time:I

    const v0, 0x1204001e    # 4.1652E-28f

    sput v0, Lcom/miui/system/internal/R$id;->no_notification_tips:I

    const v0, 0x1204001f

    sput v0, Lcom/miui/system/internal/R$id;->resident_city_name:I

    const v0, 0x12040020

    sput v0, Lcom/miui/system/internal/R$id;->resident_date:I

    const v0, 0x12040021

    sput v0, Lcom/miui/system/internal/R$id;->resident_time:I

    const v0, 0x12040022

    sput v0, Lcom/miui/system/internal/R$id;->resident_time_layout:I

    const v0, 0x12040023

    sput v0, Lcom/miui/system/internal/R$id;->scroll:I

    const v0, 0x12040024

    sput v0, Lcom/miui/system/internal/R$id;->title:I

    const v0, 0x12040025

    sput v0, Lcom/miui/system/internal/R$id;->unlock_screen_lunar_calendar_info:I

    const v0, 0x12040026

    sput v0, Lcom/miui/system/internal/R$id;->unlock_screen_owner_info:I

    const v0, 0x12040027

    sput v0, Lcom/miui/system/internal/R$id;->veto:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
