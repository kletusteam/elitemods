.class public final Lcom/miui/system/internal/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/system/internal/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static cloud_btn:I

.field public static cloud_state_disabled_bg:I

.field public static cloud_state_enabled_bg:I

.field public static ic_contact_photo_bg:I

.field public static ic_contact_photo_fg:I

.field public static ic_contact_photo_mask:I

.field public static sortable_list_dragging_item_shadow:I

.field public static vip_default_avatar:I

.field public static vip_icon_chalice:I

.field public static vip_icon_default_achievement:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const v0, 0x12020003

    sput v0, Lcom/miui/system/internal/R$drawable;->cloud_btn:I

    const v0, 0x12020006

    sput v0, Lcom/miui/system/internal/R$drawable;->cloud_state_disabled_bg:I

    const v0, 0x12020007

    sput v0, Lcom/miui/system/internal/R$drawable;->cloud_state_enabled_bg:I

    const v0, 0x1202000a

    sput v0, Lcom/miui/system/internal/R$drawable;->ic_contact_photo_bg:I

    const v0, 0x1202000b

    sput v0, Lcom/miui/system/internal/R$drawable;->ic_contact_photo_fg:I

    const v0, 0x1202000c

    sput v0, Lcom/miui/system/internal/R$drawable;->ic_contact_photo_mask:I

    const v0, 0x12020023

    sput v0, Lcom/miui/system/internal/R$drawable;->sortable_list_dragging_item_shadow:I

    const v0, 0x12020025

    sput v0, Lcom/miui/system/internal/R$drawable;->vip_default_avatar:I

    const v0, 0x12020026

    sput v0, Lcom/miui/system/internal/R$drawable;->vip_icon_chalice:I

    const v0, 0x12020027

    sput v0, Lcom/miui/system/internal/R$drawable;->vip_icon_default_achievement:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
