.class public final Lcom/miui/system/internal/R$plurals;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/system/internal/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "plurals"
.end annotation


# static fields
.field public static a_hour_ago:I

.field public static abbrev_a_hour_ago:I

.field public static abbrev_half_hour_ago:I

.field public static abbrev_in_a_hour:I

.field public static abbrev_in_half_hour:I

.field public static abbrev_in_less_than_one_minute:I

.field public static abbrev_in_num_minutes:I

.field public static abbrev_less_than_one_minute_ago:I

.field public static abbrev_num_minutes_ago:I

.field public static half_hour_ago:I

.field public static in_a_hour:I

.field public static in_half_hour:I

.field public static in_less_than_one_minute:I

.field public static in_num_minutes:I

.field public static less_than_one_minute_ago:I

.field public static num_minutes_ago:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/high16 v0, 0x120a0000

    sput v0, Lcom/miui/system/internal/R$plurals;->a_hour_ago:I

    const v0, 0x120a0001

    sput v0, Lcom/miui/system/internal/R$plurals;->abbrev_a_hour_ago:I

    const v0, 0x120a0002

    sput v0, Lcom/miui/system/internal/R$plurals;->abbrev_half_hour_ago:I

    const v0, 0x120a0003

    sput v0, Lcom/miui/system/internal/R$plurals;->abbrev_in_a_hour:I

    const v0, 0x120a0004

    sput v0, Lcom/miui/system/internal/R$plurals;->abbrev_in_half_hour:I

    const v0, 0x120a0005

    sput v0, Lcom/miui/system/internal/R$plurals;->abbrev_in_less_than_one_minute:I

    const v0, 0x120a0006

    sput v0, Lcom/miui/system/internal/R$plurals;->abbrev_in_num_minutes:I

    const v0, 0x120a0007

    sput v0, Lcom/miui/system/internal/R$plurals;->abbrev_less_than_one_minute_ago:I

    const v0, 0x120a0008

    sput v0, Lcom/miui/system/internal/R$plurals;->abbrev_num_minutes_ago:I

    const v0, 0x120a0009

    sput v0, Lcom/miui/system/internal/R$plurals;->half_hour_ago:I

    const v0, 0x120a000a

    sput v0, Lcom/miui/system/internal/R$plurals;->in_a_hour:I

    const v0, 0x120a000b

    sput v0, Lcom/miui/system/internal/R$plurals;->in_half_hour:I

    const v0, 0x120a000c

    sput v0, Lcom/miui/system/internal/R$plurals;->in_less_than_one_minute:I

    const v0, 0x120a000d

    sput v0, Lcom/miui/system/internal/R$plurals;->in_num_minutes:I

    const v0, 0x120a000e

    sput v0, Lcom/miui/system/internal/R$plurals;->less_than_one_minute_ago:I

    const v0, 0x120a000f

    sput v0, Lcom/miui/system/internal/R$plurals;->num_minutes_ago:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
