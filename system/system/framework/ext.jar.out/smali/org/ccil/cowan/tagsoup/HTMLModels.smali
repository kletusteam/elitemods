.class public interface abstract Lorg/ccil/cowan/tagsoup/HTMLModels;
.super Ljava/lang/Object;
.source "HTMLModels.java"


# static fields
.field public static final greylist M_AREA:I = 0x2

.field public static final greylist M_BLOCK:I = 0x4

.field public static final greylist M_BLOCKINLINE:I = 0x8

.field public static final greylist M_BODY:I = 0x10

.field public static final greylist M_CELL:I = 0x20

.field public static final greylist M_COL:I = 0x40

.field public static final greylist M_DEF:I = 0x80

.field public static final greylist M_FORM:I = 0x100

.field public static final greylist M_FRAME:I = 0x200

.field public static final greylist M_HEAD:I = 0x400

.field public static final greylist M_HTML:I = 0x800

.field public static final greylist M_INLINE:I = 0x1000

.field public static final greylist M_LEGEND:I = 0x2000

.field public static final greylist M_LI:I = 0x4000

.field public static final greylist M_NOLINK:I = 0x8000

.field public static final greylist M_OPTION:I = 0x10000

.field public static final greylist M_OPTIONS:I = 0x20000

.field public static final greylist M_P:I = 0x40000

.field public static final greylist M_PARAM:I = 0x80000

.field public static final greylist M_TABLE:I = 0x100000

.field public static final greylist M_TABULAR:I = 0x200000

.field public static final greylist M_TR:I = 0x400000
