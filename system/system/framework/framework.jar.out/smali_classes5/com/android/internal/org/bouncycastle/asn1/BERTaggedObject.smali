.class public Lcom/android/internal/org/bouncycastle/asn1/BERTaggedObject;
.super Lcom/android/internal/org/bouncycastle/asn1/ASN1TaggedObject;


# direct methods
.method public constructor <init>(I)V
    .locals 2

    new-instance v0, Lcom/android/internal/org/bouncycastle/asn1/BERSequence;

    invoke-direct {v0}, Lcom/android/internal/org/bouncycastle/asn1/BERSequence;-><init>()V

    const/4 v1, 0x0

    invoke-direct {p0, v1, p1, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1TaggedObject;-><init>(ZILcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    return-void
.end method

.method public constructor <init>(ILcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1TaggedObject;-><init>(ZILcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    return-void
.end method

.method public constructor <init>(ZILcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1TaggedObject;-><init>(ZILcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    return-void
.end method


# virtual methods
.method encode(Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_e

    nop

    :goto_0
    check-cast v0, Lcom/android/internal/org/bouncycastle/asn1/BEROctetString;

    goto/32 :goto_3e

    nop

    :goto_1
    invoke-virtual {p1, p2, v1, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeTag(ZII)V

    goto/32 :goto_3b

    nop

    :goto_2
    invoke-virtual {p1, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    goto/32 :goto_13

    nop

    :goto_3
    const/4 v1, 0x1

    goto/32 :goto_2a

    nop

    :goto_4
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/BERTaggedObject;->obj:Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_26

    nop

    :goto_5
    instance-of v0, v0, Lcom/android/internal/org/bouncycastle/asn1/BEROctetString;

    goto/32 :goto_20

    nop

    :goto_6
    invoke-virtual {p1, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    goto/32 :goto_27

    nop

    :goto_7
    const/4 v0, 0x0

    goto/32 :goto_6

    nop

    :goto_8
    instance-of v0, v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;

    goto/32 :goto_18

    nop

    :goto_9
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/BERTaggedObject;->obj:Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_0

    nop

    :goto_a
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/BERTaggedObject;->obj:Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_11

    nop

    :goto_b
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;->getObjects()Ljava/util/Enumeration;

    move-result-object v0

    goto/32 :goto_33

    nop

    :goto_c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_f

    nop

    :goto_d
    invoke-interface {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_e
    iget v0, p0, Lcom/android/internal/org/bouncycastle/asn1/BERTaggedObject;->tagNo:I

    goto/32 :goto_12

    nop

    :goto_f
    invoke-direct {v0, v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Exception;-><init>(Ljava/lang/String;)V

    goto/32 :goto_37

    nop

    :goto_10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_c

    nop

    :goto_11
    check-cast v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;

    goto/32 :goto_b

    nop

    :goto_12
    const/16 v1, 0xa0

    goto/32 :goto_1

    nop

    :goto_13
    iget-boolean v0, p0, Lcom/android/internal/org/bouncycastle/asn1/BERTaggedObject;->explicit:Z

    goto/32 :goto_40

    nop

    :goto_14
    invoke-virtual {p1, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeElements(Ljava/util/Enumeration;)V

    goto/32 :goto_31

    nop

    :goto_15
    return-void

    :goto_16
    iget-object v2, p0, Lcom/android/internal/org/bouncycastle/asn1/BERTaggedObject;->obj:Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_24

    nop

    :goto_17
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OctetString;->getOctets()[B

    move-result-object v2

    goto/32 :goto_2d

    nop

    :goto_18
    if-nez v0, :cond_0

    goto/32 :goto_34

    :cond_0
    goto/32 :goto_a

    nop

    :goto_19
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Set;->getObjects()Ljava/util/Enumeration;

    move-result-object v0

    :goto_1a
    goto/32 :goto_14

    nop

    :goto_1b
    if-nez v0, :cond_1

    goto/32 :goto_2f

    :cond_1
    goto/32 :goto_25

    nop

    :goto_1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_16

    nop

    :goto_1d
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_10

    nop

    :goto_1e
    invoke-virtual {v1}, Lcom/android/internal/org/bouncycastle/asn1/BEROctetString;->getObjects()Ljava/util/Enumeration;

    move-result-object v0

    goto/32 :goto_2e

    nop

    :goto_1f
    instance-of v0, v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1OctetString;

    goto/32 :goto_1b

    nop

    :goto_20
    if-nez v0, :cond_2

    goto/32 :goto_3d

    :cond_2
    goto/32 :goto_9

    nop

    :goto_21
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_23

    nop

    :goto_22
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/BERTaggedObject;->obj:Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_3a

    nop

    :goto_23
    const-string/jumbo v2, "not implemented: "

    goto/32 :goto_1c

    nop

    :goto_24
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    goto/32 :goto_1d

    nop

    :goto_25
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/BERTaggedObject;->obj:Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_5

    nop

    :goto_26
    instance-of v0, v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Set;

    goto/32 :goto_36

    nop

    :goto_27
    invoke-virtual {p1, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    goto/32 :goto_15

    nop

    :goto_28
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/BERTaggedObject;->obj:Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_d

    nop

    :goto_29
    check-cast v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Set;

    goto/32 :goto_19

    nop

    :goto_2a
    invoke-virtual {p1, v0, v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writePrimitive(Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;Z)V

    :goto_2b
    goto/32 :goto_7

    nop

    :goto_2c
    new-instance v1, Lcom/android/internal/org/bouncycastle/asn1/BEROctetString;

    goto/32 :goto_17

    nop

    :goto_2d
    invoke-direct {v1, v2}, Lcom/android/internal/org/bouncycastle/asn1/BEROctetString;-><init>([B)V

    goto/32 :goto_1e

    nop

    :goto_2e
    goto/16 :goto_1a

    :goto_2f
    goto/32 :goto_3f

    nop

    :goto_30
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/BERTaggedObject;->obj:Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_29

    nop

    :goto_31
    goto :goto_2b

    :goto_32
    goto/32 :goto_41

    nop

    :goto_33
    goto/16 :goto_1a

    :goto_34
    goto/32 :goto_4

    nop

    :goto_35
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_21

    nop

    :goto_36
    if-nez v0, :cond_3

    goto/32 :goto_32

    :cond_3
    goto/32 :goto_30

    nop

    :goto_37
    throw v0

    :goto_38
    goto/32 :goto_28

    nop

    :goto_39
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/BERTaggedObject;->obj:Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_1f

    nop

    :goto_3a
    check-cast v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1OctetString;

    goto/32 :goto_2c

    nop

    :goto_3b
    const/16 v0, 0x80

    goto/32 :goto_2

    nop

    :goto_3c
    goto/16 :goto_1a

    :goto_3d
    goto/32 :goto_22

    nop

    :goto_3e
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/BEROctetString;->getObjects()Ljava/util/Enumeration;

    move-result-object v0

    goto/32 :goto_3c

    nop

    :goto_3f
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/BERTaggedObject;->obj:Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_8

    nop

    :goto_40
    if-eqz v0, :cond_4

    goto/32 :goto_38

    :cond_4
    goto/32 :goto_39

    nop

    :goto_41
    new-instance v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Exception;

    goto/32 :goto_35

    nop
.end method

.method encodedLength()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_a

    nop

    :goto_0
    invoke-static {v1}, Lcom/android/internal/org/bouncycastle/asn1/StreamUtil;->calculateBodyLength(I)I

    move-result v3

    goto/32 :goto_d

    nop

    :goto_1
    return v2

    :goto_2
    goto/32 :goto_6

    nop

    :goto_3
    iget v2, p0, Lcom/android/internal/org/bouncycastle/asn1/BERTaggedObject;->tagNo:I

    goto/32 :goto_b

    nop

    :goto_4
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->encodedLength()I

    move-result v1

    goto/32 :goto_9

    nop

    :goto_5
    add-int/2addr v2, v1

    goto/32 :goto_f

    nop

    :goto_6
    add-int/lit8 v1, v1, -0x1

    goto/32 :goto_3

    nop

    :goto_7
    if-nez v2, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_10

    nop

    :goto_8
    invoke-static {v2}, Lcom/android/internal/org/bouncycastle/asn1/StreamUtil;->calculateTagLength(I)I

    move-result v2

    goto/32 :goto_0

    nop

    :goto_9
    iget-boolean v2, p0, Lcom/android/internal/org/bouncycastle/asn1/BERTaggedObject;->explicit:Z

    goto/32 :goto_7

    nop

    :goto_a
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/BERTaggedObject;->obj:Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_e

    nop

    :goto_b
    invoke-static {v2}, Lcom/android/internal/org/bouncycastle/asn1/StreamUtil;->calculateTagLength(I)I

    move-result v2

    goto/32 :goto_5

    nop

    :goto_c
    add-int/2addr v2, v1

    goto/32 :goto_1

    nop

    :goto_d
    add-int/2addr v2, v3

    goto/32 :goto_c

    nop

    :goto_e
    invoke-interface {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_f
    return v2

    :goto_10
    iget v2, p0, Lcom/android/internal/org/bouncycastle/asn1/BERTaggedObject;->tagNo:I

    goto/32 :goto_8

    nop
.end method

.method isConstructed()Z
    .locals 1

    goto/32 :goto_d

    nop

    :goto_0
    invoke-interface {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_1
    goto :goto_6

    :goto_2
    goto/32 :goto_9

    nop

    :goto_3
    return v0

    :goto_4
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->isConstructed()Z

    move-result v0

    goto/32 :goto_b

    nop

    :goto_5
    goto :goto_8

    :goto_6
    goto/32 :goto_7

    nop

    :goto_7
    const/4 v0, 0x1

    :goto_8
    goto/32 :goto_3

    nop

    :goto_9
    const/4 v0, 0x0

    goto/32 :goto_5

    nop

    :goto_a
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/BERTaggedObject;->obj:Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_0

    nop

    :goto_b
    if-nez v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop

    :goto_c
    if-eqz v0, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_a

    nop

    :goto_d
    iget-boolean v0, p0, Lcom/android/internal/org/bouncycastle/asn1/BERTaggedObject;->explicit:Z

    goto/32 :goto_c

    nop
.end method
