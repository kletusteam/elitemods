.class public final Lcom/android/internal/R$string;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final BaMmi:I = 0x1040044

.field public static final CLIRDefaultOffNextCallOff:I = 0x1040045

.field public static final CLIRDefaultOffNextCallOn:I = 0x1040046

.field public static final CLIRDefaultOnNextCallOff:I = 0x1040047

.field public static final CLIRDefaultOnNextCallOn:I = 0x1040048

.field public static final CLIRPermanent:I = 0x1040049

.field public static final CfMmi:I = 0x104004a

.field public static final ClipMmi:I = 0x104004e

.field public static final ClirMmi:I = 0x104004f

.field public static final ColpMmi:I = 0x1040053

.field public static final ColrMmi:I = 0x1040054

.field public static final CwMmi:I = 0x1040055

.field public static final EmergencyCallWarningSummary:I = 0x1040057

.field public static final EmergencyCallWarningTitle:I = 0x1040058

.field public static final Midnight:I = 0x1040059

.field public static final NetworkPreferenceSwitchSummary:I = 0x104005a

.field public static final NetworkPreferenceSwitchTitle:I = 0x104005b

.field public static final Noon:I = 0x104005c

.field public static final PinMmi:I = 0x10400c9

.field public static final PwdMmi:I = 0x10400ca

.field public static final RestrictedOnAllVoiceTitle:I = 0x10400cd

.field public static final RestrictedOnDataTitle:I = 0x10400cf

.field public static final RestrictedOnEmergencyTitle:I = 0x10400d1

.field public static final RestrictedOnNormalTitle:I = 0x10400d3

.field public static final RestrictedStateContent:I = 0x10400d7

.field public static final RestrictedStateContentMsimTemplate:I = 0x10400d8

.field public static final SetupCallDefault:I = 0x10400da

.field public static final VideoView_error_button:I = 0x1040010

.field public static final VideoView_error_text_invalid_progressive_playback:I = 0x1040015

.field public static final VideoView_error_text_unknown:I = 0x1040011

.field public static final VideoView_error_title:I = 0x1040012

.field public static final accept:I = 0x10400dc

.field public static final accessibility_binding_label:I = 0x10400dd

.field public static final accessibility_edit_shortcut_menu_button_title:I = 0x10400e2

.field public static final accessibility_edit_shortcut_menu_volume_title:I = 0x10400e3

.field public static final accessibility_enable_service_title:I = 0x10400e4

.field public static final accessibility_freeform_caption:I = 0x10400e6

.field public static final accessibility_gesture_3finger_instructional_text:I = 0x10400e7

.field public static final accessibility_gesture_3finger_prompt_text:I = 0x10400e8

.field public static final accessibility_gesture_instructional_text:I = 0x10400e9

.field public static final accessibility_gesture_prompt_text:I = 0x10400ea

.field public static final accessibility_magnification_chooser_text:I = 0x10400eb

.field public static final accessibility_select_shortcut_menu_title:I = 0x10400ec

.field public static final accessibility_shortcut_disabling_service:I = 0x10400f2

.field public static final accessibility_shortcut_enabling_service:I = 0x10400f3

.field public static final accessibility_shortcut_menu_item_status_off:I = 0x10400f4

.field public static final accessibility_shortcut_menu_item_status_on:I = 0x10400f5

.field public static final accessibility_shortcut_multiple_service_list:I = 0x10400f6

.field public static final accessibility_shortcut_multiple_service_warning:I = 0x10400f7

.field public static final accessibility_shortcut_multiple_service_warning_title:I = 0x10400f8

.field public static final accessibility_shortcut_off:I = 0x10400f9

.field public static final accessibility_shortcut_on:I = 0x10400fa

.field public static final accessibility_shortcut_single_service_warning:I = 0x10400fb

.field public static final accessibility_shortcut_single_service_warning_title:I = 0x10400fc

.field public static final accessibility_shortcut_spoken_feedback:I = 0x10400fd

.field public static final accessibility_system_action_back_label:I = 0x1040100

.field public static final accessibility_system_action_dismiss_notification_shade:I = 0x1040101

.field public static final accessibility_system_action_dpad_center_label:I = 0x1040102

.field public static final accessibility_system_action_dpad_down_label:I = 0x1040103

.field public static final accessibility_system_action_dpad_left_label:I = 0x1040104

.field public static final accessibility_system_action_dpad_right_label:I = 0x1040105

.field public static final accessibility_system_action_dpad_up_label:I = 0x1040106

.field public static final accessibility_system_action_hardware_a11y_shortcut_label:I = 0x1040107

.field public static final accessibility_system_action_headset_hook_label:I = 0x1040108

.field public static final accessibility_system_action_home_label:I = 0x1040109

.field public static final accessibility_system_action_lock_screen_label:I = 0x104010a

.field public static final accessibility_system_action_notifications_label:I = 0x104010b

.field public static final accessibility_system_action_on_screen_a11y_shortcut_chooser_label:I = 0x104010c

.field public static final accessibility_system_action_on_screen_a11y_shortcut_label:I = 0x104010d

.field public static final accessibility_system_action_power_dialog_label:I = 0x104010e

.field public static final accessibility_system_action_quick_settings_label:I = 0x104010f

.field public static final accessibility_system_action_recents_label:I = 0x1040110

.field public static final accessibility_system_action_screenshot_label:I = 0x1040111

.field public static final accessibility_uncheck_legacy_item_warning:I = 0x1040112

.field public static final action_bar_home_description:I = 0x1040113

.field public static final action_bar_home_description_format:I = 0x1040114

.field public static final action_bar_home_subtitle_description_format:I = 0x1040115

.field public static final action_bar_up_description:I = 0x1040116

.field public static final activity_chooser_view_see_all:I = 0x104011a

.field public static final activity_resolver_use_always:I = 0x104011c

.field public static final activity_resolver_work_profiles_support:I = 0x104011e

.field public static final activitychooserview_choose_application:I = 0x104011f

.field public static final activitychooserview_choose_application_error:I = 0x1040120

.field public static final adb_active_notification_message:I = 0x1040121

.field public static final adb_active_notification_title:I = 0x1040122

.field public static final adb_debugging_notification_channel_tv:I = 0x1040123

.field public static final adbwifi_active_notification_message:I = 0x1040124

.field public static final adbwifi_active_notification_title:I = 0x1040125

.field public static final addToDictionary:I = 0x1040126

.field public static final add_account_button_label:I = 0x1040127

.field public static final aerr_application:I = 0x1040129

.field public static final aerr_application_repeated:I = 0x104012a

.field public static final aerr_process:I = 0x104012e

.field public static final aerr_process_repeated:I = 0x104012f

.field public static final alert_windows_notification_channel_group_name:I = 0x1040134

.field public static final alert_windows_notification_channel_name:I = 0x1040135

.field public static final alert_windows_notification_message:I = 0x1040136

.field public static final alert_windows_notification_title:I = 0x1040137

.field public static final alert_windows_notification_turn_off_action:I = 0x1040138

.field public static final alternate_eri_file:I = 0x104013b

.field public static final alwaysUse:I = 0x104013c

.field public static final android_preparing_apk:I = 0x104013d

.field public static final android_start_title:I = 0x104013e

.field public static final android_system_label:I = 0x104013f

.field public static final android_upgrading_apk:I = 0x1040140

.field public static final android_upgrading_complete:I = 0x1040141

.field public static final android_upgrading_fstrim:I = 0x1040142

.field public static final android_upgrading_notification_title:I = 0x1040144

.field public static final android_upgrading_starting_apps:I = 0x1040145

.field public static final android_upgrading_title:I = 0x1040146

.field public static final anr_activity_application:I = 0x1040147

.field public static final anr_activity_process:I = 0x1040148

.field public static final anr_application_process:I = 0x1040149

.field public static final anr_process:I = 0x104014a

.field public static final anr_title:I = 0x104014b

.field public static final app_blocked_message:I = 0x104014c

.field public static final app_blocked_title:I = 0x104014d

.field public static final app_category_accessibility:I = 0x104014e

.field public static final app_category_audio:I = 0x104014f

.field public static final app_category_game:I = 0x1040150

.field public static final app_category_image:I = 0x1040151

.field public static final app_category_maps:I = 0x1040152

.field public static final app_category_news:I = 0x1040153

.field public static final app_category_productivity:I = 0x1040154

.field public static final app_category_social:I = 0x1040155

.field public static final app_category_video:I = 0x1040156

.field public static final app_running_notification_text:I = 0x1040159

.field public static final app_running_notification_title:I = 0x104015a

.field public static final app_streaming_blocked_message:I = 0x104015b

.field public static final app_streaming_blocked_message_for_fingerprint_dialog:I = 0x104015c

.field public static final app_streaming_blocked_message_for_permission_dialog:I = 0x104015d

.field public static final app_streaming_blocked_title:I = 0x104015e

.field public static final app_streaming_blocked_title_for_camera_dialog:I = 0x104015f

.field public static final app_streaming_blocked_title_for_fingerprint_dialog:I = 0x1040160

.field public static final app_streaming_blocked_title_for_microphone_dialog:I = 0x1040161

.field public static final app_streaming_blocked_title_for_permission_dialog:I = 0x1040162

.field public static final app_streaming_blocked_title_for_settings_dialog:I = 0x1040163

.field public static final app_suspended_default_message:I = 0x1040164

.field public static final app_suspended_more_details:I = 0x1040165

.field public static final app_suspended_title:I = 0x1040166

.field public static final app_suspended_unsuspend_message:I = 0x1040167

.field public static final as_app_forced_to_restricted_bucket:I = 0x1040169

.field public static final autofill:I = 0x104001a

.field public static final autofill_address_line_1_label_re:I = 0x104016c

.field public static final autofill_address_line_1_re:I = 0x104016d

.field public static final autofill_address_line_2_re:I = 0x104016e

.field public static final autofill_address_line_3_re:I = 0x104016f

.field public static final autofill_address_name_separator:I = 0x1040170

.field public static final autofill_address_summary_format:I = 0x1040171

.field public static final autofill_address_summary_name_format:I = 0x1040172

.field public static final autofill_address_summary_separator:I = 0x1040173

.field public static final autofill_address_type_same_as_re:I = 0x1040174

.field public static final autofill_address_type_use_my_re:I = 0x1040175

.field public static final autofill_area:I = 0x1040176

.field public static final autofill_area_code_notext_re:I = 0x1040177

.field public static final autofill_area_code_re:I = 0x1040178

.field public static final autofill_attention_ignored_re:I = 0x1040179

.field public static final autofill_billing_designator_re:I = 0x104017a

.field public static final autofill_card_cvc_re:I = 0x104017b

.field public static final autofill_card_ignored_re:I = 0x104017c

.field public static final autofill_card_number_re:I = 0x104017d

.field public static final autofill_city_re:I = 0x104017e

.field public static final autofill_company_re:I = 0x104017f

.field public static final autofill_continue_yes:I = 0x1040180

.field public static final autofill_country_code_re:I = 0x1040181

.field public static final autofill_country_re:I = 0x1040182

.field public static final autofill_county:I = 0x1040183

.field public static final autofill_department:I = 0x1040184

.field public static final autofill_district:I = 0x1040185

.field public static final autofill_email_re:I = 0x1040186

.field public static final autofill_emirate:I = 0x1040187

.field public static final autofill_error_cannot_autofill:I = 0x1040188

.field public static final autofill_expiration_date_re:I = 0x1040189

.field public static final autofill_expiration_month_re:I = 0x104018a

.field public static final autofill_fax_re:I = 0x104018b

.field public static final autofill_first_name_re:I = 0x104018c

.field public static final autofill_island:I = 0x104018d

.field public static final autofill_last_name_re:I = 0x104018e

.field public static final autofill_middle_initial_re:I = 0x104018f

.field public static final autofill_middle_name_re:I = 0x1040190

.field public static final autofill_name_on_card_contextual_re:I = 0x1040191

.field public static final autofill_name_on_card_re:I = 0x1040192

.field public static final autofill_name_re:I = 0x1040193

.field public static final autofill_name_specific_re:I = 0x1040194

.field public static final autofill_parish:I = 0x1040195

.field public static final autofill_phone_extension_re:I = 0x1040196

.field public static final autofill_phone_prefix_re:I = 0x1040197

.field public static final autofill_phone_prefix_separator_re:I = 0x1040198

.field public static final autofill_phone_re:I = 0x1040199

.field public static final autofill_phone_suffix_re:I = 0x104019a

.field public static final autofill_phone_suffix_separator_re:I = 0x104019b

.field public static final autofill_picker_accessibility_title:I = 0x104019c

.field public static final autofill_picker_no_suggestions:I = 0x104019d

.field public static final autofill_picker_some_suggestions:I = 0x104019e

.field public static final autofill_postal_code:I = 0x104019f

.field public static final autofill_prefecture:I = 0x10401a0

.field public static final autofill_province:I = 0x10401a1

.field public static final autofill_region_ignored_re:I = 0x10401a2

.field public static final autofill_save_accessibility_title:I = 0x10401a3

.field public static final autofill_save_never:I = 0x10401a4

.field public static final autofill_save_no:I = 0x10401a5

.field public static final autofill_save_notnow:I = 0x10401a6

.field public static final autofill_save_title:I = 0x10401a7

.field public static final autofill_save_title_with_2types:I = 0x10401a8

.field public static final autofill_save_title_with_3types:I = 0x10401a9

.field public static final autofill_save_title_with_type:I = 0x10401aa

.field public static final autofill_save_type_address:I = 0x10401ab

.field public static final autofill_save_type_credit_card:I = 0x10401ac

.field public static final autofill_save_type_debit_card:I = 0x10401ad

.field public static final autofill_save_type_email_address:I = 0x10401ae

.field public static final autofill_save_type_generic_card:I = 0x10401af

.field public static final autofill_save_type_password:I = 0x10401b0

.field public static final autofill_save_type_payment_card:I = 0x10401b1

.field public static final autofill_save_type_username:I = 0x10401b2

.field public static final autofill_save_yes:I = 0x10401b3

.field public static final autofill_shipping_designator_re:I = 0x10401b4

.field public static final autofill_state:I = 0x10401b5

.field public static final autofill_state_re:I = 0x10401b6

.field public static final autofill_this_form:I = 0x10401b7

.field public static final autofill_update_title:I = 0x10401b8

.field public static final autofill_update_title_with_2types:I = 0x10401b9

.field public static final autofill_update_title_with_3types:I = 0x10401ba

.field public static final autofill_update_title_with_type:I = 0x10401bb

.field public static final autofill_update_yes:I = 0x10401bc

.field public static final autofill_username_re:I = 0x10401bd

.field public static final autofill_window_title:I = 0x10401be

.field public static final autofill_zip_4_re:I = 0x10401bf

.field public static final autofill_zip_code:I = 0x10401c0

.field public static final autofill_zip_code_re:I = 0x10401c1

.field public static final badPin:I = 0x10401c3

.field public static final badPuk:I = 0x10401c4

.field public static final battery_saver_charged_notification_summary:I = 0x10401c5

.field public static final battery_saver_description:I = 0x10401c6

.field public static final battery_saver_description_with_learn_more:I = 0x10401c7

.field public static final battery_saver_notification_channel_name:I = 0x10401c8

.field public static final battery_saver_off_notification_title:I = 0x10401c9

.field public static final biometric_app_setting_name:I = 0x10401cb

.field public static final biometric_dialog_default_subtitle:I = 0x10401cc

.field public static final biometric_dialog_default_title:I = 0x10401cd

.field public static final biometric_error_canceled:I = 0x10401ce

.field public static final biometric_error_device_not_secured:I = 0x10401cf

.field public static final biometric_error_generic:I = 0x10401d0

.field public static final biometric_error_hw_unavailable:I = 0x10401d1

.field public static final biometric_error_user_canceled:I = 0x10401d2

.field public static final biometric_not_recognized:I = 0x10401d3

.field public static final biometric_or_screen_lock_app_setting_name:I = 0x10401d4

.field public static final biometric_or_screen_lock_dialog_default_subtitle:I = 0x10401d5

.field public static final bluetooth_a2dp_audio_route_id:I = 0x10401d6

.field public static final bluetooth_a2dp_audio_route_name:I = 0x10401d7

.field public static final bluetooth_airplane_mode_toast:I = 0x10401d8

.field public static final bugreport_countdown:I = 0x10401da

.field public static final bugreport_message:I = 0x10401db

.field public static final bugreport_option_full_summary:I = 0x10401dc

.field public static final bugreport_option_full_title:I = 0x10401dd

.field public static final bugreport_option_interactive_summary:I = 0x10401de

.field public static final bugreport_option_interactive_title:I = 0x10401df

.field public static final bugreport_screenshot_failure_toast:I = 0x10401e0

.field public static final bugreport_screenshot_success_toast:I = 0x10401e1

.field public static final bugreport_status:I = 0x10401e2

.field public static final bugreport_title:I = 0x10401e3

.field public static final byteShort:I = 0x10401e4

.field public static final call_notification_answer_action:I = 0x10401e5

.field public static final call_notification_answer_video_action:I = 0x10401e6

.field public static final call_notification_decline_action:I = 0x10401e7

.field public static final call_notification_hang_up_action:I = 0x10401e8

.field public static final call_notification_incoming_text:I = 0x10401e9

.field public static final call_notification_ongoing_text:I = 0x10401ea

.field public static final call_notification_screening_text:I = 0x10401eb

.field public static final cancel:I = 0x1040000

.field public static final capability_desc_canCaptureFingerprintGestures:I = 0x10401ed

.field public static final capability_desc_canControlMagnification:I = 0x10401ee

.field public static final capability_desc_canPerformGestures:I = 0x10401ef

.field public static final capability_desc_canRequestFilterKeyEvents:I = 0x10401f1

.field public static final capability_desc_canRequestTouchExploration:I = 0x10401f2

.field public static final capability_desc_canRetrieveWindowContent:I = 0x10401f3

.field public static final capability_desc_canTakeScreenshot:I = 0x10401f4

.field public static final capability_title_canCaptureFingerprintGestures:I = 0x10401f5

.field public static final capability_title_canControlMagnification:I = 0x10401f6

.field public static final capability_title_canPerformGestures:I = 0x10401f7

.field public static final capability_title_canRequestFilterKeyEvents:I = 0x10401f9

.field public static final capability_title_canRequestTouchExploration:I = 0x10401fa

.field public static final capability_title_canRetrieveWindowContent:I = 0x10401fb

.field public static final capability_title_canTakeScreenshot:I = 0x10401fc

.field public static final capital_off:I = 0x10401fd

.field public static final capital_on:I = 0x10401fe

.field public static final car_loading_profile:I = 0x10401ff

.field public static final car_mode_disable_notification_message:I = 0x1040200

.field public static final car_mode_disable_notification_title:I = 0x1040201

.field public static final carrier_app_notification_text:I = 0x1040205

.field public static final carrier_app_notification_title:I = 0x1040206

.field public static final cfTemplateForwarded:I = 0x1040207

.field public static final cfTemplateForwardedTime:I = 0x1040208

.field public static final cfTemplateNotForwarded:I = 0x1040209

.field public static final cfTemplateRegistered:I = 0x104020a

.field public static final cfTemplateRegisteredTime:I = 0x104020b

.field public static final checked:I = 0x104020c

.field public static final chooseActivity:I = 0x104020d

.field public static final chooseUsbActivity:I = 0x104020e

.field public static final chooser_all_apps_button_label:I = 0x1040210

.field public static final chooser_no_direct_share_targets:I = 0x1040211

.field public static final chooser_wallpaper:I = 0x1040212

.field public static final color_correction_feature_name:I = 0x1040215

.field public static final color_inversion_feature_name:I = 0x1040216

.field public static final common_last_name_prefixes:I = 0x1040217

.field public static final common_name_conjunctions:I = 0x1040219

.field public static final common_name_prefixes:I = 0x104021a

.field public static final common_name_suffixes:I = 0x104021b

.field public static final condition_provider_service_binding_label:I = 0x104021c

.field public static final conference_call:I = 0x104021d

.field public static final config_UsbDeviceConnectionHandling_component:I = 0x104021e

.field public static final config_activityRecognitionHardwarePackageName:I = 0x104021f

.field public static final config_ambientContextEventArrayExtraKey:I = 0x1040220

.field public static final config_ambientContextPackageNameExtraKey:I = 0x1040221

.field public static final config_appsAuthorizedForSharedAccounts:I = 0x1040222

.field public static final config_appsNotReportingCrashes:I = 0x1040223

.field public static final config_bandwidthEstimateSource:I = 0x1040224

.field public static final config_batterySaverDeviceSpecificConfig:I = 0x1040225

.field public static final config_batterySaverScheduleProvider:I = 0x1040226

.field public static final config_batterymeterBoltPath:I = 0x1040227

.field public static final config_batterymeterErrorPerimeterPath:I = 0x1040228

.field public static final config_batterymeterFillMask:I = 0x1040229

.field public static final config_batterymeterPerimeterPath:I = 0x104022a

.field public static final config_batterymeterPowersavePath:I = 0x104022b

.field public static final config_biometric_prompt_ui_package:I = 0x104022c

.field public static final config_cameraLaunchGestureSensorStringType:I = 0x104022f

.field public static final config_cameraLiftTriggerSensorStringType:I = 0x1040230

.field public static final config_cameraShutterSound:I = 0x1040231

.field public static final config_carrierAppInstallDialogComponent:I = 0x1040232

.field public static final config_chooseAccountActivity:I = 0x1040233

.field public static final config_chooseTypeAndAccountActivity:I = 0x1040234

.field public static final config_chooserActivity:I = 0x1040235

.field public static final config_comboNetworkLocationProvider:I = 0x1040236

.field public static final config_controlsPackage:I = 0x1040238

.field public static final config_customAdbPublicKeyConfirmationComponent:I = 0x1040239

.field public static final config_customAdbPublicKeyConfirmationSecondaryUserComponent:I = 0x104023a

.field public static final config_customAdbWifiNetworkConfirmationComponent:I = 0x104023b

.field public static final config_customAdbWifiNetworkConfirmationSecondaryUserComponent:I = 0x104023c

.field public static final config_customCountryDetector:I = 0x104023d

.field public static final config_customMediaKeyDispatcher:I = 0x104002c

.field public static final config_customMediaSessionPolicyProvider:I = 0x104002d

.field public static final config_customResolverActivity:I = 0x104023e

.field public static final config_customVpnAlwaysOnDisconnectedDialogComponent:I = 0x104023f

.field public static final config_customVpnConfirmDialogComponent:I = 0x1040240

.field public static final config_dataUsageSummaryComponent:I = 0x1040241

.field public static final config_datause_iface:I = 0x1040242

.field public static final config_defaultAccessibilityService:I = 0x1040243

.field public static final config_defaultAmbientContextConsentComponent:I = 0x1040244

.field public static final config_defaultAmbientContextDetectionService:I = 0x1040245

.field public static final config_defaultAppPredictionService:I = 0x1040246

.field public static final config_defaultAssistant:I = 0x1040021

.field public static final config_defaultAssistantAccessComponent:I = 0x1040247

.field public static final config_defaultAttentionService:I = 0x1040248

.field public static final config_defaultAugmentedAutofillService:I = 0x1040249

.field public static final config_defaultAutofillService:I = 0x104024a

.field public static final config_defaultAutomotiveNavigation:I = 0x1040040

.field public static final config_defaultBrowser:I = 0x1040022

.field public static final config_defaultBugReportHandlerApp:I = 0x104024b

.field public static final config_defaultCallRedirection:I = 0x1040025

.field public static final config_defaultCallScreening:I = 0x1040026

.field public static final config_defaultCaptivePortalLoginPackageName:I = 0x104024c

.field public static final config_defaultContentCaptureService:I = 0x104024d

.field public static final config_defaultContentSuggestionsService:I = 0x104024e

.field public static final config_defaultDialer:I = 0x1040023

.field public static final config_defaultDndAccessPackages:I = 0x104024f

.field public static final config_defaultListenerAccessPackages:I = 0x1040250

.field public static final config_defaultModuleMetadataProvider:I = 0x1040251

.field public static final config_defaultMusicRecognitionService:I = 0x1040252

.field public static final config_defaultNearbyFastPairSettingsDevicesComponent:I = 0x1040253

.field public static final config_defaultNearbySharingComponent:I = 0x1040254

.field public static final config_defaultNetworkRecommendationProviderPackage:I = 0x1040256

.field public static final config_defaultNetworkScorerPackageName:I = 0x1040257

.field public static final config_defaultOnDeviceSpeechRecognitionService:I = 0x1040258

.field public static final config_defaultProfcollectReportUploaderAction:I = 0x1040259

.field public static final config_defaultProfcollectReportUploaderApp:I = 0x104025a

.field public static final config_defaultQrCodeComponent:I = 0x104025b

.field public static final config_defaultRingtoneVibrationSound:I = 0x104003b

.field public static final config_defaultRotationResolverService:I = 0x104025c

.field public static final config_defaultSearchSelectorPackageName:I = 0x104025d

.field public static final config_defaultSearchUiService:I = 0x104025e

.field public static final config_defaultSmartspaceService:I = 0x104025f

.field public static final config_defaultSms:I = 0x1040024

.field public static final config_defaultSupervisionProfileOwnerComponent:I = 0x1040260

.field public static final config_defaultSystemCaptionsManagerService:I = 0x1040261

.field public static final config_defaultTextClassifierPackage:I = 0x1040262

.field public static final config_defaultTranslationService:I = 0x1040263

.field public static final config_defaultTrustAgent:I = 0x1040264

.field public static final config_defaultWallpaperEffectsGenerationService:I = 0x1040265

.field public static final config_defaultWellbeingPackage:I = 0x1040266

.field public static final config_default_dns_server:I = 0x1040267

.field public static final config_deviceConfiguratorPackageName:I = 0x1040268

.field public static final config_devicePolicyManagement:I = 0x104003d

.field public static final config_devicePolicyManagementUpdater:I = 0x1040269

.field public static final config_deviceProvisioningPackage:I = 0x104026a

.field public static final config_deviceSpecificAudioService:I = 0x104026b

.field public static final config_deviceSpecificDevicePolicyManagerService:I = 0x104026c

.field public static final config_deviceSpecificDeviceStatePolicyProvider:I = 0x104026d

.field public static final config_deviceSpecificDisplayAreaPolicyProvider:I = 0x104026e

.field public static final config_displayLightSensorType:I = 0x104026f

.field public static final config_displayWhiteBalanceColorTemperatureSensorName:I = 0x1040270

.field public static final config_display_features:I = 0x1040271

.field public static final config_doublePressOnPowerTargetActivity:I = 0x1040272

.field public static final config_doubleTouchGestureEnableFile:I = 0x1040273

.field public static final config_dozeComponent:I = 0x1040274

.field public static final config_dozeDoubleTapSensorType:I = 0x1040275

.field public static final config_dozeLongPressSensorType:I = 0x1040276

.field public static final config_dozeTapSensorType:I = 0x1040277

.field public static final config_dozeUdfpsLongPressSensorType:I = 0x1040278

.field public static final config_dreamsDefaultComponent:I = 0x1040279

.field public static final config_emergency_call_number:I = 0x104027a

.field public static final config_emergency_dialer_package:I = 0x104027b

.field public static final config_ethernet_iface_regex:I = 0x104027c

.field public static final config_ethernet_tcp_buffers:I = 0x104027d

.field public static final config_factoryResetPackage:I = 0x104027e

.field public static final config_feedbackIntentExtraKey:I = 0x104001f

.field public static final config_feedbackIntentNameKey:I = 0x1040020

.field public static final config_foldedArea:I = 0x104027f

.field public static final config_forceVoiceInteractionServicePackage:I = 0x1040280

.field public static final config_fusedLocationProviderPackageName:I = 0x1040281

.field public static final config_geocoderProviderPackageName:I = 0x1040282

.field public static final config_geofenceProviderPackageName:I = 0x1040283

.field public static final config_globalAppSearchDataQuerierPackage:I = 0x1040284

.field public static final config_hdmiCecSetMenuLanguageActivity:I = 0x1040285

.field public static final config_headlineFontFamily:I = 0x1040286

.field public static final config_headlineFontFamilyMedium:I = 0x1040287

.field public static final config_helpIntentExtraKey:I = 0x104001d

.field public static final config_helpIntentNameKey:I = 0x104001e

.field public static final config_helpPackageNameKey:I = 0x104001b

.field public static final config_helpPackageNameValue:I = 0x104001c

.field public static final config_iccHotswapPromptForRestartDialogComponent:I = 0x1040289

.field public static final config_icon_mask:I = 0x104028a

.field public static final config_inCallNotificationSound:I = 0x104028b

.field public static final config_incidentReportApproverPackage:I = 0x104028c

.field public static final config_inputEventCompatProcessorOverrideClassName:I = 0x104028d

.field public static final config_keyguardComponent:I = 0x104028f

.field public static final config_loggable_dream_prefix:I = 0x1040290

.field public static final config_mainBuiltInDisplayCutout:I = 0x1040291

.field public static final config_mainBuiltInDisplayCutoutRectApproximation:I = 0x1040292

.field public static final config_managed_provisioning_package:I = 0x1040293

.field public static final config_mediaProjectionPermissionDialogComponent:I = 0x1040294

.field public static final config_misprovisionedBrandValue:I = 0x1040295

.field public static final config_misprovisionedDeviceModel:I = 0x1040296

.field public static final config_mms_user_agent:I = 0x1040297

.field public static final config_mms_user_agent_profile_url:I = 0x1040298

.field public static final config_mobile_hotspot_provision_app_no_ui:I = 0x1040299

.field public static final config_mobile_hotspot_provision_response:I = 0x104029a

.field public static final config_networkLocationProviderPackageName:I = 0x104029b

.field public static final config_networkLocationProviderPackageNameGms:I = 0x104029c

.field public static final config_networkOverLimitComponent:I = 0x104029d

.field public static final config_notificationAccessConfirmationActivity:I = 0x104029f

.field public static final config_notificationHandlerPackage:I = 0x10402a0

.field public static final config_ntpServer:I = 0x10402a1

.field public static final config_optionalPackageVerifierName:I = 0x10402a2

.field public static final config_overrideComponentUiPackage:I = 0x10402a3

.field public static final config_packagedKeyboardName:I = 0x10402a4

.field public static final config_pdp_reject_dialog_title:I = 0x10402a5

.field public static final config_pdp_reject_multi_conn_to_same_pdn_not_allowed:I = 0x10402a6

.field public static final config_pdp_reject_service_not_subscribed:I = 0x10402a7

.field public static final config_pdp_reject_user_authentication_failed:I = 0x10402a8

.field public static final config_persistentDataPackageName:I = 0x10402a9

.field public static final config_platformVpnConfirmDialogComponent:I = 0x10402aa

.field public static final config_powerSaveModeChangedListenerPackage:I = 0x10402ab

.field public static final config_primaryLocationTimeZoneProviderPackageName:I = 0x10402ac

.field public static final config_qualified_networks_service_class:I = 0x10402ad

.field public static final config_qualified_networks_service_package:I = 0x10402ae

.field public static final config_quickPickupSensorType:I = 0x10402af

.field public static final config_radio_access_family:I = 0x10402b0

.field public static final config_rawContactsLocalAccountName:I = 0x10402b1

.field public static final config_rawContactsLocalAccountType:I = 0x10402b2

.field public static final config_recentsComponentName:I = 0x10402b3

.field public static final config_retailDemoPackage:I = 0x10402b4

.field public static final config_retailDemoPackageSignature:I = 0x10402b5

.field public static final config_screenRecorderComponent:I = 0x10402b6

.field public static final config_screenshotErrorReceiverComponent:I = 0x10402b7

.field public static final config_screenshotServiceComponent:I = 0x10402b8

.field public static final config_secondaryBuiltInDisplayCutout:I = 0x10402b9

.field public static final config_secondaryBuiltInDisplayCutoutRectApproximation:I = 0x10402ba

.field public static final config_secondaryHomePackage:I = 0x10402bb

.field public static final config_secondaryLocationTimeZoneProviderPackageName:I = 0x10402bc

.field public static final config_sensorUseStartedActivity:I = 0x10402bd

.field public static final config_sensorUseStartedActivity_hwToggle:I = 0x10402be

.field public static final config_servicesExtensionPackage:I = 0x10402bf

.field public static final config_signalAttributionPath:I = 0x10402c0

.field public static final config_slicePermissionComponent:I = 0x10402c1

.field public static final config_somnambulatorComponent:I = 0x10402c2

.field public static final config_supervisedUserCreationPackage:I = 0x10402c3

.field public static final config_systemActivityRecognizer:I = 0x1040038

.field public static final config_systemAmbientAudioIntelligence:I = 0x1040033

.field public static final config_systemAppProtectionService:I = 0x104003e

.field public static final config_systemAudioIntelligence:I = 0x1040034

.field public static final config_systemAutomotiveCalendarSyncManager:I = 0x104003f

.field public static final config_systemAutomotiveCluster:I = 0x1040028

.field public static final config_systemAutomotiveProjection:I = 0x1040029

.field public static final config_systemBluetoothStack:I = 0x1040043

.field public static final config_systemCompanionDeviceProvider:I = 0x1040039

.field public static final config_systemContacts:I = 0x104002b

.field public static final config_systemGallery:I = 0x1040027

.field public static final config_systemGameService:I = 0x10402c4

.field public static final config_systemImageEditor:I = 0x10402c5

.field public static final config_systemNotificationIntelligence:I = 0x1040035

.field public static final config_systemSettingsIntelligence:I = 0x1040042

.field public static final config_systemShell:I = 0x104002a

.field public static final config_systemSpeechRecognizer:I = 0x104002e

.field public static final config_systemSupervision:I = 0x104003c

.field public static final config_systemTelevisionNotificationHandler:I = 0x1040031

.field public static final config_systemTextIntelligence:I = 0x1040036

.field public static final config_systemUIServiceComponent:I = 0x10402c7

.field public static final config_systemUi:I = 0x104003a

.field public static final config_systemUiIntelligence:I = 0x1040032

.field public static final config_systemVisualIntelligence:I = 0x1040037

.field public static final config_systemWellbeing:I = 0x1040030

.field public static final config_systemWifiCoexManager:I = 0x104002f

.field public static final config_tcp_buffers:I = 0x10402c8

.field public static final config_timeZoneRulesDataPackage:I = 0x10402c9

.field public static final config_timeZoneRulesUpdaterPackage:I = 0x10402ca

.field public static final config_tvRemoteServicePackage:I = 0x10402cb

.field public static final config_usbAccessoryUriActivity:I = 0x10402cc

.field public static final config_usbConfirmActivity:I = 0x10402cd

.field public static final config_usbContaminantActivity:I = 0x10402ce

.field public static final config_usbPermissionActivity:I = 0x10402cf

.field public static final config_usbResolverActivity:I = 0x10402d0

.field public static final config_useragentprofile_url:I = 0x10402d1

.field public static final config_vendorColorModesRestoreHint:I = 0x10402d2

.field public static final config_wallpaperCropperPackage:I = 0x10402d3

.field public static final config_wallpaperManagerServiceName:I = 0x10402d4

.field public static final config_wearSysUiMainActivity:I = 0x10402d5

.field public static final config_wearSysUiPackage:I = 0x10402d6

.field public static final config_wifi_tether_enable:I = 0x10402d7

.field public static final config_wimaxManagerClassname:I = 0x10402d8

.field public static final config_wimaxNativeLibLocation:I = 0x10402d9

.field public static final config_wimaxServiceClassname:I = 0x10402da

.field public static final config_wimaxServiceJarLocation:I = 0x10402db

.field public static final config_wimaxStateTrackerClassname:I = 0x10402dc

.field public static final config_wlan_data_service_class:I = 0x10402dd

.field public static final config_wlan_data_service_package:I = 0x10402de

.field public static final config_wlan_network_service_class:I = 0x10402df

.field public static final config_wlan_network_service_package:I = 0x10402e0

.field public static final config_wwan_data_service_class:I = 0x10402e2

.field public static final config_wwan_data_service_package:I = 0x10402e3

.field public static final config_wwan_network_service_class:I = 0x10402e4

.field public static final config_wwan_network_service_package:I = 0x10402e5

.field public static final confirm_battery_saver:I = 0x10402e6

.field public static final console_running_notification_message:I = 0x10402e7

.field public static final console_running_notification_title:I = 0x10402e8

.field public static final contentServiceSync:I = 0x10402e9

.field public static final contentServiceSyncNotificationTitle:I = 0x10402ea

.field public static final contentServiceTooManyDeletesNotificationDesc:I = 0x10402eb

.field public static final conversation_single_line_image_placeholder:I = 0x10402ee

.field public static final conversation_single_line_name_display:I = 0x10402ef

.field public static final conversation_title_fallback_group_chat:I = 0x10402f0

.field public static final conversation_title_fallback_one_to_one:I = 0x10402f1

.field public static final copy:I = 0x1040001

.field public static final copyUrl:I = 0x1040002

.field public static final country_selection_title:I = 0x10402f4

.field public static final create_contact_using:I = 0x10402f5

.field public static final cut:I = 0x1040003

.field public static final data_saver_description:I = 0x10402f8

.field public static final data_saver_enable_button:I = 0x10402f9

.field public static final data_saver_enable_title:I = 0x10402fa

.field public static final data_usage_limit_body:I = 0x10402ff

.field public static final data_usage_limit_snoozed_body:I = 0x1040300

.field public static final data_usage_mobile_limit_snoozed_title:I = 0x1040301

.field public static final data_usage_mobile_limit_title:I = 0x1040302

.field public static final data_usage_rapid_app_body:I = 0x1040303

.field public static final data_usage_rapid_body:I = 0x1040304

.field public static final data_usage_rapid_title:I = 0x1040305

.field public static final data_usage_restricted_body:I = 0x1040306

.field public static final data_usage_restricted_title:I = 0x1040307

.field public static final data_usage_warning_body:I = 0x1040308

.field public static final data_usage_warning_title:I = 0x1040309

.field public static final data_usage_wifi_limit_snoozed_title:I = 0x104030a

.field public static final data_usage_wifi_limit_title:I = 0x104030b

.field public static final date_and_time:I = 0x104030c

.field public static final date_picker_day_of_week_typeface:I = 0x104030d

.field public static final date_picker_day_typeface:I = 0x104030e

.field public static final date_picker_decrement_day_button:I = 0x104030f

.field public static final date_picker_decrement_month_button:I = 0x1040310

.field public static final date_picker_decrement_year_button:I = 0x1040311

.field public static final date_picker_dialog_title:I = 0x1040312

.field public static final date_picker_increment_day_button:I = 0x1040313

.field public static final date_picker_increment_month_button:I = 0x1040314

.field public static final date_picker_increment_year_button:I = 0x1040315

.field public static final date_picker_month_typeface:I = 0x1040317

.field public static final date_picker_next_month_button:I = 0x1040318

.field public static final date_picker_prev_month_button:I = 0x1040319

.field public static final date_time:I = 0x104031a

.field public static final date_time_done:I = 0x104031b

.field public static final date_time_set:I = 0x104031c

.field public static final db_default_journal_mode:I = 0x104031f

.field public static final db_default_sync_mode:I = 0x1040320

.field public static final db_wal_sync_mode:I = 0x1040321

.field public static final decline:I = 0x1040322

.field public static final decline_remote_bugreport_action:I = 0x1040323

.field public static final defaultMsisdnAlphaTag:I = 0x1040005

.field public static final defaultVoiceMailAlphaTag:I = 0x1040004

.field public static final default_audio_route_category_name:I = 0x1040324

.field public static final default_audio_route_id:I = 0x1040325

.field public static final default_audio_route_name:I = 0x1040326

.field public static final default_audio_route_name_dock_speakers:I = 0x1040327

.field public static final default_audio_route_name_hdmi:I = 0x1040328

.field public static final default_audio_route_name_headphones:I = 0x1040329

.field public static final default_audio_route_name_usb:I = 0x104032a

.field public static final default_browser:I = 0x104032b

.field public static final default_notification_channel_label:I = 0x104032d

.field public static final default_sms_application:I = 0x104032e

.field public static final default_wallpaper_component:I = 0x104032f

.field public static final delete:I = 0x1040330

.field public static final deleteText:I = 0x1040331

.field public static final deleted_key:I = 0x1040332

.field public static final demo_restarting_message:I = 0x1040333

.field public static final demo_starting_message:I = 0x1040334

.field public static final deprecated_target_sdk_app_store:I = 0x104033b

.field public static final deprecated_target_sdk_message:I = 0x104033c

.field public static final description_target_unlock_tablet:I = 0x104033d

.field public static final device_ownership_relinquished:I = 0x104033e

.field public static final device_storage_monitor_notification_channel:I = 0x1040340

.field public static final dial_number_using:I = 0x1040342

.field public static final dialog_alert_title:I = 0x1040014

.field public static final dismiss_action:I = 0x1040344

.field public static final display_manager_built_in_display_name:I = 0x1040345

.field public static final display_manager_hdmi_display_name:I = 0x1040346

.field public static final display_manager_overlay_display_name:I = 0x1040347

.field public static final display_manager_overlay_display_secure_suffix:I = 0x1040348

.field public static final display_manager_overlay_display_title:I = 0x1040349

.field public static final dlg_ok:I = 0x104034a

.field public static final done_accessibility_shortcut_menu_button:I = 0x104034b

.field public static final done_label:I = 0x104034c

.field public static final double_tap_toast:I = 0x104034d

.field public static final dump_heap_notification:I = 0x104034e

.field public static final dump_heap_notification_detail:I = 0x104034f

.field public static final dump_heap_ready_notification:I = 0x1040350

.field public static final dump_heap_ready_text:I = 0x1040351

.field public static final dump_heap_system_text:I = 0x1040352

.field public static final dump_heap_text:I = 0x1040353

.field public static final dump_heap_title:I = 0x1040354

.field public static final duration_days_relative:I = 0x1040361

.field public static final duration_days_relative_future:I = 0x1040362

.field public static final duration_days_shortest:I = 0x1040363

.field public static final duration_days_shortest_future:I = 0x1040364

.field public static final duration_hours_relative:I = 0x1040365

.field public static final duration_hours_relative_future:I = 0x1040366

.field public static final duration_hours_shortest:I = 0x1040367

.field public static final duration_hours_shortest_future:I = 0x1040368

.field public static final duration_minutes_relative:I = 0x1040369

.field public static final duration_minutes_relative_future:I = 0x104036a

.field public static final duration_minutes_shortest:I = 0x104036b

.field public static final duration_minutes_shortest_future:I = 0x104036c

.field public static final duration_years_relative:I = 0x104036d

.field public static final duration_years_relative_future:I = 0x104036e

.field public static final duration_years_shortest:I = 0x104036f

.field public static final duration_years_shortest_future:I = 0x1040370

.field public static final dynamic_mode_notification_channel_name:I = 0x1040371

.field public static final dynamic_mode_notification_summary:I = 0x1040372

.field public static final dynamic_mode_notification_title:I = 0x1040373

.field public static final edit_accessibility_shortcut_menu_button:I = 0x1040375

.field public static final elapsed_time_short_format_h_mm_ss:I = 0x1040376

.field public static final elapsed_time_short_format_mm_ss:I = 0x1040377

.field public static final emailTypeCustom:I = 0x104037a

.field public static final emailTypeHome:I = 0x104037b

.field public static final emailTypeMobile:I = 0x104037c

.field public static final emailTypeOther:I = 0x104037d

.field public static final emailTypeWork:I = 0x104037e

.field public static final emergency_call_dialog_number_for_display:I = 0x104037f

.field public static final emergency_calls_only:I = 0x1040380

.field public static final emptyPhoneNumber:I = 0x1040006

.field public static final enablePin:I = 0x1040381

.field public static final enable_explore_by_touch_warning_message:I = 0x1040383

.field public static final enable_explore_by_touch_warning_title:I = 0x1040384

.field public static final error_message_change_not_allowed:I = 0x1040385

.field public static final error_message_title:I = 0x1040386

.field public static final etws_primary_default_message_earthquake:I = 0x1040387

.field public static final etws_primary_default_message_earthquake_and_tsunami:I = 0x1040388

.field public static final etws_primary_default_message_others:I = 0x1040389

.field public static final etws_primary_default_message_test:I = 0x104038a

.field public static final etws_primary_default_message_tsunami:I = 0x104038b

.field public static final eventTypeAnniversary:I = 0x104038c

.field public static final eventTypeBirthday:I = 0x104038d

.field public static final eventTypeCustom:I = 0x104038e

.field public static final eventTypeOther:I = 0x104038f

.field public static final expand_action_accessibility:I = 0x1040390

.field public static final expand_button_content_description_collapsed:I = 0x1040392

.field public static final expand_button_content_description_expanded:I = 0x1040393

.field public static final ext_media_badremoval_notification_message:I = 0x1040395

.field public static final ext_media_badremoval_notification_title:I = 0x1040396

.field public static final ext_media_browse_action:I = 0x1040397

.field public static final ext_media_checking_notification_message:I = 0x1040398

.field public static final ext_media_checking_notification_title:I = 0x1040399

.field public static final ext_media_init_action:I = 0x104039a

.field public static final ext_media_missing_message:I = 0x104039b

.field public static final ext_media_missing_title:I = 0x104039c

.field public static final ext_media_move_failure_message:I = 0x104039d

.field public static final ext_media_move_failure_title:I = 0x104039e

.field public static final ext_media_move_specific_title:I = 0x104039f

.field public static final ext_media_move_success_message:I = 0x10403a0

.field public static final ext_media_move_success_title:I = 0x10403a1

.field public static final ext_media_move_title:I = 0x10403a2

.field public static final ext_media_new_notification_message:I = 0x10403a3

.field public static final ext_media_nomedia_notification_message:I = 0x10403a5

.field public static final ext_media_nomedia_notification_title:I = 0x10403a6

.field public static final ext_media_ready_notification_message:I = 0x10403a7

.field public static final ext_media_seamless_action:I = 0x10403a8

.field public static final ext_media_status_bad_removal:I = 0x10403a9

.field public static final ext_media_status_checking:I = 0x10403aa

.field public static final ext_media_status_ejecting:I = 0x10403ab

.field public static final ext_media_status_formatting:I = 0x10403ac

.field public static final ext_media_status_missing:I = 0x10403ad

.field public static final ext_media_status_mounted:I = 0x10403ae

.field public static final ext_media_status_mounted_ro:I = 0x10403af

.field public static final ext_media_status_removed:I = 0x10403b0

.field public static final ext_media_status_unmountable:I = 0x10403b1

.field public static final ext_media_status_unmounted:I = 0x10403b2

.field public static final ext_media_status_unsupported:I = 0x10403b3

.field public static final ext_media_unmount_action:I = 0x10403b4

.field public static final ext_media_unmountable_notification_message:I = 0x10403b5

.field public static final ext_media_unmountable_notification_title:I = 0x10403b6

.field public static final ext_media_unmounting_notification_message:I = 0x10403b7

.field public static final ext_media_unmounting_notification_title:I = 0x10403b8

.field public static final ext_media_unsupported_notification_message:I = 0x10403b9

.field public static final ext_media_unsupported_notification_title:I = 0x10403ba

.field public static final face_acquired_insufficient:I = 0x10403bc

.field public static final face_acquired_not_detected:I = 0x10403bd

.field public static final face_acquired_obscured:I = 0x10403be

.field public static final face_acquired_pan_too_extreme:I = 0x10403bf

.field public static final face_acquired_poor_gaze:I = 0x10403c0

.field public static final face_acquired_recalibrate:I = 0x10403c1

.field public static final face_acquired_roll_too_extreme:I = 0x10403c2

.field public static final face_acquired_sensor_dirty:I = 0x10403c3

.field public static final face_acquired_tilt_too_extreme:I = 0x10403c4

.field public static final face_acquired_too_bright:I = 0x10403c5

.field public static final face_acquired_too_close:I = 0x10403c6

.field public static final face_acquired_too_dark:I = 0x10403c7

.field public static final face_acquired_too_different:I = 0x10403c8

.field public static final face_acquired_too_far:I = 0x10403c9

.field public static final face_acquired_too_high:I = 0x10403ca

.field public static final face_acquired_too_left:I = 0x10403cb

.field public static final face_acquired_too_low:I = 0x10403cc

.field public static final face_acquired_too_much_motion:I = 0x10403cd

.field public static final face_acquired_too_right:I = 0x10403ce

.field public static final face_acquired_too_similar:I = 0x10403cf

.field public static final face_app_setting_name:I = 0x10403d0

.field public static final face_authenticated_confirmation_required:I = 0x10403d1

.field public static final face_authenticated_no_confirmation_required:I = 0x10403d2

.field public static final face_dialog_default_subtitle:I = 0x10403d3

.field public static final face_error_canceled:I = 0x10403d4

.field public static final face_error_hw_not_available:I = 0x10403d5

.field public static final face_error_hw_not_present:I = 0x10403d6

.field public static final face_error_lockout:I = 0x10403d7

.field public static final face_error_lockout_permanent:I = 0x10403d8

.field public static final face_error_no_space:I = 0x10403da

.field public static final face_error_not_enrolled:I = 0x10403db

.field public static final face_error_security_update_required:I = 0x10403dc

.field public static final face_error_timeout:I = 0x10403dd

.field public static final face_error_unable_to_process:I = 0x10403de

.field public static final face_error_user_canceled:I = 0x10403df

.field public static final face_error_vendor_unknown:I = 0x10403e0

.field public static final face_name_template:I = 0x10403e2

.field public static final face_or_screen_lock_app_setting_name:I = 0x10403e3

.field public static final face_or_screen_lock_dialog_default_subtitle:I = 0x10403e4

.field public static final face_recalibrate_notification_content:I = 0x10403e5

.field public static final face_recalibrate_notification_name:I = 0x10403e6

.field public static final face_recalibrate_notification_title:I = 0x10403e7

.field public static final face_sensor_privacy_enabled:I = 0x10403e8

.field public static final faceunlock_multiple_failures:I = 0x10403eb

.field public static final factory_reset_message:I = 0x10403ec

.field public static final factory_reset_warning:I = 0x10403ed

.field public static final factorytest_failed:I = 0x10403ee

.field public static final factorytest_no_action:I = 0x10403ef

.field public static final factorytest_not_system:I = 0x10403f0

.field public static final factorytest_reboot:I = 0x10403f1

.field public static final failed_to_copy_to_clipboard:I = 0x10403f2

.field public static final fast_scroll_alphabet:I = 0x10403f3

.field public static final fileSizeSuffix:I = 0x10403f7

.field public static final file_count:I = 0x10403f8

.field public static final fingerprint_acquired_imager_dirty:I = 0x10403fe

.field public static final fingerprint_acquired_immobile:I = 0x1040400

.field public static final fingerprint_acquired_insufficient:I = 0x1040401

.field public static final fingerprint_acquired_partial:I = 0x1040402

.field public static final fingerprint_acquired_too_bright:I = 0x1040403

.field public static final fingerprint_acquired_too_fast:I = 0x1040404

.field public static final fingerprint_acquired_too_slow:I = 0x1040405

.field public static final fingerprint_app_setting_name:I = 0x1040407

.field public static final fingerprint_authenticated:I = 0x1040408

.field public static final fingerprint_dialog_default_subtitle:I = 0x1040409

.field public static final fingerprint_error_bad_calibration:I = 0x104040a

.field public static final fingerprint_error_canceled:I = 0x104040b

.field public static final fingerprint_error_hw_not_available:I = 0x104040c

.field public static final fingerprint_error_hw_not_present:I = 0x104040d

.field public static final fingerprint_error_lockout:I = 0x104040e

.field public static final fingerprint_error_lockout_permanent:I = 0x104040f

.field public static final fingerprint_error_no_fingerprints:I = 0x1040410

.field public static final fingerprint_error_no_space:I = 0x1040411

.field public static final fingerprint_error_not_match:I = 0x1040412

.field public static final fingerprint_error_security_update_required:I = 0x1040413

.field public static final fingerprint_error_timeout:I = 0x1040414

.field public static final fingerprint_error_unable_to_process:I = 0x1040415

.field public static final fingerprint_error_user_canceled:I = 0x1040416

.field public static final fingerprint_error_vendor_unknown:I = 0x1040417

.field public static final fingerprint_icon_content_description:I = 0x1040018

.field public static final fingerprint_name_template:I = 0x1040418

.field public static final fingerprint_or_screen_lock_app_setting_name:I = 0x1040419

.field public static final fingerprint_or_screen_lock_dialog_default_subtitle:I = 0x104041a

.field public static final fingerprint_recalibrate_notification_content:I = 0x104041b

.field public static final fingerprint_recalibrate_notification_name:I = 0x104041c

.field public static final fingerprint_recalibrate_notification_title:I = 0x104041d

.field public static final fingerprint_udfps_error_not_match:I = 0x1040420

.field public static final floating_toolbar_close_overflow_description:I = 0x1040422

.field public static final floating_toolbar_open_overflow_description:I = 0x1040423

.field public static final force_close:I = 0x1040430

.field public static final foreground_service_app_in_background:I = 0x1040431

.field public static final foreground_service_apps_in_background:I = 0x1040432

.field public static final foreground_service_multiple_separator:I = 0x1040433

.field public static final foreground_service_tap_for_details:I = 0x1040434

.field public static final forward_intent_to_owner:I = 0x1040435

.field public static final forward_intent_to_work:I = 0x1040436

.field public static final fp_power_button_bp_message:I = 0x1040437

.field public static final fp_power_button_bp_negative_button:I = 0x1040438

.field public static final fp_power_button_bp_positive_button:I = 0x1040439

.field public static final fp_power_button_bp_title:I = 0x104043a

.field public static final fp_power_button_enrollment_message:I = 0x104043b

.field public static final fp_power_button_enrollment_negative_button:I = 0x104043c

.field public static final fp_power_button_enrollment_positive_button:I = 0x104043d

.field public static final fp_power_button_enrollment_title:I = 0x104043e

.field public static final gadget_host_error_inflating:I = 0x104043f

.field public static final gigabyteShort:I = 0x1040441

.field public static final global_action_assist:I = 0x1040442

.field public static final global_action_bug_report:I = 0x1040443

.field public static final global_action_emergency:I = 0x1040444

.field public static final global_action_lockdown:I = 0x1040446

.field public static final global_action_logout:I = 0x1040447

.field public static final global_action_power_off:I = 0x1040448

.field public static final global_action_power_options:I = 0x1040449

.field public static final global_action_restart:I = 0x104044b

.field public static final global_action_screenshot:I = 0x104044c

.field public static final global_action_settings:I = 0x104044d

.field public static final global_action_silent_mode_off_status:I = 0x104044e

.field public static final global_action_silent_mode_on_status:I = 0x104044f

.field public static final global_action_toggle_silent_mode:I = 0x1040450

.field public static final global_action_voice_assist:I = 0x1040451

.field public static final global_actions:I = 0x1040452

.field public static final global_actions_airplane_mode_off_status:I = 0x1040453

.field public static final global_actions_airplane_mode_on_status:I = 0x1040454

.field public static final global_actions_toggle_airplane_mode:I = 0x1040455

.field public static final gpsNotifMessage:I = 0x1040458

.field public static final gpsNotifTicker:I = 0x1040459

.field public static final gpsNotifTitle:I = 0x104045a

.field public static final gpsVerifNo:I = 0x104045b

.field public static final gpsVerifYes:I = 0x104045c

.field public static final grant_permissions_header_text:I = 0x104045f

.field public static final granularity_label_character:I = 0x1040460

.field public static final granularity_label_line:I = 0x1040461

.field public static final granularity_label_link:I = 0x1040462

.field public static final granularity_label_word:I = 0x1040463

.field public static final gsm_alphabet_default_charset:I = 0x1040464

.field public static final guest_name:I = 0x1040465

.field public static final hardware:I = 0x1040466

.field public static final harmful_app_warning_open_anyway:I = 0x1040467

.field public static final harmful_app_warning_title:I = 0x1040468

.field public static final harmful_app_warning_uninstall:I = 0x1040469

.field public static final heavy_weight_notification:I = 0x104046a

.field public static final heavy_weight_notification_detail:I = 0x104046b

.field public static final hour_picker_description:I = 0x104046f

.field public static final httpError:I = 0x1040471

.field public static final httpErrorAuth:I = 0x1040472

.field public static final httpErrorBadUrl:I = 0x1040007

.field public static final httpErrorConnect:I = 0x1040473

.field public static final httpErrorFailedSslHandshake:I = 0x1040474

.field public static final httpErrorFile:I = 0x1040475

.field public static final httpErrorFileNotFound:I = 0x1040476

.field public static final httpErrorIO:I = 0x1040477

.field public static final httpErrorLookup:I = 0x1040478

.field public static final httpErrorOk:I = 0x1040479

.field public static final httpErrorProxyAuth:I = 0x104047a

.field public static final httpErrorRedirectLoop:I = 0x104047b

.field public static final httpErrorTimeout:I = 0x104047c

.field public static final httpErrorTooManyRequests:I = 0x104047d

.field public static final httpErrorUnsupportedAuthScheme:I = 0x104047e

.field public static final httpErrorUnsupportedScheme:I = 0x1040008

.field public static final imProtocolAim:I = 0x1040480

.field public static final imProtocolCustom:I = 0x1040481

.field public static final imProtocolGoogleTalk:I = 0x1040482

.field public static final imProtocolIcq:I = 0x1040483

.field public static final imProtocolJabber:I = 0x1040484

.field public static final imProtocolMsn:I = 0x1040485

.field public static final imProtocolNetMeeting:I = 0x1040486

.field public static final imProtocolQq:I = 0x1040487

.field public static final imProtocolSkype:I = 0x1040488

.field public static final imProtocolYahoo:I = 0x1040489

.field public static final imTypeCustom:I = 0x104048a

.field public static final imTypeHome:I = 0x104048b

.field public static final imTypeOther:I = 0x104048c

.field public static final imTypeWork:I = 0x104048d

.field public static final image_wallpaper_component:I = 0x104048e

.field public static final ime_action_default:I = 0x104048f

.field public static final ime_action_done:I = 0x1040490

.field public static final ime_action_go:I = 0x1040491

.field public static final ime_action_next:I = 0x1040492

.field public static final ime_action_previous:I = 0x1040493

.field public static final ime_action_search:I = 0x1040494

.field public static final ime_action_send:I = 0x1040495

.field public static final imei:I = 0x1040496

.field public static final importance_from_person:I = 0x104049a

.field public static final importance_from_user:I = 0x104049b

.field public static final in_progress:I = 0x104049c

.field public static final inputMethod:I = 0x104049d

.field public static final input_method_binding_label:I = 0x104049e

.field public static final install_carrier_app_notification_button:I = 0x10404a1

.field public static final install_carrier_app_notification_text:I = 0x10404a2

.field public static final install_carrier_app_notification_text_app_name:I = 0x10404a3

.field public static final install_carrier_app_notification_title:I = 0x10404a4

.field public static final invalidPin:I = 0x10404a5

.field public static final invalidPuk:I = 0x10404a6

.field public static final js_dialog_before_unload:I = 0x10404aa

.field public static final js_dialog_before_unload_negative_button:I = 0x10404ab

.field public static final js_dialog_before_unload_positive_button:I = 0x10404ac

.field public static final js_dialog_before_unload_title:I = 0x10404ad

.field public static final js_dialog_title:I = 0x10404ae

.field public static final js_dialog_title_default:I = 0x10404af

.field public static final keyboardview_keycode_alt:I = 0x10404b2

.field public static final keyboardview_keycode_cancel:I = 0x10404b3

.field public static final keyboardview_keycode_delete:I = 0x10404b4

.field public static final keyboardview_keycode_done:I = 0x10404b5

.field public static final keyboardview_keycode_enter:I = 0x10404b6

.field public static final keyboardview_keycode_mode_change:I = 0x10404b7

.field public static final keyboardview_keycode_shift:I = 0x10404b8

.field public static final keyguard_accessibility_password_unlock:I = 0x10404be

.field public static final keyguard_accessibility_pattern_unlock:I = 0x10404c0

.field public static final keyguard_accessibility_pin_unlock:I = 0x10404c1

.field public static final keyguard_accessibility_sim_pin_unlock:I = 0x10404c2

.field public static final keyguard_accessibility_sim_puk_unlock:I = 0x10404c3

.field public static final kg_text_message_separator:I = 0x10404f3

.field public static final kilobyteShort:I = 0x10404fb

.field public static final language_picker_section_all:I = 0x10404fc

.field public static final language_picker_section_suggested:I = 0x10404fd

.field public static final language_selection_title:I = 0x10404fe

.field public static final last_month:I = 0x10404ff

.field public static final last_num_days:I = 0x1040500

.field public static final launchBrowserDefault:I = 0x1040501

.field public static final launch_warning_original:I = 0x1040502

.field public static final launch_warning_replace:I = 0x1040503

.field public static final launch_warning_title:I = 0x1040504

.field public static final loading:I = 0x1040506

.field public static final location_changed_notification_text:I = 0x1040509

.field public static final location_changed_notification_title:I = 0x104050a

.field public static final lock_to_app_unlock_password:I = 0x1040514

.field public static final lock_to_app_unlock_pattern:I = 0x1040515

.field public static final lock_to_app_unlock_pin:I = 0x1040516

.field public static final lockscreen_access_pattern_area:I = 0x1040517

.field public static final lockscreen_access_pattern_cell_added:I = 0x1040518

.field public static final lockscreen_access_pattern_cell_added_verbose:I = 0x1040519

.field public static final lockscreen_access_pattern_cleared:I = 0x104051a

.field public static final lockscreen_access_pattern_detected:I = 0x104051b

.field public static final lockscreen_access_pattern_start:I = 0x104051c

.field public static final lockscreen_carrier_default:I = 0x104051d

.field public static final lockscreen_emergency_call:I = 0x104051e

.field public static final lockscreen_return_to_call:I = 0x1040539

.field public static final lockscreen_storage_locked:I = 0x1040541

.field public static final lockscreen_transport_pause_description:I = 0x1040548

.field public static final lockscreen_transport_play_description:I = 0x1040549

.field public static final log_access_confirmation_allow:I = 0x104054e

.field public static final log_access_confirmation_body:I = 0x104054f

.field public static final log_access_confirmation_deny:I = 0x1040550

.field public static final log_access_confirmation_title:I = 0x1040551

.field public static final low_internal_storage_view_text:I = 0x1040553

.field public static final low_internal_storage_view_text_no_boot:I = 0x1040554

.field public static final low_internal_storage_view_title:I = 0x1040555

.field public static final low_memory:I = 0x1040556

.field public static final managed_profile_label:I = 0x1040557

.field public static final managed_profile_label_badge:I = 0x1040558

.field public static final managed_profile_label_badge_2:I = 0x1040559

.field public static final managed_profile_label_badge_3:I = 0x104055a

.field public static final matches_found:I = 0x104055c

.field public static final media_route_chooser_title:I = 0x1040562

.field public static final media_route_chooser_title_for_remote_display:I = 0x1040563

.field public static final media_route_controller_disconnect:I = 0x1040564

.field public static final media_route_status_available:I = 0x1040565

.field public static final media_route_status_connecting:I = 0x1040566

.field public static final media_route_status_in_use:I = 0x1040567

.field public static final media_route_status_not_available:I = 0x1040568

.field public static final media_route_status_scanning:I = 0x1040569

.field public static final mediasize_chinese_om_dai_pa_kai:I = 0x104056a

.field public static final mediasize_chinese_om_jurro_ku_kai:I = 0x104056b

.field public static final mediasize_chinese_om_pa_kai:I = 0x104056c

.field public static final mediasize_chinese_prc_1:I = 0x104056d

.field public static final mediasize_chinese_prc_10:I = 0x104056e

.field public static final mediasize_chinese_prc_16k:I = 0x104056f

.field public static final mediasize_chinese_prc_2:I = 0x1040570

.field public static final mediasize_chinese_prc_3:I = 0x1040571

.field public static final mediasize_chinese_prc_4:I = 0x1040572

.field public static final mediasize_chinese_prc_5:I = 0x1040573

.field public static final mediasize_chinese_prc_6:I = 0x1040574

.field public static final mediasize_chinese_prc_7:I = 0x1040575

.field public static final mediasize_chinese_prc_8:I = 0x1040576

.field public static final mediasize_chinese_prc_9:I = 0x1040577

.field public static final mediasize_chinese_roc_16k:I = 0x1040578

.field public static final mediasize_chinese_roc_8k:I = 0x1040579

.field public static final mediasize_iso_a0:I = 0x104057a

.field public static final mediasize_iso_a1:I = 0x104057b

.field public static final mediasize_iso_a10:I = 0x104057c

.field public static final mediasize_iso_a2:I = 0x104057d

.field public static final mediasize_iso_a3:I = 0x104057e

.field public static final mediasize_iso_a4:I = 0x104057f

.field public static final mediasize_iso_a5:I = 0x1040580

.field public static final mediasize_iso_a6:I = 0x1040581

.field public static final mediasize_iso_a7:I = 0x1040582

.field public static final mediasize_iso_a8:I = 0x1040583

.field public static final mediasize_iso_a9:I = 0x1040584

.field public static final mediasize_iso_b0:I = 0x1040585

.field public static final mediasize_iso_b1:I = 0x1040586

.field public static final mediasize_iso_b10:I = 0x1040587

.field public static final mediasize_iso_b2:I = 0x1040588

.field public static final mediasize_iso_b3:I = 0x1040589

.field public static final mediasize_iso_b4:I = 0x104058a

.field public static final mediasize_iso_b5:I = 0x104058b

.field public static final mediasize_iso_b6:I = 0x104058c

.field public static final mediasize_iso_b7:I = 0x104058d

.field public static final mediasize_iso_b8:I = 0x104058e

.field public static final mediasize_iso_b9:I = 0x104058f

.field public static final mediasize_iso_c0:I = 0x1040590

.field public static final mediasize_iso_c1:I = 0x1040591

.field public static final mediasize_iso_c10:I = 0x1040592

.field public static final mediasize_iso_c2:I = 0x1040593

.field public static final mediasize_iso_c3:I = 0x1040594

.field public static final mediasize_iso_c4:I = 0x1040595

.field public static final mediasize_iso_c5:I = 0x1040596

.field public static final mediasize_iso_c6:I = 0x1040597

.field public static final mediasize_iso_c7:I = 0x1040598

.field public static final mediasize_iso_c8:I = 0x1040599

.field public static final mediasize_iso_c9:I = 0x104059a

.field public static final mediasize_japanese_chou2:I = 0x104059b

.field public static final mediasize_japanese_chou3:I = 0x104059c

.field public static final mediasize_japanese_chou4:I = 0x104059d

.field public static final mediasize_japanese_hagaki:I = 0x104059e

.field public static final mediasize_japanese_jis_b0:I = 0x104059f

.field public static final mediasize_japanese_jis_b1:I = 0x10405a0

.field public static final mediasize_japanese_jis_b10:I = 0x10405a1

.field public static final mediasize_japanese_jis_b2:I = 0x10405a2

.field public static final mediasize_japanese_jis_b3:I = 0x10405a3

.field public static final mediasize_japanese_jis_b4:I = 0x10405a4

.field public static final mediasize_japanese_jis_b5:I = 0x10405a5

.field public static final mediasize_japanese_jis_b6:I = 0x10405a6

.field public static final mediasize_japanese_jis_b7:I = 0x10405a7

.field public static final mediasize_japanese_jis_b8:I = 0x10405a8

.field public static final mediasize_japanese_jis_b9:I = 0x10405a9

.field public static final mediasize_japanese_jis_exec:I = 0x10405aa

.field public static final mediasize_japanese_kahu:I = 0x10405ab

.field public static final mediasize_japanese_kaku2:I = 0x10405ac

.field public static final mediasize_japanese_l:I = 0x10405ad

.field public static final mediasize_japanese_oufuku:I = 0x10405ae

.field public static final mediasize_japanese_you4:I = 0x10405af

.field public static final mediasize_na_ansi_c:I = 0x10405b0

.field public static final mediasize_na_ansi_d:I = 0x10405b1

.field public static final mediasize_na_ansi_e:I = 0x10405b2

.field public static final mediasize_na_ansi_f:I = 0x10405b3

.field public static final mediasize_na_arch_a:I = 0x10405b4

.field public static final mediasize_na_arch_b:I = 0x10405b5

.field public static final mediasize_na_arch_c:I = 0x10405b6

.field public static final mediasize_na_arch_d:I = 0x10405b7

.field public static final mediasize_na_arch_e:I = 0x10405b8

.field public static final mediasize_na_arch_e1:I = 0x10405b9

.field public static final mediasize_na_foolscap:I = 0x10405ba

.field public static final mediasize_na_gvrnmt_letter:I = 0x10405bb

.field public static final mediasize_na_index_3x5:I = 0x10405bc

.field public static final mediasize_na_index_4x6:I = 0x10405bd

.field public static final mediasize_na_index_5x8:I = 0x10405be

.field public static final mediasize_na_junior_legal:I = 0x10405bf

.field public static final mediasize_na_ledger:I = 0x10405c0

.field public static final mediasize_na_legal:I = 0x10405c1

.field public static final mediasize_na_letter:I = 0x10405c2

.field public static final mediasize_na_monarch:I = 0x10405c3

.field public static final mediasize_na_quarto:I = 0x10405c4

.field public static final mediasize_na_super_b:I = 0x10405c5

.field public static final mediasize_na_tabloid:I = 0x10405c6

.field public static final mediasize_unknown_landscape:I = 0x10405c7

.field public static final mediasize_unknown_portrait:I = 0x10405c8

.field public static final megabyteShort:I = 0x10405c9

.field public static final meid:I = 0x10405ca

.field public static final menu_alt_shortcut_label:I = 0x10405cb

.field public static final menu_ctrl_shortcut_label:I = 0x10405cc

.field public static final menu_delete_shortcut_label:I = 0x10405cd

.field public static final menu_enter_shortcut_label:I = 0x10405ce

.field public static final menu_function_shortcut_label:I = 0x10405cf

.field public static final menu_meta_shortcut_label:I = 0x10405d0

.field public static final menu_shift_shortcut_label:I = 0x10405d1

.field public static final menu_space_shortcut_label:I = 0x10405d2

.field public static final menu_sym_shortcut_label:I = 0x10405d3

.field public static final midnight:I = 0x10405d4

.field public static final mime_type_apk:I = 0x10405d5

.field public static final mime_type_audio:I = 0x10405d6

.field public static final mime_type_audio_ext:I = 0x10405d7

.field public static final mime_type_compressed:I = 0x10405d8

.field public static final mime_type_compressed_ext:I = 0x10405d9

.field public static final mime_type_document:I = 0x10405da

.field public static final mime_type_document_ext:I = 0x10405db

.field public static final mime_type_folder:I = 0x10405dc

.field public static final mime_type_generic:I = 0x10405dd

.field public static final mime_type_generic_ext:I = 0x10405de

.field public static final mime_type_image:I = 0x10405df

.field public static final mime_type_image_ext:I = 0x10405e0

.field public static final mime_type_presentation:I = 0x10405e1

.field public static final mime_type_presentation_ext:I = 0x10405e2

.field public static final mime_type_spreadsheet:I = 0x10405e3

.field public static final mime_type_spreadsheet_ext:I = 0x10405e4

.field public static final mime_type_video:I = 0x10405e5

.field public static final mime_type_video_ext:I = 0x10405e6

.field public static final miniresolver_open_in_personal:I = 0x10405e7

.field public static final miniresolver_open_in_work:I = 0x10405e8

.field public static final miniresolver_use_personal_browser:I = 0x10405e9

.field public static final miniresolver_use_work_browser:I = 0x10405ea

.field public static final minute_picker_description:I = 0x10405ec

.field public static final mismatchPin:I = 0x10405ee

.field public static final mmcc_authentication_reject:I = 0x10405ef

.field public static final mmcc_authentication_reject_msim_template:I = 0x10405f0

.field public static final mmcc_illegal_me:I = 0x10405f1

.field public static final mmcc_illegal_me_msim_template:I = 0x10405f2

.field public static final mmcc_illegal_ms:I = 0x10405f3

.field public static final mmcc_illegal_ms_msim_template:I = 0x10405f4

.field public static final mmcc_imsi_unknown_in_hlr:I = 0x10405f5

.field public static final mmcc_imsi_unknown_in_hlr_msim_template:I = 0x10405f6

.field public static final mmiComplete:I = 0x10405f7

.field public static final mmiError:I = 0x10405f8

.field public static final mmiErrorWhileRoaming:I = 0x10405f9

.field public static final mmiFdnError:I = 0x10405fa

.field public static final mobile_no_internet:I = 0x10405fb

.field public static final mobile_provisioning_apn:I = 0x10405fc

.field public static final mobile_provisioning_url:I = 0x10405fd

.field public static final month_day_year:I = 0x10405fe

.field public static final more_item_label:I = 0x10405ff

.field public static final mte_override_notification_message:I = 0x1040601

.field public static final mte_override_notification_title:I = 0x1040602

.field public static final muted_by:I = 0x1040604

.field public static final nas_upgrade_notification_content:I = 0x1040605

.field public static final nas_upgrade_notification_disable_action:I = 0x1040606

.field public static final nas_upgrade_notification_enable_action:I = 0x1040607

.field public static final nas_upgrade_notification_learn_more_action:I = 0x1040608

.field public static final nas_upgrade_notification_learn_more_content:I = 0x1040609

.field public static final nas_upgrade_notification_title:I = 0x104060a

.field public static final needPuk:I = 0x104060b

.field public static final needPuk2:I = 0x104060c

.field public static final negative_duration:I = 0x104060d

.field public static final network_available_sign_in:I = 0x104060e

.field public static final network_available_sign_in_detailed:I = 0x104060f

.field public static final network_logging_notification_text:I = 0x1040610

.field public static final network_logging_notification_title:I = 0x1040611

.field public static final network_partial_connectivity:I = 0x1040612

.field public static final network_partial_connectivity_detailed:I = 0x1040613

.field public static final network_switch_metered:I = 0x1040614

.field public static final network_switch_metered_detail:I = 0x1040615

.field public static final network_switch_metered_toast:I = 0x1040616

.field public static final network_switch_type_name_unknown:I = 0x1040617

.field public static final new_app_action:I = 0x1040618

.field public static final new_app_description:I = 0x1040619

.field public static final new_sms_notification_content:I = 0x104061a

.field public static final new_sms_notification_title:I = 0x104061b

.field public static final no:I = 0x1040009
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final noApplications:I = 0x104061d

.field public static final no_file_chosen:I = 0x104061e

.field public static final no_matches:I = 0x104061f

.field public static final noon:I = 0x1040622

.field public static final not_checked:I = 0x1040623

.field public static final not_selected:I = 0x1040624

.field public static final notification_action_check_bg_apps:I = 0x1040625

.field public static final notification_app_name_settings:I = 0x1040627

.field public static final notification_app_name_system:I = 0x1040628

.field public static final notification_channel_abusive_bg_apps:I = 0x104062c

.field public static final notification_channel_accessibility_magnification:I = 0x104062d

.field public static final notification_channel_accessibility_security_policy:I = 0x104062e

.field public static final notification_channel_account:I = 0x104062f

.field public static final notification_channel_alerts:I = 0x1040630

.field public static final notification_channel_call_forward:I = 0x1040631

.field public static final notification_channel_car_mode:I = 0x1040632

.field public static final notification_channel_developer:I = 0x1040633

.field public static final notification_channel_developer_important:I = 0x1040634

.field public static final notification_channel_device_admin:I = 0x1040635

.field public static final notification_channel_do_not_disturb:I = 0x1040636

.field public static final notification_channel_emergency_callback:I = 0x1040637

.field public static final notification_channel_foreground_service:I = 0x1040638

.field public static final notification_channel_heavy_weight_app:I = 0x1040639

.field public static final notification_channel_mobile_data_status:I = 0x104063b

.field public static final notification_channel_network_alert:I = 0x104063c

.field public static final notification_channel_network_alerts:I = 0x104063d

.field public static final notification_channel_network_available:I = 0x104063e

.field public static final notification_channel_network_status:I = 0x104063f

.field public static final notification_channel_physical_keyboard:I = 0x1040640

.field public static final notification_channel_retail_mode:I = 0x1040641

.field public static final notification_channel_security:I = 0x1040642

.field public static final notification_channel_sim:I = 0x1040643

.field public static final notification_channel_sim_high_prio:I = 0x1040644

.field public static final notification_channel_sms:I = 0x1040645

.field public static final notification_channel_system_changes:I = 0x1040646

.field public static final notification_channel_updates:I = 0x1040647

.field public static final notification_channel_usb:I = 0x1040648

.field public static final notification_channel_virtual_keyboard:I = 0x1040649

.field public static final notification_channel_voice_mail:I = 0x104064a

.field public static final notification_channel_vpn:I = 0x104064b

.field public static final notification_channel_wfc:I = 0x104064c

.field public static final notification_content_abusive_bg_apps:I = 0x104064d

.field public static final notification_content_long_running_fgs:I = 0x104064e

.field public static final notification_description:I = 0x104064f

.field public static final notification_feedback_indicator_alerted:I = 0x1040651

.field public static final notification_feedback_indicator_demoted:I = 0x1040652

.field public static final notification_feedback_indicator_promoted:I = 0x1040653

.field public static final notification_feedback_indicator_silenced:I = 0x1040654

.field public static final notification_header_divider_symbol_with_spaces:I = 0x1040656

.field public static final notification_hidden_text:I = 0x1040658

.field public static final notification_history_title_placeholder:I = 0x1040659

.field public static final notification_inbox_ellipsis:I = 0x104065a

.field public static final notification_listener_binding_label:I = 0x104065b

.field public static final notification_messaging_title_template:I = 0x104065c

.field public static final notification_ranker_binding_label:I = 0x104065e

.field public static final notification_title:I = 0x1040660

.field public static final notification_title_abusive_bg_apps:I = 0x1040661

.field public static final notification_title_long_running_fgs:I = 0x1040662

.field public static final notification_verified_content_description:I = 0x1040663

.field public static final notification_work_profile_content_description:I = 0x1040664

.field public static final now_string_shortest:I = 0x1040665

.field public static final number_picker_increment_scroll_action:I = 0x1040668

.field public static final number_picker_increment_scroll_mode:I = 0x1040669

.field public static final ok:I = 0x104000a

.field public static final old_app_action:I = 0x104066a

.field public static final older:I = 0x104066c

.field public static final one_handed_mode_feature_name:I = 0x104066e

.field public static final open_permission_deny:I = 0x104066f

.field public static final orgTypeCustom:I = 0x1040670

.field public static final orgTypeOther:I = 0x1040671

.field public static final orgTypeWork:I = 0x1040672

.field public static final other_networks_no_internet:I = 0x1040675

.field public static final owner_name:I = 0x1040677

.field public static final package_deleted_device_owner:I = 0x1040678

.field public static final package_installed_device_owner:I = 0x1040679

.field public static final package_updated_device_owner:I = 0x104067a

.field public static final passwordIncorrect:I = 0x104067b

.field public static final paste:I = 0x104000b

.field public static final paste_as_plain_text:I = 0x1040019

.field public static final pasted_content:I = 0x104067f

.field public static final pasted_from_app:I = 0x1040680

.field public static final pasted_from_clipboard:I = 0x1040681

.field public static final pasted_image:I = 0x1040682

.field public static final pasted_text:I = 0x1040683

.field public static final peerTtyModeFull:I = 0x1040684

.field public static final peerTtyModeHco:I = 0x1040685

.field public static final peerTtyModeOff:I = 0x1040686

.field public static final peerTtyModeVco:I = 0x1040687

.field public static final permission_request_notification_for_app_with_subtitle:I = 0x1040741

.field public static final permission_request_notification_with_subtitle:I = 0x1040743

.field public static final perms_description_app:I = 0x10407da

.field public static final perms_new_perm_prefix:I = 0x10407db

.field public static final personal_apps_suspended_turn_profile_on:I = 0x10407dc

.field public static final personal_apps_suspension_soon_text:I = 0x10407dd

.field public static final personal_apps_suspension_text:I = 0x10407de

.field public static final personal_apps_suspension_title:I = 0x10407df

.field public static final petabyteShort:I = 0x10407e0

.field public static final phoneTypeAssistant:I = 0x10407e1

.field public static final phoneTypeCallback:I = 0x10407e2

.field public static final phoneTypeCar:I = 0x10407e3

.field public static final phoneTypeCompanyMain:I = 0x10407e4

.field public static final phoneTypeCustom:I = 0x10407e5

.field public static final phoneTypeFaxHome:I = 0x10407e6

.field public static final phoneTypeFaxWork:I = 0x10407e7

.field public static final phoneTypeHome:I = 0x10407e8

.field public static final phoneTypeIsdn:I = 0x10407e9

.field public static final phoneTypeMain:I = 0x10407ea

.field public static final phoneTypeMms:I = 0x10407eb

.field public static final phoneTypeMobile:I = 0x10407ec

.field public static final phoneTypeOther:I = 0x10407ed

.field public static final phoneTypeOtherFax:I = 0x10407ee

.field public static final phoneTypePager:I = 0x10407ef

.field public static final phoneTypeRadio:I = 0x10407f0

.field public static final phoneTypeTelex:I = 0x10407f1

.field public static final phoneTypeTtyTdd:I = 0x10407f2

.field public static final phoneTypeWork:I = 0x10407f3

.field public static final phoneTypeWorkMobile:I = 0x10407f4

.field public static final phoneTypeWorkPager:I = 0x10407f5

.field public static final pin_specific_target:I = 0x10407f6

.field public static final policydesc_disableCamera:I = 0x10407f8

.field public static final policydesc_disableKeyguardFeatures:I = 0x10407f9

.field public static final policydesc_encryptedStorage:I = 0x10407fa

.field public static final policydesc_expirePassword:I = 0x10407fb

.field public static final policydesc_forceLock:I = 0x10407fc

.field public static final policydesc_limitPassword:I = 0x10407fd

.field public static final policydesc_resetPassword:I = 0x10407fe

.field public static final policydesc_setGlobalProxy:I = 0x10407ff

.field public static final policydesc_watchLogin:I = 0x1040800

.field public static final policydesc_watchLogin_secondaryUser:I = 0x1040801

.field public static final policydesc_wipeData:I = 0x1040802

.field public static final policydesc_wipeData_secondaryUser:I = 0x1040803

.field public static final policylab_disableCamera:I = 0x1040804

.field public static final policylab_disableKeyguardFeatures:I = 0x1040805

.field public static final policylab_encryptedStorage:I = 0x1040806

.field public static final policylab_expirePassword:I = 0x1040807

.field public static final policylab_forceLock:I = 0x1040808

.field public static final policylab_limitPassword:I = 0x1040809

.field public static final policylab_resetPassword:I = 0x104080a

.field public static final policylab_setGlobalProxy:I = 0x104080b

.field public static final policylab_watchLogin:I = 0x104080c

.field public static final policylab_wipeData:I = 0x104080d

.field public static final policylab_wipeData_secondaryUser:I = 0x104080e

.field public static final popup_window_default_title:I = 0x104080f

.field public static final postalTypeCustom:I = 0x1040810

.field public static final postalTypeHome:I = 0x1040811

.field public static final postalTypeOther:I = 0x1040812

.field public static final postalTypeWork:I = 0x1040813

.field public static final power_off:I = 0x1040815

.field public static final prepend_shortcut_label:I = 0x1040816

.field public static final preposition_for_date:I = 0x1040817

.field public static final preposition_for_time:I = 0x1040818

.field public static final print_service_installed_message:I = 0x104081a

.field public static final print_service_installed_title:I = 0x104081b

.field public static final printing_disabled_by:I = 0x104081c

.field public static final private_dns_broken_detailed:I = 0x104081d

.field public static final profile_encrypted_detail:I = 0x104081e

.field public static final profile_encrypted_message:I = 0x104081f

.field public static final profile_encrypted_title:I = 0x1040820

.field public static final progress_erasing:I = 0x1040821

.field public static final prohibit_manual_network_selection_in_gobal_mode:I = 0x1040822

.field public static final quick_contacts_not_available:I = 0x1040823

.field public static final radial_numbers_typeface:I = 0x1040824

.field public static final reason_service_unavailable:I = 0x1040825

.field public static final reason_unknown:I = 0x1040826

.field public static final reboot_safemode_confirm:I = 0x1040828

.field public static final reboot_safemode_title:I = 0x1040829

.field public static final reboot_to_reset_message:I = 0x104082b

.field public static final reboot_to_reset_title:I = 0x104082c

.field public static final reboot_to_update_package:I = 0x104082d

.field public static final reboot_to_update_prepare:I = 0x104082e

.field public static final reboot_to_update_reboot:I = 0x104082f

.field public static final reboot_to_update_title:I = 0x1040830

.field public static final redo:I = 0x1040832

.field public static final reduce_bright_colors_feature_name:I = 0x1040833

.field public static final region_picker_section_all:I = 0x1040834

.field public static final relationTypeAssistant:I = 0x1040835

.field public static final relationTypeBrother:I = 0x1040836

.field public static final relationTypeChild:I = 0x1040837

.field public static final relationTypeDomesticPartner:I = 0x1040839

.field public static final relationTypeFather:I = 0x104083a

.field public static final relationTypeFriend:I = 0x104083b

.field public static final relationTypeManager:I = 0x104083c

.field public static final relationTypeMother:I = 0x104083d

.field public static final relationTypeParent:I = 0x104083e

.field public static final relationTypePartner:I = 0x104083f

.field public static final relationTypeReferredBy:I = 0x1040840

.field public static final relationTypeRelative:I = 0x1040841

.field public static final relationTypeSister:I = 0x1040842

.field public static final relationTypeSpouse:I = 0x1040843

.field public static final relative_time:I = 0x1040844

.field public static final replace:I = 0x1040845

.field public static final report:I = 0x1040846

.field public static final reset:I = 0x1040847

.field public static final resolver_cant_access_personal_apps_explanation:I = 0x104084a

.field public static final resolver_cant_access_work_apps_explanation:I = 0x104084b

.field public static final resolver_cant_share_with_personal_apps_explanation:I = 0x104084c

.field public static final resolver_cant_share_with_work_apps_explanation:I = 0x104084d

.field public static final resolver_cross_profile_blocked:I = 0x104084e

.field public static final resolver_no_personal_apps_available:I = 0x104084f

.field public static final resolver_no_work_apps_available:I = 0x1040850

.field public static final resolver_personal_tab:I = 0x1040851

.field public static final resolver_personal_tab_accessibility:I = 0x1040852

.field public static final resolver_switch_on_work:I = 0x1040853

.field public static final resolver_turn_on_work_apps:I = 0x1040854

.field public static final resolver_work_tab:I = 0x1040855

.field public static final resolver_work_tab_accessibility:I = 0x1040856

.field public static final restr_pin_enter_admin_pin:I = 0x1040859

.field public static final restr_pin_enter_pin:I = 0x104085c

.field public static final restr_pin_incorrect:I = 0x104085f

.field public static final restr_pin_try_later:I = 0x1040860

.field public static final review_notification_settings_dismiss:I = 0x1040861

.field public static final review_notification_settings_remind_me_action:I = 0x1040862

.field public static final review_notification_settings_text:I = 0x1040863

.field public static final review_notification_settings_title:I = 0x1040864

.field public static final revoke:I = 0x1040865

.field public static final ringtone_default:I = 0x1040866

.field public static final ringtone_default_with_actual:I = 0x1040867

.field public static final ringtone_picker_title:I = 0x1040868

.field public static final ringtone_picker_title_alarm:I = 0x1040869

.field public static final ringtone_picker_title_notification:I = 0x104086a

.field public static final ringtone_silent:I = 0x104086b

.field public static final ringtone_unknown:I = 0x104086c

.field public static final roamingText0:I = 0x104086d

.field public static final roamingText1:I = 0x104086e

.field public static final roamingText10:I = 0x104086f

.field public static final roamingText11:I = 0x1040870

.field public static final roamingText12:I = 0x1040871

.field public static final roamingText2:I = 0x1040872

.field public static final roamingText3:I = 0x1040873

.field public static final roamingText4:I = 0x1040874

.field public static final roamingText5:I = 0x1040875

.field public static final roamingText6:I = 0x1040876

.field public static final roamingText7:I = 0x1040877

.field public static final roamingText8:I = 0x1040878

.field public static final roamingText9:I = 0x1040879

.field public static final roamingTextSearching:I = 0x104087a

.field public static final safe_media_volume_warning:I = 0x104087c

.field public static final safety_protection_display_text:I = 0x1040041

.field public static final sans_serif:I = 0x104087d

.field public static final save_password_label:I = 0x104087e

.field public static final save_password_message:I = 0x104087f

.field public static final save_password_never:I = 0x1040880

.field public static final save_password_notnow:I = 0x1040881

.field public static final save_password_remember:I = 0x1040882

.field public static final screen_lock_app_setting_name:I = 0x1040887

.field public static final screen_lock_dialog_default_subtitle:I = 0x1040888

.field public static final screenshot_edit:I = 0x1040889

.field public static final search_go:I = 0x104000c

.field public static final search_language_hint:I = 0x104088b

.field public static final selectAll:I = 0x104000d

.field public static final selectTextMode:I = 0x1040016

.field public static final select_day:I = 0x1040894

.field public static final select_hours:I = 0x1040895

.field public static final select_input_method:I = 0x1040896

.field public static final select_keyboard_layout_notification_message:I = 0x1040897

.field public static final select_keyboard_layout_notification_title:I = 0x1040898

.field public static final select_minutes:I = 0x1040899

.field public static final select_year:I = 0x104089a

.field public static final selected:I = 0x104089b

.field public static final sendText:I = 0x104089c

.field public static final sending:I = 0x104089d

.field public static final sensor_privacy_notification_channel_label:I = 0x104089f

.field public static final sensor_privacy_start_use_camera_notification_content_title:I = 0x10408a0

.field public static final sensor_privacy_start_use_dialog_turn_on_button:I = 0x10408a1

.field public static final sensor_privacy_start_use_mic_notification_content_title:I = 0x10408a2

.field public static final sensor_privacy_start_use_notification_content_text:I = 0x10408a3

.field public static final serviceClassData:I = 0x10408a5

.field public static final serviceClassDataAsync:I = 0x10408a6

.field public static final serviceClassDataSync:I = 0x10408a7

.field public static final serviceClassFAX:I = 0x10408a8

.field public static final serviceClassPAD:I = 0x10408a9

.field public static final serviceClassPacket:I = 0x10408aa

.field public static final serviceClassSMS:I = 0x10408ab

.field public static final serviceClassVoice:I = 0x10408ac

.field public static final serviceDisabled:I = 0x10408ad

.field public static final serviceEnabled:I = 0x10408ae

.field public static final serviceEnabledFor:I = 0x10408af

.field public static final serviceErased:I = 0x10408b0

.field public static final serviceNotProvisioned:I = 0x10408b1

.field public static final serviceRegistered:I = 0x10408b2

.field public static final setup_autofill:I = 0x10408b3

.field public static final share:I = 0x10408b6

.field public static final share_remote_bugreport_action:I = 0x10408b8

.field public static final share_remote_bugreport_notification_message_finished:I = 0x10408b9

.field public static final share_remote_bugreport_notification_title:I = 0x10408ba

.field public static final shareactionprovider_share_with:I = 0x10408bb

.field public static final shareactionprovider_share_with_application:I = 0x10408bc

.field public static final sharing_remote_bugreport_notification_title:I = 0x10408bd

.field public static final shortcut_disabled_reason_unknown:I = 0x10408be

.field public static final shortcut_restore_not_supported:I = 0x10408bf

.field public static final shortcut_restore_signature_mismatch:I = 0x10408c0

.field public static final shortcut_restore_unknown_issue:I = 0x10408c1

.field public static final shortcut_restored_on_lower_version:I = 0x10408c2

.field public static final shutdown_confirm:I = 0x10408c4

.field public static final shutdown_confirm_question:I = 0x10408c5

.field public static final shutdown_progress:I = 0x10408c6

.field public static final sim_added_message:I = 0x10408cb

.field public static final sim_added_title:I = 0x10408cc

.field public static final sim_removed_message:I = 0x10408ce

.field public static final sim_removed_title:I = 0x10408cf

.field public static final sim_restart_button:I = 0x10408d0

.field public static final sipAddressTypeCustom:I = 0x10408d1

.field public static final sipAddressTypeHome:I = 0x10408d2

.field public static final sipAddressTypeOther:I = 0x10408d3

.field public static final sipAddressTypeWork:I = 0x10408d4

.field public static final slice_more_content:I = 0x10408d6

.field public static final slices_permission_request:I = 0x10408d7

.field public static final sms_control_message:I = 0x10408d8

.field public static final sms_control_no:I = 0x10408d9

.field public static final sms_control_title:I = 0x10408da

.field public static final sms_control_yes:I = 0x10408db

.field public static final sms_premium_short_code_details:I = 0x10408dc

.field public static final sms_short_code_confirm_allow:I = 0x10408dd

.field public static final sms_short_code_confirm_always_allow:I = 0x10408de

.field public static final sms_short_code_confirm_deny:I = 0x10408df

.field public static final sms_short_code_confirm_message:I = 0x10408e0

.field public static final sms_short_code_confirm_never_allow:I = 0x10408e1

.field public static final sms_short_code_details:I = 0x10408e2

.field public static final sms_short_code_remember_undo_instruction:I = 0x10408e4

.field public static final smv_application:I = 0x10408e5

.field public static final smv_process:I = 0x10408e6

.field public static final ssl_ca_cert_noti_by_administrator:I = 0x10408e9

.field public static final ssl_ca_cert_noti_by_unknown:I = 0x10408ea

.field public static final ssl_ca_cert_noti_managed:I = 0x10408eb

.field public static final ssl_ca_cert_warning:I = 0x10408ec

.field public static final ssl_certificate:I = 0x10408ed

.field public static final ssl_certificate_is_valid:I = 0x10408ee

.field public static final status_bar_airplane:I = 0x10408ef

.field public static final status_bar_alarm_clock:I = 0x10408f0

.field public static final status_bar_battery:I = 0x10408f1

.field public static final status_bar_bluetooth:I = 0x10408f2

.field public static final status_bar_call_strength:I = 0x10408f3

.field public static final status_bar_camera:I = 0x10408f4

.field public static final status_bar_cast:I = 0x10408f5

.field public static final status_bar_cdma_eri:I = 0x10408f6

.field public static final status_bar_clock:I = 0x10408f7

.field public static final status_bar_data_connection:I = 0x10408f8

.field public static final status_bar_data_saver:I = 0x10408f9

.field public static final status_bar_ethernet:I = 0x10408fa

.field public static final status_bar_headset:I = 0x10408fb

.field public static final status_bar_hotspot:I = 0x10408fc

.field public static final status_bar_ime:I = 0x10408fd

.field public static final status_bar_location:I = 0x10408fe

.field public static final status_bar_managed_profile:I = 0x10408ff

.field public static final status_bar_microphone:I = 0x1040900

.field public static final status_bar_mobile:I = 0x1040901

.field public static final status_bar_mute:I = 0x1040902

.field public static final status_bar_nfc:I = 0x1040903

.field public static final status_bar_no_calling:I = 0x1040904

.field public static final status_bar_notification_info_overflow:I = 0x1040017

.field public static final status_bar_phone_evdo_signal:I = 0x1040905

.field public static final status_bar_phone_signal:I = 0x1040906

.field public static final status_bar_rotate:I = 0x1040907

.field public static final status_bar_screen_record:I = 0x1040908

.field public static final status_bar_secure:I = 0x1040909

.field public static final status_bar_sensors_off:I = 0x104090a

.field public static final status_bar_speakerphone:I = 0x104090b

.field public static final status_bar_sync_active:I = 0x104090c

.field public static final status_bar_sync_failing:I = 0x104090d

.field public static final status_bar_tty:I = 0x104090e

.field public static final status_bar_volume:I = 0x104090f

.field public static final status_bar_vpn:I = 0x1040910

.field public static final status_bar_wifi:I = 0x1040911

.field public static final status_bar_zen:I = 0x1040912

.field public static final status_communication_speaker_summary:I = 0x1040913

.field public static final status_communication_summary:I = 0x1040914

.field public static final stk_cc_ss_to_dial:I = 0x1040915

.field public static final stk_cc_ss_to_dial_video:I = 0x1040916

.field public static final stk_cc_ss_to_ss:I = 0x1040917

.field public static final stk_cc_ss_to_ussd:I = 0x1040918

.field public static final stk_cc_ussd_to_dial:I = 0x1040919

.field public static final stk_cc_ussd_to_dial_video:I = 0x104091a

.field public static final stk_cc_ussd_to_ss:I = 0x104091b

.field public static final stk_cc_ussd_to_ussd:I = 0x104091c

.field public static final storage_internal:I = 0x104091d

.field public static final storage_sd_card:I = 0x104091e

.field public static final storage_sd_card_label:I = 0x104091f

.field public static final storage_usb:I = 0x1040920

.field public static final storage_usb_drive:I = 0x1040921

.field public static final storage_usb_drive_label:I = 0x1040922

.field public static final submit:I = 0x1040923

.field public static final suspended_widget_accessibility:I = 0x1040925

.field public static final sync_binding_label:I = 0x1040926

.field public static final sync_do_nothing:I = 0x1040927

.field public static final sync_really_delete:I = 0x1040928

.field public static final sync_too_many_deletes_desc:I = 0x104092a

.field public static final sync_undo_deletes:I = 0x104092b

.field public static final system_error_manufacturer:I = 0x104092c

.field public static final system_error_wipe_data:I = 0x104092d

.field public static final system_locale_title:I = 0x104092e

.field public static final system_ui_date_pattern:I = 0x104092f

.field public static final taking_remote_bugreport_notification_title:I = 0x1040930

.field public static final terabyteShort:I = 0x1040931

.field public static final test_harness_mode_notification_message:I = 0x1040932

.field public static final test_harness_mode_notification_title:I = 0x1040933

.field public static final tethered_notification_multi_device_message:I = 0x1040935

.field public static final tethered_notification_no_device_message:I = 0x1040936

.field public static final tethered_notification_one_device_message:I = 0x1040937

.field public static final tethered_notification_title:I = 0x1040938

.field public static final textSelectionCABTitle:I = 0x1040939

.field public static final text_copied:I = 0x104093a

.field public static final time_of_day:I = 0x104093b

.field public static final time_picker_decrement_hour_button:I = 0x104093c

.field public static final time_picker_decrement_minute_button:I = 0x104093d

.field public static final time_picker_decrement_set_am_button:I = 0x104093e

.field public static final time_picker_dialog_title:I = 0x104093f

.field public static final time_picker_increment_hour_button:I = 0x1040942

.field public static final time_picker_increment_minute_button:I = 0x1040943

.field public static final time_picker_increment_set_pm_button:I = 0x1040944

.field public static final time_picker_radial_mode_description:I = 0x1040949

.field public static final time_picker_text_input_mode_description:I = 0x104094a

.field public static final time_placeholder:I = 0x104094b

.field public static final tooltip_popup_title:I = 0x104094d

.field public static final turn_on_magnification_settings_action:I = 0x104094f

.field public static final ui_translation_accessibility_translated_text:I = 0x1040953

.field public static final ui_translation_accessibility_translation_finished:I = 0x1040954

.field public static final undo:I = 0x1040955

.field public static final unknownName:I = 0x104000e

.field public static final unpin_specific_target:I = 0x1040956

.field public static final unread_convo_overflow:I = 0x1040958

.field public static final unsupported_compile_sdk_check_update:I = 0x1040959

.field public static final unsupported_compile_sdk_message:I = 0x104095a

.field public static final unsupported_display_size_message:I = 0x104095c

.field public static final untitled:I = 0x104000f

.field public static final upload_file:I = 0x104095e

.field public static final usb_accessory_notification_title:I = 0x104095f

.field public static final usb_cd_installer_notification_title:I = 0x1040960

.field public static final usb_charging_notification_title:I = 0x1040961

.field public static final usb_contaminant_detected_message:I = 0x1040962

.field public static final usb_contaminant_detected_title:I = 0x1040963

.field public static final usb_contaminant_not_detected_message:I = 0x1040964

.field public static final usb_contaminant_not_detected_title:I = 0x1040965

.field public static final usb_device_resolve_prompt_warn:I = 0x1040966

.field public static final usb_midi_notification_title:I = 0x1040967

.field public static final usb_midi_peripheral_manufacturer_name:I = 0x1040968

.field public static final usb_midi_peripheral_name:I = 0x1040969

.field public static final usb_midi_peripheral_product_name:I = 0x104096a

.field public static final usb_mtp_launch_notification_description:I = 0x104096b

.field public static final usb_mtp_launch_notification_title:I = 0x104096c

.field public static final usb_mtp_notification_title:I = 0x104096d

.field public static final usb_notification_message:I = 0x104096e

.field public static final usb_power_notification_message:I = 0x104096f

.field public static final usb_ptp_notification_title:I = 0x1040970

.field public static final usb_supplying_notification_title:I = 0x104097e

.field public static final usb_tether_notification_title:I = 0x104097f

.field public static final usb_unsupported_audio_accessory_message:I = 0x1040980

.field public static final usb_unsupported_audio_accessory_title:I = 0x1040981

.field public static final user_creation_account_exists:I = 0x1040983

.field public static final user_creation_adding:I = 0x1040984

.field public static final user_logging_out_message:I = 0x1040988

.field public static final user_owner_label:I = 0x1040989

.field public static final user_switched:I = 0x104098a

.field public static final user_switching_message:I = 0x104098b

.field public static final vdm_camera_access_denied:I = 0x104098d

.field public static final vendor_required_attestation_revocation_list_url:I = 0x104098e

.field public static final view_and_control_notification_content:I = 0x104098f

.field public static final view_and_control_notification_title:I = 0x1040990

.field public static final volume_alarm:I = 0x1040991

.field public static final volume_dialog_ringer_guidance_silent:I = 0x1040994

.field public static final volume_dialog_ringer_guidance_vibrate:I = 0x1040995

.field public static final volume_icon_description_bluetooth:I = 0x1040996

.field public static final volume_icon_description_incall:I = 0x1040997

.field public static final volume_icon_description_media:I = 0x1040998

.field public static final volume_icon_description_notification:I = 0x1040999

.field public static final volume_icon_description_ringer:I = 0x104099a

.field public static final vpn_lockdown_config:I = 0x10409a1

.field public static final vpn_lockdown_connected:I = 0x10409a2

.field public static final vpn_lockdown_connecting:I = 0x10409a3

.field public static final vpn_lockdown_disconnected:I = 0x10409a4

.field public static final vpn_lockdown_error:I = 0x10409a5

.field public static final vpn_text:I = 0x10409a6

.field public static final vpn_text_long:I = 0x10409a7

.field public static final vpn_title:I = 0x10409a8

.field public static final vpn_title_long:I = 0x10409a9

.field public static final vr_listener_binding_label:I = 0x10409aa

.field public static final wait:I = 0x10409ab

.field public static final wallpaper_binding_label:I = 0x10409ac

.field public static final webpage_unresponsive:I = 0x10409ad

.field public static final websearch:I = 0x10409ae

.field public static final wfcRegErrorTitle:I = 0x10409b1

.field public static final wfc_mode_cellular_preferred_summary:I = 0x10409bf

.field public static final wfc_mode_ims_preferred_summary:I = 0x10409c0

.field public static final wfc_mode_wifi_only_summary:I = 0x10409c1

.field public static final wfc_mode_wifi_preferred_summary:I = 0x10409c2

.field public static final whichApplication:I = 0x10409c3

.field public static final whichApplicationLabel:I = 0x10409c4

.field public static final whichApplicationNamed:I = 0x10409c5

.field public static final whichEditApplication:I = 0x10409c6

.field public static final whichEditApplicationLabel:I = 0x10409c7

.field public static final whichEditApplicationNamed:I = 0x10409c8

.field public static final whichGiveAccessToApplicationLabel:I = 0x10409c9

.field public static final whichHomeApplication:I = 0x10409ca

.field public static final whichHomeApplicationLabel:I = 0x10409cb

.field public static final whichHomeApplicationNamed:I = 0x10409cc

.field public static final whichImageCaptureApplication:I = 0x10409cd

.field public static final whichImageCaptureApplicationLabel:I = 0x10409ce

.field public static final whichImageCaptureApplicationNamed:I = 0x10409cf

.field public static final whichOpenHostLinksWith:I = 0x10409d0

.field public static final whichOpenHostLinksWithApp:I = 0x10409d1

.field public static final whichOpenLinksWith:I = 0x10409d2

.field public static final whichOpenLinksWithApp:I = 0x10409d3

.field public static final whichSendApplication:I = 0x10409d4

.field public static final whichSendApplicationLabel:I = 0x10409d5

.field public static final whichSendApplicationNamed:I = 0x10409d6

.field public static final whichSendToApplication:I = 0x10409d7

.field public static final whichSendToApplicationLabel:I = 0x10409d8

.field public static final whichSendToApplicationNamed:I = 0x10409d9

.field public static final whichViewApplication:I = 0x10409da

.field public static final whichViewApplicationLabel:I = 0x10409db

.field public static final whichViewApplicationNamed:I = 0x10409dc

.field public static final widget_default_class_name:I = 0x10409dd

.field public static final widget_default_package_name:I = 0x10409de

.field public static final wifi_available_sign_in:I = 0x10409df

.field public static final wifi_calling_off_summary:I = 0x10409e1

.field public static final wifi_no_internet:I = 0x10409e5

.field public static final wifi_no_internet_detailed:I = 0x10409e6

.field public static final window_magnification_prompt_content:I = 0x10409f5

.field public static final window_magnification_prompt_title:I = 0x10409f6

.field public static final wireless_display_route_description:I = 0x10409f7

.field public static final work_mode_off_message:I = 0x10409f8

.field public static final work_mode_off_title:I = 0x10409f9

.field public static final work_mode_turn_on:I = 0x10409fa

.field public static final work_profile_deleted:I = 0x10409fb

.field public static final work_profile_deleted_description_dpm_wipe:I = 0x10409fd

.field public static final work_profile_deleted_details:I = 0x10409fe

.field public static final work_profile_deleted_reason_maximum_password_failure:I = 0x10409ff

.field public static final write_fail_reason_cancelled:I = 0x1040a00

.field public static final write_fail_reason_cannot_write:I = 0x1040a01

.field public static final yes:I = 0x1040013
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final zen_mode_alarm:I = 0x1040a04

.field public static final zen_mode_default_events_name:I = 0x1040a05

.field public static final zen_mode_default_every_night_name:I = 0x1040a06

.field public static final zen_mode_default_weekends_name:I = 0x1040a07

.field public static final zen_mode_default_weeknights_name:I = 0x1040a08

.field public static final zen_mode_downtime_feature_name:I = 0x1040a09

.field public static final zen_mode_duration_hours:I = 0x1040a0a

.field public static final zen_mode_duration_hours_short:I = 0x1040a0b

.field public static final zen_mode_duration_hours_summary:I = 0x1040a0c

.field public static final zen_mode_duration_hours_summary_short:I = 0x1040a0d

.field public static final zen_mode_duration_minutes:I = 0x1040a0e

.field public static final zen_mode_duration_minutes_short:I = 0x1040a0f

.field public static final zen_mode_duration_minutes_summary:I = 0x1040a10

.field public static final zen_mode_duration_minutes_summary_short:I = 0x1040a11

.field public static final zen_mode_feature_name:I = 0x1040a12

.field public static final zen_mode_forever:I = 0x1040a13

.field public static final zen_mode_forever_dnd:I = 0x1040a14

.field public static final zen_mode_rule_name_combination:I = 0x1040a15

.field public static final zen_mode_until:I = 0x1040a16

.field public static final zen_mode_until_next_day:I = 0x1040a17

.field public static final zen_upgrade_notification_content:I = 0x1040a18

.field public static final zen_upgrade_notification_title:I = 0x1040a19

.field public static final zen_upgrade_notification_visd_content:I = 0x1040a1a

.field public static final zen_upgrade_notification_visd_title:I = 0x1040a1b


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
