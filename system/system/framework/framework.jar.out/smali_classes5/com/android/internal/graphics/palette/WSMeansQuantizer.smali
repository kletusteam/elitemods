.class public final Lcom/android/internal/graphics/palette/WSMeansQuantizer;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/android/internal/graphics/palette/Quantizer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/graphics/palette/WSMeansQuantizer$Distance;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z = false

.field private static final DEBUG:Z = false

.field private static final MAX_ITERATIONS:I = 0xa

.field private static final MIN_MOVEMENT_DISTANCE:F = 3.0f

.field private static final TAG:Ljava/lang/String; = "QuantizerWsmeans"


# instance fields
.field private mClusterIndices:[I

.field private mClusterPopulations:[I

.field private mClusters:[[F

.field private mDistanceMatrix:[[F

.field private mIndexMatrix:[[I

.field private mInputPixelToCount:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mPalette:Lcom/android/internal/graphics/palette/Palette;

.field private mPixels:[I

.field private final mPointProvider:Lcom/android/internal/graphics/palette/PointProvider;

.field private mPoints:[[F


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>([ILcom/android/internal/graphics/palette/PointProvider;Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I",
            "Lcom/android/internal/graphics/palette/PointProvider;",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    new-array v1, v0, [[I

    iput-object v1, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mIndexMatrix:[[I

    new-array v1, v0, [[F

    iput-object v1, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mDistanceMatrix:[[F

    iput-object p2, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mPointProvider:Lcom/android/internal/graphics/palette/PointProvider;

    array-length v1, p1

    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x1

    const/4 v4, 0x3

    aput v4, v2, v3

    aput v1, v2, v0

    const-class v1, F

    invoke-static {v1, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [[F

    iput-object v1, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mClusters:[[F

    const/4 v1, 0x0

    array-length v2, p1

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, p1, v0

    invoke-interface {p2, v3}, Lcom/android/internal/graphics/palette/PointProvider;->fromInt(I)[F

    move-result-object v4

    iget-object v5, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mClusters:[[F

    add-int/lit8 v6, v1, 0x1

    aput-object v4, v5, v1

    add-int/lit8 v0, v0, 0x1

    move v1, v6

    goto :goto_0

    :cond_0
    iput-object p3, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mInputPixelToCount:Ljava/util/Map;

    return-void
.end method

.method private initializeClusters(I)V
    .locals 13

    iget-object v0, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mClusters:[[F

    array-length v1, v0

    const/4 v2, 0x0

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_0
    const-wide/32 v3, 0x42688

    if-nez v1, :cond_3

    array-length v0, v0

    sub-int v0, p1, v0

    new-instance v5, Ljava/util/Random;

    invoke-direct {v5, v3, v4}, Ljava/util/Random;-><init>(J)V

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    const/4 v8, 0x0

    :goto_1
    if-ge v8, v0, :cond_2

    iget-object v9, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mPoints:[[F

    array-length v9, v9

    invoke-virtual {v5, v9}, Ljava/util/Random;->nextInt(I)I

    move-result v9

    :goto_2
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v7, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v10

    iget-object v11, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mPoints:[[F

    array-length v12, v11

    if-ge v10, v12, :cond_1

    array-length v10, v11

    invoke-virtual {v5, v10}, Ljava/util/Random;->nextInt(I)I

    move-result v9

    goto :goto_2

    :cond_1
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v7, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v10, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mPoints:[[F

    aget-object v10, v10, v9

    invoke-interface {v6, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_2
    invoke-interface {v6}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [[F

    iget-object v9, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mClusters:[[F

    invoke-static {v9, p1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [[F

    array-length v10, v9

    array-length v11, v8

    invoke-static {v8, v2, v9, v10, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v9, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mClusters:[[F

    :cond_3
    iget-object v0, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mPixels:[I

    array-length v2, v0

    new-array v2, v2, [I

    iput-object v2, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mClusterIndices:[I

    array-length v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mClusterPopulations:[I

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0, v3, v4}, Ljava/util/Random;-><init>(J)V

    const/4 v2, 0x0

    :goto_3
    iget-object v3, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mPixels:[I

    array-length v3, v3

    if-ge v2, v3, :cond_4

    invoke-virtual {v0, p1}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    iget-object v4, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mClusterIndices:[I

    aput v3, v4, v2

    iget-object v4, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mClusterPopulations:[I

    iget-object v5, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mInputPixelToCount:Ljava/util/Map;

    iget-object v6, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mPixels:[I

    aget v6, v6, v2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    aput v5, v4, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_4
    return-void
.end method

.method static synthetic lambda$calculateClusterDistances$0(Lcom/android/internal/graphics/palette/WSMeansQuantizer$Distance;Lcom/android/internal/graphics/palette/WSMeansQuantizer$Distance;)I
    .locals 2

    invoke-virtual {p0}, Lcom/android/internal/graphics/palette/WSMeansQuantizer$Distance;->getDistance()F

    move-result v0

    invoke-virtual {p1}, Lcom/android/internal/graphics/palette/WSMeansQuantizer$Distance;->getDistance()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    return v0
.end method


# virtual methods
.method calculateClusterDistances(I)V
    .locals 8

    goto/32 :goto_28

    nop

    :goto_0
    if-le v0, p1, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_25

    nop

    :goto_1
    aget-object v4, v4, v0

    goto/32 :goto_2a

    nop

    :goto_2
    const/4 v0, 0x0

    :goto_3
    goto/32 :goto_2b

    nop

    :goto_4
    iget-object v5, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mPointProvider:Lcom/android/internal/graphics/palette/PointProvider;

    goto/32 :goto_18

    nop

    :goto_5
    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_49

    nop

    :goto_6
    iget-object v3, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mIndexMatrix:[[I

    goto/32 :goto_22

    nop

    :goto_7
    return-void

    :goto_8
    aput p1, v0, v3

    goto/32 :goto_42

    nop

    :goto_9
    goto/16 :goto_31

    :goto_a
    goto/32 :goto_15

    nop

    :goto_b
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_40

    nop

    :goto_c
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->sort(Ljava/util/Comparator;)V

    goto/32 :goto_20

    nop

    :goto_d
    aput p1, v0, v3

    goto/32 :goto_32

    nop

    :goto_e
    iget-object v4, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mDistanceMatrix:[[F

    goto/32 :goto_1

    nop

    :goto_f
    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(I)V

    goto/32 :goto_4d

    nop

    :goto_10
    array-length v0, v0

    goto/32 :goto_52

    nop

    :goto_11
    new-instance v2, Lcom/android/internal/graphics/palette/WSMeansQuantizer$$ExternalSyntheticLambda0;

    goto/32 :goto_2c

    nop

    :goto_12
    check-cast v4, Lcom/android/internal/graphics/palette/WSMeansQuantizer$Distance;

    goto/32 :goto_24

    nop

    :goto_13
    invoke-static {v4, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_1d

    nop

    :goto_14
    aput v5, v6, v4

    goto/32 :goto_33

    nop

    :goto_15
    iget-object v0, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mIndexMatrix:[[I

    goto/32 :goto_10

    nop

    :goto_16
    aget-object v7, v6, v4

    goto/32 :goto_3f

    nop

    :goto_17
    new-array v0, v2, [I

    goto/32 :goto_8

    nop

    :goto_18
    iget-object v6, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mClusters:[[F

    goto/32 :goto_3e

    nop

    :goto_19
    iput-object v0, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mIndexMatrix:[[I

    :goto_1a
    goto/32 :goto_2

    nop

    :goto_1b
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_9

    nop

    :goto_1c
    if-lt v4, p1, :cond_1

    goto/32 :goto_44

    :cond_1
    goto/32 :goto_4

    nop

    :goto_1d
    check-cast v0, [[F

    goto/32 :goto_1e

    nop

    :goto_1e
    iput-object v0, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mDistanceMatrix:[[F

    :goto_1f
    goto/32 :goto_30

    nop

    :goto_20
    const/4 v2, 0x0

    :goto_21
    goto/32 :goto_2d

    nop

    :goto_22
    aget-object v3, v3, v0

    goto/32 :goto_34

    nop

    :goto_23
    const-class v4, F

    goto/32 :goto_13

    nop

    :goto_24
    invoke-virtual {v4}, Lcom/android/internal/graphics/palette/WSMeansQuantizer$Distance;->getIndex()I

    move-result v4

    goto/32 :goto_50

    nop

    :goto_25
    add-int/lit8 v4, v0, 0x1

    :goto_26
    goto/32 :goto_1c

    nop

    :goto_27
    const/4 v3, 0x1

    goto/32 :goto_38

    nop

    :goto_28
    iget-object v0, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mDistanceMatrix:[[F

    goto/32 :goto_4b

    nop

    :goto_29
    new-instance v3, Lcom/android/internal/graphics/palette/WSMeansQuantizer$Distance;

    goto/32 :goto_e

    nop

    :goto_2a
    aget v4, v4, v2

    goto/32 :goto_4c

    nop

    :goto_2b
    if-lt v0, p1, :cond_2

    goto/32 :goto_47

    :cond_2
    goto/32 :goto_2e

    nop

    :goto_2c
    invoke-direct {v2}, Lcom/android/internal/graphics/palette/WSMeansQuantizer$$ExternalSyntheticLambda0;-><init>()V

    goto/32 :goto_c

    nop

    :goto_2d
    if-lt v2, p1, :cond_3

    goto/32 :goto_36

    :cond_3
    goto/32 :goto_6

    nop

    :goto_2e
    new-instance v1, Ljava/util/ArrayList;

    goto/32 :goto_f

    nop

    :goto_2f
    const-class v1, I

    goto/32 :goto_5

    nop

    :goto_30
    const/4 v0, 0x0

    :goto_31
    goto/32 :goto_0

    nop

    :goto_32
    aput p1, v0, v1

    goto/32 :goto_2f

    nop

    :goto_33
    add-int/lit8 v4, v4, 0x1

    goto/32 :goto_43

    nop

    :goto_34
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_12

    nop

    :goto_35
    goto :goto_21

    :goto_36
    goto/32 :goto_3c

    nop

    :goto_37
    iget-object v6, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mDistanceMatrix:[[F

    goto/32 :goto_16

    nop

    :goto_38
    if-ne v0, p1, :cond_4

    goto/32 :goto_1f

    :cond_4
    goto/32 :goto_17

    nop

    :goto_39
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_b

    nop

    :goto_3a
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_35

    nop

    :goto_3b
    if-lt v2, p1, :cond_5

    goto/32 :goto_41

    :cond_5
    goto/32 :goto_29

    nop

    :goto_3c
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_46

    nop

    :goto_3d
    const/4 v2, 0x2

    goto/32 :goto_27

    nop

    :goto_3e
    aget-object v7, v6, v0

    goto/32 :goto_4a

    nop

    :goto_3f
    aput v5, v7, v0

    goto/32 :goto_48

    nop

    :goto_40
    goto :goto_4e

    :goto_41
    goto/32 :goto_11

    nop

    :goto_42
    aput p1, v0, v1

    goto/32 :goto_23

    nop

    :goto_43
    goto/16 :goto_26

    :goto_44
    goto/32 :goto_1b

    nop

    :goto_45
    invoke-interface {v5, v7, v6}, Lcom/android/internal/graphics/palette/PointProvider;->distance([F[F)F

    move-result v5

    goto/32 :goto_37

    nop

    :goto_46
    goto/16 :goto_3

    :goto_47
    goto/32 :goto_7

    nop

    :goto_48
    aget-object v6, v6, v0

    goto/32 :goto_14

    nop

    :goto_49
    check-cast v0, [[I

    goto/32 :goto_19

    nop

    :goto_4a
    aget-object v6, v6, v4

    goto/32 :goto_45

    nop

    :goto_4b
    array-length v0, v0

    goto/32 :goto_51

    nop

    :goto_4c
    invoke-direct {v3, v2, v4}, Lcom/android/internal/graphics/palette/WSMeansQuantizer$Distance;-><init>(IF)V

    goto/32 :goto_39

    nop

    :goto_4d
    const/4 v2, 0x0

    :goto_4e
    goto/32 :goto_3b

    nop

    :goto_4f
    new-array v0, v2, [I

    goto/32 :goto_d

    nop

    :goto_50
    aput v4, v3, v2

    goto/32 :goto_3a

    nop

    :goto_51
    const/4 v1, 0x0

    goto/32 :goto_3d

    nop

    :goto_52
    if-ne v0, p1, :cond_6

    goto/32 :goto_1a

    :cond_6
    goto/32 :goto_4f

    nop
.end method

.method public getQuantizedColors()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/android/internal/graphics/palette/Palette$Swatch;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mPalette:Lcom/android/internal/graphics/palette/Palette;

    invoke-virtual {v0}, Lcom/android/internal/graphics/palette/Palette;->getSwatches()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public quantize([II)V
    .locals 7

    nop

    iget-object v0, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mInputPixelToCount:Ljava/util/Map;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/internal/graphics/palette/QuantizerMap;

    invoke-direct {v0}, Lcom/android/internal/graphics/palette/QuantizerMap;-><init>()V

    invoke-virtual {v0, p1, p2}, Lcom/android/internal/graphics/palette/QuantizerMap;->quantize([II)V

    invoke-virtual {v0}, Lcom/android/internal/graphics/palette/QuantizerMap;->getColorToCount()Ljava/util/Map;

    move-result-object v1

    iput-object v1, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mInputPixelToCount:Ljava/util/Map;

    :cond_0
    iget-object v0, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mInputPixelToCount:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/4 v1, 0x3

    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x1

    aput v1, v2, v3

    const/4 v1, 0x0

    aput v0, v2, v1

    const-class v0, F

    invoke-static {v0, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[F

    iput-object v0, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mPoints:[[F

    iget-object v0, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mInputPixelToCount:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mPixels:[I

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mInputPixelToCount:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mPixels:[I

    aput v2, v3, v0

    iget-object v3, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mPoints:[[F

    iget-object v4, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mPointProvider:Lcom/android/internal/graphics/palette/PointProvider;

    invoke-interface {v4, v2}, Lcom/android/internal/graphics/palette/PointProvider;->fromInt(I)[F

    move-result-object v4

    aput-object v4, v3, v0

    nop

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mClusters:[[F

    array-length v2, v1

    if-lez v2, :cond_2

    array-length v1, v1

    invoke-static {p2, v1}, Ljava/lang/Math;->min(II)I

    move-result p2

    :cond_2
    iget-object v1, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mPoints:[[F

    array-length v1, v1

    invoke-static {p2, v1}, Ljava/lang/Math;->min(II)I

    move-result p2

    invoke-direct {p0, p2}, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->initializeClusters(I)V

    const/4 v1, 0x0

    :goto_1
    const/16 v2, 0xa

    if-ge v1, v2, :cond_4

    invoke-virtual {p0, p2}, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->calculateClusterDistances(I)V

    invoke-virtual {p0, p2}, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->reassignPoints(I)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_2

    :cond_3
    invoke-virtual {p0, p2}, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->recalculateClusterCenters(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    :goto_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_3
    if-ge v2, p2, :cond_5

    iget-object v3, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mClusters:[[F

    aget-object v3, v3, v2

    iget-object v4, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mPointProvider:Lcom/android/internal/graphics/palette/PointProvider;

    invoke-interface {v4, v3}, Lcom/android/internal/graphics/palette/PointProvider;->toInt([F)I

    move-result v4

    new-instance v5, Lcom/android/internal/graphics/palette/Palette$Swatch;

    iget-object v6, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mClusterPopulations:[I

    aget v6, v6, v2

    invoke-direct {v5, v4, v6}, Lcom/android/internal/graphics/palette/Palette$Swatch;-><init>(II)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_5
    invoke-static {v1}, Lcom/android/internal/graphics/palette/Palette;->from(Ljava/util/List;)Lcom/android/internal/graphics/palette/Palette;

    move-result-object v2

    iput-object v2, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mPalette:Lcom/android/internal/graphics/palette/Palette;

    return-void
.end method

.method reassignPoints(I)Z
    .locals 12

    goto/32 :goto_b

    nop

    :goto_0
    iget-object v2, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mPoints:[[F

    goto/32 :goto_1f

    nop

    :goto_1
    goto/16 :goto_1c

    :goto_2
    goto/32 :goto_32

    nop

    :goto_3
    const/4 v8, -0x1

    goto/32 :goto_23

    nop

    :goto_4
    const/4 v8, 0x1

    :goto_5
    goto/32 :goto_f

    nop

    :goto_6
    iget-object v10, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mPointProvider:Lcom/android/internal/graphics/palette/PointProvider;

    goto/32 :goto_a

    nop

    :goto_7
    iget-object v3, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mClusterIndices:[I

    goto/32 :goto_12

    nop

    :goto_8
    iget-object v4, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mClusters:[[F

    goto/32 :goto_36

    nop

    :goto_9
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_1

    nop

    :goto_a
    iget-object v11, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mClusters:[[F

    goto/32 :goto_33

    nop

    :goto_b
    const/4 v0, 0x0

    goto/32 :goto_1b

    nop

    :goto_c
    const/high16 v11, 0x40800000    # 4.0f

    goto/32 :goto_35

    nop

    :goto_d
    goto :goto_21

    :goto_e
    goto/32 :goto_6

    nop

    :goto_f
    if-lt v8, p1, :cond_0

    goto/32 :goto_21

    :cond_0
    goto/32 :goto_10

    nop

    :goto_10
    iget-object v9, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mIndexMatrix:[[I

    goto/32 :goto_38

    nop

    :goto_11
    iget-object v10, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mDistanceMatrix:[[F

    goto/32 :goto_1e

    nop

    :goto_12
    aget v3, v3, v1

    goto/32 :goto_8

    nop

    :goto_13
    sub-double/2addr v8, v10

    goto/32 :goto_19

    nop

    :goto_14
    float-to-double v8, v6

    goto/32 :goto_1d

    nop

    :goto_15
    if-lt v1, v3, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_37

    nop

    :goto_16
    if-ltz v11, :cond_2

    goto/32 :goto_29

    :cond_2
    goto/32 :goto_2c

    nop

    :goto_17
    float-to-double v10, v5

    goto/32 :goto_18

    nop

    :goto_18
    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    goto/32 :goto_13

    nop

    :goto_19
    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    goto/32 :goto_2b

    nop

    :goto_1a
    move v6, v5

    goto/32 :goto_34

    nop

    :goto_1b
    const/4 v1, 0x0

    :goto_1c
    goto/32 :goto_0

    nop

    :goto_1d
    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    goto/32 :goto_17

    nop

    :goto_1e
    aget-object v10, v10, v3

    goto/32 :goto_27

    nop

    :goto_1f
    array-length v3, v2

    goto/32 :goto_15

    nop

    :goto_20
    goto/16 :goto_5

    :goto_21
    goto/32 :goto_3

    nop

    :goto_22
    const/4 v0, 0x1

    goto/32 :goto_2f

    nop

    :goto_23
    if-ne v7, v8, :cond_3

    goto/32 :goto_3d

    :cond_3
    goto/32 :goto_14

    nop

    :goto_24
    aget v9, v9, v8

    goto/32 :goto_11

    nop

    :goto_25
    if-gez v10, :cond_4

    goto/32 :goto_e

    :cond_4
    goto/32 :goto_d

    nop

    :goto_26
    if-gtz v9, :cond_5

    goto/32 :goto_3d

    :cond_5
    goto/32 :goto_22

    nop

    :goto_27
    aget v10, v10, v9

    goto/32 :goto_c

    nop

    :goto_28
    move v7, v9

    :goto_29
    goto/32 :goto_2d

    nop

    :goto_2a
    const/high16 v9, 0x40400000    # 3.0f

    goto/32 :goto_2e

    nop

    :goto_2b
    double-to-float v8, v8

    goto/32 :goto_2a

    nop

    :goto_2c
    move v6, v10

    goto/32 :goto_28

    nop

    :goto_2d
    add-int/lit8 v8, v8, 0x1

    goto/32 :goto_20

    nop

    :goto_2e
    cmpl-float v9, v8, v9

    goto/32 :goto_26

    nop

    :goto_2f
    iget-object v9, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mClusterIndices:[I

    goto/32 :goto_3c

    nop

    :goto_30
    cmpl-float v10, v10, v11

    goto/32 :goto_25

    nop

    :goto_31
    cmpg-float v11, v10, v6

    goto/32 :goto_16

    nop

    :goto_32
    return v0

    :goto_33
    aget-object v11, v11, v9

    goto/32 :goto_3b

    nop

    :goto_34
    const/4 v7, -0x1

    goto/32 :goto_4

    nop

    :goto_35
    mul-float/2addr v11, v5

    goto/32 :goto_30

    nop

    :goto_36
    aget-object v4, v4, v3

    goto/32 :goto_39

    nop

    :goto_37
    aget-object v2, v2, v1

    goto/32 :goto_7

    nop

    :goto_38
    aget-object v9, v9, v3

    goto/32 :goto_24

    nop

    :goto_39
    iget-object v5, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mPointProvider:Lcom/android/internal/graphics/palette/PointProvider;

    goto/32 :goto_3a

    nop

    :goto_3a
    invoke-interface {v5, v2, v4}, Lcom/android/internal/graphics/palette/PointProvider;->distance([F[F)F

    move-result v5

    goto/32 :goto_1a

    nop

    :goto_3b
    invoke-interface {v10, v2, v11}, Lcom/android/internal/graphics/palette/PointProvider;->distance([F[F)F

    move-result v10

    goto/32 :goto_31

    nop

    :goto_3c
    aput v7, v9, v1

    :goto_3d
    goto/32 :goto_9

    nop
.end method

.method recalculateClusterCenters(I)V
    .locals 13

    goto/32 :goto_9

    nop

    :goto_0
    new-array v1, p1, [F

    goto/32 :goto_30

    nop

    :goto_1
    mul-float/2addr v7, v12

    goto/32 :goto_24

    nop

    :goto_2
    const/4 v8, 0x1

    goto/32 :goto_19

    nop

    :goto_3
    aput v12, v11, v5

    goto/32 :goto_6

    nop

    :goto_4
    aget v5, v0, v3

    goto/32 :goto_2f

    nop

    :goto_5
    aget v10, v2, v3

    goto/32 :goto_15

    nop

    :goto_6
    aget v11, v0, v5

    goto/32 :goto_16

    nop

    :goto_7
    check-cast v10, Ljava/lang/Integer;

    goto/32 :goto_11

    nop

    :goto_8
    array-length v5, v4

    goto/32 :goto_3a

    nop

    :goto_9
    new-array v0, p1, [I

    goto/32 :goto_32

    nop

    :goto_a
    int-to-float v12, v10

    goto/32 :goto_1

    nop

    :goto_b
    add-float/2addr v7, v8

    goto/32 :goto_c

    nop

    :goto_c
    aput v7, v1, v5

    goto/32 :goto_43

    nop

    :goto_d
    mul-float/2addr v6, v8

    goto/32 :goto_1e

    nop

    :goto_e
    aget v9, v9, v3

    goto/32 :goto_1f

    nop

    :goto_f
    iget-object v11, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mClusterPopulations:[I

    goto/32 :goto_12

    nop

    :goto_10
    aput v11, v0, v5

    goto/32 :goto_38

    nop

    :goto_11
    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    goto/32 :goto_f

    nop

    :goto_12
    aget v12, v11, v5

    goto/32 :goto_42

    nop

    :goto_13
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    goto/32 :goto_45

    nop

    :goto_14
    iget-object v5, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mClusterIndices:[I

    goto/32 :goto_29

    nop

    :goto_15
    iget-object v11, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mClusters:[[F

    goto/32 :goto_3c

    nop

    :goto_16
    aget v7, v4, v7

    goto/32 :goto_a

    nop

    :goto_17
    int-to-float v8, v10

    goto/32 :goto_d

    nop

    :goto_18
    aget v6, v4, v6

    goto/32 :goto_17

    nop

    :goto_19
    if-lt v3, v5, :cond_0

    goto/32 :goto_34

    :cond_0
    goto/32 :goto_14

    nop

    :goto_1a
    iget-object v9, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mPixels:[I

    goto/32 :goto_e

    nop

    :goto_1b
    aput v12, v11, v8

    goto/32 :goto_3f

    nop

    :goto_1c
    goto :goto_22

    :goto_1d
    goto/32 :goto_28

    nop

    :goto_1e
    add-float/2addr v7, v6

    goto/32 :goto_41

    nop

    :goto_1f
    iget-object v10, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mInputPixelToCount:Ljava/util/Map;

    goto/32 :goto_13

    nop

    :goto_20
    div-float v12, v10, v12

    goto/32 :goto_25

    nop

    :goto_21
    const/4 v3, 0x0

    :goto_22
    goto/32 :goto_31

    nop

    :goto_23
    const/4 v7, 0x0

    goto/32 :goto_2

    nop

    :goto_24
    add-float/2addr v11, v7

    goto/32 :goto_10

    nop

    :goto_25
    aput v12, v11, v6

    goto/32 :goto_39

    nop

    :goto_26
    div-float v12, v9, v12

    goto/32 :goto_1b

    nop

    :goto_27
    aput v12, v11, v7

    goto/32 :goto_3b

    nop

    :goto_28
    return-void

    :goto_29
    aget v5, v5, v3

    goto/32 :goto_46

    nop

    :goto_2a
    int-to-float v12, v4

    goto/32 :goto_3e

    nop

    :goto_2b
    mul-float/2addr v8, v11

    goto/32 :goto_b

    nop

    :goto_2c
    new-array v0, p1, [F

    goto/32 :goto_0

    nop

    :goto_2d
    const/4 v3, 0x0

    :goto_2e
    goto/32 :goto_35

    nop

    :goto_2f
    aget v9, v1, v3

    goto/32 :goto_5

    nop

    :goto_30
    new-array v2, p1, [F

    goto/32 :goto_2d

    nop

    :goto_31
    if-lt v3, p1, :cond_1

    goto/32 :goto_1d

    :cond_1
    goto/32 :goto_37

    nop

    :goto_32
    iput-object v0, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mClusterPopulations:[I

    goto/32 :goto_2c

    nop

    :goto_33
    goto :goto_2e

    :goto_34
    goto/32 :goto_21

    nop

    :goto_35
    iget-object v4, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mPoints:[[F

    goto/32 :goto_8

    nop

    :goto_36
    add-int/lit8 v3, v3, 0x1

    goto/32 :goto_33

    nop

    :goto_37
    iget-object v4, p0, Lcom/android/internal/graphics/palette/WSMeansQuantizer;->mClusterPopulations:[I

    goto/32 :goto_44

    nop

    :goto_38
    aget v7, v1, v5

    goto/32 :goto_3d

    nop

    :goto_39
    add-int/lit8 v3, v3, 0x1

    goto/32 :goto_1c

    nop

    :goto_3a
    const/4 v6, 0x2

    goto/32 :goto_23

    nop

    :goto_3b
    int-to-float v12, v4

    goto/32 :goto_26

    nop

    :goto_3c
    aget-object v11, v11, v3

    goto/32 :goto_2a

    nop

    :goto_3d
    aget v8, v4, v8

    goto/32 :goto_40

    nop

    :goto_3e
    div-float v12, v5, v12

    goto/32 :goto_27

    nop

    :goto_3f
    int-to-float v12, v4

    goto/32 :goto_20

    nop

    :goto_40
    int-to-float v11, v10

    goto/32 :goto_2b

    nop

    :goto_41
    aput v7, v2, v5

    goto/32 :goto_36

    nop

    :goto_42
    add-int/2addr v12, v10

    goto/32 :goto_3

    nop

    :goto_43
    aget v7, v2, v5

    goto/32 :goto_18

    nop

    :goto_44
    aget v4, v4, v3

    goto/32 :goto_4

    nop

    :goto_45
    invoke-interface {v10, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    goto/32 :goto_7

    nop

    :goto_46
    aget-object v4, v4, v3

    goto/32 :goto_1a

    nop
.end method
