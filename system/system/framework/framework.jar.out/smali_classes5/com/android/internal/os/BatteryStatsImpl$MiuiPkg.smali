.class Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/os/BatteryStatsImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MiuiPkg"
.end annotation


# instance fields
.field index:I

.field name:Ljava/lang/String;

.field state:I

.field sts:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

.field final synthetic this$0:Lcom/android/internal/os/BatteryStatsImpl;


# direct methods
.method public constructor <init>(Lcom/android/internal/os/BatteryStatsImpl;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->state:I

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->sts:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->index:I

    iput-object p2, p0, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->name:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method StartRun(IJ)V
    .locals 2

    goto/32 :goto_13

    nop

    :goto_0
    invoke-virtual {v0, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(J)V

    goto/32 :goto_e

    nop

    :goto_1
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->getStopWatchTimer(I)Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_2
    iput-object p0, v0, Lcom/android/internal/os/BatteryStatsImpl;->ForegroundPkg:Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;

    goto/32 :goto_14

    nop

    :goto_3
    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->state:I

    goto/32 :goto_c

    nop

    :goto_4
    iput p1, p0, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->index:I

    goto/32 :goto_a

    nop

    :goto_5
    if-gt v0, v1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_d

    nop

    :goto_6
    invoke-virtual {v0, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->stopAllRunningLocked(J)V

    :goto_7
    goto/32 :goto_4

    nop

    :goto_8
    return-void

    :goto_9
    goto/32 :goto_1

    nop

    :goto_a
    if-eq p1, v1, :cond_1

    goto/32 :goto_9

    :cond_1
    goto/32 :goto_8

    nop

    :goto_b
    const/4 v1, -0x1

    goto/32 :goto_5

    nop

    :goto_c
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    goto/32 :goto_2

    nop

    :goto_d
    invoke-virtual {p0, v0}, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->getStopWatchTimer(I)Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_e
    return-void

    :goto_f
    return-void

    :goto_10
    goto/32 :goto_b

    nop

    :goto_11
    if-eq v0, p1, :cond_2

    goto/32 :goto_10

    :cond_2
    goto/32 :goto_f

    nop

    :goto_12
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_3

    nop

    :goto_13
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->state:I

    goto/32 :goto_12

    nop

    :goto_14
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->index:I

    goto/32 :goto_11

    nop
.end method

.method StopRun(J)V
    .locals 3

    goto/32 :goto_7

    nop

    :goto_0
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->sts:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    goto/32 :goto_4

    nop

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_10

    :cond_0
    goto/32 :goto_f

    nop

    :goto_2
    iput v1, p0, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->index:I

    goto/32 :goto_3

    nop

    :goto_3
    return-void

    :goto_4
    aget-object v0, v2, v0

    goto/32 :goto_1

    nop

    :goto_5
    iput-object v1, v0, Lcom/android/internal/os/BatteryStatsImpl;->ForegroundPkg:Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;

    goto/32 :goto_8

    nop

    :goto_6
    const/4 v1, -0x1

    goto/32 :goto_a

    nop

    :goto_7
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->state:I

    goto/32 :goto_b

    nop

    :goto_8
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->index:I

    goto/32 :goto_6

    nop

    :goto_9
    const/4 v1, 0x0

    goto/32 :goto_5

    nop

    :goto_a
    if-eq v0, v1, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_d

    nop

    :goto_b
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_c

    nop

    :goto_c
    iput v0, p0, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->state:I

    goto/32 :goto_11

    nop

    :goto_d
    return-void

    :goto_e
    goto/32 :goto_0

    nop

    :goto_f
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->stopAllRunningLocked(J)V

    :goto_10
    goto/32 :goto_2

    nop

    :goto_11
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    goto/32 :goto_9

    nop
.end method

.method changeStartRun(IJ)V
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    return-void

    :goto_1
    goto/32 :goto_f

    nop

    :goto_2
    return-void

    :goto_3
    goto/32 :goto_4

    nop

    :goto_4
    invoke-virtual {p0, p1}, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->getStopWatchTimer(I)Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    move-result-object v0

    goto/32 :goto_e

    nop

    :goto_5
    iget v0, p0, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->index:I

    goto/32 :goto_b

    nop

    :goto_6
    if-gt v0, v1, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_a

    nop

    :goto_7
    invoke-virtual {v0, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->stopAllRunningLocked(J)V

    :goto_8
    goto/32 :goto_d

    nop

    :goto_9
    if-eq p1, v1, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_2

    nop

    :goto_a
    invoke-virtual {p0, v0}, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->getStopWatchTimer(I)Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_b
    if-eq v0, p1, :cond_2

    goto/32 :goto_1

    :cond_2
    goto/32 :goto_0

    nop

    :goto_c
    return-void

    :goto_d
    iput p1, p0, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->index:I

    goto/32 :goto_9

    nop

    :goto_e
    invoke-virtual {v0, p2, p3}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->startRunningLocked(J)V

    goto/32 :goto_c

    nop

    :goto_f
    const/4 v1, -0x1

    goto/32 :goto_6

    nop
.end method

.method getStopWatchTimer(I)Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;
    .locals 8

    goto/32 :goto_0

    nop

    :goto_0
    if-gez p1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_1a

    nop

    :goto_1
    iget-object v7, v2, Lcom/android/internal/os/BatteryStatsImpl;->mOnBatteryTimeBase:Lcom/android/internal/os/BatteryStatsImpl$TimeBase;

    goto/32 :goto_14

    nop

    :goto_2
    const/16 v5, -0x59

    goto/32 :goto_17

    nop

    :goto_3
    aget-object v0, v0, p1

    goto/32 :goto_6

    nop

    :goto_4
    if-ge p1, v1, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_b

    nop

    :goto_5
    aput-object v1, v0, p1

    goto/32 :goto_12

    nop

    :goto_6
    return-object v0

    :goto_7
    goto/32 :goto_13

    nop

    :goto_8
    return-object v1

    :goto_9
    goto/32 :goto_10

    nop

    :goto_a
    if-nez v1, :cond_2

    goto/32 :goto_9

    :cond_2
    goto/32 :goto_8

    nop

    :goto_b
    goto :goto_7

    :goto_c
    goto/32 :goto_11

    nop

    :goto_d
    const/4 v4, 0x0

    goto/32 :goto_2

    nop

    :goto_e
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    goto/32 :goto_1

    nop

    :goto_f
    invoke-direct/range {v2 .. v7}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;-><init>(Lcom/android/internal/os/Clock;Lcom/android/internal/os/BatteryStatsImpl$Uid;ILjava/util/ArrayList;Lcom/android/internal/os/BatteryStatsImpl$TimeBase;)V

    goto/32 :goto_5

    nop

    :goto_10
    new-instance v1, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    goto/32 :goto_15

    nop

    :goto_11
    aget-object v1, v0, p1

    goto/32 :goto_a

    nop

    :goto_12
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->sts:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    goto/32 :goto_3

    nop

    :goto_13
    const/4 v0, 0x0

    goto/32 :goto_16

    nop

    :goto_14
    move-object v2, v1

    goto/32 :goto_f

    nop

    :goto_15
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    goto/32 :goto_19

    nop

    :goto_16
    return-object v0

    :goto_17
    const/4 v6, 0x0

    goto/32 :goto_e

    nop

    :goto_18
    array-length v1, v0

    goto/32 :goto_4

    nop

    :goto_19
    iget-object v3, v2, Lcom/android/internal/os/BatteryStatsImpl;->mClock:Lcom/android/internal/os/Clock;

    goto/32 :goto_d

    nop

    :goto_1a
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->sts:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    goto/32 :goto_18

    nop
.end method

.method reset(J)V
    .locals 4

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_3

    nop

    :goto_1
    const/4 v2, -0x1

    goto/32 :goto_b

    nop

    :goto_2
    return-void

    :goto_3
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    goto/32 :goto_1f

    nop

    :goto_4
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->sts:[Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;

    goto/32 :goto_12

    nop

    :goto_5
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->this$0:Lcom/android/internal/os/BatteryStatsImpl;

    goto/32 :goto_19

    nop

    :goto_6
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_14

    nop

    :goto_7
    const/4 v1, 0x0

    :goto_8
    goto/32 :goto_d

    nop

    :goto_9
    goto :goto_1c

    :goto_a
    goto/32 :goto_1b

    nop

    :goto_b
    if-gt v1, v2, :cond_0

    goto/32 :goto_10

    :cond_0
    goto/32 :goto_16

    nop

    :goto_c
    const/4 v3, 0x0

    goto/32 :goto_11

    nop

    :goto_d
    const/4 v2, 0x5

    goto/32 :goto_18

    nop

    :goto_e
    if-nez v2, :cond_1

    goto/32 :goto_1c

    :cond_1
    goto/32 :goto_17

    nop

    :goto_f
    goto :goto_1e

    :goto_10
    goto/32 :goto_5

    nop

    :goto_11
    invoke-virtual {v2, v3, p1, p2}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->reset(ZJ)Z

    goto/32 :goto_9

    nop

    :goto_12
    aget-object v2, v2, v1

    goto/32 :goto_e

    nop

    :goto_13
    iget v1, p0, Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;->index:I

    goto/32 :goto_1

    nop

    :goto_14
    goto :goto_8

    :goto_15
    goto/32 :goto_2

    nop

    :goto_16
    const/4 v0, 0x1

    goto/32 :goto_f

    nop

    :goto_17
    if-nez v0, :cond_2

    goto/32 :goto_a

    :cond_2
    goto/32 :goto_c

    nop

    :goto_18
    if-lt v1, v2, :cond_3

    goto/32 :goto_15

    :cond_3
    goto/32 :goto_4

    nop

    :goto_19
    const/4 v2, 0x0

    goto/32 :goto_1d

    nop

    :goto_1a
    if-eq v1, p0, :cond_4

    goto/32 :goto_1e

    :cond_4
    goto/32 :goto_13

    nop

    :goto_1b
    invoke-virtual {v2}, Lcom/android/internal/os/BatteryStatsImpl$StopwatchTimer;->detach()V

    :goto_1c
    goto/32 :goto_6

    nop

    :goto_1d
    iput-object v2, v1, Lcom/android/internal/os/BatteryStatsImpl;->ForegroundPkg:Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;

    :goto_1e
    goto/32 :goto_7

    nop

    :goto_1f
    iget-object v1, v1, Lcom/android/internal/os/BatteryStatsImpl;->ForegroundPkg:Lcom/android/internal/os/BatteryStatsImpl$MiuiPkg;

    goto/32 :goto_1a

    nop
.end method
