.class public final Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable$BlurRegion;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BlurRegion"
.end annotation


# instance fields
.field public final alpha:F

.field public final blurRadius:I

.field public final cornerRadiusBL:F

.field public final cornerRadiusBR:F

.field public final cornerRadiusTL:F

.field public final cornerRadiusTR:F

.field public final rect:Landroid/graphics/Rect;


# direct methods
.method constructor <init>(Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable;->-$$Nest$fgetmAlpha(Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable;)F

    move-result v0

    iput v0, p0, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable$BlurRegion;->alpha:F

    invoke-static {p1}, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable;->-$$Nest$fgetmBlurRadius(Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable;)I

    move-result v0

    iput v0, p0, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable$BlurRegion;->blurRadius:I

    invoke-static {p1}, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable;->-$$Nest$fgetmCornerRadiusTL(Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable;)F

    move-result v0

    iput v0, p0, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable$BlurRegion;->cornerRadiusTL:F

    invoke-static {p1}, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable;->-$$Nest$fgetmCornerRadiusTR(Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable;)F

    move-result v0

    iput v0, p0, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable$BlurRegion;->cornerRadiusTR:F

    invoke-static {p1}, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable;->-$$Nest$fgetmCornerRadiusBL(Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable;)F

    move-result v0

    iput v0, p0, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable$BlurRegion;->cornerRadiusBL:F

    invoke-static {p1}, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable;->-$$Nest$fgetmCornerRadiusBR(Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable;)F

    move-result v0

    iput v0, p0, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable$BlurRegion;->cornerRadiusBR:F

    invoke-static {p1}, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable;->-$$Nest$fgetmRect(Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable;)Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable$BlurRegion;->rect:Landroid/graphics/Rect;

    return-void
.end method


# virtual methods
.method toFloatArray()[F
    .locals 3

    goto/32 :goto_17

    nop

    :goto_0
    aput v1, v0, v2

    goto/32 :goto_28

    nop

    :goto_1
    const/4 v2, 0x4

    goto/32 :goto_0

    nop

    :goto_2
    const/4 v2, 0x1

    goto/32 :goto_20

    nop

    :goto_3
    iget-object v1, p0, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable$BlurRegion;->rect:Landroid/graphics/Rect;

    goto/32 :goto_1a

    nop

    :goto_4
    const/16 v2, 0x9

    goto/32 :goto_a

    nop

    :goto_5
    int-to-float v1, v1

    goto/32 :goto_1

    nop

    :goto_6
    const/4 v2, 0x2

    goto/32 :goto_25

    nop

    :goto_7
    int-to-float v1, v1

    goto/32 :goto_1c

    nop

    :goto_8
    aput v1, v0, v2

    goto/32 :goto_1d

    nop

    :goto_9
    iget-object v1, p0, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable$BlurRegion;->rect:Landroid/graphics/Rect;

    goto/32 :goto_13

    nop

    :goto_a
    aput v1, v0, v2

    goto/32 :goto_23

    nop

    :goto_b
    iget v1, p0, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable$BlurRegion;->blurRadius:I

    goto/32 :goto_7

    nop

    :goto_c
    aput v1, v0, v2

    goto/32 :goto_9

    nop

    :goto_d
    iget v1, p0, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable$BlurRegion;->alpha:F

    goto/32 :goto_2

    nop

    :goto_e
    int-to-float v1, v1

    goto/32 :goto_26

    nop

    :goto_f
    int-to-float v1, v1

    goto/32 :goto_18

    nop

    :goto_10
    const/4 v2, 0x7

    goto/32 :goto_29

    nop

    :goto_11
    iget v1, p0, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable$BlurRegion;->cornerRadiusBR:F

    goto/32 :goto_4

    nop

    :goto_12
    int-to-float v1, v1

    goto/32 :goto_6

    nop

    :goto_13
    iget v1, v1, Landroid/graphics/Rect;->right:I

    goto/32 :goto_5

    nop

    :goto_14
    aput v1, v0, v2

    goto/32 :goto_27

    nop

    :goto_15
    const/16 v2, 0x8

    goto/32 :goto_21

    nop

    :goto_16
    const/4 v2, 0x6

    goto/32 :goto_14

    nop

    :goto_17
    const/16 v0, 0xa

    goto/32 :goto_1b

    nop

    :goto_18
    const/4 v2, 0x3

    goto/32 :goto_c

    nop

    :goto_19
    iget-object v1, p0, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable$BlurRegion;->rect:Landroid/graphics/Rect;

    goto/32 :goto_22

    nop

    :goto_1a
    iget v1, v1, Landroid/graphics/Rect;->left:I

    goto/32 :goto_12

    nop

    :goto_1b
    new-array v0, v0, [F

    goto/32 :goto_b

    nop

    :goto_1c
    const/4 v2, 0x0

    goto/32 :goto_24

    nop

    :goto_1d
    iget v1, p0, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable$BlurRegion;->cornerRadiusTL:F

    goto/32 :goto_16

    nop

    :goto_1e
    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_e

    nop

    :goto_1f
    iget v1, p0, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable$BlurRegion;->cornerRadiusBL:F

    goto/32 :goto_15

    nop

    :goto_20
    aput v1, v0, v2

    goto/32 :goto_3

    nop

    :goto_21
    aput v1, v0, v2

    goto/32 :goto_11

    nop

    :goto_22
    iget v1, v1, Landroid/graphics/Rect;->top:I

    goto/32 :goto_f

    nop

    :goto_23
    return-object v0

    :goto_24
    aput v1, v0, v2

    goto/32 :goto_d

    nop

    :goto_25
    aput v1, v0, v2

    goto/32 :goto_19

    nop

    :goto_26
    const/4 v2, 0x5

    goto/32 :goto_8

    nop

    :goto_27
    iget v1, p0, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable$BlurRegion;->cornerRadiusTR:F

    goto/32 :goto_10

    nop

    :goto_28
    iget-object v1, p0, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable$BlurRegion;->rect:Landroid/graphics/Rect;

    goto/32 :goto_1e

    nop

    :goto_29
    aput v1, v0, v2

    goto/32 :goto_1f

    nop
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BlurRegion{blurRadius="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable$BlurRegion;->blurRadius:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", corners={"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable$BlurRegion;->cornerRadiusTL:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable$BlurRegion;->cornerRadiusTR:F

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable$BlurRegion;->cornerRadiusBL:F

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable$BlurRegion;->cornerRadiusBR:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, alpha="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable$BlurRegion;->alpha:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", rect="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable$BlurRegion;->rect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
