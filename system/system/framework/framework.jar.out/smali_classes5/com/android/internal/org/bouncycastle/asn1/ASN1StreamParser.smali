.class public Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;
.super Ljava/lang/Object;


# instance fields
.field private final _in:Ljava/io/InputStream;

.field private final _limit:I

.field private final tmpBuffers:[[B


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    invoke-static {p1}, Lcom/android/internal/org/bouncycastle/asn1/StreamUtil;->findLimit(Ljava/io/InputStream;)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;-><init>(Ljava/io/InputStream;I)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;->_in:Ljava/io/InputStream;

    iput p2, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;->_limit:I

    const/16 v0, 0xb

    new-array v0, v0, [[B

    iput-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;->tmpBuffers:[[B

    return-void
.end method

.method public constructor <init>([B)V
    .locals 2

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    array-length v1, p1

    invoke-direct {p0, v0, v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;-><init>(Ljava/io/InputStream;I)V

    return-void
.end method

.method private set00Check(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;->_in:Ljava/io/InputStream;

    instance-of v1, v0, Lcom/android/internal/org/bouncycastle/asn1/IndefiniteLengthInputStream;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/android/internal/org/bouncycastle/asn1/IndefiniteLengthInputStream;

    invoke-virtual {v0, p1}, Lcom/android/internal/org/bouncycastle/asn1/IndefiniteLengthInputStream;->setEofOn00(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method readImplicit(ZI)Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_7

    nop

    :goto_0
    return-object v0

    :sswitch_0
    goto/32 :goto_16

    nop

    :goto_1
    invoke-direct {v0, v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Exception;-><init>(Ljava/lang/String;)V

    goto/32 :goto_23

    nop

    :goto_2
    new-instance v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Exception;

    goto/32 :goto_29

    nop

    :goto_3
    instance-of v1, v0, Lcom/android/internal/org/bouncycastle/asn1/IndefiniteLengthInputStream;

    goto/32 :goto_1f

    nop

    :goto_4
    const-string/jumbo v1, "sequences must use constructed encoding (see X.690 8.9.1/8.10.1)"

    goto/32 :goto_8

    nop

    :goto_5
    invoke-direct {v0, v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Exception;-><init>(Ljava/lang/String;)V

    goto/32 :goto_1b

    nop

    :goto_6
    const-string v1, "indefinite-length primitive encoding encountered"

    goto/32 :goto_17

    nop

    :goto_7
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;->_in:Ljava/io/InputStream;

    goto/32 :goto_3

    nop

    :goto_8
    invoke-direct {v0, v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Exception;-><init>(Ljava/lang/String;)V

    goto/32 :goto_28

    nop

    :goto_9
    return-object v0

    :goto_a
    goto/32 :goto_25

    nop

    :goto_b
    new-instance v1, Lcom/android/internal/org/bouncycastle/asn1/DEROctetStringParser;

    goto/32 :goto_d

    nop

    :goto_c
    invoke-direct {v0, p0}, Lcom/android/internal/org/bouncycastle/asn1/DLSequenceParser;-><init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;)V

    goto/32 :goto_0

    nop

    :goto_d
    check-cast v0, Lcom/android/internal/org/bouncycastle/asn1/DefiniteLengthInputStream;

    goto/32 :goto_f

    nop

    :goto_e
    if-nez p1, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_10

    nop

    :goto_f
    invoke-direct {v1, v0}, Lcom/android/internal/org/bouncycastle/asn1/DEROctetStringParser;-><init>(Lcom/android/internal/org/bouncycastle/asn1/DefiniteLengthInputStream;)V

    goto/32 :goto_13

    nop

    :goto_10
    invoke-virtual {p0, p2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;->readIndef(I)Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_11
    throw v0

    :goto_12
    goto/32 :goto_21

    nop

    :goto_13
    return-object v1

    :goto_14
    goto/32 :goto_2

    nop

    :goto_15
    invoke-direct {v0, p0}, Lcom/android/internal/org/bouncycastle/asn1/BEROctetStringParser;-><init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;)V

    goto/32 :goto_1d

    nop

    :goto_16
    new-instance v0, Lcom/android/internal/org/bouncycastle/asn1/BEROctetStringParser;

    goto/32 :goto_15

    nop

    :goto_17
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_11

    nop

    :goto_18
    goto :goto_14

    :sswitch_1
    goto/32 :goto_22

    nop

    :goto_19
    new-instance v0, Lcom/android/internal/org/bouncycastle/asn1/DLSequenceParser;

    goto/32 :goto_c

    nop

    :goto_1a
    return-object v0

    :sswitch_2
    goto/32 :goto_19

    nop

    :goto_1b
    throw v0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x10 -> :sswitch_2
        0x11 -> :sswitch_1
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x4 -> :sswitch_3
        0x10 -> :sswitch_5
        0x11 -> :sswitch_4
    .end sparse-switch

    :goto_1c
    new-instance v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Exception;

    goto/32 :goto_4

    nop

    :goto_1d
    return-object v0

    :goto_1e
    sparse-switch p2, :sswitch_data_1

    goto/32 :goto_26

    nop

    :goto_1f
    if-nez v1, :cond_1

    goto/32 :goto_12

    :cond_1
    goto/32 :goto_e

    nop

    :goto_20
    invoke-direct {v0, p0}, Lcom/android/internal/org/bouncycastle/asn1/DLSetParser;-><init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;)V

    goto/32 :goto_1a

    nop

    :goto_21
    if-nez p1, :cond_2

    goto/32 :goto_1e

    :cond_2
    sparse-switch p2, :sswitch_data_0

    goto/32 :goto_18

    nop

    :goto_22
    new-instance v0, Lcom/android/internal/org/bouncycastle/asn1/DLSetParser;

    goto/32 :goto_20

    nop

    :goto_23
    throw v0

    :sswitch_3
    goto/32 :goto_b

    nop

    :goto_24
    new-instance v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Exception;

    goto/32 :goto_27

    nop

    :goto_25
    new-instance v0, Ljava/io/IOException;

    goto/32 :goto_6

    nop

    :goto_26
    goto/16 :goto_14

    :sswitch_4
    goto/32 :goto_1c

    nop

    :goto_27
    const-string/jumbo v1, "sets must use constructed encoding (see X.690 8.11.1/8.12.1)"

    goto/32 :goto_1

    nop

    :goto_28
    throw v0

    :sswitch_5
    goto/32 :goto_24

    nop

    :goto_29
    const-string v1, "implicit tagging not implemented"

    goto/32 :goto_5

    nop
.end method

.method readIndef(I)Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sparse-switch p1, :sswitch_data_0

    goto/32 :goto_3

    nop

    :goto_0
    return-object v0

    :sswitch_0
    goto/32 :goto_8

    nop

    :goto_1
    invoke-direct {v0, p0}, Lcom/android/internal/org/bouncycastle/asn1/BERSetParser;-><init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;)V

    goto/32 :goto_15

    nop

    :goto_2
    invoke-direct {v0, v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Exception;-><init>(Ljava/lang/String;)V

    goto/32 :goto_4

    nop

    :goto_3
    new-instance v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Exception;

    goto/32 :goto_11

    nop

    :goto_4
    throw v0

    :sswitch_1
    goto/32 :goto_f

    nop

    :goto_5
    new-instance v0, Lcom/android/internal/org/bouncycastle/asn1/DERExternalParser;

    goto/32 :goto_7

    nop

    :goto_6
    return-object v0

    :sswitch_2
    goto/32 :goto_5

    nop

    :goto_7
    invoke-direct {v0, p0}, Lcom/android/internal/org/bouncycastle/asn1/DERExternalParser;-><init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;)V

    goto/32 :goto_0

    nop

    :goto_8
    new-instance v0, Lcom/android/internal/org/bouncycastle/asn1/BEROctetStringParser;

    goto/32 :goto_a

    nop

    :goto_9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_d

    nop

    :goto_a
    invoke-direct {v0, p0}, Lcom/android/internal/org/bouncycastle/asn1/BEROctetStringParser;-><init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;)V

    goto/32 :goto_12

    nop

    :goto_b
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_9

    nop

    :goto_c
    const-string/jumbo v2, "unknown BER object encountered: 0x"

    goto/32 :goto_13

    nop

    :goto_d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_2

    nop

    :goto_e
    new-instance v0, Lcom/android/internal/org/bouncycastle/asn1/BERSequenceParser;

    goto/32 :goto_10

    nop

    :goto_f
    new-instance v0, Lcom/android/internal/org/bouncycastle/asn1/BERSetParser;

    goto/32 :goto_1

    nop

    :goto_10
    invoke-direct {v0, p0}, Lcom/android/internal/org/bouncycastle/asn1/BERSequenceParser;-><init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;)V

    goto/32 :goto_6

    nop

    :goto_11
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_14

    nop

    :goto_12
    return-object v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x8 -> :sswitch_2
        0x10 -> :sswitch_3
        0x11 -> :sswitch_1
    .end sparse-switch

    :goto_13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_b

    nop

    :goto_14
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_c

    nop

    :goto_15
    return-object v0

    :sswitch_3
    goto/32 :goto_e

    nop
.end method

.method public readObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;->_in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v1, 0x0

    return-object v1

    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;->set00Check(Z)V

    iget-object v2, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;->_in:Ljava/io/InputStream;

    invoke-static {v2, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1InputStream;->readTagNumber(Ljava/io/InputStream;I)I

    move-result v2

    and-int/lit8 v3, v0, 0x20

    const/4 v4, 0x1

    if-eqz v3, :cond_1

    move v3, v4

    goto :goto_0

    :cond_1
    move v3, v1

    :goto_0
    iget-object v5, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;->_in:Ljava/io/InputStream;

    iget v6, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;->_limit:I

    const/4 v7, 0x4

    if-eq v2, v7, :cond_2

    const/16 v7, 0x10

    if-eq v2, v7, :cond_2

    const/16 v7, 0x11

    if-eq v2, v7, :cond_2

    const/16 v7, 0x8

    if-ne v2, v7, :cond_3

    :cond_2
    move v1, v4

    :cond_3
    invoke-static {v5, v6, v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1InputStream;->readLength(Ljava/io/InputStream;IZ)I

    move-result v1

    if-gez v1, :cond_7

    if-eqz v3, :cond_6

    new-instance v5, Lcom/android/internal/org/bouncycastle/asn1/IndefiniteLengthInputStream;

    iget-object v6, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;->_in:Ljava/io/InputStream;

    iget v7, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;->_limit:I

    invoke-direct {v5, v6, v7}, Lcom/android/internal/org/bouncycastle/asn1/IndefiniteLengthInputStream;-><init>(Ljava/io/InputStream;I)V

    new-instance v6, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;

    iget v7, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;->_limit:I

    invoke-direct {v6, v5, v7}, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;-><init>(Ljava/io/InputStream;I)V

    and-int/lit8 v7, v0, 0x40

    if-eqz v7, :cond_4

    new-instance v4, Lcom/android/internal/org/bouncycastle/asn1/BERApplicationSpecificParser;

    invoke-direct {v4, v2, v6}, Lcom/android/internal/org/bouncycastle/asn1/BERApplicationSpecificParser;-><init>(ILcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;)V

    return-object v4

    :cond_4
    and-int/lit16 v7, v0, 0x80

    if-eqz v7, :cond_5

    new-instance v7, Lcom/android/internal/org/bouncycastle/asn1/BERTaggedObjectParser;

    invoke-direct {v7, v4, v2, v6}, Lcom/android/internal/org/bouncycastle/asn1/BERTaggedObjectParser;-><init>(ZILcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;)V

    return-object v7

    :cond_5
    invoke-virtual {v6, v2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;->readIndef(I)Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v4

    return-object v4

    :cond_6
    new-instance v4, Ljava/io/IOException;

    const-string v5, "indefinite-length primitive encoding encountered"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_7
    new-instance v4, Lcom/android/internal/org/bouncycastle/asn1/DefiniteLengthInputStream;

    iget-object v5, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;->_in:Ljava/io/InputStream;

    iget v6, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;->_limit:I

    invoke-direct {v4, v5, v1, v6}, Lcom/android/internal/org/bouncycastle/asn1/DefiniteLengthInputStream;-><init>(Ljava/io/InputStream;II)V

    and-int/lit8 v5, v0, 0x40

    if-eqz v5, :cond_8

    new-instance v5, Lcom/android/internal/org/bouncycastle/asn1/DLApplicationSpecific;

    invoke-virtual {v4}, Lcom/android/internal/org/bouncycastle/asn1/DefiniteLengthInputStream;->toByteArray()[B

    move-result-object v6

    invoke-direct {v5, v3, v2, v6}, Lcom/android/internal/org/bouncycastle/asn1/DLApplicationSpecific;-><init>(ZI[B)V

    return-object v5

    :cond_8
    and-int/lit16 v5, v0, 0x80

    if-eqz v5, :cond_9

    new-instance v5, Lcom/android/internal/org/bouncycastle/asn1/BERTaggedObjectParser;

    new-instance v6, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;

    invoke-direct {v6, v4}, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v5, v3, v2, v6}, Lcom/android/internal/org/bouncycastle/asn1/BERTaggedObjectParser;-><init>(ZILcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;)V

    return-object v5

    :cond_9
    if-eqz v3, :cond_a

    sparse-switch v2, :sswitch_data_0

    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "unknown tag "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " encountered"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    :sswitch_0
    new-instance v5, Lcom/android/internal/org/bouncycastle/asn1/DLSetParser;

    new-instance v6, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;

    invoke-direct {v6, v4}, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v5, v6}, Lcom/android/internal/org/bouncycastle/asn1/DLSetParser;-><init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;)V

    return-object v5

    :sswitch_1
    new-instance v5, Lcom/android/internal/org/bouncycastle/asn1/DLSequenceParser;

    new-instance v6, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;

    invoke-direct {v6, v4}, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v5, v6}, Lcom/android/internal/org/bouncycastle/asn1/DLSequenceParser;-><init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;)V

    return-object v5

    :sswitch_2
    new-instance v5, Lcom/android/internal/org/bouncycastle/asn1/DERExternalParser;

    new-instance v6, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;

    invoke-direct {v6, v4}, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v5, v6}, Lcom/android/internal/org/bouncycastle/asn1/DERExternalParser;-><init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;)V

    return-object v5

    :sswitch_3
    new-instance v5, Lcom/android/internal/org/bouncycastle/asn1/BEROctetStringParser;

    new-instance v6, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;

    invoke-direct {v6, v4}, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v5, v6}, Lcom/android/internal/org/bouncycastle/asn1/BEROctetStringParser;-><init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;)V

    return-object v5

    :cond_a
    packed-switch v2, :pswitch_data_0

    :try_start_0
    iget-object v5, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;->tmpBuffers:[[B
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :pswitch_0
    new-instance v5, Lcom/android/internal/org/bouncycastle/asn1/DEROctetStringParser;

    invoke-direct {v5, v4}, Lcom/android/internal/org/bouncycastle/asn1/DEROctetStringParser;-><init>(Lcom/android/internal/org/bouncycastle/asn1/DefiniteLengthInputStream;)V

    return-object v5

    :goto_1
    :try_start_1
    invoke-static {v2, v4, v5}, Lcom/android/internal/org/bouncycastle/asn1/ASN1InputStream;->createPrimitiveDERObject(ILcom/android/internal/org/bouncycastle/asn1/DefiniteLengthInputStream;[[B)Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v5
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    return-object v5

    :catch_0
    move-exception v5

    new-instance v6, Lcom/android/internal/org/bouncycastle/asn1/ASN1Exception;

    const-string v7, "corrupted stream detected"

    invoke-direct {v6, v7, v5}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_3
        0x8 -> :sswitch_2
        0x10 -> :sswitch_1
        0x11 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method readTaggedObject(ZI)Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_1d

    nop

    :goto_0
    return-object v2

    :goto_1
    invoke-direct {v2, v3, p2, v0}, Lcom/android/internal/org/bouncycastle/asn1/DLTaggedObject;-><init>(ZILcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    goto/32 :goto_19

    nop

    :goto_2
    new-instance v2, Lcom/android/internal/org/bouncycastle/asn1/BERTaggedObject;

    goto/32 :goto_a

    nop

    :goto_3
    iget-object v2, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;->_in:Ljava/io/InputStream;

    goto/32 :goto_5

    nop

    :goto_4
    invoke-direct {v2, v3, p2, v0}, Lcom/android/internal/org/bouncycastle/asn1/BERTaggedObject;-><init>(ZILcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    goto/32 :goto_b

    nop

    :goto_5
    instance-of v2, v2, Lcom/android/internal/org/bouncycastle/asn1/IndefiniteLengthInputStream;

    goto/32 :goto_20

    nop

    :goto_6
    return-object v2

    :goto_7
    goto/32 :goto_15

    nop

    :goto_8
    if-eqz p1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_d

    nop

    :goto_9
    if-eq v2, v3, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_f

    nop

    :goto_a
    invoke-static {v1}, Lcom/android/internal/org/bouncycastle/asn1/BERFactory;->createSequence(Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;)Lcom/android/internal/org/bouncycastle/asn1/BERSequence;

    move-result-object v3

    goto/32 :goto_23

    nop

    :goto_b
    goto/16 :goto_24

    :goto_c
    goto/32 :goto_2

    nop

    :goto_d
    iget-object v1, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;->_in:Ljava/io/InputStream;

    goto/32 :goto_18

    nop

    :goto_e
    invoke-virtual {v1}, Lcom/android/internal/org/bouncycastle/asn1/DefiniteLengthInputStream;->toByteArray()[B

    move-result-object v4

    goto/32 :goto_1c

    nop

    :goto_f
    new-instance v2, Lcom/android/internal/org/bouncycastle/asn1/BERTaggedObject;

    goto/32 :goto_28

    nop

    :goto_10
    invoke-direct {v2, v0, p2, v3}, Lcom/android/internal/org/bouncycastle/asn1/DLTaggedObject;-><init>(ZILcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    goto/32 :goto_6

    nop

    :goto_11
    new-instance v3, Lcom/android/internal/org/bouncycastle/asn1/DEROctetString;

    goto/32 :goto_e

    nop

    :goto_12
    if-eq v2, v3, :cond_2

    goto/32 :goto_1a

    :cond_2
    goto/32 :goto_25

    nop

    :goto_13
    if-nez v2, :cond_3

    goto/32 :goto_1f

    :cond_3
    goto/32 :goto_1b

    nop

    :goto_14
    invoke-virtual {v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;->size()I

    move-result v2

    goto/32 :goto_12

    nop

    :goto_15
    invoke-virtual {p0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;->readVector()Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;

    move-result-object v1

    goto/32 :goto_3

    nop

    :goto_16
    new-instance v2, Lcom/android/internal/org/bouncycastle/asn1/DLTaggedObject;

    goto/32 :goto_11

    nop

    :goto_17
    invoke-static {v1}, Lcom/android/internal/org/bouncycastle/asn1/DLFactory;->createSequence(Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;)Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;

    move-result-object v3

    goto/32 :goto_26

    nop

    :goto_18
    check-cast v1, Lcom/android/internal/org/bouncycastle/asn1/DefiniteLengthInputStream;

    goto/32 :goto_16

    nop

    :goto_19
    goto :goto_27

    :goto_1a
    goto/32 :goto_21

    nop

    :goto_1b
    invoke-virtual {v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;->size()I

    move-result v2

    goto/32 :goto_9

    nop

    :goto_1c
    invoke-direct {v3, v4}, Lcom/android/internal/org/bouncycastle/asn1/DEROctetString;-><init>([B)V

    goto/32 :goto_10

    nop

    :goto_1d
    const/4 v0, 0x0

    goto/32 :goto_8

    nop

    :goto_1e
    return-object v2

    :goto_1f
    goto/32 :goto_14

    nop

    :goto_20
    const/4 v3, 0x1

    goto/32 :goto_13

    nop

    :goto_21
    new-instance v2, Lcom/android/internal/org/bouncycastle/asn1/DLTaggedObject;

    goto/32 :goto_17

    nop

    :goto_22
    invoke-virtual {v1, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;->get(I)Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_23
    invoke-direct {v2, v0, p2, v3}, Lcom/android/internal/org/bouncycastle/asn1/BERTaggedObject;-><init>(ZILcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    :goto_24
    goto/32 :goto_1e

    nop

    :goto_25
    new-instance v2, Lcom/android/internal/org/bouncycastle/asn1/DLTaggedObject;

    goto/32 :goto_22

    nop

    :goto_26
    invoke-direct {v2, v0, p2, v3}, Lcom/android/internal/org/bouncycastle/asn1/DLTaggedObject;-><init>(ZILcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    :goto_27
    goto/32 :goto_0

    nop

    :goto_28
    invoke-virtual {v1, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;->get(I)Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v0

    goto/32 :goto_4

    nop
.end method

.method readVector()Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_15

    nop

    :goto_0
    invoke-virtual {v1, v2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    :goto_1
    goto/32 :goto_13

    nop

    :goto_2
    invoke-direct {v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    :goto_3
    goto/32 :goto_f

    nop

    :goto_4
    new-instance v1, Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;

    goto/32 :goto_2

    nop

    :goto_5
    move-object v0, v2

    goto/32 :goto_c

    nop

    :goto_6
    move-object v2, v0

    goto/32 :goto_18

    nop

    :goto_7
    invoke-interface {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v2

    goto/32 :goto_0

    nop

    :goto_8
    invoke-interface {v2}, Lcom/android/internal/org/bouncycastle/asn1/InMemoryRepresentable;->getLoadedObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v2

    goto/32 :goto_b

    nop

    :goto_9
    invoke-direct {v1, v2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;-><init>(I)V

    goto/32 :goto_16

    nop

    :goto_a
    new-instance v1, Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;

    goto/32 :goto_19

    nop

    :goto_b
    invoke-virtual {v1, v2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    goto/32 :goto_d

    nop

    :goto_c
    if-eqz v2, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_11

    nop

    :goto_d
    goto :goto_1

    :goto_e
    goto/32 :goto_7

    nop

    :goto_f
    instance-of v2, v0, Lcom/android/internal/org/bouncycastle/asn1/InMemoryRepresentable;

    goto/32 :goto_10

    nop

    :goto_10
    if-nez v2, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_6

    nop

    :goto_11
    return-object v1

    :goto_12
    goto/32 :goto_14

    nop

    :goto_13
    invoke-virtual {p0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;->readObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v2

    goto/32 :goto_5

    nop

    :goto_14
    goto :goto_3

    :goto_15
    invoke-virtual {p0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1StreamParser;->readObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v0

    goto/32 :goto_1a

    nop

    :goto_16
    return-object v1

    :goto_17
    goto/32 :goto_4

    nop

    :goto_18
    check-cast v2, Lcom/android/internal/org/bouncycastle/asn1/InMemoryRepresentable;

    goto/32 :goto_8

    nop

    :goto_19
    const/4 v2, 0x0

    goto/32 :goto_9

    nop

    :goto_1a
    if-eqz v0, :cond_2

    goto/32 :goto_17

    :cond_2
    goto/32 :goto_a

    nop
.end method
