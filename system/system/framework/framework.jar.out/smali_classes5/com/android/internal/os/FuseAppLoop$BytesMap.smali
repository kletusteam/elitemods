.class Lcom/android/internal/os/FuseAppLoop$BytesMap;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/os/FuseAppLoop;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BytesMap"
.end annotation


# instance fields
.field final mEntries:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/android/internal/os/FuseAppLoop$BytesMapEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/internal/os/FuseAppLoop$BytesMap;->mEntries:Ljava/util/Map;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/os/FuseAppLoop$BytesMap-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/internal/os/FuseAppLoop$BytesMap;-><init>()V

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/os/FuseAppLoop$BytesMap;->mEntries:Ljava/util/Map;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method

.method startUsing(J)[B
    .locals 3

    goto/32 :goto_a

    nop

    :goto_0
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto/32 :goto_f

    nop

    :goto_1
    invoke-direct {v1, v2}, Lcom/android/internal/os/FuseAppLoop$BytesMapEntry;-><init>(Lcom/android/internal/os/FuseAppLoop$BytesMapEntry-IA;)V

    goto/32 :goto_7

    nop

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_9

    nop

    :goto_3
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto/32 :goto_e

    nop

    :goto_4
    check-cast v0, Lcom/android/internal/os/FuseAppLoop$BytesMapEntry;

    goto/32 :goto_11

    nop

    :goto_5
    const/4 v2, 0x0

    goto/32 :goto_1

    nop

    :goto_6
    new-instance v1, Lcom/android/internal/os/FuseAppLoop$BytesMapEntry;

    goto/32 :goto_5

    nop

    :goto_7
    move-object v0, v1

    goto/32 :goto_d

    nop

    :goto_8
    iget v1, v0, Lcom/android/internal/os/FuseAppLoop$BytesMapEntry;->counter:I

    goto/32 :goto_2

    nop

    :goto_9
    iput v1, v0, Lcom/android/internal/os/FuseAppLoop$BytesMapEntry;->counter:I

    goto/32 :goto_c

    nop

    :goto_a
    iget-object v0, p0, Lcom/android/internal/os/FuseAppLoop$BytesMap;->mEntries:Ljava/util/Map;

    goto/32 :goto_3

    nop

    :goto_b
    return-object v1

    :goto_c
    iget-object v1, v0, Lcom/android/internal/os/FuseAppLoop$BytesMapEntry;->bytes:[B

    goto/32 :goto_b

    nop

    :goto_d
    iget-object v1, p0, Lcom/android/internal/os/FuseAppLoop$BytesMap;->mEntries:Ljava/util/Map;

    goto/32 :goto_0

    nop

    :goto_e
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_f
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_10
    goto/32 :goto_8

    nop

    :goto_11
    if-eqz v0, :cond_0

    goto/32 :goto_10

    :cond_0
    goto/32 :goto_6

    nop
.end method

.method stopUsing(J)V
    .locals 3

    goto/32 :goto_8

    nop

    :goto_0
    check-cast v0, Lcom/android/internal/os/FuseAppLoop$BytesMapEntry;

    goto/32 :goto_d

    nop

    :goto_1
    return-void

    :goto_2
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto/32 :goto_3

    nop

    :goto_3
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_4
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto/32 :goto_9

    nop

    :goto_5
    if-lez v1, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_7

    nop

    :goto_6
    add-int/lit8 v1, v1, -0x1

    goto/32 :goto_c

    nop

    :goto_7
    iget-object v1, p0, Lcom/android/internal/os/FuseAppLoop$BytesMap;->mEntries:Ljava/util/Map;

    goto/32 :goto_4

    nop

    :goto_8
    iget-object v0, p0, Lcom/android/internal/os/FuseAppLoop$BytesMap;->mEntries:Ljava/util/Map;

    goto/32 :goto_2

    nop

    :goto_9
    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_a
    goto/32 :goto_1

    nop

    :goto_b
    iget v1, v0, Lcom/android/internal/os/FuseAppLoop$BytesMapEntry;->counter:I

    goto/32 :goto_6

    nop

    :goto_c
    iput v1, v0, Lcom/android/internal/os/FuseAppLoop$BytesMapEntry;->counter:I

    goto/32 :goto_e

    nop

    :goto_d
    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_b

    nop

    :goto_e
    iget v1, v0, Lcom/android/internal/os/FuseAppLoop$BytesMapEntry;->counter:I

    goto/32 :goto_5

    nop
.end method
