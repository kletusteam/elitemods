.class Lcom/android/internal/org/bouncycastle/crypto/util/PublicKeyFactory$DHPublicNumberConverter;
.super Lcom/android/internal/org/bouncycastle/crypto/util/PublicKeyFactory$SubjectPublicKeyInfoConverter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/org/bouncycastle/crypto/util/PublicKeyFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DHPublicNumberConverter"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/internal/org/bouncycastle/crypto/util/PublicKeyFactory$SubjectPublicKeyInfoConverter;-><init>(Lcom/android/internal/org/bouncycastle/crypto/util/PublicKeyFactory$SubjectPublicKeyInfoConverter-IA;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/org/bouncycastle/crypto/util/PublicKeyFactory$DHPublicNumberConverter-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/internal/org/bouncycastle/crypto/util/PublicKeyFactory$DHPublicNumberConverter;-><init>()V

    return-void
.end method


# virtual methods
.method getPublicKeyParameters(Lcom/android/internal/org/bouncycastle/asn1/x509/SubjectPublicKeyInfo;Ljava/lang/Object;)Lcom/android/internal/org/bouncycastle/crypto/params/AsymmetricKeyParameter;
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_1c

    nop

    :goto_0
    move-object v3, v6

    goto/32 :goto_29

    nop

    :goto_1
    move-object v5, v10

    goto/32 :goto_17

    nop

    :goto_2
    invoke-direct {v6, v4, v7}, Lcom/android/internal/org/bouncycastle/crypto/params/DHValidationParameters;-><init>([BI)V

    goto/32 :goto_0

    nop

    :goto_3
    move-object v12, v3

    goto/32 :goto_19

    nop

    :goto_4
    move-object v14, v3

    :goto_5
    goto/32 :goto_11

    nop

    :goto_6
    invoke-virtual {v13}, Lcom/android/internal/org/bouncycastle/asn1/x9/ValidationParams;->getSeed()[B

    move-result-object v4

    goto/32 :goto_d

    nop

    :goto_7
    goto :goto_5

    :goto_8
    goto/32 :goto_4

    nop

    :goto_9
    return-object v15

    :goto_a
    invoke-static {v2}, Lcom/android/internal/org/bouncycastle/asn1/x9/DomainParameters;->getInstance(Ljava/lang/Object;)Lcom/android/internal/org/bouncycastle/asn1/x9/DomainParameters;

    move-result-object v2

    goto/32 :goto_22

    nop

    :goto_b
    new-instance v6, Lcom/android/internal/org/bouncycastle/crypto/params/DHValidationParameters;

    goto/32 :goto_2c

    nop

    :goto_c
    if-nez v4, :cond_0

    goto/32 :goto_1a

    :cond_0
    goto/32 :goto_1e

    nop

    :goto_d
    invoke-virtual {v13}, Lcom/android/internal/org/bouncycastle/asn1/x9/ValidationParams;->getPgenCounter()Ljava/math/BigInteger;

    move-result-object v5

    goto/32 :goto_b

    nop

    :goto_e
    move-object v7, v12

    goto/32 :goto_21

    nop

    :goto_f
    invoke-virtual {v2}, Lcom/android/internal/org/bouncycastle/asn1/x9/DomainParameters;->getG()Ljava/math/BigInteger;

    move-result-object v10

    goto/32 :goto_15

    nop

    :goto_10
    invoke-virtual {v2}, Lcom/android/internal/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getParameters()Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v2

    goto/32 :goto_a

    nop

    :goto_11
    new-instance v15, Lcom/android/internal/org/bouncycastle/crypto/params/DHPublicKeyParameters;

    goto/32 :goto_2b

    nop

    :goto_12
    const/4 v3, 0x0

    goto/32 :goto_20

    nop

    :goto_13
    move-object v4, v9

    goto/32 :goto_1

    nop

    :goto_14
    move-object v0, v8

    goto/32 :goto_16

    nop

    :goto_15
    invoke-virtual {v2}, Lcom/android/internal/org/bouncycastle/asn1/x9/DomainParameters;->getQ()Ljava/math/BigInteger;

    move-result-object v11

    goto/32 :goto_1f

    nop

    :goto_16
    move-object v8, v14

    goto/32 :goto_27

    nop

    :goto_17
    move-object v6, v11

    goto/32 :goto_e

    nop

    :goto_18
    invoke-virtual {v2}, Lcom/android/internal/org/bouncycastle/asn1/x9/DomainParameters;->getJ()Ljava/math/BigInteger;

    move-result-object v4

    goto/32 :goto_c

    nop

    :goto_19
    goto :goto_25

    :goto_1a
    goto/32 :goto_24

    nop

    :goto_1b
    invoke-static {v0}, Lcom/android/internal/org/bouncycastle/asn1/x9/DHPublicKey;->getInstance(Ljava/lang/Object;)Lcom/android/internal/org/bouncycastle/asn1/x9/DHPublicKey;

    move-result-object v0

    goto/32 :goto_26

    nop

    :goto_1c
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/org/bouncycastle/asn1/x509/SubjectPublicKeyInfo;->parsePublicKey()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    goto/32 :goto_1b

    nop

    :goto_1d
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/org/bouncycastle/asn1/x509/SubjectPublicKeyInfo;->getAlgorithm()Lcom/android/internal/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v2

    goto/32 :goto_10

    nop

    :goto_1e
    invoke-virtual {v2}, Lcom/android/internal/org/bouncycastle/asn1/x9/DomainParameters;->getJ()Ljava/math/BigInteger;

    move-result-object v3

    goto/32 :goto_3

    nop

    :goto_1f
    const/4 v3, 0x0

    goto/32 :goto_18

    nop

    :goto_20
    invoke-virtual {v2}, Lcom/android/internal/org/bouncycastle/asn1/x9/DomainParameters;->getValidationParams()Lcom/android/internal/org/bouncycastle/asn1/x9/ValidationParams;

    move-result-object v13

    goto/32 :goto_23

    nop

    :goto_21
    move-object/from16 v16, v0

    goto/32 :goto_14

    nop

    :goto_22
    invoke-virtual {v2}, Lcom/android/internal/org/bouncycastle/asn1/x9/DomainParameters;->getP()Ljava/math/BigInteger;

    move-result-object v9

    goto/32 :goto_f

    nop

    :goto_23
    if-nez v13, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_6

    nop

    :goto_24
    move-object v12, v3

    :goto_25
    goto/32 :goto_12

    nop

    :goto_26
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/x9/DHPublicKey;->getY()Ljava/math/BigInteger;

    move-result-object v1

    goto/32 :goto_1d

    nop

    :goto_27
    invoke-direct/range {v3 .. v8}, Lcom/android/internal/org/bouncycastle/crypto/params/DHParameters;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Lcom/android/internal/org/bouncycastle/crypto/params/DHValidationParameters;)V

    goto/32 :goto_28

    nop

    :goto_28
    invoke-direct {v15, v1, v0}, Lcom/android/internal/org/bouncycastle/crypto/params/DHPublicKeyParameters;-><init>(Ljava/math/BigInteger;Lcom/android/internal/org/bouncycastle/crypto/params/DHParameters;)V

    goto/32 :goto_9

    nop

    :goto_29
    move-object v14, v3

    goto/32 :goto_7

    nop

    :goto_2a
    move-object v3, v8

    goto/32 :goto_13

    nop

    :goto_2b
    new-instance v8, Lcom/android/internal/org/bouncycastle/crypto/params/DHParameters;

    goto/32 :goto_2a

    nop

    :goto_2c
    invoke-virtual {v5}, Ljava/math/BigInteger;->intValue()I

    move-result v7

    goto/32 :goto_2

    nop
.end method
