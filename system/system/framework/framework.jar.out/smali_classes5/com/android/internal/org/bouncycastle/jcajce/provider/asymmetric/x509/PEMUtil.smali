.class Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil$Boundaries;
    }
.end annotation


# instance fields
.field private final _supportedBoundaries:[Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil$Boundaries;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil$Boundaries;

    new-instance v1, Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil$Boundaries;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil$Boundaries;-><init>(Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil;Ljava/lang/String;Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil$Boundaries-IA;)V

    const/4 v3, 0x0

    aput-object v1, v0, v3

    new-instance v1, Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil$Boundaries;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "X509 "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, p0, v3, v2}, Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil$Boundaries;-><init>(Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil;Ljava/lang/String;Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil$Boundaries-IA;)V

    const/4 v3, 0x1

    aput-object v1, v0, v3

    new-instance v1, Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil$Boundaries;

    const-string v3, "PKCS7"

    invoke-direct {v1, p0, v3, v2}, Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil$Boundaries;-><init>(Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil;Ljava/lang/String;Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil$Boundaries-IA;)V

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil;->_supportedBoundaries:[Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil$Boundaries;

    return-void
.end method

.method private getBoundaries(Ljava/lang/String;)Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil$Boundaries;
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil;->_supportedBoundaries:[Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil$Boundaries;

    array-length v2, v1

    if-eq v0, v2, :cond_2

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil$Boundaries;->isTheExpectedHeader(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1, p1}, Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil$Boundaries;->isTheExpectedFooter(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-object v1

    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method

.method private readLine(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v1

    move v2, v1

    const/16 v3, 0xa

    const/16 v4, 0xd

    if-eq v1, v4, :cond_1

    if-eq v2, v3, :cond_1

    if-ltz v2, :cond_1

    int-to-char v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_1
    if-ltz v2, :cond_2

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    if-gez v2, :cond_4

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x0

    return-object v1

    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_4
    if-ne v2, v4, :cond_6

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Ljava/io/InputStream;->mark(I)V

    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v4

    move v2, v4

    if-ne v4, v3, :cond_5

    invoke-virtual {p1, v1}, Ljava/io/InputStream;->mark(I)V

    :cond_5
    if-lez v2, :cond_6

    invoke-virtual {p1}, Ljava/io/InputStream;->reset()V

    :cond_6
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method readPEMObject(Ljava/io/InputStream;)Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {v1, v3}, Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil$Boundaries;->isTheExpectedHeader(Ljava/lang/String;)Z

    move-result v2

    goto/32 :goto_32

    nop

    :goto_1
    new-instance v0, Ljava/lang/StringBuffer;

    goto/32 :goto_33

    nop

    :goto_2
    throw v3

    :goto_3
    goto/32 :goto_1c

    nop

    :goto_4
    if-nez v3, :cond_0

    goto/32 :goto_3b

    :cond_0
    goto/32 :goto_18

    nop

    :goto_5
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_37

    nop

    :goto_6
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/32 :goto_3a

    nop

    :goto_7
    const-string/jumbo v4, "malformed PEM data: no footer found"

    goto/32 :goto_19

    nop

    :goto_8
    const/4 v3, 0x0

    goto/32 :goto_1e

    nop

    :goto_9
    invoke-direct {p0, p1}, Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil;->readLine(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_29

    nop

    :goto_a
    if-nez v2, :cond_1

    goto/32 :goto_1f

    :cond_1
    goto/32 :goto_2b

    nop

    :goto_b
    throw v3

    :goto_c
    goto/32 :goto_6

    nop

    :goto_d
    if-nez v2, :cond_2

    goto/32 :goto_c

    :cond_2
    goto/32 :goto_27

    nop

    :goto_e
    throw v4

    :goto_f
    goto/32 :goto_8

    nop

    :goto_10
    new-instance v2, Ljava/io/IOException;

    goto/32 :goto_2f

    nop

    :goto_11
    return-object v3

    :catch_0
    move-exception v3

    goto/32 :goto_38

    nop

    :goto_12
    invoke-direct {p0, p1}, Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil;->readLine(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_28

    nop

    :goto_13
    new-instance v3, Ljava/io/IOException;

    goto/32 :goto_7

    nop

    :goto_14
    if-nez v3, :cond_3

    goto/32 :goto_21

    :cond_3
    goto/32 :goto_20

    nop

    :goto_15
    const-string/jumbo v3, "malformed PEM data: no header found"

    goto/32 :goto_5

    nop

    :goto_16
    throw v2

    :goto_17
    goto/32 :goto_34

    nop

    :goto_18
    invoke-direct {p0, v4}, Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil;->getBoundaries(Ljava/lang/String;)Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil$Boundaries;

    move-result-object v2

    goto/32 :goto_d

    nop

    :goto_19
    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_2

    nop

    :goto_1a
    if-nez v1, :cond_4

    goto/32 :goto_26

    :cond_4
    goto/32 :goto_0

    nop

    :goto_1b
    if-eqz v1, :cond_5

    goto/32 :goto_17

    :cond_5
    goto/32 :goto_12

    nop

    :goto_1c
    new-instance v2, Ljava/io/IOException;

    goto/32 :goto_15

    nop

    :goto_1d
    new-instance v3, Ljava/io/IOException;

    goto/32 :goto_30

    nop

    :goto_1e
    return-object v3

    :goto_1f
    goto/32 :goto_13

    nop

    :goto_20
    goto/16 :goto_36

    :goto_21
    goto/32 :goto_1d

    nop

    :goto_22
    if-nez v3, :cond_6

    goto/32 :goto_f

    :cond_6
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/org/bouncycastle/util/encoders/Base64;->decode(Ljava/lang/String;)[B

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;->getInstance(Ljava/lang/Object;)Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;

    move-result-object v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_11

    nop

    :goto_23
    if-eqz v2, :cond_7

    goto/32 :goto_3b

    :cond_7
    goto/32 :goto_9

    nop

    :goto_24
    if-nez v2, :cond_8

    goto/32 :goto_17

    :cond_8
    goto/32 :goto_2e

    nop

    :goto_25
    const/4 v1, 0x0

    :goto_26
    goto/32 :goto_1b

    nop

    :goto_27
    invoke-virtual {v1, v4}, Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil$Boundaries;->isTheExpectedFooter(Ljava/lang/String;)Z

    move-result v3

    goto/32 :goto_14

    nop

    :goto_28
    move-object v3, v2

    goto/32 :goto_24

    nop

    :goto_29
    move-object v4, v3

    goto/32 :goto_4

    nop

    :goto_2a
    invoke-direct {v3, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_b

    nop

    :goto_2b
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    goto/32 :goto_22

    nop

    :goto_2c
    goto :goto_26

    :goto_2d
    goto/32 :goto_10

    nop

    :goto_2e
    invoke-direct {p0, v3}, Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil;->getBoundaries(Ljava/lang/String;)Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/x509/PEMUtil$Boundaries;

    move-result-object v1

    goto/32 :goto_1a

    nop

    :goto_2f
    const-string/jumbo v4, "malformed PEM data: found footer where header was expected"

    goto/32 :goto_3c

    nop

    :goto_30
    const-string/jumbo v5, "malformed PEM data: header/footer mismatch"

    goto/32 :goto_2a

    nop

    :goto_31
    const-string/jumbo v5, "malformed PEM data encountered"

    goto/32 :goto_39

    nop

    :goto_32
    if-nez v2, :cond_9

    goto/32 :goto_2d

    :cond_9
    goto/32 :goto_2c

    nop

    :goto_33
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    goto/32 :goto_25

    nop

    :goto_34
    if-nez v1, :cond_a

    goto/32 :goto_3

    :cond_a
    goto/32 :goto_35

    nop

    :goto_35
    const/4 v2, 0x0

    :goto_36
    goto/32 :goto_23

    nop

    :goto_37
    throw v2

    :goto_38
    new-instance v4, Ljava/io/IOException;

    goto/32 :goto_31

    nop

    :goto_39
    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_e

    nop

    :goto_3a
    goto :goto_36

    :goto_3b
    goto/32 :goto_a

    nop

    :goto_3c
    invoke-direct {v2, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_16

    nop
.end method
