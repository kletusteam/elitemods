.class Lcom/android/internal/os/ZygoteServer;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/os/ZygoteServer$UsapPoolRefillAction;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z = false

.field private static final INVALID_TIMESTAMP:I = -0x1

.field public static final TAG:Ljava/lang/String; = "ZygoteServer"

.field private static final USAP_POOL_REFILL_DELAY_MS_DEFAULT:Ljava/lang/String; = "3000"

.field private static final USAP_POOL_SIZE_MAX_DEFAULT:Ljava/lang/String; = "10"

.field private static final USAP_POOL_SIZE_MAX_LIMIT:I = 0x64

.field private static final USAP_POOL_SIZE_MIN_DEFAULT:Ljava/lang/String; = "1"

.field private static final USAP_POOL_SIZE_MIN_LIMIT:I = 0x1


# instance fields
.field private mCloseSocketFd:Z

.field private mIsFirstPropertyCheck:Z

.field private mIsForkChild:Z

.field private mLastPropCheckTimestamp:J

.field private mUsapPoolEnabled:Z

.field private final mUsapPoolEventFD:Ljava/io/FileDescriptor;

.field private mUsapPoolRefillAction:Lcom/android/internal/os/ZygoteServer$UsapPoolRefillAction;

.field private mUsapPoolRefillDelayMs:I

.field private mUsapPoolRefillThreshold:I

.field private mUsapPoolRefillTriggerTimestamp:J

.field private mUsapPoolSizeMax:I

.field private mUsapPoolSizeMin:I

.field private final mUsapPoolSocket:Landroid/net/LocalServerSocket;

.field private final mUsapPoolSupported:Z

.field private mZygoteSocket:Landroid/net/LocalServerSocket;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolEnabled:Z

    iput v0, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolSizeMax:I

    iput v0, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolSizeMin:I

    iput v0, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolRefillThreshold:I

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolRefillDelayMs:I

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/internal/os/ZygoteServer;->mIsFirstPropertyCheck:Z

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/android/internal/os/ZygoteServer;->mLastPropCheckTimestamp:J

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolEventFD:Ljava/io/FileDescriptor;

    iput-object v1, p0, Lcom/android/internal/os/ZygoteServer;->mZygoteSocket:Landroid/net/LocalServerSocket;

    iput-object v1, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolSocket:Landroid/net/LocalServerSocket;

    iput-boolean v0, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolSupported:Z

    return-void
.end method

.method constructor <init>(Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolEnabled:Z

    iput v0, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolSizeMax:I

    iput v0, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolSizeMin:I

    iput v0, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolRefillThreshold:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolRefillDelayMs:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/internal/os/ZygoteServer;->mIsFirstPropertyCheck:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/internal/os/ZygoteServer;->mLastPropCheckTimestamp:J

    invoke-static {}, Lcom/android/internal/os/Zygote;->getUsapPoolEventFD()Ljava/io/FileDescriptor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolEventFD:Ljava/io/FileDescriptor;

    if-eqz p1, :cond_0

    const-string/jumbo v0, "zygote"

    invoke-static {v0}, Lcom/android/internal/os/Zygote;->createManagedSocketFromInitSocket(Ljava/lang/String;)Landroid/net/LocalServerSocket;

    move-result-object v0

    iput-object v0, p0, Lcom/android/internal/os/ZygoteServer;->mZygoteSocket:Landroid/net/LocalServerSocket;

    nop

    const-string/jumbo v0, "usap_pool_primary"

    invoke-static {v0}, Lcom/android/internal/os/Zygote;->createManagedSocketFromInitSocket(Ljava/lang/String;)Landroid/net/LocalServerSocket;

    move-result-object v0

    iput-object v0, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolSocket:Landroid/net/LocalServerSocket;

    goto :goto_0

    :cond_0
    const-string/jumbo v0, "zygote_secondary"

    invoke-static {v0}, Lcom/android/internal/os/Zygote;->createManagedSocketFromInitSocket(Ljava/lang/String;)Landroid/net/LocalServerSocket;

    move-result-object v0

    iput-object v0, p0, Lcom/android/internal/os/ZygoteServer;->mZygoteSocket:Landroid/net/LocalServerSocket;

    nop

    const-string/jumbo v0, "usap_pool_secondary"

    invoke-static {v0}, Lcom/android/internal/os/Zygote;->createManagedSocketFromInitSocket(Ljava/lang/String;)Landroid/net/LocalServerSocket;

    move-result-object v0

    iput-object v0, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolSocket:Landroid/net/LocalServerSocket;

    :goto_0
    invoke-static {}, Lcom/android/internal/os/ZygoteInitStub;->getInstance()Lcom/android/internal/os/ZygoteInitStub;

    move-result-object v0

    invoke-static {}, Landroid/os/Process;->is64Bit()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/internal/os/ZygoteInitStub;->checkUsapAllowed(Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolSupported:Z

    invoke-direct {p0}, Lcom/android/internal/os/ZygoteServer;->fetchUsapPoolPolicyProps()V

    return-void
.end method

.method private acceptCommandPeer(Ljava/lang/String;)Lcom/android/internal/os/ZygoteConnection;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/android/internal/os/ZygoteServer;->mZygoteSocket:Landroid/net/LocalServerSocket;

    invoke-virtual {v0}, Landroid/net/LocalServerSocket;->accept()Landroid/net/LocalSocket;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/android/internal/os/ZygoteServer;->createNewConnection(Landroid/net/LocalSocket;Ljava/lang/String;)Lcom/android/internal/os/ZygoteConnection;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "IOException during accept()"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private fetchUsapPoolPolicyProps()V
    .locals 8

    iget-boolean v0, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolSupported:Z

    if-eqz v0, :cond_4

    const-string/jumbo v0, "usap_pool_size_max"

    const-string v1, "10"

    invoke-static {v0, v1}, Lcom/android/internal/os/Zygote;->getConfigurationProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    const/16 v3, 0x64

    invoke-static {v2, v3}, Ljava/lang/Integer;->min(II)I

    move-result v2

    iput v2, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolSizeMax:I

    :cond_0
    const-string/jumbo v2, "usap_pool_size_min"

    const-string v3, "1"

    invoke-static {v2, v3}, Lcom/android/internal/os/Zygote;->getConfigurationProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    nop

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x1

    invoke-static {v4, v5}, Ljava/lang/Integer;->max(II)I

    move-result v4

    iput v4, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolSizeMin:I

    :cond_1
    iget v4, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolSizeMax:I

    div-int/lit8 v4, v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "usap_refill_threshold"

    invoke-static {v5, v4}, Lcom/android/internal/os/Zygote;->getConfigurationProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    nop

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    iget v6, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolSizeMax:I

    invoke-static {v5, v6}, Ljava/lang/Integer;->min(II)I

    move-result v5

    iput v5, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolRefillThreshold:I

    :cond_2
    const-string/jumbo v5, "usap_pool_refill_delay_ms"

    const-string v6, "3000"

    invoke-static {v5, v6}, Lcom/android/internal/os/Zygote;->getConfigurationProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolRefillDelayMs:I

    :cond_3
    iget v6, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolSizeMin:I

    iget v7, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolSizeMax:I

    if-lt v6, v7, :cond_4

    const-string v6, "ZygoteServer"

    const-string v7, "The max size of the USAP pool must be greater than the minimum size.  Restoring default values."

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolSizeMax:I

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolSizeMin:I

    iget v1, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolSizeMax:I

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolRefillThreshold:I

    :cond_4
    return-void
.end method

.method private fetchUsapPoolPolicyPropsIfUnfetched()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/os/ZygoteServer;->mIsFirstPropertyCheck:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/internal/os/ZygoteServer;->mIsFirstPropertyCheck:Z

    invoke-direct {p0}, Lcom/android/internal/os/ZygoteServer;->fetchUsapPoolPolicyProps()V

    :cond_0
    return-void
.end method

.method private fetchUsapPoolPolicyPropsWithMinInterval()V
    .locals 6

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-boolean v2, p0, Lcom/android/internal/os/ZygoteServer;->mIsFirstPropertyCheck:Z

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/android/internal/os/ZygoteServer;->mLastPropCheckTimestamp:J

    sub-long v2, v0, v2

    const-wide/32 v4, 0xea60

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/internal/os/ZygoteServer;->mIsFirstPropertyCheck:Z

    iput-wide v0, p0, Lcom/android/internal/os/ZygoteServer;->mLastPropCheckTimestamp:J

    invoke-direct {p0}, Lcom/android/internal/os/ZygoteServer;->fetchUsapPoolPolicyProps()V

    :cond_1
    return-void
.end method

.method private resetUsapRefillState()V
    .locals 2

    sget-object v0, Lcom/android/internal/os/ZygoteServer$UsapPoolRefillAction;->NONE:Lcom/android/internal/os/ZygoteServer$UsapPoolRefillAction;

    iput-object v0, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolRefillAction:Lcom/android/internal/os/ZygoteServer$UsapPoolRefillAction;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolRefillTriggerTimestamp:J

    return-void
.end method


# virtual methods
.method closeServerSocket()V
    .locals 3

    goto/32 :goto_7

    nop

    :goto_0
    invoke-static {v0, v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/32 :goto_6

    nop

    :goto_1
    const-string v2, "Zygote:  error closing sockets"

    goto/32 :goto_3

    nop

    :goto_2
    const/4 v0, 0x0

    goto/32 :goto_a

    nop

    :goto_3
    invoke-static {v0, v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_4
    nop

    :goto_5
    goto/32 :goto_2

    nop

    :goto_6
    goto :goto_5

    :catch_0
    move-exception v1

    goto/32 :goto_1

    nop

    :goto_7
    const-string v0, "ZygoteServer"

    :try_start_0
    iget-object v1, p0, Lcom/android/internal/os/ZygoteServer;->mZygoteSocket:Landroid/net/LocalServerSocket;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/LocalServerSocket;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    iget-object v2, p0, Lcom/android/internal/os/ZygoteServer;->mZygoteSocket:Landroid/net/LocalServerSocket;

    invoke-virtual {v2}, Landroid/net/LocalServerSocket;->close()V

    if-eqz v1, :cond_0

    iget-boolean v2, p0, Lcom/android/internal/os/ZygoteServer;->mCloseSocketFd:Z

    if-eqz v2, :cond_0

    invoke-static {v1}, Landroid/system/Os;->close(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/system/ErrnoException; {:try_start_0 .. :try_end_0} :catch_1

    goto/32 :goto_b

    nop

    :goto_8
    const-string v2, "Zygote:  error closing descriptor"

    goto/32 :goto_0

    nop

    :goto_9
    return-void

    :goto_a
    iput-object v0, p0, Lcom/android/internal/os/ZygoteServer;->mZygoteSocket:Landroid/net/LocalServerSocket;

    goto/32 :goto_9

    nop

    :goto_b
    goto :goto_4

    :catch_1
    move-exception v1

    goto/32 :goto_8

    nop
.end method

.method protected createNewConnection(Landroid/net/LocalSocket;Ljava/lang/String;)Lcom/android/internal/os/ZygoteConnection;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/android/internal/os/ZygoteConnection;

    invoke-direct {v0, p1, p2}, Lcom/android/internal/os/ZygoteConnection;-><init>(Landroid/net/LocalSocket;Ljava/lang/String;)V

    return-object v0
.end method

.method fillUsapPool([IZ)Ljava/lang/Runnable;
    .locals 7

    goto/32 :goto_19

    nop

    :goto_0
    invoke-static {v0, v1, v2}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    goto/32 :goto_d

    nop

    :goto_1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_24

    nop

    :goto_2
    return-object v3

    :goto_3
    goto/32 :goto_7

    nop

    :goto_4
    const-string v6, "Priority USAP Pool refill. New USAPs: "

    goto/32 :goto_1a

    nop

    :goto_5
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/32 :goto_1

    nop

    :goto_6
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_c

    nop

    :goto_7
    goto :goto_f

    :goto_8
    goto/32 :goto_26

    nop

    :goto_9
    invoke-static {v3, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_21

    nop

    :goto_a
    if-nez p2, :cond_0

    goto/32 :goto_22

    :cond_0
    goto/32 :goto_15

    nop

    :goto_b
    sub-int/2addr v4, v2

    goto/32 :goto_1c

    nop

    :goto_c
    const-string v6, "Delayed USAP Pool refill. New USAPs: "

    goto/32 :goto_1d

    nop

    :goto_d
    invoke-direct {p0}, Lcom/android/internal/os/ZygoteServer;->fetchUsapPoolPolicyPropsIfUnfetched()V

    goto/32 :goto_27

    nop

    :goto_e
    invoke-static {}, Ldalvik/system/ZygoteHooks;->preFork()V

    :goto_f
    goto/32 :goto_2b

    nop

    :goto_10
    sub-int/2addr v4, v2

    goto/32 :goto_16

    nop

    :goto_11
    if-nez v3, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_2

    nop

    :goto_12
    invoke-static {v3, p1, p2}, Lcom/android/internal/os/Zygote;->forkUsap(Landroid/net/LocalServerSocket;[IZ)Ljava/lang/Runnable;

    move-result-object v3

    goto/32 :goto_11

    nop

    :goto_13
    return-object v0

    :goto_14
    iget v4, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolSizeMax:I

    goto/32 :goto_10

    nop

    :goto_15
    iget v4, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolSizeMin:I

    goto/32 :goto_b

    nop

    :goto_16
    new-instance v5, Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_17
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_9

    nop

    :goto_18
    invoke-static {v0, v1}, Landroid/os/Trace;->traceEnd(J)V

    goto/32 :goto_23

    nop

    :goto_19
    const-wide/16 v0, 0x40

    goto/32 :goto_1e

    nop

    :goto_1a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/32 :goto_1b

    nop

    :goto_1b
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/32 :goto_17

    nop

    :goto_1c
    new-instance v5, Ljava/lang/StringBuilder;

    goto/32 :goto_28

    nop

    :goto_1d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/32 :goto_5

    nop

    :goto_1e
    const-string v2, "Zygote:FillUsapPool"

    goto/32 :goto_0

    nop

    :goto_1f
    iget-object v3, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolSocket:Landroid/net/LocalServerSocket;

    goto/32 :goto_12

    nop

    :goto_20
    const-string/jumbo v3, "zygote"

    goto/32 :goto_a

    nop

    :goto_21
    goto :goto_25

    :goto_22
    goto/32 :goto_14

    nop

    :goto_23
    const/4 v0, 0x0

    goto/32 :goto_13

    nop

    :goto_24
    invoke-static {v3, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_25
    goto/32 :goto_e

    nop

    :goto_26
    invoke-static {}, Ldalvik/system/ZygoteHooks;->postForkCommon()V

    goto/32 :goto_2a

    nop

    :goto_27
    invoke-static {}, Lcom/android/internal/os/Zygote;->getUsapPoolCount()I

    move-result v2

    goto/32 :goto_20

    nop

    :goto_28
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_4

    nop

    :goto_29
    if-gez v4, :cond_2

    goto/32 :goto_8

    :cond_2
    goto/32 :goto_1f

    nop

    :goto_2a
    invoke-direct {p0}, Lcom/android/internal/os/ZygoteServer;->resetUsapRefillState()V

    goto/32 :goto_18

    nop

    :goto_2b
    add-int/lit8 v4, v4, -0x1

    goto/32 :goto_29

    nop
.end method

.method getZygoteSocketFileDescriptor()Ljava/io/FileDescriptor;
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return-object v0

    :goto_1
    invoke-virtual {v0}, Landroid/net/LocalServerSocket;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/internal/os/ZygoteServer;->mZygoteSocket:Landroid/net/LocalServerSocket;

    goto/32 :goto_1

    nop
.end method

.method public isUsapPoolEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolEnabled:Z

    return v0
.end method

.method registerServerSocketAtAbstractName(Ljava/lang/String;)V
    .locals 4

    goto/32 :goto_c

    nop

    :goto_0
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_4

    nop

    :goto_1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_3

    nop

    :goto_2
    const-string v3, "\'"

    goto/32 :goto_1

    nop

    :goto_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_6

    nop

    :goto_4
    const-string v3, "Error binding to abstract socket \'"

    goto/32 :goto_8

    nop

    :goto_5
    return-void

    :goto_6
    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/32 :goto_9

    nop

    :goto_7
    if-eqz v0, :cond_0

    goto/32 :goto_a

    :cond_0
    :try_start_0
    new-instance v0, Landroid/net/LocalServerSocket;

    invoke-direct {v0, p1}, Landroid/net/LocalServerSocket;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/internal/os/ZygoteServer;->mZygoteSocket:Landroid/net/LocalServerSocket;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/internal/os/ZygoteServer;->mCloseSocketFd:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_d

    nop

    :goto_8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_f

    nop

    :goto_9
    throw v1

    :goto_a
    goto/32 :goto_5

    nop

    :goto_b
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_0

    nop

    :goto_c
    iget-object v0, p0, Lcom/android/internal/os/ZygoteServer;->mZygoteSocket:Landroid/net/LocalServerSocket;

    goto/32 :goto_7

    nop

    :goto_d
    goto :goto_a

    :catch_0
    move-exception v0

    goto/32 :goto_e

    nop

    :goto_e
    new-instance v1, Ljava/lang/RuntimeException;

    goto/32 :goto_b

    nop

    :goto_f
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_2

    nop
.end method

.method runSelectLoop(Ljava/lang/String;)Ljava/lang/Runnable;
    .locals 18

    goto/32 :goto_4e

    nop

    :goto_0
    move v12, v11

    :goto_1
    goto/32 :goto_4d

    nop

    :goto_2
    iput-object v0, v1, Lcom/android/internal/os/ZygoteServer;->mUsapPoolRefillAction:Lcom/android/internal/os/ZygoteServer$UsapPoolRefillAction;

    goto/32 :goto_55

    nop

    :goto_3
    aget-object v0, v8, v13

    goto/32 :goto_da

    nop

    :goto_4
    throw v4

    :goto_5
    new-array v6, v6, [Landroid/system/StructPollfd;

    goto/32 :goto_8a

    nop

    :goto_6
    iput-boolean v11, v1, Lcom/android/internal/os/ZygoteServer;->mIsForkChild:Z

    goto/32 :goto_17

    nop

    :goto_7
    new-instance v14, Ljava/io/FileDescriptor;

    goto/32 :goto_57

    nop

    :goto_8
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_4c

    nop

    :goto_9
    sub-long/2addr v14, v12

    goto/32 :goto_eb

    nop

    :goto_a
    aget-object v10, v8, v0

    goto/32 :goto_c7

    nop

    :goto_b
    long-to-int v0, v14

    goto/32 :goto_1d

    nop

    :goto_c
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_104

    nop

    :goto_d
    new-instance v10, Landroid/system/StructPollfd;

    goto/32 :goto_2d

    nop

    :goto_e
    aget v13, v6, v12

    goto/32 :goto_7

    nop

    :goto_f
    invoke-static {v14, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_10
    nop

    :goto_11
    goto/32 :goto_ed

    nop

    :goto_12
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    goto/32 :goto_13

    nop

    :goto_13
    iput-wide v13, v1, Lcom/android/internal/os/ZygoteServer;->mUsapPoolRefillTriggerTimestamp:J

    :goto_14
    goto/32 :goto_b4

    nop

    :goto_15
    const/4 v0, 0x0

    goto/32 :goto_5d

    nop

    :goto_16
    move-object v2, v0

    goto/32 :goto_95

    nop

    :goto_17
    throw v0

    :goto_18
    goto/32 :goto_cb

    nop

    :goto_19
    move-object v6, v0

    goto/32 :goto_d0

    nop

    :goto_1a
    invoke-static {}, Lcom/android/internal/os/Zygote;->getUsapPoolCount()I

    move-result v0

    goto/32 :goto_fe

    nop

    :goto_1b
    if-nez v5, :cond_0

    goto/32 :goto_70

    :cond_0
    goto/32 :goto_6f

    nop

    :goto_1c
    sub-long/2addr v12, v14

    goto/32 :goto_bc

    nop

    :goto_1d
    invoke-static {v0}, Lcom/android/internal/os/Zygote;->removeUsapTableEntry(I)Z

    :goto_1e
    goto/32 :goto_bb

    nop

    :goto_1f
    aget-object v10, v8, v0

    goto/32 :goto_aa

    nop

    :goto_20
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_f2

    nop

    :goto_21
    sget-object v4, Lcom/android/internal/os/ZygoteServer$UsapPoolRefillAction;->NONE:Lcom/android/internal/os/ZygoteServer$UsapPoolRefillAction;

    goto/32 :goto_d7

    nop

    :goto_22
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    goto/32 :goto_c2

    nop

    :goto_23
    sub-int/2addr v4, v0

    goto/32 :goto_2e

    nop

    :goto_24
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    goto/32 :goto_56

    nop

    :goto_25
    iput-wide v4, v1, Lcom/android/internal/os/ZygoteServer;->mUsapPoolRefillTriggerTimestamp:J

    :goto_26
    goto/32 :goto_68

    nop

    :goto_27
    const-string v5, "Failed to read from USAP reporting pipe: "

    goto/32 :goto_e4

    nop

    :goto_28
    move-object v4, v0

    goto/32 :goto_65

    nop

    :goto_29
    invoke-virtual {v0}, Lcom/android/internal/os/ZygoteConnection;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v14

    goto/32 :goto_ae

    nop

    :goto_2a
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_8f

    nop

    :goto_2b
    if-gez v13, :cond_1

    goto/32 :goto_7c

    :cond_1
    goto/32 :goto_3

    nop

    :goto_2c
    const/4 v7, 0x1

    goto/32 :goto_b8

    nop

    :goto_2d
    invoke-direct {v10}, Landroid/system/StructPollfd;-><init>()V

    goto/32 :goto_a0

    nop

    :goto_2e
    iget v5, v1, Lcom/android/internal/os/ZygoteServer;->mUsapPoolRefillThreshold:I

    goto/32 :goto_de

    nop

    :goto_2f
    if-eqz v0, :cond_2

    goto/32 :goto_94

    :cond_2
    goto/32 :goto_93

    nop

    :goto_30
    invoke-virtual {v2, v4, v0}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v0

    goto/32 :goto_103

    nop

    :goto_31
    const/4 v0, 0x0

    goto/32 :goto_be

    nop

    :goto_32
    array-length v8, v0

    goto/32 :goto_105

    nop

    :goto_33
    goto/16 :goto_11

    :goto_34
    goto/32 :goto_92

    nop

    :goto_35
    const-wide/16 v14, 0x0

    goto/32 :goto_d6

    nop

    :goto_36
    invoke-direct {v4, v5, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/32 :goto_4

    nop

    :goto_37
    if-nez v4, :cond_3

    goto/32 :goto_14

    :cond_3
    goto/32 :goto_12

    nop

    :goto_38
    move v7, v0

    :goto_39
    goto/32 :goto_ee

    nop

    :goto_3a
    add-int/lit8 v0, v0, 0x1

    nop

    goto/32 :goto_61

    nop

    :goto_3b
    aput-object v15, v8, v0

    goto/32 :goto_c3

    nop

    :goto_3c
    goto/16 :goto_26

    :goto_3d
    goto/32 :goto_f6

    nop

    :goto_3e
    iput-wide v4, v1, Lcom/android/internal/os/ZygoteServer;->mUsapPoolRefillTriggerTimestamp:J

    goto/32 :goto_89

    nop

    :goto_3f
    invoke-virtual {v1, v0, v4}, Lcom/android/internal/os/ZygoteServer;->fillUsapPool([IZ)Ljava/lang/Runnable;

    move-result-object v5

    goto/32 :goto_1b

    nop

    :goto_40
    new-instance v15, Landroid/system/StructPollfd;

    goto/32 :goto_f0

    nop

    :goto_41
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    goto/32 :goto_25

    nop

    :goto_42
    return-object v16

    :goto_43
    :try_start_0
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "command == null"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    :goto_44
    if-nez v16, :cond_9

    invoke-virtual {v0}, Lcom/android/internal/os/ZygoteConnection;->isClosedByPeer()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v0}, Lcom/android/internal/os/ZygoteConnection;->closeSocket()V

    invoke-virtual {v3, v13}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_4
    :goto_45
    goto/32 :goto_e6

    nop

    :goto_46
    add-int/lit8 v13, v0, -0x1

    goto/32 :goto_2b

    nop

    :goto_47
    invoke-virtual {v14, v13}, Ljava/io/FileDescriptor;->setInt$(I)V

    goto/32 :goto_40

    nop

    :goto_48
    move v12, v0

    nop

    goto/32 :goto_f3

    nop

    :goto_49
    sget-object v13, Lcom/android/internal/os/ZygoteServer$UsapPoolRefillAction;->IMMEDIATE:Lcom/android/internal/os/ZygoteServer$UsapPoolRefillAction;

    goto/32 :goto_cd

    nop

    :goto_4a
    move/from16 v0, v17

    :goto_4b
    goto/32 :goto_46

    nop

    :goto_4c
    const-string v5, "Failed to read from USAP pool event FD: "

    goto/32 :goto_102

    nop

    :goto_4d
    if-lt v12, v10, :cond_5

    goto/32 :goto_b9

    :cond_5
    goto/32 :goto_e

    nop

    :goto_4e
    move-object/from16 v1, p0

    goto/32 :goto_2a

    nop

    :goto_4f
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/os/ZygoteServer;->fetchUsapPoolPolicyPropsWithMinInterval()V

    goto/32 :goto_5f

    nop

    :goto_50
    if-nez v6, :cond_6

    goto/32 :goto_d1

    :cond_6
    goto/32 :goto_9d

    nop

    :goto_51
    check-cast v10, Ljava/io/FileDescriptor;

    goto/32 :goto_5b

    nop

    :goto_52
    iput-short v12, v11, Landroid/system/StructPollfd;->events:S

    nop

    goto/32 :goto_a5

    nop

    :goto_53
    move-object v6, v0

    :goto_54
    goto/32 :goto_74

    nop

    :goto_55
    const/4 v0, 0x0

    goto/32 :goto_e8

    nop

    :goto_56
    add-int/2addr v6, v7

    goto/32 :goto_32

    nop

    :goto_57
    invoke-direct {v14}, Ljava/io/FileDescriptor;-><init>()V

    goto/32 :goto_47

    nop

    :goto_58
    goto/16 :goto_9c

    :goto_59
    goto/32 :goto_ab

    nop

    :goto_5a
    iget-object v0, v1, Lcom/android/internal/os/ZygoteServer;->mZygoteSocket:Landroid/net/LocalServerSocket;

    goto/32 :goto_a9

    nop

    :goto_5b
    new-instance v11, Landroid/system/StructPollfd;

    goto/32 :goto_dc

    nop

    :goto_5c
    invoke-static {v14, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_7d

    nop

    :goto_5d
    iput-wide v4, v1, Lcom/android/internal/os/ZygoteServer;->mUsapPoolRefillTriggerTimestamp:J

    goto/32 :goto_f8

    nop

    :goto_5e
    move-object v8, v6

    goto/32 :goto_19

    nop

    :goto_5f
    sget-object v0, Lcom/android/internal/os/ZygoteServer$UsapPoolRefillAction;->NONE:Lcom/android/internal/os/ZygoteServer$UsapPoolRefillAction;

    goto/32 :goto_2

    nop

    :goto_60
    invoke-interface {v0, v5}, Ljava/util/stream/Stream;->mapToInt(Ljava/util/function/ToIntFunction;)Ljava/util/stream/IntStream;

    move-result-object v0

    goto/32 :goto_a3

    nop

    :goto_61
    array-length v10, v6

    goto/32 :goto_0

    nop

    :goto_62
    const/4 v0, -0x1

    goto/32 :goto_ca

    nop

    :goto_63
    if-gez v10, :cond_7

    goto/32 :goto_a8

    :cond_7
    goto/32 :goto_15

    nop

    :goto_64
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_d4

    nop

    :goto_65
    move-object v0, v4

    goto/32 :goto_ef

    nop

    :goto_66
    goto/16 :goto_39

    :goto_67
    goto/32 :goto_38

    nop

    :goto_68
    move v7, v13

    :goto_69
    goto/32 :goto_fd

    nop

    :goto_6a
    move v10, v0

    goto/32 :goto_58

    nop

    :goto_6b
    iput-wide v4, v1, Lcom/android/internal/os/ZygoteServer;->mUsapPoolRefillTriggerTimestamp:J

    :goto_6c
    goto/32 :goto_4f

    nop

    :goto_6d
    invoke-direct/range {p0 .. p1}, Lcom/android/internal/os/ZygoteServer;->acceptCommandPeer(Ljava/lang/String;)Lcom/android/internal/os/ZygoteConnection;

    move-result-object v0

    goto/32 :goto_86

    nop

    :goto_6e
    iget-wide v14, v1, Lcom/android/internal/os/ZygoteServer;->mUsapPoolRefillTriggerTimestamp:J

    goto/32 :goto_1c

    nop

    :goto_6f
    return-object v5

    :goto_70
    goto/32 :goto_37

    nop

    :goto_71
    goto :goto_72

    :catch_0
    move-exception v0

    :try_start_1
    iget-boolean v4, v1, Lcom/android/internal/os/ZygoteServer;->mIsForkChild:Z

    if-nez v4, :cond_8

    const-string v4, "Exception executing zygote command: "

    invoke-static {v14, v4, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {v3, v13}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/internal/os/ZygoteConnection;

    invoke-virtual {v4}, Lcom/android/internal/os/ZygoteConnection;->closeSocket()V

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    nop

    goto/16 :goto_45

    :cond_8
    const-string v4, "Caught post-fork exception in child process."

    invoke-static {v14, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    nop

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_72
    goto/32 :goto_6

    nop

    :goto_73
    goto/16 :goto_11

    :cond_9
    :try_start_2
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "command != null"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    goto/32 :goto_71

    nop

    :goto_74
    const/4 v0, 0x0

    goto/32 :goto_84

    nop

    :goto_75
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_9f

    nop

    :goto_76
    goto/16 :goto_99

    :goto_77
    goto/32 :goto_98

    nop

    :goto_78
    iget v0, v1, Lcom/android/internal/os/ZygoteServer;->mUsapPoolRefillDelayMs:I

    goto/32 :goto_6a

    nop

    :goto_79
    cmp-long v10, v12, v14

    goto/32 :goto_63

    nop

    :goto_7a
    int-to-short v12, v12

    goto/32 :goto_cc

    nop

    :goto_7b
    goto/16 :goto_4b

    :goto_7c
    goto/32 :goto_fa

    nop

    :goto_7d
    goto/16 :goto_10

    :goto_7e
    goto/32 :goto_20

    nop

    :goto_7f
    iput-object v0, v1, Lcom/android/internal/os/ZygoteServer;->mUsapPoolRefillAction:Lcom/android/internal/os/ZygoteServer$UsapPoolRefillAction;

    goto/32 :goto_96

    nop

    :goto_80
    sget v14, Landroid/system/OsConstants;->POLLIN:I

    goto/32 :goto_8d

    nop

    :goto_81
    sget v12, Landroid/system/OsConstants;->POLLIN:I

    goto/32 :goto_ce

    nop

    :goto_82
    int-to-short v7, v7

    goto/32 :goto_91

    nop

    :goto_83
    const/4 v7, 0x1

    goto/32 :goto_50

    nop

    :goto_84
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_85
    goto/32 :goto_22

    nop

    :goto_86
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_29

    nop

    :goto_87
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_ec

    nop

    :goto_88
    iput-object v4, v1, Lcom/android/internal/os/ZygoteServer;->mUsapPoolRefillAction:Lcom/android/internal/os/ZygoteServer$UsapPoolRefillAction;

    goto/32 :goto_3c

    nop

    :goto_89
    sget-object v0, Lcom/android/internal/os/ZygoteServer$UsapPoolRefillAction;->DELAYED:Lcom/android/internal/os/ZygoteServer$UsapPoolRefillAction;

    goto/32 :goto_7f

    nop

    :goto_8a
    move-object v8, v6

    goto/32 :goto_53

    nop

    :goto_8b
    if-lt v13, v9, :cond_a

    goto/32 :goto_18

    :cond_a
    :try_start_3
    invoke-virtual {v3, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/os/ZygoteConnection;

    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/os/ZygoteServer;->isUsapPoolEnabled()Z

    move-result v15

    if-nez v15, :cond_b

    invoke-static {}, Ldalvik/system/ZygoteHooks;->isIndefiniteThreadSuspensionSafe()Z

    move-result v15

    if-eqz v15, :cond_b

    const/4 v15, 0x1

    goto :goto_8c

    :cond_b
    move v15, v11

    :goto_8c
    nop

    invoke-virtual {v0, v1, v15}, Lcom/android/internal/os/ZygoteConnection;->processCommand(Lcom/android/internal/os/ZygoteServer;Z)Ljava/lang/Runnable;

    move-result-object v16

    iget-boolean v4, v1, Lcom/android/internal/os/ZygoteServer;->mIsForkChild:Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/32 :goto_b7

    nop

    :goto_8d
    and-int/2addr v0, v14

    goto/32 :goto_2f

    nop

    :goto_8e
    aput-object v11, v8, v0

    goto/32 :goto_9a

    nop

    :goto_8f
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_16

    nop

    :goto_90
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    goto/32 :goto_51

    nop

    :goto_91
    iput-short v7, v15, Landroid/system/StructPollfd;->events:S

    nop

    goto/32 :goto_e2

    nop

    :goto_92
    const-string v14, "ZygoteServer"

    goto/32 :goto_8b

    nop

    :goto_93
    goto/16 :goto_11

    :goto_94
    goto/32 :goto_b5

    nop

    :goto_95
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_f4

    nop

    :goto_96
    goto/16 :goto_69

    :goto_97
    goto/32 :goto_31

    nop

    :goto_98
    move v4, v11

    :goto_99
    nop

    goto/32 :goto_3f

    nop

    :goto_9a
    aget-object v11, v8, v0

    goto/32 :goto_dd

    nop

    :goto_9b
    move v10, v0

    :goto_9c
    :try_start_4
    invoke-static {v8, v10}, Landroid/system/Os;->poll([Landroid/system/StructPollfd;I)I

    move-result v0
    :try_end_4
    .catch Landroid/system/ErrnoException; {:try_start_4 .. :try_end_4} :catch_1

    goto/32 :goto_48

    nop

    :goto_9d
    invoke-static {}, Lcom/android/internal/os/Zygote;->getUsapPipeFDs()[I

    move-result-object v0

    goto/32 :goto_24

    nop

    :goto_9e
    sget-object v4, Lcom/android/internal/os/ZygoteServer$UsapPoolRefillAction;->IMMEDIATE:Lcom/android/internal/os/ZygoteServer$UsapPoolRefillAction;

    goto/32 :goto_88

    nop

    :goto_9f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_5c

    nop

    :goto_a0
    aput-object v10, v8, v0

    goto/32 :goto_a

    nop

    :goto_a1
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_75

    nop

    :goto_a2
    iput-boolean v11, v1, Lcom/android/internal/os/ZygoteServer;->mIsForkChild:Z

    goto/32 :goto_42

    nop

    :goto_a3
    invoke-interface {v0}, Ljava/util/stream/IntStream;->toArray()[I

    move-result-object v0

    goto/32 :goto_a4

    nop

    :goto_a4
    iget-object v5, v1, Lcom/android/internal/os/ZygoteServer;->mUsapPoolRefillAction:Lcom/android/internal/os/ZygoteServer$UsapPoolRefillAction;

    goto/32 :goto_49

    nop

    :goto_a5
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_100

    nop

    :goto_a6
    if-nez v10, :cond_c

    goto/32 :goto_67

    :cond_c
    goto/32 :goto_d

    nop

    :goto_a7
    goto :goto_9c

    :goto_a8
    goto/32 :goto_35

    nop

    :goto_a9
    invoke-virtual {v0}, Landroid/net/LocalServerSocket;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v0

    goto/32 :goto_87

    nop

    :goto_aa
    sget v12, Landroid/system/OsConstants;->POLLIN:I

    goto/32 :goto_7a

    nop

    :goto_ab
    int-to-long v14, v0

    goto/32 :goto_9

    nop

    :goto_ac
    if-nez v16, :cond_d

    goto/32 :goto_43

    :cond_d
    nop

    goto/32 :goto_a2

    nop

    :goto_ad
    const-wide/16 v4, -0x1

    goto/32 :goto_fc

    nop

    :goto_ae
    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_33

    nop

    :goto_af
    if-lt v0, v4, :cond_e

    goto/32 :goto_3d

    :cond_e
    goto/32 :goto_9e

    nop

    :goto_b0
    goto/16 :goto_9c

    :goto_b1
    goto/32 :goto_e1

    nop

    :goto_b2
    move v7, v0

    goto/32 :goto_4a

    nop

    :goto_b3
    const/4 v4, 0x1

    goto/32 :goto_30

    nop

    :goto_b4
    const-wide/16 v4, -0x1

    goto/32 :goto_d2

    nop

    :goto_b5
    if-eqz v13, :cond_f

    goto/32 :goto_34

    :cond_f
    goto/32 :goto_6d

    nop

    :goto_b6
    invoke-direct {v5}, Lcom/android/internal/os/ZygoteServer$$ExternalSyntheticLambda0;-><init>()V

    goto/32 :goto_60

    nop

    :goto_b7
    if-nez v4, :cond_10

    goto/32 :goto_44

    :cond_10
    goto/32 :goto_ac

    nop

    :goto_b8
    goto/16 :goto_1

    :goto_b9
    goto/32 :goto_fb

    nop

    :goto_ba
    if-eq v13, v9, :cond_11

    goto/32 :goto_7e

    :cond_11
    goto/32 :goto_d9

    nop

    :goto_bb
    const/4 v7, 0x1

    goto/32 :goto_e9

    nop

    :goto_bc
    iget v0, v1, Lcom/android/internal/os/ZygoteServer;->mUsapPoolRefillDelayMs:I

    goto/32 :goto_f9

    nop

    :goto_bd
    aget-object v11, v8, v0

    goto/32 :goto_81

    nop

    :goto_be
    move/from16 v17, v7

    goto/32 :goto_b2

    nop

    :goto_bf
    const-string/jumbo v5, "poll failed"

    goto/32 :goto_36

    nop

    :goto_c0
    const-wide/16 v4, -0x1

    goto/32 :goto_7b

    nop

    :goto_c1
    new-instance v5, Lcom/android/internal/os/ZygoteServer$$ExternalSyntheticLambda0;

    goto/32 :goto_b6

    nop

    :goto_c2
    if-nez v10, :cond_12

    goto/32 :goto_101

    :cond_12
    goto/32 :goto_90

    nop

    :goto_c3
    aget-object v15, v8, v0

    goto/32 :goto_f1

    nop

    :goto_c4
    aget-object v15, v8, v0

    goto/32 :goto_ea

    nop

    :goto_c5
    iput-object v12, v10, Landroid/system/StructPollfd;->fd:Ljava/io/FileDescriptor;

    goto/32 :goto_1f

    nop

    :goto_c6
    move v10, v0

    goto/32 :goto_a7

    nop

    :goto_c7
    iget-object v12, v1, Lcom/android/internal/os/ZygoteServer;->mUsapPoolEventFD:Ljava/io/FileDescriptor;

    goto/32 :goto_c5

    nop

    :goto_c8
    add-int/lit8 v12, v12, 0x1

    goto/32 :goto_2c

    nop

    :goto_c9
    iput-object v10, v1, Lcom/android/internal/os/ZygoteServer;->mUsapPoolRefillAction:Lcom/android/internal/os/ZygoteServer$UsapPoolRefillAction;

    goto/32 :goto_c6

    nop

    :goto_ca
    move v10, v0

    goto/32 :goto_b0

    nop

    :goto_cb
    const/16 v0, 0x8

    :try_start_5
    new-array v4, v0, [B

    aget-object v5, v8, v13

    iget-object v5, v5, Landroid/system/StructPollfd;->fd:Ljava/io/FileDescriptor;

    array-length v15, v4

    invoke-static {v5, v4, v11, v15}, Landroid/system/Os;->read(Ljava/io/FileDescriptor;[BII)I

    move-result v5

    if-ne v5, v0, :cond_1b

    new-instance v0, Ljava/io/DataInputStream;

    new-instance v15, Ljava/io/ByteArrayInputStream;

    invoke-direct {v15, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v15}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v14
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    nop

    nop

    goto/32 :goto_e7

    nop

    :goto_cc
    iput-short v12, v10, Landroid/system/StructPollfd;->events:S

    goto/32 :goto_3a

    nop

    :goto_cd
    if-eq v5, v13, :cond_13

    goto/32 :goto_77

    :cond_13
    goto/32 :goto_76

    nop

    :goto_ce
    int-to-short v12, v12

    goto/32 :goto_52

    nop

    :goto_cf
    const/4 v11, 0x0

    goto/32 :goto_a6

    nop

    :goto_d0
    goto/16 :goto_54

    :goto_d1
    goto/32 :goto_d8

    nop

    :goto_d2
    goto/16 :goto_6c

    :catch_1
    move-exception v0

    goto/32 :goto_28

    nop

    :goto_d3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_f

    nop

    :goto_d4
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_d3

    nop

    :goto_d5
    goto/16 :goto_11

    :catch_2
    move-exception v0

    goto/32 :goto_ba

    nop

    :goto_d6
    cmp-long v10, v12, v14

    goto/32 :goto_df

    nop

    :goto_d7
    if-ne v0, v4, :cond_14

    goto/32 :goto_14

    :cond_14
    nop

    goto/32 :goto_db

    nop

    :goto_d8
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    goto/32 :goto_5

    nop

    :goto_d9
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_8

    nop

    :goto_da
    iget-short v0, v0, Landroid/system/StructPollfd;->revents:S

    goto/32 :goto_80

    nop

    :goto_db
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_b3

    nop

    :goto_dc
    invoke-direct {v11}, Landroid/system/StructPollfd;-><init>()V

    goto/32 :goto_8e

    nop

    :goto_dd
    iput-object v10, v11, Landroid/system/StructPollfd;->fd:Ljava/io/FileDescriptor;

    goto/32 :goto_bd

    nop

    :goto_de
    if-ge v4, v5, :cond_15

    goto/32 :goto_26

    :cond_15
    goto/32 :goto_41

    nop

    :goto_df
    if-lez v10, :cond_16

    goto/32 :goto_59

    :cond_16
    goto/32 :goto_78

    nop

    :goto_e0
    new-array v6, v6, [Landroid/system/StructPollfd;

    goto/32 :goto_5e

    nop

    :goto_e1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    goto/32 :goto_6e

    nop

    :goto_e2
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_c8

    nop

    :goto_e3
    cmp-long v0, v12, v4

    goto/32 :goto_f7

    nop

    :goto_e4
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_64

    nop

    :goto_e5
    move-object v3, v0

    goto/32 :goto_5a

    nop

    :goto_e6
    iput-boolean v11, v1, Lcom/android/internal/os/ZygoteServer;->mIsForkChild:Z

    goto/32 :goto_73

    nop

    :goto_e7
    if-gt v13, v9, :cond_17

    goto/32 :goto_1e

    :cond_17
    goto/32 :goto_b

    nop

    :goto_e8
    iget-boolean v6, v1, Lcom/android/internal/os/ZygoteServer;->mUsapPoolEnabled:Z

    goto/32 :goto_83

    nop

    :goto_e9
    move v0, v13

    goto/32 :goto_ad

    nop

    :goto_ea
    sget v7, Landroid/system/OsConstants;->POLLIN:I

    goto/32 :goto_82

    nop

    :goto_eb
    long-to-int v0, v14

    goto/32 :goto_9b

    nop

    :goto_ec
    const/4 v0, 0x0

    goto/32 :goto_c

    nop

    :goto_ed
    move v0, v13

    goto/32 :goto_c0

    nop

    :goto_ee
    iget-wide v12, v1, Lcom/android/internal/os/ZygoteServer;->mUsapPoolRefillTriggerTimestamp:J

    goto/32 :goto_e3

    nop

    :goto_ef
    new-instance v4, Ljava/lang/RuntimeException;

    goto/32 :goto_bf

    nop

    :goto_f0
    invoke-direct {v15}, Landroid/system/StructPollfd;-><init>()V

    goto/32 :goto_3b

    nop

    :goto_f1
    iput-object v14, v15, Landroid/system/StructPollfd;->fd:Ljava/io/FileDescriptor;

    goto/32 :goto_c4

    nop

    :goto_f2
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_27

    nop

    :goto_f3
    if-eqz v12, :cond_18

    goto/32 :goto_97

    :cond_18
    goto/32 :goto_3e

    nop

    :goto_f4
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_e5

    nop

    :goto_f5
    iget-boolean v10, v1, Lcom/android/internal/os/ZygoteServer;->mUsapPoolEnabled:Z

    goto/32 :goto_cf

    nop

    :goto_f6
    iget v4, v1, Lcom/android/internal/os/ZygoteServer;->mUsapPoolSizeMax:I

    goto/32 :goto_23

    nop

    :goto_f7
    if-eqz v0, :cond_19

    goto/32 :goto_b1

    :cond_19
    goto/32 :goto_62

    nop

    :goto_f8
    sget-object v10, Lcom/android/internal/os/ZygoteServer$UsapPoolRefillAction;->DELAYED:Lcom/android/internal/os/ZygoteServer$UsapPoolRefillAction;

    goto/32 :goto_c9

    nop

    :goto_f9
    int-to-long v14, v0

    goto/32 :goto_79

    nop

    :goto_fa
    if-nez v7, :cond_1a

    goto/32 :goto_26

    :cond_1a
    goto/32 :goto_1a

    nop

    :goto_fb
    move v7, v0

    goto/32 :goto_66

    nop

    :goto_fc
    goto/16 :goto_4b

    :cond_1b
    :try_start_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Incomplete read from USAP management FD of size "

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v14, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    goto/32 :goto_d5

    nop

    :goto_fd
    iget-object v0, v1, Lcom/android/internal/os/ZygoteServer;->mUsapPoolRefillAction:Lcom/android/internal/os/ZygoteServer$UsapPoolRefillAction;

    goto/32 :goto_21

    nop

    :goto_fe
    iget v4, v1, Lcom/android/internal/os/ZygoteServer;->mUsapPoolSizeMin:I

    goto/32 :goto_af

    nop

    :goto_ff
    move v9, v0

    goto/32 :goto_f5

    nop

    :goto_100
    goto/16 :goto_85

    :goto_101
    goto/32 :goto_ff

    nop

    :goto_102
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_a1

    nop

    :goto_103
    invoke-interface {v0}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v0

    goto/32 :goto_c1

    nop

    :goto_104
    const-wide/16 v4, -0x1

    goto/32 :goto_6b

    nop

    :goto_105
    add-int/2addr v6, v8

    goto/32 :goto_e0

    nop
.end method

.method setForkChild()V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x1

    goto/32 :goto_1

    nop

    :goto_1
    iput-boolean v0, p0, Lcom/android/internal/os/ZygoteServer;->mIsForkChild:Z

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method

.method setUsapPoolStatus(ZLandroid/net/LocalSocket;)Ljava/lang/Runnable;
    .locals 4

    goto/32 :goto_13

    nop

    :goto_0
    invoke-static {}, Lcom/android/internal/os/Zygote;->emptyUsapPool()V

    goto/32 :goto_9

    nop

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_8

    nop

    :goto_2
    const-string v3, "DISABLED"

    :goto_3
    goto/32 :goto_24

    nop

    :goto_4
    const-string v0, "Attempting to enable a USAP pool for a Zygote that doesn\'t support it."

    goto/32 :goto_22

    nop

    :goto_5
    return-object v0

    :goto_6
    goto/32 :goto_0

    nop

    :goto_7
    if-eq v0, p1, :cond_0

    goto/32 :goto_17

    :cond_0
    goto/32 :goto_16

    nop

    :goto_8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_18

    nop

    :goto_9
    return-object v2

    :goto_a
    invoke-virtual {v1}, Ljava/io/FileDescriptor;->getInt$()I

    move-result v1

    goto/32 :goto_1f

    nop

    :goto_b
    if-nez p1, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_12

    nop

    :goto_c
    const-string v3, "ENABLED"

    goto/32 :goto_10

    nop

    :goto_d
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_14

    nop

    :goto_e
    iput-boolean p1, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolEnabled:Z

    goto/32 :goto_b

    nop

    :goto_f
    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_e

    nop

    :goto_10
    goto :goto_3

    :goto_11
    goto/32 :goto_2

    nop

    :goto_12
    const/4 v0, 0x1

    goto/32 :goto_1a

    nop

    :goto_13
    iget-boolean v0, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolSupported:Z

    goto/32 :goto_25

    nop

    :goto_14
    if-nez p1, :cond_2

    goto/32 :goto_11

    :cond_2
    goto/32 :goto_c

    nop

    :goto_15
    iget-boolean v0, p0, Lcom/android/internal/os/ZygoteServer;->mUsapPoolEnabled:Z

    goto/32 :goto_7

    nop

    :goto_16
    return-object v2

    :goto_17
    goto/32 :goto_1

    nop

    :goto_18
    const-string v3, "USAP Pool status change: "

    goto/32 :goto_d

    nop

    :goto_19
    invoke-virtual {p0, v0, v2}, Lcom/android/internal/os/ZygoteServer;->fillUsapPool([IZ)Ljava/lang/Runnable;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_1a
    new-array v0, v0, [I

    goto/32 :goto_23

    nop

    :goto_1b
    if-eqz v0, :cond_3

    goto/32 :goto_1e

    :cond_3
    goto/32 :goto_4

    nop

    :goto_1c
    const/4 v2, 0x0

    goto/32 :goto_1b

    nop

    :goto_1d
    return-object v2

    :goto_1e
    goto/32 :goto_15

    nop

    :goto_1f
    const/4 v2, 0x0

    goto/32 :goto_21

    nop

    :goto_20
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_f

    nop

    :goto_21
    aput v1, v0, v2

    goto/32 :goto_19

    nop

    :goto_22
    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_1d

    nop

    :goto_23
    invoke-virtual {p2}, Landroid/net/LocalSocket;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    goto/32 :goto_a

    nop

    :goto_24
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_20

    nop

    :goto_25
    const-string v1, "ZygoteServer"

    goto/32 :goto_1c

    nop
.end method
