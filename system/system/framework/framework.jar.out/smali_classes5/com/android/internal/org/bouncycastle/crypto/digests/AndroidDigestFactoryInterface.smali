.class interface abstract Lcom/android/internal/org/bouncycastle/crypto/digests/AndroidDigestFactoryInterface;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getMD5()Lcom/android/internal/org/bouncycastle/crypto/Digest;
.end method

.method public abstract getSHA1()Lcom/android/internal/org/bouncycastle/crypto/Digest;
.end method

.method public abstract getSHA224()Lcom/android/internal/org/bouncycastle/crypto/Digest;
.end method

.method public abstract getSHA256()Lcom/android/internal/org/bouncycastle/crypto/Digest;
.end method

.method public abstract getSHA384()Lcom/android/internal/org/bouncycastle/crypto/Digest;
.end method

.method public abstract getSHA512()Lcom/android/internal/org/bouncycastle/crypto/Digest;
.end method
