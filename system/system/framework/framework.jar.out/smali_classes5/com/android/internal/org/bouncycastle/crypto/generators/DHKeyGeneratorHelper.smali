.class Lcom/android/internal/org/bouncycastle/crypto/generators/DHKeyGeneratorHelper;
.super Ljava/lang/Object;


# static fields
.field static final INSTANCE:Lcom/android/internal/org/bouncycastle/crypto/generators/DHKeyGeneratorHelper;

.field private static final ONE:Ljava/math/BigInteger;

.field private static final TWO:Ljava/math/BigInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/android/internal/org/bouncycastle/crypto/generators/DHKeyGeneratorHelper;

    invoke-direct {v0}, Lcom/android/internal/org/bouncycastle/crypto/generators/DHKeyGeneratorHelper;-><init>()V

    sput-object v0, Lcom/android/internal/org/bouncycastle/crypto/generators/DHKeyGeneratorHelper;->INSTANCE:Lcom/android/internal/org/bouncycastle/crypto/generators/DHKeyGeneratorHelper;

    const-wide/16 v0, 0x1

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    sput-object v0, Lcom/android/internal/org/bouncycastle/crypto/generators/DHKeyGeneratorHelper;->ONE:Ljava/math/BigInteger;

    const-wide/16 v0, 0x2

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    sput-object v0, Lcom/android/internal/org/bouncycastle/crypto/generators/DHKeyGeneratorHelper;->TWO:Ljava/math/BigInteger;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method calculatePrivate(Lcom/android/internal/org/bouncycastle/crypto/params/DHParameters;Ljava/security/SecureRandom;)Ljava/math/BigInteger;
    .locals 8

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p1}, Lcom/android/internal/org/bouncycastle/crypto/params/DHParameters;->getL()I

    move-result v0

    goto/32 :goto_1e

    nop

    :goto_1
    invoke-virtual {p1}, Lcom/android/internal/org/bouncycastle/crypto/params/DHParameters;->getQ()Ljava/math/BigInteger;

    move-result-object v3

    goto/32 :goto_13

    nop

    :goto_2
    add-int/lit8 v4, v2, -0x1

    goto/32 :goto_10

    nop

    :goto_3
    invoke-static {v1, v4, p2}, Lcom/android/internal/org/bouncycastle/util/BigIntegers;->createRandomInRange(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/security/SecureRandom;)Ljava/math/BigInteger;

    move-result-object v6

    goto/32 :goto_14

    nop

    :goto_4
    invoke-virtual {p1}, Lcom/android/internal/org/bouncycastle/crypto/params/DHParameters;->getM()I

    move-result v2

    goto/32 :goto_17

    nop

    :goto_5
    goto :goto_a

    :goto_6
    goto/32 :goto_20

    nop

    :goto_7
    invoke-virtual {v4}, Ljava/math/BigInteger;->bitLength()I

    move-result v5

    goto/32 :goto_19

    nop

    :goto_8
    invoke-virtual {v3, v4}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v4

    goto/32 :goto_7

    nop

    :goto_9
    ushr-int/lit8 v1, v0, 0x2

    :goto_a
    goto/32 :goto_1d

    nop

    :goto_b
    invoke-virtual {p1}, Lcom/android/internal/org/bouncycastle/crypto/params/DHParameters;->getP()Ljava/math/BigInteger;

    move-result-object v3

    :goto_c
    goto/32 :goto_e

    nop

    :goto_d
    invoke-static {v2}, Lcom/android/internal/org/bouncycastle/math/ec/WNafUtil;->getNafWeight(Ljava/math/BigInteger;)I

    move-result v3

    goto/32 :goto_1c

    nop

    :goto_e
    sget-object v4, Lcom/android/internal/org/bouncycastle/crypto/generators/DHKeyGeneratorHelper;->TWO:Ljava/math/BigInteger;

    goto/32 :goto_8

    nop

    :goto_f
    goto :goto_1a

    :goto_10
    invoke-virtual {v3, v4}, Ljava/math/BigInteger;->shiftLeft(I)Ljava/math/BigInteger;

    move-result-object v1

    :goto_11
    goto/32 :goto_1

    nop

    :goto_12
    add-int/lit8 v3, v0, -0x1

    goto/32 :goto_18

    nop

    :goto_13
    if-eqz v3, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_b

    nop

    :goto_14
    invoke-static {v6}, Lcom/android/internal/org/bouncycastle/math/ec/WNafUtil;->getNafWeight(Ljava/math/BigInteger;)I

    move-result v7

    goto/32 :goto_1f

    nop

    :goto_15
    return-object v6

    :goto_16
    goto/32 :goto_f

    nop

    :goto_17
    if-nez v2, :cond_1

    goto/32 :goto_11

    :cond_1
    goto/32 :goto_1b

    nop

    :goto_18
    invoke-virtual {v2, v3}, Ljava/math/BigInteger;->setBit(I)Ljava/math/BigInteger;

    move-result-object v2

    goto/32 :goto_d

    nop

    :goto_19
    ushr-int/lit8 v5, v5, 0x2

    :goto_1a
    goto/32 :goto_3

    nop

    :goto_1b
    sget-object v3, Lcom/android/internal/org/bouncycastle/crypto/generators/DHKeyGeneratorHelper;->ONE:Ljava/math/BigInteger;

    goto/32 :goto_2

    nop

    :goto_1c
    if-ge v3, v1, :cond_2

    goto/32 :goto_22

    :cond_2
    goto/32 :goto_21

    nop

    :goto_1d
    invoke-static {v0, p2}, Lcom/android/internal/org/bouncycastle/util/BigIntegers;->createRandomBigInteger(ILjava/security/SecureRandom;)Ljava/math/BigInteger;

    move-result-object v2

    goto/32 :goto_12

    nop

    :goto_1e
    if-nez v0, :cond_3

    goto/32 :goto_6

    :cond_3
    goto/32 :goto_9

    nop

    :goto_1f
    if-ge v7, v5, :cond_4

    goto/32 :goto_16

    :cond_4
    goto/32 :goto_15

    nop

    :goto_20
    sget-object v1, Lcom/android/internal/org/bouncycastle/crypto/generators/DHKeyGeneratorHelper;->TWO:Ljava/math/BigInteger;

    goto/32 :goto_4

    nop

    :goto_21
    return-object v2

    :goto_22
    goto/32 :goto_5

    nop
.end method

.method calculatePublic(Lcom/android/internal/org/bouncycastle/crypto/params/DHParameters;Ljava/math/BigInteger;)Ljava/math/BigInteger;
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p1}, Lcom/android/internal/org/bouncycastle/crypto/params/DHParameters;->getG()Ljava/math/BigInteger;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_1
    invoke-virtual {v0, p2, v1}, Ljava/math/BigInteger;->modPow(Ljava/math/BigInteger;Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_2
    invoke-virtual {p1}, Lcom/android/internal/org/bouncycastle/crypto/params/DHParameters;->getP()Ljava/math/BigInteger;

    move-result-object v1

    goto/32 :goto_1

    nop

    :goto_3
    return-object v0
.end method
