.class public Lcom/android/internal/alsa/LineTokenizer;
.super Ljava/lang/Object;


# static fields
.field public static final kTokenNotFound:I = -0x1


# instance fields
.field private final mDelimiters:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/internal/alsa/LineTokenizer;->mDelimiters:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method nextDelimiter(Ljava/lang/String;I)I
    .locals 5

    goto/32 :goto_1

    nop

    :goto_0
    return v2

    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    goto/32 :goto_5

    nop

    :goto_2
    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    goto/32 :goto_d

    nop

    :goto_3
    goto :goto_6

    :goto_4
    goto/32 :goto_b

    nop

    :goto_5
    move v1, p2

    :goto_6
    goto/32 :goto_11

    nop

    :goto_7
    goto :goto_4

    :goto_8
    goto/32 :goto_e

    nop

    :goto_9
    move v2, v1

    :goto_a
    goto/32 :goto_0

    nop

    :goto_b
    if-lt v1, v0, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_9

    nop

    :goto_c
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    goto/32 :goto_2

    nop

    :goto_d
    if-ne v3, v2, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_7

    nop

    :goto_e
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_3

    nop

    :goto_f
    if-lt v1, v0, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_10

    nop

    :goto_10
    iget-object v3, p0, Lcom/android/internal/alsa/LineTokenizer;->mDelimiters:Ljava/lang/String;

    goto/32 :goto_c

    nop

    :goto_11
    const/4 v2, -0x1

    goto/32 :goto_f

    nop
.end method

.method nextToken(Ljava/lang/String;I)I
    .locals 5

    goto/32 :goto_10

    nop

    :goto_0
    iget-object v3, p0, Lcom/android/internal/alsa/LineTokenizer;->mDelimiters:Ljava/lang/String;

    goto/32 :goto_f

    nop

    :goto_1
    if-lt v1, v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_5

    nop

    :goto_2
    goto :goto_b

    :goto_3
    goto/32 :goto_1

    nop

    :goto_4
    if-eq v3, v2, :cond_1

    goto/32 :goto_9

    :cond_1
    goto/32 :goto_8

    nop

    :goto_5
    move v2, v1

    :goto_6
    goto/32 :goto_d

    nop

    :goto_7
    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    goto/32 :goto_4

    nop

    :goto_8
    goto :goto_3

    :goto_9
    goto/32 :goto_11

    nop

    :goto_a
    move v1, p2

    :goto_b
    goto/32 :goto_c

    nop

    :goto_c
    const/4 v2, -0x1

    goto/32 :goto_e

    nop

    :goto_d
    return v2

    :goto_e
    if-lt v1, v0, :cond_2

    goto/32 :goto_3

    :cond_2
    goto/32 :goto_0

    nop

    :goto_f
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    goto/32 :goto_7

    nop

    :goto_10
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    goto/32 :goto_a

    nop

    :goto_11
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_2

    nop
.end method
