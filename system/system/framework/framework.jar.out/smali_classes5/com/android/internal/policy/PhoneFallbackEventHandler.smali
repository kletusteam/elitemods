.class public Lcom/android/internal/policy/PhoneFallbackEventHandler;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/FallbackEventHandler;


# static fields
.field private static final DEBUG:Z

.field private static TAG:Ljava/lang/String;


# instance fields
.field mAudioManager:Landroid/media/AudioManager;

.field mContext:Landroid/content/Context;

.field mKeyguardManager:Landroid/app/KeyguardManager;

.field mMediaSessionManager:Landroid/media/session/MediaSessionManager;

.field mSearchManager:Landroid/app/SearchManager;

.field mTelephonyManager:Landroid/telephony/TelephonyManager;

.field mView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "PhoneFallbackEventHandler"

    sput-object v0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mContext:Landroid/content/Context;

    return-void
.end method

.method private handleMediaKeyEvent(Landroid/view/KeyEvent;)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/internal/policy/PhoneFallbackEventHandler;->getMediaSessionManager()Landroid/media/session/MediaSessionManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/media/session/MediaSessionManager;->dispatchMediaKeyEventAsSystemService(Landroid/view/KeyEvent;)V

    return-void
.end method

.method private handleVolumeKeyEvent(Landroid/view/KeyEvent;)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/internal/policy/PhoneFallbackEventHandler;->getMediaSessionManager()Landroid/media/session/MediaSessionManager;

    move-result-object v0

    const/high16 v1, -0x80000000

    invoke-virtual {v0, p1, v1}, Landroid/media/session/MediaSessionManager;->dispatchVolumeKeyEventAsSystemService(Landroid/view/KeyEvent;I)V

    return-void
.end method

.method private isNotInstantAppAndKeyguardRestricted(Landroid/view/KeyEvent$DispatcherState;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager;->isInstantApp()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/android/internal/policy/PhoneFallbackEventHandler;->getKeyguardManager()Landroid/app/KeyguardManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isUserSetupComplete()Z
    .locals 3

    iget-object v0, p0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "user_setup_complete"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    if-nez v0, :cond_0

    invoke-virtual {p0, v1, p1}, Lcom/android/internal/policy/PhoneFallbackEventHandler;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    return v2

    :cond_0
    invoke-virtual {p0, v1, p1}, Lcom/android/internal/policy/PhoneFallbackEventHandler;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v2

    return v2
.end method

.method getAudioManager()Landroid/media/AudioManager;
    .locals 2

    goto/32 :goto_4

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mContext:Landroid/content/Context;

    goto/32 :goto_1

    nop

    :goto_1
    const-string v1, "audio"

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_3
    if-eqz v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_0

    nop

    :goto_4
    iget-object v0, p0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mAudioManager:Landroid/media/AudioManager;

    goto/32 :goto_3

    nop

    :goto_5
    return-object v0

    :goto_6
    iput-object v0, p0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mAudioManager:Landroid/media/AudioManager;

    :goto_7
    goto/32 :goto_9

    nop

    :goto_8
    check-cast v0, Landroid/media/AudioManager;

    goto/32 :goto_6

    nop

    :goto_9
    iget-object v0, p0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mAudioManager:Landroid/media/AudioManager;

    goto/32 :goto_5

    nop
.end method

.method getKeyguardManager()Landroid/app/KeyguardManager;
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mKeyguardManager:Landroid/app/KeyguardManager;

    goto/32 :goto_3

    nop

    :goto_1
    if-eqz v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_5

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mKeyguardManager:Landroid/app/KeyguardManager;

    goto/32 :goto_1

    nop

    :goto_3
    return-object v0

    :goto_4
    const-string v1, "keyguard"

    goto/32 :goto_8

    nop

    :goto_5
    iget-object v0, p0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mContext:Landroid/content/Context;

    goto/32 :goto_4

    nop

    :goto_6
    iput-object v0, p0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mKeyguardManager:Landroid/app/KeyguardManager;

    :goto_7
    goto/32 :goto_0

    nop

    :goto_8
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_9
    check-cast v0, Landroid/app/KeyguardManager;

    goto/32 :goto_6

    nop
.end method

.method getMediaSessionManager()Landroid/media/session/MediaSessionManager;
    .locals 2

    goto/32 :goto_4

    nop

    :goto_0
    const-string/jumbo v1, "media_session"

    goto/32 :goto_9

    nop

    :goto_1
    iput-object v0, p0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mMediaSessionManager:Landroid/media/session/MediaSessionManager;

    :goto_2
    goto/32 :goto_7

    nop

    :goto_3
    return-object v0

    :goto_4
    iget-object v0, p0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mMediaSessionManager:Landroid/media/session/MediaSessionManager;

    goto/32 :goto_8

    nop

    :goto_5
    iget-object v0, p0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mContext:Landroid/content/Context;

    goto/32 :goto_0

    nop

    :goto_6
    check-cast v0, Landroid/media/session/MediaSessionManager;

    goto/32 :goto_1

    nop

    :goto_7
    iget-object v0, p0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mMediaSessionManager:Landroid/media/session/MediaSessionManager;

    goto/32 :goto_3

    nop

    :goto_8
    if-eqz v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_5

    nop

    :goto_9
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_6

    nop
.end method

.method getSearchManager()Landroid/app/SearchManager;
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    if-eqz v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_9

    nop

    :goto_1
    check-cast v0, Landroid/app/SearchManager;

    goto/32 :goto_5

    nop

    :goto_2
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_3
    iget-object v0, p0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mSearchManager:Landroid/app/SearchManager;

    goto/32 :goto_0

    nop

    :goto_4
    return-object v0

    :goto_5
    iput-object v0, p0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mSearchManager:Landroid/app/SearchManager;

    :goto_6
    goto/32 :goto_8

    nop

    :goto_7
    const-string/jumbo v1, "search"

    goto/32 :goto_2

    nop

    :goto_8
    iget-object v0, p0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mSearchManager:Landroid/app/SearchManager;

    goto/32 :goto_4

    nop

    :goto_9
    iget-object v0, p0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mContext:Landroid/content/Context;

    goto/32 :goto_7

    nop
.end method

.method getTelephonyManager()Landroid/telephony/TelephonyManager;
    .locals 2

    goto/32 :goto_6

    nop

    :goto_0
    return-object v0

    :goto_1
    check-cast v0, Landroid/telephony/TelephonyManager;

    goto/32 :goto_3

    nop

    :goto_2
    if-eqz v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_8

    nop

    :goto_3
    iput-object v0, p0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    :goto_4
    goto/32 :goto_9

    nop

    :goto_5
    const-string/jumbo v1, "phone"

    goto/32 :goto_7

    nop

    :goto_6
    iget-object v0, p0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    goto/32 :goto_2

    nop

    :goto_7
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_8
    iget-object v0, p0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mContext:Landroid/content/Context;

    goto/32 :goto_5

    nop

    :goto_9
    iget-object v0, p0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    goto/32 :goto_0

    nop
.end method

.method onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 17

    goto/32 :goto_6a

    nop

    :goto_0
    const/16 v16, 0x0

    goto/32 :goto_32

    nop

    :goto_1
    goto :goto_17

    :goto_2
    goto/32 :goto_2a

    nop

    :goto_3
    const/4 v13, 0x0

    goto/32 :goto_23

    nop

    :goto_4
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/PhoneFallbackEventHandler;->isUserSetupComplete()Z

    move-result v7

    goto/32 :goto_15

    nop

    :goto_5
    return v5

    :sswitch_0
    goto/32 :goto_6c

    nop

    :goto_6
    goto/16 :goto_3f

    :goto_7
    goto/32 :goto_5b

    nop

    :goto_8
    goto/16 :goto_3f

    :goto_9
    goto/32 :goto_b

    nop

    :goto_a
    sget-object v10, Landroid/os/UserHandle;->CURRENT_OR_SELF:Landroid/os/UserHandle;

    goto/32 :goto_48

    nop

    :goto_b
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v6

    goto/32 :goto_21

    nop

    :goto_c
    sget-object v0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->TAG:Ljava/lang/String;

    goto/32 :goto_1e

    nop

    :goto_d
    new-instance v7, Landroid/content/Intent;

    goto/32 :goto_2e

    nop

    :goto_e
    if-nez v6, :cond_0

    goto/32 :goto_67

    :cond_0
    goto/32 :goto_22

    nop

    :goto_f
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/PhoneFallbackEventHandler;->isUserSetupComplete()Z

    move-result v6

    goto/32 :goto_e

    nop

    :goto_10
    if-ne v7, v5, :cond_1

    goto/32 :goto_6f

    :cond_1
    goto/32 :goto_1b

    nop

    :goto_11
    invoke-static {v0, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_2b

    nop

    :goto_12
    goto :goto_17

    :goto_13
    goto/32 :goto_50

    nop

    :goto_14
    const/high16 v0, 0x10000000

    goto/32 :goto_28

    nop

    :goto_15
    if-nez v7, :cond_2

    goto/32 :goto_1d

    :cond_2
    goto/32 :goto_d

    nop

    :goto_16
    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_17
    goto/32 :goto_3e

    nop

    :goto_18
    const/4 v5, 0x1

    sparse-switch p1, :sswitch_data_0

    goto/32 :goto_20

    nop

    :goto_19
    invoke-direct {v1, v3}, Lcom/android/internal/policy/PhoneFallbackEventHandler;->isNotInstantAppAndKeyguardRestricted(Landroid/view/KeyEvent$DispatcherState;)Z

    move-result v6

    goto/32 :goto_25

    nop

    :goto_1a
    invoke-virtual {v0}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v3

    goto/32 :goto_14

    nop

    :goto_1b
    iget v7, v6, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    goto/32 :goto_47

    nop

    :goto_1c
    goto/16 :goto_3f

    :goto_1d
    goto/32 :goto_c

    nop

    :goto_1e
    const-string v5, "Not dispatching SEARCH long press because user setup is in progress."

    goto/32 :goto_11

    nop

    :goto_1f
    iget-object v6, v1, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mContext:Landroid/content/Context;

    goto/32 :goto_26

    nop

    :goto_20
    goto/16 :goto_3f

    :sswitch_1
    goto/32 :goto_33

    nop

    :goto_21
    if-eqz v6, :cond_3

    goto/32 :goto_2d

    :cond_3
    goto/32 :goto_58

    nop

    :goto_22
    iget-object v6, v1, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mView:Landroid/view/View;

    goto/32 :goto_34

    nop

    :goto_23
    const/4 v14, 0x0

    goto/32 :goto_51

    nop

    :goto_24
    invoke-direct {v4, v7, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    goto/32 :goto_31

    nop

    :goto_25
    if-nez v6, :cond_4

    goto/32 :goto_9

    :cond_4
    goto/32 :goto_8

    nop

    :goto_26
    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    goto/32 :goto_3d

    nop

    :goto_27
    if-nez v6, :cond_5

    goto/32 :goto_13

    :cond_5
    goto/32 :goto_4f

    nop

    :goto_28
    const/4 v4, 0x0

    goto/32 :goto_18

    nop

    :goto_29
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v6

    goto/32 :goto_4c

    nop

    :goto_2a
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v6

    goto/32 :goto_78

    nop

    :goto_2b
    goto/16 :goto_3f

    :sswitch_2
    goto/32 :goto_75

    nop

    :goto_2c
    goto/16 :goto_63

    :goto_2d
    goto/32 :goto_5e

    nop

    :goto_2e
    const-string v8, "android.intent.action.SEARCH_LONG_PRESS"

    goto/32 :goto_4d

    nop

    :goto_2f
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/PhoneFallbackEventHandler;->startCallActivity()V

    :goto_30
    goto/32 :goto_12

    nop

    :goto_31
    invoke-virtual {v4, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto/32 :goto_4e

    nop

    :goto_32
    move-object v9, v4

    goto/32 :goto_5f

    nop

    :goto_33
    invoke-direct {v1, v3}, Lcom/android/internal/policy/PhoneFallbackEventHandler;->isNotInstantAppAndKeyguardRestricted(Landroid/view/KeyEvent$DispatcherState;)Z

    move-result v6

    goto/32 :goto_74

    nop

    :goto_34
    invoke-virtual {v6, v4}, Landroid/view/View;->performHapticFeedback(I)Z

    goto/32 :goto_35

    nop

    :goto_35
    new-instance v4, Landroid/content/Intent;

    goto/32 :goto_64

    nop

    :goto_36
    invoke-virtual {v7, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    :try_start_0
    iget-object v0, v1, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mView:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->performHapticFeedback(I)Z

    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/PhoneFallbackEventHandler;->getSearchManager()Landroid/app/SearchManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/SearchManager;->stopSearch()V

    iget-object v0, v1, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v7}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {v3, v2}, Landroid/view/KeyEvent$DispatcherState;->performedLongPress(Landroid/view/KeyEvent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    goto/32 :goto_79

    nop

    :goto_37
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/PhoneFallbackEventHandler;->isUserSetupComplete()Z

    move-result v6

    goto/32 :goto_27

    nop

    :goto_38
    invoke-virtual {v4, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto/32 :goto_57

    nop

    :goto_39
    invoke-direct {v1, v3}, Lcom/android/internal/policy/PhoneFallbackEventHandler;->isNotInstantAppAndKeyguardRestricted(Landroid/view/KeyEvent$DispatcherState;)Z

    move-result v6

    goto/32 :goto_3c

    nop

    :goto_3a
    return v5

    :sswitch_3
    goto/32 :goto_19

    nop

    :goto_3b
    sget-object v0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->TAG:Ljava/lang/String;

    goto/32 :goto_6d

    nop

    :goto_3c
    if-nez v6, :cond_6

    goto/32 :goto_73

    :cond_6
    goto/32 :goto_72

    nop

    :goto_3d
    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    goto/32 :goto_5a

    nop

    :goto_3e
    return v5

    :goto_3f
    goto/32 :goto_52

    nop

    :goto_40
    if-nez v6, :cond_7

    goto/32 :goto_63

    :cond_7
    goto/32 :goto_49

    nop

    :goto_41
    if-nez v6, :cond_8

    goto/32 :goto_63

    :cond_8
    goto/32 :goto_4a

    nop

    :goto_42
    goto/16 :goto_30

    :catch_0
    move-exception v0

    goto/32 :goto_2f

    nop

    :goto_43
    if-eqz v6, :cond_9

    goto/32 :goto_2

    :cond_9
    goto/32 :goto_5d

    nop

    :goto_44
    const-string v7, "android.intent.action.CAMERA_BUTTON"

    goto/32 :goto_24

    nop

    :goto_45
    new-instance v4, Landroid/content/Intent;

    goto/32 :goto_60

    nop

    :goto_46
    invoke-virtual {v3, v2}, Landroid/view/KeyEvent$DispatcherState;->isTracking(Landroid/view/KeyEvent;)Z

    move-result v6

    goto/32 :goto_59

    nop

    :goto_47
    const/4 v8, 0x2

    goto/32 :goto_6e

    nop

    :goto_48
    const/4 v11, 0x0

    goto/32 :goto_4b

    nop

    :goto_49
    invoke-virtual {v3, v2}, Landroid/view/KeyEvent$DispatcherState;->isTracking(Landroid/view/KeyEvent;)Z

    move-result v6

    goto/32 :goto_41

    nop

    :goto_4a
    invoke-virtual {v3, v2}, Landroid/view/KeyEvent$DispatcherState;->performedLongPress(Landroid/view/KeyEvent;)V

    goto/32 :goto_f

    nop

    :goto_4b
    const/4 v12, 0x0

    goto/32 :goto_3

    nop

    :goto_4c
    if-nez v6, :cond_a

    goto/32 :goto_3f

    :cond_a
    goto/32 :goto_55

    nop

    :goto_4d
    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto/32 :goto_36

    nop

    :goto_4e
    const-string v0, "android.intent.extra.KEY_EVENT"

    goto/32 :goto_38

    nop

    :goto_4f
    iget-object v6, v1, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mView:Landroid/view/View;

    goto/32 :goto_77

    nop

    :goto_50
    sget-object v0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->TAG:Ljava/lang/String;

    goto/32 :goto_53

    nop

    :goto_51
    const/4 v15, 0x0

    goto/32 :goto_0

    nop

    :goto_52
    return v4

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_4
        0x18 -> :sswitch_0
        0x19 -> :sswitch_0
        0x1b -> :sswitch_3
        0x4f -> :sswitch_2
        0x54 -> :sswitch_1
        0x55 -> :sswitch_2
        0x56 -> :sswitch_2
        0x57 -> :sswitch_2
        0x58 -> :sswitch_2
        0x59 -> :sswitch_2
        0x5a -> :sswitch_2
        0x5b -> :sswitch_2
        0x7e -> :sswitch_2
        0x7f -> :sswitch_2
        0x82 -> :sswitch_2
        0xa4 -> :sswitch_0
        0xde -> :sswitch_2
    .end sparse-switch

    :goto_53
    const-string v4, "Not starting call activity because user setup is in progress."

    goto/32 :goto_16

    nop

    :goto_54
    iget-object v0, v1, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mView:Landroid/view/View;

    goto/32 :goto_1a

    nop

    :goto_55
    invoke-virtual {v3, v2}, Landroid/view/KeyEvent$DispatcherState;->isTracking(Landroid/view/KeyEvent;)Z

    move-result v6

    goto/32 :goto_56

    nop

    :goto_56
    if-nez v6, :cond_b

    goto/32 :goto_3f

    :cond_b
    goto/32 :goto_1f

    nop

    :goto_57
    iget-object v8, v1, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mContext:Landroid/content/Context;

    goto/32 :goto_a

    nop

    :goto_58
    invoke-virtual {v3, v2, v1}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    goto/32 :goto_2c

    nop

    :goto_59
    if-nez v6, :cond_c

    goto/32 :goto_17

    :cond_c
    goto/32 :goto_76

    nop

    :goto_5a
    iget v7, v6, Landroid/content/res/Configuration;->keyboard:I

    goto/32 :goto_10

    nop

    :goto_5b
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v6

    goto/32 :goto_6b

    nop

    :goto_5c
    invoke-virtual {v4, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    :try_start_1
    iget-object v0, v1, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/32 :goto_42

    nop

    :goto_5d
    invoke-virtual {v3, v2, v1}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    goto/32 :goto_1

    nop

    :goto_5e
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v6

    goto/32 :goto_40

    nop

    :goto_5f
    invoke-virtual/range {v8 .. v16}, Landroid/content/Context;->sendOrderedBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    goto/32 :goto_66

    nop

    :goto_60
    const-string v6, "android.intent.action.VOICE_COMMAND"

    goto/32 :goto_68

    nop

    :goto_61
    move-object/from16 v2, p2

    goto/32 :goto_54

    nop

    :goto_62
    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_63
    goto/32 :goto_5

    nop

    :goto_64
    const/4 v6, 0x0

    goto/32 :goto_44

    nop

    :goto_65
    return v5

    :sswitch_4
    goto/32 :goto_39

    nop

    :goto_66
    goto :goto_63

    :goto_67
    goto/32 :goto_3b

    nop

    :goto_68
    invoke-direct {v4, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto/32 :goto_5c

    nop

    :goto_69
    invoke-virtual {v3, v2, v1}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    goto/32 :goto_70

    nop

    :goto_6a
    move-object/from16 v1, p0

    goto/32 :goto_61

    nop

    :goto_6b
    if-eqz v6, :cond_d

    goto/32 :goto_71

    :cond_d
    goto/32 :goto_69

    nop

    :goto_6c
    invoke-direct {v1, v2}, Lcom/android/internal/policy/PhoneFallbackEventHandler;->handleVolumeKeyEvent(Landroid/view/KeyEvent;)V

    goto/32 :goto_65

    nop

    :goto_6d
    const-string v4, "Not dispatching CAMERA long press because user setup is in progress."

    goto/32 :goto_62

    nop

    :goto_6e
    if-eq v7, v8, :cond_e

    goto/32 :goto_3f

    :cond_e
    :goto_6f
    goto/32 :goto_4

    nop

    :goto_70
    goto/16 :goto_3f

    :goto_71
    goto/32 :goto_29

    nop

    :goto_72
    goto/16 :goto_3f

    :goto_73
    goto/32 :goto_7a

    nop

    :goto_74
    if-nez v6, :cond_f

    goto/32 :goto_7

    :cond_f
    goto/32 :goto_6

    nop

    :goto_75
    invoke-direct {v1, v2}, Lcom/android/internal/policy/PhoneFallbackEventHandler;->handleMediaKeyEvent(Landroid/view/KeyEvent;)V

    goto/32 :goto_3a

    nop

    :goto_76
    invoke-virtual {v3, v2}, Landroid/view/KeyEvent$DispatcherState;->performedLongPress(Landroid/view/KeyEvent;)V

    goto/32 :goto_37

    nop

    :goto_77
    invoke-virtual {v6, v4}, Landroid/view/View;->performHapticFeedback(I)Z

    goto/32 :goto_45

    nop

    :goto_78
    if-nez v6, :cond_10

    goto/32 :goto_17

    :cond_10
    goto/32 :goto_46

    nop

    :goto_79
    return v5

    :catch_1
    move-exception v0

    goto/32 :goto_1c

    nop

    :goto_7a
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v6

    goto/32 :goto_43

    nop
.end method

.method onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4

    goto/32 :goto_15

    nop

    :goto_0
    goto/16 :goto_1e

    :goto_1
    goto/32 :goto_1c

    nop

    :goto_2
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v2

    goto/32 :goto_13

    nop

    :goto_3
    if-nez v2, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_0

    nop

    :goto_4
    const-string v3, "Not starting call activity because user setup is in progress."

    goto/32 :goto_1a

    nop

    :goto_5
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    :goto_6
    goto/32 :goto_b

    nop

    :goto_7
    goto/16 :goto_1e

    :goto_8
    goto/32 :goto_2a

    nop

    :goto_9
    invoke-direct {p0, p2}, Lcom/android/internal/policy/PhoneFallbackEventHandler;->handleMediaKeyEvent(Landroid/view/KeyEvent;)V

    goto/32 :goto_18

    nop

    :goto_a
    if-nez v2, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_5

    nop

    :goto_b
    return v1

    :sswitch_0
    goto/32 :goto_2

    nop

    :goto_c
    if-nez v2, :cond_2

    goto/32 :goto_1b

    :cond_2
    goto/32 :goto_f

    nop

    :goto_d
    invoke-direct {p0, v0}, Lcom/android/internal/policy/PhoneFallbackEventHandler;->isNotInstantAppAndKeyguardRestricted(Landroid/view/KeyEvent$DispatcherState;)Z

    move-result v2

    goto/32 :goto_14

    nop

    :goto_e
    invoke-direct {p0, v0}, Lcom/android/internal/policy/PhoneFallbackEventHandler;->isNotInstantAppAndKeyguardRestricted(Landroid/view/KeyEvent$DispatcherState;)Z

    move-result v2

    goto/32 :goto_3

    nop

    :goto_f
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v2

    goto/32 :goto_2b

    nop

    :goto_10
    const/4 v1, 0x0

    goto/32 :goto_12

    nop

    :goto_11
    sget-object v2, Lcom/android/internal/policy/PhoneFallbackEventHandler;->TAG:Ljava/lang/String;

    goto/32 :goto_4

    nop

    :goto_12
    return v1

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_3
        0x18 -> :sswitch_0
        0x19 -> :sswitch_0
        0x1b -> :sswitch_1
        0x4f -> :sswitch_2
        0x55 -> :sswitch_2
        0x56 -> :sswitch_2
        0x57 -> :sswitch_2
        0x58 -> :sswitch_2
        0x59 -> :sswitch_2
        0x5a -> :sswitch_2
        0x5b -> :sswitch_2
        0x7e -> :sswitch_2
        0x7f -> :sswitch_2
        0x82 -> :sswitch_2
        0xa4 -> :sswitch_0
        0xde -> :sswitch_2
    .end sparse-switch

    :goto_13
    if-eqz v2, :cond_3

    goto/32 :goto_17

    :cond_3
    goto/32 :goto_16

    nop

    :goto_14
    if-nez v2, :cond_4

    goto/32 :goto_8

    :cond_4
    goto/32 :goto_7

    nop

    :goto_15
    iget-object v0, p0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mView:Landroid/view/View;

    goto/32 :goto_19

    nop

    :goto_16
    invoke-direct {p0, p2}, Lcom/android/internal/policy/PhoneFallbackEventHandler;->handleVolumeKeyEvent(Landroid/view/KeyEvent;)V

    :goto_17
    goto/32 :goto_26

    nop

    :goto_18
    return v1

    :sswitch_1
    goto/32 :goto_e

    nop

    :goto_19
    invoke-virtual {v0}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v0

    goto/32 :goto_25

    nop

    :goto_1a
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1b
    goto/32 :goto_1d

    nop

    :goto_1c
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    move-result v2

    goto/32 :goto_a

    nop

    :goto_1d
    return v1

    :goto_1e
    goto/32 :goto_10

    nop

    :goto_1f
    goto :goto_1b

    :goto_20
    goto/32 :goto_11

    nop

    :goto_21
    invoke-virtual {p0}, Lcom/android/internal/policy/PhoneFallbackEventHandler;->startCallActivity()V

    goto/32 :goto_1f

    nop

    :goto_22
    const/4 v1, 0x1

    sparse-switch p1, :sswitch_data_0

    goto/32 :goto_24

    nop

    :goto_23
    if-nez v2, :cond_5

    goto/32 :goto_20

    :cond_5
    goto/32 :goto_21

    nop

    :goto_24
    goto :goto_1e

    :sswitch_2
    goto/32 :goto_9

    nop

    :goto_25
    if-nez v0, :cond_6

    goto/32 :goto_29

    :cond_6
    goto/32 :goto_28

    nop

    :goto_26
    return v1

    :sswitch_3
    goto/32 :goto_d

    nop

    :goto_27
    invoke-direct {p0}, Lcom/android/internal/policy/PhoneFallbackEventHandler;->isUserSetupComplete()Z

    move-result v2

    goto/32 :goto_23

    nop

    :goto_28
    invoke-virtual {v0, p2}, Landroid/view/KeyEvent$DispatcherState;->handleUpEvent(Landroid/view/KeyEvent;)V

    :goto_29
    goto/32 :goto_22

    nop

    :goto_2a
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    move-result v2

    goto/32 :goto_c

    nop

    :goto_2b
    if-eqz v2, :cond_7

    goto/32 :goto_1b

    :cond_7
    goto/32 :goto_27

    nop
.end method

.method public preDispatchKeyEvent(Landroid/view/KeyEvent;)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/internal/policy/PhoneFallbackEventHandler;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v0

    const/high16 v1, -0x80000000

    invoke-virtual {v0, p1, v1}, Landroid/media/AudioManager;->preDispatchKeyEvent(Landroid/view/KeyEvent;I)V

    return-void
.end method

.method public setView(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mView:Landroid/view/View;

    return-void
.end method

.method startCallActivity()V
    .locals 4

    goto/32 :goto_6

    nop

    :goto_0
    const-string v1, "android.intent.action.CALL_BUTTON"

    goto/32 :goto_9

    nop

    :goto_1
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    goto/32 :goto_8

    nop

    :goto_3
    const/high16 v1, 0x10000000

    goto/32 :goto_4

    nop

    :goto_4
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    :try_start_0
    iget-object v1, p0, Lcom/android/internal/policy/PhoneFallbackEventHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_a

    nop

    :goto_5
    sget-object v2, Lcom/android/internal/policy/PhoneFallbackEventHandler;->TAG:Ljava/lang/String;

    goto/32 :goto_7

    nop

    :goto_6
    new-instance v0, Landroid/content/Intent;

    goto/32 :goto_0

    nop

    :goto_7
    const-string v3, "No activity found for android.intent.action.CALL_BUTTON."

    goto/32 :goto_1

    nop

    :goto_8
    return-void

    :goto_9
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto/32 :goto_3

    nop

    :goto_a
    goto :goto_2

    :catch_0
    move-exception v1

    goto/32 :goto_5

    nop
.end method
