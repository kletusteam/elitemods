.class public Lcom/android/internal/org/bouncycastle/asn1/DERTaggedObject;
.super Lcom/android/internal/org/bouncycastle/asn1/ASN1TaggedObject;


# direct methods
.method public constructor <init>(ILcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1TaggedObject;-><init>(ZILcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    return-void
.end method

.method public constructor <init>(ZILcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1TaggedObject;-><init>(ZILcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    return-void
.end method


# virtual methods
.method encode(Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_11

    nop

    :goto_0
    const/16 v1, 0x80

    goto/32 :goto_9

    nop

    :goto_1
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->isConstructed()Z

    move-result v2

    goto/32 :goto_2

    nop

    :goto_2
    if-nez v2, :cond_0

    goto/32 :goto_f

    :cond_0
    :goto_3
    goto/32 :goto_e

    nop

    :goto_4
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->toDERObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_5
    invoke-virtual {p1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->getDERSubStream()Lcom/android/internal/org/bouncycastle/asn1/DEROutputStream;

    move-result-object v2

    goto/32 :goto_14

    nop

    :goto_6
    if-nez v2, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_15

    nop

    :goto_7
    invoke-virtual {p1, v2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeLength(I)V

    :goto_8
    goto/32 :goto_5

    nop

    :goto_9
    iget-boolean v2, p0, Lcom/android/internal/org/bouncycastle/asn1/DERTaggedObject;->explicit:Z

    goto/32 :goto_a

    nop

    :goto_a
    if-eqz v2, :cond_2

    goto/32 :goto_3

    :cond_2
    goto/32 :goto_1

    nop

    :goto_b
    invoke-virtual {v0, v2, v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->encode(Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;Z)V

    goto/32 :goto_10

    nop

    :goto_c
    invoke-virtual {p1, p2, v1, v2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeTag(ZII)V

    goto/32 :goto_12

    nop

    :goto_d
    iget v2, p0, Lcom/android/internal/org/bouncycastle/asn1/DERTaggedObject;->tagNo:I

    goto/32 :goto_c

    nop

    :goto_e
    or-int/lit8 v1, v1, 0x20

    :goto_f
    goto/32 :goto_d

    nop

    :goto_10
    return-void

    :goto_11
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERTaggedObject;->obj:Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_13

    nop

    :goto_12
    iget-boolean v2, p0, Lcom/android/internal/org/bouncycastle/asn1/DERTaggedObject;->explicit:Z

    goto/32 :goto_6

    nop

    :goto_13
    invoke-interface {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_14
    iget-boolean v3, p0, Lcom/android/internal/org/bouncycastle/asn1/DERTaggedObject;->explicit:Z

    goto/32 :goto_b

    nop

    :goto_15
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->encodedLength()I

    move-result v2

    goto/32 :goto_7

    nop
.end method

.method encodedLength()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_10

    nop

    :goto_0
    invoke-static {v2}, Lcom/android/internal/org/bouncycastle/asn1/StreamUtil;->calculateTagLength(I)I

    move-result v2

    goto/32 :goto_1

    nop

    :goto_1
    invoke-static {v1}, Lcom/android/internal/org/bouncycastle/asn1/StreamUtil;->calculateBodyLength(I)I

    move-result v3

    goto/32 :goto_b

    nop

    :goto_2
    iget-boolean v2, p0, Lcom/android/internal/org/bouncycastle/asn1/DERTaggedObject;->explicit:Z

    goto/32 :goto_8

    nop

    :goto_3
    add-int/2addr v2, v1

    goto/32 :goto_6

    nop

    :goto_4
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->encodedLength()I

    move-result v1

    goto/32 :goto_2

    nop

    :goto_5
    add-int/lit8 v1, v1, -0x1

    goto/32 :goto_7

    nop

    :goto_6
    return v2

    :goto_7
    iget v2, p0, Lcom/android/internal/org/bouncycastle/asn1/DERTaggedObject;->tagNo:I

    goto/32 :goto_d

    nop

    :goto_8
    if-nez v2, :cond_0

    goto/32 :goto_f

    :cond_0
    goto/32 :goto_9

    nop

    :goto_9
    iget v2, p0, Lcom/android/internal/org/bouncycastle/asn1/DERTaggedObject;->tagNo:I

    goto/32 :goto_0

    nop

    :goto_a
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->toDERObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_b
    add-int/2addr v2, v3

    goto/32 :goto_11

    nop

    :goto_c
    invoke-interface {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    goto/32 :goto_a

    nop

    :goto_d
    invoke-static {v2}, Lcom/android/internal/org/bouncycastle/asn1/StreamUtil;->calculateTagLength(I)I

    move-result v2

    goto/32 :goto_3

    nop

    :goto_e
    return v2

    :goto_f
    goto/32 :goto_5

    nop

    :goto_10
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERTaggedObject;->obj:Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_c

    nop

    :goto_11
    add-int/2addr v2, v1

    goto/32 :goto_e

    nop
.end method

.method isConstructed()Z
    .locals 1

    goto/32 :goto_c

    nop

    :goto_0
    if-eqz v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_a

    nop

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_2

    nop

    :goto_2
    goto :goto_e

    :goto_3
    goto/32 :goto_d

    nop

    :goto_4
    invoke-interface {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_5
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->isConstructed()Z

    move-result v0

    goto/32 :goto_b

    nop

    :goto_6
    return v0

    :goto_7
    goto :goto_3

    :goto_8
    goto/32 :goto_1

    nop

    :goto_9
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->toDERObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_a
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERTaggedObject;->obj:Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_4

    nop

    :goto_b
    if-nez v0, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_7

    nop

    :goto_c
    iget-boolean v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERTaggedObject;->explicit:Z

    goto/32 :goto_0

    nop

    :goto_d
    const/4 v0, 0x1

    :goto_e
    goto/32 :goto_6

    nop
.end method

.method toDERObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    return-object p0
.end method

.method toDLObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    return-object p0
.end method
