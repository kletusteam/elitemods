.class public interface abstract Lcom/android/internal/org/bouncycastle/crypto/DSA;
.super Ljava/lang/Object;


# virtual methods
.method public abstract generateSignature([B)[Ljava/math/BigInteger;
.end method

.method public abstract init(ZLcom/android/internal/org/bouncycastle/crypto/CipherParameters;)V
.end method

.method public abstract verifySignature([BLjava/math/BigInteger;Ljava/math/BigInteger;)Z
.end method
