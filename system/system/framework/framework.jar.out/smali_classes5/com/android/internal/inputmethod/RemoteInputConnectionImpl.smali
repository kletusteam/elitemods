.class public final Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;
.super Lcom/android/internal/view/IInputContext$Stub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$Dispatching;,
        Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$KnownAlwaysTrueEndBatchEditCache;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final MAX_END_BATCH_EDIT_RETRY:I = 0x10

.field private static final TAG:Ljava/lang/String; = "RemoteInputConnectionImpl"


# instance fields
.field private final mAccessibilityInputConnection:Lcom/android/internal/inputmethod/IRemoteAccessibilityInputConnection;

.field private final mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mFinished:Z

.field private final mH:Landroid/os/Handler;

.field private final mHasPendingInvalidation:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mInputConnection:Landroid/view/inputmethod/InputConnection;

.field private final mLock:Ljava/lang/Object;

.field private final mLooper:Landroid/os/Looper;

.field private final mParentInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

.field private final mServedView:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetmCurrentSessionId(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 0

    iget-object p0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mdispatchWithTracing(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Ljava/lang/String;Lcom/android/internal/infra/AndroidFuture;Ljava/util/function/Supplier;Ljava/util/function/Function;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Lcom/android/internal/infra/AndroidFuture;Ljava/util/function/Supplier;Ljava/util/function/Function;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdispatchWithTracing(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$smuseImeTracing()Z
    .locals 1

    invoke-static {}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->useImeTracing()Z

    move-result v0

    return v0
.end method

.method public constructor <init>(Landroid/os/Looper;Landroid/view/inputmethod/InputConnection;Landroid/view/inputmethod/InputMethodManager;Landroid/view/View;)V
    .locals 2

    invoke-direct {p0}, Lcom/android/internal/view/IInputContext$Stub;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mLock:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mFinished:Z

    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mHasPendingInvalidation:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;

    invoke-direct {v0, p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;)V

    iput-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mAccessibilityInputConnection:Lcom/android/internal/inputmethod/IRemoteAccessibilityInputConnection;

    iput-object p2, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mInputConnection:Landroid/view/inputmethod/InputConnection;

    iput-object p1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mLooper:Landroid/os/Looper;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mH:Landroid/os/Handler;

    iput-object p3, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mParentInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mServedView:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method private dispatch(Ljava/lang/Runnable;)V
    .locals 1

    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mLooper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->isCurrentThread()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mH:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private dispatchWithTracing(Ljava/lang/String;Lcom/android/internal/infra/AndroidFuture;Ljava/util/function/Supplier;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Lcom/android/internal/infra/AndroidFuture;",
            "Ljava/util/function/Supplier<",
            "TT;>;)V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Lcom/android/internal/infra/AndroidFuture;Ljava/util/function/Supplier;Ljava/util/function/Function;)V

    return-void
.end method

.method private dispatchWithTracing(Ljava/lang/String;Lcom/android/internal/infra/AndroidFuture;Ljava/util/function/Supplier;Ljava/util/function/Function;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Lcom/android/internal/infra/AndroidFuture;",
            "Ljava/util/function/Supplier<",
            "TT;>;",
            "Ljava/util/function/Function<",
            "TT;[B>;)V"
        }
    .end annotation

    move-object v3, p2

    new-instance v6, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda2;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p3

    move-object v4, p4

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda2;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Ljava/util/function/Supplier;Lcom/android/internal/infra/AndroidFuture;Ljava/util/function/Function;Ljava/lang/String;)V

    invoke-direct {p0, p1, v6}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method private dispatchWithTracing(Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 2

    const-wide/16 v0, 0x4

    invoke-static {v0, v1}, Landroid/os/Trace;->isTagEnabled(J)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda29;

    invoke-direct {v0, p1, p2}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda29;-><init>(Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_0
    move-object v0, p2

    :goto_0
    invoke-direct {p0, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatch(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$deactivate$1(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p0}, Landroid/view/View;->getViewRootImpl()Landroid/view/ViewRootImpl;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/ViewRootImpl;->getHandwritingInitiator()Landroid/view/HandwritingInitiator;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/HandwritingInitiator;->onInputConnectionClosed(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method static synthetic lambda$dispatchWithTracing$42(Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InputConnection#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-wide/16 v1, 0x4

    invoke-static {v1, v2, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    :try_start_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    nop

    return-void

    :catchall_0
    move-exception v0

    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    throw v0
.end method

.method static synthetic lambda$getCursorCapsMode$13(ILjava/lang/Integer;)[B
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p0, v0}, Lcom/android/internal/inputmethod/InputConnectionProtoDumper;->buildGetCursorCapsModeProto(II)[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$getExtractedText$15(Landroid/view/inputmethod/ExtractedTextRequest;ILandroid/view/inputmethod/ExtractedText;)[B
    .locals 1

    invoke-static {p0, p1, p2}, Lcom/android/internal/inputmethod/InputConnectionProtoDumper;->buildGetExtractedTextProto(Landroid/view/inputmethod/ExtractedTextRequest;ILandroid/view/inputmethod/ExtractedText;)[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$getSelectedText$9(ILjava/lang/CharSequence;)[B
    .locals 1

    invoke-static {p0, p1}, Lcom/android/internal/inputmethod/InputConnectionProtoDumper;->buildGetSelectedTextProto(ILjava/lang/CharSequence;)[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$getSurroundingText$11(IIILandroid/view/inputmethod/SurroundingText;)[B
    .locals 1

    invoke-static {p0, p1, p2, p3}, Lcom/android/internal/inputmethod/InputConnectionProtoDumper;->buildGetSurroundingTextProto(IIILandroid/view/inputmethod/SurroundingText;)[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$getTextAfterCursor$5(IILjava/lang/CharSequence;)[B
    .locals 1

    invoke-static {p0, p1, p2}, Lcom/android/internal/inputmethod/InputConnectionProtoDumper;->buildGetTextAfterCursorProto(IILjava/lang/CharSequence;)[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$getTextBeforeCursor$7(IILjava/lang/CharSequence;)[B
    .locals 1

    invoke-static {p0, p1, p2}, Lcom/android/internal/inputmethod/InputConnectionProtoDumper;->buildGetTextBeforeCursorProto(IILjava/lang/CharSequence;)[B

    move-result-object v0

    return-object v0
.end method

.method private requestCursorUpdatesInternal(III)Z
    .locals 4

    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mParentInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodManager;->getDisplayId()I

    move-result v2

    if-eq v2, p3, :cond_1

    iget-object v2, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mParentInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodManager;->hasVirtualDisplayToScreenMatrix()Z

    move-result v2

    if-nez v2, :cond_1

    return v1

    :cond_1
    :try_start_0
    invoke-interface {v0, p1, p2}, Landroid/view/inputmethod/InputConnection;->requestCursorUpdates(II)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/AbstractMethodError; {:try_start_0 .. :try_end_0} :catch_0

    return v1

    :catch_0
    move-exception v2

    return v1

    :cond_2
    :goto_0
    const-string v2, "RemoteInputConnectionImpl"

    const-string/jumbo v3, "requestCursorAnchorInfo on inactive InputConnection"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return v1
.end method

.method private static useImeTracing()Z
    .locals 1

    invoke-static {}, Lcom/android/internal/inputmethod/ImeTracing;->getInstance()Lcom/android/internal/inputmethod/ImeTracing;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/inputmethod/ImeTracing;->isEnabled()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public asIRemoteAccessibilityInputConnection()Lcom/android/internal/inputmethod/IRemoteAccessibilityInputConnection;
    .locals 1

    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mAccessibilityInputConnection:Lcom/android/internal/inputmethod/IRemoteAccessibilityInputConnection;

    return-object v0
.end method

.method public beginBatchEdit(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;)V
    .locals 2

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda19;

    invoke-direct {v0, p0, p1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda19;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;)V

    const-string v1, "beginBatchEdit"

    invoke-direct {p0, v1, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public clearMetaKeyStates(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;I)V
    .locals 2

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda22;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda22;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;I)V

    const-string v1, "clearMetaKeyStates"

    invoke-direct {p0, v1, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public commitCompletion(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Landroid/view/inputmethod/CompletionInfo;)V
    .locals 2

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda15;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda15;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Landroid/view/inputmethod/CompletionInfo;)V

    const-string v1, "commitCompletion"

    invoke-direct {p0, v1, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public commitContent(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Landroid/view/inputmethod/InputContentInfo;ILandroid/os/Bundle;Lcom/android/internal/infra/AndroidFuture;)V
    .locals 7

    new-instance v6, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda10;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda10;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Landroid/view/inputmethod/InputContentInfo;ILandroid/os/Bundle;)V

    const-string v0, "commitContent"

    invoke-direct {p0, v0, p5, v6}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Lcom/android/internal/infra/AndroidFuture;Ljava/util/function/Supplier;)V

    return-void
.end method

.method public commitCorrection(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Landroid/view/inputmethod/CorrectionInfo;)V
    .locals 2

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda36;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda36;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Landroid/view/inputmethod/CorrectionInfo;)V

    const-string v1, "commitCorrection"

    invoke-direct {p0, v1, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public commitText(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Ljava/lang/CharSequence;I)V
    .locals 2

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda35;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda35;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Ljava/lang/CharSequence;I)V

    const-string v1, "commitText"

    invoke-direct {p0, v1, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public commitTextForSynergy(Ljava/lang/String;I)V
    .locals 2

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda8;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda8;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Ljava/lang/String;I)V

    const-string v1, "commitTextForSynergy"

    invoke-direct {p0, v1, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public commitTextWithTextAttribute(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Ljava/lang/CharSequence;ILandroid/view/inputmethod/TextAttribute;)V
    .locals 7

    new-instance v6, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda17;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda17;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Ljava/lang/CharSequence;ILandroid/view/inputmethod/TextAttribute;)V

    const-string v0, "commitTextWithTextAttribute"

    invoke-direct {p0, v0, v6}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public deactivate()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda26;

    invoke-direct {v0, p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda26;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;)V

    invoke-direct {p0, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatch(Ljava/lang/Runnable;)V

    return-void
.end method

.method public deleteSurroundingText(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;II)V
    .locals 2

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda24;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda24;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;II)V

    const-string v1, "deleteSurroundingText"

    invoke-direct {p0, v1, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public deleteSurroundingTextInCodePoints(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;II)V
    .locals 2

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda33;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda33;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;II)V

    const-string v1, "deleteSurroundingTextInCodePoints"

    invoke-direct {p0, v1, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public dispatchReportFullscreenMode(Z)V
    .locals 1

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda42;

    invoke-direct {v0, p0, p1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda42;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Z)V

    invoke-direct {p0, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatch(Ljava/lang/Runnable;)V

    return-void
.end method

.method public dumpDebug(Landroid/util/proto/ProtoOutputStream;J)V
    .locals 2

    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mInputConnection:Landroid/view/inputmethod/InputConnection;

    instance-of v1, v1, Landroid/view/inputmethod/DumpableInputConnection;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mLooper:Landroid/os/Looper;

    invoke-virtual {v1}, Landroid/os/Looper;->isCurrentThread()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mInputConnection:Landroid/view/inputmethod/InputConnection;

    check-cast v1, Landroid/view/inputmethod/DumpableInputConnection;

    invoke-interface {v1, p1, p2, p3}, Landroid/view/inputmethod/DumpableInputConnection;->dumpDebug(Landroid/util/proto/ProtoOutputStream;J)V

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public endBatchEdit(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;)V
    .locals 2

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda32;

    invoke-direct {v0, p0, p1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda32;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;)V

    const-string v1, "endBatchEdit"

    invoke-direct {p0, v1, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public finishComposingText(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;)V
    .locals 2

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda45;

    invoke-direct {v0, p0, p1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda45;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;)V

    const-string v1, "finishComposingText"

    invoke-direct {p0, v1, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public finishComposingTextFromImm()V
    .locals 3

    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    new-instance v1, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda3;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;I)V

    const-string v2, "finishComposingTextFromImm"

    invoke-direct {p0, v2, v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public getCursorCapsMode(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;ILcom/android/internal/infra/AndroidFuture;)V
    .locals 3

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda4;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda4;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;I)V

    invoke-static {}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->useImeTracing()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda5;

    invoke-direct {v1, p2}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda5;-><init>(I)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v2, "getCursorCapsMode"

    invoke-direct {p0, v2, p3, v0, v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Lcom/android/internal/infra/AndroidFuture;Ljava/util/function/Supplier;Ljava/util/function/Function;)V

    return-void
.end method

.method public getExtractedText(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Landroid/view/inputmethod/ExtractedTextRequest;ILcom/android/internal/infra/AndroidFuture;)V
    .locals 3

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda30;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda30;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Landroid/view/inputmethod/ExtractedTextRequest;I)V

    invoke-static {}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->useImeTracing()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda31;

    invoke-direct {v1, p2, p3}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda31;-><init>(Landroid/view/inputmethod/ExtractedTextRequest;I)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v2, "getExtractedText"

    invoke-direct {p0, v2, p4, v0, v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Lcom/android/internal/infra/AndroidFuture;Ljava/util/function/Supplier;Ljava/util/function/Function;)V

    return-void
.end method

.method public getInputConnection()Landroid/view/inputmethod/InputConnection;
    .locals 2

    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mInputConnection:Landroid/view/inputmethod/InputConnection;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getSelectedText(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;ILcom/android/internal/infra/AndroidFuture;)V
    .locals 3

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda13;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda13;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;I)V

    invoke-static {}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->useImeTracing()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda14;

    invoke-direct {v1, p2}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda14;-><init>(I)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v2, "getSelectedText"

    invoke-direct {p0, v2, p3, v0, v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Lcom/android/internal/infra/AndroidFuture;Ljava/util/function/Supplier;Ljava/util/function/Function;)V

    return-void
.end method

.method public getServedView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mServedView:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public getSurroundingText(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;IIILcom/android/internal/infra/AndroidFuture;)V
    .locals 7

    new-instance v6, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda43;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda43;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;III)V

    invoke-static {}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->useImeTracing()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda44;

    invoke-direct {v0, p2, p3, p4}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda44;-><init>(III)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "getSurroundingText"

    invoke-direct {p0, v1, p5, v6, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Lcom/android/internal/infra/AndroidFuture;Ljava/util/function/Supplier;Ljava/util/function/Function;)V

    return-void
.end method

.method public getTextAfterCursor(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;IILcom/android/internal/infra/AndroidFuture;)V
    .locals 3

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda38;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda38;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;II)V

    invoke-static {}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->useImeTracing()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda39;

    invoke-direct {v1, p2, p3}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda39;-><init>(II)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v2, "getTextAfterCursor"

    invoke-direct {p0, v2, p4, v0, v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Lcom/android/internal/infra/AndroidFuture;Ljava/util/function/Supplier;Ljava/util/function/Function;)V

    return-void
.end method

.method public getTextBeforeCursor(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;IILcom/android/internal/infra/AndroidFuture;)V
    .locals 3

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda27;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda27;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;II)V

    invoke-static {}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->useImeTracing()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda28;

    invoke-direct {v1, p2, p3}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda28;-><init>(II)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v2, "getTextBeforeCursor"

    invoke-direct {p0, v2, p4, v0, v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Lcom/android/internal/infra/AndroidFuture;Ljava/util/function/Supplier;Ljava/util/function/Function;)V

    return-void
.end method

.method public hasPendingInvalidation()Z
    .locals 1

    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mHasPendingInvalidation:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public isActive()Z
    .locals 1

    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mParentInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isActive()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isFinished()Z
    .locals 2

    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mFinished:Z

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method synthetic lambda$beginBatchEdit$33$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;)V
    .locals 3

    goto/32 :goto_d

    nop

    :goto_0
    goto :goto_a

    :goto_1
    goto/32 :goto_f

    nop

    :goto_2
    const-string v2, "beginBatchEdit on inactive InputConnection"

    goto/32 :goto_c

    nop

    :goto_3
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_2

    nop

    :goto_4
    if-eqz v1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_0

    nop

    :goto_5
    return-void

    :goto_6
    goto/32 :goto_12

    nop

    :goto_7
    if-nez v0, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_b

    nop

    :goto_8
    return-void

    :goto_9
    return-void

    :goto_a
    goto/32 :goto_3

    nop

    :goto_b
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_4

    nop

    :goto_c
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_8

    nop

    :goto_d
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_11

    nop

    :goto_e
    if-ne v0, v1, :cond_2

    goto/32 :goto_6

    :cond_2
    goto/32 :goto_5

    nop

    :goto_f
    invoke-interface {v0}, Landroid/view/inputmethod/InputConnection;->beginBatchEdit()Z

    goto/32 :goto_9

    nop

    :goto_10
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_e

    nop

    :goto_11
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_10

    nop

    :goto_12
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_7

    nop
.end method

.method synthetic lambda$clearMetaKeyStates$30$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;I)V
    .locals 3

    goto/32 :goto_3

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_11

    nop

    :goto_2
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_d

    nop

    :goto_3
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_1

    nop

    :goto_4
    invoke-interface {v0, p2}, Landroid/view/inputmethod/InputConnection;->clearMetaKeyStates(I)Z

    goto/32 :goto_a

    nop

    :goto_5
    if-ne v0, v1, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_7

    nop

    :goto_6
    if-eqz v1, :cond_1

    goto/32 :goto_f

    :cond_1
    goto/32 :goto_e

    nop

    :goto_7
    return-void

    :goto_8
    goto/32 :goto_2

    nop

    :goto_9
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_c

    nop

    :goto_a
    return-void

    :goto_b
    goto/32 :goto_9

    nop

    :goto_c
    const-string v2, "clearMetaKeyStates on inactive InputConnection"

    goto/32 :goto_10

    nop

    :goto_d
    if-nez v0, :cond_2

    goto/32 :goto_b

    :cond_2
    goto/32 :goto_12

    nop

    :goto_e
    goto :goto_b

    :goto_f
    goto/32 :goto_4

    nop

    :goto_10
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_0

    nop

    :goto_11
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_5

    nop

    :goto_12
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_6

    nop
.end method

.method synthetic lambda$commitCompletion$18$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Landroid/view/inputmethod/CompletionInfo;)V
    .locals 3

    goto/32 :goto_c

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_f

    :cond_0
    goto/32 :goto_2

    nop

    :goto_1
    invoke-interface {v0, p2}, Landroid/view/inputmethod/InputConnection;->commitCompletion(Landroid/view/inputmethod/CompletionInfo;)Z

    goto/32 :goto_e

    nop

    :goto_2
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_3

    nop

    :goto_3
    if-eqz v1, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_4

    nop

    :goto_4
    goto :goto_f

    :goto_5
    goto/32 :goto_1

    nop

    :goto_6
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_9

    nop

    :goto_7
    return-void

    :goto_8
    goto/32 :goto_11

    nop

    :goto_9
    const-string v2, "commitCompletion on inactive InputConnection"

    goto/32 :goto_d

    nop

    :goto_a
    if-ne v0, v1, :cond_2

    goto/32 :goto_8

    :cond_2
    goto/32 :goto_7

    nop

    :goto_b
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_12

    nop

    :goto_c
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_b

    nop

    :goto_d
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_10

    nop

    :goto_e
    return-void

    :goto_f
    goto/32 :goto_6

    nop

    :goto_10
    return-void

    :goto_11
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_12
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_a

    nop
.end method

.method synthetic lambda$commitContent$40$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Landroid/view/inputmethod/InputContentInfo;ILandroid/os/Bundle;)Ljava/lang/Boolean;
    .locals 5

    goto/32 :goto_1c

    nop

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    goto/32 :goto_19

    nop

    :goto_1
    if-ne v0, v1, :cond_0

    goto/32 :goto_13

    :cond_0
    goto/32 :goto_12

    nop

    :goto_2
    if-nez v0, :cond_1

    goto/32 :goto_f

    :cond_1
    goto/32 :goto_15

    nop

    :goto_3
    invoke-virtual {p2}, Landroid/view/inputmethod/InputContentInfo;->validate()Z

    move-result v3

    goto/32 :goto_b

    nop

    :goto_4
    const/4 v2, 0x0

    goto/32 :goto_5

    nop

    :goto_5
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/32 :goto_1

    nop

    :goto_6
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_7

    nop

    :goto_7
    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_e

    nop

    :goto_8
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_2

    nop

    :goto_9
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_18

    nop

    :goto_a
    if-eqz v3, :cond_2

    goto/32 :goto_d

    :cond_2
    goto/32 :goto_c

    nop

    :goto_b
    if-eqz v3, :cond_3

    goto/32 :goto_17

    :cond_3
    goto/32 :goto_16

    nop

    :goto_c
    goto :goto_f

    :goto_d
    goto/32 :goto_1f

    nop

    :goto_e
    return-object v2

    :goto_f
    goto/32 :goto_22

    nop

    :goto_10
    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_20

    nop

    :goto_11
    const-string v4, "commitContent with invalid inputContentInfo="

    goto/32 :goto_14

    nop

    :goto_12
    return-object v2

    :goto_13
    goto/32 :goto_1d

    nop

    :goto_14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_21

    nop

    :goto_15
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v3

    goto/32 :goto_a

    nop

    :goto_16
    goto :goto_1b

    :goto_17
    :try_start_0
    invoke-interface {v0, p2, p3, p4}, Landroid/view/inputmethod/InputConnection;->commitContent(Landroid/view/inputmethod/InputContentInfo;ILandroid/os/Bundle;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/AbstractMethodError; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_1e

    nop

    :goto_18
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_4

    nop

    :goto_19
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_11

    nop

    :goto_1a
    return-object v2

    :goto_1b
    goto/32 :goto_0

    nop

    :goto_1c
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_9

    nop

    :goto_1d
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_1e
    return-object v1

    :catch_0
    move-exception v1

    goto/32 :goto_1a

    nop

    :goto_1f
    if-nez p2, :cond_4

    goto/32 :goto_1b

    :cond_4
    goto/32 :goto_3

    nop

    :goto_20
    return-object v2

    :goto_21
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_6

    nop

    :goto_22
    const-string v3, "commitContent on inactive InputConnection"

    goto/32 :goto_10

    nop
.end method

.method synthetic lambda$commitCorrection$19$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Landroid/view/inputmethod/CorrectionInfo;)V
    .locals 3

    goto/32 :goto_8

    nop

    :goto_0
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_3

    nop

    :goto_1
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_13

    nop

    :goto_2
    if-ne v0, v1, :cond_0

    goto/32 :goto_10

    :cond_0
    goto/32 :goto_f

    nop

    :goto_3
    if-eqz v1, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_4

    nop

    :goto_4
    goto :goto_a

    :goto_5
    :try_start_0
    invoke-interface {v0, p2}, Landroid/view/inputmethod/InputConnection;->commitCorrection(Landroid/view/inputmethod/CorrectionInfo;)Z
    :try_end_0
    .catch Ljava/lang/AbstractMethodError; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_c

    nop

    :goto_6
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_2

    nop

    :goto_7
    const-string v2, "commitCorrection on inactive InputConnection"

    goto/32 :goto_e

    nop

    :goto_8
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_12

    nop

    :goto_9
    return-void

    :goto_a
    goto/32 :goto_b

    nop

    :goto_b
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_7

    nop

    :goto_c
    goto :goto_d

    :catch_0
    move-exception v1

    :goto_d
    goto/32 :goto_9

    nop

    :goto_e
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_11

    nop

    :goto_f
    return-void

    :goto_10
    goto/32 :goto_1

    nop

    :goto_11
    return-void

    :goto_12
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_6

    nop

    :goto_13
    if-nez v0, :cond_2

    goto/32 :goto_a

    :cond_2
    goto/32 :goto_0

    nop
.end method

.method synthetic lambda$commitText$16$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Ljava/lang/CharSequence;I)V
    .locals 3

    goto/32 :goto_8

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_9

    nop

    :goto_1
    if-ne v0, v1, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_d

    nop

    :goto_2
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_6

    nop

    :goto_3
    if-eqz v1, :cond_2

    goto/32 :goto_5

    :cond_2
    goto/32 :goto_4

    nop

    :goto_4
    goto :goto_12

    :goto_5
    goto/32 :goto_a

    nop

    :goto_6
    return-void

    :goto_7
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_8
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_c

    nop

    :goto_9
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_3

    nop

    :goto_a
    invoke-interface {v0, p2, p3}, Landroid/view/inputmethod/InputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    goto/32 :goto_11

    nop

    :goto_b
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_f

    nop

    :goto_c
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_10

    nop

    :goto_d
    return-void

    :goto_e
    goto/32 :goto_7

    nop

    :goto_f
    const-string v2, "commitText on inactive InputConnection"

    goto/32 :goto_2

    nop

    :goto_10
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_1

    nop

    :goto_11
    return-void

    :goto_12
    goto/32 :goto_b

    nop
.end method

.method synthetic lambda$commitTextForSynergy$44$com-android-internal-inputmethod-RemoteInputConnectionImpl(Ljava/lang/String;I)V
    .locals 3

    goto/32 :goto_a

    nop

    :goto_0
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_4

    nop

    :goto_1
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_5

    nop

    :goto_2
    return-void

    :goto_3
    goto/32 :goto_b

    nop

    :goto_4
    return-void

    :goto_5
    if-eqz v1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_6

    nop

    :goto_6
    goto :goto_3

    :goto_7
    goto/32 :goto_8

    nop

    :goto_8
    invoke-interface {v0, p1, p2}, Landroid/view/inputmethod/InputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    goto/32 :goto_2

    nop

    :goto_9
    if-nez v0, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_1

    nop

    :goto_a
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_b
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_c

    nop

    :goto_c
    const-string v2, "commitTextForSynergy on inactive InputConnection"

    goto/32 :goto_0

    nop
.end method

.method synthetic lambda$commitTextWithTextAttribute$17$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Ljava/lang/CharSequence;ILandroid/view/inputmethod/TextAttribute;)V
    .locals 3

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_c

    nop

    :goto_1
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_2
    if-eqz v1, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_9

    nop

    :goto_3
    invoke-interface {v0, p2, p3, p4}, Landroid/view/inputmethod/InputConnection;->commitText(Ljava/lang/CharSequence;ILandroid/view/inputmethod/TextAttribute;)Z

    goto/32 :goto_11

    nop

    :goto_4
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_2

    nop

    :goto_5
    if-ne v0, v1, :cond_1

    goto/32 :goto_f

    :cond_1
    goto/32 :goto_e

    nop

    :goto_6
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_5

    nop

    :goto_7
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_b

    nop

    :goto_8
    if-nez v0, :cond_2

    goto/32 :goto_12

    :cond_2
    goto/32 :goto_4

    nop

    :goto_9
    goto :goto_12

    :goto_a
    goto/32 :goto_3

    nop

    :goto_b
    const-string v2, "commitText on inactive InputConnection"

    goto/32 :goto_d

    nop

    :goto_c
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_6

    nop

    :goto_d
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_10

    nop

    :goto_e
    return-void

    :goto_f
    goto/32 :goto_1

    nop

    :goto_10
    return-void

    :goto_11
    return-void

    :goto_12
    goto/32 :goto_7

    nop
.end method

.method synthetic lambda$deactivate$2$com-android-internal-inputmethod-RemoteInputConnectionImpl()V
    .locals 6

    goto/32 :goto_20

    nop

    :goto_0
    throw v0

    :catchall_0
    move-exception v4

    goto/32 :goto_2d

    nop

    :goto_1
    if-eqz v4, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_a

    nop

    :goto_2
    throw v0

    :goto_3
    :try_start_0
    invoke-interface {v4}, Landroid/view/inputmethod/InputConnection;->closeConnection()V
    :try_end_0
    .catch Ljava/lang/AbstractMethodError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_25

    nop

    :goto_4
    const-string v0, "InputConnection#closeConnection"

    goto/32 :goto_23

    nop

    :goto_5
    return-void

    :catchall_1
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto/32 :goto_2

    nop

    :goto_6
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    goto/32 :goto_5

    nop

    :goto_7
    throw v4

    :catchall_2
    move-exception v0

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto/32 :goto_18

    nop

    :goto_8
    invoke-virtual {v0}, Landroid/view/View;->getViewRootImpl()Landroid/view/ViewRootImpl;

    move-result-object v2

    goto/32 :goto_16

    nop

    :goto_9
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_2f

    nop

    :goto_a
    iget-object v5, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mLock:Ljava/lang/Object;

    goto/32 :goto_11

    nop

    :goto_b
    monitor-enter v5

    :try_start_3
    iput-object v3, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mInputConnection:Landroid/view/inputmethod/InputConnection;

    iput-boolean v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mFinished:Z

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto/32 :goto_28

    nop

    :goto_c
    invoke-virtual {v0}, Landroid/view/View;->getHandler()Landroid/os/Handler;

    move-result-object v1

    goto/32 :goto_27

    nop

    :goto_d
    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_f

    nop

    :goto_e
    invoke-virtual {v0}, Landroid/view/View;->onInputConnectionClosedInternal()V

    goto/32 :goto_8

    nop

    :goto_f
    new-instance v2, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda40;

    goto/32 :goto_33

    nop

    :goto_10
    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    goto/32 :goto_1d

    nop

    :goto_11
    monitor-enter v5

    :try_start_4
    iput-object v3, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mInputConnection:Landroid/view/inputmethod/InputConnection;

    iput-boolean v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mFinished:Z

    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/32 :goto_6

    nop

    :goto_12
    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mServedView:Ljava/lang/ref/WeakReference;

    goto/32 :goto_9

    nop

    :goto_13
    new-instance v2, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda41;

    goto/32 :goto_34

    nop

    :goto_14
    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_15
    goto/32 :goto_22

    nop

    :goto_16
    if-nez v2, :cond_1

    goto/32 :goto_31

    :cond_1
    goto/32 :goto_2e

    nop

    :goto_17
    monitor-enter v4

    :try_start_5
    iput-object v3, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mInputConnection:Landroid/view/inputmethod/InputConnection;

    iput-boolean v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mFinished:Z

    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    goto/32 :goto_19

    nop

    :goto_18
    throw v0

    :goto_19
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    nop

    goto/32 :goto_12

    nop

    :goto_1a
    const/4 v0, 0x1

    goto/32 :goto_24

    nop

    :goto_1b
    if-nez v2, :cond_2

    goto/32 :goto_1f

    :cond_2
    goto/32 :goto_e

    nop

    :goto_1c
    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_13

    nop

    :goto_1d
    invoke-virtual {v2}, Landroid/os/Looper;->isCurrentThread()Z

    move-result v2

    goto/32 :goto_1b

    nop

    :goto_1e
    goto :goto_15

    :goto_1f
    goto/32 :goto_d

    nop

    :goto_20
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isFinished()Z

    move-result v0

    goto/32 :goto_32

    nop

    :goto_21
    if-nez v0, :cond_3

    goto/32 :goto_15

    :cond_3
    goto/32 :goto_c

    nop

    :goto_22
    return-void

    :catchall_3
    move-exception v0

    :try_start_6
    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    goto/32 :goto_0

    nop

    :goto_23
    const-wide/16 v1, 0x4

    goto/32 :goto_2c

    nop

    :goto_24
    const/4 v3, 0x0

    :try_start_7
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/32 :goto_1

    nop

    :goto_25
    goto :goto_26

    :catch_0
    move-exception v5

    :goto_26
    goto/32 :goto_29

    nop

    :goto_27
    if-nez v1, :cond_4

    goto/32 :goto_15

    :cond_4
    goto/32 :goto_10

    nop

    :goto_28
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    goto/32 :goto_7

    nop

    :goto_29
    iget-object v4, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mLock:Ljava/lang/Object;

    goto/32 :goto_17

    nop

    :goto_2a
    return-void

    :goto_2b
    goto/32 :goto_4

    nop

    :goto_2c
    invoke-static {v1, v2, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    goto/32 :goto_1a

    nop

    :goto_2d
    iget-object v5, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mLock:Ljava/lang/Object;

    goto/32 :goto_b

    nop

    :goto_2e
    invoke-virtual {v2}, Landroid/view/ViewRootImpl;->getHandwritingInitiator()Landroid/view/HandwritingInitiator;

    move-result-object v3

    goto/32 :goto_30

    nop

    :goto_2f
    check-cast v0, Landroid/view/View;

    goto/32 :goto_21

    nop

    :goto_30
    invoke-virtual {v3, v0}, Landroid/view/HandwritingInitiator;->onInputConnectionClosed(Landroid/view/View;)V

    :goto_31
    goto/32 :goto_1e

    nop

    :goto_32
    if-nez v0, :cond_5

    goto/32 :goto_2b

    :cond_5
    goto/32 :goto_2a

    nop

    :goto_33
    invoke-direct {v2, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda40;-><init>(Landroid/view/View;)V

    goto/32 :goto_1c

    nop

    :goto_34
    invoke-direct {v2, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda41;-><init>(Landroid/view/View;)V

    goto/32 :goto_14

    nop
.end method

.method synthetic lambda$deleteSurroundingText$31$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;II)V
    .locals 3

    goto/32 :goto_b

    nop

    :goto_0
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_1
    const-string v2, "deleteSurroundingText on inactive InputConnection"

    goto/32 :goto_2

    nop

    :goto_2
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_7

    nop

    :goto_3
    if-eqz v1, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_c

    nop

    :goto_4
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_3

    nop

    :goto_5
    return-void

    :goto_6
    goto/32 :goto_0

    nop

    :goto_7
    return-void

    :goto_8
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_1

    nop

    :goto_9
    if-nez v0, :cond_1

    goto/32 :goto_11

    :cond_1
    goto/32 :goto_4

    nop

    :goto_a
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_f

    nop

    :goto_b
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_a

    nop

    :goto_c
    goto :goto_11

    :goto_d
    goto/32 :goto_e

    nop

    :goto_e
    invoke-interface {v0, p2, p3}, Landroid/view/inputmethod/InputConnection;->deleteSurroundingText(II)Z

    goto/32 :goto_10

    nop

    :goto_f
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_12

    nop

    :goto_10
    return-void

    :goto_11
    goto/32 :goto_8

    nop

    :goto_12
    if-ne v0, v1, :cond_2

    goto/32 :goto_6

    :cond_2
    goto/32 :goto_5

    nop
.end method

.method synthetic lambda$deleteSurroundingTextInCodePoints$32$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;II)V
    .locals 3

    goto/32 :goto_9

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_e

    nop

    :goto_2
    return-void

    :goto_3
    goto/32 :goto_f

    nop

    :goto_4
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_12

    nop

    :goto_5
    return-void

    :goto_6
    goto/32 :goto_a

    nop

    :goto_7
    goto :goto_8

    :catch_0
    move-exception v1

    :goto_8
    goto/32 :goto_5

    nop

    :goto_9
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_4

    nop

    :goto_a
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_d

    nop

    :goto_b
    goto :goto_6

    :goto_c
    :try_start_0
    invoke-interface {v0, p2, p3}, Landroid/view/inputmethod/InputConnection;->deleteSurroundingTextInCodePoints(II)Z
    :try_end_0
    .catch Ljava/lang/AbstractMethodError; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_7

    nop

    :goto_d
    const-string v2, "deleteSurroundingTextInCodePoints on inactive InputConnection"

    goto/32 :goto_10

    nop

    :goto_e
    if-eqz v1, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_b

    nop

    :goto_f
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_13

    nop

    :goto_10
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_0

    nop

    :goto_11
    if-ne v0, v1, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_2

    nop

    :goto_12
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_11

    nop

    :goto_13
    if-nez v0, :cond_2

    goto/32 :goto_6

    :cond_2
    goto/32 :goto_1

    nop
.end method

.method synthetic lambda$dispatchReportFullscreenMode$3$com-android-internal-inputmethod-RemoteInputConnectionImpl(Z)V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_1
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_7

    nop

    :goto_2
    return-void

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_1

    nop

    :goto_4
    goto :goto_9

    :goto_5
    goto/32 :goto_6

    nop

    :goto_6
    invoke-interface {v0, p1}, Landroid/view/inputmethod/InputConnection;->reportFullscreenMode(Z)Z

    goto/32 :goto_8

    nop

    :goto_7
    if-eqz v1, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_4

    nop

    :goto_8
    return-void

    :goto_9
    goto/32 :goto_2

    nop
.end method

.method synthetic lambda$dispatchWithTracing$43$com-android-internal-inputmethod-RemoteInputConnectionImpl(Ljava/util/function/Supplier;Lcom/android/internal/infra/AndroidFuture;Ljava/util/function/Function;Ljava/lang/String;)V
    .locals 5

    :try_start_0
    invoke-interface {p1}, Ljava/util/function/Supplier;->get()Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    nop

    goto/32 :goto_3

    nop

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_1
    throw v0

    :goto_2
    invoke-virtual {p2, v0}, Lcom/android/internal/infra/AndroidFuture;->completeExceptionally(Ljava/lang/Throwable;)Z

    goto/32 :goto_1

    nop

    :goto_3
    invoke-virtual {p2, v0}, Lcom/android/internal/infra/AndroidFuture;->complete(Ljava/lang/Object;)Z

    goto/32 :goto_d

    nop

    :goto_4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_c

    nop

    :goto_5
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_7

    nop

    :goto_6
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_b

    nop

    :goto_7
    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_4

    nop

    :goto_8
    check-cast v1, [B

    goto/32 :goto_10

    nop

    :goto_9
    invoke-virtual {v2, v3, v4, v1}, Lcom/android/internal/inputmethod/ImeTracing;->triggerClientDump(Ljava/lang/String;Landroid/view/inputmethod/InputMethodManager;[B)V

    :goto_a
    goto/32 :goto_f

    nop

    :goto_b
    const-string v4, "RemoteInputConnectionImpl#"

    goto/32 :goto_5

    nop

    :goto_c
    iget-object v4, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mParentInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    goto/32 :goto_9

    nop

    :goto_d
    if-nez p3, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_e

    nop

    :goto_e
    invoke-interface {p3, v0}, Ljava/util/function/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_f
    return-void

    :catchall_0
    move-exception v0

    goto/32 :goto_2

    nop

    :goto_10
    invoke-static {}, Lcom/android/internal/inputmethod/ImeTracing;->getInstance()Lcom/android/internal/inputmethod/ImeTracing;

    move-result-object v2

    goto/32 :goto_0

    nop
.end method

.method synthetic lambda$endBatchEdit$34$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;)V
    .locals 3

    goto/32 :goto_5

    nop

    :goto_0
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_d

    nop

    :goto_1
    if-ne v0, v1, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_11

    nop

    :goto_2
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_1

    nop

    :goto_3
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_4
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_b

    nop

    :goto_5
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_c

    nop

    :goto_6
    if-nez v0, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_a

    nop

    :goto_7
    return-void

    :goto_8
    goto/32 :goto_0

    nop

    :goto_9
    invoke-interface {v0}, Landroid/view/inputmethod/InputConnection;->endBatchEdit()Z

    goto/32 :goto_7

    nop

    :goto_a
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_e

    nop

    :goto_b
    return-void

    :goto_c
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_2

    nop

    :goto_d
    const-string v2, "endBatchEdit on inactive InputConnection"

    goto/32 :goto_4

    nop

    :goto_e
    if-eqz v1, :cond_2

    goto/32 :goto_10

    :cond_2
    goto/32 :goto_f

    nop

    :goto_f
    goto :goto_8

    :goto_10
    goto/32 :goto_9

    nop

    :goto_11
    return-void

    :goto_12
    goto/32 :goto_3

    nop
.end method

.method synthetic lambda$finishComposingText$28$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;)V
    .locals 3

    goto/32 :goto_8

    nop

    :goto_0
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_12

    nop

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_6

    nop

    :goto_2
    const-string v2, "finishComposingText on inactive InputConnection"

    goto/32 :goto_3

    nop

    :goto_3
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_10

    nop

    :goto_4
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_0

    nop

    :goto_5
    invoke-interface {v0}, Landroid/view/inputmethod/InputConnection;->finishComposingText()Z

    goto/32 :goto_f

    nop

    :goto_6
    return-void

    :goto_7
    goto/32 :goto_9

    nop

    :goto_8
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isFinished()Z

    move-result v0

    goto/32 :goto_1

    nop

    :goto_9
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_4

    nop

    :goto_a
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_2

    nop

    :goto_b
    return-void

    :goto_c
    goto/32 :goto_e

    nop

    :goto_d
    if-eqz v0, :cond_1

    goto/32 :goto_11

    :cond_1
    goto/32 :goto_a

    nop

    :goto_e
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_d

    nop

    :goto_f
    return-void

    :goto_10
    return-void

    :goto_11
    goto/32 :goto_5

    nop

    :goto_12
    if-ne v0, v1, :cond_2

    goto/32 :goto_c

    :cond_2
    goto/32 :goto_b

    nop
.end method

.method synthetic lambda$finishComposingTextFromImm$27$com-android-internal-inputmethod-RemoteInputConnectionImpl(I)V
    .locals 3

    goto/32 :goto_f

    nop

    :goto_0
    return-void

    :goto_1
    goto/32 :goto_10

    nop

    :goto_2
    return-void

    :goto_3
    if-eqz v0, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_b

    nop

    :goto_4
    if-nez v0, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_d

    nop

    :goto_5
    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_7

    nop

    :goto_6
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_9

    nop

    :goto_7
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    goto/32 :goto_8

    nop

    :goto_8
    if-ne p1, v0, :cond_2

    goto/32 :goto_1

    :cond_2
    goto/32 :goto_0

    nop

    :goto_9
    return-void

    :goto_a
    goto/32 :goto_11

    nop

    :goto_b
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_c

    nop

    :goto_c
    const-string v2, "finishComposingTextFromImm on inactive InputConnection"

    goto/32 :goto_6

    nop

    :goto_d
    return-void

    :goto_e
    goto/32 :goto_5

    nop

    :goto_f
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isFinished()Z

    move-result v0

    goto/32 :goto_4

    nop

    :goto_10
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_11
    invoke-interface {v0}, Landroid/view/inputmethod/InputConnection;->finishComposingText()Z

    goto/32 :goto_2

    nop
.end method

.method synthetic lambda$getCursorCapsMode$12$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;I)Ljava/lang/Integer;
    .locals 4

    goto/32 :goto_f

    nop

    :goto_0
    invoke-interface {v0, p2}, Landroid/view/inputmethod/InputConnection;->getCursorCapsMode(I)I

    move-result v1

    goto/32 :goto_c

    nop

    :goto_1
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_8

    nop

    :goto_2
    return-object v2

    :goto_3
    goto/32 :goto_d

    nop

    :goto_4
    return-object v1

    :goto_5
    goto/32 :goto_a

    nop

    :goto_6
    goto :goto_5

    :goto_7
    goto/32 :goto_0

    nop

    :goto_8
    if-eqz v1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_6

    nop

    :goto_9
    if-ne v0, v1, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_2

    nop

    :goto_a
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_13

    nop

    :goto_b
    return-object v2

    :goto_c
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_4

    nop

    :goto_d
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_11

    nop

    :goto_e
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/32 :goto_9

    nop

    :goto_f
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_14

    nop

    :goto_10
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_12

    nop

    :goto_11
    if-nez v0, :cond_2

    goto/32 :goto_5

    :cond_2
    goto/32 :goto_1

    nop

    :goto_12
    const/4 v2, 0x0

    goto/32 :goto_e

    nop

    :goto_13
    const-string v3, "getCursorCapsMode on inactive InputConnection"

    goto/32 :goto_15

    nop

    :goto_14
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_10

    nop

    :goto_15
    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_b

    nop
.end method

.method synthetic lambda$getExtractedText$14$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Landroid/view/inputmethod/ExtractedTextRequest;I)Landroid/view/inputmethod/ExtractedText;
    .locals 4

    goto/32 :goto_10

    nop

    :goto_0
    if-eqz v1, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_11

    nop

    :goto_1
    return-object v1

    :goto_2
    goto/32 :goto_f

    nop

    :goto_3
    const/4 v2, 0x0

    goto/32 :goto_a

    nop

    :goto_4
    invoke-interface {v0, p2, p3}, Landroid/view/inputmethod/InputConnection;->getExtractedText(Landroid/view/inputmethod/ExtractedTextRequest;I)Landroid/view/inputmethod/ExtractedText;

    move-result-object v1

    goto/32 :goto_1

    nop

    :goto_5
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_0

    nop

    :goto_6
    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_b

    nop

    :goto_7
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_3

    nop

    :goto_8
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_c

    nop

    :goto_9
    const-string v3, "getExtractedText on inactive InputConnection"

    goto/32 :goto_6

    nop

    :goto_a
    if-ne v0, v1, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_d

    nop

    :goto_b
    return-object v2

    :goto_c
    if-nez v0, :cond_2

    goto/32 :goto_2

    :cond_2
    goto/32 :goto_5

    nop

    :goto_d
    return-object v2

    :goto_e
    goto/32 :goto_8

    nop

    :goto_f
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_9

    nop

    :goto_10
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_13

    nop

    :goto_11
    goto :goto_2

    :goto_12
    goto/32 :goto_4

    nop

    :goto_13
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_7

    nop
.end method

.method synthetic lambda$getSelectedText$8$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;I)Ljava/lang/CharSequence;
    .locals 4

    goto/32 :goto_4

    nop

    :goto_0
    return-object v1

    :catch_0
    move-exception v1

    goto/32 :goto_d

    nop

    :goto_1
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_10

    nop

    :goto_2
    goto :goto_e

    :goto_3
    :try_start_0
    invoke-interface {v0, p2}, Landroid/view/inputmethod/InputConnection;->getSelectedText(I)Ljava/lang/CharSequence;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/AbstractMethodError; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_0

    nop

    :goto_4
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_8

    nop

    :goto_5
    if-eqz v1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_2

    nop

    :goto_6
    if-nez v0, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_11

    nop

    :goto_7
    const/4 v2, 0x0

    goto/32 :goto_13

    nop

    :goto_8
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_9

    nop

    :goto_9
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_7

    nop

    :goto_a
    return-object v2

    :goto_b
    return-object v2

    :goto_c
    goto/32 :goto_12

    nop

    :goto_d
    return-object v2

    :goto_e
    goto/32 :goto_1

    nop

    :goto_f
    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_a

    nop

    :goto_10
    const-string v3, "getSelectedText on inactive InputConnection"

    goto/32 :goto_f

    nop

    :goto_11
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_5

    nop

    :goto_12
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_13
    if-ne v0, v1, :cond_2

    goto/32 :goto_c

    :cond_2
    goto/32 :goto_b

    nop
.end method

.method synthetic lambda$getSurroundingText$10$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;III)Landroid/view/inputmethod/SurroundingText;
    .locals 5

    goto/32 :goto_12

    nop

    :goto_0
    invoke-interface {v0, p2, p3, p4}, Landroid/view/inputmethod/InputConnection;->getSurroundingText(III)Landroid/view/inputmethod/SurroundingText;

    move-result-object v1

    goto/32 :goto_13

    nop

    :goto_1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_1a

    nop

    :goto_2
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v3

    goto/32 :goto_21

    nop

    :goto_3
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_17

    nop

    :goto_4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_e

    nop

    :goto_5
    return-object v2

    :goto_6
    goto/32 :goto_11

    nop

    :goto_7
    return-object v2

    :goto_8
    goto/32 :goto_18

    nop

    :goto_9
    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_1f

    nop

    :goto_a
    const/4 v2, 0x0

    goto/32 :goto_10

    nop

    :goto_b
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_a

    nop

    :goto_c
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_15

    nop

    :goto_d
    return-object v2

    :goto_e
    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_7

    nop

    :goto_f
    new-instance v3, Ljava/lang/StringBuilder;

    goto/32 :goto_3

    nop

    :goto_10
    if-ne v0, v1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_5

    nop

    :goto_11
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_c

    nop

    :goto_12
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_19

    nop

    :goto_13
    return-object v1

    :goto_14
    goto/32 :goto_16

    nop

    :goto_15
    if-nez v0, :cond_1

    goto/32 :goto_14

    :cond_1
    goto/32 :goto_2

    nop

    :goto_16
    const-string v3, "getSurroundingText on inactive InputConnection"

    goto/32 :goto_25

    nop

    :goto_17
    const-string v4, "Returning null to getSurroundingText due to an invalid beforeLength="

    goto/32 :goto_1

    nop

    :goto_18
    if-ltz p3, :cond_2

    goto/32 :goto_20

    :cond_2
    goto/32 :goto_22

    nop

    :goto_19
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_b

    nop

    :goto_1a
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_4

    nop

    :goto_1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_26

    nop

    :goto_1c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_27

    nop

    :goto_1d
    if-ltz p2, :cond_3

    goto/32 :goto_8

    :cond_3
    goto/32 :goto_f

    nop

    :goto_1e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_9

    nop

    :goto_1f
    return-object v2

    :goto_20
    goto/32 :goto_0

    nop

    :goto_21
    if-eqz v3, :cond_4

    goto/32 :goto_24

    :cond_4
    goto/32 :goto_23

    nop

    :goto_22
    new-instance v3, Ljava/lang/StringBuilder;

    goto/32 :goto_1c

    nop

    :goto_23
    goto :goto_14

    :goto_24
    goto/32 :goto_1d

    nop

    :goto_25
    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_d

    nop

    :goto_26
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_1e

    nop

    :goto_27
    const-string v4, "Returning null to getSurroundingText due to an invalid afterLength="

    goto/32 :goto_1b

    nop
.end method

.method synthetic lambda$getTextAfterCursor$4$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;II)Ljava/lang/CharSequence;
    .locals 5

    goto/32 :goto_14

    nop

    :goto_0
    if-ne v0, v1, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_d

    nop

    :goto_1
    invoke-interface {v0, p2, p3}, Landroid/view/inputmethod/InputConnection;->getTextAfterCursor(II)Ljava/lang/CharSequence;

    move-result-object v1

    goto/32 :goto_1c

    nop

    :goto_2
    if-nez v0, :cond_1

    goto/32 :goto_1d

    :cond_1
    goto/32 :goto_11

    nop

    :goto_3
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_4
    new-instance v3, Ljava/lang/StringBuilder;

    goto/32 :goto_1b

    nop

    :goto_5
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_6

    nop

    :goto_6
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_19

    nop

    :goto_7
    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_b

    nop

    :goto_8
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_2

    nop

    :goto_9
    const-string v4, "Returning null to getTextAfterCursor due to an invalid length="

    goto/32 :goto_5

    nop

    :goto_a
    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_f

    nop

    :goto_b
    return-object v2

    :goto_c
    goto/32 :goto_1

    nop

    :goto_d
    return-object v2

    :goto_e
    goto/32 :goto_3

    nop

    :goto_f
    return-object v2

    :goto_10
    const/4 v2, 0x0

    goto/32 :goto_0

    nop

    :goto_11
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v3

    goto/32 :goto_17

    nop

    :goto_12
    goto :goto_1d

    :goto_13
    goto/32 :goto_16

    nop

    :goto_14
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_18

    nop

    :goto_15
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_10

    nop

    :goto_16
    if-ltz p2, :cond_2

    goto/32 :goto_c

    :cond_2
    goto/32 :goto_4

    nop

    :goto_17
    if-eqz v3, :cond_3

    goto/32 :goto_13

    :cond_3
    goto/32 :goto_12

    nop

    :goto_18
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_15

    nop

    :goto_19
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_7

    nop

    :goto_1a
    const-string v3, "getTextAfterCursor on inactive InputConnection"

    goto/32 :goto_a

    nop

    :goto_1b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_9

    nop

    :goto_1c
    return-object v1

    :goto_1d
    goto/32 :goto_1a

    nop
.end method

.method synthetic lambda$getTextBeforeCursor$6$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;II)Ljava/lang/CharSequence;
    .locals 5

    goto/32 :goto_19

    nop

    :goto_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_15

    nop

    :goto_1
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_0

    nop

    :goto_2
    return-object v2

    :goto_3
    goto/32 :goto_1b

    nop

    :goto_4
    goto :goto_a

    :goto_5
    goto/32 :goto_8

    nop

    :goto_6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_1

    nop

    :goto_7
    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_13

    nop

    :goto_8
    if-ltz p2, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_1c

    nop

    :goto_9
    return-object v1

    :goto_a
    goto/32 :goto_16

    nop

    :goto_b
    const-string v4, "Returning null to getTextBeforeCursor due to an invalid length="

    goto/32 :goto_6

    nop

    :goto_c
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v3

    goto/32 :goto_10

    nop

    :goto_d
    if-ne v0, v1, :cond_1

    goto/32 :goto_f

    :cond_1
    goto/32 :goto_e

    nop

    :goto_e
    return-object v2

    :goto_f
    goto/32 :goto_11

    nop

    :goto_10
    if-eqz v3, :cond_2

    goto/32 :goto_5

    :cond_2
    goto/32 :goto_4

    nop

    :goto_11
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_1a

    nop

    :goto_12
    const/4 v2, 0x0

    goto/32 :goto_d

    nop

    :goto_13
    return-object v2

    :goto_14
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_1d

    nop

    :goto_15
    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_2

    nop

    :goto_16
    const-string v3, "getTextBeforeCursor on inactive InputConnection"

    goto/32 :goto_7

    nop

    :goto_17
    if-nez v0, :cond_3

    goto/32 :goto_a

    :cond_3
    goto/32 :goto_c

    nop

    :goto_18
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_b

    nop

    :goto_19
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_14

    nop

    :goto_1a
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_17

    nop

    :goto_1b
    invoke-interface {v0, p2, p3}, Landroid/view/inputmethod/InputConnection;->getTextBeforeCursor(II)Ljava/lang/CharSequence;

    move-result-object v1

    goto/32 :goto_9

    nop

    :goto_1c
    new-instance v3, Ljava/lang/StringBuilder;

    goto/32 :goto_18

    nop

    :goto_1d
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_12

    nop
.end method

.method synthetic lambda$performContextMenuAction$22$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;I)V
    .locals 3

    goto/32 :goto_9

    nop

    :goto_0
    return-void

    :goto_1
    return-void

    :goto_2
    goto/32 :goto_5

    nop

    :goto_3
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_0

    nop

    :goto_4
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_11

    nop

    :goto_5
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_e

    nop

    :goto_6
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_f

    nop

    :goto_7
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_10

    nop

    :goto_8
    if-ne v0, v1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop

    :goto_9
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_4

    nop

    :goto_a
    goto :goto_d

    :goto_b
    goto/32 :goto_12

    nop

    :goto_c
    return-void

    :goto_d
    goto/32 :goto_6

    nop

    :goto_e
    if-nez v0, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_7

    nop

    :goto_f
    const-string/jumbo v2, "performContextMenuAction on inactive InputConnection"

    goto/32 :goto_3

    nop

    :goto_10
    if-eqz v1, :cond_2

    goto/32 :goto_b

    :cond_2
    goto/32 :goto_a

    nop

    :goto_11
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_8

    nop

    :goto_12
    invoke-interface {v0, p2}, Landroid/view/inputmethod/InputConnection;->performContextMenuAction(I)Z

    goto/32 :goto_c

    nop
.end method

.method synthetic lambda$performEditorAction$21$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;I)V
    .locals 3

    goto/32 :goto_6

    nop

    :goto_0
    invoke-interface {v0, p2}, Landroid/view/inputmethod/InputConnection;->performEditorAction(I)Z

    goto/32 :goto_c

    nop

    :goto_1
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_b

    nop

    :goto_2
    if-ne v0, v1, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_7

    nop

    :goto_3
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_12

    nop

    :goto_4
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_11

    nop

    :goto_5
    if-eqz v1, :cond_1

    goto/32 :goto_f

    :cond_1
    goto/32 :goto_e

    nop

    :goto_6
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_10

    nop

    :goto_7
    return-void

    :goto_8
    goto/32 :goto_1

    nop

    :goto_9
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_2

    nop

    :goto_a
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_5

    nop

    :goto_b
    if-nez v0, :cond_2

    goto/32 :goto_d

    :cond_2
    goto/32 :goto_a

    nop

    :goto_c
    return-void

    :goto_d
    goto/32 :goto_4

    nop

    :goto_e
    goto :goto_d

    :goto_f
    goto/32 :goto_0

    nop

    :goto_10
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_9

    nop

    :goto_11
    const-string/jumbo v2, "performEditorAction on inactive InputConnection"

    goto/32 :goto_3

    nop

    :goto_12
    return-void
.end method

.method synthetic lambda$performPrivateCommand$36$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3

    goto/32 :goto_6

    nop

    :goto_0
    if-ne v0, v1, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_b

    nop

    :goto_1
    return-void

    :goto_2
    goto/32 :goto_f

    nop

    :goto_3
    const-string/jumbo v2, "performPrivateCommand on inactive InputConnection"

    goto/32 :goto_7

    nop

    :goto_4
    return-void

    :goto_5
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_6
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_d

    nop

    :goto_7
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_4

    nop

    :goto_8
    if-nez v0, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_a

    nop

    :goto_9
    invoke-interface {v0, p2, p3}, Landroid/view/inputmethod/InputConnection;->performPrivateCommand(Ljava/lang/String;Landroid/os/Bundle;)Z

    goto/32 :goto_1

    nop

    :goto_a
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_12

    nop

    :goto_b
    return-void

    :goto_c
    goto/32 :goto_5

    nop

    :goto_d
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_e

    nop

    :goto_e
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_0

    nop

    :goto_f
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_3

    nop

    :goto_10
    goto :goto_2

    :goto_11
    goto/32 :goto_9

    nop

    :goto_12
    if-eqz v1, :cond_2

    goto/32 :goto_11

    :cond_2
    goto/32 :goto_10

    nop
.end method

.method synthetic lambda$performSpellCheck$35$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;)V
    .locals 3

    goto/32 :goto_5

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_6

    nop

    :goto_1
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_11

    nop

    :goto_2
    const-string/jumbo v2, "performSpellCheck on inactive InputConnection"

    goto/32 :goto_1

    nop

    :goto_3
    return-void

    :goto_4
    goto/32 :goto_7

    nop

    :goto_5
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_a

    nop

    :goto_6
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_b

    nop

    :goto_7
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_2

    nop

    :goto_8
    goto :goto_4

    :goto_9
    goto/32 :goto_10

    nop

    :goto_a
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_12

    nop

    :goto_b
    if-eqz v1, :cond_1

    goto/32 :goto_9

    :cond_1
    goto/32 :goto_8

    nop

    :goto_c
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_d
    if-ne v0, v1, :cond_2

    goto/32 :goto_f

    :cond_2
    goto/32 :goto_e

    nop

    :goto_e
    return-void

    :goto_f
    goto/32 :goto_c

    nop

    :goto_10
    invoke-interface {v0}, Landroid/view/inputmethod/InputConnection;->performSpellCheck()Z

    goto/32 :goto_3

    nop

    :goto_11
    return-void

    :goto_12
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_d

    nop
.end method

.method synthetic lambda$requestCursorUpdates$38$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;II)Ljava/lang/Boolean;
    .locals 3

    goto/32 :goto_4

    nop

    :goto_0
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_8

    nop

    :goto_1
    invoke-direct {p0, p2, v2, p3}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->requestCursorUpdatesInternal(III)Z

    move-result v0

    goto/32 :goto_2

    nop

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_3
    if-ne v0, v1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_a

    nop

    :goto_4
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_0

    nop

    :goto_5
    return-object v0

    :goto_6
    return-object v0

    :goto_7
    goto/32 :goto_1

    nop

    :goto_8
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_9

    nop

    :goto_9
    const/4 v2, 0x0

    goto/32 :goto_3

    nop

    :goto_a
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/32 :goto_6

    nop
.end method

.method synthetic lambda$requestCursorUpdatesFromImm$37$com-android-internal-inputmethod-RemoteInputConnectionImpl(IIII)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    if-ne p1, v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_5

    nop

    :goto_1
    invoke-direct {p0, p2, p3, p4}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->requestCursorUpdatesInternal(III)Z

    goto/32 :goto_4

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    goto/32 :goto_0

    nop

    :goto_4
    return-void

    :goto_5
    return-void

    :goto_6
    goto/32 :goto_1

    nop
.end method

.method synthetic lambda$requestCursorUpdatesWithFilter$39$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;III)Ljava/lang/Boolean;
    .locals 2

    goto/32 :goto_6

    nop

    :goto_0
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_2

    nop

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_2
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_8

    nop

    :goto_3
    return-object v0

    :goto_4
    goto/32 :goto_9

    nop

    :goto_5
    const/4 v0, 0x0

    goto/32 :goto_1

    nop

    :goto_6
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_0

    nop

    :goto_7
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/32 :goto_a

    nop

    :goto_8
    if-ne v0, v1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_5

    nop

    :goto_9
    invoke-direct {p0, p2, p3, p4}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->requestCursorUpdatesInternal(III)Z

    move-result v0

    goto/32 :goto_7

    nop

    :goto_a
    return-object v0
.end method

.method synthetic lambda$scheduleInvalidateInput$0$com-android-internal-inputmethod-RemoteInputConnectionImpl(I)V
    .locals 11

    goto/32 :goto_3

    nop

    :goto_0
    invoke-virtual {v3, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto/32 :goto_18

    nop

    :goto_1
    return-void

    :goto_2
    :try_start_0
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getServedView()Landroid/view/View;

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_f

    nop

    :goto_3
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isFinished()Z

    move-result v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/32 :goto_12

    nop

    :goto_4
    return-void

    :goto_5
    :try_start_2
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/32 :goto_1b

    nop

    :goto_6
    return-void

    :cond_0
    :goto_7
    :try_start_3
    iget-object v5, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mParentInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v5, v2}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/32 :goto_e

    nop

    :goto_8
    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto/32 :goto_1

    nop

    :goto_9
    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto/32 :goto_11

    nop

    :goto_a
    return-void

    :catchall_0
    move-exception v1

    goto/32 :goto_10

    nop

    :goto_b
    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    nop

    goto/32 :goto_a

    nop

    :goto_c
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mHasPendingInvalidation:Ljava/util/concurrent/atomic/AtomicBoolean;

    goto/32 :goto_d

    nop

    :goto_d
    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto/32 :goto_4

    nop

    :goto_e
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mHasPendingInvalidation:Ljava/util/concurrent/atomic/AtomicBoolean;

    goto/32 :goto_b

    nop

    :goto_f
    if-eqz v2, :cond_1

    goto/32 :goto_19

    :cond_1
    goto/32 :goto_14

    nop

    :goto_10
    iget-object v2, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mHasPendingInvalidation:Ljava/util/concurrent/atomic/AtomicBoolean;

    goto/32 :goto_9

    nop

    :goto_11
    throw v1

    :goto_12
    if-nez v1, :cond_2

    goto/32 :goto_5

    :cond_2
    goto/32 :goto_c

    nop

    :goto_13
    if-nez v6, :cond_3

    goto/32 :goto_7

    :cond_3
    goto/32 :goto_17

    nop

    :goto_14
    iget-object v3, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mHasPendingInvalidation:Ljava/util/concurrent/atomic/AtomicBoolean;

    goto/32 :goto_0

    nop

    :goto_15
    invoke-virtual {v6, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto/32 :goto_6

    nop

    :goto_16
    iget-object v2, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mHasPendingInvalidation:Ljava/util/concurrent/atomic/AtomicBoolean;

    goto/32 :goto_8

    nop

    :goto_17
    iget-object v6, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mHasPendingInvalidation:Ljava/util/concurrent/atomic/AtomicBoolean;

    goto/32 :goto_15

    nop

    :goto_18
    return-void

    :goto_19
    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    nop

    invoke-static {v3}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$KnownAlwaysTrueEndBatchEditCache;->contains(Ljava/lang/Class;)Z

    move-result v4

    if-nez v4, :cond_6

    invoke-interface {v1}, Landroid/view/inputmethod/InputConnection;->beginBatchEdit()Z

    move-result v5

    invoke-interface {v1}, Landroid/view/inputmethod/InputConnection;->finishComposingText()Z

    if-eqz v5, :cond_6

    const/4 v6, 0x0

    :cond_4
    invoke-interface {v1}, Landroid/view/inputmethod/InputConnection;->endBatchEdit()Z

    move-result v7

    if-nez v7, :cond_5

    goto :goto_1a

    :cond_5
    add-int/lit8 v6, v6, 0x1

    const/16 v7, 0x10

    if-le v6, v7, :cond_4

    const-string v8, "RemoteInputConnectionImpl"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Class;->getTypeName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "#endBatchEdit() still returns true even after retrying "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, " times.  Falling back to InputMethodManager#restartInput(View)"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x1

    invoke-static {v3}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$KnownAlwaysTrueEndBatchEditCache;->add(Ljava/lang/Class;)V

    :cond_6
    :goto_1a
    if-nez v4, :cond_0

    invoke-interface {v1}, Landroid/view/inputmethod/InputConnection;->takeSnapshot()Landroid/view/inputmethod/TextSnapshot;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v6, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mParentInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v6, p0, v5, p1}, Landroid/view/inputmethod/InputMethodManager;->doInvalidateInput(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Landroid/view/inputmethod/TextSnapshot;I)Z

    move-result v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/32 :goto_13

    nop

    :goto_1b
    if-eqz v1, :cond_7

    goto/32 :goto_2

    :cond_7
    goto/32 :goto_16

    nop
.end method

.method synthetic lambda$sendKeyEvent$29$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Landroid/view/KeyEvent;)V
    .locals 3

    goto/32 :goto_2

    nop

    :goto_0
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_8

    nop

    :goto_1
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_4

    nop

    :goto_2
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_7

    nop

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_11

    :cond_0
    goto/32 :goto_f

    nop

    :goto_4
    if-ne v0, v1, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_a

    nop

    :goto_5
    goto :goto_11

    :goto_6
    goto/32 :goto_c

    nop

    :goto_7
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_1

    nop

    :goto_8
    return-void

    :goto_9
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_12

    nop

    :goto_a
    return-void

    :goto_b
    goto/32 :goto_d

    nop

    :goto_c
    invoke-interface {v0, p2}, Landroid/view/inputmethod/InputConnection;->sendKeyEvent(Landroid/view/KeyEvent;)Z

    goto/32 :goto_10

    nop

    :goto_d
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_e
    if-eqz v1, :cond_2

    goto/32 :goto_6

    :cond_2
    goto/32 :goto_5

    nop

    :goto_f
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_e

    nop

    :goto_10
    return-void

    :goto_11
    goto/32 :goto_9

    nop

    :goto_12
    const-string/jumbo v2, "sendKeyEvent on inactive InputConnection"

    goto/32 :goto_0

    nop
.end method

.method synthetic lambda$setComposingRegion$23$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;II)V
    .locals 3

    goto/32 :goto_4

    nop

    :goto_0
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_c

    nop

    :goto_1
    const-string/jumbo v2, "setComposingRegion on inactive InputConnection"

    goto/32 :goto_e

    nop

    :goto_2
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_b

    nop

    :goto_4
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_0

    nop

    :goto_5
    return-void

    :goto_6
    goto/32 :goto_2

    nop

    :goto_7
    if-eqz v1, :cond_1

    goto/32 :goto_9

    :cond_1
    goto/32 :goto_8

    nop

    :goto_8
    goto :goto_12

    :goto_9
    :try_start_0
    invoke-interface {v0, p2, p3}, Landroid/view/inputmethod/InputConnection;->setComposingRegion(II)Z
    :try_end_0
    .catch Ljava/lang/AbstractMethodError; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_f

    nop

    :goto_a
    if-ne v0, v1, :cond_2

    goto/32 :goto_6

    :cond_2
    goto/32 :goto_5

    nop

    :goto_b
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_7

    nop

    :goto_c
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_a

    nop

    :goto_d
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_1

    nop

    :goto_e
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_13

    nop

    :goto_f
    goto :goto_10

    :catch_0
    move-exception v1

    :goto_10
    goto/32 :goto_11

    nop

    :goto_11
    return-void

    :goto_12
    goto/32 :goto_d

    nop

    :goto_13
    return-void
.end method

.method synthetic lambda$setComposingRegionWithTextAttribute$24$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;IILandroid/view/inputmethod/TextAttribute;)V
    .locals 3

    goto/32 :goto_11

    nop

    :goto_0
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_c

    nop

    :goto_1
    goto :goto_6

    :goto_2
    goto/32 :goto_e

    nop

    :goto_3
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_d

    nop

    :goto_4
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_b

    nop

    :goto_5
    return-void

    :goto_6
    goto/32 :goto_8

    nop

    :goto_7
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_f

    nop

    :goto_8
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_10

    nop

    :goto_9
    return-void

    :goto_a
    goto/32 :goto_4

    nop

    :goto_b
    if-nez v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_7

    nop

    :goto_c
    return-void

    :goto_d
    if-ne v0, v1, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_9

    nop

    :goto_e
    invoke-interface {v0, p2, p3, p4}, Landroid/view/inputmethod/InputConnection;->setComposingRegion(IILandroid/view/inputmethod/TextAttribute;)Z

    goto/32 :goto_5

    nop

    :goto_f
    if-eqz v1, :cond_2

    goto/32 :goto_2

    :cond_2
    goto/32 :goto_1

    nop

    :goto_10
    const-string/jumbo v2, "setComposingRegion on inactive InputConnection"

    goto/32 :goto_0

    nop

    :goto_11
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_12

    nop

    :goto_12
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_3

    nop
.end method

.method synthetic lambda$setComposingText$25$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Ljava/lang/CharSequence;I)V
    .locals 3

    goto/32 :goto_10

    nop

    :goto_0
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_f

    nop

    :goto_1
    invoke-interface {v0, p2, p3}, Landroid/view/inputmethod/InputConnection;->setComposingText(Ljava/lang/CharSequence;I)Z

    goto/32 :goto_5

    nop

    :goto_2
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_3
    return-void

    :goto_4
    goto/32 :goto_2

    nop

    :goto_5
    return-void

    :goto_6
    goto/32 :goto_0

    nop

    :goto_7
    if-nez v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_12

    nop

    :goto_8
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_d

    nop

    :goto_9
    goto :goto_6

    :goto_a
    goto/32 :goto_1

    nop

    :goto_b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_c

    nop

    :goto_c
    return-void

    :goto_d
    if-ne v0, v1, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_3

    nop

    :goto_e
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_8

    nop

    :goto_f
    const-string/jumbo v2, "setComposingText on inactive InputConnection"

    goto/32 :goto_b

    nop

    :goto_10
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_e

    nop

    :goto_11
    if-eqz v1, :cond_2

    goto/32 :goto_a

    :cond_2
    goto/32 :goto_9

    nop

    :goto_12
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_11

    nop
.end method

.method synthetic lambda$setComposingTextWithTextAttribute$26$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Ljava/lang/CharSequence;ILandroid/view/inputmethod/TextAttribute;)V
    .locals 3

    goto/32 :goto_11

    nop

    :goto_0
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_10

    nop

    :goto_1
    goto :goto_9

    :goto_2
    goto/32 :goto_a

    nop

    :goto_3
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_4

    nop

    :goto_4
    if-eqz v1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop

    :goto_5
    return-void

    :goto_6
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_7

    nop

    :goto_7
    const-string/jumbo v2, "setComposingText on inactive InputConnection"

    goto/32 :goto_c

    nop

    :goto_8
    return-void

    :goto_9
    goto/32 :goto_6

    nop

    :goto_a
    invoke-interface {v0, p2, p3, p4}, Landroid/view/inputmethod/InputConnection;->setComposingText(Ljava/lang/CharSequence;ILandroid/view/inputmethod/TextAttribute;)Z

    goto/32 :goto_8

    nop

    :goto_b
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_12

    nop

    :goto_c
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_5

    nop

    :goto_d
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_0

    nop

    :goto_e
    return-void

    :goto_f
    goto/32 :goto_b

    nop

    :goto_10
    if-ne v0, v1, :cond_1

    goto/32 :goto_f

    :cond_1
    goto/32 :goto_e

    nop

    :goto_11
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_d

    nop

    :goto_12
    if-nez v0, :cond_2

    goto/32 :goto_9

    :cond_2
    goto/32 :goto_3

    nop
.end method

.method synthetic lambda$setImeConsumesInput$41$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Z)V
    .locals 3

    goto/32 :goto_1

    nop

    :goto_0
    invoke-interface {v0, p2}, Landroid/view/inputmethod/InputConnection;->setImeConsumesInput(Z)Z

    goto/32 :goto_5

    nop

    :goto_1
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_11

    nop

    :goto_2
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_3
    if-ne v0, v1, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_b

    nop

    :goto_4
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_3

    nop

    :goto_5
    return-void

    :goto_6
    goto/32 :goto_12

    nop

    :goto_7
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_10

    nop

    :goto_8
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_a

    nop

    :goto_9
    if-nez v0, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_8

    nop

    :goto_a
    if-eqz v1, :cond_2

    goto/32 :goto_f

    :cond_2
    goto/32 :goto_e

    nop

    :goto_b
    return-void

    :goto_c
    goto/32 :goto_2

    nop

    :goto_d
    const-string/jumbo v2, "setImeConsumesInput on inactive InputConnection"

    goto/32 :goto_7

    nop

    :goto_e
    goto :goto_6

    :goto_f
    goto/32 :goto_0

    nop

    :goto_10
    return-void

    :goto_11
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_4

    nop

    :goto_12
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_d

    nop
.end method

.method synthetic lambda$setSelection$20$com-android-internal-inputmethod-RemoteInputConnectionImpl(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;II)V
    .locals 3

    goto/32 :goto_b

    nop

    :goto_0
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_5

    nop

    :goto_1
    goto :goto_a

    :goto_2
    goto/32 :goto_f

    nop

    :goto_3
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_11

    nop

    :goto_4
    if-nez v0, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_10

    nop

    :goto_5
    const-string/jumbo v2, "setSelection on inactive InputConnection"

    goto/32 :goto_8

    nop

    :goto_6
    return-void

    :goto_7
    if-ne v0, v1, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_d

    nop

    :goto_8
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_6

    nop

    :goto_9
    return-void

    :goto_a
    goto/32 :goto_0

    nop

    :goto_b
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_3

    nop

    :goto_c
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_d
    return-void

    :goto_e
    goto/32 :goto_c

    nop

    :goto_f
    invoke-interface {v0, p2, p3}, Landroid/view/inputmethod/InputConnection;->setSelection(II)Z

    goto/32 :goto_9

    nop

    :goto_10
    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_12

    nop

    :goto_11
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_7

    nop

    :goto_12
    if-eqz v1, :cond_2

    goto/32 :goto_2

    :cond_2
    goto/32 :goto_1

    nop
.end method

.method public performContextMenuAction(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;I)V
    .locals 2

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda1;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;I)V

    const-string/jumbo v1, "performContextMenuAction"

    invoke-direct {p0, v1, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public performEditorAction(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;I)V
    .locals 2

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda34;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda34;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;I)V

    const-string/jumbo v1, "performEditorAction"

    invoke-direct {p0, v1, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public performPrivateCommand(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda6;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda6;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Ljava/lang/String;Landroid/os/Bundle;)V

    const-string/jumbo v1, "performPrivateCommand"

    invoke-direct {p0, v1, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public performSpellCheck(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;)V
    .locals 2

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;)V

    const-string/jumbo v1, "performSpellCheck"

    invoke-direct {p0, v1, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public requestCursorUpdates(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;IILcom/android/internal/infra/AndroidFuture;)V
    .locals 2

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda12;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda12;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;II)V

    const-string/jumbo v1, "requestCursorUpdates"

    invoke-direct {p0, v1, p4, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Lcom/android/internal/infra/AndroidFuture;Ljava/util/function/Supplier;)V

    return-void
.end method

.method public requestCursorUpdatesFromImm(III)V
    .locals 8

    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    new-instance v7, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda9;

    move-object v1, v7

    move-object v2, p0

    move v3, v0

    move v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda9;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;IIII)V

    const-string/jumbo v1, "requestCursorUpdatesFromImm"

    invoke-direct {p0, v1, v7}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public requestCursorUpdatesWithFilter(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;IIILcom/android/internal/infra/AndroidFuture;)V
    .locals 7

    new-instance v6, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda21;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda21;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;III)V

    const-string/jumbo v0, "requestCursorUpdates"

    invoke-direct {p0, v0, p5, v6}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Lcom/android/internal/infra/AndroidFuture;Ljava/util/function/Supplier;)V

    return-void
.end method

.method public scheduleInvalidateInput()V
    .locals 3

    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mHasPendingInvalidation:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mCurrentSessionId:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mH:Landroid/os/Handler;

    new-instance v2, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda7;

    invoke-direct {v2, p0, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda7;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public sendKeyEvent(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Landroid/view/KeyEvent;)V
    .locals 2

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda11;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda11;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Landroid/view/KeyEvent;)V

    const-string/jumbo v1, "sendKeyEvent"

    invoke-direct {p0, v1, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public setComposingRegion(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;II)V
    .locals 2

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda23;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda23;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;II)V

    const-string/jumbo v1, "setComposingRegion"

    invoke-direct {p0, v1, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public setComposingRegionWithTextAttribute(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;IILandroid/view/inputmethod/TextAttribute;)V
    .locals 7

    new-instance v6, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda25;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda25;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;IILandroid/view/inputmethod/TextAttribute;)V

    const-string/jumbo v0, "setComposingRegionWithTextAttribute"

    invoke-direct {p0, v0, v6}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public setComposingText(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Ljava/lang/CharSequence;I)V
    .locals 2

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda20;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda20;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Ljava/lang/CharSequence;I)V

    const-string/jumbo v1, "setComposingText"

    invoke-direct {p0, v1, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public setComposingTextWithTextAttribute(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Ljava/lang/CharSequence;ILandroid/view/inputmethod/TextAttribute;)V
    .locals 7

    new-instance v6, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda18;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda18;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Ljava/lang/CharSequence;ILandroid/view/inputmethod/TextAttribute;)V

    const-string/jumbo v0, "setComposingTextWithTextAttribute"

    invoke-direct {p0, v0, v6}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public setImeConsumesInput(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Z)V
    .locals 2

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda37;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda37;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Z)V

    const-string/jumbo v1, "setImeConsumesInput"

    invoke-direct {p0, v1, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public setSelection(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;II)V
    .locals 2

    new-instance v0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda16;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$$ExternalSyntheticLambda16;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;II)V

    const-string/jumbo v1, "setSelection"

    invoke-direct {p0, v1, v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->dispatchWithTracing(Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RemoteInputConnectionImpl{connection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " finished="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isFinished()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mParentInputMethodManager.isActive()="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mParentInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->isActive()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mServedView="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->mServedView:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
