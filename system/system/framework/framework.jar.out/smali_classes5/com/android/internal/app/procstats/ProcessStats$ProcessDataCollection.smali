.class public final Lcom/android/internal/app/procstats/ProcessStats$ProcessDataCollection;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/app/procstats/ProcessStats;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProcessDataCollection"
.end annotation


# instance fields
.field public avgPss:J

.field public avgRss:J

.field public avgUss:J

.field public maxPss:J

.field public maxRss:J

.field public maxUss:J

.field final memStates:[I

.field public minPss:J

.field public minRss:J

.field public minUss:J

.field public numPss:J

.field final procStates:[I

.field final screenStates:[I

.field public totalTime:J


# direct methods
.method public constructor <init>([I[I[I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/internal/app/procstats/ProcessStats$ProcessDataCollection;->screenStates:[I

    iput-object p2, p0, Lcom/android/internal/app/procstats/ProcessStats$ProcessDataCollection;->memStates:[I

    iput-object p3, p0, Lcom/android/internal/app/procstats/ProcessStats$ProcessDataCollection;->procStates:[I

    return-void
.end method


# virtual methods
.method print(Ljava/io/PrintWriter;JZ)V
    .locals 6

    goto/32 :goto_39

    nop

    :goto_0
    const-wide/16 v2, 0x0

    goto/32 :goto_31

    nop

    :goto_1
    mul-long/2addr v4, v2

    goto/32 :goto_17

    nop

    :goto_2
    invoke-static {p1, v4, v5}, Landroid/util/DebugUtils;->printSizeValue(Ljava/io/PrintWriter;J)V

    goto/32 :goto_36

    nop

    :goto_3
    mul-long/2addr v4, v2

    goto/32 :goto_11

    nop

    :goto_4
    invoke-static {p1, v4, v5}, Landroid/util/DebugUtils;->printSizeValue(Ljava/io/PrintWriter;J)V

    goto/32 :goto_3c

    nop

    :goto_5
    iget-wide v4, p0, Lcom/android/internal/app/procstats/ProcessStats$ProcessDataCollection;->minRss:J

    goto/32 :goto_38

    nop

    :goto_6
    iget-wide v0, p0, Lcom/android/internal/app/procstats/ProcessStats$ProcessDataCollection;->minPss:J

    goto/32 :goto_2a

    nop

    :goto_7
    invoke-static {p1, v0, v1}, Landroid/util/DebugUtils;->printSizeValue(Ljava/io/PrintWriter;J)V

    goto/32 :goto_24

    nop

    :goto_8
    const-string v0, "*"

    goto/32 :goto_34

    nop

    :goto_9
    iget-wide v4, p0, Lcom/android/internal/app/procstats/ProcessStats$ProcessDataCollection;->avgUss:J

    goto/32 :goto_3

    nop

    :goto_a
    cmp-long v0, v0, p2

    goto/32 :goto_14

    nop

    :goto_b
    mul-long/2addr v4, v2

    goto/32 :goto_2

    nop

    :goto_c
    invoke-static {p1, v4, v5}, Landroid/util/DebugUtils;->printSizeValue(Ljava/io/PrintWriter;J)V

    goto/32 :goto_f

    nop

    :goto_d
    mul-long/2addr v4, v2

    goto/32 :goto_1b

    nop

    :goto_e
    const-string v0, " over "

    goto/32 :goto_28

    nop

    :goto_f
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_3f

    nop

    :goto_10
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_1d

    nop

    :goto_11
    invoke-static {p1, v4, v5}, Landroid/util/DebugUtils;->printSizeValue(Ljava/io/PrintWriter;J)V

    goto/32 :goto_16

    nop

    :goto_12
    mul-long/2addr v4, v2

    goto/32 :goto_c

    nop

    :goto_13
    invoke-static {p1, v0, v1}, Lcom/android/internal/app/procstats/DumpUtils;->printPercent(Ljava/io/PrintWriter;D)V

    goto/32 :goto_21

    nop

    :goto_14
    if-gtz v0, :cond_0

    goto/32 :goto_35

    :cond_0
    goto/32 :goto_8

    nop

    :goto_15
    iget-wide v4, p0, Lcom/android/internal/app/procstats/ProcessStats$ProcessDataCollection;->maxUss:J

    goto/32 :goto_d

    nop

    :goto_16
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_15

    nop

    :goto_17
    invoke-static {p1, v4, v5}, Landroid/util/DebugUtils;->printSizeValue(Ljava/io/PrintWriter;J)V

    goto/32 :goto_1f

    nop

    :goto_18
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    :goto_19
    goto/32 :goto_2f

    nop

    :goto_1a
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_6

    nop

    :goto_1b
    invoke-static {p1, v4, v5}, Landroid/util/DebugUtils;->printSizeValue(Ljava/io/PrintWriter;J)V

    goto/32 :goto_1c

    nop

    :goto_1c
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_5

    nop

    :goto_1d
    iget-wide v4, p0, Lcom/android/internal/app/procstats/ProcessStats$ProcessDataCollection;->minUss:J

    goto/32 :goto_33

    nop

    :goto_1e
    const-string v0, ")"

    goto/32 :goto_18

    nop

    :goto_1f
    const-string v1, "/"

    goto/32 :goto_10

    nop

    :goto_20
    iget-wide v4, p0, Lcom/android/internal/app/procstats/ProcessStats$ProcessDataCollection;->avgRss:J

    goto/32 :goto_b

    nop

    :goto_21
    iget-wide v0, p0, Lcom/android/internal/app/procstats/ProcessStats$ProcessDataCollection;->numPss:J

    goto/32 :goto_0

    nop

    :goto_22
    invoke-static {p1, v4, v5}, Landroid/util/DebugUtils;->printSizeValue(Ljava/io/PrintWriter;J)V

    goto/32 :goto_2d

    nop

    :goto_23
    const-string v0, " ("

    goto/32 :goto_1a

    nop

    :goto_24
    const-string v0, "-"

    goto/32 :goto_2b

    nop

    :goto_25
    iget-wide v0, p0, Lcom/android/internal/app/procstats/ProcessStats$ProcessDataCollection;->numPss:J

    goto/32 :goto_3a

    nop

    :goto_26
    iget-wide v0, p0, Lcom/android/internal/app/procstats/ProcessStats$ProcessDataCollection;->maxRss:J

    goto/32 :goto_2e

    nop

    :goto_27
    if-nez p4, :cond_1

    goto/32 :goto_3b

    :cond_1
    goto/32 :goto_e

    nop

    :goto_28
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_25

    nop

    :goto_29
    if-gtz v0, :cond_2

    goto/32 :goto_19

    :cond_2
    goto/32 :goto_23

    nop

    :goto_2a
    const-wide/16 v2, 0x400

    goto/32 :goto_3d

    nop

    :goto_2b
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_32

    nop

    :goto_2c
    long-to-double v0, v0

    goto/32 :goto_3e

    nop

    :goto_2d
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_20

    nop

    :goto_2e
    mul-long/2addr v0, v2

    goto/32 :goto_30

    nop

    :goto_2f
    return-void

    :goto_30
    invoke-static {p1, v0, v1}, Landroid/util/DebugUtils;->printSizeValue(Ljava/io/PrintWriter;J)V

    goto/32 :goto_27

    nop

    :goto_31
    cmp-long v0, v0, v2

    goto/32 :goto_29

    nop

    :goto_32
    iget-wide v4, p0, Lcom/android/internal/app/procstats/ProcessStats$ProcessDataCollection;->avgPss:J

    goto/32 :goto_12

    nop

    :goto_33
    mul-long/2addr v4, v2

    goto/32 :goto_4

    nop

    :goto_34
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    :goto_35
    goto/32 :goto_40

    nop

    :goto_36
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_26

    nop

    :goto_37
    div-double/2addr v0, v2

    goto/32 :goto_13

    nop

    :goto_38
    mul-long/2addr v4, v2

    goto/32 :goto_22

    nop

    :goto_39
    iget-wide v0, p0, Lcom/android/internal/app/procstats/ProcessStats$ProcessDataCollection;->totalTime:J

    goto/32 :goto_a

    nop

    :goto_3a
    invoke-virtual {p1, v0, v1}, Ljava/io/PrintWriter;->print(J)V

    :goto_3b
    goto/32 :goto_1e

    nop

    :goto_3c
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_9

    nop

    :goto_3d
    mul-long/2addr v0, v2

    goto/32 :goto_7

    nop

    :goto_3e
    long-to-double v2, p2

    goto/32 :goto_37

    nop

    :goto_3f
    iget-wide v4, p0, Lcom/android/internal/app/procstats/ProcessStats$ProcessDataCollection;->maxPss:J

    goto/32 :goto_1

    nop

    :goto_40
    iget-wide v0, p0, Lcom/android/internal/app/procstats/ProcessStats$ProcessDataCollection;->totalTime:J

    goto/32 :goto_2c

    nop
.end method
