.class Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/android/internal/org/bouncycastle/jcajce/provider/config/ProviderConfiguration;


# static fields
.field private static BC_ADDITIONAL_EC_CURVE_PERMISSION:Ljava/security/Permission;

.field private static BC_DH_LOCAL_PERMISSION:Ljava/security/Permission;

.field private static BC_DH_PERMISSION:Ljava/security/Permission;

.field private static BC_EC_CURVE_PERMISSION:Ljava/security/Permission;

.field private static BC_EC_LOCAL_PERMISSION:Ljava/security/Permission;

.field private static BC_EC_PERMISSION:Ljava/security/Permission;


# instance fields
.field private volatile acceptableNamedCurves:Ljava/util/Set;

.field private volatile additionalECParameters:Ljava/util/Map;

.field private volatile dhDefaultParams:Ljava/lang/Object;

.field private dhThreadSpec:Ljava/lang/ThreadLocal;

.field private volatile ecImplicitCaParams:Lcom/android/internal/org/bouncycastle/jce/spec/ECParameterSpec;

.field private ecThreadSpec:Ljava/lang/ThreadLocal;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/android/internal/org/bouncycastle/jcajce/provider/config/ProviderConfigurationPermission;

    const-string v1, "BC"

    const-string/jumbo v2, "threadLocalEcImplicitlyCa"

    invoke-direct {v0, v1, v2}, Lcom/android/internal/org/bouncycastle/jcajce/provider/config/ProviderConfigurationPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->BC_EC_LOCAL_PERMISSION:Ljava/security/Permission;

    new-instance v0, Lcom/android/internal/org/bouncycastle/jcajce/provider/config/ProviderConfigurationPermission;

    const-string v2, "ecImplicitlyCa"

    invoke-direct {v0, v1, v2}, Lcom/android/internal/org/bouncycastle/jcajce/provider/config/ProviderConfigurationPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->BC_EC_PERMISSION:Ljava/security/Permission;

    new-instance v0, Lcom/android/internal/org/bouncycastle/jcajce/provider/config/ProviderConfigurationPermission;

    const-string/jumbo v2, "threadLocalDhDefaultParams"

    invoke-direct {v0, v1, v2}, Lcom/android/internal/org/bouncycastle/jcajce/provider/config/ProviderConfigurationPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->BC_DH_LOCAL_PERMISSION:Ljava/security/Permission;

    new-instance v0, Lcom/android/internal/org/bouncycastle/jcajce/provider/config/ProviderConfigurationPermission;

    const-string v2, "DhDefaultParams"

    invoke-direct {v0, v1, v2}, Lcom/android/internal/org/bouncycastle/jcajce/provider/config/ProviderConfigurationPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->BC_DH_PERMISSION:Ljava/security/Permission;

    new-instance v0, Lcom/android/internal/org/bouncycastle/jcajce/provider/config/ProviderConfigurationPermission;

    const-string v2, "acceptableEcCurves"

    invoke-direct {v0, v1, v2}, Lcom/android/internal/org/bouncycastle/jcajce/provider/config/ProviderConfigurationPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->BC_EC_CURVE_PERMISSION:Ljava/security/Permission;

    new-instance v0, Lcom/android/internal/org/bouncycastle/jcajce/provider/config/ProviderConfigurationPermission;

    const-string v2, "additionalEcParameters"

    invoke-direct {v0, v1, v2}, Lcom/android/internal/org/bouncycastle/jcajce/provider/config/ProviderConfigurationPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->BC_ADDITIONAL_EC_CURVE_PERMISSION:Ljava/security/Permission;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->ecThreadSpec:Ljava/lang/ThreadLocal;

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->dhThreadSpec:Ljava/lang/ThreadLocal;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->acceptableNamedCurves:Ljava/util/Set;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->additionalECParameters:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public getAcceptableNamedCurves()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->acceptableNamedCurves:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getAdditionalECParameters()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->additionalECParameters:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getDHDefaultParameters(I)Ljavax/crypto/spec/DHParameterSpec;
    .locals 4

    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->dhThreadSpec:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->dhDefaultParams:Ljava/lang/Object;

    :cond_0
    instance-of v1, v0, Ljavax/crypto/spec/DHParameterSpec;

    if-eqz v1, :cond_2

    move-object v1, v0

    check-cast v1, Ljavax/crypto/spec/DHParameterSpec;

    invoke-virtual {v1}, Ljavax/crypto/spec/DHParameterSpec;->getP()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigInteger;->bitLength()I

    move-result v2

    if-ne v2, p1, :cond_1

    return-object v1

    :cond_1
    goto :goto_1

    :cond_2
    instance-of v1, v0, [Ljavax/crypto/spec/DHParameterSpec;

    if-eqz v1, :cond_4

    move-object v1, v0

    check-cast v1, [Ljavax/crypto/spec/DHParameterSpec;

    const/4 v2, 0x0

    :goto_0
    array-length v3, v1

    if-eq v2, v3, :cond_4

    aget-object v3, v1, v2

    invoke-virtual {v3}, Ljavax/crypto/spec/DHParameterSpec;->getP()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v3}, Ljava/math/BigInteger;->bitLength()I

    move-result v3

    if-ne v3, p1, :cond_3

    aget-object v3, v1, v2

    return-object v3

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    :goto_1
    sget-object v1, Lcom/android/internal/org/bouncycastle/crypto/CryptoServicesRegistrar$Property;->DH_DEFAULT_PARAMS:Lcom/android/internal/org/bouncycastle/crypto/CryptoServicesRegistrar$Property;

    invoke-static {v1, p1}, Lcom/android/internal/org/bouncycastle/crypto/CryptoServicesRegistrar;->getSizedProperty(Lcom/android/internal/org/bouncycastle/crypto/CryptoServicesRegistrar$Property;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/internal/org/bouncycastle/crypto/params/DHParameters;

    if-eqz v1, :cond_5

    new-instance v2, Lcom/android/internal/org/bouncycastle/jcajce/spec/DHDomainParameterSpec;

    invoke-direct {v2, v1}, Lcom/android/internal/org/bouncycastle/jcajce/spec/DHDomainParameterSpec;-><init>(Lcom/android/internal/org/bouncycastle/crypto/params/DHParameters;)V

    return-object v2

    :cond_5
    const/4 v2, 0x0

    return-object v2
.end method

.method public getDSADefaultParameters(I)Ljava/security/spec/DSAParameterSpec;
    .locals 5

    sget-object v0, Lcom/android/internal/org/bouncycastle/crypto/CryptoServicesRegistrar$Property;->DSA_DEFAULT_PARAMS:Lcom/android/internal/org/bouncycastle/crypto/CryptoServicesRegistrar$Property;

    invoke-static {v0, p1}, Lcom/android/internal/org/bouncycastle/crypto/CryptoServicesRegistrar;->getSizedProperty(Lcom/android/internal/org/bouncycastle/crypto/CryptoServicesRegistrar$Property;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/org/bouncycastle/crypto/params/DSAParameters;

    if-eqz v0, :cond_0

    new-instance v1, Ljava/security/spec/DSAParameterSpec;

    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/crypto/params/DSAParameters;->getP()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/crypto/params/DSAParameters;->getQ()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/crypto/params/DSAParameters;->getG()Ljava/math/BigInteger;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Ljava/security/spec/DSAParameterSpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    return-object v1

    :cond_0
    const/4 v1, 0x0

    return-object v1
.end method

.method public getEcImplicitlyCa()Lcom/android/internal/org/bouncycastle/jce/spec/ECParameterSpec;
    .locals 2

    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->ecThreadSpec:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/org/bouncycastle/jce/spec/ECParameterSpec;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->ecImplicitCaParams:Lcom/android/internal/org/bouncycastle/jce/spec/ECParameterSpec;

    return-object v1
.end method

.method setParameter(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3

    goto/32 :goto_5

    nop

    :goto_0
    sget-object v1, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->BC_ADDITIONAL_EC_CURVE_PERMISSION:Ljava/security/Permission;

    goto/32 :goto_2f

    nop

    :goto_1
    if-nez v1, :cond_0

    goto/32 :goto_5f

    :cond_0
    goto/32 :goto_52

    nop

    :goto_2
    return-void

    :goto_3
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto/32 :goto_54

    nop

    :goto_4
    if-nez v1, :cond_1

    goto/32 :goto_58

    :cond_1
    goto/32 :goto_8

    nop

    :goto_5
    invoke-static {}, Ljava/lang/System;->getSecurityManager()Ljava/lang/SecurityManager;

    move-result-object v0

    goto/32 :goto_80

    nop

    :goto_6
    goto/16 :goto_4b

    :goto_7
    goto/32 :goto_66

    nop

    :goto_8
    if-nez v0, :cond_2

    goto/32 :goto_69

    :cond_2
    goto/32 :goto_47

    nop

    :goto_9
    if-nez v0, :cond_3

    goto/32 :goto_1c

    :cond_3
    goto/32 :goto_7d

    nop

    :goto_a
    if-eqz p2, :cond_4

    goto/32 :goto_6f

    :cond_4
    goto/32 :goto_6e

    nop

    :goto_b
    if-nez v0, :cond_5

    goto/32 :goto_19

    :cond_5
    goto/32 :goto_81

    nop

    :goto_c
    check-cast v1, Ljava/security/spec/ECParameterSpec;

    goto/32 :goto_5a

    nop

    :goto_d
    check-cast v1, Lcom/android/internal/org/bouncycastle/jce/spec/ECParameterSpec;

    goto/32 :goto_3c

    nop

    :goto_e
    sget-object v1, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->BC_EC_LOCAL_PERMISSION:Ljava/security/Permission;

    goto/32 :goto_5c

    nop

    :goto_f
    if-nez v1, :cond_6

    goto/32 :goto_7

    :cond_6
    goto/32 :goto_9

    nop

    :goto_10
    invoke-virtual {v2, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    :goto_11
    goto/32 :goto_6a

    nop

    :goto_12
    if-eqz v1, :cond_7

    goto/32 :goto_50

    :cond_7
    goto/32 :goto_43

    nop

    :goto_13
    instance-of v1, p2, [Ljavax/crypto/spec/DHParameterSpec;

    goto/32 :goto_79

    nop

    :goto_14
    goto/16 :goto_41

    :goto_15
    goto/32 :goto_53

    nop

    :goto_16
    move-object v1, p2

    goto/32 :goto_49

    nop

    :goto_17
    const-string v1, "DhDefaultParams"

    goto/32 :goto_32

    nop

    :goto_18
    invoke-virtual {v0, v1}, Ljava/lang/SecurityManager;->checkPermission(Ljava/security/Permission;)V

    :goto_19
    goto/32 :goto_51

    nop

    :goto_1a
    iget-object v2, p0, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->dhThreadSpec:Ljava/lang/ThreadLocal;

    goto/32 :goto_46

    nop

    :goto_1b
    invoke-virtual {v0, v1}, Ljava/lang/SecurityManager;->checkPermission(Ljava/security/Permission;)V

    :goto_1c
    goto/32 :goto_20

    nop

    :goto_1d
    invoke-static {v1}, Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/util/EC5Util;->convertSpec(Ljava/security/spec/ECParameterSpec;)Lcom/android/internal/org/bouncycastle/jce/spec/ECParameterSpec;

    move-result-object v1

    goto/32 :goto_23

    nop

    :goto_1e
    move-object v1, p2

    goto/32 :goto_7c

    nop

    :goto_1f
    iput-object p2, p0, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->dhDefaultParams:Ljava/lang/Object;

    goto/32 :goto_60

    nop

    :goto_20
    instance-of v1, p2, Lcom/android/internal/org/bouncycastle/jce/spec/ECParameterSpec;

    goto/32 :goto_2e

    nop

    :goto_21
    goto :goto_11

    :goto_22
    goto/32 :goto_59

    nop

    :goto_23
    goto/16 :goto_3f

    :goto_24
    goto/32 :goto_42

    nop

    :goto_25
    const-string v1, "acceptableEcCurves"

    goto/32 :goto_78

    nop

    :goto_26
    new-instance v1, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_7e

    nop

    :goto_27
    if-eqz p2, :cond_8

    goto/32 :goto_15

    :cond_8
    goto/32 :goto_14

    nop

    :goto_28
    if-eqz p2, :cond_9

    goto/32 :goto_2a

    :cond_9
    goto/32 :goto_29

    nop

    :goto_29
    goto/16 :goto_6d

    :goto_2a
    goto/32 :goto_3a

    nop

    :goto_2b
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto/32 :goto_f

    nop

    :goto_2c
    if-nez v1, :cond_a

    goto/32 :goto_61

    :cond_a
    goto/32 :goto_b

    nop

    :goto_2d
    instance-of v1, p2, [Ljavax/crypto/spec/DHParameterSpec;

    goto/32 :goto_12

    nop

    :goto_2e
    if-eqz v1, :cond_b

    goto/32 :goto_6d

    :cond_b
    goto/32 :goto_28

    nop

    :goto_2f
    invoke-virtual {v0, v1}, Ljava/lang/SecurityManager;->checkPermission(Ljava/security/Permission;)V

    :goto_30
    goto/32 :goto_1e

    nop

    :goto_31
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_40

    nop

    :goto_32
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto/32 :goto_2c

    nop

    :goto_33
    goto/16 :goto_4e

    :goto_34
    goto/32 :goto_76

    nop

    :goto_35
    move-object v1, p2

    goto/32 :goto_3b

    nop

    :goto_36
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto/32 :goto_65

    nop

    :goto_37
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_4f

    nop

    :goto_38
    if-eqz v1, :cond_c

    goto/32 :goto_50

    :cond_c
    goto/32 :goto_2d

    nop

    :goto_39
    if-nez v0, :cond_d

    goto/32 :goto_30

    :cond_d
    goto/32 :goto_0

    nop

    :goto_3a
    move-object v1, p2

    goto/32 :goto_c

    nop

    :goto_3b
    check-cast v1, Ljava/security/spec/ECParameterSpec;

    goto/32 :goto_1d

    nop

    :goto_3c
    iput-object v1, p0, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->ecImplicitCaParams:Lcom/android/internal/org/bouncycastle/jce/spec/ECParameterSpec;

    goto/32 :goto_6

    nop

    :goto_3d
    if-eqz v1, :cond_e

    goto/32 :goto_22

    :cond_e
    goto/32 :goto_7f

    nop

    :goto_3e
    check-cast v1, Lcom/android/internal/org/bouncycastle/jce/spec/ECParameterSpec;

    :goto_3f
    goto/32 :goto_3d

    nop

    :goto_40
    throw v1

    :goto_41
    goto/32 :goto_16

    nop

    :goto_42
    move-object v1, p2

    goto/32 :goto_3e

    nop

    :goto_43
    if-eqz p2, :cond_f

    goto/32 :goto_7b

    :cond_f
    goto/32 :goto_7a

    nop

    :goto_44
    invoke-virtual {v0, v1}, Ljava/lang/SecurityManager;->checkPermission(Ljava/security/Permission;)V

    :goto_45
    goto/32 :goto_64

    nop

    :goto_46
    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->remove()V

    goto/32 :goto_33

    nop

    :goto_47
    sget-object v1, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->BC_EC_CURVE_PERMISSION:Ljava/security/Permission;

    goto/32 :goto_68

    nop

    :goto_48
    sget-object v1, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->BC_DH_LOCAL_PERMISSION:Ljava/security/Permission;

    goto/32 :goto_44

    nop

    :goto_49
    if-eqz v1, :cond_10

    goto/32 :goto_34

    :cond_10
    goto/32 :goto_1a

    nop

    :goto_4a
    iput-object v1, p0, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->additionalECParameters:Ljava/util/Map;

    :goto_4b
    goto/32 :goto_2

    nop

    :goto_4c
    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->remove()V

    goto/32 :goto_21

    nop

    :goto_4d
    invoke-virtual {v2, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    :goto_4e
    goto/32 :goto_5e

    nop

    :goto_4f
    throw v1

    :goto_50
    goto/32 :goto_1f

    nop

    :goto_51
    instance-of v1, p2, Ljavax/crypto/spec/DHParameterSpec;

    goto/32 :goto_38

    nop

    :goto_52
    if-nez v0, :cond_11

    goto/32 :goto_45

    :cond_11
    goto/32 :goto_48

    nop

    :goto_53
    new-instance v1, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_77

    nop

    :goto_54
    if-nez v1, :cond_12

    goto/32 :goto_4b

    :cond_12
    goto/32 :goto_39

    nop

    :goto_55
    if-nez v0, :cond_13

    goto/32 :goto_5d

    :cond_13
    goto/32 :goto_e

    nop

    :goto_56
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto/32 :goto_1

    nop

    :goto_57
    goto :goto_4b

    :goto_58
    goto/32 :goto_67

    nop

    :goto_59
    iget-object v2, p0, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->ecThreadSpec:Ljava/lang/ThreadLocal;

    goto/32 :goto_10

    nop

    :goto_5a
    invoke-static {v1}, Lcom/android/internal/org/bouncycastle/jcajce/provider/asymmetric/util/EC5Util;->convertSpec(Ljava/security/spec/ECParameterSpec;)Lcom/android/internal/org/bouncycastle/jce/spec/ECParameterSpec;

    move-result-object v1

    goto/32 :goto_74

    nop

    :goto_5b
    iput-object v1, p0, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->acceptableNamedCurves:Ljava/util/Set;

    goto/32 :goto_57

    nop

    :goto_5c
    invoke-virtual {v0, v1}, Ljava/lang/SecurityManager;->checkPermission(Ljava/security/Permission;)V

    :goto_5d
    goto/32 :goto_72

    nop

    :goto_5e
    goto :goto_4b

    :goto_5f
    goto/32 :goto_17

    nop

    :goto_60
    goto :goto_4b

    :goto_61
    goto/32 :goto_25

    nop

    :goto_62
    move-object v1, p2

    goto/32 :goto_63

    nop

    :goto_63
    check-cast v1, Ljava/util/Set;

    goto/32 :goto_5b

    nop

    :goto_64
    instance-of v1, p2, Ljavax/crypto/spec/DHParameterSpec;

    goto/32 :goto_75

    nop

    :goto_65
    if-nez v1, :cond_14

    goto/32 :goto_6b

    :cond_14
    goto/32 :goto_55

    nop

    :goto_66
    const-string/jumbo v1, "threadLocalDhDefaultParams"

    goto/32 :goto_56

    nop

    :goto_67
    const-string v1, "additionalEcParameters"

    goto/32 :goto_3

    nop

    :goto_68
    invoke-virtual {v0, v1}, Ljava/lang/SecurityManager;->checkPermission(Ljava/security/Permission;)V

    :goto_69
    goto/32 :goto_62

    nop

    :goto_6a
    goto/16 :goto_4b

    :goto_6b
    goto/32 :goto_71

    nop

    :goto_6c
    goto/16 :goto_4b

    :goto_6d
    goto/32 :goto_73

    nop

    :goto_6e
    goto/16 :goto_24

    :goto_6f
    goto/32 :goto_35

    nop

    :goto_70
    if-eqz v1, :cond_15

    goto/32 :goto_24

    :cond_15
    goto/32 :goto_a

    nop

    :goto_71
    const-string v1, "ecImplicitlyCa"

    goto/32 :goto_2b

    nop

    :goto_72
    instance-of v1, p2, Lcom/android/internal/org/bouncycastle/jce/spec/ECParameterSpec;

    goto/32 :goto_70

    nop

    :goto_73
    move-object v1, p2

    goto/32 :goto_d

    nop

    :goto_74
    iput-object v1, p0, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->ecImplicitCaParams:Lcom/android/internal/org/bouncycastle/jce/spec/ECParameterSpec;

    goto/32 :goto_6c

    nop

    :goto_75
    if-eqz v1, :cond_16

    goto/32 :goto_41

    :cond_16
    goto/32 :goto_13

    nop

    :goto_76
    iget-object v2, p0, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->dhThreadSpec:Ljava/lang/ThreadLocal;

    goto/32 :goto_4d

    nop

    :goto_77
    const-string/jumbo v2, "not a valid DHParameterSpec"

    goto/32 :goto_31

    nop

    :goto_78
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto/32 :goto_4

    nop

    :goto_79
    if-eqz v1, :cond_17

    goto/32 :goto_41

    :cond_17
    goto/32 :goto_27

    nop

    :goto_7a
    goto/16 :goto_50

    :goto_7b
    goto/32 :goto_26

    nop

    :goto_7c
    check-cast v1, Ljava/util/Map;

    goto/32 :goto_4a

    nop

    :goto_7d
    sget-object v1, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->BC_EC_PERMISSION:Ljava/security/Permission;

    goto/32 :goto_1b

    nop

    :goto_7e
    const-string/jumbo v2, "not a valid DHParameterSpec or DHParameterSpec[]"

    goto/32 :goto_37

    nop

    :goto_7f
    iget-object v2, p0, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->ecThreadSpec:Ljava/lang/ThreadLocal;

    goto/32 :goto_4c

    nop

    :goto_80
    const-string/jumbo v1, "threadLocalEcImplicitlyCa"

    goto/32 :goto_36

    nop

    :goto_81
    sget-object v1, Lcom/android/internal/org/bouncycastle/jce/provider/BouncyCastleProviderConfiguration;->BC_DH_PERMISSION:Ljava/security/Permission;

    goto/32 :goto_18

    nop
.end method
