.class Lcom/android/internal/os/LooperStats$Entry;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/os/LooperStats;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Entry"
.end annotation


# instance fields
.field public cpuUsageMicro:J

.field public delayMillis:J

.field public exceptionCount:J

.field public final handler:Landroid/os/Handler;

.field public final isInteractive:Z

.field public maxCpuUsageMicro:J

.field public maxDelayMillis:J

.field public maxLatencyMicro:J

.field public messageCount:J

.field public final messageName:Ljava/lang/String;

.field public recordedDelayMessageCount:J

.field public recordedMessageCount:J

.field public totalLatencyMicro:J

.field public final workSourceUid:I


# direct methods
.method constructor <init>(Landroid/os/Message;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget v0, p1, Landroid/os/Message;->workSourceUid:I

    iput v0, p0, Lcom/android/internal/os/LooperStats$Entry;->workSourceUid:I

    invoke-virtual {p1}, Landroid/os/Message;->getTarget()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/android/internal/os/LooperStats$Entry;->handler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->getMessageName(Landroid/os/Message;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/internal/os/LooperStats$Entry;->messageName:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/android/internal/os/LooperStats$Entry;->isInteractive:Z

    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/os/LooperStats$Entry;->workSourceUid:I

    iput-object p1, p0, Lcom/android/internal/os/LooperStats$Entry;->messageName:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/internal/os/LooperStats$Entry;->handler:Landroid/os/Handler;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/internal/os/LooperStats$Entry;->isInteractive:Z

    return-void
.end method

.method static idFor(Landroid/os/Message;Z)I
    .locals 3

    const/4 v0, 0x7

    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Landroid/os/Message;->workSourceUid:I

    add-int/2addr v1, v2

    mul-int/lit8 v0, v1, 0x1f

    invoke-virtual {p0}, Landroid/os/Message;->getTarget()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v1, v0, 0x1f

    invoke-virtual {p0}, Landroid/os/Message;->getTarget()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    mul-int/lit8 v0, v1, 0x1f

    if-eqz p1, :cond_0

    const/16 v2, 0x4cf

    goto :goto_0

    :cond_0
    const/16 v2, 0x4d5

    :goto_0
    add-int/2addr v0, v2

    invoke-virtual {p0}, Landroid/os/Message;->getCallback()Ljava/lang/Runnable;

    move-result-object v1

    if-eqz v1, :cond_1

    mul-int/lit8 v1, v0, 0x1f

    invoke-virtual {p0}, Landroid/os/Message;->getCallback()Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int/2addr v1, v2

    return v1

    :cond_1
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Landroid/os/Message;->what:I

    add-int/2addr v1, v2

    return v1
.end method


# virtual methods
.method reset()V
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    iput-wide v0, p0, Lcom/android/internal/os/LooperStats$Entry;->messageCount:J

    goto/32 :goto_1

    nop

    :goto_1
    iput-wide v0, p0, Lcom/android/internal/os/LooperStats$Entry;->recordedMessageCount:J

    goto/32 :goto_2

    nop

    :goto_2
    iput-wide v0, p0, Lcom/android/internal/os/LooperStats$Entry;->exceptionCount:J

    goto/32 :goto_a

    nop

    :goto_3
    iput-wide v0, p0, Lcom/android/internal/os/LooperStats$Entry;->maxLatencyMicro:J

    goto/32 :goto_7

    nop

    :goto_4
    iput-wide v0, p0, Lcom/android/internal/os/LooperStats$Entry;->maxCpuUsageMicro:J

    goto/32 :goto_6

    nop

    :goto_5
    const-wide/16 v0, 0x0

    goto/32 :goto_0

    nop

    :goto_6
    iput-wide v0, p0, Lcom/android/internal/os/LooperStats$Entry;->delayMillis:J

    goto/32 :goto_8

    nop

    :goto_7
    iput-wide v0, p0, Lcom/android/internal/os/LooperStats$Entry;->cpuUsageMicro:J

    goto/32 :goto_4

    nop

    :goto_8
    iput-wide v0, p0, Lcom/android/internal/os/LooperStats$Entry;->maxDelayMillis:J

    goto/32 :goto_b

    nop

    :goto_9
    return-void

    :goto_a
    iput-wide v0, p0, Lcom/android/internal/os/LooperStats$Entry;->totalLatencyMicro:J

    goto/32 :goto_3

    nop

    :goto_b
    iput-wide v0, p0, Lcom/android/internal/os/LooperStats$Entry;->recordedDelayMessageCount:J

    goto/32 :goto_9

    nop
.end method
