.class public final Lcom/android/internal/R$color;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final accessibility_focus_highlight_color:I = 0x106007f

.field public static final background_dark:I = 0x106000e

.field public static final background_light:I = 0x106000f

.field public static final black:I = 0x106000c

.field public static final call_notification_answer_color:I = 0x10600af

.field public static final call_notification_decline_color:I = 0x10600b0

.field public static final camera_privacy_light_day:I = 0x10600b1

.field public static final camera_privacy_light_night:I = 0x10600b2

.field public static final car_body1_light:I = 0x10600c8

.field public static final car_card_dark:I = 0x10600d5

.field public static final car_user_switcher_user_image_bgcolor:I = 0x106016d

.field public static final car_user_switcher_user_image_fgcolor:I = 0x106016e

.field public static final chooser_gradient_background:I = 0x106017a

.field public static final chooser_gradient_highlight:I = 0x106017b

.field public static final chooser_row_divider:I = 0x106017c

.field public static final config_defaultNotificationColor:I = 0x106017d

.field public static final config_letterboxBackgroundColor:I = 0x106017e

.field public static final conversation_important_highlight:I = 0x1060183

.field public static final darker_gray:I = 0x1060000

.field public static final decor_button_dark_color:I = 0x1060194

.field public static final decor_button_light_color:I = 0x1060195

.field public static final decor_view_status_guard:I = 0x1060196

.field public static final decor_view_status_guard_light:I = 0x1060197

.field public static final default_magnifier_color_overlay:I = 0x1060198

.field public static final holo_blue_bright:I = 0x106001b

.field public static final holo_blue_dark:I = 0x1060013

.field public static final holo_blue_light:I = 0x1060012

.field public static final holo_green_dark:I = 0x1060015

.field public static final holo_green_light:I = 0x1060014

.field public static final holo_orange_dark:I = 0x1060019

.field public static final holo_orange_light:I = 0x1060018

.field public static final holo_purple:I = 0x106001a

.field public static final holo_red_dark:I = 0x1060017

.field public static final holo_red_light:I = 0x1060016

.field public static final instant_app_badge:I = 0x10601d4

.field public static final lock_pattern_view_regular_color:I = 0x1060201

.field public static final lock_pattern_view_success_color:I = 0x1060202

.field public static final material_grey_300:I = 0x1060212

.field public static final notification_action_list:I = 0x106021e

.field public static final notification_default_color_dark:I = 0x1060222

.field public static final notification_default_color_light:I = 0x1060223

.field public static final notification_primary_text_color_dark:I = 0x1060225

.field public static final notification_primary_text_color_light:I = 0x1060226

.field public static final notification_progress_background_color:I = 0x1060227

.field public static final notification_secondary_text_color_dark:I = 0x1060229

.field public static final notification_secondary_text_color_light:I = 0x106022a

.field public static final overview_background:I = 0x106022b

.field public static final personal_apps_suspension_notification_color:I = 0x106022f

.field public static final primary_text_dark:I = 0x1060001
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final primary_text_dark_nodisable:I = 0x1060002
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final primary_text_light:I = 0x1060003
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final primary_text_light_nodisable:I = 0x1060004
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final profile_badge_1:I = 0x1060257

.field public static final profile_badge_1_dark:I = 0x1060258

.field public static final profile_badge_2:I = 0x1060259

.field public static final profile_badge_2_dark:I = 0x106025a

.field public static final profile_badge_3:I = 0x106025b

.field public static final profile_badge_3_dark:I = 0x106025c

.field public static final resize_shadow_end_color:I = 0x1060261

.field public static final resize_shadow_start_color:I = 0x1060262

.field public static final secondary_text_dark:I = 0x1060005
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final secondary_text_dark_nodisable:I = 0x1060006
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final secondary_text_light:I = 0x1060007
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final secondary_text_light_nodisable:I = 0x1060008
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final system_accent1_0:I = 0x1060037

.field public static final system_accent1_10:I = 0x1060038

.field public static final system_accent1_100:I = 0x106003a

.field public static final system_accent1_1000:I = 0x1060043

.field public static final system_accent1_200:I = 0x106003b

.field public static final system_accent1_300:I = 0x106003c

.field public static final system_accent1_400:I = 0x106003d

.field public static final system_accent1_50:I = 0x1060039

.field public static final system_accent1_500:I = 0x106003e

.field public static final system_accent1_600:I = 0x106003f

.field public static final system_accent1_700:I = 0x1060040

.field public static final system_accent1_800:I = 0x1060041

.field public static final system_accent1_900:I = 0x1060042

.field public static final system_accent2_0:I = 0x1060044

.field public static final system_accent2_10:I = 0x1060045

.field public static final system_accent2_100:I = 0x1060047

.field public static final system_accent2_1000:I = 0x1060050

.field public static final system_accent2_200:I = 0x1060048

.field public static final system_accent2_300:I = 0x1060049

.field public static final system_accent2_400:I = 0x106004a

.field public static final system_accent2_50:I = 0x1060046

.field public static final system_accent2_500:I = 0x106004b

.field public static final system_accent2_600:I = 0x106004c

.field public static final system_accent2_700:I = 0x106004d

.field public static final system_accent2_800:I = 0x106004e

.field public static final system_accent2_900:I = 0x106004f

.field public static final system_accent3_0:I = 0x1060051

.field public static final system_accent3_10:I = 0x1060052

.field public static final system_accent3_100:I = 0x1060054

.field public static final system_accent3_1000:I = 0x106005d

.field public static final system_accent3_200:I = 0x1060055

.field public static final system_accent3_300:I = 0x1060056

.field public static final system_accent3_400:I = 0x1060057

.field public static final system_accent3_50:I = 0x1060053

.field public static final system_accent3_500:I = 0x1060058

.field public static final system_accent3_600:I = 0x1060059

.field public static final system_accent3_700:I = 0x106005a

.field public static final system_accent3_800:I = 0x106005b

.field public static final system_accent3_900:I = 0x106005c

.field public static final system_bar_background_semi_transparent:I = 0x106029d

.field public static final system_neutral1_0:I = 0x106001d

.field public static final system_neutral1_10:I = 0x106001e

.field public static final system_neutral1_100:I = 0x1060020

.field public static final system_neutral1_1000:I = 0x1060029

.field public static final system_neutral1_200:I = 0x1060021

.field public static final system_neutral1_300:I = 0x1060022

.field public static final system_neutral1_400:I = 0x1060023

.field public static final system_neutral1_50:I = 0x106001f

.field public static final system_neutral1_500:I = 0x1060024

.field public static final system_neutral1_600:I = 0x1060025

.field public static final system_neutral1_700:I = 0x1060026

.field public static final system_neutral1_800:I = 0x1060027

.field public static final system_neutral1_900:I = 0x1060028

.field public static final system_neutral2_0:I = 0x106002a

.field public static final system_neutral2_10:I = 0x106002b

.field public static final system_neutral2_100:I = 0x106002d

.field public static final system_neutral2_1000:I = 0x1060036

.field public static final system_neutral2_200:I = 0x106002e

.field public static final system_neutral2_300:I = 0x106002f

.field public static final system_neutral2_400:I = 0x1060030

.field public static final system_neutral2_50:I = 0x106002c

.field public static final system_neutral2_500:I = 0x1060031

.field public static final system_neutral2_600:I = 0x1060032

.field public static final system_neutral2_700:I = 0x1060033

.field public static final system_neutral2_800:I = 0x1060034

.field public static final system_neutral2_900:I = 0x1060035

.field public static final system_notification_accent_color:I = 0x106001c

.field public static final tab_indicator_text:I = 0x1060009

.field public static final tab_indicator_text_v4:I = 0x10602a1

.field public static final tertiary_text_dark:I = 0x1060010
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final tertiary_text_light:I = 0x1060011
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final text_color_primary:I = 0x10602a7

.field public static final timepicker_default_ampm_selected_background_color_material:I = 0x10602af

.field public static final timepicker_default_ampm_unselected_background_color_material:I = 0x10602b0

.field public static final timepicker_default_numbers_background_color_material:I = 0x10602b2

.field public static final timepicker_default_selector_color_material:I = 0x10602b3

.field public static final timepicker_default_text_color_material:I = 0x10602b4

.field public static final transparent:I = 0x106000d

.field public static final user_icon_1:I = 0x10602b8

.field public static final user_icon_2:I = 0x10602b9

.field public static final user_icon_3:I = 0x10602ba

.field public static final user_icon_4:I = 0x10602bb

.field public static final user_icon_5:I = 0x10602bc

.field public static final user_icon_6:I = 0x10602bd

.field public static final user_icon_7:I = 0x10602be

.field public static final user_icon_8:I = 0x10602bf

.field public static final user_icon_default_gray:I = 0x10602c0

.field public static final user_icon_default_white:I = 0x10602c1

.field public static final white:I = 0x106000b

.field public static final widget_edittext_dark:I = 0x106000a


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
