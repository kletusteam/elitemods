.class public Lcom/android/internal/org/bouncycastle/asn1/DLTaggedObject;
.super Lcom/android/internal/org/bouncycastle/asn1/ASN1TaggedObject;


# direct methods
.method public constructor <init>(ZILcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1TaggedObject;-><init>(ZILcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    return-void
.end method


# virtual methods
.method encode(Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_12

    nop

    :goto_0
    return-void

    :goto_1
    or-int/lit8 v1, v1, 0x20

    :goto_2
    goto/32 :goto_11

    nop

    :goto_3
    iget-boolean v3, p0, Lcom/android/internal/org/bouncycastle/asn1/DLTaggedObject;->explicit:Z

    goto/32 :goto_8

    nop

    :goto_4
    iget-boolean v2, p0, Lcom/android/internal/org/bouncycastle/asn1/DLTaggedObject;->explicit:Z

    goto/32 :goto_e

    nop

    :goto_5
    const/16 v1, 0x80

    goto/32 :goto_c

    nop

    :goto_6
    if-nez v2, :cond_0

    goto/32 :goto_2

    :cond_0
    :goto_7
    goto/32 :goto_1

    nop

    :goto_8
    invoke-virtual {v2, v0, v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writePrimitive(Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;Z)V

    goto/32 :goto_0

    nop

    :goto_9
    invoke-virtual {p1, v2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeLength(I)V

    :goto_a
    goto/32 :goto_f

    nop

    :goto_b
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->toDLObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_c
    iget-boolean v2, p0, Lcom/android/internal/org/bouncycastle/asn1/DLTaggedObject;->explicit:Z

    goto/32 :goto_14

    nop

    :goto_d
    invoke-virtual {p1, p2, v1, v2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeTag(ZII)V

    goto/32 :goto_4

    nop

    :goto_e
    if-nez v2, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_13

    nop

    :goto_f
    invoke-virtual {p1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->getDLSubStream()Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;

    move-result-object v2

    goto/32 :goto_3

    nop

    :goto_10
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->isConstructed()Z

    move-result v2

    goto/32 :goto_6

    nop

    :goto_11
    iget v2, p0, Lcom/android/internal/org/bouncycastle/asn1/DLTaggedObject;->tagNo:I

    goto/32 :goto_d

    nop

    :goto_12
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DLTaggedObject;->obj:Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_15

    nop

    :goto_13
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->encodedLength()I

    move-result v2

    goto/32 :goto_9

    nop

    :goto_14
    if-eqz v2, :cond_2

    goto/32 :goto_7

    :cond_2
    goto/32 :goto_10

    nop

    :goto_15
    invoke-interface {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    goto/32 :goto_b

    nop
.end method

.method encodedLength()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DLTaggedObject;->obj:Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_2

    nop

    :goto_1
    invoke-static {v1}, Lcom/android/internal/org/bouncycastle/asn1/StreamUtil;->calculateTagLength(I)I

    move-result v1

    goto/32 :goto_f

    nop

    :goto_2
    invoke-interface {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_3
    iget v1, p0, Lcom/android/internal/org/bouncycastle/asn1/DLTaggedObject;->tagNo:I

    goto/32 :goto_1

    nop

    :goto_4
    return v1

    :goto_5
    goto/32 :goto_c

    nop

    :goto_6
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->toDLObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    goto/32 :goto_b

    nop

    :goto_7
    iget-boolean v1, p0, Lcom/android/internal/org/bouncycastle/asn1/DLTaggedObject;->explicit:Z

    goto/32 :goto_9

    nop

    :goto_8
    iget v1, p0, Lcom/android/internal/org/bouncycastle/asn1/DLTaggedObject;->tagNo:I

    goto/32 :goto_d

    nop

    :goto_9
    if-nez v1, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_8

    nop

    :goto_a
    add-int/2addr v1, v0

    goto/32 :goto_4

    nop

    :goto_b
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->encodedLength()I

    move-result v0

    goto/32 :goto_7

    nop

    :goto_c
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_3

    nop

    :goto_d
    invoke-static {v1}, Lcom/android/internal/org/bouncycastle/asn1/StreamUtil;->calculateTagLength(I)I

    move-result v1

    goto/32 :goto_10

    nop

    :goto_e
    add-int/2addr v1, v2

    goto/32 :goto_a

    nop

    :goto_f
    add-int/2addr v1, v0

    goto/32 :goto_11

    nop

    :goto_10
    invoke-static {v0}, Lcom/android/internal/org/bouncycastle/asn1/StreamUtil;->calculateBodyLength(I)I

    move-result v2

    goto/32 :goto_e

    nop

    :goto_11
    return v1
.end method

.method isConstructed()Z
    .locals 1

    goto/32 :goto_7

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_b

    nop

    :goto_1
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->isConstructed()Z

    move-result v0

    goto/32 :goto_0

    nop

    :goto_2
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->toDLObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_3
    const/4 v0, 0x1

    :goto_4
    goto/32 :goto_9

    nop

    :goto_5
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DLTaggedObject;->obj:Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_8

    nop

    :goto_6
    const/4 v0, 0x0

    goto/32 :goto_d

    nop

    :goto_7
    iget-boolean v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DLTaggedObject;->explicit:Z

    goto/32 :goto_a

    nop

    :goto_8
    invoke-interface {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_9
    return v0

    :goto_a
    if-eqz v0, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_5

    nop

    :goto_b
    goto :goto_e

    :goto_c
    goto/32 :goto_6

    nop

    :goto_d
    goto :goto_4

    :goto_e
    goto/32 :goto_3

    nop
.end method

.method toDLObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    return-object p0
.end method
