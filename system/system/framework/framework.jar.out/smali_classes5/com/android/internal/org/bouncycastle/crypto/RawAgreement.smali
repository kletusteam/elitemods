.class public interface abstract Lcom/android/internal/org/bouncycastle/crypto/RawAgreement;
.super Ljava/lang/Object;


# virtual methods
.method public abstract calculateAgreement(Lcom/android/internal/org/bouncycastle/crypto/CipherParameters;[BI)V
.end method

.method public abstract getAgreementSize()I
.end method

.method public abstract init(Lcom/android/internal/org/bouncycastle/crypto/CipherParameters;)V
.end method
