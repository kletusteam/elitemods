.class public Lcom/android/internal/os/BatteryStatsHistoryIterator;
.super Ljava/lang/Object;


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "BatteryStatsHistoryItr"


# instance fields
.field private final mBatteryStatsHistory:Lcom/android/internal/os/BatteryStatsHistory;

.field private final mHistoryTags:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Landroid/os/BatteryStats$HistoryTag;",
            ">;"
        }
    .end annotation
.end field

.field private final mReadHistoryStepDetails:Landroid/os/BatteryStats$HistoryStepDetails;


# direct methods
.method public constructor <init>(Lcom/android/internal/os/BatteryStatsHistory;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/BatteryStats$HistoryStepDetails;

    invoke-direct {v0}, Landroid/os/BatteryStats$HistoryStepDetails;-><init>()V

    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsHistoryIterator;->mReadHistoryStepDetails:Landroid/os/BatteryStats$HistoryStepDetails;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsHistoryIterator;->mHistoryTags:Landroid/util/SparseArray;

    iput-object p1, p0, Lcom/android/internal/os/BatteryStatsHistoryIterator;->mBatteryStatsHistory:Lcom/android/internal/os/BatteryStatsHistory;

    invoke-virtual {p1}, Lcom/android/internal/os/BatteryStatsHistory;->startIteratingHistory()Z

    return-void
.end method

.method private static readBatteryLevelInt(ILandroid/os/BatteryStats$HistoryItem;)V
    .locals 1

    const/high16 v0, -0x2000000

    and-int/2addr v0, p0

    ushr-int/lit8 v0, v0, 0x19

    int-to-byte v0, v0

    iput-byte v0, p1, Landroid/os/BatteryStats$HistoryItem;->batteryLevel:B

    const v0, 0x1ff8000

    and-int/2addr v0, p0

    ushr-int/lit8 v0, v0, 0xf

    int-to-short v0, v0

    iput-short v0, p1, Landroid/os/BatteryStats$HistoryItem;->batteryTemperature:S

    and-int/lit16 v0, p0, 0x7ffe

    ushr-int/lit8 v0, v0, 0x1

    int-to-char v0, v0

    iput-char v0, p1, Landroid/os/BatteryStats$HistoryItem;->batteryVoltage:C

    return-void
.end method

.method private readHistoryTag(Landroid/os/Parcel;ILandroid/os/BatteryStats$HistoryTag;)Z
    .locals 3

    const/4 v0, 0x0

    const v1, 0xffff

    if-ne p2, v1, :cond_0

    return v0

    :cond_0
    const v1, 0x8000

    and-int/2addr v1, p2

    if-eqz v1, :cond_1

    new-instance v0, Landroid/os/BatteryStats$HistoryTag;

    invoke-direct {v0}, Landroid/os/BatteryStats$HistoryTag;-><init>()V

    invoke-virtual {v0, p1}, Landroid/os/BatteryStats$HistoryTag;->readFromParcel(Landroid/os/Parcel;)V

    const v1, -0x8001

    and-int/2addr v1, p2

    iput v1, v0, Landroid/os/BatteryStats$HistoryTag;->poolIdx:I

    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsHistoryIterator;->mHistoryTags:Landroid/util/SparseArray;

    iget v2, v0, Landroid/os/BatteryStats$HistoryTag;->poolIdx:I

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    invoke-virtual {p3, v0}, Landroid/os/BatteryStats$HistoryTag;->setTo(Landroid/os/BatteryStats$HistoryTag;)V

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsHistoryIterator;->mHistoryTags:Landroid/util/SparseArray;

    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/BatteryStats$HistoryTag;

    if-eqz v1, :cond_2

    invoke-virtual {p3, v1}, Landroid/os/BatteryStats$HistoryTag;->setTo(Landroid/os/BatteryStats$HistoryTag;)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    iput-object v2, p3, Landroid/os/BatteryStats$HistoryTag;->string:Ljava/lang/String;

    iput v0, p3, Landroid/os/BatteryStats$HistoryTag;->uid:I

    :goto_0
    iput p2, p3, Landroid/os/BatteryStats$HistoryTag;->poolIdx:I

    :goto_1
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public next(Landroid/os/BatteryStats$HistoryItem;)Z
    .locals 7

    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsHistoryIterator;->mBatteryStatsHistory:Lcom/android/internal/os/BatteryStatsHistory;

    invoke-virtual {v0, p1}, Lcom/android/internal/os/BatteryStatsHistory;->getNextParcel(Landroid/os/BatteryStats$HistoryItem;)Landroid/os/Parcel;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/android/internal/os/BatteryStatsHistoryIterator;->mBatteryStatsHistory:Lcom/android/internal/os/BatteryStatsHistory;

    invoke-virtual {v1}, Lcom/android/internal/os/BatteryStatsHistory;->finishIteratingHistory()V

    const/4 v1, 0x0

    return v1

    :cond_0
    iget-wide v1, p1, Landroid/os/BatteryStats$HistoryItem;->time:J

    iget-wide v3, p1, Landroid/os/BatteryStats$HistoryItem;->currentTime:J

    invoke-virtual {p0, v0, p1}, Lcom/android/internal/os/BatteryStatsHistoryIterator;->readHistoryDelta(Landroid/os/Parcel;Landroid/os/BatteryStats$HistoryItem;)V

    iget-byte v5, p1, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    const/4 v6, 0x5

    if-eq v5, v6, :cond_1

    iget-byte v5, p1, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    const/4 v6, 0x7

    if-eq v5, v6, :cond_1

    const-wide/16 v5, 0x0

    cmp-long v5, v3, v5

    if-eqz v5, :cond_1

    iget-wide v5, p1, Landroid/os/BatteryStats$HistoryItem;->time:J

    sub-long/2addr v5, v1

    add-long/2addr v5, v3

    iput-wide v5, p1, Landroid/os/BatteryStats$HistoryItem;->currentTime:J

    :cond_1
    const/4 v5, 0x1

    return v5
.end method

.method readHistoryDelta(Landroid/os/Parcel;Landroid/os/BatteryStats$HistoryItem;)V
    .locals 11

    goto/32 :goto_29

    nop

    :goto_0
    and-int/lit8 v7, v7, 0x7

    goto/32 :goto_51

    nop

    :goto_1
    goto/16 :goto_58

    :goto_2
    goto/32 :goto_3a

    nop

    :goto_3
    and-int/2addr v4, v0

    goto/32 :goto_74

    nop

    :goto_4
    iget v4, p2, Landroid/os/BatteryStats$HistoryItem;->numReadInts:I

    goto/32 :goto_a0

    nop

    :goto_5
    and-int/lit8 v2, v5, 0x1

    goto/32 :goto_a8

    nop

    :goto_6
    const/4 v4, 0x4

    goto/32 :goto_79

    nop

    :goto_7
    const/high16 v8, -0x2000000

    goto/32 :goto_43

    nop

    :goto_8
    iget-object v10, p2, Landroid/os/BatteryStats$HistoryItem;->localWakeReasonTag:Landroid/os/BatteryStats$HistoryTag;

    goto/32 :goto_85

    nop

    :goto_9
    iput-byte v4, p2, Landroid/os/BatteryStats$HistoryItem;->batteryPlugType:B

    goto/32 :goto_f

    nop

    :goto_a
    shr-int/lit8 v9, v4, 0x10

    goto/32 :goto_52

    nop

    :goto_b
    return-void

    :goto_c
    goto/32 :goto_75

    nop

    :goto_d
    goto/16 :goto_6c

    :goto_e
    goto/32 :goto_6b

    nop

    :goto_f
    goto/16 :goto_8d

    :pswitch_0
    goto/32 :goto_8c

    nop

    :goto_10
    if-nez v4, :cond_0

    goto/32 :goto_1e

    :cond_0
    goto/32 :goto_4f

    nop

    :goto_11
    iput-byte v7, p2, Landroid/os/BatteryStats$HistoryItem;->batteryStatus:B

    goto/32 :goto_4c

    nop

    :goto_12
    add-int/2addr v6, v3

    goto/32 :goto_94

    nop

    :goto_13
    add-int/2addr v6, v3

    goto/32 :goto_80

    nop

    :goto_14
    goto/16 :goto_40

    :goto_15
    goto/32 :goto_3f

    nop

    :goto_16
    iput v3, p2, Landroid/os/BatteryStats$HistoryItem;->numReadInts:I

    goto/32 :goto_1f

    nop

    :goto_17
    if-nez v4, :cond_1

    goto/32 :goto_5f

    :cond_1
    goto/32 :goto_b2

    nop

    :goto_18
    iput-wide v2, p2, Landroid/os/BatteryStats$HistoryItem;->modemRailChargeMah:D

    goto/32 :goto_39

    nop

    :goto_19
    const v5, 0x7fffd

    goto/32 :goto_b5

    nop

    :goto_1a
    shr-int/lit8 v4, v2, 0x10

    goto/32 :goto_88

    nop

    :goto_1b
    goto/16 :goto_6e

    :goto_1c
    goto/32 :goto_62

    nop

    :goto_1d
    goto/16 :goto_78

    :goto_1e
    goto/32 :goto_8f

    nop

    :goto_1f
    const/4 v4, 0x2

    goto/32 :goto_19

    nop

    :goto_20
    iput-object v7, p2, Landroid/os/BatteryStats$HistoryItem;->eventTag:Landroid/os/BatteryStats$HistoryTag;

    :goto_21
    goto/32 :goto_5a

    nop

    :goto_22
    iput-object v2, p2, Landroid/os/BatteryStats$HistoryItem;->stepDetails:Landroid/os/BatteryStats$HistoryStepDetails;

    goto/32 :goto_65

    nop

    :goto_23
    iput-wide v7, p2, Landroid/os/BatteryStats$HistoryItem;->time:J

    goto/32 :goto_46

    nop

    :goto_24
    and-int/2addr v5, v0

    goto/32 :goto_27

    nop

    :goto_25
    const/high16 v4, 0x200000

    goto/32 :goto_a2

    nop

    :goto_26
    iput-wide v5, p2, Landroid/os/BatteryStats$HistoryItem;->time:J

    goto/32 :goto_1b

    nop

    :goto_27
    if-nez v5, :cond_2

    goto/32 :goto_93

    :cond_2
    goto/32 :goto_32

    nop

    :goto_28
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v5

    goto/32 :goto_7e

    nop

    :goto_29
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    goto/32 :goto_7d

    nop

    :goto_2a
    iget-object v6, p2, Landroid/os/BatteryStats$HistoryItem;->localEventTag:Landroid/os/BatteryStats$HistoryTag;

    goto/32 :goto_83

    nop

    :goto_2b
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    goto/32 :goto_60

    nop

    :goto_2c
    const/high16 v4, 0x400000

    goto/32 :goto_7f

    nop

    :goto_2d
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    goto/32 :goto_4a

    nop

    :goto_2e
    iget v6, p2, Landroid/os/BatteryStats$HistoryItem;->numReadInts:I

    goto/32 :goto_13

    nop

    :goto_2f
    and-int/2addr v6, v0

    goto/32 :goto_af

    nop

    :goto_30
    goto/16 :goto_8d

    :pswitch_1
    goto/32 :goto_9

    nop

    :goto_31
    invoke-static {v5, p2}, Lcom/android/internal/os/BatteryStatsHistoryIterator;->readBatteryLevelInt(ILandroid/os/BatteryStats$HistoryItem;)V

    goto/32 :goto_2e

    nop

    :goto_32
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    goto/32 :goto_31

    nop

    :goto_33
    int-to-byte v7, v7

    goto/32 :goto_11

    nop

    :goto_34
    add-long/2addr v5, v7

    goto/32 :goto_26

    nop

    :goto_35
    if-nez v10, :cond_3

    goto/32 :goto_42

    :cond_3
    goto/32 :goto_55

    nop

    :goto_36
    const/4 v2, 0x0

    goto/32 :goto_3e

    nop

    :goto_37
    const/high16 v4, 0x800000

    goto/32 :goto_3

    nop

    :goto_38
    int-to-long v8, v5

    goto/32 :goto_87

    nop

    :goto_39
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v2

    goto/32 :goto_4e

    nop

    :goto_3a
    and-int v4, v0, v8

    goto/32 :goto_50

    nop

    :goto_3b
    int-to-byte v7, v7

    goto/32 :goto_89

    nop

    :goto_3c
    goto/16 :goto_a5

    :goto_3d
    goto/32 :goto_a4

    nop

    :goto_3e
    iput-byte v2, p2, Landroid/os/BatteryStats$HistoryItem;->cmd:B

    goto/32 :goto_68

    nop

    :goto_3f
    iput v2, p2, Landroid/os/BatteryStats$HistoryItem;->eventCode:I

    :goto_40
    goto/32 :goto_5

    nop

    :goto_41
    goto/16 :goto_a7

    :goto_42
    goto/32 :goto_a6

    nop

    :goto_43
    if-nez v6, :cond_4

    goto/32 :goto_2

    :cond_4
    goto/32 :goto_2d

    nop

    :goto_44
    if-nez v6, :cond_5

    goto/32 :goto_ae

    :cond_5
    goto/32 :goto_64

    nop

    :goto_45
    iput-object v6, p2, Landroid/os/BatteryStats$HistoryItem;->eventTag:Landroid/os/BatteryStats$HistoryTag;

    goto/32 :goto_ad

    nop

    :goto_46
    iget v7, p2, Landroid/os/BatteryStats$HistoryItem;->numReadInts:I

    goto/32 :goto_49

    nop

    :goto_47
    and-int/lit8 v7, v7, 0x3

    goto/32 :goto_3b

    nop

    :goto_48
    and-int/lit8 v7, v7, 0x7

    goto/32 :goto_33

    nop

    :goto_49
    add-int/2addr v7, v4

    goto/32 :goto_6d

    nop

    :goto_4a
    and-int/2addr v8, v0

    goto/32 :goto_97

    nop

    :goto_4b
    or-int/2addr v7, v8

    goto/32 :goto_8b

    nop

    :goto_4c
    shr-int/lit8 v7, v6, 0x1a

    goto/32 :goto_0

    nop

    :goto_4d
    iget-object v10, p2, Landroid/os/BatteryStats$HistoryItem;->localWakeReasonTag:Landroid/os/BatteryStats$HistoryTag;

    goto/32 :goto_a3

    nop

    :goto_4e
    iput-wide v2, p2, Landroid/os/BatteryStats$HistoryItem;->wifiRailChargeMah:D

    goto/32 :goto_8e

    nop

    :goto_4f
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    goto/32 :goto_6f

    nop

    :goto_50
    iget v6, p2, Landroid/os/BatteryStats$HistoryItem;->states:I

    goto/32 :goto_73

    nop

    :goto_51
    int-to-byte v7, v7

    goto/32 :goto_95

    nop

    :goto_52
    and-int/2addr v9, v6

    goto/32 :goto_76

    nop

    :goto_53
    const/high16 v5, 0x80000

    goto/32 :goto_24

    nop

    :goto_54
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    goto/32 :goto_9d

    nop

    :goto_55
    iget-object v10, p2, Landroid/os/BatteryStats$HistoryItem;->localWakelockTag:Landroid/os/BatteryStats$HistoryTag;

    goto/32 :goto_81

    nop

    :goto_56
    if-nez v10, :cond_6

    goto/32 :goto_e

    :cond_6
    goto/32 :goto_8

    nop

    :goto_57
    iput v4, p2, Landroid/os/BatteryStats$HistoryItem;->states:I

    :goto_58
    goto/32 :goto_25

    nop

    :goto_59
    const v6, 0xffff

    goto/32 :goto_70

    nop

    :goto_5a
    iget v6, p2, Landroid/os/BatteryStats$HistoryItem;->numReadInts:I

    goto/32 :goto_12

    nop

    :goto_5b
    iget v10, p2, Landroid/os/BatteryStats$HistoryItem;->numReadInts:I

    goto/32 :goto_9c

    nop

    :goto_5c
    iput v6, p2, Landroid/os/BatteryStats$HistoryItem;->numReadInts:I

    goto/32 :goto_b3

    nop

    :goto_5d
    add-int/2addr v6, v3

    goto/32 :goto_5c

    nop

    :goto_5e
    iput v4, p2, Landroid/os/BatteryStats$HistoryItem;->states2:I

    :goto_5f
    goto/32 :goto_2c

    nop

    :goto_60
    iget-wide v6, p2, Landroid/os/BatteryStats$HistoryItem;->time:J

    goto/32 :goto_38

    nop

    :goto_61
    if-eq v1, v5, :cond_7

    goto/32 :goto_b4

    :cond_7
    goto/32 :goto_2b

    nop

    :goto_62
    if-eq v1, v5, :cond_8

    goto/32 :goto_c

    :cond_8
    goto/32 :goto_ac

    nop

    :goto_63
    add-long/2addr v7, v5

    goto/32 :goto_23

    nop

    :goto_64
    iget-object v6, p2, Landroid/os/BatteryStats$HistoryItem;->localEventTag:Landroid/os/BatteryStats$HistoryTag;

    goto/32 :goto_45

    nop

    :goto_65
    iget-object v2, p2, Landroid/os/BatteryStats$HistoryItem;->stepDetails:Landroid/os/BatteryStats$HistoryStepDetails;

    goto/32 :goto_98

    nop

    :goto_66
    const/high16 v2, 0x1000000

    goto/32 :goto_9b

    nop

    :goto_67
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    goto/32 :goto_9e

    nop

    :goto_68
    const/4 v3, 0x1

    goto/32 :goto_16

    nop

    :goto_69
    const/4 v5, 0x0

    :goto_6a
    goto/32 :goto_7a

    nop

    :goto_6b
    iput-object v7, p2, Landroid/os/BatteryStats$HistoryItem;->wakeReasonTag:Landroid/os/BatteryStats$HistoryTag;

    :goto_6c
    goto/32 :goto_5b

    nop

    :goto_6d
    iput v7, p2, Landroid/os/BatteryStats$HistoryItem;->numReadInts:I

    :goto_6e
    goto/32 :goto_53

    nop

    :goto_6f
    and-int v8, v4, v6

    goto/32 :goto_a

    nop

    :goto_70
    const/4 v7, 0x0

    goto/32 :goto_10

    nop

    :goto_71
    iget-byte v7, p2, Landroid/os/BatteryStats$HistoryItem;->batteryPlugType:B

    packed-switch v7, :pswitch_data_0

    goto/32 :goto_ab

    nop

    :goto_72
    iput v4, p2, Landroid/os/BatteryStats$HistoryItem;->eventCode:I

    goto/32 :goto_1a

    nop

    :goto_73
    and-int/2addr v6, v7

    goto/32 :goto_b0

    nop

    :goto_74
    if-nez v4, :cond_9

    goto/32 :goto_15

    :cond_9
    goto/32 :goto_7b

    nop

    :goto_75
    const v5, 0x7fffe

    goto/32 :goto_61

    nop

    :goto_76
    iget-object v10, p2, Landroid/os/BatteryStats$HistoryItem;->localWakelockTag:Landroid/os/BatteryStats$HistoryTag;

    goto/32 :goto_84

    nop

    :goto_77
    iput-object v7, p2, Landroid/os/BatteryStats$HistoryItem;->wakeReasonTag:Landroid/os/BatteryStats$HistoryTag;

    :goto_78
    goto/32 :goto_37

    nop

    :goto_79
    iput-byte v4, p2, Landroid/os/BatteryStats$HistoryItem;->batteryPlugType:B

    goto/32 :goto_30

    nop

    :goto_7a
    const/high16 v6, 0x100000

    goto/32 :goto_2f

    nop

    :goto_7b
    iget-object v2, p2, Landroid/os/BatteryStats$HistoryItem;->localEventTag:Landroid/os/BatteryStats$HistoryTag;

    goto/32 :goto_96

    nop

    :goto_7c
    and-int/2addr v1, v0

    goto/32 :goto_36

    nop

    :goto_7d
    const v1, 0x7ffff

    goto/32 :goto_7c

    nop

    :goto_7e
    iget-wide v7, p2, Landroid/os/BatteryStats$HistoryItem;->time:J

    goto/32 :goto_63

    nop

    :goto_7f
    and-int/2addr v4, v0

    goto/32 :goto_59

    nop

    :goto_80
    iput v6, p2, Landroid/os/BatteryStats$HistoryItem;->numReadInts:I

    goto/32 :goto_92

    nop

    :goto_81
    iput-object v10, p2, Landroid/os/BatteryStats$HistoryItem;->wakelockTag:Landroid/os/BatteryStats$HistoryTag;

    goto/32 :goto_41

    nop

    :goto_82
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v2

    goto/32 :goto_18

    nop

    :goto_83
    invoke-direct {p0, p1, v4, v6}, Lcom/android/internal/os/BatteryStatsHistoryIterator;->readHistoryTag(Landroid/os/Parcel;ILandroid/os/BatteryStats$HistoryTag;)Z

    move-result v6

    goto/32 :goto_44

    nop

    :goto_84
    invoke-direct {p0, p1, v8, v10}, Lcom/android/internal/os/BatteryStatsHistoryIterator;->readHistoryTag(Landroid/os/Parcel;ILandroid/os/BatteryStats$HistoryTag;)Z

    move-result v10

    goto/32 :goto_35

    nop

    :goto_85
    iput-object v10, p2, Landroid/os/BatteryStats$HistoryItem;->wakeReasonTag:Landroid/os/BatteryStats$HistoryTag;

    goto/32 :goto_d

    nop

    :goto_86
    iget-object v2, p0, Lcom/android/internal/os/BatteryStatsHistoryIterator;->mReadHistoryStepDetails:Landroid/os/BatteryStats$HistoryStepDetails;

    goto/32 :goto_22

    nop

    :goto_87
    add-long/2addr v6, v8

    goto/32 :goto_aa

    nop

    :goto_88
    and-int/2addr v4, v6

    goto/32 :goto_2a

    nop

    :goto_89
    iput-byte v7, p2, Landroid/os/BatteryStats$HistoryItem;->batteryPlugType:B

    goto/32 :goto_71

    nop

    :goto_8a
    iput v10, p2, Landroid/os/BatteryStats$HistoryItem;->numReadInts:I

    goto/32 :goto_1d

    nop

    :goto_8b
    iput v7, p2, Landroid/os/BatteryStats$HistoryItem;->states:I

    goto/32 :goto_91

    nop

    :goto_8c
    iput-byte v3, p2, Landroid/os/BatteryStats$HistoryItem;->batteryPlugType:B

    nop

    :goto_8d
    goto/32 :goto_4

    nop

    :goto_8e
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :goto_8f
    iput-object v7, p2, Landroid/os/BatteryStats$HistoryItem;->wakelockTag:Landroid/os/BatteryStats$HistoryTag;

    goto/32 :goto_77

    nop

    :goto_90
    iget v6, p2, Landroid/os/BatteryStats$HistoryItem;->numReadInts:I

    goto/32 :goto_5d

    nop

    :goto_91
    shr-int/lit8 v7, v6, 0x1d

    goto/32 :goto_48

    nop

    :goto_92
    goto/16 :goto_6a

    :goto_93
    goto/32 :goto_69

    nop

    :goto_94
    iput v6, p2, Landroid/os/BatteryStats$HistoryItem;->numReadInts:I

    goto/32 :goto_14

    nop

    :goto_95
    iput-byte v7, p2, Landroid/os/BatteryStats$HistoryItem;->batteryHealth:B

    goto/32 :goto_a1

    nop

    :goto_96
    iput-object v2, p2, Landroid/os/BatteryStats$HistoryItem;->eventTag:Landroid/os/BatteryStats$HistoryTag;

    goto/32 :goto_54

    nop

    :goto_97
    and-int/2addr v7, v6

    goto/32 :goto_4b

    nop

    :goto_98
    invoke-virtual {v2, p1}, Landroid/os/BatteryStats$HistoryStepDetails;->readFromParcel(Landroid/os/Parcel;)V

    goto/32 :goto_3c

    nop

    :goto_99
    if-nez v2, :cond_a

    goto/32 :goto_9f

    :cond_a
    goto/32 :goto_67

    nop

    :goto_9a
    int-to-long v7, v1

    goto/32 :goto_34

    nop

    :goto_9b
    and-int/2addr v2, v0

    goto/32 :goto_99

    nop

    :goto_9c
    add-int/2addr v10, v3

    goto/32 :goto_8a

    nop

    :goto_9d
    and-int v4, v2, v6

    goto/32 :goto_72

    nop

    :goto_9e
    iput v2, p2, Landroid/os/BatteryStats$HistoryItem;->batteryChargeUah:I

    :goto_9f
    goto/32 :goto_82

    nop

    :goto_a0
    add-int/2addr v4, v3

    goto/32 :goto_a9

    nop

    :goto_a1
    shr-int/lit8 v7, v6, 0x18

    goto/32 :goto_47

    nop

    :goto_a2
    and-int/2addr v4, v0

    goto/32 :goto_17

    nop

    :goto_a3
    invoke-direct {p0, p1, v9, v10}, Lcom/android/internal/os/BatteryStatsHistoryIterator;->readHistoryTag(Landroid/os/Parcel;ILandroid/os/BatteryStats$HistoryTag;)Z

    move-result v10

    goto/32 :goto_56

    nop

    :goto_a4
    iput-object v7, p2, Landroid/os/BatteryStats$HistoryItem;->stepDetails:Landroid/os/BatteryStats$HistoryStepDetails;

    :goto_a5
    goto/32 :goto_66

    nop

    :goto_a6
    iput-object v7, p2, Landroid/os/BatteryStats$HistoryItem;->wakelockTag:Landroid/os/BatteryStats$HistoryTag;

    :goto_a7
    goto/32 :goto_4d

    nop

    :goto_a8
    if-nez v2, :cond_b

    goto/32 :goto_3d

    :cond_b
    goto/32 :goto_86

    nop

    :goto_a9
    iput v4, p2, Landroid/os/BatteryStats$HistoryItem;->numReadInts:I

    goto/32 :goto_1

    nop

    :goto_aa
    iput-wide v6, p2, Landroid/os/BatteryStats$HistoryItem;->time:J

    goto/32 :goto_90

    nop

    :goto_ab
    goto/16 :goto_8d

    :pswitch_2
    goto/32 :goto_6

    nop

    :goto_ac
    invoke-virtual {p2, p1}, Landroid/os/BatteryStats$HistoryItem;->readFromParcel(Landroid/os/Parcel;)V

    goto/32 :goto_b

    nop

    :goto_ad
    goto/16 :goto_21

    :goto_ae
    goto/32 :goto_20

    nop

    :goto_af
    const v7, 0xffffff

    goto/32 :goto_7

    nop

    :goto_b0
    or-int/2addr v4, v6

    goto/32 :goto_57

    nop

    :goto_b1
    iget-wide v5, p2, Landroid/os/BatteryStats$HistoryItem;->time:J

    goto/32 :goto_9a

    nop

    :goto_b2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    goto/32 :goto_5e

    nop

    :goto_b3
    goto/16 :goto_6e

    :goto_b4
    goto/32 :goto_28

    nop

    :goto_b5
    if-lt v1, v5, :cond_c

    goto/32 :goto_1c

    :cond_c
    goto/32 :goto_b1

    nop
.end method
