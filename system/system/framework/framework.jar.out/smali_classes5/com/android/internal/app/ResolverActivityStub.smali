.class public Lcom/android/internal/app/ResolverActivityStub;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/android/internal/app/ResolverActivityStub;
    .locals 1

    const-class v0, Lcom/android/internal/app/ResolverActivityStub;

    invoke-static {v0}, Lcom/miui/base/MiuiStubUtil;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/app/ResolverActivityStub;

    return-object v0
.end method

.method public static newInstance()Lcom/android/internal/app/ResolverActivityStub;
    .locals 1

    const-class v0, Lcom/android/internal/app/ResolverActivityStub;

    invoke-static {v0}, Lcom/miui/base/MiuiStubUtil;->newInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/app/ResolverActivityStub;

    return-object v0
.end method


# virtual methods
.method public addAppNameIfNeeded(Lcom/android/internal/app/chooser/DisplayResolveInfo;)V
    .locals 0

    return-void
.end method

.method public adjustWindowLayoutTemp()V
    .locals 0

    return-void
.end method

.method public bindOfficalRecommendView()V
    .locals 0

    return-void
.end method

.method public bindProfileView()V
    .locals 0

    return-void
.end method

.method public checkStartShareActivity(Landroid/app/Activity;Landroid/os/Bundle;ILandroid/content/Intent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public configureContentView(Ljava/util/List;[Landroid/content/Intent;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/content/Intent;",
            ">;[",
            "Landroid/content/Intent;",
            "Ljava/util/List<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public customResolver()V
    .locals 0

    return-void
.end method

.method public getAlwaysUse()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getAlwaysoptionId()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getAndroidForwardIntentToOwnerId()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getAndroidForwardIntentToWorkId()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getApplicationContext()Landroid/content/Context;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getChosenIndex()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getChosenView()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getMaxPerScreen()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getMiuiTheme()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getPostListReadyRunnable()Ljava/lang/Runnable;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getRecommendable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getResolverClassName()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/internal/app/ResolverActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getShowMoreResolverButton()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public init(Landroid/content/Context;Lcom/android/internal/app/ResolverListAdapter;Landroid/app/Activity;Lcom/android/internal/compat/AlertController$AlertParams;Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public initGridViews()V
    .locals 0

    return-void
.end method

.method public isInternationalBuild()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onDestroy()V
    .locals 0

    return-void
.end method

.method public registerPackageMonitor(Landroid/content/Context;Landroid/os/Looper;Z)V
    .locals 0

    return-void
.end method

.method public sendItemSelectedAnalyticsBroadcast(Ljava/util/List;IZLjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/internal/app/chooser/DisplayResolveInfo;",
            ">;IZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    return-void
.end method

.method public setAlwaysUse(Z)V
    .locals 0

    return-void
.end method

.method public setChoseView(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public setChosenIndex(I)V
    .locals 0

    return-void
.end method

.method public setPostListReadyRunnable(Ljava/lang/Runnable;)V
    .locals 0

    return-void
.end method

.method public setRecommendable(Z)V
    .locals 0

    return-void
.end method

.method public showMore()V
    .locals 0

    return-void
.end method

.method public unregisterPackageMonitor()V
    .locals 0

    return-void
.end method

.method public useAospShareSheet()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
