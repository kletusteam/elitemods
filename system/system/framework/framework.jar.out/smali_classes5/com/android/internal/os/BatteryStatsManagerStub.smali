.class public interface abstract Lcom/android/internal/os/BatteryStatsManagerStub;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/os/BatteryStatsManagerStub$ActiveCallback;
    }
.end annotation


# direct methods
.method public static getInstance(Lcom/android/internal/os/BatteryStatsImpl;I)Lcom/android/internal/os/BatteryStatsManagerStub;
    .locals 1

    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBatteryStatsManagerStub:Lcom/android/internal/os/BatteryStatsManagerStub;

    if-nez v0, :cond_0

    const-class v0, Lcom/android/internal/os/BatteryStatsManagerStub;

    invoke-static {v0}, Lcom/miui/base/MiuiStubUtil;->newInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/os/BatteryStatsManagerStub;

    iput-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBatteryStatsManagerStub:Lcom/android/internal/os/BatteryStatsManagerStub;

    :cond_0
    iget-object v0, p0, Lcom/android/internal/os/BatteryStatsImpl;->mBatteryStatsManagerStub:Lcom/android/internal/os/BatteryStatsManagerStub;

    return-object v0
.end method


# virtual methods
.method public getBacklightLevel(I)I
    .locals 1

    div-int/lit8 v0, p1, 0x33

    return v0
.end method

.method public abstract getDefaultDataSlotId()I
.end method

.method public getProcessName(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    return-object p1
.end method

.method public abstract noteAudioOffLocked(I)V
.end method

.method public abstract noteAudioOnLocked(I)V
.end method

.method public abstract noteResetAudioLocked()V
.end method

.method public abstract noteStartGpsLocked(I)V
.end method

.method public abstract noteStopGpsLocked(I)V
.end method

.method public noteSyncStart(Ljava/lang/String;IZ)V
    .locals 0

    return-void
.end method

.method public abstract setActiveCallback(Lcom/android/internal/os/BatteryStatsManagerStub$ActiveCallback;)V
.end method
