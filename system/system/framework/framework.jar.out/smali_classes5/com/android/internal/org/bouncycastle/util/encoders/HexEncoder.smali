.class public Lcom/android/internal/org/bouncycastle/util/encoders/HexEncoder;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/android/internal/org/bouncycastle/util/encoders/Encoder;


# instance fields
.field protected final decodingTable:[B

.field protected final encodingTable:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/android/internal/org/bouncycastle/util/encoders/HexEncoder;->encodingTable:[B

    const/16 v0, 0x80

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/android/internal/org/bouncycastle/util/encoders/HexEncoder;->decodingTable:[B

    invoke-virtual {p0}, Lcom/android/internal/org/bouncycastle/util/encoders/HexEncoder;->initialiseDecodingTable()V

    return-void

    :array_0
    .array-data 1
        0x30t
        0x31t
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
        0x38t
        0x39t
        0x61t
        0x62t
        0x63t
        0x64t
        0x65t
        0x66t
    .end array-data
.end method

.method private static ignore(C)Z
    .locals 1

    const/16 v0, 0xa

    if-eq p0, v0, :cond_1

    const/16 v0, 0xd

    if-eq p0, v0, :cond_1

    const/16 v0, 0x9

    if-eq p0, v0, :cond_1

    const/16 v0, 0x20

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method


# virtual methods
.method public decode(Ljava/lang/String;Ljava/io/OutputStream;)I
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    const/16 v1, 0x24

    new-array v1, v1, [B

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    :goto_0
    if-lez v3, :cond_1

    add-int/lit8 v4, v3, -0x1

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Lcom/android/internal/org/bouncycastle/util/encoders/HexEncoder;->ignore(C)Z

    move-result v4

    if-nez v4, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    :cond_1
    :goto_1
    const/4 v4, 0x0

    :goto_2
    if-ge v4, v3, :cond_6

    :goto_3
    if-ge v4, v3, :cond_2

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Lcom/android/internal/org/bouncycastle/util/encoders/HexEncoder;->ignore(C)Z

    move-result v5

    if-eqz v5, :cond_2

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_2
    iget-object v5, p0, Lcom/android/internal/org/bouncycastle/util/encoders/HexEncoder;->decodingTable:[B

    add-int/lit8 v6, v4, 0x1

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    aget-byte v4, v5, v4

    :goto_4
    if-ge v6, v3, :cond_3

    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Lcom/android/internal/org/bouncycastle/util/encoders/HexEncoder;->ignore(C)Z

    move-result v5

    if-eqz v5, :cond_3

    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :cond_3
    iget-object v5, p0, Lcom/android/internal/org/bouncycastle/util/encoders/HexEncoder;->decodingTable:[B

    add-int/lit8 v7, v6, 0x1

    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    aget-byte v5, v5, v6

    or-int v6, v4, v5

    if-ltz v6, :cond_5

    add-int/lit8 v6, v2, 0x1

    shl-int/lit8 v8, v4, 0x4

    or-int/2addr v8, v5

    int-to-byte v8, v8

    aput-byte v8, v1, v2

    array-length v2, v1

    if-ne v6, v2, :cond_4

    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write([B)V

    const/4 v2, 0x0

    goto :goto_5

    :cond_4
    move v2, v6

    :goto_5
    add-int/lit8 v0, v0, 0x1

    move v4, v7

    goto :goto_2

    :cond_5
    new-instance v6, Ljava/io/IOException;

    const-string v8, "invalid characters encountered in Hex string"

    invoke-direct {v6, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_6
    if-lez v2, :cond_7

    const/4 v5, 0x0

    invoke-virtual {p2, v1, v5, v2}, Ljava/io/OutputStream;->write([BII)V

    :cond_7
    return v0
.end method

.method public decode([BIILjava/io/OutputStream;)I
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    const/16 v1, 0x24

    new-array v1, v1, [B

    const/4 v2, 0x0

    add-int v3, p2, p3

    :goto_0
    if-le v3, p2, :cond_1

    add-int/lit8 v4, v3, -0x1

    aget-byte v4, p1, v4

    int-to-char v4, v4

    invoke-static {v4}, Lcom/android/internal/org/bouncycastle/util/encoders/HexEncoder;->ignore(C)Z

    move-result v4

    if-nez v4, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    :cond_1
    :goto_1
    move v4, p2

    :goto_2
    if-ge v4, v3, :cond_6

    :goto_3
    if-ge v4, v3, :cond_2

    aget-byte v5, p1, v4

    int-to-char v5, v5

    invoke-static {v5}, Lcom/android/internal/org/bouncycastle/util/encoders/HexEncoder;->ignore(C)Z

    move-result v5

    if-eqz v5, :cond_2

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_2
    iget-object v5, p0, Lcom/android/internal/org/bouncycastle/util/encoders/HexEncoder;->decodingTable:[B

    add-int/lit8 v6, v4, 0x1

    aget-byte v4, p1, v4

    aget-byte v4, v5, v4

    :goto_4
    if-ge v6, v3, :cond_3

    aget-byte v5, p1, v6

    int-to-char v5, v5

    invoke-static {v5}, Lcom/android/internal/org/bouncycastle/util/encoders/HexEncoder;->ignore(C)Z

    move-result v5

    if-eqz v5, :cond_3

    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :cond_3
    iget-object v5, p0, Lcom/android/internal/org/bouncycastle/util/encoders/HexEncoder;->decodingTable:[B

    add-int/lit8 v7, v6, 0x1

    aget-byte v6, p1, v6

    aget-byte v5, v5, v6

    or-int v6, v4, v5

    if-ltz v6, :cond_5

    add-int/lit8 v6, v2, 0x1

    shl-int/lit8 v8, v4, 0x4

    or-int/2addr v8, v5

    int-to-byte v8, v8

    aput-byte v8, v1, v2

    array-length v2, v1

    if-ne v6, v2, :cond_4

    invoke-virtual {p4, v1}, Ljava/io/OutputStream;->write([B)V

    const/4 v2, 0x0

    goto :goto_5

    :cond_4
    move v2, v6

    :goto_5
    add-int/lit8 v0, v0, 0x1

    move v4, v7

    goto :goto_2

    :cond_5
    new-instance v6, Ljava/io/IOException;

    const-string v8, "invalid characters encountered in Hex data"

    invoke-direct {v6, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_6
    if-lez v2, :cond_7

    const/4 v5, 0x0

    invoke-virtual {p4, v1, v5, v2}, Ljava/io/OutputStream;->write([BII)V

    :cond_7
    return v0
.end method

.method decodeStrict(Ljava/lang/String;II)[B
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_13

    nop

    :goto_0
    and-int/lit8 v0, p3, 0x1

    goto/32 :goto_17

    nop

    :goto_1
    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_c

    nop

    :goto_2
    shl-int/lit8 v5, v2, 0x4

    goto/32 :goto_27

    nop

    :goto_3
    add-int/lit8 v6, v5, 0x1

    goto/32 :goto_a

    nop

    :goto_4
    throw v7

    :goto_5
    goto/32 :goto_21

    nop

    :goto_6
    sub-int/2addr v0, p3

    goto/32 :goto_32

    nop

    :goto_7
    const-string v1, "invalid offset and/or length specified"

    goto/32 :goto_1

    nop

    :goto_8
    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_4

    nop

    :goto_9
    const-string v8, "invalid characters encountered in Hex string"

    goto/32 :goto_8

    nop

    :goto_a
    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    goto/32 :goto_33

    nop

    :goto_b
    if-lt v3, v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_15

    nop

    :goto_c
    throw v0

    :goto_d
    goto/32 :goto_2c

    nop

    :goto_e
    if-gez v5, :cond_1

    goto/32 :goto_31

    :cond_1
    goto/32 :goto_2a

    nop

    :goto_f
    if-gez p2, :cond_2

    goto/32 :goto_1a

    :cond_2
    goto/32 :goto_10

    nop

    :goto_10
    if-gez p3, :cond_3

    goto/32 :goto_1a

    :cond_3
    goto/32 :goto_20

    nop

    :goto_11
    new-instance v7, Ljava/io/IOException;

    goto/32 :goto_9

    nop

    :goto_12
    iget-object v4, p0, Lcom/android/internal/org/bouncycastle/util/encoders/HexEncoder;->decodingTable:[B

    goto/32 :goto_3

    nop

    :goto_13
    if-nez p1, :cond_4

    goto/32 :goto_d

    :cond_4
    goto/32 :goto_f

    nop

    :goto_14
    add-int/lit8 v3, v3, 0x1

    goto/32 :goto_1d

    nop

    :goto_15
    iget-object v4, p0, Lcom/android/internal/org/bouncycastle/util/encoders/HexEncoder;->decodingTable:[B

    goto/32 :goto_26

    nop

    :goto_16
    move v2, p2

    goto/32 :goto_24

    nop

    :goto_17
    if-eqz v0, :cond_5

    goto/32 :goto_22

    :cond_5
    goto/32 :goto_18

    nop

    :goto_18
    ushr-int/lit8 v0, p3, 0x1

    goto/32 :goto_29

    nop

    :goto_19
    throw v0

    :goto_1a
    goto/32 :goto_2d

    nop

    :goto_1b
    aput-byte v7, v1, v3

    goto/32 :goto_14

    nop

    :goto_1c
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_2f

    nop

    :goto_1d
    move v2, v6

    goto/32 :goto_30

    nop

    :goto_1e
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_19

    nop

    :goto_1f
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    goto/32 :goto_28

    nop

    :goto_20
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    goto/32 :goto_6

    nop

    :goto_21
    return-object v1

    :goto_22
    goto/32 :goto_2b

    nop

    :goto_23
    const-string v1, "\'str\' cannot be null"

    goto/32 :goto_1c

    nop

    :goto_24
    const/4 v3, 0x0

    :goto_25
    goto/32 :goto_b

    nop

    :goto_26
    add-int/lit8 v5, v2, 0x1

    goto/32 :goto_1f

    nop

    :goto_27
    or-int/2addr v5, v4

    goto/32 :goto_e

    nop

    :goto_28
    aget-byte v2, v4, v2

    goto/32 :goto_12

    nop

    :goto_29
    new-array v1, v0, [B

    goto/32 :goto_16

    nop

    :goto_2a
    int-to-byte v7, v5

    goto/32 :goto_1b

    nop

    :goto_2b
    new-instance v0, Ljava/io/IOException;

    goto/32 :goto_2e

    nop

    :goto_2c
    new-instance v0, Ljava/lang/NullPointerException;

    goto/32 :goto_23

    nop

    :goto_2d
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    goto/32 :goto_7

    nop

    :goto_2e
    const-string v1, "a hexadecimal encoding must have an even number of characters"

    goto/32 :goto_1e

    nop

    :goto_2f
    throw v0

    :goto_30
    goto :goto_25

    :goto_31
    goto/32 :goto_11

    nop

    :goto_32
    if-le p2, v0, :cond_6

    goto/32 :goto_1a

    :cond_6
    goto/32 :goto_0

    nop

    :goto_33
    aget-byte v4, v4, v5

    goto/32 :goto_2

    nop
.end method

.method public encode([BIILjava/io/OutputStream;)I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0x48

    new-array v0, v0, [B

    :goto_0
    if-lez p3, :cond_0

    const/16 v1, 0x24

    invoke-static {v1, p3}, Ljava/lang/Math;->min(II)I

    move-result v7

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, v7

    move-object v5, v0

    invoke-virtual/range {v1 .. v6}, Lcom/android/internal/org/bouncycastle/util/encoders/HexEncoder;->encode([BII[BI)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p4, v0, v2, v1}, Ljava/io/OutputStream;->write([BII)V

    add-int/2addr p2, v7

    sub-int/2addr p3, v7

    goto :goto_0

    :cond_0
    mul-int/lit8 v1, p3, 0x2

    return v1
.end method

.method public encode([BII[BI)I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move v0, p2

    add-int v1, p2, p3

    move v2, p5

    :goto_0
    if-ge v0, v1, :cond_0

    add-int/lit8 v3, v0, 0x1

    aget-byte v0, p1, v0

    and-int/lit16 v0, v0, 0xff

    add-int/lit8 v4, v2, 0x1

    iget-object v5, p0, Lcom/android/internal/org/bouncycastle/util/encoders/HexEncoder;->encodingTable:[B

    ushr-int/lit8 v6, v0, 0x4

    aget-byte v6, v5, v6

    aput-byte v6, p4, v2

    add-int/lit8 v2, v4, 0x1

    and-int/lit8 v6, v0, 0xf

    aget-byte v5, v5, v6

    aput-byte v5, p4, v4

    move v0, v3

    goto :goto_0

    :cond_0
    sub-int v3, v2, p5

    return v3
.end method

.method protected initialiseDecodingTable()V
    .locals 4

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/internal/org/bouncycastle/util/encoders/HexEncoder;->decodingTable:[B

    array-length v2, v1

    if-ge v0, v2, :cond_0

    const/4 v2, -0x1

    aput-byte v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/android/internal/org/bouncycastle/util/encoders/HexEncoder;->encodingTable:[B

    array-length v2, v1

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/android/internal/org/bouncycastle/util/encoders/HexEncoder;->decodingTable:[B

    aget-byte v1, v1, v0

    int-to-byte v3, v0

    aput-byte v3, v2, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/util/encoders/HexEncoder;->decodingTable:[B

    const/16 v1, 0x41

    const/16 v2, 0x61

    aget-byte v2, v0, v2

    aput-byte v2, v0, v1

    const/16 v1, 0x42

    const/16 v2, 0x62

    aget-byte v2, v0, v2

    aput-byte v2, v0, v1

    const/16 v1, 0x43

    const/16 v2, 0x63

    aget-byte v2, v0, v2

    aput-byte v2, v0, v1

    const/16 v1, 0x44

    const/16 v2, 0x64

    aget-byte v2, v0, v2

    aput-byte v2, v0, v1

    const/16 v1, 0x45

    const/16 v2, 0x65

    aget-byte v2, v0, v2

    aput-byte v2, v0, v1

    const/16 v1, 0x46

    const/16 v2, 0x66

    aget-byte v2, v0, v2

    aput-byte v2, v0, v1

    return-void
.end method
