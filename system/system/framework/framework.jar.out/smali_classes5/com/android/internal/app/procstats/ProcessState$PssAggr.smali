.class Lcom/android/internal/app/procstats/ProcessState$PssAggr;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/app/procstats/ProcessState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PssAggr"
.end annotation


# instance fields
.field pss:J

.field samples:J


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/internal/app/procstats/ProcessState$PssAggr;->pss:J

    iput-wide v0, p0, Lcom/android/internal/app/procstats/ProcessState$PssAggr;->samples:J

    return-void
.end method


# virtual methods
.method add(JJ)V
    .locals 8

    goto/32 :goto_8

    nop

    :goto_0
    add-long v4, v2, p3

    goto/32 :goto_4

    nop

    :goto_1
    iput-wide v0, p0, Lcom/android/internal/app/procstats/ProcessState$PssAggr;->pss:J

    goto/32 :goto_a

    nop

    :goto_2
    long-to-double v4, p1

    goto/32 :goto_6

    nop

    :goto_3
    long-to-double v4, v2

    goto/32 :goto_7

    nop

    :goto_4
    div-long/2addr v0, v4

    goto/32 :goto_1

    nop

    :goto_5
    iget-wide v2, p0, Lcom/android/internal/app/procstats/ProcessState$PssAggr;->samples:J

    goto/32 :goto_3

    nop

    :goto_6
    long-to-double v6, p3

    goto/32 :goto_e

    nop

    :goto_7
    mul-double/2addr v0, v4

    goto/32 :goto_2

    nop

    :goto_8
    iget-wide v0, p0, Lcom/android/internal/app/procstats/ProcessState$PssAggr;->pss:J

    goto/32 :goto_b

    nop

    :goto_9
    iput-wide v2, p0, Lcom/android/internal/app/procstats/ProcessState$PssAggr;->samples:J

    goto/32 :goto_f

    nop

    :goto_a
    add-long/2addr v2, p3

    goto/32 :goto_9

    nop

    :goto_b
    long-to-double v0, v0

    goto/32 :goto_5

    nop

    :goto_c
    double-to-long v0, v0

    goto/32 :goto_0

    nop

    :goto_d
    add-double/2addr v0, v4

    goto/32 :goto_c

    nop

    :goto_e
    mul-double/2addr v4, v6

    goto/32 :goto_d

    nop

    :goto_f
    return-void
.end method
