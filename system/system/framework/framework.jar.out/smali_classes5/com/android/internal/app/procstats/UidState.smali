.class public final Lcom/android/internal/app/procstats/UidState;
.super Ljava/lang/Object;


# static fields
.field private static final TAG:Ljava/lang/String; = "ProcessStats"


# instance fields
.field private mCurCombinedState:I

.field private final mDurations:Lcom/android/internal/app/procstats/DurationsTable;

.field private mProcesses:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Lcom/android/internal/app/procstats/ProcessState;",
            ">;"
        }
    .end annotation
.end field

.field private mStartTime:J

.field private final mStats:Lcom/android/internal/app/procstats/ProcessStats;

.field private mTotalRunningDuration:J

.field private mTotalRunningStartTime:J

.field private final mUid:I


# direct methods
.method public constructor <init>(Lcom/android/internal/app/procstats/ProcessStats;I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/android/internal/app/procstats/UidState;->mProcesses:Landroid/util/ArraySet;

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/app/procstats/UidState;->mCurCombinedState:I

    iput-object p1, p0, Lcom/android/internal/app/procstats/UidState;->mStats:Lcom/android/internal/app/procstats/ProcessStats;

    iput p2, p0, Lcom/android/internal/app/procstats/UidState;->mUid:I

    new-instance v0, Lcom/android/internal/app/procstats/DurationsTable;

    iget-object v1, p1, Lcom/android/internal/app/procstats/ProcessStats;->mTableData:Lcom/android/internal/app/procstats/SparseMappingTable;

    invoke-direct {v0, v1}, Lcom/android/internal/app/procstats/DurationsTable;-><init>(Lcom/android/internal/app/procstats/SparseMappingTable;)V

    iput-object v0, p0, Lcom/android/internal/app/procstats/UidState;->mDurations:Lcom/android/internal/app/procstats/DurationsTable;

    return-void
.end method

.method private calcCombinedState()I
    .locals 7

    const/4 v0, -0x1

    const/4 v1, -0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/internal/app/procstats/UidState;->mProcesses:Landroid/util/ArraySet;

    invoke-virtual {v3}, Landroid/util/ArraySet;->size()I

    move-result v3

    :goto_0
    if-ge v2, v3, :cond_2

    iget-object v4, p0, Lcom/android/internal/app/procstats/UidState;->mProcesses:Landroid/util/ArraySet;

    invoke-virtual {v4, v2}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/internal/app/procstats/ProcessState;

    invoke-virtual {v4}, Lcom/android/internal/app/procstats/ProcessState;->getCombinedState()I

    move-result v4

    rem-int/lit8 v5, v4, 0x10

    const/4 v6, -0x1

    if-eq v4, v6, :cond_1

    if-eq v1, v6, :cond_0

    if-ge v5, v1, :cond_1

    :cond_0
    move v0, v4

    move v1, v5

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return v0
.end method

.method private setCombinedStateInner(IJ)V
    .locals 4

    iget v0, p0, Lcom/android/internal/app/procstats/UidState;->mCurCombinedState:I

    if-eq v0, p1, :cond_2

    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    if-ltz v2, :cond_1

    invoke-virtual {p0, p2, p3}, Lcom/android/internal/app/procstats/UidState;->commitStateTime(J)V

    const/4 v2, -0x1

    if-ne p1, v2, :cond_0

    iget-wide v0, p0, Lcom/android/internal/app/procstats/UidState;->mTotalRunningDuration:J

    iget-wide v2, p0, Lcom/android/internal/app/procstats/UidState;->mTotalRunningStartTime:J

    sub-long v2, p2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/internal/app/procstats/UidState;->mTotalRunningDuration:J

    goto :goto_0

    :cond_0
    iget v3, p0, Lcom/android/internal/app/procstats/UidState;->mCurCombinedState:I

    if-ne v3, v2, :cond_1

    iput-wide v0, p0, Lcom/android/internal/app/procstats/UidState;->mTotalRunningDuration:J

    :cond_1
    :goto_0
    iput p1, p0, Lcom/android/internal/app/procstats/UidState;->mCurCombinedState:I

    :cond_2
    return-void
.end method


# virtual methods
.method public add(Lcom/android/internal/app/procstats/UidState;)V
    .locals 4

    iget-object v0, p0, Lcom/android/internal/app/procstats/UidState;->mDurations:Lcom/android/internal/app/procstats/DurationsTable;

    iget-object v1, p1, Lcom/android/internal/app/procstats/UidState;->mDurations:Lcom/android/internal/app/procstats/DurationsTable;

    invoke-virtual {v0, v1}, Lcom/android/internal/app/procstats/DurationsTable;->addDurations(Lcom/android/internal/app/procstats/DurationsTable;)V

    iget-wide v0, p0, Lcom/android/internal/app/procstats/UidState;->mTotalRunningDuration:J

    iget-wide v2, p1, Lcom/android/internal/app/procstats/UidState;->mTotalRunningDuration:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/internal/app/procstats/UidState;->mTotalRunningDuration:J

    return-void
.end method

.method addProcess(Lcom/android/internal/app/procstats/ProcessState;)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {v0, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    iget-object v0, p0, Lcom/android/internal/app/procstats/UidState;->mProcesses:Landroid/util/ArraySet;

    goto/32 :goto_0

    nop
.end method

.method addProcess(Lcom/android/internal/app/procstats/ProcessState;J)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/app/procstats/UidState;->mProcesses:Landroid/util/ArraySet;

    goto/32 :goto_2

    nop

    :goto_1
    invoke-virtual {p1}, Lcom/android/internal/app/procstats/ProcessState;->getCombinedState()I

    move-result v0

    goto/32 :goto_4

    nop

    :goto_2
    invoke-virtual {v0, p1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    goto/32 :goto_1

    nop

    :goto_3
    return-void

    :goto_4
    invoke-direct {p0, v0, p2, p3}, Lcom/android/internal/app/procstats/UidState;->setCombinedStateInner(IJ)V

    goto/32 :goto_3

    nop
.end method

.method public clone()Lcom/android/internal/app/procstats/UidState;
    .locals 3

    new-instance v0, Lcom/android/internal/app/procstats/UidState;

    iget-object v1, p0, Lcom/android/internal/app/procstats/UidState;->mStats:Lcom/android/internal/app/procstats/ProcessStats;

    iget v2, p0, Lcom/android/internal/app/procstats/UidState;->mUid:I

    invoke-direct {v0, v1, v2}, Lcom/android/internal/app/procstats/UidState;-><init>(Lcom/android/internal/app/procstats/ProcessStats;I)V

    iget-object v1, v0, Lcom/android/internal/app/procstats/UidState;->mDurations:Lcom/android/internal/app/procstats/DurationsTable;

    iget-object v2, p0, Lcom/android/internal/app/procstats/UidState;->mDurations:Lcom/android/internal/app/procstats/DurationsTable;

    invoke-virtual {v1, v2}, Lcom/android/internal/app/procstats/DurationsTable;->addDurations(Lcom/android/internal/app/procstats/DurationsTable;)V

    iget v1, p0, Lcom/android/internal/app/procstats/UidState;->mCurCombinedState:I

    iput v1, v0, Lcom/android/internal/app/procstats/UidState;->mCurCombinedState:I

    iget-wide v1, p0, Lcom/android/internal/app/procstats/UidState;->mStartTime:J

    iput-wide v1, v0, Lcom/android/internal/app/procstats/UidState;->mStartTime:J

    iget-wide v1, p0, Lcom/android/internal/app/procstats/UidState;->mTotalRunningStartTime:J

    iput-wide v1, v0, Lcom/android/internal/app/procstats/UidState;->mTotalRunningStartTime:J

    iget-wide v1, p0, Lcom/android/internal/app/procstats/UidState;->mTotalRunningDuration:J

    iput-wide v1, v0, Lcom/android/internal/app/procstats/UidState;->mTotalRunningDuration:J

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/internal/app/procstats/UidState;->clone()Lcom/android/internal/app/procstats/UidState;

    move-result-object v0

    return-object v0
.end method

.method public commitStateTime(J)V
    .locals 7

    iget v0, p0, Lcom/android/internal/app/procstats/UidState;->mCurCombinedState:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget-wide v1, p0, Lcom/android/internal/app/procstats/UidState;->mStartTime:J

    sub-long v1, p1, v1

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/android/internal/app/procstats/UidState;->mDurations:Lcom/android/internal/app/procstats/DurationsTable;

    invoke-virtual {v3, v0, v1, v2}, Lcom/android/internal/app/procstats/DurationsTable;->addDuration(IJ)V

    :cond_0
    iget-wide v3, p0, Lcom/android/internal/app/procstats/UidState;->mTotalRunningDuration:J

    iget-wide v5, p0, Lcom/android/internal/app/procstats/UidState;->mTotalRunningStartTime:J

    sub-long v5, p1, v5

    add-long/2addr v3, v5

    iput-wide v3, p0, Lcom/android/internal/app/procstats/UidState;->mTotalRunningDuration:J

    iput-wide p1, p0, Lcom/android/internal/app/procstats/UidState;->mTotalRunningStartTime:J

    :cond_1
    iput-wide p1, p0, Lcom/android/internal/app/procstats/UidState;->mStartTime:J

    return-void
.end method

.method dumpState(Ljava/io/PrintWriter;Ljava/lang/String;[I[I[IJ)V
    .locals 24

    goto/32 :goto_29

    nop

    :goto_0
    const/4 v10, 0x0

    :goto_1
    goto/32 :goto_38

    nop

    :goto_2
    add-int/lit8 v14, v20, 0x1

    goto/32 :goto_3f

    nop

    :goto_3
    move-object/from16 v2, p3

    goto/32 :goto_2e

    nop

    :goto_4
    if-eq v14, v13, :cond_0

    goto/32 :goto_67

    :cond_0
    goto/32 :goto_5e

    nop

    :goto_5
    invoke-static {v1, v0}, Lcom/android/internal/app/procstats/DumpUtils;->printScreenLabel(Ljava/io/PrintWriter;I)V

    goto/32 :goto_86

    nop

    :goto_6
    const/16 v8, 0x2f

    goto/32 :goto_14

    nop

    :goto_7
    aget v15, v2, v8

    goto/32 :goto_68

    nop

    :goto_8
    move-object/from16 v0, p3

    goto/32 :goto_13

    nop

    :goto_9
    add-int v13, v13, v19

    goto/32 :goto_3e

    nop

    :goto_a
    return-void

    :goto_b
    add-int/lit8 v10, v10, 0x1

    goto/32 :goto_4d

    nop

    :goto_c
    move v9, v0

    :goto_d
    goto/32 :goto_1a

    nop

    :goto_e
    const-wide/16 v8, 0x0

    goto/32 :goto_6b

    nop

    :goto_f
    add-long v21, v21, v13

    goto/32 :goto_84

    nop

    :goto_10
    add-long/2addr v5, v13

    :goto_11
    goto/32 :goto_b

    nop

    :goto_12
    if-lt v8, v9, :cond_1

    goto/32 :goto_2f

    :cond_1
    goto/32 :goto_21

    nop

    :goto_13
    array-length v2, v0

    goto/32 :goto_72

    nop

    :goto_14
    invoke-static {v1, v2, v8}, Lcom/android/internal/app/procstats/DumpUtils;->printMemLabel(Ljava/io/PrintWriter;IC)V

    :goto_15
    goto/32 :goto_57

    nop

    :goto_16
    array-length v9, v3

    goto/32 :goto_71

    nop

    :goto_17
    move v0, v12

    goto/32 :goto_c

    nop

    :goto_18
    sub-long v13, p6, v13

    goto/32 :goto_f

    nop

    :goto_19
    move/from16 v14, v20

    goto/32 :goto_31

    nop

    :goto_1a
    sget-object v0, Lcom/android/internal/app/procstats/DumpUtils;->STATE_LABELS:[Ljava/lang/String;

    goto/32 :goto_2b

    nop

    :goto_1b
    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_7d

    nop

    :goto_1c
    const/4 v0, -0x1

    :goto_1d
    goto/32 :goto_34

    nop

    :goto_1e
    const/4 v2, 0x1

    goto/32 :goto_64

    nop

    :goto_1f
    if-ne v7, v15, :cond_2

    goto/32 :goto_4f

    :cond_2
    goto/32 :goto_4a

    nop

    :goto_20
    move/from16 v8, v19

    goto/32 :goto_19

    nop

    :goto_21
    const/4 v9, -0x1

    goto/32 :goto_7a

    nop

    :goto_22
    cmp-long v21, v13, v17

    goto/32 :goto_3c

    nop

    :goto_23
    iget-wide v13, v0, Lcom/android/internal/app/procstats/UidState;->mStartTime:J

    goto/32 :goto_18

    nop

    :goto_24
    move/from16 v20, v14

    goto/32 :goto_44

    nop

    :goto_25
    move/from16 v23, v13

    goto/32 :goto_6f

    nop

    :goto_26
    if-nez v0, :cond_3

    goto/32 :goto_63

    :cond_3
    goto/32 :goto_51

    nop

    :goto_27
    array-length v9, v2

    goto/32 :goto_42

    nop

    :goto_28
    aget-object v0, v0, v2

    goto/32 :goto_4b

    nop

    :goto_29
    move-object/from16 v0, p0

    goto/32 :goto_41

    nop

    :goto_2a
    if-gt v2, v8, :cond_4

    goto/32 :goto_60

    :cond_4
    goto/32 :goto_30

    nop

    :goto_2b
    aget v2, v4, v10

    goto/32 :goto_28

    nop

    :goto_2c
    move-object/from16 v0, p3

    :goto_2d
    goto/32 :goto_a

    nop

    :goto_2e
    goto/16 :goto_54

    :goto_2f
    goto/32 :goto_61

    nop

    :goto_30
    const/4 v2, -0x1

    goto/32 :goto_77

    nop

    :goto_31
    goto/16 :goto_1

    :goto_32
    goto/32 :goto_33

    nop

    :goto_33
    move/from16 v19, v8

    goto/32 :goto_7c

    nop

    :goto_34
    const/16 v2, 0x2f

    goto/32 :goto_40

    nop

    :goto_35
    if-gt v0, v2, :cond_5

    goto/32 :goto_d

    :cond_5
    nop

    goto/32 :goto_46

    nop

    :goto_36
    move-object/from16 v2, p3

    goto/32 :goto_39

    nop

    :goto_37
    iget v14, v0, Lcom/android/internal/app/procstats/UidState;->mCurCombinedState:I

    goto/32 :goto_4

    nop

    :goto_38
    array-length v15, v4

    goto/32 :goto_88

    nop

    :goto_39
    move-object/from16 v3, p4

    goto/32 :goto_49

    nop

    :goto_3a
    move-object/from16 v0, p0

    goto/32 :goto_3

    nop

    :goto_3b
    const-wide/16 v5, 0x0

    goto/32 :goto_45

    nop

    :goto_3c
    if-nez v21, :cond_6

    goto/32 :goto_11

    :cond_6
    goto/32 :goto_7b

    nop

    :goto_3d
    invoke-static {v13, v14, v1}, Landroid/util/TimeUtils;->formatDuration(JLjava/io/PrintWriter;)V

    goto/32 :goto_50

    nop

    :goto_3e
    move/from16 v19, v8

    goto/32 :goto_6e

    nop

    :goto_3f
    move-object/from16 v0, p0

    goto/32 :goto_80

    nop

    :goto_40
    invoke-static {v1, v0, v2}, Lcom/android/internal/app/procstats/DumpUtils;->printMemLabel(Ljava/io/PrintWriter;IC)V

    goto/32 :goto_17

    nop

    :goto_41
    move-object/from16 v1, p1

    goto/32 :goto_36

    nop

    :goto_42
    const-string v11, ": "

    goto/32 :goto_12

    nop

    :goto_43
    add-int/lit8 v8, v19, 0x1

    goto/32 :goto_3a

    nop

    :goto_44
    int-to-byte v14, v13

    goto/32 :goto_5a

    nop

    :goto_45
    const/4 v7, -0x1

    goto/32 :goto_53

    nop

    :goto_46
    if-ne v9, v12, :cond_7

    goto/32 :goto_59

    :cond_7
    goto/32 :goto_7e

    nop

    :goto_47
    const/4 v2, 0x1

    goto/32 :goto_35

    nop

    :goto_48
    array-length v0, v2

    goto/32 :goto_1e

    nop

    :goto_49
    move-object/from16 v4, p5

    goto/32 :goto_3b

    nop

    :goto_4a
    move v0, v15

    goto/32 :goto_4e

    nop

    :goto_4b
    invoke-virtual {v1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_5c

    nop

    :goto_4c
    aget v19, v4, v10

    goto/32 :goto_9

    nop

    :goto_4d
    move-object/from16 v0, p0

    goto/32 :goto_5d

    nop

    :goto_4e
    goto/16 :goto_6d

    :goto_4f
    goto/32 :goto_6c

    nop

    :goto_50
    invoke-virtual {v1, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/32 :goto_10

    nop

    :goto_51
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_8

    nop

    :goto_52
    add-int v13, v15, v12

    goto/32 :goto_7f

    nop

    :goto_53
    const/4 v8, 0x0

    :goto_54
    goto/32 :goto_27

    nop

    :goto_55
    const/4 v2, -0x1

    :goto_56
    goto/32 :goto_16

    nop

    :goto_57
    sget-object v2, Lcom/android/internal/app/procstats/DumpUtils;->STATE_LABEL_TOTAL:Ljava/lang/String;

    goto/32 :goto_1b

    nop

    :goto_58
    goto/16 :goto_1d

    :goto_59
    goto/32 :goto_1c

    nop

    :goto_5a
    invoke-virtual {v8, v14}, Lcom/android/internal/app/procstats/DurationsTable;->getValueForId(B)J

    move-result-wide v21

    goto/32 :goto_78

    nop

    :goto_5b
    move/from16 v19, v8

    goto/32 :goto_85

    nop

    :goto_5c
    invoke-virtual {v1, v11}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_3d

    nop

    :goto_5d
    move-object/from16 v2, p3

    goto/32 :goto_20

    nop

    :goto_5e
    const-string v8, " (running)"

    goto/32 :goto_74

    nop

    :goto_5f
    goto :goto_56

    :goto_60
    goto/32 :goto_55

    nop

    :goto_61
    move/from16 v19, v8

    goto/32 :goto_e

    nop

    :goto_62
    goto/16 :goto_2d

    :goto_63
    goto/32 :goto_2c

    nop

    :goto_64
    if-gt v0, v2, :cond_8

    goto/32 :goto_87

    :cond_8
    goto/32 :goto_1f

    nop

    :goto_65
    if-lt v14, v10, :cond_9

    goto/32 :goto_6a

    :cond_9
    goto/32 :goto_0

    nop

    :goto_66
    goto :goto_70

    :goto_67
    goto/32 :goto_25

    nop

    :goto_68
    aget v12, v3, v14

    goto/32 :goto_52

    nop

    :goto_69
    goto/16 :goto_83

    :goto_6a
    goto/32 :goto_5b

    nop

    :goto_6b
    cmp-long v0, v5, v8

    goto/32 :goto_26

    nop

    :goto_6c
    const/4 v0, -0x1

    :goto_6d
    goto/32 :goto_5

    nop

    :goto_6e
    iget-object v8, v0, Lcom/android/internal/app/procstats/UidState;->mDurations:Lcom/android/internal/app/procstats/DurationsTable;

    goto/32 :goto_24

    nop

    :goto_6f
    move-wide/from16 v13, v21

    :goto_70
    goto/32 :goto_75

    nop

    :goto_71
    if-gt v9, v8, :cond_a

    goto/32 :goto_15

    :cond_a
    goto/32 :goto_6

    nop

    :goto_72
    const/4 v8, 0x1

    goto/32 :goto_2a

    nop

    :goto_73
    array-length v0, v3

    goto/32 :goto_47

    nop

    :goto_74
    move/from16 v23, v13

    goto/32 :goto_23

    nop

    :goto_75
    const-wide/16 v17, 0x0

    goto/32 :goto_22

    nop

    :goto_76
    array-length v10, v3

    goto/32 :goto_65

    nop

    :goto_77
    invoke-static {v1, v2}, Lcom/android/internal/app/procstats/DumpUtils;->printScreenLabel(Ljava/io/PrintWriter;I)V

    goto/32 :goto_5f

    nop

    :goto_78
    const-string v8, ""

    goto/32 :goto_37

    nop

    :goto_79
    invoke-virtual/range {p1 .. p1}, Ljava/io/PrintWriter;->println()V

    goto/32 :goto_62

    nop

    :goto_7a
    const/16 v16, 0x0

    goto/32 :goto_82

    nop

    :goto_7b
    invoke-virtual/range {p1 .. p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_48

    nop

    :goto_7c
    move/from16 v20, v14

    goto/32 :goto_2

    nop

    :goto_7d
    invoke-virtual {v1, v11}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_81

    nop

    :goto_7e
    move v0, v12

    goto/32 :goto_58

    nop

    :goto_7f
    mul-int/lit8 v13, v13, 0x10

    goto/32 :goto_4c

    nop

    :goto_80
    move-object/from16 v2, p3

    goto/32 :goto_69

    nop

    :goto_81
    invoke-static {v5, v6, v1}, Landroid/util/TimeUtils;->formatDuration(JLjava/io/PrintWriter;)V

    goto/32 :goto_79

    nop

    :goto_82
    move/from16 v14, v16

    :goto_83
    goto/32 :goto_76

    nop

    :goto_84
    move-wide/from16 v13, v21

    goto/32 :goto_66

    nop

    :goto_85
    move/from16 v20, v14

    goto/32 :goto_43

    nop

    :goto_86
    move v7, v15

    :goto_87
    goto/32 :goto_73

    nop

    :goto_88
    if-lt v10, v15, :cond_b

    goto/32 :goto_32

    :cond_b
    goto/32 :goto_7

    nop
.end method

.method public getAggregatedDurationsInStates()[J
    .locals 10

    const/16 v0, 0x10

    new-array v0, v0, [J

    invoke-virtual {p0}, Lcom/android/internal/app/procstats/UidState;->getDurationsBucketCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    iget-object v3, p0, Lcom/android/internal/app/procstats/UidState;->mDurations:Lcom/android/internal/app/procstats/DurationsTable;

    invoke-virtual {v3, v2}, Lcom/android/internal/app/procstats/DurationsTable;->getKeyAt(I)I

    move-result v3

    invoke-static {v3}, Lcom/android/internal/app/procstats/SparseMappingTable;->getIdFromKey(I)B

    move-result v4

    rem-int/lit8 v5, v4, 0x10

    aget-wide v6, v0, v5

    iget-object v8, p0, Lcom/android/internal/app/procstats/UidState;->mDurations:Lcom/android/internal/app/procstats/DurationsTable;

    invoke-virtual {v8, v3}, Lcom/android/internal/app/procstats/DurationsTable;->getValue(I)J

    move-result-wide v8

    add-long/2addr v6, v8

    aput-wide v6, v0, v5

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getCombinedState()I
    .locals 1

    iget v0, p0, Lcom/android/internal/app/procstats/UidState;->mCurCombinedState:I

    return v0
.end method

.method public getDuration(IJ)J
    .locals 4

    iget-object v0, p0, Lcom/android/internal/app/procstats/UidState;->mDurations:Lcom/android/internal/app/procstats/DurationsTable;

    int-to-byte v1, p1

    invoke-virtual {v0, v1}, Lcom/android/internal/app/procstats/DurationsTable;->getValueForId(B)J

    move-result-wide v0

    iget v2, p0, Lcom/android/internal/app/procstats/UidState;->mCurCombinedState:I

    if-ne v2, p1, :cond_0

    iget-wide v2, p0, Lcom/android/internal/app/procstats/UidState;->mStartTime:J

    sub-long v2, p2, v2

    add-long/2addr v0, v2

    :cond_0
    return-wide v0
.end method

.method public getDurationsBucketCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/internal/app/procstats/UidState;->mDurations:Lcom/android/internal/app/procstats/DurationsTable;

    invoke-virtual {v0}, Lcom/android/internal/app/procstats/DurationsTable;->getKeyCount()I

    move-result v0

    return v0
.end method

.method public getTotalRunningDuration(J)J
    .locals 7

    iget-wide v0, p0, Lcom/android/internal/app/procstats/UidState;->mTotalRunningDuration:J

    iget-wide v2, p0, Lcom/android/internal/app/procstats/UidState;->mTotalRunningStartTime:J

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-eqz v6, :cond_0

    sub-long v4, p1, v2

    :cond_0
    add-long/2addr v0, v4

    return-wide v0
.end method

.method public hasPackage(Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/internal/app/procstats/UidState;->mProcesses:Landroid/util/ArraySet;

    invoke-virtual {v1}, Landroid/util/ArraySet;->size()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lcom/android/internal/app/procstats/UidState;->mProcesses:Landroid/util/ArraySet;

    invoke-virtual {v2, v0}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/internal/app/procstats/ProcessState;

    invoke-virtual {v2}, Lcom/android/internal/app/procstats/ProcessState;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/android/internal/app/procstats/ProcessState;->getPackage()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    return v3

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public isInUse()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/internal/app/procstats/UidState;->mProcesses:Landroid/util/ArraySet;

    invoke-virtual {v1}, Landroid/util/ArraySet;->size()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lcom/android/internal/app/procstats/UidState;->mProcesses:Landroid/util/ArraySet;

    invoke-virtual {v2, v0}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/internal/app/procstats/ProcessState;

    invoke-virtual {v2}, Lcom/android/internal/app/procstats/ProcessState;->isInUse()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    return v2

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method readFromParcel(Landroid/os/Parcel;)Z
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    iput-wide v0, p0, Lcom/android/internal/app/procstats/UidState;->mTotalRunningDuration:J

    goto/32 :goto_8

    nop

    :goto_1
    invoke-virtual {v0, p1}, Lcom/android/internal/app/procstats/DurationsTable;->readFromParcel(Landroid/os/Parcel;)Z

    move-result v0

    goto/32 :goto_9

    nop

    :goto_2
    return v0

    :goto_3
    iget-object v0, p0, Lcom/android/internal/app/procstats/UidState;->mDurations:Lcom/android/internal/app/procstats/DurationsTable;

    goto/32 :goto_1

    nop

    :goto_4
    const/4 v0, 0x0

    goto/32 :goto_5

    nop

    :goto_5
    return v0

    :goto_6
    goto/32 :goto_7

    nop

    :goto_7
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    goto/32 :goto_0

    nop

    :goto_8
    const/4 v0, 0x1

    goto/32 :goto_2

    nop

    :goto_9
    if-eqz v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_4

    nop
.end method

.method removeProcess(Lcom/android/internal/app/procstats/ProcessState;J)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {p1}, Lcom/android/internal/app/procstats/ProcessState;->getCombinedState()I

    move-result v0

    goto/32 :goto_1

    nop

    :goto_1
    invoke-direct {p0, v0, p2, p3}, Lcom/android/internal/app/procstats/UidState;->setCombinedStateInner(IJ)V

    goto/32 :goto_3

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/internal/app/procstats/UidState;->mProcesses:Landroid/util/ArraySet;

    goto/32 :goto_4

    nop

    :goto_3
    return-void

    :goto_4
    invoke-virtual {v0, p1}, Landroid/util/ArraySet;->remove(Ljava/lang/Object;)Z

    goto/32 :goto_0

    nop
.end method

.method public resetSafely(J)V
    .locals 1

    iget-object v0, p0, Lcom/android/internal/app/procstats/UidState;->mDurations:Lcom/android/internal/app/procstats/DurationsTable;

    invoke-virtual {v0}, Lcom/android/internal/app/procstats/DurationsTable;->resetTable()V

    iput-wide p1, p0, Lcom/android/internal/app/procstats/UidState;->mStartTime:J

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "UidState{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/internal/app/procstats/UidState;->mUid:I

    invoke-static {v2}, Landroid/os/UserHandle;->formatUid(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public updateCombinedState(IJ)V
    .locals 1

    iget v0, p0, Lcom/android/internal/app/procstats/UidState;->mCurCombinedState:I

    if-eq v0, p1, :cond_0

    invoke-virtual {p0, p2, p3}, Lcom/android/internal/app/procstats/UidState;->updateCombinedState(J)V

    :cond_0
    return-void
.end method

.method public updateCombinedState(J)V
    .locals 1

    invoke-direct {p0}, Lcom/android/internal/app/procstats/UidState;->calcCombinedState()I

    move-result v0

    invoke-direct {p0, v0, p1, p2}, Lcom/android/internal/app/procstats/UidState;->setCombinedStateInner(IJ)V

    return-void
.end method

.method writeToParcel(Landroid/os/Parcel;J)V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/app/procstats/UidState;->mDurations:Lcom/android/internal/app/procstats/DurationsTable;

    goto/32 :goto_3

    nop

    :goto_1
    invoke-virtual {p0, p2, p3}, Lcom/android/internal/app/procstats/UidState;->getTotalRunningDuration(J)J

    move-result-wide v0

    goto/32 :goto_4

    nop

    :goto_2
    return-void

    :goto_3
    invoke-virtual {v0, p1}, Lcom/android/internal/app/procstats/DurationsTable;->writeToParcel(Landroid/os/Parcel;)V

    goto/32 :goto_1

    nop

    :goto_4
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    goto/32 :goto_2

    nop
.end method
