.class Lcom/android/internal/app/AppPredictionServiceResolverComparator;
.super Lcom/android/internal/app/AbstractResolverComparator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/app/AppPredictionServiceResolverComparator$AppPredictionServiceComparatorModel;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "APSResolverComparator"


# instance fields
.field private final mAppPredictor:Landroid/app/prediction/AppPredictor;

.field private mComparatorModel:Lcom/android/internal/app/AppPredictionServiceResolverComparator$AppPredictionServiceComparatorModel;

.field private final mContext:Landroid/content/Context;

.field private final mIntent:Landroid/content/Intent;

.field private final mReferrerPackage:Ljava/lang/String;

.field private mResolverRankerService:Lcom/android/internal/app/ResolverRankerServiceResolverComparator;

.field private final mTargetRanks:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Landroid/content/ComponentName;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mTargetScores:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Landroid/content/ComponentName;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mUser:Landroid/os/UserHandle;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;Landroid/app/prediction/AppPredictor;Landroid/os/UserHandle;Lcom/android/internal/app/ChooserActivityLogger;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/internal/app/AbstractResolverComparator;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mTargetRanks:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mTargetScores:Ljava/util/Map;

    iput-object p1, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mIntent:Landroid/content/Intent;

    iput-object p4, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mAppPredictor:Landroid/app/prediction/AppPredictor;

    iput-object p5, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mUser:Landroid/os/UserHandle;

    iput-object p3, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mReferrerPackage:Ljava/lang/String;

    invoke-virtual {p0, p6}, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->setChooserActivityLogger(Lcom/android/internal/app/ChooserActivityLogger;)V

    invoke-direct {p0}, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->buildUpdatedModel()Lcom/android/internal/app/AppPredictionServiceResolverComparator$AppPredictionServiceComparatorModel;

    move-result-object v0

    iput-object v0, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mComparatorModel:Lcom/android/internal/app/AppPredictionServiceResolverComparator$AppPredictionServiceComparatorModel;

    return-void
.end method

.method private buildUpdatedModel()Lcom/android/internal/app/AppPredictionServiceResolverComparator$AppPredictionServiceComparatorModel;
    .locals 5

    new-instance v0, Lcom/android/internal/app/AppPredictionServiceResolverComparator$AppPredictionServiceComparatorModel;

    iget-object v1, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mAppPredictor:Landroid/app/prediction/AppPredictor;

    iget-object v2, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mResolverRankerService:Lcom/android/internal/app/ResolverRankerServiceResolverComparator;

    iget-object v3, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mUser:Landroid/os/UserHandle;

    iget-object v4, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mTargetRanks:Ljava/util/Map;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/internal/app/AppPredictionServiceResolverComparator$AppPredictionServiceComparatorModel;-><init>(Landroid/app/prediction/AppPredictor;Lcom/android/internal/app/ResolverRankerServiceResolverComparator;Landroid/os/UserHandle;Ljava/util/Map;)V

    return-object v0
.end method

.method private checkAppTargetRankValid(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/app/prediction/AppTarget;",
            ">;)Z"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/prediction/AppTarget;

    invoke-virtual {v1}, Landroid/app/prediction/AppTarget;->getRank()I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method private handleResult(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/app/prediction/AppTarget;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->handleSortedAppTargets(Ljava/util/List;)V

    iget-object v0, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p0}, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->afterCompute()V

    :cond_0
    return-void
.end method

.method private handleSortedAppTargets(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/app/prediction/AppTarget;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->checkAppTargetRankValid(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/internal/app/AppPredictionServiceResolverComparator$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0}, Lcom/android/internal/app/AppPredictionServiceResolverComparator$$ExternalSyntheticLambda2;-><init>(Lcom/android/internal/app/AppPredictionServiceResolverComparator;)V

    invoke-interface {p1, v0}, Ljava/util/List;->forEach(Ljava/util/function/Consumer;)V

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    new-instance v1, Landroid/content/ComponentName;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/prediction/AppTarget;

    invoke-virtual {v2}, Landroid/app/prediction/AppTarget;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/prediction/AppTarget;

    invoke-virtual {v3}, Landroid/app/prediction/AppTarget;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mTargetRanks:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleSortedAppTargets, sortedAppTargets #"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "APSResolverComparator"

    invoke-static {v3, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->buildUpdatedModel()Lcom/android/internal/app/AppPredictionServiceResolverComparator$AppPredictionServiceComparatorModel;

    move-result-object v0

    iput-object v0, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mComparatorModel:Lcom/android/internal/app/AppPredictionServiceResolverComparator$AppPredictionServiceComparatorModel;

    return-void
.end method


# virtual methods
.method compare(Landroid/content/pm/ResolveInfo;Landroid/content/pm/ResolveInfo;)I
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {v0}, Lcom/android/internal/app/AppPredictionServiceResolverComparator$AppPredictionServiceComparatorModel;->getComparator()Ljava/util/Comparator;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_1
    iget-object v0, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mComparatorModel:Lcom/android/internal/app/AppPredictionServiceResolverComparator$AppPredictionServiceComparatorModel;

    goto/32 :goto_0

    nop

    :goto_2
    return v0

    :goto_3
    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    goto/32 :goto_2

    nop
.end method

.method destroy()V
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    iput-object v0, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mResolverRankerService:Lcom/android/internal/app/ResolverRankerServiceResolverComparator;

    goto/32 :goto_3

    nop

    :goto_1
    return-void

    :goto_2
    const/4 v0, 0x0

    goto/32 :goto_0

    nop

    :goto_3
    invoke-direct {p0}, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->buildUpdatedModel()Lcom/android/internal/app/AppPredictionServiceResolverComparator$AppPredictionServiceComparatorModel;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_4
    iget-object v0, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mResolverRankerService:Lcom/android/internal/app/ResolverRankerServiceResolverComparator;

    goto/32 :goto_8

    nop

    :goto_5
    iput-object v0, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mComparatorModel:Lcom/android/internal/app/AppPredictionServiceResolverComparator$AppPredictionServiceComparatorModel;

    :goto_6
    goto/32 :goto_1

    nop

    :goto_7
    invoke-virtual {v0}, Lcom/android/internal/app/ResolverRankerServiceResolverComparator;->destroy()V

    goto/32 :goto_2

    nop

    :goto_8
    if-nez v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_7

    nop
.end method

.method doCompute(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/internal/app/ResolverActivity$ResolvedComponentInfo;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_1e

    nop

    :goto_0
    if-nez v2, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_1

    nop

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_1a

    nop

    :goto_2
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_1b

    nop

    :goto_3
    goto/16 :goto_1c

    :goto_4
    goto/32 :goto_22

    nop

    :goto_5
    return-void

    :goto_6
    iget-object v6, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mUser:Landroid/os/UserHandle;

    goto/32 :goto_c

    nop

    :goto_7
    iget-object v5, v2, Lcom/android/internal/app/ResolverActivity$ResolvedComponentInfo;->name:Landroid/content/ComponentName;

    goto/32 :goto_e

    nop

    :goto_8
    return-void

    :goto_9
    goto/32 :goto_23

    nop

    :goto_a
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    goto/32 :goto_21

    nop

    :goto_b
    if-nez v0, :cond_1

    goto/32 :goto_9

    :cond_1
    goto/32 :goto_15

    nop

    :goto_c
    invoke-direct {v3, v4, v5, v6}, Landroid/app/prediction/AppTarget$Builder;-><init>(Landroid/app/prediction/AppTargetId;Ljava/lang/String;Landroid/os/UserHandle;)V

    goto/32 :goto_d

    nop

    :goto_d
    iget-object v4, v2, Lcom/android/internal/app/ResolverActivity$ResolvedComponentInfo;->name:Landroid/content/ComponentName;

    goto/32 :goto_16

    nop

    :goto_e
    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_6

    nop

    :goto_f
    invoke-virtual {v1, v0, v2, v3}, Landroid/app/prediction/AppPredictor;->sortTargets(Ljava/util/List;Ljava/util/concurrent/Executor;Ljava/util/function/Consumer;)V

    goto/32 :goto_5

    nop

    :goto_10
    const/4 v1, 0x0

    goto/32 :goto_14

    nop

    :goto_11
    iget-object v5, v2, Lcom/android/internal/app/ResolverActivity$ResolvedComponentInfo;->name:Landroid/content/ComponentName;

    goto/32 :goto_12

    nop

    :goto_12
    invoke-virtual {v5}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_20

    nop

    :goto_13
    invoke-virtual {v3}, Landroid/app/prediction/AppTarget$Builder;->build()Landroid/app/prediction/AppTarget;

    move-result-object v3

    goto/32 :goto_17

    nop

    :goto_14
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/32 :goto_8

    nop

    :goto_15
    iget-object v0, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mHandler:Landroid/os/Handler;

    goto/32 :goto_10

    nop

    :goto_16
    invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_1f

    nop

    :goto_17
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_3

    nop

    :goto_18
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    goto/32 :goto_0

    nop

    :goto_19
    new-instance v3, Landroid/app/prediction/AppTarget$Builder;

    goto/32 :goto_24

    nop

    :goto_1a
    check-cast v2, Lcom/android/internal/app/ResolverActivity$ResolvedComponentInfo;

    goto/32 :goto_19

    nop

    :goto_1b
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1c
    goto/32 :goto_18

    nop

    :goto_1d
    invoke-direct {v3, p0, p1}, Lcom/android/internal/app/AppPredictionServiceResolverComparator$$ExternalSyntheticLambda1;-><init>(Lcom/android/internal/app/AppPredictionServiceResolverComparator;Ljava/util/List;)V

    goto/32 :goto_f

    nop

    :goto_1e
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    goto/32 :goto_b

    nop

    :goto_1f
    invoke-virtual {v3, v4}, Landroid/app/prediction/AppTarget$Builder;->setClassName(Ljava/lang/String;)Landroid/app/prediction/AppTarget$Builder;

    move-result-object v3

    goto/32 :goto_13

    nop

    :goto_20
    invoke-direct {v4, v5}, Landroid/app/prediction/AppTargetId;-><init>(Ljava/lang/String;)V

    goto/32 :goto_7

    nop

    :goto_21
    new-instance v3, Lcom/android/internal/app/AppPredictionServiceResolverComparator$$ExternalSyntheticLambda1;

    goto/32 :goto_1d

    nop

    :goto_22
    iget-object v1, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mAppPredictor:Landroid/app/prediction/AppPredictor;

    goto/32 :goto_a

    nop

    :goto_23
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_2

    nop

    :goto_24
    new-instance v4, Landroid/app/prediction/AppTargetId;

    goto/32 :goto_11

    nop
.end method

.method getScore(Landroid/content/ComponentName;)F
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mComparatorModel:Lcom/android/internal/app/AppPredictionServiceResolverComparator$AppPredictionServiceComparatorModel;

    goto/32 :goto_2

    nop

    :goto_1
    return v0

    :goto_2
    invoke-virtual {v0, p1}, Lcom/android/internal/app/AppPredictionServiceResolverComparator$AppPredictionServiceComparatorModel;->getScore(Landroid/content/ComponentName;)F

    move-result v0

    goto/32 :goto_1

    nop
.end method

.method handleResultMessage(Landroid/os/Message;)V
    .locals 2

    goto/32 :goto_13

    nop

    :goto_0
    invoke-direct {p0, v0}, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->handleSortedAppTargets(Ljava/util/List;)V

    goto/32 :goto_4

    nop

    :goto_1
    if-eqz v0, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_3

    nop

    :goto_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto/32 :goto_1

    nop

    :goto_3
    iget-object v0, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mResolverRankerService:Lcom/android/internal/app/ResolverRankerServiceResolverComparator;

    goto/32 :goto_d

    nop

    :goto_4
    goto :goto_8

    :goto_5
    goto/32 :goto_2

    nop

    :goto_6
    const-string v1, "Unexpected null result"

    goto/32 :goto_10

    nop

    :goto_7
    goto :goto_9

    :goto_8
    nop

    :goto_9
    goto/32 :goto_12

    nop

    :goto_a
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto/32 :goto_11

    nop

    :goto_b
    const-string v0, "APSResolverComparator"

    goto/32 :goto_6

    nop

    :goto_c
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto/32 :goto_e

    nop

    :goto_d
    if-eqz v0, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_b

    nop

    :goto_e
    check-cast v0, Ljava/util/List;

    goto/32 :goto_0

    nop

    :goto_f
    if-eqz v0, :cond_2

    goto/32 :goto_5

    :cond_2
    goto/32 :goto_a

    nop

    :goto_10
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_7

    nop

    :goto_11
    if-nez v0, :cond_3

    goto/32 :goto_5

    :cond_3
    goto/32 :goto_c

    nop

    :goto_12
    return-void

    :goto_13
    iget v0, p1, Landroid/os/Message;->what:I

    goto/32 :goto_f

    nop
.end method

.method synthetic lambda$doCompute$0$com-android-internal-app-AppPredictionServiceResolverComparator()V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mHandler:Landroid/os/Handler;

    goto/32 :goto_2

    nop

    :goto_1
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/32 :goto_3

    nop

    :goto_2
    const/4 v1, 0x0

    goto/32 :goto_1

    nop

    :goto_3
    return-void
.end method

.method synthetic lambda$doCompute$1$com-android-internal-app-AppPredictionServiceResolverComparator(Ljava/util/List;Ljava/util/List;)V
    .locals 8

    goto/32 :goto_7

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mResolverRankerService:Lcom/android/internal/app/ResolverRankerServiceResolverComparator;

    goto/32 :goto_18

    nop

    :goto_1
    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_f

    nop

    :goto_2
    new-instance v6, Lcom/android/internal/app/AppPredictionServiceResolverComparator$$ExternalSyntheticLambda0;

    goto/32 :goto_8

    nop

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_17

    :cond_0
    goto/32 :goto_12

    nop

    :goto_4
    iput-object v0, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mComparatorModel:Lcom/android/internal/app/AppPredictionServiceResolverComparator$AppPredictionServiceComparatorModel;

    goto/32 :goto_0

    nop

    :goto_5
    iget-object v3, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mContext:Landroid/content/Context;

    goto/32 :goto_6

    nop

    :goto_6
    iget-object v4, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mIntent:Landroid/content/Intent;

    goto/32 :goto_19

    nop

    :goto_7
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    goto/32 :goto_e

    nop

    :goto_8
    invoke-direct {v6, p0}, Lcom/android/internal/app/AppPredictionServiceResolverComparator$$ExternalSyntheticLambda0;-><init>(Lcom/android/internal/app/AppPredictionServiceResolverComparator;)V

    goto/32 :goto_a

    nop

    :goto_9
    return-void

    :goto_a
    invoke-virtual {p0}, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->getChooserActivityLogger()Lcom/android/internal/app/ChooserActivityLogger;

    move-result-object v7

    goto/32 :goto_11

    nop

    :goto_b
    invoke-direct {p0}, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->buildUpdatedModel()Lcom/android/internal/app/AppPredictionServiceResolverComparator$AppPredictionServiceComparatorModel;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_c
    const-string v0, "AppPredictionService response received"

    goto/32 :goto_1

    nop

    :goto_d
    iput-object v0, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mResolverRankerService:Lcom/android/internal/app/ResolverRankerServiceResolverComparator;

    goto/32 :goto_b

    nop

    :goto_e
    const-string v1, "APSResolverComparator"

    goto/32 :goto_3

    nop

    :goto_f
    invoke-direct {p0, p2}, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->handleResult(Ljava/util/List;)V

    :goto_10
    goto/32 :goto_9

    nop

    :goto_11
    move-object v2, v0

    goto/32 :goto_13

    nop

    :goto_12
    const-string v0, "AppPredictionService disabled. Using resolver."

    goto/32 :goto_14

    nop

    :goto_13
    invoke-direct/range {v2 .. v7}, Lcom/android/internal/app/ResolverRankerServiceResolverComparator;-><init>(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;Lcom/android/internal/app/AbstractResolverComparator$AfterCompute;Lcom/android/internal/app/ChooserActivityLogger;)V

    goto/32 :goto_d

    nop

    :goto_14
    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_15

    nop

    :goto_15
    new-instance v0, Lcom/android/internal/app/ResolverRankerServiceResolverComparator;

    goto/32 :goto_5

    nop

    :goto_16
    goto :goto_10

    :goto_17
    goto/32 :goto_c

    nop

    :goto_18
    invoke-virtual {v0, p1}, Lcom/android/internal/app/ResolverRankerServiceResolverComparator;->compute(Ljava/util/List;)V

    goto/32 :goto_16

    nop

    :goto_19
    iget-object v5, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mReferrerPackage:Ljava/lang/String;

    goto/32 :goto_2

    nop
.end method

.method synthetic lambda$handleSortedAppTargets$2$com-android-internal-app-AppPredictionServiceResolverComparator(Landroid/app/prediction/AppTarget;)V
    .locals 4

    goto/32 :goto_4

    nop

    :goto_0
    invoke-virtual {p1}, Landroid/app/prediction/AppTarget;->getClassName()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_6

    nop

    :goto_1
    new-instance v1, Landroid/content/ComponentName;

    goto/32 :goto_5

    nop

    :goto_2
    invoke-virtual {p1}, Landroid/app/prediction/AppTarget;->getRank()I

    move-result v2

    goto/32 :goto_8

    nop

    :goto_3
    return-void

    :goto_4
    iget-object v0, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mTargetScores:Ljava/util/Map;

    goto/32 :goto_1

    nop

    :goto_5
    invoke-virtual {p1}, Landroid/app/prediction/AppTarget;->getPackageName()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_0

    nop

    :goto_6
    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_2

    nop

    :goto_7
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_3

    nop

    :goto_8
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/32 :goto_7

    nop
.end method

.method updateModel(Landroid/content/ComponentName;)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {v0, p1}, Lcom/android/internal/app/AppPredictionServiceResolverComparator$AppPredictionServiceComparatorModel;->notifyOnTargetSelected(Landroid/content/ComponentName;)V

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/internal/app/AppPredictionServiceResolverComparator;->mComparatorModel:Lcom/android/internal/app/AppPredictionServiceResolverComparator$AppPredictionServiceComparatorModel;

    goto/32 :goto_1

    nop
.end method
