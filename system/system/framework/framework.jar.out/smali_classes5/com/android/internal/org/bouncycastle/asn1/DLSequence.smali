.class public Lcom/android/internal/org/bouncycastle/asn1/DLSequence;
.super Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;


# instance fields
.field private bodyLength:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSequence;->bodyLength:I

    return-void
.end method

.method public constructor <init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;-><init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSequence;->bodyLength:I

    return-void
.end method

.method public constructor <init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;-><init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSequence;->bodyLength:I

    return-void
.end method

.method public constructor <init>([Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;-><init>([Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSequence;->bodyLength:I

    return-void
.end method

.method constructor <init>([Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;Z)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;-><init>([Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;Z)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSequence;->bodyLength:I

    return-void
.end method

.method private getBodyLength()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSequence;->bodyLength:I

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSequence;->elements:[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    array-length v0, v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    iget-object v3, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSequence;->elements:[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    aget-object v3, v3, v2

    invoke-interface {v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->toDLObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->encodedLength()I

    move-result v4

    add-int/2addr v1, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iput v1, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSequence;->bodyLength:I

    :cond_1
    iget v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSequence;->bodyLength:I

    return v0
.end method


# virtual methods
.method encode(Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_9

    nop

    :goto_0
    goto/16 :goto_2c

    :goto_1
    goto/32 :goto_31

    nop

    :goto_2
    const/16 v2, 0x10

    goto/32 :goto_18

    nop

    :goto_3
    new-array v4, v1, [Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    goto/32 :goto_19

    nop

    :goto_4
    aget-object v6, v4, v5

    goto/32 :goto_28

    nop

    :goto_5
    invoke-virtual {p1, v2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeLength(I)V

    goto/32 :goto_29

    nop

    :goto_6
    goto :goto_1a

    :goto_7
    goto/32 :goto_12

    nop

    :goto_8
    const/4 v3, 0x1

    goto/32 :goto_1d

    nop

    :goto_9
    if-nez p2, :cond_0

    goto/32 :goto_1c

    :cond_0
    goto/32 :goto_14

    nop

    :goto_a
    invoke-virtual {p1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->getDLSubStream()Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;

    move-result-object v0

    goto/32 :goto_21

    nop

    :goto_b
    iget-object v6, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSequence;->elements:[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_1e

    nop

    :goto_c
    invoke-interface {v6}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v6

    goto/32 :goto_d

    nop

    :goto_d
    invoke-virtual {v6}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->toDLObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v6

    goto/32 :goto_25

    nop

    :goto_e
    iget v2, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSequence;->bodyLength:I

    goto/32 :goto_8

    nop

    :goto_f
    if-lt v5, v1, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_b

    nop

    :goto_10
    invoke-virtual {v0, v4, v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writePrimitive(Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;Z)V

    goto/32 :goto_33

    nop

    :goto_11
    if-lt v2, v1, :cond_2

    goto/32 :goto_17

    :cond_2
    goto/32 :goto_32

    nop

    :goto_12
    iput v2, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSequence;->bodyLength:I

    goto/32 :goto_5

    nop

    :goto_13
    invoke-direct {p0}, Lcom/android/internal/org/bouncycastle/asn1/DLSequence;->getBodyLength()I

    move-result v2

    goto/32 :goto_1f

    nop

    :goto_14
    const/16 v0, 0x30

    goto/32 :goto_1b

    nop

    :goto_15
    invoke-virtual {v6}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->encodedLength()I

    move-result v7

    goto/32 :goto_27

    nop

    :goto_16
    goto :goto_2f

    :goto_17
    goto/32 :goto_30

    nop

    :goto_18
    if-gt v1, v2, :cond_3

    goto/32 :goto_1

    :cond_3
    goto/32 :goto_0

    nop

    :goto_19
    const/4 v5, 0x0

    :goto_1a
    goto/32 :goto_f

    nop

    :goto_1b
    invoke-virtual {p1, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    :goto_1c
    goto/32 :goto_a

    nop

    :goto_1d
    if-ltz v2, :cond_4

    goto/32 :goto_2c

    :cond_4
    goto/32 :goto_2

    nop

    :goto_1e
    aget-object v6, v6, v5

    goto/32 :goto_c

    nop

    :goto_1f
    invoke-virtual {p1, v2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeLength(I)V

    goto/32 :goto_2e

    nop

    :goto_20
    if-lt v5, v1, :cond_5

    goto/32 :goto_17

    :cond_5
    goto/32 :goto_4

    nop

    :goto_21
    iget-object v1, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSequence;->elements:[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_26

    nop

    :goto_22
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_6

    nop

    :goto_23
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_2b

    nop

    :goto_24
    invoke-interface {v4}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v4

    goto/32 :goto_10

    nop

    :goto_25
    aput-object v6, v4, v5

    goto/32 :goto_15

    nop

    :goto_26
    array-length v1, v1

    goto/32 :goto_e

    nop

    :goto_27
    add-int/2addr v2, v7

    goto/32 :goto_22

    nop

    :goto_28
    invoke-virtual {v0, v6, v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writePrimitive(Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;Z)V

    goto/32 :goto_23

    nop

    :goto_29
    const/4 v5, 0x0

    :goto_2a
    goto/32 :goto_20

    nop

    :goto_2b
    goto :goto_2a

    :goto_2c
    goto/32 :goto_13

    nop

    :goto_2d
    aget-object v4, v4, v2

    goto/32 :goto_24

    nop

    :goto_2e
    const/4 v2, 0x0

    :goto_2f
    goto/32 :goto_11

    nop

    :goto_30
    return-void

    :goto_31
    const/4 v2, 0x0

    goto/32 :goto_3

    nop

    :goto_32
    iget-object v4, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSequence;->elements:[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_2d

    nop

    :goto_33
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_16

    nop
.end method

.method encodedLength()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_4

    nop

    :goto_0
    add-int/2addr v1, v0

    goto/32 :goto_2

    nop

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_0

    nop

    :goto_2
    return v1

    :goto_3
    invoke-static {v0}, Lcom/android/internal/org/bouncycastle/asn1/StreamUtil;->calculateBodyLength(I)I

    move-result v1

    goto/32 :goto_1

    nop

    :goto_4
    invoke-direct {p0}, Lcom/android/internal/org/bouncycastle/asn1/DLSequence;->getBodyLength()I

    move-result v0

    goto/32 :goto_3

    nop
.end method

.method toDLObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    return-object p0
.end method
