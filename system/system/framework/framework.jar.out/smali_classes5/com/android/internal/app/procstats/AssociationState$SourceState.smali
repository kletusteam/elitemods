.class public final Lcom/android/internal/app/procstats/AssociationState$SourceState;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/app/procstats/AssociationState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SourceState"
.end annotation


# instance fields
.field mActiveCount:I

.field mActiveDuration:J

.field mActiveDurations:Lcom/android/internal/app/procstats/DurationsTable;

.field mActiveNesting:I

.field mActiveProcState:I

.field mActiveStartUptime:J

.field private final mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

.field private mCommonSourceState:Lcom/android/internal/app/procstats/AssociationState$SourceState;

.field mCount:I

.field mDuration:J

.field mInTrackingList:Z

.field final mKey:Lcom/android/internal/app/procstats/AssociationState$SourceKey;

.field mNesting:I

.field mProcState:I

.field mProcStateSeq:I

.field private final mProcessStats:Lcom/android/internal/app/procstats/ProcessStats;

.field mStartUptime:J

.field private final mTargetProcess:Lcom/android/internal/app/procstats/ProcessState;

.field mTrackingUptime:J


# direct methods
.method constructor <init>(Lcom/android/internal/app/procstats/ProcessStats;Lcom/android/internal/app/procstats/AssociationState;Lcom/android/internal/app/procstats/ProcessState;Lcom/android/internal/app/procstats/AssociationState$SourceKey;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mProcStateSeq:I

    iput v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mProcState:I

    iput v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveProcState:I

    iput-object p1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mProcessStats:Lcom/android/internal/app/procstats/ProcessStats;

    iput-object p2, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    iput-object p3, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mTargetProcess:Lcom/android/internal/app/procstats/ProcessState;

    iput-object p4, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mKey:Lcom/android/internal/app/procstats/AssociationState$SourceKey;

    return-void
.end method

.method private getCommonSourceState(Z)Lcom/android/internal/app/procstats/AssociationState$SourceState;
    .locals 2

    iget-object v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mCommonSourceState:Lcom/android/internal/app/procstats/AssociationState$SourceState;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mTargetProcess:Lcom/android/internal/app/procstats/ProcessState;

    iget-object v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mKey:Lcom/android/internal/app/procstats/AssociationState$SourceKey;

    invoke-virtual {v0, v1}, Lcom/android/internal/app/procstats/ProcessState;->getOrCreateSourceState(Lcom/android/internal/app/procstats/AssociationState$SourceKey;)Lcom/android/internal/app/procstats/AssociationState$SourceState;

    move-result-object v0

    iput-object v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mCommonSourceState:Lcom/android/internal/app/procstats/AssociationState$SourceState;

    :cond_0
    iget-object v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mCommonSourceState:Lcom/android/internal/app/procstats/AssociationState$SourceState;

    return-object v0
.end method

.method private stopTracking(J)V
    .locals 5

    iget-object v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/android/internal/app/procstats/AssociationState;->-$$Nest$fgetmTotalNesting(Lcom/android/internal/app/procstats/AssociationState;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Lcom/android/internal/app/procstats/AssociationState;->-$$Nest$fputmTotalNesting(Lcom/android/internal/app/procstats/AssociationState;I)V

    iget-object v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    invoke-static {v0}, Lcom/android/internal/app/procstats/AssociationState;->-$$Nest$fgetmTotalNesting(Lcom/android/internal/app/procstats/AssociationState;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    invoke-static {v0}, Lcom/android/internal/app/procstats/AssociationState;->-$$Nest$fgetmTotalDuration(Lcom/android/internal/app/procstats/AssociationState;)J

    move-result-wide v1

    iget-object v3, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    invoke-static {v3}, Lcom/android/internal/app/procstats/AssociationState;->-$$Nest$fgetmTotalStartUptime(Lcom/android/internal/app/procstats/AssociationState;)J

    move-result-wide v3

    sub-long v3, p1, v3

    add-long/2addr v1, v3

    invoke-static {v0, v1, v2}, Lcom/android/internal/app/procstats/AssociationState;->-$$Nest$fputmTotalDuration(Lcom/android/internal/app/procstats/AssociationState;J)V

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/android/internal/app/procstats/AssociationState$SourceState;->stopActive(J)V

    iget-boolean v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mInTrackingList:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mInTrackingList:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mProcState:I

    iget-object v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mProcessStats:Lcom/android/internal/app/procstats/ProcessStats;

    iget-object v0, v0, Lcom/android/internal/app/procstats/ProcessStats;->mTrackingAssociations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ltz v1, :cond_2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p0, :cond_1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    return-void

    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Stop tracking didn\'t find in tracking list: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ProcessStats"

    invoke-static {v2, v1}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    return-void
.end method

.method private stopTrackingProcState()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mInTrackingList:Z

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mProcState:I

    iget-object v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/android/internal/app/procstats/AssociationState$SourceState;->getCommonSourceState(Z)Lcom/android/internal/app/procstats/AssociationState$SourceState;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {v0}, Lcom/android/internal/app/procstats/AssociationState$SourceState;->stopTrackingProcState()V

    :cond_0
    return-void
.end method


# virtual methods
.method add(Lcom/android/internal/app/procstats/AssociationState$SourceState;)V
    .locals 9

    goto/32 :goto_24

    nop

    :goto_0
    invoke-virtual {p0}, Lcom/android/internal/app/procstats/AssociationState$SourceState;->makeDurations()V

    goto/32 :goto_18

    nop

    :goto_1
    goto/16 :goto_49

    :goto_2
    goto/32 :goto_2d

    nop

    :goto_3
    iget v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveCount:I

    goto/32 :goto_42

    nop

    :goto_4
    iget-wide v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDuration:J

    goto/32 :goto_35

    nop

    :goto_5
    if-nez v2, :cond_0

    goto/32 :goto_47

    :cond_0
    goto/32 :goto_45

    nop

    :goto_6
    if-eqz v4, :cond_1

    goto/32 :goto_38

    :cond_1
    goto/32 :goto_3f

    nop

    :goto_7
    const-wide/16 v2, 0x0

    goto/32 :goto_28

    nop

    :goto_8
    add-long/2addr v6, v0

    goto/32 :goto_4a

    nop

    :goto_9
    iget v2, p1, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveProcState:I

    goto/32 :goto_a

    nop

    :goto_a
    iput v2, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveProcState:I

    goto/32 :goto_48

    nop

    :goto_b
    invoke-virtual {v0, v1}, Lcom/android/internal/app/procstats/DurationsTable;->addDurations(Lcom/android/internal/app/procstats/DurationsTable;)V

    goto/32 :goto_4

    nop

    :goto_c
    iget-object v2, p1, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDurations:Lcom/android/internal/app/procstats/DurationsTable;

    goto/32 :goto_5

    nop

    :goto_d
    iget v4, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveProcState:I

    goto/32 :goto_12

    nop

    :goto_e
    goto/16 :goto_49

    :goto_f
    goto/32 :goto_9

    nop

    :goto_10
    iget-object v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDurations:Lcom/android/internal/app/procstats/DurationsTable;

    goto/32 :goto_36

    nop

    :goto_11
    const/4 v5, -0x1

    goto/32 :goto_3c

    nop

    :goto_12
    iget v8, p1, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveProcState:I

    goto/32 :goto_39

    nop

    :goto_13
    iget v1, p1, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mCount:I

    goto/32 :goto_34

    nop

    :goto_14
    iget-object v4, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDurations:Lcom/android/internal/app/procstats/DurationsTable;

    goto/32 :goto_1f

    nop

    :goto_15
    invoke-virtual {p0}, Lcom/android/internal/app/procstats/AssociationState$SourceState;->makeDurations()V

    goto/32 :goto_16

    nop

    :goto_16
    iget-object v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDurations:Lcom/android/internal/app/procstats/DurationsTable;

    goto/32 :goto_27

    nop

    :goto_17
    add-long/2addr v0, v2

    goto/32 :goto_20

    nop

    :goto_18
    iget-object v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDurations:Lcom/android/internal/app/procstats/DurationsTable;

    goto/32 :goto_3b

    nop

    :goto_19
    add-int/2addr v0, v1

    goto/32 :goto_40

    nop

    :goto_1a
    iput v5, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveProcState:I

    goto/32 :goto_e

    nop

    :goto_1b
    iput v5, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveProcState:I

    goto/32 :goto_1

    nop

    :goto_1c
    iput-wide v2, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDuration:J

    goto/32 :goto_1b

    nop

    :goto_1d
    iput v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mCount:I

    goto/32 :goto_22

    nop

    :goto_1e
    iget-wide v2, p1, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mDuration:J

    goto/32 :goto_17

    nop

    :goto_1f
    if-nez v4, :cond_2

    goto/32 :goto_33

    :cond_2
    goto/32 :goto_c

    nop

    :goto_20
    iput-wide v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mDuration:J

    goto/32 :goto_3

    nop

    :goto_21
    iget-object v4, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDurations:Lcom/android/internal/app/procstats/DurationsTable;

    goto/32 :goto_23

    nop

    :goto_22
    iget-wide v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mDuration:J

    goto/32 :goto_1e

    nop

    :goto_23
    iget v6, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveProcState:I

    goto/32 :goto_29

    nop

    :goto_24
    iget v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mCount:I

    goto/32 :goto_13

    nop

    :goto_25
    goto/16 :goto_49

    :goto_26
    goto/32 :goto_0

    nop

    :goto_27
    iget-object v1, p1, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDurations:Lcom/android/internal/app/procstats/DurationsTable;

    goto/32 :goto_b

    nop

    :goto_28
    cmp-long v4, v0, v2

    goto/32 :goto_6

    nop

    :goto_29
    invoke-virtual {v4, v6, v0, v1}, Lcom/android/internal/app/procstats/DurationsTable;->addDuration(IJ)V

    goto/32 :goto_1c

    nop

    :goto_2a
    if-nez v4, :cond_3

    goto/32 :goto_49

    :cond_3
    goto/32 :goto_21

    nop

    :goto_2b
    iget-object v4, p1, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDurations:Lcom/android/internal/app/procstats/DurationsTable;

    goto/32 :goto_11

    nop

    :goto_2c
    iget-wide v0, p1, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDuration:J

    goto/32 :goto_7

    nop

    :goto_2d
    iget-wide v6, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDuration:J

    goto/32 :goto_2e

    nop

    :goto_2e
    cmp-long v4, v6, v2

    goto/32 :goto_3d

    nop

    :goto_2f
    invoke-virtual {v0, v1, v6, v7}, Lcom/android/internal/app/procstats/DurationsTable;->addDuration(IJ)V

    goto/32 :goto_10

    nop

    :goto_30
    iget v2, p1, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveProcState:I

    goto/32 :goto_3a

    nop

    :goto_31
    iget-wide v6, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDuration:J

    goto/32 :goto_2f

    nop

    :goto_32
    goto :goto_49

    :goto_33
    goto/32 :goto_2b

    nop

    :goto_34
    add-int/2addr v0, v1

    goto/32 :goto_1d

    nop

    :goto_35
    cmp-long v4, v0, v2

    goto/32 :goto_2a

    nop

    :goto_36
    iget v1, p1, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveProcState:I

    goto/32 :goto_41

    nop

    :goto_37
    if-nez v4, :cond_4

    goto/32 :goto_49

    :cond_4
    :goto_38
    goto/32 :goto_14

    nop

    :goto_39
    if-eq v4, v8, :cond_5

    goto/32 :goto_26

    :cond_5
    goto/32 :goto_8

    nop

    :goto_3a
    invoke-virtual {v4, v2, v0, v1}, Lcom/android/internal/app/procstats/DurationsTable;->addDuration(IJ)V

    goto/32 :goto_32

    nop

    :goto_3b
    iget v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveProcState:I

    goto/32 :goto_31

    nop

    :goto_3c
    if-nez v4, :cond_6

    goto/32 :goto_2

    :cond_6
    goto/32 :goto_15

    nop

    :goto_3d
    if-nez v4, :cond_7

    goto/32 :goto_f

    :cond_7
    goto/32 :goto_d

    nop

    :goto_3e
    iput-wide v2, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDuration:J

    goto/32 :goto_1a

    nop

    :goto_3f
    iget-object v4, p1, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDurations:Lcom/android/internal/app/procstats/DurationsTable;

    goto/32 :goto_37

    nop

    :goto_40
    iput v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveCount:I

    goto/32 :goto_2c

    nop

    :goto_41
    iget-wide v6, p1, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDuration:J

    goto/32 :goto_43

    nop

    :goto_42
    iget v1, p1, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveCount:I

    goto/32 :goto_19

    nop

    :goto_43
    invoke-virtual {v0, v1, v6, v7}, Lcom/android/internal/app/procstats/DurationsTable;->addDuration(IJ)V

    goto/32 :goto_3e

    nop

    :goto_44
    return-void

    :goto_45
    invoke-virtual {v4, v2}, Lcom/android/internal/app/procstats/DurationsTable;->addDurations(Lcom/android/internal/app/procstats/DurationsTable;)V

    goto/32 :goto_46

    nop

    :goto_46
    goto :goto_49

    :goto_47
    goto/32 :goto_30

    nop

    :goto_48
    iput-wide v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDuration:J

    :goto_49
    goto/32 :goto_44

    nop

    :goto_4a
    iput-wide v6, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDuration:J

    goto/32 :goto_25

    nop
.end method

.method commitStateTime(J)V
    .locals 4

    goto/32 :goto_b

    nop

    :goto_0
    const-wide/16 v2, 0x0

    goto/32 :goto_d

    nop

    :goto_1
    iget-wide v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mDuration:J

    goto/32 :goto_14

    nop

    :goto_2
    iget-wide v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveStartUptime:J

    goto/32 :goto_0

    nop

    :goto_3
    iget-object v2, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDurations:Lcom/android/internal/app/procstats/DurationsTable;

    goto/32 :goto_16

    nop

    :goto_4
    if-gtz v2, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_15

    nop

    :goto_5
    iput-wide p1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveStartUptime:J

    goto/32 :goto_3

    nop

    :goto_6
    goto :goto_12

    :goto_7
    goto/32 :goto_19

    nop

    :goto_8
    add-long/2addr v2, v0

    goto/32 :goto_11

    nop

    :goto_9
    iput-wide p1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mStartUptime:J

    :goto_a
    goto/32 :goto_2

    nop

    :goto_b
    iget v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mNesting:I

    goto/32 :goto_18

    nop

    :goto_c
    iget v3, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveProcState:I

    goto/32 :goto_10

    nop

    :goto_d
    cmp-long v2, v0, v2

    goto/32 :goto_4

    nop

    :goto_e
    return-void

    :goto_f
    add-long/2addr v0, v2

    goto/32 :goto_13

    nop

    :goto_10
    invoke-virtual {v2, v3, v0, v1}, Lcom/android/internal/app/procstats/DurationsTable;->addDuration(IJ)V

    goto/32 :goto_6

    nop

    :goto_11
    iput-wide v2, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDuration:J

    :goto_12
    goto/32 :goto_e

    nop

    :goto_13
    iput-wide v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mDuration:J

    goto/32 :goto_9

    nop

    :goto_14
    iget-wide v2, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mStartUptime:J

    goto/32 :goto_17

    nop

    :goto_15
    sub-long v0, p1, v0

    goto/32 :goto_5

    nop

    :goto_16
    if-nez v2, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_c

    nop

    :goto_17
    sub-long v2, p1, v2

    goto/32 :goto_f

    nop

    :goto_18
    if-gtz v0, :cond_2

    goto/32 :goto_a

    :cond_2
    goto/32 :goto_1

    nop

    :goto_19
    iget-wide v2, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDuration:J

    goto/32 :goto_8

    nop
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getAssociationState()Lcom/android/internal/app/procstats/AssociationState;
    .locals 1

    iget-object v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    return-object v0
.end method

.method public getProcessName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mKey:Lcom/android/internal/app/procstats/AssociationState$SourceKey;

    iget-object v0, v0, Lcom/android/internal/app/procstats/AssociationState$SourceKey;->mProcess:Ljava/lang/String;

    return-object v0
.end method

.method public getUid()I
    .locals 1

    iget-object v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mKey:Lcom/android/internal/app/procstats/AssociationState$SourceKey;

    iget v0, v0, Lcom/android/internal/app/procstats/AssociationState$SourceKey;->mUid:I

    return v0
.end method

.method isInUse()Z
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    if-gtz v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_3

    nop

    :goto_1
    return v0

    :goto_2
    iget v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mNesting:I

    goto/32 :goto_0

    nop

    :goto_3
    const/4 v0, 0x1

    goto/32 :goto_4

    nop

    :goto_4
    goto :goto_7

    :goto_5
    goto/32 :goto_6

    nop

    :goto_6
    const/4 v0, 0x0

    :goto_7
    goto/32 :goto_1

    nop
.end method

.method makeDurations()V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    iget-object v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mProcessStats:Lcom/android/internal/app/procstats/ProcessStats;

    goto/32 :goto_1

    nop

    :goto_1
    iget-object v1, v1, Lcom/android/internal/app/procstats/ProcessStats;->mTableData:Lcom/android/internal/app/procstats/SparseMappingTable;

    goto/32 :goto_3

    nop

    :goto_2
    new-instance v0, Lcom/android/internal/app/procstats/DurationsTable;

    goto/32 :goto_0

    nop

    :goto_3
    invoke-direct {v0, v1}, Lcom/android/internal/app/procstats/DurationsTable;-><init>(Lcom/android/internal/app/procstats/SparseMappingTable;)V

    goto/32 :goto_5

    nop

    :goto_4
    return-void

    :goto_5
    iput-object v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDurations:Lcom/android/internal/app/procstats/DurationsTable;

    goto/32 :goto_4

    nop
.end method

.method readFromParcel(Landroid/os/Parcel;)Ljava/lang/String;
    .locals 2

    goto/32 :goto_1b

    nop

    :goto_0
    invoke-virtual {p0}, Lcom/android/internal/app/procstats/AssociationState$SourceState;->makeDurations()V

    goto/32 :goto_13

    nop

    :goto_1
    invoke-virtual {p0}, Lcom/android/internal/app/procstats/AssociationState$SourceState;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_c

    nop

    :goto_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_14

    nop

    :goto_4
    iput v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveProcState:I

    goto/32 :goto_8

    nop

    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_1f

    nop

    :goto_6
    iput-wide v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mDuration:J

    goto/32 :goto_11

    nop

    :goto_7
    iput v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mCount:I

    goto/32 :goto_12

    nop

    :goto_8
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    goto/32 :goto_18

    nop

    :goto_9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_b

    nop

    :goto_a
    if-eqz v0, :cond_0

    goto/32 :goto_19

    :cond_0
    goto/32 :goto_5

    nop

    :goto_b
    const-string v1, " <- "

    goto/32 :goto_2

    nop

    :goto_c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_d
    const/4 v0, 0x0

    goto/32 :goto_1d

    nop

    :goto_e
    const-string v1, "Duration table corrupt: "

    goto/32 :goto_1a

    nop

    :goto_f
    invoke-virtual {v0, p1}, Lcom/android/internal/app/procstats/DurationsTable;->readFromParcel(Landroid/os/Parcel;)Z

    move-result v0

    goto/32 :goto_a

    nop

    :goto_10
    if-nez v0, :cond_1

    goto/32 :goto_15

    :cond_1
    goto/32 :goto_0

    nop

    :goto_11
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    goto/32 :goto_16

    nop

    :goto_12
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    goto/32 :goto_6

    nop

    :goto_13
    iget-object v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDurations:Lcom/android/internal/app/procstats/DurationsTable;

    goto/32 :goto_f

    nop

    :goto_14
    return-object v0

    :goto_15
    goto/32 :goto_1e

    nop

    :goto_16
    iput v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveCount:I

    goto/32 :goto_17

    nop

    :goto_17
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    goto/32 :goto_10

    nop

    :goto_18
    iput-wide v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDuration:J

    :goto_19
    goto/32 :goto_d

    nop

    :goto_1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_1c

    nop

    :goto_1b
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    goto/32 :goto_7

    nop

    :goto_1c
    iget-object v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mKey:Lcom/android/internal/app/procstats/AssociationState$SourceKey;

    goto/32 :goto_9

    nop

    :goto_1d
    return-object v0

    :goto_1e
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    goto/32 :goto_4

    nop

    :goto_1f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_e

    nop
.end method

.method resetSafely(J)V
    .locals 7

    goto/32 :goto_5

    nop

    :goto_0
    if-gtz v5, :cond_0

    goto/32 :goto_f

    :cond_0
    goto/32 :goto_6

    nop

    :goto_1
    cmp-long v5, v5, v3

    goto/32 :goto_0

    nop

    :goto_2
    const-wide/16 v3, 0x0

    goto/32 :goto_1c

    nop

    :goto_3
    iput v2, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveCount:I

    :goto_4
    goto/32 :goto_7

    nop

    :goto_5
    invoke-virtual {p0}, Lcom/android/internal/app/procstats/AssociationState$SourceState;->isInUse()Z

    move-result v0

    goto/32 :goto_a

    nop

    :goto_6
    iput v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveCount:I

    goto/32 :goto_1b

    nop

    :goto_7
    iput-wide v3, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDuration:J

    goto/32 :goto_11

    nop

    :goto_8
    iput-wide p1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mStartUptime:J

    goto/32 :goto_2

    nop

    :goto_9
    iget-object v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    goto/32 :goto_b

    nop

    :goto_a
    const/4 v1, 0x0

    goto/32 :goto_c

    nop

    :goto_b
    if-nez v0, :cond_1

    goto/32 :goto_19

    :cond_1
    goto/32 :goto_17

    nop

    :goto_c
    const/4 v2, 0x0

    goto/32 :goto_16

    nop

    :goto_d
    if-nez v0, :cond_2

    goto/32 :goto_19

    :cond_2
    goto/32 :goto_13

    nop

    :goto_e
    goto :goto_4

    :goto_f
    goto/32 :goto_3

    nop

    :goto_10
    return-void

    :goto_11
    iput-object v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDurations:Lcom/android/internal/app/procstats/DurationsTable;

    :goto_12
    goto/32 :goto_9

    nop

    :goto_13
    invoke-virtual {v0, p1, p2}, Lcom/android/internal/app/procstats/AssociationState$SourceState;->resetSafely(J)V

    goto/32 :goto_18

    nop

    :goto_14
    iput v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mCount:I

    goto/32 :goto_8

    nop

    :goto_15
    const/4 v0, 0x1

    goto/32 :goto_14

    nop

    :goto_16
    if-nez v0, :cond_3

    goto/32 :goto_12

    :cond_3
    goto/32 :goto_15

    nop

    :goto_17
    invoke-direct {p0, v2}, Lcom/android/internal/app/procstats/AssociationState$SourceState;->getCommonSourceState(Z)Lcom/android/internal/app/procstats/AssociationState$SourceState;

    move-result-object v0

    goto/32 :goto_d

    nop

    :goto_18
    iput-object v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mCommonSourceState:Lcom/android/internal/app/procstats/AssociationState$SourceState;

    :goto_19
    goto/32 :goto_10

    nop

    :goto_1a
    iget-wide v5, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveStartUptime:J

    goto/32 :goto_1

    nop

    :goto_1b
    iput-wide p1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveStartUptime:J

    goto/32 :goto_e

    nop

    :goto_1c
    iput-wide v3, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mDuration:J

    goto/32 :goto_1a

    nop
.end method

.method start()J
    .locals 3

    goto/32 :goto_5

    nop

    :goto_0
    if-nez v2, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_9

    nop

    :goto_1
    invoke-direct {p0, v2}, Lcom/android/internal/app/procstats/AssociationState$SourceState;->getCommonSourceState(Z)Lcom/android/internal/app/procstats/AssociationState$SourceState;

    move-result-object v2

    goto/32 :goto_8

    nop

    :goto_2
    return-wide v0

    :goto_3
    invoke-virtual {v2, v0, v1}, Lcom/android/internal/app/procstats/AssociationState$SourceState;->start(J)J

    :goto_4
    goto/32 :goto_2

    nop

    :goto_5
    const-wide/16 v0, -0x1

    goto/32 :goto_6

    nop

    :goto_6
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/app/procstats/AssociationState$SourceState;->start(J)J

    move-result-wide v0

    goto/32 :goto_7

    nop

    :goto_7
    iget-object v2, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    goto/32 :goto_0

    nop

    :goto_8
    if-nez v2, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_3

    nop

    :goto_9
    const/4 v2, 0x1

    goto/32 :goto_1

    nop
.end method

.method start(J)J
    .locals 4

    goto/32 :goto_d

    nop

    :goto_0
    if-ltz v0, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_a

    nop

    :goto_1
    return-wide p1

    :goto_2
    iput v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mNesting:I

    goto/32 :goto_7

    nop

    :goto_3
    const/4 v1, 0x1

    goto/32 :goto_9

    nop

    :goto_4
    add-int/2addr v0, v1

    goto/32 :goto_6

    nop

    :goto_5
    cmp-long v0, p1, v2

    goto/32 :goto_0

    nop

    :goto_6
    iput v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mCount:I

    goto/32 :goto_e

    nop

    :goto_7
    if-eq v0, v1, :cond_1

    goto/32 :goto_f

    :cond_1
    goto/32 :goto_8

    nop

    :goto_8
    const-wide/16 v2, 0x0

    goto/32 :goto_5

    nop

    :goto_9
    add-int/2addr v0, v1

    goto/32 :goto_2

    nop

    :goto_a
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide p1

    :goto_b
    goto/32 :goto_c

    nop

    :goto_c
    iget v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mCount:I

    goto/32 :goto_4

    nop

    :goto_d
    iget v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mNesting:I

    goto/32 :goto_3

    nop

    :goto_e
    iput-wide p1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mStartUptime:J

    :goto_f
    goto/32 :goto_1

    nop
.end method

.method startActive(J)V
    .locals 9

    goto/32 :goto_16

    nop

    :goto_0
    iget-object v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    goto/32 :goto_23

    nop

    :goto_1
    iget-object v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    goto/32 :goto_42

    nop

    :goto_2
    iget-wide v3, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveStartUptime:J

    goto/32 :goto_2b

    nop

    :goto_3
    const-string/jumbo v3, "startActive while not tracking: "

    goto/32 :goto_3e

    nop

    :goto_4
    if-ne v1, v3, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_46

    nop

    :goto_5
    if-eqz v1, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_13

    nop

    :goto_6
    invoke-static {v1}, Lcom/android/internal/app/procstats/AssociationState;->-$$Nest$fgetmTotalActiveNesting(Lcom/android/internal/app/procstats/AssociationState;)I

    move-result v3

    goto/32 :goto_14

    nop

    :goto_7
    iput-wide p1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveStartUptime:J

    goto/32 :goto_1c

    nop

    :goto_8
    const-string v3, "ProcessStats"

    goto/32 :goto_11

    nop

    :goto_9
    iput-wide v5, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDuration:J

    :goto_a
    goto/32 :goto_51

    nop

    :goto_b
    goto :goto_f

    :goto_c
    goto/32 :goto_33

    nop

    :goto_d
    if-nez v1, :cond_2

    goto/32 :goto_26

    :cond_2
    goto/32 :goto_2

    nop

    :goto_e
    iput v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveNesting:I

    :goto_f
    goto/32 :goto_53

    nop

    :goto_10
    iput v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveProcState:I

    goto/32 :goto_25

    nop

    :goto_11
    invoke-static {v3, v1}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :goto_12
    goto/32 :goto_54

    nop

    :goto_13
    iput-wide p1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveStartUptime:J

    goto/32 :goto_37

    nop

    :goto_14
    add-int/2addr v3, v2

    goto/32 :goto_1b

    nop

    :goto_15
    if-nez v1, :cond_3

    goto/32 :goto_3b

    :cond_3
    goto/32 :goto_3a

    nop

    :goto_16
    const/4 v0, 0x0

    goto/32 :goto_39

    nop

    :goto_17
    iget-wide v3, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDuration:J

    goto/32 :goto_1a

    nop

    :goto_18
    if-nez v1, :cond_4

    goto/32 :goto_12

    :cond_4
    goto/32 :goto_1d

    nop

    :goto_19
    return-void

    :goto_1a
    add-long/2addr v3, p1

    goto/32 :goto_24

    nop

    :goto_1b
    invoke-static {v1, v3}, Lcom/android/internal/app/procstats/AssociationState;->-$$Nest$fputmTotalActiveNesting(Lcom/android/internal/app/procstats/AssociationState;I)V

    goto/32 :goto_48

    nop

    :goto_1c
    iget-object v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    goto/32 :goto_15

    nop

    :goto_1d
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_4e

    nop

    :goto_1e
    if-nez v1, :cond_5

    goto/32 :goto_f

    :cond_5
    goto/32 :goto_6

    nop

    :goto_1f
    iget v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveCount:I

    goto/32 :goto_3f

    nop

    :goto_20
    invoke-virtual {p0}, Lcom/android/internal/app/procstats/AssociationState$SourceState;->makeDurations()V

    :goto_21
    goto/32 :goto_47

    nop

    :goto_22
    iput v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveNesting:I

    goto/32 :goto_1f

    nop

    :goto_23
    invoke-static {v1, p1, p2}, Lcom/android/internal/app/procstats/AssociationState;->-$$Nest$fputmTotalActiveStartUptime(Lcom/android/internal/app/procstats/AssociationState;J)V

    goto/32 :goto_b

    nop

    :goto_24
    iget-wide v7, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveStartUptime:J

    goto/32 :goto_34

    nop

    :goto_25
    goto :goto_12

    :goto_26
    goto/32 :goto_36

    nop

    :goto_27
    const/4 v2, 0x1

    goto/32 :goto_d

    nop

    :goto_28
    iget-object v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    goto/32 :goto_1e

    nop

    :goto_29
    if-eq v1, v2, :cond_6

    goto/32 :goto_f

    :cond_6
    goto/32 :goto_1

    nop

    :goto_2a
    add-int/2addr v1, v2

    goto/32 :goto_e

    nop

    :goto_2b
    const-wide/16 v5, 0x0

    goto/32 :goto_52

    nop

    :goto_2c
    invoke-static {v1}, Lcom/android/internal/app/procstats/AssociationState;->-$$Nest$fgetmTotalActiveNesting(Lcom/android/internal/app/procstats/AssociationState;)I

    move-result v1

    goto/32 :goto_29

    nop

    :goto_2d
    add-int/2addr v1, v2

    goto/32 :goto_22

    nop

    :goto_2e
    iput v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveCount:I

    goto/32 :goto_44

    nop

    :goto_2f
    if-eqz v1, :cond_7

    goto/32 :goto_21

    :cond_7
    goto/32 :goto_20

    nop

    :goto_30
    if-nez v1, :cond_8

    goto/32 :goto_a

    :cond_8
    goto/32 :goto_35

    nop

    :goto_31
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_3c

    nop

    :goto_32
    add-int/2addr v3, v2

    goto/32 :goto_49

    nop

    :goto_33
    iget-object v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    goto/32 :goto_40

    nop

    :goto_34
    sub-long/2addr v3, v7

    goto/32 :goto_7

    nop

    :goto_35
    iget-object v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDurations:Lcom/android/internal/app/procstats/DurationsTable;

    goto/32 :goto_2f

    nop

    :goto_36
    iget-object v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    goto/32 :goto_18

    nop

    :goto_37
    iget v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveNesting:I

    goto/32 :goto_2d

    nop

    :goto_38
    if-ne v1, v3, :cond_9

    goto/32 :goto_a

    :cond_9
    goto/32 :goto_17

    nop

    :goto_39
    iget-boolean v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mInTrackingList:Z

    goto/32 :goto_27

    nop

    :goto_3a
    const/4 v0, 0x1

    :goto_3b
    goto/32 :goto_50

    nop

    :goto_3c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_3d
    iget v7, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveProcState:I

    goto/32 :goto_41

    nop

    :goto_3e
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_31

    nop

    :goto_3f
    add-int/2addr v1, v2

    goto/32 :goto_2e

    nop

    :goto_40
    if-eqz v1, :cond_a

    goto/32 :goto_f

    :cond_a
    goto/32 :goto_4a

    nop

    :goto_41
    invoke-virtual {v1, v7, v3, v4}, Lcom/android/internal/app/procstats/DurationsTable;->addDuration(IJ)V

    goto/32 :goto_9

    nop

    :goto_42
    invoke-static {v1}, Lcom/android/internal/app/procstats/AssociationState;->-$$Nest$fgetmTotalActiveCount(Lcom/android/internal/app/procstats/AssociationState;)I

    move-result v3

    goto/32 :goto_32

    nop

    :goto_43
    if-nez v1, :cond_b

    goto/32 :goto_4c

    :cond_b
    goto/32 :goto_4f

    nop

    :goto_44
    const/4 v0, 0x1

    goto/32 :goto_28

    nop

    :goto_45
    iget v3, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mProcState:I

    goto/32 :goto_4

    nop

    :goto_46
    const/4 v3, -0x1

    goto/32 :goto_38

    nop

    :goto_47
    iget-object v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDurations:Lcom/android/internal/app/procstats/DurationsTable;

    goto/32 :goto_3d

    nop

    :goto_48
    iget-object v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    goto/32 :goto_2c

    nop

    :goto_49
    invoke-static {v1, v3}, Lcom/android/internal/app/procstats/AssociationState;->-$$Nest$fputmTotalActiveCount(Lcom/android/internal/app/procstats/AssociationState;I)V

    goto/32 :goto_0

    nop

    :goto_4a
    iget v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveNesting:I

    goto/32 :goto_2a

    nop

    :goto_4b
    invoke-virtual {v1, p1, p2}, Lcom/android/internal/app/procstats/AssociationState$SourceState;->startActive(J)V

    :goto_4c
    goto/32 :goto_19

    nop

    :goto_4d
    invoke-direct {p0, v2}, Lcom/android/internal/app/procstats/AssociationState$SourceState;->getCommonSourceState(Z)Lcom/android/internal/app/procstats/AssociationState$SourceState;

    move-result-object v1

    goto/32 :goto_43

    nop

    :goto_4e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_3

    nop

    :goto_4f
    if-nez v0, :cond_c

    goto/32 :goto_4c

    :cond_c
    goto/32 :goto_4b

    nop

    :goto_50
    cmp-long v1, v3, v5

    goto/32 :goto_30

    nop

    :goto_51
    iget v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mProcState:I

    goto/32 :goto_10

    nop

    :goto_52
    cmp-long v1, v3, v5

    goto/32 :goto_5

    nop

    :goto_53
    iget v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveProcState:I

    goto/32 :goto_45

    nop

    :goto_54
    iget-object v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    goto/32 :goto_55

    nop

    :goto_55
    if-nez v1, :cond_d

    goto/32 :goto_4c

    :cond_d
    goto/32 :goto_4d

    nop
.end method

.method stop(J)J
    .locals 4

    goto/32 :goto_c

    nop

    :goto_0
    iput-wide v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mDuration:J

    goto/32 :goto_a

    nop

    :goto_1
    add-long/2addr v0, v2

    goto/32 :goto_0

    nop

    :goto_2
    return-wide p1

    :goto_3
    if-eqz v0, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_10

    nop

    :goto_4
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide p1

    :goto_5
    goto/32 :goto_9

    nop

    :goto_6
    cmp-long v0, p1, v0

    goto/32 :goto_8

    nop

    :goto_7
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_e

    nop

    :goto_8
    if-ltz v0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_4

    nop

    :goto_9
    iget-wide v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mDuration:J

    goto/32 :goto_f

    nop

    :goto_a
    invoke-direct {p0, p1, p2}, Lcom/android/internal/app/procstats/AssociationState$SourceState;->stopTracking(J)V

    :goto_b
    goto/32 :goto_2

    nop

    :goto_c
    iget v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mNesting:I

    goto/32 :goto_7

    nop

    :goto_d
    sub-long v2, p1, v2

    goto/32 :goto_1

    nop

    :goto_e
    iput v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mNesting:I

    goto/32 :goto_3

    nop

    :goto_f
    iget-wide v2, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mStartUptime:J

    goto/32 :goto_d

    nop

    :goto_10
    const-wide/16 v0, 0x0

    goto/32 :goto_6

    nop
.end method

.method public stop()V
    .locals 3

    const-wide/16 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/internal/app/procstats/AssociationState$SourceState;->stop(J)J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/android/internal/app/procstats/AssociationState$SourceState;->getCommonSourceState(Z)Lcom/android/internal/app/procstats/AssociationState$SourceState;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2, v0, v1}, Lcom/android/internal/app/procstats/AssociationState$SourceState;->stop(J)J

    :cond_0
    return-void
.end method

.method stopActive(J)V
    .locals 12

    goto/32 :goto_21

    nop

    :goto_0
    move-wide v8, p1

    goto/32 :goto_4b

    nop

    :goto_1
    const/4 v5, 0x1

    goto/32 :goto_3c

    nop

    :goto_2
    if-nez v1, :cond_0

    goto/32 :goto_49

    :cond_0
    goto/32 :goto_31

    nop

    :goto_3
    cmp-long v1, v8, v3

    goto/32 :goto_25

    nop

    :goto_4
    iget-object v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    goto/32 :goto_32

    nop

    :goto_5
    invoke-static {v5}, Lcom/android/internal/app/procstats/AssociationState;->-$$Nest$fgetmTotalActiveStartUptime(Lcom/android/internal/app/procstats/AssociationState;)J

    move-result-wide v10

    goto/32 :goto_39

    nop

    :goto_6
    invoke-static {v5, v1}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :goto_7
    goto/32 :goto_24

    nop

    :goto_8
    if-nez v1, :cond_1

    goto/32 :goto_33

    :cond_1
    goto/32 :goto_10

    nop

    :goto_9
    cmp-long v1, v1, v3

    goto/32 :goto_e

    nop

    :goto_a
    const-wide/16 v3, 0x0

    goto/32 :goto_9

    nop

    :goto_b
    goto/16 :goto_35

    :goto_c
    goto/32 :goto_2d

    nop

    :goto_d
    move v0, v1

    goto/32 :goto_12

    nop

    :goto_e
    const/4 v2, 0x0

    goto/32 :goto_8

    nop

    :goto_f
    invoke-virtual {v1, v8, v6, v7}, Lcom/android/internal/app/procstats/DurationsTable;->addDuration(IJ)V

    goto/32 :goto_b

    nop

    :goto_10
    iget-boolean v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mInTrackingList:Z

    goto/32 :goto_43

    nop

    :goto_11
    iget-object v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    goto/32 :goto_2

    nop

    :goto_12
    iget-object v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDurations:Lcom/android/internal/app/procstats/DurationsTable;

    goto/32 :goto_3a

    nop

    :goto_13
    const-string/jumbo v5, "stopActive while not tracking: "

    goto/32 :goto_2e

    nop

    :goto_14
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_36

    nop

    :goto_15
    invoke-static {v1}, Lcom/android/internal/app/procstats/AssociationState;->-$$Nest$fgetmTotalActiveNesting(Lcom/android/internal/app/procstats/AssociationState;)I

    move-result v8

    goto/32 :goto_20

    nop

    :goto_16
    invoke-static {v1}, Lcom/android/internal/app/procstats/AssociationState;->-$$Nest$fgetmTotalActiveNesting(Lcom/android/internal/app/procstats/AssociationState;)I

    move-result v1

    goto/32 :goto_26

    nop

    :goto_17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_13

    nop

    :goto_18
    iget-object v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    goto/32 :goto_1c

    nop

    :goto_19
    move v1, v5

    goto/32 :goto_40

    nop

    :goto_1a
    iget-wide v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveStartUptime:J

    goto/32 :goto_a

    nop

    :goto_1b
    if-nez v0, :cond_2

    goto/32 :goto_49

    :cond_2
    goto/32 :goto_48

    nop

    :goto_1c
    invoke-static {v1}, Lcom/android/internal/app/procstats/AssociationState;->-$$Nest$fgetmTotalActiveDuration(Lcom/android/internal/app/procstats/AssociationState;)J

    move-result-wide v8

    goto/32 :goto_4e

    nop

    :goto_1d
    iget-wide v6, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveStartUptime:J

    goto/32 :goto_37

    nop

    :goto_1e
    if-nez v1, :cond_3

    goto/32 :goto_33

    :cond_3
    goto/32 :goto_15

    nop

    :goto_1f
    invoke-static {v1, v8, v9}, Lcom/android/internal/app/procstats/AssociationState;->-$$Nest$fputmTotalActiveDuration(Lcom/android/internal/app/procstats/AssociationState;J)V

    goto/32 :goto_4

    nop

    :goto_20
    sub-int/2addr v8, v5

    goto/32 :goto_27

    nop

    :goto_21
    const/4 v0, 0x0

    goto/32 :goto_1a

    nop

    :goto_22
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_14

    nop

    :goto_23
    iget-object v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    goto/32 :goto_1e

    nop

    :goto_24
    iget v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveNesting:I

    goto/32 :goto_1

    nop

    :goto_25
    if-eqz v1, :cond_4

    goto/32 :goto_41

    :cond_4
    goto/32 :goto_19

    nop

    :goto_26
    if-eqz v1, :cond_5

    goto/32 :goto_33

    :cond_5
    goto/32 :goto_18

    nop

    :goto_27
    invoke-static {v1, v8}, Lcom/android/internal/app/procstats/AssociationState;->-$$Nest$fputmTotalActiveNesting(Lcom/android/internal/app/procstats/AssociationState;I)V

    goto/32 :goto_38

    nop

    :goto_28
    iget v8, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveProcState:I

    goto/32 :goto_f

    nop

    :goto_29
    iget-object v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    goto/32 :goto_47

    nop

    :goto_2a
    move-wide v8, v3

    :goto_2b
    goto/32 :goto_42

    nop

    :goto_2c
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_17

    nop

    :goto_2d
    iget-wide v8, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDuration:J

    goto/32 :goto_46

    nop

    :goto_2e
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_22

    nop

    :goto_2f
    move v1, v2

    :goto_30
    goto/32 :goto_d

    nop

    :goto_31
    invoke-direct {p0, v2}, Lcom/android/internal/app/procstats/AssociationState$SourceState;->getCommonSourceState(Z)Lcom/android/internal/app/procstats/AssociationState$SourceState;

    move-result-object v1

    goto/32 :goto_4d

    nop

    :goto_32
    invoke-static {v1, v3, v4}, Lcom/android/internal/app/procstats/AssociationState;->-$$Nest$fputmTotalActiveStartUptime(Lcom/android/internal/app/procstats/AssociationState;J)V

    :goto_33
    goto/32 :goto_11

    nop

    :goto_34
    iput-wide v8, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDuration:J

    :goto_35
    goto/32 :goto_23

    nop

    :goto_36
    const-string v5, "ProcessStats"

    goto/32 :goto_6

    nop

    :goto_37
    sub-long v6, p1, v6

    goto/32 :goto_4f

    nop

    :goto_38
    iget-object v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    goto/32 :goto_16

    nop

    :goto_39
    sub-long v10, p1, v10

    goto/32 :goto_44

    nop

    :goto_3a
    if-nez v1, :cond_6

    goto/32 :goto_c

    :cond_6
    goto/32 :goto_28

    nop

    :goto_3b
    if-eqz v8, :cond_7

    goto/32 :goto_4c

    :cond_7
    goto/32 :goto_45

    nop

    :goto_3c
    sub-int/2addr v1, v5

    goto/32 :goto_4a

    nop

    :goto_3d
    goto :goto_4c

    :goto_3e
    goto/32 :goto_0

    nop

    :goto_3f
    return-void

    :goto_40
    goto :goto_30

    :goto_41
    goto/32 :goto_2f

    nop

    :goto_42
    iput-wide v8, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveStartUptime:J

    goto/32 :goto_3

    nop

    :goto_43
    if-eqz v1, :cond_8

    goto/32 :goto_7

    :cond_8
    goto/32 :goto_29

    nop

    :goto_44
    add-long/2addr v8, v10

    goto/32 :goto_1f

    nop

    :goto_45
    if-eqz v1, :cond_9

    goto/32 :goto_3e

    :cond_9
    goto/32 :goto_3d

    nop

    :goto_46
    add-long/2addr v8, v6

    goto/32 :goto_34

    nop

    :goto_47
    if-nez v1, :cond_a

    goto/32 :goto_7

    :cond_a
    goto/32 :goto_2c

    nop

    :goto_48
    invoke-virtual {v1, p1, p2}, Lcom/android/internal/app/procstats/AssociationState$SourceState;->stopActive(J)V

    :goto_49
    goto/32 :goto_3f

    nop

    :goto_4a
    iput v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveNesting:I

    goto/32 :goto_1d

    nop

    :goto_4b
    goto/16 :goto_2b

    :goto_4c
    goto/32 :goto_2a

    nop

    :goto_4d
    if-nez v1, :cond_b

    goto/32 :goto_49

    :cond_b
    goto/32 :goto_1b

    nop

    :goto_4e
    iget-object v5, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    goto/32 :goto_5

    nop

    :goto_4f
    iget-object v8, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    goto/32 :goto_3b

    nop
.end method

.method stopActiveIfNecessary(IJ)Z
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    return v0

    :goto_1
    invoke-virtual {p0, p2, p3}, Lcom/android/internal/app/procstats/AssociationState$SourceState;->stopActive(J)V

    goto/32 :goto_2

    nop

    :goto_2
    invoke-direct {p0}, Lcom/android/internal/app/procstats/AssociationState$SourceState;->stopTrackingProcState()V

    goto/32 :goto_7

    nop

    :goto_3
    iget v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mProcStateSeq:I

    goto/32 :goto_5

    nop

    :goto_4
    if-ge v0, v1, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_a

    nop

    :goto_5
    if-eq v0, p1, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_6

    nop

    :goto_6
    iget v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mProcState:I

    goto/32 :goto_9

    nop

    :goto_7
    const/4 v0, 0x1

    goto/32 :goto_0

    nop

    :goto_8
    const/4 v0, 0x0

    goto/32 :goto_c

    nop

    :goto_9
    const/16 v1, 0xb

    goto/32 :goto_4

    nop

    :goto_a
    goto :goto_d

    :goto_b
    goto/32 :goto_8

    nop

    :goto_c
    return v0

    :goto_d
    goto/32 :goto_1

    nop
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "SourceState{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mKey:Lcom/android/internal/app/procstats/AssociationState$SourceKey;

    iget-object v3, v3, Lcom/android/internal/app/procstats/AssociationState$SourceKey;->mProcess:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mKey:Lcom/android/internal/app/procstats/AssociationState$SourceKey;

    iget v3, v3, Lcom/android/internal/app/procstats/AssociationState$SourceKey;->mUid:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mProcState:I

    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/android/internal/app/procstats/DumpUtils;->STATE_NAMES:[Ljava/lang/String;

    iget v3, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mProcState:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " #"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mProcStateSeq:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public trackProcState(IIJ)V
    .locals 3

    move v0, p1

    sget-object v1, Lcom/android/internal/app/procstats/ProcessState;->PROCESS_STATE_TO_STATE:[I

    aget p1, v1, p1

    iget v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mProcStateSeq:I

    if-eq p2, v1, :cond_0

    iput p2, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mProcStateSeq:I

    iput p1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mProcState:I

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mProcState:I

    if-ge p1, v1, :cond_1

    iput p1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mProcState:I

    :cond_1
    :goto_0
    const/16 v1, 0xb

    const/4 v2, 0x1

    if-ge p1, v1, :cond_2

    iget-boolean v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mInTrackingList:Z

    if-nez v1, :cond_2

    iput-boolean v2, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mInTrackingList:Z

    iput-wide p3, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mTrackingUptime:J

    iget-object v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mProcessStats:Lcom/android/internal/app/procstats/ProcessStats;

    iget-object v1, v1, Lcom/android/internal/app/procstats/ProcessStats;->mTrackingAssociations:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v1, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mAssociationState:Lcom/android/internal/app/procstats/AssociationState;

    if-eqz v1, :cond_3

    invoke-direct {p0, v2}, Lcom/android/internal/app/procstats/AssociationState$SourceState;->getCommonSourceState(Z)Lcom/android/internal/app/procstats/AssociationState$SourceState;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1, v0, p2, p3, p4}, Lcom/android/internal/app/procstats/AssociationState$SourceState;->trackProcState(IIJ)V

    :cond_3
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-wide v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mDuration:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDurations:Lcom/android/internal/app/procstats/DurationsTable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDurations:Lcom/android/internal/app/procstats/DurationsTable;

    invoke-virtual {v0, p1}, Lcom/android/internal/app/procstats/DurationsTable;->writeToParcel(Landroid/os/Parcel;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveProcState:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-wide v0, p0, Lcom/android/internal/app/procstats/AssociationState$SourceState;->mActiveDuration:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    :goto_0
    return-void
.end method
