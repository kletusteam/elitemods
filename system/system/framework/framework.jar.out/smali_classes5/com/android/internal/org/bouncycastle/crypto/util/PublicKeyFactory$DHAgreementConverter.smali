.class Lcom/android/internal/org/bouncycastle/crypto/util/PublicKeyFactory$DHAgreementConverter;
.super Lcom/android/internal/org/bouncycastle/crypto/util/PublicKeyFactory$SubjectPublicKeyInfoConverter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/org/bouncycastle/crypto/util/PublicKeyFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DHAgreementConverter"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/internal/org/bouncycastle/crypto/util/PublicKeyFactory$SubjectPublicKeyInfoConverter;-><init>(Lcom/android/internal/org/bouncycastle/crypto/util/PublicKeyFactory$SubjectPublicKeyInfoConverter-IA;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/org/bouncycastle/crypto/util/PublicKeyFactory$DHAgreementConverter-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/internal/org/bouncycastle/crypto/util/PublicKeyFactory$DHAgreementConverter;-><init>()V

    return-void
.end method


# virtual methods
.method getPublicKeyParameters(Lcom/android/internal/org/bouncycastle/asn1/x509/SubjectPublicKeyInfo;Ljava/lang/Object;)Lcom/android/internal/org/bouncycastle/crypto/params/AsymmetricKeyParameter;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    return-object v5

    :goto_1
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/pkcs/DHParameter;->getP()Ljava/math/BigInteger;

    move-result-object v5

    goto/32 :goto_7

    nop

    :goto_2
    invoke-virtual {p1}, Lcom/android/internal/org/bouncycastle/asn1/x509/SubjectPublicKeyInfo;->getAlgorithm()Lcom/android/internal/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_3
    if-eqz v2, :cond_0

    goto/32 :goto_11

    :cond_0
    goto/32 :goto_b

    nop

    :goto_4
    invoke-virtual {v2}, Ljava/math/BigInteger;->intValue()I

    move-result v3

    :goto_5
    goto/32 :goto_f

    nop

    :goto_6
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getParameters()Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v0

    goto/32 :goto_d

    nop

    :goto_7
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/pkcs/DHParameter;->getG()Ljava/math/BigInteger;

    move-result-object v6

    goto/32 :goto_13

    nop

    :goto_8
    invoke-virtual {v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Integer;->getValue()Ljava/math/BigInteger;

    move-result-object v6

    goto/32 :goto_12

    nop

    :goto_9
    check-cast v1, Lcom/android/internal/org/bouncycastle/asn1/ASN1Integer;

    goto/32 :goto_a

    nop

    :goto_a
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/pkcs/DHParameter;->getL()Ljava/math/BigInteger;

    move-result-object v2

    goto/32 :goto_3

    nop

    :goto_b
    const/4 v3, 0x0

    goto/32 :goto_10

    nop

    :goto_c
    invoke-virtual {p1}, Lcom/android/internal/org/bouncycastle/asn1/x509/SubjectPublicKeyInfo;->parsePublicKey()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v1

    goto/32 :goto_9

    nop

    :goto_d
    invoke-static {v0}, Lcom/android/internal/org/bouncycastle/asn1/pkcs/DHParameter;->getInstance(Ljava/lang/Object;)Lcom/android/internal/org/bouncycastle/asn1/pkcs/DHParameter;

    move-result-object v0

    goto/32 :goto_c

    nop

    :goto_e
    invoke-direct {v4, v5, v6, v7, v3}, Lcom/android/internal/org/bouncycastle/crypto/params/DHParameters;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;I)V

    goto/32 :goto_14

    nop

    :goto_f
    new-instance v4, Lcom/android/internal/org/bouncycastle/crypto/params/DHParameters;

    goto/32 :goto_1

    nop

    :goto_10
    goto :goto_5

    :goto_11
    goto/32 :goto_4

    nop

    :goto_12
    invoke-direct {v5, v6, v4}, Lcom/android/internal/org/bouncycastle/crypto/params/DHPublicKeyParameters;-><init>(Ljava/math/BigInteger;Lcom/android/internal/org/bouncycastle/crypto/params/DHParameters;)V

    goto/32 :goto_0

    nop

    :goto_13
    const/4 v7, 0x0

    goto/32 :goto_e

    nop

    :goto_14
    new-instance v5, Lcom/android/internal/org/bouncycastle/crypto/params/DHPublicKeyParameters;

    goto/32 :goto_8

    nop
.end method
