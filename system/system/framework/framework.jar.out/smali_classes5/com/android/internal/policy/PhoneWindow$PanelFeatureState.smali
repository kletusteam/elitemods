.class final Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/PhoneWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "PanelFeatureState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/PhoneWindow$PanelFeatureState$SavedState;
    }
.end annotation


# instance fields
.field background:I

.field createdPanelView:Landroid/view/View;

.field decorView:Lcom/android/internal/policy/DecorView;

.field featureId:I

.field frozenActionViewState:Landroid/os/Bundle;

.field frozenMenuState:Landroid/os/Bundle;

.field fullBackground:I

.field gravity:I

.field iconMenuPresenter:Lcom/android/internal/view/menu/IconMenuPresenter;

.field isCompact:Z

.field isHandled:Z

.field isInExpandedMode:Z

.field isOpen:Z

.field isPrepared:Z

.field listMenuPresenter:Lcom/android/internal/view/menu/ListMenuPresenter;

.field listPresenterTheme:I

.field menu:Lcom/android/internal/view/menu/MenuBuilder;

.field public qwertyMode:Z

.field refreshDecorView:Z

.field refreshMenuContent:Z

.field shownPanelView:Landroid/view/View;

.field wasLastExpanded:Z

.field wasLastOpen:Z

.field windowAnimations:I

.field x:I

.field y:I


# direct methods
.method constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->featureId:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->refreshDecorView:Z

    return-void
.end method


# virtual methods
.method applyFrozenState()V
    .locals 2

    goto/32 :goto_6

    nop

    :goto_0
    return-void

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_5

    nop

    :goto_2
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/MenuBuilder;->restorePresenterStates(Landroid/os/Bundle;)V

    goto/32 :goto_3

    nop

    :goto_3
    const/4 v0, 0x0

    goto/32 :goto_7

    nop

    :goto_4
    if-nez v1, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_2

    nop

    :goto_5
    iget-object v1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->frozenMenuState:Landroid/os/Bundle;

    goto/32 :goto_4

    nop

    :goto_6
    iget-object v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    goto/32 :goto_1

    nop

    :goto_7
    iput-object v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->frozenMenuState:Landroid/os/Bundle;

    :goto_8
    goto/32 :goto_0

    nop
.end method

.method public clearMenuPresenters()V
    .locals 2

    iget-object v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->iconMenuPresenter:Lcom/android/internal/view/menu/IconMenuPresenter;

    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/MenuBuilder;->removeMenuPresenter(Lcom/android/internal/view/menu/MenuPresenter;)V

    iget-object v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    iget-object v1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->listMenuPresenter:Lcom/android/internal/view/menu/ListMenuPresenter;

    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/MenuBuilder;->removeMenuPresenter(Lcom/android/internal/view/menu/MenuPresenter;)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->iconMenuPresenter:Lcom/android/internal/view/menu/IconMenuPresenter;

    iput-object v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->listMenuPresenter:Lcom/android/internal/view/menu/ListMenuPresenter;

    return-void
.end method

.method getIconMenuView(Landroid/content/Context;Lcom/android/internal/view/menu/MenuPresenter$Callback;)Lcom/android/internal/view/menu/MenuView;
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    new-instance v0, Lcom/android/internal/view/menu/IconMenuPresenter;

    goto/32 :goto_10

    nop

    :goto_1
    iget-object v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->iconMenuPresenter:Lcom/android/internal/view/menu/IconMenuPresenter;

    goto/32 :goto_6

    nop

    :goto_2
    iput-object v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->iconMenuPresenter:Lcom/android/internal/view/menu/IconMenuPresenter;

    goto/32 :goto_a

    nop

    :goto_3
    const v1, 0x102032c

    goto/32 :goto_14

    nop

    :goto_4
    return-object v0

    :goto_5
    iget-object v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    goto/32 :goto_c

    nop

    :goto_6
    if-eqz v0, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_0

    nop

    :goto_7
    iget-object v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    goto/32 :goto_9

    nop

    :goto_8
    iget-object v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->iconMenuPresenter:Lcom/android/internal/view/menu/IconMenuPresenter;

    goto/32 :goto_3

    nop

    :goto_9
    iget-object v1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->iconMenuPresenter:Lcom/android/internal/view/menu/IconMenuPresenter;

    goto/32 :goto_d

    nop

    :goto_a
    invoke-virtual {v0, p2}, Lcom/android/internal/view/menu/IconMenuPresenter;->setCallback(Lcom/android/internal/view/menu/MenuPresenter$Callback;)V

    goto/32 :goto_8

    nop

    :goto_b
    iget-object v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->iconMenuPresenter:Lcom/android/internal/view/menu/IconMenuPresenter;

    goto/32 :goto_11

    nop

    :goto_c
    if-eqz v0, :cond_1

    goto/32 :goto_13

    :cond_1
    goto/32 :goto_15

    nop

    :goto_d
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/MenuBuilder;->addMenuPresenter(Lcom/android/internal/view/menu/MenuPresenter;)V

    :goto_e
    goto/32 :goto_b

    nop

    :goto_f
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/IconMenuPresenter;->getMenuView(Landroid/view/ViewGroup;)Lcom/android/internal/view/menu/MenuView;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_10
    invoke-direct {v0, p1}, Lcom/android/internal/view/menu/IconMenuPresenter;-><init>(Landroid/content/Context;)V

    goto/32 :goto_2

    nop

    :goto_11
    iget-object v1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->decorView:Lcom/android/internal/policy/DecorView;

    goto/32 :goto_f

    nop

    :goto_12
    return-object v0

    :goto_13
    goto/32 :goto_1

    nop

    :goto_14
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/IconMenuPresenter;->setId(I)V

    goto/32 :goto_7

    nop

    :goto_15
    const/4 v0, 0x0

    goto/32 :goto_12

    nop
.end method

.method getListMenuView(Landroid/content/Context;Lcom/android/internal/view/menu/MenuPresenter$Callback;)Lcom/android/internal/view/menu/MenuView;
    .locals 3

    goto/32 :goto_b

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->listMenuPresenter:Lcom/android/internal/view/menu/ListMenuPresenter;

    goto/32 :goto_2

    nop

    :goto_1
    iget-boolean v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->isCompact:Z

    goto/32 :goto_a

    nop

    :goto_2
    if-eqz v0, :cond_0

    goto/32 :goto_1d

    :cond_0
    goto/32 :goto_20

    nop

    :goto_3
    return-object v0

    :goto_4
    invoke-virtual {v1, v0}, Lcom/android/internal/view/menu/ListMenuPresenter;->setItemIndexOffset(I)V

    :goto_5
    goto/32 :goto_11

    nop

    :goto_6
    return-object v0

    :goto_7
    goto/32 :goto_1

    nop

    :goto_8
    invoke-virtual {p0, p1, p2}, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->getIconMenuView(Landroid/content/Context;Lcom/android/internal/view/menu/MenuPresenter$Callback;)Lcom/android/internal/view/menu/MenuView;

    :goto_9
    goto/32 :goto_0

    nop

    :goto_a
    if-eqz v0, :cond_1

    goto/32 :goto_9

    :cond_1
    goto/32 :goto_8

    nop

    :goto_b
    iget-object v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    goto/32 :goto_21

    nop

    :goto_c
    iget-object v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    goto/32 :goto_17

    nop

    :goto_d
    iput-object v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->listMenuPresenter:Lcom/android/internal/view/menu/ListMenuPresenter;

    goto/32 :goto_19

    nop

    :goto_e
    invoke-virtual {v0}, Lcom/android/internal/view/menu/IconMenuPresenter;->getNumActualItemsShown()I

    move-result v0

    goto/32 :goto_4

    nop

    :goto_f
    const v1, 0x1020388

    goto/32 :goto_1a

    nop

    :goto_10
    iget-object v1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->decorView:Lcom/android/internal/policy/DecorView;

    goto/32 :goto_15

    nop

    :goto_11
    iget-object v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->listMenuPresenter:Lcom/android/internal/view/menu/ListMenuPresenter;

    goto/32 :goto_10

    nop

    :goto_12
    const/4 v0, 0x0

    goto/32 :goto_6

    nop

    :goto_13
    iget-object v1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->listMenuPresenter:Lcom/android/internal/view/menu/ListMenuPresenter;

    goto/32 :goto_e

    nop

    :goto_14
    invoke-direct {v0, v1, v2}, Lcom/android/internal/view/menu/ListMenuPresenter;-><init>(II)V

    goto/32 :goto_d

    nop

    :goto_15
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/ListMenuPresenter;->getMenuView(Landroid/view/ViewGroup;)Lcom/android/internal/view/menu/MenuView;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_16
    if-nez v0, :cond_2

    goto/32 :goto_5

    :cond_2
    goto/32 :goto_13

    nop

    :goto_17
    iget-object v1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->listMenuPresenter:Lcom/android/internal/view/menu/ListMenuPresenter;

    goto/32 :goto_1c

    nop

    :goto_18
    iget-object v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->iconMenuPresenter:Lcom/android/internal/view/menu/IconMenuPresenter;

    goto/32 :goto_16

    nop

    :goto_19
    invoke-virtual {v0, p2}, Lcom/android/internal/view/menu/ListMenuPresenter;->setCallback(Lcom/android/internal/view/menu/MenuPresenter$Callback;)V

    goto/32 :goto_1f

    nop

    :goto_1a
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/ListMenuPresenter;->setId(I)V

    goto/32 :goto_c

    nop

    :goto_1b
    const v1, 0x10900a2

    goto/32 :goto_1e

    nop

    :goto_1c
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/MenuBuilder;->addMenuPresenter(Lcom/android/internal/view/menu/MenuPresenter;)V

    :goto_1d
    goto/32 :goto_18

    nop

    :goto_1e
    iget v2, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->listPresenterTheme:I

    goto/32 :goto_14

    nop

    :goto_1f
    iget-object v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->listMenuPresenter:Lcom/android/internal/view/menu/ListMenuPresenter;

    goto/32 :goto_f

    nop

    :goto_20
    new-instance v0, Lcom/android/internal/view/menu/ListMenuPresenter;

    goto/32 :goto_1b

    nop

    :goto_21
    if-eqz v0, :cond_3

    goto/32 :goto_7

    :cond_3
    goto/32 :goto_12

    nop
.end method

.method public hasPanelItems()Z
    .locals 4

    iget-object v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->shownPanelView:Landroid/view/View;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v2, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->createdPanelView:Landroid/view/View;

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    return v3

    :cond_1
    iget-boolean v2, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->isCompact:Z

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->isInExpandedMode:Z

    if-eqz v2, :cond_2

    goto :goto_0

    :cond_2
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_3

    move v1, v3

    :cond_3
    return v1

    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->listMenuPresenter:Lcom/android/internal/view/menu/ListMenuPresenter;

    invoke-virtual {v0}, Lcom/android/internal/view/menu/ListMenuPresenter;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_5

    move v1, v3

    :cond_5
    return v1
.end method

.method public isInListMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->isInExpandedMode:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->isCompact:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    goto/32 :goto_e

    nop

    :goto_0
    iget-boolean v1, v0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState$SavedState;->isInExpandedMode:Z

    goto/32 :goto_1

    nop

    :goto_1
    iput-boolean v1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->wasLastExpanded:Z

    goto/32 :goto_7

    nop

    :goto_2
    iput-object v1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->frozenMenuState:Landroid/os/Bundle;

    goto/32 :goto_a

    nop

    :goto_3
    iget v1, v0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState$SavedState;->featureId:I

    goto/32 :goto_8

    nop

    :goto_4
    return-void

    :goto_5
    iput-object v1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->shownPanelView:Landroid/view/View;

    goto/32 :goto_b

    nop

    :goto_6
    iput-object v1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->createdPanelView:Landroid/view/View;

    goto/32 :goto_5

    nop

    :goto_7
    iget-object v1, v0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState$SavedState;->menuState:Landroid/os/Bundle;

    goto/32 :goto_2

    nop

    :goto_8
    iput v1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->featureId:I

    goto/32 :goto_c

    nop

    :goto_9
    iput-boolean v1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->wasLastOpen:Z

    goto/32 :goto_0

    nop

    :goto_a
    const/4 v1, 0x0

    goto/32 :goto_6

    nop

    :goto_b
    iput-object v1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->decorView:Lcom/android/internal/policy/DecorView;

    goto/32 :goto_4

    nop

    :goto_c
    iget-boolean v1, v0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState$SavedState;->isOpen:Z

    goto/32 :goto_9

    nop

    :goto_d
    check-cast v0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState$SavedState;

    goto/32 :goto_3

    nop

    :goto_e
    move-object v0, p1

    goto/32 :goto_d

    nop
.end method

.method onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    goto/32 :goto_d

    nop

    :goto_0
    invoke-virtual {v1, v2}, Lcom/android/internal/view/menu/MenuBuilder;->savePresenterStates(Landroid/os/Bundle;)V

    :goto_1
    goto/32 :goto_9

    nop

    :goto_2
    iget-boolean v1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->isOpen:Z

    goto/32 :goto_c

    nop

    :goto_3
    iput-boolean v1, v0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState$SavedState;->isInExpandedMode:Z

    goto/32 :goto_8

    nop

    :goto_4
    iget-object v1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    goto/32 :goto_12

    nop

    :goto_5
    invoke-direct {v0, v1}, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState$SavedState;-><init>(Lcom/android/internal/policy/PhoneWindow$PanelFeatureState$SavedState-IA;)V

    goto/32 :goto_10

    nop

    :goto_6
    iput v1, v0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState$SavedState;->featureId:I

    goto/32 :goto_2

    nop

    :goto_7
    const/4 v1, 0x0

    goto/32 :goto_5

    nop

    :goto_8
    iget-object v1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    goto/32 :goto_e

    nop

    :goto_9
    return-object v0

    :goto_a
    new-instance v1, Landroid/os/Bundle;

    goto/32 :goto_11

    nop

    :goto_b
    iput-object v1, v0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState$SavedState;->menuState:Landroid/os/Bundle;

    goto/32 :goto_4

    nop

    :goto_c
    iput-boolean v1, v0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState$SavedState;->isOpen:Z

    goto/32 :goto_f

    nop

    :goto_d
    new-instance v0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState$SavedState;

    goto/32 :goto_7

    nop

    :goto_e
    if-nez v1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_a

    nop

    :goto_f
    iget-boolean v1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->isInExpandedMode:Z

    goto/32 :goto_3

    nop

    :goto_10
    iget v1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->featureId:I

    goto/32 :goto_6

    nop

    :goto_11
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    goto/32 :goto_b

    nop

    :goto_12
    iget-object v2, v0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState$SavedState;->menuState:Landroid/os/Bundle;

    goto/32 :goto_0

    nop
.end method

.method setMenu(Lcom/android/internal/view/menu/MenuBuilder;)V
    .locals 2

    goto/32 :goto_10

    nop

    :goto_0
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/MenuBuilder;->removeMenuPresenter(Lcom/android/internal/view/menu/MenuPresenter;)V

    goto/32 :goto_1

    nop

    :goto_1
    iget-object v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    goto/32 :goto_8

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_c

    nop

    :goto_3
    if-nez v0, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_a

    nop

    :goto_4
    if-eq p1, v0, :cond_2

    goto/32 :goto_7

    :cond_2
    goto/32 :goto_6

    nop

    :goto_5
    if-nez p1, :cond_3

    goto/32 :goto_d

    :cond_3
    goto/32 :goto_f

    nop

    :goto_6
    return-void

    :goto_7
    goto/32 :goto_e

    nop

    :goto_8
    iget-object v1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->listMenuPresenter:Lcom/android/internal/view/menu/ListMenuPresenter;

    goto/32 :goto_14

    nop

    :goto_9
    iget-object v1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->iconMenuPresenter:Lcom/android/internal/view/menu/IconMenuPresenter;

    goto/32 :goto_0

    nop

    :goto_a
    invoke-virtual {p1, v0}, Lcom/android/internal/view/menu/MenuBuilder;->addMenuPresenter(Lcom/android/internal/view/menu/MenuPresenter;)V

    :goto_b
    goto/32 :goto_11

    nop

    :goto_c
    invoke-virtual {p1, v0}, Lcom/android/internal/view/menu/MenuBuilder;->addMenuPresenter(Lcom/android/internal/view/menu/MenuPresenter;)V

    :goto_d
    goto/32 :goto_12

    nop

    :goto_e
    if-nez v0, :cond_4

    goto/32 :goto_15

    :cond_4
    goto/32 :goto_9

    nop

    :goto_f
    iget-object v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->iconMenuPresenter:Lcom/android/internal/view/menu/IconMenuPresenter;

    goto/32 :goto_3

    nop

    :goto_10
    iget-object v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    goto/32 :goto_4

    nop

    :goto_11
    iget-object v0, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->listMenuPresenter:Lcom/android/internal/view/menu/ListMenuPresenter;

    goto/32 :goto_2

    nop

    :goto_12
    return-void

    :goto_13
    iput-object p1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->menu:Lcom/android/internal/view/menu/MenuBuilder;

    goto/32 :goto_5

    nop

    :goto_14
    invoke-virtual {v0, v1}, Lcom/android/internal/view/menu/MenuBuilder;->removeMenuPresenter(Lcom/android/internal/view/menu/MenuPresenter;)V

    :goto_15
    goto/32 :goto_13

    nop
.end method

.method setStyle(Landroid/content/Context;)V
    .locals 3

    goto/32 :goto_0

    nop

    :goto_0
    sget-object v0, Lcom/android/internal/R$styleable;->Theme:[I

    goto/32 :goto_11

    nop

    :goto_1
    const/16 v1, 0x149

    goto/32 :goto_3

    nop

    :goto_2
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    goto/32 :goto_6

    nop

    :goto_3
    const v2, 0x1030418

    goto/32 :goto_2

    nop

    :goto_4
    iput-boolean v1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->isCompact:Z

    goto/32 :goto_1

    nop

    :goto_5
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    goto/32 :goto_4

    nop

    :goto_6
    iput v1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->listPresenterTheme:I

    goto/32 :goto_10

    nop

    :goto_7
    return-void

    :goto_8
    iput v1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->windowAnimations:I

    goto/32 :goto_14

    nop

    :goto_9
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    goto/32 :goto_e

    nop

    :goto_a
    iput v1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->fullBackground:I

    goto/32 :goto_b

    nop

    :goto_b
    const/16 v1, 0x5d

    goto/32 :goto_f

    nop

    :goto_c
    const/16 v1, 0x2e

    goto/32 :goto_12

    nop

    :goto_d
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    goto/32 :goto_a

    nop

    :goto_e
    iput v1, p0, Lcom/android/internal/policy/PhoneWindow$PanelFeatureState;->background:I

    goto/32 :goto_13

    nop

    :goto_f
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    goto/32 :goto_8

    nop

    :goto_10
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto/32 :goto_7

    nop

    :goto_11
    invoke-virtual {p1, v0}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    goto/32 :goto_c

    nop

    :goto_12
    const/4 v2, 0x0

    goto/32 :goto_9

    nop

    :goto_13
    const/16 v1, 0x2f

    goto/32 :goto_d

    nop

    :goto_14
    const/16 v1, 0x148

    goto/32 :goto_5

    nop
.end method
