.class public Lcom/android/internal/app/ChooserActivityStub;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/android/internal/app/ChooserActivityStub;
    .locals 1

    const-class v0, Lcom/android/internal/app/ChooserActivityStub;

    invoke-static {v0}, Lcom/miui/base/MiuiStubUtil;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/app/ChooserActivityStub;

    return-object v0
.end method

.method public static newInstance()Lcom/android/internal/app/ChooserActivityStub;
    .locals 1

    const-class v0, Lcom/android/internal/app/ChooserActivityStub;

    invoke-static {v0}, Lcom/miui/base/MiuiStubUtil;->newInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/app/ChooserActivityStub;

    return-object v0
.end method


# virtual methods
.method public canBindService(Landroid/content/Context;Landroid/content/Intent;I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public canInterceptByMiAppStore(Landroid/app/Activity;Landroid/content/Intent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dealWithActonViewIntent(Landroid/content/Intent;[Landroid/content/Intent;)V
    .locals 0

    return-void
.end method

.method public isInterceptedByMiAppStore(Landroid/app/Activity;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public notAllowHomeLaunchedAgain(Landroid/content/Intent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public startInterceptByMiAppStore(Landroid/app/Activity;Landroid/content/Intent;Ljava/lang/Object;Lcom/android/internal/app/ResolverRankerServiceResolverComparator;I)V
    .locals 0

    return-void
.end method

.method public stopInterceptByMiAppStore(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method
