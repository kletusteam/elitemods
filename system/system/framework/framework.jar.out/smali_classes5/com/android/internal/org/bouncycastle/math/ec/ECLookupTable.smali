.class public interface abstract Lcom/android/internal/org/bouncycastle/math/ec/ECLookupTable;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getSize()I
.end method

.method public abstract lookup(I)Lcom/android/internal/org/bouncycastle/math/ec/ECPoint;
.end method

.method public abstract lookupVar(I)Lcom/android/internal/org/bouncycastle/math/ec/ECPoint;
.end method
