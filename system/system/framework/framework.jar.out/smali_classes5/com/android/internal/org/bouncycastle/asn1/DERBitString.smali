.class public Lcom/android/internal/org/bouncycastle/asn1/DERBitString;
.super Lcom/android/internal/org/bouncycastle/asn1/ASN1BitString;


# direct methods
.method protected constructor <init>(BI)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1BitString;-><init>(BI)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    invoke-static {p1}, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;->getBytes(I)[B

    move-result-object v0

    invoke-static {p1}, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;->getPadBits(I)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1BitString;-><init>([BI)V

    return-void
.end method

.method public constructor <init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface {p1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    const-string v1, "DER"

    invoke-virtual {v0, v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->getEncoded(Ljava/lang/String;)[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1BitString;-><init>([BI)V

    return-void
.end method

.method public constructor <init>([B)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;-><init>([BI)V

    return-void
.end method

.method public constructor <init>([BI)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1BitString;-><init>([BI)V

    return-void
.end method

.method static fromOctetString([B)Lcom/android/internal/org/bouncycastle/asn1/DERBitString;
    .locals 5

    array-length v0, p0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_1

    const/4 v0, 0x0

    aget-byte v2, p0, v0

    array-length v3, p0

    sub-int/2addr v3, v1

    new-array v3, v3, [B

    array-length v4, v3

    if-eqz v4, :cond_0

    array-length v4, p0

    sub-int/2addr v4, v1

    invoke-static {p0, v1, v3, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_0
    new-instance v0, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;

    invoke-direct {v0, v3, v2}, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;-><init>([BI)V

    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "truncated BIT STRING detected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static getInstance(Lcom/android/internal/org/bouncycastle/asn1/ASN1TaggedObject;Z)Lcom/android/internal/org/bouncycastle/asn1/DERBitString;
    .locals 2

    invoke-virtual {p0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1TaggedObject;->getObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    if-nez p1, :cond_1

    instance-of v1, v0, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OctetString;->getInstance(Ljava/lang/Object;)Lcom/android/internal/org/bouncycastle/asn1/ASN1OctetString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OctetString;->getOctets()[B

    move-result-object v1

    invoke-static {v1}, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;->fromOctetString([B)Lcom/android/internal/org/bouncycastle/asn1/DERBitString;

    move-result-object v1

    return-object v1

    :cond_1
    :goto_0
    invoke-static {v0}, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;->getInstance(Ljava/lang/Object;)Lcom/android/internal/org/bouncycastle/asn1/DERBitString;

    move-result-object v1

    return-object v1
.end method

.method public static getInstance(Ljava/lang/Object;)Lcom/android/internal/org/bouncycastle/asn1/DERBitString;
    .locals 4

    if-eqz p0, :cond_3

    instance-of v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    instance-of v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DLBitString;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;

    move-object v1, p0

    check-cast v1, Lcom/android/internal/org/bouncycastle/asn1/DLBitString;

    iget-object v1, v1, Lcom/android/internal/org/bouncycastle/asn1/DLBitString;->data:[B

    move-object v2, p0

    check-cast v2, Lcom/android/internal/org/bouncycastle/asn1/DLBitString;

    iget v2, v2, Lcom/android/internal/org/bouncycastle/asn1/DLBitString;->padBits:I

    invoke-direct {v0, v1, v2}, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;-><init>([BI)V

    return-object v0

    :cond_1
    instance-of v0, p0, [B

    if-eqz v0, :cond_2

    :try_start_0
    move-object v0, p0

    check-cast v0, [B

    invoke-static {v0}, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;->fromByteArray([B)Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    check-cast v0, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "encoding error in getInstance: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "illegal object in getInstance: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    :goto_0
    move-object v0, p0

    check-cast v0, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;

    return-object v0
.end method


# virtual methods
.method encode(Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_21

    nop

    :goto_0
    const/16 v4, 0xff

    goto/32 :goto_14

    nop

    :goto_1
    add-int/lit8 v2, v0, -0x1

    goto/32 :goto_11

    nop

    :goto_2
    int-to-byte v2, v2

    goto/32 :goto_20

    nop

    :goto_3
    and-int/2addr v1, v2

    goto/32 :goto_2a

    nop

    :goto_4
    const/4 v7, 0x0

    goto/32 :goto_25

    nop

    :goto_5
    aget-byte v1, v1, v2

    goto/32 :goto_26

    nop

    :goto_6
    invoke-virtual {p1, p2, v1, v2, v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeEncoded(ZIB[B)V

    :goto_7
    goto/32 :goto_29

    nop

    :goto_8
    int-to-byte v5, v2

    goto/32 :goto_b

    nop

    :goto_9
    iget v3, p0, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;->padBits:I

    goto/32 :goto_0

    nop

    :goto_a
    iget-object v1, p0, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;->data:[B

    goto/32 :goto_1

    nop

    :goto_b
    iget-object v6, p0, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;->data:[B

    goto/32 :goto_4

    nop

    :goto_c
    iget v2, p0, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;->padBits:I

    goto/32 :goto_2

    nop

    :goto_d
    if-nez v1, :cond_0

    goto/32 :goto_16

    :cond_0
    goto/32 :goto_1e

    nop

    :goto_e
    move v9, v1

    goto/32 :goto_2b

    nop

    :goto_f
    move v3, p2

    goto/32 :goto_e

    nop

    :goto_10
    if-eq v1, v2, :cond_1

    goto/32 :goto_28

    :cond_1
    goto/32 :goto_27

    nop

    :goto_11
    aget-byte v1, v1, v2

    goto/32 :goto_12

    nop

    :goto_12
    iget v2, p0, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;->padBits:I

    goto/32 :goto_1b

    nop

    :goto_13
    add-int/lit8 v3, v0, -0x1

    goto/32 :goto_1f

    nop

    :goto_14
    shl-int v3, v4, v3

    goto/32 :goto_1a

    nop

    :goto_15
    goto :goto_7

    :goto_16
    goto/32 :goto_19

    nop

    :goto_17
    int-to-byte v2, v2

    goto/32 :goto_10

    nop

    :goto_18
    iget v2, p0, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;->padBits:I

    goto/32 :goto_8

    nop

    :goto_19
    const/4 v1, 0x3

    goto/32 :goto_c

    nop

    :goto_1a
    and-int/2addr v2, v3

    goto/32 :goto_17

    nop

    :goto_1b
    shl-int v2, v4, v2

    goto/32 :goto_3

    nop

    :goto_1c
    move-object v2, p1

    goto/32 :goto_f

    nop

    :goto_1d
    const/4 v4, 0x3

    goto/32 :goto_18

    nop

    :goto_1e
    iget-object v1, p0, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;->data:[B

    goto/32 :goto_2c

    nop

    :goto_1f
    aget-byte v2, v2, v3

    goto/32 :goto_9

    nop

    :goto_20
    iget-object v3, p0, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;->data:[B

    goto/32 :goto_6

    nop

    :goto_21
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;->data:[B

    goto/32 :goto_22

    nop

    :goto_22
    array-length v0, v0

    goto/32 :goto_23

    nop

    :goto_23
    if-nez v0, :cond_2

    goto/32 :goto_16

    :cond_2
    goto/32 :goto_24

    nop

    :goto_24
    iget v1, p0, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;->padBits:I

    goto/32 :goto_d

    nop

    :goto_25
    add-int/lit8 v8, v0, -0x1

    goto/32 :goto_1c

    nop

    :goto_26
    iget-object v2, p0, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;->data:[B

    goto/32 :goto_13

    nop

    :goto_27
    goto :goto_16

    :goto_28
    goto/32 :goto_a

    nop

    :goto_29
    return-void

    :goto_2a
    int-to-byte v1, v1

    goto/32 :goto_1d

    nop

    :goto_2b
    invoke-virtual/range {v2 .. v9}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeEncoded(ZIB[BIIB)V

    goto/32 :goto_15

    nop

    :goto_2c
    add-int/lit8 v2, v0, -0x1

    goto/32 :goto_5

    nop
.end method

.method encodedLength()I
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    invoke-static {v0}, Lcom/android/internal/org/bouncycastle/asn1/StreamUtil;->calculateBodyLength(I)I

    move-result v0

    goto/32 :goto_6

    nop

    :goto_1
    array-length v0, v0

    goto/32 :goto_8

    nop

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_3

    nop

    :goto_3
    return v0

    :goto_4
    array-length v1, v1

    goto/32 :goto_9

    nop

    :goto_5
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;->data:[B

    goto/32 :goto_1

    nop

    :goto_6
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_7

    nop

    :goto_7
    iget-object v1, p0, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;->data:[B

    goto/32 :goto_4

    nop

    :goto_8
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_0

    nop

    :goto_9
    add-int/2addr v0, v1

    goto/32 :goto_2

    nop
.end method

.method isConstructed()Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method toDERObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    return-object p0
.end method

.method toDLObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    return-object p0
.end method
