.class public Lcom/android/internal/org/bouncycastle/asn1/BERSequence;
.super Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;-><init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    return-void
.end method

.method public constructor <init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;-><init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;)V

    return-void
.end method

.method public constructor <init>([Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;-><init>([Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    return-void
.end method


# virtual methods
.method encode(Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p1, p2, v1, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeEncodedIndef(ZI[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/BERSequence;->elements:[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_3

    nop

    :goto_2
    return-void

    :goto_3
    const/16 v1, 0x30

    goto/32 :goto_0

    nop
.end method

.method encodedLength()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_3

    nop

    :goto_0
    invoke-interface {v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v3

    goto/32 :goto_2

    nop

    :goto_1
    return v2

    :goto_2
    invoke-virtual {v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->encodedLength()I

    move-result v4

    goto/32 :goto_e

    nop

    :goto_3
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/BERSequence;->elements:[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_4

    nop

    :goto_4
    array-length v0, v0

    goto/32 :goto_c

    nop

    :goto_5
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_6

    nop

    :goto_6
    goto :goto_a

    :goto_7
    goto/32 :goto_8

    nop

    :goto_8
    add-int/lit8 v2, v1, 0x2

    goto/32 :goto_b

    nop

    :goto_9
    const/4 v2, 0x0

    :goto_a
    goto/32 :goto_10

    nop

    :goto_b
    add-int/lit8 v2, v2, 0x2

    goto/32 :goto_1

    nop

    :goto_c
    const/4 v1, 0x0

    goto/32 :goto_9

    nop

    :goto_d
    iget-object v3, p0, Lcom/android/internal/org/bouncycastle/asn1/BERSequence;->elements:[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_f

    nop

    :goto_e
    add-int/2addr v1, v4

    goto/32 :goto_5

    nop

    :goto_f
    aget-object v3, v3, v2

    goto/32 :goto_0

    nop

    :goto_10
    if-lt v2, v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_d

    nop
.end method
