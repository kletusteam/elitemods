.class public Lcom/android/internal/org/bouncycastle/asn1/DERSequence;
.super Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;


# instance fields
.field private bodyLength:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSequence;->bodyLength:I

    return-void
.end method

.method public constructor <init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;-><init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSequence;->bodyLength:I

    return-void
.end method

.method public constructor <init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;-><init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSequence;->bodyLength:I

    return-void
.end method

.method public constructor <init>([Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;-><init>([Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSequence;->bodyLength:I

    return-void
.end method

.method constructor <init>([Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;Z)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;-><init>([Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;Z)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSequence;->bodyLength:I

    return-void
.end method

.method public static convert(Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;)Lcom/android/internal/org/bouncycastle/asn1/DERSequence;
    .locals 1

    invoke-virtual {p0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;->toDERObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    check-cast v0, Lcom/android/internal/org/bouncycastle/asn1/DERSequence;

    return-object v0
.end method

.method private getBodyLength()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSequence;->bodyLength:I

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSequence;->elements:[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    array-length v0, v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    iget-object v3, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSequence;->elements:[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    aget-object v3, v3, v2

    invoke-interface {v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->toDERObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->encodedLength()I

    move-result v4

    add-int/2addr v1, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iput v1, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSequence;->bodyLength:I

    :cond_1
    iget v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSequence;->bodyLength:I

    return v0
.end method


# virtual methods
.method encode(Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    iput v2, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSequence;->bodyLength:I

    goto/32 :goto_28

    nop

    :goto_1
    if-nez p2, :cond_0

    goto/32 :goto_24

    :cond_0
    goto/32 :goto_31

    nop

    :goto_2
    invoke-virtual {v6}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->encodedLength()I

    move-result v7

    goto/32 :goto_2f

    nop

    :goto_3
    if-lt v5, v1, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_17

    nop

    :goto_4
    return-void

    :goto_5
    const/4 v5, 0x0

    :goto_6
    goto/32 :goto_a

    nop

    :goto_7
    iget-object v4, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSequence;->elements:[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_27

    nop

    :goto_8
    invoke-virtual {p1, v2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeLength(I)V

    goto/32 :goto_20

    nop

    :goto_9
    if-lt v2, v1, :cond_2

    goto/32 :goto_c

    :cond_2
    goto/32 :goto_7

    nop

    :goto_a
    if-lt v5, v1, :cond_3

    goto/32 :goto_2a

    :cond_3
    goto/32 :goto_2c

    nop

    :goto_b
    goto :goto_21

    :goto_c
    goto/32 :goto_4

    nop

    :goto_d
    const/4 v2, 0x0

    goto/32 :goto_1c

    nop

    :goto_e
    goto :goto_15

    :goto_f
    goto/32 :goto_d

    nop

    :goto_10
    array-length v1, v1

    goto/32 :goto_22

    nop

    :goto_11
    invoke-interface {v6}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v6

    goto/32 :goto_1e

    nop

    :goto_12
    invoke-virtual {v4}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->toDERObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v4

    goto/32 :goto_25

    nop

    :goto_13
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_b

    nop

    :goto_14
    goto :goto_1b

    :goto_15
    goto/32 :goto_26

    nop

    :goto_16
    aget-object v6, v6, v5

    goto/32 :goto_11

    nop

    :goto_17
    aget-object v6, v4, v5

    goto/32 :goto_2d

    nop

    :goto_18
    iget-object v1, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSequence;->elements:[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_10

    nop

    :goto_19
    invoke-virtual {p1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->getDERSubStream()Lcom/android/internal/org/bouncycastle/asn1/DEROutputStream;

    move-result-object v0

    goto/32 :goto_18

    nop

    :goto_1a
    const/4 v5, 0x0

    :goto_1b
    goto/32 :goto_3

    nop

    :goto_1c
    new-array v4, v1, [Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    goto/32 :goto_5

    nop

    :goto_1d
    aput-object v6, v4, v5

    goto/32 :goto_2

    nop

    :goto_1e
    invoke-virtual {v6}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->toDERObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v6

    goto/32 :goto_1d

    nop

    :goto_1f
    invoke-interface {v4}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v4

    goto/32 :goto_12

    nop

    :goto_20
    const/4 v2, 0x0

    :goto_21
    goto/32 :goto_9

    nop

    :goto_22
    iget v2, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSequence;->bodyLength:I

    goto/32 :goto_2b

    nop

    :goto_23
    invoke-virtual {p1, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    :goto_24
    goto/32 :goto_19

    nop

    :goto_25
    invoke-virtual {v4, v0, v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->encode(Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;Z)V

    goto/32 :goto_13

    nop

    :goto_26
    invoke-direct {p0}, Lcom/android/internal/org/bouncycastle/asn1/DERSequence;->getBodyLength()I

    move-result v2

    goto/32 :goto_8

    nop

    :goto_27
    aget-object v4, v4, v2

    goto/32 :goto_1f

    nop

    :goto_28
    invoke-virtual {p1, v2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeLength(I)V

    goto/32 :goto_1a

    nop

    :goto_29
    goto/16 :goto_6

    :goto_2a
    goto/32 :goto_0

    nop

    :goto_2b
    const/4 v3, 0x1

    goto/32 :goto_33

    nop

    :goto_2c
    iget-object v6, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSequence;->elements:[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_16

    nop

    :goto_2d
    invoke-virtual {v6, v0, v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->encode(Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;Z)V

    goto/32 :goto_2e

    nop

    :goto_2e
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_14

    nop

    :goto_2f
    add-int/2addr v2, v7

    goto/32 :goto_34

    nop

    :goto_30
    if-gt v1, v2, :cond_4

    goto/32 :goto_f

    :cond_4
    goto/32 :goto_e

    nop

    :goto_31
    const/16 v0, 0x30

    goto/32 :goto_23

    nop

    :goto_32
    const/16 v2, 0x10

    goto/32 :goto_30

    nop

    :goto_33
    if-ltz v2, :cond_5

    goto/32 :goto_15

    :cond_5
    goto/32 :goto_32

    nop

    :goto_34
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_29

    nop
.end method

.method encodedLength()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    invoke-direct {p0}, Lcom/android/internal/org/bouncycastle/asn1/DERSequence;->getBodyLength()I

    move-result v0

    goto/32 :goto_4

    nop

    :goto_1
    add-int/2addr v1, v0

    goto/32 :goto_2

    nop

    :goto_2
    return v1

    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_1

    nop

    :goto_4
    invoke-static {v0}, Lcom/android/internal/org/bouncycastle/asn1/StreamUtil;->calculateBodyLength(I)I

    move-result v1

    goto/32 :goto_3

    nop
.end method

.method toDERObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    return-object p0
.end method

.method toDLObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    return-object p0
.end method
