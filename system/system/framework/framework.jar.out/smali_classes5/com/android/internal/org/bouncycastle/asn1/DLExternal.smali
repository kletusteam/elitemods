.class public Lcom/android/internal/org/bouncycastle/asn1/DLExternal;
.super Lcom/android/internal/org/bouncycastle/asn1/ASN1External;


# direct methods
.method public constructor <init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1External;-><init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;)V

    return-void
.end method

.method public constructor <init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/internal/org/bouncycastle/asn1/ASN1Integer;Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;ILcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/android/internal/org/bouncycastle/asn1/ASN1External;-><init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/internal/org/bouncycastle/asn1/ASN1Integer;Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;ILcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;)V

    return-void
.end method

.method public constructor <init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/internal/org/bouncycastle/asn1/ASN1Integer;Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;Lcom/android/internal/org/bouncycastle/asn1/DERTaggedObject;)V
    .locals 6

    invoke-virtual {p4}, Lcom/android/internal/org/bouncycastle/asn1/DERTaggedObject;->getTagNo()I

    move-result v4

    invoke-virtual {p4}, Lcom/android/internal/org/bouncycastle/asn1/DERTaggedObject;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/internal/org/bouncycastle/asn1/DLExternal;-><init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/internal/org/bouncycastle/asn1/ASN1Integer;Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;ILcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;)V

    return-void
.end method


# virtual methods
.method encode(Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_c

    nop

    :goto_0
    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write([B)V

    :goto_1
    goto/32 :goto_1b

    nop

    :goto_2
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    goto/32 :goto_14

    nop

    :goto_3
    invoke-virtual {v1, v2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Integer;->getEncoded(Ljava/lang/String;)[B

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_4
    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write([B)V

    goto/32 :goto_16

    nop

    :goto_5
    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    goto/32 :goto_e

    nop

    :goto_6
    return-void

    :goto_7
    invoke-virtual {v1, v2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->getEncoded(Ljava/lang/String;)[B

    move-result-object v1

    goto/32 :goto_a

    nop

    :goto_8
    invoke-virtual {v1, v2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1TaggedObject;->getEncoded(Ljava/lang/String;)[B

    move-result-object v2

    goto/32 :goto_4

    nop

    :goto_9
    if-nez v1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_15

    nop

    :goto_a
    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write([B)V

    :goto_b
    goto/32 :goto_13

    nop

    :goto_c
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    goto/32 :goto_5

    nop

    :goto_d
    invoke-direct {v1, v3, v4, v5}, Lcom/android/internal/org/bouncycastle/asn1/DLTaggedObject;-><init>(ZILcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    goto/32 :goto_8

    nop

    :goto_e
    iget-object v1, p0, Lcom/android/internal/org/bouncycastle/asn1/DLExternal;->directReference:Lcom/android/internal/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    goto/32 :goto_1a

    nop

    :goto_f
    if-nez v1, :cond_1

    goto/32 :goto_20

    :cond_1
    goto/32 :goto_17

    nop

    :goto_10
    iget-object v1, p0, Lcom/android/internal/org/bouncycastle/asn1/DLExternal;->indirectReference:Lcom/android/internal/org/bouncycastle/asn1/ASN1Integer;

    goto/32 :goto_9

    nop

    :goto_11
    if-nez v1, :cond_2

    goto/32 :goto_b

    :cond_2
    goto/32 :goto_19

    nop

    :goto_12
    invoke-virtual {v1, v2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1ObjectIdentifier;->getEncoded(Ljava/lang/String;)[B

    move-result-object v1

    goto/32 :goto_1f

    nop

    :goto_13
    new-instance v1, Lcom/android/internal/org/bouncycastle/asn1/DLTaggedObject;

    goto/32 :goto_18

    nop

    :goto_14
    invoke-virtual {p1, p2, v2, v3, v4}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeEncoded(ZII[B)V

    goto/32 :goto_6

    nop

    :goto_15
    iget-object v1, p0, Lcom/android/internal/org/bouncycastle/asn1/DLExternal;->indirectReference:Lcom/android/internal/org/bouncycastle/asn1/ASN1Integer;

    goto/32 :goto_3

    nop

    :goto_16
    const/16 v2, 0x20

    goto/32 :goto_1d

    nop

    :goto_17
    iget-object v1, p0, Lcom/android/internal/org/bouncycastle/asn1/DLExternal;->directReference:Lcom/android/internal/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    goto/32 :goto_12

    nop

    :goto_18
    const/4 v3, 0x1

    goto/32 :goto_1c

    nop

    :goto_19
    iget-object v1, p0, Lcom/android/internal/org/bouncycastle/asn1/DLExternal;->dataValueDescriptor:Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    goto/32 :goto_7

    nop

    :goto_1a
    const-string v2, "DL"

    goto/32 :goto_f

    nop

    :goto_1b
    iget-object v1, p0, Lcom/android/internal/org/bouncycastle/asn1/DLExternal;->dataValueDescriptor:Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    goto/32 :goto_11

    nop

    :goto_1c
    iget v4, p0, Lcom/android/internal/org/bouncycastle/asn1/DLExternal;->encoding:I

    goto/32 :goto_1e

    nop

    :goto_1d
    const/16 v3, 0x8

    goto/32 :goto_2

    nop

    :goto_1e
    iget-object v5, p0, Lcom/android/internal/org/bouncycastle/asn1/DLExternal;->externalContent:Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    goto/32 :goto_d

    nop

    :goto_1f
    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write([B)V

    :goto_20
    goto/32 :goto_10

    nop
.end method

.method encodedLength()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    array-length v0, v0

    goto/32 :goto_2

    nop

    :goto_1
    invoke-virtual {p0}, Lcom/android/internal/org/bouncycastle/asn1/DLExternal;->getEncoded()[B

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_2
    return v0
.end method

.method toDLObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    return-object p0
.end method
