.class Lcom/android/internal/org/bouncycastle/crypto/util/PublicKeyFactory$ECConverter;
.super Lcom/android/internal/org/bouncycastle/crypto/util/PublicKeyFactory$SubjectPublicKeyInfoConverter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/org/bouncycastle/crypto/util/PublicKeyFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ECConverter"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/internal/org/bouncycastle/crypto/util/PublicKeyFactory$SubjectPublicKeyInfoConverter;-><init>(Lcom/android/internal/org/bouncycastle/crypto/util/PublicKeyFactory$SubjectPublicKeyInfoConverter-IA;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/internal/org/bouncycastle/crypto/util/PublicKeyFactory$ECConverter-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/internal/org/bouncycastle/crypto/util/PublicKeyFactory$ECConverter;-><init>()V

    return-void
.end method


# virtual methods
.method getPublicKeyParameters(Lcom/android/internal/org/bouncycastle/asn1/x509/SubjectPublicKeyInfo;Ljava/lang/Object;)Lcom/android/internal/org/bouncycastle/crypto/params/AsymmetricKeyParameter;
    .locals 9

    goto/32 :goto_38

    nop

    :goto_0
    check-cast v1, Lcom/android/internal/org/bouncycastle/crypto/params/ECDomainParameters;

    goto/32 :goto_13

    nop

    :goto_1
    invoke-static {v0}, Lcom/android/internal/org/bouncycastle/asn1/x9/X962Parameters;->getInstance(Ljava/lang/Object;)Lcom/android/internal/org/bouncycastle/asn1/x9/X962Parameters;

    move-result-object v0

    goto/32 :goto_c

    nop

    :goto_2
    throw v7

    :goto_3
    goto/32 :goto_1b

    nop

    :goto_4
    const/4 v7, 0x2

    goto/32 :goto_3e

    nop

    :goto_5
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/x509/AlgorithmIdentifier;->getParameters()Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_6
    aget-byte v5, v3, v5

    goto/32 :goto_2f

    nop

    :goto_7
    if-eq v5, v6, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_17

    nop

    :goto_8
    new-instance v5, Lcom/android/internal/org/bouncycastle/asn1/x9/X9IntegerConverter;

    goto/32 :goto_1a

    nop

    :goto_9
    invoke-virtual {v1}, Lcom/android/internal/org/bouncycastle/crypto/params/ECDomainParameters;->getCurve()Lcom/android/internal/org/bouncycastle/math/ec/ECCurve;

    move-result-object v6

    goto/32 :goto_2c

    nop

    :goto_a
    move-object v1, v2

    :goto_b
    goto/32 :goto_3f

    nop

    :goto_c
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/x9/X962Parameters;->isNamedCurve()Z

    move-result v1

    goto/32 :goto_1e

    nop

    :goto_d
    move-object v1, v3

    goto/32 :goto_31

    nop

    :goto_e
    new-instance v4, Lcom/android/internal/org/bouncycastle/asn1/DEROctetString;

    goto/32 :goto_2b

    nop

    :goto_f
    invoke-virtual {v2}, Lcom/android/internal/org/bouncycastle/asn1/DERBitString;->getBytes()[B

    move-result-object v3

    goto/32 :goto_e

    nop

    :goto_10
    if-eqz v2, :cond_1

    goto/32 :goto_41

    :cond_1
    goto/32 :goto_40

    nop

    :goto_11
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/x9/X962Parameters;->isImplicitlyCA()Z

    move-result v1

    goto/32 :goto_2a

    nop

    :goto_12
    const/4 v5, 0x1

    goto/32 :goto_1d

    nop

    :goto_13
    goto :goto_b

    :goto_14
    goto/32 :goto_39

    nop

    :goto_15
    array-length v6, v3

    goto/32 :goto_4

    nop

    :goto_16
    aget-byte v5, v3, v7

    goto/32 :goto_1f

    nop

    :goto_17
    aget-byte v5, v3, v7

    goto/32 :goto_34

    nop

    :goto_18
    move-object v4, v6

    goto/32 :goto_26

    nop

    :goto_19
    move-object v1, p2

    goto/32 :goto_0

    nop

    :goto_1a
    invoke-direct {v5}, Lcom/android/internal/org/bouncycastle/asn1/x9/X9IntegerConverter;-><init>()V

    goto/32 :goto_3d

    nop

    :goto_1b
    new-instance v5, Lcom/android/internal/org/bouncycastle/asn1/x9/X9ECPoint;

    goto/32 :goto_9

    nop

    :goto_1c
    sub-int/2addr v7, v6

    goto/32 :goto_3c

    nop

    :goto_1d
    aget-byte v5, v3, v5

    goto/32 :goto_15

    nop

    :goto_1e
    if-nez v1, :cond_2

    goto/32 :goto_32

    :cond_2
    goto/32 :goto_3b

    nop

    :goto_1f
    if-eq v5, v6, :cond_3

    goto/32 :goto_3

    :cond_3
    :goto_20
    goto/32 :goto_8

    nop

    :goto_21
    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_2

    nop

    :goto_22
    invoke-virtual {v5}, Lcom/android/internal/org/bouncycastle/asn1/x9/X9ECPoint;->getPoint()Lcom/android/internal/org/bouncycastle/math/ec/ECPoint;

    move-result-object v7

    goto/32 :goto_3a

    nop

    :goto_23
    new-instance v3, Lcom/android/internal/org/bouncycastle/crypto/params/ECNamedDomainParameters;

    goto/32 :goto_36

    nop

    :goto_24
    new-instance v6, Lcom/android/internal/org/bouncycastle/crypto/params/ECPublicKeyParameters;

    goto/32 :goto_22

    nop

    :goto_25
    return-object v6

    :goto_26
    goto/16 :goto_3

    :catch_0
    move-exception v6

    goto/32 :goto_29

    nop

    :goto_27
    array-length v7, v3

    goto/32 :goto_1c

    nop

    :goto_28
    invoke-direct {v2, v1}, Lcom/android/internal/org/bouncycastle/crypto/params/ECDomainParameters;-><init>(Lcom/android/internal/org/bouncycastle/asn1/x9/X9ECParameters;)V

    goto/32 :goto_a

    nop

    :goto_29
    new-instance v7, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_33

    nop

    :goto_2a
    if-nez v1, :cond_4

    goto/32 :goto_14

    :cond_4
    goto/32 :goto_19

    nop

    :goto_2b
    invoke-direct {v4, v3}, Lcom/android/internal/org/bouncycastle/asn1/DEROctetString;-><init>([B)V

    goto/32 :goto_37

    nop

    :goto_2c
    invoke-direct {v5, v6, v4}, Lcom/android/internal/org/bouncycastle/asn1/x9/X9ECPoint;-><init>(Lcom/android/internal/org/bouncycastle/math/ec/ECCurve;Lcom/android/internal/org/bouncycastle/asn1/ASN1OctetString;)V

    goto/32 :goto_24

    nop

    :goto_2d
    if-eq v5, v6, :cond_5

    goto/32 :goto_3

    :cond_5
    goto/32 :goto_12

    nop

    :goto_2e
    invoke-virtual {v5, v7}, Lcom/android/internal/org/bouncycastle/asn1/x9/X9IntegerConverter;->getByteLength(Lcom/android/internal/org/bouncycastle/math/ec/ECCurve;)I

    move-result v5

    goto/32 :goto_27

    nop

    :goto_2f
    const/4 v6, 0x4

    goto/32 :goto_2d

    nop

    :goto_30
    check-cast v1, Lcom/android/internal/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    goto/32 :goto_44

    nop

    :goto_31
    goto/16 :goto_b

    :goto_32
    goto/32 :goto_11

    nop

    :goto_33
    const-string v8, "error recovering public key"

    goto/32 :goto_21

    nop

    :goto_34
    const/4 v6, 0x3

    goto/32 :goto_35

    nop

    :goto_35
    if-ne v5, v7, :cond_6

    goto/32 :goto_20

    :cond_6
    goto/32 :goto_16

    nop

    :goto_36
    invoke-direct {v3, v1, v2}, Lcom/android/internal/org/bouncycastle/crypto/params/ECNamedDomainParameters;-><init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/internal/org/bouncycastle/asn1/x9/X9ECParameters;)V

    goto/32 :goto_d

    nop

    :goto_37
    const/4 v5, 0x0

    goto/32 :goto_6

    nop

    :goto_38
    invoke-virtual {p1}, Lcom/android/internal/org/bouncycastle/asn1/x509/SubjectPublicKeyInfo;->getAlgorithm()Lcom/android/internal/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_39
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/x9/X962Parameters;->getParameters()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v1

    goto/32 :goto_42

    nop

    :goto_3a
    invoke-direct {v6, v7, v1}, Lcom/android/internal/org/bouncycastle/crypto/params/ECPublicKeyParameters;-><init>(Lcom/android/internal/org/bouncycastle/math/ec/ECPoint;Lcom/android/internal/org/bouncycastle/crypto/params/ECDomainParameters;)V

    goto/32 :goto_25

    nop

    :goto_3b
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/x9/X962Parameters;->getParameters()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v1

    goto/32 :goto_30

    nop

    :goto_3c
    if-ge v5, v7, :cond_7

    goto/32 :goto_3

    :cond_7
    :try_start_0
    invoke-static {v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->fromByteArray([B)Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v6

    check-cast v6, Lcom/android/internal/org/bouncycastle/asn1/ASN1OctetString;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_18

    nop

    :goto_3d
    invoke-virtual {v1}, Lcom/android/internal/org/bouncycastle/crypto/params/ECDomainParameters;->getCurve()Lcom/android/internal/org/bouncycastle/math/ec/ECCurve;

    move-result-object v7

    goto/32 :goto_2e

    nop

    :goto_3e
    sub-int/2addr v6, v7

    goto/32 :goto_7

    nop

    :goto_3f
    invoke-virtual {p1}, Lcom/android/internal/org/bouncycastle/asn1/x509/SubjectPublicKeyInfo;->getPublicKeyData()Lcom/android/internal/org/bouncycastle/asn1/DERBitString;

    move-result-object v2

    goto/32 :goto_f

    nop

    :goto_40
    invoke-static {v1}, Lcom/android/internal/org/bouncycastle/asn1/x9/ECNamedCurveTable;->getByOID(Lcom/android/internal/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/internal/org/bouncycastle/asn1/x9/X9ECParameters;

    move-result-object v2

    :goto_41
    goto/32 :goto_23

    nop

    :goto_42
    invoke-static {v1}, Lcom/android/internal/org/bouncycastle/asn1/x9/X9ECParameters;->getInstance(Ljava/lang/Object;)Lcom/android/internal/org/bouncycastle/asn1/x9/X9ECParameters;

    move-result-object v1

    goto/32 :goto_43

    nop

    :goto_43
    new-instance v2, Lcom/android/internal/org/bouncycastle/crypto/params/ECDomainParameters;

    goto/32 :goto_28

    nop

    :goto_44
    invoke-static {v1}, Lcom/android/internal/org/bouncycastle/crypto/ec/CustomNamedCurves;->getByOID(Lcom/android/internal/org/bouncycastle/asn1/ASN1ObjectIdentifier;)Lcom/android/internal/org/bouncycastle/asn1/x9/X9ECParameters;

    move-result-object v2

    goto/32 :goto_10

    nop
.end method
