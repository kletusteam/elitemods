.class public final Lcom/android/internal/R$anim;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "anim"
.end annotation


# static fields
.field public static final accelerate_decelerate_interpolator:I = 0x10a0004

.field public static final accelerate_interpolator:I = 0x10a0005

.field public static final activity_close_enter:I = 0x10a000d

.field public static final activity_close_exit:I = 0x10a000e

.field public static final activity_open_enter:I = 0x10a000f

.field public static final activity_open_exit:I = 0x10a0010

.field public static final activity_translucent_close_exit:I = 0x10a0011

.field public static final activity_translucent_open_enter:I = 0x10a0012

.field public static final anticipate_interpolator:I = 0x10a0007

.field public static final anticipate_overshoot_interpolator:I = 0x10a0009

.field public static final app_starting_exit:I = 0x10a0013

.field public static final bounce_interpolator:I = 0x10a000a

.field public static final cross_profile_apps_thumbnail_enter:I = 0x10a0021

.field public static final cycle_interpolator:I = 0x10a000c

.field public static final decelerate_interpolator:I = 0x10a0006

.field public static final dock_bottom_enter:I = 0x10a0026

.field public static final dock_bottom_exit:I = 0x10a0027

.field public static final dock_bottom_exit_keyguard:I = 0x10a0028

.field public static final dock_left_enter:I = 0x10a0029

.field public static final dock_left_exit:I = 0x10a002a

.field public static final dock_right_enter:I = 0x10a002b

.field public static final dock_right_exit:I = 0x10a002c

.field public static final dock_top_enter:I = 0x10a002d

.field public static final dock_top_exit:I = 0x10a002e

.field public static final dream_activity_close_exit:I = 0x10a002f

.field public static final dream_activity_open_enter:I = 0x10a0030

.field public static final dream_activity_open_exit:I = 0x10a0031

.field public static final fade_in:I = 0x10a0000

.field public static final fade_out:I = 0x10a0001

.field public static final launch_task_behind_source:I = 0x10a0069

.field public static final linear_interpolator:I = 0x10a000b

.field public static final lock_screen_behind_enter:I = 0x10a006b

.field public static final lock_screen_behind_enter_alpha:I = 0x10a006c

.field public static final lock_screen_behind_enter_fade_in:I = 0x10a006d

.field public static final lock_screen_behind_enter_scale:I = 0x10a006e

.field public static final lock_screen_behind_enter_subtle:I = 0x10a006f

.field public static final lock_screen_behind_enter_wallpaper:I = 0x10a0070

.field public static final lock_screen_wallpaper_exit:I = 0x10a0073

.field public static final miui_foldable_display_swapped_enter:I = 0x10a0074

.field public static final miui_foldable_display_swapped_exit:I = 0x10a0075

.field public static final overshoot_interpolator:I = 0x10a0008

.field public static final push_down_in:I = 0x10a007e

.field public static final push_down_out:I = 0x10a0080

.field public static final push_up_in:I = 0x10a0082

.field public static final push_up_out:I = 0x10a0083

.field public static final resolver_launch_anim:I = 0x10a0089

.field public static final rotation_animation_enter:I = 0x10a008a

.field public static final rotation_animation_jump_exit:I = 0x10a008b

.field public static final rotation_animation_xfade_exit:I = 0x10a008c

.field public static final screen_rotate_0_enter:I = 0x10a008d

.field public static final screen_rotate_0_exit:I = 0x10a008e

.field public static final screen_rotate_180_enter:I = 0x10a008f

.field public static final screen_rotate_180_exit:I = 0x10a0090

.field public static final screen_rotate_180_frame:I = 0x10a0091

.field public static final screen_rotate_alpha:I = 0x10a0092

.field public static final screen_rotate_finish_enter:I = 0x10a0093

.field public static final screen_rotate_finish_exit:I = 0x10a0094

.field public static final screen_rotate_finish_frame:I = 0x10a0095

.field public static final screen_rotate_minus_90_enter:I = 0x10a0096

.field public static final screen_rotate_minus_90_exit:I = 0x10a0097

.field public static final screen_rotate_plus_90_enter:I = 0x10a0098

.field public static final screen_rotate_plus_90_exit:I = 0x10a0099

.field public static final screen_rotate_start_enter:I = 0x10a009a

.field public static final screen_rotate_start_exit:I = 0x10a009b

.field public static final screen_rotate_start_frame:I = 0x10a009c

.field public static final screen_user_enter:I = 0x10a009d

.field public static final screen_user_exit:I = 0x10a009e

.field public static final slide_in_child_bottom:I = 0x10a00a6

.field public static final slide_in_left:I = 0x10a0002

.field public static final slide_in_right:I = 0x10a00a9

.field public static final slide_out_left:I = 0x10a00ac

.field public static final slide_out_right:I = 0x10a0003

.field public static final task_fragment_close_enter:I = 0x10a00b5

.field public static final task_fragment_close_exit:I = 0x10a00b6

.field public static final task_fragment_open_enter:I = 0x10a00b7

.field public static final task_fragment_open_exit:I = 0x10a00b8

.field public static final task_open_enter:I = 0x10a00b9

.field public static final task_open_enter_cross_profile_apps:I = 0x10a00ba

.field public static final task_open_exit:I = 0x10a00bb

.field public static final voice_activity_close_enter:I = 0x10a00c2

.field public static final voice_activity_close_exit:I = 0x10a00c3

.field public static final voice_activity_open_enter:I = 0x10a00c4

.field public static final voice_activity_open_exit:I = 0x10a00c5

.field public static final wallpaper_open_exit:I = 0x10a00d1

.field public static final window_move_from_decor:I = 0x10a00d2


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
