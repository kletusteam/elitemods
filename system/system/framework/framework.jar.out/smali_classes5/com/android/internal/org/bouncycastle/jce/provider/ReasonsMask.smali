.class Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;
.super Ljava/lang/Object;


# static fields
.field static final allReasons:Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;


# instance fields
.field private _reasons:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;

    const v1, 0x80ff

    invoke-direct {v0, v1}, Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;-><init>(I)V

    sput-object v0, Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;->allReasons:Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;-><init>(I)V

    return-void
.end method

.method private constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;->_reasons:I

    return-void
.end method

.method constructor <init>(Lcom/android/internal/org/bouncycastle/asn1/x509/ReasonFlags;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcom/android/internal/org/bouncycastle/asn1/x509/ReasonFlags;->intValue()I

    move-result v0

    iput v0, p0, Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;->_reasons:I

    return-void
.end method


# virtual methods
.method addReasons(Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;)V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p1}, Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;->getReasons()I

    move-result v1

    goto/32 :goto_2

    nop

    :goto_1
    iget v0, p0, Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;->_reasons:I

    goto/32 :goto_0

    nop

    :goto_2
    or-int/2addr v0, v1

    goto/32 :goto_3

    nop

    :goto_3
    iput v0, p0, Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;->_reasons:I

    goto/32 :goto_4

    nop

    :goto_4
    return-void
.end method

.method getReasons()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p0, Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;->_reasons:I

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method hasNewReasons(Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;)Z
    .locals 3

    goto/32 :goto_b

    nop

    :goto_0
    goto :goto_5

    :goto_1
    goto/32 :goto_4

    nop

    :goto_2
    return v0

    :goto_3
    or-int/2addr v0, v1

    goto/32 :goto_8

    nop

    :goto_4
    const/4 v0, 0x0

    :goto_5
    goto/32 :goto_2

    nop

    :goto_6
    const/4 v0, 0x1

    goto/32 :goto_0

    nop

    :goto_7
    iget v2, p0, Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;->_reasons:I

    goto/32 :goto_9

    nop

    :goto_8
    if-nez v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_6

    nop

    :goto_9
    xor-int/2addr v1, v2

    goto/32 :goto_3

    nop

    :goto_a
    invoke-virtual {p1}, Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;->getReasons()I

    move-result v1

    goto/32 :goto_7

    nop

    :goto_b
    iget v0, p0, Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;->_reasons:I

    goto/32 :goto_a

    nop
.end method

.method intersect(Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;)Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;
    .locals 4

    goto/32 :goto_6

    nop

    :goto_0
    new-instance v1, Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;

    goto/32 :goto_7

    nop

    :goto_1
    invoke-virtual {v0, v1}, Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;->addReasons(Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;)V

    goto/32 :goto_8

    nop

    :goto_2
    invoke-virtual {p1}, Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;->getReasons()I

    move-result v3

    goto/32 :goto_4

    nop

    :goto_3
    invoke-direct {v1, v2}, Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;-><init>(I)V

    goto/32 :goto_1

    nop

    :goto_4
    and-int/2addr v2, v3

    goto/32 :goto_3

    nop

    :goto_5
    invoke-direct {v0}, Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;-><init>()V

    goto/32 :goto_0

    nop

    :goto_6
    new-instance v0, Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;

    goto/32 :goto_5

    nop

    :goto_7
    iget v2, p0, Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;->_reasons:I

    goto/32 :goto_2

    nop

    :goto_8
    return-object v0
.end method

.method isAllReasons()Z
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    if-eq v0, v1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_4

    nop

    :goto_1
    iget v1, v1, Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;->_reasons:I

    goto/32 :goto_0

    nop

    :goto_2
    sget-object v1, Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;->allReasons:Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;

    goto/32 :goto_1

    nop

    :goto_3
    iget v0, p0, Lcom/android/internal/org/bouncycastle/jce/provider/ReasonsMask;->_reasons:I

    goto/32 :goto_2

    nop

    :goto_4
    const/4 v0, 0x1

    goto/32 :goto_6

    nop

    :goto_5
    return v0

    :goto_6
    goto :goto_9

    :goto_7
    goto/32 :goto_8

    nop

    :goto_8
    const/4 v0, 0x0

    :goto_9
    goto/32 :goto_5

    nop
.end method
