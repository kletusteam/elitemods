.class public Lcom/android/internal/org/bouncycastle/asn1/BERSet;
.super Lcom/android/internal/org/bouncycastle/asn1/ASN1Set;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Set;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Set;-><init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    return-void
.end method

.method public constructor <init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Set;-><init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;Z)V

    return-void
.end method

.method constructor <init>(Z[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Set;-><init>(Z[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    return-void
.end method

.method public constructor <init>([Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Set;-><init>([Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;Z)V

    return-void
.end method


# virtual methods
.method encode(Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    const/16 v1, 0x31

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {p1, p2, v1, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeEncodedIndef(ZI[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    goto/32 :goto_3

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/BERSet;->elements:[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_0

    nop

    :goto_3
    return-void
.end method

.method encodedLength()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_9

    nop

    :goto_0
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_f

    nop

    :goto_1
    if-lt v2, v0, :cond_0

    goto/32 :goto_10

    :cond_0
    goto/32 :goto_3

    nop

    :goto_2
    aget-object v3, v3, v2

    goto/32 :goto_8

    nop

    :goto_3
    iget-object v3, p0, Lcom/android/internal/org/bouncycastle/asn1/BERSet;->elements:[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_2

    nop

    :goto_4
    const/4 v1, 0x0

    goto/32 :goto_d

    nop

    :goto_5
    add-int/lit8 v2, v2, 0x2

    goto/32 :goto_7

    nop

    :goto_6
    add-int/lit8 v2, v1, 0x2

    goto/32 :goto_5

    nop

    :goto_7
    return v2

    :goto_8
    invoke-interface {v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v3

    goto/32 :goto_a

    nop

    :goto_9
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/BERSet;->elements:[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_b

    nop

    :goto_a
    invoke-virtual {v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->encodedLength()I

    move-result v4

    goto/32 :goto_c

    nop

    :goto_b
    array-length v0, v0

    goto/32 :goto_4

    nop

    :goto_c
    add-int/2addr v1, v4

    goto/32 :goto_0

    nop

    :goto_d
    const/4 v2, 0x0

    :goto_e
    goto/32 :goto_1

    nop

    :goto_f
    goto :goto_e

    :goto_10
    goto/32 :goto_6

    nop
.end method
