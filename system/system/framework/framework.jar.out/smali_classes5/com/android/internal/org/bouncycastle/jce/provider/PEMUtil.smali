.class public Lcom/android/internal/org/bouncycastle/jce/provider/PEMUtil;
.super Ljava/lang/Object;


# instance fields
.field private final _footer1:Ljava/lang/String;

.field private final _footer2:Ljava/lang/String;

.field private final _header1:Ljava/lang/String;

.field private final _header2:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "-----BEGIN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-----"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/internal/org/bouncycastle/jce/provider/PEMUtil;->_header1:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "-----BEGIN X509 "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/internal/org/bouncycastle/jce/provider/PEMUtil;->_header2:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "-----END "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/internal/org/bouncycastle/jce/provider/PEMUtil;->_footer1:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "-----END X509 "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/internal/org/bouncycastle/jce/provider/PEMUtil;->_footer2:Ljava/lang/String;

    return-void
.end method

.method private readLine(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v1

    move v2, v1

    const/16 v3, 0xd

    if-eq v1, v3, :cond_2

    const/16 v1, 0xa

    if-eq v2, v1, :cond_2

    if-ltz v2, :cond_2

    if-ne v2, v3, :cond_1

    goto :goto_0

    :cond_1
    int-to-char v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_2
    if-ltz v2, :cond_3

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    if-gez v2, :cond_4

    const/4 v1, 0x0

    return-object v1

    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method readPEMObject(Ljava/io/InputStream;)Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    new-instance v0, Ljava/lang/StringBuffer;

    goto/32 :goto_5

    nop

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    goto/32 :goto_b

    nop

    :goto_2
    check-cast v3, Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;

    goto/32 :goto_f

    nop

    :goto_3
    if-nez v1, :cond_0

    goto/32 :goto_2d

    :cond_0
    goto/32 :goto_c

    nop

    :goto_4
    const/4 v1, 0x0

    goto/32 :goto_12

    nop

    :goto_5
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    :goto_6
    goto/32 :goto_1b

    nop

    :goto_7
    invoke-direct {p0, p1}, Lcom/android/internal/org/bouncycastle/jce/provider/PEMUtil;->readLine(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_17

    nop

    :goto_8
    if-nez v3, :cond_1

    goto/32 :goto_10

    :cond_1
    goto/32 :goto_28

    nop

    :goto_9
    instance-of v3, v1, Lcom/android/internal/org/bouncycastle/asn1/ASN1Sequence;

    goto/32 :goto_8

    nop

    :goto_a
    iget-object v1, p0, Lcom/android/internal/org/bouncycastle/jce/provider/PEMUtil;->_header2:Ljava/lang/String;

    goto/32 :goto_1a

    nop

    :goto_b
    if-eqz v1, :cond_2

    goto/32 :goto_21

    :cond_2
    goto/32 :goto_a

    nop

    :goto_c
    new-instance v1, Lcom/android/internal/org/bouncycastle/asn1/ASN1InputStream;

    goto/32 :goto_1c

    nop

    :goto_d
    new-instance v3, Ljava/io/IOException;

    goto/32 :goto_18

    nop

    :goto_e
    if-nez v1, :cond_3

    goto/32 :goto_21

    :cond_3
    goto/32 :goto_29

    nop

    :goto_f
    return-object v3

    :goto_10
    goto/32 :goto_d

    nop

    :goto_11
    invoke-virtual {v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1InputStream;->readObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v1

    goto/32 :goto_9

    nop

    :goto_12
    return-object v1

    :goto_13
    goto :goto_25

    :goto_14
    goto/32 :goto_26

    nop

    :goto_15
    iget-object v1, p0, Lcom/android/internal/org/bouncycastle/jce/provider/PEMUtil;->_footer2:Ljava/lang/String;

    goto/32 :goto_27

    nop

    :goto_16
    iget-object v1, p0, Lcom/android/internal/org/bouncycastle/jce/provider/PEMUtil;->_footer1:Ljava/lang/String;

    goto/32 :goto_2e

    nop

    :goto_17
    move-object v2, v1

    goto/32 :goto_22

    nop

    :goto_18
    const-string/jumbo v4, "malformed PEM data encountered"

    goto/32 :goto_19

    nop

    :goto_19
    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_2c

    nop

    :goto_1a
    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    goto/32 :goto_20

    nop

    :goto_1b
    invoke-direct {p0, p1}, Lcom/android/internal/org/bouncycastle/jce/provider/PEMUtil;->readLine(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_2b

    nop

    :goto_1c
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_2a

    nop

    :goto_1d
    if-nez v1, :cond_4

    goto/32 :goto_14

    :cond_4
    goto/32 :goto_13

    nop

    :goto_1e
    invoke-direct {v1, v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1InputStream;-><init>([B)V

    goto/32 :goto_11

    nop

    :goto_1f
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    goto/32 :goto_3

    nop

    :goto_20
    if-nez v1, :cond_5

    goto/32 :goto_6

    :cond_5
    :goto_21
    goto/32 :goto_7

    nop

    :goto_22
    if-nez v1, :cond_6

    goto/32 :goto_25

    :cond_6
    goto/32 :goto_16

    nop

    :goto_23
    if-eqz v1, :cond_7

    goto/32 :goto_25

    :cond_7
    goto/32 :goto_15

    nop

    :goto_24
    goto :goto_21

    :goto_25
    goto/32 :goto_1f

    nop

    :goto_26
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/32 :goto_24

    nop

    :goto_27
    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    goto/32 :goto_1d

    nop

    :goto_28
    move-object v3, v1

    goto/32 :goto_2

    nop

    :goto_29
    iget-object v1, p0, Lcom/android/internal/org/bouncycastle/jce/provider/PEMUtil;->_header1:Ljava/lang/String;

    goto/32 :goto_1

    nop

    :goto_2a
    invoke-static {v3}, Lcom/android/internal/org/bouncycastle/util/encoders/Base64;->decode(Ljava/lang/String;)[B

    move-result-object v3

    goto/32 :goto_1e

    nop

    :goto_2b
    move-object v2, v1

    goto/32 :goto_e

    nop

    :goto_2c
    throw v3

    :goto_2d
    goto/32 :goto_4

    nop

    :goto_2e
    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    goto/32 :goto_23

    nop
.end method
