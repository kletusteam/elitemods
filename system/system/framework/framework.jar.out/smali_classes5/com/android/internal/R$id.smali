.class public final Lcom/android/internal/R$id;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final accessibilityActionClickOnClickableSpan:I = 0x1020195

.field public static final accessibilityActionContextClick:I = 0x102003c

.field public static final accessibilityActionDragCancel:I = 0x1020057

.field public static final accessibilityActionDragDrop:I = 0x1020056

.field public static final accessibilityActionDragStart:I = 0x1020055

.field public static final accessibilityActionHideTooltip:I = 0x1020045

.field public static final accessibilityActionImeEnter:I = 0x1020054

.field public static final accessibilityActionMoveWindow:I = 0x1020042

.field public static final accessibilityActionPageDown:I = 0x1020047

.field public static final accessibilityActionPageLeft:I = 0x1020048

.field public static final accessibilityActionPageRight:I = 0x1020049

.field public static final accessibilityActionPageUp:I = 0x1020046

.field public static final accessibilityActionPressAndHold:I = 0x102004a

.field public static final accessibilityActionScrollDown:I = 0x102003a

.field public static final accessibilityActionScrollLeft:I = 0x1020039

.field public static final accessibilityActionScrollRight:I = 0x102003b

.field public static final accessibilityActionScrollToPosition:I = 0x1020037

.field public static final accessibilityActionScrollUp:I = 0x1020038

.field public static final accessibilityActionSetProgress:I = 0x102003d

.field public static final accessibilityActionShowOnScreen:I = 0x1020036

.field public static final accessibilityActionShowTextSuggestions:I = 0x1020058

.field public static final accessibilityActionShowTooltip:I = 0x1020044

.field public static final accessibilitySystemActionBack:I = 0x102004b

.field public static final accessibilitySystemActionHome:I = 0x102004c

.field public static final accessibilitySystemActionLockScreen:I = 0x1020052

.field public static final accessibilitySystemActionNotifications:I = 0x102004e

.field public static final accessibilitySystemActionPowerDialog:I = 0x1020050

.field public static final accessibilitySystemActionQuickSettings:I = 0x102004f

.field public static final accessibilitySystemActionRecents:I = 0x102004d

.field public static final accessibilitySystemActionTakeScreenshot:I = 0x1020053

.field public static final accessibilitySystemActionToggleSplitScreen:I = 0x1020051

.field public static final accessibility_button_chooser_grid:I = 0x1020196

.field public static final accessibility_button_prompt:I = 0x1020197

.field public static final accessibility_button_prompt_prologue:I = 0x1020198

.field public static final accessibility_button_target_icon:I = 0x1020199

.field public static final accessibility_button_target_label:I = 0x102019a

.field public static final accessibility_permissionDialog_icon:I = 0x10201a2

.field public static final accessibility_permissionDialog_title:I = 0x10201a3

.field public static final accessibility_permission_enable_allow_button:I = 0x10201a4

.field public static final accessibility_permission_enable_deny_button:I = 0x10201a5

.field public static final accessibility_shortcut_target_checkbox:I = 0x10201a6

.field public static final accessibility_shortcut_target_icon:I = 0x10201a7

.field public static final accessibility_shortcut_target_label:I = 0x10201a8

.field public static final accessibility_shortcut_target_status:I = 0x10201a9

.field public static final account_name:I = 0x10201ab

.field public static final account_row_icon:I = 0x10201ac

.field public static final account_row_text:I = 0x10201ad

.field public static final account_type:I = 0x10201ae

.field public static final action0:I = 0x10201af

.field public static final action1:I = 0x10201b0

.field public static final action2:I = 0x10201b1

.field public static final action3:I = 0x10201b2

.field public static final action4:I = 0x10201b3

.field public static final action_bar:I = 0x10201bc

.field public static final action_bar_container:I = 0x10201bd

.field public static final action_bar_spinner:I = 0x10201be

.field public static final action_bar_subtitle:I = 0x10201bf

.field public static final action_bar_title:I = 0x10201c0

.field public static final action_context_bar:I = 0x10201c1

.field public static final action_menu_presenter:I = 0x10201c4

.field public static final action_mode_bar_stub:I = 0x10201c6

.field public static final action_mode_close_button:I = 0x10201c7

.field public static final actions:I = 0x10201c8

.field public static final actions_container:I = 0x10201c9

.field public static final actions_container_layout:I = 0x10201ca

.field public static final activity_chooser_view_content:I = 0x10201cb

.field public static final addToDictionary:I = 0x102002a

.field public static final addToDictionaryButton:I = 0x10201cd

.field public static final aerr_app_info:I = 0x10201d2

.field public static final aerr_close:I = 0x10201d3

.field public static final aerr_mute:I = 0x10201d4

.field public static final aerr_report:I = 0x10201d5

.field public static final aerr_restart:I = 0x10201d6

.field public static final aerr_wait:I = 0x10201d7

.field public static final alertTitle:I = 0x10201da

.field public static final alerted_icon:I = 0x10201db

.field public static final allow_button:I = 0x10201e1

.field public static final alternate_expand_target:I = 0x10201e3

.field public static final alwaysUse:I = 0x10201e7

.field public static final amPm:I = 0x10201e8

.field public static final am_label:I = 0x10201e9

.field public static final am_pm_spinner:I = 0x10201ea

.field public static final ampm_layout:I = 0x10201eb

.field public static final animator:I = 0x10201ed

.field public static final app_name_divider:I = 0x10201f0

.field public static final app_name_text:I = 0x10201f1

.field public static final app_ops:I = 0x10201f2

.field public static final ask_checkbox:I = 0x10201f5

.field public static final authtoken_type:I = 0x10201fa

.field public static final autofill:I = 0x1020043

.field public static final autofill_dataset_footer:I = 0x10201fd

.field public static final autofill_dataset_header:I = 0x10201fe

.field public static final autofill_dataset_icon:I = 0x10201ff

.field public static final autofill_dataset_list:I = 0x1020200

.field public static final autofill_dataset_picker:I = 0x1020201

.field public static final autofill_dataset_title:I = 0x1020202

.field public static final autofill_dialog_container:I = 0x1020203

.field public static final autofill_dialog_header:I = 0x1020204

.field public static final autofill_dialog_list:I = 0x1020205

.field public static final autofill_dialog_no:I = 0x1020206

.field public static final autofill_dialog_picker:I = 0x1020207

.field public static final autofill_dialog_yes:I = 0x1020208

.field public static final autofill_save_custom_subtitle:I = 0x102020a

.field public static final autofill_save_icon:I = 0x102020b

.field public static final autofill_save_no:I = 0x102020c

.field public static final autofill_save_title:I = 0x102020d

.field public static final autofill_save_yes:I = 0x102020e

.field public static final autofill_service_icon:I = 0x102020f

.field public static final back_button:I = 0x1020210

.field public static final background:I = 0x1020000

.field public static final big_picture:I = 0x1020217

.field public static final big_text:I = 0x1020218

.field public static final body:I = 0x102021a

.field public static final bottom_caption:I = 0x102021e

.field public static final breadcrumb_section:I = 0x1020221

.field public static final bubble_button:I = 0x1020222

.field public static final button0:I = 0x1020226

.field public static final button1:I = 0x1020019

.field public static final button2:I = 0x102001a

.field public static final button3:I = 0x102001b

.field public static final button4:I = 0x1020227

.field public static final button5:I = 0x1020228

.field public static final button6:I = 0x1020229

.field public static final button7:I = 0x102022a

.field public static final buttonPanel:I = 0x102022b

.field public static final button_always:I = 0x102022c

.field public static final button_bar:I = 0x102022d

.field public static final button_bar_container:I = 0x102022e

.field public static final button_once:I = 0x102022f

.field public static final button_open:I = 0x1020230

.field public static final by_common:I = 0x1020232

.field public static final by_org:I = 0x1020234

.field public static final by_org_unit:I = 0x1020236

.field public static final calendar_view:I = 0x1020239

.field public static final camera:I = 0x102023a

.field public static final cancel:I = 0x102023b

.field public static final candidatesArea:I = 0x102001d

.field public static final characterPicker:I = 0x1020244

.field public static final checkbox:I = 0x1020001

.field public static final chooser_action_row:I = 0x1020249

.field public static final chooser_copy_button:I = 0x102024a

.field public static final chooser_edit_button:I = 0x102024b

.field public static final chooser_header:I = 0x102024c

.field public static final chooser_nearby_button:I = 0x102024d

.field public static final chooser_row_text_option:I = 0x102024e

.field public static final chronometer:I = 0x102024f

.field public static final clearDefaultHint:I = 0x1020251

.field public static final clip_children_set_tag:I = 0x1020253

.field public static final clip_children_tag:I = 0x1020254

.field public static final clip_to_padding_tag:I = 0x1020256

.field public static final closeButton:I = 0x1020027

.field public static final close_window:I = 0x1020259

.field public static final compat_checkbox:I = 0x1020261

.field public static final content:I = 0x1020002

.field public static final contentPanel:I = 0x1020266

.field public static final content_preview_container:I = 0x1020267

.field public static final content_preview_file_area:I = 0x1020268

.field public static final content_preview_file_icon:I = 0x1020269

.field public static final content_preview_file_layout:I = 0x102026a

.field public static final content_preview_file_thumbnail:I = 0x102026b

.field public static final content_preview_filename:I = 0x102026c

.field public static final content_preview_image_1_large:I = 0x102026d

.field public static final content_preview_image_2_large:I = 0x102026e

.field public static final content_preview_image_2_small:I = 0x102026f

.field public static final content_preview_image_3_small:I = 0x1020270

.field public static final content_preview_image_area:I = 0x1020271

.field public static final content_preview_text:I = 0x1020272

.field public static final content_preview_text_area:I = 0x1020273

.field public static final content_preview_text_layout:I = 0x1020274

.field public static final content_preview_thumbnail:I = 0x1020275

.field public static final content_preview_title:I = 0x1020276

.field public static final content_preview_title_layout:I = 0x1020277

.field public static final conversation_face_pile:I = 0x1020279

.field public static final conversation_face_pile_bottom:I = 0x102027a

.field public static final conversation_face_pile_bottom_background:I = 0x102027b

.field public static final conversation_face_pile_top:I = 0x102027c

.field public static final conversation_header:I = 0x102027d

.field public static final conversation_icon:I = 0x102027e

.field public static final conversation_icon_badge:I = 0x102027f

.field public static final conversation_icon_badge_bg:I = 0x1020280

.field public static final conversation_icon_badge_ring:I = 0x1020281

.field public static final conversation_icon_container:I = 0x1020282

.field public static final conversation_image_message_container:I = 0x1020283

.field public static final conversation_text:I = 0x1020284

.field public static final copy:I = 0x1020021

.field public static final copyUrl:I = 0x1020023

.field public static final cross_task_transition:I = 0x1020286

.field public static final current_scene:I = 0x1020289

.field public static final custom:I = 0x102002b

.field public static final customPanel:I = 0x102028a

.field public static final cut:I = 0x1020020

.field public static final date:I = 0x102028e

.field public static final datePicker:I = 0x102028f

.field public static final date_picker_day_picker:I = 0x1020290

.field public static final date_picker_header:I = 0x1020291

.field public static final date_picker_header_date:I = 0x1020292

.field public static final date_picker_header_year:I = 0x1020293

.field public static final date_picker_year_picker:I = 0x1020294

.field public static final day:I = 0x1020296

.field public static final day_names:I = 0x1020297

.field public static final day_picker_view_pager:I = 0x1020298

.field public static final decor_content_parent:I = 0x102029a

.field public static final decor_pc_app_name:I = 0x102029b

.field public static final decor_pc_back:I = 0x102029c

.field public static final decor_pc_default:I = 0x102029d

.field public static final decor_pc_full:I = 0x102029e

.field public static final decor_pc_min:I = 0x102029f

.field public static final decrement:I = 0x10202a0

.field public static final default_activity_button:I = 0x10202a3

.field public static final deleteButton:I = 0x10202a5

.field public static final deny_button:I = 0x10202a7

.field public static final description:I = 0x10202a8

.field public static final divider:I = 0x10202af

.field public static final edit:I = 0x1020003

.field public static final edit_query:I = 0x10202b3

.field public static final edittext_container:I = 0x10202b5

.field public static final eight:I = 0x10202b6

.field public static final empty:I = 0x1020004

.field public static final expand_activities_button:I = 0x10202bd

.field public static final expand_button:I = 0x10202be

.field public static final expand_button_and_content_container:I = 0x10202bf

.field public static final expand_button_container:I = 0x10202c0

.field public static final expand_button_icon:I = 0x10202c1

.field public static final expand_button_number:I = 0x10202c2

.field public static final expand_button_pill:I = 0x10202c3

.field public static final expand_button_touch_container:I = 0x10202c4

.field public static final expires_on:I = 0x10202c6

.field public static final extractArea:I = 0x102001c

.field public static final feedback:I = 0x10202cb

.field public static final ffwd:I = 0x10202d2

.field public static final fillInIntent:I = 0x10202d4

.field public static final find:I = 0x10202d8

.field public static final find_next:I = 0x10202d9

.field public static final find_prev:I = 0x10202da

.field public static final five:I = 0x10202e4

.field public static final floating_toolbar_menu_item_image:I = 0x10202fe

.field public static final floating_toolbar_menu_item_text:I = 0x1020300

.field public static final four:I = 0x1020303

.field public static final fullscreenArea:I = 0x1020308

.field public static final group_divider:I = 0x1020310

.field public static final group_message_container:I = 0x1020311

.field public static final hard_keyboard_section:I = 0x1020314

.field public static final hard_keyboard_switch:I = 0x1020315

.field public static final header_text:I = 0x1020319

.field public static final header_text_divider:I = 0x102031a

.field public static final header_text_secondary:I = 0x102031b

.field public static final header_text_secondary_divider:I = 0x102031c

.field public static final headers:I = 0x102031d

.field public static final hint:I = 0x1020005

.field public static final home:I = 0x102002c

.field public static final hour:I = 0x1020328

.field public static final hours:I = 0x1020329

.field public static final icon:I = 0x1020006

.field public static final icon1:I = 0x1020007

.field public static final icon2:I = 0x1020008

.field public static final icon_frame:I = 0x102003e

.field public static final icon_menu_presenter:I = 0x102032c

.field public static final image:I = 0x1020331

.field public static final inbox_text0:I = 0x1020339

.field public static final inbox_text1:I = 0x102033a

.field public static final inbox_text2:I = 0x102033b

.field public static final inbox_text3:I = 0x102033c

.field public static final inbox_text4:I = 0x102033d

.field public static final inbox_text5:I = 0x102033e

.field public static final inbox_text6:I = 0x102033f

.field public static final increment:I = 0x1020342

.field public static final input:I = 0x1020009

.field public static final inputArea:I = 0x102001e

.field public static final inputExtractAccessories:I = 0x102005a

.field public static final inputExtractAction:I = 0x1020059

.field public static final inputExtractEditText:I = 0x1020025

.field public static final input_header:I = 0x1020347

.field public static final input_hour:I = 0x1020348

.field public static final input_method_nav_back:I = 0x1020349

.field public static final input_method_nav_buttons:I = 0x102034a

.field public static final input_method_nav_center_group:I = 0x102034b

.field public static final input_method_nav_ends_group:I = 0x102034c

.field public static final input_method_nav_home_handle:I = 0x102034d

.field public static final input_method_nav_horizontal:I = 0x102034e

.field public static final input_method_nav_ime_switcher:I = 0x102034f

.field public static final input_method_nav_inflater:I = 0x1020350

.field public static final input_minute:I = 0x1020352

.field public static final input_mode:I = 0x1020353

.field public static final input_separator:I = 0x1020354

.field public static final insertion_handle:I = 0x1020355

.field public static final internalEmpty:I = 0x1020360

.field public static final issued_on:I = 0x1020364

.field public static final item_touch_helper_previous_elevation:I = 0x1020368

.field public static final keyboard:I = 0x102036a

.field public static final keyboardView:I = 0x1020026
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final label_error:I = 0x1020371

.field public static final label_hour:I = 0x1020372

.field public static final label_minute:I = 0x1020373

.field public static final language_picker_header:I = 0x1020375

.field public static final language_picker_item:I = 0x1020376

.field public static final leftSpacer:I = 0x102037e

.field public static final left_icon:I = 0x102037f

.field public static final line1:I = 0x1020382

.field public static final list:I = 0x102000a

.field public static final listContainer:I = 0x1020384

.field public static final list_container:I = 0x102003f

.field public static final list_footer:I = 0x1020386

.field public static final list_item:I = 0x1020387

.field public static final list_menu_presenter:I = 0x1020388

.field public static final locale:I = 0x102038a

.field public static final locale_search_menu:I = 0x102038b

.field public static final lock_screen:I = 0x102038d

.field public static final log_access_dialog_allow_button:I = 0x102038f

.field public static final log_access_dialog_deny_button:I = 0x1020391

.field public static final log_access_dialog_title:I = 0x1020392

.field public static final mask:I = 0x102002e

.field public static final matches:I = 0x102039d

.field public static final maximize_window:I = 0x10203a0

.field public static final media_actions:I = 0x10203a5

.field public static final media_route_control_frame:I = 0x10203a6

.field public static final media_route_extended_settings_button:I = 0x10203a7

.field public static final media_route_list:I = 0x10203a8

.field public static final media_route_progress_bar:I = 0x10203a9

.field public static final media_route_volume_layout:I = 0x10203aa

.field public static final media_route_volume_slider:I = 0x10203ab

.field public static final mediacontroller_progress:I = 0x10203ac

.field public static final message:I = 0x102000b

.field public static final message_icon:I = 0x10203ae

.field public static final message_icon_container:I = 0x10203af

.field public static final message_name:I = 0x10203b0

.field public static final message_text:I = 0x10203b1

.field public static final messaging_group_content_container:I = 0x10203b2

.field public static final messaging_group_icon_container:I = 0x10203b3

.field public static final messaging_group_sending_progress:I = 0x10203b4

.field public static final messaging_group_sending_progress_container:I = 0x10203b5

.field public static final mic:I = 0x10203b6

.field public static final minute:I = 0x10203bb

.field public static final minutes:I = 0x10203bc

.field public static final miui_bottom_area:I = 0x10203bf

.field public static final mode_normal:I = 0x10203c5

.field public static final month:I = 0x10203c8

.field public static final month_name:I = 0x10203c9

.field public static final month_view:I = 0x10203ca

.field public static final move_region:I = 0x10203cc

.field public static final navigationBarBackground:I = 0x1020030

.field public static final new_app_action:I = 0x10203d7

.field public static final new_app_description:I = 0x10203d8

.field public static final new_app_icon:I = 0x10203d9

.field public static final next:I = 0x10203db

.field public static final next_button:I = 0x10203dc

.field public static final nine:I = 0x10203dd

.field public static final no_applications_message:I = 0x10203e1

.field public static final no_permissions:I = 0x10203e3

.field public static final notification_action_index_tag:I = 0x10203ec

.field public static final notification_action_list_margin_target:I = 0x10203ed

.field public static final notification_custom_view_index_tag:I = 0x10203ee

.field public static final notification_header:I = 0x10203ef

.field public static final notification_headerless_view_column:I = 0x10203f0

.field public static final notification_main_column:I = 0x10203f2

.field public static final notification_material_reply_container:I = 0x10203f3

.field public static final notification_material_reply_progress:I = 0x10203f4

.field public static final notification_material_reply_text_1:I = 0x10203f5

.field public static final notification_material_reply_text_1_container:I = 0x10203f6

.field public static final notification_material_reply_text_2:I = 0x10203f7

.field public static final notification_material_reply_text_3:I = 0x10203f8

.field public static final notification_media_content:I = 0x10203f9

.field public static final notification_messaging:I = 0x10203fa

.field public static final notification_top_line:I = 0x10203ff

.field public static final numberpicker_input:I = 0x1020405

.field public static final ok:I = 0x1020408

.field public static final old_app_action:I = 0x1020409

.field public static final old_app_icon:I = 0x102040a

.field public static final one:I = 0x102040c

.field public static final open_cross_profile:I = 0x102040f

.field public static final option1:I = 0x1020411

.field public static final option2:I = 0x1020412

.field public static final option3:I = 0x1020413

.field public static final original_app_icon:I = 0x1020415

.field public static final original_message:I = 0x1020416

.field public static final overflow:I = 0x102041a

.field public static final overlay:I = 0x102041c

.field public static final overlay_display_window_texture:I = 0x102041d

.field public static final overlay_display_window_title:I = 0x102041e

.field public static final package_label:I = 0x1020420

.field public static final packages_list:I = 0x1020421

.field public static final parentMatrix:I = 0x1020424

.field public static final parentPanel:I = 0x1020425

.field public static final paste:I = 0x1020022

.field public static final pasteAsPlainText:I = 0x1020031

.field public static final pause:I = 0x1020427

.field public static final pending_intent_tag:I = 0x1020428

.field public static final perm_icon:I = 0x1020429

.field public static final perm_name:I = 0x102042c

.field public static final permission_group:I = 0x102042d

.field public static final permission_list:I = 0x102042f

.field public static final perms_list:I = 0x1020430

.field public static final phishing_alert:I = 0x1020435

.field public static final pickers:I = 0x1020439

.field public static final pin_cancel_button:I = 0x102043a

.field public static final pin_confirm_text:I = 0x102043b

.field public static final pin_error_message:I = 0x102043c

.field public static final pin_new_text:I = 0x102043e

.field public static final pin_ok_button:I = 0x102043f

.field public static final pin_text:I = 0x1020440

.field public static final placeholder:I = 0x1020441

.field public static final pm_label:I = 0x1020442

.field public static final prefs:I = 0x1020448

.field public static final prefs_container:I = 0x1020449

.field public static final prefs_frame:I = 0x102044a

.field public static final prev:I = 0x102044d

.field public static final primary:I = 0x102000c

.field public static final profile_badge:I = 0x1020450

.field public static final profile_button:I = 0x1020451

.field public static final profile_pager:I = 0x1020452

.field public static final profile_tabhost:I = 0x1020453

.field public static final progress:I = 0x102000d

.field public static final progressContainer:I = 0x1020454

.field public static final progress_circular:I = 0x1020455

.field public static final progress_horizontal:I = 0x1020456

.field public static final progress_number:I = 0x1020457

.field public static final progress_percent:I = 0x1020458

.field public static final radial_picker:I = 0x102045d

.field public static final radio:I = 0x102045e

.field public static final reask_hint:I = 0x1020461

.field public static final redo:I = 0x1020033

.field public static final remote_checked_change_listener_tag:I = 0x1020465

.field public static final remote_input:I = 0x1020466

.field public static final remote_input_progress:I = 0x1020467

.field public static final remote_input_send:I = 0x1020468

.field public static final remote_input_tag:I = 0x1020469

.field public static final remote_views_next_child:I = 0x102046b

.field public static final remote_views_override_id:I = 0x102046c

.field public static final remote_views_stable_id:I = 0x102046d

.field public static final replaceText:I = 0x1020034

.field public static final replace_app_icon:I = 0x1020470

.field public static final replace_message:I = 0x1020471

.field public static final resolver_button_bar_divider:I = 0x1020472

.field public static final resolver_empty_state:I = 0x1020473

.field public static final resolver_empty_state_button:I = 0x1020474

.field public static final resolver_empty_state_container:I = 0x1020475

.field public static final resolver_empty_state_icon:I = 0x1020476

.field public static final resolver_empty_state_progress:I = 0x1020477

.field public static final resolver_empty_state_subtitle:I = 0x1020478

.field public static final resolver_empty_state_title:I = 0x1020479

.field public static final resolver_list:I = 0x102047a

.field public static final rew:I = 0x1020483

.field public static final rightSpacer:I = 0x1020485

.field public static final right_icon:I = 0x1020487

.field public static final rowTypeId:I = 0x102048f

.field public static final scene_layoutid_cache:I = 0x1020493

.field public static final scrollView:I = 0x1020498

.field public static final search_app_icon:I = 0x102049b

.field public static final search_badge:I = 0x102049c

.field public static final search_bar:I = 0x102049d

.field public static final search_button:I = 0x102049e

.field public static final search_close_btn:I = 0x102049f

.field public static final search_edit_frame:I = 0x10204a0

.field public static final search_go_btn:I = 0x10204a1

.field public static final search_mag_icon:I = 0x10204a2

.field public static final search_plate:I = 0x10204a3

.field public static final search_src_text:I = 0x10204a4

.field public static final search_view:I = 0x10204a5

.field public static final search_voice_btn:I = 0x10204a6

.field public static final secondaryProgress:I = 0x102000f

.field public static final seekbar:I = 0x10204a9

.field public static final selectAll:I = 0x102001f

.field public static final selectTextMode:I = 0x102002d

.field public static final select_all:I = 0x10204aa

.field public static final selectedIcon:I = 0x102000e

.field public static final selection_end_handle:I = 0x10204ac

.field public static final selection_start_handle:I = 0x10204ad

.field public static final separator:I = 0x10204b2

.field public static final serial_number:I = 0x10204b5

.field public static final seven:I = 0x10204b9

.field public static final sha1_fingerprint:I = 0x10204ba

.field public static final sha256_fingerprint:I = 0x10204bc

.field public static final share:I = 0x10204be

.field public static final shareText:I = 0x1020035

.field public static final shortcut:I = 0x10204c0

.field public static final six:I = 0x10204d2

.field public static final skip_button:I = 0x10204d3

.field public static final smart_reply_container:I = 0x10204d7

.field public static final sms_short_code_confirm_message:I = 0x10204d9

.field public static final sms_short_code_detail_layout:I = 0x10204da

.field public static final sms_short_code_detail_message:I = 0x10204db

.field public static final sms_short_code_remember_choice_checkbox:I = 0x10204dc

.field public static final sms_short_code_remember_undo_instruction:I = 0x10204de

.field public static final snooze_button:I = 0x10204df

.field public static final spacer:I = 0x10204e3

.field public static final splashscreen_branding_view:I = 0x10204e9

.field public static final splashscreen_icon_view:I = 0x10204ea

.field public static final split_action_bar:I = 0x10204ec

.field public static final startSelectingText:I = 0x1020028

.field public static final status:I = 0x10204fa

.field public static final statusBarBackground:I = 0x102002f

.field public static final status_bar_latest_event_content:I = 0x10204fb

.field public static final stopSelectingText:I = 0x1020029

.field public static final stub:I = 0x10204fe

.field public static final submenuarrow:I = 0x1020504

.field public static final submit_area:I = 0x1020505

.field public static final suggestionContainer:I = 0x1020506

.field public static final suggestionWindowContainer:I = 0x1020507

.field public static final summary:I = 0x1020010

.field public static final switchInputMethod:I = 0x1020024

.field public static final switch_new:I = 0x102050a

.field public static final switch_old:I = 0x102050b

.field public static final switch_widget:I = 0x1020040

.field public static final system_locale_subtitle:I = 0x102050f

.field public static final tabcontent:I = 0x1020011

.field public static final tabhost:I = 0x1020012

.field public static final tabs:I = 0x1020013

.field public static final tag_alpha_animator:I = 0x1020512

.field public static final tag_is_first_layout:I = 0x1020513

.field public static final tag_keep_when_showing_left_icon:I = 0x1020514

.field public static final tag_layout_top:I = 0x1020515

.field public static final tag_margin_end_when_icon_gone:I = 0x1020516

.field public static final tag_margin_end_when_icon_visible:I = 0x1020517

.field public static final tag_top_animator:I = 0x1020518

.field public static final tag_top_override:I = 0x1020519

.field public static final tag_uses_right_icon_drawable:I = 0x102051a

.field public static final text:I = 0x102051b

.field public static final text1:I = 0x1020014

.field public static final text2:I = 0x1020015

.field public static final textAssist:I = 0x1020041

.field public static final textSpacerNoButtons:I = 0x1020530

.field public static final textSpacerNoTitle:I = 0x1020531

.field public static final three:I = 0x1020539

.field public static final time:I = 0x102053a

.field public static final timePicker:I = 0x102053b

.field public static final timePickerLayout:I = 0x102053c

.field public static final time_current:I = 0x102053d

.field public static final time_divider:I = 0x102053e

.field public static final time_header:I = 0x102053f

.field public static final title:I = 0x1020016

.field public static final titleDivider:I = 0x1020541

.field public static final titleDividerNoCustom:I = 0x1020542

.field public static final titleDividerTop:I = 0x1020543

.field public static final title_container:I = 0x1020544

.field public static final title_template:I = 0x1020546

.field public static final to_common:I = 0x1020547

.field public static final to_org:I = 0x1020549

.field public static final to_org_unit:I = 0x102054b

.field public static final toggle:I = 0x1020017

.field public static final toggle_mode:I = 0x102054e

.field public static final topPanel:I = 0x1020550

.field public static final top_caption:I = 0x1020551

.field public static final transitionPosition:I = 0x1020558

.field public static final transitionTransform:I = 0x1020559

.field public static final transition_overlay_view_tag:I = 0x102055a

.field public static final two:I = 0x102055e

.field public static final undo:I = 0x1020032

.field public static final up:I = 0x1020581

.field public static final use_same_profile_browser:I = 0x1020583

.field public static final value:I = 0x102058a

.field public static final verification_divider:I = 0x102058c

.field public static final verification_icon:I = 0x102058d

.field public static final verification_text:I = 0x102058e

.field public static final websearch:I = 0x102059b

.field public static final widget_frame:I = 0x1020018

.field public static final work_widget_app_icon:I = 0x10205a4

.field public static final work_widget_badge_icon:I = 0x10205a5

.field public static final year:I = 0x10205ab

.field public static final zero:I = 0x10205ae

.field public static final zoomControls:I = 0x10205af

.field public static final zoomIn:I = 0x10205b0

.field public static final zoomMagnify:I = 0x10205b1

.field public static final zoomOut:I = 0x10205b2


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
