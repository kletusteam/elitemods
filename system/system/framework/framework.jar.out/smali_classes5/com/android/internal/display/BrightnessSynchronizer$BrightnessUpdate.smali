.class public Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/display/BrightnessSynchronizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "BrightnessUpdate"
.end annotation


# static fields
.field private static final STATE_COMPLETED:I = 0x3

.field private static final STATE_NOT_STARTED:I = 0x1

.field private static final STATE_RUNNING:I = 0x2

.field static final TYPE_FLOAT:I = 0x2

.field static final TYPE_INT:I = 0x1


# instance fields
.field private final mBrightness:F

.field private mConfirmedTypes:I

.field private mId:I

.field private final mSourceType:I

.field private mState:I

.field private mTimeUpdated:J

.field private mUpdatedTypes:I

.field final synthetic this$0:Lcom/android/internal/display/BrightnessSynchronizer;


# direct methods
.method constructor <init>(Lcom/android/internal/display/BrightnessSynchronizer;IF)V
    .locals 2

    iput-object p1, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->this$0:Lcom/android/internal/display/BrightnessSynchronizer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/android/internal/display/BrightnessSynchronizer;->-$$Nest$sfgetsBrightnessUpdateCount()I

    move-result v0

    add-int/lit8 v1, v0, 0x1

    invoke-static {v1}, Lcom/android/internal/display/BrightnessSynchronizer;->-$$Nest$sfputsBrightnessUpdateCount(I)V

    iput v0, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mId:I

    iput p2, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mSourceType:I

    iput p3, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mBrightness:F

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mTimeUpdated:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mUpdatedTypes:I

    iput v0, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mConfirmedTypes:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mState:I

    return-void
.end method

.method private getBrightnessAsFloat()F
    .locals 2

    iget v0, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mSourceType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mBrightness:F

    return v0

    :cond_0
    iget v0, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mBrightness:F

    float-to-int v0, v0

    invoke-static {v0}, Lcom/android/internal/display/BrightnessSynchronizer;->brightnessIntToFloatForLowLevel(I)F

    move-result v0

    return v0
.end method

.method private getBrightnessAsInt()I
    .locals 2

    iget v0, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mSourceType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mBrightness:F

    float-to-int v0, v0

    return v0

    :cond_0
    iget v0, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mBrightness:F

    invoke-static {v0}, Lcom/android/internal/display/BrightnessSynchronizer;->brightnessFloatToIntRangeForLowLevel(F)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method private toStringLabel(IF)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "(i)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "(f)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, ""

    :goto_0
    return-object v0
.end method


# virtual methods
.method isCompleted()Z
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    const/4 v0, 0x1

    goto/32 :goto_5

    nop

    :goto_1
    const/4 v0, 0x0

    :goto_2
    goto/32 :goto_8

    nop

    :goto_3
    iget v0, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mState:I

    goto/32 :goto_4

    nop

    :goto_4
    const/4 v1, 0x3

    goto/32 :goto_7

    nop

    :goto_5
    goto :goto_2

    :goto_6
    goto/32 :goto_1

    nop

    :goto_7
    if-eq v0, v1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_0

    nop

    :goto_8
    return v0
.end method

.method isRunning()Z
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    const/4 v0, 0x0

    :goto_1
    goto/32 :goto_7

    nop

    :goto_2
    goto :goto_1

    :goto_3
    goto/32 :goto_0

    nop

    :goto_4
    const/4 v0, 0x1

    goto/32 :goto_2

    nop

    :goto_5
    iget v0, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mState:I

    goto/32 :goto_8

    nop

    :goto_6
    if-eq v0, v1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_4

    nop

    :goto_7
    return v0

    :goto_8
    const/4 v1, 0x2

    goto/32 :goto_6

    nop
.end method

.method madeUpdates()Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mUpdatedTypes:I

    goto/32 :goto_5

    nop

    :goto_1
    const/4 v0, 0x1

    goto/32 :goto_6

    nop

    :goto_2
    return v0

    :goto_3
    const/4 v0, 0x0

    :goto_4
    goto/32 :goto_2

    nop

    :goto_5
    if-nez v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_1

    nop

    :goto_6
    goto :goto_4

    :goto_7
    goto/32 :goto_3

    nop
.end method

.method swallowUpdate(IF)Z
    .locals 5

    goto/32 :goto_25

    nop

    :goto_0
    iget v1, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mConfirmedTypes:I

    goto/32 :goto_2b

    nop

    :goto_1
    return v1

    :goto_2
    invoke-static {v0, p2}, Lcom/android/internal/display/BrightnessSynchronizer;->floatEquals(FF)Z

    move-result v0

    goto/32 :goto_2d

    nop

    :goto_3
    if-eqz v0, :cond_0

    goto/32 :goto_28

    :cond_0
    goto/32 :goto_24

    nop

    :goto_4
    goto/16 :goto_33

    :goto_5
    goto/32 :goto_32

    nop

    :goto_6
    if-eq v0, p1, :cond_1

    goto/32 :goto_14

    :cond_1
    goto/32 :goto_1c

    nop

    :goto_7
    if-eq p1, v2, :cond_2

    goto/32 :goto_a

    :cond_2
    goto/32 :goto_1d

    nop

    :goto_8
    const/4 v0, 0x2

    goto/32 :goto_34

    nop

    :goto_9
    goto :goto_19

    :goto_a
    goto/32 :goto_18

    nop

    :goto_b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_10

    nop

    :goto_c
    and-int/2addr v0, p1

    goto/32 :goto_1e

    nop

    :goto_d
    move v0, v2

    goto/32 :goto_4

    nop

    :goto_e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_12

    nop

    :goto_f
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_e

    nop

    :goto_10
    const-string v4, "Swallowing update of "

    goto/32 :goto_1b

    nop

    :goto_11
    float-to-int v4, p2

    goto/32 :goto_15

    nop

    :goto_12
    const-string v4, "BrightnessSynchronizer"

    goto/32 :goto_31

    nop

    :goto_13
    return v2

    :goto_14
    goto/32 :goto_1

    nop

    :goto_15
    if-eq v3, v4, :cond_3

    goto/32 :goto_a

    :cond_3
    goto/32 :goto_30

    nop

    :goto_16
    goto :goto_28

    :goto_17
    goto/32 :goto_27

    nop

    :goto_18
    move v3, v1

    :goto_19
    goto/32 :goto_3

    nop

    :goto_1a
    and-int/2addr v0, p1

    goto/32 :goto_2a

    nop

    :goto_1b
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_26

    nop

    :goto_1c
    iget v0, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mConfirmedTypes:I

    goto/32 :goto_c

    nop

    :goto_1d
    invoke-direct {p0}, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->getBrightnessAsInt()I

    move-result v3

    goto/32 :goto_11

    nop

    :goto_1e
    if-nez v0, :cond_4

    goto/32 :goto_21

    :cond_4
    goto/32 :goto_20

    nop

    :goto_1f
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_23

    nop

    :goto_20
    goto :goto_14

    :goto_21
    goto/32 :goto_8

    nop

    :goto_22
    invoke-direct {p0}, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->getBrightnessAsFloat()F

    move-result v0

    goto/32 :goto_2

    nop

    :goto_23
    const-string v4, " by update: "

    goto/32 :goto_29

    nop

    :goto_24
    if-nez v3, :cond_5

    goto/32 :goto_17

    :cond_5
    goto/32 :goto_16

    nop

    :goto_25
    iget v0, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mUpdatedTypes:I

    goto/32 :goto_1a

    nop

    :goto_26
    invoke-direct {p0, p1, p2}, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->toStringLabel(IF)Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_1f

    nop

    :goto_27
    return v1

    :goto_28
    goto/32 :goto_0

    nop

    :goto_29
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_f

    nop

    :goto_2a
    const/4 v1, 0x0

    goto/32 :goto_6

    nop

    :goto_2b
    or-int/2addr v1, p1

    goto/32 :goto_2e

    nop

    :goto_2c
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_b

    nop

    :goto_2d
    if-nez v0, :cond_6

    goto/32 :goto_5

    :cond_6
    goto/32 :goto_d

    nop

    :goto_2e
    iput v1, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mConfirmedTypes:I

    goto/32 :goto_2c

    nop

    :goto_2f
    if-eq p1, v0, :cond_7

    goto/32 :goto_5

    :cond_7
    goto/32 :goto_22

    nop

    :goto_30
    move v3, v2

    goto/32 :goto_9

    nop

    :goto_31
    invoke-static {v4, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_13

    nop

    :goto_32
    move v0, v1

    :goto_33
    goto/32 :goto_7

    nop

    :goto_34
    const/4 v2, 0x1

    goto/32 :goto_2f

    nop
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "{["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mSourceType:I

    iget v2, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mBrightness:F

    invoke-direct {p0, v1, v2}, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->toStringLabel(IF)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mUpdatedTypes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mUpdatedTypes:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mConfirmedTypes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mConfirmedTypes:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mTimeUpdated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mTimeUpdated:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method update()V
    .locals 9

    goto/32 :goto_2a

    nop

    :goto_0
    invoke-direct {p0, v7, v8}, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->toStringLabel(IF)Ljava/lang/String;

    move-result-object v7

    goto/32 :goto_23

    nop

    :goto_1
    const/4 v6, -0x2

    goto/32 :goto_44

    nop

    :goto_2
    iput v5, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mUpdatedTypes:I

    :goto_3
    goto/32 :goto_48

    nop

    :goto_4
    invoke-static {v5}, Lcom/android/internal/display/BrightnessSynchronizer;->-$$Nest$fgetmLatestIntBrightness(Lcom/android/internal/display/BrightnessSynchronizer;)I

    move-result v5

    goto/32 :goto_41

    nop

    :goto_5
    invoke-interface {v7}, Lcom/android/internal/display/BrightnessSynchronizer$Clock;->uptimeMillis()J

    move-result-wide v7

    goto/32 :goto_2f

    nop

    :goto_6
    invoke-static {v5}, Lcom/android/internal/display/BrightnessSynchronizer;->-$$Nest$fgetmContext(Lcom/android/internal/display/BrightnessSynchronizer;)Landroid/content/Context;

    move-result-object v5

    goto/32 :goto_1a

    nop

    :goto_7
    cmp-long v0, v3, v0

    goto/32 :goto_4c

    nop

    :goto_8
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_37

    nop

    :goto_9
    iget-object v6, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->this$0:Lcom/android/internal/display/BrightnessSynchronizer;

    goto/32 :goto_42

    nop

    :goto_a
    if-eq v0, v4, :cond_0

    goto/32 :goto_51

    :cond_0
    goto/32 :goto_f

    nop

    :goto_b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_39

    nop

    :goto_c
    or-int/2addr v6, v3

    goto/32 :goto_5b

    nop

    :goto_d
    iget-object v0, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->this$0:Lcom/android/internal/display/BrightnessSynchronizer;

    goto/32 :goto_5a

    nop

    :goto_e
    invoke-direct {p0}, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->getBrightnessAsInt()I

    move-result v0

    goto/32 :goto_33

    nop

    :goto_f
    iput v3, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mState:I

    goto/32 :goto_e

    nop

    :goto_10
    if-eq v0, v3, :cond_1

    goto/32 :goto_2d

    :cond_1
    goto/32 :goto_3e

    nop

    :goto_11
    if-eqz v6, :cond_2

    goto/32 :goto_5c

    :cond_2
    goto/32 :goto_21

    nop

    :goto_12
    iget v6, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mUpdatedTypes:I

    goto/32 :goto_58

    nop

    :goto_13
    const-wide/16 v1, 0xc8

    goto/32 :goto_57

    nop

    :goto_14
    invoke-virtual {v6, v4, v7, v8}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    :goto_15
    goto/32 :goto_5f

    nop

    :goto_16
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_45

    nop

    :goto_17
    invoke-static {v6, v5}, Lcom/android/internal/display/BrightnessSynchronizer;->-$$Nest$fputmLatestFloatBrightness(Lcom/android/internal/display/BrightnessSynchronizer;F)V

    goto/32 :goto_60

    nop

    :goto_18
    iget-object v5, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->this$0:Lcom/android/internal/display/BrightnessSynchronizer;

    goto/32 :goto_1e

    nop

    :goto_19
    return-void

    :goto_1a
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    goto/32 :goto_1

    nop

    :goto_1b
    invoke-static {v7}, Lcom/android/internal/display/BrightnessSynchronizer;->-$$Nest$fgetmClock(Lcom/android/internal/display/BrightnessSynchronizer;)Lcom/android/internal/display/BrightnessSynchronizer$Clock;

    move-result-object v7

    goto/32 :goto_5

    nop

    :goto_1c
    const-string v7, "] New Update "

    goto/32 :goto_24

    nop

    :goto_1d
    iget v5, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mUpdatedTypes:I

    goto/32 :goto_2e

    nop

    :goto_1e
    invoke-static {v5, v0}, Lcom/android/internal/display/BrightnessSynchronizer;->-$$Nest$fputmLatestIntBrightness(Lcom/android/internal/display/BrightnessSynchronizer;I)V

    goto/32 :goto_1d

    nop

    :goto_1f
    invoke-static {v4}, Lcom/android/internal/display/BrightnessSynchronizer;->-$$Nest$fgetmClock(Lcom/android/internal/display/BrightnessSynchronizer;)Lcom/android/internal/display/BrightnessSynchronizer$Clock;

    move-result-object v4

    goto/32 :goto_4f

    nop

    :goto_20
    if-ne v0, v3, :cond_3

    goto/32 :goto_4d

    :cond_3
    goto/32 :goto_3c

    nop

    :goto_21
    iget-object v6, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->this$0:Lcom/android/internal/display/BrightnessSynchronizer;

    goto/32 :goto_35

    nop

    :goto_22
    const/4 v0, 0x3

    goto/32 :goto_2c

    nop

    :goto_23
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_54

    nop

    :goto_24
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_55

    nop

    :goto_25
    const-string v7, "["

    goto/32 :goto_8

    nop

    :goto_26
    invoke-static {v7, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_9

    nop

    :goto_27
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_59

    nop

    :goto_28
    iget v3, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mUpdatedTypes:I

    goto/32 :goto_20

    nop

    :goto_29
    const/4 v4, 0x1

    goto/32 :goto_a

    nop

    :goto_2a
    iget v0, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mState:I

    goto/32 :goto_13

    nop

    :goto_2b
    and-int/2addr v7, v3

    goto/32 :goto_3f

    nop

    :goto_2c
    iput v0, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mState:I

    :goto_2d
    goto/32 :goto_19

    nop

    :goto_2e
    or-int/2addr v5, v4

    goto/32 :goto_2

    nop

    :goto_2f
    add-long/2addr v7, v1

    goto/32 :goto_14

    nop

    :goto_30
    invoke-static {v6, v5}, Lcom/android/internal/display/BrightnessSynchronizer;->floatEquals(FF)Z

    move-result v6

    goto/32 :goto_11

    nop

    :goto_31
    invoke-virtual {v6, v7, v5}, Landroid/hardware/display/DisplayManager;->setBrightness(IF)V

    goto/32 :goto_5e

    nop

    :goto_32
    new-instance v6, Ljava/lang/StringBuilder;

    goto/32 :goto_40

    nop

    :goto_33
    iget-object v5, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->this$0:Lcom/android/internal/display/BrightnessSynchronizer;

    goto/32 :goto_4

    nop

    :goto_34
    invoke-static {v6}, Lcom/android/internal/display/BrightnessSynchronizer;->-$$Nest$fgetmLatestFloatBrightness(Lcom/android/internal/display/BrightnessSynchronizer;)F

    move-result v6

    goto/32 :goto_30

    nop

    :goto_35
    invoke-static {v6}, Lcom/android/internal/display/BrightnessSynchronizer;->-$$Nest$fgetmDisplayManager(Lcom/android/internal/display/BrightnessSynchronizer;)Landroid/hardware/display/DisplayManager;

    move-result-object v6

    goto/32 :goto_43

    nop

    :goto_36
    iget v0, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mState:I

    goto/32 :goto_10

    nop

    :goto_37
    iget v7, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mId:I

    goto/32 :goto_46

    nop

    :goto_38
    int-to-float v8, v0

    goto/32 :goto_49

    nop

    :goto_39
    iget v7, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mUpdatedTypes:I

    goto/32 :goto_2b

    nop

    :goto_3a
    iget-object v5, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->this$0:Lcom/android/internal/display/BrightnessSynchronizer;

    goto/32 :goto_6

    nop

    :goto_3b
    iget-object v6, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->this$0:Lcom/android/internal/display/BrightnessSynchronizer;

    goto/32 :goto_34

    nop

    :goto_3c
    iget-wide v3, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mTimeUpdated:J

    goto/32 :goto_53

    nop

    :goto_3d
    iget v7, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mUpdatedTypes:I

    goto/32 :goto_5d

    nop

    :goto_3e
    iget v0, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mConfirmedTypes:I

    goto/32 :goto_28

    nop

    :goto_3f
    invoke-direct {p0, v7, v5}, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->toStringLabel(IF)Ljava/lang/String;

    move-result-object v7

    goto/32 :goto_16

    nop

    :goto_40
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_25

    nop

    :goto_41
    if-ne v5, v0, :cond_4

    goto/32 :goto_3

    :cond_4
    goto/32 :goto_3a

    nop

    :goto_42
    invoke-static {v6}, Lcom/android/internal/display/BrightnessSynchronizer;->-$$Nest$fgetmHandler(Lcom/android/internal/display/BrightnessSynchronizer;)Landroid/os/Handler;

    move-result-object v6

    goto/32 :goto_47

    nop

    :goto_43
    const/4 v7, 0x0

    goto/32 :goto_31

    nop

    :goto_44
    const-string/jumbo v7, "screen_brightness"

    goto/32 :goto_4b

    nop

    :goto_45
    const-string v7, " "

    goto/32 :goto_56

    nop

    :goto_46
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_1c

    nop

    :goto_47
    iget-object v7, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->this$0:Lcom/android/internal/display/BrightnessSynchronizer;

    goto/32 :goto_1b

    nop

    :goto_48
    invoke-direct {p0}, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->getBrightnessAsFloat()F

    move-result v5

    goto/32 :goto_3b

    nop

    :goto_49
    invoke-direct {p0, v7, v8}, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->toStringLabel(IF)Ljava/lang/String;

    move-result-object v7

    goto/32 :goto_27

    nop

    :goto_4a
    iget v8, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mBrightness:F

    goto/32 :goto_0

    nop

    :goto_4b
    invoke-static {v5, v7, v0, v6}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    goto/32 :goto_18

    nop

    :goto_4c
    if-ltz v0, :cond_5

    goto/32 :goto_2d

    :cond_5
    :goto_4d
    goto/32 :goto_22

    nop

    :goto_4e
    invoke-interface {v0}, Lcom/android/internal/display/BrightnessSynchronizer$Clock;->uptimeMillis()J

    move-result-wide v0

    goto/32 :goto_7

    nop

    :goto_4f
    invoke-interface {v4}, Lcom/android/internal/display/BrightnessSynchronizer$Clock;->uptimeMillis()J

    move-result-wide v6

    goto/32 :goto_50

    nop

    :goto_50
    iput-wide v6, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mTimeUpdated:J

    :goto_51
    goto/32 :goto_36

    nop

    :goto_52
    const-string v7, "BrightnessSynchronizer"

    goto/32 :goto_26

    nop

    :goto_53
    add-long/2addr v3, v1

    goto/32 :goto_d

    nop

    :goto_54
    const-string v7, " set brightness values: "

    goto/32 :goto_b

    nop

    :goto_55
    iget v7, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mSourceType:I

    goto/32 :goto_4a

    nop

    :goto_56
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_3d

    nop

    :goto_57
    const/4 v3, 0x2

    goto/32 :goto_29

    nop

    :goto_58
    if-nez v6, :cond_6

    goto/32 :goto_15

    :cond_6
    goto/32 :goto_32

    nop

    :goto_59
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/32 :goto_52

    nop

    :goto_5a
    invoke-static {v0}, Lcom/android/internal/display/BrightnessSynchronizer;->-$$Nest$fgetmClock(Lcom/android/internal/display/BrightnessSynchronizer;)Lcom/android/internal/display/BrightnessSynchronizer$Clock;

    move-result-object v0

    goto/32 :goto_4e

    nop

    :goto_5b
    iput v6, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mUpdatedTypes:I

    :goto_5c
    goto/32 :goto_12

    nop

    :goto_5d
    and-int/2addr v7, v4

    goto/32 :goto_38

    nop

    :goto_5e
    iget-object v6, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->this$0:Lcom/android/internal/display/BrightnessSynchronizer;

    goto/32 :goto_17

    nop

    :goto_5f
    iget-object v4, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->this$0:Lcom/android/internal/display/BrightnessSynchronizer;

    goto/32 :goto_1f

    nop

    :goto_60
    iget v6, p0, Lcom/android/internal/display/BrightnessSynchronizer$BrightnessUpdate;->mUpdatedTypes:I

    goto/32 :goto_c

    nop
.end method
