.class public interface abstract Lcom/android/internal/org/bouncycastle/crypto/AsymmetricCipherKeyPairGenerator;
.super Ljava/lang/Object;


# virtual methods
.method public abstract generateKeyPair()Lcom/android/internal/org/bouncycastle/crypto/AsymmetricCipherKeyPair;
.end method

.method public abstract init(Lcom/android/internal/org/bouncycastle/crypto/KeyGenerationParameters;)V
.end method
