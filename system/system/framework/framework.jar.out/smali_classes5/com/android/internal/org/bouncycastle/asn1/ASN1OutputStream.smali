.class public Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;
.super Ljava/lang/Object;


# instance fields
.field private os:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->os:Ljava/io/OutputStream;

    return-void
.end method

.method public static create(Ljava/io/OutputStream;)Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;
    .locals 1

    new-instance v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;

    invoke-direct {v0, p0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;-><init>(Ljava/io/OutputStream;)V

    return-object v0
.end method

.method public static create(Ljava/io/OutputStream;Ljava/lang/String;)Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;
    .locals 1

    const-string v0, "DER"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/internal/org/bouncycastle/asn1/DEROutputStream;

    invoke-direct {v0, p0}, Lcom/android/internal/org/bouncycastle/asn1/DEROutputStream;-><init>(Ljava/io/OutputStream;)V

    return-object v0

    :cond_0
    const-string v0, "DL"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/android/internal/org/bouncycastle/asn1/DLOutputStream;

    invoke-direct {v0, p0}, Lcom/android/internal/org/bouncycastle/asn1/DLOutputStream;-><init>(Ljava/io/OutputStream;)V

    return-object v0

    :cond_1
    new-instance v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;

    invoke-direct {v0, p0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;-><init>(Ljava/io/OutputStream;)V

    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->os:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    return-void
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->os:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    return-void
.end method

.method flushInternal()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    return-void
.end method

.method getDERSubStream()Lcom/android/internal/org/bouncycastle/asn1/DEROutputStream;
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v1, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->os:Ljava/io/OutputStream;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-direct {v0, v1}, Lcom/android/internal/org/bouncycastle/asn1/DEROutputStream;-><init>(Ljava/io/OutputStream;)V

    goto/32 :goto_0

    nop

    :goto_3
    new-instance v0, Lcom/android/internal/org/bouncycastle/asn1/DEROutputStream;

    goto/32 :goto_1

    nop
.end method

.method getDLSubStream()Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    invoke-direct {v0, v1}, Lcom/android/internal/org/bouncycastle/asn1/DLOutputStream;-><init>(Ljava/io/OutputStream;)V

    goto/32 :goto_3

    nop

    :goto_1
    new-instance v0, Lcom/android/internal/org/bouncycastle/asn1/DLOutputStream;

    goto/32 :goto_2

    nop

    :goto_2
    iget-object v1, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->os:Ljava/io/OutputStream;

    goto/32 :goto_0

    nop

    :goto_3
    return-object v0
.end method

.method final write(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->os:Ljava/io/OutputStream;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method

.method final write([BII)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->os:Ljava/io/OutputStream;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method

.method final writeElements(Ljava/util/Enumeration;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :goto_0
    goto/32 :goto_4

    nop

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_9

    nop

    :goto_2
    check-cast v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_7

    nop

    :goto_3
    return-void

    :goto_4
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    goto/32 :goto_1

    nop

    :goto_5
    goto :goto_0

    :goto_6
    goto/32 :goto_3

    nop

    :goto_7
    invoke-interface {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_8
    const/4 v1, 0x1

    goto/32 :goto_a

    nop

    :goto_9
    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_a
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writePrimitive(Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;Z)V

    goto/32 :goto_5

    nop
.end method

.method final writeElements([Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    array-length v0, p1

    goto/32 :goto_1

    nop

    :goto_1
    const/4 v1, 0x0

    :goto_2
    goto/32 :goto_b

    nop

    :goto_3
    const/4 v3, 0x1

    goto/32 :goto_5

    nop

    :goto_4
    aget-object v2, p1, v1

    goto/32 :goto_a

    nop

    :goto_5
    invoke-virtual {p0, v2, v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writePrimitive(Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;Z)V

    goto/32 :goto_9

    nop

    :goto_6
    goto :goto_2

    :goto_7
    goto/32 :goto_8

    nop

    :goto_8
    return-void

    :goto_9
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_6

    nop

    :goto_a
    invoke-interface {v2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v2

    goto/32 :goto_3

    nop

    :goto_b
    if-lt v1, v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_4

    nop
.end method

.method final writeEncoded(ZIB)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {p0, p3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    goto/32 :goto_5

    nop

    :goto_1
    const/4 v0, 0x1

    goto/32 :goto_6

    nop

    :goto_2
    if-nez p1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {p0, p2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    :goto_4
    goto/32 :goto_1

    nop

    :goto_5
    return-void

    :goto_6
    invoke-virtual {p0, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeLength(I)V

    goto/32 :goto_0

    nop
.end method

.method final writeEncoded(ZIB[B)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_6

    nop

    :goto_0
    array-length v0, p4

    goto/32 :goto_2

    nop

    :goto_1
    invoke-virtual {p0, p3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    goto/32 :goto_9

    nop

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_5

    nop

    :goto_3
    invoke-virtual {p0, p2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    :goto_4
    goto/32 :goto_0

    nop

    :goto_5
    invoke-virtual {p0, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeLength(I)V

    goto/32 :goto_1

    nop

    :goto_6
    if-nez p1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_3

    nop

    :goto_7
    invoke-virtual {p0, p4, v0, v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write([BII)V

    goto/32 :goto_a

    nop

    :goto_8
    array-length v1, p4

    goto/32 :goto_7

    nop

    :goto_9
    const/4 v0, 0x0

    goto/32 :goto_8

    nop

    :goto_a
    return-void
.end method

.method final writeEncoded(ZIB[BIIB)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p0, p4, p5, p6}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write([BII)V

    goto/32 :goto_2

    nop

    :goto_1
    if-nez p1, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_7

    nop

    :goto_2
    invoke-virtual {p0, p7}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    goto/32 :goto_4

    nop

    :goto_3
    add-int/lit8 v0, p6, 0x2

    goto/32 :goto_5

    nop

    :goto_4
    return-void

    :goto_5
    invoke-virtual {p0, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeLength(I)V

    goto/32 :goto_6

    nop

    :goto_6
    invoke-virtual {p0, p3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    goto/32 :goto_0

    nop

    :goto_7
    invoke-virtual {p0, p2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    :goto_8
    goto/32 :goto_3

    nop
.end method

.method final writeEncoded(ZII[B)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    array-length v0, p4

    goto/32 :goto_4

    nop

    :goto_1
    invoke-virtual {p0, p4, v1, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write([BII)V

    goto/32 :goto_5

    nop

    :goto_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeTag(ZII)V

    goto/32 :goto_0

    nop

    :goto_3
    array-length v0, p4

    goto/32 :goto_6

    nop

    :goto_4
    invoke-virtual {p0, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeLength(I)V

    goto/32 :goto_3

    nop

    :goto_5
    return-void

    :goto_6
    const/4 v1, 0x0

    goto/32 :goto_1

    nop
.end method

.method final writeEncoded(ZI[B)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_6

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_7

    nop

    :goto_1
    invoke-virtual {p0, p2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    :goto_2
    goto/32 :goto_3

    nop

    :goto_3
    array-length v0, p3

    goto/32 :goto_8

    nop

    :goto_4
    invoke-virtual {p0, p3, v0, v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write([BII)V

    goto/32 :goto_5

    nop

    :goto_5
    return-void

    :goto_6
    if-nez p1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop

    :goto_7
    array-length v1, p3

    goto/32 :goto_4

    nop

    :goto_8
    invoke-virtual {p0, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeLength(I)V

    goto/32 :goto_0

    nop
.end method

.method final writeEncoded(ZI[BII)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_4

    nop

    :goto_0
    invoke-virtual {p0, p5}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeLength(I)V

    goto/32 :goto_5

    nop

    :goto_1
    return-void

    :goto_2
    invoke-virtual {p0, p2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    :goto_3
    goto/32 :goto_0

    nop

    :goto_4
    if-nez p1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_2

    nop

    :goto_5
    invoke-virtual {p0, p3, p4, p5}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write([BII)V

    goto/32 :goto_1

    nop
.end method

.method final writeEncodedIndef(ZII[B)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_5

    nop

    :goto_0
    invoke-virtual {p0, p4, v1, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write([BII)V

    goto/32 :goto_6

    nop

    :goto_1
    return-void

    :goto_2
    array-length v0, p4

    goto/32 :goto_7

    nop

    :goto_3
    invoke-virtual {p0, v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    goto/32 :goto_1

    nop

    :goto_4
    invoke-virtual {p0, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    goto/32 :goto_2

    nop

    :goto_5
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeTag(ZII)V

    goto/32 :goto_8

    nop

    :goto_6
    invoke-virtual {p0, v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    goto/32 :goto_3

    nop

    :goto_7
    const/4 v1, 0x0

    goto/32 :goto_0

    nop

    :goto_8
    const/16 v0, 0x80

    goto/32 :goto_4

    nop
.end method

.method final writeEncodedIndef(ZILjava/util/Enumeration;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_6

    nop

    :goto_0
    const/16 v0, 0x80

    goto/32 :goto_8

    nop

    :goto_1
    return-void

    :goto_2
    const/4 v0, 0x0

    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {p0, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    goto/32 :goto_7

    nop

    :goto_4
    invoke-virtual {p0, p2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    :goto_5
    goto/32 :goto_0

    nop

    :goto_6
    if-nez p1, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_4

    nop

    :goto_7
    invoke-virtual {p0, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    goto/32 :goto_1

    nop

    :goto_8
    invoke-virtual {p0, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    goto/32 :goto_9

    nop

    :goto_9
    invoke-virtual {p0, p3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeElements(Ljava/util/Enumeration;)V

    goto/32 :goto_2

    nop
.end method

.method final writeEncodedIndef(ZI[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_8

    nop

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    goto/32 :goto_5

    nop

    :goto_1
    const/16 v0, 0x80

    goto/32 :goto_0

    nop

    :goto_2
    return-void

    :goto_3
    invoke-virtual {p0, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    goto/32 :goto_2

    nop

    :goto_4
    const/4 v0, 0x0

    goto/32 :goto_9

    nop

    :goto_5
    invoke-virtual {p0, p3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeElements([Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    goto/32 :goto_4

    nop

    :goto_6
    invoke-virtual {p0, p2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    :goto_7
    goto/32 :goto_1

    nop

    :goto_8
    if-nez p1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_6

    nop

    :goto_9
    invoke-virtual {p0, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    goto/32 :goto_3

    nop
.end method

.method final writeLength(I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_1d

    nop

    :goto_0
    int-to-byte v3, v3

    goto/32 :goto_3

    nop

    :goto_1
    move v1, p1

    :goto_2
    goto/32 :goto_9

    nop

    :goto_3
    invoke-virtual {p0, v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    goto/32 :goto_b

    nop

    :goto_4
    return-void

    :goto_5
    goto :goto_1a

    :goto_6
    goto/32 :goto_12

    nop

    :goto_7
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_13

    nop

    :goto_8
    int-to-byte v2, v2

    goto/32 :goto_a

    nop

    :goto_9
    ushr-int/lit8 v2, v1, 0x8

    goto/32 :goto_d

    nop

    :goto_a
    invoke-virtual {p0, v2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    goto/32 :goto_c

    nop

    :goto_b
    add-int/lit8 v2, v2, -0x8

    goto/32 :goto_15

    nop

    :goto_c
    add-int/lit8 v2, v0, -0x1

    goto/32 :goto_e

    nop

    :goto_d
    move v1, v2

    goto/32 :goto_11

    nop

    :goto_e
    mul-int/lit8 v2, v2, 0x8

    :goto_f
    goto/32 :goto_1c

    nop

    :goto_10
    const/4 v0, 0x1

    goto/32 :goto_1

    nop

    :goto_11
    if-nez v2, :cond_0

    goto/32 :goto_14

    :cond_0
    goto/32 :goto_7

    nop

    :goto_12
    int-to-byte v0, p1

    goto/32 :goto_19

    nop

    :goto_13
    goto :goto_2

    :goto_14
    goto/32 :goto_18

    nop

    :goto_15
    goto :goto_f

    :goto_16
    goto/32 :goto_5

    nop

    :goto_17
    shr-int v3, p1, v2

    goto/32 :goto_0

    nop

    :goto_18
    or-int/lit16 v2, v0, 0x80

    goto/32 :goto_8

    nop

    :goto_19
    invoke-virtual {p0, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    :goto_1a
    goto/32 :goto_4

    nop

    :goto_1b
    if-gt p1, v0, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_10

    nop

    :goto_1c
    if-gez v2, :cond_2

    goto/32 :goto_16

    :cond_2
    goto/32 :goto_17

    nop

    :goto_1d
    const/16 v0, 0x7f

    goto/32 :goto_1b

    nop
.end method

.method public writeObject(Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writePrimitive(Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;Z)V

    invoke-virtual {p0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->flushInternal()V

    return-void

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "null object detected"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public writeObject(Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writePrimitive(Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;Z)V

    invoke-virtual {p0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->flushInternal()V

    return-void

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "null object detected"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method writePrimitive(Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p1, p0, p2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->encode(Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;Z)V

    goto/32 :goto_0

    nop
.end method

.method final writeTag(ZII)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_12

    nop

    :goto_0
    return-void

    :goto_1
    goto/32 :goto_14

    nop

    :goto_2
    array-length v0, v1

    goto/32 :goto_3

    nop

    :goto_3
    sub-int/2addr v0, v2

    goto/32 :goto_1c

    nop

    :goto_4
    const/16 v0, 0x80

    goto/32 :goto_6

    nop

    :goto_5
    new-array v1, v1, [B

    goto/32 :goto_26

    nop

    :goto_6
    if-lt p3, v0, :cond_0

    goto/32 :goto_24

    :cond_0
    goto/32 :goto_a

    nop

    :goto_7
    or-int/lit8 v0, p2, 0x1f

    goto/32 :goto_b

    nop

    :goto_8
    if-le p3, v3, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_2

    nop

    :goto_9
    int-to-byte v3, v3

    goto/32 :goto_e

    nop

    :goto_a
    invoke-virtual {p0, p3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    goto/32 :goto_23

    nop

    :goto_b
    invoke-virtual {p0, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    goto/32 :goto_4

    nop

    :goto_c
    return-void

    :goto_d
    goto/32 :goto_1f

    nop

    :goto_e
    aput-byte v3, v1, v2

    :goto_f
    goto/32 :goto_1b

    nop

    :goto_10
    add-int/lit8 v2, v2, -0x1

    goto/32 :goto_25

    nop

    :goto_11
    or-int v0, p2, p3

    goto/32 :goto_22

    nop

    :goto_12
    if-eqz p1, :cond_2

    goto/32 :goto_1

    :cond_2
    goto/32 :goto_0

    nop

    :goto_13
    aput-byte v3, v1, v2

    goto/32 :goto_1a

    nop

    :goto_14
    const/16 v0, 0x1f

    goto/32 :goto_16

    nop

    :goto_15
    or-int/2addr v3, v0

    goto/32 :goto_19

    nop

    :goto_16
    if-lt p3, v0, :cond_3

    goto/32 :goto_18

    :cond_3
    goto/32 :goto_11

    nop

    :goto_17
    goto :goto_1d

    :goto_18
    goto/32 :goto_7

    nop

    :goto_19
    int-to-byte v3, v3

    goto/32 :goto_13

    nop

    :goto_1a
    const/16 v3, 0x7f

    goto/32 :goto_8

    nop

    :goto_1b
    shr-int/lit8 p3, p3, 0x7

    goto/32 :goto_20

    nop

    :goto_1c
    invoke-virtual {p0, v1, v2, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write([BII)V

    :goto_1d
    goto/32 :goto_c

    nop

    :goto_1e
    const/4 v1, 0x5

    goto/32 :goto_5

    nop

    :goto_1f
    goto :goto_f

    :goto_20
    add-int/lit8 v2, v2, -0x1

    goto/32 :goto_21

    nop

    :goto_21
    and-int/lit8 v3, p3, 0x7f

    goto/32 :goto_15

    nop

    :goto_22
    invoke-virtual {p0, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    goto/32 :goto_17

    nop

    :goto_23
    goto :goto_1d

    :goto_24
    goto/32 :goto_1e

    nop

    :goto_25
    and-int/lit8 v3, p3, 0x7f

    goto/32 :goto_9

    nop

    :goto_26
    array-length v2, v1

    goto/32 :goto_10

    nop
.end method
