.class public final Lcom/android/internal/R$dimen;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final CameraCircleBlackView_height:I = 0x105000a

.field public static final CameraCircleBlackView_radius:I = 0x105000b

.field public static final CameraCircleBlackView_width:I = 0x105000c

.field public static final accessibility_focus_highlight_stroke_width:I = 0x105000e

.field public static final accessibility_magnification_indicator_width:I = 0x1050010

.field public static final accessibility_touch_slop:I = 0x1050011

.field public static final accessibility_window_magnifier_min_size:I = 0x1050012

.field public static final action_bar_stacked_max_height:I = 0x1050023

.field public static final action_bar_stacked_tab_max_width:I = 0x1050024

.field public static final alert_dialog_round_padding:I = 0x1050035

.field public static final app_icon_size:I = 0x1050000

.field public static final autofill_dataset_picker_max_height:I = 0x1050039

.field public static final autofill_dataset_picker_max_width:I = 0x105003a

.field public static final autofill_save_custom_subtitle_max_height:I = 0x105003f

.field public static final autofill_save_icon_max_size:I = 0x1050040

.field public static final button_inset_horizontal_material:I = 0x105004c

.field public static final button_inset_vertical_material:I = 0x105004d

.field public static final button_padding_horizontal_material:I = 0x105004f

.field public static final call_notification_collapsible_indent:I = 0x1050052

.field public static final call_notification_system_action_min_width:I = 0x1050053

.field public static final car_body1_size:I = 0x1050057

.field public static final car_padding_4:I = 0x1050079

.field public static final cascading_menus_min_smallest_width:I = 0x1050095

.field public static final chooser_action_button_icon_size:I = 0x1050096

.field public static final chooser_badge_size:I = 0x1050097

.field public static final chooser_corner_radius:I = 0x1050098

.field public static final chooser_direct_share_label_placeholder_max_width:I = 0x1050099

.field public static final chooser_edge_margin_normal:I = 0x105009a

.field public static final chooser_edge_margin_thin:I = 0x105009b

.field public static final chooser_header_scroll_elevation:I = 0x105009d

.field public static final chooser_icon_size:I = 0x105009e

.field public static final chooser_max_collapsed_height:I = 0x105009f

.field public static final chooser_preview_image_border:I = 0x10500a0

.field public static final chooser_preview_image_font_size:I = 0x10500a1

.field public static final chooser_preview_image_max_dimen:I = 0x10500a2

.field public static final chooser_preview_width:I = 0x10500a3

.field public static final chooser_row_text_option_translate:I = 0x10500a4

.field public static final chooser_view_spacing:I = 0x10500a5

.field public static final chooser_width:I = 0x10500a6

.field public static final circular_display_mask_thickness:I = 0x10500a7

.field public static final config_ScreenRotAnimDamping:I = 0x10500a8

.field public static final config_ScreenRotAnimResponseLong:I = 0x10500a9

.field public static final config_ScreenRotAnimResponseMiddle:I = 0x10500aa

.field public static final config_ScreenRotAnimResponseShort:I = 0x10500ab

.field public static final config_ScreenRotAnimScale:I = 0x10500ac

.field public static final config_ambiguousGestureMultiplier:I = 0x10500ae

.field public static final config_appTransitionAnimationDurationScaleDefault:I = 0x10500af

.field public static final config_backGestureInset:I = 0x10500b0

.field public static final config_closeToSquareDisplayMaxAspectRatio:I = 0x10500b3

.field public static final config_defaultBinderHeavyHitterAutoSamplerThreshold:I = 0x10500b4

.field public static final config_defaultBinderHeavyHitterWatcherThreshold:I = 0x10500b5

.field public static final config_displayWhiteBalanceBrightnessFilterIntercept:I = 0x10500b7

.field public static final config_displayWhiteBalanceColorTemperatureFilterIntercept:I = 0x10500b8

.field public static final config_displayWhiteBalanceHighLightAmbientColorTemperature:I = 0x10500b9

.field public static final config_displayWhiteBalanceLowLightAmbientColorTemperature:I = 0x10500ba

.field public static final config_fixedOrientationLetterboxAspectRatio:I = 0x10500bb

.field public static final config_hapticChannelMaxVibrationAmplitude:I = 0x10500bc

.field public static final config_highResTaskSnapshotScale:I = 0x10500bd

.field public static final config_horizontalScrollFactor:I = 0x10500be

.field public static final config_inCallNotificationVolume:I = 0x10500bf

.field public static final config_letterboxBackgroundWallaperDarkScrimAlpha:I = 0x10500c0

.field public static final config_letterboxBackgroundWallpaperBlurRadius:I = 0x10500c1

.field public static final config_letterboxHorizontalPositionMultiplier:I = 0x10500c2

.field public static final config_lowResTaskSnapshotScale:I = 0x10500c3

.field public static final config_mediaMetadataBitmapMaxSize:I = 0x10500c4

.field public static final config_minPercentageMultiWindowSupportHeight:I = 0x10500c5

.field public static final config_minPercentageMultiWindowSupportWidth:I = 0x10500c6

.field public static final config_minScalingSpan:I = 0x10500c7

.field public static final config_minScalingTouchMajor:I = 0x10500c8

.field public static final config_minScrollbarTouchTarget:I = 0x10500c9

.field public static final config_pictureInPictureExpandedHorizontalHeight:I = 0x10500ca

.field public static final config_pictureInPictureExpandedVerticalWidth:I = 0x10500cb

.field public static final config_pictureInPictureMaxAspectRatio:I = 0x10500cc

.field public static final config_pictureInPictureMinAspectRatio:I = 0x10500cd

.field public static final config_prefDialogWidth:I = 0x10500ce

.field public static final config_qsTileStrokeWidthActive:I = 0x10500d1

.field public static final config_qsTileStrokeWidthInactive:I = 0x10500d2

.field public static final config_restrictedIconSize:I = 0x1050007

.field public static final config_screenBrightnessDimFloat:I = 0x10500d3

.field public static final config_screenBrightnessDozeFloat:I = 0x10500d4

.field public static final config_screenBrightnessMinimumDimAmountFloat:I = 0x10500d5

.field public static final config_screenBrightnessSettingDefaultFloat:I = 0x10500d6

.field public static final config_screenBrightnessSettingForVrDefaultFloat:I = 0x10500d7

.field public static final config_screenBrightnessSettingForVrMaximumFloat:I = 0x10500d8

.field public static final config_screenBrightnessSettingForVrMinimumFloat:I = 0x10500d9

.field public static final config_screenBrightnessSettingMaximumFloat:I = 0x10500da

.field public static final config_screenBrightnessSettingMinimumFloat:I = 0x10500db

.field public static final config_screen_magnification_scaling_threshold:I = 0x10500dc

.field public static final config_scrollFactor:I = 0x10500dd

.field public static final config_scrollbarSize:I = 0x10500de

.field public static final config_signalCutoutHeightFraction:I = 0x10500df

.field public static final config_signalCutoutWidthFraction:I = 0x10500e0

.field public static final config_verticalScrollFactor:I = 0x10500e1

.field public static final config_viewConfigurationHoverSlop:I = 0x10500e2

.field public static final config_viewConfigurationTouchSlop:I = 0x10500e3

.field public static final config_viewMaxFlingVelocity:I = 0x10500e4

.field public static final config_viewMinFlingVelocity:I = 0x10500e5

.field public static final config_wallpaperDimAmount:I = 0x10500e6

.field public static final config_wallpaperMaxScale:I = 0x10500e7

.field public static final content_rect_bottom_clip_allowance:I = 0x10500e9

.field public static final controls_thumbnail_image_max_height:I = 0x10500ed

.field public static final controls_thumbnail_image_max_width:I = 0x10500ee

.field public static final conversation_avatar_size:I = 0x10500ef

.field public static final conversation_avatar_size_group_expanded:I = 0x10500f0

.field public static final conversation_badge_protrusion:I = 0x10500f1

.field public static final conversation_badge_protrusion_group_expanded:I = 0x10500f2

.field public static final conversation_badge_protrusion_group_expanded_face_pile:I = 0x10500f3

.field public static final conversation_content_start:I = 0x10500f4

.field public static final conversation_face_pile_avatar_size:I = 0x10500f6

.field public static final conversation_face_pile_avatar_size_group_expanded:I = 0x10500f7

.field public static final conversation_face_pile_protection_width:I = 0x10500f8

.field public static final conversation_face_pile_protection_width_expanded:I = 0x10500f9

.field public static final conversation_header_expanded_padding_end:I = 0x10500fa

.field public static final conversation_icon_circle_start:I = 0x10500fb

.field public static final conversation_icon_container_top_padding:I = 0x10500fc

.field public static final conversation_icon_container_top_padding_small_avatar:I = 0x10500fd

.field public static final conversation_icon_size_badged:I = 0x10500fe

.field public static final cross_profile_apps_thumbnail_size:I = 0x1050100

.field public static final date_picker_day_height:I = 0x1050102

.field public static final date_picker_day_of_week_height:I = 0x1050103

.field public static final date_picker_day_of_week_text_size:I = 0x1050104

.field public static final date_picker_day_selector_radius:I = 0x1050105

.field public static final date_picker_day_text_size:I = 0x1050106

.field public static final date_picker_day_width:I = 0x1050107

.field public static final date_picker_month_height:I = 0x1050108

.field public static final date_picker_month_text_size:I = 0x1050109

.field public static final datepicker_view_animator_height:I = 0x1050114

.field public static final datepicker_year_label_height:I = 0x1050115

.field public static final day_picker_padding_top:I = 0x1050118

.field public static final default_app_widget_padding_bottom:I = 0x1050119

.field public static final default_app_widget_padding_left:I = 0x105011a

.field public static final default_app_widget_padding_right:I = 0x105011b

.field public static final default_app_widget_padding_top:I = 0x105011c

.field public static final default_background_blur_radius:I = 0x105011d

.field public static final default_gap:I = 0x105011e

.field public static final default_magnifier_corner_radius:I = 0x105011f

.field public static final default_magnifier_elevation:I = 0x1050120

.field public static final default_magnifier_height:I = 0x1050121

.field public static final default_magnifier_horizontal_offset:I = 0x1050122

.field public static final default_magnifier_vertical_offset:I = 0x1050123

.field public static final default_magnifier_width:I = 0x1050124

.field public static final default_magnifier_zoom:I = 0x1050125

.field public static final default_minimal_size_resizable_task:I = 0x1050127

.field public static final dialog_min_width_major:I = 0x1050003

.field public static final dialog_min_width_minor:I = 0x1050004

.field public static final display_cutout_touchable_region_size:I = 0x1050138

.field public static final docked_stack_divider_insets:I = 0x1050139

.field public static final docked_stack_divider_thickness:I = 0x105013a

.field public static final docked_stack_minimize_thickness:I = 0x105013b

.field public static final dropdownitem_icon_width:I = 0x105013c

.field public static final dropdownitem_text_padding_left:I = 0x105013d

.field public static final emphasized_button_stroke_width:I = 0x1050143

.field public static final expanded_group_conversation_message_padding:I = 0x1050144

.field public static final fast_scroller_minimum_touch_target:I = 0x1050146

.field public static final floating_toolbar_height:I = 0x1050147

.field public static final floating_toolbar_horizontal_margin:I = 0x1050148

.field public static final floating_toolbar_icon_text_spacing:I = 0x1050149

.field public static final floating_toolbar_maximum_overflow_height:I = 0x105014a

.field public static final floating_toolbar_menu_button_minimum_width:I = 0x105014b

.field public static final floating_toolbar_menu_button_side_padding:I = 0x105014c

.field public static final floating_toolbar_minimum_overflow_height:I = 0x1050150

.field public static final floating_toolbar_overflow_side_padding:I = 0x1050152

.field public static final floating_toolbar_preferred_width:I = 0x1050153

.field public static final floating_toolbar_text_size:I = 0x1050154

.field public static final floating_toolbar_vertical_margin:I = 0x1050155

.field public static final immersive_mode_cling_width:I = 0x105016e

.field public static final importance_ring_anim_max_stroke_width:I = 0x105016f

.field public static final importance_ring_size:I = 0x1050170

.field public static final importance_ring_stroke_width:I = 0x1050171

.field public static final input_extract_action_button_height:I = 0x1050172

.field public static final input_extract_action_button_width:I = 0x1050173

.field public static final input_method_nav_key_button_ripple_max_width:I = 0x1050176

.field public static final item_touch_helper_max_drag_scroll_per_frame:I = 0x1050179

.field public static final item_touch_helper_swipe_escape_max_velocity:I = 0x105017a

.field public static final item_touch_helper_swipe_escape_velocity:I = 0x105017b

.field public static final lock_pattern_dot_hit_factor:I = 0x10501c9

.field public static final lock_pattern_dot_line_width:I = 0x10501ca

.field public static final lock_pattern_dot_size:I = 0x10501cb

.field public static final lock_pattern_dot_size_activated:I = 0x10501cc

.field public static final lock_pattern_fade_away_gradient_width:I = 0x10501cd

.field public static final media_notification_expanded_image_margin_bottom:I = 0x10501d0

.field public static final messaging_avatar_size:I = 0x10501d3

.field public static final messaging_group_sending_progress_size:I = 0x10501d4

.field public static final messaging_group_singleline_sender_padding_end:I = 0x10501d5

.field public static final messaging_image_extra_spacing:I = 0x10501d6

.field public static final messaging_image_max_height:I = 0x10501d7

.field public static final messaging_image_min_size:I = 0x10501d8

.field public static final messaging_image_rounding:I = 0x10501d9

.field public static final messaging_layout_icon_padding_start:I = 0x10501da

.field public static final miui_default_minimal_size_resizable_task:I = 0x10501dc

.field public static final navigation_bar_frame_height:I = 0x10501dd

.field public static final navigation_bar_frame_height_landscape:I = 0x10501de

.field public static final navigation_bar_gesture_height:I = 0x10501df

.field public static final navigation_bar_gesture_larger_height:I = 0x10501e0

.field public static final navigation_bar_height:I = 0x10501e1

.field public static final navigation_bar_height_car_mode:I = 0x10501e2

.field public static final navigation_bar_height_landscape:I = 0x10501e3

.field public static final navigation_bar_height_landscape_car_mode:I = 0x10501e4

.field public static final navigation_bar_width:I = 0x10501e6

.field public static final navigation_bar_width_car_mode:I = 0x10501e7

.field public static final notification_action_disabled_alpha:I = 0x10501e9

.field public static final notification_action_emphasized_height:I = 0x10501ea

.field public static final notification_action_list_height:I = 0x10501eb

.field public static final notification_actions_collapsed_priority_width:I = 0x10501ed

.field public static final notification_actions_icon_drawable_size:I = 0x10501ee

.field public static final notification_actions_padding_start:I = 0x10501f0

.field public static final notification_badge_size:I = 0x10501f2

.field public static final notification_big_picture_max_height:I = 0x10501f3

.field public static final notification_big_picture_max_height_low_ram:I = 0x10501f4

.field public static final notification_big_picture_max_width:I = 0x10501f5

.field public static final notification_big_picture_max_width_low_ram:I = 0x10501f6

.field public static final notification_content_margin:I = 0x10501f8

.field public static final notification_content_margin_end:I = 0x10501f9

.field public static final notification_content_margin_start:I = 0x10501fa

.field public static final notification_content_margin_top:I = 0x10501fb

.field public static final notification_custom_view_max_image_height:I = 0x10501fd

.field public static final notification_custom_view_max_image_height_low_ram:I = 0x10501fe

.field public static final notification_custom_view_max_image_width:I = 0x10501ff

.field public static final notification_custom_view_max_image_width_low_ram:I = 0x1050200

.field public static final notification_grayscale_icon_max_size:I = 0x1050203

.field public static final notification_header_app_name_margin_start:I = 0x1050204

.field public static final notification_header_background_height:I = 0x1050205

.field public static final notification_header_expand_icon_size:I = 0x1050206

.field public static final notification_header_icon_size:I = 0x1050208

.field public static final notification_header_separating_margin:I = 0x105020d

.field public static final notification_header_shrink_hide_width:I = 0x105020e

.field public static final notification_header_shrink_min_width:I = 0x105020f

.field public static final notification_header_touchable_height:I = 0x1050210

.field public static final notification_headerless_margin_oneline:I = 0x1050212

.field public static final notification_headerless_margin_twoline:I = 0x1050213

.field public static final notification_heading_margin_end:I = 0x1050215

.field public static final notification_icon_circle_size:I = 0x1050217

.field public static final notification_icon_circle_start:I = 0x1050218

.field public static final notification_inbox_item_top_padding:I = 0x1050219

.field public static final notification_large_icon_height:I = 0x1050006

.field public static final notification_large_icon_width:I = 0x1050005

.field public static final notification_messaging_spacing:I = 0x105021c

.field public static final notification_messaging_spacing_conversation_group:I = 0x105021d

.field public static final notification_min_height:I = 0x105021e

.field public static final notification_person_icon_max_size:I = 0x105021f

.field public static final notification_person_icon_max_size_low_ram:I = 0x1050220

.field public static final notification_right_icon_content_margin:I = 0x1050225

.field public static final notification_right_icon_size:I = 0x1050227

.field public static final notification_right_icon_size_low_ram:I = 0x1050228

.field public static final notification_secondary_text_disabled_alpha:I = 0x1050229

.field public static final notification_small_icon_size:I = 0x105022a

.field public static final notification_small_icon_size_low_ram:I = 0x105022b

.field public static final notification_subtext_size:I = 0x105022c

.field public static final notification_text_margin_top:I = 0x105022e

.field public static final notification_text_size:I = 0x105022f

.field public static final notification_title_text_size:I = 0x1050230

.field public static final notification_top_pad:I = 0x1050231

.field public static final notification_top_pad_large_text:I = 0x1050232

.field public static final notification_top_pad_large_text_narrow:I = 0x1050233

.field public static final notification_top_pad_narrow:I = 0x1050234

.field public static final password_keyboard_spacebar_vertical_correction:I = 0x105023a

.field public static final pip_minimized_visible_size:I = 0x1050246

.field public static final popup_enter_animation_from_y_delta:I = 0x1050247

.field public static final popup_exit_animation_to_y_delta:I = 0x1050248

.field public static final preference_fragment_padding_bottom:I = 0x105024e

.field public static final preference_fragment_padding_side:I = 0x105024f

.field public static final quick_qs_offset_height:I = 0x1050268

.field public static final resize_shadow_size:I = 0x1050269

.field public static final resolver_badge_size:I = 0x105026a

.field public static final resolver_button_bar_spacing:I = 0x105026b

.field public static final resolver_edge_margin:I = 0x105026c

.field public static final resolver_elevation:I = 0x105026d

.field public static final resolver_empty_state_container_padding_bottom:I = 0x105026e

.field public static final resolver_empty_state_container_padding_top:I = 0x105026f

.field public static final resolver_empty_state_height:I = 0x1050270

.field public static final resolver_empty_state_height_with_tabs:I = 0x1050271

.field public static final resolver_icon_margin:I = 0x1050272

.field public static final resolver_icon_size:I = 0x1050273

.field public static final resolver_max_collapsed_height_with_default_with_tabs:I = 0x1050276

.field public static final resolver_max_collapsed_height_with_tabs:I = 0x1050277

.field public static final resolver_profile_tab_margin:I = 0x1050279

.field public static final resolver_small_margin:I = 0x105027a

.field public static final resolver_tab_text_size:I = 0x105027b

.field public static final resolver_title_padding_bottom:I = 0x105027c

.field public static final rounded_corner_radius:I = 0x105027f

.field public static final rounded_corner_radius_adjustment:I = 0x1050280

.field public static final rounded_corner_radius_bottom:I = 0x1050281

.field public static final rounded_corner_radius_bottom_adjustment:I = 0x1050282

.field public static final rounded_corner_radius_top:I = 0x1050283

.field public static final rounded_corner_radius_top_adjustment:I = 0x1050284

.field public static final search_view_preferred_height:I = 0x1050289

.field public static final search_view_preferred_width:I = 0x105028a

.field public static final secondary_rounded_corner_radius:I = 0x105028e

.field public static final secondary_rounded_corner_radius_adjustment:I = 0x105028f

.field public static final secondary_rounded_corner_radius_bottom:I = 0x1050290

.field public static final secondary_rounded_corner_radius_bottom_adjustment:I = 0x1050291

.field public static final secondary_rounded_corner_radius_top:I = 0x1050292

.field public static final secondary_rounded_corner_radius_top_adjustment:I = 0x1050293

.field public static final secondary_waterfall_display_bottom_edge_size:I = 0x1050294

.field public static final secondary_waterfall_display_left_edge_size:I = 0x1050295

.field public static final secondary_waterfall_display_right_edge_size:I = 0x1050296

.field public static final secondary_waterfall_display_top_edge_size:I = 0x1050297

.field public static final seekbar_thumb_exclusion_max_size:I = 0x1050298

.field public static final slice_icon_size:I = 0x105029d

.field public static final slice_padding:I = 0x105029e

.field public static final slice_shortcut_size:I = 0x105029f

.field public static final starting_surface_default_icon_size:I = 0x10502a2

.field public static final starting_surface_icon_size:I = 0x10502a3

.field public static final status_bar_height:I = 0x10502a6

.field public static final status_bar_height_default:I = 0x10502a7

.field public static final status_bar_height_landscape:I = 0x10502a8

.field public static final status_bar_height_portrait:I = 0x10502a9

.field public static final status_bar_icon_size:I = 0x10502aa

.field public static final status_bar_system_icon_intrinsic_size:I = 0x10502ab

.field public static final status_bar_system_icon_size:I = 0x10502ac

.field public static final subtitle_corner_radius:I = 0x10502ad

.field public static final subtitle_outline_width:I = 0x10502ae

.field public static final subtitle_shadow_offset:I = 0x10502af

.field public static final subtitle_shadow_radius:I = 0x10502b0

.field public static final system_app_widget_background_radius:I = 0x1050008

.field public static final system_app_widget_inner_radius:I = 0x1050009

.field public static final system_gestures_start_threshold:I = 0x10502b2

.field public static final task_height_of_minimized_mode:I = 0x10502b3

.field public static final taskbar_frame_height:I = 0x10502b4

.field public static final text_handle_min_size:I = 0x10502b7

.field public static final text_size_body_2_material:I = 0x10502ba

.field public static final text_size_small_material:I = 0x10502c6

.field public static final textview_error_popup_default_width:I = 0x10502cd

.field public static final thumbnail_height:I = 0x1050001

.field public static final thumbnail_width:I = 0x1050002

.field public static final timepicker_center_dot_radius:I = 0x10502d1

.field public static final timepicker_selector_dot_radius:I = 0x10502d9

.field public static final timepicker_selector_radius:I = 0x10502da

.field public static final timepicker_selector_stroke:I = 0x10502db

.field public static final timepicker_text_inset_inner:I = 0x10502dd

.field public static final timepicker_text_inset_normal:I = 0x10502de

.field public static final timepicker_text_size_inner:I = 0x10502df

.field public static final timepicker_text_size_normal:I = 0x10502e0

.field public static final toast_y_offset:I = 0x10502e5

.field public static final tooltip_precise_anchor_extra_offset:I = 0x10502e9

.field public static final tooltip_precise_anchor_threshold:I = 0x10502ea

.field public static final tooltip_y_offset_non_touch:I = 0x10502ec

.field public static final tooltip_y_offset_touch:I = 0x10502ed

.field public static final user_icon_size:I = 0x10502ee

.field public static final waterfall_display_bottom_edge_size:I = 0x10502ef

.field public static final waterfall_display_left_edge_size:I = 0x10502f0

.field public static final waterfall_display_right_edge_size:I = 0x10502f1

.field public static final waterfall_display_top_edge_size:I = 0x10502f2


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
