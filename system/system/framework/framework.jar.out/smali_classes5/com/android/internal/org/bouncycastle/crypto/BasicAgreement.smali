.class public interface abstract Lcom/android/internal/org/bouncycastle/crypto/BasicAgreement;
.super Ljava/lang/Object;


# virtual methods
.method public abstract calculateAgreement(Lcom/android/internal/org/bouncycastle/crypto/CipherParameters;)Ljava/math/BigInteger;
.end method

.method public abstract getFieldSize()I
.end method

.method public abstract init(Lcom/android/internal/org/bouncycastle/crypto/CipherParameters;)V
.end method
