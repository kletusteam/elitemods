.class Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;
.super Lcom/android/internal/inputmethod/IRemoteAccessibilityInputConnection$Stub;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;


# direct methods
.method constructor <init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    invoke-direct {p0}, Lcom/android/internal/inputmethod/IRemoteAccessibilityInputConnection$Stub;-><init>()V

    return-void
.end method

.method static synthetic lambda$getCursorCapsMode$9(ILjava/lang/Integer;)[B
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p0, v0}, Lcom/android/internal/inputmethod/InputConnectionProtoDumper;->buildGetCursorCapsModeProto(II)[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$getSurroundingText$3(IIILandroid/view/inputmethod/SurroundingText;)[B
    .locals 1

    invoke-static {p0, p1, p2, p3}, Lcom/android/internal/inputmethod/InputConnectionProtoDumper;->buildGetSurroundingTextProto(IIILandroid/view/inputmethod/SurroundingText;)[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public clearMetaKeyStates(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;I)V
    .locals 3

    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    new-instance v1, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1$$ExternalSyntheticLambda2;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;I)V

    const-string v2, "clearMetaKeyStatesFromA11yIme"

    invoke-static {v0, v2, v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->-$$Nest$mdispatchWithTracing(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public commitText(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Ljava/lang/CharSequence;ILandroid/view/inputmethod/TextAttribute;)V
    .locals 8

    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    new-instance v7, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1$$ExternalSyntheticLambda8;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1$$ExternalSyntheticLambda8;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Ljava/lang/CharSequence;ILandroid/view/inputmethod/TextAttribute;)V

    const-string v1, "commitTextFromA11yIme"

    invoke-static {v0, v1, v7}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->-$$Nest$mdispatchWithTracing(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public deleteSurroundingText(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;II)V
    .locals 3

    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    new-instance v1, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1$$ExternalSyntheticLambda1;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;II)V

    const-string v2, "deleteSurroundingTextFromA11yIme"

    invoke-static {v0, v2, v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->-$$Nest$mdispatchWithTracing(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public getCursorCapsMode(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;ILcom/android/internal/infra/AndroidFuture;)V
    .locals 4

    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    new-instance v1, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1$$ExternalSyntheticLambda5;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1$$ExternalSyntheticLambda5;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;I)V

    invoke-static {}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->-$$Nest$smuseImeTracing()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1$$ExternalSyntheticLambda6;

    invoke-direct {v2, p2}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1$$ExternalSyntheticLambda6;-><init>(I)V

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    const-string v3, "getCursorCapsModeFromA11yIme"

    invoke-static {v0, v3, p3, v1, v2}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->-$$Nest$mdispatchWithTracing(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Ljava/lang/String;Lcom/android/internal/infra/AndroidFuture;Ljava/util/function/Supplier;Ljava/util/function/Function;)V

    return-void
.end method

.method public getSurroundingText(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;IIILcom/android/internal/infra/AndroidFuture;)V
    .locals 8

    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    new-instance v7, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1$$ExternalSyntheticLambda3;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1$$ExternalSyntheticLambda3;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;III)V

    invoke-static {}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->-$$Nest$smuseImeTracing()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1$$ExternalSyntheticLambda4;

    invoke-direct {v1, p2, p3, p4}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1$$ExternalSyntheticLambda4;-><init>(III)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v2, "getSurroundingTextFromA11yIme"

    invoke-static {v0, v2, p5, v7, v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->-$$Nest$mdispatchWithTracing(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Ljava/lang/String;Lcom/android/internal/infra/AndroidFuture;Ljava/util/function/Supplier;Ljava/util/function/Function;)V

    return-void
.end method

.method synthetic lambda$clearMetaKeyStates$10$com-android-internal-inputmethod-RemoteInputConnectionImpl$1(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;I)V
    .locals 3

    goto/32 :goto_f

    nop

    :goto_0
    if-eqz v1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_3

    nop

    :goto_1
    if-nez v0, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_15

    nop

    :goto_2
    invoke-virtual {v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_0

    nop

    :goto_3
    goto :goto_a

    :goto_4
    goto/32 :goto_d

    nop

    :goto_5
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_8

    nop

    :goto_6
    return-void

    :goto_7
    goto/32 :goto_b

    nop

    :goto_8
    return-void

    :goto_9
    return-void

    :goto_a
    goto/32 :goto_11

    nop

    :goto_b
    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    goto/32 :goto_13

    nop

    :goto_c
    const-string v2, "clearMetaKeyStates on inactive InputConnection"

    goto/32 :goto_5

    nop

    :goto_d
    invoke-interface {v0, p2}, Landroid/view/inputmethod/InputConnection;->clearMetaKeyStates(I)Z

    goto/32 :goto_9

    nop

    :goto_e
    invoke-static {v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->-$$Nest$fgetmCurrentSessionId(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    goto/32 :goto_14

    nop

    :goto_f
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_10

    nop

    :goto_10
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    goto/32 :goto_e

    nop

    :goto_11
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_c

    nop

    :goto_12
    if-ne v0, v1, :cond_2

    goto/32 :goto_7

    :cond_2
    goto/32 :goto_6

    nop

    :goto_13
    invoke-virtual {v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_14
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_12

    nop

    :goto_15
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    goto/32 :goto_2

    nop
.end method

.method synthetic lambda$commitText$0$com-android-internal-inputmethod-RemoteInputConnectionImpl$1(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Ljava/lang/CharSequence;ILandroid/view/inputmethod/TextAttribute;)V
    .locals 3

    goto/32 :goto_2

    nop

    :goto_0
    invoke-interface {v0}, Landroid/view/inputmethod/InputConnection;->beginBatchEdit()Z

    goto/32 :goto_9

    nop

    :goto_1
    invoke-virtual {v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_2
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_8

    nop

    :goto_3
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_c

    nop

    :goto_4
    if-nez v0, :cond_0

    goto/32 :goto_13

    :cond_0
    goto/32 :goto_b

    nop

    :goto_5
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_17

    nop

    :goto_6
    invoke-interface {v0}, Landroid/view/inputmethod/InputConnection;->endBatchEdit()Z

    goto/32 :goto_12

    nop

    :goto_7
    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    goto/32 :goto_1

    nop

    :goto_8
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    goto/32 :goto_f

    nop

    :goto_9
    invoke-interface {v0}, Landroid/view/inputmethod/InputConnection;->finishComposingText()Z

    goto/32 :goto_a

    nop

    :goto_a
    invoke-interface {v0, p2, p3, p4}, Landroid/view/inputmethod/InputConnection;->commitText(Ljava/lang/CharSequence;ILandroid/view/inputmethod/TextAttribute;)Z

    goto/32 :goto_6

    nop

    :goto_b
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    goto/32 :goto_18

    nop

    :goto_c
    const-string v2, "commitText on inactive InputConnection"

    goto/32 :goto_5

    nop

    :goto_d
    return-void

    :goto_e
    goto/32 :goto_7

    nop

    :goto_f
    invoke-static {v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->-$$Nest$fgetmCurrentSessionId(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    goto/32 :goto_11

    nop

    :goto_10
    if-eqz v1, :cond_1

    goto/32 :goto_15

    :cond_1
    goto/32 :goto_14

    nop

    :goto_11
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_16

    nop

    :goto_12
    return-void

    :goto_13
    goto/32 :goto_3

    nop

    :goto_14
    goto :goto_13

    :goto_15
    goto/32 :goto_0

    nop

    :goto_16
    if-ne v0, v1, :cond_2

    goto/32 :goto_e

    :cond_2
    goto/32 :goto_d

    nop

    :goto_17
    return-void

    :goto_18
    invoke-virtual {v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_10

    nop
.end method

.method synthetic lambda$deleteSurroundingText$4$com-android-internal-inputmethod-RemoteInputConnectionImpl$1(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;II)V
    .locals 3

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_10

    nop

    :goto_1
    const-string v2, "deleteSurroundingText on inactive InputConnection"

    goto/32 :goto_7

    nop

    :goto_2
    invoke-virtual {v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_11

    nop

    :goto_3
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_1

    nop

    :goto_4
    if-nez v0, :cond_0

    goto/32 :goto_15

    :cond_0
    goto/32 :goto_f

    nop

    :goto_5
    return-void

    :goto_6
    goto/32 :goto_8

    nop

    :goto_7
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_a

    nop

    :goto_8
    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    goto/32 :goto_9

    nop

    :goto_9
    invoke-virtual {v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_a
    return-void

    :goto_b
    invoke-static {v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->-$$Nest$fgetmCurrentSessionId(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    goto/32 :goto_e

    nop

    :goto_c
    goto :goto_15

    :goto_d
    goto/32 :goto_13

    nop

    :goto_e
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_12

    nop

    :goto_f
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    goto/32 :goto_2

    nop

    :goto_10
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    goto/32 :goto_b

    nop

    :goto_11
    if-eqz v1, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_c

    nop

    :goto_12
    if-ne v0, v1, :cond_2

    goto/32 :goto_6

    :cond_2
    goto/32 :goto_5

    nop

    :goto_13
    invoke-interface {v0, p2, p3}, Landroid/view/inputmethod/InputConnection;->deleteSurroundingText(II)Z

    goto/32 :goto_14

    nop

    :goto_14
    return-void

    :goto_15
    goto/32 :goto_3

    nop
.end method

.method synthetic lambda$getCursorCapsMode$8$com-android-internal-inputmethod-RemoteInputConnectionImpl$1(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;I)Ljava/lang/Integer;
    .locals 4

    goto/32 :goto_14

    nop

    :goto_0
    invoke-static {v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->-$$Nest$fgetmCurrentSessionId(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    goto/32 :goto_f

    nop

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_8

    nop

    :goto_2
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_18

    nop

    :goto_3
    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    goto/32 :goto_10

    nop

    :goto_4
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    goto/32 :goto_0

    nop

    :goto_5
    invoke-virtual {v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_16

    nop

    :goto_6
    const/4 v2, 0x0

    goto/32 :goto_13

    nop

    :goto_7
    if-ne v0, v1, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_d

    nop

    :goto_8
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    goto/32 :goto_5

    nop

    :goto_9
    goto :goto_12

    :goto_a
    goto/32 :goto_c

    nop

    :goto_b
    return-object v2

    :goto_c
    invoke-interface {v0, p2}, Landroid/view/inputmethod/InputConnection;->getCursorCapsMode(I)I

    move-result v1

    goto/32 :goto_17

    nop

    :goto_d
    return-object v2

    :goto_e
    goto/32 :goto_3

    nop

    :goto_f
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_6

    nop

    :goto_10
    invoke-virtual {v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_11
    return-object v1

    :goto_12
    goto/32 :goto_2

    nop

    :goto_13
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/32 :goto_7

    nop

    :goto_14
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_4

    nop

    :goto_15
    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_b

    nop

    :goto_16
    if-eqz v1, :cond_2

    goto/32 :goto_a

    :cond_2
    goto/32 :goto_9

    nop

    :goto_17
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_11

    nop

    :goto_18
    const-string v3, "getCursorCapsMode on inactive InputConnection"

    goto/32 :goto_15

    nop
.end method

.method synthetic lambda$getSurroundingText$2$com-android-internal-inputmethod-RemoteInputConnectionImpl$1(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;III)Landroid/view/inputmethod/SurroundingText;
    .locals 5

    goto/32 :goto_7

    nop

    :goto_0
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1a

    nop

    :goto_1
    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_11

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_26

    :cond_0
    goto/32 :goto_9

    nop

    :goto_3
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_22

    nop

    :goto_4
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_2

    nop

    :goto_5
    return-object v2

    :goto_6
    goto/32 :goto_1c

    nop

    :goto_7
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_21

    nop

    :goto_8
    invoke-virtual {v3}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v3

    goto/32 :goto_19

    nop

    :goto_9
    iget-object v3, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    goto/32 :goto_8

    nop

    :goto_a
    const-string v4, "Returning null to getSurroundingText due to an invalid afterLength="

    goto/32 :goto_d

    nop

    :goto_b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_e

    nop

    :goto_c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_a

    nop

    :goto_d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_3

    nop

    :goto_e
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_23

    nop

    :goto_f
    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    goto/32 :goto_17

    nop

    :goto_10
    const/4 v2, 0x0

    goto/32 :goto_1f

    nop

    :goto_11
    return-object v2

    :goto_12
    goto/32 :goto_2a

    nop

    :goto_13
    goto :goto_26

    :goto_14
    goto/32 :goto_1e

    nop

    :goto_15
    return-object v2

    :goto_16
    goto/32 :goto_f

    nop

    :goto_17
    invoke-virtual {v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_18
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_10

    nop

    :goto_19
    if-eqz v3, :cond_1

    goto/32 :goto_14

    :cond_1
    goto/32 :goto_13

    nop

    :goto_1a
    const-string v4, "Returning null to getSurroundingText due to an invalid beforeLength="

    goto/32 :goto_b

    nop

    :goto_1b
    invoke-static {v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->-$$Nest$fgetmCurrentSessionId(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    goto/32 :goto_18

    nop

    :goto_1c
    invoke-interface {v0, p2, p3, p4}, Landroid/view/inputmethod/InputConnection;->getSurroundingText(III)Landroid/view/inputmethod/SurroundingText;

    move-result-object v1

    goto/32 :goto_25

    nop

    :goto_1d
    return-object v2

    :goto_1e
    if-ltz p2, :cond_2

    goto/32 :goto_12

    :cond_2
    goto/32 :goto_27

    nop

    :goto_1f
    if-ne v0, v1, :cond_3

    goto/32 :goto_16

    :cond_3
    goto/32 :goto_15

    nop

    :goto_20
    const-string v3, "getSurroundingText on inactive InputConnection"

    goto/32 :goto_28

    nop

    :goto_21
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    goto/32 :goto_1b

    nop

    :goto_22
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_24

    nop

    :goto_23
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_1

    nop

    :goto_24
    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_5

    nop

    :goto_25
    return-object v1

    :goto_26
    goto/32 :goto_20

    nop

    :goto_27
    new-instance v3, Ljava/lang/StringBuilder;

    goto/32 :goto_0

    nop

    :goto_28
    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_1d

    nop

    :goto_29
    new-instance v3, Ljava/lang/StringBuilder;

    goto/32 :goto_c

    nop

    :goto_2a
    if-ltz p3, :cond_4

    goto/32 :goto_6

    :cond_4
    goto/32 :goto_29

    nop
.end method

.method synthetic lambda$performContextMenuAction$7$com-android-internal-inputmethod-RemoteInputConnectionImpl$1(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;I)V
    .locals 3

    goto/32 :goto_e

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_13

    nop

    :goto_1
    if-eqz v1, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_6

    nop

    :goto_2
    invoke-virtual {v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_1

    nop

    :goto_3
    return-void

    :goto_4
    goto/32 :goto_10

    nop

    :goto_5
    return-void

    :goto_6
    goto :goto_a

    :goto_7
    goto/32 :goto_8

    nop

    :goto_8
    invoke-interface {v0, p2}, Landroid/view/inputmethod/InputConnection;->performContextMenuAction(I)Z

    goto/32 :goto_9

    nop

    :goto_9
    return-void

    :goto_a
    goto/32 :goto_11

    nop

    :goto_b
    if-ne v0, v1, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_3

    nop

    :goto_c
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    goto/32 :goto_14

    nop

    :goto_d
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_5

    nop

    :goto_e
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_c

    nop

    :goto_f
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_b

    nop

    :goto_10
    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    goto/32 :goto_12

    nop

    :goto_11
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_15

    nop

    :goto_12
    invoke-virtual {v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_13
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    goto/32 :goto_2

    nop

    :goto_14
    invoke-static {v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->-$$Nest$fgetmCurrentSessionId(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    goto/32 :goto_f

    nop

    :goto_15
    const-string/jumbo v2, "performContextMenuAction on inactive InputConnection"

    goto/32 :goto_d

    nop
.end method

.method synthetic lambda$performEditorAction$6$com-android-internal-inputmethod-RemoteInputConnectionImpl$1(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;I)V
    .locals 3

    goto/32 :goto_4

    nop

    :goto_0
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_e

    nop

    :goto_1
    invoke-interface {v0, p2}, Landroid/view/inputmethod/InputConnection;->performEditorAction(I)Z

    goto/32 :goto_9

    nop

    :goto_2
    return-void

    :goto_3
    goto/32 :goto_b

    nop

    :goto_4
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_10

    nop

    :goto_5
    return-void

    :goto_6
    invoke-virtual {v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_d

    nop

    :goto_7
    goto :goto_a

    :goto_8
    goto/32 :goto_1

    nop

    :goto_9
    return-void

    :goto_a
    goto/32 :goto_11

    nop

    :goto_b
    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    goto/32 :goto_14

    nop

    :goto_c
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    goto/32 :goto_6

    nop

    :goto_d
    if-eqz v1, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_7

    nop

    :goto_e
    if-ne v0, v1, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_2

    nop

    :goto_f
    const-string/jumbo v2, "performEditorAction on inactive InputConnection"

    goto/32 :goto_13

    nop

    :goto_10
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    goto/32 :goto_15

    nop

    :goto_11
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_f

    nop

    :goto_12
    if-nez v0, :cond_2

    goto/32 :goto_a

    :cond_2
    goto/32 :goto_c

    nop

    :goto_13
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_5

    nop

    :goto_14
    invoke-virtual {v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_12

    nop

    :goto_15
    invoke-static {v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->-$$Nest$fgetmCurrentSessionId(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    goto/32 :goto_0

    nop
.end method

.method synthetic lambda$sendKeyEvent$5$com-android-internal-inputmethod-RemoteInputConnectionImpl$1(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Landroid/view/KeyEvent;)V
    .locals 3

    goto/32 :goto_a

    nop

    :goto_0
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    goto/32 :goto_f

    nop

    :goto_1
    return-void

    :goto_2
    goto/32 :goto_b

    nop

    :goto_3
    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    goto/32 :goto_10

    nop

    :goto_4
    invoke-interface {v0, p2}, Landroid/view/inputmethod/InputConnection;->sendKeyEvent(Landroid/view/KeyEvent;)Z

    goto/32 :goto_1

    nop

    :goto_5
    if-eqz v1, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_11

    nop

    :goto_6
    const-string/jumbo v2, "sendKeyEvent on inactive InputConnection"

    goto/32 :goto_14

    nop

    :goto_7
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    goto/32 :goto_8

    nop

    :goto_8
    invoke-static {v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->-$$Nest$fgetmCurrentSessionId(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    goto/32 :goto_15

    nop

    :goto_9
    if-nez v0, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_0

    nop

    :goto_a
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_7

    nop

    :goto_b
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_6

    nop

    :goto_c
    if-ne v0, v1, :cond_2

    goto/32 :goto_e

    :cond_2
    goto/32 :goto_d

    nop

    :goto_d
    return-void

    :goto_e
    goto/32 :goto_3

    nop

    :goto_f
    invoke-virtual {v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_5

    nop

    :goto_10
    invoke-virtual {v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_11
    goto :goto_2

    :goto_12
    goto/32 :goto_4

    nop

    :goto_13
    return-void

    :goto_14
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_13

    nop

    :goto_15
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_c

    nop
.end method

.method synthetic lambda$setSelection$1$com-android-internal-inputmethod-RemoteInputConnectionImpl$1(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;II)V
    .locals 3

    goto/32 :goto_e

    nop

    :goto_0
    invoke-virtual {v0}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->getInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    goto/32 :goto_13

    nop

    :goto_1
    if-ne v0, v1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_3

    nop

    :goto_2
    if-eqz v1, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_5

    nop

    :goto_3
    return-void

    :goto_4
    goto/32 :goto_8

    nop

    :goto_5
    goto :goto_b

    :goto_6
    goto/32 :goto_14

    nop

    :goto_7
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    goto/32 :goto_10

    nop

    :goto_8
    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    goto/32 :goto_0

    nop

    :goto_9
    iget-object v1, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    goto/32 :goto_15

    nop

    :goto_a
    return-void

    :goto_b
    goto/32 :goto_f

    nop

    :goto_c
    const-string/jumbo v2, "setSelection on inactive InputConnection"

    goto/32 :goto_d

    nop

    :goto_d
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_12

    nop

    :goto_e
    iget v0, p1, Lcom/android/internal/inputmethod/InputConnectionCommandHeader;->mSessionId:I

    goto/32 :goto_7

    nop

    :goto_f
    const-string v1, "RemoteInputConnectionImpl"

    goto/32 :goto_c

    nop

    :goto_10
    invoke-static {v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->-$$Nest$fgetmCurrentSessionId(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    goto/32 :goto_11

    nop

    :goto_11
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    goto/32 :goto_1

    nop

    :goto_12
    return-void

    :goto_13
    if-nez v0, :cond_2

    goto/32 :goto_b

    :cond_2
    goto/32 :goto_9

    nop

    :goto_14
    invoke-interface {v0, p2, p3}, Landroid/view/inputmethod/InputConnection;->setSelection(II)Z

    goto/32 :goto_a

    nop

    :goto_15
    invoke-virtual {v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->isActive()Z

    move-result v1

    goto/32 :goto_2

    nop
.end method

.method public performContextMenuAction(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;I)V
    .locals 3

    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    new-instance v1, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1$$ExternalSyntheticLambda0;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;I)V

    const-string/jumbo v2, "performContextMenuActionFromA11yIme"

    invoke-static {v0, v2, v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->-$$Nest$mdispatchWithTracing(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public performEditorAction(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;I)V
    .locals 3

    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    new-instance v1, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1$$ExternalSyntheticLambda9;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1$$ExternalSyntheticLambda9;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;I)V

    const-string/jumbo v2, "performEditorActionFromA11yIme"

    invoke-static {v0, v2, v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->-$$Nest$mdispatchWithTracing(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public sendKeyEvent(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Landroid/view/KeyEvent;)V
    .locals 3

    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    new-instance v1, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1$$ExternalSyntheticLambda10;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1$$ExternalSyntheticLambda10;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;Landroid/view/KeyEvent;)V

    const-string/jumbo v2, "sendKeyEventFromA11yIme"

    invoke-static {v0, v2, v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->-$$Nest$mdispatchWithTracing(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method public setSelection(Lcom/android/internal/inputmethod/InputConnectionCommandHeader;II)V
    .locals 3

    iget-object v0, p0, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;->this$0:Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;

    new-instance v1, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1$$ExternalSyntheticLambda7;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1$$ExternalSyntheticLambda7;-><init>(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl$1;Lcom/android/internal/inputmethod/InputConnectionCommandHeader;II)V

    const-string/jumbo v2, "setSelectionFromA11yIme"

    invoke-static {v0, v2, v1}, Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;->-$$Nest$mdispatchWithTracing(Lcom/android/internal/inputmethod/RemoteInputConnectionImpl;Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method
