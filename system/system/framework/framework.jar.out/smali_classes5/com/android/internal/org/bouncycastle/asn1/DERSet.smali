.class public Lcom/android/internal/org/bouncycastle/asn1/DERSet;
.super Lcom/android/internal/org/bouncycastle/asn1/ASN1Set;


# instance fields
.field private bodyLength:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Set;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSet;->bodyLength:I

    return-void
.end method

.method public constructor <init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Set;-><init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSet;->bodyLength:I

    return-void
.end method

.method public constructor <init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Set;-><init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;Z)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSet;->bodyLength:I

    return-void
.end method

.method constructor <init>(Z[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V
    .locals 1

    invoke-static {p1}, Lcom/android/internal/org/bouncycastle/asn1/DERSet;->checkSorted(Z)Z

    move-result v0

    invoke-direct {p0, v0, p2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Set;-><init>(Z[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSet;->bodyLength:I

    return-void
.end method

.method public constructor <init>([Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Set;-><init>([Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;Z)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSet;->bodyLength:I

    return-void
.end method

.method private static checkSorted(Z)Z
    .locals 2

    if-eqz p0, :cond_0

    return p0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "DERSet elements should always be in sorted order"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static convert(Lcom/android/internal/org/bouncycastle/asn1/ASN1Set;)Lcom/android/internal/org/bouncycastle/asn1/DERSet;
    .locals 1

    invoke-virtual {p0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Set;->toDERObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    check-cast v0, Lcom/android/internal/org/bouncycastle/asn1/DERSet;

    return-object v0
.end method

.method private getBodyLength()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSet;->bodyLength:I

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSet;->elements:[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    array-length v0, v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    iget-object v3, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSet;->elements:[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    aget-object v3, v3, v2

    invoke-interface {v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->toDERObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->encodedLength()I

    move-result v4

    add-int/2addr v1, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iput v1, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSet;->bodyLength:I

    :cond_1
    iget v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSet;->bodyLength:I

    return v0
.end method


# virtual methods
.method encode(Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_1b

    nop

    :goto_0
    invoke-virtual {v4}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->toDERObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v4

    goto/32 :goto_c

    nop

    :goto_1
    invoke-virtual {p1, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    :goto_2
    goto/32 :goto_22

    nop

    :goto_3
    goto/16 :goto_33

    :goto_4
    goto/32 :goto_16

    nop

    :goto_5
    array-length v1, v1

    goto/32 :goto_34

    nop

    :goto_6
    add-int/2addr v2, v7

    goto/32 :goto_26

    nop

    :goto_7
    invoke-virtual {p1, v2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeLength(I)V

    goto/32 :goto_2e

    nop

    :goto_8
    const/16 v2, 0x10

    goto/32 :goto_1c

    nop

    :goto_9
    if-lt v5, v1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_2c

    nop

    :goto_a
    aget-object v6, v4, v5

    goto/32 :goto_1d

    nop

    :goto_b
    aget-object v6, v6, v5

    goto/32 :goto_e

    nop

    :goto_c
    invoke-virtual {v4, v0, v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->encode(Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;Z)V

    goto/32 :goto_15

    nop

    :goto_d
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_20

    nop

    :goto_e
    invoke-interface {v6}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v6

    goto/32 :goto_2b

    nop

    :goto_f
    aget-object v4, v4, v2

    goto/32 :goto_29

    nop

    :goto_10
    new-array v4, v1, [Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    goto/32 :goto_32

    nop

    :goto_11
    goto :goto_21

    :goto_12
    goto/32 :goto_17

    nop

    :goto_13
    if-ltz v2, :cond_1

    goto/32 :goto_21

    :cond_1
    goto/32 :goto_8

    nop

    :goto_14
    iget-object v1, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSet;->elements:[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_5

    nop

    :goto_15
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_18

    nop

    :goto_16
    iput v2, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSet;->bodyLength:I

    goto/32 :goto_7

    nop

    :goto_17
    const/4 v2, 0x0

    goto/32 :goto_10

    nop

    :goto_18
    goto/16 :goto_31

    :goto_19
    goto/32 :goto_27

    nop

    :goto_1a
    aput-object v6, v4, v5

    goto/32 :goto_23

    nop

    :goto_1b
    if-nez p2, :cond_2

    goto/32 :goto_2

    :cond_2
    goto/32 :goto_24

    nop

    :goto_1c
    if-gt v1, v2, :cond_3

    goto/32 :goto_12

    :cond_3
    goto/32 :goto_11

    nop

    :goto_1d
    invoke-virtual {v6, v0, v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->encode(Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;Z)V

    goto/32 :goto_d

    nop

    :goto_1e
    invoke-direct {p0}, Lcom/android/internal/org/bouncycastle/asn1/DERSet;->getBodyLength()I

    move-result v2

    goto/32 :goto_28

    nop

    :goto_1f
    iget-object v4, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSet;->elements:[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_f

    nop

    :goto_20
    goto :goto_2f

    :goto_21
    goto/32 :goto_1e

    nop

    :goto_22
    invoke-virtual {p1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->getDERSubStream()Lcom/android/internal/org/bouncycastle/asn1/DEROutputStream;

    move-result-object v0

    goto/32 :goto_14

    nop

    :goto_23
    invoke-virtual {v6}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->encodedLength()I

    move-result v7

    goto/32 :goto_6

    nop

    :goto_24
    const/16 v0, 0x31

    goto/32 :goto_1

    nop

    :goto_25
    if-lt v5, v1, :cond_4

    goto/32 :goto_19

    :cond_4
    goto/32 :goto_a

    nop

    :goto_26
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_3

    nop

    :goto_27
    return-void

    :goto_28
    invoke-virtual {p1, v2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeLength(I)V

    goto/32 :goto_30

    nop

    :goto_29
    invoke-interface {v4}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v4

    goto/32 :goto_0

    nop

    :goto_2a
    if-lt v2, v1, :cond_5

    goto/32 :goto_19

    :cond_5
    goto/32 :goto_1f

    nop

    :goto_2b
    invoke-virtual {v6}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->toDERObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v6

    goto/32 :goto_1a

    nop

    :goto_2c
    iget-object v6, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSet;->elements:[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_b

    nop

    :goto_2d
    const/4 v3, 0x1

    goto/32 :goto_13

    nop

    :goto_2e
    const/4 v5, 0x0

    :goto_2f
    goto/32 :goto_25

    nop

    :goto_30
    const/4 v2, 0x0

    :goto_31
    goto/32 :goto_2a

    nop

    :goto_32
    const/4 v5, 0x0

    :goto_33
    goto/32 :goto_9

    nop

    :goto_34
    iget v2, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSet;->bodyLength:I

    goto/32 :goto_2d

    nop
.end method

.method encodedLength()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    add-int/2addr v1, v0

    goto/32 :goto_1

    nop

    :goto_1
    return v1

    :goto_2
    invoke-direct {p0}, Lcom/android/internal/org/bouncycastle/asn1/DERSet;->getBodyLength()I

    move-result v0

    goto/32 :goto_3

    nop

    :goto_3
    invoke-static {v0}, Lcom/android/internal/org/bouncycastle/asn1/StreamUtil;->calculateBodyLength(I)I

    move-result v1

    goto/32 :goto_4

    nop

    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_0

    nop
.end method

.method toDERObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-super {p0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Set;->toDERObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    :goto_1
    goto/32 :goto_6

    nop

    :goto_2
    iget-boolean v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERSet;->isSorted:Z

    goto/32 :goto_7

    nop

    :goto_3
    goto :goto_1

    :goto_4
    goto/32 :goto_0

    nop

    :goto_5
    move-object v0, p0

    goto/32 :goto_3

    nop

    :goto_6
    return-object v0

    :goto_7
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_5

    nop
.end method

.method toDLObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    return-object p0
.end method
