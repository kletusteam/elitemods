.class final Lcom/android/internal/app/ChooserActivity$ItemViewHolder;
.super Lcom/android/internal/app/ChooserActivity$ViewHolderBase;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/app/ChooserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "ItemViewHolder"
.end annotation


# instance fields
.field mListPosition:I

.field mWrappedViewHolder:Lcom/android/internal/app/ResolverListAdapter$ViewHolder;

.field final synthetic this$0:Lcom/android/internal/app/ChooserActivity;


# direct methods
.method constructor <init>(Lcom/android/internal/app/ChooserActivity;Landroid/view/View;ZI)V
    .locals 1

    iput-object p1, p0, Lcom/android/internal/app/ChooserActivity$ItemViewHolder;->this$0:Lcom/android/internal/app/ChooserActivity;

    invoke-direct {p0, p2, p4}, Lcom/android/internal/app/ChooserActivity$ViewHolderBase;-><init>(Landroid/view/View;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/app/ChooserActivity$ItemViewHolder;->mListPosition:I

    new-instance v0, Lcom/android/internal/app/ResolverListAdapter$ViewHolder;

    invoke-direct {v0, p2}, Lcom/android/internal/app/ResolverListAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/android/internal/app/ChooserActivity$ItemViewHolder;->mWrappedViewHolder:Lcom/android/internal/app/ResolverListAdapter$ViewHolder;

    if-eqz p3, :cond_0

    new-instance v0, Lcom/android/internal/app/ChooserActivity$ItemViewHolder$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/android/internal/app/ChooserActivity$ItemViewHolder$$ExternalSyntheticLambda0;-><init>(Lcom/android/internal/app/ChooserActivity$ItemViewHolder;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/android/internal/app/ChooserActivity$ItemViewHolder$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/android/internal/app/ChooserActivity$ItemViewHolder$$ExternalSyntheticLambda1;-><init>(Lcom/android/internal/app/ChooserActivity$ItemViewHolder;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :cond_0
    return-void
.end method


# virtual methods
.method synthetic lambda$new$0$com-android-internal-app-ChooserActivity$ItemViewHolder(Landroid/view/View;)V
    .locals 4

    goto/32 :goto_2

    nop

    :goto_0
    const/4 v3, 0x1

    goto/32 :goto_3

    nop

    :goto_1
    const/4 v2, 0x0

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/internal/app/ChooserActivity$ItemViewHolder;->this$0:Lcom/android/internal/app/ChooserActivity;

    goto/32 :goto_5

    nop

    :goto_3
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/app/ChooserActivity;->startSelected(IZZ)V

    goto/32 :goto_4

    nop

    :goto_4
    return-void

    :goto_5
    iget v1, p0, Lcom/android/internal/app/ChooserActivity$ItemViewHolder;->mListPosition:I

    goto/32 :goto_1

    nop
.end method

.method synthetic lambda$new$1$com-android-internal-app-ChooserActivity$ItemViewHolder(Landroid/view/View;)Z
    .locals 4

    goto/32 :goto_6

    nop

    :goto_0
    iget v1, p0, Lcom/android/internal/app/ChooserActivity$ItemViewHolder;->mListPosition:I

    goto/32 :goto_b

    nop

    :goto_1
    iget-object v0, v0, Lcom/android/internal/app/ChooserActivity;->mChooserMultiProfilePagerAdapter:Lcom/android/internal/app/ChooserMultiProfilePagerAdapter;

    goto/32 :goto_5

    nop

    :goto_2
    if-nez v1, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_f

    nop

    :goto_3
    move-object v3, v0

    goto/32 :goto_7

    nop

    :goto_4
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/app/ChooserListAdapter;->targetInfoForPosition(IZ)Lcom/android/internal/app/chooser/TargetInfo;

    move-result-object v0

    goto/32 :goto_a

    nop

    :goto_5
    invoke-virtual {v0}, Lcom/android/internal/app/ChooserMultiProfilePagerAdapter;->getActiveListAdapter()Lcom/android/internal/app/ChooserListAdapter;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_6
    iget-object v0, p0, Lcom/android/internal/app/ChooserActivity$ItemViewHolder;->this$0:Lcom/android/internal/app/ChooserActivity;

    goto/32 :goto_1

    nop

    :goto_7
    check-cast v3, Lcom/android/internal/app/chooser/DisplayResolveInfo;

    goto/32 :goto_c

    nop

    :goto_8
    iget-object v1, p0, Lcom/android/internal/app/ChooserActivity$ItemViewHolder;->this$0:Lcom/android/internal/app/ChooserActivity;

    goto/32 :goto_3

    nop

    :goto_9
    invoke-static {v1, v0}, Lcom/android/internal/app/ChooserActivity;->-$$Nest$mshouldShowTargetDetails(Lcom/android/internal/app/ChooserActivity;Lcom/android/internal/app/chooser/TargetInfo;)Z

    move-result v1

    goto/32 :goto_e

    nop

    :goto_a
    instance-of v1, v0, Lcom/android/internal/app/chooser/DisplayResolveInfo;

    goto/32 :goto_2

    nop

    :goto_b
    const/4 v2, 0x1

    goto/32 :goto_4

    nop

    :goto_c
    invoke-static {v1, v3}, Lcom/android/internal/app/ChooserActivity;->-$$Nest$mshowTargetDetails(Lcom/android/internal/app/ChooserActivity;Lcom/android/internal/app/chooser/TargetInfo;)V

    :goto_d
    goto/32 :goto_10

    nop

    :goto_e
    if-nez v1, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_8

    nop

    :goto_f
    iget-object v1, p0, Lcom/android/internal/app/ChooserActivity$ItemViewHolder;->this$0:Lcom/android/internal/app/ChooserActivity;

    goto/32 :goto_9

    nop

    :goto_10
    return v2
.end method
