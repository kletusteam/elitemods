.class public interface abstract Lcom/android/internal/org/bouncycastle/util/io/pem/PemObjectGenerator;
.super Ljava/lang/Object;


# virtual methods
.method public abstract generate()Lcom/android/internal/org/bouncycastle/util/io/pem/PemObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/org/bouncycastle/util/io/pem/PemGenerationException;
        }
    .end annotation
.end method
