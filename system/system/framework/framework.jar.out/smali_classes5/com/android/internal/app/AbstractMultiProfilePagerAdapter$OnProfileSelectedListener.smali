.class public interface abstract Lcom/android/internal/app/AbstractMultiProfilePagerAdapter$OnProfileSelectedListener;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/app/AbstractMultiProfilePagerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnProfileSelectedListener"
.end annotation


# virtual methods
.method public abstract onProfilePageStateChanged(I)V
.end method

.method public abstract onProfileSelected(I)V
.end method
