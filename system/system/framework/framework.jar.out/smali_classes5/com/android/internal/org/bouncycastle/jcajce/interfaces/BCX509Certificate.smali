.class public interface abstract Lcom/android/internal/org/bouncycastle/jcajce/interfaces/BCX509Certificate;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getIssuerX500Name()Lcom/android/internal/org/bouncycastle/asn1/x500/X500Name;
.end method

.method public abstract getSubjectX500Name()Lcom/android/internal/org/bouncycastle/asn1/x500/X500Name;
.end method

.method public abstract getTBSCertificateNative()Lcom/android/internal/org/bouncycastle/asn1/x509/TBSCertificate;
.end method
