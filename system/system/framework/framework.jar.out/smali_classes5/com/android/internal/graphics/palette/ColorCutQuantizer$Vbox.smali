.class Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/graphics/palette/ColorCutQuantizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Vbox"
.end annotation


# instance fields
.field private final mLowerIndex:I

.field private mMaxBlue:I

.field private mMaxGreen:I

.field private mMaxRed:I

.field private mMinBlue:I

.field private mMinGreen:I

.field private mMinRed:I

.field private mPopulation:I

.field private mUpperIndex:I

.field final synthetic this$0:Lcom/android/internal/graphics/palette/ColorCutQuantizer;


# direct methods
.method constructor <init>(Lcom/android/internal/graphics/palette/ColorCutQuantizer;II)V
    .locals 0

    iput-object p1, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->this$0:Lcom/android/internal/graphics/palette/ColorCutQuantizer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mLowerIndex:I

    iput p3, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mUpperIndex:I

    invoke-virtual {p0}, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->fitBox()V

    return-void
.end method


# virtual methods
.method final canSplit()Z
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0}, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->getColorCount()I

    move-result v0

    goto/32 :goto_1

    nop

    :goto_1
    const/4 v1, 0x1

    goto/32 :goto_5

    nop

    :goto_2
    return v1

    :goto_3
    const/4 v1, 0x0

    :goto_4
    goto/32 :goto_2

    nop

    :goto_5
    if-gt v0, v1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_6

    nop

    :goto_6
    goto :goto_4

    :goto_7
    goto/32 :goto_3

    nop
.end method

.method final findSplitPoint()I
    .locals 8

    goto/32 :goto_21

    nop

    :goto_0
    if-le v4, v6, :cond_0

    goto/32 :goto_1e

    :cond_0
    goto/32 :goto_6

    nop

    :goto_1
    return v4

    :goto_2
    add-int/2addr v5, v7

    goto/32 :goto_1c

    nop

    :goto_3
    const/4 v5, 0x0

    :goto_4
    goto/32 :goto_15

    nop

    :goto_5
    iget v4, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mLowerIndex:I

    goto/32 :goto_1

    nop

    :goto_6
    aget v7, v1, v4

    goto/32 :goto_12

    nop

    :goto_7
    invoke-static {v6, v4}, Ljava/lang/Math;->min(II)I

    move-result v6

    goto/32 :goto_19

    nop

    :goto_8
    invoke-static {v1, v3, v4}, Ljava/util/Arrays;->sort([III)V

    goto/32 :goto_a

    nop

    :goto_9
    iget-object v1, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->this$0:Lcom/android/internal/graphics/palette/ColorCutQuantizer;

    goto/32 :goto_17

    nop

    :goto_a
    iget v3, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mLowerIndex:I

    goto/32 :goto_14

    nop

    :goto_b
    iget v4, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mUpperIndex:I

    goto/32 :goto_16

    nop

    :goto_c
    iget v4, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mUpperIndex:I

    goto/32 :goto_1f

    nop

    :goto_d
    iget v4, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mLowerIndex:I

    goto/32 :goto_3

    nop

    :goto_e
    add-int/lit8 v4, v4, 0x1

    goto/32 :goto_1d

    nop

    :goto_f
    invoke-static {v1, v0, v3, v4}, Lcom/android/internal/graphics/palette/ColorCutQuantizer;->modifySignificantOctet([IIII)V

    goto/32 :goto_1b

    nop

    :goto_10
    div-int/lit8 v3, v3, 0x2

    goto/32 :goto_d

    nop

    :goto_11
    add-int/lit8 v6, v6, -0x1

    goto/32 :goto_7

    nop

    :goto_12
    aget v7, v2, v7

    goto/32 :goto_2

    nop

    :goto_13
    iget-object v2, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->this$0:Lcom/android/internal/graphics/palette/ColorCutQuantizer;

    goto/32 :goto_20

    nop

    :goto_14
    iget v4, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mUpperIndex:I

    goto/32 :goto_f

    nop

    :goto_15
    iget v6, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mUpperIndex:I

    goto/32 :goto_0

    nop

    :goto_16
    add-int/lit8 v4, v4, 0x1

    goto/32 :goto_8

    nop

    :goto_17
    iget-object v1, v1, Lcom/android/internal/graphics/palette/ColorCutQuantizer;->mColors:[I

    goto/32 :goto_13

    nop

    :goto_18
    iget v3, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mLowerIndex:I

    goto/32 :goto_b

    nop

    :goto_19
    return v6

    :goto_1a
    goto/32 :goto_e

    nop

    :goto_1b
    iget v3, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mPopulation:I

    goto/32 :goto_10

    nop

    :goto_1c
    if-ge v5, v3, :cond_1

    goto/32 :goto_1a

    :cond_1
    goto/32 :goto_11

    nop

    :goto_1d
    goto/16 :goto_4

    :goto_1e
    goto/32 :goto_5

    nop

    :goto_1f
    invoke-static {v1, v0, v3, v4}, Lcom/android/internal/graphics/palette/ColorCutQuantizer;->modifySignificantOctet([IIII)V

    goto/32 :goto_18

    nop

    :goto_20
    iget-object v2, v2, Lcom/android/internal/graphics/palette/ColorCutQuantizer;->mHistogram:[I

    goto/32 :goto_22

    nop

    :goto_21
    invoke-virtual {p0}, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->getLongestColorDimension()I

    move-result v0

    goto/32 :goto_9

    nop

    :goto_22
    iget v3, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mLowerIndex:I

    goto/32 :goto_c

    nop
.end method

.method final fitBox()V
    .locals 14

    goto/32 :goto_28

    nop

    :goto_0
    iput v2, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mMinRed:I

    goto/32 :goto_6

    nop

    :goto_1
    aget v10, v0, v9

    goto/32 :goto_5

    nop

    :goto_2
    if-gt v13, v6, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_c

    nop

    :goto_3
    if-le v9, v10, :cond_1

    goto/32 :goto_16

    :cond_1
    goto/32 :goto_1

    nop

    :goto_4
    move v6, v5

    goto/32 :goto_2d

    nop

    :goto_5
    aget v11, v1, v10

    goto/32 :goto_20

    nop

    :goto_6
    iput v5, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mMaxRed:I

    goto/32 :goto_18

    nop

    :goto_7
    move v2, v11

    :goto_8
    goto/32 :goto_a

    nop

    :goto_9
    invoke-static {v10}, Lcom/android/internal/graphics/palette/ColorCutQuantizer;->quantizedBlue(I)I

    move-result v13

    goto/32 :goto_1a

    nop

    :goto_a
    if-gt v12, v7, :cond_2

    goto/32 :goto_1f

    :cond_2
    goto/32 :goto_1e

    nop

    :goto_b
    const v2, 0x7fffffff

    goto/32 :goto_30

    nop

    :goto_c
    move v6, v13

    :goto_d
    goto/32 :goto_1d

    nop

    :goto_e
    iget v9, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mLowerIndex:I

    :goto_f
    goto/32 :goto_21

    nop

    :goto_10
    move v3, v13

    :goto_11
    goto/32 :goto_14

    nop

    :goto_12
    move v5, v11

    :goto_13
    goto/32 :goto_31

    nop

    :goto_14
    add-int/lit8 v9, v9, 0x1

    goto/32 :goto_15

    nop

    :goto_15
    goto :goto_f

    :goto_16
    goto/32 :goto_0

    nop

    :goto_17
    iget-object v1, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->this$0:Lcom/android/internal/graphics/palette/ColorCutQuantizer;

    goto/32 :goto_24

    nop

    :goto_18
    iput v4, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mMinGreen:I

    goto/32 :goto_2b

    nop

    :goto_19
    const/high16 v5, -0x80000000

    goto/32 :goto_4

    nop

    :goto_1a
    if-gt v11, v5, :cond_3

    goto/32 :goto_13

    :cond_3
    goto/32 :goto_12

    nop

    :goto_1b
    move v4, v12

    :goto_1c
    goto/32 :goto_2

    nop

    :goto_1d
    if-lt v13, v3, :cond_4

    goto/32 :goto_11

    :cond_4
    goto/32 :goto_10

    nop

    :goto_1e
    move v7, v12

    :goto_1f
    goto/32 :goto_2f

    nop

    :goto_20
    add-int/2addr v8, v11

    goto/32 :goto_26

    nop

    :goto_21
    iget v10, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mUpperIndex:I

    goto/32 :goto_3

    nop

    :goto_22
    iput v8, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mPopulation:I

    goto/32 :goto_27

    nop

    :goto_23
    iget-object v0, v0, Lcom/android/internal/graphics/palette/ColorCutQuantizer;->mColors:[I

    goto/32 :goto_17

    nop

    :goto_24
    iget-object v1, v1, Lcom/android/internal/graphics/palette/ColorCutQuantizer;->mHistogram:[I

    goto/32 :goto_b

    nop

    :goto_25
    iput v6, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mMaxBlue:I

    goto/32 :goto_22

    nop

    :goto_26
    invoke-static {v10}, Lcom/android/internal/graphics/palette/ColorCutQuantizer;->quantizedRed(I)I

    move-result v11

    goto/32 :goto_2e

    nop

    :goto_27
    return-void

    :goto_28
    iget-object v0, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->this$0:Lcom/android/internal/graphics/palette/ColorCutQuantizer;

    goto/32 :goto_23

    nop

    :goto_29
    move v4, v2

    goto/32 :goto_19

    nop

    :goto_2a
    const/4 v8, 0x0

    goto/32 :goto_e

    nop

    :goto_2b
    iput v7, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mMaxGreen:I

    goto/32 :goto_2c

    nop

    :goto_2c
    iput v3, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mMinBlue:I

    goto/32 :goto_25

    nop

    :goto_2d
    move v7, v5

    goto/32 :goto_2a

    nop

    :goto_2e
    invoke-static {v10}, Lcom/android/internal/graphics/palette/ColorCutQuantizer;->quantizedGreen(I)I

    move-result v12

    goto/32 :goto_9

    nop

    :goto_2f
    if-lt v12, v4, :cond_5

    goto/32 :goto_1c

    :cond_5
    goto/32 :goto_1b

    nop

    :goto_30
    move v3, v2

    goto/32 :goto_29

    nop

    :goto_31
    if-lt v11, v2, :cond_6

    goto/32 :goto_8

    :cond_6
    goto/32 :goto_7

    nop
.end method

.method final getAverageColor()Lcom/android/internal/graphics/palette/Palette$Swatch;
    .locals 11

    goto/32 :goto_1d

    nop

    :goto_0
    mul-int/2addr v9, v8

    goto/32 :goto_19

    nop

    :goto_1
    const/4 v2, 0x0

    goto/32 :goto_12

    nop

    :goto_2
    int-to-float v7, v5

    goto/32 :goto_1c

    nop

    :goto_3
    iget-object v1, v1, Lcom/android/internal/graphics/palette/ColorCutQuantizer;->mHistogram:[I

    goto/32 :goto_1

    nop

    :goto_4
    add-int/2addr v2, v9

    goto/32 :goto_27

    nop

    :goto_5
    add-int/lit8 v6, v6, 0x1

    goto/32 :goto_17

    nop

    :goto_6
    invoke-static {v7}, Lcom/android/internal/graphics/palette/ColorCutQuantizer;->quantizedRed(I)I

    move-result v9

    goto/32 :goto_20

    nop

    :goto_7
    iget v6, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mLowerIndex:I

    :goto_8
    goto/32 :goto_23

    nop

    :goto_9
    const/4 v4, 0x0

    goto/32 :goto_2a

    nop

    :goto_a
    int-to-float v9, v5

    goto/32 :goto_e

    nop

    :goto_b
    new-instance v9, Lcom/android/internal/graphics/palette/Palette$Swatch;

    goto/32 :goto_15

    nop

    :goto_c
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    goto/32 :goto_28

    nop

    :goto_d
    iget-object v0, v0, Lcom/android/internal/graphics/palette/ColorCutQuantizer;->mColors:[I

    goto/32 :goto_24

    nop

    :goto_e
    div-float/2addr v8, v9

    goto/32 :goto_1a

    nop

    :goto_f
    invoke-direct {v9, v10, v5}, Lcom/android/internal/graphics/palette/Palette$Swatch;-><init>(II)V

    goto/32 :goto_1f

    nop

    :goto_10
    int-to-float v8, v5

    goto/32 :goto_21

    nop

    :goto_11
    aget v7, v0, v6

    goto/32 :goto_1e

    nop

    :goto_12
    const/4 v3, 0x0

    goto/32 :goto_9

    nop

    :goto_13
    add-int/2addr v4, v9

    goto/32 :goto_5

    nop

    :goto_14
    int-to-float v6, v2

    goto/32 :goto_2

    nop

    :goto_15
    invoke-static {v6, v7, v8}, Lcom/android/internal/graphics/palette/ColorCutQuantizer;->approximateToRgb888(III)I

    move-result v10

    goto/32 :goto_f

    nop

    :goto_16
    mul-int/2addr v9, v8

    goto/32 :goto_13

    nop

    :goto_17
    goto :goto_8

    :goto_18
    goto/32 :goto_14

    nop

    :goto_19
    add-int/2addr v3, v9

    goto/32 :goto_29

    nop

    :goto_1a
    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    goto/32 :goto_b

    nop

    :goto_1b
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    goto/32 :goto_26

    nop

    :goto_1c
    div-float/2addr v6, v7

    goto/32 :goto_1b

    nop

    :goto_1d
    iget-object v0, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->this$0:Lcom/android/internal/graphics/palette/ColorCutQuantizer;

    goto/32 :goto_d

    nop

    :goto_1e
    aget v8, v1, v7

    goto/32 :goto_22

    nop

    :goto_1f
    return-object v9

    :goto_20
    mul-int/2addr v9, v8

    goto/32 :goto_4

    nop

    :goto_21
    div-float/2addr v7, v8

    goto/32 :goto_c

    nop

    :goto_22
    add-int/2addr v5, v8

    goto/32 :goto_6

    nop

    :goto_23
    iget v7, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mUpperIndex:I

    goto/32 :goto_25

    nop

    :goto_24
    iget-object v1, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->this$0:Lcom/android/internal/graphics/palette/ColorCutQuantizer;

    goto/32 :goto_3

    nop

    :goto_25
    if-le v6, v7, :cond_0

    goto/32 :goto_18

    :cond_0
    goto/32 :goto_11

    nop

    :goto_26
    int-to-float v7, v3

    goto/32 :goto_10

    nop

    :goto_27
    invoke-static {v7}, Lcom/android/internal/graphics/palette/ColorCutQuantizer;->quantizedGreen(I)I

    move-result v9

    goto/32 :goto_0

    nop

    :goto_28
    int-to-float v8, v4

    goto/32 :goto_a

    nop

    :goto_29
    invoke-static {v7}, Lcom/android/internal/graphics/palette/ColorCutQuantizer;->quantizedBlue(I)I

    move-result v9

    goto/32 :goto_16

    nop

    :goto_2a
    const/4 v5, 0x0

    goto/32 :goto_7

    nop
.end method

.method final getColorCount()I
    .locals 2

    goto/32 :goto_4

    nop

    :goto_0
    sub-int/2addr v0, v1

    goto/32 :goto_2

    nop

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_3

    nop

    :goto_2
    return v0

    :goto_3
    iget v1, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mLowerIndex:I

    goto/32 :goto_0

    nop

    :goto_4
    iget v0, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mUpperIndex:I

    goto/32 :goto_1

    nop
.end method

.method final getLongestColorDimension()I
    .locals 4

    goto/32 :goto_c

    nop

    :goto_0
    iget v1, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mMaxGreen:I

    goto/32 :goto_b

    nop

    :goto_1
    sub-int/2addr v1, v2

    goto/32 :goto_d

    nop

    :goto_2
    const/4 v3, -0x1

    goto/32 :goto_4

    nop

    :goto_3
    if-ge v1, v2, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_5

    nop

    :goto_4
    return v3

    :goto_5
    const/4 v3, -0x2

    goto/32 :goto_6

    nop

    :goto_6
    return v3

    :goto_7
    goto/32 :goto_2

    nop

    :goto_8
    return v3

    :goto_9
    goto/32 :goto_f

    nop

    :goto_a
    if-ge v0, v2, :cond_1

    goto/32 :goto_9

    :cond_1
    goto/32 :goto_10

    nop

    :goto_b
    iget v2, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mMinGreen:I

    goto/32 :goto_1

    nop

    :goto_c
    iget v0, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mMaxRed:I

    goto/32 :goto_13

    nop

    :goto_d
    iget v2, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mMaxBlue:I

    goto/32 :goto_12

    nop

    :goto_e
    sub-int/2addr v0, v1

    goto/32 :goto_0

    nop

    :goto_f
    if-ge v1, v0, :cond_2

    goto/32 :goto_7

    :cond_2
    goto/32 :goto_3

    nop

    :goto_10
    const/4 v3, -0x3

    goto/32 :goto_8

    nop

    :goto_11
    if-ge v0, v1, :cond_3

    goto/32 :goto_9

    :cond_3
    goto/32 :goto_a

    nop

    :goto_12
    iget v3, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mMinBlue:I

    goto/32 :goto_14

    nop

    :goto_13
    iget v1, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mMinRed:I

    goto/32 :goto_e

    nop

    :goto_14
    sub-int/2addr v2, v3

    goto/32 :goto_11

    nop
.end method

.method final getVolume()I
    .locals 3

    goto/32 :goto_5

    nop

    :goto_0
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_e

    nop

    :goto_1
    iget v2, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mMinGreen:I

    goto/32 :goto_9

    nop

    :goto_2
    iget v1, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mMaxBlue:I

    goto/32 :goto_d

    nop

    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_6

    nop

    :goto_4
    return v0

    :goto_5
    iget v0, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mMaxRed:I

    goto/32 :goto_b

    nop

    :goto_6
    mul-int/2addr v0, v1

    goto/32 :goto_2

    nop

    :goto_7
    sub-int/2addr v0, v1

    goto/32 :goto_0

    nop

    :goto_8
    mul-int/2addr v0, v1

    goto/32 :goto_4

    nop

    :goto_9
    sub-int/2addr v1, v2

    goto/32 :goto_3

    nop

    :goto_a
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_8

    nop

    :goto_b
    iget v1, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mMinRed:I

    goto/32 :goto_7

    nop

    :goto_c
    sub-int/2addr v1, v2

    goto/32 :goto_a

    nop

    :goto_d
    iget v2, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mMinBlue:I

    goto/32 :goto_c

    nop

    :goto_e
    iget v1, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mMaxGreen:I

    goto/32 :goto_1

    nop
.end method

.method final splitBox()Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;
    .locals 5

    goto/32 :goto_6

    nop

    :goto_0
    const-string v1, "Can not split a box with only 1 color"

    goto/32 :goto_c

    nop

    :goto_1
    return-object v1

    :goto_2
    goto/32 :goto_5

    nop

    :goto_3
    add-int/lit8 v3, v0, 0x1

    goto/32 :goto_7

    nop

    :goto_4
    invoke-virtual {p0}, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->findSplitPoint()I

    move-result v0

    goto/32 :goto_b

    nop

    :goto_5
    new-instance v0, Ljava/lang/IllegalStateException;

    goto/32 :goto_0

    nop

    :goto_6
    invoke-virtual {p0}, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->canSplit()Z

    move-result v0

    goto/32 :goto_f

    nop

    :goto_7
    iget v4, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mUpperIndex:I

    goto/32 :goto_8

    nop

    :goto_8
    invoke-direct {v1, v2, v3, v4}, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;-><init>(Lcom/android/internal/graphics/palette/ColorCutQuantizer;II)V

    goto/32 :goto_9

    nop

    :goto_9
    iput v0, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->mUpperIndex:I

    goto/32 :goto_e

    nop

    :goto_a
    throw v0

    :goto_b
    new-instance v1, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;

    goto/32 :goto_d

    nop

    :goto_c
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_a

    nop

    :goto_d
    iget-object v2, p0, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->this$0:Lcom/android/internal/graphics/palette/ColorCutQuantizer;

    goto/32 :goto_3

    nop

    :goto_e
    invoke-virtual {p0}, Lcom/android/internal/graphics/palette/ColorCutQuantizer$Vbox;->fitBox()V

    goto/32 :goto_1

    nop

    :goto_f
    if-nez v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_4

    nop
.end method
