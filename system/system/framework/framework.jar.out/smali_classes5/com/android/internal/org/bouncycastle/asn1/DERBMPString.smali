.class public Lcom/android/internal/org/bouncycastle/asn1/DERBMPString;
.super Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

# interfaces
.implements Lcom/android/internal/org/bouncycastle/asn1/ASN1String;


# instance fields
.field private final string:[C


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;-><init>()V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    iput-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERBMPString;->string:[C

    return-void

    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "\'string\' cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method constructor <init>([B)V
    .locals 6

    invoke-direct {p0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;-><init>()V

    if-eqz p1, :cond_2

    array-length v0, p1

    and-int/lit8 v1, v0, 0x1

    if-nez v1, :cond_1

    div-int/lit8 v1, v0, 0x2

    new-array v2, v1, [C

    const/4 v3, 0x0

    :goto_0
    if-eq v3, v1, :cond_0

    mul-int/lit8 v4, v3, 0x2

    aget-byte v4, p1, v4

    shl-int/lit8 v4, v4, 0x8

    mul-int/lit8 v5, v3, 0x2

    add-int/lit8 v5, v5, 0x1

    aget-byte v5, p1, v5

    and-int/lit16 v5, v5, 0xff

    or-int/2addr v4, v5

    int-to-char v4, v4

    aput-char v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    iput-object v2, p0, Lcom/android/internal/org/bouncycastle/asn1/DERBMPString;->string:[C

    return-void

    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "malformed BMPString encoding encountered"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "\'string\' cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method constructor <init>([C)V
    .locals 2

    invoke-direct {p0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;-><init>()V

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/android/internal/org/bouncycastle/asn1/DERBMPString;->string:[C

    return-void

    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "\'string\' cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static getInstance(Lcom/android/internal/org/bouncycastle/asn1/ASN1TaggedObject;Z)Lcom/android/internal/org/bouncycastle/asn1/DERBMPString;
    .locals 3

    invoke-virtual {p0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1TaggedObject;->getObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    if-nez p1, :cond_1

    instance-of v1, v0, Lcom/android/internal/org/bouncycastle/asn1/DERBMPString;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/android/internal/org/bouncycastle/asn1/DERBMPString;

    invoke-static {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OctetString;->getInstance(Ljava/lang/Object;)Lcom/android/internal/org/bouncycastle/asn1/ASN1OctetString;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OctetString;->getOctets()[B

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/internal/org/bouncycastle/asn1/DERBMPString;-><init>([B)V

    return-object v1

    :cond_1
    :goto_0
    invoke-static {v0}, Lcom/android/internal/org/bouncycastle/asn1/DERBMPString;->getInstance(Ljava/lang/Object;)Lcom/android/internal/org/bouncycastle/asn1/DERBMPString;

    move-result-object v1

    return-object v1
.end method

.method public static getInstance(Ljava/lang/Object;)Lcom/android/internal/org/bouncycastle/asn1/DERBMPString;
    .locals 4

    if-eqz p0, :cond_2

    instance-of v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERBMPString;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    instance-of v0, p0, [B

    if-eqz v0, :cond_1

    :try_start_0
    move-object v0, p0

    check-cast v0, [B

    invoke-static {v0}, Lcom/android/internal/org/bouncycastle/asn1/DERBMPString;->fromByteArray([B)Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    check-cast v0, Lcom/android/internal/org/bouncycastle/asn1/DERBMPString;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "encoding error in getInstance: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "illegal object in getInstance: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    :goto_0
    move-object v0, p0

    check-cast v0, Lcom/android/internal/org/bouncycastle/asn1/DERBMPString;

    return-object v0
.end method


# virtual methods
.method protected asn1Equals(Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;)Z
    .locals 3

    instance-of v0, p1, Lcom/android/internal/org/bouncycastle/asn1/DERBMPString;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/android/internal/org/bouncycastle/asn1/DERBMPString;

    iget-object v1, p0, Lcom/android/internal/org/bouncycastle/asn1/DERBMPString;->string:[C

    iget-object v2, v0, Lcom/android/internal/org/bouncycastle/asn1/DERBMPString;->string:[C

    invoke-static {v1, v2}, Lcom/android/internal/org/bouncycastle/util/Arrays;->areEqual([C[C)Z

    move-result v1

    return v1
.end method

.method encode(Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;Z)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_34

    nop

    :goto_0
    const/4 v11, 0x4

    goto/32 :goto_1e

    nop

    :goto_1
    add-int/lit8 v9, v3, 0x1

    goto/32 :goto_40

    nop

    :goto_2
    const/4 v6, 0x7

    goto/32 :goto_2d

    nop

    :goto_3
    int-to-byte v11, v11

    goto/32 :goto_18

    nop

    :goto_4
    const/16 v1, 0x1e

    goto/32 :goto_39

    nop

    :goto_5
    aput-byte v6, v2, v11

    goto/32 :goto_27

    nop

    :goto_6
    iget-object v7, p0, Lcom/android/internal/org/bouncycastle/asn1/DERBMPString;->string:[C

    goto/32 :goto_8

    nop

    :goto_7
    int-to-byte v11, v11

    goto/32 :goto_15

    nop

    :goto_8
    aget-char v7, v7, v3

    goto/32 :goto_2b

    nop

    :goto_9
    const/4 v11, 0x3

    goto/32 :goto_5

    nop

    :goto_a
    const/4 v11, 0x2

    goto/32 :goto_21

    nop

    :goto_b
    if-lt v3, v4, :cond_0

    goto/32 :goto_2a

    :cond_0
    goto/32 :goto_2c

    nop

    :goto_c
    aget-char v10, v7, v10

    goto/32 :goto_46

    nop

    :goto_d
    mul-int/lit8 v1, v0, 0x2

    goto/32 :goto_24

    nop

    :goto_e
    return-void

    :goto_f
    int-to-byte v6, v9

    goto/32 :goto_9

    nop

    :goto_10
    aput-byte v11, v2, v6

    goto/32 :goto_38

    nop

    :goto_11
    if-nez p2, :cond_1

    goto/32 :goto_3a

    :cond_1
    goto/32 :goto_4

    nop

    :goto_12
    aput-byte v11, v2, v6

    goto/32 :goto_37

    nop

    :goto_13
    int-to-byte v9, v9

    goto/32 :goto_16

    nop

    :goto_14
    int-to-byte v6, v6

    goto/32 :goto_a

    nop

    :goto_15
    aput-byte v11, v2, v6

    goto/32 :goto_2

    nop

    :goto_16
    aput-byte v9, v2, v1

    goto/32 :goto_1d

    nop

    :goto_17
    if-lt v3, v0, :cond_2

    goto/32 :goto_3c

    :cond_2
    goto/32 :goto_25

    nop

    :goto_18
    aput-byte v11, v2, v5

    goto/32 :goto_44

    nop

    :goto_19
    and-int/lit8 v4, v0, -0x4

    :goto_1a
    goto/32 :goto_3f

    nop

    :goto_1b
    aput-byte v9, v2, v8

    goto/32 :goto_1c

    nop

    :goto_1c
    if-ge v3, v0, :cond_3

    goto/32 :goto_26

    :cond_3
    goto/32 :goto_3b

    nop

    :goto_1d
    add-int/lit8 v1, v8, 0x1

    goto/32 :goto_31

    nop

    :goto_1e
    aput-byte v6, v2, v11

    goto/32 :goto_41

    nop

    :goto_1f
    const/16 v1, 0x8

    goto/32 :goto_2f

    nop

    :goto_20
    shr-int/lit8 v11, v8, 0x8

    goto/32 :goto_3

    nop

    :goto_21
    aput-byte v6, v2, v11

    goto/32 :goto_f

    nop

    :goto_22
    int-to-byte v11, v10

    goto/32 :goto_10

    nop

    :goto_23
    const/4 v3, 0x0

    goto/32 :goto_19

    nop

    :goto_24
    invoke-virtual {p1, v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeLength(I)V

    goto/32 :goto_1f

    nop

    :goto_25
    const/4 v1, 0x0

    :goto_26
    goto/32 :goto_6

    nop

    :goto_27
    shr-int/lit8 v6, v10, 0x8

    goto/32 :goto_3d

    nop

    :goto_28
    array-length v0, v0

    goto/32 :goto_11

    nop

    :goto_29
    goto :goto_1a

    :goto_2a
    goto/32 :goto_17

    nop

    :goto_2b
    add-int/2addr v3, v6

    goto/32 :goto_42

    nop

    :goto_2c
    iget-object v7, p0, Lcom/android/internal/org/bouncycastle/asn1/DERBMPString;->string:[C

    goto/32 :goto_35

    nop

    :goto_2d
    int-to-byte v11, v7

    goto/32 :goto_12

    nop

    :goto_2e
    const/4 v6, 0x1

    goto/32 :goto_b

    nop

    :goto_2f
    new-array v2, v1, [B

    goto/32 :goto_23

    nop

    :goto_30
    shr-int/lit8 v11, v7, 0x8

    goto/32 :goto_7

    nop

    :goto_31
    int-to-byte v9, v7

    goto/32 :goto_1b

    nop

    :goto_32
    shr-int/lit8 v6, v9, 0x8

    goto/32 :goto_14

    nop

    :goto_33
    shr-int/lit8 v9, v7, 0x8

    goto/32 :goto_13

    nop

    :goto_34
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERBMPString;->string:[C

    goto/32 :goto_28

    nop

    :goto_35
    aget-char v8, v7, v3

    goto/32 :goto_1

    nop

    :goto_36
    add-int/lit8 v3, v3, 0x4

    goto/32 :goto_20

    nop

    :goto_37
    invoke-virtual {p1, v2, v5, v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write([BII)V

    goto/32 :goto_29

    nop

    :goto_38
    const/4 v6, 0x6

    goto/32 :goto_30

    nop

    :goto_39
    invoke-virtual {p1, v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    :goto_3a
    goto/32 :goto_d

    nop

    :goto_3b
    invoke-virtual {p1, v2, v5, v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write([BII)V

    :goto_3c
    goto/32 :goto_e

    nop

    :goto_3d
    int-to-byte v6, v6

    goto/32 :goto_0

    nop

    :goto_3e
    add-int/lit8 v10, v3, 0x2

    goto/32 :goto_c

    nop

    :goto_3f
    const/4 v5, 0x0

    goto/32 :goto_2e

    nop

    :goto_40
    aget-char v9, v7, v9

    goto/32 :goto_3e

    nop

    :goto_41
    const/4 v6, 0x5

    goto/32 :goto_22

    nop

    :goto_42
    add-int/lit8 v8, v1, 0x1

    goto/32 :goto_33

    nop

    :goto_43
    aget-char v7, v7, v11

    goto/32 :goto_36

    nop

    :goto_44
    int-to-byte v11, v8

    goto/32 :goto_45

    nop

    :goto_45
    aput-byte v11, v2, v6

    goto/32 :goto_32

    nop

    :goto_46
    add-int/lit8 v11, v3, 0x3

    goto/32 :goto_43

    nop
.end method

.method encodedLength()I
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    return v0

    :goto_1
    iget-object v1, p0, Lcom/android/internal/org/bouncycastle/asn1/DERBMPString;->string:[C

    goto/32 :goto_4

    nop

    :goto_2
    invoke-static {v0}, Lcom/android/internal/org/bouncycastle/asn1/StreamUtil;->calculateBodyLength(I)I

    move-result v0

    goto/32 :goto_8

    nop

    :goto_3
    array-length v0, v0

    goto/32 :goto_6

    nop

    :goto_4
    array-length v1, v1

    goto/32 :goto_7

    nop

    :goto_5
    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERBMPString;->string:[C

    goto/32 :goto_3

    nop

    :goto_6
    mul-int/lit8 v0, v0, 0x2

    goto/32 :goto_2

    nop

    :goto_7
    mul-int/lit8 v1, v1, 0x2

    goto/32 :goto_9

    nop

    :goto_8
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_1

    nop

    :goto_9
    add-int/2addr v0, v1

    goto/32 :goto_0

    nop
.end method

.method public getString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/android/internal/org/bouncycastle/asn1/DERBMPString;->string:[C

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DERBMPString;->string:[C

    invoke-static {v0}, Lcom/android/internal/org/bouncycastle/util/Arrays;->hashCode([C)I

    move-result v0

    return v0
.end method

.method isConstructed()Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/android/internal/org/bouncycastle/asn1/DERBMPString;->getString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
