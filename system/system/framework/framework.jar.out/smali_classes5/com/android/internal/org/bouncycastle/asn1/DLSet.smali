.class public Lcom/android/internal/org/bouncycastle/asn1/DLSet;
.super Lcom/android/internal/org/bouncycastle/asn1/ASN1Set;


# instance fields
.field private bodyLength:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Set;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSet;->bodyLength:I

    return-void
.end method

.method public constructor <init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Set;-><init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSet;->bodyLength:I

    return-void
.end method

.method public constructor <init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Set;-><init>(Lcom/android/internal/org/bouncycastle/asn1/ASN1EncodableVector;Z)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSet;->bodyLength:I

    return-void
.end method

.method constructor <init>(Z[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Set;-><init>(Z[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSet;->bodyLength:I

    return-void
.end method

.method public constructor <init>([Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Set;-><init>([Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;Z)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSet;->bodyLength:I

    return-void
.end method

.method private getBodyLength()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSet;->bodyLength:I

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSet;->elements:[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    array-length v0, v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    iget-object v3, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSet;->elements:[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    aget-object v3, v3, v2

    invoke-interface {v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->toDLObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->encodedLength()I

    move-result v4

    add-int/2addr v1, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iput v1, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSet;->bodyLength:I

    :cond_1
    iget v0, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSet;->bodyLength:I

    return v0
.end method


# virtual methods
.method encode(Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_23

    nop

    :goto_0
    const/16 v2, 0x10

    goto/32 :goto_12

    nop

    :goto_1
    invoke-virtual {v6}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->toDLObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v6

    goto/32 :goto_24

    nop

    :goto_2
    goto :goto_14

    :goto_3
    goto/32 :goto_b

    nop

    :goto_4
    if-lt v5, v1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_7

    nop

    :goto_5
    const/4 v3, 0x1

    goto/32 :goto_26

    nop

    :goto_6
    if-lt v5, v1, :cond_1

    goto/32 :goto_18

    :cond_1
    goto/32 :goto_2e

    nop

    :goto_7
    iget-object v6, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSet;->elements:[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_27

    nop

    :goto_8
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_2

    nop

    :goto_9
    const/4 v5, 0x0

    :goto_a
    goto/32 :goto_6

    nop

    :goto_b
    iput v2, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSet;->bodyLength:I

    goto/32 :goto_21

    nop

    :goto_c
    const/4 v2, 0x0

    goto/32 :goto_28

    nop

    :goto_d
    if-lt v2, v1, :cond_2

    goto/32 :goto_18

    :cond_2
    goto/32 :goto_25

    nop

    :goto_e
    aget-object v4, v4, v2

    goto/32 :goto_2d

    nop

    :goto_f
    invoke-virtual {p1, v2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeLength(I)V

    goto/32 :goto_2f

    nop

    :goto_10
    invoke-virtual {p1, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->write(I)V

    :goto_11
    goto/32 :goto_19

    nop

    :goto_12
    if-gt v1, v2, :cond_3

    goto/32 :goto_16

    :cond_3
    goto/32 :goto_15

    nop

    :goto_13
    const/4 v5, 0x0

    :goto_14
    goto/32 :goto_4

    nop

    :goto_15
    goto :goto_1f

    :goto_16
    goto/32 :goto_c

    nop

    :goto_17
    goto/16 :goto_30

    :goto_18
    goto/32 :goto_1b

    nop

    :goto_19
    invoke-virtual {p1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->getDLSubStream()Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;

    move-result-object v0

    goto/32 :goto_31

    nop

    :goto_1a
    invoke-virtual {v6}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;->encodedLength()I

    move-result v7

    goto/32 :goto_2a

    nop

    :goto_1b
    return-void

    :goto_1c
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_1e

    nop

    :goto_1d
    invoke-interface {v6}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v6

    goto/32 :goto_1

    nop

    :goto_1e
    goto :goto_a

    :goto_1f
    goto/32 :goto_2b

    nop

    :goto_20
    const/16 v0, 0x31

    goto/32 :goto_10

    nop

    :goto_21
    invoke-virtual {p1, v2}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeLength(I)V

    goto/32 :goto_9

    nop

    :goto_22
    invoke-virtual {v0, v4, v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writePrimitive(Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;Z)V

    goto/32 :goto_29

    nop

    :goto_23
    if-nez p2, :cond_4

    goto/32 :goto_11

    :cond_4
    goto/32 :goto_20

    nop

    :goto_24
    aput-object v6, v4, v5

    goto/32 :goto_1a

    nop

    :goto_25
    iget-object v4, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSet;->elements:[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_e

    nop

    :goto_26
    if-ltz v2, :cond_5

    goto/32 :goto_1f

    :cond_5
    goto/32 :goto_0

    nop

    :goto_27
    aget-object v6, v6, v5

    goto/32 :goto_1d

    nop

    :goto_28
    new-array v4, v1, [Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    goto/32 :goto_13

    nop

    :goto_29
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_17

    nop

    :goto_2a
    add-int/2addr v2, v7

    goto/32 :goto_8

    nop

    :goto_2b
    invoke-direct {p0}, Lcom/android/internal/org/bouncycastle/asn1/DLSet;->getBodyLength()I

    move-result v2

    goto/32 :goto_f

    nop

    :goto_2c
    array-length v1, v1

    goto/32 :goto_33

    nop

    :goto_2d
    invoke-interface {v4}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;->toASN1Primitive()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v4

    goto/32 :goto_22

    nop

    :goto_2e
    aget-object v6, v4, v5

    goto/32 :goto_32

    nop

    :goto_2f
    const/4 v2, 0x0

    :goto_30
    goto/32 :goto_d

    nop

    :goto_31
    iget-object v1, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSet;->elements:[Lcom/android/internal/org/bouncycastle/asn1/ASN1Encodable;

    goto/32 :goto_2c

    nop

    :goto_32
    invoke-virtual {v0, v6, v3}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writePrimitive(Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;Z)V

    goto/32 :goto_1c

    nop

    :goto_33
    iget v2, p0, Lcom/android/internal/org/bouncycastle/asn1/DLSet;->bodyLength:I

    goto/32 :goto_5

    nop
.end method

.method encodedLength()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    invoke-static {v0}, Lcom/android/internal/org/bouncycastle/asn1/StreamUtil;->calculateBodyLength(I)I

    move-result v1

    goto/32 :goto_1

    nop

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_4

    nop

    :goto_2
    invoke-direct {p0}, Lcom/android/internal/org/bouncycastle/asn1/DLSet;->getBodyLength()I

    move-result v0

    goto/32 :goto_0

    nop

    :goto_3
    return v1

    :goto_4
    add-int/2addr v1, v0

    goto/32 :goto_3

    nop
.end method

.method toDLObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    return-object p0
.end method
