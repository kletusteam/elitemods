.class public Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;
.super Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;


# static fields
.field public static final FALSE:Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;

.field private static final FALSE_VALUE:B = 0x0t

.field public static final TRUE:Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;

.field private static final TRUE_VALUE:B = -0x1t


# instance fields
.field private final value:B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;-><init>(B)V

    sput-object v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;->FALSE:Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;

    new-instance v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;-><init>(B)V

    sput-object v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;->TRUE:Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;

    return-void
.end method

.method private constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;-><init>()V

    iput-byte p1, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;->value:B

    return-void
.end method

.method static fromOctetString([B)Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;
    .locals 2

    array-length v0, p0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    aget-byte v0, p0, v0

    packed-switch v0, :pswitch_data_0

    new-instance v1, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;

    invoke-direct {v1, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;-><init>(B)V

    return-object v1

    :pswitch_0
    sget-object v1, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;->FALSE:Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;

    return-object v1

    :pswitch_1
    sget-object v1, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;->TRUE:Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;

    return-object v1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "BOOLEAN value should have 1 byte in it"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getInstance(I)Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;
    .locals 1

    if-eqz p0, :cond_0

    sget-object v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;->TRUE:Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;->FALSE:Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;

    :goto_0
    return-object v0
.end method

.method public static getInstance(Lcom/android/internal/org/bouncycastle/asn1/ASN1TaggedObject;Z)Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;
    .locals 2

    invoke-virtual {p0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1TaggedObject;->getObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v0

    if-nez p1, :cond_1

    instance-of v1, v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OctetString;->getInstance(Ljava/lang/Object;)Lcom/android/internal/org/bouncycastle/asn1/ASN1OctetString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OctetString;->getOctets()[B

    move-result-object v1

    invoke-static {v1}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;->fromOctetString([B)Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;

    move-result-object v1

    return-object v1

    :cond_1
    :goto_0
    invoke-static {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;->getInstance(Ljava/lang/Object;)Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;

    move-result-object v1

    return-object v1
.end method

.method public static getInstance(Ljava/lang/Object;)Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;
    .locals 5

    if-eqz p0, :cond_2

    instance-of v0, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    instance-of v0, p0, [B

    if-eqz v0, :cond_1

    move-object v0, p0

    check-cast v0, [B

    :try_start_0
    invoke-static {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;->fromByteArray([B)Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v1

    check-cast v1, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "failed to construct boolean from byte[]: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "illegal object in getInstance: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    :goto_0
    move-object v0, p0

    check-cast v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;

    return-object v0
.end method

.method public static getInstance(Z)Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;
    .locals 1

    if-eqz p0, :cond_0

    sget-object v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;->TRUE:Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;->FALSE:Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;

    :goto_0
    return-object v0
.end method

.method public static getInstance([B)Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;
    .locals 1

    const/4 v0, 0x0

    aget-byte v0, p0, v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;->TRUE:Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;->FALSE:Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;

    :goto_0
    return-object v0
.end method


# virtual methods
.method asn1Equals(Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;)Z
    .locals 4

    goto/32 :goto_7

    nop

    :goto_0
    check-cast v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;

    goto/32 :goto_6

    nop

    :goto_1
    if-eqz v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_4

    nop

    :goto_2
    move-object v0, p1

    goto/32 :goto_0

    nop

    :goto_3
    const/4 v1, 0x0

    goto/32 :goto_1

    nop

    :goto_4
    return v1

    :goto_5
    goto/32 :goto_2

    nop

    :goto_6
    invoke-virtual {p0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;->isTrue()Z

    move-result v2

    goto/32 :goto_b

    nop

    :goto_7
    instance-of v0, p1, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;

    goto/32 :goto_3

    nop

    :goto_8
    const/4 v1, 0x1

    :goto_9
    goto/32 :goto_c

    nop

    :goto_a
    if-eq v2, v3, :cond_1

    goto/32 :goto_9

    :cond_1
    goto/32 :goto_8

    nop

    :goto_b
    invoke-virtual {v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;->isTrue()Z

    move-result v3

    goto/32 :goto_a

    nop

    :goto_c
    return v1
.end method

.method encode(Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iget-byte v0, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;->value:B

    goto/32 :goto_3

    nop

    :goto_2
    invoke-virtual {p1, p2, v1, v0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1OutputStream;->writeEncoded(ZIB)V

    goto/32 :goto_0

    nop

    :goto_3
    const/4 v1, 0x1

    goto/32 :goto_2

    nop
.end method

.method encodedLength()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x3

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method public hashCode()I
    .locals 1

    invoke-virtual {p0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;->isTrue()Z

    move-result v0

    return v0
.end method

.method isConstructed()Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method public isTrue()Z
    .locals 1

    iget-byte v0, p0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;->value:B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method toDERObject()Lcom/android/internal/org/bouncycastle/asn1/ASN1Primitive;
    .locals 1

    goto/32 :goto_6

    nop

    :goto_0
    return-object v0

    :goto_1
    sget-object v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;->FALSE:Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;

    :goto_2
    goto/32 :goto_0

    nop

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_7

    nop

    :goto_4
    goto :goto_2

    :goto_5
    goto/32 :goto_1

    nop

    :goto_6
    invoke-virtual {p0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;->isTrue()Z

    move-result v0

    goto/32 :goto_3

    nop

    :goto_7
    sget-object v0, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;->TRUE:Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;

    goto/32 :goto_4

    nop
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/android/internal/org/bouncycastle/asn1/ASN1Boolean;->isTrue()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "TRUE"

    goto :goto_0

    :cond_0
    const-string v0, "FALSE"

    :goto_0
    return-object v0
.end method
