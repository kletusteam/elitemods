.class Landroid/webkit/FindAddress$ZipRange;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/webkit/FindAddress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ZipRange"
.end annotation


# instance fields
.field mException1:I

.field mException2:I

.field mHigh:I

.field mLow:I


# direct methods
.method constructor <init>(IIII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Landroid/webkit/FindAddress$ZipRange;->mLow:I

    iput p2, p0, Landroid/webkit/FindAddress$ZipRange;->mHigh:I

    iput p3, p0, Landroid/webkit/FindAddress$ZipRange;->mException1:I

    iput p3, p0, Landroid/webkit/FindAddress$ZipRange;->mException2:I

    return-void
.end method


# virtual methods
.method matches(Ljava/lang/String;)Z
    .locals 3

    goto/32 :goto_10

    nop

    :goto_0
    if-gt v1, v2, :cond_0

    goto/32 :goto_c

    :cond_0
    :goto_1
    goto/32 :goto_d

    nop

    :goto_2
    if-ne v1, v2, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_8

    nop

    :goto_3
    if-le v2, v1, :cond_2

    goto/32 :goto_1

    :cond_2
    goto/32 :goto_4

    nop

    :goto_4
    iget v2, p0, Landroid/webkit/FindAddress$ZipRange;->mHigh:I

    goto/32 :goto_0

    nop

    :goto_5
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_a

    nop

    :goto_6
    const/4 v0, 0x1

    :goto_7
    goto/32 :goto_9

    nop

    :goto_8
    iget v2, p0, Landroid/webkit/FindAddress$ZipRange;->mException2:I

    goto/32 :goto_b

    nop

    :goto_9
    return v0

    :goto_a
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    goto/32 :goto_e

    nop

    :goto_b
    if-eq v1, v2, :cond_3

    goto/32 :goto_7

    :cond_3
    :goto_c
    goto/32 :goto_6

    nop

    :goto_d
    iget v2, p0, Landroid/webkit/FindAddress$ZipRange;->mException1:I

    goto/32 :goto_2

    nop

    :goto_e
    iget v2, p0, Landroid/webkit/FindAddress$ZipRange;->mLow:I

    goto/32 :goto_3

    nop

    :goto_f
    const/4 v1, 0x2

    goto/32 :goto_5

    nop

    :goto_10
    const/4 v0, 0x0

    goto/32 :goto_f

    nop
.end method
