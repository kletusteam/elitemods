.class public final Landroid/window/TransitionFilter$Requirement;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/window/TransitionFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Requirement"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Landroid/window/TransitionFilter$Requirement;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mActivityType:I

.field public mFlags:I

.field public mModes:[I

.field public mMustBeIndependent:Z

.field public mMustBeTask:Z

.field public mNot:Z

.field public mOrder:I

.field public mTopActivity:Landroid/content/ComponentName;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/window/TransitionFilter$Requirement$1;

    invoke-direct {v0}, Landroid/window/TransitionFilter$Requirement$1;-><init>()V

    sput-object v0, Landroid/window/TransitionFilter$Requirement;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Landroid/window/TransitionFilter$Requirement;->mActivityType:I

    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid/window/TransitionFilter$Requirement;->mMustBeIndependent:Z

    iput-boolean v0, p0, Landroid/window/TransitionFilter$Requirement;->mNot:Z

    const/4 v1, 0x0

    iput-object v1, p0, Landroid/window/TransitionFilter$Requirement;->mModes:[I

    iput v0, p0, Landroid/window/TransitionFilter$Requirement;->mFlags:I

    iput-boolean v0, p0, Landroid/window/TransitionFilter$Requirement;->mMustBeTask:Z

    iput v0, p0, Landroid/window/TransitionFilter$Requirement;->mOrder:I

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Landroid/window/TransitionFilter$Requirement;->mActivityType:I

    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid/window/TransitionFilter$Requirement;->mMustBeIndependent:Z

    iput-boolean v0, p0, Landroid/window/TransitionFilter$Requirement;->mNot:Z

    const/4 v1, 0x0

    iput-object v1, p0, Landroid/window/TransitionFilter$Requirement;->mModes:[I

    iput v0, p0, Landroid/window/TransitionFilter$Requirement;->mFlags:I

    iput-boolean v0, p0, Landroid/window/TransitionFilter$Requirement;->mMustBeTask:Z

    iput v0, p0, Landroid/window/TransitionFilter$Requirement;->mOrder:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/window/TransitionFilter$Requirement;->mActivityType:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Landroid/window/TransitionFilter$Requirement;->mMustBeIndependent:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Landroid/window/TransitionFilter$Requirement;->mNot:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v0

    iput-object v0, p0, Landroid/window/TransitionFilter$Requirement;->mModes:[I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/window/TransitionFilter$Requirement;->mFlags:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Landroid/window/TransitionFilter$Requirement;->mMustBeTask:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/window/TransitionFilter$Requirement;->mOrder:I

    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readTypedObject(Landroid/os/Parcelable$Creator;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    iput-object v0, p0, Landroid/window/TransitionFilter$Requirement;->mTopActivity:Landroid/content/ComponentName;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/window/TransitionFilter$Requirement-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/window/TransitionFilter$Requirement;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private matchesTopActivity(Landroid/app/ActivityManager$RunningTaskInfo;)Z
    .locals 2

    iget-object v0, p0, Landroid/window/TransitionFilter$Requirement;->mTopActivity:Landroid/content/ComponentName;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x0

    return v0

    :cond_1
    iget-object v0, p1, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    iget-object v1, p0, Landroid/window/TransitionFilter$Requirement;->mTopActivity:Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method matches(Landroid/window/TransitionInfo;)Z
    .locals 7

    goto/32 :goto_3f

    nop

    :goto_0
    const/4 v3, 0x0

    goto/32 :goto_30

    nop

    :goto_1
    return v0

    :goto_2
    const/4 v0, 0x0

    goto/32 :goto_1

    nop

    :goto_3
    goto/16 :goto_25

    :goto_4
    goto/32 :goto_21

    nop

    :goto_5
    aget v5, v5, v4

    goto/32 :goto_1a

    nop

    :goto_6
    if-eq v3, v1, :cond_0

    goto/32 :goto_41

    :cond_0
    goto/32 :goto_20

    nop

    :goto_7
    if-nez v3, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_11

    nop

    :goto_8
    if-nez v3, :cond_2

    goto/32 :goto_1f

    :cond_2
    goto/32 :goto_27

    nop

    :goto_9
    if-eqz v3, :cond_3

    goto/32 :goto_4

    :cond_3
    nop

    :goto_a
    goto/32 :goto_d

    nop

    :goto_b
    goto :goto_a

    :goto_c
    goto/32 :goto_3d

    nop

    :goto_d
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_3

    nop

    :goto_e
    invoke-virtual {p1}, Landroid/window/TransitionInfo;->getChanges()Ljava/util/List;

    move-result-object v2

    goto/32 :goto_46

    nop

    :goto_f
    iget v4, p0, Landroid/window/TransitionFilter$Requirement;->mFlags:I

    goto/32 :goto_1c

    nop

    :goto_10
    const/4 v1, 0x1

    goto/32 :goto_24

    nop

    :goto_11
    invoke-virtual {v2}, Landroid/window/TransitionInfo$Change;->getTaskInfo()Landroid/app/ActivityManager$RunningTaskInfo;

    move-result-object v3

    goto/32 :goto_9

    nop

    :goto_12
    invoke-virtual {v2}, Landroid/window/TransitionInfo$Change;->getTaskInfo()Landroid/app/ActivityManager$RunningTaskInfo;

    move-result-object v3

    goto/32 :goto_33

    nop

    :goto_13
    invoke-static {v2, p1}, Landroid/window/TransitionInfo;->isIndependent(Landroid/window/TransitionInfo$Change;Landroid/window/TransitionInfo;)Z

    move-result v3

    goto/32 :goto_28

    nop

    :goto_14
    array-length v6, v5

    goto/32 :goto_44

    nop

    :goto_15
    invoke-virtual {v2}, Landroid/window/TransitionInfo$Change;->getFlags()I

    move-result v3

    goto/32 :goto_f

    nop

    :goto_16
    invoke-virtual {v2}, Landroid/window/TransitionInfo$Change;->getTaskInfo()Landroid/app/ActivityManager$RunningTaskInfo;

    move-result-object v3

    goto/32 :goto_2a

    nop

    :goto_17
    iget-boolean v3, p0, Landroid/window/TransitionFilter$Requirement;->mMustBeTask:Z

    goto/32 :goto_7

    nop

    :goto_18
    check-cast v2, Landroid/window/TransitionInfo$Change;

    goto/32 :goto_23

    nop

    :goto_19
    iget-object v5, p0, Landroid/window/TransitionFilter$Requirement;->mModes:[I

    goto/32 :goto_14

    nop

    :goto_1a
    invoke-virtual {v2}, Landroid/window/TransitionInfo$Change;->getMode()I

    move-result v6

    goto/32 :goto_29

    nop

    :goto_1b
    iget v4, p0, Landroid/window/TransitionFilter$Requirement;->mActivityType:I

    goto/32 :goto_34

    nop

    :goto_1c
    and-int/2addr v3, v4

    goto/32 :goto_45

    nop

    :goto_1d
    if-nez v3, :cond_4

    goto/32 :goto_43

    :cond_4
    goto/32 :goto_0

    nop

    :goto_1e
    goto :goto_a

    :goto_1f
    goto/32 :goto_12

    nop

    :goto_20
    if-gtz v0, :cond_5

    goto/32 :goto_41

    :cond_5
    goto/32 :goto_40

    nop

    :goto_21
    return v1

    :goto_22
    goto/32 :goto_2

    nop

    :goto_23
    iget-boolean v3, p0, Landroid/window/TransitionFilter$Requirement;->mMustBeIndependent:Z

    goto/32 :goto_26

    nop

    :goto_24
    sub-int/2addr v0, v1

    :goto_25
    goto/32 :goto_3e

    nop

    :goto_26
    if-nez v3, :cond_6

    goto/32 :goto_48

    :cond_6
    goto/32 :goto_13

    nop

    :goto_27
    invoke-virtual {v2}, Landroid/window/TransitionInfo$Change;->getTaskInfo()Landroid/app/ActivityManager$RunningTaskInfo;

    move-result-object v3

    goto/32 :goto_36

    nop

    :goto_28
    if-eqz v3, :cond_7

    goto/32 :goto_48

    :cond_7
    goto/32 :goto_47

    nop

    :goto_29
    if-eq v5, v6, :cond_8

    goto/32 :goto_2f

    :cond_8
    goto/32 :goto_38

    nop

    :goto_2a
    invoke-virtual {v3}, Landroid/app/ActivityManager$RunningTaskInfo;->getActivityType()I

    move-result v3

    goto/32 :goto_1b

    nop

    :goto_2b
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto/32 :goto_10

    nop

    :goto_2c
    goto/16 :goto_a

    :goto_2d
    goto/32 :goto_17

    nop

    :goto_2e
    goto :goto_3a

    :goto_2f
    goto/32 :goto_3b

    nop

    :goto_30
    const/4 v4, 0x0

    :goto_31
    goto/32 :goto_19

    nop

    :goto_32
    iget v3, p0, Landroid/window/TransitionFilter$Requirement;->mActivityType:I

    goto/32 :goto_8

    nop

    :goto_33
    invoke-direct {p0, v3}, Landroid/window/TransitionFilter$Requirement;->matchesTopActivity(Landroid/app/ActivityManager$RunningTaskInfo;)Z

    move-result v3

    goto/32 :goto_37

    nop

    :goto_34
    if-ne v3, v4, :cond_9

    goto/32 :goto_1f

    :cond_9
    goto/32 :goto_1e

    nop

    :goto_35
    iget v3, p0, Landroid/window/TransitionFilter$Requirement;->mOrder:I

    goto/32 :goto_6

    nop

    :goto_36
    if-nez v3, :cond_a

    goto/32 :goto_a

    :cond_a
    goto/32 :goto_16

    nop

    :goto_37
    if-eqz v3, :cond_b

    goto/32 :goto_c

    :cond_b
    goto/32 :goto_b

    nop

    :goto_38
    const/4 v3, 0x1

    goto/32 :goto_2e

    nop

    :goto_39
    goto :goto_31

    :goto_3a
    goto/32 :goto_3c

    nop

    :goto_3b
    add-int/lit8 v4, v4, 0x1

    goto/32 :goto_39

    nop

    :goto_3c
    if-eqz v3, :cond_c

    goto/32 :goto_43

    :cond_c
    goto/32 :goto_42

    nop

    :goto_3d
    iget-object v3, p0, Landroid/window/TransitionFilter$Requirement;->mModes:[I

    goto/32 :goto_1d

    nop

    :goto_3e
    if-gez v0, :cond_d

    goto/32 :goto_22

    :cond_d
    goto/32 :goto_e

    nop

    :goto_3f
    invoke-virtual {p1}, Landroid/window/TransitionInfo;->getChanges()Ljava/util/List;

    move-result-object v0

    goto/32 :goto_2b

    nop

    :goto_40
    goto/16 :goto_a

    :goto_41
    goto/32 :goto_32

    nop

    :goto_42
    goto/16 :goto_a

    :goto_43
    goto/32 :goto_15

    nop

    :goto_44
    if-lt v4, v6, :cond_e

    goto/32 :goto_3a

    :cond_e
    goto/32 :goto_5

    nop

    :goto_45
    if-ne v3, v4, :cond_f

    goto/32 :goto_2d

    :cond_f
    goto/32 :goto_2c

    nop

    :goto_46
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_18

    nop

    :goto_47
    goto/16 :goto_a

    :goto_48
    goto/32 :goto_35

    nop
.end method

.method matches(Landroid/window/TransitionRequestInfo;)Z
    .locals 3

    goto/32 :goto_11

    nop

    :goto_0
    if-eqz v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_2

    nop

    :goto_1
    invoke-direct {p0, v0}, Landroid/window/TransitionFilter$Requirement;->matchesTopActivity(Landroid/app/ActivityManager$RunningTaskInfo;)Z

    move-result v0

    goto/32 :goto_8

    nop

    :goto_2
    return v1

    :goto_3
    goto/32 :goto_9

    nop

    :goto_4
    if-eq v0, v2, :cond_1

    goto/32 :goto_10

    :cond_1
    goto/32 :goto_12

    nop

    :goto_5
    invoke-virtual {v0}, Landroid/app/ActivityManager$RunningTaskInfo;->getActivityType()I

    move-result v0

    goto/32 :goto_6

    nop

    :goto_6
    iget v2, p0, Landroid/window/TransitionFilter$Requirement;->mActivityType:I

    goto/32 :goto_4

    nop

    :goto_7
    if-nez v0, :cond_2

    goto/32 :goto_10

    :cond_2
    goto/32 :goto_c

    nop

    :goto_8
    if-nez v0, :cond_3

    goto/32 :goto_10

    :cond_3
    goto/32 :goto_f

    nop

    :goto_9
    invoke-virtual {p1}, Landroid/window/TransitionRequestInfo;->getTriggerTask()Landroid/app/ActivityManager$RunningTaskInfo;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_a
    const/4 v1, 0x1

    goto/32 :goto_0

    nop

    :goto_b
    return v1

    :goto_c
    invoke-virtual {p1}, Landroid/window/TransitionRequestInfo;->getTriggerTask()Landroid/app/ActivityManager$RunningTaskInfo;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_d
    const/4 v1, 0x0

    :goto_e
    goto/32 :goto_b

    nop

    :goto_f
    goto :goto_e

    :goto_10
    goto/32 :goto_d

    nop

    :goto_11
    iget v0, p0, Landroid/window/TransitionFilter$Requirement;->mActivityType:I

    goto/32 :goto_a

    nop

    :goto_12
    invoke-virtual {p1}, Landroid/window/TransitionRequestInfo;->getTriggerTask()Landroid/app/ActivityManager$RunningTaskInfo;

    move-result-object v0

    goto/32 :goto_1

    nop
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x7b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Landroid/window/TransitionFilter$Requirement;->mNot:Z

    if-eqz v1, :cond_0

    const-string v1, "NOT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "atype="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Landroid/window/TransitionFilter$Requirement;->mActivityType:I

    invoke-static {v2}, Landroid/app/WindowConfiguration;->activityTypeToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " independent="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Landroid/window/TransitionFilter$Requirement;->mMustBeIndependent:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " modes=["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Landroid/window/TransitionFilter$Requirement;->mModes:[I

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Landroid/window/TransitionFilter$Requirement;->mModes:[I

    array-length v2, v2

    if-ge v1, v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    if-nez v1, :cond_1

    const-string v3, ""

    goto :goto_1

    :cond_1
    const-string v3, ","

    :goto_1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Landroid/window/TransitionFilter$Requirement;->mModes:[I

    aget v3, v3, v1

    invoke-static {v3}, Landroid/window/TransitionInfo;->modeToString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " flags="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Landroid/window/TransitionFilter$Requirement;->mFlags:I

    invoke-static {v2}, Landroid/window/TransitionInfo;->flagsToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mustBeTask="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Landroid/window/TransitionFilter$Requirement;->mMustBeTask:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " order="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Landroid/window/TransitionFilter$Requirement;->mOrder:I

    invoke-static {v2}, Landroid/window/TransitionFilter;->-$$Nest$smcontainerOrderToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " topActivity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/window/TransitionFilter$Requirement;->mTopActivity:Landroid/content/ComponentName;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget v0, p0, Landroid/window/TransitionFilter$Requirement;->mActivityType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Landroid/window/TransitionFilter$Requirement;->mMustBeIndependent:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    iget-boolean v0, p0, Landroid/window/TransitionFilter$Requirement;->mNot:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    iget-object v0, p0, Landroid/window/TransitionFilter$Requirement;->mModes:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    iget v0, p0, Landroid/window/TransitionFilter$Requirement;->mFlags:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Landroid/window/TransitionFilter$Requirement;->mMustBeTask:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    iget v0, p0, Landroid/window/TransitionFilter$Requirement;->mOrder:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Landroid/window/TransitionFilter$Requirement;->mTopActivity:Landroid/content/ComponentName;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeTypedObject(Landroid/os/Parcelable;I)V

    return-void
.end method
