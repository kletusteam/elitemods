.class public Landroid/widget/ShimmeringTextView;
.super Landroid/widget/TextView;
.source "ShimmeringTextView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/ShimmeringTextView$Sovereign;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "myapp"

.field private static final sSovereign:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Landroid/widget/ShimmeringTextView$Sovereign;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private MainColor:Ljava/lang/String;

.field private ShimmerColor:Ljava/lang/String;

.field private ShimmerDelay:Ljava/lang/String;

.field public mAnimate:Z

.field public mColor:I

.field public mColorShimmer:I

.field public mDelay:I

.field private mGradientMatrix:Landroid/graphics/Matrix;

.field private mLinearGradient:Landroid/graphics/LinearGradient;

.field private mPaint:Landroid/graphics/Paint;

.field private mTranslate:I

.field private mViewWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Landroid/widget/ShimmeringTextView;->sSovereign:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/ShimmeringTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-string v1, "none"

    iput-object v1, p0, Landroid/widget/ShimmeringTextView;->MainColor:Ljava/lang/String;

    const-string v1, "none"

    iput-object v1, p0, Landroid/widget/ShimmeringTextView;->ShimmerColor:Ljava/lang/String;

    const-string v1, "none"

    iput-object v1, p0, Landroid/widget/ShimmeringTextView;->ShimmerDelay:Ljava/lang/String;

    iput v3, p0, Landroid/widget/ShimmeringTextView;->mColorShimmer:I

    iput v3, p0, Landroid/widget/ShimmeringTextView;->mDelay:I

    invoke-virtual {p0}, Landroid/widget/ShimmeringTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    iput-object v1, p0, Landroid/widget/ShimmeringTextView;->mPaint:Landroid/graphics/Paint;

    if-eqz p2, :cond_2

    const-string v1, "MainColor"

    invoke-interface {p2, v2, v1}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iput-object v0, p0, Landroid/widget/ShimmeringTextView;->MainColor:Ljava/lang/String;

    :cond_0
    const-string v1, "ShimmerColor"

    invoke-interface {p2, v2, v1}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iput-object v0, p0, Landroid/widget/ShimmeringTextView;->ShimmerColor:Ljava/lang/String;

    :cond_1
    const-string v1, "ShimmerDelay"

    invoke-interface {p2, v2, v1}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iput-object v0, p0, Landroid/widget/ShimmeringTextView;->ShimmerDelay:Ljava/lang/String;

    :cond_2
    iget-object v1, p0, Landroid/widget/ShimmeringTextView;->MainColor:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/widget/ShimmeringTextView;->update(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Landroid/widget/ShimmeringTextView;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/widget/ShimmeringTextView;->MainColor:Ljava/lang/String;

    return-object v0
.end method

.method private sizeChanged()V
    .locals 11

    const/4 v4, 0x3

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/widget/ShimmeringTextView;->getMeasuredWidth()I

    move-result v0

    iput v0, p0, Landroid/widget/ShimmeringTextView;->mViewWidth:I

    iget v0, p0, Landroid/widget/ShimmeringTextView;->mViewWidth:I

    if-lez v0, :cond_0

    iget v0, p0, Landroid/widget/ShimmeringTextView;->mViewWidth:I

    neg-int v0, v0

    int-to-float v1, v0

    iget v9, p0, Landroid/widget/ShimmeringTextView;->mColor:I

    iget v8, p0, Landroid/widget/ShimmeringTextView;->mColorShimmer:I

    iget v10, p0, Landroid/widget/ShimmeringTextView;->mColor:I

    new-instance v0, Landroid/graphics/LinearGradient;

    new-array v5, v4, [I

    const/4 v3, 0x0

    aput v9, v5, v3

    const/4 v3, 0x1

    aput v8, v5, v3

    const/4 v3, 0x2

    aput v10, v5, v3

    new-array v6, v4, [F

    fill-array-data v6, :array_0

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v3, v2

    move v4, v2

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Landroid/widget/ShimmeringTextView;->mLinearGradient:Landroid/graphics/LinearGradient;

    iget-object v0, p0, Landroid/widget/ShimmeringTextView;->mPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Landroid/widget/ShimmeringTextView;->mLinearGradient:Landroid/graphics/LinearGradient;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Landroid/widget/ShimmeringTextView;->mGradientMatrix:Landroid/graphics/Matrix;

    :cond_0
    return-void

    :array_0
    .array-data 4
        0x0
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 3

    invoke-super {p0}, Landroid/widget/TextView;->onAttachedToWindow()V

    sget-object v2, Landroid/widget/ShimmeringTextView;->sSovereign:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ShimmeringTextView$Sovereign;

    move-object v0, v1

    if-nez v1, :cond_0

    new-instance v0, Landroid/widget/ShimmeringTextView$Sovereign;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Landroid/widget/ShimmeringTextView$Sovereign;-><init>(Landroid/widget/ShimmeringTextView$1;)V

    sget-object v2, Landroid/widget/ShimmeringTextView;->sSovereign:Ljava/lang/ThreadLocal;

    invoke-virtual {v2, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {v0, p0}, Landroid/widget/ShimmeringTextView$Sovereign;->addView(Landroid/widget/ShimmeringTextView;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    invoke-super {p0}, Landroid/widget/TextView;->onDetachedFromWindow()V

    sget-object v1, Landroid/widget/ShimmeringTextView;->sSovereign:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ShimmeringTextView$Sovereign;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Landroid/widget/ShimmeringTextView$Sovereign;->removeView(Landroid/widget/ShimmeringTextView;)V

    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    iget-boolean v2, p0, Landroid/widget/ShimmeringTextView;->mAnimate:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Landroid/widget/ShimmeringTextView;->mGradientMatrix:Landroid/graphics/Matrix;

    if-eqz v2, :cond_1

    iget v0, p0, Landroid/widget/ShimmeringTextView;->mTranslate:I

    iget v1, p0, Landroid/widget/ShimmeringTextView;->mViewWidth:I

    div-int/lit8 v2, v1, 0xa

    add-int/2addr v2, v0

    iput v2, p0, Landroid/widget/ShimmeringTextView;->mTranslate:I

    iget v2, p0, Landroid/widget/ShimmeringTextView;->mTranslate:I

    mul-int/lit8 v3, v1, 0x2

    if-le v2, v3, :cond_0

    neg-int v2, v1

    iput v2, p0, Landroid/widget/ShimmeringTextView;->mTranslate:I

    :cond_0
    iget-object v2, p0, Landroid/widget/ShimmeringTextView;->mGradientMatrix:Landroid/graphics/Matrix;

    iget v3, p0, Landroid/widget/ShimmeringTextView;->mTranslate:I

    int-to-float v3, v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Matrix;->setTranslate(FF)V

    iget-object v2, p0, Landroid/widget/ShimmeringTextView;->mLinearGradient:Landroid/graphics/LinearGradient;

    iget-object v3, p0, Landroid/widget/ShimmeringTextView;->mGradientMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3}, Landroid/graphics/LinearGradient;->setLocalMatrix(Landroid/graphics/Matrix;)V

    iget v2, p0, Landroid/widget/ShimmeringTextView;->mDelay:I

    int-to-long v2, v2

    invoke-virtual {p0, v2, v3}, Landroid/widget/ShimmeringTextView;->postInvalidateDelayed(J)V

    const-string v2, "myapp"

    const-string v3, "onDraw: "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->onSizeChanged(IIII)V

    invoke-direct {p0}, Landroid/widget/ShimmeringTextView;->sizeChanged()V

    return-void
.end method

.method public update(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Landroid/widget/ShimmeringTextView;->MainColor:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/widget/ShimmeringTextView;->updateSettings()V

    invoke-virtual {p0}, Landroid/widget/ShimmeringTextView;->invalidate()V

    :cond_0
    return-void
.end method

.method public updateSettings()V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/widget/ShimmeringTextView;->MainColor:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/preference/SettingsEliteHelper;->getIntofSettings(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Landroid/widget/ShimmeringTextView;->mColor:I

    iget-object v1, p0, Landroid/widget/ShimmeringTextView;->ShimmerColor:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/preference/SettingsEliteHelper;->getIntofSettings(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Landroid/widget/ShimmeringTextView;->mColorShimmer:I

    iget-object v1, p0, Landroid/widget/ShimmeringTextView;->ShimmerDelay:Ljava/lang/String;

    const/16 v2, 0x32

    invoke-static {v1, v2}, Landroid/preference/SettingsEliteHelper;->getIntofSettings(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Landroid/widget/ShimmeringTextView;->mDelay:I

    iget v1, p0, Landroid/widget/ShimmeringTextView;->mColorShimmer:I

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Landroid/widget/ShimmeringTextView;->mAnimate:Z

    iget-boolean v0, p0, Landroid/widget/ShimmeringTextView;->mAnimate:Z

    if-nez v0, :cond_1

    iget v0, p0, Landroid/widget/ShimmeringTextView;->mColor:I

    invoke-virtual {p0, v0}, Landroid/widget/ShimmeringTextView;->setTextColor(I)V

    :cond_1
    invoke-direct {p0}, Landroid/widget/ShimmeringTextView;->sizeChanged()V

    const-string v0, "myapp"

    const-string/jumbo v1, "updateSettings: "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
