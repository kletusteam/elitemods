.class Landroid/widget/ShimmeringTextView$Sovereign;
.super Ljava/lang/Object;
.source "ShimmeringTextView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/ShimmeringTextView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Sovereign"
.end annotation


# instance fields
.field private intentFilter:Landroid/content/IntentFilter;

.field private final mAttachedViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ShimmeringTextView;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/widget/ShimmeringTextView$Sovereign;->mAttachedViews:Ljava/util/ArrayList;

    new-instance v0, Landroid/widget/ShimmeringTextView$Sovereign$1;

    invoke-direct {v0, p0}, Landroid/widget/ShimmeringTextView$Sovereign$1;-><init>(Landroid/widget/ShimmeringTextView$Sovereign;)V

    iput-object v0, p0, Landroid/widget/ShimmeringTextView$Sovereign;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method synthetic constructor <init>(Landroid/widget/ShimmeringTextView$1;)V
    .locals 0

    invoke-direct {p0}, Landroid/widget/ShimmeringTextView$Sovereign;-><init>()V

    return-void
.end method

.method private getSimmerHandler()Landroid/os/Handler;
    .locals 3

    iget-object v1, p0, Landroid/widget/ShimmeringTextView$Sovereign;->mHandler:Landroid/os/Handler;

    if-nez v1, :cond_0

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "Shimmer_text_view"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Landroid/widget/ShimmeringTextView$Sovereign;->mHandler:Landroid/os/Handler;

    :cond_0
    iget-object v1, p0, Landroid/widget/ShimmeringTextView$Sovereign;->mHandler:Landroid/os/Handler;

    return-object v1
.end method


# virtual methods
.method public addView(Landroid/widget/ShimmeringTextView;)V
    .locals 5

    iget-object v2, p0, Landroid/widget/ShimmeringTextView$Sovereign;->mAttachedViews:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Landroid/widget/ShimmeringTextView$Sovereign;->mAttachedViews:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    iget-object v1, p0, Landroid/widget/ShimmeringTextView$Sovereign;->mAttachedViews:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/widget/ShimmeringTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p1}, Landroid/widget/ShimmeringTextView;->access$100(Landroid/widget/ShimmeringTextView;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p0, v1, v3, v4}, Landroid/widget/ShimmeringTextView$Sovereign;->register(Landroid/content/Context;Ljava/lang/String;I)V

    :goto_0
    invoke-static {p1}, Landroid/widget/ShimmeringTextView;->access$100(Landroid/widget/ShimmeringTextView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/ShimmeringTextView;->update(Ljava/lang/String;)V

    monitor-exit v2

    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/widget/ShimmeringTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/widget/ShimmeringTextView$Sovereign;->unregister(Landroid/content/Context;)V

    invoke-virtual {p1}, Landroid/widget/ShimmeringTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p1}, Landroid/widget/ShimmeringTextView;->access$100(Landroid/widget/ShimmeringTextView;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {p0, v1, v3, v4}, Landroid/widget/ShimmeringTextView$Sovereign;->register(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method register(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 4

    if-nez p3, :cond_0

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Landroid/widget/ShimmeringTextView$Sovereign;->intentFilter:Landroid/content/IntentFilter;

    :cond_0
    const-string v0, "none"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/widget/ShimmeringTextView$Sovereign;->intentFilter:Landroid/content/IntentFilter;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "my.settings.intent."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".CHANGE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Landroid/widget/ShimmeringTextView$Sovereign;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Landroid/widget/ShimmeringTextView$Sovereign;->intentFilter:Landroid/content/IntentFilter;

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/widget/ShimmeringTextView$Sovereign;->getSimmerHandler()Landroid/os/Handler;

    move-result-object v3

    invoke-static {p1, v0, v1, v2, v3}, Landroid/Utils/Utils;->registerReceiverAsUser(Landroid/content/Context;Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)V

    return-void
.end method

.method public removeView(Landroid/widget/ShimmeringTextView;)V
    .locals 2

    iget-object v1, p0, Landroid/widget/ShimmeringTextView$Sovereign;->mAttachedViews:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Landroid/widget/ShimmeringTextView$Sovereign;->mAttachedViews:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Landroid/widget/ShimmeringTextView$Sovereign;->mAttachedViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/widget/ShimmeringTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/ShimmeringTextView$Sovereign;->unregister(Landroid/content/Context;)V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method unregister(Landroid/content/Context;)V
    .locals 1

    iget-object v0, p0, Landroid/widget/ShimmeringTextView$Sovereign;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method updateAll(Ljava/lang/String;)V
    .locals 4

    iget-object v2, p0, Landroid/widget/ShimmeringTextView$Sovereign;->mAttachedViews:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Landroid/widget/ShimmeringTextView$Sovereign;->mAttachedViews:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ShimmeringTextView;

    new-instance v3, Landroid/widget/ShimmeringTextView$Sovereign$2;

    invoke-direct {v3, p0, v0, p1}, Landroid/widget/ShimmeringTextView$Sovereign$2;-><init>(Landroid/widget/ShimmeringTextView$Sovereign;Landroid/widget/ShimmeringTextView;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Landroid/widget/ShimmeringTextView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
