.class Landroid/widget/ColorTextView$Sovereign$1;
.super Landroid/content/BroadcastReceiver;
.source "ColorTextView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/ColorTextView$Sovereign;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/ColorTextView$Sovereign;


# direct methods
.method constructor <init>(Landroid/widget/ColorTextView$Sovereign;)V
    .locals 0

    iput-object p1, p0, Landroid/widget/ColorTextView$Sovereign$1;->this$0:Landroid/widget/ColorTextView$Sovereign;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/widget/ColorTextView$Sovereign$1;->this$0:Landroid/widget/ColorTextView$Sovereign;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ColorTextView$Sovereign;->updateScreenState(Z)V

    goto :goto_0

    :cond_1
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/widget/ColorTextView$Sovereign$1;->this$0:Landroid/widget/ColorTextView$Sovereign;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ColorTextView$Sovereign;->updateScreenState(Z)V

    goto :goto_0

    :cond_2
    const-string v1, "my.settings.intent."

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ".CHANGE"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Landroid/widget/ColorTextView$Sovereign$1;->this$0:Landroid/widget/ColorTextView$Sovereign;

    invoke-virtual {v1, v0}, Landroid/widget/ColorTextView$Sovereign;->updateAll(Ljava/lang/String;)V

    goto :goto_0
.end method
