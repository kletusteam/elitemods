.class public Landroid/widget/ColorShimmeringTextView;
.super Landroid/widget/ColorTextView;
.source "ColorShimmeringTextView.java"


# instance fields
.field private isScreenOn:Z

.field private mEnableKey:Ljava/lang/String;

.field private mEnableShimmeng:Z

.field private mGradientMatrix:Landroid/graphics/Matrix;

.field private mLinearGradient:Landroid/graphics/LinearGradient;

.field private mPaint:Landroid/graphics/Paint;

.field private mShimmengColor:I

.field private mShimmengKey:Ljava/lang/String;

.field private mTranslate:I

.field private mViewWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/ColorShimmeringTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/ColorTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0}, Landroid/widget/ColorShimmeringTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    iput-object v0, p0, Landroid/widget/ColorShimmeringTextView;->mPaint:Landroid/graphics/Paint;

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/widget/ColorShimmeringTextView;->isScreenOn:Z

    return-void
.end method

.method private sizeChanged()V
    .locals 11

    const/4 v4, 0x3

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/widget/ColorShimmeringTextView;->getMeasuredWidth()I

    move-result v0

    iput v0, p0, Landroid/widget/ColorShimmeringTextView;->mViewWidth:I

    iget v0, p0, Landroid/widget/ColorShimmeringTextView;->mViewWidth:I

    if-lez v0, :cond_0

    iget v0, p0, Landroid/widget/ColorShimmeringTextView;->mViewWidth:I

    neg-int v0, v0

    int-to-float v1, v0

    iget-object v0, p0, Landroid/widget/ColorShimmeringTextView;->mValueKey:[I

    aget v9, v0, v3

    iget v8, p0, Landroid/widget/ColorShimmeringTextView;->mShimmengColor:I

    iget-object v0, p0, Landroid/widget/ColorShimmeringTextView;->mValueKey:[I

    aget v10, v0, v3

    new-instance v0, Landroid/graphics/LinearGradient;

    new-array v5, v4, [I

    aput v9, v5, v3

    const/4 v3, 0x1

    aput v8, v5, v3

    const/4 v3, 0x2

    aput v10, v5, v3

    new-array v6, v4, [F

    fill-array-data v6, :array_0

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v3, v2

    move v4, v2

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Landroid/widget/ColorShimmeringTextView;->mLinearGradient:Landroid/graphics/LinearGradient;

    iget-object v0, p0, Landroid/widget/ColorShimmeringTextView;->mPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Landroid/widget/ColorShimmeringTextView;->mLinearGradient:Landroid/graphics/LinearGradient;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Landroid/widget/ColorShimmeringTextView;->mGradientMatrix:Landroid/graphics/Matrix;

    :cond_0
    return-void

    :array_0
    .array-data 4
        0x0
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    invoke-super {p0, p1}, Landroid/widget/ColorTextView;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Landroid/widget/ColorShimmeringTextView;->isShown()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Landroid/widget/ColorShimmeringTextView;->isScreenOn:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Landroid/widget/ColorShimmeringTextView;->mEnableShimmeng:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Landroid/widget/ColorShimmeringTextView;->mGradientMatrix:Landroid/graphics/Matrix;

    if-eqz v2, :cond_1

    iget v0, p0, Landroid/widget/ColorShimmeringTextView;->mTranslate:I

    iget v1, p0, Landroid/widget/ColorShimmeringTextView;->mViewWidth:I

    div-int/lit8 v2, v1, 0xa

    add-int/2addr v2, v0

    iput v2, p0, Landroid/widget/ColorShimmeringTextView;->mTranslate:I

    iget v2, p0, Landroid/widget/ColorShimmeringTextView;->mTranslate:I

    mul-int/lit8 v3, v1, 0x2

    if-le v2, v3, :cond_0

    neg-int v2, v1

    iput v2, p0, Landroid/widget/ColorShimmeringTextView;->mTranslate:I

    :cond_0
    iget-object v2, p0, Landroid/widget/ColorShimmeringTextView;->mGradientMatrix:Landroid/graphics/Matrix;

    iget v3, p0, Landroid/widget/ColorShimmeringTextView;->mTranslate:I

    int-to-float v3, v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Matrix;->setTranslate(FF)V

    iget-object v2, p0, Landroid/widget/ColorShimmeringTextView;->mLinearGradient:Landroid/graphics/LinearGradient;

    iget-object v3, p0, Landroid/widget/ColorShimmeringTextView;->mGradientMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3}, Landroid/graphics/LinearGradient;->setLocalMatrix(Landroid/graphics/Matrix;)V

    const-wide/16 v2, 0x32

    invoke-virtual {p0, v2, v3}, Landroid/widget/ColorShimmeringTextView;->postInvalidateDelayed(J)V

    :cond_1
    return-void
.end method

.method public onScreenStateChange(Z)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/ColorTextView;->onScreenStateChange(Z)V

    iput-boolean p1, p0, Landroid/widget/ColorShimmeringTextView;->isScreenOn:Z

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroid/widget/ColorShimmeringTextView;->invalidate()V

    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ColorTextView;->onSizeChanged(IIII)V

    invoke-direct {p0}, Landroid/widget/ColorShimmeringTextView;->sizeChanged()V

    return-void
.end method

.method public setKey()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Landroid/widget/ColorShimmeringTextView;->mNameKey:[Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_color_shimm"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/widget/ColorShimmeringTextView;->mShimmengKey:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Landroid/widget/ColorShimmeringTextView;->mNameKey:[Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_enable_shim"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/widget/ColorShimmeringTextView;->mEnableKey:Ljava/lang/String;

    invoke-super {p0}, Landroid/widget/ColorTextView;->setKey()V

    return-void
.end method

.method public setKeyTypeFase(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/widget/ColorTextView;->setKeyTypeFase(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "_color"

    const-string v1, "_color_shimm"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/widget/ColorShimmeringTextView;->mShimmengKey:Ljava/lang/String;

    const-string v0, "_color"

    const-string v1, "_enable_shim"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/widget/ColorShimmeringTextView;->mEnableKey:Ljava/lang/String;

    return-void
.end method

.method public updateSettings()V
    .locals 1

    invoke-super {p0}, Landroid/widget/ColorTextView;->updateSettings()V

    iget-object v0, p0, Landroid/widget/ColorShimmeringTextView;->mShimmengKey:Ljava/lang/String;

    invoke-static {v0}, Landroid/preference/SettingsEliteHelper;->getIntofSettings(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Landroid/widget/ColorShimmeringTextView;->mShimmengColor:I

    iget-object v0, p0, Landroid/widget/ColorShimmeringTextView;->mEnableKey:Ljava/lang/String;

    invoke-static {v0}, Landroid/preference/SettingsEliteHelper;->getBoolofSettings(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Landroid/widget/ColorShimmeringTextView;->mEnableShimmeng:Z

    invoke-direct {p0}, Landroid/widget/ColorShimmeringTextView;->sizeChanged()V

    invoke-virtual {p0}, Landroid/widget/ColorShimmeringTextView;->invalidate()V

    return-void
.end method
