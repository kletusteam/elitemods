.class public Landroid/widget/ColorTextView;
.super Landroid/widget/TextView;
.source "ColorTextView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/widget/ColorTextView$Sovereign;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String;

.field private static final sSovereign:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Landroid/widget/ColorTextView$Sovereign;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final color:I

.field private final division:I

.field private mColorSet:I

.field private mFactor:F

.field public mNameKey:[Ljava/lang/String;

.field private mPaint:Landroid/graphics/Paint;

.field public mValueKey:[I

.field private final typeFase:I

.field private final typeFaseStyle:I

.field private final zoom:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Landroid/widget/ColorTextView;->sSovereign:Ljava/lang/ThreadLocal;

    const-class v0, Landroid/widget/ColorTextView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/widget/ColorTextView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/ColorTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x4

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0}, Landroid/widget/ColorTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    iput-object v1, p0, Landroid/widget/ColorTextView;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "none"

    aput-object v2, v1, v3

    const-string v2, "none"

    aput-object v2, v1, v5

    const-string v2, "none"

    aput-object v2, v1, v6

    const-string v2, "none"

    aput-object v2, v1, v7

    const-string v2, "none"

    aput-object v2, v1, v4

    iput-object v1, p0, Landroid/widget/ColorTextView;->mNameKey:[Ljava/lang/String;

    new-array v1, v4, [I

    fill-array-data v1, :array_0

    iput-object v1, p0, Landroid/widget/ColorTextView;->mValueKey:[I

    iput v3, p0, Landroid/widget/ColorTextView;->color:I

    iput v4, p0, Landroid/widget/ColorTextView;->typeFase:I

    iput v6, p0, Landroid/widget/ColorTextView;->typeFaseStyle:I

    iput v5, p0, Landroid/widget/ColorTextView;->division:I

    iput v7, p0, Landroid/widget/ColorTextView;->zoom:I

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Landroid/widget/ColorTextView;->mFactor:F

    if-eqz p2, :cond_1

    const/4 v1, 0x0

    const-string v2, "settingsKey"

    invoke-interface {p2, v1, v2}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Landroid/widget/ColorTextView;->mNameKey:[Ljava/lang/String;

    aput-object v0, v1, v3

    :cond_0
    invoke-virtual {p0}, Landroid/widget/ColorTextView;->setKey()V

    :cond_1
    const/4 v1, -0x1

    iput v1, p0, Landroid/widget/ColorTextView;->mColorSet:I

    invoke-virtual {p0}, Landroid/widget/ColorTextView;->updateSettings()V

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x13
        0x1
        0xf
    .end array-data
.end method

.method private getTypeFace(Ljava/lang/String;)Landroid/graphics/Typeface;
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    invoke-static {p1}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    sget-object v2, Landroid/widget/ColorTextView;->TAG:Ljava/lang/String;

    const-string v3, "getTypeFase : fonts not found"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private updateTyperface()V
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x2

    iget-object v2, p0, Landroid/widget/ColorTextView;->mNameKey:[Ljava/lang/String;

    aget-object v2, v2, v5

    const-string v3, "none_typefase"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Landroid/widget/ColorTextView;->mNameKey:[Ljava/lang/String;

    aget-object v2, v2, v5

    invoke-static {v2}, Landroid/preference/SettingsEliteHelper;->getStringofSettings(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v0}, Landroid/widget/ColorTextView;->getTypeFace(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Landroid/widget/ColorTextView;->mValueKey:[I

    aget v2, v2, v4

    invoke-virtual {p0, v1, v2}, Landroid/widget/ColorTextView;->setTypeface(Landroid/graphics/Typeface;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Landroid/widget/ColorTextView;->mValueKey:[I

    aget v2, v2, v4

    invoke-static {v2}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {p0, v2}, Landroid/widget/ColorTextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 2

    invoke-super {p0}, Landroid/widget/TextView;->onAttachedToWindow()V

    sget-object v1, Landroid/widget/ColorTextView;->sSovereign:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ColorTextView$Sovereign;

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/ColorTextView$Sovereign;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/widget/ColorTextView$Sovereign;-><init>(Landroid/widget/ColorTextView$1;)V

    sget-object v1, Landroid/widget/ColorTextView;->sSovereign:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {v0, p0}, Landroid/widget/ColorTextView$Sovereign;->addView(Landroid/widget/ColorTextView;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    invoke-super {p0}, Landroid/widget/TextView;->onDetachedFromWindow()V

    sget-object v1, Landroid/widget/ColorTextView;->sSovereign:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ColorTextView$Sovereign;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Landroid/widget/ColorTextView$Sovereign;->removeView(Landroid/widget/ColorTextView;)V

    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Landroid/widget/ColorTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    iput-object v1, p0, Landroid/widget/ColorTextView;->mPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Landroid/widget/ColorTextView;->mPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Landroid/widget/ColorTextView;->mValueKey:[I

    aget v1, v1, v3

    if-nez v1, :cond_0

    iget v1, p0, Landroid/widget/ColorTextView;->mColorSet:I

    :goto_0
    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Landroid/widget/ColorTextView;->mValueKey:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    int-to-float v1, v1

    const/high16 v2, 0x41200000    # 10.0f

    div-float v0, v1, v2

    invoke-virtual {p0}, Landroid/widget/ColorTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/widget/ColorTextView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    iget-object v4, p0, Landroid/widget/ColorTextView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->descent()F

    move-result v4

    iget-object v5, p0, Landroid/widget/ColorTextView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v5}, Landroid/graphics/Paint;->ascent()F

    move-result v5

    add-float/2addr v4, v5

    div-float/2addr v4, v0

    sub-float/2addr v3, v4

    iget-object v4, p0, Landroid/widget/ColorTextView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    return-void

    :cond_0
    iget-object v1, p0, Landroid/widget/ColorTextView;->mValueKey:[I

    aget v1, v1, v3

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 5

    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->onMeasure(II)V

    iget-object v2, p0, Landroid/widget/ColorTextView;->mValueKey:[I

    const/4 v3, 0x2

    aget v2, v2, v3

    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    const-string v1, " "

    :goto_0
    iget-object v2, p0, Landroid/widget/ColorTextView;->mPaint:Landroid/graphics/Paint;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/widget/ColorTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v2

    float-to-int v0, v2

    invoke-virtual {p0}, Landroid/widget/ColorTextView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0, v0, v2}, Landroid/widget/ColorTextView;->setMeasuredDimension(II)V

    return-void

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public onScreenStateChange(Z)V
    .locals 0

    return-void
.end method

.method public setKey()V
    .locals 6

    const/4 v5, 0x0

    iget-object v1, p0, Landroid/widget/ColorTextView;->mNameKey:[Ljava/lang/String;

    aget-object v0, v1, v5

    iget-object v1, p0, Landroid/widget/ColorTextView;->mNameKey:[Ljava/lang/String;

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_division"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    iget-object v1, p0, Landroid/widget/ColorTextView;->mNameKey:[Ljava/lang/String;

    const/4 v2, 0x4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_typefase"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    iget-object v1, p0, Landroid/widget/ColorTextView;->mNameKey:[Ljava/lang/String;

    const/4 v2, 0x2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_typefasestyle"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    iget-object v1, p0, Landroid/widget/ColorTextView;->mNameKey:[Ljava/lang/String;

    const/4 v2, 0x3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_zoom"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    iget-object v1, p0, Landroid/widget/ColorTextView;->mNameKey:[Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_color"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    iget-object v1, p0, Landroid/widget/ColorTextView;->mNameKey:[Ljava/lang/String;

    aget-object v1, v1, v5

    invoke-virtual {p0, v1}, Landroid/widget/ColorTextView;->update(Ljava/lang/String;)V

    return-void
.end method

.method public setKey(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Landroid/widget/ColorTextView;->mNameKey:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/widget/ColorTextView;->mNameKey:[Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    iget-object v0, p0, Landroid/widget/ColorTextView;->mNameKey:[Ljava/lang/String;

    const/4 v1, 0x3

    aput-object p2, v0, v1

    invoke-virtual {p0, p1}, Landroid/widget/ColorTextView;->update(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setKeyTypeFase(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Landroid/widget/ColorTextView;->mNameKey:[Ljava/lang/String;

    const/4 v1, 0x2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_typefase"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Landroid/widget/ColorTextView;->mNameKey:[Ljava/lang/String;

    const/4 v1, 0x4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_typefasestyle"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Landroid/widget/ColorTextView;->mNameKey:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Landroid/widget/ColorTextView;->update(Ljava/lang/String;)V

    return-void
.end method

.method public setScaleFactor(F)V
    .locals 0

    iput p1, p0, Landroid/widget/ColorTextView;->mFactor:F

    return-void
.end method

.method public setTextColor()V
    .locals 1

    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p0}, Landroid/widget/ColorTextView;->invalidate()V

    return-void
.end method

.method public setTextColor(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/widget/TextView;->setTextColor(I)V

    if-eqz p1, :cond_0

    iput p1, p0, Landroid/widget/ColorTextView;->mColorSet:I

    :cond_0
    invoke-virtual {p0}, Landroid/widget/ColorTextView;->setTextColor()V

    return-void
.end method

.method public setTextSize(F)V
    .locals 2

    const/4 v1, 0x3

    iget-object v0, p0, Landroid/widget/ColorTextView;->mValueKey:[I

    aget v0, v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/widget/ColorTextView;->mValueKey:[I

    aget v0, v0, v1

    int-to-float v0, v0

    iget v1, p0, Landroid/widget/ColorTextView;->mFactor:F

    mul-float p1, v0, v1

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    return-void
.end method

.method public setTextSize(IF)V
    .locals 2

    const/4 v1, 0x3

    iget-object v0, p0, Landroid/widget/ColorTextView;->mValueKey:[I

    aget v0, v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/widget/ColorTextView;->mValueKey:[I

    aget v0, v0, v1

    int-to-float v0, v0

    iget v1, p0, Landroid/widget/ColorTextView;->mFactor:F

    mul-float p2, v0, v1

    :cond_0
    const/4 v0, 0x2

    invoke-super {p0, v0, p2}, Landroid/widget/TextView;->setTextSize(IF)V

    return-void
.end method

.method public update(Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x3

    iget-object v0, p0, Landroid/widget/ColorTextView;->mNameKey:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/widget/ColorTextView;->mNameKey:[Ljava/lang/String;

    aget-object v0, v0, v2

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    invoke-virtual {p0}, Landroid/widget/ColorTextView;->updateSettings()V

    invoke-virtual {p0}, Landroid/widget/ColorTextView;->setTextColor()V

    iget-object v0, p0, Landroid/widget/ColorTextView;->mValueKey:[I

    aget v0, v0, v2

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/widget/ColorTextView;->mValueKey:[I

    aget v0, v0, v2

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Landroid/widget/ColorTextView;->setTextSize(F)V

    :cond_1
    invoke-direct {p0}, Landroid/widget/ColorTextView;->updateTyperface()V

    invoke-virtual {p0}, Landroid/widget/ColorTextView;->invalidate()V

    :cond_2
    return-void
.end method

.method public updateSettings()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Landroid/widget/ColorTextView;->mValueKey:[I

    iget-object v1, p0, Landroid/widget/ColorTextView;->mNameKey:[Ljava/lang/String;

    aget-object v1, v1, v3

    const/4 v2, -0x1

    invoke-static {v1, v2}, Landroid/preference/SettingsEliteHelper;->getIntofSettings(Ljava/lang/String;I)I

    move-result v1

    aput v1, v0, v3

    iget-object v0, p0, Landroid/widget/ColorTextView;->mValueKey:[I

    iget-object v1, p0, Landroid/widget/ColorTextView;->mNameKey:[Ljava/lang/String;

    aget-object v1, v1, v6

    const/16 v2, 0x1e

    invoke-static {v1, v2}, Landroid/preference/SettingsEliteHelper;->getIntofSettings(Ljava/lang/String;I)I

    move-result v1

    aput v1, v0, v6

    iget-object v0, p0, Landroid/widget/ColorTextView;->mValueKey:[I

    iget-object v1, p0, Landroid/widget/ColorTextView;->mNameKey:[Ljava/lang/String;

    aget-object v1, v1, v5

    invoke-static {v1, v3}, Landroid/preference/SettingsEliteHelper;->getIntofSettings(Ljava/lang/String;I)I

    move-result v1

    aput v1, v0, v5

    iget-object v0, p0, Landroid/widget/ColorTextView;->mValueKey:[I

    iget-object v1, p0, Landroid/widget/ColorTextView;->mNameKey:[Ljava/lang/String;

    aget-object v1, v1, v4

    const/16 v2, 0x13

    invoke-static {v1, v2}, Landroid/preference/SettingsEliteHelper;->getIntofSettings(Ljava/lang/String;I)I

    move-result v1

    aput v1, v0, v4

    return-void
.end method
