.class public interface abstract Landroid/widget/EditorStub;
.super Ljava/lang/Object;
.source "EditorStub.java"


# direct methods
.method public static newInstance()Landroid/widget/EditorStub;
    .locals 1

    const-class v0, Landroid/widget/EditorStub;

    invoke-static {v0}, Lcom/miui/base/MiuiStubUtil;->newInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditorStub;

    return-object v0
.end method


# virtual methods
.method public init(Landroid/widget/TextView;)V
    .locals 0

    return-void
.end method

.method public insertionHandlePositionChanged(ILandroid/graphics/RectF;)V
    .locals 0

    return-void
.end method

.method public needSendHandlePosition()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public selectionHandlePositionChanged(ZILandroid/graphics/RectF;)V
    .locals 0

    return-void
.end method
