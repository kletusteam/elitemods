.class public final Landroid/widget/Magnifier$Builder;
.super Ljava/lang/Object;
.source "Magnifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/Magnifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private mBottomContentBound:I

.field private mClippingEnabled:Z

.field private mCornerRadius:F

.field private mElevation:F

.field private mHeight:I

.field private mHorizontalDefaultSourceToMagnifierOffset:I

.field private mIsFishEyeStyle:Z

.field private mLeftContentBound:I

.field private mOverlay:Landroid/graphics/drawable/Drawable;

.field private mRightContentBound:I

.field private mSourceHeight:I

.field private mSourceWidth:I

.field private mTopContentBound:I

.field private mVerticalDefaultSourceToMagnifierOffset:I

.field private mView:Landroid/view/View;

.field private mWidth:I

.field private mZoom:F


# direct methods
.method static bridge synthetic -$$Nest$fgetmBottomContentBound(Landroid/widget/Magnifier$Builder;)I
    .locals 0

    iget p0, p0, Landroid/widget/Magnifier$Builder;->mBottomContentBound:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmClippingEnabled(Landroid/widget/Magnifier$Builder;)Z
    .locals 0

    iget-boolean p0, p0, Landroid/widget/Magnifier$Builder;->mClippingEnabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmCornerRadius(Landroid/widget/Magnifier$Builder;)F
    .locals 0

    iget p0, p0, Landroid/widget/Magnifier$Builder;->mCornerRadius:F

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmElevation(Landroid/widget/Magnifier$Builder;)F
    .locals 0

    iget p0, p0, Landroid/widget/Magnifier$Builder;->mElevation:F

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmHeight(Landroid/widget/Magnifier$Builder;)I
    .locals 0

    iget p0, p0, Landroid/widget/Magnifier$Builder;->mHeight:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmHorizontalDefaultSourceToMagnifierOffset(Landroid/widget/Magnifier$Builder;)I
    .locals 0

    iget p0, p0, Landroid/widget/Magnifier$Builder;->mHorizontalDefaultSourceToMagnifierOffset:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsFishEyeStyle(Landroid/widget/Magnifier$Builder;)Z
    .locals 0

    iget-boolean p0, p0, Landroid/widget/Magnifier$Builder;->mIsFishEyeStyle:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLeftContentBound(Landroid/widget/Magnifier$Builder;)I
    .locals 0

    iget p0, p0, Landroid/widget/Magnifier$Builder;->mLeftContentBound:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmOverlay(Landroid/widget/Magnifier$Builder;)Landroid/graphics/drawable/Drawable;
    .locals 0

    iget-object p0, p0, Landroid/widget/Magnifier$Builder;->mOverlay:Landroid/graphics/drawable/Drawable;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmRightContentBound(Landroid/widget/Magnifier$Builder;)I
    .locals 0

    iget p0, p0, Landroid/widget/Magnifier$Builder;->mRightContentBound:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSourceHeight(Landroid/widget/Magnifier$Builder;)I
    .locals 0

    iget p0, p0, Landroid/widget/Magnifier$Builder;->mSourceHeight:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSourceWidth(Landroid/widget/Magnifier$Builder;)I
    .locals 0

    iget p0, p0, Landroid/widget/Magnifier$Builder;->mSourceWidth:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmTopContentBound(Landroid/widget/Magnifier$Builder;)I
    .locals 0

    iget p0, p0, Landroid/widget/Magnifier$Builder;->mTopContentBound:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmVerticalDefaultSourceToMagnifierOffset(Landroid/widget/Magnifier$Builder;)I
    .locals 0

    iget p0, p0, Landroid/widget/Magnifier$Builder;->mVerticalDefaultSourceToMagnifierOffset:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmView(Landroid/widget/Magnifier$Builder;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Landroid/widget/Magnifier$Builder;->mView:Landroid/view/View;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmWidth(Landroid/widget/Magnifier$Builder;)I
    .locals 0

    iget p0, p0, Landroid/widget/Magnifier$Builder;->mWidth:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmZoom(Landroid/widget/Magnifier$Builder;)F
    .locals 0

    iget p0, p0, Landroid/widget/Magnifier$Builder;->mZoom:F

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmBottomContentBound(Landroid/widget/Magnifier$Builder;I)V
    .locals 0

    iput p1, p0, Landroid/widget/Magnifier$Builder;->mBottomContentBound:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmClippingEnabled(Landroid/widget/Magnifier$Builder;Z)V
    .locals 0

    iput-boolean p1, p0, Landroid/widget/Magnifier$Builder;->mClippingEnabled:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmCornerRadius(Landroid/widget/Magnifier$Builder;F)V
    .locals 0

    iput p1, p0, Landroid/widget/Magnifier$Builder;->mCornerRadius:F

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmElevation(Landroid/widget/Magnifier$Builder;F)V
    .locals 0

    iput p1, p0, Landroid/widget/Magnifier$Builder;->mElevation:F

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmHeight(Landroid/widget/Magnifier$Builder;I)V
    .locals 0

    iput p1, p0, Landroid/widget/Magnifier$Builder;->mHeight:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmHorizontalDefaultSourceToMagnifierOffset(Landroid/widget/Magnifier$Builder;I)V
    .locals 0

    iput p1, p0, Landroid/widget/Magnifier$Builder;->mHorizontalDefaultSourceToMagnifierOffset:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLeftContentBound(Landroid/widget/Magnifier$Builder;I)V
    .locals 0

    iput p1, p0, Landroid/widget/Magnifier$Builder;->mLeftContentBound:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmOverlay(Landroid/widget/Magnifier$Builder;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    iput-object p1, p0, Landroid/widget/Magnifier$Builder;->mOverlay:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmRightContentBound(Landroid/widget/Magnifier$Builder;I)V
    .locals 0

    iput p1, p0, Landroid/widget/Magnifier$Builder;->mRightContentBound:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmTopContentBound(Landroid/widget/Magnifier$Builder;I)V
    .locals 0

    iput p1, p0, Landroid/widget/Magnifier$Builder;->mTopContentBound:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmVerticalDefaultSourceToMagnifierOffset(Landroid/widget/Magnifier$Builder;I)V
    .locals 0

    iput p1, p0, Landroid/widget/Magnifier$Builder;->mVerticalDefaultSourceToMagnifierOffset:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmWidth(Landroid/widget/Magnifier$Builder;I)V
    .locals 0

    iput p1, p0, Landroid/widget/Magnifier$Builder;->mWidth:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmZoom(Landroid/widget/Magnifier$Builder;F)V
    .locals 0

    iput p1, p0, Landroid/widget/Magnifier$Builder;->mZoom:F

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Landroid/widget/Magnifier$Builder;->mView:Landroid/view/View;

    invoke-direct {p0}, Landroid/widget/Magnifier$Builder;->applyDefaults()V

    return-void
.end method

.method private applyDefaults()V
    .locals 4

    iget-object v0, p0, Landroid/widget/Magnifier$Builder;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1050124

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Landroid/widget/Magnifier$Builder;->mWidth:I

    const v1, 0x1050121

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Landroid/widget/Magnifier$Builder;->mHeight:I

    const v1, 0x1050120

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Landroid/widget/Magnifier$Builder;->mElevation:F

    const v1, 0x105011f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Landroid/widget/Magnifier$Builder;->mCornerRadius:F

    const v1, 0x1050125

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v1

    iput v1, p0, Landroid/widget/Magnifier$Builder;->mZoom:F

    nop

    const v1, 0x1050122

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Landroid/widget/Magnifier$Builder;->mHorizontalDefaultSourceToMagnifierOffset:I

    nop

    const v1, 0x1050123

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Landroid/widget/Magnifier$Builder;->mVerticalDefaultSourceToMagnifierOffset:I

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const v2, 0x1060198

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v1, p0, Landroid/widget/Magnifier$Builder;->mOverlay:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid/widget/Magnifier$Builder;->mClippingEnabled:Z

    iput v1, p0, Landroid/widget/Magnifier$Builder;->mLeftContentBound:I

    iput v1, p0, Landroid/widget/Magnifier$Builder;->mTopContentBound:I

    iput v1, p0, Landroid/widget/Magnifier$Builder;->mRightContentBound:I

    iput v1, p0, Landroid/widget/Magnifier$Builder;->mBottomContentBound:I

    const/4 v1, 0x0

    iput-boolean v1, p0, Landroid/widget/Magnifier$Builder;->mIsFishEyeStyle:Z

    return-void
.end method


# virtual methods
.method public build()Landroid/widget/Magnifier;
    .locals 2

    new-instance v0, Landroid/widget/Magnifier;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroid/widget/Magnifier;-><init>(Landroid/widget/Magnifier$Builder;Landroid/widget/Magnifier-IA;)V

    return-object v0
.end method

.method public setClippingEnabled(Z)Landroid/widget/Magnifier$Builder;
    .locals 0

    iput-boolean p1, p0, Landroid/widget/Magnifier$Builder;->mClippingEnabled:Z

    return-object p0
.end method

.method public setCornerRadius(F)Landroid/widget/Magnifier$Builder;
    .locals 1

    const-string v0, "Corner radius should be non-negative"

    invoke-static {p1, v0}, Lcom/android/internal/util/Preconditions;->checkArgumentNonNegative(FLjava/lang/String;)F

    iput p1, p0, Landroid/widget/Magnifier$Builder;->mCornerRadius:F

    return-object p0
.end method

.method public setDefaultSourceToMagnifierOffset(II)Landroid/widget/Magnifier$Builder;
    .locals 0

    iput p1, p0, Landroid/widget/Magnifier$Builder;->mHorizontalDefaultSourceToMagnifierOffset:I

    iput p2, p0, Landroid/widget/Magnifier$Builder;->mVerticalDefaultSourceToMagnifierOffset:I

    return-object p0
.end method

.method public setElevation(F)Landroid/widget/Magnifier$Builder;
    .locals 1

    const-string v0, "Elevation should be non-negative"

    invoke-static {p1, v0}, Lcom/android/internal/util/Preconditions;->checkArgumentNonNegative(FLjava/lang/String;)F

    iput p1, p0, Landroid/widget/Magnifier$Builder;->mElevation:F

    return-object p0
.end method

.method setFishEyeStyle()Landroid/widget/Magnifier$Builder;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/widget/Magnifier$Builder;->mIsFishEyeStyle:Z

    return-object p0
.end method

.method public setInitialZoom(F)Landroid/widget/Magnifier$Builder;
    .locals 1

    const-string v0, "Zoom should be positive"

    invoke-static {p1, v0}, Lcom/android/internal/util/Preconditions;->checkArgumentPositive(FLjava/lang/String;)F

    iput p1, p0, Landroid/widget/Magnifier$Builder;->mZoom:F

    return-object p0
.end method

.method public setOverlay(Landroid/graphics/drawable/Drawable;)Landroid/widget/Magnifier$Builder;
    .locals 0

    iput-object p1, p0, Landroid/widget/Magnifier$Builder;->mOverlay:Landroid/graphics/drawable/Drawable;

    return-object p0
.end method

.method public setSize(II)Landroid/widget/Magnifier$Builder;
    .locals 1

    const-string v0, "Width should be positive"

    invoke-static {p1, v0}, Lcom/android/internal/util/Preconditions;->checkArgumentPositive(ILjava/lang/String;)I

    const-string v0, "Height should be positive"

    invoke-static {p2, v0}, Lcom/android/internal/util/Preconditions;->checkArgumentPositive(ILjava/lang/String;)I

    iput p1, p0, Landroid/widget/Magnifier$Builder;->mWidth:I

    iput p2, p0, Landroid/widget/Magnifier$Builder;->mHeight:I

    return-object p0
.end method

.method public setSourceBounds(IIII)Landroid/widget/Magnifier$Builder;
    .locals 0

    iput p1, p0, Landroid/widget/Magnifier$Builder;->mLeftContentBound:I

    iput p2, p0, Landroid/widget/Magnifier$Builder;->mTopContentBound:I

    iput p3, p0, Landroid/widget/Magnifier$Builder;->mRightContentBound:I

    iput p4, p0, Landroid/widget/Magnifier$Builder;->mBottomContentBound:I

    return-object p0
.end method

.method setSourceSize(II)Landroid/widget/Magnifier$Builder;
    .locals 0

    iput p1, p0, Landroid/widget/Magnifier$Builder;->mSourceWidth:I

    iput p2, p0, Landroid/widget/Magnifier$Builder;->mSourceHeight:I

    return-object p0
.end method
