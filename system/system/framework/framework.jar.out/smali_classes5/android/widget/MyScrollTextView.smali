.class public Landroid/widget/MyScrollTextView;
.super Landroid/widget/TextView;
.source "MyScrollTextView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    sget-object v0, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, v0}, Landroid/widget/MyScrollTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    invoke-virtual {p0, v1}, Landroid/widget/MyScrollTextView;->setHorizontallyScrolling(Z)V

    invoke-virtual {p0, v1}, Landroid/widget/MyScrollTextView;->setMaxLines(I)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Landroid/widget/MyScrollTextView;->setMarqueeRepeatLimit(I)V

    invoke-virtual {p0, v1}, Landroid/widget/MyScrollTextView;->setSingleLine(Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-object v0, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, v0}, Landroid/widget/MyScrollTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    invoke-virtual {p0, v1}, Landroid/widget/MyScrollTextView;->setHorizontallyScrolling(Z)V

    invoke-virtual {p0, v1}, Landroid/widget/MyScrollTextView;->setMaxLines(I)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Landroid/widget/MyScrollTextView;->setMarqueeRepeatLimit(I)V

    invoke-virtual {p0, v1}, Landroid/widget/MyScrollTextView;->setSingleLine(Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v0, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, v0}, Landroid/widget/MyScrollTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    invoke-virtual {p0, v1}, Landroid/widget/MyScrollTextView;->setHorizontallyScrolling(Z)V

    invoke-virtual {p0, v1}, Landroid/widget/MyScrollTextView;->setMaxLines(I)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Landroid/widget/MyScrollTextView;->setMarqueeRepeatLimit(I)V

    invoke-virtual {p0, v1}, Landroid/widget/MyScrollTextView;->setSingleLine(Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    sget-object v0, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, v0}, Landroid/widget/MyScrollTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    invoke-virtual {p0, v1}, Landroid/widget/MyScrollTextView;->setHorizontallyScrolling(Z)V

    invoke-virtual {p0, v1}, Landroid/widget/MyScrollTextView;->setMaxLines(I)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Landroid/widget/MyScrollTextView;->setMarqueeRepeatLimit(I)V

    invoke-virtual {p0, v1}, Landroid/widget/MyScrollTextView;->setSingleLine(Z)V

    return-void
.end method


# virtual methods
.method public isFocused()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 0

    if-eqz p1, :cond_0

    invoke-super {p0, p1, p2, p3}, Landroid/widget/TextView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    :cond_0
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0

    if-eqz p1, :cond_0

    invoke-super {p0, p1}, Landroid/widget/TextView;->onWindowFocusChanged(Z)V

    :cond_0
    return-void
.end method

.method public setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
    .locals 1

    sget-object v0, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-super {p0, v0}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    return-void
.end method

.method public setHorizontallyScrolling(Z)V
    .locals 1

    const/4 v0, 0x1

    invoke-super {p0, v0}, Landroid/widget/TextView;->setHorizontallyScrolling(Z)V

    return-void
.end method

.method public setMarqueeRepeatLimit(I)V
    .locals 0

    const/4 p1, -0x1

    invoke-super {p0, p1}, Landroid/widget/TextView;->setMarqueeRepeatLimit(I)V

    return-void
.end method

.method public setMaxLines(I)V
    .locals 0

    const/4 p1, 0x1

    invoke-super {p0, p1}, Landroid/widget/TextView;->setMaxLines(I)V

    return-void
.end method

.method public setSingleLine(Z)V
    .locals 1

    const/4 v0, 0x1

    invoke-super {p0, v0}, Landroid/widget/TextView;->setSingleLine(Z)V

    return-void
.end method
