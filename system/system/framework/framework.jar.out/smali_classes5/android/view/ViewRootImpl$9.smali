.class Landroid/view/ViewRootImpl$9;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/window/SurfaceSyncer$SyncTarget;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/ViewRootImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/view/ViewRootImpl;


# direct methods
.method constructor <init>(Landroid/view/ViewRootImpl;)V
    .locals 0

    iput-object p1, p0, Landroid/view/ViewRootImpl$9;->this$0:Landroid/view/ViewRootImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method synthetic lambda$onSyncComplete$0$android-view-ViewRootImpl$9()V
    .locals 3

    goto/32 :goto_2

    nop

    :goto_0
    invoke-static {v0}, Landroid/view/ViewRootImpl;->-$$Nest$fgetmNumSyncsInProgress(Landroid/view/ViewRootImpl;)I

    move-result v1

    goto/32 :goto_1

    nop

    :goto_1
    const/4 v2, 0x1

    goto/32 :goto_9

    nop

    :goto_2
    iget-object v0, p0, Landroid/view/ViewRootImpl$9;->this$0:Landroid/view/ViewRootImpl;

    goto/32 :goto_0

    nop

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_5

    nop

    :goto_4
    iget-object v0, v0, Landroid/view/View$AttachInfo;->mThreadedRenderer:Landroid/view/ThreadedRenderer;

    goto/32 :goto_3

    nop

    :goto_5
    invoke-static {v2}, Landroid/graphics/HardwareRenderer;->setRtAnimationsEnabled(Z)V

    :goto_6
    goto/32 :goto_c

    nop

    :goto_7
    if-eqz v1, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_b

    nop

    :goto_8
    iget-object v0, v0, Landroid/view/ViewRootImpl;->mAttachInfo:Landroid/view/View$AttachInfo;

    goto/32 :goto_4

    nop

    :goto_9
    sub-int/2addr v1, v2

    goto/32 :goto_a

    nop

    :goto_a
    invoke-static {v0, v1}, Landroid/view/ViewRootImpl;->-$$Nest$fputmNumSyncsInProgress(Landroid/view/ViewRootImpl;I)V

    goto/32 :goto_7

    nop

    :goto_b
    iget-object v0, p0, Landroid/view/ViewRootImpl$9;->this$0:Landroid/view/ViewRootImpl;

    goto/32 :goto_8

    nop

    :goto_c
    return-void
.end method

.method public onReadyToSync(Landroid/window/SurfaceSyncer$SyncBufferCallback;)V
    .locals 1

    iget-object v0, p0, Landroid/view/ViewRootImpl$9;->this$0:Landroid/view/ViewRootImpl;

    invoke-static {v0, p1}, Landroid/view/ViewRootImpl;->-$$Nest$mreadyToSync(Landroid/view/ViewRootImpl;Landroid/window/SurfaceSyncer$SyncBufferCallback;)V

    return-void
.end method

.method public onSyncComplete()V
    .locals 2

    iget-object v0, p0, Landroid/view/ViewRootImpl$9;->this$0:Landroid/view/ViewRootImpl;

    iget-object v0, v0, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    new-instance v1, Landroid/view/ViewRootImpl$9$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Landroid/view/ViewRootImpl$9$$ExternalSyntheticLambda0;-><init>(Landroid/view/ViewRootImpl$9;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewRootImpl$ViewRootHandler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    return-void
.end method
