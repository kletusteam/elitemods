.class Landroid/view/PendingInsetsController$HideRequest;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/PendingInsetsController$PendingRequest;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/PendingInsetsController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HideRequest"
.end annotation


# instance fields
.field private final mTypes:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Landroid/view/PendingInsetsController$HideRequest;->mTypes:I

    return-void
.end method


# virtual methods
.method public replay(Landroid/view/InsetsController;)V
    .locals 1

    iget v0, p0, Landroid/view/PendingInsetsController$HideRequest;->mTypes:I

    invoke-virtual {p1, v0}, Landroid/view/InsetsController;->hide(I)V

    return-void
.end method
