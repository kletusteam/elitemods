.class public Landroid/view/ViewForceDarkStateStub;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static newInstance()Landroid/view/ViewForceDarkStateStub;
    .locals 1

    const-class v0, Landroid/view/ViewForceDarkStateStub;

    invoke-static {v0}, Lcom/miui/base/MiuiStubUtil;->newInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewForceDarkStateStub;

    return-object v0
.end method


# virtual methods
.method public cleanForceDarkViewTreeDirty()V
    .locals 0

    return-void
.end method

.method public getBackgroundBitmapPalette()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getBackgroundDrawableName()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getBitmapPalette(Landroid/graphics/drawable/Drawable;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getForceDarkUsageHint()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public getLayoutXmlName()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getUsageHint()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public getViewClass()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getXmlIndex()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public hasText()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isApplyForceDarkConfig()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isBackgroundLoadedFromResources()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isForceDarkAllowed()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isForceDarkViewDirtyRoot()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isForceDarkViewTreeDirty()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public markForceDarkDirtyRoot()V
    .locals 0

    return-void
.end method

.method public markForceDarkViewTreeDirty()V
    .locals 0

    return-void
.end method

.method public resetForceDarkState()V
    .locals 0

    return-void
.end method

.method public setForceDarkAllowed(Z)V
    .locals 0

    return-void
.end method

.method public setHostView(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public setLayoutXmlName(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public setXmlIndex(I)V
    .locals 0

    return-void
.end method
