.class Landroid/view/ViewPropertyAnimator$PropertyBundle;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/ViewPropertyAnimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PropertyBundle"
.end annotation


# instance fields
.field mNameValuesHolder:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/view/ViewPropertyAnimator$NameValuesHolder;",
            ">;"
        }
    .end annotation
.end field

.field mPropertyMask:I


# direct methods
.method constructor <init>(ILjava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList<",
            "Landroid/view/ViewPropertyAnimator$NameValuesHolder;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Landroid/view/ViewPropertyAnimator$PropertyBundle;->mPropertyMask:I

    iput-object p2, p0, Landroid/view/ViewPropertyAnimator$PropertyBundle;->mNameValuesHolder:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method cancel(I)Z
    .locals 5

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p0, Landroid/view/ViewPropertyAnimator$PropertyBundle;->mPropertyMask:I

    goto/32 :goto_3

    nop

    :goto_1
    iget-object v2, p0, Landroid/view/ViewPropertyAnimator$PropertyBundle;->mNameValuesHolder:Ljava/util/ArrayList;

    goto/32 :goto_1a

    nop

    :goto_2
    check-cast v2, Landroid/view/ViewPropertyAnimator$NameValuesHolder;

    goto/32 :goto_7

    nop

    :goto_3
    and-int/2addr v0, p1

    goto/32 :goto_6

    nop

    :goto_4
    return v3

    :goto_5
    goto/32 :goto_c

    nop

    :goto_6
    if-nez v0, :cond_0

    goto/32 :goto_17

    :cond_0
    goto/32 :goto_12

    nop

    :goto_7
    iget v3, v2, Landroid/view/ViewPropertyAnimator$NameValuesHolder;->mNameConstant:I

    goto/32 :goto_13

    nop

    :goto_8
    const/4 v3, 0x1

    goto/32 :goto_4

    nop

    :goto_9
    return v0

    :goto_a
    if-lt v1, v0, :cond_1

    goto/32 :goto_17

    :cond_1
    goto/32 :goto_1

    nop

    :goto_b
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto/32 :goto_e

    nop

    :goto_c
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_16

    nop

    :goto_d
    if-nez v0, :cond_2

    goto/32 :goto_17

    :cond_2
    goto/32 :goto_1b

    nop

    :goto_e
    iget v3, p0, Landroid/view/ViewPropertyAnimator$PropertyBundle;->mPropertyMask:I

    goto/32 :goto_f

    nop

    :goto_f
    not-int v4, p1

    goto/32 :goto_10

    nop

    :goto_10
    and-int/2addr v3, v4

    goto/32 :goto_19

    nop

    :goto_11
    const/4 v0, 0x0

    goto/32 :goto_9

    nop

    :goto_12
    iget-object v0, p0, Landroid/view/ViewPropertyAnimator$PropertyBundle;->mNameValuesHolder:Ljava/util/ArrayList;

    goto/32 :goto_d

    nop

    :goto_13
    if-eq v3, p1, :cond_3

    goto/32 :goto_5

    :cond_3
    goto/32 :goto_18

    nop

    :goto_14
    const/4 v1, 0x0

    :goto_15
    goto/32 :goto_a

    nop

    :goto_16
    goto :goto_15

    :goto_17
    goto/32 :goto_11

    nop

    :goto_18
    iget-object v3, p0, Landroid/view/ViewPropertyAnimator$PropertyBundle;->mNameValuesHolder:Ljava/util/ArrayList;

    goto/32 :goto_b

    nop

    :goto_19
    iput v3, p0, Landroid/view/ViewPropertyAnimator$PropertyBundle;->mPropertyMask:I

    goto/32 :goto_8

    nop

    :goto_1a
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_2

    nop

    :goto_1b
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_14

    nop
.end method
