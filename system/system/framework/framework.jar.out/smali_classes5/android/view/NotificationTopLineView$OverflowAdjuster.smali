.class final Landroid/view/NotificationTopLineView$OverflowAdjuster;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/NotificationTopLineView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "OverflowAdjuster"
.end annotation


# instance fields
.field private mHeightSpec:I

.field private mOverflow:I

.field private mRegrowView:Landroid/view/View;

.field final synthetic this$0:Landroid/view/NotificationTopLineView;


# direct methods
.method private constructor <init>(Landroid/view/NotificationTopLineView;)V
    .locals 0

    iput-object p1, p0, Landroid/view/NotificationTopLineView$OverflowAdjuster;->this$0:Landroid/view/NotificationTopLineView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/view/NotificationTopLineView;Landroid/view/NotificationTopLineView$OverflowAdjuster-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/view/NotificationTopLineView$OverflowAdjuster;-><init>(Landroid/view/NotificationTopLineView;)V

    return-void
.end method

.method private getHorizontalMargins(Landroid/view/View;)I
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v0}, Landroid/view/ViewGroup$MarginLayoutParams;->getMarginStart()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/ViewGroup$MarginLayoutParams;->getMarginEnd()I

    move-result v2

    add-int/2addr v1, v2

    return v1
.end method


# virtual methods
.method adjust(Landroid/view/View;Landroid/view/View;I)Landroid/view/NotificationTopLineView$OverflowAdjuster;
    .locals 8

    goto/32 :goto_f

    nop

    :goto_0
    return-object p0

    :goto_1
    goto/32 :goto_20

    nop

    :goto_2
    if-nez p2, :cond_0

    goto/32 :goto_1d

    :cond_0
    goto/32 :goto_23

    nop

    :goto_3
    if-nez v3, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_3c

    nop

    :goto_4
    add-int/2addr v7, v1

    goto/32 :goto_25

    nop

    :goto_5
    iget v5, p0, Landroid/view/NotificationTopLineView$OverflowAdjuster;->mOverflow:I

    goto/32 :goto_45

    nop

    :goto_6
    sub-int v2, v0, v2

    goto/32 :goto_44

    nop

    :goto_7
    if-eqz v2, :cond_2

    goto/32 :goto_1d

    :cond_2
    goto/32 :goto_12

    nop

    :goto_8
    invoke-static {v5, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    goto/32 :goto_1b

    nop

    :goto_9
    const/4 v2, 0x0

    :goto_a
    goto/32 :goto_4a

    nop

    :goto_b
    if-eqz p3, :cond_3

    goto/32 :goto_a

    :cond_3
    goto/32 :goto_26

    nop

    :goto_c
    sub-int v5, v1, v5

    goto/32 :goto_3f

    nop

    :goto_d
    if-gtz v0, :cond_4

    goto/32 :goto_1

    :cond_4
    goto/32 :goto_2d

    nop

    :goto_e
    invoke-static {v1}, Landroid/view/NotificationTopLineView;->-$$Nest$fgetmViewsToDisappear(Landroid/view/NotificationTopLineView;)Ljava/util/Set;

    move-result-object v1

    goto/32 :goto_2f

    nop

    :goto_f
    iget v0, p0, Landroid/view/NotificationTopLineView$OverflowAdjuster;->mOverflow:I

    goto/32 :goto_d

    nop

    :goto_10
    iget-object v3, p0, Landroid/view/NotificationTopLineView$OverflowAdjuster;->mRegrowView:Landroid/view/View;

    goto/32 :goto_3

    nop

    :goto_11
    iput v5, p0, Landroid/view/NotificationTopLineView$OverflowAdjuster;->mOverflow:I

    goto/32 :goto_7

    nop

    :goto_12
    iget-object v5, p0, Landroid/view/NotificationTopLineView$OverflowAdjuster;->this$0:Landroid/view/NotificationTopLineView;

    goto/32 :goto_22

    nop

    :goto_13
    iget v2, p0, Landroid/view/NotificationTopLineView$OverflowAdjuster;->mOverflow:I

    goto/32 :goto_6

    nop

    :goto_14
    iget-object v1, p0, Landroid/view/NotificationTopLineView$OverflowAdjuster;->mRegrowView:Landroid/view/View;

    goto/32 :goto_21

    nop

    :goto_15
    if-lt v2, v3, :cond_5

    goto/32 :goto_a

    :cond_5
    goto/32 :goto_10

    nop

    :goto_16
    const/16 v1, 0x8

    goto/32 :goto_3e

    nop

    :goto_17
    invoke-virtual {p2, v5, v6}, Landroid/view/View;->measure(II)V

    goto/32 :goto_1a

    nop

    :goto_18
    if-le v0, p3, :cond_6

    goto/32 :goto_35

    :cond_6
    goto/32 :goto_34

    nop

    :goto_19
    invoke-direct {p0, p2}, Landroid/view/NotificationTopLineView$OverflowAdjuster;->getHorizontalMargins(Landroid/view/View;)I

    move-result v7

    goto/32 :goto_4

    nop

    :goto_1a
    iget v6, p0, Landroid/view/NotificationTopLineView$OverflowAdjuster;->mOverflow:I

    goto/32 :goto_19

    nop

    :goto_1b
    iget v6, p0, Landroid/view/NotificationTopLineView$OverflowAdjuster;->mHeightSpec:I

    goto/32 :goto_17

    nop

    :goto_1c
    iput v6, p0, Landroid/view/NotificationTopLineView$OverflowAdjuster;->mOverflow:I

    :goto_1d
    goto/32 :goto_24

    nop

    :goto_1e
    const/4 v5, 0x0

    goto/32 :goto_8

    nop

    :goto_1f
    if-nez v2, :cond_7

    goto/32 :goto_4c

    :cond_7
    goto/32 :goto_4b

    nop

    :goto_20
    return-object p0

    :goto_21
    if-nez v1, :cond_8

    goto/32 :goto_3b

    :cond_8
    goto/32 :goto_33

    nop

    :goto_22
    invoke-static {v5}, Landroid/view/NotificationTopLineView;->-$$Nest$fgetmViewsToDisappear(Landroid/view/NotificationTopLineView;)Ljava/util/Set;

    move-result-object v5

    goto/32 :goto_49

    nop

    :goto_23
    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result v5

    goto/32 :goto_47

    nop

    :goto_24
    iget v1, p0, Landroid/view/NotificationTopLineView$OverflowAdjuster;->mOverflow:I

    goto/32 :goto_2a

    nop

    :goto_25
    sub-int/2addr v6, v7

    goto/32 :goto_1c

    nop

    :goto_26
    iget-object v3, p0, Landroid/view/NotificationTopLineView$OverflowAdjuster;->this$0:Landroid/view/NotificationTopLineView;

    goto/32 :goto_31

    nop

    :goto_27
    iget v5, p0, Landroid/view/NotificationTopLineView$OverflowAdjuster;->mOverflow:I

    goto/32 :goto_29

    nop

    :goto_28
    invoke-virtual {v6, v3, v7}, Landroid/view/View;->measure(II)V

    goto/32 :goto_42

    nop

    :goto_29
    invoke-direct {p0, p1}, Landroid/view/NotificationTopLineView$OverflowAdjuster;->getHorizontalMargins(Landroid/view/View;)I

    move-result v6

    goto/32 :goto_36

    nop

    :goto_2a
    if-ltz v1, :cond_9

    goto/32 :goto_3b

    :cond_9
    goto/32 :goto_14

    nop

    :goto_2b
    iget v7, p0, Landroid/view/NotificationTopLineView$OverflowAdjuster;->mHeightSpec:I

    goto/32 :goto_28

    nop

    :goto_2c
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    goto/32 :goto_18

    nop

    :goto_2d
    if-nez p1, :cond_a

    goto/32 :goto_1

    :cond_a
    goto/32 :goto_32

    nop

    :goto_2e
    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    goto/32 :goto_39

    nop

    :goto_2f
    invoke-interface {v1, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/32 :goto_30

    nop

    :goto_30
    invoke-virtual {p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    goto/32 :goto_1e

    nop

    :goto_31
    invoke-static {v3}, Landroid/view/NotificationTopLineView;->-$$Nest$fgetmChildHideWidth(Landroid/view/NotificationTopLineView;)I

    move-result v3

    goto/32 :goto_15

    nop

    :goto_32
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    goto/32 :goto_16

    nop

    :goto_33
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    goto/32 :goto_43

    nop

    :goto_34
    return-object p0

    :goto_35
    goto/32 :goto_13

    nop

    :goto_36
    sub-int/2addr v5, v6

    goto/32 :goto_46

    nop

    :goto_37
    goto/16 :goto_1

    :goto_38
    goto/32 :goto_2c

    nop

    :goto_39
    iget v5, p0, Landroid/view/NotificationTopLineView$OverflowAdjuster;->mHeightSpec:I

    goto/32 :goto_41

    nop

    :goto_3a
    return-object p0

    :goto_3b
    goto/32 :goto_1f

    nop

    :goto_3c
    if-ne v3, p1, :cond_b

    goto/32 :goto_a

    :cond_b
    goto/32 :goto_9

    nop

    :goto_3d
    iget-object v6, p0, Landroid/view/NotificationTopLineView$OverflowAdjuster;->mRegrowView:Landroid/view/View;

    goto/32 :goto_2b

    nop

    :goto_3e
    if-eq v0, v1, :cond_c

    goto/32 :goto_38

    :cond_c
    goto/32 :goto_37

    nop

    :goto_3f
    invoke-static {v5, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    goto/32 :goto_3d

    nop

    :goto_40
    sub-int/2addr v5, v6

    goto/32 :goto_11

    nop

    :goto_41
    invoke-virtual {p1, v4, v5}, Landroid/view/View;->measure(II)V

    goto/32 :goto_5

    nop

    :goto_42
    invoke-virtual {p0}, Landroid/view/NotificationTopLineView$OverflowAdjuster;->finish()V

    goto/32 :goto_3a

    nop

    :goto_43
    iget v5, p0, Landroid/view/NotificationTopLineView$OverflowAdjuster;->mOverflow:I

    goto/32 :goto_c

    nop

    :goto_44
    invoke-static {p3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto/32 :goto_b

    nop

    :goto_45
    sub-int v6, v0, v2

    goto/32 :goto_40

    nop

    :goto_46
    iput v5, p0, Landroid/view/NotificationTopLineView$OverflowAdjuster;->mOverflow:I

    goto/32 :goto_2

    nop

    :goto_47
    if-ne v5, v1, :cond_d

    goto/32 :goto_1d

    :cond_d
    goto/32 :goto_48

    nop

    :goto_48
    iget-object v1, p0, Landroid/view/NotificationTopLineView$OverflowAdjuster;->this$0:Landroid/view/NotificationTopLineView;

    goto/32 :goto_e

    nop

    :goto_49
    invoke-interface {v5, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/32 :goto_27

    nop

    :goto_4a
    const/high16 v3, -0x80000000

    goto/32 :goto_2e

    nop

    :goto_4b
    iput-object p1, p0, Landroid/view/NotificationTopLineView$OverflowAdjuster;->mRegrowView:Landroid/view/View;

    :goto_4c
    goto/32 :goto_0

    nop
.end method

.method finish()V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p0, v0, v0}, Landroid/view/NotificationTopLineView$OverflowAdjuster;->resetForOverflow(II)Landroid/view/NotificationTopLineView$OverflowAdjuster;

    goto/32 :goto_2

    nop

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method resetForOverflow(II)Landroid/view/NotificationTopLineView$OverflowAdjuster;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    iput-object v0, p0, Landroid/view/NotificationTopLineView$OverflowAdjuster;->mRegrowView:Landroid/view/View;

    goto/32 :goto_3

    nop

    :goto_1
    iput p1, p0, Landroid/view/NotificationTopLineView$OverflowAdjuster;->mOverflow:I

    goto/32 :goto_4

    nop

    :goto_2
    const/4 v0, 0x0

    goto/32 :goto_0

    nop

    :goto_3
    return-object p0

    :goto_4
    iput p2, p0, Landroid/view/NotificationTopLineView$OverflowAdjuster;->mHeightSpec:I

    goto/32 :goto_2

    nop
.end method
