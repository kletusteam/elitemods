.class public interface abstract Landroid/view/translation/ViewTranslationCallback;
.super Ljava/lang/Object;


# virtual methods
.method public enableContentPadding()V
    .locals 0

    return-void
.end method

.method public abstract onClearTranslation(Landroid/view/View;)Z
.end method

.method public abstract onHideTranslation(Landroid/view/View;)Z
.end method

.method public abstract onShowTranslation(Landroid/view/View;)Z
.end method

.method public setAnimationDurationMillis(I)V
    .locals 0

    return-void
.end method
