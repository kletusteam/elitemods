.class public interface abstract Landroid/view/Window$WindowControllerCallback;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/Window;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "WindowControllerCallback"
.end annotation


# virtual methods
.method public abstract enterPictureInPictureModeIfPossible()V
.end method

.method public abstract getWindowingMode()I
.end method

.method public abstract isInSplitScreen()Z
.end method

.method public abstract isTaskRoot()Z
.end method

.method public abstract toggleFreeformWindowingMode()V
.end method

.method public abstract updateNavigationBarColor(I)V
.end method

.method public abstract updateStatusBarColor(I)V
.end method
