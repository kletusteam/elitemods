.class public final Landroid/view/RemoteAnimationTargetProto;
.super Ljava/lang/Object;


# static fields
.field public static final CLIP_RECT:J = 0x10b00000005L

.field public static final CONTENT_INSETS:J = 0x10b00000006L

.field public static final IS_TRANSLUCENT:J = 0x10800000004L

.field public static final LEASH:J = 0x10b00000003L

.field public static final LOCAL_BOUNDS:J = 0x10b0000000dL

.field public static final MODE:J = 0x10500000002L

.field public static final PACKAGE_NAME:J = 0x1090000000fL

.field public static final POSITION:J = 0x10b00000008L

.field public static final PREFIX_ORDER_INDEX:J = 0x10500000007L

.field public static final SCREEN_SPACE_BOUNDS:J = 0x10b0000000eL

.field public static final SOURCE_CONTAINER_BOUNDS:J = 0x10b00000009L

.field public static final START_BOUNDS:J = 0x10b0000000cL

.field public static final START_LEASH:J = 0x10b0000000bL

.field public static final TASK_ID:J = 0x10500000001L

.field public static final WINDOW_CONFIGURATION:J = 0x10b0000000aL


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
