.class public interface abstract annotation Landroid/view/FrameMetrics$Index;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/FrameMetrics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2609
    name = "Index"
.end annotation

.annotation runtime Ljava/lang/annotation/Retention;
    value = .enum Ljava/lang/annotation/RetentionPolicy;->SOURCE:Ljava/lang/annotation/RetentionPolicy;
.end annotation


# static fields
.field public static final ANIMATION_START:I = 0x6

.field public static final COMMAND_SUBMISSION_COMPLETED:I = 0x16

.field public static final DEQUEUE_BUFFER_DURATION:I = 0x11

.field public static final DISPLAY_PRESENT_TIME:I = 0x15

.field public static final DRAW_START:I = 0x8

.field public static final FLAGS:I = 0x0

.field public static final FRAME_COMPLETED:I = 0x10

.field public static final FRAME_DEADLINE:I = 0x9

.field public static final FRAME_INTERVAL:I = 0xb

.field public static final FRAME_START_TIME:I = 0xa

.field public static final FRAME_STATS_COUNT:I = 0x17

.field public static final FRAME_TIMELINE_VSYNC_ID:I = 0x1

.field public static final GPU_COMPLETED:I = 0x13

.field public static final HANDLE_INPUT_START:I = 0x5

.field public static final INPUT_EVENT_ID:I = 0x4

.field public static final INTENDED_VSYNC:I = 0x2

.field public static final ISSUE_DRAW_COMMANDS_START:I = 0xe

.field public static final PERFORM_TRAVERSALS_START:I = 0x7

.field public static final QUEUE_BUFFER_DURATION:I = 0x12

.field public static final SWAP_BUFFERS:I = 0xf

.field public static final SWAP_BUFFERS_COMPLETED:I = 0x14

.field public static final SYNC_QUEUED:I = 0xc

.field public static final SYNC_START:I = 0xd

.field public static final VSYNC:I = 0x3
