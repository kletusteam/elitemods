.class final Landroid/view/contentcapture/ChildContentCaptureSession;
.super Landroid/view/contentcapture/ContentCaptureSession;


# instance fields
.field private final mParent:Landroid/view/contentcapture/ContentCaptureSession;


# direct methods
.method protected constructor <init>(Landroid/view/contentcapture/ContentCaptureSession;Landroid/view/contentcapture/ContentCaptureContext;)V
    .locals 0

    invoke-direct {p0, p2}, Landroid/view/contentcapture/ContentCaptureSession;-><init>(Landroid/view/contentcapture/ContentCaptureContext;)V

    iput-object p1, p0, Landroid/view/contentcapture/ChildContentCaptureSession;->mParent:Landroid/view/contentcapture/ContentCaptureSession;

    return-void
.end method


# virtual methods
.method flush(I)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {v0, p1}, Landroid/view/contentcapture/ContentCaptureSession;->flush(I)V

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, Landroid/view/contentcapture/ChildContentCaptureSession;->mParent:Landroid/view/contentcapture/ContentCaptureSession;

    goto/32 :goto_1

    nop
.end method

.method getMainCaptureSession()Landroid/view/contentcapture/MainContentCaptureSession;
    .locals 2

    goto/32 :goto_7

    nop

    :goto_0
    invoke-virtual {v0}, Landroid/view/contentcapture/ContentCaptureSession;->getMainCaptureSession()Landroid/view/contentcapture/MainContentCaptureSession;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_1
    instance-of v1, v0, Landroid/view/contentcapture/MainContentCaptureSession;

    goto/32 :goto_6

    nop

    :goto_2
    return-object v0

    :goto_3
    goto/32 :goto_0

    nop

    :goto_4
    return-object v0

    :goto_5
    check-cast v0, Landroid/view/contentcapture/MainContentCaptureSession;

    goto/32 :goto_2

    nop

    :goto_6
    if-nez v1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_5

    nop

    :goto_7
    iget-object v0, p0, Landroid/view/contentcapture/ChildContentCaptureSession;->mParent:Landroid/view/contentcapture/ContentCaptureSession;

    goto/32 :goto_1

    nop
.end method

.method internalNotifySessionPaused()V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {v0}, Landroid/view/contentcapture/MainContentCaptureSession;->notifySessionPaused()V

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    invoke-virtual {p0}, Landroid/view/contentcapture/ChildContentCaptureSession;->getMainCaptureSession()Landroid/view/contentcapture/MainContentCaptureSession;

    move-result-object v0

    goto/32 :goto_0

    nop
.end method

.method internalNotifySessionResumed()V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0}, Landroid/view/contentcapture/ChildContentCaptureSession;->getMainCaptureSession()Landroid/view/contentcapture/MainContentCaptureSession;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    invoke-virtual {v0}, Landroid/view/contentcapture/MainContentCaptureSession;->notifySessionResumed()V

    goto/32 :goto_1

    nop
.end method

.method internalNotifyViewAppeared(Landroid/view/contentcapture/ViewNode$ViewStructureImpl;)V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    iget v1, p0, Landroid/view/contentcapture/ChildContentCaptureSession;->mId:I

    goto/32 :goto_3

    nop

    :goto_1
    return-void

    :goto_2
    invoke-virtual {p0}, Landroid/view/contentcapture/ChildContentCaptureSession;->getMainCaptureSession()Landroid/view/contentcapture/MainContentCaptureSession;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_3
    invoke-virtual {v0, v1, p1}, Landroid/view/contentcapture/MainContentCaptureSession;->notifyViewAppeared(ILandroid/view/contentcapture/ViewNode$ViewStructureImpl;)V

    goto/32 :goto_1

    nop
.end method

.method internalNotifyViewDisappeared(Landroid/view/autofill/AutofillId;)V
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {v0, v1, p1}, Landroid/view/contentcapture/MainContentCaptureSession;->notifyViewDisappeared(ILandroid/view/autofill/AutofillId;)V

    goto/32 :goto_0

    nop

    :goto_2
    iget v1, p0, Landroid/view/contentcapture/ChildContentCaptureSession;->mId:I

    goto/32 :goto_1

    nop

    :goto_3
    invoke-virtual {p0}, Landroid/view/contentcapture/ChildContentCaptureSession;->getMainCaptureSession()Landroid/view/contentcapture/MainContentCaptureSession;

    move-result-object v0

    goto/32 :goto_2

    nop
.end method

.method internalNotifyViewInsetsChanged(Landroid/graphics/Insets;)V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0}, Landroid/view/contentcapture/ChildContentCaptureSession;->getMainCaptureSession()Landroid/view/contentcapture/MainContentCaptureSession;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_1
    invoke-virtual {v0, v1, p1}, Landroid/view/contentcapture/MainContentCaptureSession;->notifyViewInsetsChanged(ILandroid/graphics/Insets;)V

    goto/32 :goto_2

    nop

    :goto_2
    return-void

    :goto_3
    iget v1, p0, Landroid/view/contentcapture/ChildContentCaptureSession;->mId:I

    goto/32 :goto_1

    nop
.end method

.method internalNotifyViewTextChanged(Landroid/view/autofill/AutofillId;Ljava/lang/CharSequence;)V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0}, Landroid/view/contentcapture/ChildContentCaptureSession;->getMainCaptureSession()Landroid/view/contentcapture/MainContentCaptureSession;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_2
    iget v1, p0, Landroid/view/contentcapture/ChildContentCaptureSession;->mId:I

    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {v0, v1, p1, p2}, Landroid/view/contentcapture/MainContentCaptureSession;->notifyViewTextChanged(ILandroid/view/autofill/AutofillId;Ljava/lang/CharSequence;)V

    goto/32 :goto_0

    nop
.end method

.method public internalNotifyViewTreeEvent(Z)V
    .locals 2

    invoke-virtual {p0}, Landroid/view/contentcapture/ChildContentCaptureSession;->getMainCaptureSession()Landroid/view/contentcapture/MainContentCaptureSession;

    move-result-object v0

    iget v1, p0, Landroid/view/contentcapture/ChildContentCaptureSession;->mId:I

    invoke-virtual {v0, v1, p1}, Landroid/view/contentcapture/MainContentCaptureSession;->notifyViewTreeEvent(IZ)V

    return-void
.end method

.method isContentCaptureEnabled()Z
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    invoke-virtual {p0}, Landroid/view/contentcapture/ChildContentCaptureSession;->getMainCaptureSession()Landroid/view/contentcapture/MainContentCaptureSession;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {v0}, Landroid/view/contentcapture/MainContentCaptureSession;->isContentCaptureEnabled()Z

    move-result v0

    goto/32 :goto_0

    nop
.end method

.method newChild(Landroid/view/contentcapture/ContentCaptureContext;)Landroid/view/contentcapture/ContentCaptureSession;
    .locals 4

    goto/32 :goto_1

    nop

    :goto_0
    iget v2, p0, Landroid/view/contentcapture/ChildContentCaptureSession;->mId:I

    goto/32 :goto_2

    nop

    :goto_1
    new-instance v0, Landroid/view/contentcapture/ChildContentCaptureSession;

    goto/32 :goto_5

    nop

    :goto_2
    iget v3, v0, Landroid/view/contentcapture/ContentCaptureSession;->mId:I

    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {v1, v2, v3, p1}, Landroid/view/contentcapture/MainContentCaptureSession;->notifyChildSessionStarted(IILandroid/view/contentcapture/ContentCaptureContext;)V

    goto/32 :goto_6

    nop

    :goto_4
    invoke-virtual {p0}, Landroid/view/contentcapture/ChildContentCaptureSession;->getMainCaptureSession()Landroid/view/contentcapture/MainContentCaptureSession;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_5
    invoke-direct {v0, p0, p1}, Landroid/view/contentcapture/ChildContentCaptureSession;-><init>(Landroid/view/contentcapture/ContentCaptureSession;Landroid/view/contentcapture/ContentCaptureContext;)V

    goto/32 :goto_4

    nop

    :goto_6
    return-object v0
.end method

.method onDestroy()V
    .locals 3

    goto/32 :goto_3

    nop

    :goto_0
    invoke-virtual {v0, v1, v2}, Landroid/view/contentcapture/MainContentCaptureSession;->notifyChildSessionFinished(II)V

    goto/32 :goto_2

    nop

    :goto_1
    iget v1, v1, Landroid/view/contentcapture/ContentCaptureSession;->mId:I

    goto/32 :goto_5

    nop

    :goto_2
    return-void

    :goto_3
    invoke-virtual {p0}, Landroid/view/contentcapture/ChildContentCaptureSession;->getMainCaptureSession()Landroid/view/contentcapture/MainContentCaptureSession;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_4
    iget-object v1, p0, Landroid/view/contentcapture/ChildContentCaptureSession;->mParent:Landroid/view/contentcapture/ContentCaptureSession;

    goto/32 :goto_1

    nop

    :goto_5
    iget v2, p0, Landroid/view/contentcapture/ChildContentCaptureSession;->mId:I

    goto/32 :goto_0

    nop
.end method

.method public updateContentCaptureContext(Landroid/view/contentcapture/ContentCaptureContext;)V
    .locals 2

    invoke-virtual {p0}, Landroid/view/contentcapture/ChildContentCaptureSession;->getMainCaptureSession()Landroid/view/contentcapture/MainContentCaptureSession;

    move-result-object v0

    iget v1, p0, Landroid/view/contentcapture/ChildContentCaptureSession;->mId:I

    invoke-virtual {v0, v1, p1}, Landroid/view/contentcapture/MainContentCaptureSession;->notifyContextUpdated(ILandroid/view/contentcapture/ContentCaptureContext;)V

    return-void
.end method
