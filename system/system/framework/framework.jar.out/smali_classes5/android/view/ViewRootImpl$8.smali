.class Landroid/view/ViewRootImpl$8;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/graphics/HardwareRenderer$FrameDrawingCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/view/ViewRootImpl;->registerCallbacksForSync(ZLandroid/window/SurfaceSyncer$SyncBufferCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/view/ViewRootImpl;

.field final synthetic val$syncBuffer:Z

.field final synthetic val$syncBufferCallback:Landroid/window/SurfaceSyncer$SyncBufferCallback;


# direct methods
.method constructor <init>(Landroid/view/ViewRootImpl;Landroid/window/SurfaceSyncer$SyncBufferCallback;Z)V
    .locals 0

    iput-object p1, p0, Landroid/view/ViewRootImpl$8;->this$0:Landroid/view/ViewRootImpl;

    iput-object p2, p0, Landroid/view/ViewRootImpl$8;->val$syncBufferCallback:Landroid/window/SurfaceSyncer$SyncBufferCallback;

    iput-boolean p3, p0, Landroid/view/ViewRootImpl$8;->val$syncBuffer:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method synthetic lambda$onFrameDraw$0$android-view-ViewRootImpl$8(JLandroid/window/SurfaceSyncer$SyncBufferCallback;ZZ)V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_c

    nop

    :goto_1
    invoke-interface {p3, v0}, Landroid/window/SurfaceSyncer$SyncBufferCallback;->onBufferReady(Landroid/view/SurfaceControl$Transaction;)V

    goto/32 :goto_9

    nop

    :goto_2
    invoke-static {v1}, Landroid/view/ViewRootImpl;->-$$Nest$fgetmBlastBufferQueue(Landroid/view/ViewRootImpl;)Landroid/graphics/BLASTBufferQueue;

    move-result-object v1

    goto/32 :goto_4

    nop

    :goto_3
    return-void

    :goto_4
    invoke-virtual {v1, v0}, Landroid/graphics/BLASTBufferQueue;->syncNextTransaction(Ljava/util/function/Consumer;)V

    goto/32 :goto_5

    nop

    :goto_5
    iget-object v0, p0, Landroid/view/ViewRootImpl$8;->this$0:Landroid/view/ViewRootImpl;

    goto/32 :goto_6

    nop

    :goto_6
    invoke-static {v0}, Landroid/view/ViewRootImpl;->-$$Nest$fgetmBlastBufferQueue(Landroid/view/ViewRootImpl;)Landroid/graphics/BLASTBufferQueue;

    move-result-object v0

    goto/32 :goto_b

    nop

    :goto_7
    if-eqz p4, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_d

    nop

    :goto_8
    iget-object v1, p0, Landroid/view/ViewRootImpl$8;->this$0:Landroid/view/ViewRootImpl;

    goto/32 :goto_2

    nop

    :goto_9
    return-void

    :goto_a
    goto/32 :goto_7

    nop

    :goto_b
    invoke-virtual {v0, p1, p2}, Landroid/graphics/BLASTBufferQueue;->gatherPendingTransactions(J)Landroid/view/SurfaceControl$Transaction;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_c
    if-eqz p5, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_8

    nop

    :goto_d
    invoke-interface {p3, v0}, Landroid/window/SurfaceSyncer$SyncBufferCallback;->onBufferReady(Landroid/view/SurfaceControl$Transaction;)V

    :goto_e
    goto/32 :goto_3

    nop
.end method

.method public onFrameDraw(IJ)Landroid/graphics/HardwareRenderer$FrameCommitCallback;
    .locals 9

    and-int/lit8 v0, p1, 0x6

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/view/ViewRootImpl$8;->val$syncBufferCallback:Landroid/window/SurfaceSyncer$SyncBufferCallback;

    iget-object v1, p0, Landroid/view/ViewRootImpl$8;->this$0:Landroid/view/ViewRootImpl;

    invoke-static {v1}, Landroid/view/ViewRootImpl;->-$$Nest$fgetmBlastBufferQueue(Landroid/view/ViewRootImpl;)Landroid/graphics/BLASTBufferQueue;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Landroid/graphics/BLASTBufferQueue;->gatherPendingTransactions(J)Landroid/view/SurfaceControl$Transaction;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/window/SurfaceSyncer$SyncBufferCallback;->onBufferReady(Landroid/view/SurfaceControl$Transaction;)V

    const/4 v0, 0x0

    return-object v0

    :cond_0
    iget-boolean v0, p0, Landroid/view/ViewRootImpl$8;->val$syncBuffer:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/view/ViewRootImpl$8;->this$0:Landroid/view/ViewRootImpl;

    invoke-static {v0}, Landroid/view/ViewRootImpl;->-$$Nest$fgetmBlastBufferQueue(Landroid/view/ViewRootImpl;)Landroid/graphics/BLASTBufferQueue;

    move-result-object v0

    iget-object v1, p0, Landroid/view/ViewRootImpl$8;->val$syncBufferCallback:Landroid/window/SurfaceSyncer$SyncBufferCallback;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Landroid/view/ViewRootImpl$8$$ExternalSyntheticLambda0;

    invoke-direct {v2, v1}, Landroid/view/ViewRootImpl$8$$ExternalSyntheticLambda0;-><init>(Landroid/window/SurfaceSyncer$SyncBufferCallback;)V

    invoke-virtual {v0, v2}, Landroid/graphics/BLASTBufferQueue;->syncNextTransaction(Ljava/util/function/Consumer;)V

    :cond_1
    iget-object v7, p0, Landroid/view/ViewRootImpl$8;->val$syncBufferCallback:Landroid/window/SurfaceSyncer$SyncBufferCallback;

    iget-boolean v8, p0, Landroid/view/ViewRootImpl$8;->val$syncBuffer:Z

    new-instance v0, Landroid/view/ViewRootImpl$8$$ExternalSyntheticLambda1;

    move-object v3, v0

    move-object v4, p0

    move-wide v5, p2

    invoke-direct/range {v3 .. v8}, Landroid/view/ViewRootImpl$8$$ExternalSyntheticLambda1;-><init>(Landroid/view/ViewRootImpl$8;JLandroid/window/SurfaceSyncer$SyncBufferCallback;Z)V

    return-object v0
.end method

.method public onFrameDraw(J)V
    .locals 0

    return-void
.end method
