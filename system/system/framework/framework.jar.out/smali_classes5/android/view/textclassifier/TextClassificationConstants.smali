.class public final Landroid/view/textclassifier/TextClassificationConstants;
.super Ljava/lang/Object;


# static fields
.field private static final DEFAULT_TEXT_CLASSIFIER_SERVICE_PACKAGE_OVERRIDE:Ljava/lang/String;

.field static final GENERATE_LINKS_MAX_TEXT_LENGTH:Ljava/lang/String; = "generate_links_max_text_length"

.field private static final GENERATE_LINKS_MAX_TEXT_LENGTH_DEFAULT:I = 0x186a0

.field static final LOCAL_TEXT_CLASSIFIER_ENABLED:Ljava/lang/String; = "local_textclassifier_enabled"

.field private static final LOCAL_TEXT_CLASSIFIER_ENABLED_DEFAULT:Z = true

.field private static final MODEL_DARK_LAUNCH_ENABLED:Ljava/lang/String; = "model_dark_launch_enabled"

.field private static final MODEL_DARK_LAUNCH_ENABLED_DEFAULT:Z = false

.field private static final SMART_LINKIFY_ENABLED:Ljava/lang/String; = "smart_linkify_enabled"

.field private static final SMART_LINKIFY_ENABLED_DEFAULT:Z = true

.field private static final SMART_SELECTION_ENABLED:Ljava/lang/String; = "smart_selection_enabled"

.field private static final SMART_SELECTION_ENABLED_DEFAULT:Z = true

.field private static final SMART_SELECTION_TRIM_DELTA:Ljava/lang/String; = "smart_selection_trim_delta"

.field private static final SMART_SELECTION_TRIM_DELTA_DEFAULT:I = 0x78

.field private static final SMART_SELECT_ANIMATION_ENABLED:Ljava/lang/String; = "smart_select_animation_enabled"

.field private static final SMART_SELECT_ANIMATION_ENABLED_DEFAULT:Z = true

.field private static final SMART_TEXT_SHARE_ENABLED:Ljava/lang/String; = "smart_text_share_enabled"

.field private static final SMART_TEXT_SHARE_ENABLED_DEFAULT:Z = true

.field static final SYSTEM_TEXT_CLASSIFIER_API_TIMEOUT_IN_SECOND:Ljava/lang/String; = "system_textclassifier_api_timeout_in_second"

.field private static final SYSTEM_TEXT_CLASSIFIER_API_TIMEOUT_IN_SECOND_DEFAULT:J = 0x3cL

.field static final SYSTEM_TEXT_CLASSIFIER_ENABLED:Ljava/lang/String; = "system_textclassifier_enabled"

.field private static final SYSTEM_TEXT_CLASSIFIER_ENABLED_DEFAULT:Z = true

.field static final TEXT_CLASSIFIER_SERVICE_PACKAGE_OVERRIDE:Ljava/lang/String; = "textclassifier_service_package_override"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Landroid/view/textclassifier/TextClassificationConstants;->DEFAULT_TEXT_CLASSIFIER_SERVICE_PACKAGE_OVERRIDE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method dump(Lcom/android/internal/util/IndentingPrintWriter;)V
    .locals 2

    goto/32 :goto_17

    nop

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/32 :goto_2e

    nop

    :goto_1
    invoke-virtual {p0}, Landroid/view/textclassifier/TextClassificationConstants;->isSmartSelectionEnabled()Z

    move-result v0

    goto/32 :goto_0

    nop

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/32 :goto_23

    nop

    :goto_3
    invoke-virtual {p0}, Landroid/view/textclassifier/TextClassificationConstants;->getGenerateLinksMaxTextLength()I

    move-result v0

    goto/32 :goto_d

    nop

    :goto_4
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/32 :goto_27

    nop

    :goto_5
    const-string v1, "generate_links_max_text_length"

    goto/32 :goto_28

    nop

    :goto_6
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto/32 :goto_1d

    nop

    :goto_7
    invoke-virtual {p1, v0}, Lcom/android/internal/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    goto/32 :goto_1a

    nop

    :goto_8
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;Ljava/lang/Object;)Landroid/util/IndentingPrintWriter;

    move-result-object v0

    goto/32 :goto_18

    nop

    :goto_9
    invoke-virtual {v0}, Landroid/util/IndentingPrintWriter;->println()V

    goto/32 :goto_22

    nop

    :goto_a
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;Ljava/lang/Object;)Landroid/util/IndentingPrintWriter;

    move-result-object v0

    goto/32 :goto_35

    nop

    :goto_b
    const-string v1, "local_textclassifier_enabled"

    goto/32 :goto_39

    nop

    :goto_c
    invoke-virtual {p0}, Landroid/view/textclassifier/TextClassificationConstants;->getSmartSelectionTrimDelta()I

    move-result v0

    goto/32 :goto_32

    nop

    :goto_d
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_e
    invoke-virtual {v0}, Landroid/util/IndentingPrintWriter;->println()V

    goto/32 :goto_2f

    nop

    :goto_f
    const-string/jumbo v1, "smart_selection_trim_delta"

    goto/32 :goto_1e

    nop

    :goto_10
    invoke-virtual {v0}, Landroid/util/IndentingPrintWriter;->println()V

    goto/32 :goto_25

    nop

    :goto_11
    invoke-virtual {v0}, Landroid/util/IndentingPrintWriter;->println()V

    nop

    goto/32 :goto_2d

    nop

    :goto_12
    invoke-virtual {p0}, Landroid/view/textclassifier/TextClassificationConstants;->getTextClassifierServicePackageOverride()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_2b

    nop

    :goto_13
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/32 :goto_37

    nop

    :goto_14
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;Ljava/lang/Object;)Landroid/util/IndentingPrintWriter;

    move-result-object v0

    goto/32 :goto_e

    nop

    :goto_15
    invoke-virtual {p0}, Landroid/view/textclassifier/TextClassificationConstants;->isSmartLinkifyEnabled()Z

    move-result v0

    goto/32 :goto_13

    nop

    :goto_16
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;Ljava/lang/Object;)Landroid/util/IndentingPrintWriter;

    move-result-object v0

    goto/32 :goto_24

    nop

    :goto_17
    const-string v0, "TextClassificationConstants:"

    goto/32 :goto_7

    nop

    :goto_18
    invoke-virtual {v0}, Landroid/util/IndentingPrintWriter;->println()V

    goto/32 :goto_1

    nop

    :goto_19
    invoke-virtual {p0}, Landroid/view/textclassifier/TextClassificationConstants;->isSmartSelectionAnimationEnabled()Z

    move-result v0

    goto/32 :goto_29

    nop

    :goto_1a
    invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->increaseIndent()Lcom/android/internal/util/IndentingPrintWriter;

    goto/32 :goto_3

    nop

    :goto_1b
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/32 :goto_b

    nop

    :goto_1c
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;Ljava/lang/Object;)Landroid/util/IndentingPrintWriter;

    move-result-object v0

    goto/32 :goto_33

    nop

    :goto_1d
    const-string/jumbo v1, "system_textclassifier_api_timeout_in_second"

    goto/32 :goto_1c

    nop

    :goto_1e
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;Ljava/lang/Object;)Landroid/util/IndentingPrintWriter;

    move-result-object v0

    goto/32 :goto_2a

    nop

    :goto_1f
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;Ljava/lang/Object;)Landroid/util/IndentingPrintWriter;

    move-result-object v0

    goto/32 :goto_26

    nop

    :goto_20
    return-void

    :goto_21
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;Ljava/lang/Object;)Landroid/util/IndentingPrintWriter;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_22
    invoke-virtual {p0}, Landroid/view/textclassifier/TextClassificationConstants;->isSmartTextShareEnabled()Z

    move-result v0

    goto/32 :goto_2

    nop

    :goto_23
    const-string/jumbo v1, "smart_text_share_enabled"

    goto/32 :goto_14

    nop

    :goto_24
    invoke-virtual {v0}, Landroid/util/IndentingPrintWriter;->println()V

    goto/32 :goto_15

    nop

    :goto_25
    invoke-virtual {p0}, Landroid/view/textclassifier/TextClassificationConstants;->isModelDarkLaunchEnabled()Z

    move-result v0

    goto/32 :goto_30

    nop

    :goto_26
    invoke-virtual {v0}, Landroid/util/IndentingPrintWriter;->println()V

    nop

    goto/32 :goto_12

    nop

    :goto_27
    const-string/jumbo v1, "system_textclassifier_enabled"

    goto/32 :goto_1f

    nop

    :goto_28
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;Ljava/lang/Object;)Landroid/util/IndentingPrintWriter;

    move-result-object v0

    goto/32 :goto_34

    nop

    :goto_29
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/32 :goto_36

    nop

    :goto_2a
    invoke-virtual {v0}, Landroid/util/IndentingPrintWriter;->println()V

    goto/32 :goto_38

    nop

    :goto_2b
    const-string/jumbo v1, "textclassifier_service_package_override"

    goto/32 :goto_3a

    nop

    :goto_2c
    const-string/jumbo v1, "model_dark_launch_enabled"

    goto/32 :goto_16

    nop

    :goto_2d
    invoke-virtual {p0}, Landroid/view/textclassifier/TextClassificationConstants;->getSystemTextClassifierApiTimeoutInSecond()J

    move-result-wide v0

    goto/32 :goto_6

    nop

    :goto_2e
    const-string/jumbo v1, "smart_selection_enabled"

    goto/32 :goto_21

    nop

    :goto_2f
    invoke-virtual {p0}, Landroid/view/textclassifier/TextClassificationConstants;->isSystemTextClassifierEnabled()Z

    move-result v0

    goto/32 :goto_4

    nop

    :goto_30
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/32 :goto_2c

    nop

    :goto_31
    invoke-virtual {p0}, Landroid/view/textclassifier/TextClassificationConstants;->isLocalTextClassifierEnabled()Z

    move-result v0

    goto/32 :goto_1b

    nop

    :goto_32
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/32 :goto_f

    nop

    :goto_33
    invoke-virtual {v0}, Landroid/util/IndentingPrintWriter;->println()V

    goto/32 :goto_c

    nop

    :goto_34
    invoke-virtual {v0}, Landroid/util/IndentingPrintWriter;->println()V

    goto/32 :goto_31

    nop

    :goto_35
    invoke-virtual {v0}, Landroid/util/IndentingPrintWriter;->println()V

    goto/32 :goto_19

    nop

    :goto_36
    const-string/jumbo v1, "smart_select_animation_enabled"

    goto/32 :goto_8

    nop

    :goto_37
    const-string/jumbo v1, "smart_linkify_enabled"

    goto/32 :goto_a

    nop

    :goto_38
    invoke-virtual {p1}, Lcom/android/internal/util/IndentingPrintWriter;->decreaseIndent()Lcom/android/internal/util/IndentingPrintWriter;

    goto/32 :goto_20

    nop

    :goto_39
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;Ljava/lang/Object;)Landroid/util/IndentingPrintWriter;

    move-result-object v0

    goto/32 :goto_10

    nop

    :goto_3a
    invoke-virtual {p1, v1, v0}, Lcom/android/internal/util/IndentingPrintWriter;->print(Ljava/lang/String;Ljava/lang/Object;)Landroid/util/IndentingPrintWriter;

    move-result-object v0

    goto/32 :goto_11

    nop
.end method

.method public getGenerateLinksMaxTextLength()I
    .locals 3

    const-string/jumbo v0, "textclassifier"

    const-string v1, "generate_links_max_text_length"

    const v2, 0x186a0

    invoke-static {v0, v1, v2}, Landroid/provider/DeviceConfig;->getInt(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getSmartSelectionTrimDelta()I
    .locals 3

    const-string/jumbo v0, "textclassifier"

    const-string/jumbo v1, "smart_selection_trim_delta"

    const/16 v2, 0x78

    invoke-static {v0, v1, v2}, Landroid/provider/DeviceConfig;->getInt(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getSystemTextClassifierApiTimeoutInSecond()J
    .locals 4

    const-string/jumbo v0, "textclassifier"

    const-string/jumbo v1, "system_textclassifier_api_timeout_in_second"

    const-wide/16 v2, 0x3c

    invoke-static {v0, v1, v2, v3}, Landroid/provider/DeviceConfig;->getLong(Ljava/lang/String;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getTextClassifierServicePackageOverride()Ljava/lang/String;
    .locals 3

    sget-object v0, Landroid/view/textclassifier/TextClassificationConstants;->DEFAULT_TEXT_CLASSIFIER_SERVICE_PACKAGE_OVERRIDE:Ljava/lang/String;

    const-string/jumbo v1, "textclassifier"

    const-string/jumbo v2, "textclassifier_service_package_override"

    invoke-static {v1, v2, v0}, Landroid/provider/DeviceConfig;->getString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isLocalTextClassifierEnabled()Z
    .locals 3

    const-string/jumbo v0, "textclassifier"

    const-string v1, "local_textclassifier_enabled"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/DeviceConfig;->getBoolean(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isModelDarkLaunchEnabled()Z
    .locals 3

    const-string/jumbo v0, "textclassifier"

    const-string/jumbo v1, "model_dark_launch_enabled"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/DeviceConfig;->getBoolean(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isSmartLinkifyEnabled()Z
    .locals 3

    const-string/jumbo v0, "textclassifier"

    const-string/jumbo v1, "smart_linkify_enabled"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/DeviceConfig;->getBoolean(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isSmartSelectionAnimationEnabled()Z
    .locals 3

    const-string/jumbo v0, "textclassifier"

    const-string/jumbo v1, "smart_select_animation_enabled"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/DeviceConfig;->getBoolean(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isSmartSelectionEnabled()Z
    .locals 3

    const-string/jumbo v0, "textclassifier"

    const-string/jumbo v1, "smart_selection_enabled"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/DeviceConfig;->getBoolean(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isSmartTextShareEnabled()Z
    .locals 3

    const-string/jumbo v0, "textclassifier"

    const-string/jumbo v1, "smart_text_share_enabled"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/DeviceConfig;->getBoolean(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isSystemTextClassifierEnabled()Z
    .locals 3

    const-string/jumbo v0, "textclassifier"

    const-string/jumbo v1, "system_textclassifier_enabled"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/DeviceConfig;->getBoolean(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
