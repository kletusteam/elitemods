.class final Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/textclassifier/TextClassificationSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "SelectionEventHelper"
.end annotation


# instance fields
.field private final mContext:Landroid/view/textclassifier/TextClassificationContext;

.field private mInvocationMethod:I

.field private mPrevEvent:Landroid/view/textclassifier/SelectionEvent;

.field private final mSessionId:Landroid/view/textclassifier/TextClassificationSessionId;

.field private mSmartEvent:Landroid/view/textclassifier/SelectionEvent;

.field private mStartEvent:Landroid/view/textclassifier/SelectionEvent;


# direct methods
.method constructor <init>(Landroid/view/textclassifier/TextClassificationSessionId;Landroid/view/textclassifier/TextClassificationContext;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mInvocationMethod:I

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/textclassifier/TextClassificationSessionId;

    iput-object v0, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mSessionId:Landroid/view/textclassifier/TextClassificationSessionId;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/textclassifier/TextClassificationContext;

    iput-object v0, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mContext:Landroid/view/textclassifier/TextClassificationContext;

    return-void
.end method

.method private modifyAutoSelectionEventType(Landroid/view/textclassifier/SelectionEvent;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/textclassifier/SelectionEvent;->getEventType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    return-void

    :pswitch_0
    nop

    invoke-virtual {p1}, Landroid/view/textclassifier/SelectionEvent;->getResultId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/view/textclassifier/SelectionSessionLogger;->isPlatformLocalTextClassifierSmartSelection(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/textclassifier/SelectionEvent;->getAbsoluteEnd()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/textclassifier/SelectionEvent;->getAbsoluteStart()I

    move-result v1

    sub-int/2addr v0, v1

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/view/textclassifier/SelectionEvent;->setEventType(I)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Landroid/view/textclassifier/SelectionEvent;->setEventType(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Landroid/view/textclassifier/SelectionEvent;->setEventType(I)V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private updateInvocationMethod(Landroid/view/textclassifier/SelectionEvent;)V
    .locals 1

    iget-object v0, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mContext:Landroid/view/textclassifier/TextClassificationContext;

    invoke-virtual {p1, v0}, Landroid/view/textclassifier/SelectionEvent;->setTextClassificationSessionContext(Landroid/view/textclassifier/TextClassificationContext;)V

    invoke-virtual {p1}, Landroid/view/textclassifier/SelectionEvent;->getInvocationMethod()I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mInvocationMethod:I

    invoke-virtual {p1, v0}, Landroid/view/textclassifier/SelectionEvent;->setInvocationMethod(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/view/textclassifier/SelectionEvent;->getInvocationMethod()I

    move-result v0

    iput v0, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mInvocationMethod:I

    :goto_0
    return-void
.end method


# virtual methods
.method endSession()V
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    return-void

    :goto_1
    iput-object v0, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mStartEvent:Landroid/view/textclassifier/SelectionEvent;

    goto/32 :goto_0

    nop

    :goto_2
    iput-object v0, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mPrevEvent:Landroid/view/textclassifier/SelectionEvent;

    goto/32 :goto_3

    nop

    :goto_3
    iput-object v0, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mSmartEvent:Landroid/view/textclassifier/SelectionEvent;

    goto/32 :goto_1

    nop

    :goto_4
    const/4 v0, 0x0

    goto/32 :goto_2

    nop
.end method

.method sanitizeEvent(Landroid/view/textclassifier/SelectionEvent;)Z
    .locals 7

    goto/32 :goto_27

    nop

    :goto_0
    iget-object v5, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mStartEvent:Landroid/view/textclassifier/SelectionEvent;

    goto/32 :goto_22

    nop

    :goto_1
    invoke-virtual {v1}, Landroid/view/textclassifier/SelectionEvent;->getEventTime()J

    move-result-wide v5

    goto/32 :goto_3b

    nop

    :goto_2
    if-eq v0, v5, :cond_0

    goto/32 :goto_2f

    :cond_0
    goto/32 :goto_3e

    nop

    :goto_3
    invoke-virtual {p1, v0}, Landroid/view/textclassifier/SelectionEvent;->setSessionId(Landroid/view/textclassifier/TextClassificationSessionId;)Landroid/view/textclassifier/SelectionEvent;

    goto/32 :goto_2e

    nop

    :goto_4
    invoke-virtual {p1}, Landroid/view/textclassifier/SelectionEvent;->getAbsoluteStart()I

    move-result v5

    goto/32 :goto_3c

    nop

    :goto_5
    iget-object v1, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mStartEvent:Landroid/view/textclassifier/SelectionEvent;

    goto/32 :goto_1

    nop

    :goto_6
    invoke-virtual {p1}, Landroid/view/textclassifier/SelectionEvent;->getEventType()I

    move-result v0

    goto/32 :goto_50

    nop

    :goto_7
    iget-object v1, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mSmartEvent:Landroid/view/textclassifier/SelectionEvent;

    goto/32 :goto_13

    nop

    :goto_8
    invoke-virtual {v0}, Landroid/view/textclassifier/SelectionEvent;->getSessionId()Landroid/view/textclassifier/TextClassificationSessionId;

    move-result-object v0

    goto/32 :goto_56

    nop

    :goto_9
    move v1, v2

    :goto_a
    goto/32 :goto_4d

    nop

    :goto_b
    invoke-direct {p0, p1}, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->modifyAutoSelectionEventType(Landroid/view/textclassifier/SelectionEvent;)V

    goto/32 :goto_6

    nop

    :goto_c
    invoke-virtual {v0, v1}, Landroid/view/textclassifier/SelectionEvent;->setSmartStart(I)Landroid/view/textclassifier/SelectionEvent;

    move-result-object v0

    goto/32 :goto_46

    nop

    :goto_d
    if-eq v0, v5, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_9

    nop

    :goto_e
    add-int/2addr v1, v2

    goto/32 :goto_1f

    nop

    :goto_f
    if-nez v0, :cond_2

    goto/32 :goto_20

    :cond_2
    goto/32 :goto_1a

    nop

    :goto_10
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    goto/32 :goto_3f

    nop

    :goto_11
    const-string v0, "TextClassificationSession"

    goto/32 :goto_23

    nop

    :goto_12
    add-int/2addr v5, v2

    goto/32 :goto_d

    nop

    :goto_13
    invoke-virtual {v1}, Landroid/view/textclassifier/SelectionEvent;->getAbsoluteStart()I

    move-result v1

    goto/32 :goto_4a

    nop

    :goto_14
    invoke-virtual {v1}, Landroid/view/textclassifier/SelectionEvent;->getAbsoluteEnd()I

    move-result v1

    goto/32 :goto_0

    nop

    :goto_15
    if-eqz v0, :cond_3

    goto/32 :goto_45

    :cond_3
    goto/32 :goto_11

    nop

    :goto_16
    invoke-virtual {p1, v0, v1}, Landroid/view/textclassifier/SelectionEvent;->setDurationSincePreviousEvent(J)Landroid/view/textclassifier/SelectionEvent;

    move-result-object v0

    goto/32 :goto_3d

    nop

    :goto_17
    if-ne v0, v2, :cond_4

    goto/32 :goto_45

    :cond_4
    goto/32 :goto_34

    nop

    :goto_18
    goto/16 :goto_2f

    :sswitch_0
    goto/32 :goto_26

    nop

    :goto_19
    if-nez v0, :cond_5

    goto/32 :goto_33

    :cond_5
    goto/32 :goto_8

    nop

    :goto_1a
    invoke-virtual {v0}, Landroid/view/textclassifier/SelectionEvent;->getEventTime()J

    move-result-wide v0

    goto/32 :goto_39

    nop

    :goto_1b
    invoke-virtual {v5}, Landroid/view/textclassifier/SelectionEvent;->getAbsoluteStart()I

    move-result v5

    goto/32 :goto_31

    nop

    :goto_1c
    invoke-virtual {v5}, Landroid/view/textclassifier/SelectionEvent;->getAbsoluteStart()I

    move-result v5

    goto/32 :goto_48

    nop

    :goto_1d
    invoke-virtual {p1}, Landroid/view/textclassifier/SelectionEvent;->getAbsoluteEnd()I

    move-result v0

    goto/32 :goto_5b

    nop

    :goto_1e
    invoke-virtual {p1, v3, v4}, Landroid/view/textclassifier/SelectionEvent;->setEventTime(J)Landroid/view/textclassifier/SelectionEvent;

    goto/32 :goto_4e

    nop

    :goto_1f
    invoke-virtual {v0, v1}, Landroid/view/textclassifier/SelectionEvent;->setEventIndex(I)Landroid/view/textclassifier/SelectionEvent;

    :goto_20
    goto/32 :goto_42

    nop

    :goto_21
    iget-object v0, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mSmartEvent:Landroid/view/textclassifier/SelectionEvent;

    goto/32 :goto_59

    nop

    :goto_22
    invoke-virtual {v5}, Landroid/view/textclassifier/SelectionEvent;->getAbsoluteStart()I

    move-result v5

    goto/32 :goto_41

    nop

    :goto_23
    const-string v2, "Selection session not yet started. Ignoring event"

    goto/32 :goto_53

    nop

    :goto_24
    invoke-virtual {p1, v0}, Landroid/view/textclassifier/SelectionEvent;->setEntityType(Ljava/lang/String;)V

    goto/32 :goto_18

    nop

    :goto_25
    const/4 v2, 0x1

    goto/32 :goto_17

    nop

    :goto_26
    iput-object p1, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mSmartEvent:Landroid/view/textclassifier/SelectionEvent;

    goto/32 :goto_5a

    nop

    :goto_27
    invoke-direct {p0, p1}, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->updateInvocationMethod(Landroid/view/textclassifier/SelectionEvent;)V

    goto/32 :goto_b

    nop

    :goto_28
    invoke-virtual {v0, v5, v6}, Landroid/view/textclassifier/SelectionEvent;->setDurationSinceSessionStart(J)Landroid/view/textclassifier/SelectionEvent;

    move-result-object v0

    goto/32 :goto_2d

    nop

    :goto_29
    if-nez v0, :cond_6

    goto/32 :goto_2f

    :cond_6
    goto/32 :goto_55

    nop

    :goto_2a
    if-nez v0, :cond_7

    goto/32 :goto_2f

    :cond_7
    goto/32 :goto_30

    nop

    :goto_2b
    goto :goto_2f

    :sswitch_1
    goto/32 :goto_5c

    nop

    :goto_2c
    invoke-virtual {v0}, Landroid/view/textclassifier/SelectionEvent;->getResultId()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_57

    nop

    :goto_2d
    invoke-virtual {p1}, Landroid/view/textclassifier/SelectionEvent;->getAbsoluteStart()I

    move-result v1

    goto/32 :goto_36

    nop

    :goto_2e
    iput-object p1, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mStartEvent:Landroid/view/textclassifier/SelectionEvent;

    nop

    :goto_2f
    goto/32 :goto_1e

    nop

    :goto_30
    invoke-virtual {v0}, Landroid/view/textclassifier/SelectionEvent;->getAbsoluteStart()I

    move-result v0

    goto/32 :goto_4

    nop

    :goto_31
    sub-int/2addr v1, v5

    goto/32 :goto_4c

    nop

    :goto_32
    invoke-virtual {v0, v1}, Landroid/view/textclassifier/SelectionEvent;->setEnd(I)Landroid/view/textclassifier/SelectionEvent;

    :goto_33
    goto/32 :goto_21

    nop

    :goto_34
    iget-object v0, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mStartEvent:Landroid/view/textclassifier/SelectionEvent;

    goto/32 :goto_15

    nop

    :goto_35
    invoke-virtual {v0}, Landroid/view/textclassifier/SelectionEvent;->getAbsoluteEnd()I

    move-result v0

    goto/32 :goto_47

    nop

    :goto_36
    iget-object v5, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mStartEvent:Landroid/view/textclassifier/SelectionEvent;

    goto/32 :goto_1b

    nop

    :goto_37
    invoke-virtual {v0, v1}, Landroid/view/textclassifier/SelectionEvent;->setSmartEnd(I)Landroid/view/textclassifier/SelectionEvent;

    :goto_38
    goto/32 :goto_58

    nop

    :goto_39
    sub-long v0, v3, v0

    goto/32 :goto_16

    nop

    :goto_3a
    sub-int/2addr v1, v5

    goto/32 :goto_32

    nop

    :goto_3b
    sub-long v5, v3, v5

    goto/32 :goto_28

    nop

    :goto_3c
    if-eq v0, v5, :cond_8

    goto/32 :goto_2f

    :cond_8
    goto/32 :goto_54

    nop

    :goto_3d
    iget-object v1, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mPrevEvent:Landroid/view/textclassifier/SelectionEvent;

    goto/32 :goto_43

    nop

    :goto_3e
    return v1

    :sswitch_2
    nop

    goto/32 :goto_1d

    nop

    :goto_3f
    invoke-virtual {p1}, Landroid/view/textclassifier/SelectionEvent;->getEventType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto/32 :goto_2b

    nop

    :goto_40
    return v2

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x3 -> :sswitch_0
        0x4 -> :sswitch_0
        0x5 -> :sswitch_0
        0x64 -> :sswitch_1
        0x6b -> :sswitch_1
    .end sparse-switch

    :goto_41
    sub-int/2addr v1, v5

    goto/32 :goto_37

    nop

    :goto_42
    iput-object p1, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mPrevEvent:Landroid/view/textclassifier/SelectionEvent;

    goto/32 :goto_40

    nop

    :goto_43
    invoke-virtual {v1}, Landroid/view/textclassifier/SelectionEvent;->getEventIndex()I

    move-result v1

    goto/32 :goto_e

    nop

    :goto_44
    return v1

    :goto_45
    goto/32 :goto_10

    nop

    :goto_46
    iget-object v1, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mSmartEvent:Landroid/view/textclassifier/SelectionEvent;

    goto/32 :goto_14

    nop

    :goto_47
    invoke-virtual {p1}, Landroid/view/textclassifier/SelectionEvent;->getAbsoluteEnd()I

    move-result v5

    goto/32 :goto_2

    nop

    :goto_48
    sub-int/2addr v1, v5

    goto/32 :goto_c

    nop

    :goto_49
    invoke-virtual {v5}, Landroid/view/textclassifier/SelectionEvent;->getAbsoluteStart()I

    move-result v5

    goto/32 :goto_3a

    nop

    :goto_4a
    iget-object v5, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mStartEvent:Landroid/view/textclassifier/SelectionEvent;

    goto/32 :goto_1c

    nop

    :goto_4b
    iget-object v0, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mPrevEvent:Landroid/view/textclassifier/SelectionEvent;

    goto/32 :goto_2a

    nop

    :goto_4c
    invoke-virtual {v0, v1}, Landroid/view/textclassifier/SelectionEvent;->setStart(I)Landroid/view/textclassifier/SelectionEvent;

    move-result-object v0

    goto/32 :goto_52

    nop

    :goto_4d
    invoke-static {v1}, Lcom/android/internal/util/Preconditions;->checkArgument(Z)V

    goto/32 :goto_51

    nop

    :goto_4e
    iget-object v0, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mStartEvent:Landroid/view/textclassifier/SelectionEvent;

    goto/32 :goto_19

    nop

    :goto_4f
    iget-object v5, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mStartEvent:Landroid/view/textclassifier/SelectionEvent;

    goto/32 :goto_49

    nop

    :goto_50
    const/4 v1, 0x0

    goto/32 :goto_25

    nop

    :goto_51
    iget-object v0, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mSessionId:Landroid/view/textclassifier/TextClassificationSessionId;

    goto/32 :goto_3

    nop

    :goto_52
    invoke-virtual {p1}, Landroid/view/textclassifier/SelectionEvent;->getAbsoluteEnd()I

    move-result v1

    goto/32 :goto_4f

    nop

    :goto_53
    invoke-static {v0, v2}, Landroid/view/textclassifier/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_44

    nop

    :goto_54
    iget-object v0, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mPrevEvent:Landroid/view/textclassifier/SelectionEvent;

    goto/32 :goto_35

    nop

    :goto_55
    invoke-virtual {v0}, Landroid/view/textclassifier/SelectionEvent;->getEntityType()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_24

    nop

    :goto_56
    invoke-virtual {p1, v0}, Landroid/view/textclassifier/SelectionEvent;->setSessionId(Landroid/view/textclassifier/TextClassificationSessionId;)Landroid/view/textclassifier/SelectionEvent;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_57
    invoke-virtual {p1, v0}, Landroid/view/textclassifier/SelectionEvent;->setResultId(Ljava/lang/String;)Landroid/view/textclassifier/SelectionEvent;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_58
    iget-object v0, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mPrevEvent:Landroid/view/textclassifier/SelectionEvent;

    goto/32 :goto_f

    nop

    :goto_59
    if-nez v0, :cond_9

    goto/32 :goto_38

    :cond_9
    goto/32 :goto_2c

    nop

    :goto_5a
    goto/16 :goto_2f

    :sswitch_3
    goto/32 :goto_4b

    nop

    :goto_5b
    invoke-virtual {p1}, Landroid/view/textclassifier/SelectionEvent;->getAbsoluteStart()I

    move-result v5

    goto/32 :goto_12

    nop

    :goto_5c
    iget-object v0, p0, Landroid/view/textclassifier/TextClassificationSession$SelectionEventHelper;->mPrevEvent:Landroid/view/textclassifier/SelectionEvent;

    goto/32 :goto_29

    nop
.end method
