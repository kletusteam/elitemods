.class final Landroid/view/RemoteAccessibilityController$RemoteAccessibilityEmbeddedConnection;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/RemoteAccessibilityController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "RemoteAccessibilityEmbeddedConnection"
.end annotation


# instance fields
.field private final mConnection:Landroid/view/accessibility/IAccessibilityEmbeddedConnection;

.field private final mLeashToken:Landroid/os/IBinder;

.field final synthetic this$0:Landroid/view/RemoteAccessibilityController;


# direct methods
.method static bridge synthetic -$$Nest$fgetmConnection(Landroid/view/RemoteAccessibilityController$RemoteAccessibilityEmbeddedConnection;)Landroid/view/accessibility/IAccessibilityEmbeddedConnection;
    .locals 0

    iget-object p0, p0, Landroid/view/RemoteAccessibilityController$RemoteAccessibilityEmbeddedConnection;->mConnection:Landroid/view/accessibility/IAccessibilityEmbeddedConnection;

    return-object p0
.end method

.method constructor <init>(Landroid/view/RemoteAccessibilityController;Landroid/view/accessibility/IAccessibilityEmbeddedConnection;Landroid/os/IBinder;)V
    .locals 0

    iput-object p1, p0, Landroid/view/RemoteAccessibilityController$RemoteAccessibilityEmbeddedConnection;->this$0:Landroid/view/RemoteAccessibilityController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Landroid/view/RemoteAccessibilityController$RemoteAccessibilityEmbeddedConnection;->mConnection:Landroid/view/accessibility/IAccessibilityEmbeddedConnection;

    iput-object p3, p0, Landroid/view/RemoteAccessibilityController$RemoteAccessibilityEmbeddedConnection;->mLeashToken:Landroid/os/IBinder;

    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 2

    invoke-virtual {p0}, Landroid/view/RemoteAccessibilityController$RemoteAccessibilityEmbeddedConnection;->unlinkToDeath()V

    iget-object v0, p0, Landroid/view/RemoteAccessibilityController$RemoteAccessibilityEmbeddedConnection;->this$0:Landroid/view/RemoteAccessibilityController;

    new-instance v1, Landroid/view/RemoteAccessibilityController$RemoteAccessibilityEmbeddedConnection$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Landroid/view/RemoteAccessibilityController$RemoteAccessibilityEmbeddedConnection$$ExternalSyntheticLambda0;-><init>(Landroid/view/RemoteAccessibilityController$RemoteAccessibilityEmbeddedConnection;)V

    invoke-static {v0, v1}, Landroid/view/RemoteAccessibilityController;->-$$Nest$mrunOnUiThread(Landroid/view/RemoteAccessibilityController;Ljava/lang/Runnable;)V

    return-void
.end method

.method getConnection()Landroid/view/accessibility/IAccessibilityEmbeddedConnection;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Landroid/view/RemoteAccessibilityController$RemoteAccessibilityEmbeddedConnection;->mConnection:Landroid/view/accessibility/IAccessibilityEmbeddedConnection;

    goto/32 :goto_0

    nop
.end method

.method getLeashToken()Landroid/os/IBinder;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Landroid/view/RemoteAccessibilityController$RemoteAccessibilityEmbeddedConnection;->mLeashToken:Landroid/os/IBinder;

    goto/32 :goto_0

    nop
.end method

.method synthetic lambda$binderDied$0$android-view-RemoteAccessibilityController$RemoteAccessibilityEmbeddedConnection()V
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    const/4 v1, 0x0

    goto/32 :goto_3

    nop

    :goto_1
    iget-object v0, p0, Landroid/view/RemoteAccessibilityController$RemoteAccessibilityEmbeddedConnection;->this$0:Landroid/view/RemoteAccessibilityController;

    goto/32 :goto_0

    nop

    :goto_2
    if-eq v0, p0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_1

    nop

    :goto_3
    invoke-static {v0, v1}, Landroid/view/RemoteAccessibilityController;->-$$Nest$fputmConnectionWrapper(Landroid/view/RemoteAccessibilityController;Landroid/view/RemoteAccessibilityController$RemoteAccessibilityEmbeddedConnection;)V

    :goto_4
    goto/32 :goto_7

    nop

    :goto_5
    iget-object v0, p0, Landroid/view/RemoteAccessibilityController$RemoteAccessibilityEmbeddedConnection;->this$0:Landroid/view/RemoteAccessibilityController;

    goto/32 :goto_6

    nop

    :goto_6
    invoke-static {v0}, Landroid/view/RemoteAccessibilityController;->-$$Nest$fgetmConnectionWrapper(Landroid/view/RemoteAccessibilityController;)Landroid/view/RemoteAccessibilityController$RemoteAccessibilityEmbeddedConnection;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_7
    return-void
.end method

.method linkToDeath()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    const/4 v1, 0x0

    goto/32 :goto_3

    nop

    :goto_1
    iget-object v0, p0, Landroid/view/RemoteAccessibilityController$RemoteAccessibilityEmbeddedConnection;->mConnection:Landroid/view/accessibility/IAccessibilityEmbeddedConnection;

    goto/32 :goto_4

    nop

    :goto_2
    return-void

    :goto_3
    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    goto/32 :goto_2

    nop

    :goto_4
    invoke-interface {v0}, Landroid/view/accessibility/IAccessibilityEmbeddedConnection;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    goto/32 :goto_0

    nop
.end method

.method unlinkToDeath()V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/view/RemoteAccessibilityController$RemoteAccessibilityEmbeddedConnection;->mConnection:Landroid/view/accessibility/IAccessibilityEmbeddedConnection;

    goto/32 :goto_4

    nop

    :goto_1
    const/4 v1, 0x0

    goto/32 :goto_3

    nop

    :goto_2
    return-void

    :goto_3
    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    goto/32 :goto_2

    nop

    :goto_4
    invoke-interface {v0}, Landroid/view/accessibility/IAccessibilityEmbeddedConnection;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    goto/32 :goto_1

    nop
.end method
