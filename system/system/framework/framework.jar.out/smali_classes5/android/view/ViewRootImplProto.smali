.class public final Landroid/view/ViewRootImplProto;
.super Ljava/lang/Object;


# static fields
.field public static final ADDED:J = 0x10800000009L

.field public static final APP_VISIBLE:J = 0x10800000003L

.field public static final CUR_SCROLL_Y:J = 0x1050000000fL

.field public static final DISPLAY_ID:J = 0x10500000002L

.field public static final HEIGHT:J = 0x10500000005L

.field public static final IS_ANIMATING:J = 0x10800000006L

.field public static final IS_DRAWING:J = 0x10800000008L

.field public static final LAST_WINDOW_INSETS:J = 0x1090000000cL

.field public static final PENDING_DISPLAY_CUTOUT:J = 0x10b0000000bL

.field public static final REMOVED:J = 0x10800000010L

.field public static final SCROLL_Y:J = 0x1050000000eL

.field public static final SOFT_INPUT_MODE:J = 0x1090000000dL

.field public static final VIEW:J = 0x10900000001L

.field public static final VISIBLE_RECT:J = 0x10b00000007L

.field public static final WIDTH:J = 0x10500000004L

.field public static final WINDOW_ATTRIBUTES:J = 0x10b00000011L

.field public static final WIN_FRAME:J = 0x10b0000000aL


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
