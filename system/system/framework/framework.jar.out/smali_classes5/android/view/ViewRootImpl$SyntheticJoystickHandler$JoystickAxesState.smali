.class final Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/ViewRootImpl$SyntheticJoystickHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "JoystickAxesState"
.end annotation


# static fields
.field private static final STATE_DOWN_OR_RIGHT:I = 0x1

.field private static final STATE_NEUTRAL:I = 0x0

.field private static final STATE_UP_OR_LEFT:I = -0x1


# instance fields
.field final mAxisStatesHat:[I

.field final mAxisStatesStick:[I

.field final synthetic this$1:Landroid/view/ViewRootImpl$SyntheticJoystickHandler;


# direct methods
.method constructor <init>(Landroid/view/ViewRootImpl$SyntheticJoystickHandler;)V
    .locals 2

    iput-object p1, p0, Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;->this$1:Landroid/view/ViewRootImpl$SyntheticJoystickHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    new-array v1, v0, [I

    fill-array-data v1, :array_0

    iput-object v1, p0, Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;->mAxisStatesHat:[I

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;->mAxisStatesStick:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method private isXAxis(I)Z
    .locals 1

    if-eqz p1, :cond_1

    const/16 v0, 0xf

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private isYAxis(I)Z
    .locals 2

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/16 v1, 0x10

    if-ne p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0
.end method

.method private joystickAxisAndStateToKeycode(II)I
    .locals 3

    invoke-direct {p0, p1}, Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;->isXAxis(I)Z

    move-result v0

    const/4 v1, -0x1

    if-eqz v0, :cond_0

    if-ne p2, v1, :cond_0

    const/16 v0, 0x15

    return v0

    :cond_0
    invoke-direct {p0, p1}, Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;->isXAxis(I)Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    if-ne p2, v2, :cond_1

    const/16 v0, 0x16

    return v0

    :cond_1
    invoke-direct {p0, p1}, Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;->isYAxis(I)Z

    move-result v0

    if-eqz v0, :cond_2

    if-ne p2, v1, :cond_2

    const/16 v0, 0x13

    return v0

    :cond_2
    invoke-direct {p0, p1}, Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;->isYAxis(I)Z

    move-result v0

    if-eqz v0, :cond_3

    if-ne p2, v2, :cond_3

    const/16 v0, 0x14

    return v0

    :cond_3
    iget-object v0, p0, Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;->this$1:Landroid/view/ViewRootImpl$SyntheticJoystickHandler;

    iget-object v0, v0, Landroid/view/ViewRootImpl$SyntheticJoystickHandler;->this$0:Landroid/view/ViewRootImpl;

    invoke-static {v0}, Landroid/view/ViewRootImpl;->-$$Nest$fgetmTag(Landroid/view/ViewRootImpl;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown axis "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " or direction "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method private joystickAxisValueToState(F)I
    .locals 1

    const/high16 v0, 0x3f000000    # 0.5f

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/high16 v0, -0x41000000    # -0.5f

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_1

    const/4 v0, -0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method resetState()V
    .locals 3

    goto/32 :goto_6

    nop

    :goto_0
    const/4 v1, 0x0

    goto/32 :goto_4

    nop

    :goto_1
    iget-object v0, p0, Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;->mAxisStatesStick:[I

    goto/32 :goto_8

    nop

    :goto_2
    aput v1, v0, v2

    goto/32 :goto_5

    nop

    :goto_3
    const/4 v2, 0x1

    goto/32 :goto_7

    nop

    :goto_4
    aput v1, v0, v1

    goto/32 :goto_3

    nop

    :goto_5
    return-void

    :goto_6
    iget-object v0, p0, Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;->mAxisStatesHat:[I

    goto/32 :goto_0

    nop

    :goto_7
    aput v1, v0, v2

    goto/32 :goto_1

    nop

    :goto_8
    aput v1, v0, v1

    goto/32 :goto_2

    nop
.end method

.method updateStateForAxis(Landroid/view/MotionEvent;JIF)V
    .locals 28

    goto/32 :goto_5f

    nop

    :goto_0
    move-object/from16 v24, v13

    goto/32 :goto_83

    nop

    :goto_1
    if-eq v7, v14, :cond_0

    goto/32 :goto_3c

    :cond_0
    :goto_2
    goto/32 :goto_9b

    nop

    :goto_3
    move/from16 v25, v15

    goto/32 :goto_92

    nop

    :goto_4
    iget-object v6, v0, Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;->this$1:Landroid/view/ViewRootImpl$SyntheticJoystickHandler;

    goto/32 :goto_3b

    nop

    :goto_5
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_7f

    nop

    :goto_6
    aput v5, v6, v2

    :goto_7
    goto/32 :goto_15

    nop

    :goto_8
    if-nez v1, :cond_1

    goto/32 :goto_68

    :cond_1
    goto/32 :goto_9d

    nop

    :goto_9
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v21

    goto/32 :goto_14

    nop

    :goto_a
    const/4 v3, 0x1

    goto/32 :goto_4b

    nop

    :goto_b
    move-object/from16 v23, v14

    goto/32 :goto_49

    nop

    :goto_c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_31

    nop

    :goto_d
    invoke-static {v2}, Landroid/view/ViewRootImpl;->-$$Nest$fgetmTag(Landroid/view/ViewRootImpl;)Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_46

    nop

    :goto_e
    goto/16 :goto_68

    :goto_f
    goto/32 :goto_60

    nop

    :goto_10
    goto/16 :goto_2c

    :goto_11
    goto/32 :goto_2b

    nop

    :goto_12
    invoke-virtual {v8, v6}, Landroid/view/ViewRootImpl;->enqueueInputEvent(Landroid/view/InputEvent;)V

    goto/32 :goto_79

    nop

    :goto_13
    move/from16 v13, v16

    goto/32 :goto_b

    nop

    :goto_14
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDeviceId()I

    move-result v15

    goto/32 :goto_9f

    nop

    :goto_15
    return-void

    :goto_16
    goto/32 :goto_36

    nop

    :goto_17
    aget v7, v7, v2

    goto/32 :goto_67

    nop

    :goto_18
    move/from16 v14, v23

    goto/32 :goto_34

    nop

    :goto_19
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_5

    nop

    :goto_1a
    invoke-direct/range {v8 .. v20}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    goto/32 :goto_1f

    nop

    :goto_1b
    invoke-direct/range {v8 .. v20}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    goto/32 :goto_4d

    nop

    :goto_1c
    move/from16 v20, v22

    goto/32 :goto_1b

    nop

    :goto_1d
    iget-object v8, v0, Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;->this$1:Landroid/view/ViewRootImpl$SyntheticJoystickHandler;

    goto/32 :goto_66

    nop

    :goto_1e
    move-object/from16 v8, v23

    goto/32 :goto_58

    nop

    :goto_1f
    move-object/from16 v15, v23

    goto/32 :goto_1d

    nop

    :goto_20
    invoke-virtual {v14, v8}, Landroid/os/Message;->setAsynchronous(Z)V

    goto/32 :goto_84

    nop

    :goto_21
    const/4 v6, 0x1

    goto/32 :goto_72

    nop

    :goto_22
    invoke-direct {v0, v1, v5}, Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;->joystickAxisAndStateToKeycode(II)I

    move-result v6

    goto/32 :goto_9e

    nop

    :goto_23
    move-wide/from16 v11, p2

    goto/32 :goto_9a

    nop

    :goto_24
    iget-object v8, v0, Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;->this$1:Landroid/view/ViewRootImpl$SyntheticJoystickHandler;

    goto/32 :goto_7b

    nop

    :goto_25
    goto/16 :goto_7

    :goto_26
    goto/32 :goto_3e

    nop

    :goto_27
    move/from16 v20, v22

    goto/32 :goto_1a

    nop

    :goto_28
    const/16 v16, 0x0

    goto/32 :goto_7a

    nop

    :goto_29
    move-wide/from16 v11, p2

    goto/32 :goto_8b

    nop

    :goto_2a
    move/from16 v4, p5

    goto/32 :goto_30

    nop

    :goto_2b
    move v8, v15

    :goto_2c
    goto/32 :goto_a1

    nop

    :goto_2d
    const/4 v8, 0x1

    goto/32 :goto_20

    nop

    :goto_2e
    const/4 v3, 0x2

    :goto_2f
    goto/32 :goto_2a

    nop

    :goto_30
    invoke-direct {v0, v4}, Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;->joystickAxisValueToState(F)I

    move-result v5

    goto/32 :goto_50

    nop

    :goto_31
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_98

    nop

    :goto_32
    invoke-static {}, Landroid/view/ViewConfiguration;->getKeyRepeatTimeout()I

    move-result v9

    goto/32 :goto_8a

    nop

    :goto_33
    new-instance v11, Landroid/view/KeyEvent;

    goto/32 :goto_8c

    nop

    :goto_34
    move/from16 v25, v15

    goto/32 :goto_61

    nop

    :goto_35
    move/from16 v15, v25

    goto/32 :goto_5b

    nop

    :goto_36
    move/from16 v4, p5

    goto/32 :goto_41

    nop

    :goto_37
    const/16 v19, 0x400

    goto/32 :goto_38

    nop

    :goto_38
    move-object v8, v11

    goto/32 :goto_6c

    nop

    :goto_39
    const/4 v6, -0x1

    goto/32 :goto_56

    nop

    :goto_3a
    const/4 v2, 0x1

    goto/32 :goto_2e

    nop

    :goto_3b
    invoke-virtual {v6, v3}, Landroid/view/ViewRootImpl$SyntheticJoystickHandler;->removeMessages(I)V

    :goto_3c
    goto/32 :goto_21

    nop

    :goto_3d
    const/16 v19, 0x420

    goto/32 :goto_77

    nop

    :goto_3e
    iget-object v6, v0, Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;->mAxisStatesStick:[I

    goto/32 :goto_6

    nop

    :goto_3f
    iget-object v7, v0, Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;->mAxisStatesStick:[I

    goto/32 :goto_47

    nop

    :goto_40
    move/from16 v17, v25

    goto/32 :goto_55

    nop

    :goto_41
    iget-object v2, v0, Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;->this$1:Landroid/view/ViewRootImpl$SyntheticJoystickHandler;

    goto/32 :goto_57

    nop

    :goto_42
    const/4 v2, 0x0

    goto/32 :goto_a

    nop

    :goto_43
    invoke-virtual {v8, v14, v9, v10}, Landroid/view/ViewRootImpl$SyntheticJoystickHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/32 :goto_9c

    nop

    :goto_44
    invoke-static {v6}, Landroid/view/ViewRootImpl$SyntheticJoystickHandler;->-$$Nest$fgetmDeviceKeyEvents(Landroid/view/ViewRootImpl$SyntheticJoystickHandler;)Landroid/util/SparseArray;

    move-result-object v6

    goto/32 :goto_81

    nop

    :goto_45
    if-eq v7, v5, :cond_2

    goto/32 :goto_53

    :cond_2
    goto/32 :goto_52

    nop

    :goto_46
    new-instance v3, Ljava/lang/StringBuilder;

    goto/32 :goto_a2

    nop

    :goto_47
    aget v7, v7, v2

    :goto_48
    goto/32 :goto_45

    nop

    :goto_49
    move v14, v6

    goto/32 :goto_78

    nop

    :goto_4a
    const/16 v16, 0x1

    goto/32 :goto_6b

    nop

    :goto_4b
    goto/16 :goto_2f

    :goto_4c
    goto/32 :goto_64

    nop

    :goto_4d
    move-object/from16 v8, v24

    goto/32 :goto_12

    nop

    :goto_4e
    const/4 v13, 0x0

    goto/32 :goto_28

    nop

    :goto_4f
    if-nez v23, :cond_3

    goto/32 :goto_5c

    :cond_3
    goto/32 :goto_24

    nop

    :goto_50
    const/4 v6, 0x1

    goto/32 :goto_8

    nop

    :goto_51
    const/4 v14, -0x1

    goto/32 :goto_88

    nop

    :goto_52
    return-void

    :goto_53
    goto/32 :goto_9

    nop

    :goto_54
    invoke-virtual {v8, v3, v15}, Landroid/view/ViewRootImpl$SyntheticJoystickHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v14

    goto/32 :goto_2d

    nop

    :goto_55
    invoke-direct/range {v8 .. v20}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    goto/32 :goto_62

    nop

    :goto_56
    if-eq v5, v6, :cond_4

    goto/32 :goto_8f

    :cond_4
    goto/32 :goto_8e

    nop

    :goto_57
    iget-object v2, v2, Landroid/view/ViewRootImpl$SyntheticJoystickHandler;->this$0:Landroid/view/ViewRootImpl;

    goto/32 :goto_d

    nop

    :goto_58
    move-wide/from16 v9, p2

    goto/32 :goto_23

    nop

    :goto_59
    move/from16 v1, p4

    goto/32 :goto_7e

    nop

    :goto_5a
    invoke-static {v8}, Landroid/view/ViewRootImpl$SyntheticJoystickHandler;->-$$Nest$fgetmDeviceKeyEvents(Landroid/view/ViewRootImpl$SyntheticJoystickHandler;)Landroid/util/SparseArray;

    move-result-object v13

    goto/32 :goto_33

    nop

    :goto_5b
    invoke-virtual {v6, v15, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :goto_5c
    goto/32 :goto_4

    nop

    :goto_5d
    invoke-virtual {v8, v15}, Landroid/view/ViewRootImpl;->enqueueInputEvent(Landroid/view/InputEvent;)V

    goto/32 :goto_91

    nop

    :goto_5e
    move/from16 v17, v25

    goto/32 :goto_1c

    nop

    :goto_5f
    move-object/from16 v0, p0

    goto/32 :goto_59

    nop

    :goto_60
    iget-object v7, v0, Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;->mAxisStatesHat:[I

    goto/32 :goto_17

    nop

    :goto_61
    move/from16 v15, v17

    goto/32 :goto_6d

    nop

    :goto_62
    move/from16 v8, v25

    goto/32 :goto_80

    nop

    :goto_63
    aput v5, v6, v2

    goto/32 :goto_25

    nop

    :goto_64
    invoke-direct {v0, v1}, Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;->isYAxis(I)Z

    move-result v2

    goto/32 :goto_6f

    nop

    :goto_65
    const/4 v6, 0x1

    goto/32 :goto_96

    nop

    :goto_66
    iget-object v8, v8, Landroid/view/ViewRootImpl$SyntheticJoystickHandler;->this$0:Landroid/view/ViewRootImpl;

    goto/32 :goto_5d

    nop

    :goto_67
    goto/16 :goto_48

    :goto_68
    goto/32 :goto_3f

    nop

    :goto_69
    move-object/from16 v9, v27

    goto/32 :goto_90

    nop

    :goto_6a
    move/from16 v16, v21

    goto/32 :goto_40

    nop

    :goto_6b
    const/16 v17, 0x0

    goto/32 :goto_6e

    nop

    :goto_6c
    move-wide/from16 v9, p2

    goto/32 :goto_97

    nop

    :goto_6d
    move/from16 v16, v21

    goto/32 :goto_5e

    nop

    :goto_6e
    const/16 v18, 0x0

    goto/32 :goto_37

    nop

    :goto_6f
    if-nez v2, :cond_5

    goto/32 :goto_16

    :cond_5
    goto/32 :goto_3a

    nop

    :goto_70
    const/16 v17, 0x0

    goto/32 :goto_3d

    nop

    :goto_71
    new-instance v11, Landroid/view/KeyEvent;

    goto/32 :goto_4a

    nop

    :goto_72
    if-ne v5, v6, :cond_6

    goto/32 :goto_75

    :cond_6
    goto/32 :goto_39

    nop

    :goto_73
    move-object/from16 v26, v11

    goto/32 :goto_29

    nop

    :goto_74
    goto/16 :goto_2c

    :goto_75
    goto/32 :goto_22

    nop

    :goto_76
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_c

    nop

    :goto_77
    move-object v8, v11

    goto/32 :goto_95

    nop

    :goto_78
    move-object/from16 v24, v15

    goto/32 :goto_85

    nop

    :goto_79
    iget-object v6, v0, Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;->this$1:Landroid/view/ViewRootImpl$SyntheticJoystickHandler;

    goto/32 :goto_44

    nop

    :goto_7a
    const/16 v18, 0x0

    goto/32 :goto_86

    nop

    :goto_7b
    iget-object v13, v8, Landroid/view/ViewRootImpl$SyntheticJoystickHandler;->this$0:Landroid/view/ViewRootImpl;

    goto/32 :goto_71

    nop

    :goto_7c
    const-string v5, "Unexpected axis "

    goto/32 :goto_19

    nop

    :goto_7d
    move v8, v15

    goto/32 :goto_74

    nop

    :goto_7e
    invoke-direct {v0, v1}, Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;->isXAxis(I)Z

    move-result v2

    goto/32 :goto_82

    nop

    :goto_7f
    const-string v5, " in updateStateForAxis!"

    goto/32 :goto_76

    nop

    :goto_80
    move-object/from16 v10, v26

    goto/32 :goto_69

    nop

    :goto_81
    const/4 v8, 0x0

    goto/32 :goto_35

    nop

    :goto_82
    if-nez v2, :cond_7

    goto/32 :goto_4c

    :cond_7
    goto/32 :goto_42

    nop

    :goto_83
    move/from16 v13, v16

    goto/32 :goto_18

    nop

    :goto_84
    iget-object v8, v0, Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;->this$1:Landroid/view/ViewRootImpl$SyntheticJoystickHandler;

    goto/32 :goto_32

    nop

    :goto_85
    move/from16 v15, v17

    goto/32 :goto_6a

    nop

    :goto_86
    const/16 v19, 0x400

    goto/32 :goto_1e

    nop

    :goto_87
    move-wide/from16 v11, p2

    goto/32 :goto_0

    nop

    :goto_88
    if-ne v7, v6, :cond_8

    goto/32 :goto_2

    :cond_8
    goto/32 :goto_1

    nop

    :goto_89
    iget-object v6, v0, Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;->mAxisStatesHat:[I

    goto/32 :goto_63

    nop

    :goto_8a
    int-to-long v9, v9

    goto/32 :goto_43

    nop

    :goto_8b
    move-object/from16 v27, v13

    goto/32 :goto_13

    nop

    :goto_8c
    const/16 v16, 0x1

    goto/32 :goto_70

    nop

    :goto_8d
    move/from16 v17, v25

    goto/32 :goto_27

    nop

    :goto_8e
    goto/16 :goto_75

    :goto_8f
    goto/32 :goto_7d

    nop

    :goto_90
    invoke-virtual {v9, v8, v10}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto/32 :goto_10

    nop

    :goto_91
    iget-object v8, v0, Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;->this$1:Landroid/view/ViewRootImpl$SyntheticJoystickHandler;

    goto/32 :goto_54

    nop

    :goto_92
    move/from16 v15, v16

    goto/32 :goto_a0

    nop

    :goto_93
    goto/16 :goto_26

    :goto_94
    goto/32 :goto_89

    nop

    :goto_95
    move-wide/from16 v9, p2

    goto/32 :goto_73

    nop

    :goto_96
    if-eq v1, v6, :cond_9

    goto/32 :goto_94

    :cond_9
    goto/32 :goto_93

    nop

    :goto_97
    move-object v6, v11

    goto/32 :goto_87

    nop

    :goto_98
    return-void

    :goto_99
    new-instance v23, Landroid/view/KeyEvent;

    goto/32 :goto_4e

    nop

    :goto_9a
    move v14, v6

    goto/32 :goto_3

    nop

    :goto_9b
    invoke-direct {v0, v1, v7}, Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;->joystickAxisAndStateToKeycode(II)I

    move-result v23

    goto/32 :goto_4f

    nop

    :goto_9c
    iget-object v8, v0, Landroid/view/ViewRootImpl$SyntheticJoystickHandler$JoystickAxesState;->this$1:Landroid/view/ViewRootImpl$SyntheticJoystickHandler;

    goto/32 :goto_5a

    nop

    :goto_9d
    if-eq v1, v6, :cond_a

    goto/32 :goto_f

    :cond_a
    goto/32 :goto_e

    nop

    :goto_9e
    if-nez v6, :cond_b

    goto/32 :goto_11

    :cond_b
    goto/32 :goto_99

    nop

    :goto_9f
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v22

    goto/32 :goto_51

    nop

    :goto_a0
    move/from16 v16, v21

    goto/32 :goto_8d

    nop

    :goto_a1
    if-nez v1, :cond_c

    goto/32 :goto_26

    :cond_c
    goto/32 :goto_65

    nop

    :goto_a2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_7c

    nop
.end method
