.class public final Landroid/view/ImeFocusController;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/ImeFocusController$InputMethodManagerDelegate;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "ImeFocusController"


# instance fields
.field private mDelegate:Landroid/view/ImeFocusController$InputMethodManagerDelegate;

.field private mHasImeFocus:Z

.field private mNextServedView:Landroid/view/View;

.field private mServedView:Landroid/view/View;

.field private final mViewRootImpl:Landroid/view/ViewRootImpl;


# direct methods
.method constructor <init>(Landroid/view/ViewRootImpl;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/view/ImeFocusController;->mHasImeFocus:Z

    iput-object p1, p0, Landroid/view/ImeFocusController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    return-void
.end method

.method private getImmDelegate()Landroid/view/ImeFocusController$InputMethodManagerDelegate;
    .locals 2

    iget-object v0, p0, Landroid/view/ImeFocusController;->mDelegate:Landroid/view/ImeFocusController$InputMethodManagerDelegate;

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/view/ImeFocusController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    iget-object v0, v0, Landroid/view/ViewRootImpl;->mContext:Landroid/content/Context;

    const-class v1, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->getDelegate()Landroid/view/inputmethod/InputMethodManager$DelegateImpl;

    move-result-object v0

    iput-object v0, p0, Landroid/view/ImeFocusController;->mDelegate:Landroid/view/ImeFocusController$InputMethodManagerDelegate;

    :cond_0
    iget-object v0, p0, Landroid/view/ImeFocusController;->mDelegate:Landroid/view/ImeFocusController$InputMethodManagerDelegate;

    return-object v0
.end method

.method private static isInLocalFocusMode(Landroid/view/WindowManager$LayoutParams;)Z
    .locals 2

    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v1, 0x10000000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public checkFocus(ZZ)Z
    .locals 7

    invoke-direct {p0}, Landroid/view/ImeFocusController;->getImmDelegate()Landroid/view/ImeFocusController$InputMethodManagerDelegate;

    move-result-object v6

    iget-object v0, p0, Landroid/view/ImeFocusController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    invoke-interface {v6, v0}, Landroid/view/ImeFocusController$InputMethodManagerDelegate;->isCurrentRootView(Landroid/view/ViewRootImpl;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/view/ImeFocusController;->mServedView:Landroid/view/View;

    iget-object v2, p0, Landroid/view/ImeFocusController;->mNextServedView:Landroid/view/View;

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Landroid/view/ImeFocusController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    invoke-virtual {v0}, Landroid/view/ViewRootImpl;->isHandWritingKeyboardTypeChanged()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Landroid/view/ImeFocusController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    invoke-virtual {v0}, Landroid/view/ViewRootImpl;->refreshHandWritingLastKeyboardType()V

    iget-object v0, p0, Landroid/view/ImeFocusController;->mNextServedView:Landroid/view/View;

    if-nez v0, :cond_1

    invoke-interface {v6}, Landroid/view/ImeFocusController$InputMethodManagerDelegate;->finishInput()V

    invoke-interface {v6}, Landroid/view/ImeFocusController$InputMethodManagerDelegate;->closeCurrentIme()V

    return v1

    :cond_1
    iput-object v0, p0, Landroid/view/ImeFocusController;->mServedView:Landroid/view/View;

    invoke-interface {v6}, Landroid/view/ImeFocusController$InputMethodManagerDelegate;->finishComposingText()V

    if-eqz p2, :cond_2

    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, v6

    invoke-interface/range {v0 .. v5}, Landroid/view/ImeFocusController$InputMethodManagerDelegate;->startInput(ILandroid/view/View;III)Z

    :cond_2
    const/4 v0, 0x1

    return v0

    :cond_3
    :goto_0
    return v1
.end method

.method dumpDebug(Landroid/util/proto/ProtoOutputStream;J)V
    .locals 5

    goto/32 :goto_6

    nop

    :goto_0
    return-void

    :goto_1
    const-wide v3, 0x10800000001L

    goto/32 :goto_8

    nop

    :goto_2
    const-wide v3, 0x10900000003L

    goto/32 :goto_b

    nop

    :goto_3
    invoke-static {v2}, Ljava/util/Objects;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_d

    nop

    :goto_4
    invoke-virtual {p1, v3, v4, v2}, Landroid/util/proto/ProtoOutputStream;->write(JLjava/lang/String;)V

    goto/32 :goto_9

    nop

    :goto_5
    iget-boolean v2, p0, Landroid/view/ImeFocusController;->mHasImeFocus:Z

    goto/32 :goto_1

    nop

    :goto_6
    invoke-virtual {p1, p2, p3}, Landroid/util/proto/ProtoOutputStream;->start(J)J

    move-result-wide v0

    goto/32 :goto_5

    nop

    :goto_7
    iget-object v2, p0, Landroid/view/ImeFocusController;->mServedView:Landroid/view/View;

    goto/32 :goto_3

    nop

    :goto_8
    invoke-virtual {p1, v3, v4, v2}, Landroid/util/proto/ProtoOutputStream;->write(JZ)V

    goto/32 :goto_7

    nop

    :goto_9
    iget-object v2, p0, Landroid/view/ImeFocusController;->mNextServedView:Landroid/view/View;

    goto/32 :goto_c

    nop

    :goto_a
    invoke-virtual {p1, v0, v1}, Landroid/util/proto/ProtoOutputStream;->end(J)V

    goto/32 :goto_0

    nop

    :goto_b
    invoke-virtual {p1, v3, v4, v2}, Landroid/util/proto/ProtoOutputStream;->write(JLjava/lang/String;)V

    goto/32 :goto_a

    nop

    :goto_c
    invoke-static {v2}, Ljava/util/Objects;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_2

    nop

    :goto_d
    const-wide v3, 0x10900000002L

    goto/32 :goto_4

    nop
.end method

.method public getNextServedView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Landroid/view/ImeFocusController;->mNextServedView:Landroid/view/View;

    return-object v0
.end method

.method public getServedView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Landroid/view/ImeFocusController;->mServedView:Landroid/view/View;

    return-object v0
.end method

.method hasImeFocus()Z
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    iget-boolean v0, p0, Landroid/view/ImeFocusController;->mHasImeFocus:Z

    goto/32 :goto_0

    nop
.end method

.method public onInteractiveChanged(Z)V
    .locals 3

    invoke-direct {p0}, Landroid/view/ImeFocusController;->getImmDelegate()Landroid/view/ImeFocusController$InputMethodManagerDelegate;

    move-result-object v0

    iget-object v1, p0, Landroid/view/ImeFocusController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    invoke-interface {v0, v1}, Landroid/view/ImeFocusController$InputMethodManagerDelegate;->isCurrentRootView(Landroid/view/ViewRootImpl;)Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_2

    iget-object v1, p0, Landroid/view/ImeFocusController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    iget-object v1, v1, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v1, v2}, Landroid/view/ImeFocusController;->onViewFocusChanged(Landroid/view/View;Z)V

    goto :goto_1

    :cond_2
    iget-object v1, p0, Landroid/view/ImeFocusController;->mDelegate:Landroid/view/ImeFocusController$InputMethodManagerDelegate;

    invoke-interface {v1}, Landroid/view/ImeFocusController$InputMethodManagerDelegate;->finishInputAndReportToIme()V

    :goto_1
    return-void
.end method

.method onMovedToDisplay()V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_2

    nop

    :goto_2
    iput-object v0, p0, Landroid/view/ImeFocusController;->mDelegate:Landroid/view/ImeFocusController$InputMethodManagerDelegate;

    goto/32 :goto_0

    nop
.end method

.method onPostWindowFocus(Landroid/view/View;ZLandroid/view/WindowManager$LayoutParams;)V
    .locals 6

    goto/32 :goto_15

    nop

    :goto_0
    return-void

    :goto_1
    goto/32 :goto_14

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_c

    nop

    :goto_3
    if-nez p1, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_1c

    nop

    :goto_4
    invoke-interface {v2, v0, v4, v5, v1}, Landroid/view/ImeFocusController$InputMethodManagerDelegate;->startInputAsyncOnWindowFocusGain(Landroid/view/View;IIZ)V

    goto/32 :goto_0

    nop

    :goto_5
    iget-object v0, v0, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    :goto_6
    goto/32 :goto_22

    nop

    :goto_7
    goto :goto_6

    :goto_8
    goto/32 :goto_26

    nop

    :goto_9
    const/4 v1, 0x1

    :goto_a
    goto/32 :goto_23

    nop

    :goto_b
    if-eq v4, v0, :cond_2

    goto/32 :goto_21

    :cond_2
    goto/32 :goto_20

    nop

    :goto_c
    invoke-static {p3}, Landroid/view/ImeFocusController;->isInLocalFocusMode(Landroid/view/WindowManager$LayoutParams;)Z

    move-result v0

    goto/32 :goto_11

    nop

    :goto_d
    if-eqz v4, :cond_3

    goto/32 :goto_13

    :cond_3
    goto/32 :goto_12

    nop

    :goto_e
    iget v5, p3, Landroid/view/WindowManager$LayoutParams;->flags:I

    goto/32 :goto_4

    nop

    :goto_f
    invoke-direct {p0}, Landroid/view/ImeFocusController;->getImmDelegate()Landroid/view/ImeFocusController$InputMethodManagerDelegate;

    move-result-object v2

    goto/32 :goto_16

    nop

    :goto_10
    invoke-interface {v2, v3}, Landroid/view/ImeFocusController$InputMethodManagerDelegate;->isRestartOnNextWindowFocus(Z)Z

    move-result v4

    goto/32 :goto_24

    nop

    :goto_11
    if-nez v0, :cond_4

    goto/32 :goto_18

    :cond_4
    goto/32 :goto_17

    nop

    :goto_12
    const/4 v1, 0x1

    :goto_13
    goto/32 :goto_19

    nop

    :goto_14
    return-void

    :goto_15
    if-nez p2, :cond_5

    goto/32 :goto_1

    :cond_5
    goto/32 :goto_1b

    nop

    :goto_16
    const/4 v3, 0x1

    goto/32 :goto_10

    nop

    :goto_17
    goto :goto_1

    :goto_18
    goto/32 :goto_3

    nop

    :goto_19
    iget v4, p3, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    goto/32 :goto_e

    nop

    :goto_1a
    if-nez v3, :cond_6

    goto/32 :goto_13

    :cond_6
    goto/32 :goto_25

    nop

    :goto_1b
    iget-boolean v0, p0, Landroid/view/ImeFocusController;->mHasImeFocus:Z

    goto/32 :goto_2

    nop

    :goto_1c
    move-object v0, p1

    goto/32 :goto_7

    nop

    :goto_1d
    const/4 v3, 0x0

    :goto_1e
    goto/32 :goto_1a

    nop

    :goto_1f
    iget-object v4, p0, Landroid/view/ImeFocusController;->mServedView:Landroid/view/View;

    goto/32 :goto_b

    nop

    :goto_20
    goto :goto_1e

    :goto_21
    goto/32 :goto_1d

    nop

    :goto_22
    const/4 v1, 0x0

    goto/32 :goto_f

    nop

    :goto_23
    invoke-virtual {p0, v0, v3}, Landroid/view/ImeFocusController;->onViewFocusChanged(Landroid/view/View;Z)V

    goto/32 :goto_1f

    nop

    :goto_24
    if-nez v4, :cond_7

    goto/32 :goto_a

    :cond_7
    goto/32 :goto_9

    nop

    :goto_25
    invoke-interface {v2, v0}, Landroid/view/ImeFocusController$InputMethodManagerDelegate;->hasActiveConnection(Landroid/view/View;)Z

    move-result v4

    goto/32 :goto_d

    nop

    :goto_26
    iget-object v0, p0, Landroid/view/ImeFocusController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    goto/32 :goto_5

    nop
.end method

.method onPreWindowFocus(ZLandroid/view/WindowManager$LayoutParams;)V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_d

    nop

    :goto_1
    return-void

    :goto_2
    iget-boolean v0, p0, Landroid/view/ImeFocusController;->mHasImeFocus:Z

    goto/32 :goto_0

    nop

    :goto_3
    return-void

    :goto_4
    goto/32 :goto_1

    nop

    :goto_5
    if-nez v0, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_b

    nop

    :goto_6
    if-nez p1, :cond_2

    goto/32 :goto_9

    :cond_2
    goto/32 :goto_a

    nop

    :goto_7
    iget-object v1, p0, Landroid/view/ImeFocusController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    goto/32 :goto_8

    nop

    :goto_8
    invoke-interface {v0, v1}, Landroid/view/ImeFocusController$InputMethodManagerDelegate;->setCurrentRootView(Landroid/view/ViewRootImpl;)V

    :goto_9
    goto/32 :goto_3

    nop

    :goto_a
    invoke-direct {p0}, Landroid/view/ImeFocusController;->getImmDelegate()Landroid/view/ImeFocusController$InputMethodManagerDelegate;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_b
    goto :goto_4

    :goto_c
    goto/32 :goto_6

    nop

    :goto_d
    invoke-static {p2}, Landroid/view/ImeFocusController;->isInLocalFocusMode(Landroid/view/WindowManager$LayoutParams;)Z

    move-result v0

    goto/32 :goto_5

    nop
.end method

.method onProcessImeInputStage(Ljava/lang/Object;Landroid/view/InputEvent;Landroid/view/WindowManager$LayoutParams;Landroid/view/inputmethod/InputMethodManager$FinishedInputEventCallback;)I
    .locals 3

    goto/32 :goto_3

    nop

    :goto_0
    if-eqz v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_4

    nop

    :goto_1
    iget-object v0, v0, Landroid/view/ViewRootImpl;->mContext:Landroid/content/Context;

    goto/32 :goto_c

    nop

    :goto_2
    return v1

    :goto_3
    iget-boolean v0, p0, Landroid/view/ImeFocusController;->mHasImeFocus:Z

    goto/32 :goto_8

    nop

    :goto_4
    return v1

    :goto_5
    goto/32 :goto_7

    nop

    :goto_6
    iget-object v0, p0, Landroid/view/ImeFocusController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    goto/32 :goto_1

    nop

    :goto_7
    iget-object v1, p0, Landroid/view/ImeFocusController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    goto/32 :goto_10

    nop

    :goto_8
    const/4 v1, 0x0

    goto/32 :goto_9

    nop

    :goto_9
    if-nez v0, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_11

    nop

    :goto_a
    if-nez v0, :cond_2

    goto/32 :goto_13

    :cond_2
    goto/32 :goto_12

    nop

    :goto_b
    invoke-virtual {v0, p2, p1, p4, v1}, Landroid/view/inputmethod/InputMethodManager;->dispatchInputEvent(Landroid/view/InputEvent;Ljava/lang/Object;Landroid/view/inputmethod/InputMethodManager$FinishedInputEventCallback;Landroid/os/Handler;)I

    move-result v1

    goto/32 :goto_d

    nop

    :goto_c
    const-class v2, Landroid/view/inputmethod/InputMethodManager;

    goto/32 :goto_14

    nop

    :goto_d
    return v1

    :goto_e
    goto/32 :goto_2

    nop

    :goto_f
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    goto/32 :goto_0

    nop

    :goto_10
    iget-object v1, v1, Landroid/view/ViewRootImpl;->mHandler:Landroid/view/ViewRootImpl$ViewRootHandler;

    goto/32 :goto_b

    nop

    :goto_11
    invoke-static {p3}, Landroid/view/ImeFocusController;->isInLocalFocusMode(Landroid/view/WindowManager$LayoutParams;)Z

    move-result v0

    goto/32 :goto_a

    nop

    :goto_12
    goto :goto_e

    :goto_13
    goto/32 :goto_6

    nop

    :goto_14
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_f

    nop
.end method

.method onTraversal(ZLandroid/view/WindowManager$LayoutParams;)V
    .locals 3

    goto/32 :goto_13

    nop

    :goto_0
    iput-boolean v0, p0, Landroid/view/ImeFocusController;->mHasImeFocus:Z

    goto/32 :goto_2

    nop

    :goto_1
    invoke-static {p2}, Landroid/view/ImeFocusController;->isInLocalFocusMode(Landroid/view/WindowManager$LayoutParams;)Z

    move-result v1

    goto/32 :goto_10

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_3

    nop

    :goto_3
    const/4 v1, 0x1

    goto/32 :goto_16

    nop

    :goto_4
    if-nez p1, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_1

    nop

    :goto_5
    invoke-virtual {p0, v2, v1, p2}, Landroid/view/ImeFocusController;->onPostWindowFocus(Landroid/view/View;ZLandroid/view/WindowManager$LayoutParams;)V

    :goto_6
    goto/32 :goto_d

    nop

    :goto_7
    iget-object v2, p0, Landroid/view/ImeFocusController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    goto/32 :goto_f

    nop

    :goto_8
    if-eq v0, v1, :cond_2

    goto/32 :goto_c

    :cond_2
    goto/32 :goto_b

    nop

    :goto_9
    invoke-virtual {v2}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v2

    goto/32 :goto_5

    nop

    :goto_a
    iget-boolean v1, p0, Landroid/view/ImeFocusController;->mHasImeFocus:Z

    goto/32 :goto_8

    nop

    :goto_b
    return-void

    :goto_c
    goto/32 :goto_0

    nop

    :goto_d
    return-void

    :goto_e
    goto/32 :goto_12

    nop

    :goto_f
    iget-object v2, v2, Landroid/view/ViewRootImpl;->mView:Landroid/view/View;

    goto/32 :goto_9

    nop

    :goto_10
    if-nez v1, :cond_3

    goto/32 :goto_15

    :cond_3
    goto/32 :goto_14

    nop

    :goto_11
    invoke-virtual {p0, p2, v0}, Landroid/view/ImeFocusController;->updateImeFocusable(Landroid/view/WindowManager$LayoutParams;Z)Z

    move-result v0

    goto/32 :goto_4

    nop

    :goto_12
    return-void

    :goto_13
    const/4 v0, 0x0

    goto/32 :goto_11

    nop

    :goto_14
    goto :goto_e

    :goto_15
    goto/32 :goto_a

    nop

    :goto_16
    invoke-virtual {p0, v1, p2}, Landroid/view/ImeFocusController;->onPreWindowFocus(ZLandroid/view/WindowManager$LayoutParams;)V

    goto/32 :goto_7

    nop
.end method

.method onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 2

    goto/32 :goto_8

    nop

    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getViewRootImpl()Landroid/view/ViewRootImpl;

    move-result-object v1

    goto/32 :goto_4

    nop

    :goto_1
    if-eqz v0, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_b

    nop

    :goto_2
    iget-object v0, p0, Landroid/view/ImeFocusController;->mServedView:Landroid/view/View;

    goto/32 :goto_d

    nop

    :goto_3
    iget-object v0, p0, Landroid/view/ImeFocusController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    goto/32 :goto_5

    nop

    :goto_4
    invoke-interface {v0, v1}, Landroid/view/ImeFocusController$InputMethodManagerDelegate;->isCurrentRootView(Landroid/view/ViewRootImpl;)Z

    move-result v0

    goto/32 :goto_1

    nop

    :goto_5
    invoke-virtual {v0}, Landroid/view/ViewRootImpl;->dispatchCheckFocus()V

    :goto_6
    goto/32 :goto_7

    nop

    :goto_7
    return-void

    :goto_8
    invoke-direct {p0}, Landroid/view/ImeFocusController;->getImmDelegate()Landroid/view/ImeFocusController$InputMethodManagerDelegate;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_9
    const/4 v0, 0x0

    goto/32 :goto_f

    nop

    :goto_a
    iget-object v0, p0, Landroid/view/ImeFocusController;->mNextServedView:Landroid/view/View;

    goto/32 :goto_e

    nop

    :goto_b
    return-void

    :goto_c
    goto/32 :goto_a

    nop

    :goto_d
    if-eq v0, p1, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_3

    nop

    :goto_e
    if-eq v0, p1, :cond_2

    goto/32 :goto_10

    :cond_2
    goto/32 :goto_9

    nop

    :goto_f
    iput-object v0, p0, Landroid/view/ImeFocusController;->mNextServedView:Landroid/view/View;

    :goto_10
    goto/32 :goto_2

    nop
.end method

.method onViewFocusChanged(Landroid/view/View;Z)V
    .locals 2

    goto/32 :goto_a

    nop

    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->hasWindowFocus()Z

    move-result v0

    goto/32 :goto_d

    nop

    :goto_1
    return-void

    :goto_2
    goto/32 :goto_1a

    nop

    :goto_3
    invoke-direct {p0}, Landroid/view/ImeFocusController;->getImmDelegate()Landroid/view/ImeFocusController$InputMethodManagerDelegate;

    move-result-object v0

    goto/32 :goto_11

    nop

    :goto_4
    invoke-virtual {p1}, Landroid/view/View;->isTemporarilyDetached()Z

    move-result v0

    goto/32 :goto_b

    nop

    :goto_5
    return-void

    :goto_6
    goto/32 :goto_e

    nop

    :goto_7
    return-void

    :goto_8
    if-nez p2, :cond_0

    goto/32 :goto_19

    :cond_0
    goto/32 :goto_18

    nop

    :goto_9
    invoke-virtual {v0}, Landroid/view/ViewRootImpl;->dispatchCheckFocus()V

    goto/32 :goto_5

    nop

    :goto_a
    if-nez p1, :cond_1

    goto/32 :goto_f

    :cond_1
    goto/32 :goto_4

    nop

    :goto_b
    if-nez v0, :cond_2

    goto/32 :goto_15

    :cond_2
    goto/32 :goto_14

    nop

    :goto_c
    invoke-interface {v0, v1}, Landroid/view/ImeFocusController$InputMethodManagerDelegate;->isCurrentRootView(Landroid/view/ViewRootImpl;)Z

    move-result v0

    goto/32 :goto_12

    nop

    :goto_d
    if-eqz v0, :cond_3

    goto/32 :goto_17

    :cond_3
    goto/32 :goto_16

    nop

    :goto_e
    return-void

    :goto_f
    goto/32 :goto_7

    nop

    :goto_10
    iget-object v0, p0, Landroid/view/ImeFocusController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    goto/32 :goto_9

    nop

    :goto_11
    invoke-virtual {p1}, Landroid/view/View;->getViewRootImpl()Landroid/view/ViewRootImpl;

    move-result-object v1

    goto/32 :goto_c

    nop

    :goto_12
    if-eqz v0, :cond_4

    goto/32 :goto_2

    :cond_4
    goto/32 :goto_1

    nop

    :goto_13
    if-nez v0, :cond_5

    goto/32 :goto_6

    :cond_5
    goto/32 :goto_0

    nop

    :goto_14
    goto :goto_f

    :goto_15
    goto/32 :goto_3

    nop

    :goto_16
    goto :goto_6

    :goto_17
    goto/32 :goto_8

    nop

    :goto_18
    iput-object p1, p0, Landroid/view/ImeFocusController;->mNextServedView:Landroid/view/View;

    :goto_19
    goto/32 :goto_10

    nop

    :goto_1a
    invoke-virtual {p1}, Landroid/view/View;->hasImeFocus()Z

    move-result v0

    goto/32 :goto_13

    nop
.end method

.method onWindowDismissed()V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    invoke-direct {p0}, Landroid/view/ImeFocusController;->getImmDelegate()Landroid/view/ImeFocusController$InputMethodManagerDelegate;

    move-result-object v0

    goto/32 :goto_a

    nop

    :goto_1
    const/4 v1, 0x0

    goto/32 :goto_b

    nop

    :goto_2
    if-eqz v1, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_7

    nop

    :goto_3
    invoke-interface {v0}, Landroid/view/ImeFocusController$InputMethodManagerDelegate;->finishInput()V

    :goto_4
    goto/32 :goto_1

    nop

    :goto_5
    iput-boolean v1, p0, Landroid/view/ImeFocusController;->mHasImeFocus:Z

    goto/32 :goto_6

    nop

    :goto_6
    return-void

    :goto_7
    return-void

    :goto_8
    goto/32 :goto_d

    nop

    :goto_9
    if-nez v1, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_3

    nop

    :goto_a
    iget-object v1, p0, Landroid/view/ImeFocusController;->mViewRootImpl:Landroid/view/ViewRootImpl;

    goto/32 :goto_c

    nop

    :goto_b
    invoke-interface {v0, v1}, Landroid/view/ImeFocusController$InputMethodManagerDelegate;->setCurrentRootView(Landroid/view/ViewRootImpl;)V

    goto/32 :goto_e

    nop

    :goto_c
    invoke-interface {v0, v1}, Landroid/view/ImeFocusController$InputMethodManagerDelegate;->isCurrentRootView(Landroid/view/ViewRootImpl;)Z

    move-result v1

    goto/32 :goto_2

    nop

    :goto_d
    iget-object v1, p0, Landroid/view/ImeFocusController;->mServedView:Landroid/view/View;

    goto/32 :goto_9

    nop

    :goto_e
    const/4 v1, 0x0

    goto/32 :goto_5

    nop
.end method

.method public setNextServedView(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Landroid/view/ImeFocusController;->mNextServedView:Landroid/view/View;

    return-void
.end method

.method public setServedView(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Landroid/view/ImeFocusController;->mServedView:Landroid/view/View;

    return-void
.end method

.method updateImeFocusable(Landroid/view/WindowManager$LayoutParams;Z)Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    goto/32 :goto_4

    nop

    :goto_1
    iput-boolean v0, p0, Landroid/view/ImeFocusController;->mHasImeFocus:Z

    :goto_2
    goto/32 :goto_5

    nop

    :goto_3
    if-nez p2, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop

    :goto_4
    invoke-static {v0}, Landroid/view/WindowManager$LayoutParams;->mayUseInputMethod(I)Z

    move-result v0

    goto/32 :goto_3

    nop

    :goto_5
    return v0
.end method
