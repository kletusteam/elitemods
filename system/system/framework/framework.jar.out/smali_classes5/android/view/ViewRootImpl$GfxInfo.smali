.class final Landroid/view/ViewRootImpl$GfxInfo;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/ViewRootImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "GfxInfo"
.end annotation


# instance fields
.field public renderNodeMemoryAllocated:J

.field public renderNodeMemoryUsage:J

.field public viewCount:I


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method add(Landroid/view/ViewRootImpl$GfxInfo;)V
    .locals 4

    goto/32 :goto_a

    nop

    :goto_0
    iget v1, p1, Landroid/view/ViewRootImpl$GfxInfo;->viewCount:I

    goto/32 :goto_8

    nop

    :goto_1
    iget-wide v0, p0, Landroid/view/ViewRootImpl$GfxInfo;->renderNodeMemoryUsage:J

    goto/32 :goto_3

    nop

    :goto_2
    iput-wide v0, p0, Landroid/view/ViewRootImpl$GfxInfo;->renderNodeMemoryUsage:J

    goto/32 :goto_b

    nop

    :goto_3
    iget-wide v2, p1, Landroid/view/ViewRootImpl$GfxInfo;->renderNodeMemoryUsage:J

    goto/32 :goto_5

    nop

    :goto_4
    iput-wide v0, p0, Landroid/view/ViewRootImpl$GfxInfo;->renderNodeMemoryAllocated:J

    goto/32 :goto_7

    nop

    :goto_5
    add-long/2addr v0, v2

    goto/32 :goto_2

    nop

    :goto_6
    add-long/2addr v0, v2

    goto/32 :goto_4

    nop

    :goto_7
    return-void

    :goto_8
    add-int/2addr v0, v1

    goto/32 :goto_9

    nop

    :goto_9
    iput v0, p0, Landroid/view/ViewRootImpl$GfxInfo;->viewCount:I

    goto/32 :goto_1

    nop

    :goto_a
    iget v0, p0, Landroid/view/ViewRootImpl$GfxInfo;->viewCount:I

    goto/32 :goto_0

    nop

    :goto_b
    iget-wide v0, p0, Landroid/view/ViewRootImpl$GfxInfo;->renderNodeMemoryAllocated:J

    goto/32 :goto_c

    nop

    :goto_c
    iget-wide v2, p1, Landroid/view/ViewRootImpl$GfxInfo;->renderNodeMemoryAllocated:J

    goto/32 :goto_6

    nop
.end method
