.class public final Landroid/view/ThreadedRenderer;
.super Landroid/graphics/HardwareRenderer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/ThreadedRenderer$SimpleRenderer;,
        Landroid/view/ThreadedRenderer$DrawCallbacks;,
        Landroid/view/ThreadedRenderer$WebViewOverlayProvider;
    }
.end annotation


# static fields
.field public static final DEBUG_DIRTY_REGIONS_PROPERTY:Ljava/lang/String; = "debug.hwui.show_dirty_regions"

.field public static final DEBUG_FORCE_DARK:Ljava/lang/String; = "debug.hwui.force_dark"

.field public static final DEBUG_FPS_DIVISOR:Ljava/lang/String; = "debug.hwui.fps_divisor"

.field public static final DEBUG_OVERDRAW_PROPERTY:Ljava/lang/String; = "debug.hwui.overdraw"

.field public static final DEBUG_SHOW_LAYERS_UPDATES_PROPERTY:Ljava/lang/String; = "debug.hwui.show_layers_updates"

.field public static final DEBUG_SHOW_NON_RECTANGULAR_CLIP_PROPERTY:Ljava/lang/String; = "debug.hwui.show_non_rect_clip"

.field public static EGL_CONTEXT_PRIORITY_HIGH_IMG:I = 0x0

.field public static EGL_CONTEXT_PRIORITY_LOW_IMG:I = 0x0

.field public static EGL_CONTEXT_PRIORITY_MEDIUM_IMG:I = 0x0

.field public static EGL_CONTEXT_PRIORITY_REALTIME_NV:I = 0x0

.field public static final OVERDRAW_PROPERTY_SHOW:Ljava/lang/String; = "show"

.field static final PRINT_CONFIG_PROPERTY:Ljava/lang/String; = "debug.hwui.print_config"

.field static final PROFILE_MAXFRAMES_PROPERTY:Ljava/lang/String; = "debug.hwui.profile.maxframes"

.field public static final PROFILE_PROPERTY:Ljava/lang/String; = "debug.hwui.profile"

.field public static final PROFILE_PROPERTY_VISUALIZE_BARS:Ljava/lang/String; = "visual_bars"

.field private static final VISUALIZERS:[Ljava/lang/String;

.field public static sRendererEnabled:Z

.field public static sTrimForeground:Z


# instance fields
.field private mEnabled:Z

.field private mHeight:I

.field private mInitialized:Z

.field private mInsetLeft:I

.field private mInsetTop:I

.field private final mLightRadius:F

.field private final mLightY:F

.field private final mLightZ:F

.field private mNextRtFrameCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/graphics/HardwareRenderer$FrameDrawingCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mRequested:Z

.field private mRootNodeNeedsUpdate:Z

.field private mSurfaceHeight:I

.field private mSurfaceWidth:I

.field private final mWebViewOverlayProvider:Landroid/view/ThreadedRenderer$WebViewOverlayProvider;

.field private mWebViewOverlaysEnabled:Z

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x3357

    sput v0, Landroid/view/ThreadedRenderer;->EGL_CONTEXT_PRIORITY_REALTIME_NV:I

    const/16 v0, 0x3101

    sput v0, Landroid/view/ThreadedRenderer;->EGL_CONTEXT_PRIORITY_HIGH_IMG:I

    const/16 v0, 0x3102

    sput v0, Landroid/view/ThreadedRenderer;->EGL_CONTEXT_PRIORITY_MEDIUM_IMG:I

    const/16 v0, 0x3103

    sput v0, Landroid/view/ThreadedRenderer;->EGL_CONTEXT_PRIORITY_LOW_IMG:I

    const/4 v0, 0x1

    sput-boolean v0, Landroid/view/ThreadedRenderer;->sRendererEnabled:Z

    const/4 v0, 0x0

    sput-boolean v0, Landroid/view/ThreadedRenderer;->sTrimForeground:Z

    const-string/jumbo v0, "visual_bars"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/view/ThreadedRenderer;->VISUALIZERS:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;ZLjava/lang/String;)V
    .locals 5

    invoke-direct {p0}, Landroid/graphics/HardwareRenderer;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/view/ThreadedRenderer;->mInitialized:Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid/view/ThreadedRenderer;->mRequested:Z

    new-instance v2, Landroid/view/ThreadedRenderer$WebViewOverlayProvider;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Landroid/view/ThreadedRenderer$WebViewOverlayProvider;-><init>(Landroid/view/ThreadedRenderer$WebViewOverlayProvider-IA;)V

    iput-object v2, p0, Landroid/view/ThreadedRenderer;->mWebViewOverlayProvider:Landroid/view/ThreadedRenderer$WebViewOverlayProvider;

    iput-boolean v0, p0, Landroid/view/ThreadedRenderer;->mWebViewOverlaysEnabled:Z

    invoke-virtual {p0, p3}, Landroid/view/ThreadedRenderer;->setName(Ljava/lang/String;)V

    xor-int/lit8 v2, p2, 0x1

    invoke-virtual {p0, v2}, Landroid/view/ThreadedRenderer;->setOpaque(Z)V

    sget-object v2, Lcom/android/internal/R$styleable;->Lighting:[I

    invoke-virtual {p1, v3, v2, v0, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    iput v3, p0, Landroid/view/ThreadedRenderer;->mLightY:F

    const/4 v3, 0x4

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    iput v3, p0, Landroid/view/ThreadedRenderer;->mLightZ:F

    const/4 v3, 0x2

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    iput v3, p0, Landroid/view/ThreadedRenderer;->mLightRadius:F

    invoke-virtual {v2, v0, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    invoke-virtual {v2, v1, v4}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0, v0, v1}, Landroid/view/ThreadedRenderer;->setLightSourceAlpha(FF)V

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    invoke-static {}, Landroid/view/ThreadedRenderer;->isWebViewOverlaysEnabled()Z

    move-result v0

    return v0
.end method

.method public static create(Landroid/content/Context;ZLjava/lang/String;)Landroid/view/ThreadedRenderer;
    .locals 1

    new-instance v0, Landroid/view/ThreadedRenderer;

    invoke-direct {v0, p0, p1, p2}, Landroid/view/ThreadedRenderer;-><init>(Landroid/content/Context;ZLjava/lang/String;)V

    return-object v0
.end method

.method private static destroyResources(Landroid/view/View;)V
    .locals 0

    invoke-virtual {p0}, Landroid/view/View;->destroyHardwareResources()V

    return-void
.end method

.method private static dumpArgsToFlags([Ljava/lang/String;)I
    .locals 6

    const/4 v0, 0x1

    if-eqz p0, :cond_3

    array-length v1, p0

    if-nez v1, :cond_0

    goto :goto_3

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    array-length v3, p0

    if-ge v2, v3, :cond_2

    aget-object v3, p0, v2

    const/4 v4, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_1
    goto :goto_1

    :sswitch_0
    const-string/jumbo v5, "reset"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v4, v0

    goto :goto_1

    :sswitch_1
    const-string v5, "-a"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v4, 0x2

    goto :goto_1

    :sswitch_2
    const-string v5, "framestats"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v4, 0x0

    :goto_1
    packed-switch v4, :pswitch_data_0

    goto :goto_2

    :pswitch_0
    const/4 v1, 0x1

    goto :goto_2

    :pswitch_1
    or-int/lit8 v1, v1, 0x2

    goto :goto_2

    :pswitch_2
    or-int/lit8 v1, v1, 0x1

    nop

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return v1

    :cond_3
    :goto_3
    return v0

    :sswitch_data_0
    .sparse-switch
        -0xf0608ae -> :sswitch_2
        0x5d4 -> :sswitch_1
        0x6761d4f -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static enableForegroundTrimming()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Landroid/view/ThreadedRenderer;->sTrimForeground:Z

    return-void
.end method

.method public static handleDumpGfxInfo(Ljava/io/FileDescriptor;[Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Landroid/view/ThreadedRenderer;->dumpArgsToFlags([Ljava/lang/String;)I

    move-result v0

    invoke-static {p0, v0}, Landroid/view/ThreadedRenderer;->dumpGlobalProfileInfo(Ljava/io/FileDescriptor;I)V

    invoke-static {}, Landroid/view/WindowManagerGlobal;->getInstance()Landroid/view/WindowManagerGlobal;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/view/WindowManagerGlobal;->dumpGfxInfo(Ljava/io/FileDescriptor;[Ljava/lang/String;)V

    return-void
.end method

.method public static initForSystemProcess()V
    .locals 1

    invoke-static {}, Landroid/app/ActivityManager;->isHighEndGfx()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    sput-boolean v0, Landroid/view/ThreadedRenderer;->sRendererEnabled:Z

    goto :goto_0

    :cond_0
    invoke-static {}, Landroid/view/ThreadedRenderer;->enableForegroundTrimming()V

    :goto_0
    return-void
.end method

.method private updateEnabledState(Landroid/view/Surface;)V
    .locals 1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/view/Surface;->isValid()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Landroid/view/ThreadedRenderer;->mInitialized:Z

    invoke-virtual {p0, v0}, Landroid/view/ThreadedRenderer;->setEnabled(Z)V

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/ThreadedRenderer;->setEnabled(Z)V

    :goto_1
    return-void
.end method

.method private updateRootDisplayList(Landroid/view/View;Landroid/view/ThreadedRenderer$DrawCallbacks;)V
    .locals 6

    const-wide/16 v0, 0x8

    const-string v2, "Record View#draw()"

    invoke-static {v0, v1, v2}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    invoke-interface {p2}, Landroid/view/ThreadedRenderer$DrawCallbacks;->onPreRecordViewDraw()V

    invoke-direct {p0, p1}, Landroid/view/ThreadedRenderer;->updateViewTreeDisplayList(Landroid/view/View;)V

    iget-object v2, p0, Landroid/view/ThreadedRenderer;->mNextRtFrameCallbacks:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/view/ThreadedRenderer;->mNextRtFrameCallbacks:Ljava/util/ArrayList;

    const/4 v3, 0x0

    iput-object v3, p0, Landroid/view/ThreadedRenderer;->mNextRtFrameCallbacks:Ljava/util/ArrayList;

    new-instance v3, Landroid/view/ThreadedRenderer$1;

    invoke-direct {v3, p0, v2}, Landroid/view/ThreadedRenderer$1;-><init>(Landroid/view/ThreadedRenderer;Ljava/util/ArrayList;)V

    invoke-virtual {p0, v3}, Landroid/view/ThreadedRenderer;->setFrameCallback(Landroid/graphics/HardwareRenderer$FrameDrawingCallback;)V

    :cond_0
    iget-boolean v2, p0, Landroid/view/ThreadedRenderer;->mRootNodeNeedsUpdate:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Landroid/view/ThreadedRenderer;->mRootNode:Landroid/graphics/RenderNode;

    invoke-virtual {v2}, Landroid/graphics/RenderNode;->hasDisplayList()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    iget-object v2, p0, Landroid/view/ThreadedRenderer;->mRootNode:Landroid/graphics/RenderNode;

    iget v3, p0, Landroid/view/ThreadedRenderer;->mSurfaceWidth:I

    iget v4, p0, Landroid/view/ThreadedRenderer;->mSurfaceHeight:I

    invoke-virtual {v2, v3, v4}, Landroid/graphics/RenderNode;->beginRecording(II)Landroid/graphics/RecordingCanvas;

    move-result-object v2

    :try_start_0
    invoke-virtual {v2}, Landroid/graphics/RecordingCanvas;->save()I

    move-result v3

    iget v4, p0, Landroid/view/ThreadedRenderer;->mInsetLeft:I

    int-to-float v4, v4

    iget v5, p0, Landroid/view/ThreadedRenderer;->mInsetTop:I

    int-to-float v5, v5

    invoke-virtual {v2, v4, v5}, Landroid/graphics/RecordingCanvas;->translate(FF)V

    invoke-interface {p2, v2}, Landroid/view/ThreadedRenderer$DrawCallbacks;->onPreDraw(Landroid/graphics/RecordingCanvas;)V

    invoke-virtual {v2}, Landroid/graphics/RecordingCanvas;->enableZ()V

    invoke-virtual {p1}, Landroid/view/View;->updateDisplayListIfDirty()Landroid/graphics/RenderNode;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/graphics/RecordingCanvas;->drawRenderNode(Landroid/graphics/RenderNode;)V

    invoke-virtual {v2}, Landroid/graphics/RecordingCanvas;->disableZ()V

    invoke-interface {p2, v2}, Landroid/view/ThreadedRenderer$DrawCallbacks;->onPostDraw(Landroid/graphics/RecordingCanvas;)V

    invoke-virtual {v2, v3}, Landroid/graphics/RecordingCanvas;->restoreToCount(I)V

    const/4 v4, 0x0

    iput-boolean v4, p0, Landroid/view/ThreadedRenderer;->mRootNodeNeedsUpdate:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v3, p0, Landroid/view/ThreadedRenderer;->mRootNode:Landroid/graphics/RenderNode;

    invoke-virtual {v3}, Landroid/graphics/RenderNode;->endRecording()V

    nop

    :cond_2
    invoke-static {v0, v1}, Landroid/os/Trace;->traceEnd(J)V

    invoke-interface {p2}, Landroid/view/ThreadedRenderer$DrawCallbacks;->onPostRecordViewDraw()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Landroid/view/ThreadedRenderer;->mRootNode:Landroid/graphics/RenderNode;

    invoke-virtual {v1}, Landroid/graphics/RenderNode;->endRecording()V

    throw v0
.end method

.method private updateViewTreeDisplayList(Landroid/view/View;)V
    .locals 3

    iget v0, p1, Landroid/view/View;->mPrivateFlags:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p1, Landroid/view/View;->mPrivateFlags:I

    iget v0, p1, Landroid/view/View;->mPrivateFlags:I

    const/high16 v1, -0x80000000

    and-int/2addr v0, v1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean v0, p1, Landroid/view/View;->mRecreateDisplayList:Z

    iget v0, p1, Landroid/view/View;->mPrivateFlags:I

    const v1, 0x7fffffff

    and-int/2addr v0, v1

    iput v0, p1, Landroid/view/View;->mPrivateFlags:I

    invoke-virtual {p1}, Landroid/view/View;->updateDisplayListIfDirty()Landroid/graphics/RenderNode;

    iput-boolean v2, p1, Landroid/view/View;->mRecreateDisplayList:Z

    return-void
.end method

.method private updateWebViewOverlayCallbacks()V
    .locals 2

    iget-object v0, p0, Landroid/view/ThreadedRenderer;->mWebViewOverlayProvider:Landroid/view/ThreadedRenderer$WebViewOverlayProvider;

    invoke-virtual {v0}, Landroid/view/ThreadedRenderer$WebViewOverlayProvider;->shouldEnableOverlaySupport()Z

    move-result v0

    iget-boolean v1, p0, Landroid/view/ThreadedRenderer;->mWebViewOverlaysEnabled:Z

    if-eq v0, v1, :cond_1

    iput-boolean v0, p0, Landroid/view/ThreadedRenderer;->mWebViewOverlaysEnabled:Z

    if-eqz v0, :cond_0

    iget-object v1, p0, Landroid/view/ThreadedRenderer;->mWebViewOverlayProvider:Landroid/view/ThreadedRenderer$WebViewOverlayProvider;

    invoke-virtual {p0, v1}, Landroid/view/ThreadedRenderer;->setASurfaceTransactionCallback(Landroid/graphics/HardwareRenderer$ASurfaceTransactionCallback;)V

    iget-object v1, p0, Landroid/view/ThreadedRenderer;->mWebViewOverlayProvider:Landroid/view/ThreadedRenderer$WebViewOverlayProvider;

    invoke-virtual {p0, v1}, Landroid/view/ThreadedRenderer;->setPrepareSurfaceControlForWebviewCallback(Landroid/graphics/HardwareRenderer$PrepareSurfaceControlForWebviewCallback;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/view/ThreadedRenderer;->setASurfaceTransactionCallback(Landroid/graphics/HardwareRenderer$ASurfaceTransactionCallback;)V

    invoke-virtual {p0, v1}, Landroid/view/ThreadedRenderer;->setPrepareSurfaceControlForWebviewCallback(Landroid/graphics/HardwareRenderer$PrepareSurfaceControlForWebviewCallback;)V

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method captureRenderingCommands()Landroid/graphics/Picture;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_0

    nop
.end method

.method public destroy()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/view/ThreadedRenderer;->mInitialized:Z

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/view/ThreadedRenderer;->updateEnabledState(Landroid/view/Surface;)V

    invoke-super {p0}, Landroid/graphics/HardwareRenderer;->destroy()V

    return-void
.end method

.method destroyHardwareResources(Landroid/view/View;)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p0}, Landroid/view/ThreadedRenderer;->clearContent()V

    goto/32 :goto_2

    nop

    :goto_1
    invoke-static {p1}, Landroid/view/ThreadedRenderer;->destroyResources(Landroid/view/View;)V

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method draw(Landroid/view/View;Landroid/view/View$AttachInfo;Landroid/view/ThreadedRenderer$DrawCallbacks;)V
    .locals 4

    goto/32 :goto_26

    nop

    :goto_0
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_3

    nop

    :goto_1
    const-string/jumbo v3, "syncAndDrawFrame"

    goto/32 :goto_28

    nop

    :goto_2
    iget-object v2, p2, Landroid/view/View$AttachInfo;->mPendingAnimatingRenderNodes:Ljava/util/List;

    goto/32 :goto_20

    nop

    :goto_3
    goto/16 :goto_2c

    :goto_4
    goto/32 :goto_17

    nop

    :goto_5
    and-int/lit8 v1, v3, 0x2

    goto/32 :goto_19

    nop

    :goto_6
    invoke-virtual {p0, v0}, Landroid/view/ThreadedRenderer;->syncAndDrawFrame(Landroid/graphics/FrameInfo;)I

    move-result v3

    goto/32 :goto_11

    nop

    :goto_7
    invoke-virtual {p0, v2}, Landroid/view/ThreadedRenderer;->registerAnimatingRenderNode(Landroid/graphics/RenderNode;)V

    goto/32 :goto_0

    nop

    :goto_8
    iget-object v0, p2, Landroid/view/View$AttachInfo;->mViewRootImpl:Landroid/view/ViewRootImpl;

    goto/32 :goto_25

    nop

    :goto_9
    iput-object v1, p2, Landroid/view/View$AttachInfo;->mPendingAnimatingRenderNodes:Ljava/util/List;

    :goto_a
    goto/32 :goto_8

    nop

    :goto_b
    const/4 v2, 0x1

    goto/32 :goto_2a

    nop

    :goto_c
    and-int/lit8 v1, v3, 0x1

    goto/32 :goto_d

    nop

    :goto_d
    if-nez v1, :cond_0

    goto/32 :goto_16

    :cond_0
    goto/32 :goto_1e

    nop

    :goto_e
    iget-object v0, v0, Landroid/view/ViewRootImpl;->mViewFrameInfo:Landroid/view/ViewFrameInfo;

    goto/32 :goto_18

    nop

    :goto_f
    const/4 v1, 0x0

    goto/32 :goto_9

    nop

    :goto_10
    invoke-interface {v1}, Ljava/util/List;->clear()V

    goto/32 :goto_f

    nop

    :goto_11
    invoke-static {v1, v2}, Landroid/os/Trace;->traceEnd(J)V

    goto/32 :goto_5

    nop

    :goto_12
    iget-object v1, p2, Landroid/view/View$AttachInfo;->mViewRootImpl:Landroid/view/ViewRootImpl;

    goto/32 :goto_b

    nop

    :goto_13
    iget-object v0, p2, Landroid/view/View$AttachInfo;->mPendingAnimatingRenderNodes:Ljava/util/List;

    goto/32 :goto_1f

    nop

    :goto_14
    const-string v2, "Surface lost, forcing relayout"

    goto/32 :goto_27

    nop

    :goto_15
    invoke-virtual {v1}, Landroid/view/ViewRootImpl;->invalidate()V

    :goto_16
    goto/32 :goto_21

    nop

    :goto_17
    iget-object v1, p2, Landroid/view/View$AttachInfo;->mPendingAnimatingRenderNodes:Ljava/util/List;

    goto/32 :goto_10

    nop

    :goto_18
    invoke-virtual {v0}, Landroid/view/ViewFrameInfo;->markDrawStart()V

    goto/32 :goto_2e

    nop

    :goto_19
    if-nez v1, :cond_1

    goto/32 :goto_24

    :cond_1
    goto/32 :goto_29

    nop

    :goto_1a
    iget-object v1, p2, Landroid/view/View$AttachInfo;->mViewRootImpl:Landroid/view/ViewRootImpl;

    goto/32 :goto_23

    nop

    :goto_1b
    const-wide/16 v1, 0x8

    goto/32 :goto_1

    nop

    :goto_1c
    if-lt v1, v0, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_2

    nop

    :goto_1d
    check-cast v2, Landroid/graphics/RenderNode;

    goto/32 :goto_7

    nop

    :goto_1e
    iget-object v1, p2, Landroid/view/View$AttachInfo;->mViewRootImpl:Landroid/view/ViewRootImpl;

    goto/32 :goto_15

    nop

    :goto_1f
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto/32 :goto_2b

    nop

    :goto_20
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_1d

    nop

    :goto_21
    return-void

    :goto_22
    if-nez v0, :cond_3

    goto/32 :goto_a

    :cond_3
    goto/32 :goto_13

    nop

    :goto_23
    invoke-virtual {v1}, Landroid/view/ViewRootImpl;->requestLayout()V

    :goto_24
    goto/32 :goto_c

    nop

    :goto_25
    invoke-virtual {v0}, Landroid/view/ViewRootImpl;->getUpdatedFrameInfo()Landroid/graphics/FrameInfo;

    move-result-object v0

    goto/32 :goto_1b

    nop

    :goto_26
    iget-object v0, p2, Landroid/view/View$AttachInfo;->mViewRootImpl:Landroid/view/ViewRootImpl;

    goto/32 :goto_e

    nop

    :goto_27
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_12

    nop

    :goto_28
    invoke-static {v1, v2, v3}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    goto/32 :goto_6

    nop

    :goto_29
    const-string v1, "OpenGLRenderer"

    goto/32 :goto_14

    nop

    :goto_2a
    iput-boolean v2, v1, Landroid/view/ViewRootImpl;->mForceNextWindowRelayout:Z

    goto/32 :goto_1a

    nop

    :goto_2b
    const/4 v1, 0x0

    :goto_2c
    goto/32 :goto_1c

    nop

    :goto_2d
    iget-object v0, p2, Landroid/view/View$AttachInfo;->mPendingAnimatingRenderNodes:Ljava/util/List;

    goto/32 :goto_22

    nop

    :goto_2e
    invoke-direct {p0, p1, p3}, Landroid/view/ThreadedRenderer;->updateRootDisplayList(Landroid/view/View;Landroid/view/ThreadedRenderer$DrawCallbacks;)V

    goto/32 :goto_2d

    nop
.end method

.method dumpGfxInfo(Ljava/io/PrintWriter;Ljava/io/FileDescriptor;[Ljava/lang/String;)V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    invoke-static {p3}, Landroid/view/ThreadedRenderer;->dumpArgsToFlags([Ljava/lang/String;)I

    move-result v0

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {p0, p2, v0}, Landroid/view/ThreadedRenderer;->dumpProfileInfo(Ljava/io/FileDescriptor;I)V

    goto/32 :goto_2

    nop

    :goto_2
    return-void

    :goto_3
    invoke-virtual {p1}, Ljava/io/PrintWriter;->flush()V

    goto/32 :goto_0

    nop
.end method

.method getHeight()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p0, Landroid/view/ThreadedRenderer;->mHeight:I

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method public getRootNode()Landroid/graphics/RenderNode;
    .locals 1

    iget-object v0, p0, Landroid/view/ThreadedRenderer;->mRootNode:Landroid/graphics/RenderNode;

    return-object v0
.end method

.method getWidth()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p0, Landroid/view/ThreadedRenderer;->mWidth:I

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method initialize(Landroid/view/Surface;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/view/Surface$OutOfResourcesException;
        }
    .end annotation

    goto/32 :goto_5

    nop

    :goto_0
    iput-boolean v1, p0, Landroid/view/ThreadedRenderer;->mInitialized:Z

    goto/32 :goto_4

    nop

    :goto_1
    xor-int/2addr v0, v1

    goto/32 :goto_0

    nop

    :goto_2
    const/4 v1, 0x1

    goto/32 :goto_1

    nop

    :goto_3
    invoke-virtual {p0, p1}, Landroid/view/ThreadedRenderer;->setSurface(Landroid/view/Surface;)V

    goto/32 :goto_6

    nop

    :goto_4
    invoke-direct {p0, p1}, Landroid/view/ThreadedRenderer;->updateEnabledState(Landroid/view/Surface;)V

    goto/32 :goto_3

    nop

    :goto_5
    iget-boolean v0, p0, Landroid/view/ThreadedRenderer;->mInitialized:Z

    goto/32 :goto_2

    nop

    :goto_6
    return v0
.end method

.method initializeIfNeeded(IILandroid/view/View$AttachInfo;Landroid/view/Surface;Landroid/graphics/Rect;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/view/Surface$OutOfResourcesException;
        }
    .end annotation

    goto/32 :goto_6

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_5

    nop

    :goto_1
    const/4 v0, 0x1

    goto/32 :goto_7

    nop

    :goto_2
    invoke-virtual {p0, p1, p2, p3, p5}, Landroid/view/ThreadedRenderer;->setup(IILandroid/view/View$AttachInfo;Landroid/graphics/Rect;)V

    goto/32 :goto_1

    nop

    :goto_3
    invoke-virtual {p0, p4}, Landroid/view/ThreadedRenderer;->initialize(Landroid/view/Surface;)Z

    move-result v0

    goto/32 :goto_9

    nop

    :goto_4
    return v0

    :goto_5
    invoke-virtual {p0}, Landroid/view/ThreadedRenderer;->isEnabled()Z

    move-result v0

    goto/32 :goto_b

    nop

    :goto_6
    invoke-virtual {p0}, Landroid/view/ThreadedRenderer;->isRequested()Z

    move-result v0

    goto/32 :goto_0

    nop

    :goto_7
    return v0

    :goto_8
    goto/32 :goto_a

    nop

    :goto_9
    if-nez v0, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_2

    nop

    :goto_a
    const/4 v0, 0x0

    goto/32 :goto_4

    nop

    :goto_b
    if-eqz v0, :cond_2

    goto/32 :goto_8

    :cond_2
    goto/32 :goto_3

    nop
.end method

.method invalidateRoot()V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    iput-boolean v0, p0, Landroid/view/ThreadedRenderer;->mRootNodeNeedsUpdate:Z

    goto/32 :goto_0

    nop

    :goto_2
    const/4 v0, 0x1

    goto/32 :goto_1

    nop
.end method

.method isEnabled()Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-boolean v0, p0, Landroid/view/ThreadedRenderer;->mEnabled:Z

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method isRequested()Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-boolean v0, p0, Landroid/view/ThreadedRenderer;->mRequested:Z

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method public loadSystemProperties()Z
    .locals 1

    invoke-super {p0}, Landroid/graphics/HardwareRenderer;->loadSystemProperties()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/ThreadedRenderer;->invalidateRoot()V

    :cond_0
    return v0
.end method

.method registerRtFrameCallback(Landroid/graphics/HardwareRenderer$FrameDrawingCallback;)V
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Landroid/view/ThreadedRenderer;->mNextRtFrameCallbacks:Ljava/util/ArrayList;

    goto/32 :goto_3

    nop

    :goto_2
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_5

    nop

    :goto_3
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_7

    nop

    :goto_4
    iget-object v0, p0, Landroid/view/ThreadedRenderer;->mNextRtFrameCallbacks:Ljava/util/ArrayList;

    goto/32 :goto_8

    nop

    :goto_5
    iput-object v0, p0, Landroid/view/ThreadedRenderer;->mNextRtFrameCallbacks:Ljava/util/ArrayList;

    :goto_6
    goto/32 :goto_1

    nop

    :goto_7
    return-void

    :goto_8
    if-eqz v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_0

    nop
.end method

.method public rendererOwnsSurfaceControlOpacity()Z
    .locals 1

    iget-object v0, p0, Landroid/view/ThreadedRenderer;->mWebViewOverlayProvider:Landroid/view/ThreadedRenderer$WebViewOverlayProvider;

    invoke-static {v0}, Landroid/view/ThreadedRenderer$WebViewOverlayProvider;->-$$Nest$fgetmSurfaceControl(Landroid/view/ThreadedRenderer$WebViewOverlayProvider;)Landroid/view/SurfaceControl;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setBlastBufferQueue(Landroid/graphics/BLASTBufferQueue;)V
    .locals 1

    iget-object v0, p0, Landroid/view/ThreadedRenderer;->mWebViewOverlayProvider:Landroid/view/ThreadedRenderer$WebViewOverlayProvider;

    invoke-virtual {v0, p1}, Landroid/view/ThreadedRenderer$WebViewOverlayProvider;->setBLASTBufferQueue(Landroid/graphics/BLASTBufferQueue;)V

    invoke-direct {p0}, Landroid/view/ThreadedRenderer;->updateWebViewOverlayCallbacks()V

    return-void
.end method

.method setEnabled(Z)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iput-boolean p1, p0, Landroid/view/ThreadedRenderer;->mEnabled:Z

    goto/32 :goto_0

    nop
.end method

.method setLightCenter(Landroid/view/View$AttachInfo;)V
    .locals 5

    goto/32 :goto_1

    nop

    :goto_0
    iget v3, p0, Landroid/view/ThreadedRenderer;->mLightZ:F

    goto/32 :goto_e

    nop

    :goto_1
    iget-object v0, p1, Landroid/view/View$AttachInfo;->mPoint:Landroid/graphics/Point;

    goto/32 :goto_c

    nop

    :goto_2
    int-to-float v1, v1

    goto/32 :goto_d

    nop

    :goto_3
    invoke-virtual {v1, v0}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    goto/32 :goto_6

    nop

    :goto_4
    int-to-float v3, v3

    goto/32 :goto_10

    nop

    :goto_5
    iget v2, p0, Landroid/view/ThreadedRenderer;->mLightY:F

    goto/32 :goto_a

    nop

    :goto_6
    iget v1, v0, Landroid/graphics/Point;->x:I

    goto/32 :goto_2

    nop

    :goto_7
    return-void

    :goto_8
    int-to-float v2, v2

    goto/32 :goto_f

    nop

    :goto_9
    iget v2, p1, Landroid/view/View$AttachInfo;->mWindowLeft:I

    goto/32 :goto_8

    nop

    :goto_a
    iget v3, p1, Landroid/view/View$AttachInfo;->mWindowTop:I

    goto/32 :goto_4

    nop

    :goto_b
    div-float/2addr v1, v2

    goto/32 :goto_9

    nop

    :goto_c
    iget-object v1, p1, Landroid/view/View$AttachInfo;->mDisplay:Landroid/view/Display;

    goto/32 :goto_3

    nop

    :goto_d
    const/high16 v2, 0x40000000    # 2.0f

    goto/32 :goto_b

    nop

    :goto_e
    iget v4, p0, Landroid/view/ThreadedRenderer;->mLightRadius:F

    goto/32 :goto_11

    nop

    :goto_f
    sub-float/2addr v1, v2

    goto/32 :goto_5

    nop

    :goto_10
    sub-float/2addr v2, v3

    goto/32 :goto_0

    nop

    :goto_11
    invoke-virtual {p0, v1, v2, v3, v4}, Landroid/view/ThreadedRenderer;->setLightSourceGeometry(FFFF)V

    goto/32 :goto_7

    nop
.end method

.method setRequested(Z)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iput-boolean p1, p0, Landroid/view/ThreadedRenderer;->mRequested:Z

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method public setSurface(Landroid/view/Surface;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/Surface;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/graphics/HardwareRenderer;->setSurface(Landroid/view/Surface;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/graphics/HardwareRenderer;->setSurface(Landroid/view/Surface;)V

    :goto_0
    return-void
.end method

.method public setSurfaceControl(Landroid/view/SurfaceControl;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/graphics/HardwareRenderer;->setSurfaceControl(Landroid/view/SurfaceControl;)V

    iget-object v0, p0, Landroid/view/ThreadedRenderer;->mWebViewOverlayProvider:Landroid/view/ThreadedRenderer$WebViewOverlayProvider;

    invoke-virtual {v0, p1}, Landroid/view/ThreadedRenderer$WebViewOverlayProvider;->setSurfaceControl(Landroid/view/SurfaceControl;)V

    invoke-direct {p0}, Landroid/view/ThreadedRenderer;->updateWebViewOverlayCallbacks()V

    return-void
.end method

.method public setSurfaceControlOpaque(Z)Z
    .locals 1

    iget-object v0, p0, Landroid/view/ThreadedRenderer;->mWebViewOverlayProvider:Landroid/view/ThreadedRenderer$WebViewOverlayProvider;

    invoke-virtual {v0, p1}, Landroid/view/ThreadedRenderer$WebViewOverlayProvider;->setSurfaceControlOpaque(Z)Z

    move-result v0

    return v0
.end method

.method setup(IILandroid/view/View$AttachInfo;Landroid/graphics/Rect;)V
    .locals 5

    goto/32 :goto_16

    nop

    :goto_0
    iput p2, p0, Landroid/view/ThreadedRenderer;->mSurfaceHeight:I

    :goto_1
    goto/32 :goto_14

    nop

    :goto_2
    iput v1, p0, Landroid/view/ThreadedRenderer;->mSurfaceHeight:I

    goto/32 :goto_18

    nop

    :goto_3
    add-int/2addr v1, p1

    goto/32 :goto_7

    nop

    :goto_4
    iput v0, p0, Landroid/view/ThreadedRenderer;->mInsetTop:I

    goto/32 :goto_f

    nop

    :goto_5
    if-eqz v1, :cond_0

    goto/32 :goto_2a

    :cond_0
    goto/32 :goto_2b

    nop

    :goto_6
    invoke-virtual {p0, p3}, Landroid/view/ThreadedRenderer;->setLightCenter(Landroid/view/View$AttachInfo;)V

    goto/32 :goto_13

    nop

    :goto_7
    iget v2, p4, Landroid/graphics/Rect;->right:I

    goto/32 :goto_17

    nop

    :goto_8
    add-int/2addr v1, v2

    goto/32 :goto_2

    nop

    :goto_9
    if-eqz v1, :cond_1

    goto/32 :goto_2a

    :cond_1
    goto/32 :goto_d

    nop

    :goto_a
    iget v1, p4, Landroid/graphics/Rect;->left:I

    goto/32 :goto_5

    nop

    :goto_b
    if-nez p4, :cond_2

    goto/32 :goto_20

    :cond_2
    goto/32 :goto_a

    nop

    :goto_c
    iget v3, p0, Landroid/view/ThreadedRenderer;->mSurfaceWidth:I

    goto/32 :goto_2c

    nop

    :goto_d
    iget v1, p4, Landroid/graphics/Rect;->top:I

    goto/32 :goto_23

    nop

    :goto_e
    iget v1, p0, Landroid/view/ThreadedRenderer;->mInsetLeft:I

    goto/32 :goto_3

    nop

    :goto_f
    iput p1, p0, Landroid/view/ThreadedRenderer;->mSurfaceWidth:I

    goto/32 :goto_0

    nop

    :goto_10
    iget v1, p4, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_29

    nop

    :goto_11
    iget v2, p0, Landroid/view/ThreadedRenderer;->mInsetTop:I

    goto/32 :goto_26

    nop

    :goto_12
    iput v0, p0, Landroid/view/ThreadedRenderer;->mInsetLeft:I

    goto/32 :goto_4

    nop

    :goto_13
    return-void

    :goto_14
    iget-object v0, p0, Landroid/view/ThreadedRenderer;->mRootNode:Landroid/graphics/RenderNode;

    goto/32 :goto_22

    nop

    :goto_15
    iput p2, p0, Landroid/view/ThreadedRenderer;->mHeight:I

    goto/32 :goto_1b

    nop

    :goto_16
    iput p1, p0, Landroid/view/ThreadedRenderer;->mWidth:I

    goto/32 :goto_15

    nop

    :goto_17
    add-int/2addr v1, v2

    goto/32 :goto_27

    nop

    :goto_18
    invoke-virtual {p0, v0}, Landroid/view/ThreadedRenderer;->setOpaque(Z)V

    goto/32 :goto_1f

    nop

    :goto_19
    iget v1, p0, Landroid/view/ThreadedRenderer;->mInsetTop:I

    goto/32 :goto_1c

    nop

    :goto_1a
    iget v1, p4, Landroid/graphics/Rect;->left:I

    goto/32 :goto_1e

    nop

    :goto_1b
    const/4 v0, 0x0

    goto/32 :goto_b

    nop

    :goto_1c
    add-int/2addr v1, p2

    goto/32 :goto_28

    nop

    :goto_1d
    iput v1, p0, Landroid/view/ThreadedRenderer;->mInsetTop:I

    goto/32 :goto_e

    nop

    :goto_1e
    iput v1, p0, Landroid/view/ThreadedRenderer;->mInsetLeft:I

    goto/32 :goto_21

    nop

    :goto_1f
    goto/16 :goto_1

    :goto_20
    goto/32 :goto_12

    nop

    :goto_21
    iget v1, p4, Landroid/graphics/Rect;->top:I

    goto/32 :goto_1d

    nop

    :goto_22
    iget v1, p0, Landroid/view/ThreadedRenderer;->mInsetLeft:I

    goto/32 :goto_25

    nop

    :goto_23
    if-eqz v1, :cond_3

    goto/32 :goto_2a

    :cond_3
    goto/32 :goto_10

    nop

    :goto_24
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RenderNode;->setLeftTopRightBottom(IIII)Z

    goto/32 :goto_6

    nop

    :goto_25
    neg-int v1, v1

    goto/32 :goto_11

    nop

    :goto_26
    neg-int v2, v2

    goto/32 :goto_c

    nop

    :goto_27
    iput v1, p0, Landroid/view/ThreadedRenderer;->mSurfaceWidth:I

    goto/32 :goto_19

    nop

    :goto_28
    iget v2, p4, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_8

    nop

    :goto_29
    if-nez v1, :cond_4

    goto/32 :goto_20

    :cond_4
    :goto_2a
    goto/32 :goto_1a

    nop

    :goto_2b
    iget v1, p4, Landroid/graphics/Rect;->right:I

    goto/32 :goto_9

    nop

    :goto_2c
    iget v4, p0, Landroid/view/ThreadedRenderer;->mSurfaceHeight:I

    goto/32 :goto_24

    nop
.end method

.method unregisterRtFrameCallback(Landroid/graphics/HardwareRenderer$FrameDrawingCallback;)V
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    if-eqz v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_2

    nop

    :goto_1
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/32 :goto_5

    nop

    :goto_2
    return-void

    :goto_3
    goto/32 :goto_1

    nop

    :goto_4
    iget-object v0, p0, Landroid/view/ThreadedRenderer;->mNextRtFrameCallbacks:Ljava/util/ArrayList;

    goto/32 :goto_0

    nop

    :goto_5
    return-void
.end method

.method updateSurface(Landroid/view/Surface;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/view/Surface$OutOfResourcesException;
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-direct {p0, p1}, Landroid/view/ThreadedRenderer;->updateEnabledState(Landroid/view/Surface;)V

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {p0, p1}, Landroid/view/ThreadedRenderer;->setSurface(Landroid/view/Surface;)V

    goto/32 :goto_0

    nop
.end method
