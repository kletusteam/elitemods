.class final Landroid/view/ViewRootImpl$TrackballAxis;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/ViewRootImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "TrackballAxis"
.end annotation


# static fields
.field static final ACCEL_MOVE_SCALING_FACTOR:F = 0.025f

.field static final FAST_MOVE_TIME:J = 0x96L

.field static final FIRST_MOVEMENT_THRESHOLD:F = 0.5f

.field static final MAX_ACCELERATION:F = 20.0f

.field static final SECOND_CUMULATIVE_MOVEMENT_THRESHOLD:F = 2.0f

.field static final SUBSEQUENT_INCREMENTAL_MOVEMENT_THRESHOLD:F = 1.0f


# instance fields
.field acceleration:F

.field dir:I

.field lastMoveTime:J

.field nonAccelMovement:I

.field position:F

.field step:I


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Landroid/view/ViewRootImpl$TrackballAxis;->acceleration:F

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Landroid/view/ViewRootImpl$TrackballAxis;->lastMoveTime:J

    return-void
.end method


# virtual methods
.method collect(FJLjava/lang/String;)F
    .locals 11

    goto/32 :goto_2

    nop

    :goto_0
    float-to-long v7, v1

    goto/32 :goto_3b

    nop

    :goto_1
    const/high16 v6, 0x3f800000    # 1.0f

    goto/32 :goto_3e

    nop

    :goto_2
    const/4 v0, 0x0

    goto/32 :goto_b

    nop

    :goto_3
    if-gtz v4, :cond_0

    goto/32 :goto_3a

    :cond_0
    goto/32 :goto_39

    nop

    :goto_4
    if-gtz v0, :cond_1

    goto/32 :goto_48

    :cond_1
    goto/32 :goto_a

    nop

    :goto_5
    cmp-long v3, v0, v7

    goto/32 :goto_40

    nop

    :goto_6
    if-gtz v4, :cond_2

    goto/32 :goto_30

    :cond_2
    goto/32 :goto_2f

    nop

    :goto_7
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    goto/32 :goto_11

    nop

    :goto_8
    cmp-long v0, v7, v4

    goto/32 :goto_4

    nop

    :goto_9
    add-float/2addr v0, p1

    goto/32 :goto_3f

    nop

    :goto_a
    iget-wide v0, p0, Landroid/view/ViewRootImpl$TrackballAxis;->lastMoveTime:J

    goto/32 :goto_4b

    nop

    :goto_b
    cmpl-float v1, p1, v0

    goto/32 :goto_31

    nop

    :goto_c
    iput v6, p0, Landroid/view/ViewRootImpl$TrackballAxis;->acceleration:F

    goto/32 :goto_16

    nop

    :goto_d
    iget v2, p0, Landroid/view/ViewRootImpl$TrackballAxis;->acceleration:F

    goto/32 :goto_5

    nop

    :goto_e
    if-ltz v3, :cond_3

    goto/32 :goto_1e

    :cond_3
    goto/32 :goto_36

    nop

    :goto_f
    mul-float/2addr v3, p1

    goto/32 :goto_23

    nop

    :goto_10
    iput v2, p0, Landroid/view/ViewRootImpl$TrackballAxis;->step:I

    goto/32 :goto_c

    nop

    :goto_11
    return v0

    :goto_12
    if-ltz v1, :cond_4

    goto/32 :goto_1b

    :cond_4
    goto/32 :goto_46

    nop

    :goto_13
    iput v0, p0, Landroid/view/ViewRootImpl$TrackballAxis;->position:F

    goto/32 :goto_10

    nop

    :goto_14
    mul-float/2addr v1, v3

    goto/32 :goto_0

    nop

    :goto_15
    neg-float v1, p1

    goto/32 :goto_14

    nop

    :goto_16
    iput-wide v4, p0, Landroid/view/ViewRootImpl$TrackballAxis;->lastMoveTime:J

    :goto_17
    goto/32 :goto_4f

    nop

    :goto_18
    const/high16 v4, 0x41a00000    # 20.0f

    goto/32 :goto_2e

    nop

    :goto_19
    if-ltz v5, :cond_5

    goto/32 :goto_27

    :cond_5
    goto/32 :goto_26

    nop

    :goto_1a
    iput-wide v4, p0, Landroid/view/ViewRootImpl$TrackballAxis;->lastMoveTime:J

    :goto_1b
    goto/32 :goto_2c

    nop

    :goto_1c
    sub-long v9, v0, v7

    goto/32 :goto_24

    nop

    :goto_1d
    goto/16 :goto_48

    :goto_1e
    goto/32 :goto_1c

    nop

    :goto_1f
    goto :goto_34

    :goto_20
    goto/32 :goto_2b

    nop

    :goto_21
    iget v1, p0, Landroid/view/ViewRootImpl$TrackballAxis;->dir:I

    goto/32 :goto_12

    nop

    :goto_22
    mul-float/2addr v3, v4

    goto/32 :goto_43

    nop

    :goto_23
    float-to-long v7, v3

    goto/32 :goto_21

    nop

    :goto_24
    long-to-float v3, v9

    goto/32 :goto_22

    nop

    :goto_25
    if-ltz v1, :cond_6

    goto/32 :goto_42

    :cond_6
    goto/32 :goto_15

    nop

    :goto_26
    move v4, v2

    :goto_27
    goto/32 :goto_32

    nop

    :goto_28
    iget v0, p0, Landroid/view/ViewRootImpl$TrackballAxis;->position:F

    goto/32 :goto_9

    nop

    :goto_29
    mul-float/2addr v2, v3

    :goto_2a
    goto/32 :goto_18

    nop

    :goto_2b
    cmpg-float v1, p1, v0

    goto/32 :goto_25

    nop

    :goto_2c
    const/4 v0, 0x1

    goto/32 :goto_2d

    nop

    :goto_2d
    iput v0, p0, Landroid/view/ViewRootImpl$TrackballAxis;->dir:I

    goto/32 :goto_1f

    nop

    :goto_2e
    cmpg-float v5, v2, v4

    goto/32 :goto_19

    nop

    :goto_2f
    move v6, v2

    :goto_30
    goto/32 :goto_47

    nop

    :goto_31
    const/4 v2, 0x0

    goto/32 :goto_35

    nop

    :goto_32
    iput v4, p0, Landroid/view/ViewRootImpl$TrackballAxis;->acceleration:F

    goto/32 :goto_1d

    nop

    :goto_33
    const-wide/16 v7, 0x0

    :goto_34
    goto/32 :goto_8

    nop

    :goto_35
    const/high16 v3, 0x43160000    # 150.0f

    goto/32 :goto_4d

    nop

    :goto_36
    sub-long v9, v7, v0

    goto/32 :goto_4a

    nop

    :goto_37
    cmpl-float v4, v2, v6

    goto/32 :goto_6

    nop

    :goto_38
    iput v0, p0, Landroid/view/ViewRootImpl$TrackballAxis;->dir:I

    goto/32 :goto_41

    nop

    :goto_39
    div-float/2addr v2, v3

    :goto_3a
    goto/32 :goto_37

    nop

    :goto_3b
    iget v1, p0, Landroid/view/ViewRootImpl$TrackballAxis;->dir:I

    goto/32 :goto_49

    nop

    :goto_3c
    if-gtz v4, :cond_7

    goto/32 :goto_2a

    :cond_7
    goto/32 :goto_29

    nop

    :goto_3d
    iput-wide p2, p0, Landroid/view/ViewRootImpl$TrackballAxis;->lastMoveTime:J

    goto/32 :goto_d

    nop

    :goto_3e
    if-gtz v1, :cond_8

    goto/32 :goto_20

    :cond_8
    goto/32 :goto_f

    nop

    :goto_3f
    iput v0, p0, Landroid/view/ViewRootImpl$TrackballAxis;->position:F

    goto/32 :goto_7

    nop

    :goto_40
    const v4, 0x3ccccccd    # 0.025f

    goto/32 :goto_e

    nop

    :goto_41
    goto :goto_34

    :goto_42
    goto/32 :goto_33

    nop

    :goto_43
    cmpl-float v4, v3, v6

    goto/32 :goto_3

    nop

    :goto_44
    mul-float/2addr v3, v4

    goto/32 :goto_4c

    nop

    :goto_45
    iput v6, p0, Landroid/view/ViewRootImpl$TrackballAxis;->acceleration:F

    goto/32 :goto_1a

    nop

    :goto_46
    iput v0, p0, Landroid/view/ViewRootImpl$TrackballAxis;->position:F

    goto/32 :goto_4e

    nop

    :goto_47
    iput v6, p0, Landroid/view/ViewRootImpl$TrackballAxis;->acceleration:F

    :goto_48
    goto/32 :goto_28

    nop

    :goto_49
    if-gtz v1, :cond_9

    goto/32 :goto_17

    :cond_9
    goto/32 :goto_13

    nop

    :goto_4a
    long-to-float v3, v9

    goto/32 :goto_44

    nop

    :goto_4b
    sub-long v0, p2, v0

    goto/32 :goto_3d

    nop

    :goto_4c
    cmpl-float v4, v3, v6

    goto/32 :goto_3c

    nop

    :goto_4d
    const-wide/16 v4, 0x0

    goto/32 :goto_1

    nop

    :goto_4e
    iput v2, p0, Landroid/view/ViewRootImpl$TrackballAxis;->step:I

    goto/32 :goto_45

    nop

    :goto_4f
    const/4 v0, -0x1

    goto/32 :goto_38

    nop
.end method

.method generate()I
    .locals 5

    goto/32 :goto_15

    nop

    :goto_0
    sub-float/2addr v1, v4

    goto/32 :goto_2b

    nop

    :goto_1
    iput v1, p0, Landroid/view/ViewRootImpl$TrackballAxis;->nonAccelMovement:I

    goto/32 :goto_2a

    nop

    :goto_2
    const/4 v3, 0x1

    goto/32 :goto_2d

    nop

    :goto_3
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    goto/32 :goto_36

    nop

    :goto_4
    iget v1, p0, Landroid/view/ViewRootImpl$TrackballAxis;->position:F

    goto/32 :goto_7

    nop

    :goto_5
    mul-float/2addr v1, v3

    goto/32 :goto_30

    nop

    :goto_6
    const/4 v1, 0x2

    goto/32 :goto_20

    nop

    :goto_7
    int-to-float v4, v2

    goto/32 :goto_a

    nop

    :goto_8
    goto/16 :goto_41

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :goto_9
    goto/16 :goto_32

    :pswitch_0
    goto/32 :goto_24

    nop

    :goto_a
    mul-float/2addr v4, v3

    goto/32 :goto_1b

    nop

    :goto_b
    move v2, v3

    goto/32 :goto_2e

    nop

    :goto_c
    cmpg-float v3, v1, v3

    goto/32 :goto_25

    nop

    :goto_d
    cmpg-float v1, v1, v3

    goto/32 :goto_1d

    nop

    :goto_e
    iget v1, p0, Landroid/view/ViewRootImpl$TrackballAxis;->nonAccelMovement:I

    goto/32 :goto_3b

    nop

    :goto_f
    mul-float/2addr v4, v3

    goto/32 :goto_0

    nop

    :goto_10
    iget v3, p0, Landroid/view/ViewRootImpl$TrackballAxis;->acceleration:F

    :goto_11
    goto/32 :goto_31

    nop

    :goto_12
    iput v1, p0, Landroid/view/ViewRootImpl$TrackballAxis;->nonAccelMovement:I

    goto/32 :goto_4

    nop

    :goto_13
    goto :goto_11

    :goto_14
    goto/32 :goto_10

    nop

    :goto_15
    const/4 v0, 0x0

    goto/32 :goto_3c

    nop

    :goto_16
    add-int/2addr v0, v2

    goto/32 :goto_17

    nop

    :goto_17
    iget v1, p0, Landroid/view/ViewRootImpl$TrackballAxis;->position:F

    goto/32 :goto_1c

    nop

    :goto_18
    return v0

    :goto_19
    goto/32 :goto_26

    nop

    :goto_1a
    const v3, 0x3f8ccccd    # 1.1f

    goto/32 :goto_5

    nop

    :goto_1b
    sub-float/2addr v1, v4

    goto/32 :goto_35

    nop

    :goto_1c
    int-to-float v4, v2

    goto/32 :goto_f

    nop

    :goto_1d
    if-ltz v1, :cond_0

    goto/32 :goto_3e

    :cond_0
    goto/32 :goto_3d

    nop

    :goto_1e
    cmpg-float v1, v1, v4

    goto/32 :goto_45

    nop

    :goto_1f
    add-int/2addr v0, v2

    goto/32 :goto_e

    nop

    :goto_20
    iput v1, p0, Landroid/view/ViewRootImpl$TrackballAxis;->step:I

    goto/32 :goto_9

    nop

    :goto_21
    iget v4, p0, Landroid/view/ViewRootImpl$TrackballAxis;->step:I

    packed-switch v4, :pswitch_data_0

    goto/32 :goto_42

    nop

    :goto_22
    cmpg-float v1, v1, v3

    goto/32 :goto_23

    nop

    :goto_23
    if-ltz v1, :cond_1

    goto/32 :goto_29

    :cond_1
    goto/32 :goto_27

    nop

    :goto_24
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    goto/32 :goto_43

    nop

    :goto_25
    if-ltz v3, :cond_2

    goto/32 :goto_14

    :cond_2
    goto/32 :goto_3f

    nop

    :goto_26
    add-int/2addr v0, v2

    goto/32 :goto_3a

    nop

    :goto_27
    return v0

    :pswitch_1
    goto/32 :goto_3

    nop

    :goto_28
    goto :goto_32

    :goto_29
    goto/32 :goto_16

    nop

    :goto_2a
    iput v3, p0, Landroid/view/ViewRootImpl$TrackballAxis;->step:I

    goto/32 :goto_28

    nop

    :goto_2b
    iput v1, p0, Landroid/view/ViewRootImpl$TrackballAxis;->position:F

    goto/32 :goto_2c

    nop

    :goto_2c
    iget v1, p0, Landroid/view/ViewRootImpl$TrackballAxis;->acceleration:F

    goto/32 :goto_1a

    nop

    :goto_2d
    if-gez v2, :cond_3

    goto/32 :goto_2f

    :cond_3
    goto/32 :goto_b

    nop

    :goto_2e
    goto :goto_34

    :goto_2f
    goto/32 :goto_33

    nop

    :goto_30
    const/high16 v3, 0x41a00000    # 20.0f

    goto/32 :goto_c

    nop

    :goto_31
    iput v3, p0, Landroid/view/ViewRootImpl$TrackballAxis;->acceleration:F

    :goto_32
    goto/32 :goto_8

    nop

    :goto_33
    const/4 v2, -0x1

    :goto_34
    goto/32 :goto_21

    nop

    :goto_35
    iput v1, p0, Landroid/view/ViewRootImpl$TrackballAxis;->position:F

    goto/32 :goto_6

    nop

    :goto_36
    const/high16 v3, 0x40000000    # 2.0f

    goto/32 :goto_d

    nop

    :goto_37
    const/4 v2, 0x0

    goto/32 :goto_38

    nop

    :goto_38
    cmpl-float v2, v1, v2

    goto/32 :goto_2

    nop

    :goto_39
    add-int/2addr v1, v2

    goto/32 :goto_1

    nop

    :goto_3a
    iget v1, p0, Landroid/view/ViewRootImpl$TrackballAxis;->nonAccelMovement:I

    goto/32 :goto_39

    nop

    :goto_3b
    add-int/2addr v1, v2

    goto/32 :goto_12

    nop

    :goto_3c
    const/4 v1, 0x0

    goto/32 :goto_40

    nop

    :goto_3d
    return v0

    :goto_3e
    goto/32 :goto_1f

    nop

    :goto_3f
    move v3, v1

    goto/32 :goto_13

    nop

    :goto_40
    iput v1, p0, Landroid/view/ViewRootImpl$TrackballAxis;->nonAccelMovement:I

    :goto_41
    goto/32 :goto_46

    nop

    :goto_42
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    goto/32 :goto_44

    nop

    :goto_43
    const/high16 v4, 0x3f000000    # 0.5f

    goto/32 :goto_1e

    nop

    :goto_44
    const/high16 v3, 0x3f800000    # 1.0f

    goto/32 :goto_22

    nop

    :goto_45
    if-ltz v1, :cond_4

    goto/32 :goto_19

    :cond_4
    goto/32 :goto_18

    nop

    :goto_46
    iget v1, p0, Landroid/view/ViewRootImpl$TrackballAxis;->position:F

    goto/32 :goto_37

    nop
.end method

.method reset(I)V
    .locals 2

    goto/32 :goto_7

    nop

    :goto_0
    iput v0, p0, Landroid/view/ViewRootImpl$TrackballAxis;->position:F

    goto/32 :goto_3

    nop

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_5

    nop

    :goto_2
    iput v0, p0, Landroid/view/ViewRootImpl$TrackballAxis;->acceleration:F

    goto/32 :goto_4

    nop

    :goto_3
    const/high16 v0, 0x3f800000    # 1.0f

    goto/32 :goto_2

    nop

    :goto_4
    const-wide/16 v0, 0x0

    goto/32 :goto_9

    nop

    :goto_5
    iput v0, p0, Landroid/view/ViewRootImpl$TrackballAxis;->dir:I

    goto/32 :goto_6

    nop

    :goto_6
    return-void

    :goto_7
    const/4 v0, 0x0

    goto/32 :goto_0

    nop

    :goto_8
    iput p1, p0, Landroid/view/ViewRootImpl$TrackballAxis;->step:I

    goto/32 :goto_1

    nop

    :goto_9
    iput-wide v0, p0, Landroid/view/ViewRootImpl$TrackballAxis;->lastMoveTime:J

    goto/32 :goto_8

    nop
.end method
