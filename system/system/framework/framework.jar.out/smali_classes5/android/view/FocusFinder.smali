.class public Landroid/view/FocusFinder;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/FocusFinder$UserSpecifiedFocusComparator;,
        Landroid/view/FocusFinder$FocusSorter;
    }
.end annotation


# static fields
.field private static final tlFocusFinder:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Landroid/view/FocusFinder;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final mBestCandidateRect:Landroid/graphics/Rect;

.field private final mFocusSorter:Landroid/view/FocusFinder$FocusSorter;

.field final mFocusedRect:Landroid/graphics/Rect;

.field final mOtherRect:Landroid/graphics/Rect;

.field private final mTempList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final mUserSpecifiedClusterComparator:Landroid/view/FocusFinder$UserSpecifiedFocusComparator;

.field private final mUserSpecifiedFocusComparator:Landroid/view/FocusFinder$UserSpecifiedFocusComparator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/view/FocusFinder$1;

    invoke-direct {v0}, Landroid/view/FocusFinder$1;-><init>()V

    sput-object v0, Landroid/view/FocusFinder;->tlFocusFinder:Ljava/lang/ThreadLocal;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/view/FocusFinder;->mFocusedRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/view/FocusFinder;->mOtherRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/view/FocusFinder;->mBestCandidateRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/view/FocusFinder$UserSpecifiedFocusComparator;

    new-instance v1, Landroid/view/FocusFinder$$ExternalSyntheticLambda0;

    invoke-direct {v1}, Landroid/view/FocusFinder$$ExternalSyntheticLambda0;-><init>()V

    invoke-direct {v0, v1}, Landroid/view/FocusFinder$UserSpecifiedFocusComparator;-><init>(Landroid/view/FocusFinder$UserSpecifiedFocusComparator$NextFocusGetter;)V

    iput-object v0, p0, Landroid/view/FocusFinder;->mUserSpecifiedFocusComparator:Landroid/view/FocusFinder$UserSpecifiedFocusComparator;

    new-instance v0, Landroid/view/FocusFinder$UserSpecifiedFocusComparator;

    new-instance v1, Landroid/view/FocusFinder$$ExternalSyntheticLambda1;

    invoke-direct {v1}, Landroid/view/FocusFinder$$ExternalSyntheticLambda1;-><init>()V

    invoke-direct {v0, v1}, Landroid/view/FocusFinder$UserSpecifiedFocusComparator;-><init>(Landroid/view/FocusFinder$UserSpecifiedFocusComparator$NextFocusGetter;)V

    iput-object v0, p0, Landroid/view/FocusFinder;->mUserSpecifiedClusterComparator:Landroid/view/FocusFinder$UserSpecifiedFocusComparator;

    new-instance v0, Landroid/view/FocusFinder$FocusSorter;

    invoke-direct {v0}, Landroid/view/FocusFinder$FocusSorter;-><init>()V

    iput-object v0, p0, Landroid/view/FocusFinder;->mFocusSorter:Landroid/view/FocusFinder$FocusSorter;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/view/FocusFinder;->mTempList:Ljava/util/ArrayList;

    return-void
.end method

.method synthetic constructor <init>(Landroid/view/FocusFinder-IA;)V
    .locals 0

    invoke-direct {p0}, Landroid/view/FocusFinder;-><init>()V

    return-void
.end method

.method private findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;I)Landroid/view/View;
    .locals 9

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/FocusFinder;->getEffectiveRoot(Landroid/view/ViewGroup;Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v7

    if-eqz p2, :cond_0

    invoke-direct {p0, v7, p2, p4}, Landroid/view/FocusFinder;->findNextUserSpecifiedFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    return-object v0

    :cond_1
    iget-object v8, p0, Landroid/view/FocusFinder;->mTempList:Ljava/util/ArrayList;

    :try_start_0
    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {v7, v8, p4}, Landroid/view/ViewGroup;->addFocusables(Ljava/util/ArrayList;I)V

    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    move-object v1, p0

    move-object v2, v7

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, v8

    invoke-direct/range {v1 .. v6}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;ILjava/util/ArrayList;)Landroid/view/View;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v1

    :cond_2
    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    nop

    return-object v0

    :catchall_0
    move-exception v1

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    throw v1
.end method

.method private findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;ILjava/util/ArrayList;)Landroid/view/View;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Landroid/view/View;",
            "Landroid/graphics/Rect;",
            "I",
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    if-eqz p2, :cond_1

    if-nez p3, :cond_0

    iget-object p3, p0, Landroid/view/FocusFinder;->mFocusedRect:Landroid/graphics/Rect;

    :cond_0
    invoke-virtual {p2, p3}, Landroid/view/View;->getFocusedRect(Landroid/graphics/Rect;)V

    invoke-virtual {p1, p2, p3}, Landroid/view/ViewGroup;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    goto :goto_0

    :cond_1
    if-nez p3, :cond_4

    iget-object p3, p0, Landroid/view/FocusFinder;->mFocusedRect:Landroid/graphics/Rect;

    sparse-switch p4, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    invoke-direct {p0, p1, p3}, Landroid/view/FocusFinder;->setFocusTopLeft(Landroid/view/ViewGroup;Landroid/graphics/Rect;)V

    goto :goto_0

    :sswitch_1
    invoke-direct {p0, p1, p3}, Landroid/view/FocusFinder;->setFocusBottomRight(Landroid/view/ViewGroup;Landroid/graphics/Rect;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Landroid/view/ViewGroup;->isLayoutRtl()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1, p3}, Landroid/view/FocusFinder;->setFocusBottomRight(Landroid/view/ViewGroup;Landroid/graphics/Rect;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1, p3}, Landroid/view/FocusFinder;->setFocusTopLeft(Landroid/view/ViewGroup;Landroid/graphics/Rect;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Landroid/view/ViewGroup;->isLayoutRtl()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, p1, p3}, Landroid/view/FocusFinder;->setFocusTopLeft(Landroid/view/ViewGroup;Landroid/graphics/Rect;)V

    goto :goto_0

    :cond_3
    invoke-direct {p0, p1, p3}, Landroid/view/FocusFinder;->setFocusBottomRight(Landroid/view/ViewGroup;Landroid/graphics/Rect;)V

    :cond_4
    :goto_0
    sparse-switch p4, :sswitch_data_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown direction: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_4
    move-object v1, p0

    move-object v2, p5

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    invoke-virtual/range {v1 .. v6}, Landroid/view/FocusFinder;->findNextFocusInAbsoluteDirection(Ljava/util/ArrayList;Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;I)Landroid/view/View;

    move-result-object v0

    return-object v0

    :sswitch_5
    move-object v1, p0

    move-object v2, p5

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    invoke-direct/range {v1 .. v6}, Landroid/view/FocusFinder;->findNextFocusInRelativeDirection(Ljava/util/ArrayList;Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;I)Landroid/view/View;

    move-result-object v0

    return-object v0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_3
        0x2 -> :sswitch_2
        0x11 -> :sswitch_1
        0x21 -> :sswitch_1
        0x42 -> :sswitch_0
        0x82 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_5
        0x2 -> :sswitch_5
        0x11 -> :sswitch_4
        0x21 -> :sswitch_4
        0x42 -> :sswitch_4
        0x82 -> :sswitch_4
    .end sparse-switch
.end method

.method private findNextFocusInRelativeDirection(Ljava/util/ArrayList;Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;I)Landroid/view/View;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;",
            "Landroid/view/ViewGroup;",
            "Landroid/view/View;",
            "Landroid/graphics/Rect;",
            "I)",
            "Landroid/view/View;"
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Landroid/view/FocusFinder;->mUserSpecifiedFocusComparator:Landroid/view/FocusFinder$UserSpecifiedFocusComparator;

    invoke-virtual {v0, p1, p2}, Landroid/view/FocusFinder$UserSpecifiedFocusComparator;->setFocusables(Ljava/util/List;Landroid/view/View;)V

    iget-object v0, p0, Landroid/view/FocusFinder;->mUserSpecifiedFocusComparator:Landroid/view/FocusFinder$UserSpecifiedFocusComparator;

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Landroid/view/FocusFinder;->mUserSpecifiedFocusComparator:Landroid/view/FocusFinder$UserSpecifiedFocusComparator;

    invoke-virtual {v0}, Landroid/view/FocusFinder$UserSpecifiedFocusComparator;->recycle()V

    nop

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    const/4 v1, 0x0

    return-object v1

    :cond_0
    packed-switch p5, :pswitch_data_0

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    return-object v1

    :pswitch_0
    invoke-static {p3, p1, v0}, Landroid/view/FocusFinder;->getNextFocusable(Landroid/view/View;Ljava/util/ArrayList;I)Landroid/view/View;

    move-result-object v1

    return-object v1

    :pswitch_1
    invoke-static {p3, p1, v0}, Landroid/view/FocusFinder;->getPreviousFocusable(Landroid/view/View;Ljava/util/ArrayList;I)Landroid/view/View;

    move-result-object v1

    return-object v1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Landroid/view/FocusFinder;->mUserSpecifiedFocusComparator:Landroid/view/FocusFinder$UserSpecifiedFocusComparator;

    invoke-virtual {v1}, Landroid/view/FocusFinder$UserSpecifiedFocusComparator;->recycle()V

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private findNextKeyboardNavigationCluster(Landroid/view/View;Landroid/view/View;Ljava/util/List;I)Landroid/view/View;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/view/View;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;I)",
            "Landroid/view/View;"
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Landroid/view/FocusFinder;->mUserSpecifiedClusterComparator:Landroid/view/FocusFinder$UserSpecifiedFocusComparator;

    invoke-virtual {v0, p3, p1}, Landroid/view/FocusFinder$UserSpecifiedFocusComparator;->setFocusables(Ljava/util/List;Landroid/view/View;)V

    iget-object v0, p0, Landroid/view/FocusFinder;->mUserSpecifiedClusterComparator:Landroid/view/FocusFinder$UserSpecifiedFocusComparator;

    invoke-static {p3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Landroid/view/FocusFinder;->mUserSpecifiedClusterComparator:Landroid/view/FocusFinder$UserSpecifiedFocusComparator;

    invoke-virtual {v0}, Landroid/view/FocusFinder$UserSpecifiedFocusComparator;->recycle()V

    nop

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    sparse-switch p4, :sswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown direction: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :sswitch_0
    invoke-static {p1, p2, p3, v0}, Landroid/view/FocusFinder;->getNextKeyboardNavigationCluster(Landroid/view/View;Landroid/view/View;Ljava/util/List;I)Landroid/view/View;

    move-result-object v1

    return-object v1

    :sswitch_1
    invoke-static {p1, p2, p3, v0}, Landroid/view/FocusFinder;->getPreviousKeyboardNavigationCluster(Landroid/view/View;Landroid/view/View;Ljava/util/List;I)Landroid/view/View;

    move-result-object v1

    return-object v1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Landroid/view/FocusFinder;->mUserSpecifiedClusterComparator:Landroid/view/FocusFinder$UserSpecifiedFocusComparator;

    invoke-virtual {v1}, Landroid/view/FocusFinder$UserSpecifiedFocusComparator;->recycle()V

    throw v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_0
        0x11 -> :sswitch_1
        0x21 -> :sswitch_1
        0x42 -> :sswitch_0
        0x82 -> :sswitch_0
    .end sparse-switch
.end method

.method private findNextUserSpecifiedFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;
    .locals 4

    invoke-virtual {p2, p1, p3}, Landroid/view/View;->findUserSetNextFocus(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    const/4 v2, 0x1

    :cond_0
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/view/View;->isFocusable()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->isInTouchMode()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->isFocusableInTouchMode()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    return-object v0

    :cond_2
    invoke-virtual {v0, p1, p3}, Landroid/view/View;->findUserSetNextFocus(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    if-nez v2, :cond_3

    const/4 v3, 0x1

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    :goto_0
    move v2, v3

    if-eqz v3, :cond_0

    invoke-virtual {v1, p1, p3}, Landroid/view/View;->findUserSetNextFocus(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    if-ne v1, v0, :cond_0

    :cond_4
    const/4 v3, 0x0

    return-object v3
.end method

.method private findNextUserSpecifiedKeyboardNavigationCluster(Landroid/view/View;Landroid/view/View;I)Landroid/view/View;
    .locals 2

    nop

    invoke-virtual {p2, p1, p3}, Landroid/view/View;->findUserSetNextKeyboardNavigationCluster(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->hasFocusable()Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_0
    const/4 v1, 0x0

    return-object v1
.end method

.method private getEffectiveRoot(Landroid/view/ViewGroup;Landroid/view/View;)Landroid/view/ViewGroup;
    .locals 5

    if-eqz p2, :cond_5

    if-ne p2, p1, :cond_0

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    :cond_1
    if-ne v1, p1, :cond_3

    if-eqz v0, :cond_2

    move-object v2, v0

    goto :goto_0

    :cond_2
    move-object v2, p1

    :goto_0
    return-object v2

    :cond_3
    move-object v2, v1

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getTouchscreenBlocksFocus()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "android.hardware.touchscreen"

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v2}, Landroid/view/ViewGroup;->isKeyboardNavigationCluster()Z

    move-result v3

    if-eqz v3, :cond_4

    move-object v0, v2

    :cond_4
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v2, v1, Landroid/view/ViewGroup;

    if-nez v2, :cond_1

    return-object p1

    :cond_5
    :goto_1
    return-object p1
.end method

.method public static getInstance()Landroid/view/FocusFinder;
    .locals 1

    sget-object v0, Landroid/view/FocusFinder;->tlFocusFinder:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/FocusFinder;

    return-object v0
.end method

.method private static getNextFocusable(Landroid/view/View;Ljava/util/ArrayList;I)Landroid/view/View;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;I)",
            "Landroid/view/View;"
        }
    .end annotation

    const/4 v0, 0x2

    if-ge p2, v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    if-eqz p0, :cond_1

    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_1

    add-int/lit8 v1, v0, 0x1

    if-ge v1, p2, :cond_1

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    return-object v1

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private static getNextKeyboardNavigationCluster(Landroid/view/View;Landroid/view/View;Ljava/util/List;I)Landroid/view/View;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/view/View;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;I)",
            "Landroid/view/View;"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0

    :cond_0
    invoke-interface {p2, p1}, Ljava/util/List;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_1

    add-int/lit8 v1, v0, 0x1

    if-ge v1, p3, :cond_1

    add-int/lit8 v1, v0, 0x1

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    return-object v1

    :cond_1
    return-object p0
.end method

.method private static getPreviousFocusable(Landroid/view/View;Ljava/util/ArrayList;I)Landroid/view/View;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;I)",
            "Landroid/view/View;"
        }
    .end annotation

    const/4 v0, 0x2

    if-ge p2, v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    if-eqz p0, :cond_1

    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_1

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    return-object v1

    :cond_1
    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private static getPreviousKeyboardNavigationCluster(Landroid/view/View;Landroid/view/View;Ljava/util/List;I)Landroid/view/View;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/view/View;",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;I)",
            "Landroid/view/View;"
        }
    .end annotation

    if-nez p1, :cond_0

    add-int/lit8 v0, p3, -0x1

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0

    :cond_0
    invoke-interface {p2, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_1

    add-int/lit8 v1, v0, -0x1

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    return-object v1

    :cond_1
    return-object p0
.end method

.method private isTouchCandidate(IILandroid/graphics/Rect;I)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    sparse-switch p4, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    iget v2, p3, Landroid/graphics/Rect;->top:I

    if-lt v2, p2, :cond_0

    iget v2, p3, Landroid/graphics/Rect;->left:I

    if-gt v2, p1, :cond_0

    iget v2, p3, Landroid/graphics/Rect;->right:I

    if-gt p1, v2, :cond_0

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :sswitch_1
    iget v2, p3, Landroid/graphics/Rect;->left:I

    if-lt v2, p1, :cond_1

    iget v2, p3, Landroid/graphics/Rect;->top:I

    if-gt v2, p2, :cond_1

    iget v2, p3, Landroid/graphics/Rect;->bottom:I

    if-gt p2, v2, :cond_1

    goto :goto_1

    :cond_1
    move v0, v1

    :goto_1
    return v0

    :sswitch_2
    iget v2, p3, Landroid/graphics/Rect;->top:I

    if-gt v2, p2, :cond_2

    iget v2, p3, Landroid/graphics/Rect;->left:I

    if-gt v2, p1, :cond_2

    iget v2, p3, Landroid/graphics/Rect;->right:I

    if-gt p1, v2, :cond_2

    goto :goto_2

    :cond_2
    move v0, v1

    :goto_2
    return v0

    :sswitch_3
    iget v2, p3, Landroid/graphics/Rect;->left:I

    if-gt v2, p1, :cond_3

    iget v2, p3, Landroid/graphics/Rect;->top:I

    if-gt v2, p2, :cond_3

    iget v2, p3, Landroid/graphics/Rect;->bottom:I

    if-gt p2, v2, :cond_3

    goto :goto_3

    :cond_3
    move v0, v1

    :goto_3
    return v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_3
        0x21 -> :sswitch_2
        0x42 -> :sswitch_1
        0x82 -> :sswitch_0
    .end sparse-switch
.end method

.method private static final isValidId(I)Z
    .locals 1

    if-eqz p0, :cond_0

    const/4 v0, -0x1

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method static synthetic lambda$new$0(Landroid/view/View;Landroid/view/View;)Landroid/view/View;
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getNextFocusForwardId()I

    move-result v0

    invoke-static {v0}, Landroid/view/FocusFinder;->isValidId(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p1, p0, v0}, Landroid/view/View;->findUserSetNextFocus(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method static synthetic lambda$new$1(Landroid/view/View;Landroid/view/View;)Landroid/view/View;
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getNextClusterForwardId()I

    move-result v0

    invoke-static {v0}, Landroid/view/FocusFinder;->isValidId(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p1, p0, v0}, Landroid/view/View;->findUserSetNextKeyboardNavigationCluster(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method static majorAxisDistance(ILandroid/graphics/Rect;Landroid/graphics/Rect;)I
    .locals 2

    invoke-static {p0, p1, p2}, Landroid/view/FocusFinder;->majorAxisDistanceRaw(ILandroid/graphics/Rect;Landroid/graphics/Rect;)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method static majorAxisDistanceRaw(ILandroid/graphics/Rect;Landroid/graphics/Rect;)I
    .locals 2

    sparse-switch p0, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    iget v0, p2, Landroid/graphics/Rect;->top:I

    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v1

    return v0

    :sswitch_1
    iget v0, p2, Landroid/graphics/Rect;->left:I

    iget v1, p1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    return v0

    :sswitch_2
    iget v0, p1, Landroid/graphics/Rect;->top:I

    iget v1, p2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v1

    return v0

    :sswitch_3
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget v1, p2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    return v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_3
        0x21 -> :sswitch_2
        0x42 -> :sswitch_1
        0x82 -> :sswitch_0
    .end sparse-switch
.end method

.method static majorAxisDistanceToFarEdge(ILandroid/graphics/Rect;Landroid/graphics/Rect;)I
    .locals 2

    invoke-static {p0, p1, p2}, Landroid/view/FocusFinder;->majorAxisDistanceToFarEdgeRaw(ILandroid/graphics/Rect;Landroid/graphics/Rect;)I

    move-result v0

    const/4 v1, 0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method static majorAxisDistanceToFarEdgeRaw(ILandroid/graphics/Rect;Landroid/graphics/Rect;)I
    .locals 2

    sparse-switch p0, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    iget v0, p2, Landroid/graphics/Rect;->bottom:I

    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v1

    return v0

    :sswitch_1
    iget v0, p2, Landroid/graphics/Rect;->right:I

    iget v1, p1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    return v0

    :sswitch_2
    iget v0, p1, Landroid/graphics/Rect;->top:I

    iget v1, p2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    return v0

    :sswitch_3
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget v1, p2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    return v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_3
        0x21 -> :sswitch_2
        0x42 -> :sswitch_1
        0x82 -> :sswitch_0
    .end sparse-switch
.end method

.method static minorAxisDistance(ILandroid/graphics/Rect;Landroid/graphics/Rect;)I
    .locals 3

    sparse-switch p0, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    iget v0, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget v1, p2, Landroid/graphics/Rect;->left:I

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    return v0

    :sswitch_1
    iget v0, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget v1, p2, Landroid/graphics/Rect;->top:I

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    return v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_1
        0x21 -> :sswitch_0
        0x42 -> :sswitch_1
        0x82 -> :sswitch_0
    .end sparse-switch
.end method

.method private setFocusBottomRight(Landroid/view/ViewGroup;Landroid/graphics/Rect;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getScrollY()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getScrollX()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p2, v1, v0, v1, v0}, Landroid/graphics/Rect;->set(IIII)V

    return-void
.end method

.method private setFocusTopLeft(Landroid/view/ViewGroup;Landroid/graphics/Rect;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getScrollY()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getScrollX()I

    move-result v1

    invoke-virtual {p2, v1, v0, v1, v0}, Landroid/graphics/Rect;->set(IIII)V

    return-void
.end method

.method public static sort([Landroid/view/View;IILandroid/view/ViewGroup;Z)V
    .locals 7

    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v0

    iget-object v1, v0, Landroid/view/FocusFinder;->mFocusSorter:Landroid/view/FocusFinder$FocusSorter;

    move-object v2, p0

    move v3, p1

    move v4, p2

    move-object v5, p3

    move v6, p4

    invoke-virtual/range {v1 .. v6}, Landroid/view/FocusFinder$FocusSorter;->sort([Landroid/view/View;IILandroid/view/ViewGroup;Z)V

    return-void
.end method


# virtual methods
.method beamBeats(ILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)Z
    .locals 6

    goto/32 :goto_7

    nop

    :goto_0
    if-eqz v3, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_d

    nop

    :goto_1
    invoke-static {p1, p2, p4}, Landroid/view/FocusFinder;->majorAxisDistanceToFarEdge(ILandroid/graphics/Rect;Landroid/graphics/Rect;)I

    move-result v5

    goto/32 :goto_14

    nop

    :goto_2
    const/4 v4, 0x1

    goto/32 :goto_0

    nop

    :goto_3
    goto :goto_6

    :goto_4
    goto/32 :goto_19

    nop

    :goto_5
    return v4

    :goto_6
    goto/32 :goto_1a

    nop

    :goto_7
    invoke-virtual {p0, p1, p2, p3}, Landroid/view/FocusFinder;->beamsOverlap(ILandroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v0

    goto/32 :goto_1b

    nop

    :goto_8
    move v2, v4

    :goto_9
    goto/32 :goto_15

    nop

    :goto_a
    goto :goto_16

    :goto_b
    goto/32 :goto_11

    nop

    :goto_c
    const/16 v3, 0x11

    goto/32 :goto_10

    nop

    :goto_d
    return v4

    :goto_e
    goto/32 :goto_c

    nop

    :goto_f
    const/4 v2, 0x0

    goto/32 :goto_13

    nop

    :goto_10
    if-ne p1, v3, :cond_1

    goto/32 :goto_16

    :cond_1
    goto/32 :goto_12

    nop

    :goto_11
    invoke-static {p1, p2, p3}, Landroid/view/FocusFinder;->majorAxisDistance(ILandroid/graphics/Rect;Landroid/graphics/Rect;)I

    move-result v3

    goto/32 :goto_1

    nop

    :goto_12
    const/16 v3, 0x42

    goto/32 :goto_18

    nop

    :goto_13
    if-eqz v1, :cond_2

    goto/32 :goto_6

    :cond_2
    goto/32 :goto_17

    nop

    :goto_14
    if-lt v3, v5, :cond_3

    goto/32 :goto_9

    :cond_3
    goto/32 :goto_8

    nop

    :goto_15
    return v2

    :goto_16
    goto/32 :goto_5

    nop

    :goto_17
    if-eqz v0, :cond_4

    goto/32 :goto_4

    :cond_4
    goto/32 :goto_3

    nop

    :goto_18
    if-eq p1, v3, :cond_5

    goto/32 :goto_b

    :cond_5
    goto/32 :goto_a

    nop

    :goto_19
    invoke-virtual {p0, p1, p2, p4}, Landroid/view/FocusFinder;->isToDirectionOf(ILandroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v3

    goto/32 :goto_2

    nop

    :goto_1a
    return v2

    :goto_1b
    invoke-virtual {p0, p1, p2, p4}, Landroid/view/FocusFinder;->beamsOverlap(ILandroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v1

    goto/32 :goto_f

    nop
.end method

.method beamsOverlap(ILandroid/graphics/Rect;Landroid/graphics/Rect;)Z
    .locals 4

    goto/32 :goto_9

    nop

    :goto_0
    goto/16 :goto_17

    :goto_1
    goto/32 :goto_16

    nop

    :goto_2
    iget v2, p3, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_14

    nop

    :goto_3
    if-lt v2, v3, :cond_0

    goto/32 :goto_19

    :cond_0
    goto/32 :goto_18

    nop

    :goto_4
    iget v3, p2, Landroid/graphics/Rect;->right:I

    goto/32 :goto_3

    nop

    :goto_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_6

    nop

    :goto_6
    const-string v1, "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}."

    goto/32 :goto_10

    nop

    :goto_7
    iget v3, p2, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_13

    nop

    :goto_8
    iget v2, p3, Landroid/graphics/Rect;->top:I

    goto/32 :goto_7

    nop

    :goto_9
    const/4 v0, 0x1

    goto/32 :goto_15

    nop

    :goto_a
    iget v2, p3, Landroid/graphics/Rect;->right:I

    goto/32 :goto_1b

    nop

    :goto_b
    if-gt v2, v3, :cond_1

    goto/32 :goto_19

    :cond_1
    goto/32 :goto_1a

    nop

    :goto_c
    if-gt v2, v3, :cond_2

    goto/32 :goto_1

    :cond_2
    goto/32 :goto_8

    nop

    :goto_d
    move v0, v1

    :goto_e
    goto/32 :goto_12

    nop

    :goto_f
    throw v0

    :sswitch_0
    goto/32 :goto_a

    nop

    :goto_10
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_f

    nop

    :goto_11
    return v0

    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_1
        0x21 -> :sswitch_0
        0x42 -> :sswitch_1
        0x82 -> :sswitch_0
    .end sparse-switch

    :goto_12
    return v0

    :sswitch_1
    goto/32 :goto_2

    nop

    :goto_13
    if-lt v2, v3, :cond_3

    goto/32 :goto_1

    :cond_3
    goto/32 :goto_0

    nop

    :goto_14
    iget v3, p2, Landroid/graphics/Rect;->top:I

    goto/32 :goto_c

    nop

    :goto_15
    const/4 v1, 0x0

    sparse-switch p1, :sswitch_data_0

    goto/32 :goto_5

    nop

    :goto_16
    move v0, v1

    :goto_17
    goto/32 :goto_11

    nop

    :goto_18
    goto :goto_e

    :goto_19
    goto/32 :goto_d

    nop

    :goto_1a
    iget v2, p3, Landroid/graphics/Rect;->left:I

    goto/32 :goto_4

    nop

    :goto_1b
    iget v3, p2, Landroid/graphics/Rect;->left:I

    goto/32 :goto_b

    nop
.end method

.method public findNearestTouchable(Landroid/view/ViewGroup;III[I)Landroid/view/View;
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    invoke-virtual/range {p1 .. p1}, Landroid/view/ViewGroup;->getTouchables()Ljava/util/ArrayList;

    move-result-object v5

    const v6, 0x7fffffff

    const/4 v7, 0x0

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v8

    iget-object v9, v1, Landroid/view/ViewGroup;->mContext:Landroid/content/Context;

    invoke-static {v9}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/ViewConfiguration;->getScaledEdgeSlop()I

    move-result v9

    new-instance v10, Landroid/graphics/Rect;

    invoke-direct {v10}, Landroid/graphics/Rect;-><init>()V

    iget-object v11, v0, Landroid/view/FocusFinder;->mOtherRect:Landroid/graphics/Rect;

    const/4 v12, 0x0

    :goto_0
    if-ge v12, v8, :cond_3

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/view/View;

    invoke-virtual {v13, v11}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    const/4 v14, 0x1

    invoke-virtual {v1, v13, v11, v14, v14}, Landroid/view/ViewGroup;->offsetRectBetweenParentAndChild(Landroid/view/View;Landroid/graphics/Rect;ZZ)V

    invoke-direct {v0, v2, v3, v11, v4}, Landroid/view/FocusFinder;->isTouchCandidate(IILandroid/graphics/Rect;I)Z

    move-result v15

    if-nez v15, :cond_0

    goto :goto_3

    :cond_0
    const v15, 0x7fffffff

    sparse-switch v4, :sswitch_data_0

    goto :goto_1

    :sswitch_0
    iget v15, v11, Landroid/graphics/Rect;->top:I

    goto :goto_1

    :sswitch_1
    iget v15, v11, Landroid/graphics/Rect;->left:I

    goto :goto_1

    :sswitch_2
    iget v0, v11, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v3, v0

    add-int/lit8 v15, v0, 0x1

    goto :goto_1

    :sswitch_3
    iget v0, v11, Landroid/graphics/Rect;->right:I

    sub-int v0, v2, v0

    add-int/lit8 v15, v0, 0x1

    nop

    :goto_1
    if-ge v15, v9, :cond_2

    if-eqz v7, :cond_1

    invoke-virtual {v10, v11}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v11, v10}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v0

    if-nez v0, :cond_2

    if-ge v15, v6, :cond_2

    :cond_1
    move v0, v15

    move-object v6, v13

    invoke-virtual {v10, v11}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    const/4 v7, 0x0

    sparse-switch v4, :sswitch_data_1

    goto :goto_2

    :sswitch_4
    aput v15, p5, v14

    goto :goto_2

    :sswitch_5
    aput v15, p5, v7

    goto :goto_2

    :sswitch_6
    neg-int v7, v15

    aput v7, p5, v14

    goto :goto_2

    :sswitch_7
    neg-int v14, v15

    aput v14, p5, v7

    nop

    :goto_2
    move-object v7, v6

    move v6, v0

    :cond_2
    :goto_3
    add-int/lit8 v12, v12, 0x1

    move-object/from16 v0, p0

    goto :goto_0

    :cond_3
    return-object v7

    nop

    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_3
        0x21 -> :sswitch_2
        0x42 -> :sswitch_1
        0x82 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x11 -> :sswitch_7
        0x21 -> :sswitch_6
        0x42 -> :sswitch_5
        0x82 -> :sswitch_4
    .end sparse-switch
.end method

.method public final findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public findNextFocusFromRect(Landroid/view/ViewGroup;Landroid/graphics/Rect;I)Landroid/view/View;
    .locals 2

    iget-object v0, p0, Landroid/view/FocusFinder;->mFocusedRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v0, p0, Landroid/view/FocusFinder;->mFocusedRect:Landroid/graphics/Rect;

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0, p3}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method findNextFocusInAbsoluteDirection(Ljava/util/ArrayList;Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Rect;I)Landroid/view/View;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;",
            "Landroid/view/ViewGroup;",
            "Landroid/view/View;",
            "Landroid/graphics/Rect;",
            "I)",
            "Landroid/view/View;"
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    iget-object v5, p0, Landroid/view/FocusFinder;->mOtherRect:Landroid/graphics/Rect;

    goto/32 :goto_27

    nop

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_33

    nop

    :goto_2
    iget-object v0, p0, Landroid/view/FocusFinder;->mBestCandidateRect:Landroid/graphics/Rect;

    goto/32 :goto_8

    nop

    :goto_3
    iget-object v4, p0, Landroid/view/FocusFinder;->mOtherRect:Landroid/graphics/Rect;

    goto/32 :goto_29

    nop

    :goto_4
    goto/16 :goto_25

    :sswitch_0
    goto/32 :goto_9

    nop

    :goto_5
    move-object v0, v3

    :goto_6
    goto/32 :goto_1

    nop

    :goto_7
    return-object v0

    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_2
        0x21 -> :sswitch_1
        0x42 -> :sswitch_0
        0x82 -> :sswitch_3
    .end sparse-switch

    :goto_8
    invoke-virtual {v0, p4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto/32 :goto_22

    nop

    :goto_9
    iget-object v1, p0, Landroid/view/FocusFinder;->mBestCandidateRect:Landroid/graphics/Rect;

    goto/32 :goto_13

    nop

    :goto_a
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_1c

    nop

    :goto_b
    invoke-virtual {v1, v0, v2}, Landroid/graphics/Rect;->offset(II)V

    goto/32 :goto_18

    nop

    :goto_c
    neg-int v2, v2

    goto/32 :goto_1a

    nop

    :goto_d
    goto :goto_6

    :goto_e
    goto/32 :goto_12

    nop

    :goto_f
    goto/16 :goto_25

    :sswitch_1
    goto/32 :goto_21

    nop

    :goto_10
    invoke-virtual {p4}, Landroid/graphics/Rect;->height()I

    move-result v2

    goto/32 :goto_30

    nop

    :goto_11
    iget-object v1, p0, Landroid/view/FocusFinder;->mBestCandidateRect:Landroid/graphics/Rect;

    goto/32 :goto_10

    nop

    :goto_12
    iget-object v4, p0, Landroid/view/FocusFinder;->mOtherRect:Landroid/graphics/Rect;

    goto/32 :goto_2f

    nop

    :goto_13
    invoke-virtual {p4}, Landroid/graphics/Rect;->width()I

    move-result v2

    goto/32 :goto_31

    nop

    :goto_14
    const/4 v0, 0x0

    goto/32 :goto_a

    nop

    :goto_15
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_b

    nop

    :goto_16
    neg-int v2, v2

    goto/32 :goto_35

    nop

    :goto_17
    check-cast v3, Landroid/view/View;

    goto/32 :goto_1e

    nop

    :goto_18
    goto :goto_25

    :sswitch_2
    goto/32 :goto_1f

    nop

    :goto_19
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_24

    nop

    :goto_1a
    invoke-virtual {v1, v2, v0}, Landroid/graphics/Rect;->offset(II)V

    goto/32 :goto_f

    nop

    :goto_1b
    invoke-virtual {p4}, Landroid/graphics/Rect;->width()I

    move-result v2

    goto/32 :goto_19

    nop

    :goto_1c
    const/4 v2, 0x0

    :goto_1d
    goto/32 :goto_28

    nop

    :goto_1e
    if-ne v3, p3, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_2a

    nop

    :goto_1f
    iget-object v1, p0, Landroid/view/FocusFinder;->mBestCandidateRect:Landroid/graphics/Rect;

    goto/32 :goto_1b

    nop

    :goto_20
    iget-object v4, p0, Landroid/view/FocusFinder;->mBestCandidateRect:Landroid/graphics/Rect;

    goto/32 :goto_0

    nop

    :goto_21
    iget-object v1, p0, Landroid/view/FocusFinder;->mBestCandidateRect:Landroid/graphics/Rect;

    goto/32 :goto_2b

    nop

    :goto_22
    const/4 v0, 0x0

    sparse-switch p5, :sswitch_data_0

    goto/32 :goto_26

    nop

    :goto_23
    if-nez v4, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_20

    nop

    :goto_24
    invoke-virtual {v1, v2, v0}, Landroid/graphics/Rect;->offset(II)V

    nop

    :goto_25
    goto/32 :goto_14

    nop

    :goto_26
    goto :goto_25

    :sswitch_3
    goto/32 :goto_11

    nop

    :goto_27
    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto/32 :goto_5

    nop

    :goto_28
    if-lt v2, v1, :cond_2

    goto/32 :goto_34

    :cond_2
    goto/32 :goto_2e

    nop

    :goto_29
    iget-object v5, p0, Landroid/view/FocusFinder;->mBestCandidateRect:Landroid/graphics/Rect;

    goto/32 :goto_2c

    nop

    :goto_2a
    if-eq v3, p2, :cond_3

    goto/32 :goto_e

    :cond_3
    goto/32 :goto_d

    nop

    :goto_2b
    invoke-virtual {p4}, Landroid/graphics/Rect;->height()I

    move-result v2

    goto/32 :goto_15

    nop

    :goto_2c
    invoke-virtual {p0, p5, p4, v4, v5}, Landroid/view/FocusFinder;->isBetterCandidate(ILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v4

    goto/32 :goto_23

    nop

    :goto_2d
    invoke-virtual {p2, v3, v4}, Landroid/view/ViewGroup;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    goto/32 :goto_3

    nop

    :goto_2e
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_17

    nop

    :goto_2f
    invoke-virtual {v3, v4}, Landroid/view/View;->getFocusedRect(Landroid/graphics/Rect;)V

    goto/32 :goto_32

    nop

    :goto_30
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_16

    nop

    :goto_31
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_c

    nop

    :goto_32
    iget-object v4, p0, Landroid/view/FocusFinder;->mOtherRect:Landroid/graphics/Rect;

    goto/32 :goto_2d

    nop

    :goto_33
    goto/16 :goto_1d

    :goto_34
    goto/32 :goto_7

    nop

    :goto_35
    invoke-virtual {v1, v0, v2}, Landroid/graphics/Rect;->offset(II)V

    goto/32 :goto_4

    nop
.end method

.method public findNextKeyboardNavigationCluster(Landroid/view/View;Landroid/view/View;I)Landroid/view/View;
    .locals 3

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/FocusFinder;->findNextUserSpecifiedKeyboardNavigationCluster(Landroid/view/View;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    iget-object v1, p0, Landroid/view/FocusFinder;->mTempList:Ljava/util/ArrayList;

    :try_start_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {p1, v1, p3}, Landroid/view/View;->addKeyboardNavigationClusters(Ljava/util/Collection;I)V

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-direct {p0, p1, p2, v1, p3}, Landroid/view/FocusFinder;->findNextKeyboardNavigationCluster(Landroid/view/View;Landroid/view/View;Ljava/util/List;I)Landroid/view/View;

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v2

    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    nop

    return-object v0

    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    throw v2
.end method

.method getWeightedDistanceFor(JJ)J
    .locals 4

    goto/32 :goto_3

    nop

    :goto_0
    add-long/2addr v0, v2

    goto/32 :goto_1

    nop

    :goto_1
    return-wide v0

    :goto_2
    mul-long/2addr v0, p1

    goto/32 :goto_5

    nop

    :goto_3
    const-wide/16 v0, 0xd

    goto/32 :goto_4

    nop

    :goto_4
    mul-long/2addr v0, p1

    goto/32 :goto_2

    nop

    :goto_5
    mul-long v2, p3, p3

    goto/32 :goto_0

    nop
.end method

.method isBetterCandidate(ILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)Z
    .locals 9

    goto/32 :goto_12

    nop

    :goto_0
    invoke-virtual {p0, v3, v4, v5, v6}, Landroid/view/FocusFinder;->getWeightedDistanceFor(JJ)J

    move-result-wide v3

    goto/32 :goto_e

    nop

    :goto_1
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/view/FocusFinder;->beamBeats(ILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v0

    goto/32 :goto_6

    nop

    :goto_2
    int-to-long v5, v0

    goto/32 :goto_0

    nop

    :goto_3
    return v1

    :goto_4
    move v1, v2

    :goto_5
    goto/32 :goto_3

    nop

    :goto_6
    if-nez v0, :cond_0

    goto/32 :goto_1e

    :cond_0
    goto/32 :goto_1d

    nop

    :goto_7
    if-ltz v0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_4

    nop

    :goto_8
    cmp-long v0, v3, v5

    goto/32 :goto_7

    nop

    :goto_9
    if-eqz v0, :cond_2

    goto/32 :goto_14

    :cond_2
    goto/32 :goto_13

    nop

    :goto_a
    int-to-long v3, v0

    goto/32 :goto_c

    nop

    :goto_b
    if-eqz v0, :cond_3

    goto/32 :goto_1b

    :cond_3
    goto/32 :goto_1a

    nop

    :goto_c
    invoke-static {p1, p2, p3}, Landroid/view/FocusFinder;->minorAxisDistance(ILandroid/graphics/Rect;Landroid/graphics/Rect;)I

    move-result v0

    goto/32 :goto_2

    nop

    :goto_d
    invoke-static {p1, p2, p3}, Landroid/view/FocusFinder;->majorAxisDistance(ILandroid/graphics/Rect;Landroid/graphics/Rect;)I

    move-result v0

    goto/32 :goto_a

    nop

    :goto_e
    invoke-static {p1, p2, p4}, Landroid/view/FocusFinder;->majorAxisDistance(ILandroid/graphics/Rect;Landroid/graphics/Rect;)I

    move-result v0

    goto/32 :goto_11

    nop

    :goto_f
    return v1

    :goto_10
    nop

    goto/32 :goto_d

    nop

    :goto_11
    int-to-long v5, v0

    goto/32 :goto_16

    nop

    :goto_12
    invoke-virtual {p0, p2, p3, p1}, Landroid/view/FocusFinder;->isCandidate(Landroid/graphics/Rect;Landroid/graphics/Rect;I)Z

    move-result v0

    goto/32 :goto_1f

    nop

    :goto_13
    return v2

    :goto_14
    goto/32 :goto_1

    nop

    :goto_15
    invoke-virtual {p0, p2, p4, p1}, Landroid/view/FocusFinder;->isCandidate(Landroid/graphics/Rect;Landroid/graphics/Rect;I)Z

    move-result v0

    goto/32 :goto_1c

    nop

    :goto_16
    invoke-static {p1, p2, p4}, Landroid/view/FocusFinder;->minorAxisDistance(ILandroid/graphics/Rect;Landroid/graphics/Rect;)I

    move-result v0

    goto/32 :goto_20

    nop

    :goto_17
    invoke-virtual {p0, v5, v6, v7, v8}, Landroid/view/FocusFinder;->getWeightedDistanceFor(JJ)J

    move-result-wide v5

    goto/32 :goto_8

    nop

    :goto_18
    if-nez v0, :cond_4

    goto/32 :goto_10

    :cond_4
    goto/32 :goto_f

    nop

    :goto_19
    invoke-virtual {p0, p1, p2, p4, p3}, Landroid/view/FocusFinder;->beamBeats(ILandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v0

    goto/32 :goto_18

    nop

    :goto_1a
    return v1

    :goto_1b
    goto/32 :goto_15

    nop

    :goto_1c
    const/4 v2, 0x1

    goto/32 :goto_9

    nop

    :goto_1d
    return v2

    :goto_1e
    goto/32 :goto_19

    nop

    :goto_1f
    const/4 v1, 0x0

    goto/32 :goto_b

    nop

    :goto_20
    int-to-long v7, v0

    goto/32 :goto_17

    nop
.end method

.method isCandidate(Landroid/graphics/Rect;Landroid/graphics/Rect;I)Z
    .locals 4

    goto/32 :goto_35

    nop

    :goto_0
    goto/16 :goto_30

    :goto_1
    goto/32 :goto_2f

    nop

    :goto_2
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_5

    nop

    :goto_3
    return v0

    :sswitch_0
    goto/32 :goto_2e

    nop

    :goto_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_3d

    nop

    :goto_5
    throw v0

    :sswitch_1
    goto/32 :goto_1b

    nop

    :goto_6
    iget v3, p2, Landroid/graphics/Rect;->top:I

    goto/32 :goto_31

    nop

    :goto_7
    iget v3, p2, Landroid/graphics/Rect;->right:I

    goto/32 :goto_12

    nop

    :goto_8
    iget v3, p2, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_25

    nop

    :goto_9
    if-le v2, v3, :cond_0

    goto/32 :goto_10

    :cond_0
    :goto_a
    goto/32 :goto_23

    nop

    :goto_b
    iget v2, p1, Landroid/graphics/Rect;->right:I

    goto/32 :goto_1a

    nop

    :goto_c
    iget v2, p1, Landroid/graphics/Rect;->left:I

    goto/32 :goto_14

    nop

    :goto_d
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_e

    nop

    :goto_e
    iget v3, p2, Landroid/graphics/Rect;->top:I

    goto/32 :goto_33

    nop

    :goto_f
    goto :goto_19

    :goto_10
    goto/32 :goto_18

    nop

    :goto_11
    return v0

    :sswitch_2
    goto/32 :goto_1c

    nop

    :goto_12
    if-lt v2, v3, :cond_1

    goto/32 :goto_10

    :cond_1
    goto/32 :goto_f

    nop

    :goto_13
    iget v2, p1, Landroid/graphics/Rect;->top:I

    goto/32 :goto_8

    nop

    :goto_14
    iget v3, p2, Landroid/graphics/Rect;->left:I

    goto/32 :goto_3f

    nop

    :goto_15
    return v0

    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_3
        0x21 -> :sswitch_0
        0x42 -> :sswitch_2
        0x82 -> :sswitch_1
    .end sparse-switch

    :goto_16
    if-ge v2, v3, :cond_2

    goto/32 :goto_34

    :cond_2
    goto/32 :goto_d

    nop

    :goto_17
    iget v2, p1, Landroid/graphics/Rect;->top:I

    goto/32 :goto_6

    nop

    :goto_18
    move v0, v1

    :goto_19
    goto/32 :goto_3

    nop

    :goto_1a
    iget v3, p2, Landroid/graphics/Rect;->left:I

    goto/32 :goto_9

    nop

    :goto_1b
    iget v2, p1, Landroid/graphics/Rect;->top:I

    goto/32 :goto_20

    nop

    :goto_1c
    iget v2, p1, Landroid/graphics/Rect;->left:I

    goto/32 :goto_2a

    nop

    :goto_1d
    iget v3, p2, Landroid/graphics/Rect;->right:I

    goto/32 :goto_2b

    nop

    :goto_1e
    goto :goto_28

    :goto_1f
    goto/32 :goto_27

    nop

    :goto_20
    iget v3, p2, Landroid/graphics/Rect;->top:I

    goto/32 :goto_16

    nop

    :goto_21
    iget v2, p1, Landroid/graphics/Rect;->left:I

    goto/32 :goto_1d

    nop

    :goto_22
    iget v3, p2, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_36

    nop

    :goto_23
    iget v2, p1, Landroid/graphics/Rect;->right:I

    goto/32 :goto_7

    nop

    :goto_24
    if-lt v2, v3, :cond_3

    goto/32 :goto_1

    :cond_3
    goto/32 :goto_0

    nop

    :goto_25
    if-ge v2, v3, :cond_4

    goto/32 :goto_1f

    :cond_4
    :goto_26
    goto/32 :goto_17

    nop

    :goto_27
    move v0, v1

    :goto_28
    goto/32 :goto_32

    nop

    :goto_29
    iget v2, p1, Landroid/graphics/Rect;->right:I

    goto/32 :goto_3a

    nop

    :goto_2a
    iget v3, p2, Landroid/graphics/Rect;->left:I

    goto/32 :goto_3e

    nop

    :goto_2b
    if-ge v2, v3, :cond_5

    goto/32 :goto_39

    :cond_5
    :goto_2c
    goto/32 :goto_c

    nop

    :goto_2d
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_3b

    nop

    :goto_2e
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_22

    nop

    :goto_2f
    move v0, v1

    :goto_30
    goto/32 :goto_11

    nop

    :goto_31
    if-gt v2, v3, :cond_6

    goto/32 :goto_1f

    :cond_6
    goto/32 :goto_1e

    nop

    :goto_32
    return v0

    :sswitch_3
    goto/32 :goto_29

    nop

    :goto_33
    if-le v2, v3, :cond_7

    goto/32 :goto_1

    :cond_7
    :goto_34
    goto/32 :goto_2d

    nop

    :goto_35
    const/4 v0, 0x1

    goto/32 :goto_37

    nop

    :goto_36
    if-le v2, v3, :cond_8

    goto/32 :goto_26

    :cond_8
    goto/32 :goto_13

    nop

    :goto_37
    const/4 v1, 0x0

    sparse-switch p3, :sswitch_data_0

    goto/32 :goto_4

    nop

    :goto_38
    goto :goto_41

    :goto_39
    goto/32 :goto_40

    nop

    :goto_3a
    iget v3, p2, Landroid/graphics/Rect;->right:I

    goto/32 :goto_3c

    nop

    :goto_3b
    iget v3, p2, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_24

    nop

    :goto_3c
    if-le v2, v3, :cond_9

    goto/32 :goto_2c

    :cond_9
    goto/32 :goto_21

    nop

    :goto_3d
    const-string v1, "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}."

    goto/32 :goto_2

    nop

    :goto_3e
    if-ge v2, v3, :cond_a

    goto/32 :goto_a

    :cond_a
    goto/32 :goto_b

    nop

    :goto_3f
    if-gt v2, v3, :cond_b

    goto/32 :goto_39

    :cond_b
    goto/32 :goto_38

    nop

    :goto_40
    move v0, v1

    :goto_41
    goto/32 :goto_15

    nop
.end method

.method isToDirectionOf(ILandroid/graphics/Rect;Landroid/graphics/Rect;)Z
    .locals 4

    goto/32 :goto_a

    nop

    :goto_0
    goto :goto_16

    :goto_1
    goto/32 :goto_15

    nop

    :goto_2
    return v0

    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_0
        0x21 -> :sswitch_2
        0x42 -> :sswitch_3
        0x82 -> :sswitch_1
    .end sparse-switch

    :goto_3
    move v0, v1

    :goto_4
    goto/32 :goto_17

    nop

    :goto_5
    if-le v2, v3, :cond_0

    goto/32 :goto_1b

    :cond_0
    goto/32 :goto_1a

    nop

    :goto_6
    const-string v1, "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}."

    goto/32 :goto_23

    nop

    :goto_7
    return v0

    :sswitch_0
    goto/32 :goto_11

    nop

    :goto_8
    goto :goto_13

    :goto_9
    goto/32 :goto_12

    nop

    :goto_a
    const/4 v0, 0x1

    goto/32 :goto_1e

    nop

    :goto_b
    throw v0

    :sswitch_1
    goto/32 :goto_c

    nop

    :goto_c
    iget v2, p2, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_10

    nop

    :goto_d
    if-le v2, v3, :cond_1

    goto/32 :goto_19

    :cond_1
    goto/32 :goto_18

    nop

    :goto_e
    move v0, v1

    :goto_f
    goto/32 :goto_25

    nop

    :goto_10
    iget v3, p3, Landroid/graphics/Rect;->top:I

    goto/32 :goto_d

    nop

    :goto_11
    iget v2, p2, Landroid/graphics/Rect;->left:I

    goto/32 :goto_1f

    nop

    :goto_12
    move v0, v1

    :goto_13
    goto/32 :goto_2

    nop

    :goto_14
    new-instance v0, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_6

    nop

    :goto_15
    move v0, v1

    :goto_16
    goto/32 :goto_7

    nop

    :goto_17
    return v0

    :sswitch_2
    goto/32 :goto_20

    nop

    :goto_18
    goto :goto_f

    :goto_19
    goto/32 :goto_e

    nop

    :goto_1a
    goto :goto_4

    :goto_1b
    goto/32 :goto_3

    nop

    :goto_1c
    iget v3, p3, Landroid/graphics/Rect;->left:I

    goto/32 :goto_5

    nop

    :goto_1d
    iget v3, p3, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_24

    nop

    :goto_1e
    const/4 v1, 0x0

    sparse-switch p1, :sswitch_data_0

    goto/32 :goto_14

    nop

    :goto_1f
    iget v3, p3, Landroid/graphics/Rect;->right:I

    goto/32 :goto_21

    nop

    :goto_20
    iget v2, p2, Landroid/graphics/Rect;->top:I

    goto/32 :goto_1d

    nop

    :goto_21
    if-ge v2, v3, :cond_2

    goto/32 :goto_9

    :cond_2
    goto/32 :goto_8

    nop

    :goto_22
    iget v2, p2, Landroid/graphics/Rect;->right:I

    goto/32 :goto_1c

    nop

    :goto_23
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_b

    nop

    :goto_24
    if-ge v2, v3, :cond_3

    goto/32 :goto_1

    :cond_3
    goto/32 :goto_0

    nop

    :goto_25
    return v0

    :sswitch_3
    goto/32 :goto_22

    nop
.end method
