.class public abstract Landroid/view/autofill/AutofillManagerInternal;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getAutofillOptions(Ljava/lang/String;JI)Landroid/content/AutofillOptions;
.end method

.method public abstract isAugmentedAutofillServiceForUser(II)Z
.end method

.method public abstract onBackKeyPressed()V
.end method
