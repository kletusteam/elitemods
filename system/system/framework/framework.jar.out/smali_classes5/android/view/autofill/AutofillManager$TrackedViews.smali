.class Landroid/view/autofill/AutofillManager$TrackedViews;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/autofill/AutofillManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TrackedViews"
.end annotation


# instance fields
.field private mInvisibleTrackedIds:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Landroid/view/autofill/AutofillId;",
            ">;"
        }
    .end annotation
.end field

.field private mVisibleTrackedIds:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Landroid/view/autofill/AutofillId;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Landroid/view/autofill/AutofillManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetmInvisibleTrackedIds(Landroid/view/autofill/AutofillManager$TrackedViews;)Landroid/util/ArraySet;
    .locals 0

    iget-object p0, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mInvisibleTrackedIds:Landroid/util/ArraySet;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmVisibleTrackedIds(Landroid/view/autofill/AutofillManager$TrackedViews;)Landroid/util/ArraySet;
    .locals 0

    iget-object p0, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mVisibleTrackedIds:Landroid/util/ArraySet;

    return-object p0
.end method

.method constructor <init>(Landroid/view/autofill/AutofillManager;[Landroid/view/autofill/AutofillId;)V
    .locals 7

    iput-object p1, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->this$0:Landroid/view/autofill/AutofillManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Landroid/view/autofill/AutofillManager;->-$$Nest$mgetClient(Landroid/view/autofill/AutofillManager;)Landroid/view/autofill/AutofillManager$AutofillClient;

    move-result-object v0

    invoke-static {p2}, Lcom/android/internal/util/ArrayUtils;->isEmpty([Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "AutofillManager"

    if-nez v1, :cond_3

    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/view/autofill/AutofillManager$AutofillClient;->autofillClientIsVisibleForAutofill()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-boolean v1, Landroid/view/autofill/Helper;->sVerbose:Z

    if-eqz v1, :cond_0

    const-string v1, "client is visible, check tracked ids"

    invoke-static {v2, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-interface {v0, p2}, Landroid/view/autofill/AutofillManager$AutofillClient;->autofillClientGetViewVisibility([Landroid/view/autofill/AutofillId;)[Z

    move-result-object v1

    goto :goto_0

    :cond_1
    array-length v1, p2

    new-array v1, v1, [Z

    :goto_0
    array-length v3, p2

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v3, :cond_3

    aget-object v5, p2, v4

    invoke-virtual {v5}, Landroid/view/autofill/AutofillId;->resetSessionId()V

    aget-boolean v6, v1, v4

    if-eqz v6, :cond_2

    iget-object v6, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mVisibleTrackedIds:Landroid/util/ArraySet;

    invoke-direct {p0, v6, v5}, Landroid/view/autofill/AutofillManager$TrackedViews;->addToSet(Landroid/util/ArraySet;Ljava/lang/Object;)Landroid/util/ArraySet;

    move-result-object v6

    iput-object v6, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mVisibleTrackedIds:Landroid/util/ArraySet;

    goto :goto_2

    :cond_2
    iget-object v6, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mInvisibleTrackedIds:Landroid/util/ArraySet;

    invoke-direct {p0, v6, v5}, Landroid/view/autofill/AutofillManager$TrackedViews;->addToSet(Landroid/util/ArraySet;Ljava/lang/Object;)Landroid/util/ArraySet;

    move-result-object v6

    iput-object v6, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mInvisibleTrackedIds:Landroid/util/ArraySet;

    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_3
    sget-boolean v1, Landroid/view/autofill/Helper;->sVerbose:Z

    if-eqz v1, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TrackedViews(trackedIds="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "):  mVisibleTrackedIds="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mVisibleTrackedIds:Landroid/util/ArraySet;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " mInvisibleTrackedIds="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mInvisibleTrackedIds:Landroid/util/ArraySet;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v1, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mVisibleTrackedIds:Landroid/util/ArraySet;

    if-nez v1, :cond_5

    const/4 v1, 0x4

    invoke-static {p1, v1}, Landroid/view/autofill/AutofillManager;->-$$Nest$mfinishSessionLocked(Landroid/view/autofill/AutofillManager;I)V

    :cond_5
    return-void
.end method

.method private addToSet(Landroid/util/ArraySet;Ljava/lang/Object;)Landroid/util/ArraySet;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/util/ArraySet<",
            "TT;>;TT;)",
            "Landroid/util/ArraySet<",
            "TT;>;"
        }
    .end annotation

    if-nez p1, :cond_0

    new-instance v0, Landroid/util/ArraySet;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/util/ArraySet;-><init>(I)V

    move-object p1, v0

    :cond_0
    invoke-virtual {p1, p2}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    return-object p1
.end method

.method private isInSet(Landroid/util/ArraySet;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/util/ArraySet<",
            "TT;>;TT;)Z"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-virtual {p1, p2}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private removeFromSet(Landroid/util/ArraySet;Ljava/lang/Object;)Landroid/util/ArraySet;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/util/ArraySet<",
            "TT;>;TT;)",
            "Landroid/util/ArraySet<",
            "TT;>;"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    invoke-virtual {p1, p2}, Landroid/util/ArraySet;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Landroid/util/ArraySet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    return-object v0

    :cond_1
    return-object p1
.end method


# virtual methods
.method notifyViewVisibilityChangedLocked(Landroid/view/autofill/AutofillId;Z)V
    .locals 3

    goto/32 :goto_2a

    nop

    :goto_0
    invoke-direct {p0, v0, p1}, Landroid/view/autofill/AutofillManager$TrackedViews;->removeFromSet(Landroid/util/ArraySet;Ljava/lang/Object;)Landroid/util/ArraySet;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_1
    iput-object v0, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mInvisibleTrackedIds:Landroid/util/ArraySet;

    goto/32 :goto_11

    nop

    :goto_2
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_2c

    nop

    :goto_3
    iget-object v0, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mVisibleTrackedIds:Landroid/util/ArraySet;

    goto/32 :goto_37

    nop

    :goto_4
    iget-object v0, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->this$0:Landroid/view/autofill/AutofillManager;

    goto/32 :goto_33

    nop

    :goto_5
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_c

    nop

    :goto_6
    if-nez v0, :cond_0

    goto/32 :goto_23

    :cond_0
    goto/32 :goto_7

    nop

    :goto_7
    iget-object v0, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mVisibleTrackedIds:Landroid/util/ArraySet;

    goto/32 :goto_36

    nop

    :goto_8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_b

    nop

    :goto_9
    iput-object v0, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mVisibleTrackedIds:Landroid/util/ArraySet;

    goto/32 :goto_2b

    nop

    :goto_a
    iget-object v0, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mInvisibleTrackedIds:Landroid/util/ArraySet;

    goto/32 :goto_24

    nop

    :goto_b
    const-string v2, "No more visible ids. Invisible = "

    goto/32 :goto_e

    nop

    :goto_c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_2f

    nop

    :goto_d
    if-eqz v0, :cond_1

    goto/32 :goto_2e

    :cond_1
    goto/32 :goto_26

    nop

    :goto_e
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_18

    nop

    :goto_f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_1b

    nop

    :goto_10
    invoke-direct {p0, v0, p1}, Landroid/view/autofill/AutofillManager$TrackedViews;->addToSet(Landroid/util/ArraySet;Ljava/lang/Object;)Landroid/util/ArraySet;

    move-result-object v0

    goto/32 :goto_1d

    nop

    :goto_11
    iget-object v0, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mVisibleTrackedIds:Landroid/util/ArraySet;

    goto/32 :goto_10

    nop

    :goto_12
    iget-object v0, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->this$0:Landroid/view/autofill/AutofillManager;

    goto/32 :goto_35

    nop

    :goto_13
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_8

    nop

    :goto_14
    goto :goto_23

    :goto_15
    goto/32 :goto_3

    nop

    :goto_16
    iget-object v0, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mVisibleTrackedIds:Landroid/util/ArraySet;

    goto/32 :goto_d

    nop

    :goto_17
    return-void

    :goto_18
    iget-object v2, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mInvisibleTrackedIds:Landroid/util/ArraySet;

    goto/32 :goto_5

    nop

    :goto_19
    if-nez p2, :cond_2

    goto/32 :goto_15

    :cond_2
    goto/32 :goto_a

    nop

    :goto_1a
    iget-object v0, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mInvisibleTrackedIds:Landroid/util/ArraySet;

    goto/32 :goto_0

    nop

    :goto_1b
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1c
    goto/32 :goto_12

    nop

    :goto_1d
    iput-object v0, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mVisibleTrackedIds:Landroid/util/ArraySet;

    goto/32 :goto_14

    nop

    :goto_1e
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_29

    nop

    :goto_1f
    invoke-direct {p0, v0, p1}, Landroid/view/autofill/AutofillManager$TrackedViews;->addToSet(Landroid/util/ArraySet;Ljava/lang/Object;)Landroid/util/ArraySet;

    move-result-object v0

    goto/32 :goto_22

    nop

    :goto_20
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_28

    nop

    :goto_21
    const-string v2, " isVisible="

    goto/32 :goto_2

    nop

    :goto_22
    iput-object v0, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mInvisibleTrackedIds:Landroid/util/ArraySet;

    :goto_23
    goto/32 :goto_16

    nop

    :goto_24
    invoke-direct {p0, v0, p1}, Landroid/view/autofill/AutofillManager$TrackedViews;->isInSet(Landroid/util/ArraySet;Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_38

    nop

    :goto_25
    const-string v1, "AutofillManager"

    goto/32 :goto_32

    nop

    :goto_26
    sget-boolean v0, Landroid/view/autofill/Helper;->sVerbose:Z

    goto/32 :goto_34

    nop

    :goto_27
    if-nez v0, :cond_3

    goto/32 :goto_23

    :cond_3
    goto/32 :goto_19

    nop

    :goto_28
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_31

    nop

    :goto_29
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_21

    nop

    :goto_2a
    sget-boolean v0, Landroid/view/autofill/Helper;->sDebug:Z

    goto/32 :goto_25

    nop

    :goto_2b
    iget-object v0, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mInvisibleTrackedIds:Landroid/util/ArraySet;

    goto/32 :goto_1f

    nop

    :goto_2c
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_f

    nop

    :goto_2d
    invoke-static {v0, v1}, Landroid/view/autofill/AutofillManager;->-$$Nest$mfinishSessionLocked(Landroid/view/autofill/AutofillManager;I)V

    :goto_2e
    goto/32 :goto_17

    nop

    :goto_2f
    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_30
    goto/32 :goto_4

    nop

    :goto_31
    const-string/jumbo v2, "notifyViewVisibilityChangedLocked(): id="

    goto/32 :goto_1e

    nop

    :goto_32
    if-nez v0, :cond_4

    goto/32 :goto_1c

    :cond_4
    goto/32 :goto_20

    nop

    :goto_33
    const/4 v1, 0x4

    goto/32 :goto_2d

    nop

    :goto_34
    if-nez v0, :cond_5

    goto/32 :goto_30

    :cond_5
    goto/32 :goto_13

    nop

    :goto_35
    invoke-static {v0}, Landroid/view/autofill/AutofillManager;->-$$Nest$misClientVisibleForAutofillLocked(Landroid/view/autofill/AutofillManager;)Z

    move-result v0

    goto/32 :goto_27

    nop

    :goto_36
    invoke-direct {p0, v0, p1}, Landroid/view/autofill/AutofillManager$TrackedViews;->removeFromSet(Landroid/util/ArraySet;Ljava/lang/Object;)Landroid/util/ArraySet;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_37
    invoke-direct {p0, v0, p1}, Landroid/view/autofill/AutofillManager$TrackedViews;->isInSet(Landroid/util/ArraySet;Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_6

    nop

    :goto_38
    if-nez v0, :cond_6

    goto/32 :goto_23

    :cond_6
    goto/32 :goto_1a

    nop
.end method

.method onVisibleForAutofillChangedLocked()V
    .locals 12

    goto/32 :goto_17

    nop

    :goto_0
    add-int/lit8 v8, v8, 0x1

    goto/32 :goto_10

    nop

    :goto_1
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    goto/32 :goto_4f

    nop

    :goto_2
    iput-object v1, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mVisibleTrackedIds:Landroid/util/ArraySet;

    :goto_3
    goto/32 :goto_39

    nop

    :goto_4
    if-nez v4, :cond_0

    goto/32 :goto_11

    :cond_0
    goto/32 :goto_29

    nop

    :goto_5
    goto/16 :goto_2e

    :goto_6
    goto/32 :goto_58

    nop

    :goto_7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_3d

    nop

    :goto_8
    aget-boolean v10, v6, v8

    goto/32 :goto_5f

    nop

    :goto_9
    const/4 v8, 0x0

    :goto_a
    goto/32 :goto_2c

    nop

    :goto_b
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_36

    nop

    :goto_c
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/32 :goto_2d

    nop

    :goto_d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_62

    nop

    :goto_e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_47

    nop

    :goto_f
    if-nez v10, :cond_1

    goto/32 :goto_42

    :cond_1
    goto/32 :goto_5c

    nop

    :goto_10
    goto :goto_a

    :goto_11
    goto/32 :goto_1d

    nop

    :goto_12
    iget-object v4, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mInvisibleTrackedIds:Landroid/util/ArraySet;

    goto/32 :goto_32

    nop

    :goto_13
    invoke-static {v3, v4}, Landroid/view/autofill/AutofillManager;->-$$Nest$mfinishSessionLocked(Landroid/view/autofill/AutofillManager;I)V

    :goto_14
    goto/32 :goto_55

    nop

    :goto_15
    const/4 v8, 0x0

    :goto_16
    goto/32 :goto_1b

    nop

    :goto_17
    iget-object v0, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->this$0:Landroid/view/autofill/AutofillManager;

    goto/32 :goto_1c

    nop

    :goto_18
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    goto/32 :goto_15

    nop

    :goto_19
    new-instance v4, Ljava/util/ArrayList;

    goto/32 :goto_64

    nop

    :goto_1a
    if-eqz v4, :cond_2

    goto/32 :goto_14

    :cond_2
    goto/32 :goto_59

    nop

    :goto_1b
    if-lt v8, v7, :cond_3

    goto/32 :goto_40

    :cond_3
    goto/32 :goto_2b

    nop

    :goto_1c
    invoke-static {v0}, Landroid/view/autofill/AutofillManager;->-$$Nest$mgetClient(Landroid/view/autofill/AutofillManager;)Landroid/view/autofill/AutofillManager$AutofillClient;

    move-result-object v0

    goto/32 :goto_3b

    nop

    :goto_1d
    iget-object v4, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mVisibleTrackedIds:Landroid/util/ArraySet;

    goto/32 :goto_26

    nop

    :goto_1e
    const-string v11, " became invisible"

    goto/32 :goto_49

    nop

    :goto_1f
    const-string/jumbo v4, "onVisibleForAutofillChangedLocked(): no more visible ids"

    goto/32 :goto_56

    nop

    :goto_20
    iget-object v3, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->this$0:Landroid/view/autofill/AutofillManager;

    goto/32 :goto_4a

    nop

    :goto_21
    if-nez v10, :cond_4

    goto/32 :goto_31

    :cond_4
    goto/32 :goto_52

    nop

    :goto_22
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    goto/32 :goto_54

    nop

    :goto_23
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_24
    goto/32 :goto_12

    nop

    :goto_25
    invoke-direct {v4, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    nop

    goto/32 :goto_46

    nop

    :goto_26
    if-nez v4, :cond_5

    goto/32 :goto_40

    :cond_5
    goto/32 :goto_19

    nop

    :goto_27
    iget-object v6, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mInvisibleTrackedIds:Landroid/util/ArraySet;

    goto/32 :goto_35

    nop

    :goto_28
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_22

    nop

    :goto_29
    new-instance v4, Ljava/util/ArrayList;

    goto/32 :goto_27

    nop

    :goto_2a
    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    goto/32 :goto_5e

    nop

    :goto_2b
    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    goto/32 :goto_4c

    nop

    :goto_2c
    if-lt v8, v7, :cond_6

    goto/32 :goto_11

    :cond_6
    goto/32 :goto_2a

    nop

    :goto_2d
    invoke-static {v3, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2e
    goto/32 :goto_33

    nop

    :goto_2f
    if-nez v10, :cond_7

    goto/32 :goto_2e

    :cond_7
    goto/32 :goto_4e

    nop

    :goto_30
    goto :goto_42

    :goto_31
    goto/32 :goto_41

    nop

    :goto_32
    const-string/jumbo v5, "onVisibleForAutofill() "

    goto/32 :goto_4

    nop

    :goto_33
    add-int/lit8 v8, v8, 0x1

    goto/32 :goto_3f

    nop

    :goto_34
    sget-boolean v4, Landroid/view/autofill/Helper;->sVerbose:Z

    goto/32 :goto_4b

    nop

    :goto_35
    invoke-direct {v4, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    nop

    goto/32 :goto_45

    nop

    :goto_36
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_65

    nop

    :goto_37
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_3a

    nop

    :goto_38
    sget-boolean v10, Landroid/view/autofill/Helper;->sDebug:Z

    goto/32 :goto_f

    nop

    :goto_39
    iget-object v4, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mVisibleTrackedIds:Landroid/util/ArraySet;

    goto/32 :goto_1a

    nop

    :goto_3a
    iget-object v5, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mInvisibleTrackedIds:Landroid/util/ArraySet;

    goto/32 :goto_d

    nop

    :goto_3b
    const/4 v1, 0x0

    goto/32 :goto_5a

    nop

    :goto_3c
    invoke-interface {v0, v6}, Landroid/view/autofill/AutofillManager$AutofillClient;->autofillClientGetViewVisibility([Landroid/view/autofill/AutofillId;)[Z

    move-result-object v6

    goto/32 :goto_4d

    nop

    :goto_3d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_23

    nop

    :goto_3e
    if-nez v4, :cond_8

    goto/32 :goto_57

    :cond_8
    goto/32 :goto_1f

    nop

    :goto_3f
    goto/16 :goto_16

    :goto_40
    goto/32 :goto_61

    nop

    :goto_41
    invoke-direct {p0, v2, v9}, Landroid/view/autofill/AutofillManager$TrackedViews;->addToSet(Landroid/util/ArraySet;Ljava/lang/Object;)Landroid/util/ArraySet;

    move-result-object v2

    :goto_42
    goto/32 :goto_0

    nop

    :goto_43
    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    goto/32 :goto_1e

    nop

    :goto_44
    invoke-direct {p0, v1, v9}, Landroid/view/autofill/AutofillManager$TrackedViews;->addToSet(Landroid/util/ArraySet;Ljava/lang/Object;)Landroid/util/ArraySet;

    move-result-object v1

    goto/32 :goto_5

    nop

    :goto_45
    invoke-static {v4}, Landroid/view/autofill/Helper;->toArray(Ljava/util/Collection;)[Landroid/view/autofill/AutofillId;

    move-result-object v6

    goto/32 :goto_3c

    nop

    :goto_46
    invoke-static {v4}, Landroid/view/autofill/Helper;->toArray(Ljava/util/Collection;)[Landroid/view/autofill/AutofillId;

    move-result-object v6

    goto/32 :goto_51

    nop

    :goto_47
    iget-object v5, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mVisibleTrackedIds:Landroid/util/ArraySet;

    goto/32 :goto_7

    nop

    :goto_48
    sget-boolean v10, Landroid/view/autofill/Helper;->sDebug:Z

    goto/32 :goto_2f

    nop

    :goto_49
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    goto/32 :goto_c

    nop

    :goto_4a
    const/4 v4, 0x4

    goto/32 :goto_13

    nop

    :goto_4b
    if-nez v4, :cond_9

    goto/32 :goto_24

    :cond_9
    goto/32 :goto_b

    nop

    :goto_4c
    check-cast v9, Landroid/view/autofill/AutofillId;

    goto/32 :goto_8

    nop

    :goto_4d
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    goto/32 :goto_9

    nop

    :goto_4e
    new-instance v10, Ljava/lang/StringBuilder;

    goto/32 :goto_5b

    nop

    :goto_4f
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/32 :goto_5d

    nop

    :goto_50
    aget-boolean v10, v6, v8

    goto/32 :goto_21

    nop

    :goto_51
    invoke-interface {v0, v6}, Landroid/view/autofill/AutofillManager$AutofillClient;->autofillClientGetViewVisibility([Landroid/view/autofill/AutofillId;)[Z

    move-result-object v6

    goto/32 :goto_18

    nop

    :goto_52
    invoke-direct {p0, v1, v9}, Landroid/view/autofill/AutofillManager$TrackedViews;->addToSet(Landroid/util/ArraySet;Ljava/lang/Object;)Landroid/util/ArraySet;

    move-result-object v1

    goto/32 :goto_38

    nop

    :goto_53
    if-nez v0, :cond_a

    goto/32 :goto_3

    :cond_a
    goto/32 :goto_34

    nop

    :goto_54
    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    goto/32 :goto_66

    nop

    :goto_55
    return-void

    :goto_56
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_57
    goto/32 :goto_20

    nop

    :goto_58
    invoke-direct {p0, v2, v9}, Landroid/view/autofill/AutofillManager$TrackedViews;->addToSet(Landroid/util/ArraySet;Ljava/lang/Object;)Landroid/util/ArraySet;

    move-result-object v2

    goto/32 :goto_48

    nop

    :goto_59
    sget-boolean v4, Landroid/view/autofill/Helper;->sVerbose:Z

    goto/32 :goto_3e

    nop

    :goto_5a
    const/4 v2, 0x0

    goto/32 :goto_63

    nop

    :goto_5b
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_60

    nop

    :goto_5c
    new-instance v10, Ljava/lang/StringBuilder;

    goto/32 :goto_28

    nop

    :goto_5d
    invoke-static {v3, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_30

    nop

    :goto_5e
    check-cast v9, Landroid/view/autofill/AutofillId;

    goto/32 :goto_50

    nop

    :goto_5f
    if-nez v10, :cond_b

    goto/32 :goto_6

    :cond_b
    goto/32 :goto_44

    nop

    :goto_60
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    goto/32 :goto_43

    nop

    :goto_61
    iput-object v2, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mInvisibleTrackedIds:Landroid/util/ArraySet;

    goto/32 :goto_2

    nop

    :goto_62
    const-string v5, " vis="

    goto/32 :goto_e

    nop

    :goto_63
    const-string v3, "AutofillManager"

    goto/32 :goto_53

    nop

    :goto_64
    iget-object v6, p0, Landroid/view/autofill/AutofillManager$TrackedViews;->mVisibleTrackedIds:Landroid/util/ArraySet;

    goto/32 :goto_25

    nop

    :goto_65
    const-string/jumbo v5, "onVisibleForAutofillChangedLocked(): inv= "

    goto/32 :goto_37

    nop

    :goto_66
    const-string v11, " became visible"

    goto/32 :goto_1

    nop
.end method
