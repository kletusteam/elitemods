.class public interface abstract Landroid/view/HandWritingStub;
.super Ljava/lang/Object;


# static fields
.field public static final HAND_WRITING_KEYBOARD_TYPE_NORMAL:I = 0x1

.field public static final HAND_WRITING_KEYBOARD_TYPE_STYLUS:I = 0x2


# direct methods
.method public static newInstance()Landroid/view/HandWritingStub;
    .locals 1

    const-class v0, Landroid/view/HandWritingStub;

    invoke-static {v0}, Lcom/miui/base/MiuiStubUtil;->newInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/HandWritingStub;

    return-object v0
.end method


# virtual methods
.method public getCurrentKeyboardType()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public handleBlackListSituation(Landroid/view/KeyEvent;)V
    .locals 0

    return-void
.end method

.method public isKeyboardTypeChanged()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public recordCustomToolType(Landroid/view/InputEvent;)V
    .locals 0

    return-void
.end method

.method public refreshLastKeyboardType()V
    .locals 0

    return-void
.end method

.method public setCurrentKeyboardType(I)V
    .locals 0

    return-void
.end method
