.class public Landroid/view/WindowLayout;
.super Ljava/lang/Object;


# static fields
.field private static final DEBUG:Z = false

.field static final MAX_X:I = 0x186a0

.field static final MAX_Y:I = 0x186a0

.field static final MIN_X:I = -0x186a0

.field static final MIN_Y:I = -0x186a0

.field private static final TAG:Ljava/lang/String;

.field public static final UNSPECIFIED_LENGTH:I = -0x1


# instance fields
.field private final mTempDisplayCutoutSafeExceptMaybeBarsRect:Landroid/graphics/Rect;

.field private final mTempRect:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Landroid/view/WindowLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/view/WindowLayout;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/view/WindowLayout;->mTempDisplayCutoutSafeExceptMaybeBarsRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/view/WindowLayout;->mTempRect:Landroid/graphics/Rect;

    return-void
.end method

.method public static computeSurfaceSize(Landroid/view/WindowManager$LayoutParams;Landroid/graphics/Rect;IILandroid/graphics/Rect;ZLandroid/graphics/Point;)V
    .locals 5

    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v0, v0, 0x4000

    if-eqz v0, :cond_0

    move v0, p2

    move v1, p3

    goto :goto_0

    :cond_0
    if-eqz p5, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    goto :goto_0

    :cond_1
    invoke-virtual {p4}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p4}, Landroid/graphics/Rect;->height()I

    move-result v1

    :goto_0
    const/4 v2, 0x1

    if-ge v0, v2, :cond_2

    const/4 v0, 0x1

    :cond_2
    if-ge v1, v2, :cond_3

    const/4 v1, 0x1

    :cond_3
    iget-object v2, p0, Landroid/view/WindowManager$LayoutParams;->surfaceInsets:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->left:I

    iget v4, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    iget v3, v2, Landroid/graphics/Rect;->top:I

    iget v4, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v4

    add-int/2addr v1, v3

    invoke-virtual {p6, v0, v1}, Landroid/graphics/Point;->set(II)V

    return-void
.end method

.method private getScaledFrame(Landroid/graphics/Rect;F)Landroid/graphics/Rect;
    .locals 3

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    invoke-virtual {v0, p2}, Landroid/graphics/Rect;->scale(F)V

    iget v1, p1, Landroid/graphics/Rect;->left:I

    iget v2, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offsetTo(II)V

    return-object v0
.end method


# virtual methods
.method public computeFrames(Landroid/view/WindowManager$LayoutParams;Landroid/view/InsetsState;Landroid/graphics/Rect;Landroid/graphics/Rect;IIILandroid/view/InsetsVisibilities;Landroid/graphics/Rect;FLandroid/window/ClientWindowFrames;)V
    .locals 42

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p10

    move-object/from16 v7, p11

    iget v8, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    iget v9, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    iget v10, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    and-int/lit16 v11, v9, 0x100

    const/16 v12, 0x100

    if-ne v11, v12, :cond_0

    const/4 v11, 0x1

    goto :goto_0

    :cond_0
    const/4 v11, 0x0

    :goto_0
    iget-object v12, v7, Landroid/window/ClientWindowFrames;->displayFrame:Landroid/graphics/Rect;

    iget-object v15, v7, Landroid/window/ClientWindowFrames;->parentFrame:Landroid/graphics/Rect;

    iget-object v14, v7, Landroid/window/ClientWindowFrames;->frame:Landroid/graphics/Rect;

    invoke-virtual/range {p1 .. p1}, Landroid/view/WindowManager$LayoutParams;->getFitInsetsTypes()I

    move-result v13

    move/from16 v22, v9

    invoke-virtual/range {p1 .. p1}, Landroid/view/WindowManager$LayoutParams;->isFitInsetsIgnoringVisibility()Z

    move-result v9

    invoke-virtual {v2, v4, v13, v9}, Landroid/view/InsetsState;->calculateInsets(Landroid/graphics/Rect;IZ)Landroid/graphics/Insets;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Landroid/view/WindowManager$LayoutParams;->getFitInsetsSides()I

    move-result v13

    and-int/lit8 v18, v13, 0x1

    if-eqz v18, :cond_1

    move-object/from16 v23, v14

    iget v14, v9, Landroid/graphics/Insets;->left:I

    goto :goto_1

    :cond_1
    move-object/from16 v23, v14

    const/4 v14, 0x0

    :goto_1
    and-int/lit8 v18, v13, 0x2

    if-eqz v18, :cond_2

    iget v6, v9, Landroid/graphics/Insets;->top:I

    goto :goto_2

    :cond_2
    const/4 v6, 0x0

    :goto_2
    and-int/lit8 v18, v13, 0x4

    if-eqz v18, :cond_3

    move/from16 v24, v8

    iget v8, v9, Landroid/graphics/Insets;->right:I

    goto :goto_3

    :cond_3
    move/from16 v24, v8

    const/4 v8, 0x0

    :goto_3
    and-int/lit8 v18, v13, 0x8

    if-eqz v18, :cond_4

    move/from16 v25, v13

    iget v13, v9, Landroid/graphics/Insets;->bottom:I

    goto :goto_4

    :cond_4
    move/from16 v25, v13

    const/4 v13, 0x0

    :goto_4
    move-object/from16 v26, v9

    iget v9, v4, Landroid/graphics/Rect;->left:I

    add-int/2addr v9, v14

    move/from16 v27, v14

    iget v14, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v14, v6

    move/from16 v28, v6

    iget v6, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v6, v8

    move/from16 v29, v8

    iget v8, v4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v8, v13

    invoke-virtual {v12, v9, v14, v6, v8}, Landroid/graphics/Rect;->set(IIII)V

    if-nez p9, :cond_6

    invoke-virtual {v15, v12}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    const/high16 v6, 0x40000000    # 2.0f

    and-int/2addr v6, v10

    if-eqz v6, :cond_8

    const/16 v6, 0x13

    invoke-virtual {v2, v6}, Landroid/view/InsetsState;->peekSource(I)Landroid/view/InsetsSource;

    move-result-object v6

    if-eqz v6, :cond_5

    const/4 v8, 0x0

    invoke-virtual {v6, v15, v8}, Landroid/view/InsetsSource;->calculateInsets(Landroid/graphics/Rect;Z)Landroid/graphics/Insets;

    move-result-object v9

    invoke-virtual {v15, v9}, Landroid/graphics/Rect;->inset(Landroid/graphics/Insets;)V

    :cond_5
    goto :goto_6

    :cond_6
    if-nez v11, :cond_7

    move-object/from16 v6, p9

    goto :goto_5

    :cond_7
    move-object v6, v12

    :goto_5
    invoke-virtual {v15, v6}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    :cond_8
    :goto_6
    invoke-static {}, Landroid/view/WindowLayoutStub;->getInstance()Landroid/view/WindowLayoutStub;

    move-result-object v6

    invoke-interface {v6, v1}, Landroid/view/WindowLayoutStub;->getLayoutInDisplayCutoutMode(Landroid/view/WindowManager$LayoutParams;)I

    move-result v6

    invoke-virtual/range {p2 .. p2}, Landroid/view/InsetsState;->getDisplayCutout()Landroid/view/DisplayCutout;

    move-result-object v8

    iget-object v9, v0, Landroid/view/WindowLayout;->mTempDisplayCutoutSafeExceptMaybeBarsRect:Landroid/graphics/Rect;

    invoke-virtual {v9, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    const/4 v14, 0x0

    iput-boolean v14, v7, Landroid/window/ClientWindowFrames;->isParentFrameClippedByDisplayCutout:Z

    const/4 v14, 0x3

    const/high16 v18, 0x10000

    const/high16 v19, 0x3f800000    # 1.0f

    if-eq v6, v14, :cond_21

    invoke-virtual {v8}, Landroid/view/DisplayCutout;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_21

    invoke-virtual/range {p2 .. p2}, Landroid/view/InsetsState;->getDisplayFrame()Landroid/graphics/Rect;

    move-result-object v14

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/view/InsetsState;->peekSource(I)Landroid/view/InsetsSource;

    move-result-object v21

    if-eqz v21, :cond_9

    iget v4, v3, Landroid/graphics/Rect;->top:I

    move-object/from16 v30, v8

    iget v8, v14, Landroid/graphics/Rect;->top:I

    if-le v4, v8, :cond_a

    nop

    invoke-virtual/range {v21 .. v21}, Landroid/view/InsetsSource;->getFrame()Landroid/graphics/Rect;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    iget v8, v3, Landroid/graphics/Rect;->top:I

    invoke-static {v4, v8}, Ljava/lang/Math;->max(II)I

    move-result v4

    iput v4, v9, Landroid/graphics/Rect;->top:I

    goto :goto_7

    :cond_9
    move-object/from16 v30, v8

    :cond_a
    :goto_7
    const/4 v8, 0x1

    if-ne v6, v8, :cond_c

    invoke-virtual {v14}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {v14}, Landroid/graphics/Rect;->height()I

    move-result v4

    if-ge v8, v4, :cond_b

    const/high16 v4, -0x80000000

    iput v4, v9, Landroid/graphics/Rect;->top:I

    const v8, 0x7fffffff

    iput v8, v9, Landroid/graphics/Rect;->bottom:I

    goto :goto_8

    :cond_b
    const/high16 v4, -0x80000000

    const v8, 0x7fffffff

    iput v4, v9, Landroid/graphics/Rect;->left:I

    iput v8, v9, Landroid/graphics/Rect;->right:I

    :cond_c
    :goto_8
    iget v4, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int v4, v4, v18

    if-eqz v4, :cond_d

    const/4 v4, 0x1

    goto :goto_9

    :cond_d
    const/4 v4, 0x0

    :goto_9
    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Landroid/view/InsetsState;->getSourceOrDefaultVisibility(I)Z

    move-result v17

    const/4 v8, 0x1

    xor-int/lit8 v16, v17, 0x1

    move/from16 v17, v16

    invoke-virtual {v2, v8}, Landroid/view/InsetsState;->getSourceOrDefaultVisibility(I)Z

    move-result v16

    xor-int/lit8 v34, v16, 0x1

    move/from16 v8, v34

    move/from16 v34, v10

    const/4 v10, 0x5

    if-ne v5, v10, :cond_f

    if-eqz v11, :cond_f

    if-eqz v4, :cond_f

    if-nez v17, :cond_f

    if-eqz v6, :cond_e

    const/4 v10, 0x1

    if-ne v6, v10, :cond_f

    :cond_e
    const/high16 v10, -0x80000000

    iput v10, v9, Landroid/graphics/Rect;->top:I

    :cond_f
    const/4 v10, 0x5

    if-ne v5, v10, :cond_12

    if-eqz v11, :cond_12

    if-eqz v4, :cond_12

    if-nez v8, :cond_12

    if-eqz v6, :cond_11

    const/4 v10, 0x1

    if-ne v6, v10, :cond_10

    goto :goto_a

    :cond_10
    move/from16 v35, v8

    goto :goto_b

    :cond_11
    :goto_a
    invoke-static {}, Landroid/util/MiuiFreeformUtils;->getNavBarPosition()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    :pswitch_0
    move/from16 v35, v8

    goto :goto_b

    :pswitch_1
    move/from16 v35, v8

    const v8, 0x7fffffff

    iput v8, v9, Landroid/graphics/Rect;->bottom:I

    goto :goto_b

    :pswitch_2
    move/from16 v35, v8

    const v8, 0x7fffffff

    iput v8, v9, Landroid/graphics/Rect;->right:I

    goto :goto_b

    :pswitch_3
    move/from16 v35, v8

    const/high16 v8, -0x80000000

    iput v8, v9, Landroid/graphics/Rect;->left:I

    goto :goto_b

    :cond_12
    move/from16 v35, v8

    :goto_b
    if-eqz v11, :cond_18

    if-eqz v4, :cond_18

    if-eqz v6, :cond_14

    const/4 v8, 0x1

    if-ne v6, v8, :cond_13

    goto :goto_c

    :cond_13
    move-object/from16 v10, p8

    move/from16 v36, v4

    goto :goto_f

    :cond_14
    :goto_c
    nop

    invoke-static {}, Landroid/view/WindowInsets$Type;->systemBars()I

    move-result v8

    move-object/from16 v10, p8

    invoke-virtual {v2, v14, v8, v10}, Landroid/view/InsetsState;->calculateInsets(Landroid/graphics/Rect;ILandroid/view/InsetsVisibilities;)Landroid/graphics/Insets;

    move-result-object v8

    move/from16 v36, v4

    iget v4, v8, Landroid/graphics/Insets;->left:I

    if-lez v4, :cond_15

    const/high16 v4, -0x80000000

    iput v4, v9, Landroid/graphics/Rect;->left:I

    goto :goto_d

    :cond_15
    const/high16 v4, -0x80000000

    :goto_d
    iget v4, v8, Landroid/graphics/Insets;->top:I

    if-lez v4, :cond_16

    const/high16 v4, -0x80000000

    iput v4, v9, Landroid/graphics/Rect;->top:I

    :cond_16
    iget v4, v8, Landroid/graphics/Insets;->right:I

    if-lez v4, :cond_17

    const v4, 0x7fffffff

    iput v4, v9, Landroid/graphics/Rect;->right:I

    goto :goto_e

    :cond_17
    const v4, 0x7fffffff

    :goto_e
    iget v4, v8, Landroid/graphics/Insets;->bottom:I

    if-lez v4, :cond_19

    const v4, 0x7fffffff

    iput v4, v9, Landroid/graphics/Rect;->bottom:I

    goto :goto_f

    :cond_18
    move-object/from16 v10, p8

    move/from16 v36, v4

    :cond_19
    :goto_f
    const/16 v4, 0x7db

    move/from16 v8, v24

    if-ne v8, v4, :cond_1a

    move/from16 v24, v6

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/view/InsetsState;->peekSource(I)Landroid/view/InsetsSource;

    move-result-object v6

    if-eqz v6, :cond_1b

    invoke-virtual {v6, v14, v4}, Landroid/view/InsetsSource;->calculateInsets(Landroid/graphics/Rect;Z)Landroid/graphics/Insets;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Insets;->bottom:I

    if-lez v2, :cond_1b

    const v2, 0x7fffffff

    iput v2, v9, Landroid/graphics/Rect;->bottom:I

    goto :goto_10

    :cond_1a
    move/from16 v24, v6

    :cond_1b
    :goto_10
    if-eqz p9, :cond_1c

    if-nez v11, :cond_1c

    const/4 v2, 0x1

    goto :goto_11

    :cond_1c
    const/4 v2, 0x0

    :goto_11
    invoke-virtual/range {p1 .. p1}, Landroid/view/WindowManager$LayoutParams;->isFullscreen()Z

    move-result v4

    if-nez v4, :cond_1d

    if-eqz v11, :cond_1d

    const/4 v4, 0x1

    if-eq v8, v4, :cond_1d

    const/4 v4, 0x1

    goto :goto_12

    :cond_1d
    const/4 v4, 0x0

    :goto_12
    if-nez v2, :cond_1f

    if-nez v4, :cond_1f

    iget-object v6, v0, Landroid/view/WindowLayout;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v6, v15}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    const/4 v6, 0x5

    if-ne v5, v6, :cond_1e

    move/from16 v6, p10

    move/from16 v31, v2

    invoke-direct {v0, v15, v6}, Landroid/view/WindowLayout;->getScaledFrame(Landroid/graphics/Rect;F)Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/graphics/Rect;->intersectUnchecked(Landroid/graphics/Rect;)V

    move/from16 v32, v4

    div-float v4, v19, v6

    invoke-direct {v0, v2, v4}, Landroid/view/WindowLayout;->getScaledFrame(Landroid/graphics/Rect;F)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v15, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_13

    :cond_1e
    move/from16 v6, p10

    move/from16 v31, v2

    move/from16 v32, v4

    invoke-virtual {v15, v9}, Landroid/graphics/Rect;->intersectUnchecked(Landroid/graphics/Rect;)V

    :goto_13
    iget-object v2, v0, Landroid/view/WindowLayout;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v15}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v4, 0x1

    xor-int/2addr v2, v4

    iput-boolean v2, v7, Landroid/window/ClientWindowFrames;->isParentFrameClippedByDisplayCutout:Z

    goto :goto_14

    :cond_1f
    move/from16 v6, p10

    move/from16 v31, v2

    move/from16 v32, v4

    :goto_14
    const/4 v2, 0x5

    if-ne v5, v2, :cond_20

    invoke-direct {v0, v12, v6}, Landroid/view/WindowLayout;->getScaledFrame(Landroid/graphics/Rect;F)Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/graphics/Rect;->intersectUnchecked(Landroid/graphics/Rect;)V

    div-float v4, v19, v6

    invoke-direct {v0, v2, v4}, Landroid/view/WindowLayout;->getScaledFrame(Landroid/graphics/Rect;F)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v12, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_15

    :cond_20
    invoke-virtual {v12, v9}, Landroid/graphics/Rect;->intersectUnchecked(Landroid/graphics/Rect;)V

    goto :goto_15

    :cond_21
    move-object/from16 v30, v8

    move/from16 v34, v10

    move/from16 v8, v24

    move-object/from16 v10, p8

    move/from16 v24, v6

    move/from16 v6, p10

    :goto_15
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v2, v2, 0x200

    if-eqz v2, :cond_22

    const/4 v2, 0x1

    goto :goto_16

    :cond_22
    const/4 v2, 0x0

    :goto_16
    invoke-static/range {p5 .. p5}, Landroid/app/WindowConfiguration;->inMultiWindowMode(I)Z

    move-result v4

    if-eqz v2, :cond_23

    const/16 v14, 0x7da

    if-eq v8, v14, :cond_23

    if-nez v4, :cond_23

    const v14, -0x186a0

    iput v14, v12, Landroid/graphics/Rect;->left:I

    iput v14, v12, Landroid/graphics/Rect;->top:I

    const v14, 0x186a0

    iput v14, v12, Landroid/graphics/Rect;->right:I

    iput v14, v12, Landroid/graphics/Rect;->bottom:I

    :cond_23
    cmpl-float v14, v6, v19

    if-eqz v14, :cond_24

    const/4 v14, 0x5

    if-eq v5, v14, :cond_24

    const/4 v14, 0x1

    goto :goto_17

    :cond_24
    const/4 v14, 0x0

    :goto_17
    invoke-virtual {v15}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-virtual {v15}, Landroid/graphics/Rect;->height()I

    move-result v7

    move/from16 v31, v8

    iget v8, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    and-int/lit16 v8, v8, 0x2000

    if-eqz v8, :cond_25

    const/4 v8, 0x1

    goto :goto_18

    :cond_25
    const/4 v8, 0x0

    :goto_18
    move/from16 v17, p6

    move/from16 v19, p7

    move-object/from16 v32, v9

    const/4 v9, -0x1

    move/from16 v10, v17

    if-eq v10, v9, :cond_27

    if-eqz v8, :cond_26

    goto :goto_19

    :cond_26
    move v9, v10

    goto :goto_1a

    :cond_27
    :goto_19
    iget v9, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    if-ltz v9, :cond_28

    iget v9, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    goto :goto_1a

    :cond_28
    move v9, v5

    :goto_1a
    move/from16 v35, v11

    move/from16 v10, v19

    const/4 v11, -0x1

    if-eq v10, v11, :cond_29

    if-eqz v8, :cond_2b

    :cond_29
    iget v11, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    if-ltz v11, :cond_2a

    iget v11, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    goto :goto_1b

    :cond_2a
    move v11, v7

    :goto_1b
    move/from16 v19, v11

    move/from16 v10, v19

    :cond_2b
    iget v11, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v11, v11, 0x4000

    const/high16 v19, 0x3f000000    # 0.5f

    if-eqz v11, :cond_30

    iget v11, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    if-gez v11, :cond_2c

    move v11, v5

    goto :goto_1c

    :cond_2c
    if-eqz v14, :cond_2d

    iget v11, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    int-to-float v11, v11

    mul-float/2addr v11, v6

    add-float v11, v11, v19

    float-to-int v11, v11

    goto :goto_1c

    :cond_2d
    iget v11, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    :goto_1c
    move/from16 v17, v11

    iget v11, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    if-gez v11, :cond_2e

    move v11, v7

    move/from16 v37, v9

    move v9, v11

    move/from16 v36, v13

    move/from16 v11, v17

    goto :goto_1e

    :cond_2e
    if-eqz v14, :cond_2f

    iget v11, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    int-to-float v11, v11

    mul-float/2addr v11, v6

    add-float v11, v11, v19

    float-to-int v11, v11

    move/from16 v37, v9

    move v9, v11

    move/from16 v36, v13

    move/from16 v11, v17

    goto :goto_1e

    :cond_2f
    iget v11, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    move/from16 v37, v9

    move v9, v11

    move/from16 v36, v13

    move/from16 v11, v17

    goto :goto_1e

    :cond_30
    iget v11, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    move/from16 v36, v13

    const/4 v13, -0x1

    if-ne v11, v13, :cond_31

    move v11, v5

    goto :goto_1d

    :cond_31
    if-eqz v14, :cond_32

    int-to-float v11, v9

    mul-float/2addr v11, v6

    add-float v11, v11, v19

    float-to-int v11, v11

    goto :goto_1d

    :cond_32
    move v11, v9

    :goto_1d
    iget v13, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    move/from16 v37, v9

    const/4 v9, -0x1

    if-ne v13, v9, :cond_33

    move v9, v7

    goto :goto_1e

    :cond_33
    if-eqz v14, :cond_34

    int-to-float v9, v10

    mul-float/2addr v9, v6

    add-float v9, v9, v19

    float-to-int v9, v9

    goto :goto_1e

    :cond_34
    move v9, v10

    :goto_1e
    if-eqz v14, :cond_35

    iget v13, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    int-to-float v13, v13

    mul-float/2addr v13, v6

    move/from16 v38, v10

    iget v10, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    int-to-float v10, v10

    mul-float/2addr v10, v6

    goto :goto_1f

    :cond_35
    move/from16 v38, v10

    iget v10, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    int-to-float v13, v10

    iget v10, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    int-to-float v10, v10

    :goto_1f
    if-eqz v4, :cond_36

    iget v6, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    and-int v6, v6, v18

    if-nez v6, :cond_36

    invoke-static {v11, v5}, Ljava/lang/Math;->min(II)I

    move-result v11

    invoke-static {v9, v7}, Ljava/lang/Math;->min(II)I

    move-result v9

    :cond_36
    if-eqz v4, :cond_38

    iget v6, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    move/from16 v39, v4

    const/4 v4, 0x1

    if-eq v6, v4, :cond_37

    if-nez v2, :cond_37

    goto :goto_20

    :cond_37
    const/4 v4, 0x0

    goto :goto_20

    :cond_38
    move/from16 v39, v4

    const/4 v4, 0x1

    :goto_20
    iget v6, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    move/from16 v33, v2

    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->horizontalMargin:F

    move/from16 v40, v14

    int-to-float v14, v5

    mul-float/2addr v2, v14

    add-float/2addr v2, v13

    float-to-int v2, v2

    iget v14, v1, Landroid/view/WindowManager$LayoutParams;->verticalMargin:F

    move/from16 v41, v5

    int-to-float v5, v7

    mul-float/2addr v14, v5

    add-float/2addr v14, v10

    float-to-int v5, v14

    move-object v14, v15

    move v15, v6

    move/from16 v16, v11

    move/from16 v17, v9

    move-object/from16 v18, v14

    move/from16 v19, v2

    move/from16 v20, v5

    move-object/from16 v21, v23

    invoke-static/range {v15 .. v21}, Landroid/view/Gravity;->apply(IIILandroid/graphics/Rect;IILandroid/graphics/Rect;)V

    if-eqz v4, :cond_39

    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    move-object/from16 v5, v23

    invoke-static {v2, v12, v5}, Landroid/view/Gravity;->applyDisplay(ILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    goto :goto_21

    :cond_39
    move-object/from16 v5, v23

    :goto_21
    if-eqz v8, :cond_3a

    invoke-virtual {v3, v5}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v2

    if-nez v2, :cond_3a

    iget-object v2, v0, Landroid/view/WindowLayout;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    const/high16 v2, 0x11000000

    iget v6, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    const v15, -0x11000001

    and-int/2addr v6, v15

    iget-object v15, v0, Landroid/view/WindowLayout;->mTempRect:Landroid/graphics/Rect;

    invoke-static {v6, v3, v15}, Landroid/view/Gravity;->applyDisplay(ILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    iget-object v6, v0, Landroid/view/WindowLayout;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v6, v12}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    move-result v6

    if-eqz v6, :cond_3a

    iget-object v6, v0, Landroid/view/WindowLayout;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v5, v6}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    :cond_3a
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
