.class final Landroid/view/Surface$HwuiContext;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/Surface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "HwuiContext"
.end annotation


# instance fields
.field private mCanvas:Landroid/graphics/RecordingCanvas;

.field private mHardwareRenderer:Landroid/graphics/HardwareRenderer;

.field private final mIsWideColorGamut:Z

.field private final mRenderNode:Landroid/graphics/RenderNode;

.field final synthetic this$0:Landroid/view/Surface;


# direct methods
.method constructor <init>(Landroid/view/Surface;Z)V
    .locals 3

    iput-object p1, p0, Landroid/view/Surface$HwuiContext;->this$0:Landroid/view/Surface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "HwuiCanvas"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/graphics/RenderNode;->create(Ljava/lang/String;Landroid/graphics/RenderNode$AnimationHost;)Landroid/graphics/RenderNode;

    move-result-object v0

    iput-object v0, p0, Landroid/view/Surface$HwuiContext;->mRenderNode:Landroid/graphics/RenderNode;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/RenderNode;->setClipToBounds(Z)Z

    invoke-virtual {v0, v1}, Landroid/graphics/RenderNode;->setForceDarkAllowed(Z)Z

    iput-boolean p2, p0, Landroid/view/Surface$HwuiContext;->mIsWideColorGamut:Z

    new-instance v2, Landroid/graphics/HardwareRenderer;

    invoke-direct {v2}, Landroid/graphics/HardwareRenderer;-><init>()V

    iput-object v2, p0, Landroid/view/Surface$HwuiContext;->mHardwareRenderer:Landroid/graphics/HardwareRenderer;

    invoke-virtual {v2, v0}, Landroid/graphics/HardwareRenderer;->setContentRoot(Landroid/graphics/RenderNode;)V

    iget-object v0, p0, Landroid/view/Surface$HwuiContext;->mHardwareRenderer:Landroid/graphics/HardwareRenderer;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2}, Landroid/graphics/HardwareRenderer;->setSurface(Landroid/view/Surface;Z)V

    iget-object p1, p0, Landroid/view/Surface$HwuiContext;->mHardwareRenderer:Landroid/graphics/HardwareRenderer;

    if-eqz p2, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    nop

    :goto_0
    invoke-virtual {p1, v1}, Landroid/graphics/HardwareRenderer;->setColorMode(I)V

    iget-object p1, p0, Landroid/view/Surface$HwuiContext;->mHardwareRenderer:Landroid/graphics/HardwareRenderer;

    const/4 v0, 0x0

    invoke-virtual {p1, v0, v0}, Landroid/graphics/HardwareRenderer;->setLightSourceAlpha(FF)V

    iget-object p1, p0, Landroid/view/Surface$HwuiContext;->mHardwareRenderer:Landroid/graphics/HardwareRenderer;

    invoke-virtual {p1, v0, v0, v0, v0}, Landroid/graphics/HardwareRenderer;->setLightSourceGeometry(FFFF)V

    return-void
.end method


# virtual methods
.method destroy()V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/view/Surface$HwuiContext;->mHardwareRenderer:Landroid/graphics/HardwareRenderer;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {v0}, Landroid/graphics/HardwareRenderer;->destroy()V

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method

.method isWideColorGamut()Z
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    iget-boolean v0, p0, Landroid/view/Surface$HwuiContext;->mIsWideColorGamut:Z

    goto/32 :goto_0

    nop
.end method

.method lockCanvas(II)Landroid/graphics/Canvas;
    .locals 2

    goto/32 :goto_6

    nop

    :goto_0
    const-string v1, "Surface was already locked!"

    goto/32 :goto_3

    nop

    :goto_1
    new-instance v0, Ljava/lang/IllegalStateException;

    goto/32 :goto_0

    nop

    :goto_2
    iput-object v0, p0, Landroid/view/Surface$HwuiContext;->mCanvas:Landroid/graphics/RecordingCanvas;

    goto/32 :goto_9

    nop

    :goto_3
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_4

    nop

    :goto_4
    throw v0

    :goto_5
    invoke-virtual {v0, p1, p2}, Landroid/graphics/RenderNode;->beginRecording(II)Landroid/graphics/RecordingCanvas;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_6
    iget-object v0, p0, Landroid/view/Surface$HwuiContext;->mCanvas:Landroid/graphics/RecordingCanvas;

    goto/32 :goto_8

    nop

    :goto_7
    iget-object v0, p0, Landroid/view/Surface$HwuiContext;->mRenderNode:Landroid/graphics/RenderNode;

    goto/32 :goto_5

    nop

    :goto_8
    if-eqz v0, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_7

    nop

    :goto_9
    return-object v0

    :goto_a
    goto/32 :goto_1

    nop
.end method

.method unlockAndPost(Landroid/graphics/Canvas;)V
    .locals 3

    goto/32 :goto_a

    nop

    :goto_0
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_10

    nop

    :goto_1
    invoke-virtual {v0, v1, v2}, Landroid/graphics/HardwareRenderer$FrameRenderRequest;->setVsyncTime(J)Landroid/graphics/HardwareRenderer$FrameRenderRequest;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_2
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v1

    goto/32 :goto_1

    nop

    :goto_3
    const-string v1, "canvas object must be the same instance that was previously returned by lockCanvas"

    goto/32 :goto_0

    nop

    :goto_4
    return-void

    :goto_5
    goto/32 :goto_b

    nop

    :goto_6
    const/4 v0, 0x0

    goto/32 :goto_9

    nop

    :goto_7
    invoke-virtual {v0}, Landroid/graphics/HardwareRenderer$FrameRenderRequest;->syncAndDraw()I

    goto/32 :goto_4

    nop

    :goto_8
    iget-object v0, p0, Landroid/view/Surface$HwuiContext;->mRenderNode:Landroid/graphics/RenderNode;

    goto/32 :goto_c

    nop

    :goto_9
    iput-object v0, p0, Landroid/view/Surface$HwuiContext;->mCanvas:Landroid/graphics/RecordingCanvas;

    goto/32 :goto_f

    nop

    :goto_a
    iget-object v0, p0, Landroid/view/Surface$HwuiContext;->mCanvas:Landroid/graphics/RecordingCanvas;

    goto/32 :goto_e

    nop

    :goto_b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_3

    nop

    :goto_c
    invoke-virtual {v0}, Landroid/graphics/RenderNode;->endRecording()V

    goto/32 :goto_6

    nop

    :goto_d
    invoke-virtual {v0}, Landroid/graphics/HardwareRenderer;->createRenderRequest()Landroid/graphics/HardwareRenderer$FrameRenderRequest;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_e
    if-eq p1, v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_8

    nop

    :goto_f
    iget-object v0, p0, Landroid/view/Surface$HwuiContext;->mHardwareRenderer:Landroid/graphics/HardwareRenderer;

    goto/32 :goto_d

    nop

    :goto_10
    throw v0
.end method

.method updateSurface()V
    .locals 3

    goto/32 :goto_3

    nop

    :goto_0
    invoke-virtual {v0, v1, v2}, Landroid/graphics/HardwareRenderer;->setSurface(Landroid/view/Surface;Z)V

    goto/32 :goto_4

    nop

    :goto_1
    const/4 v2, 0x1

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v1, p0, Landroid/view/Surface$HwuiContext;->this$0:Landroid/view/Surface;

    goto/32 :goto_1

    nop

    :goto_3
    iget-object v0, p0, Landroid/view/Surface$HwuiContext;->mHardwareRenderer:Landroid/graphics/HardwareRenderer;

    goto/32 :goto_2

    nop

    :goto_4
    return-void
.end method
