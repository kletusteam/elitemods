.class final Landroid/view/FocusFinder$FocusSorter;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/FocusFinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "FocusSorter"
.end annotation


# instance fields
.field private mLastPoolRect:I

.field private mRectByView:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Landroid/view/View;",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field private mRectPool:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field private mRtlMult:I

.field private mSidesComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mTopsComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/view/FocusFinder$FocusSorter;->mRectPool:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/view/FocusFinder$FocusSorter;->mRectByView:Ljava/util/HashMap;

    new-instance v0, Landroid/view/FocusFinder$FocusSorter$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Landroid/view/FocusFinder$FocusSorter$$ExternalSyntheticLambda0;-><init>(Landroid/view/FocusFinder$FocusSorter;)V

    iput-object v0, p0, Landroid/view/FocusFinder$FocusSorter;->mTopsComparator:Ljava/util/Comparator;

    new-instance v0, Landroid/view/FocusFinder$FocusSorter$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Landroid/view/FocusFinder$FocusSorter$$ExternalSyntheticLambda1;-><init>(Landroid/view/FocusFinder$FocusSorter;)V

    iput-object v0, p0, Landroid/view/FocusFinder$FocusSorter;->mSidesComparator:Ljava/util/Comparator;

    return-void
.end method


# virtual methods
.method synthetic lambda$new$0$android-view-FocusFinder$FocusSorter(Landroid/view/View;Landroid/view/View;)I
    .locals 5

    goto/32 :goto_6

    nop

    :goto_0
    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_c

    nop

    :goto_1
    return v0

    :goto_2
    goto/32 :goto_7

    nop

    :goto_3
    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_12

    nop

    :goto_4
    iget v2, v0, Landroid/graphics/Rect;->top:I

    goto/32 :goto_a

    nop

    :goto_5
    if-eqz v2, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_0

    nop

    :goto_6
    if-eq p1, p2, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_8

    nop

    :goto_7
    iget-object v0, p0, Landroid/view/FocusFinder$FocusSorter;->mRectByView:Ljava/util/HashMap;

    goto/32 :goto_9

    nop

    :goto_8
    const/4 v0, 0x0

    goto/32 :goto_1

    nop

    :goto_9
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_11

    nop

    :goto_a
    iget v3, v1, Landroid/graphics/Rect;->top:I

    goto/32 :goto_10

    nop

    :goto_b
    sub-int/2addr v3, v4

    goto/32 :goto_d

    nop

    :goto_c
    iget v4, v1, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_b

    nop

    :goto_d
    return v3

    :goto_e
    goto/32 :goto_f

    nop

    :goto_f
    return v2

    :goto_10
    sub-int/2addr v2, v3

    goto/32 :goto_5

    nop

    :goto_11
    check-cast v0, Landroid/graphics/Rect;

    goto/32 :goto_13

    nop

    :goto_12
    check-cast v1, Landroid/graphics/Rect;

    goto/32 :goto_4

    nop

    :goto_13
    iget-object v1, p0, Landroid/view/FocusFinder$FocusSorter;->mRectByView:Ljava/util/HashMap;

    goto/32 :goto_3

    nop
.end method

.method synthetic lambda$new$1$android-view-FocusFinder$FocusSorter(Landroid/view/View;Landroid/view/View;)I
    .locals 5

    goto/32 :goto_a

    nop

    :goto_0
    iget-object v0, p0, Landroid/view/FocusFinder$FocusSorter;->mRectByView:Ljava/util/HashMap;

    goto/32 :goto_4

    nop

    :goto_1
    sub-int/2addr v2, v3

    goto/32 :goto_f

    nop

    :goto_2
    mul-int/2addr v3, v2

    goto/32 :goto_7

    nop

    :goto_3
    const/4 v0, 0x0

    goto/32 :goto_11

    nop

    :goto_4
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_15

    nop

    :goto_5
    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_6

    nop

    :goto_6
    check-cast v1, Landroid/graphics/Rect;

    goto/32 :goto_10

    nop

    :goto_7
    return v3

    :goto_8
    iget v4, v1, Landroid/graphics/Rect;->right:I

    goto/32 :goto_b

    nop

    :goto_9
    iget-object v1, p0, Landroid/view/FocusFinder$FocusSorter;->mRectByView:Ljava/util/HashMap;

    goto/32 :goto_5

    nop

    :goto_a
    if-eq p1, p2, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_3

    nop

    :goto_b
    sub-int/2addr v3, v4

    goto/32 :goto_c

    nop

    :goto_c
    return v3

    :goto_d
    goto/32 :goto_e

    nop

    :goto_e
    iget v3, p0, Landroid/view/FocusFinder$FocusSorter;->mRtlMult:I

    goto/32 :goto_2

    nop

    :goto_f
    if-eqz v2, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_13

    nop

    :goto_10
    iget v2, v0, Landroid/graphics/Rect;->left:I

    goto/32 :goto_14

    nop

    :goto_11
    return v0

    :goto_12
    goto/32 :goto_0

    nop

    :goto_13
    iget v3, v0, Landroid/graphics/Rect;->right:I

    goto/32 :goto_8

    nop

    :goto_14
    iget v3, v1, Landroid/graphics/Rect;->left:I

    goto/32 :goto_1

    nop

    :goto_15
    check-cast v0, Landroid/graphics/Rect;

    goto/32 :goto_9

    nop
.end method

.method public sort([Landroid/view/View;IILandroid/view/ViewGroup;Z)V
    .locals 7

    sub-int v0, p3, p2

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Landroid/view/FocusFinder$FocusSorter;->mRectByView:Ljava/util/HashMap;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Landroid/view/FocusFinder$FocusSorter;->mRectByView:Ljava/util/HashMap;

    :cond_1
    const/4 v1, 0x1

    if-eqz p5, :cond_2

    const/4 v2, -0x1

    goto :goto_0

    :cond_2
    move v2, v1

    :goto_0
    iput v2, p0, Landroid/view/FocusFinder$FocusSorter;->mRtlMult:I

    iget-object v2, p0, Landroid/view/FocusFinder$FocusSorter;->mRectPool:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_1
    if-ge v2, v0, :cond_3

    iget-object v3, p0, Landroid/view/FocusFinder$FocusSorter;->mRectPool:Ljava/util/ArrayList;

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    move v2, p2

    :goto_2
    if-ge v2, p3, :cond_4

    iget-object v3, p0, Landroid/view/FocusFinder$FocusSorter;->mRectPool:Ljava/util/ArrayList;

    iget v4, p0, Landroid/view/FocusFinder$FocusSorter;->mLastPoolRect:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Landroid/view/FocusFinder$FocusSorter;->mLastPoolRect:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Rect;

    aget-object v4, p1, v2

    invoke-virtual {v4, v3}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    aget-object v4, p1, v2

    invoke-virtual {p4, v4, v3}, Landroid/view/ViewGroup;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    iget-object v4, p0, Landroid/view/FocusFinder$FocusSorter;->mRectByView:Ljava/util/HashMap;

    aget-object v5, p1, v2

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    iget-object v2, p0, Landroid/view/FocusFinder$FocusSorter;->mTopsComparator:Ljava/util/Comparator;

    invoke-static {p1, p2, v0, v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;IILjava/util/Comparator;)V

    iget-object v2, p0, Landroid/view/FocusFinder$FocusSorter;->mRectByView:Ljava/util/HashMap;

    aget-object v3, p1, p2

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    move v3, p2

    add-int/lit8 v4, p2, 0x1

    :goto_3
    if-ge v4, p3, :cond_7

    iget-object v5, p0, Landroid/view/FocusFinder$FocusSorter;->mRectByView:Ljava/util/HashMap;

    aget-object v6, p1, v4

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Rect;

    iget v6, v5, Landroid/graphics/Rect;->top:I

    if-lt v6, v2, :cond_6

    sub-int v6, v4, v3

    if-le v6, v1, :cond_5

    iget-object v6, p0, Landroid/view/FocusFinder$FocusSorter;->mSidesComparator:Ljava/util/Comparator;

    invoke-static {p1, v3, v4, v6}, Ljava/util/Arrays;->sort([Ljava/lang/Object;IILjava/util/Comparator;)V

    :cond_5
    iget v2, v5, Landroid/graphics/Rect;->bottom:I

    move v3, v4

    goto :goto_4

    :cond_6
    iget v6, v5, Landroid/graphics/Rect;->bottom:I

    invoke-static {v2, v6}, Ljava/lang/Math;->max(II)I

    move-result v2

    :goto_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_7
    sub-int v5, v4, v3

    if-le v5, v1, :cond_8

    iget-object v1, p0, Landroid/view/FocusFinder$FocusSorter;->mSidesComparator:Ljava/util/Comparator;

    invoke-static {p1, v3, v4, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;IILjava/util/Comparator;)V

    :cond_8
    const/4 v1, 0x0

    iput v1, p0, Landroid/view/FocusFinder$FocusSorter;->mLastPoolRect:I

    iget-object v1, p0, Landroid/view/FocusFinder$FocusSorter;->mRectByView:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    return-void
.end method
