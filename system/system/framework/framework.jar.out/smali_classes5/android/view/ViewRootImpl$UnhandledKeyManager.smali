.class Landroid/view/ViewRootImpl$UnhandledKeyManager;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/ViewRootImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UnhandledKeyManager"
.end annotation


# instance fields
.field private final mCapturedKeys:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation
.end field

.field private mCurrentReceiver:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mDispatched:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/view/ViewRootImpl$UnhandledKeyManager;->mDispatched:Z

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Landroid/view/ViewRootImpl$UnhandledKeyManager;->mCapturedKeys:Landroid/util/SparseArray;

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/view/ViewRootImpl$UnhandledKeyManager;->mCurrentReceiver:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method synthetic constructor <init>(Landroid/view/ViewRootImpl$UnhandledKeyManager-IA;)V
    .locals 0

    invoke-direct {p0}, Landroid/view/ViewRootImpl$UnhandledKeyManager;-><init>()V

    return-void
.end method


# virtual methods
.method dispatch(Landroid/view/View;Landroid/view/KeyEvent;)Z
    .locals 8

    goto/32 :goto_3

    nop

    :goto_0
    invoke-static {v2, v3}, Landroid/os/Trace;->traceEnd(J)V

    goto/32 :goto_6

    nop

    :goto_1
    const/4 v1, 0x0

    goto/32 :goto_8

    nop

    :goto_2
    if-nez v4, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_a

    nop

    :goto_3
    iget-boolean v0, p0, Landroid/view/ViewRootImpl$UnhandledKeyManager;->mDispatched:Z

    goto/32 :goto_1

    nop

    :goto_4
    return v1

    :goto_5
    goto/32 :goto_c

    nop

    :goto_6
    throw v0

    :goto_7
    invoke-static {v2, v3}, Landroid/os/Trace;->traceEnd(J)V

    nop

    goto/32 :goto_2

    nop

    :goto_8
    if-nez v0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_4

    nop

    :goto_9
    return v1

    :catchall_0
    move-exception v0

    goto/32 :goto_0

    nop

    :goto_a
    move v1, v0

    :goto_b
    goto/32 :goto_9

    nop

    :goto_c
    const-wide/16 v2, 0x8

    :try_start_0
    const-string v0, "UnhandledKeyEvent dispatch"

    invoke-static {v2, v3, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/view/ViewRootImpl$UnhandledKeyManager;->mDispatched:Z

    invoke-virtual {p1, p2}, Landroid/view/View;->dispatchUnhandledKeyEvent(Landroid/view/KeyEvent;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v5

    if-eqz v4, :cond_2

    invoke-static {v5}, Landroid/view/KeyEvent;->isModifierKey(I)Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Landroid/view/ViewRootImpl$UnhandledKeyManager;->mCapturedKeys:Landroid/util/SparseArray;

    new-instance v7, Ljava/lang/ref/WeakReference;

    invoke-direct {v7, v4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v6, v5, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    goto/32 :goto_7

    nop
.end method

.method preDispatch(Landroid/view/KeyEvent;)V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    iput-object v1, p0, Landroid/view/ViewRootImpl$UnhandledKeyManager;->mCurrentReceiver:Ljava/lang/ref/WeakReference;

    goto/32 :goto_4

    nop

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_6

    nop

    :goto_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    goto/32 :goto_c

    nop

    :goto_3
    return-void

    :goto_4
    iget-object v1, p0, Landroid/view/ViewRootImpl$UnhandledKeyManager;->mCapturedKeys:Landroid/util/SparseArray;

    goto/32 :goto_d

    nop

    :goto_5
    check-cast v1, Ljava/lang/ref/WeakReference;

    goto/32 :goto_0

    nop

    :goto_6
    iput-object v0, p0, Landroid/view/ViewRootImpl$UnhandledKeyManager;->mCurrentReceiver:Ljava/lang/ref/WeakReference;

    goto/32 :goto_2

    nop

    :goto_7
    iget-object v1, p0, Landroid/view/ViewRootImpl$UnhandledKeyManager;->mCapturedKeys:Landroid/util/SparseArray;

    goto/32 :goto_8

    nop

    :goto_8
    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_5

    nop

    :goto_9
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    goto/32 :goto_10

    nop

    :goto_a
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    goto/32 :goto_9

    nop

    :goto_b
    iget-object v0, p0, Landroid/view/ViewRootImpl$UnhandledKeyManager;->mCapturedKeys:Landroid/util/SparseArray;

    goto/32 :goto_a

    nop

    :goto_c
    const/4 v1, 0x1

    goto/32 :goto_f

    nop

    :goto_d
    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->removeAt(I)V

    :goto_e
    goto/32 :goto_3

    nop

    :goto_f
    if-eq v0, v1, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_b

    nop

    :goto_10
    if-gez v0, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_7

    nop
.end method

.method preViewDispatch(Landroid/view/KeyEvent;)Z
    .locals 3

    goto/32 :goto_13

    nop

    :goto_0
    const/4 v2, 0x1

    goto/32 :goto_4

    nop

    :goto_1
    if-nez v1, :cond_0

    goto/32 :goto_17

    :cond_0
    goto/32 :goto_7

    nop

    :goto_2
    invoke-virtual {v0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v1

    goto/32 :goto_9

    nop

    :goto_3
    iget-object v1, p0, Landroid/view/ViewRootImpl$UnhandledKeyManager;->mCapturedKeys:Landroid/util/SparseArray;

    goto/32 :goto_5

    nop

    :goto_4
    if-eq v1, v2, :cond_1

    goto/32 :goto_1a

    :cond_1
    goto/32 :goto_12

    nop

    :goto_5
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    goto/32 :goto_c

    nop

    :goto_6
    check-cast v1, Ljava/lang/ref/WeakReference;

    goto/32 :goto_a

    nop

    :goto_7
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_f

    nop

    :goto_8
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    goto/32 :goto_0

    nop

    :goto_9
    if-nez v1, :cond_2

    goto/32 :goto_15

    :cond_2
    goto/32 :goto_14

    nop

    :goto_a
    iput-object v1, p0, Landroid/view/ViewRootImpl$UnhandledKeyManager;->mCurrentReceiver:Ljava/lang/ref/WeakReference;

    :goto_b
    goto/32 :goto_d

    nop

    :goto_c
    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_6

    nop

    :goto_d
    iget-object v1, p0, Landroid/view/ViewRootImpl$UnhandledKeyManager;->mCurrentReceiver:Ljava/lang/ref/WeakReference;

    goto/32 :goto_1

    nop

    :goto_e
    if-eqz v1, :cond_3

    goto/32 :goto_b

    :cond_3
    goto/32 :goto_3

    nop

    :goto_f
    check-cast v0, Landroid/view/View;

    goto/32 :goto_8

    nop

    :goto_10
    iget-object v1, p0, Landroid/view/ViewRootImpl$UnhandledKeyManager;->mCurrentReceiver:Ljava/lang/ref/WeakReference;

    goto/32 :goto_e

    nop

    :goto_11
    if-nez v0, :cond_4

    goto/32 :goto_15

    :cond_4
    goto/32 :goto_2

    nop

    :goto_12
    const/4 v1, 0x0

    goto/32 :goto_19

    nop

    :goto_13
    const/4 v0, 0x0

    goto/32 :goto_1b

    nop

    :goto_14
    invoke-virtual {v0, p1}, Landroid/view/View;->onUnhandledKeyEvent(Landroid/view/KeyEvent;)Z

    :goto_15
    goto/32 :goto_16

    nop

    :goto_16
    return v2

    :goto_17
    goto/32 :goto_18

    nop

    :goto_18
    return v0

    :goto_19
    iput-object v1, p0, Landroid/view/ViewRootImpl$UnhandledKeyManager;->mCurrentReceiver:Ljava/lang/ref/WeakReference;

    :goto_1a
    goto/32 :goto_11

    nop

    :goto_1b
    iput-boolean v0, p0, Landroid/view/ViewRootImpl$UnhandledKeyManager;->mDispatched:Z

    goto/32 :goto_10

    nop
.end method
