.class public interface abstract Landroid/view/Window$DecorCallback;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/Window;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DecorCallback"
.end annotation


# virtual methods
.method public abstract onDrawLegacyNavigationBarBackgroundChanged(Z)Z
.end method

.method public abstract onSystemBarAppearanceChanged(I)V
.end method
