.class public interface abstract Landroid/view/ViewRootImpl$IDragEndInformation;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/view/ViewRootImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IDragEndInformation"
.end annotation


# virtual methods
.method public abstract getDragEndHeight()I
.end method

.method public abstract getDragEndLocation()[I
.end method

.method public abstract getDragEndWidth()I
.end method
