.class public final Landroid/view/Choreographer;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/view/Choreographer$CallbackQueue;,
        Landroid/view/Choreographer$CallbackRecord;,
        Landroid/view/Choreographer$FrameDisplayEventReceiver;,
        Landroid/view/Choreographer$FrameHandler;,
        Landroid/view/Choreographer$VsyncCallback;,
        Landroid/view/Choreographer$FrameData;,
        Landroid/view/Choreographer$FrameTimeline;,
        Landroid/view/Choreographer$FrameCallback;
    }
.end annotation


# static fields
.field public static final CALLBACK_ANIMATION:I = 0x1

.field public static final CALLBACK_COMMIT:I = 0x4

.field public static final CALLBACK_INPUT:I = 0x0

.field public static final CALLBACK_INSETS_ANIMATION:I = 0x2

.field private static final CALLBACK_LAST:I = 0x4

.field private static final CALLBACK_TRACE_TITLES:[Ljava/lang/String;

.field public static final CALLBACK_TRAVERSAL:I = 0x3

.field private static final DEBUG_FRAMES:Z = false

.field private static final DEBUG_JANK:Z = false

.field private static final DEFAULT_FRAME_DELAY:J = 0xaL

.field private static final FRAME_CALLBACK_TOKEN:Ljava/lang/Object;

.field private static final MOTION_EVENT_ACTION_CANCEL:I = 0x3

.field private static final MOTION_EVENT_ACTION_DOWN:I = 0x0

.field private static final MOTION_EVENT_ACTION_MOVE:I = 0x2

.field private static final MOTION_EVENT_ACTION_UP:I = 0x1

.field private static final MSG_CACHE_BUFFER_DO_FRAME:I = 0x64

.field private static final MSG_DO_FRAME:I = 0x0

.field private static final MSG_DO_SCHEDULE_CALLBACK:I = 0x2

.field private static final MSG_DO_SCHEDULE_VSYNC:I = 0x1

.field private static final OPTS_INPUT:Z = false

.field private static final SKIPPED_FRAME_WARNING_LIMIT:I

.field private static final TAG:Ljava/lang/String; = "Choreographer"

.field private static final USE_FRAME_TIME:Z

.field private static final USE_VSYNC:Z

.field private static final VSYNC_CALLBACK_TOKEN:Ljava/lang/Object;

.field private static volatile mMainInstance:Landroid/view/Choreographer;

.field private static volatile sFrameDelay:J

.field private static final sSfThreadInstance:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Landroid/view/Choreographer;",
            ">;"
        }
    .end annotation
.end field

.field private static final sThreadInstance:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Landroid/view/Choreographer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCallbackPool:Landroid/view/Choreographer$CallbackRecord;

.field private final mCallbackQueues:[Landroid/view/Choreographer$CallbackQueue;

.field private mCallbacksRunning:Z

.field private mChoreographerMonitor:Landroid/os/perfdebug/ChoreographerMonitor;

.field private mConsumedDown:Z

.field private mConsumedMove:Z

.field private mDebugPrintNextFrameTimeDelta:Z

.field private final mDisplayEventReceiver:Landroid/view/Choreographer$FrameDisplayEventReceiver;

.field private mFPSDivisor:I

.field private mFirstVSync:Z

.field mFrameInfo:Landroid/graphics/FrameInfo;

.field private mFrameIntervalNanos:J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private mFrameScheduled:Z

.field private final mHandler:Landroid/view/Choreographer$FrameHandler;

.field private mIsDoFrameProcessing:Z

.field private mIsVsyncScheduled:Z

.field private mLastFrameIntervalNanos:J

.field private mLastFrameTimeNanos:J

.field private mLastTouchOptTimeNanos:J

.field private mLastVsyncEventData:Landroid/view/DisplayEventReceiver$VsyncEventData;

.field private final mLock:Ljava/lang/Object;

.field private final mLooper:Landroid/os/Looper;

.field private mMotionEventType:I

.field private mTouchMoveNum:I


# direct methods
.method static bridge synthetic -$$Nest$fgetmFirstVSync(Landroid/view/Choreographer;)Z
    .locals 0

    iget-boolean p0, p0, Landroid/view/Choreographer;->mFirstVSync:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Landroid/view/Choreographer;)Landroid/view/Choreographer$FrameHandler;
    .locals 0

    iget-object p0, p0, Landroid/view/Choreographer;->mHandler:Landroid/view/Choreographer$FrameHandler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmFirstVSync(Landroid/view/Choreographer;Z)V
    .locals 0

    iput-boolean p1, p0, Landroid/view/Choreographer;->mFirstVSync:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmFrameScheduled(Landroid/view/Choreographer;Z)V
    .locals 0

    iput-boolean p1, p0, Landroid/view/Choreographer;->mFrameScheduled:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mobtainCallbackLocked(Landroid/view/Choreographer;JLjava/lang/Object;Ljava/lang/Object;)Landroid/view/Choreographer$CallbackRecord;
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/view/Choreographer;->obtainCallbackLocked(JLjava/lang/Object;Ljava/lang/Object;)Landroid/view/Choreographer$CallbackRecord;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mrecycleCallbackLocked(Landroid/view/Choreographer;Landroid/view/Choreographer$CallbackRecord;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/view/Choreographer;->recycleCallbackLocked(Landroid/view/Choreographer$CallbackRecord;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetFRAME_CALLBACK_TOKEN()Ljava/lang/Object;
    .locals 1

    sget-object v0, Landroid/view/Choreographer;->FRAME_CALLBACK_TOKEN:Ljava/lang/Object;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetVSYNC_CALLBACK_TOKEN()Ljava/lang/Object;
    .locals 1

    sget-object v0, Landroid/view/Choreographer;->VSYNC_CALLBACK_TOKEN:Ljava/lang/Object;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfputmMainInstance(Landroid/view/Choreographer;)V
    .locals 0

    sput-object p0, Landroid/view/Choreographer;->mMainInstance:Landroid/view/Choreographer;

    return-void
.end method

.method static constructor <clinit>()V
    .locals 5

    const-wide/16 v0, 0xa

    sput-wide v0, Landroid/view/Choreographer;->sFrameDelay:J

    new-instance v0, Landroid/view/Choreographer$1;

    invoke-direct {v0}, Landroid/view/Choreographer$1;-><init>()V

    sput-object v0, Landroid/view/Choreographer;->sThreadInstance:Ljava/lang/ThreadLocal;

    new-instance v0, Landroid/view/Choreographer$2;

    invoke-direct {v0}, Landroid/view/Choreographer$2;-><init>()V

    sput-object v0, Landroid/view/Choreographer;->sSfThreadInstance:Ljava/lang/ThreadLocal;

    const-string v0, "debug.choreographer.vsync"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Landroid/view/Choreographer;->USE_VSYNC:Z

    const-string v0, "debug.choreographer.frametime"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Landroid/view/Choreographer;->USE_FRAME_TIME:Z

    const-string v0, "debug.choreographer.skipwarning"

    const/16 v1, 0x1e

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Landroid/view/Choreographer;->SKIPPED_FRAME_WARNING_LIMIT:I

    new-instance v0, Landroid/view/Choreographer$3;

    invoke-direct {v0}, Landroid/view/Choreographer$3;-><init>()V

    sput-object v0, Landroid/view/Choreographer;->FRAME_CALLBACK_TOKEN:Ljava/lang/Object;

    new-instance v0, Landroid/view/Choreographer$4;

    invoke-direct {v0}, Landroid/view/Choreographer$4;-><init>()V

    sput-object v0, Landroid/view/Choreographer;->VSYNC_CALLBACK_TOKEN:Ljava/lang/Object;

    const-string v0, "input"

    const-string v1, "animation"

    const-string v2, "insets_animation"

    const-string/jumbo v3, "traversal"

    const-string v4, "commit"

    filled-new-array {v0, v1, v2, v3, v4}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/view/Choreographer;->CALLBACK_TRACE_TITLES:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/os/Looper;I)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Landroid/view/Choreographer;->mLock:Ljava/lang/Object;

    const/4 v0, 0x1

    iput v0, p0, Landroid/view/Choreographer;->mFPSDivisor:I

    new-instance v1, Landroid/view/DisplayEventReceiver$VsyncEventData;

    invoke-direct {v1}, Landroid/view/DisplayEventReceiver$VsyncEventData;-><init>()V

    iput-object v1, p0, Landroid/view/Choreographer;->mLastVsyncEventData:Landroid/view/DisplayEventReceiver$VsyncEventData;

    const/4 v1, -0x1

    iput v1, p0, Landroid/view/Choreographer;->mTouchMoveNum:I

    iput v1, p0, Landroid/view/Choreographer;->mMotionEventType:I

    const/4 v1, 0x0

    iput-boolean v1, p0, Landroid/view/Choreographer;->mConsumedMove:Z

    iput-boolean v1, p0, Landroid/view/Choreographer;->mConsumedDown:Z

    iput-boolean v1, p0, Landroid/view/Choreographer;->mIsVsyncScheduled:Z

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Landroid/view/Choreographer;->mLastTouchOptTimeNanos:J

    iput-boolean v1, p0, Landroid/view/Choreographer;->mIsDoFrameProcessing:Z

    new-instance v2, Landroid/graphics/FrameInfo;

    invoke-direct {v2}, Landroid/graphics/FrameInfo;-><init>()V

    iput-object v2, p0, Landroid/view/Choreographer;->mFrameInfo:Landroid/graphics/FrameInfo;

    iput-boolean v1, p0, Landroid/view/Choreographer;->mFirstVSync:Z

    invoke-static {}, Landroid/os/perfdebug/ChoreographerMonitor;->newChoreographerMonitor()Landroid/os/perfdebug/ChoreographerMonitor;

    move-result-object v1

    iput-object v1, p0, Landroid/view/Choreographer;->mChoreographerMonitor:Landroid/os/perfdebug/ChoreographerMonitor;

    iput-object p1, p0, Landroid/view/Choreographer;->mLooper:Landroid/os/Looper;

    new-instance v1, Landroid/view/Choreographer$FrameHandler;

    invoke-direct {v1, p0, p1}, Landroid/view/Choreographer$FrameHandler;-><init>(Landroid/view/Choreographer;Landroid/os/Looper;)V

    iput-object v1, p0, Landroid/view/Choreographer;->mHandler:Landroid/view/Choreographer$FrameHandler;

    sget-boolean v1, Landroid/view/Choreographer;->USE_VSYNC:Z

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    new-instance v1, Landroid/view/Choreographer$FrameDisplayEventReceiver;

    invoke-direct {v1, p0, p1, p2}, Landroid/view/Choreographer$FrameDisplayEventReceiver;-><init>(Landroid/view/Choreographer;Landroid/os/Looper;I)V

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    iput-object v1, p0, Landroid/view/Choreographer;->mDisplayEventReceiver:Landroid/view/Choreographer$FrameDisplayEventReceiver;

    const-wide/high16 v3, -0x8000000000000000L

    iput-wide v3, p0, Landroid/view/Choreographer;->mLastFrameTimeNanos:J

    const v1, 0x4e6e6b28    # 1.0E9f

    invoke-static {}, Landroid/view/Choreographer;->getRefreshRate()F

    move-result v3

    div-float/2addr v1, v3

    float-to-long v3, v1

    iput-wide v3, p0, Landroid/view/Choreographer;->mFrameIntervalNanos:J

    invoke-static {v3, v4}, Landroid/util/BoostFramework$ScrollOptimizer;->setFrameInterval(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Landroid/view/Choreographer$CallbackQueue;

    iput-object v1, p0, Landroid/view/Choreographer;->mCallbackQueues:[Landroid/view/Choreographer$CallbackQueue;

    const/4 v1, 0x0

    :goto_1
    const/4 v3, 0x4

    if-gt v1, v3, :cond_1

    iget-object v3, p0, Landroid/view/Choreographer;->mCallbackQueues:[Landroid/view/Choreographer$CallbackQueue;

    new-instance v4, Landroid/view/Choreographer$CallbackQueue;

    invoke-direct {v4, p0, v2}, Landroid/view/Choreographer$CallbackQueue;-><init>(Landroid/view/Choreographer;Landroid/view/Choreographer$CallbackQueue-IA;)V

    aput-object v4, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    const-string v1, "debug.hwui.fps_divisor"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/Choreographer;->setFPSDivisor(I)V

    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v0

    invoke-interface {v0}, Landroid/os/TurboSchedMonitor;->isCoreAppOptimizerEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/os/TurboSchedMonitor;->addTid(I)V

    :cond_2
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Looper;ILandroid/view/Choreographer-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/view/Choreographer;-><init>(Landroid/os/Looper;I)V

    return-void
.end method

.method private dispose()V
    .locals 1

    iget-object v0, p0, Landroid/view/Choreographer;->mDisplayEventReceiver:Landroid/view/Choreographer$FrameDisplayEventReceiver;

    invoke-virtual {v0}, Landroid/view/Choreographer$FrameDisplayEventReceiver;->dispose()V

    return-void
.end method

.method public static getFrameDelay()J
    .locals 2

    sget-wide v0, Landroid/view/Choreographer;->sFrameDelay:J

    return-wide v0
.end method

.method public static getInstance()Landroid/view/Choreographer;
    .locals 1

    sget-object v0, Landroid/view/Choreographer;->sThreadInstance:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/Choreographer;

    return-object v0
.end method

.method public static getMainThreadInstance()Landroid/view/Choreographer;
    .locals 1

    sget-object v0, Landroid/view/Choreographer;->mMainInstance:Landroid/view/Choreographer;

    return-object v0
.end method

.method private static getRefreshRate()F
    .locals 2

    invoke-static {}, Landroid/hardware/display/DisplayManagerGlobal;->getInstance()Landroid/hardware/display/DisplayManagerGlobal;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManagerGlobal;->getDisplayInfo(I)Landroid/view/DisplayInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/DisplayInfo;->getRefreshRate()F

    move-result v1

    return v1
.end method

.method public static getSfInstance()Landroid/view/Choreographer;
    .locals 1

    sget-object v0, Landroid/view/Choreographer;->sSfThreadInstance:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/Choreographer;

    return-object v0
.end method

.method private isRunningOnLooperThreadLocked()Z
    .locals 2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, Landroid/view/Choreographer;->mLooper:Landroid/os/Looper;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private obtainCallbackLocked(JLjava/lang/Object;Ljava/lang/Object;)Landroid/view/Choreographer$CallbackRecord;
    .locals 3

    iget-object v0, p0, Landroid/view/Choreographer;->mCallbackPool:Landroid/view/Choreographer$CallbackRecord;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    new-instance v2, Landroid/view/Choreographer$CallbackRecord;

    invoke-direct {v2, v1}, Landroid/view/Choreographer$CallbackRecord;-><init>(Landroid/view/Choreographer$CallbackRecord-IA;)V

    move-object v0, v2

    goto :goto_0

    :cond_0
    iget-object v2, v0, Landroid/view/Choreographer$CallbackRecord;->next:Landroid/view/Choreographer$CallbackRecord;

    iput-object v2, p0, Landroid/view/Choreographer;->mCallbackPool:Landroid/view/Choreographer$CallbackRecord;

    iput-object v1, v0, Landroid/view/Choreographer$CallbackRecord;->next:Landroid/view/Choreographer$CallbackRecord;

    :goto_0
    iput-wide p1, v0, Landroid/view/Choreographer$CallbackRecord;->dueTime:J

    iput-object p3, v0, Landroid/view/Choreographer$CallbackRecord;->action:Ljava/lang/Object;

    iput-object p4, v0, Landroid/view/Choreographer$CallbackRecord;->token:Ljava/lang/Object;

    return-object v0
.end method

.method private postCallbackDelayedInternal(ILjava/lang/Object;Ljava/lang/Object;J)V
    .locals 7

    iget-object v0, p0, Landroid/view/Choreographer;->mLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    add-long v3, v1, p4

    iget-object v5, p0, Landroid/view/Choreographer;->mCallbackQueues:[Landroid/view/Choreographer$CallbackQueue;

    aget-object v5, v5, p1

    invoke-virtual {v5, v3, v4, p2, p3}, Landroid/view/Choreographer$CallbackQueue;->addCallbackLocked(JLjava/lang/Object;Ljava/lang/Object;)V

    invoke-static {}, Landroid/view/animation/ChoreographerInjectorStub;->getInstance()Landroid/view/animation/ChoreographerInjectorStub;

    move-result-object v5

    invoke-interface {v5, p1}, Landroid/view/animation/ChoreographerInjectorStub;->disableAnimation(I)Z

    move-result v5

    if-eqz v5, :cond_0

    sget-object v5, Landroid/view/Choreographer;->mMainInstance:Landroid/view/Choreographer;

    if-ne v5, p0, :cond_0

    monitor-exit v0

    return-void

    :cond_0
    cmp-long v5, v3, v1

    if-gtz v5, :cond_1

    invoke-direct {p0, v1, v2}, Landroid/view/Choreographer;->scheduleFrameLocked(J)V

    goto :goto_0

    :cond_1
    iget-object v5, p0, Landroid/view/Choreographer;->mHandler:Landroid/view/Choreographer$FrameHandler;

    const/4 v6, 0x2

    invoke-virtual {v5, v6, p2}, Landroid/view/Choreographer$FrameHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    iput p1, v5, Landroid/os/Message;->arg1:I

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/os/Message;->setAsynchronous(Z)V

    iget-object v6, p0, Landroid/view/Choreographer;->mHandler:Landroid/view/Choreographer$FrameHandler;

    invoke-virtual {v6, v5, v3, v4}, Landroid/view/Choreographer$FrameHandler;->sendMessageAtTime(Landroid/os/Message;J)Z

    :goto_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private recycleCallbackLocked(Landroid/view/Choreographer$CallbackRecord;)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p1, Landroid/view/Choreographer$CallbackRecord;->action:Ljava/lang/Object;

    iput-object v0, p1, Landroid/view/Choreographer$CallbackRecord;->token:Ljava/lang/Object;

    iget-object v0, p0, Landroid/view/Choreographer;->mCallbackPool:Landroid/view/Choreographer$CallbackRecord;

    iput-object v0, p1, Landroid/view/Choreographer$CallbackRecord;->next:Landroid/view/Choreographer$CallbackRecord;

    iput-object p1, p0, Landroid/view/Choreographer;->mCallbackPool:Landroid/view/Choreographer$CallbackRecord;

    return-void
.end method

.method public static releaseInstance()V
    .locals 2

    sget-object v0, Landroid/view/Choreographer;->sThreadInstance:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/Choreographer;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->remove()V

    invoke-direct {v1}, Landroid/view/Choreographer;->dispose()V

    return-void
.end method

.method private removeCallbacksInternal(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    iget-object v0, p0, Landroid/view/Choreographer;->mLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Landroid/view/Choreographer;->mCallbackQueues:[Landroid/view/Choreographer$CallbackQueue;

    aget-object v1, v1, p1

    invoke-virtual {v1, p2, p3}, Landroid/view/Choreographer$CallbackQueue;->removeCallbacksLocked(Ljava/lang/Object;Ljava/lang/Object;)V

    if-eqz p2, :cond_0

    if-nez p3, :cond_0

    iget-object v1, p0, Landroid/view/Choreographer;->mHandler:Landroid/view/Choreographer$FrameHandler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p2}, Landroid/view/Choreographer$FrameHandler;->removeMessages(ILjava/lang/Object;)V

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private scheduleFrameLocked(J)V
    .locals 5

    iget-boolean v0, p0, Landroid/view/Choreographer;->mFrameScheduled:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/view/Choreographer;->mFrameScheduled:Z

    sget-boolean v1, Landroid/view/Choreographer;->USE_VSYNC:Z

    invoke-static {v1}, Landroid/util/BoostFramework$ScrollOptimizer;->shouldUseVsync(Z)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Landroid/view/Choreographer;->isRunningOnLooperThreadLocked()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Landroid/view/Choreographer;->scheduleVsyncLocked()V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Landroid/view/Choreographer;->mHandler:Landroid/view/Choreographer$FrameHandler;

    invoke-virtual {v1, v0}, Landroid/view/Choreographer$FrameHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Message;->setAsynchronous(Z)V

    iget-object v0, p0, Landroid/view/Choreographer;->mHandler:Landroid/view/Choreographer$FrameHandler;

    invoke-virtual {v0, v1}, Landroid/view/Choreographer$FrameHandler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    goto :goto_0

    :cond_1
    sget-wide v1, Landroid/view/Choreographer;->sFrameDelay:J

    iget-wide v3, p0, Landroid/view/Choreographer;->mLastFrameTimeNanos:J

    invoke-static {v1, v2, v3, v4}, Landroid/util/BoostFramework$ScrollOptimizer;->getFrameDelay(JJ)J

    move-result-wide v1

    sput-wide v1, Landroid/view/Choreographer;->sFrameDelay:J

    iget-wide v1, p0, Landroid/view/Choreographer;->mLastFrameTimeNanos:J

    const-wide/32 v3, 0xf4240

    div-long/2addr v1, v3

    sget-wide v3, Landroid/view/Choreographer;->sFrameDelay:J

    add-long/2addr v1, v3

    invoke-static {v1, v2, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    iget-object v3, p0, Landroid/view/Choreographer;->mHandler:Landroid/view/Choreographer$FrameHandler;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/Choreographer$FrameHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/os/Message;->setAsynchronous(Z)V

    iget-object v0, p0, Landroid/view/Choreographer;->mHandler:Landroid/view/Choreographer$FrameHandler;

    invoke-virtual {v0, v3, v1, v2}, Landroid/view/Choreographer$FrameHandler;->sendMessageAtTime(Landroid/os/Message;J)Z

    :cond_2
    :goto_0
    return-void
.end method

.method private scheduleVsyncLocked()V
    .locals 3

    const-wide/16 v0, 0x8

    :try_start_0
    const-string v2, "Choreographer#scheduleVsyncLocked"

    invoke-static {v0, v1, v2}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    iget-object v2, p0, Landroid/view/Choreographer;->mDisplayEventReceiver:Landroid/view/Choreographer$FrameDisplayEventReceiver;

    invoke-virtual {v2}, Landroid/view/Choreographer$FrameDisplayEventReceiver;->scheduleVsync()V

    const/4 v2, 0x1

    iput-boolean v2, p0, Landroid/view/Choreographer;->mIsVsyncScheduled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v0, v1}, Landroid/os/Trace;->traceEnd(J)V

    nop

    return-void

    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Trace;->traceEnd(J)V

    throw v2
.end method

.method public static setFrameDelay(J)V
    .locals 0

    sput-wide p0, Landroid/view/Choreographer;->sFrameDelay:J

    return-void
.end method

.method public static subtractFrameDelay(J)J
    .locals 4

    sget-wide v0, Landroid/view/Choreographer;->sFrameDelay:J

    cmp-long v2, p0, v0

    if-gtz v2, :cond_0

    const-wide/16 v2, 0x0

    goto :goto_0

    :cond_0
    sub-long v2, p0, v0

    :goto_0
    return-wide v2
.end method

.method private traceMessage(Ljava/lang/String;)V
    .locals 2

    const-wide/16 v0, 0x8

    invoke-static {v0, v1, p1}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    invoke-static {v0, v1}, Landroid/os/Trace;->traceEnd(J)V

    return-void
.end method


# virtual methods
.method doCallbacks(ILandroid/view/Choreographer$FrameData;J)V
    .locals 17

    goto/32 :goto_5

    nop

    :goto_0
    const/4 v6, 0x0

    :try_start_0
    sget-object v7, Landroid/view/Choreographer;->CALLBACK_TRACE_TITLES:[Ljava/lang/String;

    aget-object v7, v7, v2

    invoke-static {v11, v12, v7}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    move-object v7, v9

    :goto_1
    if-eqz v7, :cond_1

    iget-object v8, v1, Landroid/view/Choreographer;->mChoreographerMonitor:Landroid/os/perfdebug/ChoreographerMonitor;

    iget-object v10, v7, Landroid/view/Choreographer$CallbackRecord;->action:Ljava/lang/Object;

    iget-wide v13, v7, Landroid/view/Choreographer$CallbackRecord;->dueTime:J

    invoke-virtual {v8, v2, v10, v13, v14}, Landroid/os/perfdebug/ChoreographerMonitor;->monitorCallbackExecuteBegin(ILjava/lang/Object;J)V

    if-ne v2, v0, :cond_0

    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v8

    iget-object v10, v7, Landroid/view/Choreographer$CallbackRecord;->action:Ljava/lang/Object;

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v8, v10}, Landroid/os/TurboSchedMonitor;->monitorAnimationCallback(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v7, v3}, Landroid/view/Choreographer$CallbackRecord;->run(Landroid/view/Choreographer$FrameData;)V

    iget-object v8, v1, Landroid/view/Choreographer;->mChoreographerMonitor:Landroid/os/perfdebug/ChoreographerMonitor;

    invoke-virtual {v8}, Landroid/os/perfdebug/ChoreographerMonitor;->monitorCallbackExecuteEnd()V

    iget-object v8, v7, Landroid/view/Choreographer$CallbackRecord;->next:Landroid/view/Choreographer$CallbackRecord;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto/32 :goto_d

    nop

    :goto_2
    throw v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/32 :goto_11

    nop

    :goto_3
    iget-object v6, v1, Landroid/view/Choreographer;->mLock:Ljava/lang/Object;

    goto/32 :goto_15

    nop

    :goto_4
    throw v0

    :catchall_1
    move-exception v0

    goto/32 :goto_c

    nop

    :goto_5
    move-object/from16 v1, p0

    goto/32 :goto_6

    nop

    :goto_6
    move/from16 v2, p1

    goto/32 :goto_10

    nop

    :goto_7
    invoke-static {v11, v12}, Landroid/os/Trace;->traceEnd(J)V

    goto/32 :goto_2

    nop

    :goto_8
    return-void

    :catchall_2
    move-exception v0

    :try_start_2
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto/32 :goto_4

    nop

    :goto_9
    throw v0

    :goto_a
    goto :goto_1

    :cond_1
    goto/32 :goto_e

    nop

    :goto_b
    monitor-enter v7

    :try_start_3
    iput-boolean v6, v1, Landroid/view/Choreographer;->mCallbacksRunning:Z

    :cond_2
    iget-object v0, v9, Landroid/view/Choreographer$CallbackRecord;->next:Landroid/view/Choreographer$CallbackRecord;

    invoke-direct {v1, v9}, Landroid/view/Choreographer;->recycleCallbackLocked(Landroid/view/Choreographer$CallbackRecord;)V

    move-object v9, v0

    if-nez v9, :cond_2

    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto/32 :goto_f

    nop

    :goto_c
    iget-object v7, v1, Landroid/view/Choreographer;->mLock:Ljava/lang/Object;

    goto/32 :goto_12

    nop

    :goto_d
    move-object v7, v8

    goto/32 :goto_a

    nop

    :goto_e
    iget-object v7, v1, Landroid/view/Choreographer;->mLock:Ljava/lang/Object;

    goto/32 :goto_b

    nop

    :goto_f
    invoke-static {v11, v12}, Landroid/os/Trace;->traceEnd(J)V

    nop

    goto/32 :goto_8

    nop

    :goto_10
    move-object/from16 v3, p2

    goto/32 :goto_14

    nop

    :goto_11
    throw v0

    :catchall_3
    move-exception v0

    :try_start_4
    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    goto/32 :goto_9

    nop

    :goto_12
    monitor-enter v7

    :try_start_5
    iput-boolean v6, v1, Landroid/view/Choreographer;->mCallbacksRunning:Z

    :goto_13
    iget-object v6, v9, Landroid/view/Choreographer$CallbackRecord;->next:Landroid/view/Choreographer$CallbackRecord;

    invoke-direct {v1, v9}, Landroid/view/Choreographer;->recycleCallbackLocked(Landroid/view/Choreographer$CallbackRecord;)V

    move-object v9, v6

    if-eqz v9, :cond_3

    goto :goto_13

    :cond_3
    monitor-exit v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/32 :goto_7

    nop

    :goto_14
    invoke-static/range {p2 .. p2}, Landroid/view/Choreographer$FrameData;->-$$Nest$fgetmFrameTimeNanos(Landroid/view/Choreographer$FrameData;)J

    move-result-wide v4

    goto/32 :goto_3

    nop

    :goto_15
    monitor-enter v6

    :try_start_6
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v7

    iget-object v0, v1, Landroid/view/Choreographer;->mCallbackQueues:[Landroid/view/Choreographer$CallbackQueue;

    aget-object v0, v0, v2

    const-wide/32 v9, 0xf4240

    div-long v9, v7, v9

    invoke-virtual {v0, v9, v10}, Landroid/view/Choreographer$CallbackQueue;->extractDueCallbacksLocked(J)Landroid/view/Choreographer$CallbackRecord;

    move-result-object v0

    move-object v9, v0

    if-nez v9, :cond_4

    monitor-exit v6

    return-void

    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, v1, Landroid/view/Choreographer;->mCallbacksRunning:Z

    const/4 v10, 0x4

    const-wide/16 v11, 0x8

    if-ne v2, v10, :cond_5

    sub-long v13, v7, v4

    const-string v10, "jitterNanos"

    long-to-int v15, v13

    invoke-static {v11, v12, v10, v15}, Landroid/os/Trace;->traceCounter(JLjava/lang/String;I)V

    const-wide/16 v15, 0x2

    mul-long v15, v15, p3

    cmp-long v10, v13, v15

    if-ltz v10, :cond_5

    rem-long v15, v13, p3

    add-long v15, v15, p3

    sub-long v4, v7, v15

    iput-wide v4, v1, Landroid/view/Choreographer;->mLastFrameTimeNanos:J

    invoke-virtual {v3, v4, v5}, Landroid/view/Choreographer$FrameData;->updateFrameData(J)V

    :cond_5
    monitor-exit v6
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    goto/32 :goto_0

    nop
.end method

.method doFrame(JILandroid/view/DisplayEventReceiver$VsyncEventData;)V
    .locals 39

    goto/32 :goto_cc

    nop

    :goto_0
    move-object v2, v15

    goto/32 :goto_2d

    nop

    :goto_1
    iget-wide v13, v15, Landroid/view/DisplayEventReceiver$VsyncEventData;->frameInterval:J

    goto/32 :goto_bd

    nop

    :goto_2
    sub-long v18, v16, v5

    goto/32 :goto_d9

    nop

    :goto_3
    if-eqz v2, :cond_0

    goto/32 :goto_11

    :cond_0
    :try_start_0
    const-string v0, "Frame not scheduled"

    invoke-direct {v1, v0}, Landroid/view/Choreographer;->traceMessage(Ljava/lang/String;)V

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_6

    goto/32 :goto_30

    nop

    :goto_4
    move-object/from16 v15, p4

    goto/32 :goto_1

    nop

    :goto_5
    move/from16 v4, p3

    goto/32 :goto_c8

    nop

    :goto_6
    move-object/from16 v1, p0

    goto/32 :goto_86

    nop

    :goto_7
    move-wide v7, v13

    goto/32 :goto_40

    nop

    :goto_8
    if-eq v3, v1, :cond_1

    goto/32 :goto_af

    :cond_1
    goto/32 :goto_19

    nop

    :goto_9
    iget-object v0, v1, Landroid/view/Choreographer;->mChoreographerMonitor:Landroid/os/perfdebug/ChoreographerMonitor;

    goto/32 :goto_cd

    nop

    :goto_a
    move-object v3, v9

    goto/32 :goto_91

    nop

    :goto_b
    const-wide/16 v22, 0x8

    goto/32 :goto_32

    nop

    :goto_c
    move-object/from16 v1, p0

    :try_start_1
    iput-boolean v15, v1, Landroid/view/Choreographer;->mFrameScheduled:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_5

    goto/32 :goto_ce

    nop

    :goto_d
    invoke-static {}, Landroid/view/Choreographer;->getMainThreadInstance()Landroid/view/Choreographer;

    move-result-object v0

    goto/32 :goto_5a

    nop

    :goto_e
    iget-object v0, v1, Landroid/view/Choreographer;->mChoreographerMonitor:Landroid/os/perfdebug/ChoreographerMonitor;

    goto/32 :goto_1d

    nop

    :goto_f
    move-object v3, v9

    goto/32 :goto_ad

    nop

    :goto_10
    goto/16 :goto_96

    :goto_11
    goto/32 :goto_6c

    nop

    :goto_12
    const-wide/16 v22, 0x0

    goto/32 :goto_4f

    nop

    :goto_13
    if-nez v0, :cond_2

    goto/32 :goto_a7

    :cond_2
    goto/32 :goto_b1

    nop

    :goto_14
    invoke-static {}, Landroid/view/Choreographer;->getMainThreadInstance()Landroid/view/Choreographer;

    move-result-object v0

    goto/32 :goto_1a

    nop

    :goto_15
    goto/16 :goto_96

    :catchall_0
    move-exception v0

    goto/32 :goto_6

    nop

    :goto_16
    if-nez v2, :cond_3

    goto/32 :goto_b4

    :cond_3
    :try_start_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Choreographer#doFrame "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p4 .. p4}, Landroid/view/DisplayEventReceiver$VsyncEventData;->preferredFrameTimeline()Landroid/view/DisplayEventReceiver$VsyncEventData$FrameTimeline;

    move-result-object v3

    iget-wide v3, v3, Landroid/view/DisplayEventReceiver$VsyncEventData$FrameTimeline;->vsyncId:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v11, v12, v2}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_c

    goto/32 :goto_8f

    nop

    :goto_17
    cmp-long v2, v11, v5

    goto/32 :goto_34

    nop

    :goto_18
    const-wide/16 v22, 0x8

    goto/32 :goto_1f

    nop

    :goto_19
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v3

    goto/32 :goto_ae

    nop

    :goto_1a
    if-eq v0, v1, :cond_4

    goto/32 :goto_1c

    :cond_4
    goto/32 :goto_a9

    nop

    :goto_1b
    invoke-interface {v0}, Landroid/os/TurboSchedMonitor;->releaseAppToken()V

    :goto_1c
    goto/32 :goto_63

    nop

    :goto_1d
    invoke-virtual {v0}, Landroid/os/perfdebug/ChoreographerMonitor;->monitorDoFrameEnd()V

    goto/32 :goto_d6

    nop

    :goto_1e
    cmp-long v2, v5, v7

    goto/32 :goto_d1

    nop

    :goto_1f
    invoke-static/range {v22 .. v23}, Landroid/os/Trace;->traceEnd(J)V

    goto/32 :goto_9

    nop

    :goto_20
    if-eq v3, v1, :cond_5

    goto/32 :goto_39

    :cond_5
    goto/32 :goto_21

    nop

    :goto_21
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v3

    goto/32 :goto_38

    nop

    :goto_22
    move-wide/from16 v5, v35

    goto/32 :goto_7e

    nop

    :goto_23
    if-gt v2, v0, :cond_6

    goto/32 :goto_79

    :cond_6
    goto/32 :goto_c9

    nop

    :goto_24
    move-object/from16 v24, v10

    goto/32 :goto_2f

    nop

    :goto_25
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v0

    goto/32 :goto_a6

    nop

    :goto_26
    move-object/from16 v24, v10

    goto/32 :goto_b5

    nop

    :goto_27
    move/from16 v4, p3

    goto/32 :goto_aa

    nop

    :goto_28
    move-wide/from16 v22, v11

    goto/32 :goto_6d

    nop

    :goto_29
    move-wide v7, v13

    goto/32 :goto_81

    nop

    :goto_2a
    move-wide v7, v13

    goto/32 :goto_c1

    nop

    :goto_2b
    move-object/from16 v1, p0

    :goto_2c
    goto/32 :goto_98

    nop

    :goto_2d
    goto/16 :goto_b9

    :catchall_1
    move-exception v0

    goto/32 :goto_6f

    nop

    :goto_2e
    iput-boolean v0, v1, Landroid/view/Choreographer;->mIsDoFrameProcessing:Z

    goto/32 :goto_b7

    nop

    :goto_2f
    move-wide v5, v11

    goto/32 :goto_8d

    nop

    :goto_30
    invoke-static {}, Landroid/view/animation/AnimationUtils;->unlockAnimationClock()V

    goto/32 :goto_37

    nop

    :goto_31
    sub-long v5, v16, v22

    :try_start_3
    invoke-virtual {v9, v5, v6}, Landroid/view/Choreographer$FrameData;->updateFrameData(J)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto/32 :goto_ca

    nop

    :goto_32
    goto/16 :goto_96

    :goto_33
    goto/32 :goto_31

    nop

    :goto_34
    if-ltz v2, :cond_7

    goto/32 :goto_4d

    :cond_7
    :try_start_4
    const-string v0, "Frame time goes backward"

    invoke-direct {v1, v0}, Landroid/view/Choreographer;->traceMessage(Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Landroid/view/Choreographer;->scheduleVsyncLocked()V

    monitor-exit v10
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_8

    goto/32 :goto_93

    nop

    :goto_35
    move/from16 v4, p3

    goto/32 :goto_dd

    nop

    :goto_36
    invoke-interface {v0}, Landroid/os/TurboSchedMonitor;->isCoreAppOptimizerEnabled()Z

    move-result v0

    goto/32 :goto_13

    nop

    :goto_37
    invoke-static {v11, v12}, Landroid/os/Trace;->traceEnd(J)V

    goto/32 :goto_3a

    nop

    :goto_38
    invoke-interface {v3}, Landroid/os/TurboSchedMonitor;->releaseAppToken()V

    :goto_39
    goto/32 :goto_2e

    nop

    :goto_3a
    iget-object v0, v1, Landroid/view/Choreographer;->mChoreographerMonitor:Landroid/os/perfdebug/ChoreographerMonitor;

    goto/32 :goto_94

    nop

    :goto_3b
    move-object/from16 v34, v9

    goto/32 :goto_26

    nop

    :goto_3c
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v0

    goto/32 :goto_42

    nop

    :goto_3d
    move-object/from16 v2, p4

    goto/32 :goto_8e

    nop

    :goto_3e
    move/from16 v4, p3

    goto/32 :goto_77

    nop

    :goto_3f
    goto :goto_4a

    :catchall_2
    move-exception v0

    goto/32 :goto_57

    nop

    :goto_40
    move-object v2, v15

    goto/32 :goto_72

    nop

    :goto_41
    move/from16 v4, p3

    goto/32 :goto_85

    nop

    :goto_42
    invoke-interface {v0}, Landroid/os/TurboSchedMonitor;->releaseAppToken()V

    :goto_43
    goto/32 :goto_7c

    nop

    :goto_44
    move-wide v7, v13

    goto/32 :goto_8a

    nop

    :goto_45
    if-gtz v3, :cond_8

    goto/32 :goto_46

    :cond_8
    :try_start_5
    iget-wide v3, v1, Landroid/view/Choreographer;->mFrameIntervalNanos:J

    sub-long v13, v7, v3

    invoke-static {v13, v14}, Ljava/lang/Math;->abs(J)J

    move-result-wide v3

    cmp-long v3, v3, v9

    if-lez v3, :cond_9

    iput-wide v7, v1, Landroid/view/Choreographer;->mFrameIntervalNanos:J

    invoke-static {v7, v8}, Landroid/util/BoostFramework$ScrollOptimizer;->setFrameInterval(J)V

    :cond_9
    :goto_46
    const/4 v3, 0x1

    invoke-static {v3}, Landroid/util/BoostFramework$ScrollOptimizer;->setUITaskStatus(Z)V

    div-long v11, v5, v9

    invoke-static {v11, v12}, Landroid/view/animation/AnimationUtils;->lockAnimationClock(J)V

    invoke-static {}, Landroid/view/Choreographer;->getMainThreadInstance()Landroid/view/Choreographer;

    move-result-object v3

    if-ne v3, v1, :cond_c

    invoke-static {}, Landroid/os/perfdebug/VsyncFrame;->get()Landroid/os/perfdebug/VsyncFrame;

    move-result-object v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_f

    goto/32 :goto_5d

    nop

    :goto_47
    goto/16 :goto_96

    :catchall_3
    move-exception v0

    goto/32 :goto_9e

    nop

    :goto_48
    goto/16 :goto_33

    :catchall_4
    move-exception v0

    goto/32 :goto_cb

    nop

    :goto_49
    move-wide v11, v5

    :goto_4a
    :try_start_6
    iget-wide v5, v1, Landroid/view/Choreographer;->mLastFrameTimeNanos:J
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/32 :goto_17

    nop

    :goto_4b
    move-wide/from16 v35, v11

    goto/32 :goto_50

    nop

    :goto_4c
    goto/16 :goto_96

    :goto_4d
    :try_start_7
    iget v2, v1, Landroid/view/Choreographer;->mFPSDivisor:I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto/32 :goto_23

    nop

    :goto_4e
    const/4 v15, 0x0

    goto/32 :goto_3b

    nop

    :goto_4f
    cmp-long v2, v13, v20

    goto/32 :goto_99

    nop

    :goto_50
    move-wide/from16 v11, v16

    goto/32 :goto_a2

    nop

    :goto_51
    const-wide/32 v9, 0xf4240

    goto/32 :goto_45

    nop

    :goto_52
    if-eq v0, v1, :cond_a

    goto/32 :goto_a7

    :cond_a
    goto/32 :goto_25

    nop

    :goto_53
    move-object v2, v15

    goto/32 :goto_b

    nop

    :goto_54
    cmp-long v2, v11, v7

    goto/32 :goto_62

    nop

    :goto_55
    move-wide v5, v11

    goto/32 :goto_29

    nop

    :goto_56
    move-object/from16 v24, v10

    goto/32 :goto_be

    nop

    :goto_57
    move/from16 v4, p3

    goto/32 :goto_9b

    nop

    :goto_58
    cmp-long v2, v5, v20

    goto/32 :goto_90

    nop

    :goto_59
    const-wide/16 v22, 0x8

    goto/32 :goto_4c

    nop

    :goto_5a
    if-eq v0, v1, :cond_b

    goto/32 :goto_6a

    :cond_b
    goto/32 :goto_c0

    nop

    :goto_5b
    move-wide v5, v11

    goto/32 :goto_4e

    nop

    :goto_5c
    move-object/from16 v2, p4

    goto/32 :goto_d7

    nop

    :goto_5d
    move/from16 v4, p3

    :try_start_8
    invoke-virtual {v3, v4}, Landroid/os/perfdebug/VsyncFrame;->updateCurFrame(I)V

    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v3

    invoke-interface {v3}, Landroid/os/TurboSchedMonitor;->getAppToken()V

    goto :goto_5e

    :cond_c
    move/from16 v4, p3

    :goto_5e
    iget-object v3, v1, Landroid/view/Choreographer;->mChoreographerMonitor:Landroid/os/perfdebug/ChoreographerMonitor;

    invoke-virtual {v3}, Landroid/os/perfdebug/ChoreographerMonitor;->monitorDoFrameBegin()V

    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v3

    invoke-interface {v3}, Landroid/os/TurboSchedMonitor;->isCoreAppOptimizerEnabled()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-static {}, Landroid/view/Choreographer;->getMainThreadInstance()Landroid/view/Choreographer;

    move-result-object v3

    if-ne v3, v1, :cond_d

    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v3

    invoke-interface {v3, v7, v8}, Landroid/os/TurboSchedMonitor;->monitorFrameDelay(J)V

    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v28

    invoke-virtual/range {p4 .. p4}, Landroid/view/DisplayEventReceiver$VsyncEventData;->preferredFrameTimeline()Landroid/view/DisplayEventReceiver$VsyncEventData$FrameTimeline;

    move-result-object v3

    iget-wide v9, v3, Landroid/view/DisplayEventReceiver$VsyncEventData$FrameTimeline;->vsyncId:J

    move/from16 v29, p3

    move-wide/from16 v30, v5

    move-wide/from16 v32, v9

    invoke-interface/range {v28 .. v33}, Landroid/os/TurboSchedMonitor;->recordFrameStart(IJJ)V

    :cond_d
    iget-object v3, v1, Landroid/view/Choreographer;->mFrameInfo:Landroid/graphics/FrameInfo;

    invoke-virtual {v3}, Landroid/graphics/FrameInfo;->markInputHandlingStart()V

    move-object/from16 v3, v34

    invoke-virtual {v1, v0, v3, v7, v8}, Landroid/view/Choreographer;->doCallbacks(ILandroid/view/Choreographer$FrameData;J)V

    iget-object v9, v1, Landroid/view/Choreographer;->mFrameInfo:Landroid/graphics/FrameInfo;

    invoke-virtual {v9}, Landroid/graphics/FrameInfo;->markAnimationsStart()V

    const/4 v9, 0x1

    invoke-virtual {v1, v9, v3, v7, v8}, Landroid/view/Choreographer;->doCallbacks(ILandroid/view/Choreographer$FrameData;J)V

    const/4 v9, 0x2

    invoke-virtual {v1, v9, v3, v7, v8}, Landroid/view/Choreographer;->doCallbacks(ILandroid/view/Choreographer$FrameData;J)V

    iget-object v9, v1, Landroid/view/Choreographer;->mFrameInfo:Landroid/graphics/FrameInfo;

    invoke-virtual {v9}, Landroid/graphics/FrameInfo;->markPerformTraversalsStart()V

    const/4 v9, 0x3

    invoke-virtual {v1, v9, v3, v7, v8}, Landroid/view/Choreographer;->doCallbacks(ILandroid/view/Choreographer$FrameData;J)V

    const/4 v9, 0x4

    invoke-virtual {v1, v9, v3, v7, v8}, Landroid/view/Choreographer;->doCallbacks(ILandroid/view/Choreographer$FrameData;J)V

    invoke-static {v0}, Landroid/util/BoostFramework$ScrollOptimizer;->setUITaskStatus(Z)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_d

    goto/32 :goto_a4

    nop

    :goto_5f
    move-object/from16 v24, v10

    goto/32 :goto_55

    nop

    :goto_60
    move-wide v7, v13

    goto/32 :goto_da

    nop

    :goto_61
    goto/16 :goto_96

    :catchall_5
    move-exception v0

    goto/32 :goto_83

    nop

    :goto_62
    if-gez v2, :cond_e

    goto/32 :goto_33

    :cond_e
    :try_start_9
    const-string v2, "Choreographer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Skipped "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " frames!  The application may be doing too much work on its main thread."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    goto/32 :goto_48

    nop

    :goto_63
    return-void

    :catchall_6
    move-exception v0

    goto/32 :goto_97

    nop

    :goto_64
    move-object/from16 v2, p4

    :goto_65
    goto/32 :goto_88

    nop

    :goto_66
    invoke-static/range {v22 .. v23}, Landroid/os/Trace;->traceEnd(J)V

    goto/32 :goto_d3

    nop

    :goto_67
    goto/16 :goto_96

    :goto_68
    goto/32 :goto_49

    nop

    :goto_69
    invoke-interface {v0}, Landroid/os/TurboSchedMonitor;->releaseAppToken()V

    :goto_6a
    goto/32 :goto_7f

    nop

    :goto_6b
    move-wide/from16 v7, v37

    goto/32 :goto_61

    nop

    :goto_6c
    move-wide/from16 v3, p1

    :try_start_a
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v16
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    goto/32 :goto_2

    nop

    :goto_6d
    move-wide v7, v13

    goto/32 :goto_95

    nop

    :goto_6e
    const-wide/16 v20, 0x0

    goto/32 :goto_c7

    nop

    :goto_6f
    move/from16 v4, p3

    goto/32 :goto_a5

    nop

    :goto_70
    invoke-static {}, Landroid/view/Choreographer;->getMainThreadInstance()Landroid/view/Choreographer;

    move-result-object v3

    goto/32 :goto_20

    nop

    :goto_71
    move/from16 v4, p3

    goto/32 :goto_64

    nop

    :goto_72
    const-wide/16 v22, 0x8

    goto/32 :goto_67

    nop

    :goto_73
    move/from16 v4, p3

    goto/32 :goto_ba

    nop

    :goto_74
    const/4 v7, 0x0

    :try_start_b
    iput-boolean v7, v1, Landroid/view/Choreographer;->mIsVsyncScheduled:Z

    iget-boolean v2, v1, Landroid/view/Choreographer;->mFrameScheduled:Z
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    goto/32 :goto_3

    nop

    :goto_75
    int-to-long v7, v2

    goto/32 :goto_8c

    nop

    :goto_76
    move/from16 v4, p3

    goto/32 :goto_d2

    nop

    :goto_77
    goto :goto_65

    :catchall_7
    move-exception v0

    goto/32 :goto_71

    nop

    :goto_78
    goto/16 :goto_c4

    :goto_79
    goto/32 :goto_c3

    nop

    :goto_7a
    move-object v3, v9

    goto/32 :goto_ac

    nop

    :goto_7b
    const-wide/16 v22, 0x8

    goto/32 :goto_78

    nop

    :goto_7c
    return-void

    :catchall_8
    move-exception v0

    goto/32 :goto_41

    nop

    :goto_7d
    invoke-virtual {v3}, Landroid/os/perfdebug/ChoreographerMonitor;->monitorDoFrameEnd()V

    goto/32 :goto_dc

    nop

    :goto_7e
    move-wide/from16 v7, v37

    goto/32 :goto_15

    nop

    :goto_7f
    return-void

    :goto_80
    goto/32 :goto_7b

    nop

    :goto_81
    move-object v2, v15

    goto/32 :goto_59

    nop

    :goto_82
    invoke-static {v5, v6}, Landroid/os/Trace;->traceEnd(J)V

    goto/32 :goto_e

    nop

    :goto_83
    goto/16 :goto_2c

    :catchall_9
    move-exception v0

    goto/32 :goto_2b

    nop

    :goto_84
    move-object v2, v15

    goto/32 :goto_b8

    nop

    :goto_85
    move-object v3, v9

    goto/32 :goto_5f

    nop

    :goto_86
    goto :goto_87

    :catchall_a
    move-exception v0

    :goto_87
    goto/32 :goto_73

    nop

    :goto_88
    move-object/from16 v3, v34

    goto/32 :goto_d4

    nop

    :goto_89
    goto :goto_96

    :catchall_b
    move-exception v0

    goto/32 :goto_76

    nop

    :goto_8a
    move-object v2, v15

    :goto_8b
    goto/32 :goto_bb

    nop

    :goto_8c
    mul-long/2addr v7, v13

    goto/32 :goto_1e

    nop

    :goto_8d
    move-wide v7, v13

    goto/32 :goto_84

    nop

    :goto_8e
    move v0, v15

    :try_start_c
    iput-object v2, v1, Landroid/view/Choreographer;->mLastVsyncEventData:Landroid/view/DisplayEventReceiver$VsyncEventData;

    monitor-exit v24
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_e

    goto/32 :goto_9f

    nop

    :goto_8f
    goto/16 :goto_b4

    :catchall_c
    move-exception v0

    goto/32 :goto_5

    nop

    :goto_90
    if-gtz v2, :cond_f

    goto/32 :goto_80

    :cond_f
    :try_start_d
    const-string v0, "Frame skipped due to FPSDivisor"

    invoke-direct {v1, v0}, Landroid/view/Choreographer;->traceMessage(Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Landroid/view/Choreographer;->scheduleVsyncLocked()V

    monitor-exit v10
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_8

    goto/32 :goto_cf

    nop

    :goto_91
    move-object/from16 v24, v10

    goto/32 :goto_a0

    nop

    :goto_92
    invoke-static/range {v22 .. v23}, Landroid/os/Trace;->traceEnd(J)V

    goto/32 :goto_9d

    nop

    :goto_93
    invoke-static {}, Landroid/view/animation/AnimationUtils;->unlockAnimationClock()V

    goto/32 :goto_b0

    nop

    :goto_94
    invoke-virtual {v0}, Landroid/os/perfdebug/ChoreographerMonitor;->monitorDoFrameEnd()V

    goto/32 :goto_14

    nop

    :goto_95
    move-object v2, v15

    :goto_96
    :try_start_e
    monitor-exit v24
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_10

    :try_start_f
    throw v0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_d

    :catchall_d
    move-exception v0

    goto/32 :goto_c5

    nop

    :goto_97
    move/from16 v4, p3

    goto/32 :goto_f

    nop

    :goto_98
    move/from16 v4, p3

    goto/32 :goto_5c

    nop

    :goto_99
    if-eqz v2, :cond_10

    goto/32 :goto_9a

    :cond_10
    :try_start_10
    const-string v2, "Choreographer"

    const-string v8, "Vsync data empty due to timeout"

    invoke-static {v2, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_33

    :goto_9a
    rem-long v24, v18, v13

    move-wide/from16 v22, v24

    div-long v24, v18, v13

    move-wide/from16 v26, v24

    sget v2, Landroid/view/Choreographer;->SKIPPED_FRAME_WARNING_LIMIT:I
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_6

    goto/32 :goto_db

    nop

    :goto_9b
    move-object v3, v9

    goto/32 :goto_a3

    nop

    :goto_9c
    if-eq v0, v1, :cond_11

    goto/32 :goto_43

    :cond_11
    goto/32 :goto_3c

    nop

    :goto_9d
    iget-object v3, v1, Landroid/view/Choreographer;->mChoreographerMonitor:Landroid/os/perfdebug/ChoreographerMonitor;

    goto/32 :goto_7d

    nop

    :goto_9e
    move/from16 v4, p3

    goto/32 :goto_7a

    nop

    :goto_9f
    cmp-long v3, v7, v20

    goto/32 :goto_51

    nop

    :goto_a0
    move-wide v7, v13

    goto/32 :goto_53

    nop

    :goto_a1
    move-wide v13, v0

    :try_start_11
    invoke-virtual/range {v2 .. v14}, Landroid/graphics/FrameInfo;->setVsync(JJJJJJ)V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_9

    goto/32 :goto_c

    nop

    :goto_a2
    move-wide/from16 v37, v13

    goto/32 :goto_a1

    nop

    :goto_a3
    move-object/from16 v24, v10

    goto/32 :goto_7

    nop

    :goto_a4
    invoke-static {}, Landroid/view/animation/AnimationUtils;->unlockAnimationClock()V

    goto/32 :goto_66

    nop

    :goto_a5
    move-object v3, v9

    goto/32 :goto_24

    nop

    :goto_a6
    invoke-interface {v0}, Landroid/os/TurboSchedMonitor;->setDrawFrameFinished()V

    :goto_a7
    goto/32 :goto_c2

    nop

    :goto_a8
    move-wide/from16 v5, p1

    goto/32 :goto_4

    nop

    :goto_a9
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v0

    goto/32 :goto_1b

    nop

    :goto_aa
    goto/16 :goto_8b

    :catchall_e
    move-exception v0

    goto/32 :goto_3e

    nop

    :goto_ab
    throw v0

    :goto_ac
    move-object/from16 v24, v10

    goto/32 :goto_28

    nop

    :goto_ad
    move-object/from16 v24, v10

    goto/32 :goto_c6

    nop

    :goto_ae
    invoke-interface {v3}, Landroid/os/TurboSchedMonitor;->releaseAppToken()V

    :goto_af
    goto/32 :goto_ab

    nop

    :goto_b0
    const-wide/16 v5, 0x8

    goto/32 :goto_82

    nop

    :goto_b1
    invoke-static {}, Landroid/view/Choreographer;->getMainThreadInstance()Landroid/view/Choreographer;

    move-result-object v0

    goto/32 :goto_52

    nop

    :goto_b2
    move-wide/from16 v11, v26

    goto/32 :goto_54

    nop

    :goto_b3
    goto/16 :goto_8b

    :goto_b4
    :try_start_12
    new-instance v2, Landroid/view/Choreographer$FrameData;

    invoke-direct {v2, v5, v6, v15}, Landroid/view/Choreographer$FrameData;-><init>(JLandroid/view/DisplayEventReceiver$VsyncEventData;)V

    move-object v9, v2

    iget-object v10, v1, Landroid/view/Choreographer;->mLock:Ljava/lang/Object;

    monitor-enter v10
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_b

    goto/32 :goto_74

    nop

    :goto_b5
    move-wide/from16 v9, v26

    goto/32 :goto_4b

    nop

    :goto_b6
    invoke-virtual {v3}, Landroid/os/perfdebug/ChoreographerMonitor;->monitorDoFrameEnd()V

    goto/32 :goto_70

    nop

    :goto_b7
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v0

    goto/32 :goto_36

    nop

    :goto_b8
    const-wide/16 v22, 0x8

    :goto_b9
    goto/32 :goto_47

    nop

    :goto_ba
    move-object v3, v9

    goto/32 :goto_56

    nop

    :goto_bb
    invoke-static {}, Landroid/view/animation/AnimationUtils;->unlockAnimationClock()V

    goto/32 :goto_92

    nop

    :goto_bc
    move-wide/from16 v26, v5

    goto/32 :goto_5b

    nop

    :goto_bd
    const/4 v0, 0x1

    goto/32 :goto_d0

    nop

    :goto_be
    move-wide v5, v11

    goto/32 :goto_d5

    nop

    :goto_bf
    move-wide/from16 v7, v37

    :try_start_13
    iput-wide v7, v1, Landroid/view/Choreographer;->mLastFrameIntervalNanos:J
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_7

    goto/32 :goto_3d

    nop

    :goto_c0
    invoke-static {}, Landroid/os/TurboSchedMonitor;->getInstance()Landroid/os/TurboSchedMonitor;

    move-result-object v0

    goto/32 :goto_69

    nop

    :goto_c1
    move-object v2, v15

    goto/32 :goto_b3

    nop

    :goto_c2
    return-void

    :catchall_f
    move-exception v0

    goto/32 :goto_27

    nop

    :goto_c3
    const-wide/16 v22, 0x8

    :goto_c4
    :try_start_14
    iget-object v2, v1, Landroid/view/Choreographer;->mFrameInfo:Landroid/graphics/FrameInfo;

    invoke-virtual/range {p4 .. p4}, Landroid/view/DisplayEventReceiver$VsyncEventData;->preferredFrameTimeline()Landroid/view/DisplayEventReceiver$VsyncEventData$FrameTimeline;

    move-result-object v5

    iget-wide v7, v5, Landroid/view/DisplayEventReceiver$VsyncEventData$FrameTimeline;->vsyncId:J

    invoke-virtual/range {p4 .. p4}, Landroid/view/DisplayEventReceiver$VsyncEventData;->preferredFrameTimeline()Landroid/view/DisplayEventReceiver$VsyncEventData$FrameTimeline;

    move-result-object v5

    iget-wide v5, v5, Landroid/view/DisplayEventReceiver$VsyncEventData$FrameTimeline;->deadline:J
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_a

    :try_start_15
    iget-wide v0, v15, Landroid/view/DisplayEventReceiver$VsyncEventData;->frameInterval:J
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    goto/32 :goto_bc

    nop

    :goto_c5
    goto/16 :goto_8b

    :catchall_10
    move-exception v0

    goto/32 :goto_89

    nop

    :goto_c6
    move-wide/from16 v22, v11

    goto/32 :goto_60

    nop

    :goto_c7
    if-gez v2, :cond_12

    goto/32 :goto_68

    :cond_12
    goto/32 :goto_12

    nop

    :goto_c8
    move-wide/from16 v22, v11

    goto/32 :goto_2a

    nop

    :goto_c9
    sub-long v5, v11, v5

    goto/32 :goto_75

    nop

    :goto_ca
    move-wide v11, v5

    goto/32 :goto_3f

    nop

    :goto_cb
    move/from16 v4, p3

    goto/32 :goto_a

    nop

    :goto_cc
    move-object/from16 v1, p0

    goto/32 :goto_a8

    nop

    :goto_cd
    invoke-virtual {v0}, Landroid/os/perfdebug/ChoreographerMonitor;->monitorDoFrameEnd()V

    goto/32 :goto_d

    nop

    :goto_ce
    move-wide/from16 v5, v35

    :try_start_16
    iput-wide v5, v1, Landroid/view/Choreographer;->mLastFrameTimeNanos:J
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_11

    goto/32 :goto_bf

    nop

    :goto_cf
    invoke-static {}, Landroid/view/animation/AnimationUtils;->unlockAnimationClock()V

    goto/32 :goto_18

    nop

    :goto_d0
    const-wide/16 v11, 0x8

    :try_start_17
    iput-boolean v0, v1, Landroid/view/Choreographer;->mIsDoFrameProcessing:Z

    invoke-static {v11, v12}, Landroid/os/Trace;->isTagEnabled(J)Z

    move-result v2
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_b

    goto/32 :goto_16

    nop

    :goto_d1
    if-ltz v2, :cond_13

    goto/32 :goto_80

    :cond_13
    goto/32 :goto_58

    nop

    :goto_d2
    move-wide/from16 v22, v11

    goto/32 :goto_44

    nop

    :goto_d3
    iget-object v3, v1, Landroid/view/Choreographer;->mChoreographerMonitor:Landroid/os/perfdebug/ChoreographerMonitor;

    goto/32 :goto_b6

    nop

    :goto_d4
    goto/16 :goto_96

    :catchall_11
    move-exception v0

    goto/32 :goto_35

    nop

    :goto_d5
    move-wide v7, v13

    goto/32 :goto_0

    nop

    :goto_d6
    invoke-static {}, Landroid/view/Choreographer;->getMainThreadInstance()Landroid/view/Choreographer;

    move-result-object v0

    goto/32 :goto_9c

    nop

    :goto_d7
    move-object/from16 v3, v34

    goto/32 :goto_22

    nop

    :goto_d8
    move-object/from16 v3, v34

    goto/32 :goto_6b

    nop

    :goto_d9
    cmp-long v2, v18, v13

    goto/32 :goto_6e

    nop

    :goto_da
    move-object v2, v15

    goto/32 :goto_10

    nop

    :goto_db
    int-to-long v7, v2

    goto/32 :goto_b2

    nop

    :goto_dc
    invoke-static {}, Landroid/view/Choreographer;->getMainThreadInstance()Landroid/view/Choreographer;

    move-result-object v3

    goto/32 :goto_8

    nop

    :goto_dd
    move-object/from16 v2, p4

    goto/32 :goto_d8

    nop
.end method

.method doScheduleCallback(I)V
    .locals 4

    goto/32 :goto_1

    nop

    :goto_0
    throw v1

    :goto_1
    iget-object v0, p0, Landroid/view/Choreographer;->mLock:Ljava/lang/Object;

    goto/32 :goto_2

    nop

    :goto_2
    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Landroid/view/Choreographer;->mFrameScheduled:Z

    if-nez v1, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iget-object v3, p0, Landroid/view/Choreographer;->mCallbackQueues:[Landroid/view/Choreographer$CallbackQueue;

    aget-object v3, v3, p1

    invoke-virtual {v3, v1, v2}, Landroid/view/Choreographer$CallbackQueue;->hasDueCallbacksLocked(J)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0, v1, v2}, Landroid/view/Choreographer;->scheduleFrameLocked(J)V

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_0

    nop
.end method

.method doScheduleVsync()V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/view/Choreographer;->mLock:Ljava/lang/Object;

    goto/32 :goto_1

    nop

    :goto_1
    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Landroid/view/Choreographer;->mFrameScheduled:Z

    if-eqz v1, :cond_0

    invoke-direct {p0}, Landroid/view/Choreographer;->scheduleVsyncLocked()V

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_2

    nop

    :goto_2
    throw v1
.end method

.method dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 5

    goto/32 :goto_c

    nop

    :goto_0
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_2

    nop

    :goto_1
    const-string/jumbo v1, "mFrameScheduled="

    goto/32 :goto_a

    nop

    :goto_2
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_3
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_4

    nop

    :goto_4
    iget-wide v1, p0, Landroid/view/Choreographer;->mLastFrameTimeNanos:J

    goto/32 :goto_12

    nop

    :goto_5
    div-long/2addr v1, v3

    goto/32 :goto_16

    nop

    :goto_6
    const-string v1, "  "

    goto/32 :goto_9

    nop

    :goto_7
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_15

    nop

    :goto_8
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_e

    nop

    :goto_9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_a
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_d

    nop

    :goto_b
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/32 :goto_13

    nop

    :goto_c
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_0

    nop

    :goto_d
    iget-boolean v1, p0, Landroid/view/Choreographer;->mFrameScheduled:Z

    goto/32 :goto_14

    nop

    :goto_e
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_f

    nop

    :goto_f
    const-string v1, "Choreographer:"

    goto/32 :goto_b

    nop

    :goto_10
    return-void

    :goto_11
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/32 :goto_10

    nop

    :goto_12
    const-wide/32 v3, 0xf4240

    goto/32 :goto_5

    nop

    :goto_13
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_1

    nop

    :goto_14
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Z)V

    goto/32 :goto_7

    nop

    :goto_15
    const-string/jumbo v1, "mLastFrameTime="

    goto/32 :goto_3

    nop

    :goto_16
    invoke-static {v1, v2}, Landroid/util/TimeUtils;->formatUptime(J)Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_11

    nop
.end method

.method public getFrameDeadline()J
    .locals 2

    iget-object v0, p0, Landroid/view/Choreographer;->mLastVsyncEventData:Landroid/view/DisplayEventReceiver$VsyncEventData;

    invoke-virtual {v0}, Landroid/view/DisplayEventReceiver$VsyncEventData;->preferredFrameTimeline()Landroid/view/DisplayEventReceiver$VsyncEventData$FrameTimeline;

    move-result-object v0

    iget-wide v0, v0, Landroid/view/DisplayEventReceiver$VsyncEventData$FrameTimeline;->deadline:J

    return-wide v0
.end method

.method public getFrameIntervalNanos()J
    .locals 3

    iget-object v0, p0, Landroid/view/Choreographer;->mLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-wide v1, p0, Landroid/view/Choreographer;->mLastFrameIntervalNanos:J

    monitor-exit v0

    return-wide v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getFrameTime()J
    .locals 4

    invoke-virtual {p0}, Landroid/view/Choreographer;->getFrameTimeNanos()J

    move-result-wide v0

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public getFrameTimeNanos()J
    .locals 3

    iget-object v0, p0, Landroid/view/Choreographer;->mLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Landroid/view/Choreographer;->mCallbacksRunning:Z

    if-eqz v1, :cond_1

    sget-boolean v1, Landroid/view/Choreographer;->USE_FRAME_TIME:Z

    if-eqz v1, :cond_0

    iget-wide v1, p0, Landroid/view/Choreographer;->mLastFrameTimeNanos:J

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v1

    :goto_0
    monitor-exit v0

    return-wide v1

    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "This method must only be called as part of a callback while a frame is in progress."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getLastFrameTimeNanos()J
    .locals 3

    iget-object v0, p0, Landroid/view/Choreographer;->mLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    sget-boolean v1, Landroid/view/Choreographer;->USE_FRAME_TIME:Z

    if-eqz v1, :cond_0

    iget-wide v1, p0, Landroid/view/Choreographer;->mLastFrameTimeNanos:J

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v1

    :goto_0
    monitor-exit v0

    return-wide v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getVsyncId()J
    .locals 2

    iget-object v0, p0, Landroid/view/Choreographer;->mLastVsyncEventData:Landroid/view/DisplayEventReceiver$VsyncEventData;

    invoke-virtual {v0}, Landroid/view/DisplayEventReceiver$VsyncEventData;->preferredFrameTimeline()Landroid/view/DisplayEventReceiver$VsyncEventData$FrameTimeline;

    move-result-object v0

    iget-wide v0, v0, Landroid/view/DisplayEventReceiver$VsyncEventData$FrameTimeline;->vsyncId:J

    return-wide v0
.end method

.method public postCallback(ILjava/lang/Runnable;Ljava/lang/Object;)V
    .locals 6

    const-wide/16 v4, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Landroid/view/Choreographer;->postCallbackDelayed(ILjava/lang/Runnable;Ljava/lang/Object;J)V

    return-void
.end method

.method public postCallbackDelayed(ILjava/lang/Runnable;Ljava/lang/Object;J)V
    .locals 2

    if-eqz p2, :cond_1

    if-ltz p1, :cond_0

    const/4 v0, 0x4

    if-gt p1, v0, :cond_0

    invoke-direct/range {p0 .. p5}, Landroid/view/Choreographer;->postCallbackDelayedInternal(ILjava/lang/Object;Ljava/lang/Object;J)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "callbackType is invalid"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "action must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public postCallbackWithoutVSync(ILjava/lang/Runnable;Ljava/lang/Object;)V
    .locals 7

    if-eqz p2, :cond_2

    if-ltz p1, :cond_1

    const/4 v0, 0x4

    if-gt p1, v0, :cond_1

    iget-object v0, p0, Landroid/view/Choreographer;->mLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iget-object v3, p0, Landroid/view/Choreographer;->mCallbackQueues:[Landroid/view/Choreographer$CallbackQueue;

    aget-object v3, v3, p1

    invoke-virtual {v3, v1, v2, p2, p3}, Landroid/view/Choreographer$CallbackQueue;->addCallbackLocked(JLjava/lang/Object;Ljava/lang/Object;)V

    const/4 v3, 0x1

    iput-boolean v3, p0, Landroid/view/Choreographer;->mFirstVSync:Z

    iget-boolean v4, p0, Landroid/view/Choreographer;->mFrameScheduled:Z

    if-nez v4, :cond_0

    iput-boolean v3, p0, Landroid/view/Choreographer;->mFrameScheduled:Z

    iget-object v4, p0, Landroid/view/Choreographer;->mHandler:Landroid/view/Choreographer$FrameHandler;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/Choreographer$FrameHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/os/Message;->setAsynchronous(Z)V

    iget-object v3, p0, Landroid/view/Choreographer;->mHandler:Landroid/view/Choreographer$FrameHandler;

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/view/Choreographer$FrameHandler;->sendMessageAtTime(Landroid/os/Message;J)Z

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "callbackType is invalid"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "action must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, v0, v1}, Landroid/view/Choreographer;->postFrameCallbackDelayed(Landroid/view/Choreographer$FrameCallback;J)V

    return-void
.end method

.method public postFrameCallbackDelayed(Landroid/view/Choreographer$FrameCallback;J)V
    .locals 6

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    sget-object v3, Landroid/view/Choreographer;->FRAME_CALLBACK_TOKEN:Ljava/lang/Object;

    move-object v0, p0

    move-object v2, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Landroid/view/Choreographer;->postCallbackDelayedInternal(ILjava/lang/Object;Ljava/lang/Object;J)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "callback must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public postVsyncCallback(Landroid/view/Choreographer$VsyncCallback;)V
    .locals 6

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    sget-object v3, Landroid/view/Choreographer;->VSYNC_CALLBACK_TOKEN:Ljava/lang/Object;

    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Landroid/view/Choreographer;->postCallbackDelayedInternal(ILjava/lang/Object;Ljava/lang/Object;J)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "callback must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public removeCallbacks(ILjava/lang/Runnable;Ljava/lang/Object;)V
    .locals 2

    if-ltz p1, :cond_0

    const/4 v0, 0x4

    if-gt p1, v0, :cond_0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/Choreographer;->removeCallbacksInternal(ILjava/lang/Object;Ljava/lang/Object;)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "callbackType is invalid"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public removeFrameCallback(Landroid/view/Choreographer$FrameCallback;)V
    .locals 2

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    sget-object v1, Landroid/view/Choreographer;->FRAME_CALLBACK_TOKEN:Ljava/lang/Object;

    invoke-direct {p0, v0, p1, v1}, Landroid/view/Choreographer;->removeCallbacksInternal(ILjava/lang/Object;Ljava/lang/Object;)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "callback must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public removeVsyncCallback(Landroid/view/Choreographer$VsyncCallback;)V
    .locals 2

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    sget-object v1, Landroid/view/Choreographer;->VSYNC_CALLBACK_TOKEN:Ljava/lang/Object;

    invoke-direct {p0, v0, p1, v1}, Landroid/view/Choreographer;->removeCallbacksInternal(ILjava/lang/Object;Ljava/lang/Object;)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "callback must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method setFPSDivisor(I)V
    .locals 0

    goto/32 :goto_5

    nop

    :goto_0
    return-void

    :goto_1
    const/4 p1, 0x1

    :goto_2
    goto/32 :goto_4

    nop

    :goto_3
    invoke-static {p1}, Landroid/view/ThreadedRenderer;->setFPSDivisor(I)V

    goto/32 :goto_0

    nop

    :goto_4
    iput p1, p0, Landroid/view/Choreographer;->mFPSDivisor:I

    goto/32 :goto_3

    nop

    :goto_5
    if-lez p1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop
.end method

.method public setMotionEventInfo(II)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput p2, p0, Landroid/view/Choreographer;->mTouchMoveNum:I

    iput p1, p0, Landroid/view/Choreographer;->mMotionEventType:I

    invoke-static {p1}, Landroid/util/BoostFramework$ScrollOptimizer;->setMotionType(I)V

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
