.class final Landroid/view/inputmethod/IAccessibilityInputMethodSessionInvoker;
.super Ljava/lang/Object;


# static fields
.field private static final TAG:Ljava/lang/String; = "IAccessibilityInputMethodSessionInvoker"


# instance fields
.field private final mSession:Lcom/android/internal/inputmethod/IAccessibilityInputMethodSession;


# direct methods
.method private constructor <init>(Lcom/android/internal/inputmethod/IAccessibilityInputMethodSession;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/view/inputmethod/IAccessibilityInputMethodSessionInvoker;->mSession:Lcom/android/internal/inputmethod/IAccessibilityInputMethodSession;

    return-void
.end method

.method public static createOrNull(Lcom/android/internal/inputmethod/IAccessibilityInputMethodSession;)Landroid/view/inputmethod/IAccessibilityInputMethodSessionInvoker;
    .locals 1

    if-nez p0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/view/inputmethod/IAccessibilityInputMethodSessionInvoker;

    invoke-direct {v0, p0}, Landroid/view/inputmethod/IAccessibilityInputMethodSessionInvoker;-><init>(Lcom/android/internal/inputmethod/IAccessibilityInputMethodSession;)V

    :goto_0
    return-object v0
.end method


# virtual methods
.method finishInput()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Landroid/view/inputmethod/IAccessibilityInputMethodSessionInvoker;->mSession:Lcom/android/internal/inputmethod/IAccessibilityInputMethodSession;

    invoke-interface {v0}, Lcom/android/internal/inputmethod/IAccessibilityInputMethodSession;->finishInput()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    goto :goto_3

    :catch_0
    move-exception v0

    goto/32 :goto_4

    nop

    :goto_2
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_3
    goto/32 :goto_0

    nop

    :goto_4
    const-string v1, "IAccessibilityInputMethodSessionInvoker"

    goto/32 :goto_5

    nop

    :goto_5
    const-string v2, "A11yIME died"

    goto/32 :goto_2

    nop
.end method

.method invalidateInput(Landroid/view/inputmethod/EditorInfo;Lcom/android/internal/inputmethod/IRemoteAccessibilityInputConnection;I)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Landroid/view/inputmethod/IAccessibilityInputMethodSessionInvoker;->mSession:Lcom/android/internal/inputmethod/IAccessibilityInputMethodSession;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/inputmethod/IAccessibilityInputMethodSession;->invalidateInput(Landroid/view/inputmethod/EditorInfo;Lcom/android/internal/inputmethod/IRemoteAccessibilityInputConnection;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_1

    nop

    :goto_0
    const-string v2, "A11yIME died"

    goto/32 :goto_3

    nop

    :goto_1
    goto :goto_4

    :catch_0
    move-exception v0

    goto/32 :goto_5

    nop

    :goto_2
    return-void

    :goto_3
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_4
    goto/32 :goto_2

    nop

    :goto_5
    const-string v1, "IAccessibilityInputMethodSessionInvoker"

    goto/32 :goto_0

    nop
.end method

.method updateSelection(IIIIII)V
    .locals 7

    :try_start_0
    iget-object v0, p0, Landroid/view/inputmethod/IAccessibilityInputMethodSessionInvoker;->mSession:Lcom/android/internal/inputmethod/IAccessibilityInputMethodSession;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/android/internal/inputmethod/IAccessibilityInputMethodSession;->updateSelection(IIIIII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_0

    nop

    :goto_0
    goto :goto_5

    :catch_0
    move-exception v0

    goto/32 :goto_2

    nop

    :goto_1
    const-string v2, "A11yIME died"

    goto/32 :goto_4

    nop

    :goto_2
    const-string v1, "IAccessibilityInputMethodSessionInvoker"

    goto/32 :goto_1

    nop

    :goto_3
    return-void

    :goto_4
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_5
    goto/32 :goto_3

    nop
.end method
