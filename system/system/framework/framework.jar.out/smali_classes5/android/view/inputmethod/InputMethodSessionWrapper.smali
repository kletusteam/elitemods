.class final Landroid/view/inputmethod/InputMethodSessionWrapper;
.super Ljava/lang/Object;


# static fields
.field private static final TAG:Ljava/lang/String; = "InputMethodSessionWrapper"


# instance fields
.field private final mSession:Lcom/android/internal/view/IInputMethodSession;


# direct methods
.method private constructor <init>(Lcom/android/internal/view/IInputMethodSession;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/view/inputmethod/InputMethodSessionWrapper;->mSession:Lcom/android/internal/view/IInputMethodSession;

    return-void
.end method

.method public static createOrNull(Lcom/android/internal/view/IInputMethodSession;)Landroid/view/inputmethod/InputMethodSessionWrapper;
    .locals 1

    if-eqz p0, :cond_0

    new-instance v0, Landroid/view/inputmethod/InputMethodSessionWrapper;

    invoke-direct {v0, p0}, Landroid/view/inputmethod/InputMethodSessionWrapper;-><init>(Lcom/android/internal/view/IInputMethodSession;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method


# virtual methods
.method appPrivateCommand(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodSessionWrapper;->mSession:Lcom/android/internal/view/IInputMethodSession;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/view/IInputMethodSession;->appPrivateCommand(Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_3

    nop

    :goto_0
    const-string v2, "IME died"

    goto/32 :goto_4

    nop

    :goto_1
    const-string v1, "InputMethodSessionWrapper"

    goto/32 :goto_0

    nop

    :goto_2
    return-void

    :goto_3
    goto :goto_5

    :catch_0
    move-exception v0

    goto/32 :goto_1

    nop

    :goto_4
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_5
    goto/32 :goto_2

    nop
.end method

.method displayCompletions([Landroid/view/inputmethod/CompletionInfo;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodSessionWrapper;->mSession:Lcom/android/internal/view/IInputMethodSession;

    invoke-interface {v0, p1}, Lcom/android/internal/view/IInputMethodSession;->displayCompletions([Landroid/view/inputmethod/CompletionInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    goto :goto_3

    :catch_0
    move-exception v0

    goto/32 :goto_5

    nop

    :goto_2
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_3
    goto/32 :goto_0

    nop

    :goto_4
    const-string v2, "IME died"

    goto/32 :goto_2

    nop

    :goto_5
    const-string v1, "InputMethodSessionWrapper"

    goto/32 :goto_4

    nop
.end method

.method finishInput()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodSessionWrapper;->mSession:Lcom/android/internal/view/IInputMethodSession;

    invoke-interface {v0}, Lcom/android/internal/view/IInputMethodSession;->finishInput()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_3

    nop

    :goto_0
    const-string v2, "IME died"

    goto/32 :goto_4

    nop

    :goto_1
    return-void

    :goto_2
    const-string v1, "InputMethodSessionWrapper"

    goto/32 :goto_0

    nop

    :goto_3
    goto :goto_5

    :catch_0
    move-exception v0

    goto/32 :goto_2

    nop

    :goto_4
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_5
    goto/32 :goto_1

    nop
.end method

.method invalidateInput(Landroid/view/inputmethod/EditorInfo;Lcom/android/internal/view/IInputContext;I)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodSessionWrapper;->mSession:Lcom/android/internal/view/IInputMethodSession;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/internal/view/IInputMethodSession;->invalidateInput(Landroid/view/inputmethod/EditorInfo;Lcom/android/internal/view/IInputContext;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_1

    nop

    :goto_0
    const-string v2, "IME died"

    goto/32 :goto_2

    nop

    :goto_1
    goto :goto_3

    :catch_0
    move-exception v0

    goto/32 :goto_5

    nop

    :goto_2
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_3
    goto/32 :goto_4

    nop

    :goto_4
    return-void

    :goto_5
    const-string v1, "InputMethodSessionWrapper"

    goto/32 :goto_0

    nop
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/view/inputmethod/InputMethodSessionWrapper;->mSession:Lcom/android/internal/view/IInputMethodSession;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method updateCursor(Landroid/graphics/Rect;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodSessionWrapper;->mSession:Lcom/android/internal/view/IInputMethodSession;

    invoke-interface {v0, p1}, Lcom/android/internal/view/IInputMethodSession;->updateCursor(Landroid/graphics/Rect;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_3

    nop

    :goto_0
    return-void

    :goto_1
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_2
    goto/32 :goto_0

    nop

    :goto_3
    goto :goto_2

    :catch_0
    move-exception v0

    goto/32 :goto_5

    nop

    :goto_4
    const-string v2, "IME died"

    goto/32 :goto_1

    nop

    :goto_5
    const-string v1, "InputMethodSessionWrapper"

    goto/32 :goto_4

    nop
.end method

.method updateCursorAnchorInfo(Landroid/view/inputmethod/CursorAnchorInfo;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodSessionWrapper;->mSession:Lcom/android/internal/view/IInputMethodSession;

    invoke-interface {v0, p1}, Lcom/android/internal/view/IInputMethodSession;->updateCursorAnchorInfo(Landroid/view/inputmethod/CursorAnchorInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_0

    nop

    :goto_0
    goto :goto_5

    :catch_0
    move-exception v0

    goto/32 :goto_1

    nop

    :goto_1
    const-string v1, "InputMethodSessionWrapper"

    goto/32 :goto_2

    nop

    :goto_2
    const-string v2, "IME died"

    goto/32 :goto_4

    nop

    :goto_3
    return-void

    :goto_4
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_5
    goto/32 :goto_3

    nop
.end method

.method updateExtractedText(ILandroid/view/inputmethod/ExtractedText;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodSessionWrapper;->mSession:Lcom/android/internal/view/IInputMethodSession;

    invoke-interface {v0, p1, p2}, Lcom/android/internal/view/IInputMethodSession;->updateExtractedText(ILandroid/view/inputmethod/ExtractedText;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_0

    nop

    :goto_0
    goto :goto_3

    :catch_0
    move-exception v0

    goto/32 :goto_4

    nop

    :goto_1
    return-void

    :goto_2
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_3
    goto/32 :goto_1

    nop

    :goto_4
    const-string v1, "InputMethodSessionWrapper"

    goto/32 :goto_5

    nop

    :goto_5
    const-string v2, "IME died"

    goto/32 :goto_2

    nop
.end method

.method updateSelection(IIIIII)V
    .locals 7

    :try_start_0
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodSessionWrapper;->mSession:Lcom/android/internal/view/IInputMethodSession;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/android/internal/view/IInputMethodSession;->updateSelection(IIIIII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_2

    nop

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_1
    goto/32 :goto_5

    nop

    :goto_2
    goto :goto_1

    :catch_0
    move-exception v0

    goto/32 :goto_4

    nop

    :goto_3
    const-string v2, "IME died"

    goto/32 :goto_0

    nop

    :goto_4
    const-string v1, "InputMethodSessionWrapper"

    goto/32 :goto_3

    nop

    :goto_5
    return-void
.end method

.method viewClicked(Z)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Landroid/view/inputmethod/InputMethodSessionWrapper;->mSession:Lcom/android/internal/view/IInputMethodSession;

    invoke-interface {v0, p1}, Lcom/android/internal/view/IInputMethodSession;->viewClicked(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_2

    nop

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_1
    goto/32 :goto_5

    nop

    :goto_2
    goto :goto_1

    :catch_0
    move-exception v0

    goto/32 :goto_3

    nop

    :goto_3
    const-string v1, "InputMethodSessionWrapper"

    goto/32 :goto_4

    nop

    :goto_4
    const-string v2, "IME died"

    goto/32 :goto_0

    nop

    :goto_5
    return-void
.end method
