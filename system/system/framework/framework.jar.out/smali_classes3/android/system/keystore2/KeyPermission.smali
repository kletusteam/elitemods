.class public interface abstract annotation Landroid/system/keystore2/KeyPermission;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final CONVERT_STORAGE_KEY_TO_EPHEMERAL:I = 0x800

.field public static final DELETE:I = 0x1

.field public static final GEN_UNIQUE_ID:I = 0x2

.field public static final GET_INFO:I = 0x4

.field public static final GRANT:I = 0x8

.field public static final MANAGE_BLOB:I = 0x10

.field public static final NONE:I = 0x0

.field public static final REBIND:I = 0x20

.field public static final REQ_FORCED_OP:I = 0x40

.field public static final UPDATE:I = 0x80

.field public static final USE:I = 0x100

.field public static final USE_DEV_ID:I = 0x200

.field public static final USE_NO_LSKF_BINDING:I = 0x400
