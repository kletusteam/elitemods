.class public interface abstract annotation Landroid/system/keystore2/ResponseCode;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final BACKEND_BUSY:I = 0x12

.field public static final INVALID_ARGUMENT:I = 0x14

.field public static final KEY_NOT_FOUND:I = 0x7

.field public static final KEY_PERMANENTLY_INVALIDATED:I = 0x11

.field public static final LOCKED:I = 0x2

.field public static final OPERATION_BUSY:I = 0x13

.field public static final OUT_OF_KEYS:I = 0x16

.field public static final PERMISSION_DENIED:I = 0x6

.field public static final SYSTEM_ERROR:I = 0x4

.field public static final TOO_MUCH_DATA:I = 0x15

.field public static final UNINITIALIZED:I = 0x3

.field public static final VALUE_CORRUPTED:I = 0x8
