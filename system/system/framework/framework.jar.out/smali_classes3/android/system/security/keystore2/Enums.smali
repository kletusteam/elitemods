.class public final Landroid/system/security/keystore2/Enums;
.super Ljava/lang/Object;


# static fields
.field public static final AES:I = 0x20

.field public static final ALGORITHM_UNSPECIFIED:I = 0x0

.field public static final EC:I = 0x3

.field public static final HMAC:I = 0x80

.field public static final RSA:I = 0x1

.field public static final SECURITY_LEVEL_KEYSTORE:I = 0x4

.field public static final SECURITY_LEVEL_SOFTWARE:I = 0x1

.field public static final SECURITY_LEVEL_STRONGBOX:I = 0x3

.field public static final SECURITY_LEVEL_TRUSTED_ENVIRONMENT:I = 0x2

.field public static final SECURITY_LEVEL_UNSPECIFIED:I = 0x0

.field public static final TRIPLE_DES:I = 0x21


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
