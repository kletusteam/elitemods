.class public interface abstract annotation Landroid/security/apc/ResponseCode;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final ABORTED:I = 0x2

.field public static final CANCELLED:I = 0x1

.field public static final IGNORED:I = 0x4

.field public static final OK:I = 0x0

.field public static final OPERATION_PENDING:I = 0x3

.field public static final PERMISSION_DENIED:I = 0x1e

.field public static final SYSTEM_ERROR:I = 0x5

.field public static final UNIMPLEMENTED:I = 0x6
