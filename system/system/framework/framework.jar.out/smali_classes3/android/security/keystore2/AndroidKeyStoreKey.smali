.class public Landroid/security/keystore2/AndroidKeyStoreKey;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/security/Key;


# instance fields
.field private final mAlgorithm:Ljava/lang/String;

.field private final mAuthorizations:[Landroid/system/keystore2/Authorization;

.field private final mDescriptor:Landroid/system/keystore2/KeyDescriptor;

.field private final mKeyId:J

.field private final mSecurityLevel:Landroid/security/KeyStoreSecurityLevel;


# direct methods
.method public constructor <init>(Landroid/system/keystore2/KeyDescriptor;J[Landroid/system/keystore2/Authorization;Ljava/lang/String;Landroid/security/KeyStoreSecurityLevel;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/security/keystore2/AndroidKeyStoreKey;->mDescriptor:Landroid/system/keystore2/KeyDescriptor;

    iput-wide p2, p0, Landroid/security/keystore2/AndroidKeyStoreKey;->mKeyId:J

    iput-object p4, p0, Landroid/security/keystore2/AndroidKeyStoreKey;->mAuthorizations:[Landroid/system/keystore2/Authorization;

    iput-object p5, p0, Landroid/security/keystore2/AndroidKeyStoreKey;->mAlgorithm:Ljava/lang/String;

    iput-object p6, p0, Landroid/security/keystore2/AndroidKeyStoreKey;->mSecurityLevel:Landroid/security/KeyStoreSecurityLevel;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-nez p1, :cond_1

    return v1

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_2

    return v1

    :cond_2
    move-object v2, p1

    check-cast v2, Landroid/security/keystore2/AndroidKeyStoreKey;

    iget-wide v3, p0, Landroid/security/keystore2/AndroidKeyStoreKey;->mKeyId:J

    iget-wide v5, v2, Landroid/security/keystore2/AndroidKeyStoreKey;->mKeyId:J

    cmp-long v3, v3, v5

    if-nez v3, :cond_3

    goto :goto_0

    :cond_3
    move v0, v1

    :goto_0
    return v0
.end method

.method public getAlgorithm()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/security/keystore2/AndroidKeyStoreKey;->mAlgorithm:Ljava/lang/String;

    return-object v0
.end method

.method getAuthorizations()[Landroid/system/keystore2/Authorization;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Landroid/security/keystore2/AndroidKeyStoreKey;->mAuthorizations:[Landroid/system/keystore2/Authorization;

    goto/32 :goto_0

    nop
.end method

.method public getEncoded()[B
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getFormat()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method getKeyIdDescriptor()Landroid/system/keystore2/KeyDescriptor;
    .locals 3

    goto/32 :goto_8

    nop

    :goto_0
    iget-wide v1, p0, Landroid/security/keystore2/AndroidKeyStoreKey;->mKeyId:J

    goto/32 :goto_1

    nop

    :goto_1
    iput-wide v1, v0, Landroid/system/keystore2/KeyDescriptor;->nspace:J

    goto/32 :goto_7

    nop

    :goto_2
    iput-object v1, v0, Landroid/system/keystore2/KeyDescriptor;->alias:Ljava/lang/String;

    goto/32 :goto_5

    nop

    :goto_3
    return-object v0

    :goto_4
    iput v1, v0, Landroid/system/keystore2/KeyDescriptor;->domain:I

    goto/32 :goto_6

    nop

    :goto_5
    iput-object v1, v0, Landroid/system/keystore2/KeyDescriptor;->blob:[B

    goto/32 :goto_3

    nop

    :goto_6
    const/4 v1, 0x0

    goto/32 :goto_2

    nop

    :goto_7
    const/4 v1, 0x4

    goto/32 :goto_4

    nop

    :goto_8
    new-instance v0, Landroid/system/keystore2/KeyDescriptor;

    goto/32 :goto_9

    nop

    :goto_9
    invoke-direct {v0}, Landroid/system/keystore2/KeyDescriptor;-><init>()V

    goto/32 :goto_0

    nop
.end method

.method getSecurityLevel()Landroid/security/KeyStoreSecurityLevel;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Landroid/security/keystore2/AndroidKeyStoreKey;->mSecurityLevel:Landroid/security/KeyStoreSecurityLevel;

    goto/32 :goto_0

    nop
.end method

.method getUserKeyDescriptor()Landroid/system/keystore2/KeyDescriptor;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Landroid/security/keystore2/AndroidKeyStoreKey;->mDescriptor:Landroid/system/keystore2/KeyDescriptor;

    goto/32 :goto_0

    nop
.end method

.method public hashCode()I
    .locals 7

    const/16 v0, 0x1f

    const/4 v1, 0x1

    mul-int/lit8 v2, v1, 0x1f

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    add-int/2addr v2, v3

    mul-int/lit8 v1, v2, 0x1f

    iget-wide v3, p0, Landroid/security/keystore2/AndroidKeyStoreKey;->mKeyId:J

    const/16 v5, 0x20

    ushr-long v5, v3, v5

    long-to-int v5, v5

    add-int/2addr v1, v5

    mul-int/lit8 v2, v1, 0x1f

    const-wide/16 v5, -0x1

    and-long/2addr v3, v5

    long-to-int v3, v3

    add-int/2addr v2, v3

    return v2
.end method
