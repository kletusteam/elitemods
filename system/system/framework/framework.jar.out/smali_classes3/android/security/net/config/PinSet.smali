.class public final Landroid/security/net/config/PinSet;
.super Ljava/lang/Object;


# static fields
.field public static final EMPTY_PINSET:Landroid/security/net/config/PinSet;


# instance fields
.field public final expirationTime:J

.field public final pins:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Landroid/security/net/config/Pin;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Landroid/security/net/config/PinSet;

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    const-wide v2, 0x7fffffffffffffffL

    invoke-direct {v0, v1, v2, v3}, Landroid/security/net/config/PinSet;-><init>(Ljava/util/Set;J)V

    sput-object v0, Landroid/security/net/config/PinSet;->EMPTY_PINSET:Landroid/security/net/config/PinSet;

    return-void
.end method

.method public constructor <init>(Ljava/util/Set;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Landroid/security/net/config/Pin;",
            ">;J)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    iput-object p1, p0, Landroid/security/net/config/PinSet;->pins:Ljava/util/Set;

    iput-wide p2, p0, Landroid/security/net/config/PinSet;->expirationTime:J

    return-void

    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "pins must not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method getPinAlgorithms()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    goto/32 :goto_a

    nop

    :goto_0
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    goto/32 :goto_d

    nop

    :goto_2
    iget-object v1, p0, Landroid/security/net/config/PinSet;->pins:Ljava/util/Set;

    goto/32 :goto_0

    nop

    :goto_3
    iget-object v3, v2, Landroid/security/net/config/Pin;->digestAlgorithm:Ljava/lang/String;

    goto/32 :goto_4

    nop

    :goto_4
    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/32 :goto_b

    nop

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_6

    nop

    :goto_6
    check-cast v2, Landroid/security/net/config/Pin;

    goto/32 :goto_3

    nop

    :goto_7
    return-object v0

    :goto_8
    if-nez v2, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_5

    nop

    :goto_9
    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    goto/32 :goto_2

    nop

    :goto_a
    new-instance v0, Landroid/util/ArraySet;

    goto/32 :goto_9

    nop

    :goto_b
    goto :goto_1

    :goto_c
    goto/32 :goto_7

    nop

    :goto_d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    goto/32 :goto_8

    nop
.end method
