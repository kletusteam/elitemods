.class public Landroid/security/identity/PersonalizationData;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/security/identity/PersonalizationData$Builder;,
        Landroid/security/identity/PersonalizationData$EntryData;,
        Landroid/security/identity/PersonalizationData$NamespaceData;
    }
.end annotation


# instance fields
.field private mNamespaces:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Landroid/security/identity/PersonalizationData$NamespaceData;",
            ">;"
        }
    .end annotation
.end field

.field private mProfiles:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Landroid/security/identity/AccessControlProfile;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetmNamespaces(Landroid/security/identity/PersonalizationData;)Ljava/util/LinkedHashMap;
    .locals 0

    iget-object p0, p0, Landroid/security/identity/PersonalizationData;->mNamespaces:Ljava/util/LinkedHashMap;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmProfiles(Landroid/security/identity/PersonalizationData;)Ljava/util/LinkedList;
    .locals 0

    iget-object p0, p0, Landroid/security/identity/PersonalizationData;->mProfiles:Ljava/util/LinkedList;

    return-object p0
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Landroid/security/identity/PersonalizationData;->mProfiles:Ljava/util/LinkedList;

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Landroid/security/identity/PersonalizationData;->mNamespaces:Ljava/util/LinkedHashMap;

    return-void
.end method

.method synthetic constructor <init>(Landroid/security/identity/PersonalizationData-IA;)V
    .locals 0

    invoke-direct {p0}, Landroid/security/identity/PersonalizationData;-><init>()V

    return-void
.end method


# virtual methods
.method getAccessControlProfiles()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Landroid/security/identity/AccessControlProfile;",
            ">;"
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/security/identity/PersonalizationData;->mProfiles:Ljava/util/LinkedList;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_2
    return-object v0
.end method

.method getNamespaceData(Ljava/lang/String;)Landroid/security/identity/PersonalizationData$NamespaceData;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/security/identity/PersonalizationData;->mNamespaces:Ljava/util/LinkedHashMap;

    goto/32 :goto_3

    nop

    :goto_1
    check-cast v0, Landroid/security/identity/PersonalizationData$NamespaceData;

    goto/32 :goto_2

    nop

    :goto_2
    return-object v0

    :goto_3
    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_1

    nop
.end method

.method getNamespaces()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/security/identity/PersonalizationData;->mNamespaces:Ljava/util/LinkedHashMap;

    goto/32 :goto_3

    nop

    :goto_1
    return-object v0

    :goto_2
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_3
    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    goto/32 :goto_2

    nop
.end method
