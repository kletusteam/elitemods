.class public Landroid/security/identity/AccessControlProfile;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/security/identity/AccessControlProfile$Builder;
    }
.end annotation


# instance fields
.field private mAccessControlProfileId:Landroid/security/identity/AccessControlProfileId;

.field private mReaderCertificate:Ljava/security/cert/X509Certificate;

.field private mUserAuthenticationRequired:Z

.field private mUserAuthenticationTimeout:J


# direct methods
.method static bridge synthetic -$$Nest$fputmAccessControlProfileId(Landroid/security/identity/AccessControlProfile;Landroid/security/identity/AccessControlProfileId;)V
    .locals 0

    iput-object p1, p0, Landroid/security/identity/AccessControlProfile;->mAccessControlProfileId:Landroid/security/identity/AccessControlProfileId;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmReaderCertificate(Landroid/security/identity/AccessControlProfile;Ljava/security/cert/X509Certificate;)V
    .locals 0

    iput-object p1, p0, Landroid/security/identity/AccessControlProfile;->mReaderCertificate:Ljava/security/cert/X509Certificate;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmUserAuthenticationRequired(Landroid/security/identity/AccessControlProfile;Z)V
    .locals 0

    iput-boolean p1, p0, Landroid/security/identity/AccessControlProfile;->mUserAuthenticationRequired:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmUserAuthenticationTimeout(Landroid/security/identity/AccessControlProfile;J)V
    .locals 0

    iput-wide p1, p0, Landroid/security/identity/AccessControlProfile;->mUserAuthenticationTimeout:J

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/security/identity/AccessControlProfileId;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/security/identity/AccessControlProfileId;-><init>(I)V

    iput-object v0, p0, Landroid/security/identity/AccessControlProfile;->mAccessControlProfileId:Landroid/security/identity/AccessControlProfileId;

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/security/identity/AccessControlProfile;->mReaderCertificate:Ljava/security/cert/X509Certificate;

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/security/identity/AccessControlProfile;->mUserAuthenticationRequired:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Landroid/security/identity/AccessControlProfile;->mUserAuthenticationTimeout:J

    return-void
.end method

.method synthetic constructor <init>(Landroid/security/identity/AccessControlProfile-IA;)V
    .locals 0

    invoke-direct {p0}, Landroid/security/identity/AccessControlProfile;-><init>()V

    return-void
.end method


# virtual methods
.method getAccessControlProfileId()Landroid/security/identity/AccessControlProfileId;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/security/identity/AccessControlProfile;->mAccessControlProfileId:Landroid/security/identity/AccessControlProfileId;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method getReaderCertificate()Ljava/security/cert/X509Certificate;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Landroid/security/identity/AccessControlProfile;->mReaderCertificate:Ljava/security/cert/X509Certificate;

    goto/32 :goto_0

    nop
.end method

.method getUserAuthenticationTimeout()J
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    return-wide v0

    :goto_1
    iget-wide v0, p0, Landroid/security/identity/AccessControlProfile;->mUserAuthenticationTimeout:J

    goto/32 :goto_0

    nop
.end method

.method isUserAuthenticationRequired()Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-boolean v0, p0, Landroid/security/identity/AccessControlProfile;->mUserAuthenticationRequired:Z

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method
