.class public interface abstract annotation Landroid/security/authorization/ResponseCode;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final AUTH_TOKEN_EXPIRED:I = 0x2

.field public static final INVALID_ARGUMENT:I = 0x14

.field public static final KEY_NOT_FOUND:I = 0x7

.field public static final NO_AUTH_TOKEN_FOUND:I = 0x1

.field public static final PERMISSION_DENIED:I = 0x6

.field public static final SYSTEM_ERROR:I = 0x4

.field public static final VALUE_CORRUPTED:I = 0x8
