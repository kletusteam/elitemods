.class public Landroid/security/KeyStoreOperation;
.super Ljava/lang/Object;


# static fields
.field static final TAG:Ljava/lang/String; = "KeyStoreOperation"


# instance fields
.field private final mChallenge:Ljava/lang/Long;

.field private final mOperation:Landroid/system/keystore2/IKeystoreOperation;

.field private final mParameters:[Landroid/hardware/security/keymint/KeyParameter;


# direct methods
.method public constructor <init>(Landroid/system/keystore2/IKeystoreOperation;Ljava/lang/Long;[Landroid/hardware/security/keymint/KeyParameter;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-interface {p1}, Landroid/system/keystore2/IKeystoreOperation;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/os/Binder;->allowBlocking(Landroid/os/IBinder;)Landroid/os/IBinder;

    iput-object p1, p0, Landroid/security/KeyStoreOperation;->mOperation:Landroid/system/keystore2/IKeystoreOperation;

    iput-object p2, p0, Landroid/security/KeyStoreOperation;->mChallenge:Ljava/lang/Long;

    iput-object p3, p0, Landroid/security/KeyStoreOperation;->mParameters:[Landroid/hardware/security/keymint/KeyParameter;

    return-void
.end method

.method private handleExceptions(Landroid/security/CheckedRemoteRequest;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/security/CheckedRemoteRequest<",
            "TR;>;)TR;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/security/KeyStoreException;
        }
    .end annotation

    :try_start_0
    invoke-interface {p1}, Landroid/security/CheckedRemoteRequest;->execute()Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catch Landroid/os/ServiceSpecificException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "KeyStoreOperation"

    const-string v2, "Remote exception while advancing a KeyStoreOperation."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v1, Landroid/security/KeyStoreException;

    const/16 v2, -0x1c

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-direct {v1, v2, v4, v3}, Landroid/security/KeyStoreException;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    throw v1

    :catch_1
    move-exception v0

    iget v1, v0, Landroid/os/ServiceSpecificException;->errorCode:I

    packed-switch v1, :pswitch_data_0

    iget v1, v0, Landroid/os/ServiceSpecificException;->errorCode:I

    invoke-virtual {v0}, Landroid/os/ServiceSpecificException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/security/KeyStore2;->getKeyStoreException(ILjava/lang/String;)Landroid/security/KeyStoreException;

    move-result-object v1

    throw v1

    :pswitch_0
    new-instance v1, Ljava/lang/IllegalThreadStateException;

    const-string v2, "Cannot update the same operation concurrently."

    invoke-direct {v1, v2}, Ljava/lang/IllegalThreadStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public abort()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/security/KeyStoreException;
        }
    .end annotation

    new-instance v0, Landroid/security/KeyStoreOperation$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Landroid/security/KeyStoreOperation$$ExternalSyntheticLambda0;-><init>(Landroid/security/KeyStoreOperation;)V

    invoke-direct {p0, v0}, Landroid/security/KeyStoreOperation;->handleExceptions(Landroid/security/CheckedRemoteRequest;)Ljava/lang/Object;

    return-void
.end method

.method public finish([B[B)[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/security/KeyStoreException;
        }
    .end annotation

    new-instance v0, Landroid/security/KeyStoreOperation$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0, p1, p2}, Landroid/security/KeyStoreOperation$$ExternalSyntheticLambda2;-><init>(Landroid/security/KeyStoreOperation;[B[B)V

    invoke-direct {p0, v0}, Landroid/security/KeyStoreOperation;->handleExceptions(Landroid/security/CheckedRemoteRequest;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    return-object v0
.end method

.method public getChallenge()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Landroid/security/KeyStoreOperation;->mChallenge:Ljava/lang/Long;

    return-object v0
.end method

.method public getParameters()[Landroid/hardware/security/keymint/KeyParameter;
    .locals 1

    iget-object v0, p0, Landroid/security/KeyStoreOperation;->mParameters:[Landroid/hardware/security/keymint/KeyParameter;

    return-object v0
.end method

.method synthetic lambda$abort$3$android-security-KeyStoreOperation()Ljava/lang/Integer;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    goto/32 :goto_3

    nop

    :goto_0
    return-object v0

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_4

    nop

    :goto_2
    invoke-interface {v0}, Landroid/system/keystore2/IKeystoreOperation;->abort()V

    goto/32 :goto_1

    nop

    :goto_3
    iget-object v0, p0, Landroid/security/KeyStoreOperation;->mOperation:Landroid/system/keystore2/IKeystoreOperation;

    goto/32 :goto_2

    nop

    :goto_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/32 :goto_0

    nop
.end method

.method synthetic lambda$finish$2$android-security-KeyStoreOperation([B[B)[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    invoke-interface {v0, p1, p2}, Landroid/system/keystore2/IKeystoreOperation;->finish([B[B)[B

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0

    :goto_2
    iget-object v0, p0, Landroid/security/KeyStoreOperation;->mOperation:Landroid/system/keystore2/IKeystoreOperation;

    goto/32 :goto_0

    nop
.end method

.method synthetic lambda$update$1$android-security-KeyStoreOperation([B)[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/security/KeyStoreOperation;->mOperation:Landroid/system/keystore2/IKeystoreOperation;

    goto/32 :goto_2

    nop

    :goto_1
    return-object v0

    :goto_2
    invoke-interface {v0, p1}, Landroid/system/keystore2/IKeystoreOperation;->update([B)[B

    move-result-object v0

    goto/32 :goto_1

    nop
.end method

.method synthetic lambda$updateAad$0$android-security-KeyStoreOperation([B)Ljava/lang/Integer;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/security/KeyStoreOperation;->mOperation:Landroid/system/keystore2/IKeystoreOperation;

    goto/32 :goto_2

    nop

    :goto_1
    return-object v0

    :goto_2
    invoke-interface {v0, p1}, Landroid/system/keystore2/IKeystoreOperation;->updateAad([B)V

    goto/32 :goto_4

    nop

    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_4
    const/4 v0, 0x0

    goto/32 :goto_3

    nop
.end method

.method public update([B)[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/security/KeyStoreException;
        }
    .end annotation

    new-instance v0, Landroid/security/KeyStoreOperation$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0, p1}, Landroid/security/KeyStoreOperation$$ExternalSyntheticLambda1;-><init>(Landroid/security/KeyStoreOperation;[B)V

    invoke-direct {p0, v0}, Landroid/security/KeyStoreOperation;->handleExceptions(Landroid/security/CheckedRemoteRequest;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    return-object v0
.end method

.method public updateAad([B)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/security/KeyStoreException;
        }
    .end annotation

    new-instance v0, Landroid/security/KeyStoreOperation$$ExternalSyntheticLambda3;

    invoke-direct {v0, p0, p1}, Landroid/security/KeyStoreOperation$$ExternalSyntheticLambda3;-><init>(Landroid/security/KeyStoreOperation;[B)V

    invoke-direct {p0, v0}, Landroid/security/KeyStoreOperation;->handleExceptions(Landroid/security/CheckedRemoteRequest;)Ljava/lang/Object;

    return-void
.end method
