.class public interface abstract annotation Landroid/security/metrics/Outcome;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final ABORT:I = 0x3

.field public static final DROPPED:I = 0x1

.field public static final ERROR:I = 0x5

.field public static final OUTCOME_UNSPECIFIED:I = 0x0

.field public static final PRUNED:I = 0x4

.field public static final SUCCESS:I = 0x2
