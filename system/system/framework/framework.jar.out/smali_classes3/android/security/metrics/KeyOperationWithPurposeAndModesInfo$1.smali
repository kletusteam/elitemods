.class Landroid/security/metrics/KeyOperationWithPurposeAndModesInfo$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/security/metrics/KeyOperationWithPurposeAndModesInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Landroid/security/metrics/KeyOperationWithPurposeAndModesInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Landroid/security/metrics/KeyOperationWithPurposeAndModesInfo;
    .locals 1

    new-instance v0, Landroid/security/metrics/KeyOperationWithPurposeAndModesInfo;

    invoke-direct {v0}, Landroid/security/metrics/KeyOperationWithPurposeAndModesInfo;-><init>()V

    invoke-virtual {v0, p1}, Landroid/security/metrics/KeyOperationWithPurposeAndModesInfo;->readFromParcel(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Landroid/security/metrics/KeyOperationWithPurposeAndModesInfo$1;->createFromParcel(Landroid/os/Parcel;)Landroid/security/metrics/KeyOperationWithPurposeAndModesInfo;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Landroid/security/metrics/KeyOperationWithPurposeAndModesInfo;
    .locals 1

    new-array v0, p1, [Landroid/security/metrics/KeyOperationWithPurposeAndModesInfo;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Landroid/security/metrics/KeyOperationWithPurposeAndModesInfo$1;->newArray(I)[Landroid/security/metrics/KeyOperationWithPurposeAndModesInfo;

    move-result-object p1

    return-object p1
.end method
