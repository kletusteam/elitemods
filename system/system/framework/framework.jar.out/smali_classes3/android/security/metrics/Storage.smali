.class public interface abstract annotation Landroid/security/metrics/Storage;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final AUTH_TOKEN:I = 0xb

.field public static final BLOB_ENTRY:I = 0x4

.field public static final BLOB_ENTRY_KEY_ENTRY_ID_INDEX:I = 0x5

.field public static final BLOB_METADATA:I = 0xc

.field public static final BLOB_METADATA_BLOB_ENTRY_ID_INDEX:I = 0xd

.field public static final DATABASE:I = 0xf

.field public static final GRANT:I = 0xa

.field public static final KEY_ENTRY:I = 0x1

.field public static final KEY_ENTRY_DOMAIN_NAMESPACE_INDEX:I = 0x3

.field public static final KEY_ENTRY_ID_INDEX:I = 0x2

.field public static final KEY_METADATA:I = 0x8

.field public static final KEY_METADATA_KEY_ENTRY_ID_INDEX:I = 0x9

.field public static final KEY_PARAMETER:I = 0x6

.field public static final KEY_PARAMETER_KEY_ENTRY_ID_INDEX:I = 0x7

.field public static final LEGACY_STORAGE:I = 0x10

.field public static final METADATA:I = 0xe

.field public static final STORAGE_UNSPECIFIED:I
