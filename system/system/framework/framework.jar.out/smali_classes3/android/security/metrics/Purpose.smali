.class public interface abstract annotation Landroid/security/metrics/Purpose;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final AGREE_KEY:I = 0x7

.field public static final ATTEST_KEY:I = 0x8

.field public static final DECRYPT:I = 0x2

.field public static final ENCRYPT:I = 0x1

.field public static final KEY_PURPOSE_UNSPECIFIED:I = 0x0

.field public static final SIGN:I = 0x3

.field public static final VERIFY:I = 0x4

.field public static final WRAP_KEY:I = 0x6
