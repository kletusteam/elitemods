.class public interface abstract annotation Landroid/security/metrics/PoolStatus;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final ATTESTED:I = 0x3

.field public static final EXPIRING:I = 0x1

.field public static final TOTAL:I = 0x4

.field public static final UNASSIGNED:I = 0x2
