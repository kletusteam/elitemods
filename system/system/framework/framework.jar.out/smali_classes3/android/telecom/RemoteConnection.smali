.class public final Landroid/telecom/RemoteConnection;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/telecom/RemoteConnection$CallbackRecord;,
        Landroid/telecom/RemoteConnection$VideoProvider;,
        Landroid/telecom/RemoteConnection$Callback;
    }
.end annotation


# instance fields
.field private mAddress:Landroid/net/Uri;

.field private mAddressPresentation:I

.field private final mCallbackRecords:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Landroid/telecom/RemoteConnection$CallbackRecord;",
            ">;"
        }
    .end annotation
.end field

.field private mCallerDisplayName:Ljava/lang/String;

.field private mCallerDisplayNamePresentation:I

.field private mCallingPackageAbbreviation:Ljava/lang/String;

.field private mConference:Landroid/telecom/RemoteConference;

.field private final mConferenceableConnections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/telecom/RemoteConnection;",
            ">;"
        }
    .end annotation
.end field

.field private mConnected:Z

.field private mConnectionCapabilities:I

.field private final mConnectionId:Ljava/lang/String;

.field private mConnectionProperties:I

.field private mConnectionService:Lcom/android/internal/telecom/IConnectionService;

.field private mDisconnectCause:Landroid/telecom/DisconnectCause;

.field private mExtras:Landroid/os/Bundle;

.field private mIsVoipAudioMode:Z

.field private mRingbackRequested:Z

.field private mState:I

.field private mStatusHints:Landroid/telecom/StatusHints;

.field private final mUnmodifiableconferenceableConnections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/telecom/RemoteConnection;",
            ">;"
        }
    .end annotation
.end field

.field private mVideoProvider:Landroid/telecom/RemoteConnection$VideoProvider;

.field private mVideoState:I


# direct methods
.method static bridge synthetic -$$Nest$fgetmExtras(Landroid/telecom/RemoteConnection;)Landroid/os/Bundle;
    .locals 0

    iget-object p0, p0, Landroid/telecom/RemoteConnection;->mExtras:Landroid/os/Bundle;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmUnmodifiableconferenceableConnections(Landroid/telecom/RemoteConnection;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Landroid/telecom/RemoteConnection;->mUnmodifiableconferenceableConnections:Ljava/util/List;

    return-object p0
.end method

.method constructor <init>(Landroid/telecom/DisconnectCause;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v1, 0x8

    const v2, 0x3f666666    # 0.9f

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/telecom/RemoteConnection;->mConferenceableConnections:Ljava/util/List;

    nop

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Landroid/telecom/RemoteConnection;->mUnmodifiableconferenceableConnections:Ljava/util/List;

    iput v3, p0, Landroid/telecom/RemoteConnection;->mState:I

    const-string v0, "NULL"

    iput-object v0, p0, Landroid/telecom/RemoteConnection;->mConnectionId:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/telecom/RemoteConnection;->mConnected:Z

    const/4 v0, 0x6

    iput v0, p0, Landroid/telecom/RemoteConnection;->mState:I

    iput-object p1, p0, Landroid/telecom/RemoteConnection;->mDisconnectCause:Landroid/telecom/DisconnectCause;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lcom/android/internal/telecom/IConnectionService;Landroid/telecom/ConnectionRequest;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v1, 0x8

    const v2, 0x3f666666    # 0.9f

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/telecom/RemoteConnection;->mConferenceableConnections:Ljava/util/List;

    nop

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Landroid/telecom/RemoteConnection;->mUnmodifiableconferenceableConnections:Ljava/util/List;

    iput v3, p0, Landroid/telecom/RemoteConnection;->mState:I

    iput-object p1, p0, Landroid/telecom/RemoteConnection;->mConnectionId:Ljava/lang/String;

    iput-object p2, p0, Landroid/telecom/RemoteConnection;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iput-boolean v3, p0, Landroid/telecom/RemoteConnection;->mConnected:Z

    const/4 v0, 0x0

    iput v0, p0, Landroid/telecom/RemoteConnection;->mState:I

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/telecom/ConnectionRequest;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p3}, Landroid/telecom/ConnectionRequest;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "android.telecom.extra.REMOTE_CONNECTION_ORIGINATING_PACKAGE_NAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p3}, Landroid/telecom/ConnectionRequest;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telecom/Log;->getPackageAbbreviation(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/telecom/RemoteConnection;->mCallingPackageAbbreviation:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lcom/android/internal/telecom/IConnectionService;Landroid/telecom/ParcelableConnection;Ljava/lang/String;I)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v1, 0x8

    const v2, 0x3f666666    # 0.9f

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/telecom/RemoteConnection;->mConferenceableConnections:Ljava/util/List;

    nop

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Landroid/telecom/RemoteConnection;->mUnmodifiableconferenceableConnections:Ljava/util/List;

    iput v3, p0, Landroid/telecom/RemoteConnection;->mState:I

    iput-object p1, p0, Landroid/telecom/RemoteConnection;->mConnectionId:Ljava/lang/String;

    iput-object p2, p0, Landroid/telecom/RemoteConnection;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iput-boolean v3, p0, Landroid/telecom/RemoteConnection;->mConnected:Z

    invoke-virtual {p3}, Landroid/telecom/ParcelableConnection;->getState()I

    move-result v0

    iput v0, p0, Landroid/telecom/RemoteConnection;->mState:I

    invoke-virtual {p3}, Landroid/telecom/ParcelableConnection;->getDisconnectCause()Landroid/telecom/DisconnectCause;

    move-result-object v0

    iput-object v0, p0, Landroid/telecom/RemoteConnection;->mDisconnectCause:Landroid/telecom/DisconnectCause;

    invoke-virtual {p3}, Landroid/telecom/ParcelableConnection;->isRingbackRequested()Z

    move-result v0

    iput-boolean v0, p0, Landroid/telecom/RemoteConnection;->mRingbackRequested:Z

    invoke-virtual {p3}, Landroid/telecom/ParcelableConnection;->getConnectionCapabilities()I

    move-result v0

    iput v0, p0, Landroid/telecom/RemoteConnection;->mConnectionCapabilities:I

    invoke-virtual {p3}, Landroid/telecom/ParcelableConnection;->getConnectionProperties()I

    move-result v0

    iput v0, p0, Landroid/telecom/RemoteConnection;->mConnectionProperties:I

    invoke-virtual {p3}, Landroid/telecom/ParcelableConnection;->getVideoState()I

    move-result v0

    iput v0, p0, Landroid/telecom/RemoteConnection;->mVideoState:I

    invoke-virtual {p3}, Landroid/telecom/ParcelableConnection;->getVideoProvider()Lcom/android/internal/telecom/IVideoProvider;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-instance v2, Landroid/telecom/RemoteConnection$VideoProvider;

    invoke-direct {v2, v0, p4, p5}, Landroid/telecom/RemoteConnection$VideoProvider;-><init>(Lcom/android/internal/telecom/IVideoProvider;Ljava/lang/String;I)V

    iput-object v2, p0, Landroid/telecom/RemoteConnection;->mVideoProvider:Landroid/telecom/RemoteConnection$VideoProvider;

    goto :goto_0

    :cond_0
    iput-object v1, p0, Landroid/telecom/RemoteConnection;->mVideoProvider:Landroid/telecom/RemoteConnection$VideoProvider;

    :goto_0
    invoke-virtual {p3}, Landroid/telecom/ParcelableConnection;->getIsVoipAudioMode()Z

    move-result v2

    iput-boolean v2, p0, Landroid/telecom/RemoteConnection;->mIsVoipAudioMode:Z

    invoke-virtual {p3}, Landroid/telecom/ParcelableConnection;->getStatusHints()Landroid/telecom/StatusHints;

    move-result-object v2

    iput-object v2, p0, Landroid/telecom/RemoteConnection;->mStatusHints:Landroid/telecom/StatusHints;

    invoke-virtual {p3}, Landroid/telecom/ParcelableConnection;->getHandle()Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Landroid/telecom/RemoteConnection;->mAddress:Landroid/net/Uri;

    invoke-virtual {p3}, Landroid/telecom/ParcelableConnection;->getHandlePresentation()I

    move-result v2

    iput v2, p0, Landroid/telecom/RemoteConnection;->mAddressPresentation:I

    invoke-virtual {p3}, Landroid/telecom/ParcelableConnection;->getCallerDisplayName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Landroid/telecom/RemoteConnection;->mCallerDisplayName:Ljava/lang/String;

    invoke-virtual {p3}, Landroid/telecom/ParcelableConnection;->getCallerDisplayNamePresentation()I

    move-result v2

    iput v2, p0, Landroid/telecom/RemoteConnection;->mCallerDisplayNamePresentation:I

    iput-object v1, p0, Landroid/telecom/RemoteConnection;->mConference:Landroid/telecom/RemoteConference;

    invoke-virtual {p3}, Landroid/telecom/ParcelableConnection;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/telecom/RemoteConnection;->putExtras(Landroid/os/Bundle;)V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "android.telecom.extra.ORIGINAL_CONNECTION_ID"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Landroid/telecom/RemoteConnection;->putExtras(Landroid/os/Bundle;)V

    invoke-static {p4}, Landroid/telecom/Log;->getPackageAbbreviation(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Landroid/telecom/RemoteConnection;->mCallingPackageAbbreviation:Ljava/lang/String;

    return-void
.end method

.method public static failure(Landroid/telecom/DisconnectCause;)Landroid/telecom/RemoteConnection;
    .locals 1

    new-instance v0, Landroid/telecom/RemoteConnection;

    invoke-direct {v0, p0}, Landroid/telecom/RemoteConnection;-><init>(Landroid/telecom/DisconnectCause;)V

    return-object v0
.end method

.method private getActiveOwnerInfo()Ljava/lang/String;
    .locals 2

    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    return-object v1

    :cond_0
    iget-object v1, v0, Landroid/telecom/Logging/Session$Info;->ownerInfo:Ljava/lang/String;

    return-object v1
.end method

.method static synthetic lambda$onRemoteRttRequest$3(Landroid/telecom/RemoteConnection$Callback;Landroid/telecom/RemoteConnection;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/telecom/RemoteConnection$Callback;->onRemoteRttRequest(Landroid/telecom/RemoteConnection;)V

    return-void
.end method

.method static synthetic lambda$onRttInitiationFailure$1(Landroid/telecom/RemoteConnection$Callback;Landroid/telecom/RemoteConnection;I)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Landroid/telecom/RemoteConnection$Callback;->onRttInitiationFailure(Landroid/telecom/RemoteConnection;I)V

    return-void
.end method

.method static synthetic lambda$onRttInitiationSuccess$0(Landroid/telecom/RemoteConnection$Callback;Landroid/telecom/RemoteConnection;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/telecom/RemoteConnection$Callback;->onRttInitiationSuccess(Landroid/telecom/RemoteConnection;)V

    return-void
.end method

.method static synthetic lambda$onRttSessionRemotelyTerminated$2(Landroid/telecom/RemoteConnection$Callback;Landroid/telecom/RemoteConnection;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/telecom/RemoteConnection$Callback;->onRttSessionRemotelyTerminated(Landroid/telecom/RemoteConnection;)V

    return-void
.end method

.method private notifyExtrasChanged()V
    .locals 6

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/RemoteConnection$CallbackRecord;

    move-object v2, p0

    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getCallback()Landroid/telecom/RemoteConnection$Callback;

    move-result-object v3

    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v4

    new-instance v5, Landroid/telecom/RemoteConnection$17;

    invoke-direct {v5, p0, v3, v2}, Landroid/telecom/RemoteConnection$17;-><init>(Landroid/telecom/RemoteConnection;Landroid/telecom/RemoteConnection$Callback;Landroid/telecom/RemoteConnection;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public abort()V
    .locals 3

    invoke-direct {p0}, Landroid/telecom/RemoteConnection;->getActiveOwnerInfo()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RC.a"

    invoke-static {v1, v0}, Landroid/telecom/Log;->startSession(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-boolean v0, p0, Landroid/telecom/RemoteConnection;->mConnected:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iget-object v1, p0, Landroid/telecom/RemoteConnection;->mConnectionId:Ljava/lang/String;

    iget-object v2, p0, Landroid/telecom/RemoteConnection;->mCallingPackageAbbreviation:Ljava/lang/String;

    invoke-static {v2}, Landroid/telecom/Log;->getExternalSession(Ljava/lang/String;)Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/android/internal/telecom/IConnectionService;->abort(Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/telecom/Log;->endSession()V

    throw v0

    :catch_0
    move-exception v0

    :cond_0
    :goto_0
    invoke-static {}, Landroid/telecom/Log;->endSession()V

    nop

    return-void
.end method

.method public addConferenceParticipants(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    :try_start_0
    iget-boolean v0, p0, Landroid/telecom/RemoteConnection;->mConnected:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iget-object v1, p0, Landroid/telecom/RemoteConnection;->mConnectionId:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, p1, v2}, Lcom/android/internal/telecom/IConnectionService;->addConferenceParticipants(Ljava/lang/String;Ljava/util/List;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    goto :goto_0

    :catch_0
    move-exception v0

    :goto_0
    return-void
.end method

.method public answer()V
    .locals 3

    invoke-direct {p0}, Landroid/telecom/RemoteConnection;->getActiveOwnerInfo()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RC.an"

    invoke-static {v1, v0}, Landroid/telecom/Log;->startSession(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-boolean v0, p0, Landroid/telecom/RemoteConnection;->mConnected:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iget-object v1, p0, Landroid/telecom/RemoteConnection;->mConnectionId:Ljava/lang/String;

    iget-object v2, p0, Landroid/telecom/RemoteConnection;->mCallingPackageAbbreviation:Ljava/lang/String;

    invoke-static {v2}, Landroid/telecom/Log;->getExternalSession(Ljava/lang/String;)Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/android/internal/telecom/IConnectionService;->answer(Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/telecom/Log;->endSession()V

    throw v0

    :catch_0
    move-exception v0

    :cond_0
    :goto_0
    invoke-static {}, Landroid/telecom/Log;->endSession()V

    nop

    return-void
.end method

.method public answer(I)V
    .locals 3

    invoke-direct {p0}, Landroid/telecom/RemoteConnection;->getActiveOwnerInfo()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RC.an2"

    invoke-static {v1, v0}, Landroid/telecom/Log;->startSession(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-boolean v0, p0, Landroid/telecom/RemoteConnection;->mConnected:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iget-object v1, p0, Landroid/telecom/RemoteConnection;->mConnectionId:Ljava/lang/String;

    iget-object v2, p0, Landroid/telecom/RemoteConnection;->mCallingPackageAbbreviation:Ljava/lang/String;

    invoke-static {v2}, Landroid/telecom/Log;->getExternalSession(Ljava/lang/String;)Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v0, v1, p1, v2}, Lcom/android/internal/telecom/IConnectionService;->answerVideo(Ljava/lang/String;ILandroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/telecom/Log;->endSession()V

    throw v0

    :catch_0
    move-exception v0

    :cond_0
    :goto_0
    invoke-static {}, Landroid/telecom/Log;->endSession()V

    nop

    return-void
.end method

.method public disconnect()V
    .locals 3

    invoke-direct {p0}, Landroid/telecom/RemoteConnection;->getActiveOwnerInfo()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RC.d"

    invoke-static {v1, v0}, Landroid/telecom/Log;->startSession(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-boolean v0, p0, Landroid/telecom/RemoteConnection;->mConnected:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iget-object v1, p0, Landroid/telecom/RemoteConnection;->mConnectionId:Ljava/lang/String;

    iget-object v2, p0, Landroid/telecom/RemoteConnection;->mCallingPackageAbbreviation:Ljava/lang/String;

    invoke-static {v2}, Landroid/telecom/Log;->getExternalSession(Ljava/lang/String;)Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/android/internal/telecom/IConnectionService;->disconnect(Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/telecom/Log;->endSession()V

    throw v0

    :catch_0
    move-exception v0

    :cond_0
    :goto_0
    invoke-static {}, Landroid/telecom/Log;->endSession()V

    nop

    return-void
.end method

.method public getAddress()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mAddress:Landroid/net/Uri;

    return-object v0
.end method

.method public getAddressPresentation()I
    .locals 1

    iget v0, p0, Landroid/telecom/RemoteConnection;->mAddressPresentation:I

    return v0
.end method

.method public getCallerDisplayName()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallerDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getCallerDisplayNamePresentation()I
    .locals 1

    iget v0, p0, Landroid/telecom/RemoteConnection;->mCallerDisplayNamePresentation:I

    return v0
.end method

.method public getConference()Landroid/telecom/RemoteConference;
    .locals 1

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mConference:Landroid/telecom/RemoteConference;

    return-object v0
.end method

.method public getConferenceableConnections()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/telecom/RemoteConnection;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mUnmodifiableconferenceableConnections:Ljava/util/List;

    return-object v0
.end method

.method public getConnectionCapabilities()I
    .locals 1

    iget v0, p0, Landroid/telecom/RemoteConnection;->mConnectionCapabilities:I

    return v0
.end method

.method public getConnectionProperties()I
    .locals 1

    iget v0, p0, Landroid/telecom/RemoteConnection;->mConnectionProperties:I

    return v0
.end method

.method getConnectionService()Lcom/android/internal/telecom/IConnectionService;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    goto/32 :goto_0

    nop
.end method

.method public getDisconnectCause()Landroid/telecom/DisconnectCause;
    .locals 1

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mDisconnectCause:Landroid/telecom/DisconnectCause;

    return-object v0
.end method

.method public final getExtras()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mExtras:Landroid/os/Bundle;

    return-object v0
.end method

.method getId()Ljava/lang/String;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mConnectionId:Ljava/lang/String;

    goto/32 :goto_0

    nop
.end method

.method public getState()I
    .locals 1

    iget v0, p0, Landroid/telecom/RemoteConnection;->mState:I

    return v0
.end method

.method public getStatusHints()Landroid/telecom/StatusHints;
    .locals 1

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mStatusHints:Landroid/telecom/StatusHints;

    return-object v0
.end method

.method public final getVideoProvider()Landroid/telecom/RemoteConnection$VideoProvider;
    .locals 1

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mVideoProvider:Landroid/telecom/RemoteConnection$VideoProvider;

    return-object v0
.end method

.method public getVideoState()I
    .locals 1

    iget v0, p0, Landroid/telecom/RemoteConnection;->mVideoState:I

    return v0
.end method

.method public hold()V
    .locals 3

    invoke-direct {p0}, Landroid/telecom/RemoteConnection;->getActiveOwnerInfo()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RC.h"

    invoke-static {v1, v0}, Landroid/telecom/Log;->startSession(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-boolean v0, p0, Landroid/telecom/RemoteConnection;->mConnected:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iget-object v1, p0, Landroid/telecom/RemoteConnection;->mConnectionId:Ljava/lang/String;

    iget-object v2, p0, Landroid/telecom/RemoteConnection;->mCallingPackageAbbreviation:Ljava/lang/String;

    invoke-static {v2}, Landroid/telecom/Log;->getExternalSession(Ljava/lang/String;)Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/android/internal/telecom/IConnectionService;->hold(Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/telecom/Log;->endSession()V

    throw v0

    :catch_0
    move-exception v0

    :cond_0
    :goto_0
    invoke-static {}, Landroid/telecom/Log;->endSession()V

    nop

    return-void
.end method

.method public isRingbackRequested()Z
    .locals 1

    iget-boolean v0, p0, Landroid/telecom/RemoteConnection;->mRingbackRequested:Z

    return v0
.end method

.method public isVoipAudioMode()Z
    .locals 1

    iget-boolean v0, p0, Landroid/telecom/RemoteConnection;->mIsVoipAudioMode:Z

    return v0
.end method

.method public onCallFilteringCompleted(Landroid/telecom/Connection$CallFilteringCompletionInfo;)V
    .locals 3
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    invoke-direct {p0}, Landroid/telecom/RemoteConnection;->getActiveOwnerInfo()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RC.oCFC"

    invoke-static {v1, v0}, Landroid/telecom/Log;->startSession(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-boolean v0, p0, Landroid/telecom/RemoteConnection;->mConnected:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iget-object v1, p0, Landroid/telecom/RemoteConnection;->mConnectionId:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, p1, v2}, Lcom/android/internal/telecom/IConnectionService;->onCallFilteringCompleted(Ljava/lang/String;Landroid/telecom/Connection$CallFilteringCompletionInfo;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/telecom/Log;->endSession()V

    throw v0

    :catch_0
    move-exception v0

    :cond_0
    :goto_0
    invoke-static {}, Landroid/telecom/Log;->endSession()V

    nop

    return-void
.end method

.method onConnectionEvent(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 11

    goto/32 :goto_10

    nop

    :goto_0
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v9

    goto/32 :goto_4

    nop

    :goto_1
    if-nez v1, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_5

    nop

    :goto_2
    move-object v5, p0

    goto/32 :goto_12

    nop

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_1

    nop

    :goto_4
    new-instance v10, Landroid/telecom/RemoteConnection$18;

    goto/32 :goto_6

    nop

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_13

    nop

    :goto_6
    move-object v2, v10

    goto/32 :goto_9

    nop

    :goto_7
    move-object v7, p2

    goto/32 :goto_a

    nop

    :goto_8
    return-void

    :goto_9
    move-object v3, p0

    goto/32 :goto_b

    nop

    :goto_a
    invoke-direct/range {v2 .. v7}, Landroid/telecom/RemoteConnection$18;-><init>(Landroid/telecom/RemoteConnection;Landroid/telecom/RemoteConnection$Callback;Landroid/telecom/RemoteConnection;Ljava/lang/String;Landroid/os/Bundle;)V

    goto/32 :goto_11

    nop

    :goto_b
    move-object v4, v8

    goto/32 :goto_14

    nop

    :goto_c
    goto :goto_f

    :goto_d
    goto/32 :goto_8

    nop

    :goto_e
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_f
    goto/32 :goto_3

    nop

    :goto_10
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_e

    nop

    :goto_11
    invoke-virtual {v9, v10}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_c

    nop

    :goto_12
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getCallback()Landroid/telecom/RemoteConnection$Callback;

    move-result-object v8

    goto/32 :goto_0

    nop

    :goto_13
    check-cast v1, Landroid/telecom/RemoteConnection$CallbackRecord;

    goto/32 :goto_2

    nop

    :goto_14
    move-object v6, p1

    goto/32 :goto_7

    nop
.end method

.method onPostDialChar(C)V
    .locals 6

    goto/32 :goto_b

    nop

    :goto_0
    new-instance v5, Landroid/telecom/RemoteConnection$8;

    goto/32 :goto_4

    nop

    :goto_1
    goto :goto_7

    :goto_2
    goto/32 :goto_9

    nop

    :goto_3
    check-cast v1, Landroid/telecom/RemoteConnection$CallbackRecord;

    goto/32 :goto_a

    nop

    :goto_4
    invoke-direct {v5, p0, v3, v2, p1}, Landroid/telecom/RemoteConnection$8;-><init>(Landroid/telecom/RemoteConnection;Landroid/telecom/RemoteConnection$Callback;Landroid/telecom/RemoteConnection;C)V

    goto/32 :goto_d

    nop

    :goto_5
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v4

    goto/32 :goto_0

    nop

    :goto_6
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_7
    goto/32 :goto_e

    nop

    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_3

    nop

    :goto_9
    return-void

    :goto_a
    move-object v2, p0

    goto/32 :goto_c

    nop

    :goto_b
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_6

    nop

    :goto_c
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getCallback()Landroid/telecom/RemoteConnection$Callback;

    move-result-object v3

    goto/32 :goto_5

    nop

    :goto_d
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_1

    nop

    :goto_e
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_f

    nop

    :goto_f
    if-nez v1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_8

    nop
.end method

.method onRemoteRttRequest()V
    .locals 6

    goto/32 :goto_7

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    goto/32 :goto_d

    nop

    :goto_2
    goto :goto_1

    :goto_3
    goto/32 :goto_b

    nop

    :goto_4
    if-nez v1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_8

    nop

    :goto_5
    move-object v2, p0

    goto/32 :goto_e

    nop

    :goto_6
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_2

    nop

    :goto_7
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_0

    nop

    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_f

    nop

    :goto_9
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v4

    goto/32 :goto_a

    nop

    :goto_a
    new-instance v5, Landroid/telecom/RemoteConnection$$ExternalSyntheticLambda2;

    goto/32 :goto_c

    nop

    :goto_b
    return-void

    :goto_c
    invoke-direct {v5, v3, v2}, Landroid/telecom/RemoteConnection$$ExternalSyntheticLambda2;-><init>(Landroid/telecom/RemoteConnection$Callback;Landroid/telecom/RemoteConnection;)V

    goto/32 :goto_6

    nop

    :goto_d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_4

    nop

    :goto_e
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getCallback()Landroid/telecom/RemoteConnection$Callback;

    move-result-object v3

    goto/32 :goto_9

    nop

    :goto_f
    check-cast v1, Landroid/telecom/RemoteConnection$CallbackRecord;

    goto/32 :goto_5

    nop
.end method

.method onRttInitiationFailure(I)V
    .locals 6

    goto/32 :goto_e

    nop

    :goto_0
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getCallback()Landroid/telecom/RemoteConnection$Callback;

    move-result-object v3

    goto/32 :goto_5

    nop

    :goto_1
    check-cast v1, Landroid/telecom/RemoteConnection$CallbackRecord;

    goto/32 :goto_7

    nop

    :goto_2
    if-nez v1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_8

    nop

    :goto_3
    goto :goto_c

    :goto_4
    goto/32 :goto_d

    nop

    :goto_5
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v4

    goto/32 :goto_f

    nop

    :goto_6
    invoke-direct {v5, v3, v2, p1}, Landroid/telecom/RemoteConnection$$ExternalSyntheticLambda3;-><init>(Landroid/telecom/RemoteConnection$Callback;Landroid/telecom/RemoteConnection;I)V

    goto/32 :goto_a

    nop

    :goto_7
    move-object v2, p0

    goto/32 :goto_0

    nop

    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_1

    nop

    :goto_9
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_2

    nop

    :goto_a
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_3

    nop

    :goto_b
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_c
    goto/32 :goto_9

    nop

    :goto_d
    return-void

    :goto_e
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_b

    nop

    :goto_f
    new-instance v5, Landroid/telecom/RemoteConnection$$ExternalSyntheticLambda3;

    goto/32 :goto_6

    nop
.end method

.method onRttInitiationSuccess()V
    .locals 6

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_8

    nop

    :goto_1
    goto :goto_9

    :goto_2
    goto/32 :goto_c

    nop

    :goto_3
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_1

    nop

    :goto_4
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v4

    goto/32 :goto_d

    nop

    :goto_5
    check-cast v1, Landroid/telecom/RemoteConnection$CallbackRecord;

    goto/32 :goto_e

    nop

    :goto_6
    if-nez v1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_a

    nop

    :goto_7
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getCallback()Landroid/telecom/RemoteConnection$Callback;

    move-result-object v3

    goto/32 :goto_4

    nop

    :goto_8
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_9
    goto/32 :goto_f

    nop

    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_5

    nop

    :goto_b
    invoke-direct {v5, v3, v2}, Landroid/telecom/RemoteConnection$$ExternalSyntheticLambda1;-><init>(Landroid/telecom/RemoteConnection$Callback;Landroid/telecom/RemoteConnection;)V

    goto/32 :goto_3

    nop

    :goto_c
    return-void

    :goto_d
    new-instance v5, Landroid/telecom/RemoteConnection$$ExternalSyntheticLambda1;

    goto/32 :goto_b

    nop

    :goto_e
    move-object v2, p0

    goto/32 :goto_7

    nop

    :goto_f
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_6

    nop
.end method

.method onRttSessionRemotelyTerminated()V
    .locals 6

    goto/32 :goto_d

    nop

    :goto_0
    move-object v2, p0

    goto/32 :goto_c

    nop

    :goto_1
    new-instance v5, Landroid/telecom/RemoteConnection$$ExternalSyntheticLambda0;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-direct {v5, v3, v2}, Landroid/telecom/RemoteConnection$$ExternalSyntheticLambda0;-><init>(Landroid/telecom/RemoteConnection$Callback;Landroid/telecom/RemoteConnection;)V

    goto/32 :goto_b

    nop

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_4
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_5
    goto/32 :goto_e

    nop

    :goto_6
    return-void

    :goto_7
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v4

    goto/32 :goto_1

    nop

    :goto_8
    check-cast v1, Landroid/telecom/RemoteConnection$CallbackRecord;

    goto/32 :goto_0

    nop

    :goto_9
    goto :goto_5

    :goto_a
    goto/32 :goto_6

    nop

    :goto_b
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_9

    nop

    :goto_c
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getCallback()Landroid/telecom/RemoteConnection$Callback;

    move-result-object v3

    goto/32 :goto_7

    nop

    :goto_d
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_4

    nop

    :goto_e
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_f

    nop

    :goto_f
    if-nez v1, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_3

    nop
.end method

.method public playDtmfTone(C)V
    .locals 3

    invoke-direct {p0}, Landroid/telecom/RemoteConnection;->getActiveOwnerInfo()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RC.pDT"

    invoke-static {v1, v0}, Landroid/telecom/Log;->startSession(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-boolean v0, p0, Landroid/telecom/RemoteConnection;->mConnected:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iget-object v1, p0, Landroid/telecom/RemoteConnection;->mConnectionId:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, p1, v2}, Lcom/android/internal/telecom/IConnectionService;->playDtmfTone(Ljava/lang/String;CLandroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/telecom/Log;->endSession()V

    throw v0

    :catch_0
    move-exception v0

    :cond_0
    :goto_0
    invoke-static {}, Landroid/telecom/Log;->endSession()V

    nop

    return-void
.end method

.method public postDialContinue(Z)V
    .locals 3

    invoke-direct {p0}, Landroid/telecom/RemoteConnection;->getActiveOwnerInfo()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RC.pDC"

    invoke-static {v1, v0}, Landroid/telecom/Log;->startSession(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-boolean v0, p0, Landroid/telecom/RemoteConnection;->mConnected:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iget-object v1, p0, Landroid/telecom/RemoteConnection;->mConnectionId:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, p1, v2}, Lcom/android/internal/telecom/IConnectionService;->onPostDialContinue(Ljava/lang/String;ZLandroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/telecom/Log;->endSession()V

    throw v0

    :catch_0
    move-exception v0

    :cond_0
    :goto_0
    invoke-static {}, Landroid/telecom/Log;->endSession()V

    nop

    return-void
.end method

.method public pullExternalCall()V
    .locals 3

    invoke-direct {p0}, Landroid/telecom/RemoteConnection;->getActiveOwnerInfo()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RC.pEC"

    invoke-static {v1, v0}, Landroid/telecom/Log;->startSession(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-boolean v0, p0, Landroid/telecom/RemoteConnection;->mConnected:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iget-object v1, p0, Landroid/telecom/RemoteConnection;->mConnectionId:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/android/internal/telecom/IConnectionService;->pullExternalCall(Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/telecom/Log;->endSession()V

    throw v0

    :catch_0
    move-exception v0

    :cond_0
    :goto_0
    invoke-static {}, Landroid/telecom/Log;->endSession()V

    nop

    return-void
.end method

.method putExtras(Landroid/os/Bundle;)V
    .locals 3

    goto/32 :goto_a

    nop

    :goto_0
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mExtras:Landroid/os/Bundle;

    goto/32 :goto_14

    nop

    :goto_1
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_7

    nop

    :goto_2
    return-void

    :goto_3
    goto/32 :goto_0

    nop

    :goto_4
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_9

    nop

    :goto_5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_4

    nop

    :goto_6
    new-array v2, v2, [Ljava/lang/Object;

    goto/32 :goto_10

    nop

    :goto_7
    const-string/jumbo v2, "putExtras: could not unmarshal extras; exception = "

    goto/32 :goto_5

    nop

    :goto_8
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_1

    nop

    :goto_9
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_15

    nop

    :goto_a
    if-eqz p1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_2

    nop

    :goto_b
    return-void

    :goto_c
    iput-object v0, p0, Landroid/telecom/RemoteConnection;->mExtras:Landroid/os/Bundle;

    :goto_d
    :try_start_0
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mExtras:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/BadParcelableException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_f

    nop

    :goto_e
    new-instance v0, Landroid/os/Bundle;

    goto/32 :goto_12

    nop

    :goto_f
    goto :goto_11

    :catch_0
    move-exception v0

    goto/32 :goto_8

    nop

    :goto_10
    invoke-static {p0, v1, v2}, Landroid/telecom/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_11
    goto/32 :goto_13

    nop

    :goto_12
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto/32 :goto_c

    nop

    :goto_13
    invoke-direct {p0}, Landroid/telecom/RemoteConnection;->notifyExtrasChanged()V

    goto/32 :goto_b

    nop

    :goto_14
    if-eqz v0, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_e

    nop

    :goto_15
    const/4 v2, 0x0

    goto/32 :goto_6

    nop
.end method

.method public registerCallback(Landroid/telecom/RemoteConnection$Callback;)V
    .locals 1

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-virtual {p0, p1, v0}, Landroid/telecom/RemoteConnection;->registerCallback(Landroid/telecom/RemoteConnection$Callback;Landroid/os/Handler;)V

    return-void
.end method

.method public registerCallback(Landroid/telecom/RemoteConnection$Callback;Landroid/os/Handler;)V
    .locals 2

    invoke-virtual {p0, p1}, Landroid/telecom/RemoteConnection;->unregisterCallback(Landroid/telecom/RemoteConnection$Callback;)V

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    new-instance v1, Landroid/telecom/RemoteConnection$CallbackRecord;

    invoke-direct {v1, p1, p2}, Landroid/telecom/RemoteConnection$CallbackRecord;-><init>(Landroid/telecom/RemoteConnection$Callback;Landroid/os/Handler;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public reject()V
    .locals 3

    invoke-direct {p0}, Landroid/telecom/RemoteConnection;->getActiveOwnerInfo()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RC.r"

    invoke-static {v1, v0}, Landroid/telecom/Log;->startSession(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-boolean v0, p0, Landroid/telecom/RemoteConnection;->mConnected:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iget-object v1, p0, Landroid/telecom/RemoteConnection;->mConnectionId:Ljava/lang/String;

    iget-object v2, p0, Landroid/telecom/RemoteConnection;->mCallingPackageAbbreviation:Ljava/lang/String;

    invoke-static {v2}, Landroid/telecom/Log;->getExternalSession(Ljava/lang/String;)Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/android/internal/telecom/IConnectionService;->reject(Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/telecom/Log;->endSession()V

    throw v0

    :catch_0
    move-exception v0

    :cond_0
    :goto_0
    invoke-static {}, Landroid/telecom/Log;->endSession()V

    nop

    return-void
.end method

.method removeExtras(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_10

    nop

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    goto/32 :goto_9

    nop

    :goto_1
    goto :goto_13

    :goto_2
    goto/32 :goto_b

    nop

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_7

    nop

    :goto_4
    if-nez v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop

    :goto_5
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    goto/32 :goto_4

    nop

    :goto_6
    return-void

    :goto_7
    if-nez v1, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_d

    nop

    :goto_8
    if-nez v0, :cond_2

    goto/32 :goto_13

    :cond_2
    goto/32 :goto_11

    nop

    :goto_9
    goto :goto_c

    :goto_a
    goto/32 :goto_e

    nop

    :goto_b
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_c
    goto/32 :goto_3

    nop

    :goto_d
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_14

    nop

    :goto_e
    invoke-direct {p0}, Landroid/telecom/RemoteConnection;->notifyExtrasChanged()V

    goto/32 :goto_12

    nop

    :goto_f
    iget-object v2, p0, Landroid/telecom/RemoteConnection;->mExtras:Landroid/os/Bundle;

    goto/32 :goto_0

    nop

    :goto_10
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mExtras:Landroid/os/Bundle;

    goto/32 :goto_8

    nop

    :goto_11
    if-nez p1, :cond_3

    goto/32 :goto_13

    :cond_3
    goto/32 :goto_5

    nop

    :goto_12
    return-void

    :goto_13
    goto/32 :goto_6

    nop

    :goto_14
    check-cast v1, Ljava/lang/String;

    goto/32 :goto_f

    nop
.end method

.method public sendRttUpgradeResponse(Landroid/telecom/Connection$RttTextStream;)V
    .locals 5

    invoke-direct {p0}, Landroid/telecom/RemoteConnection;->getActiveOwnerInfo()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RC.sRUR"

    invoke-static {v1, v0}, Landroid/telecom/Log;->startSession(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-boolean v0, p0, Landroid/telecom/RemoteConnection;->mConnected:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    iget-object v1, p0, Landroid/telecom/RemoteConnection;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iget-object v2, p0, Landroid/telecom/RemoteConnection;->mConnectionId:Ljava/lang/String;

    invoke-interface {v1, v2, v0, v0, v0}, Lcom/android/internal/telecom/IConnectionService;->respondToRttUpgradeRequest(Ljava/lang/String;Landroid/os/ParcelFileDescriptor;Landroid/os/ParcelFileDescriptor;Landroid/telecom/Logging/Session$Info;)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Landroid/telecom/RemoteConnection;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iget-object v2, p0, Landroid/telecom/RemoteConnection;->mConnectionId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/telecom/Connection$RttTextStream;->getFdFromInCall()Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    invoke-virtual {p1}, Landroid/telecom/Connection$RttTextStream;->getFdToInCall()Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/android/internal/telecom/IConnectionService;->respondToRttUpgradeRequest(Ljava/lang/String;Landroid/os/ParcelFileDescriptor;Landroid/os/ParcelFileDescriptor;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/telecom/Log;->endSession()V

    throw v0

    :catch_0
    move-exception v0

    :cond_1
    :goto_0
    invoke-static {}, Landroid/telecom/Log;->endSession()V

    nop

    return-void
.end method

.method setAddress(Landroid/net/Uri;I)V
    .locals 11

    goto/32 :goto_1

    nop

    :goto_0
    new-instance v10, Landroid/telecom/RemoteConnection$13;

    goto/32 :goto_a

    nop

    :goto_1
    iput-object p1, p0, Landroid/telecom/RemoteConnection;->mAddress:Landroid/net/Uri;

    goto/32 :goto_4

    nop

    :goto_2
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v9

    goto/32 :goto_0

    nop

    :goto_3
    move-object v6, p1

    goto/32 :goto_e

    nop

    :goto_4
    iput p2, p0, Landroid/telecom/RemoteConnection;->mAddressPresentation:I

    goto/32 :goto_c

    nop

    :goto_5
    move-object v4, v8

    goto/32 :goto_3

    nop

    :goto_6
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_7
    goto/32 :goto_10

    nop

    :goto_8
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getCallback()Landroid/telecom/RemoteConnection$Callback;

    move-result-object v8

    goto/32 :goto_2

    nop

    :goto_9
    move-object v5, p0

    goto/32 :goto_8

    nop

    :goto_a
    move-object v2, v10

    goto/32 :goto_15

    nop

    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_13

    nop

    :goto_c
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_6

    nop

    :goto_d
    invoke-direct/range {v2 .. v7}, Landroid/telecom/RemoteConnection$13;-><init>(Landroid/telecom/RemoteConnection;Landroid/telecom/RemoteConnection$Callback;Landroid/telecom/RemoteConnection;Landroid/net/Uri;I)V

    goto/32 :goto_14

    nop

    :goto_e
    move v7, p2

    goto/32 :goto_d

    nop

    :goto_f
    return-void

    :goto_10
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_16

    nop

    :goto_11
    goto :goto_7

    :goto_12
    goto/32 :goto_f

    nop

    :goto_13
    check-cast v1, Landroid/telecom/RemoteConnection$CallbackRecord;

    goto/32 :goto_9

    nop

    :goto_14
    invoke-virtual {v9, v10}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_11

    nop

    :goto_15
    move-object v3, p0

    goto/32 :goto_5

    nop

    :goto_16
    if-nez v1, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_b

    nop
.end method

.method public setAudioState(Landroid/telecom/AudioState;)V
    .locals 1
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance v0, Landroid/telecom/CallAudioState;

    invoke-direct {v0, p1}, Landroid/telecom/CallAudioState;-><init>(Landroid/telecom/AudioState;)V

    invoke-virtual {p0, v0}, Landroid/telecom/RemoteConnection;->setCallAudioState(Landroid/telecom/CallAudioState;)V

    return-void
.end method

.method public setCallAudioState(Landroid/telecom/CallAudioState;)V
    .locals 3

    invoke-direct {p0}, Landroid/telecom/RemoteConnection;->getActiveOwnerInfo()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RC.sCAS"

    invoke-static {v1, v0}, Landroid/telecom/Log;->startSession(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-boolean v0, p0, Landroid/telecom/RemoteConnection;->mConnected:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iget-object v1, p0, Landroid/telecom/RemoteConnection;->mConnectionId:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, p1, v2}, Lcom/android/internal/telecom/IConnectionService;->onCallAudioStateChanged(Ljava/lang/String;Landroid/telecom/CallAudioState;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/telecom/Log;->endSession()V

    throw v0

    :catch_0
    move-exception v0

    :cond_0
    :goto_0
    invoke-static {}, Landroid/telecom/Log;->endSession()V

    nop

    return-void
.end method

.method setCallerDisplayName(Ljava/lang/String;I)V
    .locals 11

    goto/32 :goto_15

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    goto/32 :goto_7

    nop

    :goto_2
    move-object v5, p0

    goto/32 :goto_10

    nop

    :goto_3
    return-void

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_b

    nop

    :goto_5
    move-object v6, p1

    goto/32 :goto_e

    nop

    :goto_6
    invoke-direct/range {v2 .. v7}, Landroid/telecom/RemoteConnection$14;-><init>(Landroid/telecom/RemoteConnection;Landroid/telecom/RemoteConnection$Callback;Landroid/telecom/RemoteConnection;Ljava/lang/String;I)V

    goto/32 :goto_a

    nop

    :goto_7
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_f

    nop

    :goto_8
    move-object v4, v8

    goto/32 :goto_5

    nop

    :goto_9
    new-instance v10, Landroid/telecom/RemoteConnection$14;

    goto/32 :goto_12

    nop

    :goto_a
    invoke-virtual {v9, v10}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_c

    nop

    :goto_b
    check-cast v1, Landroid/telecom/RemoteConnection$CallbackRecord;

    goto/32 :goto_2

    nop

    :goto_c
    goto :goto_1

    :goto_d
    goto/32 :goto_3

    nop

    :goto_e
    move v7, p2

    goto/32 :goto_6

    nop

    :goto_f
    if-nez v1, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_4

    nop

    :goto_10
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getCallback()Landroid/telecom/RemoteConnection$Callback;

    move-result-object v8

    goto/32 :goto_11

    nop

    :goto_11
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v9

    goto/32 :goto_9

    nop

    :goto_12
    move-object v2, v10

    goto/32 :goto_14

    nop

    :goto_13
    iput p2, p0, Landroid/telecom/RemoteConnection;->mCallerDisplayNamePresentation:I

    goto/32 :goto_16

    nop

    :goto_14
    move-object v3, p0

    goto/32 :goto_8

    nop

    :goto_15
    iput-object p1, p0, Landroid/telecom/RemoteConnection;->mCallerDisplayName:Ljava/lang/String;

    goto/32 :goto_13

    nop

    :goto_16
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_0

    nop
.end method

.method setConference(Landroid/telecom/RemoteConference;)V
    .locals 6

    goto/32 :goto_3

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_c

    nop

    :goto_1
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v4

    goto/32 :goto_e

    nop

    :goto_2
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_10

    nop

    :goto_3
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mConference:Landroid/telecom/RemoteConference;

    goto/32 :goto_12

    nop

    :goto_4
    move-object v2, p0

    goto/32 :goto_5

    nop

    :goto_5
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getCallback()Landroid/telecom/RemoteConnection$Callback;

    move-result-object v3

    goto/32 :goto_1

    nop

    :goto_6
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_9

    nop

    :goto_7
    return-void

    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_b

    nop

    :goto_9
    goto :goto_11

    :goto_a
    goto/32 :goto_7

    nop

    :goto_b
    if-nez v1, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_0

    nop

    :goto_c
    check-cast v1, Landroid/telecom/RemoteConnection$CallbackRecord;

    goto/32 :goto_4

    nop

    :goto_d
    invoke-direct {v5, p0, v3, v2, p1}, Landroid/telecom/RemoteConnection$16;-><init>(Landroid/telecom/RemoteConnection;Landroid/telecom/RemoteConnection$Callback;Landroid/telecom/RemoteConnection;Landroid/telecom/RemoteConference;)V

    goto/32 :goto_6

    nop

    :goto_e
    new-instance v5, Landroid/telecom/RemoteConnection$16;

    goto/32 :goto_d

    nop

    :goto_f
    iput-object p1, p0, Landroid/telecom/RemoteConnection;->mConference:Landroid/telecom/RemoteConference;

    goto/32 :goto_2

    nop

    :goto_10
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_11
    goto/32 :goto_8

    nop

    :goto_12
    if-ne v0, p1, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_f

    nop
.end method

.method setConferenceableConnections(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/telecom/RemoteConnection;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_a

    nop

    :goto_0
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mConferenceableConnections:Ljava/util/List;

    goto/32 :goto_5

    nop

    :goto_1
    move-object v2, p0

    goto/32 :goto_6

    nop

    :goto_2
    return-void

    :goto_3
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    goto/32 :goto_7

    nop

    :goto_5
    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/32 :goto_f

    nop

    :goto_6
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getCallback()Landroid/telecom/RemoteConnection$Callback;

    move-result-object v3

    goto/32 :goto_11

    nop

    :goto_7
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_e

    nop

    :goto_8
    invoke-direct {v5, p0, v3, v2}, Landroid/telecom/RemoteConnection$15;-><init>(Landroid/telecom/RemoteConnection;Landroid/telecom/RemoteConnection$Callback;Landroid/telecom/RemoteConnection;)V

    goto/32 :goto_9

    nop

    :goto_9
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_b

    nop

    :goto_a
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mConferenceableConnections:Ljava/util/List;

    goto/32 :goto_10

    nop

    :goto_b
    goto :goto_4

    :goto_c
    goto/32 :goto_2

    nop

    :goto_d
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_12

    nop

    :goto_e
    if-nez v1, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_d

    nop

    :goto_f
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_3

    nop

    :goto_10
    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto/32 :goto_0

    nop

    :goto_11
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v4

    goto/32 :goto_13

    nop

    :goto_12
    check-cast v1, Landroid/telecom/RemoteConnection$CallbackRecord;

    goto/32 :goto_1

    nop

    :goto_13
    new-instance v5, Landroid/telecom/RemoteConnection$15;

    goto/32 :goto_8

    nop
.end method

.method setConnectionCapabilities(I)V
    .locals 6

    goto/32 :goto_c

    nop

    :goto_0
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    goto/32 :goto_7

    nop

    :goto_3
    goto :goto_2

    :goto_4
    goto/32 :goto_f

    nop

    :goto_5
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_3

    nop

    :goto_6
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getCallback()Landroid/telecom/RemoteConnection$Callback;

    move-result-object v3

    goto/32 :goto_8

    nop

    :goto_7
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_b

    nop

    :goto_8
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v4

    goto/32 :goto_9

    nop

    :goto_9
    new-instance v5, Landroid/telecom/RemoteConnection$4;

    goto/32 :goto_d

    nop

    :goto_a
    check-cast v1, Landroid/telecom/RemoteConnection$CallbackRecord;

    goto/32 :goto_e

    nop

    :goto_b
    if-nez v1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_10

    nop

    :goto_c
    iput p1, p0, Landroid/telecom/RemoteConnection;->mConnectionCapabilities:I

    goto/32 :goto_0

    nop

    :goto_d
    invoke-direct {v5, p0, v3, v2, p1}, Landroid/telecom/RemoteConnection$4;-><init>(Landroid/telecom/RemoteConnection;Landroid/telecom/RemoteConnection$Callback;Landroid/telecom/RemoteConnection;I)V

    goto/32 :goto_5

    nop

    :goto_e
    move-object v2, p0

    goto/32 :goto_6

    nop

    :goto_f
    return-void

    :goto_10
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_a

    nop
.end method

.method setConnectionProperties(I)V
    .locals 6

    goto/32 :goto_0

    nop

    :goto_0
    iput p1, p0, Landroid/telecom/RemoteConnection;->mConnectionProperties:I

    goto/32 :goto_5

    nop

    :goto_1
    if-nez v1, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_2

    nop

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_7

    nop

    :goto_3
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_a

    nop

    :goto_4
    new-instance v5, Landroid/telecom/RemoteConnection$5;

    goto/32 :goto_6

    nop

    :goto_5
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_e

    nop

    :goto_6
    invoke-direct {v5, p0, v3, v2, p1}, Landroid/telecom/RemoteConnection$5;-><init>(Landroid/telecom/RemoteConnection;Landroid/telecom/RemoteConnection$Callback;Landroid/telecom/RemoteConnection;I)V

    goto/32 :goto_3

    nop

    :goto_7
    check-cast v1, Landroid/telecom/RemoteConnection$CallbackRecord;

    goto/32 :goto_d

    nop

    :goto_8
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v4

    goto/32 :goto_4

    nop

    :goto_9
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getCallback()Landroid/telecom/RemoteConnection$Callback;

    move-result-object v3

    goto/32 :goto_8

    nop

    :goto_a
    goto :goto_f

    :goto_b
    goto/32 :goto_10

    nop

    :goto_c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_1

    nop

    :goto_d
    move-object v2, p0

    goto/32 :goto_9

    nop

    :goto_e
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_f
    goto/32 :goto_c

    nop

    :goto_10
    return-void
.end method

.method setDestroyed()V
    .locals 6

    goto/32 :goto_17

    nop

    :goto_0
    new-instance v5, Landroid/telecom/RemoteConnection$6;

    goto/32 :goto_a

    nop

    :goto_1
    invoke-interface {v0}, Ljava/util/Set;->clear()V

    goto/32 :goto_19

    nop

    :goto_2
    if-eqz v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_11

    nop

    :goto_3
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    goto/32 :goto_2

    nop

    :goto_4
    invoke-direct {v0, v1, v2}, Landroid/telecom/DisconnectCause;-><init>(ILjava/lang/String;)V

    goto/32 :goto_e

    nop

    :goto_5
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getCallback()Landroid/telecom/RemoteConnection$Callback;

    move-result-object v3

    goto/32 :goto_8

    nop

    :goto_6
    iput-boolean v0, p0, Landroid/telecom/RemoteConnection;->mConnected:Z

    :goto_7
    goto/32 :goto_1b

    nop

    :goto_8
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v4

    goto/32 :goto_0

    nop

    :goto_9
    if-ne v0, v1, :cond_1

    goto/32 :goto_f

    :cond_1
    goto/32 :goto_1d

    nop

    :goto_a
    invoke-direct {v5, p0, v3, v2}, Landroid/telecom/RemoteConnection$6;-><init>(Landroid/telecom/RemoteConnection;Landroid/telecom/RemoteConnection$Callback;Landroid/telecom/RemoteConnection;)V

    goto/32 :goto_b

    nop

    :goto_b
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_c

    nop

    :goto_c
    goto :goto_15

    :goto_d
    goto/32 :goto_10

    nop

    :goto_e
    invoke-virtual {p0, v0}, Landroid/telecom/RemoteConnection;->setDisconnected(Landroid/telecom/DisconnectCause;)V

    :goto_f
    goto/32 :goto_18

    nop

    :goto_10
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_1

    nop

    :goto_11
    iget v0, p0, Landroid/telecom/RemoteConnection;->mState:I

    goto/32 :goto_13

    nop

    :goto_12
    check-cast v1, Landroid/telecom/RemoteConnection$CallbackRecord;

    goto/32 :goto_16

    nop

    :goto_13
    const/4 v1, 0x6

    goto/32 :goto_9

    nop

    :goto_14
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_15
    goto/32 :goto_1a

    nop

    :goto_16
    move-object v2, p0

    goto/32 :goto_5

    nop

    :goto_17
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_3

    nop

    :goto_18
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_14

    nop

    :goto_19
    const/4 v0, 0x0

    goto/32 :goto_6

    nop

    :goto_1a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_1f

    nop

    :goto_1b
    return-void

    :goto_1c
    const-string v2, "Connection destroyed."

    goto/32 :goto_4

    nop

    :goto_1d
    new-instance v0, Landroid/telecom/DisconnectCause;

    goto/32 :goto_1e

    nop

    :goto_1e
    const/4 v1, 0x1

    goto/32 :goto_1c

    nop

    :goto_1f
    if-nez v1, :cond_2

    goto/32 :goto_d

    :cond_2
    goto/32 :goto_20

    nop

    :goto_20
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_12

    nop
.end method

.method setDisconnected(Landroid/telecom/DisconnectCause;)V
    .locals 6

    goto/32 :goto_13

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    goto/32 :goto_8

    nop

    :goto_2
    if-nez v1, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_10

    nop

    :goto_3
    return-void

    :goto_4
    goto :goto_1

    :goto_5
    goto/32 :goto_3

    nop

    :goto_6
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_4

    nop

    :goto_7
    iput-object p1, p0, Landroid/telecom/RemoteConnection;->mDisconnectCause:Landroid/telecom/DisconnectCause;

    goto/32 :goto_f

    nop

    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_2

    nop

    :goto_9
    move-object v2, p0

    goto/32 :goto_12

    nop

    :goto_a
    if-ne v0, v1, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_b

    nop

    :goto_b
    iput v1, p0, Landroid/telecom/RemoteConnection;->mState:I

    goto/32 :goto_7

    nop

    :goto_c
    check-cast v1, Landroid/telecom/RemoteConnection$CallbackRecord;

    goto/32 :goto_9

    nop

    :goto_d
    new-instance v5, Landroid/telecom/RemoteConnection$2;

    goto/32 :goto_14

    nop

    :goto_e
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v4

    goto/32 :goto_d

    nop

    :goto_f
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_0

    nop

    :goto_10
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_c

    nop

    :goto_11
    const/4 v1, 0x6

    goto/32 :goto_a

    nop

    :goto_12
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getCallback()Landroid/telecom/RemoteConnection$Callback;

    move-result-object v3

    goto/32 :goto_e

    nop

    :goto_13
    iget v0, p0, Landroid/telecom/RemoteConnection;->mState:I

    goto/32 :goto_11

    nop

    :goto_14
    invoke-direct {v5, p0, v3, v2, p1}, Landroid/telecom/RemoteConnection$2;-><init>(Landroid/telecom/RemoteConnection;Landroid/telecom/RemoteConnection$Callback;Landroid/telecom/RemoteConnection;Landroid/telecom/DisconnectCause;)V

    goto/32 :goto_6

    nop
.end method

.method setIsVoipAudioMode(Z)V
    .locals 6

    goto/32 :goto_0

    nop

    :goto_0
    iput-boolean p1, p0, Landroid/telecom/RemoteConnection;->mIsVoipAudioMode:Z

    goto/32 :goto_e

    nop

    :goto_1
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getCallback()Landroid/telecom/RemoteConnection$Callback;

    move-result-object v3

    goto/32 :goto_d

    nop

    :goto_2
    check-cast v1, Landroid/telecom/RemoteConnection$CallbackRecord;

    goto/32 :goto_8

    nop

    :goto_3
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    goto/32 :goto_10

    nop

    :goto_5
    return-void

    :goto_6
    if-nez v1, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_f

    nop

    :goto_7
    invoke-direct {v5, p0, v3, v2, p1}, Landroid/telecom/RemoteConnection$11;-><init>(Landroid/telecom/RemoteConnection;Landroid/telecom/RemoteConnection$Callback;Landroid/telecom/RemoteConnection;Z)V

    goto/32 :goto_b

    nop

    :goto_8
    move-object v2, p0

    goto/32 :goto_1

    nop

    :goto_9
    goto :goto_4

    :goto_a
    goto/32 :goto_5

    nop

    :goto_b
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_9

    nop

    :goto_c
    new-instance v5, Landroid/telecom/RemoteConnection$11;

    goto/32 :goto_7

    nop

    :goto_d
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v4

    goto/32 :goto_c

    nop

    :goto_e
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_3

    nop

    :goto_f
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_2

    nop

    :goto_10
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_6

    nop
.end method

.method setPostDialWait(Ljava/lang/String;)V
    .locals 6

    goto/32 :goto_5

    nop

    :goto_0
    check-cast v1, Landroid/telecom/RemoteConnection$CallbackRecord;

    goto/32 :goto_7

    nop

    :goto_1
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    goto/32 :goto_3

    nop

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_6

    nop

    :goto_4
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v4

    goto/32 :goto_9

    nop

    :goto_5
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_1

    nop

    :goto_6
    if-nez v1, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_8

    nop

    :goto_7
    move-object v2, p0

    goto/32 :goto_d

    nop

    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_9
    new-instance v5, Landroid/telecom/RemoteConnection$7;

    goto/32 :goto_e

    nop

    :goto_a
    goto :goto_2

    :goto_b
    goto/32 :goto_c

    nop

    :goto_c
    return-void

    :goto_d
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getCallback()Landroid/telecom/RemoteConnection$Callback;

    move-result-object v3

    goto/32 :goto_4

    nop

    :goto_e
    invoke-direct {v5, p0, v3, v2, p1}, Landroid/telecom/RemoteConnection$7;-><init>(Landroid/telecom/RemoteConnection;Landroid/telecom/RemoteConnection$Callback;Landroid/telecom/RemoteConnection;Ljava/lang/String;)V

    goto/32 :goto_f

    nop

    :goto_f
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_a

    nop
.end method

.method setRingbackRequested(Z)V
    .locals 6

    goto/32 :goto_e

    nop

    :goto_0
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_3

    nop

    :goto_1
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_9

    nop

    :goto_2
    return-void

    :goto_3
    goto :goto_a

    :goto_4
    goto/32 :goto_2

    nop

    :goto_5
    new-instance v5, Landroid/telecom/RemoteConnection$3;

    goto/32 :goto_b

    nop

    :goto_6
    if-ne v0, p1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_12

    nop

    :goto_7
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_10

    nop

    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_11

    nop

    :goto_9
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_a
    goto/32 :goto_8

    nop

    :goto_b
    invoke-direct {v5, p0, v3, v2, p1}, Landroid/telecom/RemoteConnection$3;-><init>(Landroid/telecom/RemoteConnection;Landroid/telecom/RemoteConnection$Callback;Landroid/telecom/RemoteConnection;Z)V

    goto/32 :goto_0

    nop

    :goto_c
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v4

    goto/32 :goto_5

    nop

    :goto_d
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getCallback()Landroid/telecom/RemoteConnection$Callback;

    move-result-object v3

    goto/32 :goto_c

    nop

    :goto_e
    iget-boolean v0, p0, Landroid/telecom/RemoteConnection;->mRingbackRequested:Z

    goto/32 :goto_6

    nop

    :goto_f
    move-object v2, p0

    goto/32 :goto_d

    nop

    :goto_10
    check-cast v1, Landroid/telecom/RemoteConnection$CallbackRecord;

    goto/32 :goto_f

    nop

    :goto_11
    if-nez v1, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_7

    nop

    :goto_12
    iput-boolean p1, p0, Landroid/telecom/RemoteConnection;->mRingbackRequested:Z

    goto/32 :goto_1

    nop
.end method

.method setState(I)V
    .locals 6

    goto/32 :goto_6

    nop

    :goto_0
    check-cast v1, Landroid/telecom/RemoteConnection$CallbackRecord;

    goto/32 :goto_5

    nop

    :goto_1
    goto :goto_b

    :goto_2
    goto/32 :goto_d

    nop

    :goto_3
    new-instance v5, Landroid/telecom/RemoteConnection$1;

    goto/32 :goto_c

    nop

    :goto_4
    if-nez v1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_e

    nop

    :goto_5
    move-object v2, p0

    goto/32 :goto_12

    nop

    :goto_6
    iget v0, p0, Landroid/telecom/RemoteConnection;->mState:I

    goto/32 :goto_11

    nop

    :goto_7
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_a

    nop

    :goto_8
    iput p1, p0, Landroid/telecom/RemoteConnection;->mState:I

    goto/32 :goto_7

    nop

    :goto_9
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v4

    goto/32 :goto_3

    nop

    :goto_a
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_b
    goto/32 :goto_10

    nop

    :goto_c
    invoke-direct {v5, p0, v3, v2, p1}, Landroid/telecom/RemoteConnection$1;-><init>(Landroid/telecom/RemoteConnection;Landroid/telecom/RemoteConnection$Callback;Landroid/telecom/RemoteConnection;I)V

    goto/32 :goto_f

    nop

    :goto_d
    return-void

    :goto_e
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_f
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_1

    nop

    :goto_10
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_4

    nop

    :goto_11
    if-ne v0, p1, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_8

    nop

    :goto_12
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getCallback()Landroid/telecom/RemoteConnection$Callback;

    move-result-object v3

    goto/32 :goto_9

    nop
.end method

.method setStatusHints(Landroid/telecom/StatusHints;)V
    .locals 6

    goto/32 :goto_9

    nop

    :goto_0
    return-void

    :goto_1
    new-instance v5, Landroid/telecom/RemoteConnection$12;

    goto/32 :goto_f

    nop

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_e

    nop

    :goto_3
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    goto/32 :goto_5

    nop

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_c

    nop

    :goto_6
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getCallback()Landroid/telecom/RemoteConnection$Callback;

    move-result-object v3

    goto/32 :goto_8

    nop

    :goto_7
    move-object v2, p0

    goto/32 :goto_6

    nop

    :goto_8
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v4

    goto/32 :goto_1

    nop

    :goto_9
    iput-object p1, p0, Landroid/telecom/RemoteConnection;->mStatusHints:Landroid/telecom/StatusHints;

    goto/32 :goto_d

    nop

    :goto_a
    goto :goto_4

    :goto_b
    goto/32 :goto_0

    nop

    :goto_c
    if-nez v1, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_2

    nop

    :goto_d
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_3

    nop

    :goto_e
    check-cast v1, Landroid/telecom/RemoteConnection$CallbackRecord;

    goto/32 :goto_7

    nop

    :goto_f
    invoke-direct {v5, p0, v3, v2, p1}, Landroid/telecom/RemoteConnection$12;-><init>(Landroid/telecom/RemoteConnection;Landroid/telecom/RemoteConnection$Callback;Landroid/telecom/RemoteConnection;Landroid/telecom/StatusHints;)V

    goto/32 :goto_10

    nop

    :goto_10
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_a

    nop
.end method

.method setVideoProvider(Landroid/telecom/RemoteConnection$VideoProvider;)V
    .locals 6

    goto/32 :goto_d

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_7

    nop

    :goto_1
    new-instance v5, Landroid/telecom/RemoteConnection$10;

    goto/32 :goto_3

    nop

    :goto_2
    move-object v2, p0

    goto/32 :goto_b

    nop

    :goto_3
    invoke-direct {v5, p0, v3, v2, p1}, Landroid/telecom/RemoteConnection$10;-><init>(Landroid/telecom/RemoteConnection;Landroid/telecom/RemoteConnection$Callback;Landroid/telecom/RemoteConnection;Landroid/telecom/RemoteConnection$VideoProvider;)V

    goto/32 :goto_e

    nop

    :goto_4
    if-nez v1, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_0

    nop

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_4

    nop

    :goto_6
    return-void

    :goto_7
    check-cast v1, Landroid/telecom/RemoteConnection$CallbackRecord;

    goto/32 :goto_2

    nop

    :goto_8
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v4

    goto/32 :goto_1

    nop

    :goto_9
    goto :goto_10

    :goto_a
    goto/32 :goto_6

    nop

    :goto_b
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getCallback()Landroid/telecom/RemoteConnection$Callback;

    move-result-object v3

    goto/32 :goto_8

    nop

    :goto_c
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_f

    nop

    :goto_d
    iput-object p1, p0, Landroid/telecom/RemoteConnection;->mVideoProvider:Landroid/telecom/RemoteConnection$VideoProvider;

    goto/32 :goto_c

    nop

    :goto_e
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_9

    nop

    :goto_f
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_10
    goto/32 :goto_5

    nop
.end method

.method setVideoState(I)V
    .locals 6

    goto/32 :goto_a

    nop

    :goto_0
    new-instance v5, Landroid/telecom/RemoteConnection$9;

    goto/32 :goto_3

    nop

    :goto_1
    return-void

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_9

    nop

    :goto_3
    invoke-direct {v5, p0, v3, v2, p1}, Landroid/telecom/RemoteConnection$9;-><init>(Landroid/telecom/RemoteConnection;Landroid/telecom/RemoteConnection$Callback;Landroid/telecom/RemoteConnection;I)V

    goto/32 :goto_e

    nop

    :goto_4
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v4

    goto/32 :goto_0

    nop

    :goto_5
    check-cast v1, Landroid/telecom/RemoteConnection$CallbackRecord;

    goto/32 :goto_8

    nop

    :goto_6
    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getCallback()Landroid/telecom/RemoteConnection$Callback;

    move-result-object v3

    goto/32 :goto_4

    nop

    :goto_7
    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_f

    nop

    :goto_8
    move-object v2, p0

    goto/32 :goto_6

    nop

    :goto_9
    if-nez v1, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_b

    nop

    :goto_a
    iput p1, p0, Landroid/telecom/RemoteConnection;->mVideoState:I

    goto/32 :goto_7

    nop

    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_5

    nop

    :goto_c
    goto :goto_10

    :goto_d
    goto/32 :goto_1

    nop

    :goto_e
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_c

    nop

    :goto_f
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_10
    goto/32 :goto_2

    nop
.end method

.method public startRtt(Landroid/telecom/Connection$RttTextStream;)V
    .locals 5

    invoke-direct {p0}, Landroid/telecom/RemoteConnection;->getActiveOwnerInfo()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RC.sR"

    invoke-static {v1, v0}, Landroid/telecom/Log;->startSession(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-boolean v0, p0, Landroid/telecom/RemoteConnection;->mConnected:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iget-object v1, p0, Landroid/telecom/RemoteConnection;->mConnectionId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/telecom/Connection$RttTextStream;->getFdFromInCall()Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    invoke-virtual {p1}, Landroid/telecom/Connection$RttTextStream;->getFdToInCall()Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/android/internal/telecom/IConnectionService;->startRtt(Ljava/lang/String;Landroid/os/ParcelFileDescriptor;Landroid/os/ParcelFileDescriptor;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/telecom/Log;->endSession()V

    throw v0

    :catch_0
    move-exception v0

    :cond_0
    :goto_0
    invoke-static {}, Landroid/telecom/Log;->endSession()V

    nop

    return-void
.end method

.method public stopDtmfTone()V
    .locals 3

    invoke-direct {p0}, Landroid/telecom/RemoteConnection;->getActiveOwnerInfo()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RC.sDT"

    invoke-static {v1, v0}, Landroid/telecom/Log;->startSession(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-boolean v0, p0, Landroid/telecom/RemoteConnection;->mConnected:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iget-object v1, p0, Landroid/telecom/RemoteConnection;->mConnectionId:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/android/internal/telecom/IConnectionService;->stopDtmfTone(Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/telecom/Log;->endSession()V

    throw v0

    :catch_0
    move-exception v0

    :cond_0
    :goto_0
    invoke-static {}, Landroid/telecom/Log;->endSession()V

    nop

    return-void
.end method

.method public stopRtt()V
    .locals 3

    invoke-direct {p0}, Landroid/telecom/RemoteConnection;->getActiveOwnerInfo()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RC.stR"

    invoke-static {v1, v0}, Landroid/telecom/Log;->startSession(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-boolean v0, p0, Landroid/telecom/RemoteConnection;->mConnected:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iget-object v1, p0, Landroid/telecom/RemoteConnection;->mConnectionId:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/android/internal/telecom/IConnectionService;->stopRtt(Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/telecom/Log;->endSession()V

    throw v0

    :catch_0
    move-exception v0

    :cond_0
    :goto_0
    invoke-static {}, Landroid/telecom/Log;->endSession()V

    nop

    return-void
.end method

.method public unhold()V
    .locals 3

    invoke-direct {p0}, Landroid/telecom/RemoteConnection;->getActiveOwnerInfo()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RC.u"

    invoke-static {v1, v0}, Landroid/telecom/Log;->startSession(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-boolean v0, p0, Landroid/telecom/RemoteConnection;->mConnected:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iget-object v1, p0, Landroid/telecom/RemoteConnection;->mConnectionId:Ljava/lang/String;

    iget-object v2, p0, Landroid/telecom/RemoteConnection;->mCallingPackageAbbreviation:Ljava/lang/String;

    invoke-static {v2}, Landroid/telecom/Log;->getExternalSession(Ljava/lang/String;)Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/android/internal/telecom/IConnectionService;->unhold(Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/telecom/Log;->endSession()V

    throw v0

    :catch_0
    move-exception v0

    :cond_0
    :goto_0
    invoke-static {}, Landroid/telecom/Log;->endSession()V

    nop

    return-void
.end method

.method public unregisterCallback(Landroid/telecom/RemoteConnection$Callback;)V
    .locals 3

    if-eqz p1, :cond_1

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/RemoteConnection$CallbackRecord;

    invoke-virtual {v1}, Landroid/telecom/RemoteConnection$CallbackRecord;->getCallback()Landroid/telecom/RemoteConnection$Callback;

    move-result-object v2

    if-ne v2, p1, :cond_0

    iget-object v0, p0, Landroid/telecom/RemoteConnection;->mCallbackRecords:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method
