.class final Landroid/telecom/ConnectionServiceAdapter;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field private final mAdapters:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/android/internal/telecom/IConnectionServiceAdapter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v1, 0x8

    const v2, 0x3f666666    # 0.9f

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method addAdapter(Lcom/android/internal/telecom/IConnectionServiceAdapter;)V
    .locals 5

    goto/32 :goto_18

    nop

    :goto_0
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    goto/32 :goto_8

    nop

    :goto_1
    invoke-interface {p1}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->asBinder()Landroid/os/IBinder;

    move-result-object v4

    goto/32 :goto_2

    nop

    :goto_2
    if-eq v3, v4, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_a

    nop

    :goto_3
    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_15

    nop

    :goto_4
    iget-object v1, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_f

    nop

    :goto_5
    goto :goto_10

    :catch_0
    move-exception v0

    goto/32 :goto_4

    nop

    :goto_6
    return-void

    :goto_7
    goto/32 :goto_13

    nop

    :goto_8
    invoke-interface {v1}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    goto/32 :goto_1

    nop

    :goto_9
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_3

    nop

    :goto_a
    new-array v0, v2, [Ljava/lang/Object;

    goto/32 :goto_16

    nop

    :goto_b
    if-nez v1, :cond_1

    goto/32 :goto_14

    :cond_1
    goto/32 :goto_19

    nop

    :goto_c
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_d
    goto/32 :goto_11

    nop

    :goto_e
    const/4 v2, 0x0

    goto/32 :goto_b

    nop

    :goto_f
    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :goto_10
    goto/32 :goto_12

    nop

    :goto_11
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_e

    nop

    :goto_12
    return-void

    :goto_13
    goto :goto_d

    :goto_14
    goto/32 :goto_9

    nop

    :goto_15
    if-nez v0, :cond_2

    goto/32 :goto_10

    :cond_2
    :try_start_0
    invoke-interface {p1}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-interface {v0, p0, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_5

    nop

    :goto_16
    const-string v2, "Ignoring duplicate adapter addition."

    goto/32 :goto_17

    nop

    :goto_17
    invoke-static {p0, v2, v0}, Landroid/telecom/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_6

    nop

    :goto_18
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_c

    nop

    :goto_19
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_0

    nop
.end method

.method addConferenceCall(Ljava/lang/String;Landroid/telecom/ParcelableConference;)V
    .locals 3

    goto/32 :goto_2

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_1

    nop

    :goto_1
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, p2, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->addConferenceCall(Ljava/lang/String;Landroid/telecom/ParcelableConference;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_6

    nop

    :goto_2
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_3

    nop

    :goto_3
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    goto/32 :goto_b

    nop

    :goto_5
    return-void

    :goto_6
    goto :goto_7

    :catch_0
    move-exception v2

    :goto_7
    goto/32 :goto_8

    nop

    :goto_8
    goto :goto_4

    :goto_9
    goto/32 :goto_5

    nop

    :goto_a
    if-nez v1, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_0

    nop

    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_a

    nop
.end method

.method addExistingConnection(Ljava/lang/String;Landroid/telecom/ParcelableConnection;)V
    .locals 3

    goto/32 :goto_f

    nop

    :goto_0
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_4

    nop

    :goto_1
    invoke-static {p0, v1, v0}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_2

    nop

    :goto_2
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_5

    nop

    :goto_3
    const-string v1, "addExistingConnection: %s"

    goto/32 :goto_1

    nop

    :goto_4
    const/4 v1, 0x0

    goto/32 :goto_e

    nop

    :goto_5
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    goto/32 :goto_11

    nop

    :goto_7
    if-nez v1, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_10

    nop

    :goto_8
    goto :goto_6

    :goto_9
    goto/32 :goto_d

    nop

    :goto_a
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, p2, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->addExistingConnection(Ljava/lang/String;Landroid/telecom/ParcelableConnection;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_b

    nop

    :goto_b
    goto :goto_c

    :catch_0
    move-exception v2

    :goto_c
    goto/32 :goto_8

    nop

    :goto_d
    return-void

    :goto_e
    aput-object p1, v0, v1

    goto/32 :goto_3

    nop

    :goto_f
    const/4 v0, 0x1

    goto/32 :goto_0

    nop

    :goto_10
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_a

    nop

    :goto_11
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_7

    nop
.end method

.method public binderDied()V
    .locals 4

    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    invoke-interface {v1}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-interface {v2}, Landroid/os/IBinder;->isBinderAlive()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    invoke-interface {v1}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, p0, v3}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    :cond_0
    goto :goto_0

    :cond_1
    return-void
.end method

.method handleCreateConferenceComplete(Ljava/lang/String;Landroid/telecom/ConnectionRequest;Landroid/telecom/ParcelableConference;)V
    .locals 3

    goto/32 :goto_9

    nop

    :goto_0
    if-nez v1, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_3

    nop

    :goto_1
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    goto/32 :goto_6

    nop

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_4
    goto :goto_5

    :catch_0
    move-exception v2

    :goto_5
    goto/32 :goto_a

    nop

    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_0

    nop

    :goto_7
    return-void

    :goto_8
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    nop

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, p2, p3, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->handleCreateConferenceComplete(Ljava/lang/String;Landroid/telecom/ConnectionRequest;Landroid/telecom/ParcelableConference;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_4

    nop

    :goto_9
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_1

    nop

    :goto_a
    goto :goto_2

    :goto_b
    goto/32 :goto_7

    nop
.end method

.method handleCreateConnectionComplete(Ljava/lang/String;Landroid/telecom/ConnectionRequest;Landroid/telecom/ParcelableConnection;)V
    .locals 3

    goto/32 :goto_2

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    goto/32 :goto_5

    nop

    :goto_2
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_0

    nop

    :goto_3
    return-void

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_b

    nop

    :goto_6
    goto :goto_7

    :catch_0
    move-exception v2

    :goto_7
    goto/32 :goto_9

    nop

    :goto_8
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    nop

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, p2, p3, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->handleCreateConnectionComplete(Ljava/lang/String;Landroid/telecom/ConnectionRequest;Landroid/telecom/ParcelableConnection;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_6

    nop

    :goto_9
    goto :goto_1

    :goto_a
    goto/32 :goto_3

    nop

    :goto_b
    if-nez v1, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_4

    nop
.end method

.method onConferenceMergeFailed(Ljava/lang/String;)V
    .locals 5

    goto/32 :goto_9

    nop

    :goto_0
    return-void

    :goto_1
    if-nez v1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_a

    nop

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_1

    nop

    :goto_3
    goto :goto_8

    :goto_4
    goto/32 :goto_0

    nop

    :goto_5
    goto :goto_6

    :catch_0
    move-exception v2

    :goto_6
    goto/32 :goto_3

    nop

    :goto_7
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_8
    goto/32 :goto_2

    nop

    :goto_9
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_7

    nop

    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_b

    nop

    :goto_b
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    const-string/jumbo v2, "merge failed for call %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {p0, v2, v3}, Landroid/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->setConferenceMergeFailed(Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_5

    nop
.end method

.method onConnectionEvent(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3

    goto/32 :goto_e

    nop

    :goto_0
    aput-object p2, v0, v1

    goto/32 :goto_1

    nop

    :goto_1
    const-string/jumbo v1, "onConnectionEvent: %s"

    goto/32 :goto_3

    nop

    :goto_2
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_d

    nop

    :goto_3
    invoke-static {p0, v1, v0}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_b

    nop

    :goto_4
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_5
    goto/32 :goto_11

    nop

    :goto_6
    return-void

    :goto_7
    if-nez v1, :cond_0

    goto/32 :goto_10

    :cond_0
    goto/32 :goto_a

    nop

    :goto_8
    goto :goto_9

    :catch_0
    move-exception v2

    :goto_9
    goto/32 :goto_f

    nop

    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_c

    nop

    :goto_b
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_4

    nop

    :goto_c
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, p2, p3, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->onConnectionEvent(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_8

    nop

    :goto_d
    const/4 v1, 0x0

    goto/32 :goto_0

    nop

    :goto_e
    const/4 v0, 0x1

    goto/32 :goto_2

    nop

    :goto_f
    goto :goto_5

    :goto_10
    goto/32 :goto_6

    nop

    :goto_11
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_7

    nop
.end method

.method onConnectionServiceFocusReleased()V
    .locals 4

    goto/32 :goto_b

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_1

    nop

    :goto_1
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    const-string/jumbo v2, "onConnectionServiceFocusReleased"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p0, v2, v3}, Landroid/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->onConnectionServiceFocusReleased(Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_8

    nop

    :goto_2
    if-nez v1, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_0

    nop

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_2

    nop

    :goto_4
    goto :goto_7

    :goto_5
    goto/32 :goto_a

    nop

    :goto_6
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_7
    goto/32 :goto_3

    nop

    :goto_8
    goto :goto_9

    :catch_0
    move-exception v2

    :goto_9
    goto/32 :goto_4

    nop

    :goto_a
    return-void

    :goto_b
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_6

    nop
.end method

.method onPhoneAccountChanged(Ljava/lang/String;Landroid/telecom/PhoneAccountHandle;)V
    .locals 5

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_a

    nop

    :goto_2
    goto :goto_3

    :catch_0
    move-exception v2

    :goto_3
    goto/32 :goto_5

    nop

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_5
    goto :goto_b

    :goto_6
    goto/32 :goto_0

    nop

    :goto_7
    if-nez v1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_4

    nop

    :goto_8
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    const-string/jumbo v2, "onPhoneAccountChanged %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {p0, v2, v3}, Landroid/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, p2, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->onPhoneAccountChanged(Ljava/lang/String;Landroid/telecom/PhoneAccountHandle;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_2

    nop

    :goto_9
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_7

    nop

    :goto_a
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_b
    goto/32 :goto_9

    nop
.end method

.method onPostDialChar(Ljava/lang/String;C)V
    .locals 3

    goto/32 :goto_a

    nop

    :goto_0
    goto :goto_1

    :catch_0
    move-exception v2

    :goto_1
    goto/32 :goto_3

    nop

    :goto_2
    if-nez v1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_9

    nop

    :goto_3
    goto :goto_7

    :goto_4
    goto/32 :goto_b

    nop

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_2

    nop

    :goto_6
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_7
    goto/32 :goto_5

    nop

    :goto_8
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, p2, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->onPostDialChar(Ljava/lang/String;CLandroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_0

    nop

    :goto_9
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_a
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_6

    nop

    :goto_b
    return-void
.end method

.method onPostDialWait(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    goto/32 :goto_b

    nop

    :goto_0
    if-nez v1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_a

    nop

    :goto_1
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, p2, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->onPostDialWait(Ljava/lang/String;Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_3

    nop

    :goto_2
    return-void

    :goto_3
    goto :goto_4

    :catch_0
    move-exception v2

    :goto_4
    goto/32 :goto_5

    nop

    :goto_5
    goto :goto_9

    :goto_6
    goto/32 :goto_2

    nop

    :goto_7
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_0

    nop

    :goto_8
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_9
    goto/32 :goto_7

    nop

    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_1

    nop

    :goto_b
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_8

    nop
.end method

.method onRemoteRttRequest(Ljava/lang/String;)V
    .locals 3

    goto/32 :goto_3

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    goto/32 :goto_2

    nop

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_a

    nop

    :goto_3
    const/4 v0, 0x1

    goto/32 :goto_10

    nop

    :goto_4
    const-string/jumbo v1, "onRemoteRttRequest: %s"

    goto/32 :goto_9

    nop

    :goto_5
    goto :goto_6

    :catch_0
    move-exception v2

    :goto_6
    goto/32 :goto_b

    nop

    :goto_7
    const/4 v1, 0x0

    goto/32 :goto_11

    nop

    :goto_8
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->onRemoteRttRequest(Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_5

    nop

    :goto_9
    invoke-static {p0, v1, v0}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_f

    nop

    :goto_a
    if-nez v1, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_e

    nop

    :goto_b
    goto :goto_1

    :goto_c
    goto/32 :goto_d

    nop

    :goto_d
    return-void

    :goto_e
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_f
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_0

    nop

    :goto_10
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_7

    nop

    :goto_11
    aput-object p1, v0, v1

    goto/32 :goto_4

    nop
.end method

.method onRttInitiationFailure(Ljava/lang/String;I)V
    .locals 3

    goto/32 :goto_4

    nop

    :goto_0
    if-nez v1, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_f

    nop

    :goto_1
    const-string/jumbo v1, "onRttInitiationFailure: %s"

    goto/32 :goto_3

    nop

    :goto_2
    return-void

    :goto_3
    invoke-static {p0, v1, v0}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_d

    nop

    :goto_4
    const/4 v0, 0x1

    goto/32 :goto_6

    nop

    :goto_5
    const/4 v1, 0x0

    goto/32 :goto_10

    nop

    :goto_6
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_5

    nop

    :goto_7
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_8
    goto/32 :goto_e

    nop

    :goto_9
    goto :goto_a

    :catch_0
    move-exception v2

    :goto_a
    goto/32 :goto_b

    nop

    :goto_b
    goto :goto_8

    :goto_c
    goto/32 :goto_2

    nop

    :goto_d
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_7

    nop

    :goto_e
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_0

    nop

    :goto_f
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_11

    nop

    :goto_10
    aput-object p1, v0, v1

    goto/32 :goto_1

    nop

    :goto_11
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, p2, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->onRttInitiationFailure(Ljava/lang/String;ILandroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_9

    nop
.end method

.method onRttInitiationSuccess(Ljava/lang/String;)V
    .locals 3

    goto/32 :goto_4

    nop

    :goto_0
    invoke-static {p0, v1, v0}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_a

    nop

    :goto_1
    goto :goto_e

    :goto_2
    goto/32 :goto_c

    nop

    :goto_3
    const-string/jumbo v1, "onRttInitiationSuccess: %s"

    goto/32 :goto_0

    nop

    :goto_4
    const/4 v0, 0x1

    goto/32 :goto_11

    nop

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_8

    nop

    :goto_6
    goto :goto_7

    :catch_0
    move-exception v2

    :goto_7
    goto/32 :goto_1

    nop

    :goto_8
    if-nez v1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_b

    nop

    :goto_9
    const/4 v1, 0x0

    goto/32 :goto_f

    nop

    :goto_a
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_d

    nop

    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_10

    nop

    :goto_c
    return-void

    :goto_d
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_e
    goto/32 :goto_5

    nop

    :goto_f
    aput-object p1, v0, v1

    goto/32 :goto_3

    nop

    :goto_10
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->onRttInitiationSuccess(Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_6

    nop

    :goto_11
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_9

    nop
.end method

.method onRttSessionRemotelyTerminated(Ljava/lang/String;)V
    .locals 3

    goto/32 :goto_f

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    goto/32 :goto_11

    nop

    :goto_2
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->onRttSessionRemotelyTerminated(Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_9

    nop

    :goto_3
    return-void

    :goto_4
    invoke-static {p0, v1, v0}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_5

    nop

    :goto_5
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_0

    nop

    :goto_6
    goto :goto_1

    :goto_7
    goto/32 :goto_3

    nop

    :goto_8
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_d

    nop

    :goto_9
    goto :goto_a

    :catch_0
    move-exception v2

    :goto_a
    goto/32 :goto_6

    nop

    :goto_b
    if-nez v1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_e

    nop

    :goto_c
    aput-object p1, v0, v1

    goto/32 :goto_10

    nop

    :goto_d
    const/4 v1, 0x0

    goto/32 :goto_c

    nop

    :goto_e
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_2

    nop

    :goto_f
    const/4 v0, 0x1

    goto/32 :goto_8

    nop

    :goto_10
    const-string/jumbo v1, "onRttSessionRemotelyTerminated: %s"

    goto/32 :goto_4

    nop

    :goto_11
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_b

    nop
.end method

.method putExtra(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    goto/32 :goto_2

    nop

    :goto_0
    aput-object p1, v0, v1

    goto/32 :goto_5

    nop

    :goto_1
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_a

    nop

    :goto_2
    const/4 v0, 0x3

    goto/32 :goto_4

    nop

    :goto_3
    const/4 v1, 0x0

    goto/32 :goto_0

    nop

    :goto_4
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_3

    nop

    :goto_5
    const/4 v1, 0x1

    goto/32 :goto_7

    nop

    :goto_6
    const/4 v2, 0x2

    goto/32 :goto_d

    nop

    :goto_7
    aput-object p2, v0, v1

    goto/32 :goto_12

    nop

    :goto_8
    if-nez v1, :cond_0

    goto/32 :goto_14

    :cond_0
    goto/32 :goto_e

    nop

    :goto_9
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_8

    nop

    :goto_a
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_b
    goto/32 :goto_9

    nop

    :goto_c
    const-string/jumbo v1, "putExtra: %s %s=%d"

    goto/32 :goto_11

    nop

    :goto_d
    aput-object v1, v0, v2

    goto/32 :goto_c

    nop

    :goto_e
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_f

    nop

    :goto_f
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v2, p2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v3

    invoke-interface {v1, p1, v2, v3}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->putExtras(Ljava/lang/String;Landroid/os/Bundle;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_15

    nop

    :goto_10
    return-void

    :goto_11
    invoke-static {p0, v1, v0}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_1

    nop

    :goto_12
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_6

    nop

    :goto_13
    goto :goto_b

    :goto_14
    goto/32 :goto_10

    nop

    :goto_15
    goto :goto_16

    :catch_0
    move-exception v2

    :goto_16
    goto/32 :goto_13

    nop
.end method

.method putExtra(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    goto/32 :goto_3

    nop

    :goto_0
    const/4 v1, 0x2

    goto/32 :goto_e

    nop

    :goto_1
    const-string/jumbo v1, "putExtra: %s %s=%s"

    goto/32 :goto_13

    nop

    :goto_2
    aput-object p2, v0, v1

    goto/32 :goto_0

    nop

    :goto_3
    const/4 v0, 0x3

    goto/32 :goto_a

    nop

    :goto_4
    const/4 v1, 0x0

    goto/32 :goto_f

    nop

    :goto_5
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    goto/32 :goto_11

    nop

    :goto_7
    return-void

    :goto_8
    goto :goto_9

    :catch_0
    move-exception v2

    :goto_9
    goto/32 :goto_14

    nop

    :goto_a
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_4

    nop

    :goto_b
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v2, p2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v3

    invoke-interface {v1, p1, v2, v3}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->putExtras(Ljava/lang/String;Landroid/os/Bundle;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_8

    nop

    :goto_c
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_5

    nop

    :goto_d
    if-nez v1, :cond_0

    goto/32 :goto_15

    :cond_0
    goto/32 :goto_12

    nop

    :goto_e
    aput-object p3, v0, v1

    goto/32 :goto_1

    nop

    :goto_f
    aput-object p1, v0, v1

    goto/32 :goto_10

    nop

    :goto_10
    const/4 v1, 0x1

    goto/32 :goto_2

    nop

    :goto_11
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_d

    nop

    :goto_12
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_b

    nop

    :goto_13
    invoke-static {p0, v1, v0}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_c

    nop

    :goto_14
    goto :goto_6

    :goto_15
    goto/32 :goto_7

    nop
.end method

.method putExtra(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4

    goto/32 :goto_14

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    goto/32 :goto_16

    nop

    :goto_2
    goto :goto_1

    :goto_3
    goto/32 :goto_11

    nop

    :goto_4
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_0

    nop

    :goto_5
    goto :goto_6

    :catch_0
    move-exception v2

    :goto_6
    goto/32 :goto_2

    nop

    :goto_7
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_d

    nop

    :goto_8
    const-string/jumbo v1, "putExtra: %s %s=%b"

    goto/32 :goto_13

    nop

    :goto_9
    const/4 v1, 0x1

    goto/32 :goto_12

    nop

    :goto_a
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v2, p2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v3

    invoke-interface {v1, p1, v2, v3}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->putExtras(Ljava/lang/String;Landroid/os/Bundle;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_5

    nop

    :goto_b
    const/4 v2, 0x2

    goto/32 :goto_c

    nop

    :goto_c
    aput-object v1, v0, v2

    goto/32 :goto_8

    nop

    :goto_d
    const/4 v1, 0x0

    goto/32 :goto_f

    nop

    :goto_e
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto/32 :goto_b

    nop

    :goto_f
    aput-object p1, v0, v1

    goto/32 :goto_9

    nop

    :goto_10
    if-nez v1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_15

    nop

    :goto_11
    return-void

    :goto_12
    aput-object p2, v0, v1

    goto/32 :goto_e

    nop

    :goto_13
    invoke-static {p0, v1, v0}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_4

    nop

    :goto_14
    const/4 v0, 0x3

    goto/32 :goto_7

    nop

    :goto_15
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_a

    nop

    :goto_16
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_10

    nop
.end method

.method putExtras(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3

    goto/32 :goto_4

    nop

    :goto_0
    const/4 v1, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    aput-object p1, v0, v1

    goto/32 :goto_5

    nop

    :goto_2
    return-void

    :goto_3
    invoke-static {p0, v1, v0}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_11

    nop

    :goto_4
    const/4 v0, 0x1

    goto/32 :goto_e

    nop

    :goto_5
    const-string/jumbo v1, "putExtras: %s"

    goto/32 :goto_3

    nop

    :goto_6
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_7
    goto/32 :goto_8

    nop

    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_a

    nop

    :goto_9
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, p2, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->putExtras(Ljava/lang/String;Landroid/os/Bundle;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_c

    nop

    :goto_a
    if-nez v1, :cond_0

    goto/32 :goto_10

    :cond_0
    goto/32 :goto_b

    nop

    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_9

    nop

    :goto_c
    goto :goto_d

    :catch_0
    move-exception v2

    :goto_d
    goto/32 :goto_f

    nop

    :goto_e
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_0

    nop

    :goto_f
    goto :goto_7

    :goto_10
    goto/32 :goto_2

    nop

    :goto_11
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_6

    nop
.end method

.method queryRemoteConnectionServices(Lcom/android/internal/telecom/RemoteServiceCallback;Ljava/lang/String;)V
    .locals 4

    goto/32 :goto_7

    nop

    :goto_0
    if-eq v0, v3, :cond_0

    goto/32 :goto_2

    :cond_0
    :try_start_0
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v3

    invoke-interface {v0, p1, p2, v3}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->queryRemoteConnectionServices(Lcom/android/internal/telecom/RemoteServiceCallback;Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    goto/32 :goto_b

    nop

    :goto_1
    goto :goto_10

    :goto_2
    :try_start_1
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    sget-object v3, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    invoke-interface {p1, v0, v3}, Lcom/android/internal/telecom/RemoteServiceCallback;->onResult(Ljava/util/List;Ljava/util/List;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto/32 :goto_3

    nop

    :goto_3
    goto :goto_10

    :catch_0
    move-exception v0

    goto/32 :goto_e

    nop

    :goto_4
    const/4 v1, 0x0

    goto/32 :goto_5

    nop

    :goto_5
    const-string v2, "Exception trying to query for remote CSs"

    goto/32 :goto_c

    nop

    :goto_6
    return-void

    :goto_7
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_a

    nop

    :goto_8
    invoke-static {p0, v0, v2, v1}, Landroid/telecom/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_9
    goto/32 :goto_1

    nop

    :goto_a
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    goto/32 :goto_4

    nop

    :goto_b
    goto :goto_9

    :catch_1
    move-exception v0

    goto/32 :goto_d

    nop

    :goto_c
    const/4 v3, 0x1

    goto/32 :goto_0

    nop

    :goto_d
    new-array v1, v1, [Ljava/lang/Object;

    goto/32 :goto_8

    nop

    :goto_e
    new-array v1, v1, [Ljava/lang/Object;

    goto/32 :goto_f

    nop

    :goto_f
    invoke-static {p0, v0, v2, v1}, Landroid/telecom/Log;->e(Ljava/lang/Object;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_10
    goto/32 :goto_6

    nop
.end method

.method removeAdapter(Lcom/android/internal/telecom/IConnectionServiceAdapter;)V
    .locals 4

    goto/32 :goto_14

    nop

    :goto_0
    invoke-interface {v0, p0, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    goto/32 :goto_12

    nop

    :goto_1
    invoke-interface {p1}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    goto/32 :goto_11

    nop

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_e

    nop

    :goto_3
    const/4 v2, 0x0

    goto/32 :goto_0

    nop

    :goto_4
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_5
    goto/32 :goto_f

    nop

    :goto_6
    invoke-interface {p1}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_7
    iget-object v2, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_10

    nop

    :goto_8
    invoke-interface {v1}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    goto/32 :goto_1

    nop

    :goto_9
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_4

    nop

    :goto_a
    if-nez v1, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_2

    nop

    :goto_b
    return-void

    :goto_c
    goto :goto_5

    :goto_d
    goto/32 :goto_b

    nop

    :goto_e
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    goto/32 :goto_8

    nop

    :goto_f
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_a

    nop

    :goto_10
    invoke-interface {v2, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v2

    goto/32 :goto_15

    nop

    :goto_11
    if-eq v2, v3, :cond_1

    goto/32 :goto_13

    :cond_1
    goto/32 :goto_7

    nop

    :goto_12
    goto :goto_d

    :goto_13
    goto/32 :goto_c

    nop

    :goto_14
    if-nez p1, :cond_2

    goto/32 :goto_d

    :cond_2
    goto/32 :goto_9

    nop

    :goto_15
    if-nez v2, :cond_3

    goto/32 :goto_13

    :cond_3
    goto/32 :goto_6

    nop
.end method

.method removeCall(Ljava/lang/String;)V
    .locals 3

    goto/32 :goto_5

    nop

    :goto_0
    goto :goto_1

    :catch_0
    move-exception v2

    :goto_1
    goto/32 :goto_8

    nop

    :goto_2
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->removeCall(Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_0

    nop

    :goto_3
    return-void

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_7

    nop

    :goto_5
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_a

    nop

    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_2

    nop

    :goto_7
    if-nez v1, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_6

    nop

    :goto_8
    goto :goto_b

    :goto_9
    goto/32 :goto_3

    nop

    :goto_a
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_b
    goto/32 :goto_4

    nop
.end method

.method removeExtras(Ljava/lang/String;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_6

    nop

    :goto_0
    const/4 v1, 0x0

    goto/32 :goto_c

    nop

    :goto_1
    invoke-static {p0, v1, v0}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_13

    nop

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_7

    nop

    :goto_3
    if-nez v1, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_2

    nop

    :goto_4
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_5
    goto/32 :goto_d

    nop

    :goto_6
    const/4 v0, 0x2

    goto/32 :goto_a

    nop

    :goto_7
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, p2, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->removeExtras(Ljava/lang/String;Ljava/util/List;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_f

    nop

    :goto_8
    const-string/jumbo v1, "removeExtras: %s %s"

    goto/32 :goto_1

    nop

    :goto_9
    aput-object p2, v0, v1

    goto/32 :goto_8

    nop

    :goto_a
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_0

    nop

    :goto_b
    const/4 v1, 0x1

    goto/32 :goto_9

    nop

    :goto_c
    aput-object p1, v0, v1

    goto/32 :goto_b

    nop

    :goto_d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_3

    nop

    :goto_e
    return-void

    :goto_f
    goto :goto_10

    :catch_0
    move-exception v2

    :goto_10
    goto/32 :goto_11

    nop

    :goto_11
    goto :goto_5

    :goto_12
    goto/32 :goto_e

    nop

    :goto_13
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_4

    nop
.end method

.method resetConnectionTime(Ljava/lang/String;)V
    .locals 3

    goto/32 :goto_3

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_4

    nop

    :goto_1
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    goto/32 :goto_0

    nop

    :goto_3
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_1

    nop

    :goto_4
    if-nez v1, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_5

    nop

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_6

    nop

    :goto_6
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->resetConnectionTime(Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_a

    nop

    :goto_7
    return-void

    :goto_8
    goto :goto_2

    :goto_9
    goto/32 :goto_7

    nop

    :goto_a
    goto :goto_b

    :catch_0
    move-exception v2

    :goto_b
    goto/32 :goto_8

    nop
.end method

.method setActive(Ljava/lang/String;)V
    .locals 3

    goto/32 :goto_b

    nop

    :goto_0
    goto :goto_5

    :goto_1
    goto/32 :goto_6

    nop

    :goto_2
    goto :goto_3

    :catch_0
    move-exception v2

    :goto_3
    goto/32 :goto_0

    nop

    :goto_4
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_5
    goto/32 :goto_9

    nop

    :goto_6
    return-void

    :goto_7
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_8
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->setActive(Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_2

    nop

    :goto_9
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_a

    nop

    :goto_a
    if-nez v1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_7

    nop

    :goto_b
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_4

    nop
.end method

.method setAddress(Ljava/lang/String;Landroid/net/Uri;I)V
    .locals 3

    goto/32 :goto_6

    nop

    :goto_0
    if-nez v1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_7

    nop

    :goto_1
    return-void

    :goto_2
    goto :goto_b

    :goto_3
    goto/32 :goto_1

    nop

    :goto_4
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, p2, p3, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->setAddress(Ljava/lang/String;Landroid/net/Uri;ILandroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_8

    nop

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_0

    nop

    :goto_6
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_a

    nop

    :goto_7
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_4

    nop

    :goto_8
    goto :goto_9

    :catch_0
    move-exception v2

    :goto_9
    goto/32 :goto_2

    nop

    :goto_a
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_b
    goto/32 :goto_5

    nop
.end method

.method setAudioRoute(Ljava/lang/String;ILjava/lang/String;)V
    .locals 3

    goto/32 :goto_10

    nop

    :goto_0
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_2

    nop

    :goto_1
    if-nez v1, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_14

    nop

    :goto_2
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    goto/32 :goto_15

    nop

    :goto_4
    goto :goto_5

    :catch_0
    move-exception v2

    :goto_5
    goto/32 :goto_c

    nop

    :goto_6
    const/4 v2, 0x1

    goto/32 :goto_9

    nop

    :goto_7
    aput-object p1, v0, v1

    goto/32 :goto_f

    nop

    :goto_8
    const/4 v1, 0x2

    goto/32 :goto_e

    nop

    :goto_9
    aput-object v1, v0, v2

    goto/32 :goto_8

    nop

    :goto_a
    const/4 v1, 0x0

    goto/32 :goto_7

    nop

    :goto_b
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    nop

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, p2, p3, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->setAudioRoute(Ljava/lang/String;ILjava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_4

    nop

    :goto_c
    goto :goto_3

    :goto_d
    goto/32 :goto_12

    nop

    :goto_e
    aput-object p3, v0, v1

    goto/32 :goto_13

    nop

    :goto_f
    invoke-static {p2}, Landroid/telecom/CallAudioState;->audioRouteToString(I)Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_6

    nop

    :goto_10
    const/4 v0, 0x3

    goto/32 :goto_11

    nop

    :goto_11
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_a

    nop

    :goto_12
    return-void

    :goto_13
    const-string/jumbo v1, "setAudioRoute: %s %s %s"

    goto/32 :goto_16

    nop

    :goto_14
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_b

    nop

    :goto_15
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_1

    nop

    :goto_16
    invoke-static {p0, v1, v0}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_0

    nop
.end method

.method setCallDirection(Ljava/lang/String;I)V
    .locals 3

    goto/32 :goto_7

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_b

    nop

    :goto_1
    return-void

    :goto_2
    goto :goto_a

    :goto_3
    goto/32 :goto_1

    nop

    :goto_4
    goto :goto_5

    :catch_0
    move-exception v2

    :goto_5
    goto/32 :goto_2

    nop

    :goto_6
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, p2, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->setCallDirection(Ljava/lang/String;ILandroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_4

    nop

    :goto_7
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_9

    nop

    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_6

    nop

    :goto_9
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_a
    goto/32 :goto_0

    nop

    :goto_b
    if-nez v1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_8

    nop
.end method

.method setCallerDisplayName(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    goto/32 :goto_5

    nop

    :goto_0
    goto :goto_1

    :catch_0
    move-exception v2

    :goto_1
    goto/32 :goto_7

    nop

    :goto_2
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    goto/32 :goto_b

    nop

    :goto_4
    if-nez v1, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_6

    nop

    :goto_5
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_2

    nop

    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_9

    nop

    :goto_7
    goto :goto_3

    :goto_8
    goto/32 :goto_a

    nop

    :goto_9
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    nop

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, p2, p3, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->setCallerDisplayName(Ljava/lang/String;Ljava/lang/String;ILandroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_0

    nop

    :goto_a
    return-void

    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_4

    nop
.end method

.method setConferenceState(Ljava/lang/String;Z)V
    .locals 3

    goto/32 :goto_11

    nop

    :goto_0
    goto :goto_1

    :catch_0
    move-exception v2

    :goto_1
    goto/32 :goto_8

    nop

    :goto_2
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_14

    nop

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_b

    nop

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_5

    nop

    :goto_5
    if-nez v1, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_3

    nop

    :goto_6
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto/32 :goto_a

    nop

    :goto_7
    const-string/jumbo v1, "setConferenceState: %s %b"

    goto/32 :goto_c

    nop

    :goto_8
    goto :goto_f

    :goto_9
    goto/32 :goto_d

    nop

    :goto_a
    const/4 v2, 0x1

    goto/32 :goto_10

    nop

    :goto_b
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, p2, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->setConferenceState(Ljava/lang/String;ZLandroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_0

    nop

    :goto_c
    invoke-static {p0, v1, v0}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_13

    nop

    :goto_d
    return-void

    :goto_e
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_f
    goto/32 :goto_4

    nop

    :goto_10
    aput-object v1, v0, v2

    goto/32 :goto_7

    nop

    :goto_11
    const/4 v0, 0x2

    goto/32 :goto_2

    nop

    :goto_12
    aput-object p1, v0, v1

    goto/32 :goto_6

    nop

    :goto_13
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_e

    nop

    :goto_14
    const/4 v1, 0x0

    goto/32 :goto_12

    nop
.end method

.method setConferenceableConnections(Ljava/lang/String;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x2

    goto/32 :goto_f

    nop

    :goto_1
    aput-object p1, v0, v1

    goto/32 :goto_d

    nop

    :goto_2
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    goto/32 :goto_5

    nop

    :goto_4
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_2

    nop

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_6

    nop

    :goto_6
    if-nez v1, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_c

    nop

    :goto_7
    const-string/jumbo v1, "setConferenceableConnections: %s, %s"

    goto/32 :goto_10

    nop

    :goto_8
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    nop

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, p2, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->setConferenceableConnections(Ljava/lang/String;Ljava/util/List;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_12

    nop

    :goto_9
    goto :goto_3

    :goto_a
    goto/32 :goto_e

    nop

    :goto_b
    aput-object p2, v0, v1

    goto/32 :goto_7

    nop

    :goto_c
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_d
    const/4 v1, 0x1

    goto/32 :goto_b

    nop

    :goto_e
    return-void

    :goto_f
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_11

    nop

    :goto_10
    invoke-static {p0, v1, v0}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_4

    nop

    :goto_11
    const/4 v1, 0x0

    goto/32 :goto_1

    nop

    :goto_12
    goto :goto_13

    :catch_0
    move-exception v2

    :goto_13
    goto/32 :goto_9

    nop
.end method

.method setConnectionCapabilities(Ljava/lang/String;I)V
    .locals 3

    goto/32 :goto_a

    nop

    :goto_0
    if-nez v1, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_3

    nop

    :goto_1
    goto :goto_2

    :catch_0
    move-exception v2

    :goto_2
    goto/32 :goto_7

    nop

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_6

    nop

    :goto_4
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_5
    goto/32 :goto_b

    nop

    :goto_6
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, p2, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->setConnectionCapabilities(Ljava/lang/String;ILandroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_1

    nop

    :goto_7
    goto :goto_5

    :goto_8
    goto/32 :goto_9

    nop

    :goto_9
    return-void

    :goto_a
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_4

    nop

    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_0

    nop
.end method

.method setConnectionProperties(Ljava/lang/String;I)V
    .locals 3

    goto/32 :goto_9

    nop

    :goto_0
    goto :goto_4

    :goto_1
    goto/32 :goto_2

    nop

    :goto_2
    return-void

    :goto_3
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    goto/32 :goto_a

    nop

    :goto_5
    if-nez v1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_b

    nop

    :goto_6
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, p2, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->setConnectionProperties(Ljava/lang/String;ILandroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_7

    nop

    :goto_7
    goto :goto_8

    :catch_0
    move-exception v2

    :goto_8
    goto/32 :goto_0

    nop

    :goto_9
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_3

    nop

    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_5

    nop

    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_6

    nop
.end method

.method setDialing(Ljava/lang/String;)V
    .locals 3

    goto/32 :goto_3

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    goto/32 :goto_a

    nop

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_b

    nop

    :goto_3
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_0

    nop

    :goto_4
    if-nez v1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_2

    nop

    :goto_5
    goto :goto_1

    :goto_6
    goto/32 :goto_7

    nop

    :goto_7
    return-void

    :goto_8
    goto :goto_9

    :catch_0
    move-exception v2

    :goto_9
    goto/32 :goto_5

    nop

    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_4

    nop

    :goto_b
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->setDialing(Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_8

    nop
.end method

.method setDisconnected(Ljava/lang/String;Landroid/telecom/DisconnectCause;)V
    .locals 3

    goto/32 :goto_b

    nop

    :goto_0
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, p2, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->setDisconnected(Ljava/lang/String;Landroid/telecom/DisconnectCause;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_1

    nop

    :goto_1
    goto :goto_2

    :catch_0
    move-exception v2

    :goto_2
    goto/32 :goto_3

    nop

    :goto_3
    goto :goto_7

    :goto_4
    goto/32 :goto_9

    nop

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_a

    nop

    :goto_6
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_7
    goto/32 :goto_5

    nop

    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_9
    return-void

    :goto_a
    if-nez v1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_8

    nop

    :goto_b
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_6

    nop
.end method

.method setIsConferenced(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    goto/32 :goto_a

    nop

    :goto_0
    goto :goto_1

    :catch_0
    move-exception v2

    :goto_1
    goto/32 :goto_8

    nop

    :goto_2
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    goto/32 :goto_6

    nop

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_5

    nop

    :goto_5
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    const-string/jumbo v2, "sending connection %s with conference %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-static {p0, v2, v3}, Landroid/telecom/Log;->d(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, p2, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->setIsConferenced(Ljava/lang/String;Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_0

    nop

    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_7

    nop

    :goto_7
    if-nez v1, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_4

    nop

    :goto_8
    goto :goto_3

    :goto_9
    goto/32 :goto_b

    nop

    :goto_a
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_2

    nop

    :goto_b
    return-void
.end method

.method setIsVoipAudioMode(Ljava/lang/String;Z)V
    .locals 3

    goto/32 :goto_b

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_1

    nop

    :goto_1
    if-nez v1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_7

    nop

    :goto_2
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    goto/32 :goto_0

    nop

    :goto_4
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, p2, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->setIsVoipAudioMode(Ljava/lang/String;ZLandroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_8

    nop

    :goto_5
    goto :goto_3

    :goto_6
    goto/32 :goto_a

    nop

    :goto_7
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_4

    nop

    :goto_8
    goto :goto_9

    :catch_0
    move-exception v2

    :goto_9
    goto/32 :goto_5

    nop

    :goto_a
    return-void

    :goto_b
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_2

    nop
.end method

.method setOnHold(Ljava/lang/String;)V
    .locals 3

    goto/32 :goto_b

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->setOnHold(Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_7

    nop

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_4

    nop

    :goto_4
    if-nez v1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_0

    nop

    :goto_5
    goto :goto_a

    :goto_6
    goto/32 :goto_1

    nop

    :goto_7
    goto :goto_8

    :catch_0
    move-exception v2

    :goto_8
    goto/32 :goto_5

    nop

    :goto_9
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_a
    goto/32 :goto_3

    nop

    :goto_b
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_9

    nop
.end method

.method setPulling(Ljava/lang/String;)V
    .locals 3

    goto/32 :goto_1

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_9

    nop

    :goto_1
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_a

    nop

    :goto_2
    goto :goto_b

    :goto_3
    goto/32 :goto_6

    nop

    :goto_4
    goto :goto_5

    :catch_0
    move-exception v2

    :goto_5
    goto/32 :goto_2

    nop

    :goto_6
    return-void

    :goto_7
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_8
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->setPulling(Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_4

    nop

    :goto_9
    if-nez v1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_7

    nop

    :goto_a
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_b
    goto/32 :goto_0

    nop
.end method

.method setRingbackRequested(Ljava/lang/String;Z)V
    .locals 3

    goto/32 :goto_a

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    goto/32 :goto_6

    nop

    :goto_2
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, p2, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->setRingbackRequested(Ljava/lang/String;ZLandroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_3

    nop

    :goto_3
    goto :goto_4

    :catch_0
    move-exception v2

    :goto_4
    goto/32 :goto_7

    nop

    :goto_5
    if-nez v1, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_9

    nop

    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_5

    nop

    :goto_7
    goto :goto_1

    :goto_8
    goto/32 :goto_b

    nop

    :goto_9
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_2

    nop

    :goto_a
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_0

    nop

    :goto_b
    return-void
.end method

.method setRinging(Ljava/lang/String;)V
    .locals 3

    goto/32 :goto_5

    nop

    :goto_0
    if-nez v1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_9

    nop

    :goto_1
    goto :goto_b

    :goto_2
    goto/32 :goto_8

    nop

    :goto_3
    goto :goto_4

    :catch_0
    move-exception v2

    :goto_4
    goto/32 :goto_1

    nop

    :goto_5
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_a

    nop

    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_0

    nop

    :goto_7
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->setRinging(Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_3

    nop

    :goto_8
    return-void

    :goto_9
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_7

    nop

    :goto_a
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_b
    goto/32 :goto_6

    nop
.end method

.method setStatusHints(Ljava/lang/String;Landroid/telecom/StatusHints;)V
    .locals 3

    goto/32 :goto_8

    nop

    :goto_0
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, p2, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->setStatusHints(Ljava/lang/String;Landroid/telecom/StatusHints;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_9

    nop

    :goto_1
    goto :goto_6

    :goto_2
    goto/32 :goto_b

    nop

    :goto_3
    if-nez v1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_7

    nop

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_3

    nop

    :goto_5
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    goto/32 :goto_4

    nop

    :goto_7
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_8
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_5

    nop

    :goto_9
    goto :goto_a

    :catch_0
    move-exception v2

    :goto_a
    goto/32 :goto_1

    nop

    :goto_b
    return-void
.end method

.method setVideoProvider(Ljava/lang/String;Landroid/telecom/Connection$VideoProvider;)V
    .locals 4

    goto/32 :goto_5

    nop

    :goto_0
    return-void

    :goto_1
    goto :goto_2

    :catch_0
    move-exception v2

    :goto_2
    goto/32 :goto_3

    nop

    :goto_3
    goto :goto_10

    :goto_4
    goto/32 :goto_0

    nop

    :goto_5
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_f

    nop

    :goto_6
    goto :goto_8

    :goto_7
    :try_start_0
    invoke-virtual {p2}, Landroid/telecom/Connection$VideoProvider;->getInterface()Lcom/android/internal/telecom/IVideoProvider;

    move-result-object v2

    :goto_8
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v3

    invoke-interface {v1, p1, v2, v3}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->setVideoProvider(Ljava/lang/String;Lcom/android/internal/telecom/IVideoProvider;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_1

    nop

    :goto_9
    if-eqz p2, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_e

    nop

    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_c

    nop

    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_d

    nop

    :goto_c
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    nop

    goto/32 :goto_9

    nop

    :goto_d
    if-nez v1, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_a

    nop

    :goto_e
    const/4 v2, 0x0

    goto/32 :goto_6

    nop

    :goto_f
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_10
    goto/32 :goto_b

    nop
.end method

.method setVideoState(Ljava/lang/String;I)V
    .locals 3

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x1

    goto/32 :goto_4

    nop

    :goto_1
    goto :goto_2

    :catch_0
    move-exception v2

    :goto_2
    goto/32 :goto_a

    nop

    :goto_3
    if-nez v1, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_8

    nop

    :goto_4
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_10

    nop

    :goto_5
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    goto/32 :goto_11

    nop

    :goto_7
    iget-object v0, p0, Landroid/telecom/ConnectionServiceAdapter;->mAdapters:Ljava/util/Set;

    goto/32 :goto_5

    nop

    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_e

    nop

    :goto_9
    const-string/jumbo v1, "setVideoState: %d"

    goto/32 :goto_12

    nop

    :goto_a
    goto :goto_6

    :goto_b
    goto/32 :goto_d

    nop

    :goto_c
    const/4 v2, 0x0

    goto/32 :goto_f

    nop

    :goto_d
    return-void

    :goto_e
    check-cast v1, Lcom/android/internal/telecom/IConnectionServiceAdapter;

    :try_start_0
    invoke-static {}, Landroid/telecom/Log;->getExternalSession()Landroid/telecom/Logging/Session$Info;

    move-result-object v2

    invoke-interface {v1, p1, p2, v2}, Lcom/android/internal/telecom/IConnectionServiceAdapter;->setVideoState(Ljava/lang/String;ILandroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_1

    nop

    :goto_f
    aput-object v1, v0, v2

    goto/32 :goto_9

    nop

    :goto_10
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_c

    nop

    :goto_11
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_3

    nop

    :goto_12
    invoke-static {p0, v1, v0}, Landroid/telecom/Log;->v(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_7

    nop
.end method
