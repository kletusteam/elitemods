.class public final Landroid/telecom/RemoteConference;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/telecom/RemoteConference$Callback;
    }
.end annotation


# instance fields
.field private final mCallbackRecords:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Landroid/telecom/CallbackRecord<",
            "Landroid/telecom/RemoteConference$Callback;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mChildConnections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/telecom/RemoteConnection;",
            ">;"
        }
    .end annotation
.end field

.field private final mConferenceableConnections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/telecom/RemoteConnection;",
            ">;"
        }
    .end annotation
.end field

.field private mConnectionCapabilities:I

.field private mConnectionProperties:I

.field private final mConnectionService:Lcom/android/internal/telecom/IConnectionService;

.field private mDisconnectCause:Landroid/telecom/DisconnectCause;

.field private mExtras:Landroid/os/Bundle;

.field private final mId:Ljava/lang/String;

.field private mState:I

.field private final mUnmodifiableChildConnections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/telecom/RemoteConnection;",
            ">;"
        }
    .end annotation
.end field

.field private final mUnmodifiableConferenceableConnections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/telecom/RemoteConnection;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetmConnectionCapabilities(Landroid/telecom/RemoteConference;)I
    .locals 0

    iget p0, p0, Landroid/telecom/RemoteConference;->mConnectionCapabilities:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmConnectionProperties(Landroid/telecom/RemoteConference;)I
    .locals 0

    iget p0, p0, Landroid/telecom/RemoteConference;->mConnectionProperties:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmExtras(Landroid/telecom/RemoteConference;)Landroid/os/Bundle;
    .locals 0

    iget-object p0, p0, Landroid/telecom/RemoteConference;->mExtras:Landroid/os/Bundle;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmUnmodifiableConferenceableConnections(Landroid/telecom/RemoteConference;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Landroid/telecom/RemoteConference;->mUnmodifiableConferenceableConnections:Ljava/util/List;

    return-object p0
.end method

.method constructor <init>(Landroid/telecom/DisconnectCause;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Landroid/telecom/RemoteConference;->mCallbackRecords:Ljava/util/Set;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Landroid/telecom/RemoteConference;->mChildConnections:Ljava/util/List;

    nop

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Landroid/telecom/RemoteConference;->mUnmodifiableChildConnections:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/telecom/RemoteConference;->mConferenceableConnections:Ljava/util/List;

    nop

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Landroid/telecom/RemoteConference;->mUnmodifiableConferenceableConnections:Ljava/util/List;

    const/4 v0, 0x1

    iput v0, p0, Landroid/telecom/RemoteConference;->mState:I

    const-string v0, "NULL"

    iput-object v0, p0, Landroid/telecom/RemoteConference;->mId:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/telecom/RemoteConference;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    const/4 v0, 0x6

    iput v0, p0, Landroid/telecom/RemoteConference;->mState:I

    iput-object p1, p0, Landroid/telecom/RemoteConference;->mDisconnectCause:Landroid/telecom/DisconnectCause;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lcom/android/internal/telecom/IConnectionService;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Landroid/telecom/RemoteConference;->mCallbackRecords:Ljava/util/Set;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Landroid/telecom/RemoteConference;->mChildConnections:Ljava/util/List;

    nop

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Landroid/telecom/RemoteConference;->mUnmodifiableChildConnections:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/telecom/RemoteConference;->mConferenceableConnections:Ljava/util/List;

    nop

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Landroid/telecom/RemoteConference;->mUnmodifiableConferenceableConnections:Ljava/util/List;

    const/4 v0, 0x1

    iput v0, p0, Landroid/telecom/RemoteConference;->mState:I

    iput-object p1, p0, Landroid/telecom/RemoteConference;->mId:Ljava/lang/String;

    iput-object p2, p0, Landroid/telecom/RemoteConference;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    return-void
.end method

.method public static failure(Landroid/telecom/DisconnectCause;)Landroid/telecom/RemoteConference;
    .locals 1

    new-instance v0, Landroid/telecom/RemoteConference;

    invoke-direct {v0, p0}, Landroid/telecom/RemoteConference;-><init>(Landroid/telecom/DisconnectCause;)V

    return-object v0
.end method

.method private notifyExtrasChanged()V
    .locals 6

    iget-object v0, p0, Landroid/telecom/RemoteConference;->mCallbackRecords:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/CallbackRecord;

    move-object v2, p0

    invoke-virtual {v1}, Landroid/telecom/CallbackRecord;->getCallback()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telecom/RemoteConference$Callback;

    invoke-virtual {v1}, Landroid/telecom/CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v4

    new-instance v5, Landroid/telecom/RemoteConference$9;

    invoke-direct {v5, p0, v3, v2}, Landroid/telecom/RemoteConference$9;-><init>(Landroid/telecom/RemoteConference;Landroid/telecom/RemoteConference$Callback;Landroid/telecom/RemoteConference;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method addConnection(Landroid/telecom/RemoteConnection;)V
    .locals 6

    goto/32 :goto_3

    nop

    :goto_0
    invoke-virtual {p1, p0}, Landroid/telecom/RemoteConnection;->setConference(Landroid/telecom/RemoteConference;)V

    goto/32 :goto_7

    nop

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_6

    nop

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_16

    nop

    :goto_3
    iget-object v0, p0, Landroid/telecom/RemoteConference;->mChildConnections:Ljava/util/List;

    goto/32 :goto_e

    nop

    :goto_4
    move-object v2, p0

    goto/32 :goto_a

    nop

    :goto_5
    invoke-direct {v5, p0, v3, v2, p1}, Landroid/telecom/RemoteConference$3;-><init>(Landroid/telecom/RemoteConference;Landroid/telecom/RemoteConference$Callback;Landroid/telecom/RemoteConference;Landroid/telecom/RemoteConnection;)V

    goto/32 :goto_c

    nop

    :goto_6
    if-nez v1, :cond_0

    goto/32 :goto_15

    :cond_0
    goto/32 :goto_2

    nop

    :goto_7
    iget-object v0, p0, Landroid/telecom/RemoteConference;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_11

    nop

    :goto_8
    check-cast v3, Landroid/telecom/RemoteConference$Callback;

    goto/32 :goto_f

    nop

    :goto_9
    iget-object v0, p0, Landroid/telecom/RemoteConference;->mChildConnections:Ljava/util/List;

    goto/32 :goto_b

    nop

    :goto_a
    invoke-virtual {v1}, Landroid/telecom/CallbackRecord;->getCallback()Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_8

    nop

    :goto_b
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_0

    nop

    :goto_c
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_14

    nop

    :goto_d
    if-eqz v0, :cond_1

    goto/32 :goto_15

    :cond_1
    goto/32 :goto_9

    nop

    :goto_e
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_d

    nop

    :goto_f
    invoke-virtual {v1}, Landroid/telecom/CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v4

    goto/32 :goto_13

    nop

    :goto_10
    return-void

    :goto_11
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_12
    goto/32 :goto_1

    nop

    :goto_13
    new-instance v5, Landroid/telecom/RemoteConference$3;

    goto/32 :goto_5

    nop

    :goto_14
    goto :goto_12

    :goto_15
    goto/32 :goto_10

    nop

    :goto_16
    check-cast v1, Landroid/telecom/CallbackRecord;

    goto/32 :goto_4

    nop
.end method

.method public disconnect()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Landroid/telecom/RemoteConference;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iget-object v1, p0, Landroid/telecom/RemoteConference;->mId:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/android/internal/telecom/IConnectionService;->disconnect(Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :goto_0
    return-void
.end method

.method public getConferenceableConnections()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/telecom/RemoteConnection;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Landroid/telecom/RemoteConference;->mUnmodifiableConferenceableConnections:Ljava/util/List;

    return-object v0
.end method

.method public final getConnectionCapabilities()I
    .locals 1

    iget v0, p0, Landroid/telecom/RemoteConference;->mConnectionCapabilities:I

    return v0
.end method

.method public final getConnectionProperties()I
    .locals 1

    iget v0, p0, Landroid/telecom/RemoteConference;->mConnectionProperties:I

    return v0
.end method

.method public final getConnections()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/telecom/RemoteConnection;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Landroid/telecom/RemoteConference;->mUnmodifiableChildConnections:Ljava/util/List;

    return-object v0
.end method

.method public getDisconnectCause()Landroid/telecom/DisconnectCause;
    .locals 1

    iget-object v0, p0, Landroid/telecom/RemoteConference;->mDisconnectCause:Landroid/telecom/DisconnectCause;

    return-object v0
.end method

.method public final getExtras()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Landroid/telecom/RemoteConference;->mExtras:Landroid/os/Bundle;

    return-object v0
.end method

.method getId()Ljava/lang/String;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Landroid/telecom/RemoteConference;->mId:Ljava/lang/String;

    goto/32 :goto_0

    nop
.end method

.method public final getState()I
    .locals 1

    iget v0, p0, Landroid/telecom/RemoteConference;->mState:I

    return v0
.end method

.method public hold()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Landroid/telecom/RemoteConference;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iget-object v1, p0, Landroid/telecom/RemoteConference;->mId:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/android/internal/telecom/IConnectionService;->hold(Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :goto_0
    return-void
.end method

.method public merge()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Landroid/telecom/RemoteConference;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iget-object v1, p0, Landroid/telecom/RemoteConference;->mId:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/android/internal/telecom/IConnectionService;->mergeConference(Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :goto_0
    return-void
.end method

.method public playDtmfTone(C)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Landroid/telecom/RemoteConference;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iget-object v1, p0, Landroid/telecom/RemoteConference;->mId:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, p1, v2}, Lcom/android/internal/telecom/IConnectionService;->playDtmfTone(Ljava/lang/String;CLandroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :goto_0
    return-void
.end method

.method putExtras(Landroid/os/Bundle;)V
    .locals 1

    goto/32 :goto_8

    nop

    :goto_0
    iget-object v0, p0, Landroid/telecom/RemoteConference;->mExtras:Landroid/os/Bundle;

    goto/32 :goto_9

    nop

    :goto_1
    return-void

    :goto_2
    iput-object v0, p0, Landroid/telecom/RemoteConference;->mExtras:Landroid/os/Bundle;

    :goto_3
    goto/32 :goto_b

    nop

    :goto_4
    return-void

    :goto_5
    goto/32 :goto_0

    nop

    :goto_6
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    goto/32 :goto_a

    nop

    :goto_7
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto/32 :goto_2

    nop

    :goto_8
    if-eqz p1, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_4

    nop

    :goto_9
    if-eqz v0, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_c

    nop

    :goto_a
    invoke-direct {p0}, Landroid/telecom/RemoteConference;->notifyExtrasChanged()V

    goto/32 :goto_1

    nop

    :goto_b
    iget-object v0, p0, Landroid/telecom/RemoteConference;->mExtras:Landroid/os/Bundle;

    goto/32 :goto_6

    nop

    :goto_c
    new-instance v0, Landroid/os/Bundle;

    goto/32 :goto_7

    nop
.end method

.method public final registerCallback(Landroid/telecom/RemoteConference$Callback;)V
    .locals 1

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-virtual {p0, p1, v0}, Landroid/telecom/RemoteConference;->registerCallback(Landroid/telecom/RemoteConference$Callback;Landroid/os/Handler;)V

    return-void
.end method

.method public final registerCallback(Landroid/telecom/RemoteConference$Callback;Landroid/os/Handler;)V
    .locals 2

    invoke-virtual {p0, p1}, Landroid/telecom/RemoteConference;->unregisterCallback(Landroid/telecom/RemoteConference$Callback;)V

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p0, Landroid/telecom/RemoteConference;->mCallbackRecords:Ljava/util/Set;

    new-instance v1, Landroid/telecom/CallbackRecord;

    invoke-direct {v1, p1, p2}, Landroid/telecom/CallbackRecord;-><init>(Ljava/lang/Object;Landroid/os/Handler;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method removeConnection(Landroid/telecom/RemoteConnection;)V
    .locals 6

    goto/32 :goto_3

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_7

    nop

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_9

    nop

    :goto_2
    const/4 v0, 0x0

    goto/32 :goto_a

    nop

    :goto_3
    iget-object v0, p0, Landroid/telecom/RemoteConference;->mChildConnections:Ljava/util/List;

    goto/32 :goto_6

    nop

    :goto_4
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_5
    goto/32 :goto_1

    nop

    :goto_6
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_8

    nop

    :goto_7
    check-cast v1, Landroid/telecom/CallbackRecord;

    goto/32 :goto_17

    nop

    :goto_8
    if-nez v0, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_f

    nop

    :goto_9
    if-nez v1, :cond_1

    goto/32 :goto_12

    :cond_1
    goto/32 :goto_0

    nop

    :goto_a
    invoke-virtual {p1, v0}, Landroid/telecom/RemoteConnection;->setConference(Landroid/telecom/RemoteConference;)V

    goto/32 :goto_14

    nop

    :goto_b
    return-void

    :goto_c
    new-instance v5, Landroid/telecom/RemoteConference$4;

    goto/32 :goto_13

    nop

    :goto_d
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/32 :goto_2

    nop

    :goto_e
    check-cast v3, Landroid/telecom/RemoteConference$Callback;

    goto/32 :goto_15

    nop

    :goto_f
    iget-object v0, p0, Landroid/telecom/RemoteConference;->mChildConnections:Ljava/util/List;

    goto/32 :goto_d

    nop

    :goto_10
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_11

    nop

    :goto_11
    goto :goto_5

    :goto_12
    goto/32 :goto_b

    nop

    :goto_13
    invoke-direct {v5, p0, v3, v2, p1}, Landroid/telecom/RemoteConference$4;-><init>(Landroid/telecom/RemoteConference;Landroid/telecom/RemoteConference$Callback;Landroid/telecom/RemoteConference;Landroid/telecom/RemoteConnection;)V

    goto/32 :goto_10

    nop

    :goto_14
    iget-object v0, p0, Landroid/telecom/RemoteConference;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_4

    nop

    :goto_15
    invoke-virtual {v1}, Landroid/telecom/CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v4

    goto/32 :goto_c

    nop

    :goto_16
    invoke-virtual {v1}, Landroid/telecom/CallbackRecord;->getCallback()Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_e

    nop

    :goto_17
    move-object v2, p0

    goto/32 :goto_16

    nop
.end method

.method removeExtras(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_10

    nop

    :goto_0
    return-void

    :goto_1
    goto :goto_12

    :goto_2
    goto/32 :goto_14

    nop

    :goto_3
    goto :goto_c

    :goto_4
    goto/32 :goto_11

    nop

    :goto_5
    iget-object v2, p0, Landroid/telecom/RemoteConference;->mExtras:Landroid/os/Bundle;

    goto/32 :goto_8

    nop

    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_e

    nop

    :goto_7
    if-nez p1, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_f

    nop

    :goto_8
    invoke-virtual {v2, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    goto/32 :goto_1

    nop

    :goto_9
    check-cast v1, Ljava/lang/String;

    goto/32 :goto_5

    nop

    :goto_a
    if-nez v0, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_7

    nop

    :goto_b
    return-void

    :goto_c
    goto/32 :goto_0

    nop

    :goto_d
    if-nez v0, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_3

    nop

    :goto_e
    if-nez v1, :cond_3

    goto/32 :goto_2

    :cond_3
    goto/32 :goto_13

    nop

    :goto_f
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    goto/32 :goto_d

    nop

    :goto_10
    iget-object v0, p0, Landroid/telecom/RemoteConference;->mExtras:Landroid/os/Bundle;

    goto/32 :goto_a

    nop

    :goto_11
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_12
    goto/32 :goto_6

    nop

    :goto_13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_9

    nop

    :goto_14
    invoke-direct {p0}, Landroid/telecom/RemoteConference;->notifyExtrasChanged()V

    goto/32 :goto_b

    nop
.end method

.method public separate(Landroid/telecom/RemoteConnection;)V
    .locals 3

    iget-object v0, p0, Landroid/telecom/RemoteConference;->mChildConnections:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Landroid/telecom/RemoteConference;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    invoke-virtual {p1}, Landroid/telecom/RemoteConnection;->getId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/android/internal/telecom/IConnectionService;->splitFromConference(Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :cond_0
    :goto_0
    return-void
.end method

.method public setAudioState(Landroid/telecom/AudioState;)V
    .locals 1
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance v0, Landroid/telecom/CallAudioState;

    invoke-direct {v0, p1}, Landroid/telecom/CallAudioState;-><init>(Landroid/telecom/AudioState;)V

    invoke-virtual {p0, v0}, Landroid/telecom/RemoteConference;->setCallAudioState(Landroid/telecom/CallAudioState;)V

    return-void
.end method

.method public setCallAudioState(Landroid/telecom/CallAudioState;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Landroid/telecom/RemoteConference;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iget-object v1, p0, Landroid/telecom/RemoteConference;->mId:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, p1, v2}, Lcom/android/internal/telecom/IConnectionService;->onCallAudioStateChanged(Ljava/lang/String;Landroid/telecom/CallAudioState;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :goto_0
    return-void
.end method

.method setConferenceableConnections(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/telecom/RemoteConnection;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_12

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto/32 :goto_a

    nop

    :goto_1
    invoke-virtual {v1}, Landroid/telecom/CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v4

    goto/32 :goto_f

    nop

    :goto_2
    return-void

    :goto_3
    iget-object v0, p0, Landroid/telecom/RemoteConference;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_8

    nop

    :goto_4
    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/32 :goto_3

    nop

    :goto_5
    check-cast v3, Landroid/telecom/RemoteConference$Callback;

    goto/32 :goto_1

    nop

    :goto_6
    if-nez v1, :cond_0

    goto/32 :goto_14

    :cond_0
    goto/32 :goto_e

    nop

    :goto_7
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_6

    nop

    :goto_8
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_9
    goto/32 :goto_7

    nop

    :goto_a
    iget-object v0, p0, Landroid/telecom/RemoteConference;->mConferenceableConnections:Ljava/util/List;

    goto/32 :goto_4

    nop

    :goto_b
    move-object v2, p0

    goto/32 :goto_10

    nop

    :goto_c
    invoke-direct {v5, p0, v3, v2}, Landroid/telecom/RemoteConference$7;-><init>(Landroid/telecom/RemoteConference;Landroid/telecom/RemoteConference$Callback;Landroid/telecom/RemoteConference;)V

    goto/32 :goto_d

    nop

    :goto_d
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_13

    nop

    :goto_e
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_11

    nop

    :goto_f
    new-instance v5, Landroid/telecom/RemoteConference$7;

    goto/32 :goto_c

    nop

    :goto_10
    invoke-virtual {v1}, Landroid/telecom/CallbackRecord;->getCallback()Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_5

    nop

    :goto_11
    check-cast v1, Landroid/telecom/CallbackRecord;

    goto/32 :goto_b

    nop

    :goto_12
    iget-object v0, p0, Landroid/telecom/RemoteConference;->mConferenceableConnections:Ljava/util/List;

    goto/32 :goto_0

    nop

    :goto_13
    goto :goto_9

    :goto_14
    goto/32 :goto_2

    nop
.end method

.method setConnectionCapabilities(I)V
    .locals 6

    goto/32 :goto_8

    nop

    :goto_0
    move-object v2, p0

    goto/32 :goto_f

    nop

    :goto_1
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_12

    nop

    :goto_2
    invoke-direct {v5, p0, v3, v2}, Landroid/telecom/RemoteConference$5;-><init>(Landroid/telecom/RemoteConference;Landroid/telecom/RemoteConference$Callback;Landroid/telecom/RemoteConference;)V

    goto/32 :goto_1

    nop

    :goto_3
    invoke-virtual {v1}, Landroid/telecom/CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v4

    goto/32 :goto_9

    nop

    :goto_4
    if-ne v0, p1, :cond_0

    goto/32 :goto_13

    :cond_0
    goto/32 :goto_e

    nop

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_6

    nop

    :goto_6
    if-nez v1, :cond_1

    goto/32 :goto_13

    :cond_1
    goto/32 :goto_d

    nop

    :goto_7
    iget-object v0, p0, Landroid/telecom/RemoteConference;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_10

    nop

    :goto_8
    iget v0, p0, Landroid/telecom/RemoteConference;->mConnectionCapabilities:I

    goto/32 :goto_4

    nop

    :goto_9
    new-instance v5, Landroid/telecom/RemoteConference$5;

    goto/32 :goto_2

    nop

    :goto_a
    return-void

    :goto_b
    check-cast v1, Landroid/telecom/CallbackRecord;

    goto/32 :goto_0

    nop

    :goto_c
    check-cast v3, Landroid/telecom/RemoteConference$Callback;

    goto/32 :goto_3

    nop

    :goto_d
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_b

    nop

    :goto_e
    iput p1, p0, Landroid/telecom/RemoteConference;->mConnectionCapabilities:I

    goto/32 :goto_7

    nop

    :goto_f
    invoke-virtual {v1}, Landroid/telecom/CallbackRecord;->getCallback()Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_c

    nop

    :goto_10
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_11
    goto/32 :goto_5

    nop

    :goto_12
    goto :goto_11

    :goto_13
    goto/32 :goto_a

    nop
.end method

.method setConnectionProperties(I)V
    .locals 6

    goto/32 :goto_12

    nop

    :goto_0
    invoke-virtual {v1}, Landroid/telecom/CallbackRecord;->getCallback()Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_2

    nop

    :goto_1
    iput p1, p0, Landroid/telecom/RemoteConference;->mConnectionProperties:I

    goto/32 :goto_3

    nop

    :goto_2
    check-cast v3, Landroid/telecom/RemoteConference$Callback;

    goto/32 :goto_7

    nop

    :goto_3
    iget-object v0, p0, Landroid/telecom/RemoteConference;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_8

    nop

    :goto_4
    if-ne v0, p1, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_1

    nop

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_13

    nop

    :goto_6
    new-instance v5, Landroid/telecom/RemoteConference$6;

    goto/32 :goto_c

    nop

    :goto_7
    invoke-virtual {v1}, Landroid/telecom/CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v4

    goto/32 :goto_6

    nop

    :goto_8
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_9
    goto/32 :goto_5

    nop

    :goto_a
    move-object v2, p0

    goto/32 :goto_0

    nop

    :goto_b
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_d

    nop

    :goto_c
    invoke-direct {v5, p0, v3, v2}, Landroid/telecom/RemoteConference$6;-><init>(Landroid/telecom/RemoteConference;Landroid/telecom/RemoteConference$Callback;Landroid/telecom/RemoteConference;)V

    goto/32 :goto_b

    nop

    :goto_d
    goto :goto_9

    :goto_e
    goto/32 :goto_10

    nop

    :goto_f
    check-cast v1, Landroid/telecom/CallbackRecord;

    goto/32 :goto_a

    nop

    :goto_10
    return-void

    :goto_11
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_f

    nop

    :goto_12
    iget v0, p0, Landroid/telecom/RemoteConference;->mConnectionProperties:I

    goto/32 :goto_4

    nop

    :goto_13
    if-nez v1, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_11

    nop
.end method

.method setDestroyed()V
    .locals 6

    goto/32 :goto_8

    nop

    :goto_0
    if-nez v1, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_b

    nop

    :goto_1
    goto :goto_14

    :goto_2
    goto/32 :goto_18

    nop

    :goto_3
    check-cast v1, Landroid/telecom/RemoteConnection;

    goto/32 :goto_17

    nop

    :goto_4
    move-object v2, p0

    goto/32 :goto_9

    nop

    :goto_5
    invoke-virtual {v1}, Landroid/telecom/CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v4

    goto/32 :goto_6

    nop

    :goto_6
    new-instance v5, Landroid/telecom/RemoteConference$1;

    goto/32 :goto_7

    nop

    :goto_7
    invoke-direct {v5, p0, v3, v2}, Landroid/telecom/RemoteConference$1;-><init>(Landroid/telecom/RemoteConference;Landroid/telecom/RemoteConference$Callback;Landroid/telecom/RemoteConference;)V

    goto/32 :goto_f

    nop

    :goto_8
    iget-object v0, p0, Landroid/telecom/RemoteConference;->mChildConnections:Ljava/util/List;

    goto/32 :goto_13

    nop

    :goto_9
    invoke-virtual {v1}, Landroid/telecom/CallbackRecord;->getCallback()Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_12

    nop

    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_0

    nop

    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_e

    nop

    :goto_c
    goto :goto_1a

    :goto_d
    goto/32 :goto_15

    nop

    :goto_e
    check-cast v1, Landroid/telecom/CallbackRecord;

    goto/32 :goto_4

    nop

    :goto_f
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_c

    nop

    :goto_10
    if-nez v1, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_11

    nop

    :goto_11
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_3

    nop

    :goto_12
    check-cast v3, Landroid/telecom/RemoteConference$Callback;

    goto/32 :goto_5

    nop

    :goto_13
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_14
    goto/32 :goto_16

    nop

    :goto_15
    return-void

    :goto_16
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_10

    nop

    :goto_17
    const/4 v2, 0x0

    goto/32 :goto_1b

    nop

    :goto_18
    iget-object v0, p0, Landroid/telecom/RemoteConference;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_19

    nop

    :goto_19
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1a
    goto/32 :goto_a

    nop

    :goto_1b
    invoke-virtual {v1, v2}, Landroid/telecom/RemoteConnection;->setConference(Landroid/telecom/RemoteConference;)V

    goto/32 :goto_1

    nop
.end method

.method setDisconnected(Landroid/telecom/DisconnectCause;)V
    .locals 6

    goto/32 :goto_13

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_11

    nop

    :goto_1
    invoke-direct {v5, p0, v3, v2, p1}, Landroid/telecom/RemoteConference$8;-><init>(Landroid/telecom/RemoteConference;Landroid/telecom/RemoteConference$Callback;Landroid/telecom/RemoteConference;Landroid/telecom/DisconnectCause;)V

    goto/32 :goto_10

    nop

    :goto_2
    return-void

    :goto_3
    invoke-virtual {v1}, Landroid/telecom/CallbackRecord;->getCallback()Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_8

    nop

    :goto_4
    const/4 v1, 0x6

    goto/32 :goto_9

    nop

    :goto_5
    check-cast v1, Landroid/telecom/CallbackRecord;

    goto/32 :goto_b

    nop

    :goto_6
    new-instance v5, Landroid/telecom/RemoteConference$8;

    goto/32 :goto_1

    nop

    :goto_7
    iget-object v0, p0, Landroid/telecom/RemoteConference;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_c

    nop

    :goto_8
    check-cast v3, Landroid/telecom/RemoteConference$Callback;

    goto/32 :goto_e

    nop

    :goto_9
    if-ne v0, v1, :cond_0

    goto/32 :goto_15

    :cond_0
    goto/32 :goto_a

    nop

    :goto_a
    iput-object p1, p0, Landroid/telecom/RemoteConference;->mDisconnectCause:Landroid/telecom/DisconnectCause;

    goto/32 :goto_12

    nop

    :goto_b
    move-object v2, p0

    goto/32 :goto_3

    nop

    :goto_c
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_d
    goto/32 :goto_0

    nop

    :goto_e
    invoke-virtual {v1}, Landroid/telecom/CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v4

    goto/32 :goto_6

    nop

    :goto_f
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_5

    nop

    :goto_10
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_14

    nop

    :goto_11
    if-nez v1, :cond_1

    goto/32 :goto_15

    :cond_1
    goto/32 :goto_f

    nop

    :goto_12
    invoke-virtual {p0, v1}, Landroid/telecom/RemoteConference;->setState(I)V

    goto/32 :goto_7

    nop

    :goto_13
    iget v0, p0, Landroid/telecom/RemoteConference;->mState:I

    goto/32 :goto_4

    nop

    :goto_14
    goto :goto_d

    :goto_15
    goto/32 :goto_2

    nop
.end method

.method setState(I)V
    .locals 12

    goto/32 :goto_9

    nop

    :goto_0
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_13

    nop

    :goto_1
    invoke-virtual {v10, v11}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_15

    nop

    :goto_2
    if-ne p1, v0, :cond_0

    goto/32 :goto_23

    :cond_0
    goto/32 :goto_7

    nop

    :goto_3
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    goto/32 :goto_c

    nop

    :goto_5
    const/4 v0, 0x5

    goto/32 :goto_d

    nop

    :goto_6
    move v6, p1

    goto/32 :goto_18

    nop

    :goto_7
    const/4 v0, 0x1

    goto/32 :goto_0

    nop

    :goto_8
    return-void

    :goto_9
    const/4 v0, 0x4

    goto/32 :goto_2a

    nop

    :goto_a
    invoke-static {p1}, Landroid/telecom/Connection;->stateToString(I)Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_1b

    nop

    :goto_b
    check-cast v8, Landroid/telecom/CallbackRecord;

    goto/32 :goto_27

    nop

    :goto_c
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_e

    nop

    :goto_d
    if-ne p1, v0, :cond_1

    goto/32 :goto_23

    :cond_1
    goto/32 :goto_1d

    nop

    :goto_e
    if-nez v1, :cond_2

    goto/32 :goto_16

    :cond_2
    goto/32 :goto_1e

    nop

    :goto_f
    invoke-static {p0, v1, v0}, Landroid/telecom/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_22

    nop

    :goto_10
    new-instance v11, Landroid/telecom/RemoteConference$2;

    goto/32 :goto_14

    nop

    :goto_11
    if-ne v0, p1, :cond_3

    goto/32 :goto_16

    :cond_3
    goto/32 :goto_24

    nop

    :goto_12
    invoke-virtual {v8}, Landroid/telecom/CallbackRecord;->getCallback()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_25

    nop

    :goto_13
    const/4 v1, 0x0

    goto/32 :goto_a

    nop

    :goto_14
    move-object v1, v11

    goto/32 :goto_29

    nop

    :goto_15
    goto :goto_4

    :goto_16
    goto/32 :goto_8

    nop

    :goto_17
    iget-object v1, p0, Landroid/telecom/RemoteConference;->mCallbackRecords:Ljava/util/Set;

    goto/32 :goto_3

    nop

    :goto_18
    invoke-direct/range {v1 .. v6}, Landroid/telecom/RemoteConference$2;-><init>(Landroid/telecom/RemoteConference;Landroid/telecom/RemoteConference$Callback;Landroid/telecom/RemoteConference;II)V

    goto/32 :goto_1

    nop

    :goto_19
    check-cast v9, Landroid/telecom/RemoteConference$Callback;

    goto/32 :goto_28

    nop

    :goto_1a
    const-string v1, "Unsupported state transition for Conference call."

    goto/32 :goto_f

    nop

    :goto_1b
    aput-object v2, v0, v1

    goto/32 :goto_1a

    nop

    :goto_1c
    move-object v3, v9

    goto/32 :goto_26

    nop

    :goto_1d
    const/4 v0, 0x6

    goto/32 :goto_2

    nop

    :goto_1e
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_21

    nop

    :goto_1f
    iget v0, p0, Landroid/telecom/RemoteConference;->mState:I

    goto/32 :goto_11

    nop

    :goto_20
    iput p1, p0, Landroid/telecom/RemoteConference;->mState:I

    goto/32 :goto_17

    nop

    :goto_21
    move-object v8, v1

    goto/32 :goto_b

    nop

    :goto_22
    return-void

    :goto_23
    goto/32 :goto_1f

    nop

    :goto_24
    iget v0, p0, Landroid/telecom/RemoteConference;->mState:I

    goto/32 :goto_20

    nop

    :goto_25
    move-object v9, v1

    goto/32 :goto_19

    nop

    :goto_26
    move v5, v0

    goto/32 :goto_6

    nop

    :goto_27
    move-object v4, p0

    goto/32 :goto_12

    nop

    :goto_28
    invoke-virtual {v8}, Landroid/telecom/CallbackRecord;->getHandler()Landroid/os/Handler;

    move-result-object v10

    goto/32 :goto_10

    nop

    :goto_29
    move-object v2, p0

    goto/32 :goto_1c

    nop

    :goto_2a
    if-ne p1, v0, :cond_4

    goto/32 :goto_23

    :cond_4
    goto/32 :goto_5

    nop
.end method

.method public stopDtmfTone()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Landroid/telecom/RemoteConference;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iget-object v1, p0, Landroid/telecom/RemoteConference;->mId:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/android/internal/telecom/IConnectionService;->stopDtmfTone(Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :goto_0
    return-void
.end method

.method public swap()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Landroid/telecom/RemoteConference;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iget-object v1, p0, Landroid/telecom/RemoteConference;->mId:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/android/internal/telecom/IConnectionService;->swapConference(Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :goto_0
    return-void
.end method

.method public unhold()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Landroid/telecom/RemoteConference;->mConnectionService:Lcom/android/internal/telecom/IConnectionService;

    iget-object v1, p0, Landroid/telecom/RemoteConference;->mId:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/android/internal/telecom/IConnectionService;->unhold(Ljava/lang/String;Landroid/telecom/Logging/Session$Info;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :goto_0
    return-void
.end method

.method public final unregisterCallback(Landroid/telecom/RemoteConference$Callback;)V
    .locals 3

    if-eqz p1, :cond_1

    iget-object v0, p0, Landroid/telecom/RemoteConference;->mCallbackRecords:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/CallbackRecord;

    invoke-virtual {v1}, Landroid/telecom/CallbackRecord;->getCallback()Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p1, :cond_0

    iget-object v0, p0, Landroid/telecom/RemoteConference;->mCallbackRecords:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method
