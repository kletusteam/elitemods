.class public final Landroid/telecom/Phone;
.super Ljava/lang/Object;


# annotations
.annotation runtime Landroid/annotation/SystemApi;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/telecom/Phone$Listener;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final SDK_VERSION_R:I = 0x1e


# instance fields
.field private mCallAudioState:Landroid/telecom/CallAudioState;

.field private final mCallByTelecomCallId:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/telecom/Call;",
            ">;"
        }
    .end annotation
.end field

.field private final mCallingPackage:Ljava/lang/String;

.field private final mCalls:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/telecom/Call;",
            ">;"
        }
    .end annotation
.end field

.field private mCanAddCall:Z

.field private final mInCallAdapter:Landroid/telecom/InCallAdapter;

.field private final mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/telecom/Phone$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private final mLock:Ljava/lang/Object;

.field private final mTargetSdkVersion:I

.field private final mUnmodifiableCalls:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/telecom/Call;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/telecom/InCallAdapter;Ljava/lang/String;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Landroid/telecom/Phone;->mCallByTelecomCallId:Ljava/util/Map;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Landroid/telecom/Phone;->mCalls:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Landroid/telecom/Phone;->mUnmodifiableCalls:Ljava/util/List;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Landroid/telecom/Phone;->mListeners:Ljava/util/List;

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/telecom/Phone;->mCanAddCall:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Landroid/telecom/Phone;->mLock:Ljava/lang/Object;

    iput-object p1, p0, Landroid/telecom/Phone;->mInCallAdapter:Landroid/telecom/InCallAdapter;

    iput-object p2, p0, Landroid/telecom/Phone;->mCallingPackage:Ljava/lang/String;

    iput p3, p0, Landroid/telecom/Phone;->mTargetSdkVersion:I

    return-void
.end method

.method private checkCallTree(Landroid/telecom/ParcelableCall;)V
    .locals 4

    invoke-virtual {p1}, Landroid/telecom/ParcelableCall;->getChildCallIds()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Landroid/telecom/ParcelableCall;->getChildCallIds()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Landroid/telecom/Phone;->mCallByTelecomCallId:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/telecom/ParcelableCall;->getChildCallIds()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/telecom/ParcelableCall;->getId()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1}, Landroid/telecom/ParcelableCall;->getChildCallIds()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const-string v2, "ParcelableCall %s has nonexistent child %s"

    invoke-static {p0, v2, v1}, Landroid/telecom/Log;->wtf(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private fireBringToForeground(Z)V
    .locals 2

    iget-object v0, p0, Landroid/telecom/Phone;->mListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/Phone$Listener;

    invoke-virtual {v1, p0, p1}, Landroid/telecom/Phone$Listener;->onBringToForeground(Landroid/telecom/Phone;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private fireCallAdded(Landroid/telecom/Call;)V
    .locals 2

    iget-object v0, p0, Landroid/telecom/Phone;->mListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/Phone$Listener;

    invoke-virtual {v1, p0, p1}, Landroid/telecom/Phone$Listener;->onCallAdded(Landroid/telecom/Phone;Landroid/telecom/Call;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private fireCallAudioStateChanged(Landroid/telecom/CallAudioState;)V
    .locals 3

    iget-object v0, p0, Landroid/telecom/Phone;->mListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/Phone$Listener;

    invoke-virtual {v1, p0, p1}, Landroid/telecom/Phone$Listener;->onCallAudioStateChanged(Landroid/telecom/Phone;Landroid/telecom/CallAudioState;)V

    new-instance v2, Landroid/telecom/AudioState;

    invoke-direct {v2, p1}, Landroid/telecom/AudioState;-><init>(Landroid/telecom/CallAudioState;)V

    invoke-virtual {v1, p0, v2}, Landroid/telecom/Phone$Listener;->onAudioStateChanged(Landroid/telecom/Phone;Landroid/telecom/AudioState;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private fireCallRemoved(Landroid/telecom/Call;)V
    .locals 2

    iget-object v0, p0, Landroid/telecom/Phone;->mListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/Phone$Listener;

    invoke-virtual {v1, p0, p1}, Landroid/telecom/Phone$Listener;->onCallRemoved(Landroid/telecom/Phone;Landroid/telecom/Call;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private fireCanAddCallChanged(Z)V
    .locals 2

    iget-object v0, p0, Landroid/telecom/Phone;->mListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/Phone$Listener;

    invoke-virtual {v1, p0, p1}, Landroid/telecom/Phone$Listener;->onCanAddCallChanged(Landroid/telecom/Phone;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private fireSilenceRinger()V
    .locals 2

    iget-object v0, p0, Landroid/telecom/Phone;->mListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/Phone$Listener;

    invoke-virtual {v1, p0}, Landroid/telecom/Phone$Listener;->onSilenceRinger(Landroid/telecom/Phone;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final addListener(Landroid/telecom/Phone$Listener;)V
    .locals 1

    iget-object v0, p0, Landroid/telecom/Phone;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final canAddCall()Z
    .locals 1

    iget-boolean v0, p0, Landroid/telecom/Phone;->mCanAddCall:Z

    return v0
.end method

.method final destroy()V
    .locals 5

    goto/32 :goto_11

    nop

    :goto_0
    const/4 v4, 0x7

    goto/32 :goto_d

    nop

    :goto_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    goto/32 :goto_c

    nop

    :goto_3
    check-cast v1, Landroid/telecom/Call;

    goto/32 :goto_12

    nop

    :goto_4
    invoke-virtual {v2}, Landroid/telecom/InCallService$VideoCall;->destroy()V

    :goto_5
    goto/32 :goto_10

    nop

    :goto_6
    goto :goto_2

    :goto_7
    goto/32 :goto_b

    nop

    :goto_8
    if-nez v2, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_4

    nop

    :goto_9
    invoke-virtual {v1}, Landroid/telecom/Call;->internalSetDisconnected()V

    :goto_a
    goto/32 :goto_6

    nop

    :goto_b
    return-void

    :goto_c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_e

    nop

    :goto_d
    if-ne v3, v4, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_9

    nop

    :goto_e
    if-nez v1, :cond_2

    goto/32 :goto_7

    :cond_2
    goto/32 :goto_f

    nop

    :goto_f
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_3

    nop

    :goto_10
    invoke-virtual {v1}, Landroid/telecom/Call;->getState()I

    move-result v3

    goto/32 :goto_0

    nop

    :goto_11
    iget-object v0, p0, Landroid/telecom/Phone;->mCalls:Ljava/util/List;

    goto/32 :goto_1

    nop

    :goto_12
    invoke-virtual {v1}, Landroid/telecom/Call;->getVideoCall()Landroid/telecom/InCallService$VideoCall;

    move-result-object v2

    goto/32 :goto_8

    nop
.end method

.method public final getAudioState()Landroid/telecom/AudioState;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance v0, Landroid/telecom/AudioState;

    iget-object v1, p0, Landroid/telecom/Phone;->mCallAudioState:Landroid/telecom/CallAudioState;

    invoke-direct {v0, v1}, Landroid/telecom/AudioState;-><init>(Landroid/telecom/CallAudioState;)V

    return-object v0
.end method

.method public final getCallAudioState()Landroid/telecom/CallAudioState;
    .locals 1

    iget-object v0, p0, Landroid/telecom/Phone;->mCallAudioState:Landroid/telecom/CallAudioState;

    return-object v0
.end method

.method getCallById(Ljava/lang/String;)Landroid/telecom/Call;
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Landroid/telecom/Phone;->mCallByTelecomCallId:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/Call;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Landroid/telecom/Phone;->mLock:Ljava/lang/Object;

    goto/32 :goto_0

    nop

    :goto_2
    throw v1
.end method

.method public final getCalls()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/telecom/Call;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Landroid/telecom/Phone;->mUnmodifiableCalls:Ljava/util/List;

    return-object v0
.end method

.method final internalAddCall(Landroid/telecom/ParcelableCall;)V
    .locals 9

    goto/32 :goto_17

    nop

    :goto_0
    new-array v1, v1, [Ljava/lang/Object;

    goto/32 :goto_2

    nop

    :goto_1
    if-lt v0, v2, :cond_0

    goto/32 :goto_2c

    :cond_0
    goto/32 :goto_1d

    nop

    :goto_2
    invoke-static {p0, v0, v1}, Landroid/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_2b

    nop

    :goto_3
    const-string v2, "Call %s added, but it was already present"

    goto/32 :goto_e

    nop

    :goto_4
    const/4 v1, 0x0

    goto/32 :goto_22

    nop

    :goto_5
    invoke-direct {p0, p1}, Landroid/telecom/Phone;->checkCallTree(Landroid/telecom/ParcelableCall;)V

    goto/32 :goto_26

    nop

    :goto_6
    invoke-virtual {v0}, Landroid/telecom/Call;->internalGetCallId()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_1c

    nop

    :goto_7
    iget-object v1, p0, Landroid/telecom/Phone;->mCallByTelecomCallId:Ljava/util/Map;

    goto/32 :goto_1a

    nop

    :goto_8
    iget-object v7, p0, Landroid/telecom/Phone;->mCallingPackage:Ljava/lang/String;

    goto/32 :goto_f

    nop

    :goto_9
    move-object v0, v2

    goto/32 :goto_1e

    nop

    :goto_a
    move-object v2, v1

    goto/32 :goto_29

    nop

    :goto_b
    iget-object v3, p0, Landroid/telecom/Phone;->mLock:Ljava/lang/Object;

    goto/32 :goto_12

    nop

    :goto_c
    invoke-virtual {p1}, Landroid/telecom/ParcelableCall;->getId()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_18

    nop

    :goto_d
    invoke-virtual {v2, p1, v0}, Landroid/telecom/Call;->internalUpdate(Landroid/telecom/ParcelableCall;Ljava/util/Map;)V

    goto/32 :goto_14

    nop

    :goto_e
    const/4 v3, 0x1

    goto/32 :goto_13

    nop

    :goto_f
    iget v8, p0, Landroid/telecom/Phone;->mTargetSdkVersion:I

    goto/32 :goto_a

    nop

    :goto_10
    const-string v0, "Skipping adding audio processing call for sdk compatibility"

    goto/32 :goto_0

    nop

    :goto_11
    if-eq v0, v2, :cond_1

    goto/32 :goto_2c

    :cond_1
    goto/32 :goto_10

    nop

    :goto_12
    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Landroid/telecom/Phone;->mCallByTelecomCallId:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/telecom/ParcelableCall;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Landroid/telecom/Phone;->mCalls:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_5

    nop

    :goto_13
    new-array v3, v3, [Ljava/lang/Object;

    goto/32 :goto_6

    nop

    :goto_14
    invoke-direct {p0, v2}, Landroid/telecom/Phone;->fireCallAdded(Landroid/telecom/Call;)V

    goto/32 :goto_9

    nop

    :goto_15
    throw v0

    :goto_16
    goto/32 :goto_3

    nop

    :goto_17
    iget v0, p0, Landroid/telecom/Phone;->mTargetSdkVersion:I

    goto/32 :goto_4

    nop

    :goto_18
    iget-object v5, p0, Landroid/telecom/Phone;->mInCallAdapter:Landroid/telecom/InCallAdapter;

    goto/32 :goto_23

    nop

    :goto_19
    if-eqz v0, :cond_2

    goto/32 :goto_16

    :cond_2
    goto/32 :goto_25

    nop

    :goto_1a
    invoke-virtual {v0, p1, v1}, Landroid/telecom/Call;->internalUpdate(Landroid/telecom/ParcelableCall;Ljava/util/Map;)V

    :goto_1b
    goto/32 :goto_24

    nop

    :goto_1c
    aput-object v4, v3, v1

    goto/32 :goto_21

    nop

    :goto_1d
    invoke-virtual {p1}, Landroid/telecom/ParcelableCall;->getState()I

    move-result v0

    goto/32 :goto_1f

    nop

    :goto_1e
    goto :goto_1b

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/32 :goto_15

    nop

    :goto_1f
    const/16 v2, 0xc

    goto/32 :goto_11

    nop

    :goto_20
    invoke-direct {p0, p1}, Landroid/telecom/Phone;->checkCallTree(Landroid/telecom/ParcelableCall;)V

    goto/32 :goto_7

    nop

    :goto_21
    invoke-static {p0, v2, v3}, Landroid/telecom/Log;->w(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_20

    nop

    :goto_22
    const/16 v2, 0x1e

    goto/32 :goto_1

    nop

    :goto_23
    invoke-virtual {p1}, Landroid/telecom/ParcelableCall;->getState()I

    move-result v6

    goto/32 :goto_8

    nop

    :goto_24
    return-void

    :goto_25
    new-instance v1, Landroid/telecom/Call;

    goto/32 :goto_c

    nop

    :goto_26
    iget-object v0, p0, Landroid/telecom/Phone;->mCallByTelecomCallId:Ljava/util/Map;

    goto/32 :goto_d

    nop

    :goto_27
    invoke-direct/range {v2 .. v8}, Landroid/telecom/Call;-><init>(Landroid/telecom/Phone;Ljava/lang/String;Landroid/telecom/InCallAdapter;ILjava/lang/String;I)V

    goto/32 :goto_b

    nop

    :goto_28
    invoke-virtual {p1}, Landroid/telecom/ParcelableCall;->getId()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_2a

    nop

    :goto_29
    move-object v3, p0

    goto/32 :goto_27

    nop

    :goto_2a
    invoke-virtual {p0, v0}, Landroid/telecom/Phone;->getCallById(Ljava/lang/String;)Landroid/telecom/Call;

    move-result-object v0

    goto/32 :goto_19

    nop

    :goto_2b
    return-void

    :goto_2c
    goto/32 :goto_28

    nop
.end method

.method final internalBringToForeground(Z)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-direct {p0, p1}, Landroid/telecom/Phone;->fireBringToForeground(Z)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method final internalCallAudioStateChanged(Landroid/telecom/CallAudioState;)V
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    invoke-direct {p0, p1}, Landroid/telecom/Phone;->fireCallAudioStateChanged(Landroid/telecom/CallAudioState;)V

    :goto_1
    goto/32 :goto_5

    nop

    :goto_2
    invoke-static {v0, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_6

    nop

    :goto_3
    iput-object p1, p0, Landroid/telecom/Phone;->mCallAudioState:Landroid/telecom/CallAudioState;

    goto/32 :goto_0

    nop

    :goto_4
    iget-object v0, p0, Landroid/telecom/Phone;->mCallAudioState:Landroid/telecom/CallAudioState;

    goto/32 :goto_2

    nop

    :goto_5
    return-void

    :goto_6
    if-eqz v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_3

    nop
.end method

.method final internalGetCallByTelecomId(Ljava/lang/String;)Landroid/telecom/Call;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0, p1}, Landroid/telecom/Phone;->getCallById(Ljava/lang/String;)Landroid/telecom/Call;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method final internalOnConnectionEvent(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0, p1}, Landroid/telecom/Phone;->getCallById(Ljava/lang/String;)Landroid/telecom/Call;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_1
    return-void

    :goto_2
    invoke-virtual {v0, p2, p3}, Landroid/telecom/Call;->internalOnConnectionEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    :goto_3
    goto/32 :goto_1

    nop

    :goto_4
    if-nez v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_2

    nop
.end method

.method final internalOnHandoverComplete(Ljava/lang/String;)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_2

    nop

    :goto_1
    invoke-virtual {p0, p1}, Landroid/telecom/Phone;->getCallById(Ljava/lang/String;)Landroid/telecom/Call;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_2
    invoke-virtual {v0}, Landroid/telecom/Call;->internalOnHandoverComplete()V

    :goto_3
    goto/32 :goto_4

    nop

    :goto_4
    return-void
.end method

.method final internalOnHandoverFailed(Ljava/lang/String;I)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {v0, p2}, Landroid/telecom/Call;->internalOnHandoverFailed(I)V

    :goto_1
    goto/32 :goto_4

    nop

    :goto_2
    invoke-virtual {p0, p1}, Landroid/telecom/Phone;->getCallById(Ljava/lang/String;)Landroid/telecom/Call;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_0

    nop

    :goto_4
    return-void
.end method

.method final internalOnRttInitiationFailure(Ljava/lang/String;I)V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    invoke-virtual {v0, p2}, Landroid/telecom/Call;->internalOnRttInitiationFailure(I)V

    :goto_1
    goto/32 :goto_4

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_0

    nop

    :goto_3
    invoke-virtual {p0, p1}, Landroid/telecom/Phone;->getCallById(Ljava/lang/String;)Landroid/telecom/Call;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_4
    return-void
.end method

.method final internalOnRttUpgradeRequest(Ljava/lang/String;I)V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    invoke-virtual {v0, p2}, Landroid/telecom/Call;->internalOnRttUpgradeRequest(I)V

    :goto_1
    goto/32 :goto_4

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_0

    nop

    :goto_3
    invoke-virtual {p0, p1}, Landroid/telecom/Phone;->getCallById(Ljava/lang/String;)Landroid/telecom/Call;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_4
    return-void
.end method

.method final internalRemoveCall(Landroid/telecom/Call;)V
    .locals 3

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/telecom/Phone;->mLock:Ljava/lang/Object;

    goto/32 :goto_6

    nop

    :goto_1
    throw v1

    :goto_2
    return-void

    :catchall_0
    move-exception v1

    :try_start_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_1

    nop

    :goto_3
    invoke-virtual {p1}, Landroid/telecom/Call;->getVideoCall()Landroid/telecom/InCallService$VideoCall;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_4
    invoke-virtual {v0}, Landroid/telecom/InCallService$VideoCall;->destroy()V

    :goto_5
    goto/32 :goto_8

    nop

    :goto_6
    monitor-enter v0

    :try_start_1
    iget-object v1, p0, Landroid/telecom/Phone;->mCallByTelecomCallId:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/telecom/Call;->internalGetCallId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Landroid/telecom/Phone;->mCalls:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/32 :goto_3

    nop

    :goto_7
    if-nez v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_4

    nop

    :goto_8
    invoke-direct {p0, p1}, Landroid/telecom/Phone;->fireCallRemoved(Landroid/telecom/Call;)V

    goto/32 :goto_2

    nop
.end method

.method final internalSetCanAddCall(Z)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-boolean v0, p0, Landroid/telecom/Phone;->mCanAddCall:Z

    goto/32 :goto_5

    nop

    :goto_1
    invoke-direct {p0, p1}, Landroid/telecom/Phone;->fireCanAddCallChanged(Z)V

    :goto_2
    goto/32 :goto_4

    nop

    :goto_3
    iput-boolean p1, p0, Landroid/telecom/Phone;->mCanAddCall:Z

    goto/32 :goto_1

    nop

    :goto_4
    return-void

    :goto_5
    if-ne v0, p1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_3

    nop
.end method

.method final internalSetPostDialWait(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    invoke-virtual {v0, p2}, Landroid/telecom/Call;->internalSetPostDialWait(Ljava/lang/String;)V

    :goto_1
    goto/32 :goto_4

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_0

    nop

    :goto_3
    invoke-virtual {p0, p1}, Landroid/telecom/Phone;->getCallById(Ljava/lang/String;)Landroid/telecom/Call;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_4
    return-void
.end method

.method final internalSilenceRinger()V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-direct {p0}, Landroid/telecom/Phone;->fireSilenceRinger()V

    goto/32 :goto_0

    nop
.end method

.method final internalUpdateCall(Landroid/telecom/ParcelableCall;)V
    .locals 4

    goto/32 :goto_1

    nop

    :goto_0
    const/4 v3, 0x4

    goto/32 :goto_13

    nop

    :goto_1
    iget v0, p0, Landroid/telecom/Phone;->mTargetSdkVersion:I

    goto/32 :goto_27

    nop

    :goto_2
    const/16 v2, 0x1e

    goto/32 :goto_f

    nop

    :goto_3
    invoke-virtual {p0, v0}, Landroid/telecom/Phone;->getCallById(Ljava/lang/String;)Landroid/telecom/Call;

    move-result-object v0

    goto/32 :goto_b

    nop

    :goto_4
    const-string v2, "adding call during update for sdk compatibility"

    goto/32 :goto_22

    nop

    :goto_5
    if-eq v2, v3, :cond_0

    goto/32 :goto_21

    :cond_0
    :goto_6
    goto/32 :goto_14

    nop

    :goto_7
    invoke-direct {p0, p1}, Landroid/telecom/Phone;->checkCallTree(Landroid/telecom/ParcelableCall;)V

    goto/32 :goto_e

    nop

    :goto_8
    new-array v0, v1, [Ljava/lang/Object;

    goto/32 :goto_1a

    nop

    :goto_9
    goto/16 :goto_21

    :goto_a
    goto/32 :goto_19

    nop

    :goto_b
    if-nez v0, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_c

    nop

    :goto_c
    invoke-virtual {p0, v0}, Landroid/telecom/Phone;->internalRemoveCall(Landroid/telecom/Call;)V

    :goto_d
    goto/32 :goto_1d

    nop

    :goto_e
    iget-object v1, p0, Landroid/telecom/Phone;->mCallByTelecomCallId:Ljava/util/Map;

    goto/32 :goto_1b

    nop

    :goto_f
    if-lt v0, v2, :cond_2

    goto/32 :goto_1e

    :cond_2
    goto/32 :goto_1c

    nop

    :goto_10
    invoke-static {p0, v1, v0}, Landroid/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_15

    nop

    :goto_11
    const/16 v3, 0xd

    goto/32 :goto_5

    nop

    :goto_12
    if-nez v0, :cond_3

    goto/32 :goto_a

    :cond_3
    goto/32 :goto_7

    nop

    :goto_13
    if-ne v2, v3, :cond_4

    goto/32 :goto_6

    :cond_4
    goto/32 :goto_23

    nop

    :goto_14
    new-array v1, v1, [Ljava/lang/Object;

    goto/32 :goto_4

    nop

    :goto_15
    invoke-virtual {p1}, Landroid/telecom/ParcelableCall;->getId()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_16
    invoke-virtual {p1}, Landroid/telecom/ParcelableCall;->getState()I

    move-result v2

    goto/32 :goto_0

    nop

    :goto_17
    return-void

    :goto_18
    invoke-virtual {p0, v0}, Landroid/telecom/Phone;->getCallById(Ljava/lang/String;)Landroid/telecom/Call;

    move-result-object v0

    goto/32 :goto_12

    nop

    :goto_19
    iget v3, p0, Landroid/telecom/Phone;->mTargetSdkVersion:I

    goto/32 :goto_25

    nop

    :goto_1a
    const-string/jumbo v1, "removing audio processing call during update for sdk compatibility"

    goto/32 :goto_10

    nop

    :goto_1b
    invoke-virtual {v0, p1, v1}, Landroid/telecom/Call;->internalUpdate(Landroid/telecom/ParcelableCall;Ljava/util/Map;)V

    goto/32 :goto_9

    nop

    :goto_1c
    invoke-virtual {p1}, Landroid/telecom/ParcelableCall;->getState()I

    move-result v0

    goto/32 :goto_26

    nop

    :goto_1d
    return-void

    :goto_1e
    goto/32 :goto_1f

    nop

    :goto_1f
    invoke-virtual {p1}, Landroid/telecom/ParcelableCall;->getId()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_18

    nop

    :goto_20
    invoke-virtual {p0, p1}, Landroid/telecom/Phone;->internalAddCall(Landroid/telecom/ParcelableCall;)V

    :goto_21
    goto/32 :goto_17

    nop

    :goto_22
    invoke-static {p0, v2, v1}, Landroid/telecom/Log;->i(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_20

    nop

    :goto_23
    invoke-virtual {p1}, Landroid/telecom/ParcelableCall;->getState()I

    move-result v2

    goto/32 :goto_11

    nop

    :goto_24
    if-eq v0, v3, :cond_5

    goto/32 :goto_1e

    :cond_5
    goto/32 :goto_8

    nop

    :goto_25
    if-lt v3, v2, :cond_6

    goto/32 :goto_21

    :cond_6
    goto/32 :goto_16

    nop

    :goto_26
    const/16 v3, 0xc

    goto/32 :goto_24

    nop

    :goto_27
    const/4 v1, 0x0

    goto/32 :goto_2

    nop
.end method

.method public final removeListener(Landroid/telecom/Phone$Listener;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Landroid/telecom/Phone;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public requestBluetoothAudio(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Landroid/telecom/Phone;->mInCallAdapter:Landroid/telecom/InCallAdapter;

    invoke-virtual {v0, p1}, Landroid/telecom/InCallAdapter;->requestBluetoothAudio(Ljava/lang/String;)V

    return-void
.end method

.method public final setAudioRoute(I)V
    .locals 1

    iget-object v0, p0, Landroid/telecom/Phone;->mInCallAdapter:Landroid/telecom/InCallAdapter;

    invoke-virtual {v0, p1}, Landroid/telecom/InCallAdapter;->setAudioRoute(I)V

    return-void
.end method

.method public final setMuted(Z)V
    .locals 1

    iget-object v0, p0, Landroid/telecom/Phone;->mInCallAdapter:Landroid/telecom/InCallAdapter;

    invoke-virtual {v0, p1}, Landroid/telecom/InCallAdapter;->mute(Z)V

    return-void
.end method

.method public final setProximitySensorOff(Z)V
    .locals 1

    iget-object v0, p0, Landroid/telecom/Phone;->mInCallAdapter:Landroid/telecom/InCallAdapter;

    invoke-virtual {v0, p1}, Landroid/telecom/InCallAdapter;->turnProximitySensorOff(Z)V

    return-void
.end method

.method public final setProximitySensorOn()V
    .locals 1

    iget-object v0, p0, Landroid/telecom/Phone;->mInCallAdapter:Landroid/telecom/InCallAdapter;

    invoke-virtual {v0}, Landroid/telecom/InCallAdapter;->turnProximitySensorOn()V

    return-void
.end method
