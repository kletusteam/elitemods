.class public final Landroid/timezone/CountryTimeZones$TimeZoneMapping;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/timezone/CountryTimeZones;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TimeZoneMapping"
.end annotation


# instance fields
.field private mDelegate:Lcom/android/i18n/timezone/CountryTimeZones$TimeZoneMapping;


# direct methods
.method constructor <init>(Lcom/android/i18n/timezone/CountryTimeZones$TimeZoneMapping;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/i18n/timezone/CountryTimeZones$TimeZoneMapping;

    iput-object v0, p0, Landroid/timezone/CountryTimeZones$TimeZoneMapping;->mDelegate:Lcom/android/i18n/timezone/CountryTimeZones$TimeZoneMapping;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_1

    goto :goto_0

    :cond_1
    move-object v0, p1

    check-cast v0, Landroid/timezone/CountryTimeZones$TimeZoneMapping;

    iget-object v1, p0, Landroid/timezone/CountryTimeZones$TimeZoneMapping;->mDelegate:Lcom/android/i18n/timezone/CountryTimeZones$TimeZoneMapping;

    iget-object v2, v0, Landroid/timezone/CountryTimeZones$TimeZoneMapping;->mDelegate:Lcom/android/i18n/timezone/CountryTimeZones$TimeZoneMapping;

    invoke-virtual {v1, v2}, Lcom/android/i18n/timezone/CountryTimeZones$TimeZoneMapping;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1

    :cond_2
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public getTimeZone()Landroid/icu/util/TimeZone;
    .locals 1

    iget-object v0, p0, Landroid/timezone/CountryTimeZones$TimeZoneMapping;->mDelegate:Lcom/android/i18n/timezone/CountryTimeZones$TimeZoneMapping;

    invoke-virtual {v0}, Lcom/android/i18n/timezone/CountryTimeZones$TimeZoneMapping;->getTimeZone()Landroid/icu/util/TimeZone;

    move-result-object v0

    return-object v0
.end method

.method public getTimeZoneId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/timezone/CountryTimeZones$TimeZoneMapping;->mDelegate:Lcom/android/i18n/timezone/CountryTimeZones$TimeZoneMapping;

    invoke-virtual {v0}, Lcom/android/i18n/timezone/CountryTimeZones$TimeZoneMapping;->getTimeZoneId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Landroid/timezone/CountryTimeZones$TimeZoneMapping;->mDelegate:Lcom/android/i18n/timezone/CountryTimeZones$TimeZoneMapping;

    invoke-virtual {v0}, Lcom/android/i18n/timezone/CountryTimeZones$TimeZoneMapping;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/timezone/CountryTimeZones$TimeZoneMapping;->mDelegate:Lcom/android/i18n/timezone/CountryTimeZones$TimeZoneMapping;

    invoke-virtual {v0}, Lcom/android/i18n/timezone/CountryTimeZones$TimeZoneMapping;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
