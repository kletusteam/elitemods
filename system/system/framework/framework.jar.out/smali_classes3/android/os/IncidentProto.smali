.class public final Landroid/os/IncidentProto;
.super Ljava/lang/Object;


# static fields
.field public static final ACTIVITIES:J = 0x10b00000bc4L

.field public static final ALARM:J = 0x10b00000bc8L

.field public static final AMPROCESSES:J = 0x10b00000bc7L

.field public static final AMSERVICES:J = 0x10b00000bc6L

.field public static final APPWIDGET:J = 0x10b00000bbbL

.field public static final BATTERY:J = 0x10b00000bbeL

.field public static final BATTERYSTATS:J = 0x10b00000bbdL

.field public static final BATTERY_HISTORY:J = 0x10b00000bceL

.field public static final BATTERY_TYPE:J = 0x10b000007d6L

.field public static final BLUETOOTH_MANAGER:J = 0x10b00000beaL

.field public static final BROADCASTS:J = 0x10b00000bc5L

.field public static final CONTEXT_HUB:J = 0x10b00000bebL

.field public static final CPU_FREQ:J = 0x10b000007d4L

.field public static final CPU_INFO:J = 0x10b000007d3L

.field public static final CRASH_LOGS:J = 0x10b00000451L

.field public static final DISKSTATS:J = 0x10b00000bbfL

.field public static final DROPBOX_DATA_APP_ANR:J = 0x10b00000bd4L

.field public static final DROPBOX_DATA_APP_CRASH:J = 0x10b00000bd3L

.field public static final DROPBOX_DATA_APP_NATIVE_CRASH:J = 0x10b00000bd5L

.field public static final DROPBOX_DATA_APP_STRICTMODE:J = 0x10b00000bd6L

.field public static final DROPBOX_DATA_APP_WTF:J = 0x10b00000bd7L

.field public static final DROPBOX_SUBSYSTEM_RESTART:J = 0x10b00000be6L

.field public static final DROPBOX_SYSTEM_APP_ANR:J = 0x10b00000bd9L

.field public static final DROPBOX_SYSTEM_APP_CRASH:J = 0x10b00000bd8L

.field public static final DROPBOX_SYSTEM_APP_NATIVE_CRASH:J = 0x10b00000bdaL

.field public static final DROPBOX_SYSTEM_APP_STRICTMODE:J = 0x10b00000bdbL

.field public static final DROPBOX_SYSTEM_APP_WTF:J = 0x10b00000bdcL

.field public static final DROPBOX_SYSTEM_RECOVERY_LOG:J = 0x10b00000be4L

.field public static final DROPBOX_SYSTEM_SERVER_ANR:J = 0x10b00000bdeL

.field public static final DROPBOX_SYSTEM_SERVER_CRASH:J = 0x10b00000bddL

.field public static final DROPBOX_SYSTEM_SERVER_LOWMEM:J = 0x10b00000be0L

.field public static final DROPBOX_SYSTEM_SERVER_NATIVE_CRASH:J = 0x10b00000bdfL

.field public static final DROPBOX_SYSTEM_SERVER_STRICTMODE:J = 0x10b00000be1L

.field public static final DROPBOX_SYSTEM_SERVER_WATCHDOG:J = 0x10b00000be2L

.field public static final DROPBOX_SYSTEM_SERVER_WTF:J = 0x10b00000be3L

.field public static final DROPBOX_SYSTEM_TOMBSTONE:J = 0x10b00000be5L

.field public static final EVENTS_LOGS:J = 0x10b0000044fL

.field public static final EVENT_LOG_TAG_MAP:J = 0x10b0000044cL

.field public static final FINGERPRINT:J = 0x10b00000bb8L

.field public static final GRAPHICSSTATS:J = 0x10b00000bcbL

.field public static final HAL_TRACES:J = 0x10b000004b1L

.field public static final HEADER:J = 0x20b00000001L

.field public static final IP_CONNECTIVITY_METRICS:J = 0x10b00000be9L

.field public static final JAVA_TRACES:J = 0x10b000004b2L

.field public static final JOBSCHEDULER:J = 0x10b00000bccL

.field public static final KERNEL_LOGS:J = 0x10b00000454L

.field public static final KERNEL_VERSION:J = 0x109000003eaL

.field public static final KERNEL_WAKE_SOURCES:J = 0x10b000007d2L

.field public static final LAST_CRASH_LOGS:J = 0x10b00000459L

.field public static final LAST_EVENTS_LOGS:J = 0x10b00000457L

.field public static final LAST_KMSG:J = 0x10b000007d7L

.field public static final LAST_MAIN_LOGS:J = 0x10b00000455L

.field public static final LAST_RADIO_LOGS:J = 0x10b00000456L

.field public static final LAST_SECURITY_LOGS:J = 0x10b0000045bL

.field public static final LAST_STATS_LOGS:J = 0x10b0000045aL

.field public static final LAST_SYSTEM_LOGS:J = 0x10b00000458L

.field public static final MAIN_LOGS:J = 0x10b0000044dL

.field public static final MEMINFO:J = 0x10b00000bcaL

.field public static final METADATA:J = 0x10b00000002L

.field public static final NATIVE_TRACES:J = 0x10b000004b0L

.field public static final NETSTATS:J = 0x10b00000bb9L

.field public static final NFC_SERVICE:J = 0x10b00000becL

.field public static final NOTIFICATION:J = 0x10b00000bbcL

.field public static final PACKAGE:J = 0x10b00000bc0L

.field public static final PAGE_TYPE_INFO:J = 0x10b000007d1L

.field public static final PERSISTED_LOGS:J = 0x10b0000045cL

.field public static final POWER:J = 0x10b00000bc1L

.field public static final POWERSTATS_METER:J = 0x10b00000beeL

.field public static final POWERSTATS_MODEL:J = 0x10b00000befL

.field public static final POWERSTATS_RESIDENCY:J = 0x10b00000bf0L

.field public static final PRINT:J = 0x10b00000bc2L

.field public static final PROCESSES_AND_THREADS:J = 0x10b000007d5L

.field public static final PROCESS_CPU_USAGE:J = 0x10b00000be7L

.field public static final PROCRANK:J = 0x10b000007d0L

.field public static final PROCSTATS:J = 0x10b00000bc3L

.field public static final RADIO_LOGS:J = 0x10b0000044eL

.field public static final RESTRICTED_IMAGES:J = 0x10b00000bd1L

.field public static final ROLE:J = 0x10b00000bd0L

.field public static final SECURITY_LOGS:J = 0x10b00000453L

.field public static final SENSOR_SERVICE:J = 0x10b00000bedL

.field public static final SETTINGS:J = 0x10b00000bbaL

.field public static final STATS_DATA:J = 0x10b00000bcfL

.field public static final STATS_LOGS:J = 0x10b00000452L

.field public static final SYSTEM_LOGS:J = 0x10b00000450L

.field public static final SYSTEM_PROPERTIES:J = 0x10b000003e8L

.field public static final SYSTEM_TRACE:J = 0x10c00000bd2L

.field public static final TEXTDUMP_BLUETOOTH:J = 0x10b00000fa1L

.field public static final TEXTDUMP_WIFI:J = 0x10b00000fa0L

.field public static final USB:J = 0x10b00000bcdL

.field public static final WINDOW:J = 0x10b00000bc9L


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
