.class public interface abstract annotation Landroid/os/InputConfig;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final DEFAULT:I = 0x0

.field public static final DISABLE_USER_ACTIVITY:I = 0x800

.field public static final DROP_INPUT:I = 0x1000

.field public static final DROP_INPUT_IF_OBSCURED:I = 0x2000

.field public static final DUPLICATE_TOUCH_TO_WALLPAPER:I = 0x20

.field public static final INTERCEPTS_STYLUS:I = 0x8000

.field public static final IS_WALLPAPER:I = 0x40

.field public static final NOT_FOCUSABLE:I = 0x4

.field public static final NOT_TOUCHABLE:I = 0x8

.field public static final NOT_VISIBLE:I = 0x2

.field public static final NO_INPUT_CHANNEL:I = 0x1

.field public static final PAUSE_DISPATCHING:I = 0x80

.field public static final PREVENT_SPLITTING:I = 0x10

.field public static final SKIP_HANDWRITING_INJECT_MOTION_EVENT:I = -0x80000000

.field public static final SLIPPERY:I = 0x400

.field public static final SPY:I = 0x4000

.field public static final TRUSTED_OVERLAY:I = 0x100

.field public static final WATCH_OUTSIDE_TOUCH:I = 0x200
