.class public Landroid/os/perfdebug/ViewRootMonitor;
.super Ljava/lang/Object;


# static fields
.field public static final DRAW_MODE_HARDWARE:I = 0x1

.field public static final DRAW_MODE_SOFTWARE:I = 0x2

.field public static final DRAW_MODE_SURFACE:I = 0x3


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static newViewRootMonitor()Landroid/os/perfdebug/ViewRootMonitor;
    .locals 1

    const-class v0, Landroid/os/perfdebug/ViewRootMonitor;

    invoke-static {v0}, Lcom/miui/base/MiuiStubUtil;->newInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/perfdebug/ViewRootMonitor;

    return-object v0
.end method


# virtual methods
.method public monitorGlobalLayoutBegin()V
    .locals 0

    return-void
.end method

.method public monitorGlobalLayoutEnd()V
    .locals 0

    return-void
.end method

.method public monitorPerformDrawBegin()V
    .locals 0

    return-void
.end method

.method public monitorPerformDrawEnd()V
    .locals 0

    return-void
.end method

.method public monitorPerformLayoutBegin()V
    .locals 0

    return-void
.end method

.method public monitorPerformLayoutEnd()V
    .locals 0

    return-void
.end method

.method public monitorPerformMeasureBegin()V
    .locals 0

    return-void
.end method

.method public monitorPerformMeasureEnd()V
    .locals 0

    return-void
.end method

.method public monitorTraversalsBegin()V
    .locals 0

    return-void
.end method

.method public monitorTraversalsEnd()V
    .locals 0

    return-void
.end method

.method public monitorViewDrawBegin()V
    .locals 0

    return-void
.end method

.method public monitorViewDrawEnd()V
    .locals 0

    return-void
.end method

.method public monitorViewTreeDrawBegin()V
    .locals 0

    return-void
.end method

.method public monitorViewTreeDrawEnd()V
    .locals 0

    return-void
.end method

.method public monitorViewTreePreDrawBegin()V
    .locals 0

    return-void
.end method

.method public monitorViewTreePreDrawEnd()V
    .locals 0

    return-void
.end method

.method public onAppVisibilityChanged(Z)V
    .locals 0

    return-void
.end method

.method public onSurfaceCreate()V
    .locals 0

    return-void
.end method

.method public onSurfaceDestroy(Landroid/view/Surface;)V
    .locals 0

    return-void
.end method

.method public onWindowStoped(Landroid/view/ViewRootImpl;Z)V
    .locals 0

    return-void
.end method

.method public setDrawMode(I)V
    .locals 0

    return-void
.end method
