.class public Landroid/os/perfdebug/ViewMonitor;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static get()Landroid/os/perfdebug/ViewMonitor;
    .locals 1

    const-class v0, Landroid/os/perfdebug/ViewMonitor;

    invoke-static {v0}, Lcom/miui/base/MiuiStubUtil;->getImpl(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/perfdebug/ViewMonitor;

    return-object v0
.end method


# virtual methods
.method public markDrawBegin(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public markDrawEnd()V
    .locals 0

    return-void
.end method

.method public markLayoutBegin(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public markLayoutEnd()V
    .locals 0

    return-void
.end method

.method public markMeasureBegin(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public markMeasureEnd()V
    .locals 0

    return-void
.end method

.method public markOnDrawBegin(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public markOnDrawEnd()V
    .locals 0

    return-void
.end method
