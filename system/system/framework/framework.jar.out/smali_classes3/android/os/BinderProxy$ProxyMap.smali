.class final Landroid/os/BinderProxy$ProxyMap;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/BinderProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProxyMap"
.end annotation


# static fields
.field private static final CRASH_AT_SIZE:I = 0x61a8

.field private static final LOG_MAIN_INDEX_SIZE:I = 0x8

.field private static final MAIN_INDEX_MASK:I = 0xff

.field private static final MAIN_INDEX_SIZE:I = 0x100

.field static final MAX_NUM_INTERFACES_TO_DUMP:I = 0xa

.field private static final WARN_INCREMENT:I = 0xa


# instance fields
.field private final mMainIndexKeys:[[Ljava/lang/Long;

.field private final mMainIndexValues:[Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/ArrayList<",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/os/BinderProxy;",
            ">;>;"
        }
    .end annotation
.end field

.field private mProxyMapErrorDump:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mRandom:I

.field private mWarnBucketSize:I


# direct methods
.method static bridge synthetic -$$Nest$mdumpPerUidProxyCounts(Landroid/os/BinderProxy$ProxyMap;)V
    .locals 0

    invoke-direct {p0}, Landroid/os/BinderProxy$ProxyMap;->dumpPerUidProxyCounts()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdumpProxyInterfaceCounts(Landroid/os/BinderProxy$ProxyMap;)V
    .locals 0

    invoke-direct {p0}, Landroid/os/BinderProxy$ProxyMap;->dumpProxyInterfaceCounts()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetSortedInterfaceCounts(Landroid/os/BinderProxy$ProxyMap;I)[Landroid/os/BinderProxy$InterfaceCount;
    .locals 0

    invoke-direct {p0, p1}, Landroid/os/BinderProxy$ProxyMap;->getSortedInterfaceCounts(I)[Landroid/os/BinderProxy$InterfaceCount;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$msize(Landroid/os/BinderProxy$ProxyMap;)I
    .locals 0

    invoke-direct {p0}, Landroid/os/BinderProxy$ProxyMap;->size()I

    move-result p0

    return p0
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/os/BinderProxy$ProxyMap;->mProxyMapErrorDump:Ljava/util/ArrayList;

    const/16 v0, 0x14

    iput v0, p0, Landroid/os/BinderProxy$ProxyMap;->mWarnBucketSize:I

    const/16 v0, 0x100

    new-array v1, v0, [[Ljava/lang/Long;

    iput-object v1, p0, Landroid/os/BinderProxy$ProxyMap;->mMainIndexKeys:[[Ljava/lang/Long;

    new-array v0, v0, [Ljava/util/ArrayList;

    iput-object v0, p0, Landroid/os/BinderProxy$ProxyMap;->mMainIndexValues:[Ljava/util/ArrayList;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/BinderProxy$ProxyMap-IA;)V
    .locals 0

    invoke-direct {p0}, Landroid/os/BinderProxy$ProxyMap;-><init>()V

    return-void
.end method

.method private addProxyErrorLog(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Landroid/os/BinderProxy$ProxyMap;->mProxyMapErrorDump:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/os/BinderProxy$ProxyMap;->mProxyMapErrorDump:Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Landroid/os/BinderProxy$ProxyMap;->mProxyMapErrorDump:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private appendFile()V
    .locals 5

    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/data/anr/BinderProxy_error_Pid_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_Tid_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".txt"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    new-instance v1, Ljava/io/FileWriter;

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v2, p0, Landroid/os/BinderProxy$ProxyMap;->mProxyMapErrorDump:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    const/16 v4, 0xa

    invoke-virtual {v1, v4}, Ljava/io/FileWriter;->write(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_0
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    :catchall_0
    move-exception v2

    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v3

    :try_start_4
    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    :goto_1
    throw v2
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    :catch_0
    move-exception v1

    const-string v2, "Binder"

    const-string v3, "Exception while writing BinderProxy error to new file!"

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    return-void
.end method

.method private dumpPerUidProxyCounts()V
    .locals 8

    invoke-static {}, Lcom/android/internal/os/BinderInternal;->nGetBinderProxyPerUidCounts()Landroid/util/SparseIntArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    const-string v1, "Binder"

    const-string v2, "Per Uid Binder Proxy Counts:"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v2}, Landroid/os/BinderProxy$ProxyMap;->addProxyErrorLog(Ljava/lang/String;)V

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    invoke-virtual {v0, v2}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v3

    invoke-virtual {v0, v2}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "UID : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "  count = "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Landroid/os/BinderProxy$ProxyMap;->addProxyErrorLog(Ljava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private dumpProxyInterfaceCounts()V
    .locals 7

    const/16 v0, 0xa

    invoke-direct {p0, v0}, Landroid/os/BinderProxy$ProxyMap;->getSortedInterfaceCounts(I)[Landroid/os/BinderProxy$InterfaceCount;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BinderProxy descriptor histogram (top "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "):"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v5, "Binder"

    invoke-static {v5, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/BinderProxy$ProxyMap;->addProxyErrorLog(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " #"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ": "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v6, v1, v0

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Landroid/os/BinderProxy$ProxyMap;->addProxyErrorLog(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private getSortedInterfaceCounts(I)[Landroid/os/BinderProxy$InterfaceCount;
    .locals 10

    if-ltz p1, :cond_4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Landroid/os/BinderProxy;->-$$Nest$sfgetsProxyMap()Landroid/os/BinderProxy$ProxyMap;

    move-result-object v2

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Landroid/os/BinderProxy$ProxyMap;->mMainIndexValues:[Ljava/util/ArrayList;

    array-length v4, v3

    const/4 v5, 0x0

    move v6, v5

    :goto_0
    if-ge v6, v4, :cond_1

    aget-object v7, v3, v6

    if-eqz v7, :cond_0

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v2

    invoke-interface {v2, v5}, Landroid/app/IActivityManager;->enableAppFreezer(Z)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    const-string v3, "Binder"

    const-string v4, "RemoteException while disabling app freezer"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    new-instance v3, Landroid/os/BinderProxy$ProxyMap$$ExternalSyntheticLambda0;

    invoke-direct {v3, v1, v0}, Landroid/os/BinderProxy$ProxyMap$$ExternalSyntheticLambda0;-><init>(Ljava/util/ArrayList;Ljava/util/Map;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    :try_start_2
    invoke-interface {v2}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    const-wide/16 v3, 0x14

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3, v4, v5}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v4, "Binder"

    const-string v5, "Failed to complete binder proxy dump, dumping what we have so far."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_2
    goto :goto_2

    :catch_1
    move-exception v3

    :goto_2
    :try_start_3
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Landroid/app/IActivityManager;->enableAppFreezer(Z)Z
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_3

    :catch_2
    move-exception v3

    const-string v4, "Binder"

    const-string v5, "RemoteException while re-enabling app freezer"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v4

    new-array v4, v4, [Ljava/util/Map$Entry;

    invoke-interface {v3, v4}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/util/Map$Entry;

    new-instance v4, Landroid/os/BinderProxy$ProxyMap$$ExternalSyntheticLambda1;

    invoke-direct {v4}, Landroid/os/BinderProxy$ProxyMap$$ExternalSyntheticLambda1;-><init>()V

    invoke-static {v3, v4}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    array-length v4, v3

    invoke-static {p1, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    new-array v5, v4, [Landroid/os/BinderProxy$InterfaceCount;

    const/4 v6, 0x0

    :goto_4
    if-ge v6, v4, :cond_3

    new-instance v7, Landroid/os/BinderProxy$InterfaceCount;

    aget-object v8, v3, v6

    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    aget-object v9, v3, v6

    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-direct {v7, v8, v9}, Landroid/os/BinderProxy$InterfaceCount;-><init>(Ljava/lang/String;I)V

    aput-object v7, v5, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :cond_3
    return-object v5

    :catchall_0
    move-exception v3

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v3

    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "negative interface count"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static hash(J)I
    .locals 4

    const/4 v0, 0x2

    shr-long v0, p0, v0

    const/16 v2, 0xa

    shr-long v2, p0, v2

    xor-long/2addr v0, v2

    long-to-int v0, v0

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method static synthetic lambda$getSortedInterfaceCounts$0(Ljava/util/ArrayList;Ljava/util/Map;)V
    .locals 7

    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/BinderProxy;

    if-nez v2, :cond_0

    const-string v3, "<cleared weak-ref>"

    goto :goto_1

    :cond_0
    :try_start_0
    invoke-virtual {v2}, Landroid/os/BinderProxy;->getInterfaceDescriptor()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    invoke-virtual {v2}, Landroid/os/BinderProxy;->isBinderAlive()Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "<proxy to dead node>"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v3, v4

    :cond_2
    goto :goto_1

    :catchall_0
    move-exception v3

    const-string v4, "<exception during getDescriptor>"

    move-object v3, v4

    :goto_1
    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    const/4 v5, 0x1

    if-nez v4, :cond_3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {p1, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_3
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v6

    add-int/2addr v6, v5

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {p1, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_2
    goto :goto_0

    :cond_4
    return-void
.end method

.method static synthetic lambda$getSortedInterfaceCounts$1(Ljava/util/Map$Entry;Ljava/util/Map$Entry;)I
    .locals 2

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-interface {p0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result v0

    return v0
.end method

.method private remove(II)V
    .locals 4

    iget-object v0, p0, Landroid/os/BinderProxy$ProxyMap;->mMainIndexKeys:[[Ljava/lang/Long;

    aget-object v0, v0, p1

    iget-object v1, p0, Landroid/os/BinderProxy$ProxyMap;->mMainIndexValues:[Ljava/util/ArrayList;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v3, v2, -0x1

    if-eq p2, v3, :cond_0

    add-int/lit8 v3, v2, -0x1

    aget-object v3, v0, v3

    aput-object v3, v0, p2

    add-int/lit8 v3, v2, -0x1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1, p2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v3, v2, -0x1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    return-void
.end method

.method private size()I
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/os/BinderProxy$ProxyMap;->mMainIndexValues:[Ljava/util/ArrayList;

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/2addr v0, v5

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method private unclearedSize()I
    .locals 8

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/os/BinderProxy$ProxyMap;->mMainIndexValues:[Ljava/util/ArrayList;

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v1, v3

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/ref/WeakReference;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/lang/ref/WeakReference;->refersTo(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    goto :goto_1

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    return v0
.end method


# virtual methods
.method get(J)Landroid/os/BinderProxy;
    .locals 10

    goto/32 :goto_7

    nop

    :goto_0
    if-nez v9, :cond_0

    goto/32 :goto_17

    :cond_0
    goto/32 :goto_16

    nop

    :goto_1
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    goto/32 :goto_b

    nop

    :goto_2
    iget-object v1, p0, Landroid/os/BinderProxy$ProxyMap;->mMainIndexKeys:[[Ljava/lang/Long;

    goto/32 :goto_f

    nop

    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_d

    nop

    :goto_4
    return-object v2

    :goto_5
    return-object v2

    :goto_6
    goto/32 :goto_3

    nop

    :goto_7
    invoke-static {p1, p2}, Landroid/os/BinderProxy$ProxyMap;->hash(J)I

    move-result v0

    goto/32 :goto_2

    nop

    :goto_8
    const/4 v5, 0x0

    :goto_9
    goto/32 :goto_a

    nop

    :goto_a
    if-lt v5, v4, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_15

    nop

    :goto_b
    check-cast v8, Ljava/lang/ref/WeakReference;

    goto/32 :goto_18

    nop

    :goto_c
    check-cast v9, Landroid/os/BinderProxy;

    goto/32 :goto_0

    nop

    :goto_d
    goto :goto_9

    :goto_e
    goto/32 :goto_4

    nop

    :goto_f
    aget-object v1, v1, v0

    goto/32 :goto_1c

    nop

    :goto_10
    return-object v2

    :goto_11
    goto/32 :goto_14

    nop

    :goto_12
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    goto/32 :goto_8

    nop

    :goto_13
    if-eqz v1, :cond_2

    goto/32 :goto_11

    :cond_2
    goto/32 :goto_10

    nop

    :goto_14
    iget-object v3, p0, Landroid/os/BinderProxy$ProxyMap;->mMainIndexValues:[Ljava/util/ArrayList;

    goto/32 :goto_19

    nop

    :goto_15
    aget-object v6, v1, v5

    goto/32 :goto_1b

    nop

    :goto_16
    return-object v9

    :goto_17
    goto/32 :goto_1a

    nop

    :goto_18
    invoke-virtual {v8}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v9

    goto/32 :goto_c

    nop

    :goto_19
    aget-object v3, v3, v0

    goto/32 :goto_12

    nop

    :goto_1a
    invoke-direct {p0, v0, v5}, Landroid/os/BinderProxy$ProxyMap;->remove(II)V

    goto/32 :goto_5

    nop

    :goto_1b
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    goto/32 :goto_1d

    nop

    :goto_1c
    const/4 v2, 0x0

    goto/32 :goto_13

    nop

    :goto_1d
    cmp-long v8, p1, v6

    goto/32 :goto_1e

    nop

    :goto_1e
    if-eqz v8, :cond_3

    goto/32 :goto_6

    :cond_3
    goto/32 :goto_1

    nop
.end method

.method set(JLandroid/os/BinderProxy;)V
    .locals 10

    goto/32 :goto_66

    nop

    :goto_0
    if-lt v7, v3, :cond_0

    goto/32 :goto_15

    :cond_0
    goto/32 :goto_47

    nop

    :goto_1
    const-string v9, " (uncleared), "

    goto/32 :goto_42

    nop

    :goto_2
    add-int/2addr v8, v3

    goto/32 :goto_32

    nop

    :goto_3
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_20

    nop

    :goto_4
    const-string v9, "BinderProxy map has many cleared entries: "

    goto/32 :goto_58

    nop

    :goto_5
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    goto/32 :goto_80

    nop

    :goto_6
    iget v8, p0, Landroid/os/BinderProxy$ProxyMap;->mRandom:I

    goto/32 :goto_17

    nop

    :goto_7
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_44

    nop

    :goto_8
    if-gt v6, v3, :cond_1

    goto/32 :goto_11

    :cond_1
    goto/32 :goto_1a

    nop

    :goto_9
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_7a

    nop

    :goto_a
    add-int/lit8 v6, v6, 0x2

    goto/32 :goto_4b

    nop

    :goto_b
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    goto/32 :goto_12

    nop

    :goto_c
    div-int/lit8 v6, v1, 0x2

    goto/32 :goto_7b

    nop

    :goto_d
    const-string v8, " total = "

    goto/32 :goto_50

    nop

    :goto_e
    invoke-direct {p0}, Landroid/os/BinderProxy$ProxyMap;->appendFile()V

    goto/32 :goto_43

    nop

    :goto_f
    invoke-static {}, Landroid/app/ActivityThread;->isSystem()Z

    move-result v7

    goto/32 :goto_59

    nop

    :goto_10
    throw v3

    :goto_11
    goto/32 :goto_6e

    nop

    :goto_12
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto/32 :goto_71

    nop

    :goto_13
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    goto/32 :goto_8d

    nop

    :goto_14
    goto :goto_11

    :goto_15
    goto/32 :goto_89

    nop

    :goto_16
    invoke-virtual {v2, v1, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto/32 :goto_5a

    nop

    :goto_17
    add-int/2addr v8, v3

    goto/32 :goto_53

    nop

    :goto_18
    if-nez v6, :cond_2

    goto/32 :goto_3d

    :cond_2
    goto/32 :goto_3e

    nop

    :goto_19
    add-int/2addr v7, v3

    goto/32 :goto_85

    nop

    :goto_1a
    new-instance v3, Ljava/lang/StringBuilder;

    goto/32 :goto_88

    nop

    :goto_1b
    goto/16 :goto_70

    :goto_1c
    goto/32 :goto_16

    nop

    :goto_1d
    iget-object v1, p0, Landroid/os/BinderProxy$ProxyMap;->mMainIndexKeys:[[Ljava/lang/Long;

    goto/32 :goto_75

    nop

    :goto_1e
    add-int/lit8 v8, v5, 0x1

    goto/32 :goto_2

    nop

    :goto_1f
    iget v7, p0, Landroid/os/BinderProxy$ProxyMap;->mWarnBucketSize:I

    goto/32 :goto_5c

    nop

    :goto_20
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_54

    nop

    :goto_21
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    goto/32 :goto_24

    nop

    :goto_22
    new-instance v8, Ljava/lang/StringBuilder;

    goto/32 :goto_3b

    nop

    :goto_23
    div-int/lit8 v3, v3, 0x2

    goto/32 :goto_8

    nop

    :goto_24
    aput-object v6, v5, v1

    :goto_25
    goto/32 :goto_29

    nop

    :goto_26
    invoke-virtual {v7, v3}, Landroid/os/BinderStub;->stopTracking(Z)V

    :goto_27
    goto/32 :goto_55

    nop

    :goto_28
    new-instance v4, Ljava/util/ArrayList;

    goto/32 :goto_63

    nop

    :goto_29
    iget v6, p0, Landroid/os/BinderProxy$ProxyMap;->mWarnBucketSize:I

    goto/32 :goto_62

    nop

    :goto_2a
    if-eqz v2, :cond_3

    goto/32 :goto_38

    :cond_3
    goto/32 :goto_28

    nop

    :goto_2b
    invoke-static {v8, v3}, Ljava/lang/Math;->floorMod(II)I

    move-result v3

    goto/32 :goto_1e

    nop

    :goto_2c
    const/16 v3, 0x61a8

    goto/32 :goto_4d

    nop

    :goto_2d
    if-eq v6, v1, :cond_4

    goto/32 :goto_52

    :cond_4
    goto/32 :goto_c

    nop

    :goto_2e
    iget-object v6, p0, Landroid/os/BinderProxy$ProxyMap;->mMainIndexKeys:[[Ljava/lang/Long;

    goto/32 :goto_84

    nop

    :goto_2f
    const/4 v7, 0x0

    goto/32 :goto_3a

    nop

    :goto_30
    invoke-virtual {v3}, Ljava/lang/Runtime;->gc()V

    goto/32 :goto_90

    nop

    :goto_31
    invoke-static {v5, v7, v6, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto/32 :goto_7c

    nop

    :goto_32
    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    goto/32 :goto_69

    nop

    :goto_33
    aput-object v4, v1, v0

    goto/32 :goto_8b

    nop

    :goto_34
    aput-object v6, v7, v0

    goto/32 :goto_51

    nop

    :goto_35
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_1b

    nop

    :goto_36
    add-int/lit8 v7, v5, 0x1

    goto/32 :goto_19

    nop

    :goto_37
    aput-object v4, v1, v0

    :goto_38
    goto/32 :goto_6d

    nop

    :goto_39
    iget-object v7, p0, Landroid/os/BinderProxy$ProxyMap;->mMainIndexKeys:[[Ljava/lang/Long;

    goto/32 :goto_34

    nop

    :goto_3a
    invoke-virtual {v6, v7}, Ljava/lang/ref/WeakReference;->refersTo(Ljava/lang/Object;)Z

    move-result v6

    goto/32 :goto_18

    nop

    :goto_3b
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_8e

    nop

    :goto_3c
    return-void

    :goto_3d
    goto/32 :goto_35

    nop

    :goto_3e
    invoke-virtual {v2, v5, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_2e

    nop

    :goto_3f
    invoke-static {}, Landroid/os/BinderStub;->get()Landroid/os/BinderStub;

    move-result-object v7

    goto/32 :goto_26

    nop

    :goto_40
    check-cast v6, Ljava/lang/ref/WeakReference;

    goto/32 :goto_2f

    nop

    :goto_41
    array-length v6, v5

    goto/32 :goto_2d

    nop

    :goto_42
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    goto/32 :goto_4e

    nop

    :goto_43
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v3

    goto/32 :goto_30

    nop

    :goto_44
    const-string v8, "BinderProxy map growth! bucket size = "

    goto/32 :goto_6b

    nop

    :goto_45
    const/4 v7, 0x0

    goto/32 :goto_31

    nop

    :goto_46
    if-lt v5, v1, :cond_5

    goto/32 :goto_1c

    :cond_5
    goto/32 :goto_68

    nop

    :goto_47
    mul-int/lit8 v3, v7, 0x3

    goto/32 :goto_23

    nop

    :goto_48
    if-nez v7, :cond_6

    goto/32 :goto_86

    :cond_6
    goto/32 :goto_36

    nop

    :goto_49
    add-int/lit8 v8, v1, -0x1

    goto/32 :goto_6a

    nop

    :goto_4a
    const/4 v3, 0x1

    goto/32 :goto_2a

    nop

    :goto_4b
    new-array v6, v6, [Ljava/lang/Long;

    goto/32 :goto_45

    nop

    :goto_4c
    invoke-direct {v3, v8}, Landroid/os/BinderProxy$BinderProxyMapSizeException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_10

    nop

    :goto_4d
    if-ge v6, v3, :cond_7

    goto/32 :goto_11

    :cond_7
    goto/32 :goto_61

    nop

    :goto_4e
    invoke-direct {p0}, Landroid/os/BinderProxy$ProxyMap;->unclearedSize()I

    move-result v9

    goto/32 :goto_78

    nop

    :goto_4f
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    goto/32 :goto_1

    nop

    :goto_50
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    goto/32 :goto_b

    nop

    :goto_51
    goto/16 :goto_25

    :goto_52
    goto/32 :goto_21

    nop

    :goto_53
    iput v8, p0, Landroid/os/BinderProxy$ProxyMap;->mRandom:I

    goto/32 :goto_64

    nop

    :goto_54
    const-string v9, " are cleared"

    goto/32 :goto_9

    nop

    :goto_55
    sget-boolean v3, Landroid/os/Build;->IS_DEBUGGABLE:Z

    goto/32 :goto_57

    nop

    :goto_56
    invoke-virtual {v8, v7}, Ljava/lang/ref/WeakReference;->refersTo(Ljava/lang/Object;)Z

    move-result v7

    goto/32 :goto_48

    nop

    :goto_57
    if-nez v3, :cond_8

    goto/32 :goto_11

    :cond_8
    goto/32 :goto_2c

    nop

    :goto_58
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_60

    nop

    :goto_59
    if-nez v7, :cond_9

    goto/32 :goto_27

    :cond_9
    goto/32 :goto_3f

    nop

    :goto_5a
    iget-object v5, p0, Landroid/os/BinderProxy$ProxyMap;->mMainIndexKeys:[[Ljava/lang/Long;

    goto/32 :goto_73

    nop

    :goto_5b
    invoke-direct {p0}, Landroid/os/BinderProxy$ProxyMap;->size()I

    move-result v6

    goto/32 :goto_6c

    nop

    :goto_5c
    add-int/lit8 v7, v7, 0xa

    goto/32 :goto_5d

    nop

    :goto_5d
    iput v7, p0, Landroid/os/BinderProxy$ProxyMap;->mWarnBucketSize:I

    goto/32 :goto_81

    nop

    :goto_5e
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    goto/32 :goto_d

    nop

    :goto_5f
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_77

    nop

    :goto_60
    sub-int v9, v6, v7

    goto/32 :goto_5f

    nop

    :goto_61
    invoke-direct {p0}, Landroid/os/BinderProxy$ProxyMap;->unclearedSize()I

    move-result v7

    goto/32 :goto_0

    nop

    :goto_62
    if-ge v1, v6, :cond_a

    goto/32 :goto_11

    :cond_a
    goto/32 :goto_5b

    nop

    :goto_63
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_33

    nop

    :goto_64
    add-int/lit8 v3, v5, 0x1

    goto/32 :goto_79

    nop

    :goto_65
    invoke-direct {p0}, Landroid/os/BinderProxy$ProxyMap;->dumpPerUidProxyCounts()V

    goto/32 :goto_e

    nop

    :goto_66
    invoke-static {p1, p2}, Landroid/os/BinderProxy$ProxyMap;->hash(J)I

    move-result v0

    goto/32 :goto_8c

    nop

    :goto_67
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    goto/32 :goto_4f

    nop

    :goto_68
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    goto/32 :goto_40

    nop

    :goto_69
    check-cast v8, Ljava/lang/ref/WeakReference;

    goto/32 :goto_56

    nop

    :goto_6a
    if-lt v5, v8, :cond_b

    goto/32 :goto_86

    :cond_b
    goto/32 :goto_6

    nop

    :goto_6b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    goto/32 :goto_5e

    nop

    :goto_6c
    new-instance v7, Ljava/lang/StringBuilder;

    goto/32 :goto_7

    nop

    :goto_6d
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_8f

    nop

    :goto_6e
    return-void

    :goto_6f
    const/4 v5, 0x0

    :goto_70
    goto/32 :goto_46

    nop

    :goto_71
    const-string v8, "Binder"

    goto/32 :goto_7f

    nop

    :goto_72
    invoke-static {v8, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_14

    nop

    :goto_73
    aget-object v5, v5, v0

    goto/32 :goto_41

    nop

    :goto_74
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    goto/32 :goto_83

    nop

    :goto_75
    new-array v4, v3, [Ljava/lang/Long;

    goto/32 :goto_37

    nop

    :goto_76
    if-ge v1, v7, :cond_c

    goto/32 :goto_27

    :cond_c
    goto/32 :goto_f

    nop

    :goto_77
    const-string v9, " of "

    goto/32 :goto_3

    nop

    :goto_78
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    goto/32 :goto_87

    nop

    :goto_79
    sub-int v3, v1, v3

    goto/32 :goto_2b

    nop

    :goto_7a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_72

    nop

    :goto_7b
    add-int/2addr v6, v1

    goto/32 :goto_a

    nop

    :goto_7c
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    goto/32 :goto_8a

    nop

    :goto_7d
    aget-object v2, v1, v0

    goto/32 :goto_4a

    nop

    :goto_7e
    invoke-direct {v4, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    goto/32 :goto_6f

    nop

    :goto_7f
    invoke-static {v8, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_1f

    nop

    :goto_80
    const-string v9, " (total), "

    goto/32 :goto_67

    nop

    :goto_81
    const/16 v7, 0x59d8

    goto/32 :goto_76

    nop

    :goto_82
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    goto/32 :goto_5

    nop

    :goto_83
    aput-object v8, v6, v5

    goto/32 :goto_49

    nop

    :goto_84
    aget-object v6, v6, v0

    goto/32 :goto_74

    nop

    :goto_85
    invoke-direct {p0, v0, v7}, Landroid/os/BinderProxy$ProxyMap;->remove(II)V

    :goto_86
    goto/32 :goto_3c

    nop

    :goto_87
    const-string v9, " (uncleared after GC). BinderProxy leak?"

    goto/32 :goto_13

    nop

    :goto_88
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_4

    nop

    :goto_89
    invoke-direct {p0}, Landroid/os/BinderProxy$ProxyMap;->dumpProxyInterfaceCounts()V

    goto/32 :goto_65

    nop

    :goto_8a
    aput-object v7, v6, v1

    goto/32 :goto_39

    nop

    :goto_8b
    move-object v2, v4

    goto/32 :goto_1d

    nop

    :goto_8c
    iget-object v1, p0, Landroid/os/BinderProxy$ProxyMap;->mMainIndexValues:[Ljava/util/ArrayList;

    goto/32 :goto_7d

    nop

    :goto_8d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto/32 :goto_4c

    nop

    :goto_8e
    const-string v9, "Binder ProxyMap has too many entries: "

    goto/32 :goto_82

    nop

    :goto_8f
    new-instance v4, Ljava/lang/ref/WeakReference;

    goto/32 :goto_7e

    nop

    :goto_90
    new-instance v3, Landroid/os/BinderProxy$BinderProxyMapSizeException;

    goto/32 :goto_22

    nop
.end method
