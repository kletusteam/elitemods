.class public interface abstract annotation Landroid/os/StatsBootstrapAtomValue$Tag;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/StatsBootstrapAtomValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2609
    name = "Tag"
.end annotation


# static fields
.field public static final boolValue:I = 0x0

.field public static final bytesValue:I = 0x5

.field public static final floatValue:I = 0x3

.field public static final intValue:I = 0x1

.field public static final longValue:I = 0x2

.field public static final stringValue:I = 0x4
