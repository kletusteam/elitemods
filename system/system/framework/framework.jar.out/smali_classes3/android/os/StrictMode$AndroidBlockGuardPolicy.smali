.class Landroid/os/StrictMode$AndroidBlockGuardPolicy;
.super Ljava/lang/Object;

# interfaces
.implements Ldalvik/system/BlockGuard$Policy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/StrictMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AndroidBlockGuardPolicy"
.end annotation


# instance fields
.field private mLastViolationTime:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mRealLastViolationTime:Landroid/util/SparseLongArray;

.field private mThreadPolicyMask:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mThreadPolicyMask:I

    return-void
.end method

.method static synthetic lambda$onThreadPolicyViolation$1(Landroid/os/StrictMode$OnThreadViolationListener;Landroid/os/strictmode/Violation;)V
    .locals 2

    invoke-static {}, Landroid/os/StrictMode;->allowThreadViolations()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    :try_start_0
    invoke-interface {p0, p1}, Landroid/os/StrictMode$OnThreadViolationListener;->onThreadViolation(Landroid/os/strictmode/Violation;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    nop

    return-void

    :catchall_0
    move-exception v1

    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    throw v1
.end method


# virtual methods
.method public getPolicyMask()I
    .locals 1

    iget v0, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mThreadPolicyMask:I

    return v0
.end method

.method public getThreadPolicyMask()I
    .locals 1

    iget v0, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mThreadPolicyMask:I

    return v0
.end method

.method handleViolationWithTimingAttempt(Landroid/os/StrictMode$ViolationInfo;)V
    .locals 5

    goto/32 :goto_13

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_7

    nop

    :goto_2
    const/16 v3, 0xa

    goto/32 :goto_3

    nop

    :goto_3
    if-ge v2, v3, :cond_0

    goto/32 :goto_1f

    :cond_0
    goto/32 :goto_1e

    nop

    :goto_4
    return-void

    :goto_5
    goto/32 :goto_2d

    nop

    :goto_6
    invoke-virtual {v2}, Landroid/util/Singleton;->get()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_e

    nop

    :goto_7
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    goto/32 :goto_19

    nop

    :goto_8
    invoke-static {}, Landroid/os/StrictMode;->-$$Nest$sfgetsWindowManager()Landroid/util/Singleton;

    move-result-object v2

    goto/32 :goto_6

    nop

    :goto_9
    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_2a

    nop

    :goto_a
    if-gt v2, v3, :cond_1

    goto/32 :goto_22

    :cond_1
    goto/32 :goto_21

    nop

    :goto_b
    if-nez v2, :cond_2

    goto/32 :goto_24

    :cond_2
    goto/32 :goto_8

    nop

    :goto_c
    const/high16 v2, 0x10000000

    goto/32 :goto_d

    nop

    :goto_d
    if-eq v1, v2, :cond_3

    goto/32 :goto_2c

    :cond_3
    goto/32 :goto_2b

    nop

    :goto_e
    check-cast v2, Landroid/view/IWindowManager;

    goto/32 :goto_23

    nop

    :goto_f
    invoke-static {p1}, Landroid/os/StrictMode$ViolationInfo;->-$$Nest$fgetmPenaltyMask(Landroid/os/StrictMode$ViolationInfo;)I

    move-result v1

    goto/32 :goto_c

    nop

    :goto_10
    if-nez v2, :cond_4

    goto/32 :goto_18

    :cond_4
    :try_start_0
    invoke-interface {v2, v3}, Landroid/view/IWindowManager;->showStrictModeViolation(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_17

    nop

    :goto_11
    invoke-virtual {p1, v2}, Landroid/os/StrictMode$ViolationInfo;->penaltyEnabled(I)Z

    move-result v2

    goto/32 :goto_b

    nop

    :goto_12
    const/high16 v2, 0x8000000

    goto/32 :goto_11

    nop

    :goto_13
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    goto/32 :goto_26

    nop

    :goto_14
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    goto/32 :goto_2

    nop

    :goto_15
    iput v1, p1, Landroid/os/StrictMode$ViolationInfo;->durationMillis:I

    goto/32 :goto_28

    nop

    :goto_16
    invoke-direct {v4, p0, v2, v1}, Landroid/os/StrictMode$AndroidBlockGuardPolicy$$ExternalSyntheticLambda1;-><init>(Landroid/os/StrictMode$AndroidBlockGuardPolicy;Landroid/view/IWindowManager;Ljava/util/ArrayList;)V

    goto/32 :goto_29

    nop

    :goto_17
    goto :goto_18

    :catch_0
    move-exception v3

    :goto_18
    goto/32 :goto_1d

    nop

    :goto_19
    const/4 v3, 0x1

    goto/32 :goto_a

    nop

    :goto_1a
    const/4 v2, 0x0

    :goto_1b
    goto/32 :goto_10

    nop

    :goto_1c
    invoke-static {}, Landroid/os/StrictMode;->-$$Nest$sfgetviolationsBeingTimed()Ljava/lang/ThreadLocal;

    move-result-object v1

    goto/32 :goto_20

    nop

    :goto_1d
    invoke-static {}, Landroid/os/StrictMode;->-$$Nest$sfgetTHREAD_HANDLER()Ljava/lang/ThreadLocal;

    move-result-object v3

    goto/32 :goto_9

    nop

    :goto_1e
    return-void

    :goto_1f
    goto/32 :goto_1

    nop

    :goto_20
    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_25

    nop

    :goto_21
    return-void

    :goto_22
    goto/32 :goto_12

    nop

    :goto_23
    goto :goto_1b

    :goto_24
    goto/32 :goto_1a

    nop

    :goto_25
    check-cast v1, Ljava/util/ArrayList;

    goto/32 :goto_14

    nop

    :goto_26
    if-nez v0, :cond_5

    goto/32 :goto_5

    :cond_5
    goto/32 :goto_f

    nop

    :goto_27
    new-instance v4, Landroid/os/StrictMode$AndroidBlockGuardPolicy$$ExternalSyntheticLambda1;

    goto/32 :goto_16

    nop

    :goto_28
    invoke-virtual {p0, p1}, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->onThreadPolicyViolation(Landroid/os/StrictMode$ViolationInfo;)V

    goto/32 :goto_0

    nop

    :goto_29
    invoke-virtual {v3, v4}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    goto/32 :goto_4

    nop

    :goto_2a
    check-cast v3, Landroid/os/Handler;

    goto/32 :goto_27

    nop

    :goto_2b
    goto/16 :goto_5

    :goto_2c
    goto/32 :goto_1c

    nop

    :goto_2d
    const/4 v1, -0x1

    goto/32 :goto_15

    nop
.end method

.method synthetic lambda$handleViolationWithTimingAttempt$0$android-os-StrictMode$AndroidBlockGuardPolicy(Landroid/view/IWindowManager;Ljava/util/ArrayList;)V
    .locals 6

    goto/32 :goto_a

    nop

    :goto_0
    invoke-virtual {p0, v3}, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->onThreadPolicyViolation(Landroid/os/StrictMode$ViolationInfo;)V

    goto/32 :goto_7

    nop

    :goto_1
    goto :goto_2

    :catch_0
    move-exception v2

    :goto_2
    goto/32 :goto_12

    nop

    :goto_3
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_14

    nop

    :goto_4
    iput v4, v3, Landroid/os/StrictMode$ViolationInfo;->violationNumThisLoop:I

    goto/32 :goto_6

    nop

    :goto_5
    sub-long v4, v0, v4

    goto/32 :goto_e

    nop

    :goto_6
    iget-wide v4, v3, Landroid/os/StrictMode$ViolationInfo;->violationUptimeMillis:J

    goto/32 :goto_5

    nop

    :goto_7
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_10

    nop

    :goto_8
    const/4 v2, 0x0

    :try_start_0
    invoke-interface {p1, v2}, Landroid/view/IWindowManager;->showStrictModeViolation(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_1

    nop

    :goto_9
    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    goto/32 :goto_c

    nop

    :goto_a
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    goto/32 :goto_b

    nop

    :goto_b
    if-nez p1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_8

    nop

    :goto_c
    return-void

    :goto_d
    if-lt v2, v3, :cond_1

    goto/32 :goto_11

    :cond_1
    goto/32 :goto_3

    nop

    :goto_e
    long-to-int v4, v4

    goto/32 :goto_16

    nop

    :goto_f
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    goto/32 :goto_d

    nop

    :goto_10
    goto :goto_13

    :goto_11
    goto/32 :goto_9

    nop

    :goto_12
    const/4 v2, 0x0

    :goto_13
    goto/32 :goto_f

    nop

    :goto_14
    check-cast v3, Landroid/os/StrictMode$ViolationInfo;

    goto/32 :goto_15

    nop

    :goto_15
    add-int/lit8 v4, v2, 0x1

    goto/32 :goto_4

    nop

    :goto_16
    iput v4, v3, Landroid/os/StrictMode$ViolationInfo;->durationMillis:I

    goto/32 :goto_0

    nop
.end method

.method onCustomSlowCall(Ljava/lang/String;)V
    .locals 1

    goto/32 :goto_6

    nop

    :goto_0
    if-eqz v0, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_9

    nop

    :goto_1
    new-instance v0, Landroid/os/strictmode/CustomViolation;

    goto/32 :goto_b

    nop

    :goto_2
    invoke-static {}, Landroid/os/StrictMode;->-$$Nest$smtooManyViolationsThisLoop()Z

    move-result v0

    goto/32 :goto_4

    nop

    :goto_3
    and-int/lit8 v0, v0, 0x8

    goto/32 :goto_0

    nop

    :goto_4
    if-nez v0, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_7

    nop

    :goto_5
    return-void

    :goto_6
    iget v0, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mThreadPolicyMask:I

    goto/32 :goto_3

    nop

    :goto_7
    return-void

    :goto_8
    goto/32 :goto_1

    nop

    :goto_9
    return-void

    :goto_a
    goto/32 :goto_2

    nop

    :goto_b
    invoke-direct {v0, p1}, Landroid/os/strictmode/CustomViolation;-><init>(Ljava/lang/String;)V

    goto/32 :goto_c

    nop

    :goto_c
    invoke-virtual {p0, v0}, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->startHandlingViolationException(Landroid/os/strictmode/Violation;)V

    goto/32 :goto_5

    nop
.end method

.method public onExplicitGc()V
    .locals 1

    iget v0, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mThreadPolicyMask:I

    and-int/lit8 v0, v0, 0x40

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Landroid/os/StrictMode;->-$$Nest$smtooManyViolationsThisLoop()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_1
    new-instance v0, Landroid/os/strictmode/ExplicitGcViolation;

    invoke-direct {v0}, Landroid/os/strictmode/ExplicitGcViolation;-><init>()V

    invoke-virtual {p0, v0}, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->startHandlingViolationException(Landroid/os/strictmode/Violation;)V

    return-void
.end method

.method public onNetwork()V
    .locals 2

    iget v0, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mThreadPolicyMask:I

    and-int/lit8 v1, v0, 0x4

    if-nez v1, :cond_0

    return-void

    :cond_0
    const/high16 v1, 0x2000000

    and-int/2addr v0, v1

    if-nez v0, :cond_2

    invoke-static {}, Landroid/os/StrictMode;->-$$Nest$smtooManyViolationsThisLoop()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_1
    new-instance v0, Landroid/os/strictmode/NetworkViolation;

    invoke-direct {v0}, Landroid/os/strictmode/NetworkViolation;-><init>()V

    invoke-virtual {p0, v0}, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->startHandlingViolationException(Landroid/os/strictmode/Violation;)V

    return-void

    :cond_2
    new-instance v0, Landroid/os/NetworkOnMainThreadException;

    invoke-direct {v0}, Landroid/os/NetworkOnMainThreadException;-><init>()V

    throw v0
.end method

.method public onReadFromDisk()V
    .locals 1

    iget v0, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mThreadPolicyMask:I

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Landroid/os/StrictMode;->-$$Nest$smtooManyViolationsThisLoop()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_1
    new-instance v0, Landroid/os/strictmode/DiskReadViolation;

    invoke-direct {v0}, Landroid/os/strictmode/DiskReadViolation;-><init>()V

    invoke-virtual {p0, v0}, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->startHandlingViolationException(Landroid/os/strictmode/Violation;)V

    return-void
.end method

.method onResourceMismatch(Ljava/lang/Object;)V
    .locals 1

    goto/32 :goto_7

    nop

    :goto_0
    new-instance v0, Landroid/os/strictmode/ResourceMismatchViolation;

    goto/32 :goto_a

    nop

    :goto_1
    and-int/lit8 v0, v0, 0x10

    goto/32 :goto_2

    nop

    :goto_2
    if-eqz v0, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_b

    nop

    :goto_3
    return-void

    :goto_4
    goto/32 :goto_0

    nop

    :goto_5
    return-void

    :goto_6
    invoke-virtual {p0, v0}, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->startHandlingViolationException(Landroid/os/strictmode/Violation;)V

    goto/32 :goto_5

    nop

    :goto_7
    iget v0, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mThreadPolicyMask:I

    goto/32 :goto_1

    nop

    :goto_8
    invoke-static {}, Landroid/os/StrictMode;->-$$Nest$smtooManyViolationsThisLoop()Z

    move-result v0

    goto/32 :goto_9

    nop

    :goto_9
    if-nez v0, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_3

    nop

    :goto_a
    invoke-direct {v0, p1}, Landroid/os/strictmode/ResourceMismatchViolation;-><init>(Ljava/lang/Object;)V

    goto/32 :goto_6

    nop

    :goto_b
    return-void

    :goto_c
    goto/32 :goto_8

    nop
.end method

.method onThreadPolicyViolation(Landroid/os/StrictMode$ViolationInfo;)V
    .locals 19

    goto/32 :goto_31

    nop

    :goto_0
    cmp-long v0, v6, v17

    goto/32 :goto_63

    nop

    :goto_1
    or-int/2addr v0, v10

    :goto_2
    goto/32 :goto_50

    nop

    :goto_3
    const-wide/16 v17, 0x0

    goto/32 :goto_0

    nop

    :goto_4
    invoke-static {v11, v2}, Landroid/os/StrictMode;->-$$Nest$smhandleApplicationStrictModeViolation(ILandroid/os/StrictMode$ViolationInfo;)V

    :goto_5
    goto/32 :goto_62

    nop

    :goto_6
    goto/16 :goto_48

    :goto_7
    goto/32 :goto_5c

    nop

    :goto_8
    move-object v4, v0

    goto/32 :goto_9e

    nop

    :goto_9
    if-nez v0, :cond_0

    goto/32 :goto_45

    :cond_0
    goto/32 :goto_5d

    nop

    :goto_a
    const/high16 v0, 0x40000000    # 2.0f

    goto/32 :goto_13

    nop

    :goto_b
    invoke-static {v3, v12, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_c
    goto/32 :goto_41

    nop

    :goto_d
    const-string v3, "StrictMode"

    goto/32 :goto_9

    nop

    :goto_e
    invoke-virtual {v2, v0}, Landroid/os/StrictMode$ViolationInfo;->penaltyEnabled(I)Z

    move-result v0

    goto/32 :goto_30

    nop

    :goto_f
    move-object/from16 v16, v5

    :goto_10
    goto/32 :goto_3

    nop

    :goto_11
    check-cast v0, Ljava/util/ArrayList;

    goto/32 :goto_34

    nop

    :goto_12
    invoke-static {v10, v4, v5}, Landroid/os/StrictMode;->-$$Nest$smclampViolationTimeMap(Landroid/util/SparseLongArray;J)V

    goto/32 :goto_57

    nop

    :goto_13
    invoke-virtual {v2, v0}, Landroid/os/StrictMode$ViolationInfo;->penaltyEnabled(I)Z

    move-result v0

    goto/32 :goto_67

    nop

    :goto_14
    new-instance v0, Ljava/lang/RuntimeException;

    goto/32 :goto_8f

    nop

    :goto_15
    invoke-direct {v0, v4}, Landroid/util/SparseLongArray;-><init>(I)V

    goto/32 :goto_47

    nop

    :goto_16
    if-eqz v0, :cond_1

    goto/32 :goto_42

    :cond_1
    goto/32 :goto_70

    nop

    :goto_17
    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_18
    if-nez v4, :cond_2

    goto/32 :goto_c

    :cond_2
    goto/32 :goto_40

    nop

    :goto_19
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    :goto_1a
    goto/32 :goto_95

    nop

    :goto_1b
    invoke-interface {v0, v2}, Landroid/os/StrictMode$ViolationLogger;->log(Landroid/os/StrictMode$ViolationInfo;)V

    :goto_1c
    goto/32 :goto_4a

    nop

    :goto_1d
    iget-object v0, v1, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mRealLastViolationTime:Landroid/util/SparseLongArray;

    goto/32 :goto_27

    nop

    :goto_1e
    invoke-static {}, Landroid/os/StrictMode;->-$$Nest$sfgetgatheredViolations()Ljava/lang/ThreadLocal;

    move-result-object v3

    goto/32 :goto_7b

    nop

    :goto_1f
    move-object/from16 v2, p1

    goto/32 :goto_35

    nop

    :goto_20
    move v11, v0

    :goto_21
    goto/32 :goto_55

    nop

    :goto_22
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_7f

    nop

    :goto_23
    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_11

    nop

    :goto_24
    invoke-static {}, Landroid/os/StrictMode;->-$$Nest$sfgetsLogger()Landroid/os/StrictMode$ViolationLogger;

    move-result-object v0

    goto/32 :goto_1b

    nop

    :goto_25
    goto :goto_10

    :goto_26
    goto/32 :goto_f

    nop

    :goto_27
    if-nez v0, :cond_3

    goto/32 :goto_7

    :cond_3
    goto/32 :goto_58

    nop

    :goto_28
    goto/16 :goto_54

    :goto_29
    goto/32 :goto_53

    nop

    :goto_2a
    move-object v10, v0

    goto/32 :goto_80

    nop

    :goto_2b
    const-wide/16 v4, 0xbb8

    goto/32 :goto_4c

    nop

    :goto_2c
    invoke-static {}, Landroid/os/StrictMode;->-$$Nest$sfgetsThreadViolationExecutor()Ljava/lang/ThreadLocal;

    move-result-object v0

    goto/32 :goto_6d

    nop

    :goto_2d
    goto/16 :goto_5

    :goto_2e
    goto/32 :goto_4

    nop

    :goto_2f
    invoke-direct {v0, v3, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/32 :goto_64

    nop

    :goto_30
    const/4 v4, 0x1

    goto/32 :goto_37

    nop

    :goto_31
    move-object/from16 v1, p0

    goto/32 :goto_1f

    nop

    :goto_32
    invoke-static/range {p1 .. p1}, Landroid/os/StrictMode$ViolationInfo;->-$$Nest$fgetmPenaltyMask(Landroid/os/StrictMode$ViolationInfo;)I

    move-result v0

    goto/32 :goto_73

    nop

    :goto_33
    invoke-virtual {v2, v0}, Landroid/os/StrictMode$ViolationInfo;->penaltyEnabled(I)Z

    move-result v0

    goto/32 :goto_16

    nop

    :goto_34
    if-eqz v0, :cond_4

    goto/32 :goto_7c

    :cond_4
    goto/32 :goto_4d

    nop

    :goto_35
    invoke-static {}, Landroid/os/StrictMode;->-$$Nest$sfgetLOG_V()Z

    move-result v0

    goto/32 :goto_d

    nop

    :goto_36
    const/high16 v0, -0x80000000

    goto/32 :goto_e

    nop

    :goto_37
    if-nez v0, :cond_5

    goto/32 :goto_6a

    :cond_5
    goto/32 :goto_74

    nop

    :goto_38
    invoke-virtual/range {p1 .. p1}, Landroid/os/StrictMode$ViolationInfo;->getStackTrace()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_3e

    nop

    :goto_39
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    goto/32 :goto_6c

    nop

    :goto_3a
    const-string v12, "ThreadPolicy penaltyCallback failed"

    goto/32 :goto_b

    nop

    :goto_3b
    const-string/jumbo v4, "onThreadPolicyViolation; penalty="

    goto/32 :goto_22

    nop

    :goto_3c
    invoke-virtual {v0, v10}, Landroid/util/SparseLongArray;->get(I)J

    move-result-wide v15

    goto/32 :goto_5a

    nop

    :goto_3d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_44

    nop

    :goto_3e
    invoke-virtual {v4}, Landroid/os/StrictMode$ViolationInfo;->getStackTrace()Ljava/lang/String;

    move-result-object v6

    goto/32 :goto_68

    nop

    :goto_3f
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_60

    nop

    :goto_40
    if-nez v10, :cond_6

    goto/32 :goto_c

    :cond_6
    :try_start_0
    new-instance v0, Landroid/os/StrictMode$AndroidBlockGuardPolicy$$ExternalSyntheticLambda0;

    invoke-direct {v0, v4, v5}, Landroid/os/StrictMode$AndroidBlockGuardPolicy$$ExternalSyntheticLambda0;-><init>(Landroid/os/StrictMode$OnThreadViolationListener;Landroid/os/strictmode/Violation;)V

    invoke-interface {v10, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_9b

    nop

    :goto_41
    return-void

    :goto_42
    goto/32 :goto_14

    nop

    :goto_43
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto/32 :goto_85

    nop

    :goto_44
    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_45
    goto/32 :goto_36

    nop

    :goto_46
    move v0, v4

    goto/32 :goto_66

    nop

    :goto_47
    iput-object v0, v1, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mRealLastViolationTime:Landroid/util/SparseLongArray;

    :goto_48
    goto/32 :goto_8a

    nop

    :goto_49
    move-wide/from16 v17, v6

    goto/32 :goto_2b

    nop

    :goto_4a
    invoke-static/range {p1 .. p1}, Landroid/os/StrictMode$ViolationInfo;->-$$Nest$fgetmViolation(Landroid/os/StrictMode$ViolationInfo;)Landroid/os/strictmode/Violation;

    move-result-object v5

    goto/32 :goto_7d

    nop

    :goto_4b
    invoke-virtual/range {p1 .. p1}, Landroid/os/StrictMode$ViolationInfo;->hashCode()I

    move-result v0

    goto/32 :goto_43

    nop

    :goto_4c
    invoke-static {v13, v14, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    goto/32 :goto_84

    nop

    :goto_4d
    new-instance v3, Ljava/util/ArrayList;

    goto/32 :goto_52

    nop

    :goto_4e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_3b

    nop

    :goto_4f
    invoke-virtual {v0, v5, v8, v9}, Landroid/util/SparseLongArray;->put(IJ)V

    goto/32 :goto_25

    nop

    :goto_50
    const/high16 v10, 0x4000000

    goto/32 :goto_59

    nop

    :goto_51
    new-instance v0, Landroid/util/SparseLongArray;

    goto/32 :goto_56

    nop

    :goto_52
    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    goto/32 :goto_77

    nop

    :goto_53
    sub-long v17, v8, v6

    :goto_54
    goto/32 :goto_a

    nop

    :goto_55
    if-nez v11, :cond_7

    goto/32 :goto_5

    :cond_7
    goto/32 :goto_32

    nop

    :goto_56
    const/4 v4, 0x1

    goto/32 :goto_15

    nop

    :goto_57
    move-wide/from16 v6, v17

    goto/32 :goto_76

    nop

    :goto_58
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v10

    goto/32 :goto_3c

    nop

    :goto_59
    invoke-virtual {v2, v10}, Landroid/os/StrictMode$ViolationInfo;->penaltyEnabled(I)Z

    move-result v11

    goto/32 :goto_90

    nop

    :goto_5a
    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto/32 :goto_65

    nop

    :goto_5b
    cmp-long v0, v17, v11

    goto/32 :goto_6f

    nop

    :goto_5c
    move-object/from16 v16, v5

    goto/32 :goto_51

    nop

    :goto_5d
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_4e

    nop

    :goto_5e
    cmp-long v11, v17, v11

    goto/32 :goto_89

    nop

    :goto_5f
    if-nez v11, :cond_8

    goto/32 :goto_2

    :cond_8
    goto/32 :goto_9a

    nop

    :goto_60
    check-cast v4, Landroid/os/StrictMode$ViolationInfo;

    goto/32 :goto_38

    nop

    :goto_61
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    goto/32 :goto_6b

    nop

    :goto_62
    const/high16 v0, 0x10000000

    goto/32 :goto_33

    nop

    :goto_63
    if-eqz v0, :cond_9

    goto/32 :goto_29

    :cond_9
    goto/32 :goto_9f

    nop

    :goto_64
    throw v0

    :goto_65
    if-nez v0, :cond_a

    goto/32 :goto_1a

    :cond_a
    goto/32 :goto_19

    nop

    :goto_66
    if-nez v0, :cond_b

    goto/32 :goto_2e

    :cond_b
    goto/32 :goto_81

    nop

    :goto_67
    if-nez v0, :cond_c

    goto/32 :goto_1c

    :cond_c
    goto/32 :goto_5b

    nop

    :goto_68
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    goto/32 :goto_96

    nop

    :goto_69
    return-void

    :goto_6a
    goto/32 :goto_4b

    nop

    :goto_6b
    if-nez v4, :cond_d

    goto/32 :goto_79

    :cond_d
    goto/32 :goto_3f

    nop

    :goto_6c
    invoke-static {}, Landroid/os/StrictMode;->-$$Nest$sfgetsLogger()Landroid/os/StrictMode$ViolationLogger;

    move-result-object v0

    goto/32 :goto_99

    nop

    :goto_6d
    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_2a

    nop

    :goto_6e
    const-wide/16 v13, 0x7530

    goto/32 :goto_91

    nop

    :goto_6f
    if-gtz v0, :cond_e

    goto/32 :goto_1c

    :cond_e
    goto/32 :goto_24

    nop

    :goto_70
    invoke-static {}, Landroid/os/StrictMode;->-$$Nest$sfgetsThreadViolationListener()Ljava/lang/ThreadLocal;

    move-result-object v0

    goto/32 :goto_17

    nop

    :goto_71
    if-eq v0, v10, :cond_f

    goto/32 :goto_26

    :cond_f
    goto/32 :goto_1d

    nop

    :goto_72
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    move-result v5

    goto/32 :goto_4f

    nop

    :goto_73
    if-eq v0, v10, :cond_10

    goto/32 :goto_87

    :cond_10
    goto/32 :goto_86

    nop

    :goto_74
    invoke-static {}, Landroid/os/StrictMode;->-$$Nest$sfgetgatheredViolations()Ljava/lang/ThreadLocal;

    move-result-object v0

    goto/32 :goto_23

    nop

    :goto_75
    const-wide/16 v11, 0xbb8

    goto/32 :goto_5e

    nop

    :goto_76
    const/4 v4, 0x1

    goto/32 :goto_6

    nop

    :goto_77
    move-object v0, v3

    goto/32 :goto_1e

    nop

    :goto_78
    goto :goto_83

    :goto_79
    goto/32 :goto_94

    nop

    :goto_7a
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_3d

    nop

    :goto_7b
    invoke-virtual {v3, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    :goto_7c
    goto/32 :goto_82

    nop

    :goto_7d
    const/4 v0, 0x0

    goto/32 :goto_7e

    nop

    :goto_7e
    const/high16 v10, 0x20000000

    goto/32 :goto_8d

    nop

    :goto_7f
    invoke-static/range {p1 .. p1}, Landroid/os/StrictMode$ViolationInfo;->-$$Nest$fgetmPenaltyMask(Landroid/os/StrictMode$ViolationInfo;)I

    move-result v4

    goto/32 :goto_7a

    nop

    :goto_80
    check-cast v10, Ljava/util/concurrent/Executor;

    goto/32 :goto_18

    nop

    :goto_81
    invoke-static {v11, v2}, Landroid/os/StrictMode;->-$$Nest$smdropboxViolationAsync(ILandroid/os/StrictMode$ViolationInfo;)V

    goto/32 :goto_2d

    nop

    :goto_82
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_83
    goto/32 :goto_61

    nop

    :goto_84
    invoke-static {v11, v12, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    goto/32 :goto_12

    nop

    :goto_85
    const-wide/16 v6, 0x0

    goto/32 :goto_39

    nop

    :goto_86
    goto/16 :goto_9d

    :goto_87
    goto/32 :goto_9c

    nop

    :goto_88
    move v11, v0

    goto/32 :goto_92

    nop

    :goto_89
    if-gtz v11, :cond_11

    goto/32 :goto_93

    :cond_11
    goto/32 :goto_8c

    nop

    :goto_8a
    iget-object v0, v1, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mRealLastViolationTime:Landroid/util/SparseLongArray;

    goto/32 :goto_72

    nop

    :goto_8b
    move-object/from16 v16, v5

    goto/32 :goto_49

    nop

    :goto_8c
    or-int/2addr v0, v10

    goto/32 :goto_88

    nop

    :goto_8d
    invoke-virtual {v2, v10}, Landroid/os/StrictMode$ViolationInfo;->penaltyEnabled(I)Z

    move-result v11

    goto/32 :goto_5f

    nop

    :goto_8e
    if-gtz v11, :cond_12

    goto/32 :goto_2

    :cond_12
    goto/32 :goto_1

    nop

    :goto_8f
    const-string v3, "StrictMode ThreadPolicy violation"

    goto/32 :goto_2f

    nop

    :goto_90
    if-nez v11, :cond_13

    goto/32 :goto_93

    :cond_13
    goto/32 :goto_75

    nop

    :goto_91
    const-wide/16 v11, 0x3e8

    goto/32 :goto_71

    nop

    :goto_92
    goto/16 :goto_21

    :goto_93
    goto/32 :goto_20

    nop

    :goto_94
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_69

    nop

    :goto_95
    iget-object v10, v1, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mRealLastViolationTime:Landroid/util/SparseLongArray;

    goto/32 :goto_8b

    nop

    :goto_96
    if-nez v5, :cond_14

    goto/32 :goto_98

    :cond_14
    goto/32 :goto_97

    nop

    :goto_97
    return-void

    :goto_98
    goto/32 :goto_78

    nop

    :goto_99
    invoke-static {}, Landroid/os/StrictMode;->-$$Nest$sfgetLOGCAT_LOGGER()Landroid/os/StrictMode$ViolationLogger;

    move-result-object v10

    goto/32 :goto_6e

    nop

    :goto_9a
    cmp-long v11, v17, v13

    goto/32 :goto_8e

    nop

    :goto_9b
    goto/16 :goto_c

    :catch_0
    move-exception v0

    goto/32 :goto_3a

    nop

    :goto_9c
    const/4 v4, 0x0

    :goto_9d
    goto/32 :goto_46

    nop

    :goto_9e
    check-cast v4, Landroid/os/StrictMode$OnThreadViolationListener;

    goto/32 :goto_2c

    nop

    :goto_9f
    const-wide v17, 0x7fffffffffffffffL

    goto/32 :goto_28

    nop
.end method

.method public onUnbufferedIO()V
    .locals 1

    iget v0, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mThreadPolicyMask:I

    and-int/lit8 v0, v0, 0x20

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Landroid/os/StrictMode;->-$$Nest$smtooManyViolationsThisLoop()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_1
    new-instance v0, Landroid/os/strictmode/UnbufferedIoViolation;

    invoke-direct {v0}, Landroid/os/strictmode/UnbufferedIoViolation;-><init>()V

    invoke-virtual {p0, v0}, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->startHandlingViolationException(Landroid/os/strictmode/Violation;)V

    return-void
.end method

.method public onWriteToDisk()V
    .locals 1

    iget v0, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mThreadPolicyMask:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Landroid/os/StrictMode;->-$$Nest$smtooManyViolationsThisLoop()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_1
    new-instance v0, Landroid/os/strictmode/DiskWriteViolation;

    invoke-direct {v0}, Landroid/os/strictmode/DiskWriteViolation;-><init>()V

    invoke-virtual {p0, v0}, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->startHandlingViolationException(Landroid/os/strictmode/Violation;)V

    return-void
.end method

.method public setThreadPolicyMask(I)V
    .locals 0

    iput p1, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mThreadPolicyMask:I

    return-void
.end method

.method startHandlingViolationException(Landroid/os/strictmode/Violation;)V
    .locals 4

    goto/32 :goto_7

    nop

    :goto_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    goto/32 :goto_5

    nop

    :goto_1
    new-instance v1, Landroid/os/StrictMode$ViolationInfo;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-direct {v1, p1, v0}, Landroid/os/StrictMode$ViolationInfo;-><init>(Landroid/os/strictmode/Violation;I)V

    goto/32 :goto_0

    nop

    :goto_3
    const/high16 v1, -0x10000

    goto/32 :goto_4

    nop

    :goto_4
    and-int/2addr v0, v1

    goto/32 :goto_1

    nop

    :goto_5
    iput-wide v2, v1, Landroid/os/StrictMode$ViolationInfo;->violationUptimeMillis:J

    goto/32 :goto_6

    nop

    :goto_6
    invoke-virtual {p0, v1}, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->handleViolationWithTimingAttempt(Landroid/os/StrictMode$ViolationInfo;)V

    goto/32 :goto_8

    nop

    :goto_7
    iget v0, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mThreadPolicyMask:I

    goto/32 :goto_3

    nop

    :goto_8
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AndroidBlockGuardPolicy; mPolicyMask="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/os/StrictMode$AndroidBlockGuardPolicy;->mThreadPolicyMask:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
