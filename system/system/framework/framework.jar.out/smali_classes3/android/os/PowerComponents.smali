.class Landroid/os/PowerComponents;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/os/PowerComponents$Builder;
    }
.end annotation


# instance fields
.field private final mData:Landroid/os/BatteryConsumer$BatteryConsumerData;


# direct methods
.method static bridge synthetic -$$Nest$fgetmData(Landroid/os/PowerComponents;)Landroid/os/BatteryConsumer$BatteryConsumerData;
    .locals 0

    iget-object p0, p0, Landroid/os/PowerComponents;->mData:Landroid/os/BatteryConsumer$BatteryConsumerData;

    return-object p0
.end method

.method constructor <init>(Landroid/os/BatteryConsumer$BatteryConsumerData;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/os/PowerComponents;->mData:Landroid/os/BatteryConsumer$BatteryConsumerData;

    return-void
.end method

.method constructor <init>(Landroid/os/PowerComponents$Builder;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Landroid/os/PowerComponents$Builder;->-$$Nest$fgetmData(Landroid/os/PowerComponents$Builder;)Landroid/os/BatteryConsumer$BatteryConsumerData;

    move-result-object v0

    iput-object v0, p0, Landroid/os/PowerComponents;->mData:Landroid/os/BatteryConsumer$BatteryConsumerData;

    return-void
.end method

.method static parseXml(Landroid/util/TypedXmlPullParser;Landroid/os/PowerComponents$Builder;)V
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-interface/range {p0 .. p0}, Landroid/util/TypedXmlPullParser;->getEventType()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_9

    invoke-interface/range {p0 .. p0}, Landroid/util/TypedXmlPullParser;->getName()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "power_components"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    :goto_0
    const/4 v4, 0x3

    if-ne v2, v4, :cond_1

    invoke-interface/range {p0 .. p0}, Landroid/util/TypedXmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    goto :goto_1

    :cond_0
    move/from16 v19, v2

    goto/16 :goto_d

    :cond_1
    :goto_1
    const/4 v6, 0x1

    if-eq v2, v6, :cond_8

    if-ne v2, v3, :cond_7

    invoke-interface/range {p0 .. p0}, Landroid/util/TypedXmlPullParser;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->hashCode()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    :cond_2
    goto :goto_2

    :sswitch_0
    const-string v8, "custom_component"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    move v7, v6

    goto :goto_3

    :sswitch_1
    const-string v8, "component"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v7, 0x0

    goto :goto_3

    :goto_2
    const/4 v7, -0x1

    :goto_3
    const-string/jumbo v8, "power"

    const-string/jumbo v11, "id"

    const-string v12, "duration"

    packed-switch v7, :pswitch_data_0

    move/from16 v19, v2

    goto/16 :goto_c

    :pswitch_0
    const/4 v4, -0x1

    const-wide/16 v13, 0x0

    const-wide/16 v15, 0x0

    const/4 v7, 0x0

    move-wide v9, v15

    :goto_4
    invoke-interface/range {p0 .. p0}, Landroid/util/TypedXmlPullParser;->getAttributeCount()I

    move-result v3

    if-ge v7, v3, :cond_4

    invoke-interface {v0, v7}, Landroid/util/TypedXmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v17

    sparse-switch v17, :sswitch_data_1

    :cond_3
    goto :goto_5

    :sswitch_2
    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    move v3, v6

    goto :goto_6

    :sswitch_3
    invoke-virtual {v3, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x0

    goto :goto_6

    :sswitch_4
    invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x2

    goto :goto_6

    :goto_5
    const/4 v3, -0x1

    :goto_6
    packed-switch v3, :pswitch_data_1

    goto :goto_7

    :pswitch_1
    invoke-interface {v0, v7}, Landroid/util/TypedXmlPullParser;->getAttributeLong(I)J

    move-result-wide v9

    goto :goto_7

    :pswitch_2
    invoke-interface {v0, v7}, Landroid/util/TypedXmlPullParser;->getAttributeDouble(I)D

    move-result-wide v13

    goto :goto_7

    :pswitch_3
    invoke-interface {v0, v7}, Landroid/util/TypedXmlPullParser;->getAttributeInt(I)I

    move-result v3

    move v4, v3

    :goto_7
    add-int/lit8 v7, v7, 0x1

    const/4 v3, 0x2

    goto :goto_4

    :cond_4
    invoke-virtual {v1, v4, v13, v14}, Landroid/os/PowerComponents$Builder;->setConsumedPowerForCustomComponent(ID)Landroid/os/PowerComponents$Builder;

    invoke-virtual {v1, v4, v9, v10}, Landroid/os/PowerComponents$Builder;->setUsageDurationForCustomComponentMillis(IJ)Landroid/os/PowerComponents$Builder;

    move/from16 v19, v2

    goto/16 :goto_c

    :pswitch_4
    const/4 v3, -0x1

    const/4 v7, 0x0

    const-wide/16 v9, 0x0

    const-wide/16 v13, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move/from16 v4, v17

    move/from16 v6, v18

    :goto_8
    invoke-interface/range {p0 .. p0}, Landroid/util/TypedXmlPullParser;->getAttributeCount()I

    move-result v15

    if-ge v6, v15, :cond_6

    invoke-interface {v0, v6}, Landroid/util/TypedXmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/String;->hashCode()I

    move-result v19

    sparse-switch v19, :sswitch_data_2

    move/from16 v19, v2

    goto :goto_9

    :sswitch_5
    move/from16 v19, v2

    const-string/jumbo v2, "process_state"

    invoke-virtual {v15, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    goto :goto_a

    :sswitch_6
    move/from16 v19, v2

    invoke-virtual {v15, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x2

    goto :goto_a

    :sswitch_7
    move/from16 v19, v2

    const-string/jumbo v2, "model"

    invoke-virtual {v15, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x4

    goto :goto_a

    :sswitch_8
    move/from16 v19, v2

    invoke-virtual {v15, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x0

    goto :goto_a

    :sswitch_9
    move/from16 v19, v2

    invoke-virtual {v15, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x3

    goto :goto_a

    :cond_5
    :goto_9
    const/4 v2, -0x1

    :goto_a
    packed-switch v2, :pswitch_data_2

    goto :goto_b

    :pswitch_5
    invoke-interface {v0, v6}, Landroid/util/TypedXmlPullParser;->getAttributeInt(I)I

    move-result v2

    move v4, v2

    goto :goto_b

    :pswitch_6
    invoke-interface {v0, v6}, Landroid/util/TypedXmlPullParser;->getAttributeLong(I)J

    move-result-wide v13

    goto :goto_b

    :pswitch_7
    invoke-interface {v0, v6}, Landroid/util/TypedXmlPullParser;->getAttributeDouble(I)D

    move-result-wide v9

    goto :goto_b

    :pswitch_8
    invoke-interface {v0, v6}, Landroid/util/TypedXmlPullParser;->getAttributeInt(I)I

    move-result v2

    move v7, v2

    goto :goto_b

    :pswitch_9
    invoke-interface {v0, v6}, Landroid/util/TypedXmlPullParser;->getAttributeInt(I)I

    move-result v2

    move v3, v2

    :goto_b
    add-int/lit8 v6, v6, 0x1

    move/from16 v2, v19

    goto :goto_8

    :cond_6
    move/from16 v19, v2

    invoke-static/range {p1 .. p1}, Landroid/os/PowerComponents$Builder;->-$$Nest$fgetmData(Landroid/os/PowerComponents$Builder;)Landroid/os/BatteryConsumer$BatteryConsumerData;

    move-result-object v2

    invoke-virtual {v2, v3, v7}, Landroid/os/BatteryConsumer$BatteryConsumerData;->getKey(II)Landroid/os/BatteryConsumer$Key;

    move-result-object v2

    invoke-virtual {v1, v2, v9, v10, v4}, Landroid/os/PowerComponents$Builder;->setConsumedPower(Landroid/os/BatteryConsumer$Key;DI)Landroid/os/PowerComponents$Builder;

    invoke-virtual {v1, v2, v13, v14}, Landroid/os/PowerComponents$Builder;->setUsageDurationMillis(Landroid/os/BatteryConsumer$Key;J)Landroid/os/PowerComponents$Builder;

    goto :goto_c

    :cond_7
    move/from16 v19, v2

    :goto_c
    invoke-interface/range {p0 .. p0}, Landroid/util/TypedXmlPullParser;->next()I

    move-result v2

    const/4 v3, 0x2

    goto/16 :goto_0

    :cond_8
    move/from16 v19, v2

    :goto_d
    return-void

    :cond_9
    new-instance v3, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v4, "Invalid XML parser state"

    invoke-direct {v3, v4}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v3

    :sswitch_data_0
    .sparse-switch
        -0x5370e303 -> :sswitch_1
        -0x1a183651 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_0
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        -0x76bbb26c -> :sswitch_4
        0xd1b -> :sswitch_3
        0x65e8905 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :sswitch_data_2
    .sparse-switch
        -0x76bbb26c -> :sswitch_9
        0xd1b -> :sswitch_8
        0x633fb29 -> :sswitch_7
        0x65e8905 -> :sswitch_6
        0x633976c1 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method private writePowerComponentUsage(Landroid/util/proto/ProtoOutputStream;JIJJ)V
    .locals 4

    invoke-virtual {p1, p2, p3}, Landroid/util/proto/ProtoOutputStream;->start(J)J

    move-result-wide v0

    const-wide v2, 0x10500000001L

    invoke-virtual {p1, v2, v3, p4}, Landroid/util/proto/ProtoOutputStream;->write(JI)V

    const-wide v2, 0x10300000002L

    invoke-virtual {p1, v2, v3, p5, p6}, Landroid/util/proto/ProtoOutputStream;->write(JJ)V

    const-wide v2, 0x10300000003L

    invoke-virtual {p1, v2, v3, p7, p8}, Landroid/util/proto/ProtoOutputStream;->write(JJ)V

    invoke-virtual {p1, v0, v1}, Landroid/util/proto/ProtoOutputStream;->end(J)V

    return-void
.end method

.method private writePowerUsageSlice(Landroid/util/proto/ProtoOutputStream;IJJI)V
    .locals 13

    move-object v9, p1

    move/from16 v10, p7

    const-wide v0, 0x20b00000003L

    invoke-virtual {p1, v0, v1}, Landroid/util/proto/ProtoOutputStream;->start(J)J

    move-result-wide v11

    const-wide v2, 0x10b00000001L

    move-object v0, p0

    move-object v1, p1

    move v4, p2

    move-wide/from16 v5, p3

    move-wide/from16 v7, p5

    invoke-direct/range {v0 .. v8}, Landroid/os/PowerComponents;->writePowerComponentUsage(Landroid/util/proto/ProtoOutputStream;JIJJ)V

    packed-switch v10, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown process state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/4 v0, 0x4

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x1

    nop

    :goto_0
    const-wide v1, 0x10e00000002L

    invoke-virtual {p1, v1, v2, v0}, Landroid/util/proto/ProtoOutputStream;->write(JI)V

    invoke-virtual {p1, v11, v12}, Landroid/util/proto/ProtoOutputStream;->end(J)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private writeStatsProtoImpl(Landroid/util/proto/ProtoOutputStream;)Z
    .locals 22

    move-object/from16 v9, p0

    const/4 v0, 0x0

    const/4 v1, 0x0

    move v10, v1

    :goto_0
    const/16 v1, 0x12

    const-wide/16 v11, 0x0

    const/4 v13, 0x1

    if-ge v10, v1, :cond_4

    iget-object v1, v9, Landroid/os/PowerComponents;->mData:Landroid/os/BatteryConsumer$BatteryConsumerData;

    invoke-virtual {v1, v10}, Landroid/os/BatteryConsumer$BatteryConsumerData;->getKeys(I)[Landroid/os/BatteryConsumer$Key;

    move-result-object v14

    array-length v15, v14

    const/4 v1, 0x0

    move v7, v1

    :goto_1
    if-ge v7, v15, :cond_3

    aget-object v8, v14, v7

    invoke-virtual {v9, v8}, Landroid/os/PowerComponents;->getConsumedPower(Landroid/os/BatteryConsumer$Key;)D

    move-result-wide v1

    invoke-static {v1, v2}, Landroid/os/BatteryConsumer;->convertMahToDeciCoulombs(D)J

    move-result-wide v16

    invoke-virtual {v9, v8}, Landroid/os/PowerComponents;->getUsageDurationMillis(Landroid/os/BatteryConsumer$Key;)J

    move-result-wide v18

    cmp-long v1, v16, v11

    if-nez v1, :cond_0

    cmp-long v1, v18, v11

    if-nez v1, :cond_0

    move/from16 v21, v7

    goto :goto_3

    :cond_0
    const/16 v20, 0x1

    if-nez p1, :cond_1

    return v13

    :cond_1
    iget v0, v8, Landroid/os/BatteryConsumer$Key;->processState:I

    if-nez v0, :cond_2

    const-wide v2, 0x20b00000002L

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move v4, v10

    move-wide/from16 v5, v16

    move/from16 v21, v7

    move-object v13, v8

    move-wide/from16 v7, v18

    invoke-direct/range {v0 .. v8}, Landroid/os/PowerComponents;->writePowerComponentUsage(Landroid/util/proto/ProtoOutputStream;JIJJ)V

    goto :goto_2

    :cond_2
    move/from16 v21, v7

    move-object v13, v8

    iget v7, v13, Landroid/os/BatteryConsumer$Key;->processState:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move v2, v10

    move-wide/from16 v3, v16

    move-wide/from16 v5, v18

    invoke-direct/range {v0 .. v7}, Landroid/os/PowerComponents;->writePowerUsageSlice(Landroid/util/proto/ProtoOutputStream;IJJI)V

    :goto_2
    move/from16 v0, v20

    :goto_3
    add-int/lit8 v7, v21, 0x1

    const/4 v13, 0x1

    goto :goto_1

    :cond_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    move v10, v1

    :goto_4
    iget-object v1, v9, Landroid/os/PowerComponents;->mData:Landroid/os/BatteryConsumer$BatteryConsumerData;

    iget-object v1, v1, Landroid/os/BatteryConsumer$BatteryConsumerData;->layout:Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;

    iget v1, v1, Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;->customPowerComponentCount:I

    if-ge v10, v1, :cond_7

    add-int/lit16 v13, v10, 0x3e8

    nop

    invoke-virtual {v9, v13}, Landroid/os/PowerComponents;->getConsumedPowerForCustomComponent(I)D

    move-result-wide v1

    invoke-static {v1, v2}, Landroid/os/BatteryConsumer;->convertMahToDeciCoulombs(D)J

    move-result-wide v14

    invoke-virtual {v9, v13}, Landroid/os/PowerComponents;->getUsageDurationForCustomComponentMillis(I)J

    move-result-wide v16

    cmp-long v1, v14, v11

    if-nez v1, :cond_5

    cmp-long v1, v16, v11

    if-nez v1, :cond_5

    const/16 v19, 0x1

    goto :goto_5

    :cond_5
    const/16 v18, 0x1

    if-nez p1, :cond_6

    const/16 v19, 0x1

    return v19

    :cond_6
    const/16 v19, 0x1

    const-wide v2, 0x20b00000002L

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move v4, v13

    move-wide v5, v14

    move-wide/from16 v7, v16

    invoke-direct/range {v0 .. v8}, Landroid/os/PowerComponents;->writePowerComponentUsage(Landroid/util/proto/ProtoOutputStream;JIJJ)V

    move/from16 v0, v18

    :goto_5
    add-int/lit8 v10, v10, 0x1

    goto :goto_4

    :cond_7
    return v0
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;Z)V
    .locals 18

    move-object/from16 v0, p0

    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    :goto_0
    const/16 v4, 0x12

    const-wide/16 v5, 0x0

    const-string v7, "="

    if-ge v3, v4, :cond_3

    iget-object v4, v0, Landroid/os/PowerComponents;->mData:Landroid/os/BatteryConsumer$BatteryConsumerData;

    invoke-virtual {v4, v3}, Landroid/os/BatteryConsumer$BatteryConsumerData;->getKeys(I)[Landroid/os/BatteryConsumer$Key;

    move-result-object v4

    array-length v8, v4

    const/4 v9, 0x0

    :goto_1
    if-ge v9, v8, :cond_2

    aget-object v10, v4, v9

    invoke-virtual {v0, v10}, Landroid/os/PowerComponents;->getConsumedPower(Landroid/os/BatteryConsumer$Key;)D

    move-result-wide v11

    invoke-virtual {v0, v10}, Landroid/os/PowerComponents;->getUsageDurationMillis(Landroid/os/BatteryConsumer$Key;)J

    move-result-wide v13

    const-wide/16 v15, 0x0

    if-eqz p2, :cond_0

    cmpl-double v17, v11, v5

    if-nez v17, :cond_0

    cmp-long v17, v13, v15

    if-nez v17, :cond_0

    goto :goto_2

    :cond_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v10}, Landroid/os/BatteryConsumer$Key;->toShortString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v11, v12}, Landroid/os/BatteryStats;->formatCharge(D)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    cmp-long v5, v13, v15

    if-eqz v5, :cond_1

    const-string v5, " ("

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v2, v13, v14}, Landroid/os/BatteryStats;->formatTimeMsNoSpace(Ljava/lang/StringBuilder;J)V

    const-string v5, ")"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    :goto_2
    add-int/lit8 v9, v9, 0x1

    const-wide/16 v5, 0x0

    goto :goto_1

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    iget-object v3, v0, Landroid/os/PowerComponents;->mData:Landroid/os/BatteryConsumer$BatteryConsumerData;

    iget-object v3, v3, Landroid/os/BatteryConsumer$BatteryConsumerData;->layout:Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;

    iget v3, v3, Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;->customPowerComponentCount:I

    const/16 v4, 0x3e8

    :goto_3
    add-int/lit16 v5, v3, 0x3e8

    if-ge v4, v5, :cond_6

    nop

    invoke-virtual {v0, v4}, Landroid/os/PowerComponents;->getConsumedPowerForCustomComponent(I)D

    move-result-wide v5

    if-eqz p2, :cond_4

    const-wide/16 v8, 0x0

    cmpl-double v10, v5, v8

    if-nez v10, :cond_5

    goto :goto_4

    :cond_4
    const-wide/16 v8, 0x0

    :cond_5
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v4}, Landroid/os/PowerComponents;->getCustomPowerComponentName(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v5, v6}, Landroid/os/BatteryStats;->formatCharge(D)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_6
    move-object/from16 v4, p1

    invoke-virtual {v4, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    return-void
.end method

.method public getConsumedPower(Landroid/os/BatteryConsumer$Dimensions;)D
    .locals 6

    iget v0, p1, Landroid/os/BatteryConsumer$Dimensions;->powerComponent:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Landroid/os/PowerComponents;->mData:Landroid/os/BatteryConsumer$BatteryConsumerData;

    iget v1, p1, Landroid/os/BatteryConsumer$Dimensions;->powerComponent:I

    iget v2, p1, Landroid/os/BatteryConsumer$Dimensions;->processState:I

    invoke-virtual {v0, v1, v2}, Landroid/os/BatteryConsumer$BatteryConsumerData;->getKeyOrThrow(II)Landroid/os/BatteryConsumer$Key;

    move-result-object v1

    iget v1, v1, Landroid/os/BatteryConsumer$Key;->mPowerColumnIndex:I

    invoke-virtual {v0, v1}, Landroid/os/BatteryConsumer$BatteryConsumerData;->getDouble(I)D

    move-result-wide v0

    return-wide v0

    :cond_0
    iget v0, p1, Landroid/os/BatteryConsumer$Dimensions;->processState:I

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/os/PowerComponents;->mData:Landroid/os/BatteryConsumer$BatteryConsumerData;

    iget-object v0, v0, Landroid/os/BatteryConsumer$BatteryConsumerData;->layout:Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;

    iget-boolean v0, v0, Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;->processStateDataIncluded:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/os/PowerComponents;->mData:Landroid/os/BatteryConsumer$BatteryConsumerData;

    iget-object v0, v0, Landroid/os/BatteryConsumer$BatteryConsumerData;->layout:Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;

    iget-object v0, v0, Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;->processStateKeys:[[Landroid/os/BatteryConsumer$Key;

    iget v1, p1, Landroid/os/BatteryConsumer$Dimensions;->processState:I

    aget-object v0, v0, v1

    const-wide/16 v1, 0x0

    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    :goto_0
    if-ltz v3, :cond_1

    iget-object v4, p0, Landroid/os/PowerComponents;->mData:Landroid/os/BatteryConsumer$BatteryConsumerData;

    aget-object v5, v0, v3

    iget v5, v5, Landroid/os/BatteryConsumer$Key;->mPowerColumnIndex:I

    invoke-virtual {v4, v5}, Landroid/os/BatteryConsumer$BatteryConsumerData;->getDouble(I)D

    move-result-wide v4

    add-double/2addr v1, v4

    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    :cond_1
    return-wide v1

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No data included in BatteryUsageStats for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v0, p0, Landroid/os/PowerComponents;->mData:Landroid/os/BatteryConsumer$BatteryConsumerData;

    iget-object v1, v0, Landroid/os/BatteryConsumer$BatteryConsumerData;->layout:Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;

    iget v1, v1, Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;->totalConsumedPowerColumnIndex:I

    invoke-virtual {v0, v1}, Landroid/os/BatteryConsumer$BatteryConsumerData;->getDouble(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public getConsumedPower(Landroid/os/BatteryConsumer$Key;)D
    .locals 2

    iget-object v0, p0, Landroid/os/PowerComponents;->mData:Landroid/os/BatteryConsumer$BatteryConsumerData;

    iget v1, p1, Landroid/os/BatteryConsumer$Key;->mPowerColumnIndex:I

    invoke-virtual {v0, v1}, Landroid/os/BatteryConsumer$BatteryConsumerData;->getDouble(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public getConsumedPowerForCustomComponent(I)D
    .locals 4

    add-int/lit16 v0, p1, -0x3e8

    if-ltz v0, :cond_0

    iget-object v1, p0, Landroid/os/PowerComponents;->mData:Landroid/os/BatteryConsumer$BatteryConsumerData;

    iget-object v1, v1, Landroid/os/BatteryConsumer$BatteryConsumerData;->layout:Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;

    iget v1, v1, Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;->customPowerComponentCount:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Landroid/os/PowerComponents;->mData:Landroid/os/BatteryConsumer$BatteryConsumerData;

    iget-object v2, v1, Landroid/os/BatteryConsumer$BatteryConsumerData;->layout:Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;

    iget v2, v2, Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;->firstCustomConsumedPowerColumn:I

    add-int/2addr v2, v0

    invoke-virtual {v1, v2}, Landroid/os/BatteryConsumer$BatteryConsumerData;->getDouble(I)D

    move-result-wide v1

    return-wide v1

    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported custom power component ID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getCustomPowerComponentName(I)Ljava/lang/String;
    .locals 5

    add-int/lit16 v0, p1, -0x3e8

    const-string v1, "Unsupported custom power component ID: "

    if-ltz v0, :cond_0

    iget-object v2, p0, Landroid/os/PowerComponents;->mData:Landroid/os/BatteryConsumer$BatteryConsumerData;

    iget-object v2, v2, Landroid/os/BatteryConsumer$BatteryConsumerData;->layout:Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;

    iget v2, v2, Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;->customPowerComponentCount:I

    if-ge v0, v2, :cond_0

    :try_start_0
    iget-object v2, p0, Landroid/os/PowerComponents;->mData:Landroid/os/BatteryConsumer$BatteryConsumerData;

    iget-object v2, v2, Landroid/os/BatteryConsumer$BatteryConsumerData;->layout:Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;

    iget-object v2, v2, Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;->customPowerComponentNames:[Ljava/lang/String;

    aget-object v1, v2, v0
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v2

    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method getPowerModel(Landroid/os/BatteryConsumer$Key;)I
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    throw v0

    :goto_1
    const/4 v1, -0x1

    goto/32 :goto_a

    nop

    :goto_2
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_0

    nop

    :goto_3
    iget v0, p1, Landroid/os/BatteryConsumer$Key;->mPowerModelColumnIndex:I

    goto/32 :goto_1

    nop

    :goto_4
    iget-object v0, p0, Landroid/os/PowerComponents;->mData:Landroid/os/BatteryConsumer$BatteryConsumerData;

    goto/32 :goto_b

    nop

    :goto_5
    return v0

    :goto_6
    goto/32 :goto_8

    nop

    :goto_7
    const-string v1, "Power model IDs were not requested in the BatteryUsageStatsQuery"

    goto/32 :goto_2

    nop

    :goto_8
    new-instance v0, Ljava/lang/IllegalStateException;

    goto/32 :goto_7

    nop

    :goto_9
    invoke-virtual {v0, v1}, Landroid/os/BatteryConsumer$BatteryConsumerData;->getInt(I)I

    move-result v0

    goto/32 :goto_5

    nop

    :goto_a
    if-ne v0, v1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_4

    nop

    :goto_b
    iget v1, p1, Landroid/os/BatteryConsumer$Key;->mPowerModelColumnIndex:I

    goto/32 :goto_9

    nop
.end method

.method public getUsageDurationForCustomComponentMillis(I)J
    .locals 4

    add-int/lit16 v0, p1, -0x3e8

    if-ltz v0, :cond_0

    iget-object v1, p0, Landroid/os/PowerComponents;->mData:Landroid/os/BatteryConsumer$BatteryConsumerData;

    iget-object v1, v1, Landroid/os/BatteryConsumer$BatteryConsumerData;->layout:Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;

    iget v1, v1, Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;->customPowerComponentCount:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Landroid/os/PowerComponents;->mData:Landroid/os/BatteryConsumer$BatteryConsumerData;

    iget-object v2, v1, Landroid/os/BatteryConsumer$BatteryConsumerData;->layout:Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;

    iget v2, v2, Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;->firstCustomUsageDurationColumn:I

    add-int/2addr v2, v0

    invoke-virtual {v1, v2}, Landroid/os/BatteryConsumer$BatteryConsumerData;->getLong(I)J

    move-result-wide v1

    return-wide v1

    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported custom power component ID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getUsageDurationMillis(Landroid/os/BatteryConsumer$Key;)J
    .locals 2

    iget-object v0, p0, Landroid/os/PowerComponents;->mData:Landroid/os/BatteryConsumer$BatteryConsumerData;

    iget v1, p1, Landroid/os/BatteryConsumer$Key;->mDurationColumnIndex:I

    invoke-virtual {v0, v1}, Landroid/os/BatteryConsumer$BatteryConsumerData;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method hasStatsProtoData()Z
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-direct {p0, v0}, Landroid/os/PowerComponents;->writeStatsProtoImpl(Landroid/util/proto/ProtoOutputStream;)Z

    move-result v0

    goto/32 :goto_2

    nop

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_0

    nop

    :goto_2
    return v0
.end method

.method writeStatsProto(Landroid/util/proto/ProtoOutputStream;)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-direct {p0, p1}, Landroid/os/PowerComponents;->writeStatsProtoImpl(Landroid/util/proto/ProtoOutputStream;)Z

    goto/32 :goto_0

    nop
.end method

.method writeToXml(Landroid/util/TypedXmlSerializer;)V
    .locals 24
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_6d

    nop

    :goto_0
    invoke-virtual {v0, v15}, Landroid/os/PowerComponents;->getUsageDurationMillis(Landroid/os/BatteryConsumer$Key;)J

    move-result-wide v9

    goto/32 :goto_2e

    nop

    :goto_1
    move-object/from16 v17, v3

    goto/32 :goto_18

    nop

    :goto_2
    return-void

    :goto_3
    cmpl-double v13, v4, v11

    goto/32 :goto_74

    nop

    :goto_4
    if-nez v15, :cond_0

    goto/32 :goto_15

    :cond_0
    goto/32 :goto_14

    nop

    :goto_5
    if-lt v14, v13, :cond_1

    goto/32 :goto_68

    :cond_1
    goto/32 :goto_7

    nop

    :goto_6
    cmp-long v13, v9, v11

    goto/32 :goto_11

    nop

    :goto_7
    aget-object v15, v5, v14

    goto/32 :goto_7d

    nop

    :goto_8
    iget-boolean v5, v5, Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;->powerModelsIncluded:Z

    goto/32 :goto_42

    nop

    :goto_9
    const/4 v13, 0x0

    :goto_a
    goto/32 :goto_2b

    nop

    :goto_b
    move-object/from16 v22, v5

    goto/32 :goto_2d

    nop

    :goto_c
    iget-object v5, v5, Landroid/os/BatteryConsumer$BatteryConsumerData;->layout:Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;

    goto/32 :goto_8

    nop

    :goto_d
    if-eqz v20, :cond_2

    goto/32 :goto_28

    :cond_2
    goto/32 :goto_1b

    nop

    :goto_e
    const-wide/16 v11, 0x0

    goto/32 :goto_6

    nop

    :goto_f
    const/4 v14, 0x0

    :goto_10
    goto/32 :goto_5

    nop

    :goto_11
    if-eqz v13, :cond_3

    goto/32 :goto_81

    :cond_3
    goto/32 :goto_46

    nop

    :goto_12
    invoke-interface {v1, v13, v7, v2, v3}, Landroid/util/TypedXmlSerializer;->attributeDouble(Ljava/lang/String;Ljava/lang/String;D)Lorg/xmlpull/v1/XmlSerializer;

    :goto_13
    goto/32 :goto_41

    nop

    :goto_14
    invoke-interface {v1, v12, v7, v4, v5}, Landroid/util/TypedXmlSerializer;->attributeDouble(Ljava/lang/String;Ljava/lang/String;D)Lorg/xmlpull/v1/XmlSerializer;

    :goto_15
    goto/32 :goto_61

    nop

    :goto_16
    invoke-interface {v1, v12, v3}, Landroid/util/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto/32 :goto_2

    nop

    :goto_17
    move/from16 v23, v13

    goto/32 :goto_4d

    nop

    :goto_18
    move-object/from16 v22, v5

    goto/32 :goto_47

    nop

    :goto_19
    move-object/from16 v17, v3

    goto/32 :goto_55

    nop

    :goto_1a
    const/4 v2, 0x0

    goto/32 :goto_5f

    nop

    :goto_1b
    move-object/from16 v22, v5

    goto/32 :goto_3e

    nop

    :goto_1c
    invoke-virtual {v0, v3}, Landroid/os/PowerComponents;->getConsumedPowerForCustomComponent(I)D

    move-result-wide v4

    goto/32 :goto_6f

    nop

    :goto_1d
    goto :goto_a

    :goto_1e
    goto/32 :goto_9

    nop

    :goto_1f
    cmp-long v18, v9, v15

    goto/32 :goto_23

    nop

    :goto_20
    iget v12, v15, Landroid/os/BatteryConsumer$Key;->processState:I

    goto/32 :goto_6b

    nop

    :goto_21
    const-string v11, "component"

    goto/32 :goto_44

    nop

    :goto_22
    if-nez v5, :cond_4

    goto/32 :goto_13

    :cond_4
    goto/32 :goto_12

    nop

    :goto_23
    if-nez v18, :cond_5

    goto/32 :goto_4a

    :cond_5
    goto/32 :goto_49

    nop

    :goto_24
    const/4 v13, 0x0

    goto/32 :goto_70

    nop

    :goto_25
    cmp-long v20, v9, v18

    goto/32 :goto_d

    nop

    :goto_26
    const-string/jumbo v7, "power"

    goto/32 :goto_4b

    nop

    :goto_27
    goto :goto_2c

    :goto_28
    goto/32 :goto_21

    nop

    :goto_29
    add-int/lit8 v14, v14, 0x1

    goto/32 :goto_60

    nop

    :goto_2a
    const-string/jumbo v12, "model"

    goto/32 :goto_24

    nop

    :goto_2b
    invoke-interface {v1, v13, v11}, Landroid/util/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    :goto_2c
    goto/32 :goto_29

    nop

    :goto_2d
    move/from16 v23, v13

    goto/32 :goto_57

    nop

    :goto_2e
    cmpl-double v20, v2, v11

    goto/32 :goto_5c

    nop

    :goto_2f
    const-wide/16 v15, 0x0

    goto/32 :goto_80

    nop

    :goto_30
    const/4 v2, 0x0

    goto/32 :goto_7a

    nop

    :goto_31
    invoke-interface {v1, v12, v8, v3}, Landroid/util/TypedXmlSerializer;->attributeInt(Ljava/lang/String;Ljava/lang/String;I)Lorg/xmlpull/v1/XmlSerializer;

    goto/32 :goto_6a

    nop

    :goto_32
    invoke-interface {v1, v12, v8, v4}, Landroid/util/TypedXmlSerializer;->attributeInt(Ljava/lang/String;Ljava/lang/String;I)Lorg/xmlpull/v1/XmlSerializer;

    goto/32 :goto_20

    nop

    :goto_33
    invoke-interface {v1, v2, v3}, Landroid/util/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto/32 :goto_3b

    nop

    :goto_34
    iget-object v5, v0, Landroid/os/PowerComponents;->mData:Landroid/os/BatteryConsumer$BatteryConsumerData;

    goto/32 :goto_c

    nop

    :goto_35
    cmpl-double v15, v4, v13

    goto/32 :goto_4

    nop

    :goto_36
    if-nez v5, :cond_6

    goto/32 :goto_66

    :cond_6
    goto/32 :goto_65

    nop

    :goto_37
    const/4 v2, 0x0

    goto/32 :goto_54

    nop

    :goto_38
    if-lt v3, v2, :cond_7

    goto/32 :goto_72

    :cond_7
    goto/32 :goto_1c

    nop

    :goto_39
    const-string v11, "custom_component"

    goto/32 :goto_82

    nop

    :goto_3a
    cmpl-double v5, v2, v20

    goto/32 :goto_22

    nop

    :goto_3b
    const/4 v4, 0x0

    :goto_3c
    goto/32 :goto_43

    nop

    :goto_3d
    move-object/from16 v3, v17

    goto/32 :goto_16

    nop

    :goto_3e
    move/from16 v23, v13

    goto/32 :goto_27

    nop

    :goto_3f
    const-string v6, "duration"

    goto/32 :goto_26

    nop

    :goto_40
    array-length v13, v5

    goto/32 :goto_f

    nop

    :goto_41
    const-wide/16 v18, 0x0

    goto/32 :goto_79

    nop

    :goto_42
    if-nez v5, :cond_8

    goto/32 :goto_1e

    :cond_8
    nop

    goto/32 :goto_50

    nop

    :goto_43
    const/16 v5, 0x12

    goto/32 :goto_3f

    nop

    :goto_44
    const/4 v12, 0x0

    goto/32 :goto_69

    nop

    :goto_45
    iget v12, v15, Landroid/os/BatteryConsumer$Key;->processState:I

    goto/32 :goto_76

    nop

    :goto_46
    const/4 v12, 0x0

    goto/32 :goto_4c

    nop

    :goto_47
    add-int/lit8 v4, v4, 0x1

    goto/32 :goto_30

    nop

    :goto_48
    const-wide/16 v18, 0x0

    goto/32 :goto_25

    nop

    :goto_49
    invoke-interface {v1, v12, v6, v9, v10}, Landroid/util/TypedXmlSerializer;->attributeLong(Ljava/lang/String;Ljava/lang/String;J)Lorg/xmlpull/v1/XmlSerializer;

    :goto_4a
    goto/32 :goto_62

    nop

    :goto_4b
    const-string/jumbo v8, "id"

    goto/32 :goto_6c

    nop

    :goto_4c
    const-wide/16 v13, 0x0

    goto/32 :goto_2f

    nop

    :goto_4d
    const/4 v13, 0x0

    goto/32 :goto_5b

    nop

    :goto_4e
    add-int/lit8 v3, v3, 0x1

    goto/32 :goto_71

    nop

    :goto_4f
    add-int/lit16 v2, v2, 0x3e8

    goto/32 :goto_7e

    nop

    :goto_50
    invoke-virtual {v0, v15}, Landroid/os/PowerComponents;->getPowerModel(Landroid/os/BatteryConsumer$Key;)I

    move-result v5

    goto/32 :goto_2a

    nop

    :goto_51
    invoke-virtual {v0, v15}, Landroid/os/PowerComponents;->getConsumedPower(Landroid/os/BatteryConsumer$Key;)D

    move-result-wide v2

    goto/32 :goto_0

    nop

    :goto_52
    iget-object v2, v2, Landroid/os/BatteryConsumer$BatteryConsumerData;->layout:Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;

    goto/32 :goto_56

    nop

    :goto_53
    move-object/from16 v1, p1

    goto/32 :goto_37

    nop

    :goto_54
    const-string/jumbo v3, "power_components"

    goto/32 :goto_33

    nop

    :goto_55
    iget-object v2, v0, Landroid/os/PowerComponents;->mData:Landroid/os/BatteryConsumer$BatteryConsumerData;

    goto/32 :goto_52

    nop

    :goto_56
    iget v2, v2, Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;->customPowerComponentCount:I

    goto/32 :goto_4f

    nop

    :goto_57
    const/4 v13, 0x0

    :goto_58
    goto/32 :goto_78

    nop

    :goto_59
    move/from16 v13, v23

    goto/32 :goto_1a

    nop

    :goto_5a
    const/4 v12, 0x0

    goto/32 :goto_3d

    nop

    :goto_5b
    invoke-interface {v1, v13, v5, v12}, Landroid/util/TypedXmlSerializer;->attributeInt(Ljava/lang/String;Ljava/lang/String;I)Lorg/xmlpull/v1/XmlSerializer;

    goto/32 :goto_5d

    nop

    :goto_5c
    if-eqz v20, :cond_9

    goto/32 :goto_28

    :cond_9
    goto/32 :goto_48

    nop

    :goto_5d
    goto :goto_58

    :goto_5e
    goto/32 :goto_b

    nop

    :goto_5f
    const-wide/16 v11, 0x0

    goto/32 :goto_67

    nop

    :goto_60
    move-object/from16 v3, v17

    goto/32 :goto_77

    nop

    :goto_61
    const-wide/16 v15, 0x0

    goto/32 :goto_1f

    nop

    :goto_62
    invoke-interface {v1, v12, v11}, Landroid/util/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    :goto_63
    goto/32 :goto_4e

    nop

    :goto_64
    if-lt v4, v5, :cond_a

    goto/32 :goto_7b

    :cond_a
    goto/32 :goto_73

    nop

    :goto_65
    invoke-interface {v1, v13, v6, v9, v10}, Landroid/util/TypedXmlSerializer;->attributeLong(Ljava/lang/String;Ljava/lang/String;J)Lorg/xmlpull/v1/XmlSerializer;

    :goto_66
    goto/32 :goto_34

    nop

    :goto_67
    goto/16 :goto_10

    :goto_68
    goto/32 :goto_1

    nop

    :goto_69
    invoke-interface {v1, v12, v11}, Landroid/util/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto/32 :goto_32

    nop

    :goto_6a
    const-wide/16 v13, 0x0

    goto/32 :goto_35

    nop

    :goto_6b
    if-nez v12, :cond_b

    goto/32 :goto_5e

    :cond_b
    goto/32 :goto_45

    nop

    :goto_6c
    const-wide/16 v11, 0x0

    goto/32 :goto_64

    nop

    :goto_6d
    move-object/from16 v0, p0

    goto/32 :goto_53

    nop

    :goto_6e
    const-wide/16 v11, 0x0

    goto/32 :goto_3

    nop

    :goto_6f
    invoke-virtual {v0, v3}, Landroid/os/PowerComponents;->getUsageDurationForCustomComponentMillis(I)J

    move-result-wide v9

    goto/32 :goto_6e

    nop

    :goto_70
    invoke-interface {v1, v13, v12, v5}, Landroid/util/TypedXmlSerializer;->attributeInt(Ljava/lang/String;Ljava/lang/String;I)Lorg/xmlpull/v1/XmlSerializer;

    goto/32 :goto_1d

    nop

    :goto_71
    goto :goto_7f

    :goto_72
    goto/32 :goto_5a

    nop

    :goto_73
    iget-object v5, v0, Landroid/os/PowerComponents;->mData:Landroid/os/BatteryConsumer$BatteryConsumerData;

    goto/32 :goto_75

    nop

    :goto_74
    if-eqz v13, :cond_c

    goto/32 :goto_81

    :cond_c
    goto/32 :goto_e

    nop

    :goto_75
    invoke-virtual {v5, v4}, Landroid/os/BatteryConsumer$BatteryConsumerData;->getKeys(I)[Landroid/os/BatteryConsumer$Key;

    move-result-object v5

    goto/32 :goto_40

    nop

    :goto_76
    move-object/from16 v22, v5

    goto/32 :goto_83

    nop

    :goto_77
    move-object/from16 v5, v22

    goto/32 :goto_59

    nop

    :goto_78
    const-wide/16 v20, 0x0

    goto/32 :goto_3a

    nop

    :goto_79
    cmp-long v5, v9, v18

    goto/32 :goto_36

    nop

    :goto_7a
    goto/16 :goto_3c

    :goto_7b
    goto/32 :goto_19

    nop

    :goto_7c
    invoke-interface {v1, v12, v11}, Landroid/util/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto/32 :goto_31

    nop

    :goto_7d
    move-object/from16 v17, v3

    goto/32 :goto_51

    nop

    :goto_7e
    const/16 v3, 0x3e8

    :goto_7f
    goto/32 :goto_38

    nop

    :goto_80
    goto/16 :goto_63

    :goto_81
    goto/32 :goto_39

    nop

    :goto_82
    const/4 v12, 0x0

    goto/32 :goto_7c

    nop

    :goto_83
    const-string/jumbo v5, "process_state"

    goto/32 :goto_17

    nop
.end method
