.class Landroid/os/SystemVibrator$AllVibratorsStateListener;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/SystemVibrator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AllVibratorsStateListener"
.end annotation


# instance fields
.field private final mDelegate:Landroid/os/Vibrator$OnVibratorStateChangedListener;

.field private final mExecutor:Ljava/util/concurrent/Executor;

.field private mInitializedMask:I

.field private final mLock:Ljava/lang/Object;

.field private mVibratingMask:I

.field private final mVibratorListeners:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Landroid/os/SystemVibrator$SingleVibratorStateListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/concurrent/Executor;Landroid/os/Vibrator$OnVibratorStateChangedListener;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Landroid/os/SystemVibrator$AllVibratorsStateListener;->mLock:Ljava/lang/Object;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Landroid/os/SystemVibrator$AllVibratorsStateListener;->mVibratorListeners:Landroid/util/SparseArray;

    iput-object p1, p0, Landroid/os/SystemVibrator$AllVibratorsStateListener;->mExecutor:Ljava/util/concurrent/Executor;

    iput-object p2, p0, Landroid/os/SystemVibrator$AllVibratorsStateListener;->mDelegate:Landroid/os/Vibrator$OnVibratorStateChangedListener;

    return-void
.end method


# virtual methods
.method hasRegisteredListeners()Z
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/os/SystemVibrator$AllVibratorsStateListener;->mLock:Ljava/lang/Object;

    goto/32 :goto_2

    nop

    :goto_1
    throw v1

    :goto_2
    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Landroid/os/SystemVibrator$AllVibratorsStateListener;->mVibratorListeners:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_3

    :cond_0
    const/4 v1, 0x0

    :goto_3
    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_1

    nop
.end method

.method synthetic lambda$onVibrating$0$android-os-SystemVibrator$AllVibratorsStateListener(IZ)V
    .locals 7

    goto/32 :goto_6

    nop

    :goto_0
    throw v1

    :goto_1
    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Landroid/os/SystemVibrator$AllVibratorsStateListener;->mVibratorListeners:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    const/4 v2, 0x1

    shl-int v1, v2, v1

    sub-int/2addr v1, v2

    shl-int v3, v2, p1

    iget v4, p0, Landroid/os/SystemVibrator$AllVibratorsStateListener;->mInitializedMask:I

    and-int v5, v4, v3

    const/4 v6, 0x0

    if-nez v5, :cond_1

    or-int/2addr v4, v3

    iput v4, p0, Landroid/os/SystemVibrator$AllVibratorsStateListener;->mInitializedMask:I

    iget v4, p0, Landroid/os/SystemVibrator$AllVibratorsStateListener;->mVibratingMask:I

    if-eqz p2, :cond_0

    move v5, v3

    goto :goto_2

    :cond_0
    move v5, v6

    :goto_2
    or-int/2addr v4, v5

    iput v4, p0, Landroid/os/SystemVibrator$AllVibratorsStateListener;->mVibratingMask:I

    goto :goto_4

    :cond_1
    iget v4, p0, Landroid/os/SystemVibrator$AllVibratorsStateListener;->mVibratingMask:I

    and-int v5, v4, v3

    if-eqz v5, :cond_2

    move v5, v2

    goto :goto_3

    :cond_2
    move v5, v6

    :goto_3
    if-eq v5, p2, :cond_3

    xor-int/2addr v4, v3

    iput v4, p0, Landroid/os/SystemVibrator$AllVibratorsStateListener;->mVibratingMask:I

    :cond_3
    :goto_4
    iget v4, p0, Landroid/os/SystemVibrator$AllVibratorsStateListener;->mInitializedMask:I

    if-eq v4, v1, :cond_4

    monitor-exit v0

    return-void

    :cond_4
    iget v4, p0, Landroid/os/SystemVibrator$AllVibratorsStateListener;->mVibratingMask:I

    if-eqz v4, :cond_5

    goto :goto_5

    :cond_5
    move v2, v6

    :goto_5
    move v1, v2

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_8

    nop

    :goto_6
    iget-object v0, p0, Landroid/os/SystemVibrator$AllVibratorsStateListener;->mLock:Ljava/lang/Object;

    goto/32 :goto_1

    nop

    :goto_7
    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/32 :goto_0

    nop

    :goto_8
    iget-object v0, p0, Landroid/os/SystemVibrator$AllVibratorsStateListener;->mDelegate:Landroid/os/Vibrator$OnVibratorStateChangedListener;

    goto/32 :goto_9

    nop

    :goto_9
    invoke-interface {v0, v1}, Landroid/os/Vibrator$OnVibratorStateChangedListener;->onVibratorStateChanged(Z)V

    goto/32 :goto_7

    nop
.end method

.method onVibrating(IZ)V
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    new-instance v1, Landroid/os/SystemVibrator$AllVibratorsStateListener$$ExternalSyntheticLambda0;

    goto/32 :goto_2

    nop

    :goto_1
    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto/32 :goto_4

    nop

    :goto_2
    invoke-direct {v1, p0, p1, p2}, Landroid/os/SystemVibrator$AllVibratorsStateListener$$ExternalSyntheticLambda0;-><init>(Landroid/os/SystemVibrator$AllVibratorsStateListener;IZ)V

    goto/32 :goto_1

    nop

    :goto_3
    iget-object v0, p0, Landroid/os/SystemVibrator$AllVibratorsStateListener;->mExecutor:Ljava/util/concurrent/Executor;

    goto/32 :goto_0

    nop

    :goto_4
    return-void
.end method

.method register(Landroid/os/VibratorManager;)V
    .locals 9

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p1}, Landroid/os/VibratorManager;->getVibratorIds()[I

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_1
    iget-object v1, p0, Landroid/os/SystemVibrator$AllVibratorsStateListener;->mLock:Ljava/lang/Object;

    goto/32 :goto_6

    nop

    :goto_2
    goto :goto_9

    :catch_0
    move-exception v5

    :try_start_0
    invoke-virtual {p0, p1}, Landroid/os/SystemVibrator$AllVibratorsStateListener;->unregister(Landroid/os/VibratorManager;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_4

    nop

    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_2

    nop

    :goto_4
    goto :goto_5

    :catch_1
    move-exception v6

    :try_start_1
    const-string v7, "Vibrator"

    const-string v8, "Failed to unregister listener while recovering from a failed register call"

    invoke-static {v7, v8, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_5
    nop

    throw v5

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/32 :goto_7

    nop

    :goto_6
    monitor-enter v1

    goto/32 :goto_8

    nop

    :goto_7
    throw v2

    :goto_8
    const/4 v2, 0x0

    :goto_9
    :try_start_2
    array-length v3, v0

    if-ge v2, v3, :cond_0

    aget v3, v0, v2

    new-instance v4, Landroid/os/SystemVibrator$SingleVibratorStateListener;

    invoke-direct {v4, p0, v2}, Landroid/os/SystemVibrator$SingleVibratorStateListener;-><init>(Landroid/os/SystemVibrator$AllVibratorsStateListener;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {p1, v3}, Landroid/os/VibratorManager;->getVibrator(I)Landroid/os/Vibrator;

    move-result-object v5

    iget-object v6, p0, Landroid/os/SystemVibrator$AllVibratorsStateListener;->mExecutor:Ljava/util/concurrent/Executor;

    invoke-virtual {v5, v6, v4}, Landroid/os/Vibrator;->addVibratorStateListener(Ljava/util/concurrent/Executor;Landroid/os/Vibrator$OnVibratorStateChangedListener;)V

    iget-object v5, p0, Landroid/os/SystemVibrator$AllVibratorsStateListener;->mVibratorListeners:Landroid/util/SparseArray;

    invoke-virtual {v5, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    nop

    goto/32 :goto_3

    nop
.end method

.method unregister(Landroid/os/VibratorManager;)V
    .locals 5

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/os/SystemVibrator$AllVibratorsStateListener;->mLock:Ljava/lang/Object;

    goto/32 :goto_1

    nop

    :goto_1
    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Landroid/os/SystemVibrator$AllVibratorsStateListener;->mVibratorListeners:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    :goto_2
    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_0

    iget-object v2, p0, Landroid/os/SystemVibrator$AllVibratorsStateListener;->mVibratorListeners:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    iget-object v3, p0, Landroid/os/SystemVibrator$AllVibratorsStateListener;->mVibratorListeners:Landroid/util/SparseArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/SystemVibrator$SingleVibratorStateListener;

    invoke-virtual {p1, v2}, Landroid/os/VibratorManager;->getVibrator(I)Landroid/os/Vibrator;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/os/Vibrator;->removeVibratorStateListener(Landroid/os/Vibrator$OnVibratorStateChangedListener;)V

    iget-object v4, p0, Landroid/os/SystemVibrator$AllVibratorsStateListener;->mVibratorListeners:Landroid/util/SparseArray;

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->removeAt(I)V

    goto :goto_2

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_3

    nop

    :goto_3
    throw v1
.end method
