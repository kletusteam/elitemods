.class Landroid/os/health/HealthKeys$SortedIntArray;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/health/HealthKeys;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SortedIntArray"
.end annotation


# instance fields
.field mArray:[I

.field mCount:I


# direct methods
.method constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, p1, [I

    iput-object v0, p0, Landroid/os/health/HealthKeys$SortedIntArray;->mArray:[I

    return-void
.end method


# virtual methods
.method addValue(I)V
    .locals 3

    goto/32 :goto_4

    nop

    :goto_0
    return-void

    :goto_1
    iput v2, p0, Landroid/os/health/HealthKeys$SortedIntArray;->mCount:I

    goto/32 :goto_2

    nop

    :goto_2
    aput p1, v0, v1

    goto/32 :goto_0

    nop

    :goto_3
    add-int/lit8 v2, v1, 0x1

    goto/32 :goto_1

    nop

    :goto_4
    iget-object v0, p0, Landroid/os/health/HealthKeys$SortedIntArray;->mArray:[I

    goto/32 :goto_5

    nop

    :goto_5
    iget v1, p0, Landroid/os/health/HealthKeys$SortedIntArray;->mCount:I

    goto/32 :goto_3

    nop
.end method

.method getArray()[I
    .locals 4

    goto/32 :goto_2

    nop

    :goto_0
    return-object v0

    :goto_1
    goto/32 :goto_3

    nop

    :goto_2
    iget v0, p0, Landroid/os/health/HealthKeys$SortedIntArray;->mCount:I

    goto/32 :goto_4

    nop

    :goto_3
    new-array v2, v0, [I

    goto/32 :goto_b

    nop

    :goto_4
    iget-object v1, p0, Landroid/os/health/HealthKeys$SortedIntArray;->mArray:[I

    goto/32 :goto_7

    nop

    :goto_5
    invoke-static {v1, v3, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto/32 :goto_8

    nop

    :goto_6
    if-eq v0, v2, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_a

    nop

    :goto_7
    array-length v2, v1

    goto/32 :goto_6

    nop

    :goto_8
    invoke-static {v2}, Ljava/util/Arrays;->sort([I)V

    goto/32 :goto_9

    nop

    :goto_9
    return-object v2

    :goto_a
    invoke-static {v1}, Ljava/util/Arrays;->sort([I)V

    goto/32 :goto_c

    nop

    :goto_b
    const/4 v3, 0x0

    goto/32 :goto_5

    nop

    :goto_c
    iget-object v0, p0, Landroid/os/health/HealthKeys$SortedIntArray;->mArray:[I

    goto/32 :goto_0

    nop
.end method
