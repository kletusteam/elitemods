.class public interface abstract Landroid/os/TurboSchedMonitor;
.super Ljava/lang/Object;


# direct methods
.method public static getInstance()Landroid/os/TurboSchedMonitor;
    .locals 1

    const-class v0, Landroid/os/TurboSchedMonitor;

    invoke-static {v0}, Lcom/miui/base/MiuiStubUtil;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/TurboSchedMonitor;

    return-object v0
.end method


# virtual methods
.method public addTid(I)V
    .locals 0

    return-void
.end method

.method public breakThermlimit(IJ)V
    .locals 0

    return-void
.end method

.method public enableCoreAppOptimizer(I)V
    .locals 0

    return-void
.end method

.method public getAppToken()V
    .locals 0

    return-void
.end method

.method public getBoostDuration()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getCoreAppFrameDealyThreshold()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getCoreAppList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public handleCallbackDelay()V
    .locals 0

    return-void
.end method

.method public isCoreApp(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isCoreAppOptimizerEnabled()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isDebugMode()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public monitorAnimationCallback(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public monitorFrameDelay(J)V
    .locals 0

    return-void
.end method

.method public monitorInputEvent(Landroid/view/InputEvent;)V
    .locals 0

    return-void
.end method

.method public notifyOnScroll(Z)V
    .locals 0

    return-void
.end method

.method public recordFrameStart(IJJ)V
    .locals 0

    return-void
.end method

.method public releaseAppToken()V
    .locals 0

    return-void
.end method

.method public setBoostDuration(IJ)V
    .locals 0

    return-void
.end method

.method public setCoreAppFrameDealyThreshold(IF)V
    .locals 0

    return-void
.end method

.method public setDebugMode(I)V
    .locals 0

    return-void
.end method

.method public setDrawFrameFinished()V
    .locals 0

    return-void
.end method

.method public setForegroundActivity(Landroid/content/Context;)V
    .locals 0

    return-void
.end method

.method public triggerBoostAction()V
    .locals 0

    return-void
.end method

.method public triggerBoostAction(Ljava/lang/String;I)V
    .locals 0

    return-void
.end method
