.class public final Landroid/os/Message;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Landroid/os/Message;",
            ">;"
        }
    .end annotation
.end field

.field static final FLAGS_TO_CLEAR_ON_COPY_FROM:I = 0x1

.field static final FLAG_ASYNCHRONOUS:I = 0x2

.field static final FLAG_IN_USE:I = 0x1

.field private static final MAX_POOL_SIZE:I = 0x32

.field public static final UID_NONE:I = -0x1

.field private static gCheckRecycle:Z

.field private static sPool:Landroid/os/Message;

.field private static sPoolSize:I

.field public static final sPoolSync:Ljava/lang/Object;


# instance fields
.field public arg1:I

.field public arg2:I

.field callback:Ljava/lang/Runnable;

.field data:Landroid/os/Bundle;

.field flags:I

.field monitorInfo:Landroid/os/perfdebug/MessageMonitor$MessageMonitorInfo;

.field next:Landroid/os/Message;

.field public obj:Ljava/lang/Object;

.field public replyTo:Landroid/os/Messenger;

.field public sendingUid:I

.field target:Landroid/os/Handler;

.field public what:I

.field public when:J

.field public workSourceUid:I


# direct methods
.method static bridge synthetic -$$Nest$mreadFromParcel(Landroid/os/Message;Landroid/os/Parcel;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/os/Message;->readFromParcel(Landroid/os/Parcel;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid/os/Message;->sPoolSync:Ljava/lang/Object;

    const/4 v0, 0x0

    sput v0, Landroid/os/Message;->sPoolSize:I

    const/4 v0, 0x1

    sput-boolean v0, Landroid/os/Message;->gCheckRecycle:Z

    new-instance v0, Landroid/os/Message$1;

    invoke-direct {v0}, Landroid/os/Message$1;-><init>()V

    sput-object v0, Landroid/os/Message;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Landroid/os/Message;->sendingUid:I

    iput v0, p0, Landroid/os/Message;->workSourceUid:I

    invoke-static {}, Landroid/os/perfdebug/MessageMonitor;->newMessageMonitorInfo()Landroid/os/perfdebug/MessageMonitor$MessageMonitorInfo;

    move-result-object v0

    iput-object v0, p0, Landroid/os/Message;->monitorInfo:Landroid/os/perfdebug/MessageMonitor$MessageMonitorInfo;

    return-void
.end method

.method public static obtain()Landroid/os/Message;
    .locals 3

    sget-object v0, Landroid/os/Message;->sPoolSync:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    sget-object v1, Landroid/os/Message;->sPool:Landroid/os/Message;

    if-eqz v1, :cond_0

    nop

    iget-object v2, v1, Landroid/os/Message;->next:Landroid/os/Message;

    sput-object v2, Landroid/os/Message;->sPool:Landroid/os/Message;

    const/4 v2, 0x0

    iput-object v2, v1, Landroid/os/Message;->next:Landroid/os/Message;

    const/4 v2, 0x0

    iput v2, v1, Landroid/os/Message;->flags:I

    sget v2, Landroid/os/Message;->sPoolSize:I

    add-int/lit8 v2, v2, -0x1

    sput v2, Landroid/os/Message;->sPoolSize:I

    monitor-exit v0

    return-object v1

    :cond_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    return-object v0

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public static obtain(Landroid/os/Handler;)Landroid/os/Message;
    .locals 1

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    iput-object p0, v0, Landroid/os/Message;->target:Landroid/os/Handler;

    return-object v0
.end method

.method public static obtain(Landroid/os/Handler;I)Landroid/os/Message;
    .locals 1

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    iput-object p0, v0, Landroid/os/Message;->target:Landroid/os/Handler;

    iput p1, v0, Landroid/os/Message;->what:I

    return-object v0
.end method

.method public static obtain(Landroid/os/Handler;III)Landroid/os/Message;
    .locals 1

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    iput-object p0, v0, Landroid/os/Message;->target:Landroid/os/Handler;

    iput p1, v0, Landroid/os/Message;->what:I

    iput p2, v0, Landroid/os/Message;->arg1:I

    iput p3, v0, Landroid/os/Message;->arg2:I

    return-object v0
.end method

.method public static obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;
    .locals 1

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    iput-object p0, v0, Landroid/os/Message;->target:Landroid/os/Handler;

    iput p1, v0, Landroid/os/Message;->what:I

    iput p2, v0, Landroid/os/Message;->arg1:I

    iput p3, v0, Landroid/os/Message;->arg2:I

    iput-object p4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    return-object v0
.end method

.method public static obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
    .locals 1

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    iput-object p0, v0, Landroid/os/Message;->target:Landroid/os/Handler;

    iput p1, v0, Landroid/os/Message;->what:I

    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    return-object v0
.end method

.method public static obtain(Landroid/os/Handler;Ljava/lang/Runnable;)Landroid/os/Message;
    .locals 1

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    iput-object p0, v0, Landroid/os/Message;->target:Landroid/os/Handler;

    iput-object p1, v0, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static obtain(Landroid/os/Message;)Landroid/os/Message;
    .locals 3

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    iget v1, p0, Landroid/os/Message;->what:I

    iput v1, v0, Landroid/os/Message;->what:I

    iget v1, p0, Landroid/os/Message;->arg1:I

    iput v1, v0, Landroid/os/Message;->arg1:I

    iget v1, p0, Landroid/os/Message;->arg2:I

    iput v1, v0, Landroid/os/Message;->arg2:I

    iget-object v1, p0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    iput-object v1, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    iget v1, p0, Landroid/os/Message;->sendingUid:I

    iput v1, v0, Landroid/os/Message;->sendingUid:I

    iget v1, p0, Landroid/os/Message;->workSourceUid:I

    iput v1, v0, Landroid/os/Message;->workSourceUid:I

    iget-object v1, p0, Landroid/os/Message;->data:Landroid/os/Bundle;

    if-eqz v1, :cond_0

    new-instance v1, Landroid/os/Bundle;

    iget-object v2, p0, Landroid/os/Message;->data:Landroid/os/Bundle;

    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    iput-object v1, v0, Landroid/os/Message;->data:Landroid/os/Bundle;

    :cond_0
    iget-object v1, p0, Landroid/os/Message;->target:Landroid/os/Handler;

    iput-object v1, v0, Landroid/os/Message;->target:Landroid/os/Handler;

    iget-object v1, p0, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    iput-object v1, v0, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    return-object v0
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/os/Message;->what:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/os/Message;->arg1:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/os/Message;->arg2:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-class v1, Ljava/lang/Object;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/os/Message;->obj:Ljava/lang/Object;

    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/os/Message;->when:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Landroid/os/Message;->data:Landroid/os/Bundle;

    invoke-static {p1}, Landroid/os/Messenger;->readMessengerOrNullFromParcel(Landroid/os/Parcel;)Landroid/os/Messenger;

    move-result-object v0

    iput-object v0, p0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/os/Message;->sendingUid:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/os/Message;->workSourceUid:I

    return-void
.end method

.method public static updateCheckRecycle(I)V
    .locals 1

    const/16 v0, 0x15

    if-ge p0, v0, :cond_0

    const/4 v0, 0x0

    sput-boolean v0, Landroid/os/Message;->gCheckRecycle:Z

    :cond_0
    return-void
.end method


# virtual methods
.method public copyFrom(Landroid/os/Message;)V
    .locals 1

    iget v0, p1, Landroid/os/Message;->flags:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Landroid/os/Message;->flags:I

    iget v0, p1, Landroid/os/Message;->what:I

    iput v0, p0, Landroid/os/Message;->what:I

    iget v0, p1, Landroid/os/Message;->arg1:I

    iput v0, p0, Landroid/os/Message;->arg1:I

    iget v0, p1, Landroid/os/Message;->arg2:I

    iput v0, p0, Landroid/os/Message;->arg2:I

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    iput-object v0, p0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v0, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    iput-object v0, p0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    iget v0, p1, Landroid/os/Message;->sendingUid:I

    iput v0, p0, Landroid/os/Message;->sendingUid:I

    iget v0, p1, Landroid/os/Message;->workSourceUid:I

    iput v0, p0, Landroid/os/Message;->workSourceUid:I

    iget-object v0, p1, Landroid/os/Message;->data:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/Bundle;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    iput-object v0, p0, Landroid/os/Message;->data:Landroid/os/Bundle;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/os/Message;->data:Landroid/os/Bundle;

    :goto_0
    return-void
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method dumpDebug(Landroid/util/proto/ProtoOutputStream;J)V
    .locals 6

    goto/32 :goto_1c

    nop

    :goto_0
    invoke-virtual {p1, v2, v3, v4}, Landroid/util/proto/ProtoOutputStream;->write(JLjava/lang/String;)V

    goto/32 :goto_5

    nop

    :goto_1
    return-void

    :goto_2
    if-nez v2, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_19

    nop

    :goto_3
    invoke-virtual {p1, v3, v4, v2}, Landroid/util/proto/ProtoOutputStream;->write(JI)V

    :goto_4
    goto/32 :goto_25

    nop

    :goto_5
    goto :goto_14

    :goto_6
    goto/32 :goto_f

    nop

    :goto_7
    iget v2, p0, Landroid/os/Message;->arg2:I

    goto/32 :goto_22

    nop

    :goto_8
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    goto/32 :goto_10

    nop

    :goto_9
    iget v4, p0, Landroid/os/Message;->what:I

    goto/32 :goto_20

    nop

    :goto_a
    const-wide v2, 0x10900000007L

    goto/32 :goto_b

    nop

    :goto_b
    iget-object v4, p0, Landroid/os/Message;->target:Landroid/os/Handler;

    goto/32 :goto_27

    nop

    :goto_c
    const-wide v4, 0x10300000001L

    goto/32 :goto_18

    nop

    :goto_d
    if-nez v2, :cond_1

    goto/32 :goto_2c

    :cond_1
    goto/32 :goto_e

    nop

    :goto_e
    const-wide v3, 0x10900000002L

    goto/32 :goto_8

    nop

    :goto_f
    const-wide v2, 0x10500000008L

    goto/32 :goto_29

    nop

    :goto_10
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_2d

    nop

    :goto_11
    invoke-virtual {p1, v3, v4, v2}, Landroid/util/proto/ProtoOutputStream;->write(JLjava/lang/String;)V

    :goto_12
    goto/32 :goto_a

    nop

    :goto_13
    invoke-virtual {p1, v2, v3, v4}, Landroid/util/proto/ProtoOutputStream;->write(JI)V

    :goto_14
    goto/32 :goto_1b

    nop

    :goto_15
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_11

    nop

    :goto_16
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_0

    nop

    :goto_17
    if-nez v2, :cond_2

    goto/32 :goto_24

    :cond_2
    goto/32 :goto_28

    nop

    :goto_18
    invoke-virtual {p1, v4, v5, v2, v3}, Landroid/util/proto/ProtoOutputStream;->write(JJ)V

    goto/32 :goto_2a

    nop

    :goto_19
    iget-object v2, p0, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    goto/32 :goto_d

    nop

    :goto_1a
    iget-wide v2, p0, Landroid/os/Message;->when:J

    goto/32 :goto_c

    nop

    :goto_1b
    invoke-virtual {p1, v0, v1}, Landroid/util/proto/ProtoOutputStream;->end(J)V

    goto/32 :goto_1

    nop

    :goto_1c
    invoke-virtual {p1, p2, p3}, Landroid/util/proto/ProtoOutputStream;->start(J)J

    move-result-wide v0

    goto/32 :goto_1a

    nop

    :goto_1d
    const-wide v3, 0x10500000005L

    goto/32 :goto_3

    nop

    :goto_1e
    if-nez v2, :cond_3

    goto/32 :goto_12

    :cond_3
    goto/32 :goto_1f

    nop

    :goto_1f
    const-wide v3, 0x10900000006L

    goto/32 :goto_15

    nop

    :goto_20
    invoke-virtual {p1, v2, v3, v4}, Landroid/util/proto/ProtoOutputStream;->write(JI)V

    :goto_21
    goto/32 :goto_26

    nop

    :goto_22
    if-nez v2, :cond_4

    goto/32 :goto_4

    :cond_4
    goto/32 :goto_1d

    nop

    :goto_23
    invoke-virtual {p1, v3, v4, v2}, Landroid/util/proto/ProtoOutputStream;->write(JI)V

    :goto_24
    goto/32 :goto_7

    nop

    :goto_25
    iget-object v2, p0, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto/32 :goto_1e

    nop

    :goto_26
    iget v2, p0, Landroid/os/Message;->arg1:I

    goto/32 :goto_17

    nop

    :goto_27
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    goto/32 :goto_16

    nop

    :goto_28
    const-wide v3, 0x10500000004L

    goto/32 :goto_23

    nop

    :goto_29
    iget v4, p0, Landroid/os/Message;->arg1:I

    goto/32 :goto_13

    nop

    :goto_2a
    iget-object v2, p0, Landroid/os/Message;->target:Landroid/os/Handler;

    goto/32 :goto_2

    nop

    :goto_2b
    goto :goto_21

    :goto_2c
    goto/32 :goto_2e

    nop

    :goto_2d
    invoke-virtual {p1, v3, v4, v2}, Landroid/util/proto/ProtoOutputStream;->write(JLjava/lang/String;)V

    goto/32 :goto_2b

    nop

    :goto_2e
    const-wide v2, 0x10500000003L

    goto/32 :goto_9

    nop
.end method

.method public getCallback()Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    return-object v0
.end method

.method public getData()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Landroid/os/Message;->data:Landroid/os/Bundle;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Landroid/os/Message;->data:Landroid/os/Bundle;

    :cond_0
    iget-object v0, p0, Landroid/os/Message;->data:Landroid/os/Bundle;

    return-object v0
.end method

.method public getTarget()Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Landroid/os/Message;->target:Landroid/os/Handler;

    return-object v0
.end method

.method public getWhen()J
    .locals 2

    iget-wide v0, p0, Landroid/os/Message;->when:J

    return-wide v0
.end method

.method public isAsynchronous()Z
    .locals 1

    iget v0, p0, Landroid/os/Message;->flags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method isInUse()Z
    .locals 2

    goto/32 :goto_6

    nop

    :goto_0
    const/4 v1, 0x0

    :goto_1
    goto/32 :goto_5

    nop

    :goto_2
    and-int/2addr v0, v1

    goto/32 :goto_8

    nop

    :goto_3
    goto :goto_1

    :goto_4
    goto/32 :goto_0

    nop

    :goto_5
    return v1

    :goto_6
    iget v0, p0, Landroid/os/Message;->flags:I

    goto/32 :goto_7

    nop

    :goto_7
    const/4 v1, 0x1

    goto/32 :goto_2

    nop

    :goto_8
    if-eq v0, v1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_3

    nop
.end method

.method markInUse()V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p0, Landroid/os/Message;->flags:I

    goto/32 :goto_3

    nop

    :goto_1
    iput v0, p0, Landroid/os/Message;->flags:I

    goto/32 :goto_2

    nop

    :goto_2
    return-void

    :goto_3
    or-int/lit8 v0, v0, 0x1

    goto/32 :goto_1

    nop
.end method

.method public peekData()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Landroid/os/Message;->data:Landroid/os/Bundle;

    return-object v0
.end method

.method public recycle()V
    .locals 2

    invoke-virtual {p0}, Landroid/os/Message;->isInUse()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-boolean v0, Landroid/os/Message;->gCheckRecycle:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This message cannot be recycled because it is still in use."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p0}, Landroid/os/Message;->recycleUnchecked()V

    return-void
.end method

.method recycleUnchecked()V
    .locals 4

    goto/32 :goto_12

    nop

    :goto_0
    const-wide/16 v2, 0x0

    goto/32 :goto_f

    nop

    :goto_1
    iput-object v1, p0, Landroid/os/Message;->data:Landroid/os/Bundle;

    goto/32 :goto_3

    nop

    :goto_2
    monitor-enter v1

    :try_start_0
    sget v2, Landroid/os/Message;->sPoolSize:I

    const/16 v3, 0x32

    if-ge v2, v3, :cond_0

    sget-object v3, Landroid/os/Message;->sPool:Landroid/os/Message;

    iput-object v3, p0, Landroid/os/Message;->next:Landroid/os/Message;

    sput-object p0, Landroid/os/Message;->sPool:Landroid/os/Message;

    add-int/2addr v2, v0

    sput v2, Landroid/os/Message;->sPoolSize:I

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_10

    nop

    :goto_3
    invoke-static {}, Landroid/os/perfdebug/MessageMonitor;->newMessageMonitorInfo()Landroid/os/perfdebug/MessageMonitor$MessageMonitorInfo;

    move-result-object v1

    goto/32 :goto_d

    nop

    :goto_4
    iput v1, p0, Landroid/os/Message;->arg2:I

    goto/32 :goto_11

    nop

    :goto_5
    iput v2, p0, Landroid/os/Message;->workSourceUid:I

    goto/32 :goto_0

    nop

    :goto_6
    iput v1, p0, Landroid/os/Message;->arg1:I

    goto/32 :goto_4

    nop

    :goto_7
    const/4 v2, -0x1

    goto/32 :goto_e

    nop

    :goto_8
    iput-object v1, p0, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto/32 :goto_9

    nop

    :goto_9
    iput-object v1, p0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    goto/32 :goto_7

    nop

    :goto_a
    iput v0, p0, Landroid/os/Message;->flags:I

    goto/32 :goto_14

    nop

    :goto_b
    iput-object v1, p0, Landroid/os/Message;->target:Landroid/os/Handler;

    goto/32 :goto_15

    nop

    :goto_c
    sget-object v1, Landroid/os/Message;->sPoolSync:Ljava/lang/Object;

    goto/32 :goto_2

    nop

    :goto_d
    iput-object v1, p0, Landroid/os/Message;->monitorInfo:Landroid/os/perfdebug/MessageMonitor$MessageMonitorInfo;

    goto/32 :goto_c

    nop

    :goto_e
    iput v2, p0, Landroid/os/Message;->sendingUid:I

    goto/32 :goto_5

    nop

    :goto_f
    iput-wide v2, p0, Landroid/os/Message;->when:J

    goto/32 :goto_b

    nop

    :goto_10
    throw v0

    :goto_11
    const/4 v1, 0x0

    goto/32 :goto_8

    nop

    :goto_12
    const/4 v0, 0x1

    goto/32 :goto_a

    nop

    :goto_13
    iput v1, p0, Landroid/os/Message;->what:I

    goto/32 :goto_6

    nop

    :goto_14
    const/4 v1, 0x0

    goto/32 :goto_13

    nop

    :goto_15
    iput-object v1, p0, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    goto/32 :goto_1

    nop
.end method

.method public sendToTarget()V
    .locals 1

    iget-object v0, p0, Landroid/os/Message;->target:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public setAsynchronous(Z)V
    .locals 1

    if-eqz p1, :cond_0

    iget v0, p0, Landroid/os/Message;->flags:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Landroid/os/Message;->flags:I

    goto :goto_0

    :cond_0
    iget v0, p0, Landroid/os/Message;->flags:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Landroid/os/Message;->flags:I

    :goto_0
    return-void
.end method

.method public setCallback(Ljava/lang/Runnable;)Landroid/os/Message;
    .locals 0

    iput-object p1, p0, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    return-object p0
.end method

.method public setData(Landroid/os/Bundle;)V
    .locals 0

    iput-object p1, p0, Landroid/os/Message;->data:Landroid/os/Bundle;

    return-void
.end method

.method public setTarget(Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Landroid/os/Message;->target:Landroid/os/Handler;

    return-void
.end method

.method public setWhat(I)Landroid/os/Message;
    .locals 0

    iput p1, p0, Landroid/os/Message;->what:I

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Landroid/os/Message;->toString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method toString(J)Ljava/lang/String;
    .locals 3

    goto/32 :goto_32

    nop

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :goto_1
    goto/32 :goto_15

    nop

    :goto_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_18

    nop

    :goto_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_3b

    nop

    :goto_4
    const-string/jumbo v1, "{ when="

    goto/32 :goto_24

    nop

    :goto_5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_25

    nop

    :goto_6
    iget-wide v1, p0, Landroid/os/Message;->when:J

    goto/32 :goto_1e

    nop

    :goto_7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_10

    nop

    :goto_8
    const-string v1, " target="

    goto/32 :goto_2

    nop

    :goto_9
    const-string v1, " arg1="

    goto/32 :goto_2c

    nop

    :goto_a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_3a

    nop

    :goto_b
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_7

    nop

    :goto_c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_22

    nop

    :goto_d
    invoke-static {v1, v2, v0}, Landroid/util/TimeUtils;->formatDuration(JLjava/lang/StringBuilder;)V

    goto/32 :goto_e

    nop

    :goto_e
    iget-object v1, p0, Landroid/os/Message;->target:Landroid/os/Handler;

    goto/32 :goto_28

    nop

    :goto_f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_2e

    nop

    :goto_10
    goto/16 :goto_35

    :goto_11
    goto/32 :goto_12

    nop

    :goto_12
    const-string v1, " barrier="

    goto/32 :goto_5

    nop

    :goto_13
    iget v1, p0, Landroid/os/Message;->arg2:I

    goto/32 :goto_17

    nop

    :goto_14
    if-nez v1, :cond_0

    goto/32 :goto_3c

    :cond_0
    goto/32 :goto_1d

    nop

    :goto_15
    iget-object v1, p0, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto/32 :goto_39

    nop

    :goto_16
    iget v1, p0, Landroid/os/Message;->arg1:I

    goto/32 :goto_26

    nop

    :goto_17
    if-nez v1, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_37

    nop

    :goto_18
    iget-object v1, p0, Landroid/os/Message;->target:Landroid/os/Handler;

    goto/32 :goto_2d

    nop

    :goto_19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_1a
    goto/32 :goto_8

    nop

    :goto_1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_31

    nop

    :goto_1c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_f

    nop

    :goto_1d
    const-string v1, " callback="

    goto/32 :goto_36

    nop

    :goto_1e
    sub-long/2addr v1, p1

    goto/32 :goto_d

    nop

    :goto_1f
    const-string v1, " }"

    goto/32 :goto_1c

    nop

    :goto_20
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_3

    nop

    :goto_21
    iget v1, p0, Landroid/os/Message;->arg1:I

    goto/32 :goto_38

    nop

    :goto_22
    iget v1, p0, Landroid/os/Message;->arg2:I

    goto/32 :goto_0

    nop

    :goto_23
    const-string v1, " what="

    goto/32 :goto_1b

    nop

    :goto_24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_25
    iget v1, p0, Landroid/os/Message;->arg1:I

    goto/32 :goto_34

    nop

    :goto_26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :goto_27
    goto/32 :goto_13

    nop

    :goto_28
    if-nez v1, :cond_2

    goto/32 :goto_11

    :cond_2
    goto/32 :goto_33

    nop

    :goto_29
    iget-object v1, p0, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    goto/32 :goto_3d

    nop

    :goto_2a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :goto_2b
    goto/32 :goto_21

    nop

    :goto_2c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_16

    nop

    :goto_2d
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    goto/32 :goto_b

    nop

    :goto_2e
    return-object v1

    :goto_2f
    const-string v1, " obj="

    goto/32 :goto_a

    nop

    :goto_30
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_4

    nop

    :goto_31
    iget v1, p0, Landroid/os/Message;->what:I

    goto/32 :goto_2a

    nop

    :goto_32
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_30

    nop

    :goto_33
    iget-object v1, p0, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    goto/32 :goto_14

    nop

    :goto_34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :goto_35
    goto/32 :goto_1f

    nop

    :goto_36
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_29

    nop

    :goto_37
    const-string v1, " arg2="

    goto/32 :goto_c

    nop

    :goto_38
    if-nez v1, :cond_3

    goto/32 :goto_27

    :cond_3
    goto/32 :goto_9

    nop

    :goto_39
    if-nez v1, :cond_4

    goto/32 :goto_1a

    :cond_4
    goto/32 :goto_2f

    nop

    :goto_3a
    iget-object v1, p0, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto/32 :goto_19

    nop

    :goto_3b
    goto :goto_2b

    :goto_3c
    goto/32 :goto_23

    nop

    :goto_3d
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    goto/32 :goto_20

    nop
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    iget-object v0, p0, Landroid/os/Message;->callback:Ljava/lang/Runnable;

    if-nez v0, :cond_1

    iget v0, p0, Landroid/os/Message;->what:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Landroid/os/Message;->arg1:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Landroid/os/Message;->arg2:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v0, :cond_0

    :try_start_0
    check-cast v0, Landroid/os/Parcelable;

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Can\'t marshal non-Parcelable objects across processes."

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    :goto_0
    iget-wide v0, p0, Landroid/os/Message;->when:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Landroid/os/Message;->data:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-static {v0, p1}, Landroid/os/Messenger;->writeMessengerOrNullToParcel(Landroid/os/Messenger;Landroid/os/Parcel;)V

    iget v0, p0, Landroid/os/Message;->sendingUid:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Landroid/os/Message;->workSourceUid:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can\'t marshal callbacks across processes."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
