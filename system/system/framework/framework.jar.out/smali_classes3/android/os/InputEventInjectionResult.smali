.class public interface abstract annotation Landroid/os/InputEventInjectionResult;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final FAILED:I = 0x2

.field public static final PENDING:I = -0x1

.field public static final SUCCEEDED:I = 0x0

.field public static final TARGET_MISMATCH:I = 0x1

.field public static final TIMED_OUT:I = 0x3
