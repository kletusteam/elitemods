.class public Landroid/os/BinderStub;
.super Ljava/lang/Object;


# static fields
.field private static final INSTANCE:Landroid/os/BinderStub;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Landroid/os/BinderStub;

    invoke-static {v0}, Lcom/miui/base/MiuiStubUtil;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/BinderStub;

    sput-object v0, Landroid/os/BinderStub;->INSTANCE:Landroid/os/BinderStub;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static get()Landroid/os/BinderStub;
    .locals 1

    sget-object v0, Landroid/os/BinderStub;->INSTANCE:Landroid/os/BinderStub;

    return-object v0
.end method


# virtual methods
.method public dump(Landroid/os/ParcelFileDescriptor;)V
    .locals 0

    return-void
.end method

.method public isEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public startTracking()V
    .locals 0

    return-void
.end method

.method public stopTracking(Z)V
    .locals 0

    return-void
.end method

.method track(Landroid/os/Binder;)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    return-void
.end method
