.class Landroid/os/BatteryConsumer$BatteryConsumerData;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/BatteryConsumer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "BatteryConsumerData"
.end annotation


# instance fields
.field public final layout:Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;

.field private final mCursorRow:I

.field private final mCursorWindow:Landroid/database/CursorWindow;


# direct methods
.method constructor <init>(Landroid/database/CursorWindow;ILandroid/os/BatteryConsumer$BatteryConsumerDataLayout;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/os/BatteryConsumer$BatteryConsumerData;->mCursorWindow:Landroid/database/CursorWindow;

    iput p2, p0, Landroid/os/BatteryConsumer$BatteryConsumerData;->mCursorRow:I

    iput-object p3, p0, Landroid/os/BatteryConsumer$BatteryConsumerData;->layout:Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;

    return-void
.end method

.method static create(Landroid/database/CursorWindow;Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;)Landroid/os/BatteryConsumer$BatteryConsumerData;
    .locals 3

    invoke-virtual {p0}, Landroid/database/CursorWindow;->getNumRows()I

    move-result v0

    invoke-virtual {p0}, Landroid/database/CursorWindow;->allocRow()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot allocate BatteryConsumerData: too many UIDs: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BatteryConsumer"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, -0x1

    :cond_0
    new-instance v1, Landroid/os/BatteryConsumer$BatteryConsumerData;

    invoke-direct {v1, p0, v0, p1}, Landroid/os/BatteryConsumer$BatteryConsumerData;-><init>(Landroid/database/CursorWindow;ILandroid/os/BatteryConsumer$BatteryConsumerDataLayout;)V

    return-object v1
.end method


# virtual methods
.method getDouble(I)D
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    if-eq v0, v1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_8

    nop

    :goto_1
    iget v0, p0, Landroid/os/BatteryConsumer$BatteryConsumerData;->mCursorRow:I

    goto/32 :goto_4

    nop

    :goto_2
    invoke-virtual {v1, v0, p1}, Landroid/database/CursorWindow;->getDouble(II)D

    move-result-wide v0

    goto/32 :goto_5

    nop

    :goto_3
    iget-object v1, p0, Landroid/os/BatteryConsumer$BatteryConsumerData;->mCursorWindow:Landroid/database/CursorWindow;

    goto/32 :goto_2

    nop

    :goto_4
    const/4 v1, -0x1

    goto/32 :goto_0

    nop

    :goto_5
    return-wide v0

    :goto_6
    return-wide v0

    :goto_7
    goto/32 :goto_3

    nop

    :goto_8
    const-wide/16 v0, 0x0

    goto/32 :goto_6

    nop
.end method

.method getInt(I)I
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p0, Landroid/os/BatteryConsumer$BatteryConsumerData;->mCursorRow:I

    goto/32 :goto_7

    nop

    :goto_1
    return v0

    :goto_2
    return v0

    :goto_3
    goto/32 :goto_5

    nop

    :goto_4
    invoke-virtual {v1, v0, p1}, Landroid/database/CursorWindow;->getInt(II)I

    move-result v0

    goto/32 :goto_1

    nop

    :goto_5
    iget-object v1, p0, Landroid/os/BatteryConsumer$BatteryConsumerData;->mCursorWindow:Landroid/database/CursorWindow;

    goto/32 :goto_4

    nop

    :goto_6
    if-eq v0, v1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_8

    nop

    :goto_7
    const/4 v1, -0x1

    goto/32 :goto_6

    nop

    :goto_8
    const/4 v0, 0x0

    goto/32 :goto_2

    nop
.end method

.method getKey(II)Landroid/os/BatteryConsumer$Key;
    .locals 6

    goto/32 :goto_19

    nop

    :goto_0
    iget-object v2, v2, Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;->keys:[[Landroid/os/BatteryConsumer$Key;

    goto/32 :goto_10

    nop

    :goto_1
    const/4 v1, 0x0

    goto/32 :goto_12

    nop

    :goto_2
    if-eq v5, p2, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_a

    nop

    :goto_3
    if-lt v1, v3, :cond_1

    goto/32 :goto_14

    :cond_1
    goto/32 :goto_4

    nop

    :goto_4
    aget-object v4, v2, v1

    goto/32 :goto_5

    nop

    :goto_5
    iget v5, v4, Landroid/os/BatteryConsumer$Key;->processState:I

    goto/32 :goto_2

    nop

    :goto_6
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_13

    nop

    :goto_7
    array-length v3, v2

    :goto_8
    goto/32 :goto_3

    nop

    :goto_9
    return-object v0

    :goto_a
    return-object v4

    :goto_b
    goto/32 :goto_6

    nop

    :goto_c
    if-ge p1, v1, :cond_2

    goto/32 :goto_f

    :cond_2
    goto/32 :goto_e

    nop

    :goto_d
    iget-object v2, p0, Landroid/os/BatteryConsumer$BatteryConsumerData;->layout:Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;

    goto/32 :goto_0

    nop

    :goto_e
    return-object v0

    :goto_f
    goto/32 :goto_1

    nop

    :goto_10
    aget-object v2, v2, p1

    goto/32 :goto_7

    nop

    :goto_11
    iget-object v0, v0, Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;->keys:[[Landroid/os/BatteryConsumer$Key;

    goto/32 :goto_15

    nop

    :goto_12
    if-eqz p2, :cond_3

    goto/32 :goto_18

    :cond_3
    goto/32 :goto_16

    nop

    :goto_13
    goto :goto_8

    :goto_14
    goto/32 :goto_9

    nop

    :goto_15
    aget-object v0, v0, p1

    goto/32 :goto_1b

    nop

    :goto_16
    iget-object v0, p0, Landroid/os/BatteryConsumer$BatteryConsumerData;->layout:Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;

    goto/32 :goto_11

    nop

    :goto_17
    return-object v0

    :goto_18
    goto/32 :goto_d

    nop

    :goto_19
    const/4 v0, 0x0

    goto/32 :goto_1a

    nop

    :goto_1a
    const/16 v1, 0x12

    goto/32 :goto_c

    nop

    :goto_1b
    aget-object v0, v0, v1

    goto/32 :goto_17

    nop
.end method

.method getKeyOrThrow(II)Landroid/os/BatteryConsumer$Key;
    .locals 4

    goto/32 :goto_17

    nop

    :goto_0
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_e

    nop

    :goto_1
    if-eqz p2, :cond_0

    goto/32 :goto_13

    :cond_0
    goto/32 :goto_9

    nop

    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    goto/32 :goto_7

    nop

    :goto_3
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_18

    nop

    :goto_4
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_6

    nop

    :goto_5
    new-instance v3, Ljava/lang/StringBuilder;

    goto/32 :goto_f

    nop

    :goto_6
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_19

    nop

    :goto_7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_15

    nop

    :goto_8
    const-string v1, "Unsupported power component ID: "

    goto/32 :goto_1

    nop

    :goto_9
    new-instance v2, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_2

    nop

    :goto_a
    new-instance v2, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_5

    nop

    :goto_b
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_c
    invoke-direct {v2, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_10

    nop

    :goto_d
    if-eqz v0, :cond_1

    goto/32 :goto_11

    :cond_1
    goto/32 :goto_8

    nop

    :goto_e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_c

    nop

    :goto_f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_4

    nop

    :goto_10
    throw v2

    :goto_11
    goto/32 :goto_14

    nop

    :goto_12
    throw v2

    :goto_13
    goto/32 :goto_a

    nop

    :goto_14
    return-object v0

    :goto_15
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_3

    nop

    :goto_16
    invoke-direct {v2, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_12

    nop

    :goto_17
    invoke-virtual {p0, p1, p2}, Landroid/os/BatteryConsumer$BatteryConsumerData;->getKey(II)Landroid/os/BatteryConsumer$Key;

    move-result-object v0

    goto/32 :goto_d

    nop

    :goto_18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_16

    nop

    :goto_19
    const-string v3, " process state: "

    goto/32 :goto_b

    nop
.end method

.method public getKeys(I)[Landroid/os/BatteryConsumer$Key;
    .locals 1

    iget-object v0, p0, Landroid/os/BatteryConsumer$BatteryConsumerData;->layout:Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;

    iget-object v0, v0, Landroid/os/BatteryConsumer$BatteryConsumerDataLayout;->keys:[[Landroid/os/BatteryConsumer$Key;

    aget-object v0, v0, p1

    return-object v0
.end method

.method getLong(I)J
    .locals 2

    goto/32 :goto_7

    nop

    :goto_0
    if-eq v0, v1, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_3

    nop

    :goto_1
    iget-object v1, p0, Landroid/os/BatteryConsumer$BatteryConsumerData;->mCursorWindow:Landroid/database/CursorWindow;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {v1, v0, p1}, Landroid/database/CursorWindow;->getLong(II)J

    move-result-wide v0

    goto/32 :goto_6

    nop

    :goto_3
    const-wide/16 v0, 0x0

    goto/32 :goto_4

    nop

    :goto_4
    return-wide v0

    :goto_5
    goto/32 :goto_1

    nop

    :goto_6
    return-wide v0

    :goto_7
    iget v0, p0, Landroid/os/BatteryConsumer$BatteryConsumerData;->mCursorRow:I

    goto/32 :goto_8

    nop

    :goto_8
    const/4 v1, -0x1

    goto/32 :goto_0

    nop
.end method

.method getString(I)Ljava/lang/String;
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p0, Landroid/os/BatteryConsumer$BatteryConsumerData;->mCursorRow:I

    goto/32 :goto_1

    nop

    :goto_1
    const/4 v1, -0x1

    goto/32 :goto_4

    nop

    :goto_2
    return-object v0

    :goto_3
    goto/32 :goto_7

    nop

    :goto_4
    if-eq v0, v1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_8

    nop

    :goto_5
    invoke-virtual {v1, v0, p1}, Landroid/database/CursorWindow;->getString(II)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_6
    return-object v0

    :goto_7
    iget-object v1, p0, Landroid/os/BatteryConsumer$BatteryConsumerData;->mCursorWindow:Landroid/database/CursorWindow;

    goto/32 :goto_5

    nop

    :goto_8
    const/4 v0, 0x0

    goto/32 :goto_2

    nop
.end method

.method putDouble(ID)V
    .locals 2

    goto/32 :goto_6

    nop

    :goto_0
    return-void

    :goto_1
    return-void

    :goto_2
    goto/32 :goto_5

    nop

    :goto_3
    invoke-virtual {v1, p2, p3, v0, p1}, Landroid/database/CursorWindow;->putDouble(DII)Z

    goto/32 :goto_0

    nop

    :goto_4
    const/4 v1, -0x1

    goto/32 :goto_7

    nop

    :goto_5
    iget-object v1, p0, Landroid/os/BatteryConsumer$BatteryConsumerData;->mCursorWindow:Landroid/database/CursorWindow;

    goto/32 :goto_3

    nop

    :goto_6
    iget v0, p0, Landroid/os/BatteryConsumer$BatteryConsumerData;->mCursorRow:I

    goto/32 :goto_4

    nop

    :goto_7
    if-eq v0, v1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop
.end method

.method putInt(II)V
    .locals 4

    goto/32 :goto_1

    nop

    :goto_0
    iget-object v1, p0, Landroid/os/BatteryConsumer$BatteryConsumerData;->mCursorWindow:Landroid/database/CursorWindow;

    goto/32 :goto_8

    nop

    :goto_1
    iget v0, p0, Landroid/os/BatteryConsumer$BatteryConsumerData;->mCursorRow:I

    goto/32 :goto_3

    nop

    :goto_2
    if-eq v0, v1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_5

    nop

    :goto_3
    const/4 v1, -0x1

    goto/32 :goto_2

    nop

    :goto_4
    return-void

    :goto_5
    return-void

    :goto_6
    goto/32 :goto_0

    nop

    :goto_7
    invoke-virtual {v1, v2, v3, v0, p1}, Landroid/database/CursorWindow;->putLong(JII)Z

    goto/32 :goto_4

    nop

    :goto_8
    int-to-long v2, p2

    goto/32 :goto_7

    nop
.end method

.method putLong(IJ)V
    .locals 2

    goto/32 :goto_4

    nop

    :goto_0
    return-void

    :goto_1
    goto/32 :goto_3

    nop

    :goto_2
    if-eq v0, v1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_0

    nop

    :goto_3
    iget-object v1, p0, Landroid/os/BatteryConsumer$BatteryConsumerData;->mCursorWindow:Landroid/database/CursorWindow;

    goto/32 :goto_6

    nop

    :goto_4
    iget v0, p0, Landroid/os/BatteryConsumer$BatteryConsumerData;->mCursorRow:I

    goto/32 :goto_7

    nop

    :goto_5
    return-void

    :goto_6
    invoke-virtual {v1, p2, p3, v0, p1}, Landroid/database/CursorWindow;->putLong(JII)Z

    goto/32 :goto_5

    nop

    :goto_7
    const/4 v1, -0x1

    goto/32 :goto_2

    nop
.end method

.method putString(ILjava/lang/String;)V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p0, Landroid/os/BatteryConsumer$BatteryConsumerData;->mCursorRow:I

    goto/32 :goto_4

    nop

    :goto_1
    iget-object v1, p0, Landroid/os/BatteryConsumer$BatteryConsumerData;->mCursorWindow:Landroid/database/CursorWindow;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {v1, p2, v0, p1}, Landroid/database/CursorWindow;->putString(Ljava/lang/String;II)Z

    goto/32 :goto_5

    nop

    :goto_3
    if-eq v0, v1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_6

    nop

    :goto_4
    const/4 v1, -0x1

    goto/32 :goto_3

    nop

    :goto_5
    return-void

    :goto_6
    return-void

    :goto_7
    goto/32 :goto_1

    nop
.end method
