.class Landroid/telephony/ims/stub/ImsEcbmImplBase$1;
.super Lcom/android/ims/internal/IImsEcbm$Stub;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/telephony/ims/stub/ImsEcbmImplBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/telephony/ims/stub/ImsEcbmImplBase;


# direct methods
.method constructor <init>(Landroid/telephony/ims/stub/ImsEcbmImplBase;)V
    .locals 0

    iput-object p1, p0, Landroid/telephony/ims/stub/ImsEcbmImplBase$1;->this$0:Landroid/telephony/ims/stub/ImsEcbmImplBase;

    invoke-direct {p0}, Lcom/android/ims/internal/IImsEcbm$Stub;-><init>()V

    return-void
.end method

.method private executeMethodAsync(Ljava/lang/Runnable;Ljava/lang/String;)V
    .locals 3

    :try_start_0
    new-instance v0, Landroid/telephony/ims/stub/ImsEcbmImplBase$1$$ExternalSyntheticLambda2;

    invoke-direct {v0, p1}, Landroid/telephony/ims/stub/ImsEcbmImplBase$1$$ExternalSyntheticLambda2;-><init>(Ljava/lang/Runnable;)V

    iget-object v1, p0, Landroid/telephony/ims/stub/ImsEcbmImplBase$1;->this$0:Landroid/telephony/ims/stub/ImsEcbmImplBase;

    invoke-static {v1}, Landroid/telephony/ims/stub/ImsEcbmImplBase;->-$$Nest$fgetmExecutor(Landroid/telephony/ims/stub/ImsEcbmImplBase;)Ljava/util/concurrent/Executor;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/concurrent/CompletableFuture;->runAsync(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)Ljava/util/concurrent/CompletableFuture;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CompletableFuture;->join()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/CompletionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ImsEcbmImplBase Binder - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ImsEcbmImplBase"

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method static synthetic lambda$executeMethodAsync$2(Ljava/lang/Runnable;)V
    .locals 0

    invoke-static {p0}, Lcom/android/internal/telephony/util/TelephonyUtils;->runWithCleanCallingIdentity(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public exitEmergencyCallbackMode()V
    .locals 2

    new-instance v0, Landroid/telephony/ims/stub/ImsEcbmImplBase$1$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Landroid/telephony/ims/stub/ImsEcbmImplBase$1$$ExternalSyntheticLambda0;-><init>(Landroid/telephony/ims/stub/ImsEcbmImplBase$1;)V

    const-string v1, "exitEmergencyCallbackMode"

    invoke-direct {p0, v0, v1}, Landroid/telephony/ims/stub/ImsEcbmImplBase$1;->executeMethodAsync(Ljava/lang/Runnable;Ljava/lang/String;)V

    return-void
.end method

.method synthetic lambda$exitEmergencyCallbackMode$1$android-telephony-ims-stub-ImsEcbmImplBase$1()V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v0, p0, Landroid/telephony/ims/stub/ImsEcbmImplBase$1;->this$0:Landroid/telephony/ims/stub/ImsEcbmImplBase;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {v0}, Landroid/telephony/ims/stub/ImsEcbmImplBase;->exitEmergencyCallbackMode()V

    goto/32 :goto_0

    nop
.end method

.method synthetic lambda$setListener$0$android-telephony-ims-stub-ImsEcbmImplBase$1(Lcom/android/ims/internal/IImsEcbmListener;)V
    .locals 4

    goto/32 :goto_9

    nop

    :goto_0
    invoke-interface {v0}, Lcom/android/ims/internal/IImsEcbmListener;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_1
    invoke-static {v0}, Landroid/telephony/ims/stub/ImsEcbmImplBase;->-$$Nest$fgetmListener(Landroid/telephony/ims/stub/ImsEcbmImplBase;)Lcom/android/ims/internal/IImsEcbmListener;

    move-result-object v0

    goto/32 :goto_b

    nop

    :goto_2
    iget-object v0, p0, Landroid/telephony/ims/stub/ImsEcbmImplBase$1;->this$0:Landroid/telephony/ims/stub/ImsEcbmImplBase;

    goto/32 :goto_24

    nop

    :goto_3
    invoke-static {v0, v1}, Landroid/telephony/ims/stub/ImsEcbmImplBase;->-$$Nest$fputmListener(Landroid/telephony/ims/stub/ImsEcbmImplBase;Lcom/android/ims/internal/IImsEcbmListener;)V

    goto/32 :goto_d

    nop

    :goto_4
    iget-object v0, p0, Landroid/telephony/ims/stub/ImsEcbmImplBase$1;->this$0:Landroid/telephony/ims/stub/ImsEcbmImplBase;

    goto/32 :goto_7

    nop

    :goto_5
    invoke-interface {p1}, Lcom/android/ims/internal/IImsEcbmListener;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    goto/32 :goto_2d

    nop

    :goto_6
    const/4 v1, 0x0

    goto/32 :goto_23

    nop

    :goto_7
    invoke-static {v0, p1}, Landroid/telephony/ims/stub/ImsEcbmImplBase;->-$$Nest$fputmListener(Landroid/telephony/ims/stub/ImsEcbmImplBase;Lcom/android/ims/internal/IImsEcbmListener;)V

    goto/32 :goto_28

    nop

    :goto_8
    invoke-interface {v0}, Landroid/os/IBinder;->isBinderAlive()Z

    move-result v0

    goto/32 :goto_1f

    nop

    :goto_9
    iget-object v0, p0, Landroid/telephony/ims/stub/ImsEcbmImplBase$1;->this$0:Landroid/telephony/ims/stub/ImsEcbmImplBase;

    goto/32 :goto_1d

    nop

    :goto_a
    if-nez p1, :cond_0

    goto/32 :goto_29

    :cond_0
    goto/32 :goto_f

    nop

    :goto_b
    if-nez v0, :cond_1

    goto/32 :goto_11

    :cond_1
    goto/32 :goto_22

    nop

    :goto_c
    iget-object v0, p0, Landroid/telephony/ims/stub/ImsEcbmImplBase$1;->this$0:Landroid/telephony/ims/stub/ImsEcbmImplBase;

    goto/32 :goto_19

    nop

    :goto_d
    goto/16 :goto_25

    :goto_e
    goto/32 :goto_a

    nop

    :goto_f
    iget-object v0, p0, Landroid/telephony/ims/stub/ImsEcbmImplBase$1;->this$0:Landroid/telephony/ims/stub/ImsEcbmImplBase;

    goto/32 :goto_20

    nop

    :goto_10
    return-void

    :goto_11
    goto/32 :goto_27

    nop

    :goto_12
    if-nez v0, :cond_2

    goto/32 :goto_2b

    :cond_2
    goto/32 :goto_c

    nop

    :goto_13
    invoke-static {v0}, Landroid/telephony/ims/stub/ImsEcbmImplBase;->-$$Nest$fgetmListener(Landroid/telephony/ims/stub/ImsEcbmImplBase;)Lcom/android/ims/internal/IImsEcbmListener;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_14
    iget-object v0, p0, Landroid/telephony/ims/stub/ImsEcbmImplBase$1;->this$0:Landroid/telephony/ims/stub/ImsEcbmImplBase;

    goto/32 :goto_3

    nop

    :goto_15
    const-string/jumbo v0, "setListener is being called when there is already an active listener"

    goto/32 :goto_1e

    nop

    :goto_16
    return-void

    :goto_17
    const-string/jumbo v0, "setListener: discarding dead Binder"

    goto/32 :goto_18

    nop

    :goto_18
    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_1c

    nop

    :goto_19
    invoke-static {v0}, Landroid/telephony/ims/stub/ImsEcbmImplBase;->-$$Nest$fgetmListener(Landroid/telephony/ims/stub/ImsEcbmImplBase;)Lcom/android/ims/internal/IImsEcbmListener;

    move-result-object v0

    goto/32 :goto_1b

    nop

    :goto_1a
    if-nez v0, :cond_3

    goto/32 :goto_11

    :cond_3
    goto/32 :goto_10

    nop

    :goto_1b
    invoke-interface {v0}, Lcom/android/ims/internal/IImsEcbmListener;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_1c
    iget-object v0, p0, Landroid/telephony/ims/stub/ImsEcbmImplBase$1;->this$0:Landroid/telephony/ims/stub/ImsEcbmImplBase;

    goto/32 :goto_2a

    nop

    :goto_1d
    invoke-static {v0}, Landroid/telephony/ims/stub/ImsEcbmImplBase;->-$$Nest$fgetmListener(Landroid/telephony/ims/stub/ImsEcbmImplBase;)Lcom/android/ims/internal/IImsEcbmListener;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_1e
    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_2

    nop

    :goto_1f
    if-eqz v0, :cond_4

    goto/32 :goto_2b

    :cond_4
    goto/32 :goto_17

    nop

    :goto_20
    invoke-static {v0}, Landroid/telephony/ims/stub/ImsEcbmImplBase;->-$$Nest$fgetmListener(Landroid/telephony/ims/stub/ImsEcbmImplBase;)Lcom/android/ims/internal/IImsEcbmListener;

    move-result-object v0

    goto/32 :goto_21

    nop

    :goto_21
    if-eqz v0, :cond_5

    goto/32 :goto_29

    :cond_5
    goto/32 :goto_4

    nop

    :goto_22
    if-nez p1, :cond_6

    goto/32 :goto_11

    :cond_6
    goto/32 :goto_2c

    nop

    :goto_23
    const-string v2, "ImsEcbmImplBase"

    goto/32 :goto_12

    nop

    :goto_24
    invoke-static {v0, p1}, Landroid/telephony/ims/stub/ImsEcbmImplBase;->-$$Nest$fputmListener(Landroid/telephony/ims/stub/ImsEcbmImplBase;Lcom/android/ims/internal/IImsEcbmListener;)V

    :goto_25
    goto/32 :goto_16

    nop

    :goto_26
    iget-object v0, p0, Landroid/telephony/ims/stub/ImsEcbmImplBase$1;->this$0:Landroid/telephony/ims/stub/ImsEcbmImplBase;

    goto/32 :goto_1

    nop

    :goto_27
    if-eqz p1, :cond_7

    goto/32 :goto_e

    :cond_7
    goto/32 :goto_14

    nop

    :goto_28
    goto :goto_25

    :goto_29
    goto/32 :goto_15

    nop

    :goto_2a
    invoke-static {v0, v1}, Landroid/telephony/ims/stub/ImsEcbmImplBase;->-$$Nest$fputmListener(Landroid/telephony/ims/stub/ImsEcbmImplBase;Lcom/android/ims/internal/IImsEcbmListener;)V

    :goto_2b
    goto/32 :goto_26

    nop

    :goto_2c
    iget-object v0, p0, Landroid/telephony/ims/stub/ImsEcbmImplBase$1;->this$0:Landroid/telephony/ims/stub/ImsEcbmImplBase;

    goto/32 :goto_13

    nop

    :goto_2d
    invoke-static {v0, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_1a

    nop
.end method

.method public setListener(Lcom/android/ims/internal/IImsEcbmListener;)V
    .locals 2

    new-instance v0, Landroid/telephony/ims/stub/ImsEcbmImplBase$1$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0, p1}, Landroid/telephony/ims/stub/ImsEcbmImplBase$1$$ExternalSyntheticLambda1;-><init>(Landroid/telephony/ims/stub/ImsEcbmImplBase$1;Lcom/android/ims/internal/IImsEcbmListener;)V

    const-string/jumbo v1, "setListener"

    invoke-direct {p0, v0, v1}, Landroid/telephony/ims/stub/ImsEcbmImplBase$1;->executeMethodAsync(Ljava/lang/Runnable;Ljava/lang/String;)V

    return-void
.end method
