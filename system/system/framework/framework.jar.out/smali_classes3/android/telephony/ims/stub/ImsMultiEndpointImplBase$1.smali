.class Landroid/telephony/ims/stub/ImsMultiEndpointImplBase$1;
.super Lcom/android/ims/internal/IImsMultiEndpoint$Stub;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;


# direct methods
.method constructor <init>(Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;)V
    .locals 0

    iput-object p1, p0, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase$1;->this$0:Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;

    invoke-direct {p0}, Lcom/android/ims/internal/IImsMultiEndpoint$Stub;-><init>()V

    return-void
.end method

.method private executeMethodAsync(Ljava/lang/Runnable;Ljava/lang/String;)V
    .locals 3

    :try_start_0
    new-instance v0, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase$1$$ExternalSyntheticLambda2;

    invoke-direct {v0, p1}, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase$1$$ExternalSyntheticLambda2;-><init>(Ljava/lang/Runnable;)V

    iget-object v1, p0, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase$1;->this$0:Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;

    invoke-static {v1}, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;->-$$Nest$fgetmExecutor(Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;)Ljava/util/concurrent/Executor;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/concurrent/CompletableFuture;->runAsync(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)Ljava/util/concurrent/CompletableFuture;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CompletableFuture;->join()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/CompletionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ImsMultiEndpointImplBase Binder - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MultiEndpointImplBase"

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method static synthetic lambda$executeMethodAsync$2(Ljava/lang/Runnable;)V
    .locals 0

    invoke-static {p0}, Lcom/android/internal/telephony/util/TelephonyUtils;->runWithCleanCallingIdentity(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method synthetic lambda$requestImsExternalCallStateInfo$1$android-telephony-ims-stub-ImsMultiEndpointImplBase$1()V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {v0}, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;->requestImsExternalCallStateInfo()V

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase$1;->this$0:Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method synthetic lambda$setListener$0$android-telephony-ims-stub-ImsMultiEndpointImplBase$1(Lcom/android/ims/internal/IImsExternalCallStateListener;)V
    .locals 4

    goto/32 :goto_18

    nop

    :goto_0
    invoke-static {v0}, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;->-$$Nest$fgetmListener(Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;)Lcom/android/ims/internal/IImsExternalCallStateListener;

    move-result-object v0

    goto/32 :goto_1f

    nop

    :goto_1
    iget-object v0, p0, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase$1;->this$0:Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;

    goto/32 :goto_24

    nop

    :goto_2
    invoke-interface {v0}, Lcom/android/ims/internal/IImsExternalCallStateListener;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    goto/32 :goto_19

    nop

    :goto_3
    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_1

    nop

    :goto_4
    const/4 v1, 0x0

    goto/32 :goto_1e

    nop

    :goto_5
    iget-object v0, p0, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase$1;->this$0:Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;

    goto/32 :goto_10

    nop

    :goto_6
    if-nez p1, :cond_0

    goto/32 :goto_1d

    :cond_0
    goto/32 :goto_28

    nop

    :goto_7
    if-eqz p1, :cond_1

    goto/32 :goto_12

    :cond_1
    goto/32 :goto_5

    nop

    :goto_8
    return-void

    :goto_9
    goto/32 :goto_7

    nop

    :goto_a
    if-eqz v0, :cond_2

    goto/32 :goto_25

    :cond_2
    goto/32 :goto_2d

    nop

    :goto_b
    iget-object v0, p0, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase$1;->this$0:Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;

    goto/32 :goto_e

    nop

    :goto_c
    invoke-interface {v0}, Lcom/android/ims/internal/IImsExternalCallStateListener;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    goto/32 :goto_20

    nop

    :goto_d
    iget-object v0, p0, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase$1;->this$0:Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;

    goto/32 :goto_0

    nop

    :goto_e
    invoke-static {v0, p1}, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;->-$$Nest$fputmListener(Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;Lcom/android/ims/internal/IImsExternalCallStateListener;)V

    :goto_f
    goto/32 :goto_2a

    nop

    :goto_10
    invoke-static {v0, v1}, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;->-$$Nest$fputmListener(Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;Lcom/android/ims/internal/IImsExternalCallStateListener;)V

    goto/32 :goto_11

    nop

    :goto_11
    goto :goto_f

    :goto_12
    goto/32 :goto_6

    nop

    :goto_13
    invoke-static {v0, p1}, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;->-$$Nest$fputmListener(Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;Lcom/android/ims/internal/IImsExternalCallStateListener;)V

    goto/32 :goto_1c

    nop

    :goto_14
    if-nez p1, :cond_3

    goto/32 :goto_9

    :cond_3
    goto/32 :goto_17

    nop

    :goto_15
    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_b

    nop

    :goto_16
    invoke-static {v0}, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;->-$$Nest$fgetmListener(Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;)Lcom/android/ims/internal/IImsExternalCallStateListener;

    move-result-object v0

    goto/32 :goto_22

    nop

    :goto_17
    iget-object v0, p0, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase$1;->this$0:Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;

    goto/32 :goto_2b

    nop

    :goto_18
    iget-object v0, p0, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase$1;->this$0:Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;

    goto/32 :goto_29

    nop

    :goto_19
    invoke-interface {v0}, Landroid/os/IBinder;->isBinderAlive()Z

    move-result v0

    goto/32 :goto_a

    nop

    :goto_1a
    iget-object v0, p0, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase$1;->this$0:Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;

    goto/32 :goto_21

    nop

    :goto_1b
    if-nez v0, :cond_4

    goto/32 :goto_25

    :cond_4
    goto/32 :goto_1a

    nop

    :goto_1c
    goto :goto_f

    :goto_1d
    goto/32 :goto_2c

    nop

    :goto_1e
    const-string v2, "MultiEndpointImplBase"

    goto/32 :goto_1b

    nop

    :goto_1f
    if-nez v0, :cond_5

    goto/32 :goto_9

    :cond_5
    goto/32 :goto_14

    nop

    :goto_20
    invoke-interface {p1}, Lcom/android/ims/internal/IImsExternalCallStateListener;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    goto/32 :goto_26

    nop

    :goto_21
    invoke-static {v0}, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;->-$$Nest$fgetmListener(Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;)Lcom/android/ims/internal/IImsExternalCallStateListener;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_22
    if-eqz v0, :cond_6

    goto/32 :goto_1d

    :cond_6
    goto/32 :goto_27

    nop

    :goto_23
    if-nez v0, :cond_7

    goto/32 :goto_9

    :cond_7
    goto/32 :goto_8

    nop

    :goto_24
    invoke-static {v0, v1}, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;->-$$Nest$fputmListener(Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;Lcom/android/ims/internal/IImsExternalCallStateListener;)V

    :goto_25
    goto/32 :goto_d

    nop

    :goto_26
    invoke-static {v0, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_23

    nop

    :goto_27
    iget-object v0, p0, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase$1;->this$0:Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;

    goto/32 :goto_13

    nop

    :goto_28
    iget-object v0, p0, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase$1;->this$0:Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;

    goto/32 :goto_16

    nop

    :goto_29
    invoke-static {v0}, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;->-$$Nest$fgetmListener(Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;)Lcom/android/ims/internal/IImsExternalCallStateListener;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_2a
    return-void

    :goto_2b
    invoke-static {v0}, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;->-$$Nest$fgetmListener(Landroid/telephony/ims/stub/ImsMultiEndpointImplBase;)Lcom/android/ims/internal/IImsExternalCallStateListener;

    move-result-object v0

    goto/32 :goto_c

    nop

    :goto_2c
    const-string/jumbo v0, "setListener is being called when there is already an active listener"

    goto/32 :goto_15

    nop

    :goto_2d
    const-string/jumbo v0, "setListener: discarding dead Binder"

    goto/32 :goto_3

    nop
.end method

.method public requestImsExternalCallStateInfo()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    new-instance v0, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase$1$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase$1$$ExternalSyntheticLambda1;-><init>(Landroid/telephony/ims/stub/ImsMultiEndpointImplBase$1;)V

    const-string/jumbo v1, "requestImsExternalCallStateInfo"

    invoke-direct {p0, v0, v1}, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase$1;->executeMethodAsync(Ljava/lang/Runnable;Ljava/lang/String;)V

    return-void
.end method

.method public setListener(Lcom/android/ims/internal/IImsExternalCallStateListener;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    new-instance v0, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase$1$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1}, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase$1$$ExternalSyntheticLambda0;-><init>(Landroid/telephony/ims/stub/ImsMultiEndpointImplBase$1;Lcom/android/ims/internal/IImsExternalCallStateListener;)V

    const-string/jumbo v1, "setListener"

    invoke-direct {p0, v0, v1}, Landroid/telephony/ims/stub/ImsMultiEndpointImplBase$1;->executeMethodAsync(Ljava/lang/Runnable;Ljava/lang/String;)V

    return-void
.end method
