.class public interface abstract Landroid/telephony/ims/stub/CapabilityExchangeEventListener$OptionsRequestCallback;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/telephony/ims/stub/CapabilityExchangeEventListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OptionsRequestCallback"
.end annotation


# virtual methods
.method public abstract onRespondToCapabilityRequest(Landroid/telephony/ims/RcsContactUceCapability;Z)V
.end method

.method public abstract onRespondToCapabilityRequestWithError(ILjava/lang/String;)V
.end method
