.class public interface abstract Landroid/telephony/IMiuiSubscriptionManager;
.super Ljava/lang/Object;


# virtual methods
.method public abstract disableSubscriptionsCache()V
.end method

.method public abstract enableSubscriptionsCache()V
.end method

.method public abstract getAllSubscriptionInfoCount()I
.end method

.method public abstract getDefaultDataSlotId()I
.end method

.method public abstract getDefaultDataSubscriptionId()I
.end method

.method public abstract getDefaultSlotId()I
.end method

.method public abstract getDefaultSmsSlotId()I
.end method

.method public abstract getDefaultSmsSubscriptionId()I
.end method

.method public abstract getDefaultSubscriptionId()I
.end method

.method public abstract getDefaultVoiceSlotId()I
.end method

.method public abstract getDefaultVoiceSubscriptionId()I
.end method

.method public abstract getPhoneIdForSlot(I)I
.end method

.method public abstract getPhoneIdForSubscription(I)I
.end method

.method public abstract getSlotIdForPhone(I)I
.end method

.method public abstract getSlotIdForSubscription(I)I
.end method

.method public abstract getSubscriptionIdForSlot(I)I
.end method

.method public abstract getSubscriptionInfoCount()I
.end method

.method public abstract setDefaultDataSubscriptionId(I)V
.end method

.method public abstract setDefaultSmsSlotId(I)V
.end method

.method public abstract setDefaultSmsSubscriptionId(I)V
.end method

.method public abstract setDefaultVoiceSlotId(I)V
.end method

.method public abstract setDefaultVoiceSubscriptionId(I)V
.end method

.method public abstract setDisplayNameForSlot(Ljava/lang/String;I)I
.end method

.method public abstract setDisplayNameForSubscription(Ljava/lang/String;I)I
.end method

.method public abstract setDisplayNumberForSlot(Ljava/lang/String;I)I
.end method

.method public abstract setDisplayNumberForSubscription(Ljava/lang/String;I)I
.end method
