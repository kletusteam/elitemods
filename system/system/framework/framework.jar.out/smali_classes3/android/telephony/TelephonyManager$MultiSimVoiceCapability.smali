.class public interface abstract Landroid/telephony/TelephonyManager$MultiSimVoiceCapability;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/telephony/TelephonyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MultiSimVoiceCapability"
.end annotation


# static fields
.field public static final DSDA:I = 0x3

.field public static final DSDS:I = 0x1

.field public static final PSEUDO_DSDA:I = 0x2

.field public static final UNKNOWN:I
