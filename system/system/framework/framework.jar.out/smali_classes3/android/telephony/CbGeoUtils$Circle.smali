.class public Landroid/telephony/CbGeoUtils$Circle;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/telephony/CbGeoUtils$Geometry;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/telephony/CbGeoUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Circle"
.end annotation


# instance fields
.field private final mCenter:Landroid/telephony/CbGeoUtils$LatLng;

.field private final mRadiusMeter:D


# direct methods
.method public constructor <init>(Landroid/telephony/CbGeoUtils$LatLng;D)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/telephony/CbGeoUtils$Circle;->mCenter:Landroid/telephony/CbGeoUtils$LatLng;

    iput-wide p2, p0, Landroid/telephony/CbGeoUtils$Circle;->mRadiusMeter:D

    return-void
.end method


# virtual methods
.method public contains(Landroid/telephony/CbGeoUtils$LatLng;)Z
    .locals 4

    iget-object v0, p0, Landroid/telephony/CbGeoUtils$Circle;->mCenter:Landroid/telephony/CbGeoUtils$LatLng;

    invoke-virtual {v0, p1}, Landroid/telephony/CbGeoUtils$LatLng;->distance(Landroid/telephony/CbGeoUtils$LatLng;)D

    move-result-wide v0

    iget-wide v2, p0, Landroid/telephony/CbGeoUtils$Circle;->mRadiusMeter:D

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Landroid/telephony/CbGeoUtils$Circle;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    :cond_1
    move-object v1, p1

    check-cast v1, Landroid/telephony/CbGeoUtils$Circle;

    iget-object v3, p0, Landroid/telephony/CbGeoUtils$Circle;->mCenter:Landroid/telephony/CbGeoUtils$LatLng;

    iget-object v4, v1, Landroid/telephony/CbGeoUtils$Circle;->mCenter:Landroid/telephony/CbGeoUtils$LatLng;

    invoke-virtual {v3, v4}, Landroid/telephony/CbGeoUtils$LatLng;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-wide v3, p0, Landroid/telephony/CbGeoUtils$Circle;->mRadiusMeter:D

    iget-wide v5, v1, Landroid/telephony/CbGeoUtils$Circle;->mRadiusMeter:D

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Double;->compare(DD)I

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    :cond_2
    move v0, v2

    :goto_0
    return v0
.end method

.method public getCenter()Landroid/telephony/CbGeoUtils$LatLng;
    .locals 1

    iget-object v0, p0, Landroid/telephony/CbGeoUtils$Circle;->mCenter:Landroid/telephony/CbGeoUtils$LatLng;

    return-object v0
.end method

.method public getRadius()D
    .locals 2

    iget-wide v0, p0, Landroid/telephony/CbGeoUtils$Circle;->mRadiusMeter:D

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "Circle: "

    sget-boolean v1, Lcom/android/internal/telephony/util/TelephonyUtils;->IS_DEBUGGABLE:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/telephony/CbGeoUtils$Circle;->mCenter:Landroid/telephony/CbGeoUtils$LatLng;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", radius = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Landroid/telephony/CbGeoUtils$Circle;->mRadiusMeter:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method
