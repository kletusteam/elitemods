.class public Landroid/telephony/TelephonyManager$CallComposerException;
.super Ljava/lang/Exception;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/telephony/TelephonyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CallComposerException"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/telephony/TelephonyManager$CallComposerException$CallComposerError;
    }
.end annotation


# static fields
.field public static final ERROR_AUTHENTICATION_FAILED:I = 0x3

.field public static final ERROR_FILE_TOO_LARGE:I = 0x2

.field public static final ERROR_INPUT_CLOSED:I = 0x4

.field public static final ERROR_IO_EXCEPTION:I = 0x5

.field public static final ERROR_NETWORK_UNAVAILABLE:I = 0x6

.field public static final ERROR_REMOTE_END_CLOSED:I = 0x1

.field public static final ERROR_UNKNOWN:I = 0x0

.field public static final SUCCESS:I = -0x1


# instance fields
.field private final mErrorCode:I

.field private final mIOException:Ljava/io/IOException;


# direct methods
.method public constructor <init>(ILjava/io/IOException;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    iput p1, p0, Landroid/telephony/TelephonyManager$CallComposerException;->mErrorCode:I

    iput-object p2, p0, Landroid/telephony/TelephonyManager$CallComposerException;->mIOException:Ljava/io/IOException;

    return-void
.end method


# virtual methods
.method public getErrorCode()I
    .locals 1

    iget v0, p0, Landroid/telephony/TelephonyManager$CallComposerException;->mErrorCode:I

    return v0
.end method

.method public getIOException()Ljava/io/IOException;
    .locals 1

    iget-object v0, p0, Landroid/telephony/TelephonyManager$CallComposerException;->mIOException:Ljava/io/IOException;

    return-object v0
.end method
