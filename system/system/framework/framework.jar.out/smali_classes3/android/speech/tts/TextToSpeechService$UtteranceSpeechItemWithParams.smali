.class abstract Landroid/speech/tts/TextToSpeechService$UtteranceSpeechItemWithParams;
.super Landroid/speech/tts/TextToSpeechService$UtteranceSpeechItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/speech/tts/TextToSpeechService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "UtteranceSpeechItemWithParams"
.end annotation


# instance fields
.field protected final mParams:Landroid/os/Bundle;

.field protected final mUtteranceId:Ljava/lang/String;

.field final synthetic this$0:Landroid/speech/tts/TextToSpeechService;


# direct methods
.method constructor <init>(Landroid/speech/tts/TextToSpeechService;Ljava/lang/Object;IILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Landroid/speech/tts/TextToSpeechService$UtteranceSpeechItemWithParams;->this$0:Landroid/speech/tts/TextToSpeechService;

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/speech/tts/TextToSpeechService$UtteranceSpeechItem;-><init>(Landroid/speech/tts/TextToSpeechService;Ljava/lang/Object;II)V

    iput-object p5, p0, Landroid/speech/tts/TextToSpeechService$UtteranceSpeechItemWithParams;->mParams:Landroid/os/Bundle;

    iput-object p6, p0, Landroid/speech/tts/TextToSpeechService$UtteranceSpeechItemWithParams;->mUtteranceId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method getAudioParams()Landroid/speech/tts/TextToSpeechService$AudioOutputParams;
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    const/4 v1, 0x1

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Landroid/speech/tts/TextToSpeechService$UtteranceSpeechItemWithParams;->mParams:Landroid/os/Bundle;

    goto/32 :goto_0

    nop

    :goto_2
    invoke-static {v0, v1}, Landroid/speech/tts/TextToSpeechService$AudioOutputParams;->createFromParamsBundle(Landroid/os/Bundle;Z)Landroid/speech/tts/TextToSpeechService$AudioOutputParams;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_3
    return-object v0
.end method

.method getPitch()I
    .locals 3

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    iget-object v0, p0, Landroid/speech/tts/TextToSpeechService$UtteranceSpeechItemWithParams;->mParams:Landroid/os/Bundle;

    goto/32 :goto_5

    nop

    :goto_2
    invoke-virtual {p0, v0, v2, v1}, Landroid/speech/tts/TextToSpeechService$UtteranceSpeechItemWithParams;->getIntParam(Landroid/os/Bundle;Ljava/lang/String;I)I

    move-result v0

    goto/32 :goto_0

    nop

    :goto_3
    invoke-static {v1}, Landroid/speech/tts/TextToSpeechService;->-$$Nest$mgetDefaultPitch(Landroid/speech/tts/TextToSpeechService;)I

    move-result v1

    goto/32 :goto_4

    nop

    :goto_4
    const-string/jumbo v2, "pitch"

    goto/32 :goto_2

    nop

    :goto_5
    iget-object v1, p0, Landroid/speech/tts/TextToSpeechService$UtteranceSpeechItemWithParams;->this$0:Landroid/speech/tts/TextToSpeechService;

    goto/32 :goto_3

    nop
.end method

.method getSpeechRate()I
    .locals 3

    goto/32 :goto_5

    nop

    :goto_0
    iget-object v1, p0, Landroid/speech/tts/TextToSpeechService$UtteranceSpeechItemWithParams;->this$0:Landroid/speech/tts/TextToSpeechService;

    goto/32 :goto_2

    nop

    :goto_1
    invoke-virtual {p0, v0, v2, v1}, Landroid/speech/tts/TextToSpeechService$UtteranceSpeechItemWithParams;->getIntParam(Landroid/os/Bundle;Ljava/lang/String;I)I

    move-result v0

    goto/32 :goto_4

    nop

    :goto_2
    invoke-static {v1}, Landroid/speech/tts/TextToSpeechService;->-$$Nest$mgetDefaultSpeechRate(Landroid/speech/tts/TextToSpeechService;)I

    move-result v1

    goto/32 :goto_3

    nop

    :goto_3
    const-string/jumbo v2, "rate"

    goto/32 :goto_1

    nop

    :goto_4
    return v0

    :goto_5
    iget-object v0, p0, Landroid/speech/tts/TextToSpeechService$UtteranceSpeechItemWithParams;->mParams:Landroid/os/Bundle;

    goto/32 :goto_0

    nop
.end method

.method public getUtteranceId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/speech/tts/TextToSpeechService$UtteranceSpeechItemWithParams;->mUtteranceId:Ljava/lang/String;

    return-object v0
.end method

.method hasLanguage()Z
    .locals 3

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p0, v0, v1, v2}, Landroid/speech/tts/TextToSpeechService$UtteranceSpeechItemWithParams;->getStringParam(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_1
    iget-object v0, p0, Landroid/speech/tts/TextToSpeechService$UtteranceSpeechItemWithParams;->mParams:Landroid/os/Bundle;

    goto/32 :goto_5

    nop

    :goto_2
    const/4 v2, 0x0

    goto/32 :goto_0

    nop

    :goto_3
    xor-int/lit8 v0, v0, 0x1

    goto/32 :goto_4

    nop

    :goto_4
    return v0

    :goto_5
    const-string/jumbo v1, "language"

    goto/32 :goto_2

    nop

    :goto_6
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    goto/32 :goto_3

    nop
.end method
