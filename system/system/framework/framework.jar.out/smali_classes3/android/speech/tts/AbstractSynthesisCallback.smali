.class abstract Landroid/speech/tts/AbstractSynthesisCallback;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/speech/tts/SynthesisCallback;


# instance fields
.field protected final mClientIsUsingV2:Z


# direct methods
.method constructor <init>(Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Landroid/speech/tts/AbstractSynthesisCallback;->mClientIsUsingV2:Z

    return-void
.end method


# virtual methods
.method errorCodeOnStop()I
    .locals 1

    goto/32 :goto_5

    nop

    :goto_0
    const/4 v0, -0x1

    :goto_1
    goto/32 :goto_7

    nop

    :goto_2
    goto :goto_1

    :goto_3
    goto/32 :goto_0

    nop

    :goto_4
    if-nez v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_6

    nop

    :goto_5
    iget-boolean v0, p0, Landroid/speech/tts/AbstractSynthesisCallback;->mClientIsUsingV2:Z

    goto/32 :goto_4

    nop

    :goto_6
    const/4 v0, -0x2

    goto/32 :goto_2

    nop

    :goto_7
    return v0
.end method

.method abstract stop()V
.end method
