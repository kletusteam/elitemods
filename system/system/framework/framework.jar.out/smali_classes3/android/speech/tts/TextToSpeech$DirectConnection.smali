.class Landroid/speech/tts/TextToSpeech$DirectConnection;
.super Landroid/speech/tts/TextToSpeech$Connection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/speech/tts/TextToSpeech;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DirectConnection"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/speech/tts/TextToSpeech;


# direct methods
.method private constructor <init>(Landroid/speech/tts/TextToSpeech;)V
    .locals 1

    iput-object p1, p0, Landroid/speech/tts/TextToSpeech$DirectConnection;->this$0:Landroid/speech/tts/TextToSpeech;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/speech/tts/TextToSpeech$Connection;-><init>(Landroid/speech/tts/TextToSpeech;Landroid/speech/tts/TextToSpeech$Connection-IA;)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/speech/tts/TextToSpeech;Landroid/speech/tts/TextToSpeech$DirectConnection-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/speech/tts/TextToSpeech$DirectConnection;-><init>(Landroid/speech/tts/TextToSpeech;)V

    return-void
.end method


# virtual methods
.method connect(Ljava/lang/String;)Z
    .locals 3

    goto/32 :goto_2

    nop

    :goto_0
    const/4 v2, 0x1

    goto/32 :goto_6

    nop

    :goto_1
    const-string v1, "android.intent.action.TTS_SERVICE"

    goto/32 :goto_7

    nop

    :goto_2
    new-instance v0, Landroid/content/Intent;

    goto/32 :goto_1

    nop

    :goto_3
    invoke-static {v1}, Landroid/speech/tts/TextToSpeech;->-$$Nest$fgetmContext(Landroid/speech/tts/TextToSpeech;)Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_4
    iget-object v1, p0, Landroid/speech/tts/TextToSpeech$DirectConnection;->this$0:Landroid/speech/tts/TextToSpeech;

    goto/32 :goto_3

    nop

    :goto_5
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    goto/32 :goto_4

    nop

    :goto_6
    invoke-virtual {v1, v0, p0, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    goto/32 :goto_8

    nop

    :goto_7
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto/32 :goto_5

    nop

    :goto_8
    return v1
.end method

.method disconnect()V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0}, Landroid/speech/tts/TextToSpeech$DirectConnection;->clearServiceConnection()Z

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, Landroid/speech/tts/TextToSpeech$DirectConnection;->this$0:Landroid/speech/tts/TextToSpeech;

    goto/32 :goto_4

    nop

    :goto_3
    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    goto/32 :goto_1

    nop

    :goto_4
    invoke-static {v0}, Landroid/speech/tts/TextToSpeech;->-$$Nest$fgetmContext(Landroid/speech/tts/TextToSpeech;)Landroid/content/Context;

    move-result-object v0

    goto/32 :goto_3

    nop
.end method
