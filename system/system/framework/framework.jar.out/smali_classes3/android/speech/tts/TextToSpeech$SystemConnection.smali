.class Landroid/speech/tts/TextToSpeech$SystemConnection;
.super Landroid/speech/tts/TextToSpeech$Connection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/speech/tts/TextToSpeech;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SystemConnection"
.end annotation


# instance fields
.field private volatile mSession:Landroid/speech/tts/ITextToSpeechSession;

.field final synthetic this$0:Landroid/speech/tts/TextToSpeech;


# direct methods
.method static bridge synthetic -$$Nest$fputmSession(Landroid/speech/tts/TextToSpeech$SystemConnection;Landroid/speech/tts/ITextToSpeechSession;)V
    .locals 0

    iput-object p1, p0, Landroid/speech/tts/TextToSpeech$SystemConnection;->mSession:Landroid/speech/tts/ITextToSpeechSession;

    return-void
.end method

.method private constructor <init>(Landroid/speech/tts/TextToSpeech;)V
    .locals 1

    iput-object p1, p0, Landroid/speech/tts/TextToSpeech$SystemConnection;->this$0:Landroid/speech/tts/TextToSpeech;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/speech/tts/TextToSpeech$Connection;-><init>(Landroid/speech/tts/TextToSpeech;Landroid/speech/tts/TextToSpeech$Connection-IA;)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/speech/tts/TextToSpeech;Landroid/speech/tts/TextToSpeech$SystemConnection-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/speech/tts/TextToSpeech$SystemConnection;-><init>(Landroid/speech/tts/TextToSpeech;)V

    return-void
.end method


# virtual methods
.method connect(Ljava/lang/String;)Z
    .locals 5

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {v3}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v2

    goto/32 :goto_7

    nop

    :goto_1
    invoke-static {v0}, Landroid/speech/tts/ITextToSpeechManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/speech/tts/ITextToSpeechManager;

    move-result-object v1

    goto/32 :goto_b

    nop

    :goto_2
    const-string/jumbo v0, "texttospeech"

    goto/32 :goto_8

    nop

    :goto_3
    invoke-static {v2, v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/32 :goto_0

    nop

    :goto_4
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_c

    nop

    :goto_5
    return v2

    :catch_0
    move-exception v3

    goto/32 :goto_9

    nop

    :goto_6
    const/4 v2, 0x1

    goto/32 :goto_5

    nop

    :goto_7
    throw v2

    :goto_8
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_9
    const-string v4, "Error communicating with the System Server: "

    goto/32 :goto_3

    nop

    :goto_a
    const-string v3, "System service is not available!"

    goto/32 :goto_4

    nop

    :goto_b
    const-string v2, "TextToSpeech"

    goto/32 :goto_f

    nop

    :goto_c
    const/4 v2, 0x0

    goto/32 :goto_d

    nop

    :goto_d
    return v2

    :goto_e
    :try_start_0
    new-instance v3, Landroid/speech/tts/TextToSpeech$SystemConnection$1;

    invoke-direct {v3, p0}, Landroid/speech/tts/TextToSpeech$SystemConnection$1;-><init>(Landroid/speech/tts/TextToSpeech$SystemConnection;)V

    invoke-interface {v1, p1, v3}, Landroid/speech/tts/ITextToSpeechManager;->createSession(Ljava/lang/String;Landroid/speech/tts/ITextToSpeechSessionCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_6

    nop

    :goto_f
    if-eqz v1, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_a

    nop
.end method

.method disconnect()V
    .locals 4

    goto/32 :goto_3

    nop

    :goto_0
    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_1
    goto/32 :goto_8

    nop

    :goto_2
    const-string v3, "Error disconnecting session"

    goto/32 :goto_0

    nop

    :goto_3
    iget-object v0, p0, Landroid/speech/tts/TextToSpeech$SystemConnection;->mSession:Landroid/speech/tts/ITextToSpeechSession;

    goto/32 :goto_5

    nop

    :goto_4
    return-void

    :goto_5
    if-nez v0, :cond_0

    goto/32 :goto_9

    :cond_0
    :try_start_0
    invoke-interface {v0}, Landroid/speech/tts/ITextToSpeechSession;->disconnect()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_7

    nop

    :goto_6
    const-string v2, "TextToSpeech"

    goto/32 :goto_2

    nop

    :goto_7
    goto :goto_1

    :catch_0
    move-exception v1

    goto/32 :goto_6

    nop

    :goto_8
    invoke-virtual {p0}, Landroid/speech/tts/TextToSpeech$SystemConnection;->clearServiceConnection()Z

    :goto_9
    goto/32 :goto_4

    nop
.end method
