.class public final Landroid/stats/mediametrics/Mediametrics;
.super Ljava/lang/Object;


# static fields
.field public static final ABANDONED:I = 0xf

.field public static final AUDIO:I = 0x0

.field public static final BUFFERING:I = 0x6

.field public static final CONTENT_TYPE_AD:I = 0x2

.field public static final CONTENT_TYPE_MAIN:I = 0x1

.field public static final CONTENT_TYPE_OTHER:I = 0x3

.field public static final CONTENT_TYPE_UNKNOWN:I = 0x0

.field public static final DRM_TYPE_NONE:I = 0x0

.field public static final DRM_TYPE_OTHER:I = 0x1

.field public static final DRM_TYPE_PLAY_READY:I = 0x2

.field public static final DRM_TYPE_WV_L1:I = 0x3

.field public static final DRM_TYPE_WV_L3:I = 0x4

.field public static final ENCODED_SURROUND_OUTPUT_ALWAYS:I = 0x3

.field public static final ENCODED_SURROUND_OUTPUT_AUTO:I = 0x1

.field public static final ENCODED_SURROUND_OUTPUT_MANUAL:I = 0x4

.field public static final ENCODED_SURROUND_OUTPUT_NEVER:I = 0x2

.field public static final ENCODED_SURROUND_OUTPUT_UNKNOWN:I = 0x0

.field public static final ENCODING_AAC_ELD:I = 0xf

.field public static final ENCODING_AAC_HE_V1:I = 0xb

.field public static final ENCODING_AAC_HE_V2:I = 0xc

.field public static final ENCODING_AAC_LC:I = 0xa

.field public static final ENCODING_AAC_XHE:I = 0x10

.field public static final ENCODING_AC3:I = 0x5

.field public static final ENCODING_AC4:I = 0x11

.field public static final ENCODING_DEFAULT:I = 0x1

.field public static final ENCODING_DOLBY_MAT:I = 0x13

.field public static final ENCODING_DOLBY_TRUEHD:I = 0xe

.field public static final ENCODING_DRA:I = 0x1c

.field public static final ENCODING_DTS:I = 0x7

.field public static final ENCODING_DTS_HD:I = 0x8

.field public static final ENCODING_DTS_UHD:I = 0x1b

.field public static final ENCODING_E_AC3:I = 0x6

.field public static final ENCODING_E_AC3_JOC:I = 0x12

.field public static final ENCODING_IEC61937:I = 0xd

.field public static final ENCODING_INVALID:I = 0x0

.field public static final ENCODING_MP3:I = 0x9

.field public static final ENCODING_MPEGH_BL_L3:I = 0x17

.field public static final ENCODING_MPEGH_BL_L4:I = 0x18

.field public static final ENCODING_MPEGH_LC_L3:I = 0x19

.field public static final ENCODING_MPEGH_LC_L4:I = 0x1a

.field public static final ENCODING_OPUS:I = 0x14

.field public static final ENCODING_PCM_16BIT:I = 0x2

.field public static final ENCODING_PCM_24BIT_PACKED:I = 0x15

.field public static final ENCODING_PCM_32BIT:I = 0x16

.field public static final ENCODING_PCM_8BIT:I = 0x3

.field public static final ENCODING_PCM_FLOAT:I = 0x4

.field public static final ENDED:I = 0xb

.field public static final ERROR_CODE_AUDIOTRACK_INIT:I = 0x11

.field public static final ERROR_CODE_AUDIOTRACK_OTHER:I = 0x13

.field public static final ERROR_CODE_AUDIOTRACK_WRITE:I = 0x12

.field public static final ERROR_CODE_DECODER_DECODE:I = 0xe

.field public static final ERROR_CODE_DECODER_INIT:I = 0xd

.field public static final ERROR_CODE_DECODER_OOM:I = 0xf

.field public static final ERROR_CODE_DECODER_OTHER:I = 0x10

.field public static final ERROR_CODE_DRM_CONTENT_ERROR:I = 0x1c

.field public static final ERROR_CODE_DRM_DISALLOWED:I = 0x1a

.field public static final ERROR_CODE_DRM_LICENSE_ERROR:I = 0x19

.field public static final ERROR_CODE_DRM_OTHER:I = 0x1e

.field public static final ERROR_CODE_DRM_PROVISIONING_FAILED:I = 0x18

.field public static final ERROR_CODE_DRM_REVOKED:I = 0x27

.field public static final ERROR_CODE_DRM_SYSTEM_ERROR:I = 0x1b

.field public static final ERROR_CODE_DRM_UNAVAILABLE:I = 0x17

.field public static final ERROR_CODE_MEDIA_MANIFET:I = 0xa

.field public static final ERROR_CODE_MEDIA_OTHER:I = 0xc

.field public static final ERROR_CODE_MEDIA_PARSER:I = 0xb

.field public static final ERROR_CODE_NETWORK_BAD_STATUS:I = 0x5

.field public static final ERROR_CODE_NETWORK_CLOSED:I = 0x8

.field public static final ERROR_CODE_NETWORK_CONNECT:I = 0x4

.field public static final ERROR_CODE_NETWORK_DNS:I = 0x6

.field public static final ERROR_CODE_NETWORK_OFFLINE:I = 0x3

.field public static final ERROR_CODE_NETWORK_OTHER:I = 0x9

.field public static final ERROR_CODE_NETWORK_TIMEOUT:I = 0x7

.field public static final ERROR_CODE_OTHER:I = 0x1

.field public static final ERROR_CODE_PLAYER_BEHIND_LIVE_WINDOW:I = 0x15

.field public static final ERROR_CODE_PLAYER_OTHER:I = 0x16

.field public static final ERROR_CODE_PLAYER_REMOTE:I = 0x14

.field public static final ERROR_CODE_RUNTIME:I = 0x2

.field public static final ERROR_CODE_UNKNOWN:I = 0x0

.field public static final FAILED:I = 0xd

.field public static final HDR_TYPE_DOLBY_VISION:I = 0x1

.field public static final HDR_TYPE_HDR10:I = 0x2

.field public static final HDR_TYPE_HDR10_PLUS:I = 0x4

.field public static final HDR_TYPE_HLG:I = 0x3

.field public static final INTERRUPTED_BY_AD:I = 0xe

.field public static final JOINING_BACKGROUND:I = 0x1

.field public static final JOINING_FOREGROUND:I = 0x2

.field public static final MATCH_CONTENT_FRAMERATE_ALWAYS:I = 0x3

.field public static final MATCH_CONTENT_FRAMERATE_NEVER:I = 0x1

.field public static final MATCH_CONTENT_FRAMERATE_SEAMLESSS_ONLY:I = 0x2

.field public static final MATCH_CONTENT_FRAMERATE_UNKNOWN:I = 0x0

.field public static final NETWORK_TYPE_2G:I = 0x4

.field public static final NETWORK_TYPE_3G:I = 0x5

.field public static final NETWORK_TYPE_4G:I = 0x6

.field public static final NETWORK_TYPE_5G_NSA:I = 0x7

.field public static final NETWORK_TYPE_5G_SA:I = 0x8

.field public static final NETWORK_TYPE_ETHERNET:I = 0x3

.field public static final NETWORK_TYPE_OFFLINE:I = 0x9

.field public static final NETWORK_TYPE_OTHER:I = 0x1

.field public static final NETWORK_TYPE_UNKNOWN:I = 0x0

.field public static final NETWORK_TYPE_WIFI:I = 0x2

.field public static final NOT_STARTED:I = 0x0

.field public static final OFF:I = 0x0

.field public static final ON:I = 0x1

.field public static final PAUSED:I = 0x4

.field public static final PAUSED_BUFFERING:I = 0x7

.field public static final PLAYBACK_TYPE_LIVE:I = 0x2

.field public static final PLAYBACK_TYPE_OTHER:I = 0x3

.field public static final PLAYBACK_TYPE_UNKNOWN:I = 0x0

.field public static final PLAYBACK_TYPE_VOD:I = 0x1

.field public static final PLAYING:I = 0x3

.field public static final REASON_ADAPTIVE:I = 0x4

.field public static final REASON_INITIAL:I = 0x2

.field public static final REASON_MANUAL:I = 0x3

.field public static final REASON_OTHER:I = 0x1

.field public static final REASON_UNKNOWN:I = 0x0

.field public static final SEEKING:I = 0x5

.field public static final STOPPED:I = 0xc

.field public static final STREAM_SOURCE_DEVICE:I = 0x2

.field public static final STREAM_SOURCE_MIXED:I = 0x3

.field public static final STREAM_SOURCE_NETWORK:I = 0x1

.field public static final STREAM_SOURCE_UNKNOWN:I = 0x0

.field public static final STREAM_TYPE_DASH:I = 0x3

.field public static final STREAM_TYPE_HLS:I = 0x4

.field public static final STREAM_TYPE_OTHER:I = 0x1

.field public static final STREAM_TYPE_PROGRESSIVE:I = 0x2

.field public static final STREAM_TYPE_SS:I = 0x5

.field public static final STREAM_TYPE_UNKNOWN:I = 0x0

.field public static final SUPPRESSED:I = 0x9

.field public static final SUPPRESSED_BUFFERING:I = 0xa

.field public static final TEXT:I = 0x2

.field public static final VIDEO:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
