.class public final Landroid/stats/style/StyleEnums;
.super Ljava/lang/Object;


# static fields
.field public static final APP_LAUNCHED:I = 0x16

.field public static final COLOR_PRESET_APPLIED:I = 0x1a

.field public static final COLOR_WALLPAPER_HOME_APPLIED:I = 0x17

.field public static final COLOR_WALLPAPER_HOME_LOCK_APPLIED:I = 0x19

.field public static final COLOR_WALLPAPER_LOCK_APPLIED:I = 0x18

.field public static final DATE_MANUAL:I = 0x2

.field public static final DATE_PREFERENCE_UNSPECIFIED:I = 0x0

.field public static final DATE_UNAVAILABLE:I = 0x1

.field public static final DEFAULT_ACTION:I = 0x0

.field public static final EFFECT_APPLIED_OFF:I = 0x3

.field public static final EFFECT_APPLIED_ON_FAILED:I = 0x2

.field public static final EFFECT_APPLIED_ON_SUCCESS:I = 0x1

.field public static final EFFECT_PREFERENCE_UNSPECIFIED:I = 0x0

.field public static final LAUNCHED_CROP_AND_SET_ACTION:I = 0x6

.field public static final LAUNCHED_DEEP_LINK:I = 0x7

.field public static final LAUNCHED_LAUNCHER:I = 0x1

.field public static final LAUNCHED_LAUNCH_ICON:I = 0x5

.field public static final LAUNCHED_PREFERENCE_UNSPECIFIED:I = 0x0

.field public static final LAUNCHED_SETTINGS:I = 0x2

.field public static final LAUNCHED_SETTINGS_SEARCH:I = 0x8

.field public static final LAUNCHED_SUW:I = 0x3

.field public static final LAUNCHED_TIPS:I = 0x4

.field public static final LIVE_WALLPAPER_APPLIED:I = 0x10

.field public static final LIVE_WALLPAPER_CUSTOMIZE_SELECT:I = 0x12

.field public static final LIVE_WALLPAPER_DELETE_FAILED:I = 0xf

.field public static final LIVE_WALLPAPER_DELETE_SUCCESS:I = 0xe

.field public static final LIVE_WALLPAPER_DOWNLOAD_CANCELLED:I = 0xd

.field public static final LIVE_WALLPAPER_DOWNLOAD_FAILED:I = 0xc

.field public static final LIVE_WALLPAPER_DOWNLOAD_SUCCESS:I = 0xb

.field public static final LIVE_WALLPAPER_EFFECT_SHOW:I = 0x15

.field public static final LIVE_WALLPAPER_INFO_SELECT:I = 0x11

.field public static final LIVE_WALLPAPER_QUESTIONNAIRE_APPLIED:I = 0x14

.field public static final LIVE_WALLPAPER_QUESTIONNAIRE_SELECT:I = 0x13

.field public static final LOCATION_CURRENT:I = 0x2

.field public static final LOCATION_MANUAL:I = 0x3

.field public static final LOCATION_PREFERENCE_UNSPECIFIED:I = 0x0

.field public static final LOCATION_UNAVAILABLE:I = 0x1

.field public static final ONRESUME:I = 0x1

.field public static final ONSTOP:I = 0x2

.field public static final PICKER_APPLIED:I = 0x4

.field public static final PICKER_SELECT:I = 0x3

.field public static final SNAPSHOT:I = 0x1c

.field public static final WALLPAPER_APPLIED:I = 0x7

.field public static final WALLPAPER_DOWNLOAD:I = 0x9

.field public static final WALLPAPER_EFFECT_APPLIED:I = 0x1b

.field public static final WALLPAPER_EXPLORE:I = 0x8

.field public static final WALLPAPER_OPEN_CATEGORY:I = 0x5

.field public static final WALLPAPER_REMOVE:I = 0xa

.field public static final WALLPAPER_SELECT:I = 0x6


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
