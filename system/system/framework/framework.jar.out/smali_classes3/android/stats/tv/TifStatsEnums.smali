.class public final Landroid/stats/tv/TifStatsEnums;
.super Ljava/lang/Object;


# static fields
.field public static final COMPONENT:I = 0x3ec

.field public static final COMPOSITE:I = 0x3e9

.field public static final CREATED:I = 0x1

.field public static final DISPLAY_PORT:I = 0x3f0

.field public static final DVI:I = 0x3ee

.field public static final HDMI:I = 0x3ef

.field public static final OTHER:I = 0x3e8

.field public static final RELEASED:I = 0x4

.field public static final SCART:I = 0x3eb

.field public static final SURFACE_ATTACHED:I = 0x2

.field public static final SURFACE_DETACHED:I = 0x3

.field public static final SVIDEO:I = 0x3ea

.field public static final TIF_INPUT_TYPE_UNKNOWN:I = 0x0

.field public static final TIF_TUNE_STATE_UNKNOWN:I = 0x0

.field public static final TUNER:I = 0x1

.field public static final TUNE_STARTED:I = 0x5

.field public static final VGA:I = 0x3ed

.field public static final VIDEO_AVAILABLE:I = 0x6

.field public static final VIDEO_UNAVAILABLE_REASON_AUDIO_ONLY:I = 0x68

.field public static final VIDEO_UNAVAILABLE_REASON_BUFFERING:I = 0x67

.field public static final VIDEO_UNAVAILABLE_REASON_CAS_BLACKOUT:I = 0x74

.field public static final VIDEO_UNAVAILABLE_REASON_CAS_CARD_INVALID:I = 0x73

.field public static final VIDEO_UNAVAILABLE_REASON_CAS_CARD_MUTE:I = 0x72

.field public static final VIDEO_UNAVAILABLE_REASON_CAS_INSUFFICIENT_OUTPUT_PROTECTION:I = 0x6b

.field public static final VIDEO_UNAVAILABLE_REASON_CAS_LICENSE_EXPIRED:I = 0x6e

.field public static final VIDEO_UNAVAILABLE_REASON_CAS_NEED_ACTIVATION:I = 0x6f

.field public static final VIDEO_UNAVAILABLE_REASON_CAS_NEED_PAIRING:I = 0x70

.field public static final VIDEO_UNAVAILABLE_REASON_CAS_NO_CARD:I = 0x71

.field public static final VIDEO_UNAVAILABLE_REASON_CAS_NO_LICENSE:I = 0x6d

.field public static final VIDEO_UNAVAILABLE_REASON_CAS_PVR_RECORDING_NOT_ALLOWED:I = 0x6c

.field public static final VIDEO_UNAVAILABLE_REASON_CAS_REBOOTING:I = 0x75

.field public static final VIDEO_UNAVAILABLE_REASON_CAS_UNKNOWN:I = 0x76

.field public static final VIDEO_UNAVAILABLE_REASON_INSUFFICIENT_RESOURCE:I = 0x6a

.field public static final VIDEO_UNAVAILABLE_REASON_NOT_CONNECTED:I = 0x69

.field public static final VIDEO_UNAVAILABLE_REASON_TUNING:I = 0x65

.field public static final VIDEO_UNAVAILABLE_REASON_UNKNOWN:I = 0x64

.field public static final VIDEO_UNAVAILABLE_REASON_WEAK_SIGNAL:I = 0x66


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
