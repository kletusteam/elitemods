.class public final Landroid/stats/connectivity/NetworkStackProto;
.super Ljava/lang/Object;


# static fields
.field public static final DC_ERROR_STARTING_IPREACHABILITYMONITOR:I = 0x6

.field public static final DC_ERROR_STARTING_IPV4:I = 0x4

.field public static final DC_ERROR_STARTING_IPV6:I = 0x5

.field public static final DC_INTERFACE_NOT_FOUND:I = 0x8

.field public static final DC_INVALID_PROVISIONING:I = 0x7

.field public static final DC_NONE:I = 0x0

.field public static final DC_NORMAL_TERMINATION:I = 0x1

.field public static final DC_PROVISIONING_FAIL:I = 0x2

.field public static final DC_PROVISIONING_TIMEOUT:I = 0x9

.field public static final DF_DAD:I = 0x3

.field public static final DF_FILS:I = 0x4

.field public static final DF_INITREBOOT:I = 0x1

.field public static final DF_RAPIDCOMMIT:I = 0x2

.field public static final DF_UNKNOWN:I = 0x0

.field public static final ET_BOOTP_TOO_SHORT:I = 0x4010000

.field public static final ET_BUFFER_UNDERFLOW:I = 0x5010000

.field public static final ET_DHCP_BAD_MAGIC_COOKIE:I = 0x4020000

.field public static final ET_DHCP_ERROR:I = 0x4

.field public static final ET_DHCP_INVALID_OPTION_LENGTH:I = 0x4030000

.field public static final ET_DHCP_NO_COOKIE:I = 0x4060000

.field public static final ET_DHCP_NO_MSG_TYPE:I = 0x4040000

.field public static final ET_DHCP_UNKNOWN_MSG_TYPE:I = 0x4050000

.field public static final ET_L2_ERROR:I = 0x1

.field public static final ET_L2_TOO_SHORT:I = 0x1010000

.field public static final ET_L2_WRONG_ETH_TYPE:I = 0x1020000

.field public static final ET_L3_ERROR:I = 0x2

.field public static final ET_L3_INVALID_IP:I = 0x2030000

.field public static final ET_L3_NOT_IPV4:I = 0x2020000

.field public static final ET_L3_TOO_SHORT:I = 0x2010000

.field public static final ET_L4_ERROR:I = 0x3

.field public static final ET_L4_NOT_UDP:I = 0x3010000

.field public static final ET_L4_WRONG_PORT:I = 0x3020000

.field public static final ET_MISC_ERROR:I = 0x5

.field public static final ET_PARSING_ERROR:I = 0x5030000

.field public static final ET_RECEIVE_ERROR:I = 0x5020000

.field public static final ET_UNKNOWN:I = 0x0

.field public static final HTR_DISABLE:I = 0x3

.field public static final HTR_FAILURE:I = 0x2

.field public static final HTR_SUCCESS:I = 0x1

.field public static final HTR_UNKNOWN:I = 0x0

.field public static final IPV4:I = 0x4

.field public static final IPV6:I = 0x6

.field public static final NUD_CONFIRM_FAILED:I = 0x3

.field public static final NUD_CONFIRM_FAILED_CRITICAL:I = 0x4

.field public static final NUD_CONFIRM_MAC_ADDRESS_CHANGED:I = 0x9

.field public static final NUD_EVENT_UNKNOWN:I = 0x0

.field public static final NUD_MAC_ADDRESS_CHANGED:I = 0x7

.field public static final NUD_NEIGHBOR_BOTH:I = 0x3

.field public static final NUD_NEIGHBOR_DNS:I = 0x2

.field public static final NUD_NEIGHBOR_GATEWAY:I = 0x1

.field public static final NUD_NEIGHBOR_UNKNOWN:I = 0x0

.field public static final NUD_ORGANIC_FAILED:I = 0x5

.field public static final NUD_ORGANIC_FAILED_CRITICAL:I = 0x6

.field public static final NUD_ORGANIC_MAC_ADDRESS_CHANGED:I = 0xa

.field public static final NUD_POST_ROAMING_FAILED:I = 0x1

.field public static final NUD_POST_ROAMING_FAILED_CRITICAL:I = 0x2

.field public static final NUD_POST_ROAMING_MAC_ADDRESS_CHANGED:I = 0x8

.field public static final PR_FAILURE:I = 0x2

.field public static final PR_PORTAL:I = 0x3

.field public static final PR_PRIVATE_IP_DNS:I = 0x4

.field public static final PR_SUCCESS:I = 0x1

.field public static final PR_UNKNOWN:I = 0x0

.field public static final PT_CAPPORT_API:I = 0x7

.field public static final PT_DNS:I = 0x1

.field public static final PT_FALLBACK:I = 0x5

.field public static final PT_HTTP:I = 0x2

.field public static final PT_HTTPS:I = 0x3

.field public static final PT_PAC:I = 0x4

.field public static final PT_PRIVDNS:I = 0x6

.field public static final PT_UNKNOWN:I = 0x0

.field public static final QE_IPV6_PROVISIONING_ROUTER_LOST:I = 0x1

.field public static final QE_UNKNOWN:I = 0x0

.field public static final RR_ERROR_IP_EXPIRE:I = 0x4

.field public static final RR_ERROR_IP_MISMATCH:I = 0x3

.field public static final RR_ERROR_NAK:I = 0x2

.field public static final RR_SUCCESS:I = 0x1

.field public static final RR_UNKNOWN:I = 0x0

.field public static final TT_BLUETOOTH:I = 0x3

.field public static final TT_BLUETOOTH_VPN:I = 0x9

.field public static final TT_CELLULAR:I = 0x1

.field public static final TT_CELLULAR_VPN:I = 0x7

.field public static final TT_ETHERNET:I = 0x4

.field public static final TT_ETHERNET_VPN:I = 0xa

.field public static final TT_LOWPAN:I = 0x6

.field public static final TT_TEST:I = 0xc

.field public static final TT_UNKNOWN:I = 0x0

.field public static final TT_WIFI:I = 0x2

.field public static final TT_WIFI_AWARE:I = 0x5

.field public static final TT_WIFI_CELLULAR_VPN:I = 0xb

.field public static final TT_WIFI_VPN:I = 0x8

.field public static final UNKNOWN:I = 0x0

.field public static final VR_FAILURE:I = 0x2

.field public static final VR_PARTIAL:I = 0x4

.field public static final VR_PORTAL:I = 0x3

.field public static final VR_SUCCESS:I = 0x1

.field public static final VR_UNKNOWN:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
