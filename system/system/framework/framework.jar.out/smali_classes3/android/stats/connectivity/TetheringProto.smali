.class public final Landroid/stats/connectivity/TetheringProto;
.super Ljava/lang/Object;


# static fields
.field public static final DS_TETHERING_BLUETOOTH:I = 0x3

.field public static final DS_TETHERING_ETHERNET:I = 0x6

.field public static final DS_TETHERING_NCM:I = 0x5

.field public static final DS_TETHERING_USB:I = 0x2

.field public static final DS_TETHERING_WIFI:I = 0x1

.field public static final DS_TETHERING_WIFI_P2P:I = 0x4

.field public static final DS_UNSPECIFIED:I = 0x0

.field public static final EC_DHCPSERVER_ERROR:I = 0xc

.field public static final EC_DISABLE_FORWARDING_ERROR:I = 0x9

.field public static final EC_ENABLE_FORWARDING_ERROR:I = 0x8

.field public static final EC_ENTITLEMENT_UNKNOWN:I = 0xd

.field public static final EC_IFACE_CFG_ERROR:I = 0xa

.field public static final EC_INTERNAL_ERROR:I = 0x5

.field public static final EC_NO_ACCESS_TETHERING_PERMISSION:I = 0xf

.field public static final EC_NO_CHANGE_TETHERING_PERMISSION:I = 0xe

.field public static final EC_NO_ERROR:I = 0x0

.field public static final EC_PROVISIONING_FAILED:I = 0xb

.field public static final EC_SERVICE_UNAVAIL:I = 0x2

.field public static final EC_TETHER_IFACE_ERROR:I = 0x6

.field public static final EC_UNAVAIL_IFACE:I = 0x4

.field public static final EC_UNKNOWN_IFACE:I = 0x1

.field public static final EC_UNKNOWN_TYPE:I = 0x10

.field public static final EC_UNSUPPORTED:I = 0x3

.field public static final EC_UNTETHER_IFACE_ERROR:I = 0x7

.field public static final USER_GMS:I = 0x3

.field public static final USER_SETTINGS:I = 0x1

.field public static final USER_SYSTEMUI:I = 0x2

.field public static final USER_UNKNOWN:I = 0x0

.field public static final UT_BLUETOOTH:I = 0x3

.field public static final UT_BLUETOOTH_VPN:I = 0x9

.field public static final UT_CELLULAR:I = 0x1

.field public static final UT_CELLULAR_VPN:I = 0x7

.field public static final UT_DUN_CELLULAR:I = 0xd

.field public static final UT_ETHERNET:I = 0x4

.field public static final UT_ETHERNET_VPN:I = 0xa

.field public static final UT_LOWPAN:I = 0x6

.field public static final UT_TEST:I = 0xc

.field public static final UT_UNKNOWN:I = 0x0

.field public static final UT_WIFI:I = 0x2

.field public static final UT_WIFI_AWARE:I = 0x5

.field public static final UT_WIFI_CELLULAR_VPN:I = 0xb

.field public static final UT_WIFI_VPN:I = 0x8


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
