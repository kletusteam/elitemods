.class public final Landroid/stats/textclassifier/TextclassifierEnums;
.super Ljava/lang/Object;


# static fields
.field public static final ACTIONS_GENERATED:I = 0x14

.field public static final ACTIONS_SHOWN:I = 0x6

.field public static final AUTO_SELECTION:I = 0x5

.field public static final COPY_ACTION:I = 0x9

.field public static final CUT_ACTION:I = 0xb

.field public static final LINKS_GENERATED:I = 0x15

.field public static final LINK_CLICKED:I = 0x7

.field public static final MANUAL_REPLY:I = 0x13

.field public static final OTHER_ACTION:I = 0x10

.field public static final OVERTYPE:I = 0x8

.field public static final PASTE_ACTION:I = 0xa

.field public static final READ_CLIPBOARD:I = 0x16

.field public static final SELECTION_DESTROYED:I = 0xf

.field public static final SELECTION_DRAG:I = 0xe

.field public static final SELECTION_MODIFIED:I = 0x2

.field public static final SELECTION_RESET:I = 0x12

.field public static final SELECTION_STARTED:I = 0x1

.field public static final SELECT_ALL:I = 0x11

.field public static final SHARE_ACTION:I = 0xc

.field public static final SMART_ACTION:I = 0xd

.field public static final SMART_SELECTION_MULTI:I = 0x4

.field public static final SMART_SELECTION_SINGLE:I = 0x3

.field public static final TYPE_UNKNOWN:I = 0x0

.field public static final WIDGET_TYPE_CLIPBOARD:I = 0xa

.field public static final WIDGET_TYPE_CUSTOM_EDITTEXT:I = 0x7

.field public static final WIDGET_TYPE_CUSTOM_TEXTVIEW:I = 0x6

.field public static final WIDGET_TYPE_CUSTOM_UNSELECTABLE_TEXTVIEW:I = 0x8

.field public static final WIDGET_TYPE_EDITTEXT:I = 0x2

.field public static final WIDGET_TYPE_EDIT_WEBVIEW:I = 0x5

.field public static final WIDGET_TYPE_NOTIFICATION:I = 0x9

.field public static final WIDGET_TYPE_TEXTVIEW:I = 0x1

.field public static final WIDGET_TYPE_UNKNOWN:I = 0x0

.field public static final WIDGET_TYPE_UNSELECTABLE_TEXTVIEW:I = 0x3

.field public static final WIDGET_TYPE_WEBVIEW:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
