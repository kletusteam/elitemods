.class public final Landroid/stats/otaupdate/UpdateengineEnums;
.super Ljava/lang/Object;


# static fields
.field public static final ABNORMAL_TERMINATION:I = 0x271a

.field public static final DELTA:I = 0x2711

.field public static final DOWNLOAD_INVALID_METADATA_MAGIC_STRING:I = 0x2725

.field public static final DOWNLOAD_INVALID_METADATA_SIGNATURE:I = 0x2731

.field public static final DOWNLOAD_INVALID_METADATA_SIZE:I = 0x2730

.field public static final DOWNLOAD_MANIFEST_PARSE_ERROR:I = 0x2727

.field public static final DOWNLOAD_METADATA_SIGNATURE_ERROR:I = 0x2728

.field public static final DOWNLOAD_METADATA_SIGNATURE_MISMATCH:I = 0x272a

.field public static final DOWNLOAD_METADATA_SIGNATURE_MISSING_ERROR:I = 0x2737

.field public static final DOWNLOAD_METADATA_SIGNATURE_VERIFICATION_ERROR:I = 0x2729

.field public static final DOWNLOAD_NEW_PARTITION_INFO_ERROR:I = 0x271d

.field public static final DOWNLOAD_OPERATION_EXECUTION_ERROR:I = 0x272c

.field public static final DOWNLOAD_OPERATION_HASH_MISMATCH:I = 0x272d

.field public static final DOWNLOAD_OPERATION_HASH_MISSING_ERROR:I = 0x2736

.field public static final DOWNLOAD_OPERATION_HASH_VERIFICATION_ERROR:I = 0x272b

.field public static final DOWNLOAD_PAYLOAD_PUB_KEY_VERIFICATION_ERROR:I = 0x2722

.field public static final DOWNLOAD_PAYLOAD_VERIFICATION_ERROR:I = 0x271c

.field public static final DOWNLOAD_SIGNATURE_MISSING_IN_MANIFEST:I = 0x2726

.field public static final DOWNLOAD_STATE_INITIALIZATION_ERROR:I = 0x2724

.field public static final DOWNLOAD_TRANSFER_ERROR:I = 0x2719

.field public static final DOWNLOAD_WRITE_ERROR:I = 0x271e

.field public static final ERROR:I = 0x2711

.field public static final FILESYSTEM_COPIER_ERROR:I = 0x2714

.field public static final FILESYSTEM_VERIFIER_ERROR:I = 0x273f

.field public static final FULL:I = 0x2710

.field public static final INSTALL_DEVICE_OPEN_ERROR:I = 0x2717

.field public static final INTERNAL_ERROR:I = 0x2711

.field public static final KERNEL_DEVICE_OPEN_ERROR:I = 0x2718

.field public static final METADATA_MALFORMED:I = 0x2713

.field public static final METADATA_VERIFICATION_FAILED:I = 0x2716

.field public static final NEW_ROOTFS_VERIFICATION_ERROR:I = 0x271f

.field public static final OPERATION_EXECUTION_ERROR:I = 0x2715

.field public static final OPERATION_MALFORMED:I = 0x2714

.field public static final PAYLOAD_DOWNLOAD_ERROR:I = 0x2712

.field public static final PAYLOAD_HASH_MISMATCH_ERROR:I = 0x271a

.field public static final PAYLOAD_MISMATCHED_TYPE_ERROR:I = 0x2716

.field public static final PAYLOAD_SIZE_MISMATCH_ERROR:I = 0x271b

.field public static final PAYLOAD_TIMESTAMP_ERROR:I = 0x2743

.field public static final PAYLOAD_VERIFICATION_FAILED:I = 0x2717

.field public static final POSTINSTALL_FAILED:I = 0x2719

.field public static final POST_INSTALL_RUNNER_ERROR:I = 0x2715

.field public static final SIGNED_DELTA_PAYLOAD_EXPECTED_ERROR:I = 0x2721

.field public static final SUCCESS:I = 0x2710

.field public static final UNSUPPORTED_MAJOR_PAYLOAD_VERSION:I = 0x273c

.field public static final UNSUPPORTED_MINOR_PAYLOAD_VERSION:I = 0x273d

.field public static final UPDATED_BUT_NOT_ACTIVE:I = 0x2744

.field public static final UPDATE_CANCELED:I = 0x271b

.field public static final UPDATE_SUCCEEDED:I = 0x2710

.field public static final UPDATE_SUCCEEDED_NOT_ACTIVE:I = 0x271c

.field public static final USER_CANCELED:I = 0x2740

.field public static final VERIFICATION_FAILED:I = 0x2718


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
