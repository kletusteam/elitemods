.class public final Landroid/stats/hdmi/HdmiStatsEnums;
.super Ljava/lang/Object;


# static fields
.field public static final ANCESTOR:I = 0x2

.field public static final AUDIO_SYSTEM:I = 0x5

.field public static final BUSY:I = 0xc

.field public static final CANNOT_PROVIDE_SOURCE:I = 0xc

.field public static final DESCENDANT:I = 0x3

.field public static final DIFFERENT_BRANCH:I = 0x1

.field public static final DOWN:I = 0x102

.field public static final EXIT:I = 0x10d

.field public static final FAIL:I = 0xd

.field public static final FEATURE_ABORT_REASON_UNKNOWN:I = 0x0

.field public static final INCOMING:I = 0x3

.field public static final INPUT_SELECT:I = 0x134

.field public static final INVALID_OPERAND:I = 0xd

.field public static final LEFT:I = 0x103

.field public static final LEFT_DOWN:I = 0x108

.field public static final LEFT_UP:I = 0x107

.field public static final LOGICAL_ADDRESS_UNKNOWN:I = -0x1

.field public static final MESSAGE_DIRECTION_OTHER:I = 0x1

.field public static final MESSAGE_DIRECTION_UNKNOWN:I = 0x0

.field public static final NACK:I = 0xb

.field public static final NOT_IN_CORRECT_MODE_TO_RESPOND:I = 0xb

.field public static final NUMBER:I = 0x2

.field public static final OUTGOING:I = 0x2

.field public static final PLAYBACK_DEVICE_1:I = 0x4

.field public static final PLAYBACK_DEVICE_2:I = 0x8

.field public static final PLAYBACK_DEVICE_3:I = 0xb

.field public static final POWER:I = 0x140

.field public static final POWER_OFF:I = 0x16c

.field public static final POWER_ON:I = 0x16d

.field public static final POWER_TOGGLE:I = 0x16b

.field public static final RECORDING_DEVICE_1:I = 0x1

.field public static final RECORDING_DEVICE_2:I = 0x2

.field public static final RECORDING_DEVICE_3:I = 0x9

.field public static final REFUSED:I = 0xe

.field public static final RELATIONSHIP_TO_ACTIVE_SOURCE_UNKNOWN:I = 0x0

.field public static final RESERVED_1:I = 0xc

.field public static final RESERVED_2:I = 0xd

.field public static final RIGHT:I = 0x104

.field public static final RIGHT_DOWN:I = 0x106

.field public static final RIGHT_UP:I = 0x105

.field public static final SAME:I = 0x5

.field public static final SELECT:I = 0x100

.field public static final SEND_MESSAGE_RESULT_UNKNOWN:I = 0x0

.field public static final SIBLING:I = 0x4

.field public static final SPECIFIC_USE:I = 0xe

.field public static final SUCCESS:I = 0xa

.field public static final TO_SELF:I = 0x4

.field public static final TUNER_1:I = 0x3

.field public static final TUNER_2:I = 0x6

.field public static final TUNER_3:I = 0x7

.field public static final TUNER_4:I = 0xa

.field public static final TV:I = 0x0

.field public static final UNABLE_TO_DETERMINE:I = 0xf

.field public static final UNRECOGNIZED_OPCODE:I = 0xa

.field public static final UNREGISTERED_OR_BROADCAST:I = 0xf

.field public static final UP:I = 0x101

.field public static final USER_CONTROL_PRESSED_COMMAND_OTHER:I = 0x1

.field public static final USER_CONTROL_PRESSED_COMMAND_UNKNOWN:I = 0x0

.field public static final VOLUME_DOWN:I = 0x142

.field public static final VOLUME_MUTE:I = 0x143

.field public static final VOLUME_UP:I = 0x141


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
