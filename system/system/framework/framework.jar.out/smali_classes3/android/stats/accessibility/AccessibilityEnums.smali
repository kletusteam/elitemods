.class public final Landroid/stats/accessibility/AccessibilityEnums;
.super Ljava/lang/Object;


# static fields
.field public static final A11Y_BUTTON:I = 0x1

.field public static final A11Y_BUTTON_LONG_PRESS:I = 0x4

.field public static final A11Y_FLOATING_MENU:I = 0x5

.field public static final A11Y_GESTURE:I = 0x6

.field public static final DISABLED:I = 0x2

.field public static final ENABLED:I = 0x1

.field public static final MAGNIFICATION_ALL:I = 0x3

.field public static final MAGNIFICATION_FULL_SCREEN:I = 0x1

.field public static final MAGNIFICATION_UNKNOWN_MODE:I = 0x0

.field public static final MAGNIFICATION_WINDOW:I = 0x2

.field public static final TEXT_READING_ACCESSIBILITY_SETTINGS:I = 0x4

.field public static final TEXT_READING_BOLD_TEXT:I = 0x3

.field public static final TEXT_READING_DISPLAY_SETTINGS:I = 0x3

.field public static final TEXT_READING_DISPLAY_SIZE:I = 0x2

.field public static final TEXT_READING_FONT_SIZE:I = 0x1

.field public static final TEXT_READING_HIGH_CONTRAST_TEXT:I = 0x4

.field public static final TEXT_READING_RESET:I = 0x5

.field public static final TEXT_READING_SUW_ANYTHING_ELSE:I = 0x2

.field public static final TEXT_READING_SUW_VISION_SETTINGS:I = 0x1

.field public static final TEXT_READING_UNKNOWN_ENTRY:I = 0x0

.field public static final TEXT_READING_UNKNOWN_ITEM:I = 0x0

.field public static final TRIPLE_TAP:I = 0x3

.field public static final UNKNOWN:I = 0x0

.field public static final UNKNOWN_TYPE:I = 0x0

.field public static final VOLUME_KEY:I = 0x2

.field public static final WARNING_CLICKED:I = 0x2

.field public static final WARNING_SERVICE_DISABLED:I = 0x3

.field public static final WARNING_SHOWN:I = 0x1

.field public static final WARNING_UNKNOWN:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
