.class public final Landroid/providers/settings/ConfigSettingsProto;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/providers/settings/ConfigSettingsProto$NamespaceProto;
    }
.end annotation


# static fields
.field public static final ACTIVITY_MANAGER_NATIVE_BOOT_SETTINGS:J = 0x20b00000003L

.field public static final ACTIVITY_MANAGER_SETTINGS:J = 0x20b00000004L

.field public static final ALARM_MANAGER_SETTINGS:J = 0x20b0000001aL

.field public static final APP_COMPAT_SETTINGS:J = 0x20b00000005L

.field public static final APP_STANDBY_SETTINGS:J = 0x20b0000001bL

.field public static final AUTOFILL_SETTINGS:J = 0x20b00000006L

.field public static final BLOBSTORE_SETTINGS:J = 0x20b00000017L

.field public static final CONNECTIVITY_SETTINGS:J = 0x20b00000007L

.field public static final CONTENT_CAPTURE_SETTINGS:J = 0x20b00000008L

.field public static final DEVICE_IDLE_SETTINGS:J = 0x20b00000018L

.field public static final EXTRA_NAMESPACES:J = 0x20b00000002L

.field public static final GAME_DRIVER_SETTINGS:J = 0x20b0000000aL

.field public static final HISTORICAL_OPERATIONS:J = 0x20b00000001L

.field public static final INPUT_NATIVE_BOOT_SETTINGS:J = 0x20b0000000bL

.field public static final JOB_SCHEDULER_SETTINGS:J = 0x20b00000019L

.field public static final NETD_NATIVE_SETTINGS:J = 0x20b0000000cL

.field public static final PRIVACY_SETTINGS:J = 0x20b0000000dL

.field public static final ROLLBACK_BOOT_SETTINGS:J = 0x20b0000000eL

.field public static final ROLLBACK_SETTINGS:J = 0x20b0000000fL

.field public static final RUNTIME_NATIVE_BOOT_SETTINGS:J = 0x20b00000010L

.field public static final RUNTIME_NATIVE_SETTINGS:J = 0x20b00000011L

.field public static final RUNTIME_SETTINGS:J = 0x20b00000012L

.field public static final STORAGE_SETTINGS:J = 0x20b00000013L

.field public static final SYSTEMUI_SETTINGS:J = 0x20b00000014L

.field public static final TELEPHONY_SETTINGS:J = 0x20b00000015L

.field public static final TEXTCLASSIFIER_SETTINGS:J = 0x20b00000016L


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
