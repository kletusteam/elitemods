.class Landroid/sax/Children;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/sax/Children$Child;
    }
.end annotation


# instance fields
.field children:[Landroid/sax/Children$Child;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x10

    new-array v0, v0, [Landroid/sax/Children$Child;

    iput-object v0, p0, Landroid/sax/Children;->children:[Landroid/sax/Children$Child;

    return-void
.end method


# virtual methods
.method get(Ljava/lang/String;Ljava/lang/String;)Landroid/sax/Element;
    .locals 5

    goto/32 :goto_9

    nop

    :goto_0
    if-eqz v4, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop

    :goto_1
    return-object v2

    :goto_2
    goto/32 :goto_14

    nop

    :goto_3
    iget-object v4, v2, Landroid/sax/Children$Child;->uri:Ljava/lang/String;

    goto/32 :goto_10

    nop

    :goto_4
    iget-object v4, v2, Landroid/sax/Children$Child;->localName:Ljava/lang/String;

    goto/32 :goto_8

    nop

    :goto_5
    if-eqz v4, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_4

    nop

    :goto_6
    return-object v3

    :goto_7
    goto/32 :goto_d

    nop

    :goto_8
    invoke-virtual {v4, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    goto/32 :goto_0

    nop

    :goto_9
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/32 :goto_17

    nop

    :goto_a
    and-int/lit8 v1, v0, 0xf

    goto/32 :goto_18

    nop

    :goto_b
    return-object v3

    :goto_c
    goto/32 :goto_12

    nop

    :goto_d
    goto :goto_c

    :goto_e
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto/32 :goto_13

    nop

    :goto_f
    if-eqz v2, :cond_2

    goto/32 :goto_7

    :cond_2
    goto/32 :goto_6

    nop

    :goto_10
    invoke-virtual {v4, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    goto/32 :goto_5

    nop

    :goto_11
    const/4 v3, 0x0

    goto/32 :goto_16

    nop

    :goto_12
    iget v4, v2, Landroid/sax/Children$Child;->hash:I

    goto/32 :goto_15

    nop

    :goto_13
    add-int/2addr v0, v1

    goto/32 :goto_a

    nop

    :goto_14
    iget-object v2, v2, Landroid/sax/Children$Child;->next:Landroid/sax/Children$Child;

    goto/32 :goto_f

    nop

    :goto_15
    if-eq v4, v0, :cond_3

    goto/32 :goto_2

    :cond_3
    goto/32 :goto_3

    nop

    :goto_16
    if-eqz v2, :cond_4

    goto/32 :goto_c

    :cond_4
    goto/32 :goto_b

    nop

    :goto_17
    mul-int/lit8 v0, v0, 0x1f

    goto/32 :goto_e

    nop

    :goto_18
    iget-object v2, p0, Landroid/sax/Children;->children:[Landroid/sax/Children$Child;

    goto/32 :goto_19

    nop

    :goto_19
    aget-object v2, v2, v1

    goto/32 :goto_11

    nop
.end method

.method getOrCreate(Landroid/sax/Element;Ljava/lang/String;Ljava/lang/String;)Landroid/sax/Element;
    .locals 11

    goto/32 :goto_2c

    nop

    :goto_0
    aput-object v2, v3, v1

    goto/32 :goto_29

    nop

    :goto_1
    if-eqz v2, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_6

    nop

    :goto_2
    and-int/lit8 v1, v0, 0xf

    goto/32 :goto_f

    nop

    :goto_3
    invoke-virtual {p3}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto/32 :goto_9

    nop

    :goto_4
    add-int/lit8 v6, v2, 0x1

    goto/32 :goto_21

    nop

    :goto_5
    move-object v4, p2

    goto/32 :goto_26

    nop

    :goto_6
    return-object v8

    :goto_7
    goto/32 :goto_18

    nop

    :goto_8
    move v7, v0

    goto/32 :goto_27

    nop

    :goto_9
    add-int/2addr v0, v1

    goto/32 :goto_2

    nop

    :goto_a
    move-object v5, p3

    goto/32 :goto_8

    nop

    :goto_b
    move v7, v0

    goto/32 :goto_1d

    nop

    :goto_c
    invoke-virtual {v2, p3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    goto/32 :goto_1

    nop

    :goto_d
    return-object v2

    :goto_e
    goto/32 :goto_12

    nop

    :goto_f
    iget-object v2, p0, Landroid/sax/Children;->children:[Landroid/sax/Children$Child;

    goto/32 :goto_19

    nop

    :goto_10
    move-object v4, p2

    goto/32 :goto_a

    nop

    :goto_11
    move-object v3, p1

    goto/32 :goto_5

    nop

    :goto_12
    goto/16 :goto_2a

    :goto_13
    new-instance v10, Landroid/sax/Children$Child;

    goto/32 :goto_25

    nop

    :goto_14
    move-object v3, p1

    goto/32 :goto_10

    nop

    :goto_15
    add-int/lit8 v6, v2, 0x1

    goto/32 :goto_1a

    nop

    :goto_16
    if-eqz v2, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_1e

    nop

    :goto_17
    iget-object v3, p0, Landroid/sax/Children;->children:[Landroid/sax/Children$Child;

    goto/32 :goto_0

    nop

    :goto_18
    move-object v9, v8

    goto/32 :goto_23

    nop

    :goto_19
    aget-object v8, v2, v1

    goto/32 :goto_1c

    nop

    :goto_1a
    move-object v2, v10

    goto/32 :goto_11

    nop

    :goto_1b
    iget-object v2, v8, Landroid/sax/Children$Child;->uri:Ljava/lang/String;

    goto/32 :goto_2d

    nop

    :goto_1c
    if-eqz v8, :cond_2

    goto/32 :goto_2a

    :cond_2
    goto/32 :goto_24

    nop

    :goto_1d
    invoke-direct/range {v2 .. v7}, Landroid/sax/Children$Child;-><init>(Landroid/sax/Element;Ljava/lang/String;Ljava/lang/String;II)V

    goto/32 :goto_1f

    nop

    :goto_1e
    iget-object v2, v8, Landroid/sax/Children$Child;->localName:Ljava/lang/String;

    goto/32 :goto_c

    nop

    :goto_1f
    iput-object v2, v9, Landroid/sax/Children$Child;->next:Landroid/sax/Children$Child;

    goto/32 :goto_d

    nop

    :goto_20
    if-eqz v8, :cond_3

    goto/32 :goto_e

    :cond_3
    goto/32 :goto_13

    nop

    :goto_21
    move-object v2, v9

    goto/32 :goto_14

    nop

    :goto_22
    mul-int/lit8 v0, v0, 0x1f

    goto/32 :goto_3

    nop

    :goto_23
    iget-object v8, v8, Landroid/sax/Children$Child;->next:Landroid/sax/Children$Child;

    goto/32 :goto_20

    nop

    :goto_24
    new-instance v9, Landroid/sax/Children$Child;

    goto/32 :goto_2b

    nop

    :goto_25
    iget v2, p1, Landroid/sax/Element;->depth:I

    goto/32 :goto_15

    nop

    :goto_26
    move-object v5, p3

    goto/32 :goto_b

    nop

    :goto_27
    invoke-direct/range {v2 .. v7}, Landroid/sax/Children$Child;-><init>(Landroid/sax/Element;Ljava/lang/String;Ljava/lang/String;II)V

    goto/32 :goto_17

    nop

    :goto_28
    if-eq v2, v0, :cond_4

    goto/32 :goto_7

    :cond_4
    goto/32 :goto_1b

    nop

    :goto_29
    return-object v2

    :goto_2a
    goto/32 :goto_2e

    nop

    :goto_2b
    iget v2, p1, Landroid/sax/Element;->depth:I

    goto/32 :goto_4

    nop

    :goto_2c
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/32 :goto_22

    nop

    :goto_2d
    invoke-virtual {v2, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    goto/32 :goto_16

    nop

    :goto_2e
    iget v2, v8, Landroid/sax/Children$Child;->hash:I

    goto/32 :goto_28

    nop
.end method
