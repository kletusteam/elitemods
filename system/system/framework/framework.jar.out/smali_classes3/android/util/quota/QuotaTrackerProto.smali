.class public final Landroid/util/quota/QuotaTrackerProto;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/util/quota/QuotaTrackerProto$InQuotaAlarmListener;
    }
.end annotation


# static fields
.field public static final ELAPSED_REALTIME:J = 0x10300000003L

.field public static final IN_QUOTA_ALARM_LISTENER:J = 0x10b00000004L

.field public static final IS_ENABLED:J = 0x10800000001L

.field public static final IS_GLOBAL_QUOTA_FREE:J = 0x10800000002L


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
