.class public Landroid/util/SystemConfigFileCommitEventLogger;
.super Ljava/lang/Object;


# annotations
.annotation runtime Landroid/annotation/SystemApi;
    client = .enum Landroid/annotation/SystemApi$Client;->MODULE_LIBRARIES:Landroid/annotation/SystemApi$Client;
.end annotation


# instance fields
.field private final mName:Ljava/lang/String;

.field private mStartTime:J


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/util/SystemConfigFileCommitEventLogger;->mName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method onFinishWrite()V
    .locals 5

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/util/SystemConfigFileCommitEventLogger;->mName:Ljava/lang/String;

    goto/32 :goto_3

    nop

    :goto_1
    sub-long/2addr v1, v3

    goto/32 :goto_4

    nop

    :goto_2
    return-void

    :goto_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    goto/32 :goto_5

    nop

    :goto_4
    invoke-static {v0, v1, v2}, Lcom/android/internal/logging/EventLogTags;->writeCommitSysConfigFile(Ljava/lang/String;J)V

    goto/32 :goto_2

    nop

    :goto_5
    iget-wide v3, p0, Landroid/util/SystemConfigFileCommitEventLogger;->mStartTime:J

    goto/32 :goto_1

    nop
.end method

.method onStartWrite()V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    goto/32 :goto_2

    nop

    :goto_2
    iput-wide v0, p0, Landroid/util/SystemConfigFileCommitEventLogger;->mStartTime:J

    goto/32 :goto_0

    nop
.end method

.method public setStartTime(J)V
    .locals 0

    iput-wide p1, p0, Landroid/util/SystemConfigFileCommitEventLogger;->mStartTime:J

    return-void
.end method
