.class public interface abstract Landroid/util/TrustedTime;
.super Ljava/lang/Object;


# virtual methods
.method public abstract currentTimeMillis()J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract forceRefresh()Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract getCacheAge()J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract hasCache()Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method
