.class public Landroid/renderscript/Mesh;
.super Landroid/renderscript/BaseObj;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/renderscript/Mesh$TriangleMeshBuilder;,
        Landroid/renderscript/Mesh$AllocationBuilder;,
        Landroid/renderscript/Mesh$Builder;,
        Landroid/renderscript/Mesh$Primitive;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field mIndexBuffers:[Landroid/renderscript/Allocation;

.field mPrimitives:[Landroid/renderscript/Mesh$Primitive;

.field mVertexBuffers:[Landroid/renderscript/Allocation;


# direct methods
.method constructor <init>(JLandroid/renderscript/RenderScript;)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Landroid/renderscript/BaseObj;-><init>(JLandroid/renderscript/RenderScript;)V

    iget-object v0, p0, Landroid/renderscript/Mesh;->guard:Ldalvik/system/CloseGuard;

    const-string v1, "destroy"

    invoke-virtual {v0, v1}, Ldalvik/system/CloseGuard;->open(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getIndexSetAllocation(I)Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Landroid/renderscript/Mesh;->mIndexBuffers:[Landroid/renderscript/Allocation;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getPrimitive(I)Landroid/renderscript/Mesh$Primitive;
    .locals 1

    iget-object v0, p0, Landroid/renderscript/Mesh;->mPrimitives:[Landroid/renderscript/Mesh$Primitive;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getPrimitiveCount()I
    .locals 1

    iget-object v0, p0, Landroid/renderscript/Mesh;->mIndexBuffers:[Landroid/renderscript/Allocation;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    array-length v0, v0

    return v0
.end method

.method public getVertexAllocation(I)Landroid/renderscript/Allocation;
    .locals 1

    iget-object v0, p0, Landroid/renderscript/Mesh;->mVertexBuffers:[Landroid/renderscript/Allocation;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getVertexAllocationCount()I
    .locals 1

    iget-object v0, p0, Landroid/renderscript/Mesh;->mVertexBuffers:[Landroid/renderscript/Allocation;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    array-length v0, v0

    return v0
.end method

.method updateFromNative()V
    .locals 18

    goto/32 :goto_1b

    nop

    :goto_0
    move-object v8, v4

    goto/32 :goto_39

    nop

    :goto_1
    new-array v4, v2, [J

    goto/32 :goto_21

    nop

    :goto_2
    cmp-long v6, v8, v6

    goto/32 :goto_17

    nop

    :goto_3
    iget-object v6, v0, Landroid/renderscript/Mesh;->mVertexBuffers:[Landroid/renderscript/Allocation;

    goto/32 :goto_44

    nop

    :goto_4
    iget-object v8, v0, Landroid/renderscript/Mesh;->mIndexBuffers:[Landroid/renderscript/Allocation;

    goto/32 :goto_3b

    nop

    :goto_5
    const/16 v17, 0x1

    goto/32 :goto_8

    nop

    :goto_6
    invoke-virtual {v0, v2}, Landroid/renderscript/Mesh;->getID(Landroid/renderscript/RenderScript;)J

    move-result-wide v2

    goto/32 :goto_20

    nop

    :goto_7
    aget-wide v8, v4, v5

    goto/32 :goto_b

    nop

    :goto_8
    move-object v12, v7

    goto/32 :goto_41

    nop

    :goto_9
    iget-object v6, v0, Landroid/renderscript/Mesh;->mRS:Landroid/renderscript/RenderScript;

    goto/32 :goto_34

    nop

    :goto_a
    new-array v5, v2, [Landroid/renderscript/Allocation;

    goto/32 :goto_3a

    nop

    :goto_b
    cmp-long v8, v8, v6

    goto/32 :goto_46

    nop

    :goto_c
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_1d

    nop

    :goto_d
    invoke-virtual {v2, v3, v4}, Landroid/renderscript/RenderScript;->nMeshGetIndexCount(J)I

    move-result v2

    goto/32 :goto_47

    nop

    :goto_e
    const/16 v16, 0x0

    goto/32 :goto_18

    nop

    :goto_f
    iget-object v5, v0, Landroid/renderscript/Mesh;->mRS:Landroid/renderscript/RenderScript;

    goto/32 :goto_50

    nop

    :goto_10
    invoke-super/range {p0 .. p0}, Landroid/renderscript/BaseObj;->updateFromNative()V

    goto/32 :goto_11

    nop

    :goto_11
    iget-object v1, v0, Landroid/renderscript/Mesh;->mRS:Landroid/renderscript/RenderScript;

    goto/32 :goto_4f

    nop

    :goto_12
    aget-wide v13, v4, v5

    goto/32 :goto_23

    nop

    :goto_13
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_35

    nop

    :goto_14
    invoke-virtual {v0, v6}, Landroid/renderscript/Mesh;->getID(Landroid/renderscript/RenderScript;)J

    move-result-wide v6

    goto/32 :goto_0

    nop

    :goto_15
    iget-object v8, v0, Landroid/renderscript/Mesh;->mIndexBuffers:[Landroid/renderscript/Allocation;

    goto/32 :goto_37

    nop

    :goto_16
    aget-wide v13, v3, v5

    goto/32 :goto_30

    nop

    :goto_17
    if-nez v6, :cond_0

    goto/32 :goto_32

    :cond_0
    goto/32 :goto_3

    nop

    :goto_18
    const/16 v17, 0x1

    goto/32 :goto_4c

    nop

    :goto_19
    invoke-virtual/range {v5 .. v10}, Landroid/renderscript/RenderScript;->nMeshGetIndices(J[J[II)V

    goto/32 :goto_29

    nop

    :goto_1a
    aget-object v6, v6, v5

    goto/32 :goto_31

    nop

    :goto_1b
    move-object/from16 v0, p0

    goto/32 :goto_10

    nop

    :goto_1c
    iget-object v8, v0, Landroid/renderscript/Mesh;->mPrimitives:[Landroid/renderscript/Mesh$Primitive;

    goto/32 :goto_4d

    nop

    :goto_1d
    goto :goto_2d

    :goto_1e
    goto/32 :goto_2f

    nop

    :goto_1f
    iget-object v2, v0, Landroid/renderscript/Mesh;->mRS:Landroid/renderscript/RenderScript;

    goto/32 :goto_48

    nop

    :goto_20
    invoke-virtual {v1, v2, v3}, Landroid/renderscript/RenderScript;->nMeshGetVertexBufferCount(J)I

    move-result v1

    goto/32 :goto_1f

    nop

    :goto_21
    new-array v11, v2, [I

    goto/32 :goto_22

    nop

    :goto_22
    iget-object v5, v0, Landroid/renderscript/Mesh;->mRS:Landroid/renderscript/RenderScript;

    goto/32 :goto_9

    nop

    :goto_23
    iget-object v15, v0, Landroid/renderscript/Mesh;->mRS:Landroid/renderscript/RenderScript;

    goto/32 :goto_e

    nop

    :goto_24
    aput-object v9, v8, v5

    goto/32 :goto_c

    nop

    :goto_25
    move v10, v2

    goto/32 :goto_19

    nop

    :goto_26
    invoke-virtual {v8}, Landroid/renderscript/Allocation;->updateFromNative()V

    :goto_27
    goto/32 :goto_1c

    nop

    :goto_28
    const/16 v16, 0x0

    goto/32 :goto_5

    nop

    :goto_29
    new-array v5, v1, [Landroid/renderscript/Allocation;

    goto/32 :goto_3c

    nop

    :goto_2a
    aput-object v9, v8, v5

    goto/32 :goto_15

    nop

    :goto_2b
    aget v10, v11, v5

    goto/32 :goto_49

    nop

    :goto_2c
    const/4 v5, 0x0

    :goto_2d
    goto/32 :goto_4a

    nop

    :goto_2e
    aput-object v7, v6, v5

    goto/32 :goto_42

    nop

    :goto_2f
    return-void

    :goto_30
    iget-object v15, v0, Landroid/renderscript/Mesh;->mRS:Landroid/renderscript/RenderScript;

    goto/32 :goto_28

    nop

    :goto_31
    invoke-virtual {v6}, Landroid/renderscript/Allocation;->updateFromNative()V

    :goto_32
    goto/32 :goto_13

    nop

    :goto_33
    invoke-direct/range {v12 .. v17}, Landroid/renderscript/Allocation;-><init>(JLandroid/renderscript/RenderScript;Landroid/renderscript/Type;I)V

    goto/32 :goto_2a

    nop

    :goto_34
    invoke-virtual {v0, v6}, Landroid/renderscript/Mesh;->getID(Landroid/renderscript/RenderScript;)J

    move-result-wide v6

    goto/32 :goto_45

    nop

    :goto_35
    goto :goto_40

    :goto_36
    goto/32 :goto_2c

    nop

    :goto_37
    aget-object v8, v8, v5

    goto/32 :goto_26

    nop

    :goto_38
    invoke-virtual {v0, v3}, Landroid/renderscript/Mesh;->getID(Landroid/renderscript/RenderScript;)J

    move-result-wide v3

    goto/32 :goto_d

    nop

    :goto_39
    move-object v9, v11

    goto/32 :goto_25

    nop

    :goto_3a
    iput-object v5, v0, Landroid/renderscript/Mesh;->mIndexBuffers:[Landroid/renderscript/Allocation;

    goto/32 :goto_4e

    nop

    :goto_3b
    new-instance v9, Landroid/renderscript/Allocation;

    goto/32 :goto_12

    nop

    :goto_3c
    iput-object v5, v0, Landroid/renderscript/Mesh;->mVertexBuffers:[Landroid/renderscript/Allocation;

    goto/32 :goto_a

    nop

    :goto_3d
    if-lt v5, v1, :cond_1

    goto/32 :goto_36

    :cond_1
    goto/32 :goto_3e

    nop

    :goto_3e
    aget-wide v8, v3, v5

    goto/32 :goto_2

    nop

    :goto_3f
    const/4 v5, 0x0

    :goto_40
    goto/32 :goto_4b

    nop

    :goto_41
    invoke-direct/range {v12 .. v17}, Landroid/renderscript/Allocation;-><init>(JLandroid/renderscript/RenderScript;Landroid/renderscript/Type;I)V

    goto/32 :goto_2e

    nop

    :goto_42
    iget-object v6, v0, Landroid/renderscript/Mesh;->mVertexBuffers:[Landroid/renderscript/Allocation;

    goto/32 :goto_1a

    nop

    :goto_43
    iput-object v5, v0, Landroid/renderscript/Mesh;->mPrimitives:[Landroid/renderscript/Mesh$Primitive;

    goto/32 :goto_3f

    nop

    :goto_44
    new-instance v7, Landroid/renderscript/Allocation;

    goto/32 :goto_16

    nop

    :goto_45
    invoke-virtual {v5, v6, v7, v3, v1}, Landroid/renderscript/RenderScript;->nMeshGetVertices(J[JI)V

    goto/32 :goto_f

    nop

    :goto_46
    if-nez v8, :cond_2

    goto/32 :goto_27

    :cond_2
    goto/32 :goto_4

    nop

    :goto_47
    new-array v3, v1, [J

    goto/32 :goto_1

    nop

    :goto_48
    iget-object v3, v0, Landroid/renderscript/Mesh;->mRS:Landroid/renderscript/RenderScript;

    goto/32 :goto_38

    nop

    :goto_49
    aget-object v9, v9, v10

    goto/32 :goto_24

    nop

    :goto_4a
    if-lt v5, v2, :cond_3

    goto/32 :goto_1e

    :cond_3
    goto/32 :goto_7

    nop

    :goto_4b
    const-wide/16 v6, 0x0

    goto/32 :goto_3d

    nop

    :goto_4c
    move-object v12, v9

    goto/32 :goto_33

    nop

    :goto_4d
    invoke-static {}, Landroid/renderscript/Mesh$Primitive;->values()[Landroid/renderscript/Mesh$Primitive;

    move-result-object v9

    goto/32 :goto_2b

    nop

    :goto_4e
    new-array v5, v2, [Landroid/renderscript/Mesh$Primitive;

    goto/32 :goto_43

    nop

    :goto_4f
    iget-object v2, v0, Landroid/renderscript/Mesh;->mRS:Landroid/renderscript/RenderScript;

    goto/32 :goto_6

    nop

    :goto_50
    iget-object v6, v0, Landroid/renderscript/Mesh;->mRS:Landroid/renderscript/RenderScript;

    goto/32 :goto_14

    nop
.end method
