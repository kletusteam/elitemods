.class public Landroid/renderscript/Type;
.super Landroid/renderscript/BaseObj;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/renderscript/Type$Builder;,
        Landroid/renderscript/Type$CubemapFace;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static final mMaxArrays:I = 0x4


# instance fields
.field mArrays:[I

.field mDimFaces:Z

.field mDimMipmaps:Z

.field mDimX:I

.field mDimY:I

.field mDimYuv:I

.field mDimZ:I

.field mElement:Landroid/renderscript/Element;

.field mElementCount:I


# direct methods
.method constructor <init>(JLandroid/renderscript/RenderScript;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/renderscript/BaseObj;-><init>(JLandroid/renderscript/RenderScript;)V

    return-void
.end method

.method public static createX(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;I)Landroid/renderscript/Type;
    .locals 10

    const/4 v0, 0x1

    if-lt p2, v0, :cond_0

    invoke-virtual {p1, p0}, Landroid/renderscript/Element;->getID(Landroid/renderscript/RenderScript;)J

    move-result-wide v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, p0

    move v4, p2

    invoke-virtual/range {v1 .. v9}, Landroid/renderscript/RenderScript;->nTypeCreate(JIIIZZI)J

    move-result-wide v0

    new-instance v2, Landroid/renderscript/Type;

    invoke-direct {v2, v0, v1, p0}, Landroid/renderscript/Type;-><init>(JLandroid/renderscript/RenderScript;)V

    iput-object p1, v2, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    iput p2, v2, Landroid/renderscript/Type;->mDimX:I

    invoke-virtual {v2}, Landroid/renderscript/Type;->calcElementCount()V

    return-object v2

    :cond_0
    new-instance v0, Landroid/renderscript/RSInvalidStateException;

    const-string v1, "Dimension must be >= 1."

    invoke-direct {v0, v1}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static createXY(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;II)Landroid/renderscript/Type;
    .locals 10

    const/4 v0, 0x1

    if-lt p2, v0, :cond_0

    if-lt p3, v0, :cond_0

    invoke-virtual {p1, p0}, Landroid/renderscript/Element;->getID(Landroid/renderscript/RenderScript;)J

    move-result-wide v2

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, p0

    move v4, p2

    move v5, p3

    invoke-virtual/range {v1 .. v9}, Landroid/renderscript/RenderScript;->nTypeCreate(JIIIZZI)J

    move-result-wide v0

    new-instance v2, Landroid/renderscript/Type;

    invoke-direct {v2, v0, v1, p0}, Landroid/renderscript/Type;-><init>(JLandroid/renderscript/RenderScript;)V

    iput-object p1, v2, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    iput p2, v2, Landroid/renderscript/Type;->mDimX:I

    iput p3, v2, Landroid/renderscript/Type;->mDimY:I

    invoke-virtual {v2}, Landroid/renderscript/Type;->calcElementCount()V

    return-object v2

    :cond_0
    new-instance v0, Landroid/renderscript/RSInvalidStateException;

    const-string v1, "Dimension must be >= 1."

    invoke-direct {v0, v1}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static createXYZ(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;III)Landroid/renderscript/Type;
    .locals 10

    const/4 v0, 0x1

    if-lt p2, v0, :cond_0

    if-lt p3, v0, :cond_0

    if-lt p4, v0, :cond_0

    invoke-virtual {p1, p0}, Landroid/renderscript/Element;->getID(Landroid/renderscript/RenderScript;)J

    move-result-wide v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, p0

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-virtual/range {v1 .. v9}, Landroid/renderscript/RenderScript;->nTypeCreate(JIIIZZI)J

    move-result-wide v0

    new-instance v2, Landroid/renderscript/Type;

    invoke-direct {v2, v0, v1, p0}, Landroid/renderscript/Type;-><init>(JLandroid/renderscript/RenderScript;)V

    iput-object p1, v2, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    iput p2, v2, Landroid/renderscript/Type;->mDimX:I

    iput p3, v2, Landroid/renderscript/Type;->mDimY:I

    iput p4, v2, Landroid/renderscript/Type;->mDimZ:I

    invoke-virtual {v2}, Landroid/renderscript/Type;->calcElementCount()V

    return-object v2

    :cond_0
    new-instance v0, Landroid/renderscript/RSInvalidStateException;

    const-string v1, "Dimension must be >= 1."

    invoke-direct {v0, v1}, Landroid/renderscript/RSInvalidStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method calcElementCount()V
    .locals 9

    goto/32 :goto_10

    nop

    :goto_0
    if-eqz v2, :cond_0

    goto/32 :goto_2e

    :cond_0
    goto/32 :goto_2d

    nop

    :goto_1
    goto :goto_e

    :goto_2
    goto/32 :goto_25

    nop

    :goto_3
    if-gt v3, v6, :cond_1

    goto/32 :goto_22

    :cond_1
    goto/32 :goto_21

    nop

    :goto_4
    mul-int/2addr v6, v4

    goto/32 :goto_32

    nop

    :goto_5
    mul-int/2addr v6, v3

    goto/32 :goto_4

    nop

    :goto_6
    if-eqz v1, :cond_2

    goto/32 :goto_a

    :cond_2
    goto/32 :goto_9

    nop

    :goto_7
    if-gt v1, v6, :cond_3

    goto/32 :goto_c

    :cond_3
    goto/32 :goto_b

    nop

    :goto_8
    if-eqz v3, :cond_4

    goto/32 :goto_30

    :cond_4
    goto/32 :goto_2f

    nop

    :goto_9
    const/4 v1, 0x1

    :goto_a
    goto/32 :goto_0

    nop

    :goto_b
    shr-int/lit8 v1, v1, 0x1

    :goto_c
    goto/32 :goto_19

    nop

    :goto_d
    mul-int/2addr v5, v4

    :goto_e
    goto/32 :goto_24

    nop

    :goto_f
    if-le v2, v6, :cond_5

    goto/32 :goto_1f

    :cond_5
    goto/32 :goto_1e

    nop

    :goto_10
    invoke-virtual {p0}, Landroid/renderscript/Type;->hasMipmaps()Z

    move-result v0

    goto/32 :goto_20

    nop

    :goto_11
    aget v7, v7, v6

    goto/32 :goto_29

    nop

    :goto_12
    const/4 v4, 0x6

    :goto_13
    goto/32 :goto_6

    nop

    :goto_14
    shr-int/lit8 v2, v2, 0x1

    :goto_15
    goto/32 :goto_3

    nop

    :goto_16
    add-int/lit8 v6, v6, 0x1

    goto/32 :goto_26

    nop

    :goto_17
    invoke-virtual {p0}, Landroid/renderscript/Type;->getZ()I

    move-result v3

    goto/32 :goto_1c

    nop

    :goto_18
    iput v5, p0, Landroid/renderscript/Type;->mElementCount:I

    goto/32 :goto_38

    nop

    :goto_19
    if-gt v2, v6, :cond_6

    goto/32 :goto_15

    :cond_6
    goto/32 :goto_14

    nop

    :goto_1a
    if-lt v6, v8, :cond_7

    goto/32 :goto_27

    :cond_7
    goto/32 :goto_11

    nop

    :goto_1b
    iget-object v7, p0, Landroid/renderscript/Type;->mArrays:[I

    goto/32 :goto_1d

    nop

    :goto_1c
    const/4 v4, 0x1

    goto/32 :goto_2c

    nop

    :goto_1d
    array-length v8, v7

    goto/32 :goto_1a

    nop

    :goto_1e
    if-gt v3, v6, :cond_8

    goto/32 :goto_2

    :cond_8
    :goto_1f
    goto/32 :goto_7

    nop

    :goto_20
    invoke-virtual {p0}, Landroid/renderscript/Type;->getX()I

    move-result v1

    goto/32 :goto_34

    nop

    :goto_21
    shr-int/lit8 v3, v3, 0x1

    :goto_22
    goto/32 :goto_23

    nop

    :goto_23
    mul-int v6, v1, v2

    goto/32 :goto_5

    nop

    :goto_24
    if-nez v0, :cond_9

    goto/32 :goto_2

    :cond_9
    goto/32 :goto_28

    nop

    :goto_25
    iget-object v6, p0, Landroid/renderscript/Type;->mArrays:[I

    goto/32 :goto_2a

    nop

    :goto_26
    goto :goto_36

    :goto_27
    goto/32 :goto_18

    nop

    :goto_28
    const/4 v6, 0x1

    goto/32 :goto_37

    nop

    :goto_29
    mul-int/2addr v5, v7

    goto/32 :goto_16

    nop

    :goto_2a
    if-nez v6, :cond_a

    goto/32 :goto_27

    :cond_a
    goto/32 :goto_35

    nop

    :goto_2b
    mul-int/2addr v5, v3

    goto/32 :goto_d

    nop

    :goto_2c
    invoke-virtual {p0}, Landroid/renderscript/Type;->hasFaces()Z

    move-result v5

    goto/32 :goto_33

    nop

    :goto_2d
    const/4 v2, 0x1

    :goto_2e
    goto/32 :goto_8

    nop

    :goto_2f
    const/4 v3, 0x1

    :goto_30
    goto/32 :goto_31

    nop

    :goto_31
    mul-int v5, v1, v2

    goto/32 :goto_2b

    nop

    :goto_32
    add-int/2addr v5, v6

    goto/32 :goto_1

    nop

    :goto_33
    if-nez v5, :cond_b

    goto/32 :goto_13

    :cond_b
    goto/32 :goto_12

    nop

    :goto_34
    invoke-virtual {p0}, Landroid/renderscript/Type;->getY()I

    move-result v2

    goto/32 :goto_17

    nop

    :goto_35
    const/4 v6, 0x0

    :goto_36
    goto/32 :goto_1b

    nop

    :goto_37
    if-le v1, v6, :cond_c

    goto/32 :goto_1f

    :cond_c
    goto/32 :goto_f

    nop

    :goto_38
    return-void
.end method

.method public getArray(I)I
    .locals 2

    if-ltz p1, :cond_2

    const/4 v0, 0x4

    if-ge p1, v0, :cond_2

    iget-object v0, p0, Landroid/renderscript/Type;->mArrays:[I

    if-eqz v0, :cond_1

    array-length v1, v0

    if-lt p1, v1, :cond_0

    goto :goto_0

    :cond_0
    aget v0, v0, p1

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0

    :cond_2
    new-instance v0, Landroid/renderscript/RSIllegalArgumentException;

    const-string v1, "Array dimension out of range."

    invoke-direct {v0, v1}, Landroid/renderscript/RSIllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getArrayCount()I
    .locals 1

    iget-object v0, p0, Landroid/renderscript/Type;->mArrays:[I

    if-eqz v0, :cond_0

    array-length v0, v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 1

    iget v0, p0, Landroid/renderscript/Type;->mElementCount:I

    return v0
.end method

.method public getElement()Landroid/renderscript/Element;
    .locals 1

    iget-object v0, p0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    return-object v0
.end method

.method public getX()I
    .locals 1

    iget v0, p0, Landroid/renderscript/Type;->mDimX:I

    return v0
.end method

.method public getY()I
    .locals 1

    iget v0, p0, Landroid/renderscript/Type;->mDimY:I

    return v0
.end method

.method public getYuv()I
    .locals 1

    iget v0, p0, Landroid/renderscript/Type;->mDimYuv:I

    return v0
.end method

.method public getZ()I
    .locals 1

    iget v0, p0, Landroid/renderscript/Type;->mDimZ:I

    return v0
.end method

.method public hasFaces()Z
    .locals 1

    iget-boolean v0, p0, Landroid/renderscript/Type;->mDimFaces:Z

    return v0
.end method

.method public hasMipmaps()Z
    .locals 1

    iget-boolean v0, p0, Landroid/renderscript/Type;->mDimMipmaps:Z

    return v0
.end method

.method updateFromNative()V
    .locals 7

    goto/32 :goto_18

    nop

    :goto_0
    invoke-virtual {v3}, Landroid/renderscript/Element;->updateFromNative()V

    :goto_1
    goto/32 :goto_21

    nop

    :goto_2
    move v3, v2

    goto/32 :goto_1a

    nop

    :goto_3
    if-eqz v3, :cond_0

    goto/32 :goto_17

    :cond_0
    goto/32 :goto_16

    nop

    :goto_4
    const-wide/16 v5, 0x1

    goto/32 :goto_d

    nop

    :goto_5
    aget-wide v3, v0, v2

    goto/32 :goto_2e

    nop

    :goto_6
    aget-wide v3, v0, v3

    goto/32 :goto_a

    nop

    :goto_7
    iget-object v1, p0, Landroid/renderscript/Type;->mRS:Landroid/renderscript/RenderScript;

    goto/32 :goto_10

    nop

    :goto_8
    const/4 v1, 0x0

    goto/32 :goto_29

    nop

    :goto_9
    iput v3, p0, Landroid/renderscript/Type;->mDimY:I

    goto/32 :goto_14

    nop

    :goto_a
    cmp-long v3, v3, v5

    goto/32 :goto_3

    nop

    :goto_b
    invoke-direct {v3, v1, v2, v4}, Landroid/renderscript/Element;-><init>(JLandroid/renderscript/RenderScript;)V

    goto/32 :goto_2c

    nop

    :goto_c
    const/4 v2, 0x1

    goto/32 :goto_5

    nop

    :goto_d
    cmp-long v3, v3, v5

    goto/32 :goto_28

    nop

    :goto_e
    aget-wide v3, v0, v3

    goto/32 :goto_4

    nop

    :goto_f
    iput-boolean v1, p0, Landroid/renderscript/Type;->mDimFaces:Z

    goto/32 :goto_20

    nop

    :goto_10
    iget-object v2, p0, Landroid/renderscript/Type;->mRS:Landroid/renderscript/RenderScript;

    goto/32 :goto_12

    nop

    :goto_11
    iput v2, p0, Landroid/renderscript/Type;->mDimX:I

    goto/32 :goto_c

    nop

    :goto_12
    invoke-virtual {p0, v2}, Landroid/renderscript/Type;->getID(Landroid/renderscript/RenderScript;)J

    move-result-wide v2

    goto/32 :goto_2b

    nop

    :goto_13
    long-to-int v3, v3

    goto/32 :goto_1c

    nop

    :goto_14
    const/4 v3, 0x2

    goto/32 :goto_2a

    nop

    :goto_15
    const/4 v3, 0x3

    goto/32 :goto_e

    nop

    :goto_16
    move v1, v2

    :goto_17
    goto/32 :goto_f

    nop

    :goto_18
    const/4 v0, 0x6

    goto/32 :goto_1f

    nop

    :goto_19
    if-nez v3, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_1e

    nop

    :goto_1a
    goto :goto_30

    :goto_1b
    goto/32 :goto_2f

    nop

    :goto_1c
    iput v3, p0, Landroid/renderscript/Type;->mDimZ:I

    goto/32 :goto_15

    nop

    :goto_1d
    iput-boolean v3, p0, Landroid/renderscript/Type;->mDimMipmaps:Z

    goto/32 :goto_23

    nop

    :goto_1e
    new-instance v3, Landroid/renderscript/Element;

    goto/32 :goto_27

    nop

    :goto_1f
    new-array v0, v0, [J

    goto/32 :goto_7

    nop

    :goto_20
    const/4 v1, 0x5

    goto/32 :goto_25

    nop

    :goto_21
    invoke-virtual {p0}, Landroid/renderscript/Type;->calcElementCount()V

    goto/32 :goto_24

    nop

    :goto_22
    long-to-int v2, v2

    goto/32 :goto_11

    nop

    :goto_23
    const/4 v3, 0x4

    goto/32 :goto_6

    nop

    :goto_24
    return-void

    :goto_25
    aget-wide v1, v0, v1

    goto/32 :goto_2d

    nop

    :goto_26
    cmp-long v3, v1, v3

    goto/32 :goto_19

    nop

    :goto_27
    iget-object v4, p0, Landroid/renderscript/Type;->mRS:Landroid/renderscript/RenderScript;

    goto/32 :goto_b

    nop

    :goto_28
    if-eqz v3, :cond_2

    goto/32 :goto_1b

    :cond_2
    goto/32 :goto_2

    nop

    :goto_29
    aget-wide v2, v0, v1

    goto/32 :goto_22

    nop

    :goto_2a
    aget-wide v3, v0, v3

    goto/32 :goto_13

    nop

    :goto_2b
    invoke-virtual {v1, v2, v3, v0}, Landroid/renderscript/RenderScript;->nTypeGetNativeData(J[J)V

    goto/32 :goto_8

    nop

    :goto_2c
    iput-object v3, p0, Landroid/renderscript/Type;->mElement:Landroid/renderscript/Element;

    goto/32 :goto_0

    nop

    :goto_2d
    const-wide/16 v3, 0x0

    goto/32 :goto_26

    nop

    :goto_2e
    long-to-int v3, v3

    goto/32 :goto_9

    nop

    :goto_2f
    move v3, v1

    :goto_30
    goto/32 :goto_1d

    nop
.end method
