.class public final Landroid/renderscript/ScriptGroup$Input;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/renderscript/ScriptGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Input"
.end annotation


# instance fields
.field mArgIndex:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/util/Pair<",
            "Landroid/renderscript/ScriptGroup$Closure;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field mFieldID:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/util/Pair<",
            "Landroid/renderscript/ScriptGroup$Closure;",
            "Landroid/renderscript/Script$FieldID;",
            ">;>;"
        }
    .end annotation
.end field

.field mValue:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/renderscript/ScriptGroup$Input;->mFieldID:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/renderscript/ScriptGroup$Input;->mArgIndex:Ljava/util/List;

    return-void
.end method


# virtual methods
.method addReference(Landroid/renderscript/ScriptGroup$Closure;I)V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_1

    nop

    :goto_1
    invoke-static {p1, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    goto/32 :goto_4

    nop

    :goto_2
    iget-object v0, p0, Landroid/renderscript/ScriptGroup$Input;->mArgIndex:Ljava/util/List;

    goto/32 :goto_0

    nop

    :goto_3
    return-void

    :goto_4
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_3

    nop
.end method

.method addReference(Landroid/renderscript/ScriptGroup$Closure;Landroid/renderscript/Script$FieldID;)V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    goto/32 :goto_3

    nop

    :goto_1
    iget-object v0, p0, Landroid/renderscript/ScriptGroup$Input;->mFieldID:Ljava/util/List;

    goto/32 :goto_0

    nop

    :goto_2
    return-void

    :goto_3
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_2

    nop
.end method

.method get()Ljava/lang/Object;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Landroid/renderscript/ScriptGroup$Input;->mValue:Ljava/lang/Object;

    goto/32 :goto_0

    nop
.end method

.method set(Ljava/lang/Object;)V
    .locals 4

    goto/32 :goto_1d

    nop

    :goto_0
    goto :goto_7

    :goto_1
    goto/32 :goto_13

    nop

    :goto_2
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    goto/32 :goto_1e

    nop

    :goto_3
    check-cast v1, Landroid/util/Pair;

    goto/32 :goto_2

    nop

    :goto_4
    iget-object v3, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    goto/32 :goto_15

    nop

    :goto_5
    if-nez v1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_1b

    nop

    :goto_6
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_7
    goto/32 :goto_1c

    nop

    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_10

    nop

    :goto_9
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_1a

    nop

    :goto_a
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    goto/32 :goto_c

    nop

    :goto_b
    check-cast v3, Landroid/renderscript/Script$FieldID;

    goto/32 :goto_16

    nop

    :goto_c
    check-cast v2, Landroid/renderscript/ScriptGroup$Closure;

    goto/32 :goto_d

    nop

    :goto_d
    iget-object v3, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    goto/32 :goto_b

    nop

    :goto_e
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_f
    goto/32 :goto_9

    nop

    :goto_10
    check-cast v1, Landroid/util/Pair;

    goto/32 :goto_a

    nop

    :goto_11
    goto :goto_f

    :goto_12
    goto/32 :goto_18

    nop

    :goto_13
    iget-object v0, p0, Landroid/renderscript/ScriptGroup$Input;->mFieldID:Ljava/util/List;

    goto/32 :goto_e

    nop

    :goto_14
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    goto/32 :goto_17

    nop

    :goto_15
    check-cast v3, Ljava/lang/Integer;

    goto/32 :goto_14

    nop

    :goto_16
    invoke-virtual {v2, v3, p1}, Landroid/renderscript/ScriptGroup$Closure;->setGlobal(Landroid/renderscript/Script$FieldID;Ljava/lang/Object;)V

    goto/32 :goto_11

    nop

    :goto_17
    invoke-virtual {v2, v3, p1}, Landroid/renderscript/ScriptGroup$Closure;->setArg(ILjava/lang/Object;)V

    goto/32 :goto_0

    nop

    :goto_18
    return-void

    :goto_19
    iget-object v0, p0, Landroid/renderscript/ScriptGroup$Input;->mArgIndex:Ljava/util/List;

    goto/32 :goto_6

    nop

    :goto_1a
    if-nez v1, :cond_1

    goto/32 :goto_12

    :cond_1
    goto/32 :goto_8

    nop

    :goto_1b
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_3

    nop

    :goto_1c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_5

    nop

    :goto_1d
    iput-object p1, p0, Landroid/renderscript/ScriptGroup$Input;->mValue:Ljava/lang/Object;

    goto/32 :goto_19

    nop

    :goto_1e
    check-cast v2, Landroid/renderscript/ScriptGroup$Closure;

    goto/32 :goto_4

    nop
.end method
