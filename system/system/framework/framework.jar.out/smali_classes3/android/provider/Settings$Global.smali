.class public final Landroid/provider/Settings$Global;
.super Landroid/provider/Settings$NameValueTable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/provider/Settings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Global"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/provider/Settings$Global$Wearable;
    }
.end annotation


# static fields
.field public static final ACTIVITY_MANAGER_CONSTANTS:Ljava/lang/String; = "activity_manager_constants"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ACTIVITY_STARTS_LOGGING_ENABLED:Ljava/lang/String; = "activity_starts_logging_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ADAPTIVE_BATTERY_MANAGEMENT_ENABLED:Ljava/lang/String; = "adaptive_battery_management_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ADB_ALLOWED_CONNECTION_TIME:Ljava/lang/String; = "adb_allowed_connection_time"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ADB_ENABLED:Ljava/lang/String; = "adb_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ADB_WIFI_ENABLED:Ljava/lang/String; = "adb_wifi_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ADD_USERS_WHEN_LOCKED:Ljava/lang/String; = "add_users_when_locked"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ADVANCED_BATTERY_USAGE_AMOUNT:Ljava/lang/String; = "advanced_battery_usage_amount"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final AIRPLANE_MODE_ON:Ljava/lang/String; = "airplane_mode_on"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final AIRPLANE_MODE_RADIOS:Ljava/lang/String; = "airplane_mode_radios"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final AIRPLANE_MODE_TOGGLEABLE_RADIOS:Ljava/lang/String; = "airplane_mode_toggleable_radios"
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ALLOW_USER_SWITCHING_WHEN_SYSTEM_USER_LOCKED:Ljava/lang/String; = "allow_user_switching_when_system_user_locked"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ALWAYS_FINISH_ACTIVITIES:Ljava/lang/String; = "always_finish_activities"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ALWAYS_ON_DISPLAY_CONSTANTS:Ljava/lang/String; = "always_on_display_constants"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ANGLE_DEBUG_PACKAGE:Ljava/lang/String; = "angle_debug_package"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ANGLE_EGL_FEATURES:Ljava/lang/String; = "angle_egl_features"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ANGLE_GL_DRIVER_ALL_ANGLE:Ljava/lang/String; = "angle_gl_driver_all_angle"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ANGLE_GL_DRIVER_SELECTION_PKGS:Ljava/lang/String; = "angle_gl_driver_selection_pkgs"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ANGLE_GL_DRIVER_SELECTION_VALUES:Ljava/lang/String; = "angle_gl_driver_selection_values"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ANIMATOR_DURATION_SCALE:Ljava/lang/String; = "animator_duration_scale"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ANOMALY_CONFIG:Ljava/lang/String; = "anomaly_config"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ANOMALY_CONFIG_VERSION:Ljava/lang/String; = "anomaly_config_version"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ANOMALY_DETECTION_CONSTANTS:Ljava/lang/String; = "anomaly_detection_constants"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final APN_DB_UPDATE_CONTENT_URL:Ljava/lang/String; = "apn_db_content_url"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final APN_DB_UPDATE_METADATA_URL:Ljava/lang/String; = "apn_db_metadata_url"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final APPLY_RAMPING_RINGER:Ljava/lang/String; = "apply_ramping_ringer"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final APPOP_HISTORY_BASE_INTERVAL_MILLIS:Ljava/lang/String; = "baseIntervalMillis"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final APPOP_HISTORY_INTERVAL_MULTIPLIER:Ljava/lang/String; = "intervalMultiplier"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final APPOP_HISTORY_MODE:Ljava/lang/String; = "mode"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final APPOP_HISTORY_PARAMETERS:Ljava/lang/String; = "appop_history_parameters"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final APP_AUTO_RESTRICTION_ENABLED:Ljava/lang/String; = "app_auto_restriction_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final APP_BINDING_CONSTANTS:Ljava/lang/String; = "app_binding_constants"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final APP_INTEGRITY_VERIFICATION_TIMEOUT:Ljava/lang/String; = "app_integrity_verification_timeout"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final APP_OPS_CONSTANTS:Ljava/lang/String; = "app_ops_constants"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final APP_STANDBY_ENABLED:Ljava/lang/String; = "app_standby_enabled"
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final APP_TIME_LIMIT_USAGE_SOURCE:Ljava/lang/String; = "app_time_limit_usage_source"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ARE_USER_DISABLED_HDR_FORMATS_ALLOWED:Ljava/lang/String; = "are_user_disabled_hdr_formats_allowed"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ART_VERIFIER_VERIFY_DEBUGGABLE:Ljava/lang/String; = "art_verifier_verify_debuggable"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ASSISTED_GPS_ENABLED:Ljava/lang/String; = "assisted_gps_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final AUDIO_SAFE_VOLUME_STATE:Ljava/lang/String; = "audio_safe_volume_state"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final AUTOFILL_COMPAT_MODE_ALLOWED_PACKAGES:Ljava/lang/String; = "autofill_compat_mode_allowed_packages"
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final AUTOFILL_LOGGING_LEVEL:Ljava/lang/String; = "autofill_logging_level"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final AUTOFILL_MAX_PARTITIONS_SIZE:Ljava/lang/String; = "autofill_max_partitions_size"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final AUTOFILL_MAX_VISIBLE_DATASETS:Ljava/lang/String; = "autofill_max_visible_datasets"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final AUTOMATIC_POWER_SAVE_MODE:Ljava/lang/String; = "automatic_power_save_mode"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final AUTO_REVOKE_PARAMETERS:Ljava/lang/String; = "auto_revoke_parameters"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final AUTO_TIME:Ljava/lang/String; = "auto_time"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final AUTO_TIME_ZONE:Ljava/lang/String; = "auto_time_zone"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final AVERAGE_TIME_TO_DISCHARGE:Ljava/lang/String; = "average_time_to_discharge"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final AWARE_ALLOWED:Ljava/lang/String; = "aware_allowed"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BACKUP_AGENT_TIMEOUT_PARAMETERS:Ljava/lang/String; = "backup_agent_timeout_parameters"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BATTERY_CHARGING_STATE_UPDATE_DELAY:Ljava/lang/String; = "battery_charging_state_update_delay"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BATTERY_DISCHARGE_DURATION_THRESHOLD:Ljava/lang/String; = "battery_discharge_duration_threshold"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BATTERY_DISCHARGE_THRESHOLD:Ljava/lang/String; = "battery_discharge_threshold"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BATTERY_ESTIMATES_LAST_UPDATE_TIME:Ljava/lang/String; = "battery_estimates_last_update_time"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final BATTERY_SAVER_CONSTANTS:Ljava/lang/String; = "battery_saver_constants"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BATTERY_SAVER_DEVICE_SPECIFIC_CONSTANTS:Ljava/lang/String; = "battery_saver_device_specific_constants"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BATTERY_STATS_CONSTANTS:Ljava/lang/String; = "battery_stats_constants"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BATTERY_TIP_CONSTANTS:Ljava/lang/String; = "battery_tip_constants"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BINDER_CALLS_STATS:Ljava/lang/String; = "binder_calls_stats"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLE_SCAN_ALWAYS_AVAILABLE:Ljava/lang/String; = "ble_scan_always_enabled"
    .annotation runtime Landroid/annotation/SystemApi;
        client = .enum Landroid/annotation/SystemApi$Client;->MODULE_LIBRARIES:Landroid/annotation/SystemApi$Client;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLE_SCAN_BACKGROUND_MODE:Ljava/lang/String; = "ble_scan_background_mode"
    .annotation runtime Landroid/annotation/SystemApi;
        client = .enum Landroid/annotation/SystemApi$Client;->MODULE_LIBRARIES:Landroid/annotation/SystemApi$Client;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLE_SCAN_BALANCED_INTERVAL_MS:Ljava/lang/String; = "ble_scan_balanced_interval_ms"
    .annotation runtime Landroid/annotation/SystemApi;
        client = .enum Landroid/annotation/SystemApi$Client;->MODULE_LIBRARIES:Landroid/annotation/SystemApi$Client;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLE_SCAN_BALANCED_WINDOW_MS:Ljava/lang/String; = "ble_scan_balanced_window_ms"
    .annotation runtime Landroid/annotation/SystemApi;
        client = .enum Landroid/annotation/SystemApi$Client;->MODULE_LIBRARIES:Landroid/annotation/SystemApi$Client;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLE_SCAN_LOW_LATENCY_INTERVAL_MS:Ljava/lang/String; = "ble_scan_low_latency_interval_ms"
    .annotation runtime Landroid/annotation/SystemApi;
        client = .enum Landroid/annotation/SystemApi$Client;->MODULE_LIBRARIES:Landroid/annotation/SystemApi$Client;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLE_SCAN_LOW_LATENCY_WINDOW_MS:Ljava/lang/String; = "ble_scan_low_latency_window_ms"
    .annotation runtime Landroid/annotation/SystemApi;
        client = .enum Landroid/annotation/SystemApi$Client;->MODULE_LIBRARIES:Landroid/annotation/SystemApi$Client;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLE_SCAN_LOW_POWER_INTERVAL_MS:Ljava/lang/String; = "ble_scan_low_power_interval_ms"
    .annotation runtime Landroid/annotation/SystemApi;
        client = .enum Landroid/annotation/SystemApi$Client;->MODULE_LIBRARIES:Landroid/annotation/SystemApi$Client;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLE_SCAN_LOW_POWER_WINDOW_MS:Ljava/lang/String; = "ble_scan_low_power_window_ms"
    .annotation runtime Landroid/annotation/SystemApi;
        client = .enum Landroid/annotation/SystemApi$Client;->MODULE_LIBRARIES:Landroid/annotation/SystemApi$Client;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLOCKED_SLICES:Ljava/lang/String; = "blocked_slices"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLOCKING_HELPER_DISMISS_TO_VIEW_RATIO_LIMIT:Ljava/lang/String; = "blocking_helper_dismiss_to_view_ratio"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLOCKING_HELPER_STREAK_LIMIT:Ljava/lang/String; = "blocking_helper_streak_limit"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLOCK_UNTRUSTED_TOUCHES_MODE:Ljava/lang/String; = "block_untrusted_touches"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLUETOOTH_A2DP_OPTIONAL_CODECS_ENABLED_PREFIX:Ljava/lang/String; = "bluetooth_a2dp_optional_codecs_enabled_"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLUETOOTH_A2DP_SINK_PRIORITY_PREFIX:Ljava/lang/String; = "bluetooth_a2dp_sink_priority_"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLUETOOTH_A2DP_SRC_PRIORITY_PREFIX:Ljava/lang/String; = "bluetooth_a2dp_src_priority_"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLUETOOTH_A2DP_SUPPORTS_OPTIONAL_CODECS_PREFIX:Ljava/lang/String; = "bluetooth_a2dp_supports_optional_codecs_"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLUETOOTH_BTSNOOP_DEFAULT_MODE:Ljava/lang/String; = "bluetooth_btsnoop_default_mode"
    .annotation runtime Landroid/annotation/SystemApi;
        client = .enum Landroid/annotation/SystemApi$Client;->MODULE_LIBRARIES:Landroid/annotation/SystemApi$Client;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLUETOOTH_CLASS_OF_DEVICE:Ljava/lang/String; = "bluetooth_class_of_device"
    .annotation runtime Landroid/annotation/SystemApi;
        client = .enum Landroid/annotation/SystemApi$Client;->MODULE_LIBRARIES:Landroid/annotation/SystemApi$Client;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLUETOOTH_DISABLED_PROFILES:Ljava/lang/String; = "bluetooth_disabled_profiles"
    .annotation runtime Landroid/annotation/SystemApi;
        client = .enum Landroid/annotation/SystemApi$Client;->MODULE_LIBRARIES:Landroid/annotation/SystemApi$Client;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLUETOOTH_HEADSET_PRIORITY_PREFIX:Ljava/lang/String; = "bluetooth_headset_priority_"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLUETOOTH_HEARING_AID_PRIORITY_PREFIX:Ljava/lang/String; = "bluetooth_hearing_aid_priority_"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLUETOOTH_INPUT_DEVICE_PRIORITY_PREFIX:Ljava/lang/String; = "bluetooth_input_device_priority_"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLUETOOTH_INTEROPERABILITY_LIST:Ljava/lang/String; = "bluetooth_interoperability_list"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLUETOOTH_MAP_CLIENT_PRIORITY_PREFIX:Ljava/lang/String; = "bluetooth_map_client_priority_"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLUETOOTH_MAP_PRIORITY_PREFIX:Ljava/lang/String; = "bluetooth_map_priority_"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLUETOOTH_ON:Ljava/lang/String; = "bluetooth_on"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLUETOOTH_PAN_PRIORITY_PREFIX:Ljava/lang/String; = "bluetooth_pan_priority_"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLUETOOTH_PBAP_CLIENT_PRIORITY_PREFIX:Ljava/lang/String; = "bluetooth_pbap_client_priority_"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BLUETOOTH_SAP_PRIORITY_PREFIX:Ljava/lang/String; = "bluetooth_sap_priority_"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BOOT_COUNT:Ljava/lang/String; = "boot_count"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BROADCAST_BG_CONSTANTS:Ljava/lang/String; = "bcast_bg_constants"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BROADCAST_FG_CONSTANTS:Ljava/lang/String; = "bcast_fg_constants"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BROADCAST_OFFLOAD_CONSTANTS:Ljava/lang/String; = "bcast_offload_constants"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BUGREPORT_IN_POWER_MENU:Ljava/lang/String; = "bugreport_in_power_menu"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final BYPASS_DEVICE_POLICY_MANAGEMENT_ROLE_QUALIFICATIONS:Ljava/lang/String; = "bypass_device_policy_management_role_qualifications"

.field public static final CACHED_APPS_FREEZER_ENABLED:Ljava/lang/String; = "cached_apps_freezer"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CALL_AUTO_RETRY:Ljava/lang/String; = "call_auto_retry"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CAPTIVE_PORTAL_DETECTION_ENABLED:Ljava/lang/String; = "captive_portal_detection_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CAPTIVE_PORTAL_FALLBACK_PROBE_SPECS:Ljava/lang/String; = "captive_portal_fallback_probe_specs"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CAPTIVE_PORTAL_FALLBACK_URL:Ljava/lang/String; = "captive_portal_fallback_url"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CAPTIVE_PORTAL_HTTPS_URL:Ljava/lang/String; = "captive_portal_https_url"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CAPTIVE_PORTAL_HTTP_URL:Ljava/lang/String; = "captive_portal_http_url"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CAPTIVE_PORTAL_MODE:Ljava/lang/String; = "captive_portal_mode"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CAPTIVE_PORTAL_MODE_AVOID:I = 0x2

.field public static final CAPTIVE_PORTAL_MODE_IGNORE:I = 0x0

.field public static final CAPTIVE_PORTAL_MODE_PROMPT:I = 0x1

.field public static final CAPTIVE_PORTAL_OTHER_FALLBACK_URLS:Ljava/lang/String; = "captive_portal_other_fallback_urls"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CAPTIVE_PORTAL_SERVER:Ljava/lang/String; = "captive_portal_server"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CAPTIVE_PORTAL_USER_AGENT:Ljava/lang/String; = "captive_portal_user_agent"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CAPTIVE_PORTAL_USE_HTTPS:Ljava/lang/String; = "captive_portal_use_https"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CARRIER_APP_NAMES:Ljava/lang/String; = "carrier_app_names"
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CARRIER_APP_WHITELIST:Ljava/lang/String; = "carrier_app_whitelist"
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CAR_DOCK_SOUND:Ljava/lang/String; = "car_dock_sound"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CAR_UNDOCK_SOUND:Ljava/lang/String; = "car_undock_sound"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CDMA_CELL_BROADCAST_SMS:Ljava/lang/String; = "cdma_cell_broadcast_sms"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CDMA_ROAMING_MODE:Ljava/lang/String; = "roaming_settings"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CDMA_SUBSCRIPTION_MODE:Ljava/lang/String; = "subscription_mode"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CELL_ON:Ljava/lang/String; = "cell_on"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CERT_PIN_UPDATE_CONTENT_URL:Ljava/lang/String; = "cert_pin_content_url"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CERT_PIN_UPDATE_METADATA_URL:Ljava/lang/String; = "cert_pin_metadata_url"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CHAINED_BATTERY_ATTRIBUTION_ENABLED:Ljava/lang/String; = "chained_battery_attribution_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CHARGING_SOUNDS_ENABLED:Ljava/lang/String; = "charging_sounds_enabled"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CHARGING_STARTED_SOUND:Ljava/lang/String; = "charging_started_sound"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CHARGING_VIBRATION_ENABLED:Ljava/lang/String; = "charging_vibration_enabled"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CLOCKWORK_HOME_READY:Ljava/lang/String; = "clockwork_home_ready"

.field public static final COMPATIBILITY_MODE:Ljava/lang/String; = "compatibility_mode"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CONNECTIVITY_CHANGE_DELAY:Ljava/lang/String; = "connectivity_change_delay"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CONNECTIVITY_METRICS_BUFFER_SIZE:Ljava/lang/String; = "connectivity_metrics_buffer_size"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CONNECTIVITY_SAMPLING_INTERVAL_IN_SECONDS:Ljava/lang/String; = "connectivity_sampling_interval_in_seconds"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CONTACTS_DATABASE_WAL_ENABLED:Ljava/lang/String; = "contacts_database_wal_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CONTACT_METADATA_SYNC:Ljava/lang/String; = "contact_metadata_sync"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CONTACT_METADATA_SYNC_ENABLED:Ljava/lang/String; = "contact_metadata_sync_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final CONVERSATION_ACTIONS_UPDATE_CONTENT_URL:Ljava/lang/String; = "conversation_actions_content_url"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CONVERSATION_ACTIONS_UPDATE_METADATA_URL:Ljava/lang/String; = "conversation_actions_metadata_url"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CUSTOM_BUGREPORT_HANDLER_APP:Ljava/lang/String; = "custom_bugreport_handler_app"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final CUSTOM_BUGREPORT_HANDLER_USER:Ljava/lang/String; = "custom_bugreport_handler_user"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DATABASE_CREATION_BUILDID:Ljava/lang/String; = "database_creation_buildid"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DATABASE_DOWNGRADE_REASON:Ljava/lang/String; = "database_downgrade_reason"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DATA_ACTIVITY_TIMEOUT_MOBILE:Ljava/lang/String; = "data_activity_timeout_mobile"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DATA_ACTIVITY_TIMEOUT_WIFI:Ljava/lang/String; = "data_activity_timeout_wifi"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DATA_ROAMING:Ljava/lang/String; = "data_roaming"
    .annotation runtime Landroid/provider/Settings$Readable;
        maxTargetSdk = 0x20
    .end annotation
.end field

.field public static final DATA_STALL_ALARM_AGGRESSIVE_DELAY_IN_MS:Ljava/lang/String; = "data_stall_alarm_aggressive_delay_in_ms"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DATA_STALL_ALARM_NON_AGGRESSIVE_DELAY_IN_MS:Ljava/lang/String; = "data_stall_alarm_non_aggressive_delay_in_ms"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DATA_STALL_RECOVERY_ON_BAD_NETWORK:Ljava/lang/String; = "data_stall_recovery_on_bad_network"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DEBUG_APP:Ljava/lang/String; = "debug_app"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DEBUG_VIEW_ATTRIBUTES:Ljava/lang/String; = "debug_view_attributes"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DEBUG_VIEW_ATTRIBUTES_APPLICATION_PACKAGE:Ljava/lang/String; = "debug_view_attributes_application_package"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DEFAULT_ADB_ALLOWED_CONNECTION_TIME:J = 0x240c8400L

.field public static final DEFAULT_DNS_SERVER:Ljava/lang/String; = "default_dns_server"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DEFAULT_ENABLE_RESTRICTED_BUCKET:I = 0x1

.field public static final DEFAULT_ENABLE_TARE:I = 0x0

.field public static final DEFAULT_ENABLE_TARE_ALARM_MANAGER:I = 0x0

.field public static final DEFAULT_ENABLE_TARE_JOB_SCHEDULER:I = 0x0

.field public static final DEFAULT_INSTALL_LOCATION:Ljava/lang/String; = "default_install_location"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DEFAULT_RESTRICT_BACKGROUND_DATA:Ljava/lang/String; = "default_restrict_background_data"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DEFAULT_SM_DP_PLUS:Ljava/lang/String; = "default_sm_dp_plus"
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DESK_DOCK_SOUND:Ljava/lang/String; = "desk_dock_sound"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DESK_UNDOCK_SOUND:Ljava/lang/String; = "desk_undock_sound"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DEVELOPMENT_ENABLE_FREEFORM_WINDOWS_SUPPORT:Ljava/lang/String; = "enable_freeform_support"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DEVELOPMENT_ENABLE_NON_RESIZABLE_MULTI_WINDOW:Ljava/lang/String; = "enable_non_resizable_multi_window"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DEVELOPMENT_FORCE_DESKTOP_MODE_ON_EXTERNAL_DISPLAYS:Ljava/lang/String; = "force_desktop_mode_on_external_displays"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DEVELOPMENT_FORCE_RESIZABLE_ACTIVITIES:Ljava/lang/String; = "force_resizable_activities"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DEVELOPMENT_FORCE_RTL:Ljava/lang/String; = "debug.force_rtl"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DEVELOPMENT_RENDER_SHADOWS_IN_COMPOSITOR:Ljava/lang/String; = "render_shadows_in_compositor"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DEVELOPMENT_SETTINGS_ENABLED:Ljava/lang/String; = "development_settings_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DEVELOPMENT_USE_BLAST_ADAPTER_VR:Ljava/lang/String; = "use_blast_adapter_vr"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DEVELOPMENT_WM_DISPLAY_SETTINGS_PATH:Ljava/lang/String; = "wm_display_settings_path"

.field public static final DEVICE_CONFIG_SYNC_DISABLED:Ljava/lang/String; = "device_config_sync_disabled"

.field public static final DEVICE_DEMO_MODE:Ljava/lang/String; = "device_demo_mode"
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DEVICE_NAME:Ljava/lang/String; = "device_name"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DEVICE_POLICY_CONSTANTS:Ljava/lang/String; = "device_policy_constants"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DEVICE_PROVISIONED:Ljava/lang/String; = "device_provisioned"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DEVICE_PROVISIONING_MOBILE_DATA_ENABLED:Ljava/lang/String; = "device_provisioning_mobile_data"
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DISABLE_WINDOW_BLURS:Ljava/lang/String; = "disable_window_blurs"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DISK_FREE_CHANGE_REPORTING_THRESHOLD:Ljava/lang/String; = "disk_free_change_reporting_threshold"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DISPLAY_PANEL_LPM:Ljava/lang/String; = "display_panel_lpm"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DISPLAY_SCALING_FORCE:Ljava/lang/String; = "display_scaling_force"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DISPLAY_SIZE_FORCED:Ljava/lang/String; = "display_size_forced"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DNS_RESOLVER_MAX_SAMPLES:Ljava/lang/String; = "dns_resolver_max_samples"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DNS_RESOLVER_MIN_SAMPLES:Ljava/lang/String; = "dns_resolver_min_samples"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DNS_RESOLVER_SAMPLE_VALIDITY_SECONDS:Ljava/lang/String; = "dns_resolver_sample_validity_seconds"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DNS_RESOLVER_SUCCESS_THRESHOLD_PERCENT:Ljava/lang/String; = "dns_resolver_success_threshold_percent"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DOCK_AUDIO_MEDIA_ENABLED:Ljava/lang/String; = "dock_audio_media_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DOCK_SOUNDS_ENABLED:Ljava/lang/String; = "dock_sounds_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DOCK_SOUNDS_ENABLED_WHEN_ACCESSIBILITY:Ljava/lang/String; = "dock_sounds_enabled_when_accessbility"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DOWNLOAD_MAX_BYTES_OVER_MOBILE:Ljava/lang/String; = "download_manager_max_bytes_over_mobile"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DOWNLOAD_RECOMMENDED_MAX_BYTES_OVER_MOBILE:Ljava/lang/String; = "download_manager_recommended_max_bytes_over_mobile"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DROPBOX_AGE_SECONDS:Ljava/lang/String; = "dropbox_age_seconds"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DROPBOX_MAX_FILES:Ljava/lang/String; = "dropbox_max_files"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DROPBOX_QUOTA_KB:Ljava/lang/String; = "dropbox_quota_kb"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DROPBOX_QUOTA_PERCENT:Ljava/lang/String; = "dropbox_quota_percent"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DROPBOX_RESERVE_PERCENT:Ljava/lang/String; = "dropbox_reserve_percent"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DROPBOX_TAG_PREFIX:Ljava/lang/String; = "dropbox:"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DYNAMIC_POWER_SAVINGS_DISABLE_THRESHOLD:Ljava/lang/String; = "dynamic_power_savings_disable_threshold"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final DYNAMIC_POWER_SAVINGS_ENABLED:Ljava/lang/String; = "dynamic_power_savings_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final EMERGENCY_AFFORDANCE_NEEDED:Ljava/lang/String; = "emergency_affordance_needed"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final EMERGENCY_GESTURE_POWER_BUTTON_COOLDOWN_PERIOD_MS:Ljava/lang/String; = "emergency_gesture_power_button_cooldown_period_ms"

.field public static final EMERGENCY_GESTURE_TAP_DETECTION_MIN_TIME_MS:Ljava/lang/String; = "emergency_gesture_tap_detection_min_time_ms"

.field public static final EMERGENCY_TONE:Ljava/lang/String; = "emergency_tone"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final EMULATE_DISPLAY_CUTOUT:Ljava/lang/String; = "emulate_display_cutout"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final EMULATE_DISPLAY_CUTOUT_OFF:I = 0x0

.field public static final EMULATE_DISPLAY_CUTOUT_ON:I = 0x1

.field public static final ENABLED_SUBSCRIPTION_FOR_SLOT:Ljava/lang/String; = "enabled_subscription_for_slot"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ENABLE_ACCESSIBILITY_GLOBAL_GESTURE_ENABLED:Ljava/lang/String; = "enable_accessibility_global_gesture_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ENABLE_ADB_INCREMENTAL_INSTALL_DEFAULT:Ljava/lang/String; = "enable_adb_incremental_install_default"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ENABLE_AUTOMATIC_SYSTEM_SERVER_HEAP_DUMPS:Ljava/lang/String; = "enable_automatic_system_server_heap_dumps"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ENABLE_BACK_ANIMATION:Ljava/lang/String; = "enable_back_animation"

.field public static final ENABLE_CACHE_QUOTA_CALCULATION:Ljava/lang/String; = "enable_cache_quota_calculation"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ENABLE_CELLULAR_ON_BOOT:Ljava/lang/String; = "enable_cellular_on_boot"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ENABLE_DELETION_HELPER_NO_THRESHOLD_TOGGLE:Ljava/lang/String; = "enable_deletion_helper_no_threshold_toggle"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ENABLE_DISKSTATS_LOGGING:Ljava/lang/String; = "enable_diskstats_logging"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ENABLE_EPHEMERAL_FEATURE:Ljava/lang/String; = "enable_ephemeral_feature"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ENABLE_GNSS_RAW_MEAS_FULL_TRACKING:Ljava/lang/String; = "enable_gnss_raw_meas_full_tracking"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ENABLE_GPU_DEBUG_LAYERS:Ljava/lang/String; = "enable_gpu_debug_layers"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ENABLE_MULTI_SLOT_TIMEOUT_MILLIS:Ljava/lang/String; = "enable_multi_slot_timeout_millis"

.field public static final ENABLE_RADIO_BUG_DETECTION:Ljava/lang/String; = "enable_radio_bug_detection"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ENABLE_RESTRICTED_BUCKET:Ljava/lang/String; = "enable_restricted_bucket"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ENABLE_TARE:Ljava/lang/String; = "enable_tare"

.field public static final ENABLE_TARE_ALARM_MANAGER:Ljava/lang/String; = "enable_tare_alarm_manager"

.field public static final ENABLE_TARE_JOB_SCHEDULER:Ljava/lang/String; = "enable_tare_job_scheduler"

.field public static final ENCODED_SURROUND_OUTPUT:Ljava/lang/String; = "encoded_surround_output"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ENCODED_SURROUND_OUTPUT_ALWAYS:I = 0x2

.field public static final ENCODED_SURROUND_OUTPUT_AUTO:I = 0x0

.field public static final ENCODED_SURROUND_OUTPUT_ENABLED_FORMATS:Ljava/lang/String; = "encoded_surround_output_enabled_formats"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ENCODED_SURROUND_OUTPUT_MANUAL:I = 0x3

.field public static final ENCODED_SURROUND_OUTPUT_NEVER:I = 0x1

.field public static final ENCODED_SURROUND_SC_MAX:I = 0x3

.field public static final ENHANCED_4G_MODE_ENABLED:Ljava/lang/String; = "volte_vt_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final EPHEMERAL_COOKIE_MAX_SIZE_BYTES:Ljava/lang/String; = "ephemeral_cookie_max_size_bytes"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ERROR_LOGCAT_PREFIX:Ljava/lang/String; = "logcat_for_"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final EUICC_FACTORY_RESET_TIMEOUT_MILLIS:Ljava/lang/String; = "euicc_factory_reset_timeout_millis"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final EUICC_PROVISIONED:Ljava/lang/String; = "euicc_provisioned"
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final EUICC_REMOVING_INVISIBLE_PROFILES_TIMEOUT_MILLIS:Ljava/lang/String; = "euicc_removing_invisible_profiles_timeout_millis"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final EUICC_SUPPORTED_COUNTRIES:Ljava/lang/String; = "euicc_supported_countries"
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final EUICC_SWITCH_SLOT_TIMEOUT_MILLIS:Ljava/lang/String; = "euicc_switch_slot_timeout_millis"

.field public static final EUICC_UNSUPPORTED_COUNTRIES:Ljava/lang/String; = "euicc_unsupported_countries"
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final FANCY_IME_ANIMATIONS:Ljava/lang/String; = "fancy_ime_animations"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final FORCED_APP_STANDBY_ENABLED:Ljava/lang/String; = "forced_app_standby_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final FORCED_APP_STANDBY_FOR_SMALL_BATTERY_ENABLED:Ljava/lang/String; = "forced_app_standby_for_small_battery_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final FORCE_ALLOW_ON_EXTERNAL:Ljava/lang/String; = "force_allow_on_external"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final FORCE_NON_DEBUGGABLE_FINAL_BUILD_FOR_COMPAT:Ljava/lang/String; = "force_non_debuggable_final_build_for_compat"

.field public static final FOREGROUND_SERVICE_STARTS_LOGGING_ENABLED:Ljava/lang/String; = "foreground_service_starts_logging_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final FPS_DEVISOR:Ljava/lang/String; = "fps_divisor"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final FSTRIM_MANDATORY_INTERVAL:Ljava/lang/String; = "fstrim_mandatory_interval"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final GLOBAL_HTTP_PROXY_EXCLUSION_LIST:Ljava/lang/String; = "global_http_proxy_exclusion_list"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final GLOBAL_HTTP_PROXY_HOST:Ljava/lang/String; = "global_http_proxy_host"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final GLOBAL_HTTP_PROXY_PAC:Ljava/lang/String; = "global_proxy_pac_url"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final GLOBAL_HTTP_PROXY_PORT:Ljava/lang/String; = "global_http_proxy_port"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final GNSS_HAL_LOCATION_REQUEST_DURATION_MILLIS:Ljava/lang/String; = "gnss_hal_location_request_duration_millis"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final GNSS_SATELLITE_BLOCKLIST:Ljava/lang/String; = "gnss_satellite_blocklist"

.field public static final GPRS_REGISTER_CHECK_PERIOD_MS:Ljava/lang/String; = "gprs_register_check_period_ms"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final GPU_DEBUG_APP:Ljava/lang/String; = "gpu_debug_app"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final GPU_DEBUG_LAYERS:Ljava/lang/String; = "gpu_debug_layers"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final GPU_DEBUG_LAYERS_GLES:Ljava/lang/String; = "gpu_debug_layers_gles"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final GPU_DEBUG_LAYER_APP:Ljava/lang/String; = "gpu_debug_layer_app"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final HEADS_UP_NOTIFICATIONS_ENABLED:Ljava/lang/String; = "heads_up_notifications_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final HEADS_UP_OFF:I = 0x0

.field public static final HEADS_UP_ON:I = 0x1

.field public static final HIDDEN_API_BLACKLIST_EXEMPTIONS:Ljava/lang/String; = "hidden_api_blacklist_exemptions"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final HIDDEN_API_POLICY:Ljava/lang/String; = "hidden_api_policy"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final HIDE_ERROR_DIALOGS:Ljava/lang/String; = "hide_error_dialogs"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final HTTP_PROXY:Ljava/lang/String; = "http_proxy"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final INET_CONDITION_DEBOUNCE_DOWN_DELAY:Ljava/lang/String; = "inet_condition_debounce_down_delay"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final INET_CONDITION_DEBOUNCE_UP_DELAY:Ljava/lang/String; = "inet_condition_debounce_up_delay"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final INSTALLED_INSTANT_APP_MAX_CACHE_PERIOD:Ljava/lang/String; = "installed_instant_app_max_cache_period"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final INSTALLED_INSTANT_APP_MIN_CACHE_PERIOD:Ljava/lang/String; = "installed_instant_app_min_cache_period"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final INSTALL_CARRIER_APP_NOTIFICATION_PERSISTENT:Ljava/lang/String; = "install_carrier_app_notification_persistent"
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final INSTALL_CARRIER_APP_NOTIFICATION_SLEEP_MILLIS:Ljava/lang/String; = "install_carrier_app_notification_sleep_millis"
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final INSTALL_NON_MARKET_APPS:Ljava/lang/String; = "install_non_market_apps"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final INSTANT_APP_DEXOPT_ENABLED:Ljava/lang/String; = "instant_app_dexopt_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final INSTANT_APP_SETTINGS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final INTEGRITY_CHECK_INCLUDES_RULE_PROVIDER:Ljava/lang/String; = "verify_integrity_for_rule_provider"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final INTENT_FIREWALL_UPDATE_CONTENT_URL:Ljava/lang/String; = "intent_firewall_content_url"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final INTENT_FIREWALL_UPDATE_METADATA_URL:Ljava/lang/String; = "intent_firewall_metadata_url"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final KEEP_PROFILE_IN_BACKGROUND:Ljava/lang/String; = "keep_profile_in_background"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final KERNEL_CPU_THREAD_READER:Ljava/lang/String; = "kernel_cpu_thread_reader"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final KEY_CHORD_POWER_VOLUME_UP:Ljava/lang/String; = "key_chord_power_volume_up"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final LANG_ID_UPDATE_CONTENT_URL:Ljava/lang/String; = "lang_id_content_url"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final LANG_ID_UPDATE_METADATA_URL:Ljava/lang/String; = "lang_id_metadata_url"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final LEGACY_RESTORE_SETTINGS:[Ljava/lang/String;

.field public static final LID_BEHAVIOR:Ljava/lang/String; = "lid_behavior"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final LOCATION_BACKGROUND_THROTTLE_INTERVAL_MS:Ljava/lang/String; = "location_background_throttle_interval_ms"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final LOCATION_BACKGROUND_THROTTLE_PACKAGE_WHITELIST:Ljava/lang/String; = "location_background_throttle_package_whitelist"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final LOCATION_BACKGROUND_THROTTLE_PROXIMITY_ALERT_INTERVAL_MS:Ljava/lang/String; = "location_background_throttle_proximity_alert_interval_ms"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final LOCATION_ENABLE_STATIONARY_THROTTLE:Ljava/lang/String; = "location_enable_stationary_throttle"

.field public static final LOCATION_IGNORE_SETTINGS_PACKAGE_WHITELIST:Ljava/lang/String; = "location_ignore_settings_package_whitelist"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final LOCATION_SETTINGS_LINK_TO_PERMISSIONS_ENABLED:Ljava/lang/String; = "location_settings_link_to_permissions_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final LOCK_SOUND:Ljava/lang/String; = "lock_sound"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final LOOPER_STATS:Ljava/lang/String; = "looper_stats"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final LOW_BATTERY_SOUND:Ljava/lang/String; = "low_battery_sound"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final LOW_BATTERY_SOUND_TIMEOUT:Ljava/lang/String; = "low_battery_sound_timeout"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final LOW_POWER_MODE:Ljava/lang/String; = "low_power"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final LOW_POWER_MODE_STICKY:Ljava/lang/String; = "low_power_sticky"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final LOW_POWER_MODE_STICKY_AUTO_DISABLE_ENABLED:Ljava/lang/String; = "low_power_sticky_auto_disable_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final LOW_POWER_MODE_STICKY_AUTO_DISABLE_LEVEL:Ljava/lang/String; = "low_power_sticky_auto_disable_level"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final LOW_POWER_MODE_SUGGESTION_PARAMS:Ljava/lang/String; = "low_power_mode_suggestion_params"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final LOW_POWER_MODE_TRIGGER_LEVEL:Ljava/lang/String; = "low_power_trigger_level"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final LOW_POWER_MODE_TRIGGER_LEVEL_MAX:Ljava/lang/String; = "low_power_trigger_level_max"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final LOW_POWER_STANDBY_ACTIVE_DURING_MAINTENANCE:Ljava/lang/String; = "low_power_standby_active_during_maintenance"

.field public static final LOW_POWER_STANDBY_ENABLED:Ljava/lang/String; = "low_power_standby_enabled"

.field public static final LTE_SERVICE_FORCED:Ljava/lang/String; = "lte_service_forced"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final MANAGED_PROVISIONING_DEFER_PROVISIONING_TO_ROLE_HOLDER:Ljava/lang/String; = "managed_provisioning_defer_provisioning_to_role_holder"

.field public static final MAXIMUM_OBSCURING_OPACITY_FOR_TOUCH:Ljava/lang/String; = "maximum_obscuring_opacity_for_touch"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final MAX_ERROR_BYTES_PREFIX:Ljava/lang/String; = "max_error_bytes_for_"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final MAX_NOTIFICATION_ENQUEUE_RATE:Ljava/lang/String; = "max_notification_enqueue_rate"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final MAX_SOUND_TRIGGER_DETECTION_SERVICE_OPS_PER_DAY:Ljava/lang/String; = "max_sound_trigger_detection_service_ops_per_day"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final MDC_INITIAL_MAX_RETRY:Ljava/lang/String; = "mdc_initial_max_retry"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final MHL_INPUT_SWITCHING_ENABLED:Ljava/lang/String; = "mhl_input_switching_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final MHL_POWER_CHARGE_ENABLED:Ljava/lang/String; = "mhl_power_charge_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final MIN_DURATION_BETWEEN_RECOVERY_STEPS_IN_MS:Ljava/lang/String; = "min_duration_between_recovery_steps"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final MOBILE_DATA:Ljava/lang/String; = "mobile_data"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final MOBILE_DATA_ALWAYS_ON:Ljava/lang/String; = "mobile_data_always_on"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final MODEM_STACK_ENABLED_FOR_SLOT:Ljava/lang/String; = "modem_stack_enabled_for_slot"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final MODE_RINGER:Ljava/lang/String; = "mode_ringer"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field private static final MOVED_TO_SECURE:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final MOVED_TO_SYSTEM:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final MULTI_SIM_DATA_CALL_SUBSCRIPTION:Ljava/lang/String; = "multi_sim_data_call"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final MULTI_SIM_SMS_PROMPT:Ljava/lang/String; = "multi_sim_sms_prompt"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final MULTI_SIM_SMS_SUBSCRIPTION:Ljava/lang/String; = "multi_sim_sms"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final MULTI_SIM_USER_PREFERRED_SUBS:[Ljava/lang/String;
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final MULTI_SIM_VOICE_CALL_SUBSCRIPTION:Ljava/lang/String; = "multi_sim_voice_call"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final MULTI_SIM_VOICE_PROMPT:Ljava/lang/String; = "multi_sim_voice_prompt"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NATIVE_FLAGS_HEALTH_CHECK_ENABLED:Ljava/lang/String; = "native_flags_health_check_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETPOLICY_OVERRIDE_ENABLED:Ljava/lang/String; = "netpolicy_override_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETPOLICY_QUOTA_ENABLED:Ljava/lang/String; = "netpolicy_quota_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETPOLICY_QUOTA_FRAC_JOBS:Ljava/lang/String; = "netpolicy_quota_frac_jobs"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETPOLICY_QUOTA_FRAC_MULTIPATH:Ljava/lang/String; = "netpolicy_quota_frac_multipath"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETPOLICY_QUOTA_LIMITED:Ljava/lang/String; = "netpolicy_quota_limited"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETPOLICY_QUOTA_UNLIMITED:Ljava/lang/String; = "netpolicy_quota_unlimited"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETSTATS_AUGMENT_ENABLED:Ljava/lang/String; = "netstats_augment_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETSTATS_COMBINE_SUBTYPE_ENABLED:Ljava/lang/String; = "netstats_combine_subtype_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETSTATS_DEV_BUCKET_DURATION:Ljava/lang/String; = "netstats_dev_bucket_duration"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETSTATS_DEV_DELETE_AGE:Ljava/lang/String; = "netstats_dev_delete_age"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETSTATS_DEV_PERSIST_BYTES:Ljava/lang/String; = "netstats_dev_persist_bytes"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETSTATS_DEV_ROTATE_AGE:Ljava/lang/String; = "netstats_dev_rotate_age"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETSTATS_ENABLED:Ljava/lang/String; = "netstats_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETSTATS_GLOBAL_ALERT_BYTES:Ljava/lang/String; = "netstats_global_alert_bytes"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETSTATS_POLL_INTERVAL:Ljava/lang/String; = "netstats_poll_interval"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETSTATS_SAMPLE_ENABLED:Ljava/lang/String; = "netstats_sample_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETSTATS_TIME_CACHE_MAX_AGE:Ljava/lang/String; = "netstats_time_cache_max_age"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final NETSTATS_UID_BUCKET_DURATION:Ljava/lang/String; = "netstats_uid_bucket_duration"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETSTATS_UID_DELETE_AGE:Ljava/lang/String; = "netstats_uid_delete_age"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETSTATS_UID_PERSIST_BYTES:Ljava/lang/String; = "netstats_uid_persist_bytes"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETSTATS_UID_ROTATE_AGE:Ljava/lang/String; = "netstats_uid_rotate_age"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETSTATS_UID_TAG_BUCKET_DURATION:Ljava/lang/String; = "netstats_uid_tag_bucket_duration"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETSTATS_UID_TAG_DELETE_AGE:Ljava/lang/String; = "netstats_uid_tag_delete_age"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETSTATS_UID_TAG_PERSIST_BYTES:Ljava/lang/String; = "netstats_uid_tag_persist_bytes"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETSTATS_UID_TAG_ROTATE_AGE:Ljava/lang/String; = "netstats_uid_tag_rotate_age"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETWORK_AVOID_BAD_WIFI:Ljava/lang/String; = "network_avoid_bad_wifi"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETWORK_DEFAULT_DAILY_MULTIPATH_QUOTA_BYTES:Ljava/lang/String; = "network_default_daily_multipath_quota_bytes"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETWORK_METERED_MULTIPATH_PREFERENCE:Ljava/lang/String; = "network_metered_multipath_preference"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETWORK_PREFERENCE:Ljava/lang/String; = "network_preference"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETWORK_RECOMMENDATIONS_ENABLED:Ljava/lang/String; = "network_recommendations_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final NETWORK_RECOMMENDATIONS_PACKAGE:Ljava/lang/String; = "network_recommendations_package"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final NETWORK_SCORER_APP:Ljava/lang/String; = "network_scorer_app"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETWORK_SCORING_PROVISIONED:Ljava/lang/String; = "network_scoring_provisioned"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETWORK_SCORING_UI_ENABLED:Ljava/lang/String; = "network_scoring_ui_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final NETWORK_SWITCH_NOTIFICATION_DAILY_LIMIT:Ljava/lang/String; = "network_switch_notification_daily_limit"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETWORK_SWITCH_NOTIFICATION_RATE_LIMIT_MILLIS:Ljava/lang/String; = "network_switch_notification_rate_limit_millis"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETWORK_WATCHLIST_ENABLED:Ljava/lang/String; = "network_watchlist_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NETWORK_WATCHLIST_LAST_REPORT_TIME:Ljava/lang/String; = "network_watchlist_last_report_time"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NEW_CONTACT_AGGREGATOR:Ljava/lang/String; = "new_contact_aggregator"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NIGHT_DISPLAY_FORCED_AUTO_MODE_AVAILABLE:Ljava/lang/String; = "night_display_forced_auto_mode_available"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NITZ_NETWORK_DISCONNECT_RETENTION:Ljava/lang/String; = "nitz_network_disconnect_retention"

.field public static final NITZ_UPDATE_DIFF:Ljava/lang/String; = "nitz_update_diff"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NITZ_UPDATE_SPACING:Ljava/lang/String; = "nitz_update_spacing"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NOTIFICATION_BUBBLES:Ljava/lang/String; = "notification_bubbles"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final NOTIFICATION_FEEDBACK_ENABLED:Ljava/lang/String; = "notification_feedback_enabled"

.field public static final NOTIFICATION_SNOOZE_OPTIONS:Ljava/lang/String; = "notification_snooze_options"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NR_NSA_TRACKING_SCREEN_OFF_MODE:Ljava/lang/String; = "nr_nsa_tracking_screen_off_mode"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NTP_SERVER:Ljava/lang/String; = "ntp_server"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NTP_SERVER_2:Ljava/lang/String; = "ntp_server_2"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final NTP_TIMEOUT:Ljava/lang/String; = "ntp_timeout"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ONE_HANDED_KEYGUARD_SIDE:Ljava/lang/String; = "one_handed_keyguard_side"

.field public static final ONE_HANDED_KEYGUARD_SIDE_LEFT:I = 0x0

.field public static final ONE_HANDED_KEYGUARD_SIDE_RIGHT:I = 0x1

.field public static final OTA_DISABLE_AUTOMATIC_UPDATE:Ljava/lang/String; = "ota_disable_automatic_update"
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final OVERLAY_DISPLAY_DEVICES:Ljava/lang/String; = "overlay_display_devices"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final OVERRIDE_SETTINGS_PROVIDER_RESTORE_ANY_VERSION:Ljava/lang/String; = "override_settings_provider_restore_any_version"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final PACKAGE_STREAMING_VERIFIER_TIMEOUT:Ljava/lang/String; = "streaming_verifier_timeout"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final PACKAGE_VERIFIER_DEFAULT_RESPONSE:Ljava/lang/String; = "verifier_default_response"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final PACKAGE_VERIFIER_INCLUDE_ADB:Ljava/lang/String; = "verifier_verify_adb_installs"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final PACKAGE_VERIFIER_SETTING_VISIBLE:Ljava/lang/String; = "verifier_setting_visible"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final PACKAGE_VERIFIER_TIMEOUT:Ljava/lang/String; = "verifier_timeout"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final PAC_CHANGE_DELAY:Ljava/lang/String; = "pac_change_delay"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final PDP_WATCHDOG_ERROR_POLL_COUNT:Ljava/lang/String; = "pdp_watchdog_error_poll_count"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final PDP_WATCHDOG_ERROR_POLL_INTERVAL_MS:Ljava/lang/String; = "pdp_watchdog_error_poll_interval_ms"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final PDP_WATCHDOG_LONG_POLL_INTERVAL_MS:Ljava/lang/String; = "pdp_watchdog_long_poll_interval_ms"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final PDP_WATCHDOG_MAX_PDP_RESET_FAIL_COUNT:Ljava/lang/String; = "pdp_watchdog_max_pdp_reset_fail_count"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final PDP_WATCHDOG_POLL_INTERVAL_MS:Ljava/lang/String; = "pdp_watchdog_poll_interval_ms"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final PDP_WATCHDOG_TRIGGER_PACKET_COUNT:Ljava/lang/String; = "pdp_watchdog_trigger_packet_count"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final PEOPLE_SPACE_CONVERSATION_TYPE:Ljava/lang/String; = "people_space_conversation_type"

.field public static final POLICY_CONTROL:Ljava/lang/String; = "policy_control"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final POWER_BUTTON_LONG_PRESS:Ljava/lang/String; = "power_button_long_press"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final POWER_BUTTON_LONG_PRESS_DURATION_MS:Ljava/lang/String; = "power_button_long_press_duration_ms"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final POWER_BUTTON_SUPPRESSION_DELAY_AFTER_GESTURE_WAKE:Ljava/lang/String; = "power_button_suppression_delay_after_gesture_wake"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final POWER_BUTTON_VERY_LONG_PRESS:Ljava/lang/String; = "power_button_very_long_press"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final POWER_MANAGER_CONSTANTS:Ljava/lang/String; = "power_manager_constants"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final POWER_SOUNDS_ENABLED:Ljava/lang/String; = "power_sounds_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final PREFERRED_NETWORK_MODE:Ljava/lang/String; = "preferred_network_mode"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final PRIVATE_DNS_DEFAULT_MODE:Ljava/lang/String; = "private_dns_default_mode"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final PRIVATE_DNS_MODE:Ljava/lang/String; = "private_dns_mode"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final PRIVATE_DNS_SPECIFIER:Ljava/lang/String; = "private_dns_specifier"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final PROVISIONING_APN_ALARM_DELAY_IN_MS:Ljava/lang/String; = "provisioning_apn_alarm_delay_in_ms"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final RADIO_BLUETOOTH:Ljava/lang/String; = "bluetooth"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final RADIO_BUG_SYSTEM_ERROR_COUNT_THRESHOLD:Ljava/lang/String; = "radio_bug_system_error_count_threshold"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final RADIO_BUG_WAKELOCK_TIMEOUT_COUNT_THRESHOLD:Ljava/lang/String; = "radio_bug_wakelock_timeout_count_threshold"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final RADIO_CELL:Ljava/lang/String; = "cell"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final RADIO_NFC:Ljava/lang/String; = "nfc"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final RADIO_WIFI:Ljava/lang/String; = "wifi"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final RADIO_WIMAX:Ljava/lang/String; = "wimax"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final READ_EXTERNAL_STORAGE_ENFORCED_DEFAULT:Ljava/lang/String; = "read_external_storage_enforced_default"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final RECOMMENDED_NETWORK_EVALUATOR_CACHE_EXPIRY_MS:Ljava/lang/String; = "recommended_network_evaluator_cache_expiry_ms"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final REQUIRE_PASSWORD_TO_DECRYPT:Ljava/lang/String; = "require_password_to_decrypt"
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final RESTRICTED_NETWORKING_MODE:Ljava/lang/String; = "restricted_networking_mode"

.field public static final REVIEW_PERMISSIONS_NOTIFICATION_STATE:Ljava/lang/String; = "review_permissions_notification_state"

.field public static final SAFE_BOOT_DISALLOWED:Ljava/lang/String; = "safe_boot_disallowed"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SELINUX_STATUS:Ljava/lang/String; = "selinux_status"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SELINUX_UPDATE_CONTENT_URL:Ljava/lang/String; = "selinux_content_url"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SELINUX_UPDATE_METADATA_URL:Ljava/lang/String; = "selinux_metadata_url"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SEND_ACTION_APP_ERROR:Ljava/lang/String; = "send_action_app_error"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SETTINGS_USE_EXTERNAL_PROVIDER_API:Ljava/lang/String; = "settings_use_external_provider_api"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SETTINGS_USE_PSD_API:Ljava/lang/String; = "settings_use_psd_api"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SETUP_PREPAID_DATA_SERVICE_URL:Ljava/lang/String; = "setup_prepaid_data_service_url"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SETUP_PREPAID_DETECTION_REDIR_HOST:Ljava/lang/String; = "setup_prepaid_detection_redir_host"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SETUP_PREPAID_DETECTION_TARGET_URL:Ljava/lang/String; = "setup_prepaid_detection_target_url"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SET_GLOBAL_HTTP_PROXY:Ljava/lang/String; = "set_global_http_proxy"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SET_INSTALL_LOCATION:Ljava/lang/String; = "set_install_location"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SHORTCUT_MANAGER_CONSTANTS:Ljava/lang/String; = "shortcut_manager_constants"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SHOW_ANGLE_IN_USE_DIALOG_BOX:Ljava/lang/String; = "show_angle_in_use_dialog_box"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SHOW_FIRST_CRASH_DIALOG:Ljava/lang/String; = "show_first_crash_dialog"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SHOW_HIDDEN_LAUNCHER_ICON_APPS_ENABLED:Ljava/lang/String; = "show_hidden_icon_apps_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SHOW_MEDIA_ON_QUICK_SETTINGS:Ljava/lang/String; = "qs_media_controls"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SHOW_MUTE_IN_CRASH_DIALOG:Ljava/lang/String; = "show_mute_in_crash_dialog"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SHOW_NEW_APP_INSTALLED_NOTIFICATION_ENABLED:Ljava/lang/String; = "show_new_app_installed_notification_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SHOW_NEW_NOTIF_DISMISS:Ljava/lang/String; = "show_new_notif_dismiss"

.field public static final SHOW_NOTIFICATION_CHANNEL_WARNINGS:Ljava/lang/String; = "show_notification_channel_warnings"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SHOW_PEOPLE_SPACE:Ljava/lang/String; = "show_people_space"

.field public static final SHOW_PROCESSES:Ljava/lang/String; = "show_processes"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SHOW_RESTART_IN_CRASH_DIALOG:Ljava/lang/String; = "show_restart_in_crash_dialog"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SHOW_TEMPERATURE_WARNING:Ljava/lang/String; = "show_temperature_warning"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SHOW_USB_TEMPERATURE_ALARM:Ljava/lang/String; = "show_usb_temperature_alarm"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SHOW_ZEN_SETTINGS_SUGGESTION:Ljava/lang/String; = "show_zen_settings_suggestion"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SHOW_ZEN_UPGRADE_NOTIFICATION:Ljava/lang/String; = "show_zen_upgrade_notification"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SIGNED_CONFIG_VERSION:Ljava/lang/String; = "signed_config_version"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SMART_DDS_SWITCH:Ljava/lang/String; = "smart_dds_switch"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SMART_REPLIES_IN_NOTIFICATIONS_FLAGS:Ljava/lang/String; = "smart_replies_in_notifications_flags"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SMART_SELECTION_UPDATE_CONTENT_URL:Ljava/lang/String; = "smart_selection_content_url"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SMART_SELECTION_UPDATE_METADATA_URL:Ljava/lang/String; = "smart_selection_metadata_url"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SMART_SUGGESTIONS_IN_NOTIFICATIONS_FLAGS:Ljava/lang/String; = "smart_suggestions_in_notifications_flags"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SMS_OUTGOING_CHECK_INTERVAL_MS:Ljava/lang/String; = "sms_outgoing_check_interval_ms"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SMS_OUTGOING_CHECK_MAX_COUNT:Ljava/lang/String; = "sms_outgoing_check_max_count"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SMS_SHORT_CODES_UPDATE_CONTENT_URL:Ljava/lang/String; = "sms_short_codes_content_url"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SMS_SHORT_CODES_UPDATE_METADATA_URL:Ljava/lang/String; = "sms_short_codes_metadata_url"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SMS_SHORT_CODE_CONFIRMATION:Ljava/lang/String; = "sms_short_code_confirmation"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SMS_SHORT_CODE_RULE:Ljava/lang/String; = "sms_short_code_rule"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SOFT_AP_TIMEOUT_ENABLED:Ljava/lang/String; = "soft_ap_timeout_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SOUND_TRIGGER_DETECTION_SERVICE_OP_TIMEOUT:Ljava/lang/String; = "sound_trigger_detection_service_op_timeout"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SPEED_LABEL_CACHE_EVICTION_AGE_MILLIS:Ljava/lang/String; = "speed_label_cache_eviction_age_millis"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final SQLITE_COMPATIBILITY_WAL_FLAGS:Ljava/lang/String; = "sqlite_compatibility_wal_flags"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final STAY_ON_WHILE_PLUGGED_IN:Ljava/lang/String; = "stay_on_while_plugged_in"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final STORAGE_BENCHMARK_INTERVAL:Ljava/lang/String; = "storage_benchmark_interval"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final STORAGE_SETTINGS_CLOBBER_THRESHOLD:Ljava/lang/String; = "storage_settings_clobber_threshold"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final STYLUS_HANDWRITING_ENABLED:Ljava/lang/String; = "stylus_handwriting_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SYNC_MANAGER_CONSTANTS:Ljava/lang/String; = "sync_manager_constants"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SYNC_MAX_RETRY_DELAY_IN_SECONDS:Ljava/lang/String; = "sync_max_retry_delay_in_seconds"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SYS_FREE_STORAGE_LOG_INTERVAL:Ljava/lang/String; = "sys_free_storage_log_interval"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SYS_STORAGE_CACHE_PERCENTAGE:Ljava/lang/String; = "sys_storage_cache_percentage"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SYS_STORAGE_FULL_THRESHOLD_BYTES:Ljava/lang/String; = "sys_storage_full_threshold_bytes"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SYS_STORAGE_THRESHOLD_MAX_BYTES:Ljava/lang/String; = "sys_storage_threshold_max_bytes"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SYS_STORAGE_THRESHOLD_PERCENTAGE:Ljava/lang/String; = "sys_storage_threshold_percentage"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SYS_TRACED:Ljava/lang/String; = "sys_traced"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final SYS_UIDCPUPOWER:Ljava/lang/String; = "sys_uidcpupower"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final TARE_ALARM_MANAGER_CONSTANTS:Ljava/lang/String; = "tare_alarm_manager_constants"

.field public static final TARE_JOB_SCHEDULER_CONSTANTS:Ljava/lang/String; = "tare_job_scheduler_constants"

.field public static final TCP_DEFAULT_INIT_RWND:Ljava/lang/String; = "tcp_default_init_rwnd"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final TETHER_DUN_APN:Ljava/lang/String; = "tether_dun_apn"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final TETHER_DUN_REQUIRED:Ljava/lang/String; = "tether_dun_required"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final TETHER_ENABLE_LEGACY_DHCP_SERVER:Ljava/lang/String; = "tether_enable_legacy_dhcp_server"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final TETHER_OFFLOAD_DISABLED:Ljava/lang/String; = "tether_offload_disabled"
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final TETHER_SUPPORTED:Ljava/lang/String; = "tether_supported"
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final TEXT_CLASSIFIER_ACTION_MODEL_PARAMS:Ljava/lang/String; = "text_classifier_action_model_params"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final TEXT_CLASSIFIER_CONSTANTS:Ljava/lang/String; = "text_classifier_constants"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final THEATER_MODE_ON:Ljava/lang/String; = "theater_mode_on"
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final TIME_ONLY_MODE_CONSTANTS:Ljava/lang/String; = "time_only_mode_constants"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final TIME_REMAINING_ESTIMATE_BASED_ON_USAGE:Ljava/lang/String; = "time_remaining_estimate_based_on_usage"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TIME_REMAINING_ESTIMATE_MILLIS:Ljava/lang/String; = "time_remaining_estimate_millis"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TRANSIENT_SETTINGS:[Ljava/lang/String;

.field public static final TRANSITION_ANIMATION_SCALE:Ljava/lang/String; = "transition_animation_scale"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final TRUSTED_SOUND:Ljava/lang/String; = "trusted_sound"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final TZINFO_UPDATE_CONTENT_URL:Ljava/lang/String; = "tzinfo_content_url"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final TZINFO_UPDATE_METADATA_URL:Ljava/lang/String; = "tzinfo_metadata_url"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final UNGAZE_SLEEP_ENABLED:Ljava/lang/String; = "ungaze_sleep_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final UNINSTALLED_INSTANT_APP_MAX_CACHE_PERIOD:Ljava/lang/String; = "uninstalled_instant_app_max_cache_period"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final UNINSTALLED_INSTANT_APP_MIN_CACHE_PERIOD:Ljava/lang/String; = "uninstalled_instant_app_min_cache_period"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final UNLOCK_SOUND:Ljava/lang/String; = "unlock_sound"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final UNUSED_STATIC_SHARED_LIB_MIN_CACHE_PERIOD:Ljava/lang/String; = "unused_static_shared_lib_min_cache_period"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final UPDATABLE_DRIVER_ALL_APPS:Ljava/lang/String; = "updatable_driver_all_apps"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final UPDATABLE_DRIVER_PRERELEASE_OPT_IN_APPS:Ljava/lang/String; = "updatable_driver_prerelease_opt_in_apps"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final UPDATABLE_DRIVER_PRODUCTION_ALLOWLIST:Ljava/lang/String; = "updatable_driver_production_allowlist"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final UPDATABLE_DRIVER_PRODUCTION_DENYLIST:Ljava/lang/String; = "updatable_driver_production_denylist"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final UPDATABLE_DRIVER_PRODUCTION_DENYLISTS:Ljava/lang/String; = "updatable_driver_production_denylists"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final UPDATABLE_DRIVER_PRODUCTION_OPT_IN_APPS:Ljava/lang/String; = "updatable_driver_production_opt_in_apps"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final UPDATABLE_DRIVER_PRODUCTION_OPT_OUT_APPS:Ljava/lang/String; = "updatable_driver_production_opt_out_apps"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final UPDATABLE_DRIVER_SPHAL_LIBRARIES:Ljava/lang/String; = "updatable_driver_sphal_libraries"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final USB_MASS_STORAGE_ENABLED:Ljava/lang/String; = "usb_mass_storage_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final USER_ABSENT_RADIOS_OFF_FOR_SMALL_BATTERY_ENABLED:Ljava/lang/String; = "user_absent_radios_off_for_small_battery_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final USER_ABSENT_TOUCH_OFF_FOR_SMALL_BATTERY_ENABLED:Ljava/lang/String; = "user_absent_touch_off_for_small_battery_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final USER_DISABLED_HDR_FORMATS:Ljava/lang/String; = "user_disabled_hdr_formats"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final USER_PREFERRED_REFRESH_RATE:Ljava/lang/String; = "user_preferred_refresh_rate"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final USER_PREFERRED_RESOLUTION_HEIGHT:Ljava/lang/String; = "user_preferred_resolution_height"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final USER_PREFERRED_RESOLUTION_WIDTH:Ljava/lang/String; = "user_preferred_resolution_width"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final USER_SWITCHER_ENABLED:Ljava/lang/String; = "user_switcher_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final USE_GOOGLE_MAIL:Ljava/lang/String; = "use_google_mail"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final USE_OPEN_WIFI_PACKAGE:Ljava/lang/String; = "use_open_wifi_package"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final UWB_ENABLED:Ljava/lang/String; = "uwb_enabled"

.field public static final VIBRATING_FOR_OUTGOING_CALL_ACCEPTED:Ljava/lang/String; = "vibrating_for_outgoing_call_accepted"

.field public static final VT_IMS_ENABLED:Ljava/lang/String; = "vt_ims_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WAIT_FOR_DEBUGGER:Ljava/lang/String; = "wait_for_debugger"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WARNING_TEMPERATURE:Ljava/lang/String; = "warning_temperature"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WATCHDOG_TIMEOUT_MILLIS:Ljava/lang/String; = "system_server_watchdog_timeout_ms"

.field public static final WEBVIEW_DATA_REDUCTION_PROXY_KEY:Ljava/lang/String; = "webview_data_reduction_proxy_key"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WEBVIEW_MULTIPROCESS:Ljava/lang/String; = "webview_multiprocess"
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WEBVIEW_PROVIDER:Ljava/lang/String; = "webview_provider"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WFC_IMS_ENABLED:Ljava/lang/String; = "wfc_ims_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WFC_IMS_MODE:Ljava/lang/String; = "wfc_ims_mode"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WFC_IMS_ROAMING_ENABLED:Ljava/lang/String; = "wfc_ims_roaming_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WFC_IMS_ROAMING_MODE:Ljava/lang/String; = "wfc_ims_roaming_mode"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_ALWAYS_REQUESTED:Ljava/lang/String; = "wifi_always_requested"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WIFI_BADGING_THRESHOLDS:Ljava/lang/String; = "wifi_badging_thresholds"
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WIFI_BOUNCE_DELAY_OVERRIDE_MS:Ljava/lang/String; = "wifi_bounce_delay_override_ms"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WIFI_CONNECTED_MAC_RANDOMIZATION_ENABLED:Ljava/lang/String; = "wifi_connected_mac_randomization_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_COUNTRY_CODE:Ljava/lang/String; = "wifi_country_code"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WIFI_DEVICE_OWNER_CONFIGS_LOCKDOWN:Ljava/lang/String; = "wifi_device_owner_configs_lockdown"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WIFI_DISPLAY_CERTIFICATION_ON:Ljava/lang/String; = "wifi_display_certification_on"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WIFI_DISPLAY_ON:Ljava/lang/String; = "wifi_display_on"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WIFI_DISPLAY_WPS_CONFIG:Ljava/lang/String; = "wifi_display_wps_config"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WIFI_ENHANCED_AUTO_JOIN:Ljava/lang/String; = "wifi_enhanced_auto_join"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WIFI_EPHEMERAL_OUT_OF_RANGE_TIMEOUT_MS:Ljava/lang/String; = "wifi_ephemeral_out_of_range_timeout_ms"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WIFI_FRAMEWORK_SCAN_INTERVAL_MS:Ljava/lang/String; = "wifi_framework_scan_interval_ms"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WIFI_FREQUENCY_BAND:Ljava/lang/String; = "wifi_frequency_band"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WIFI_IDLE_MS:Ljava/lang/String; = "wifi_idle_ms"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WIFI_MAX_DHCP_RETRY_COUNT:Ljava/lang/String; = "wifi_max_dhcp_retry_count"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WIFI_MIGRATION_COMPLETED:Ljava/lang/String; = "wifi_migration_completed"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WIFI_MOBILE_DATA_TRANSITION_WAKELOCK_TIMEOUT_MS:Ljava/lang/String; = "wifi_mobile_data_transition_wakelock_timeout_ms"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WIFI_NETWORKS_AVAILABLE_NOTIFICATION_ON:Ljava/lang/String; = "wifi_networks_available_notification_on"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_NETWORKS_AVAILABLE_REPEAT_DELAY:Ljava/lang/String; = "wifi_networks_available_repeat_delay"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_NETWORK_SHOW_RSSI:Ljava/lang/String; = "wifi_network_show_rssi"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WIFI_NUM_OPEN_NETWORKS_KEPT:Ljava/lang/String; = "wifi_num_open_networks_kept"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_ON:Ljava/lang/String; = "wifi_on"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WIFI_ON_WHEN_PROXY_DISCONNECTED:Ljava/lang/String; = "wifi_on_when_proxy_disconnected"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WIFI_P2P_DEVICE_NAME:Ljava/lang/String; = "wifi_p2p_device_name"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_P2P_PENDING_FACTORY_RESET:Ljava/lang/String; = "wifi_p2p_pending_factory_reset"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_SCAN_ALWAYS_AVAILABLE:Ljava/lang/String; = "wifi_scan_always_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_SCAN_INTERVAL_WHEN_P2P_CONNECTED_MS:Ljava/lang/String; = "wifi_scan_interval_p2p_connected_ms"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WIFI_SCAN_THROTTLE_ENABLED:Ljava/lang/String; = "wifi_scan_throttle_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_SCORE_PARAMS:Ljava/lang/String; = "wifi_score_params"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_SLEEP_POLICY:Ljava/lang/String; = "wifi_sleep_policy"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_SLEEP_POLICY_DEFAULT:I = 0x0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_SLEEP_POLICY_NEVER:I = 0x2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_SLEEP_POLICY_NEVER_WHILE_PLUGGED:I = 0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_SUPPLICANT_SCAN_INTERVAL_MS:Ljava/lang/String; = "wifi_supplicant_scan_interval_ms"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WIFI_VERBOSE_LOGGING_ENABLED:Ljava/lang/String; = "wifi_verbose_logging_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_WAKEUP_ENABLED:Ljava/lang/String; = "wifi_wakeup_enabled"
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WIFI_WATCHDOG_ON:Ljava/lang/String; = "wifi_watchdog_on"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WIFI_WATCHDOG_POOR_NETWORK_TEST_ENABLED:Ljava/lang/String; = "wifi_watchdog_poor_network_test_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WIMAX_NETWORKS_AVAILABLE_NOTIFICATION_ON:Ljava/lang/String; = "wimax_networks_available_notification_on"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WINDOW_ANIMATION_SCALE:Ljava/lang/String; = "window_animation_scale"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WIRELESS_CHARGING_STARTED_SOUND:Ljava/lang/String; = "wireless_charging_started_sound"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final WTF_IS_FATAL:Ljava/lang/String; = "wtf_is_fatal"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ZEN_DURATION:Ljava/lang/String; = "zen_duration"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ZEN_DURATION_FOREVER:I = 0x0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ZEN_DURATION_PROMPT:I = -0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ZEN_MODE:Ljava/lang/String; = "zen_mode"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ZEN_MODE_ALARMS:I = 0x3

.field public static final ZEN_MODE_CONFIG_ETAG:Ljava/lang/String; = "zen_mode_config_etag"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ZEN_MODE_IMPORTANT_INTERRUPTIONS:I = 0x1

.field public static final ZEN_MODE_NO_INTERRUPTIONS:I = 0x2

.field public static final ZEN_MODE_OFF:I = 0x0

.field public static final ZEN_MODE_RINGER_LEVEL:Ljava/lang/String; = "zen_mode_ringer_level"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field public static final ZEN_SETTINGS_SUGGESTION_VIEWED:Ljava/lang/String; = "zen_settings_suggestion_viewed"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ZEN_SETTINGS_UPDATED:Ljava/lang/String; = "zen_settings_updated"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ZRAM_ENABLED:Ljava/lang/String; = "zram_enabled"
    .annotation runtime Landroid/provider/Settings$Readable;
    .end annotation
.end field

.field private static final sNameValueCache:Landroid/provider/Settings$NameValueCache;

.field private static final sProviderHolder:Landroid/provider/Settings$ContentProviderHolder;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const-string v0, "content://settings/global"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sput-object v2, Landroid/provider/Settings$Global;->CONTENT_URI:Landroid/net/Uri;

    const-string v0, "clockwork_home_ready"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/provider/Settings$Global;->TRANSIENT_SETTINGS:[Ljava/lang/String;

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Landroid/provider/Settings$Global;->LEGACY_RESTORE_SETTINGS:[Ljava/lang/String;

    new-instance v6, Landroid/provider/Settings$ContentProviderHolder;

    invoke-direct {v6, v2}, Landroid/provider/Settings$ContentProviderHolder;-><init>(Landroid/net/Uri;)V

    sput-object v6, Landroid/provider/Settings$Global;->sProviderHolder:Landroid/provider/Settings$ContentProviderHolder;

    new-instance v0, Landroid/provider/Settings$NameValueCache;

    const-class v7, Landroid/provider/Settings$Global;

    const-string v3, "GET_global"

    const-string v4, "PUT_global"

    const-string v5, "DELETE_global"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Landroid/provider/Settings$NameValueCache;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/provider/Settings$ContentProviderHolder;Ljava/lang/Class;)V

    sput-object v0, Landroid/provider/Settings$Global;->sNameValueCache:Landroid/provider/Settings$NameValueCache;

    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    sput-object v0, Landroid/provider/Settings$Global;->MOVED_TO_SECURE:Ljava/util/HashSet;

    const-string/jumbo v1, "install_non_market_apps"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const-string/jumbo v1, "zen_duration"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const-string/jumbo v1, "show_zen_upgrade_notification"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const-string/jumbo v1, "show_zen_settings_suggestion"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const-string/jumbo v1, "zen_settings_updated"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const-string/jumbo v1, "zen_settings_suggestion_viewed"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const-string v1, "charging_sounds_enabled"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const-string v1, "charging_vibration_enabled"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const-string/jumbo v1, "notification_bubbles"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    sput-object v0, Landroid/provider/Settings$Global;->MOVED_TO_SYSTEM:Ljava/util/HashSet;

    const-string v1, "apply_ramping_ringer"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const-string/jumbo v0, "user_preferred_sub1"

    const-string/jumbo v1, "user_preferred_sub2"

    const-string/jumbo v2, "user_preferred_sub3"

    filled-new-array {v0, v1, v2}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/provider/Settings$Global;->MULTI_SIM_USER_PREFERRED_SUBS:[Ljava/lang/String;

    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    sput-object v0, Landroid/provider/Settings$Global;->INSTANT_APP_SETTINGS:Ljava/util/Set;

    const-string/jumbo v1, "wait_for_debugger"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "device_provisioned"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "force_resizable_activities"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "debug.force_rtl"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "ephemeral_cookie_max_size_bytes"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "airplane_mode_on"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string/jumbo v1, "window_animation_scale"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string/jumbo v1, "transition_animation_scale"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "animator_duration_scale"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "debug_view_attributes"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "debug_view_attributes_application_package"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string/jumbo v1, "wtf_is_fatal"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string/jumbo v1, "send_action_app_error"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string/jumbo v1, "zen_mode"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/provider/Settings$NameValueTable;-><init>()V

    return-void
.end method

.method public static clearProviderForTest()V
    .locals 1

    sget-object v0, Landroid/provider/Settings$Global;->sProviderHolder:Landroid/provider/Settings$ContentProviderHolder;

    invoke-virtual {v0}, Landroid/provider/Settings$ContentProviderHolder;->clearProviderForTest()V

    sget-object v0, Landroid/provider/Settings$Global;->sNameValueCache:Landroid/provider/Settings$NameValueCache;

    invoke-virtual {v0}, Landroid/provider/Settings$NameValueCache;->clearGenerationTrackerForTest()V

    return-void
.end method

.method public static getFloat(Landroid/content/ContentResolver;Ljava/lang/String;)F
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/provider/Settings$SettingNotFoundException;
        }
    .end annotation

    invoke-static {p0, p1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/provider/Settings;->-$$Nest$smparseFloatSetting(Ljava/lang/String;Ljava/lang/String;)F

    move-result v1

    return v1
.end method

.method public static getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F
    .locals 2

    invoke-static {p0, p1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Landroid/provider/Settings;->-$$Nest$smparseFloatSettingWithDefault(Ljava/lang/String;F)F

    move-result v1

    return v1
.end method

.method public static getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/provider/Settings$SettingNotFoundException;
        }
    .end annotation

    invoke-static {p0, p1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/provider/Settings;->-$$Nest$smparseIntSetting(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method public static getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    .locals 2

    invoke-static {p0, p1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Landroid/provider/Settings;->-$$Nest$smparseIntSettingWithDefault(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getLong(Landroid/content/ContentResolver;Ljava/lang/String;)J
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/provider/Settings$SettingNotFoundException;
        }
    .end annotation

    invoke-static {p0, p1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/provider/Settings;->-$$Nest$smparseLongSetting(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v1

    return-wide v1
.end method

.method public static getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J
    .locals 3

    invoke-static {p0, p1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2, p3}, Landroid/provider/Settings;->-$$Nest$smparseLongSettingWithDefault(Ljava/lang/String;J)J

    move-result-wide v1

    return-wide v1
.end method

.method public static getMovedToSecureSettings(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    sget-object v0, Landroid/provider/Settings$Global;->MOVED_TO_SECURE:Ljava/util/HashSet;

    invoke-interface {p0, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public static getMovedToSystemSettings(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    sget-object v0, Landroid/provider/Settings$Global;->MOVED_TO_SYSTEM:Ljava/util/HashSet;

    invoke-interface {p0, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public static getPublicSettings(Ljava/util/Set;Ljava/util/Set;Landroid/util/ArrayMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    const-class v0, Landroid/provider/Settings$Global;

    invoke-static {v0, p0, p1, p2}, Landroid/provider/Settings;->-$$Nest$smgetPublicSettingsForClass(Ljava/lang/Class;Ljava/util/Set;Ljava/util/Set;Landroid/util/ArrayMap;)V

    return-void
.end method

.method public static getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Landroid/content/ContentResolver;->getUserId()I

    move-result v0

    invoke-static {p0, p1, v0}, Landroid/provider/Settings$Global;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;
    .locals 3

    sget-object v0, Landroid/provider/Settings$Global;->MOVED_TO_SECURE:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "Setting "

    const-string v2, "Settings"

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " has moved from android.provider.Settings.Global to android.provider.Settings.Secure, returning read-only value."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, p1, p2}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Landroid/provider/Settings$Global;->MOVED_TO_SYSTEM:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " has moved from android.provider.Settings.Global to android.provider.Settings.System, returning read-only value."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, p1, p2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    sget-object v0, Landroid/provider/Settings$Global;->sNameValueCache:Landroid/provider/Settings$NameValueCache;

    invoke-virtual {v0, p0, p1, p2}, Landroid/provider/Settings$NameValueCache;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getUriFor(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    sget-object v0, Landroid/provider/Settings$Global;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p0}, Landroid/provider/Settings$Global;->getUriFor(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static isValidZenMode(I)Z
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    return v0

    :pswitch_0
    const/4 v0, 0x1

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z
    .locals 1

    invoke-static {p2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    .locals 1

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z
    .locals 1

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7

    invoke-virtual {p0}, Landroid/content/ContentResolver;->getUserId()I

    move-result v5

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v6}, Landroid/provider/Settings$Global;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIZ)Z

    move-result v0

    return v0
.end method

.method public static putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 7
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    nop

    invoke-virtual {p0}, Landroid/content/ContentResolver;->getUserId()I

    move-result v5

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-static/range {v0 .. v6}, Landroid/provider/Settings$Global;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIZ)Z

    move-result v0

    return v0
.end method

.method public static putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Z
    .locals 7

    nop

    invoke-virtual {p0}, Landroid/content/ContentResolver;->getUserId()I

    move-result v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v6, p5

    invoke-static/range {v0 .. v6}, Landroid/provider/Settings$Global;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIZ)Z

    move-result v0

    return v0
.end method

.method public static putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 7

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p3

    invoke-static/range {v0 .. v6}, Landroid/provider/Settings$Global;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIZ)Z

    move-result v0

    return v0
.end method

.method public static putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIZ)Z
    .locals 9

    sget-object v0, Landroid/provider/Settings$Global;->MOVED_TO_SECURE:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "Setting "

    const-string v2, "Settings"

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " has moved from android.provider.Settings.Global to android.provider.Settings.Secure, value is unchanged."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static/range {p0 .. p6}, Landroid/provider/Settings$Secure;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIZ)Z

    move-result v0

    return v0

    :cond_0
    sget-object v0, Landroid/provider/Settings$Global;->MOVED_TO_SYSTEM:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " has moved from android.provider.Settings.Global to android.provider.Settings.System, value is unchanged."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static/range {p0 .. p6}, Landroid/provider/Settings$System;->-$$Nest$smputStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIZ)Z

    move-result v0

    return v0

    :cond_1
    sget-object v1, Landroid/provider/Settings$Global;->sNameValueCache:Landroid/provider/Settings$NameValueCache;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move v7, p5

    move v8, p6

    invoke-virtual/range {v1 .. v8}, Landroid/provider/Settings$NameValueCache;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIZ)Z

    move-result v0

    return v0
.end method

.method public static resetToDefaults(Landroid/content/ContentResolver;Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    nop

    invoke-virtual {p0}, Landroid/content/ContentResolver;->getUserId()I

    move-result v0

    const/4 v1, 0x1

    invoke-static {p0, p1, v1, v0}, Landroid/provider/Settings$Global;->resetToDefaultsAsUser(Landroid/content/ContentResolver;Ljava/lang/String;II)V

    return-void
.end method

.method public static resetToDefaultsAsUser(Landroid/content/ContentResolver;Ljava/lang/String;II)V
    .locals 7

    :try_start_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "_user"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    if-eqz p1, :cond_0

    const-string v1, "_tag"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v1, "_reset_mode"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    sget-object v2, Landroid/provider/Settings$Global;->sProviderHolder:Landroid/provider/Settings$ContentProviderHolder;

    invoke-virtual {v2, p0}, Landroid/provider/Settings$ContentProviderHolder;->getProvider(Landroid/content/ContentResolver;)Landroid/content/IContentProvider;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/ContentResolver;->getAttributionSource()Landroid/content/AttributionSource;

    move-result-object v3

    invoke-static {v2}, Landroid/provider/Settings$ContentProviderHolder;->-$$Nest$fgetmUri(Landroid/provider/Settings$ContentProviderHolder;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v4

    const-string v5, "RESET_global"

    const/4 v6, 0x0

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v0

    invoke-interface/range {v1 .. v6}, Landroid/content/IContentProvider;->call(Landroid/content/AttributionSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    nop

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t reset do defaults for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Landroid/provider/Settings$Global;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Settings"

    invoke-static {v2, v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method public static zenModeToString(I)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    const-string v0, "ZEN_MODE_IMPORTANT_INTERRUPTIONS"

    return-object v0

    :cond_0
    const/4 v0, 0x3

    if-ne p0, v0, :cond_1

    const-string v0, "ZEN_MODE_ALARMS"

    return-object v0

    :cond_1
    const/4 v0, 0x2

    if-ne p0, v0, :cond_2

    const-string v0, "ZEN_MODE_NO_INTERRUPTIONS"

    return-object v0

    :cond_2
    const-string v0, "ZEN_MODE_OFF"

    return-object v0
.end method
