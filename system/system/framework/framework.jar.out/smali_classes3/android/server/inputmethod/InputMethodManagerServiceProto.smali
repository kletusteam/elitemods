.class public final Landroid/server/inputmethod/InputMethodManagerServiceProto;
.super Ljava/lang/Object;


# static fields
.field public static final ACCESSIBILITY_REQUESTING_NO_SOFT_KEYBOARD:J = 0x10800000018L

.field public static final BACK_DISPOSITION:J = 0x10500000015L

.field public static final BOUND_TO_METHOD:J = 0x10800000013L

.field public static final CUR_ATTRIBUTE:J = 0x10b00000007L

.field public static final CUR_CLIENT:J = 0x10900000003L

.field public static final CUR_FOCUSED_WINDOW_NAME:J = 0x10900000004L

.field public static final CUR_FOCUSED_WINDOW_SOFT_INPUT_MODE:J = 0x10900000006L

.field public static final CUR_ID:J = 0x10900000008L

.field public static final CUR_METHOD_ID:J = 0x10900000001L

.field public static final CUR_SEQ:J = 0x10500000002L

.field public static final CUR_TOKEN:J = 0x1090000000eL

.field public static final CUR_TOKEN_DISPLAY_ID:J = 0x1050000000fL

.field public static final HAVE_CONNECTION:J = 0x10800000012L

.field public static final IME_WINDOW_VISIBILITY:J = 0x10500000016L

.field public static final INPUT_SHOWN:J = 0x1080000000cL

.field public static final IN_FULLSCREEN_MODE:J = 0x1080000000dL

.field public static final IS_INTERACTIVE:J = 0x10800000014L

.field public static final LAST_IME_TARGET_WINDOW_NAME:J = 0x10900000005L

.field public static final LAST_SWITCH_USER_ID:J = 0x10500000011L

.field public static final SHOW_EXPLICITLY_REQUESTED:J = 0x1080000000aL

.field public static final SHOW_FORCED:J = 0x1080000000bL

.field public static final SHOW_IME_WITH_HARD_KEYBOARD:J = 0x10800000017L

.field public static final SHOW_REQUESTED:J = 0x10800000009L

.field public static final SYSTEM_READY:J = 0x10800000010L


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
