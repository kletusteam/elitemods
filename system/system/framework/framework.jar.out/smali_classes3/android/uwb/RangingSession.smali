.class public final Landroid/uwb/RangingSession;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/AutoCloseable;


# annotations
.annotation runtime Landroid/annotation/SystemApi;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/uwb/RangingSession$Callback;,
        Landroid/uwb/RangingSession$State;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Uwb.RangingSession"


# instance fields
.field private final mAdapter:Landroid/uwb/IUwbAdapter;

.field private final mCallback:Landroid/uwb/RangingSession$Callback;

.field private final mChipId:Ljava/lang/String;

.field private final mExecutor:Ljava/util/concurrent/Executor;

.field private final mSessionHandle:Landroid/uwb/SessionHandle;

.field private mState:Landroid/uwb/RangingSession$State;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Landroid/uwb/RangingSession$Callback;Landroid/uwb/IUwbAdapter;Landroid/uwb/SessionHandle;)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Landroid/uwb/RangingSession;-><init>(Ljava/util/concurrent/Executor;Landroid/uwb/RangingSession$Callback;Landroid/uwb/IUwbAdapter;Landroid/uwb/SessionHandle;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Landroid/uwb/RangingSession$Callback;Landroid/uwb/IUwbAdapter;Landroid/uwb/SessionHandle;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Landroid/uwb/RangingSession$State;->INIT:Landroid/uwb/RangingSession$State;

    iput-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    sget-object v0, Landroid/uwb/RangingSession$State;->INIT:Landroid/uwb/RangingSession$State;

    iput-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    iput-object p1, p0, Landroid/uwb/RangingSession;->mExecutor:Ljava/util/concurrent/Executor;

    iput-object p2, p0, Landroid/uwb/RangingSession;->mCallback:Landroid/uwb/RangingSession$Callback;

    iput-object p3, p0, Landroid/uwb/RangingSession;->mAdapter:Landroid/uwb/IUwbAdapter;

    iput-object p4, p0, Landroid/uwb/RangingSession;->mSessionHandle:Landroid/uwb/SessionHandle;

    iput-object p5, p0, Landroid/uwb/RangingSession;->mChipId:Ljava/lang/String;

    return-void
.end method

.method private executeCallback(Ljava/lang/Runnable;)V
    .locals 3

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    :try_start_0
    iget-object v2, p0, Landroid/uwb/RangingSession;->mExecutor:Ljava/util/concurrent/Executor;

    invoke-interface {v2, p1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    nop

    return-void

    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v2
.end method


# virtual methods
.method public addControlee(Landroid/os/PersistableBundle;)V
    .locals 2

    iget-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    sget-object v1, Landroid/uwb/RangingSession$State;->ACTIVE:Landroid/uwb/RangingSession$State;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    sget-object v1, Landroid/uwb/RangingSession$State;->IDLE:Landroid/uwb/RangingSession$State;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    :goto_0
    :try_start_0
    iget-object v0, p0, Landroid/uwb/RangingSession;->mAdapter:Landroid/uwb/IUwbAdapter;

    iget-object v1, p0, Landroid/uwb/RangingSession;->mSessionHandle:Landroid/uwb/SessionHandle;

    invoke-interface {v0, v1, p1}, Landroid/uwb/IUwbAdapter;->addControlee(Landroid/uwb/SessionHandle;Landroid/os/PersistableBundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    nop

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1
.end method

.method public close()V
    .locals 2

    iget-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    sget-object v1, Landroid/uwb/RangingSession$State;->CLOSED:Landroid/uwb/RangingSession$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Landroid/uwb/RangingSession;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Landroid/uwb/RangingSession$$ExternalSyntheticLambda7;

    invoke-direct {v1, p0}, Landroid/uwb/RangingSession$$ExternalSyntheticLambda7;-><init>(Landroid/uwb/RangingSession;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Landroid/uwb/RangingSession;->mAdapter:Landroid/uwb/IUwbAdapter;

    iget-object v1, p0, Landroid/uwb/RangingSession;->mSessionHandle:Landroid/uwb/SessionHandle;

    invoke-interface {v0, v1}, Landroid/uwb/IUwbAdapter;->closeRanging(Landroid/uwb/SessionHandle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    nop

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1
.end method

.method public isOpen()Z
    .locals 2

    iget-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    sget-object v1, Landroid/uwb/RangingSession$State;->IDLE:Landroid/uwb/RangingSession$State;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    sget-object v1, Landroid/uwb/RangingSession$State;->ACTIVE:Landroid/uwb/RangingSession$State;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method synthetic lambda$close$0$android-uwb-RangingSession()V
    .locals 3

    goto/32 :goto_4

    nop

    :goto_0
    return-void

    :goto_1
    invoke-interface {v0, v2, v1}, Landroid/uwb/RangingSession$Callback;->onClosed(ILandroid/os/PersistableBundle;)V

    goto/32 :goto_0

    nop

    :goto_2
    new-instance v1, Landroid/os/PersistableBundle;

    goto/32 :goto_5

    nop

    :goto_3
    const/4 v2, 0x1

    goto/32 :goto_1

    nop

    :goto_4
    iget-object v0, p0, Landroid/uwb/RangingSession;->mCallback:Landroid/uwb/RangingSession$Callback;

    goto/32 :goto_2

    nop

    :goto_5
    invoke-direct {v1}, Landroid/os/PersistableBundle;-><init>()V

    goto/32 :goto_3

    nop
.end method

.method synthetic lambda$onControleeAddFailed$12$android-uwb-RangingSession(ILandroid/os/PersistableBundle;)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/uwb/RangingSession;->mCallback:Landroid/uwb/RangingSession$Callback;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-interface {v0, p1, p2}, Landroid/uwb/RangingSession$Callback;->onControleeAddFailed(ILandroid/os/PersistableBundle;)V

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method

.method synthetic lambda$onControleeAdded$11$android-uwb-RangingSession(Landroid/os/PersistableBundle;)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v0, p0, Landroid/uwb/RangingSession;->mCallback:Landroid/uwb/RangingSession$Callback;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-interface {v0, p1}, Landroid/uwb/RangingSession$Callback;->onControleeAdded(Landroid/os/PersistableBundle;)V

    goto/32 :goto_0

    nop
.end method

.method synthetic lambda$onControleeRemoveFailed$14$android-uwb-RangingSession(ILandroid/os/PersistableBundle;)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/uwb/RangingSession;->mCallback:Landroid/uwb/RangingSession$Callback;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-interface {v0, p1, p2}, Landroid/uwb/RangingSession$Callback;->onControleeRemoveFailed(ILandroid/os/PersistableBundle;)V

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method

.method synthetic lambda$onControleeRemoved$13$android-uwb-RangingSession(Landroid/os/PersistableBundle;)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-interface {v0, p1}, Landroid/uwb/RangingSession$Callback;->onControleeRemoved(Landroid/os/PersistableBundle;)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    iget-object v0, p0, Landroid/uwb/RangingSession;->mCallback:Landroid/uwb/RangingSession$Callback;

    goto/32 :goto_0

    nop
.end method

.method synthetic lambda$onDataReceiveFailed$22$android-uwb-RangingSession(Landroid/uwb/UwbAddress;ILandroid/os/PersistableBundle;)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    invoke-interface {v0, p1, p2, p3}, Landroid/uwb/RangingSession$Callback;->onDataReceiveFailed(Landroid/uwb/UwbAddress;ILandroid/os/PersistableBundle;)V

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, Landroid/uwb/RangingSession;->mCallback:Landroid/uwb/RangingSession$Callback;

    goto/32 :goto_1

    nop
.end method

.method synthetic lambda$onDataReceived$21$android-uwb-RangingSession(Landroid/uwb/UwbAddress;Landroid/os/PersistableBundle;[B)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/uwb/RangingSession;->mCallback:Landroid/uwb/RangingSession$Callback;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-interface {v0, p1, p2, p3}, Landroid/uwb/RangingSession$Callback;->onDataReceived(Landroid/uwb/UwbAddress;Landroid/os/PersistableBundle;[B)V

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method

.method synthetic lambda$onDataSendFailed$20$android-uwb-RangingSession(Landroid/uwb/UwbAddress;ILandroid/os/PersistableBundle;)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    invoke-interface {v0, p1, p2, p3}, Landroid/uwb/RangingSession$Callback;->onDataSendFailed(Landroid/uwb/UwbAddress;ILandroid/os/PersistableBundle;)V

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, Landroid/uwb/RangingSession;->mCallback:Landroid/uwb/RangingSession$Callback;

    goto/32 :goto_1

    nop
.end method

.method synthetic lambda$onDataSent$19$android-uwb-RangingSession(Landroid/uwb/UwbAddress;Landroid/os/PersistableBundle;)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    invoke-interface {v0, p1, p2}, Landroid/uwb/RangingSession$Callback;->onDataSent(Landroid/uwb/UwbAddress;Landroid/os/PersistableBundle;)V

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, Landroid/uwb/RangingSession;->mCallback:Landroid/uwb/RangingSession$Callback;

    goto/32 :goto_1

    nop
.end method

.method synthetic lambda$onRangingClosed$9$android-uwb-RangingSession(ILandroid/os/PersistableBundle;)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-interface {v0, p1, p2}, Landroid/uwb/RangingSession$Callback;->onClosed(ILandroid/os/PersistableBundle;)V

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Landroid/uwb/RangingSession;->mCallback:Landroid/uwb/RangingSession$Callback;

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method synthetic lambda$onRangingOpenFailed$2$android-uwb-RangingSession(ILandroid/os/PersistableBundle;)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/uwb/RangingSession;->mCallback:Landroid/uwb/RangingSession$Callback;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-interface {v0, p1, p2}, Landroid/uwb/RangingSession$Callback;->onOpenFailed(ILandroid/os/PersistableBundle;)V

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method

.method synthetic lambda$onRangingOpened$1$android-uwb-RangingSession()V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    invoke-interface {v0, p0}, Landroid/uwb/RangingSession$Callback;->onOpened(Landroid/uwb/RangingSession;)V

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, Landroid/uwb/RangingSession;->mCallback:Landroid/uwb/RangingSession$Callback;

    goto/32 :goto_1

    nop
.end method

.method synthetic lambda$onRangingPauseFailed$16$android-uwb-RangingSession(ILandroid/os/PersistableBundle;)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    invoke-interface {v0, p1, p2}, Landroid/uwb/RangingSession$Callback;->onPauseFailed(ILandroid/os/PersistableBundle;)V

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, Landroid/uwb/RangingSession;->mCallback:Landroid/uwb/RangingSession$Callback;

    goto/32 :goto_1

    nop
.end method

.method synthetic lambda$onRangingPaused$15$android-uwb-RangingSession(Landroid/os/PersistableBundle;)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/uwb/RangingSession;->mCallback:Landroid/uwb/RangingSession$Callback;

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    invoke-interface {v0, p1}, Landroid/uwb/RangingSession$Callback;->onPaused(Landroid/os/PersistableBundle;)V

    goto/32 :goto_1

    nop
.end method

.method synthetic lambda$onRangingReconfigureFailed$6$android-uwb-RangingSession(ILandroid/os/PersistableBundle;)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-interface {v0, p1, p2}, Landroid/uwb/RangingSession$Callback;->onReconfigureFailed(ILandroid/os/PersistableBundle;)V

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Landroid/uwb/RangingSession;->mCallback:Landroid/uwb/RangingSession$Callback;

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method synthetic lambda$onRangingReconfigured$5$android-uwb-RangingSession(Landroid/os/PersistableBundle;)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/uwb/RangingSession;->mCallback:Landroid/uwb/RangingSession$Callback;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-interface {v0, p1}, Landroid/uwb/RangingSession$Callback;->onReconfigured(Landroid/os/PersistableBundle;)V

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method

.method synthetic lambda$onRangingResult$10$android-uwb-RangingSession(Landroid/uwb/RangingReport;)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    invoke-interface {v0, p1}, Landroid/uwb/RangingSession$Callback;->onReportReceived(Landroid/uwb/RangingReport;)V

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, Landroid/uwb/RangingSession;->mCallback:Landroid/uwb/RangingSession$Callback;

    goto/32 :goto_1

    nop
.end method

.method synthetic lambda$onRangingResumeFailed$18$android-uwb-RangingSession(ILandroid/os/PersistableBundle;)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v0, p0, Landroid/uwb/RangingSession;->mCallback:Landroid/uwb/RangingSession$Callback;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-interface {v0, p1, p2}, Landroid/uwb/RangingSession$Callback;->onResumeFailed(ILandroid/os/PersistableBundle;)V

    goto/32 :goto_0

    nop
.end method

.method synthetic lambda$onRangingResumed$17$android-uwb-RangingSession(Landroid/os/PersistableBundle;)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/uwb/RangingSession;->mCallback:Landroid/uwb/RangingSession$Callback;

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    invoke-interface {v0, p1}, Landroid/uwb/RangingSession$Callback;->onResumed(Landroid/os/PersistableBundle;)V

    goto/32 :goto_1

    nop
.end method

.method synthetic lambda$onRangingStartFailed$4$android-uwb-RangingSession(ILandroid/os/PersistableBundle;)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    invoke-interface {v0, p1, p2}, Landroid/uwb/RangingSession$Callback;->onStartFailed(ILandroid/os/PersistableBundle;)V

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, Landroid/uwb/RangingSession;->mCallback:Landroid/uwb/RangingSession$Callback;

    goto/32 :goto_1

    nop
.end method

.method synthetic lambda$onRangingStarted$3$android-uwb-RangingSession(Landroid/os/PersistableBundle;)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/uwb/RangingSession;->mCallback:Landroid/uwb/RangingSession$Callback;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-interface {v0, p1}, Landroid/uwb/RangingSession$Callback;->onStarted(Landroid/os/PersistableBundle;)V

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method

.method synthetic lambda$onRangingStopFailed$8$android-uwb-RangingSession(ILandroid/os/PersistableBundle;)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-interface {v0, p1, p2}, Landroid/uwb/RangingSession$Callback;->onStopFailed(ILandroid/os/PersistableBundle;)V

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Landroid/uwb/RangingSession;->mCallback:Landroid/uwb/RangingSession$Callback;

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method synthetic lambda$onRangingStopped$7$android-uwb-RangingSession(ILandroid/os/PersistableBundle;)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    invoke-interface {v0, p1, p2}, Landroid/uwb/RangingSession$Callback;->onStopped(ILandroid/os/PersistableBundle;)V

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, Landroid/uwb/RangingSession;->mCallback:Landroid/uwb/RangingSession$Callback;

    goto/32 :goto_1

    nop
.end method

.method synthetic lambda$onServiceConnected$24$android-uwb-RangingSession(Landroid/os/PersistableBundle;)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-interface {v0, p1}, Landroid/uwb/RangingSession$Callback;->onServiceConnected(Landroid/os/PersistableBundle;)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    iget-object v0, p0, Landroid/uwb/RangingSession;->mCallback:Landroid/uwb/RangingSession$Callback;

    goto/32 :goto_0

    nop
.end method

.method synthetic lambda$onServiceDiscovered$23$android-uwb-RangingSession(Landroid/os/PersistableBundle;)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v0, p0, Landroid/uwb/RangingSession;->mCallback:Landroid/uwb/RangingSession$Callback;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-interface {v0, p1}, Landroid/uwb/RangingSession$Callback;->onServiceDiscovered(Landroid/os/PersistableBundle;)V

    goto/32 :goto_0

    nop
.end method

.method public onControleeAddFailed(ILandroid/os/PersistableBundle;)V
    .locals 2

    invoke-virtual {p0}, Landroid/uwb/RangingSession;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Uwb.RangingSession"

    const-string/jumbo v1, "onControleeAddFailed invoked for non-open session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v0, Landroid/uwb/RangingSession$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0, p1, p2}, Landroid/uwb/RangingSession$$ExternalSyntheticLambda2;-><init>(Landroid/uwb/RangingSession;ILandroid/os/PersistableBundle;)V

    invoke-direct {p0, v0}, Landroid/uwb/RangingSession;->executeCallback(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onControleeAdded(Landroid/os/PersistableBundle;)V
    .locals 2

    invoke-virtual {p0}, Landroid/uwb/RangingSession;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Uwb.RangingSession"

    const-string/jumbo v1, "onControleeAdded invoked for non-open session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v0, Landroid/uwb/RangingSession$$ExternalSyntheticLambda5;

    invoke-direct {v0, p0, p1}, Landroid/uwb/RangingSession$$ExternalSyntheticLambda5;-><init>(Landroid/uwb/RangingSession;Landroid/os/PersistableBundle;)V

    invoke-direct {p0, v0}, Landroid/uwb/RangingSession;->executeCallback(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onControleeRemoveFailed(ILandroid/os/PersistableBundle;)V
    .locals 2

    invoke-virtual {p0}, Landroid/uwb/RangingSession;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Uwb.RangingSession"

    const-string/jumbo v1, "onControleeRemoveFailed invoked for non-open session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v0, Landroid/uwb/RangingSession$$ExternalSyntheticLambda11;

    invoke-direct {v0, p0, p1, p2}, Landroid/uwb/RangingSession$$ExternalSyntheticLambda11;-><init>(Landroid/uwb/RangingSession;ILandroid/os/PersistableBundle;)V

    invoke-direct {p0, v0}, Landroid/uwb/RangingSession;->executeCallback(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onControleeRemoved(Landroid/os/PersistableBundle;)V
    .locals 2

    invoke-virtual {p0}, Landroid/uwb/RangingSession;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Uwb.RangingSession"

    const-string/jumbo v1, "onControleeRemoved invoked for non-open session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v0, Landroid/uwb/RangingSession$$ExternalSyntheticLambda3;

    invoke-direct {v0, p0, p1}, Landroid/uwb/RangingSession$$ExternalSyntheticLambda3;-><init>(Landroid/uwb/RangingSession;Landroid/os/PersistableBundle;)V

    invoke-direct {p0, v0}, Landroid/uwb/RangingSession;->executeCallback(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onDataReceiveFailed(Landroid/uwb/UwbAddress;ILandroid/os/PersistableBundle;)V
    .locals 2

    invoke-virtual {p0}, Landroid/uwb/RangingSession;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Uwb.RangingSession"

    const-string/jumbo v1, "onDataReceiveFailed invoked for non-open session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v0, Landroid/uwb/RangingSession$$ExternalSyntheticLambda16;

    invoke-direct {v0, p0, p1, p2, p3}, Landroid/uwb/RangingSession$$ExternalSyntheticLambda16;-><init>(Landroid/uwb/RangingSession;Landroid/uwb/UwbAddress;ILandroid/os/PersistableBundle;)V

    invoke-direct {p0, v0}, Landroid/uwb/RangingSession;->executeCallback(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onDataReceived(Landroid/uwb/UwbAddress;Landroid/os/PersistableBundle;[B)V
    .locals 2

    invoke-virtual {p0}, Landroid/uwb/RangingSession;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Uwb.RangingSession"

    const-string/jumbo v1, "onDataReceived invoked for non-open session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v0, Landroid/uwb/RangingSession$$ExternalSyntheticLambda24;

    invoke-direct {v0, p0, p1, p2, p3}, Landroid/uwb/RangingSession$$ExternalSyntheticLambda24;-><init>(Landroid/uwb/RangingSession;Landroid/uwb/UwbAddress;Landroid/os/PersistableBundle;[B)V

    invoke-direct {p0, v0}, Landroid/uwb/RangingSession;->executeCallback(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onDataSendFailed(Landroid/uwb/UwbAddress;ILandroid/os/PersistableBundle;)V
    .locals 2

    invoke-virtual {p0}, Landroid/uwb/RangingSession;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Uwb.RangingSession"

    const-string/jumbo v1, "onDataSendFailed invoked for non-open session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v0, Landroid/uwb/RangingSession$$ExternalSyntheticLambda6;

    invoke-direct {v0, p0, p1, p2, p3}, Landroid/uwb/RangingSession$$ExternalSyntheticLambda6;-><init>(Landroid/uwb/RangingSession;Landroid/uwb/UwbAddress;ILandroid/os/PersistableBundle;)V

    invoke-direct {p0, v0}, Landroid/uwb/RangingSession;->executeCallback(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onDataSent(Landroid/uwb/UwbAddress;Landroid/os/PersistableBundle;)V
    .locals 2

    invoke-virtual {p0}, Landroid/uwb/RangingSession;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Uwb.RangingSession"

    const-string/jumbo v1, "onDataSent invoked for non-open session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v0, Landroid/uwb/RangingSession$$ExternalSyntheticLambda9;

    invoke-direct {v0, p0, p1, p2}, Landroid/uwb/RangingSession$$ExternalSyntheticLambda9;-><init>(Landroid/uwb/RangingSession;Landroid/uwb/UwbAddress;Landroid/os/PersistableBundle;)V

    invoke-direct {p0, v0}, Landroid/uwb/RangingSession;->executeCallback(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onRangingClosed(ILandroid/os/PersistableBundle;)V
    .locals 1

    sget-object v0, Landroid/uwb/RangingSession$State;->CLOSED:Landroid/uwb/RangingSession$State;

    iput-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    new-instance v0, Landroid/uwb/RangingSession$$ExternalSyntheticLambda20;

    invoke-direct {v0, p0, p1, p2}, Landroid/uwb/RangingSession$$ExternalSyntheticLambda20;-><init>(Landroid/uwb/RangingSession;ILandroid/os/PersistableBundle;)V

    invoke-direct {p0, v0}, Landroid/uwb/RangingSession;->executeCallback(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onRangingOpenFailed(ILandroid/os/PersistableBundle;)V
    .locals 2

    iget-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    sget-object v1, Landroid/uwb/RangingSession$State;->CLOSED:Landroid/uwb/RangingSession$State;

    if-ne v0, v1, :cond_0

    const-string v0, "Uwb.RangingSession"

    const-string/jumbo v1, "onRangingOpenFailed invoked for a closed session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    sget-object v0, Landroid/uwb/RangingSession$State;->CLOSED:Landroid/uwb/RangingSession$State;

    iput-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    new-instance v0, Landroid/uwb/RangingSession$$ExternalSyntheticLambda8;

    invoke-direct {v0, p0, p1, p2}, Landroid/uwb/RangingSession$$ExternalSyntheticLambda8;-><init>(Landroid/uwb/RangingSession;ILandroid/os/PersistableBundle;)V

    invoke-direct {p0, v0}, Landroid/uwb/RangingSession;->executeCallback(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onRangingOpened()V
    .locals 2

    iget-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    sget-object v1, Landroid/uwb/RangingSession$State;->CLOSED:Landroid/uwb/RangingSession$State;

    if-ne v0, v1, :cond_0

    const-string v0, "Uwb.RangingSession"

    const-string/jumbo v1, "onRangingOpened invoked for a closed session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    sget-object v0, Landroid/uwb/RangingSession$State;->IDLE:Landroid/uwb/RangingSession$State;

    iput-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    new-instance v0, Landroid/uwb/RangingSession$$ExternalSyntheticLambda17;

    invoke-direct {v0, p0}, Landroid/uwb/RangingSession$$ExternalSyntheticLambda17;-><init>(Landroid/uwb/RangingSession;)V

    invoke-direct {p0, v0}, Landroid/uwb/RangingSession;->executeCallback(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onRangingPauseFailed(ILandroid/os/PersistableBundle;)V
    .locals 2

    invoke-virtual {p0}, Landroid/uwb/RangingSession;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Uwb.RangingSession"

    const-string/jumbo v1, "onRangingPauseFailed invoked for non-open session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v0, Landroid/uwb/RangingSession$$ExternalSyntheticLambda19;

    invoke-direct {v0, p0, p1, p2}, Landroid/uwb/RangingSession$$ExternalSyntheticLambda19;-><init>(Landroid/uwb/RangingSession;ILandroid/os/PersistableBundle;)V

    invoke-direct {p0, v0}, Landroid/uwb/RangingSession;->executeCallback(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onRangingPaused(Landroid/os/PersistableBundle;)V
    .locals 2

    invoke-virtual {p0}, Landroid/uwb/RangingSession;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Uwb.RangingSession"

    const-string/jumbo v1, "onRangingPaused invoked for non-open session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v0, Landroid/uwb/RangingSession$$ExternalSyntheticLambda12;

    invoke-direct {v0, p0, p1}, Landroid/uwb/RangingSession$$ExternalSyntheticLambda12;-><init>(Landroid/uwb/RangingSession;Landroid/os/PersistableBundle;)V

    invoke-direct {p0, v0}, Landroid/uwb/RangingSession;->executeCallback(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onRangingReconfigureFailed(ILandroid/os/PersistableBundle;)V
    .locals 2

    iget-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    sget-object v1, Landroid/uwb/RangingSession$State;->CLOSED:Landroid/uwb/RangingSession$State;

    if-ne v0, v1, :cond_0

    const-string v0, "Uwb.RangingSession"

    const-string/jumbo v1, "onRangingReconfigureFailed invoked for a closed session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v0, Landroid/uwb/RangingSession$$ExternalSyntheticLambda14;

    invoke-direct {v0, p0, p1, p2}, Landroid/uwb/RangingSession$$ExternalSyntheticLambda14;-><init>(Landroid/uwb/RangingSession;ILandroid/os/PersistableBundle;)V

    invoke-direct {p0, v0}, Landroid/uwb/RangingSession;->executeCallback(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onRangingReconfigured(Landroid/os/PersistableBundle;)V
    .locals 2

    iget-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    sget-object v1, Landroid/uwb/RangingSession$State;->CLOSED:Landroid/uwb/RangingSession$State;

    if-ne v0, v1, :cond_0

    const-string v0, "Uwb.RangingSession"

    const-string/jumbo v1, "onRangingReconfigured invoked for a closed session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v0, Landroid/uwb/RangingSession$$ExternalSyntheticLambda21;

    invoke-direct {v0, p0, p1}, Landroid/uwb/RangingSession$$ExternalSyntheticLambda21;-><init>(Landroid/uwb/RangingSession;Landroid/os/PersistableBundle;)V

    invoke-direct {p0, v0}, Landroid/uwb/RangingSession;->executeCallback(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onRangingResult(Landroid/uwb/RangingReport;)V
    .locals 2

    invoke-virtual {p0}, Landroid/uwb/RangingSession;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Uwb.RangingSession"

    const-string/jumbo v1, "onRangingResult invoked for non-open session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v0, Landroid/uwb/RangingSession$$ExternalSyntheticLambda18;

    invoke-direct {v0, p0, p1}, Landroid/uwb/RangingSession$$ExternalSyntheticLambda18;-><init>(Landroid/uwb/RangingSession;Landroid/uwb/RangingReport;)V

    invoke-direct {p0, v0}, Landroid/uwb/RangingSession;->executeCallback(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onRangingResumeFailed(ILandroid/os/PersistableBundle;)V
    .locals 2

    invoke-virtual {p0}, Landroid/uwb/RangingSession;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Uwb.RangingSession"

    const-string/jumbo v1, "onRangingResumeFailed invoked for non-open session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v0, Landroid/uwb/RangingSession$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0, p1, p2}, Landroid/uwb/RangingSession$$ExternalSyntheticLambda1;-><init>(Landroid/uwb/RangingSession;ILandroid/os/PersistableBundle;)V

    invoke-direct {p0, v0}, Landroid/uwb/RangingSession;->executeCallback(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onRangingResumed(Landroid/os/PersistableBundle;)V
    .locals 2

    invoke-virtual {p0}, Landroid/uwb/RangingSession;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Uwb.RangingSession"

    const-string/jumbo v1, "onRangingResumed invoked for non-open session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v0, Landroid/uwb/RangingSession$$ExternalSyntheticLambda22;

    invoke-direct {v0, p0, p1}, Landroid/uwb/RangingSession$$ExternalSyntheticLambda22;-><init>(Landroid/uwb/RangingSession;Landroid/os/PersistableBundle;)V

    invoke-direct {p0, v0}, Landroid/uwb/RangingSession;->executeCallback(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onRangingStartFailed(ILandroid/os/PersistableBundle;)V
    .locals 2

    iget-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    sget-object v1, Landroid/uwb/RangingSession$State;->CLOSED:Landroid/uwb/RangingSession$State;

    if-ne v0, v1, :cond_0

    const-string v0, "Uwb.RangingSession"

    const-string/jumbo v1, "onRangingStartFailed invoked for a closed session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v0, Landroid/uwb/RangingSession$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1, p2}, Landroid/uwb/RangingSession$$ExternalSyntheticLambda0;-><init>(Landroid/uwb/RangingSession;ILandroid/os/PersistableBundle;)V

    invoke-direct {p0, v0}, Landroid/uwb/RangingSession;->executeCallback(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onRangingStarted(Landroid/os/PersistableBundle;)V
    .locals 2

    iget-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    sget-object v1, Landroid/uwb/RangingSession$State;->CLOSED:Landroid/uwb/RangingSession$State;

    if-ne v0, v1, :cond_0

    const-string v0, "Uwb.RangingSession"

    const-string/jumbo v1, "onRangingStarted invoked for a closed session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    sget-object v0, Landroid/uwb/RangingSession$State;->ACTIVE:Landroid/uwb/RangingSession$State;

    iput-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    new-instance v0, Landroid/uwb/RangingSession$$ExternalSyntheticLambda15;

    invoke-direct {v0, p0, p1}, Landroid/uwb/RangingSession$$ExternalSyntheticLambda15;-><init>(Landroid/uwb/RangingSession;Landroid/os/PersistableBundle;)V

    invoke-direct {p0, v0}, Landroid/uwb/RangingSession;->executeCallback(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onRangingStopFailed(ILandroid/os/PersistableBundle;)V
    .locals 2

    iget-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    sget-object v1, Landroid/uwb/RangingSession$State;->CLOSED:Landroid/uwb/RangingSession$State;

    if-ne v0, v1, :cond_0

    const-string v0, "Uwb.RangingSession"

    const-string/jumbo v1, "onRangingStopFailed invoked for a closed session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v0, Landroid/uwb/RangingSession$$ExternalSyntheticLambda23;

    invoke-direct {v0, p0, p1, p2}, Landroid/uwb/RangingSession$$ExternalSyntheticLambda23;-><init>(Landroid/uwb/RangingSession;ILandroid/os/PersistableBundle;)V

    invoke-direct {p0, v0}, Landroid/uwb/RangingSession;->executeCallback(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onRangingStopped(ILandroid/os/PersistableBundle;)V
    .locals 2

    iget-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    sget-object v1, Landroid/uwb/RangingSession$State;->CLOSED:Landroid/uwb/RangingSession$State;

    if-ne v0, v1, :cond_0

    const-string v0, "Uwb.RangingSession"

    const-string/jumbo v1, "onRangingStopped invoked for a closed session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    sget-object v0, Landroid/uwb/RangingSession$State;->IDLE:Landroid/uwb/RangingSession$State;

    iput-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    new-instance v0, Landroid/uwb/RangingSession$$ExternalSyntheticLambda10;

    invoke-direct {v0, p0, p1, p2}, Landroid/uwb/RangingSession$$ExternalSyntheticLambda10;-><init>(Landroid/uwb/RangingSession;ILandroid/os/PersistableBundle;)V

    invoke-direct {p0, v0}, Landroid/uwb/RangingSession;->executeCallback(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onServiceConnected(Landroid/os/PersistableBundle;)V
    .locals 2

    invoke-virtual {p0}, Landroid/uwb/RangingSession;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Uwb.RangingSession"

    const-string/jumbo v1, "onServiceConnected invoked for non-open session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v0, Landroid/uwb/RangingSession$$ExternalSyntheticLambda4;

    invoke-direct {v0, p0, p1}, Landroid/uwb/RangingSession$$ExternalSyntheticLambda4;-><init>(Landroid/uwb/RangingSession;Landroid/os/PersistableBundle;)V

    invoke-direct {p0, v0}, Landroid/uwb/RangingSession;->executeCallback(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onServiceDiscovered(Landroid/os/PersistableBundle;)V
    .locals 2

    invoke-virtual {p0}, Landroid/uwb/RangingSession;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Uwb.RangingSession"

    const-string/jumbo v1, "onServiceDiscovered invoked for non-open session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v0, Landroid/uwb/RangingSession$$ExternalSyntheticLambda13;

    invoke-direct {v0, p0, p1}, Landroid/uwb/RangingSession$$ExternalSyntheticLambda13;-><init>(Landroid/uwb/RangingSession;Landroid/os/PersistableBundle;)V

    invoke-direct {p0, v0}, Landroid/uwb/RangingSession;->executeCallback(Ljava/lang/Runnable;)V

    return-void
.end method

.method public pause(Landroid/os/PersistableBundle;)V
    .locals 2

    iget-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    sget-object v1, Landroid/uwb/RangingSession$State;->ACTIVE:Landroid/uwb/RangingSession$State;

    if-ne v0, v1, :cond_0

    :try_start_0
    iget-object v0, p0, Landroid/uwb/RangingSession;->mAdapter:Landroid/uwb/IUwbAdapter;

    iget-object v1, p0, Landroid/uwb/RangingSession;->mSessionHandle:Landroid/uwb/SessionHandle;

    invoke-interface {v0, v1, p1}, Landroid/uwb/IUwbAdapter;->pause(Landroid/uwb/SessionHandle;Landroid/os/PersistableBundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    nop

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public reconfigure(Landroid/os/PersistableBundle;)V
    .locals 2

    iget-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    sget-object v1, Landroid/uwb/RangingSession$State;->ACTIVE:Landroid/uwb/RangingSession$State;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    sget-object v1, Landroid/uwb/RangingSession$State;->IDLE:Landroid/uwb/RangingSession$State;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    :goto_0
    :try_start_0
    iget-object v0, p0, Landroid/uwb/RangingSession;->mAdapter:Landroid/uwb/IUwbAdapter;

    iget-object v1, p0, Landroid/uwb/RangingSession;->mSessionHandle:Landroid/uwb/SessionHandle;

    invoke-interface {v0, v1, p1}, Landroid/uwb/IUwbAdapter;->reconfigureRanging(Landroid/uwb/SessionHandle;Landroid/os/PersistableBundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    nop

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1
.end method

.method public removeControlee(Landroid/os/PersistableBundle;)V
    .locals 2

    iget-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    sget-object v1, Landroid/uwb/RangingSession$State;->ACTIVE:Landroid/uwb/RangingSession$State;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    sget-object v1, Landroid/uwb/RangingSession$State;->IDLE:Landroid/uwb/RangingSession$State;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    :goto_0
    :try_start_0
    iget-object v0, p0, Landroid/uwb/RangingSession;->mAdapter:Landroid/uwb/IUwbAdapter;

    iget-object v1, p0, Landroid/uwb/RangingSession;->mSessionHandle:Landroid/uwb/SessionHandle;

    invoke-interface {v0, v1, p1}, Landroid/uwb/IUwbAdapter;->removeControlee(Landroid/uwb/SessionHandle;Landroid/os/PersistableBundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    nop

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1
.end method

.method public resume(Landroid/os/PersistableBundle;)V
    .locals 2

    iget-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    sget-object v1, Landroid/uwb/RangingSession$State;->ACTIVE:Landroid/uwb/RangingSession$State;

    if-ne v0, v1, :cond_0

    :try_start_0
    iget-object v0, p0, Landroid/uwb/RangingSession;->mAdapter:Landroid/uwb/IUwbAdapter;

    iget-object v1, p0, Landroid/uwb/RangingSession;->mSessionHandle:Landroid/uwb/SessionHandle;

    invoke-interface {v0, v1, p1}, Landroid/uwb/IUwbAdapter;->resume(Landroid/uwb/SessionHandle;Landroid/os/PersistableBundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    nop

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public sendData(Landroid/uwb/UwbAddress;Landroid/os/PersistableBundle;[B)V
    .locals 2

    iget-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    sget-object v1, Landroid/uwb/RangingSession$State;->ACTIVE:Landroid/uwb/RangingSession$State;

    if-ne v0, v1, :cond_0

    :try_start_0
    iget-object v0, p0, Landroid/uwb/RangingSession;->mAdapter:Landroid/uwb/IUwbAdapter;

    iget-object v1, p0, Landroid/uwb/RangingSession;->mSessionHandle:Landroid/uwb/SessionHandle;

    invoke-interface {v0, v1, p1, p2, p3}, Landroid/uwb/IUwbAdapter;->sendData(Landroid/uwb/SessionHandle;Landroid/uwb/UwbAddress;Landroid/os/PersistableBundle;[B)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    nop

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public start(Landroid/os/PersistableBundle;)V
    .locals 2

    iget-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    sget-object v1, Landroid/uwb/RangingSession$State;->IDLE:Landroid/uwb/RangingSession$State;

    if-ne v0, v1, :cond_0

    :try_start_0
    iget-object v0, p0, Landroid/uwb/RangingSession;->mAdapter:Landroid/uwb/IUwbAdapter;

    iget-object v1, p0, Landroid/uwb/RangingSession;->mSessionHandle:Landroid/uwb/SessionHandle;

    invoke-interface {v0, v1, p1}, Landroid/uwb/IUwbAdapter;->startRanging(Landroid/uwb/SessionHandle;Landroid/os/PersistableBundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    nop

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public stop()V
    .locals 2

    iget-object v0, p0, Landroid/uwb/RangingSession;->mState:Landroid/uwb/RangingSession$State;

    sget-object v1, Landroid/uwb/RangingSession$State;->ACTIVE:Landroid/uwb/RangingSession$State;

    if-ne v0, v1, :cond_0

    :try_start_0
    iget-object v0, p0, Landroid/uwb/RangingSession;->mAdapter:Landroid/uwb/IUwbAdapter;

    iget-object v1, p0, Landroid/uwb/RangingSession;->mSessionHandle:Landroid/uwb/SessionHandle;

    invoke-interface {v0, v1}, Landroid/uwb/IUwbAdapter;->stopRanging(Landroid/uwb/SessionHandle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    nop

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method
