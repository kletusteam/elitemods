.class public interface abstract Landroid/uwb/IUwbAdapterStateCallbacks;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/uwb/IUwbAdapterStateCallbacks$Stub;,
        Landroid/uwb/IUwbAdapterStateCallbacks$Default;
    }
.end annotation


# static fields
.field public static final DESCRIPTOR:Ljava/lang/String; = "android.uwb.IUwbAdapterStateCallbacks"


# virtual methods
.method public abstract onAdapterStateChanged(II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
