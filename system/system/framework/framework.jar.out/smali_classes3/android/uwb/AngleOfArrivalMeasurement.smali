.class public final Landroid/uwb/AngleOfArrivalMeasurement;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation runtime Landroid/annotation/SystemApi;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/uwb/AngleOfArrivalMeasurement$Builder;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Landroid/uwb/AngleOfArrivalMeasurement;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAltitudeAngleMeasurement:Landroid/uwb/AngleMeasurement;

.field private final mAzimuthAngleMeasurement:Landroid/uwb/AngleMeasurement;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/uwb/AngleOfArrivalMeasurement$1;

    invoke-direct {v0}, Landroid/uwb/AngleOfArrivalMeasurement$1;-><init>()V

    sput-object v0, Landroid/uwb/AngleOfArrivalMeasurement;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/uwb/AngleMeasurement;Landroid/uwb/AngleMeasurement;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/uwb/AngleOfArrivalMeasurement;->mAzimuthAngleMeasurement:Landroid/uwb/AngleMeasurement;

    iput-object p2, p0, Landroid/uwb/AngleOfArrivalMeasurement;->mAltitudeAngleMeasurement:Landroid/uwb/AngleMeasurement;

    return-void
.end method

.method synthetic constructor <init>(Landroid/uwb/AngleMeasurement;Landroid/uwb/AngleMeasurement;Landroid/uwb/AngleOfArrivalMeasurement-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/uwb/AngleOfArrivalMeasurement;-><init>(Landroid/uwb/AngleMeasurement;Landroid/uwb/AngleMeasurement;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Landroid/uwb/AngleOfArrivalMeasurement;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    move-object v1, p1

    check-cast v1, Landroid/uwb/AngleOfArrivalMeasurement;

    iget-object v3, p0, Landroid/uwb/AngleOfArrivalMeasurement;->mAzimuthAngleMeasurement:Landroid/uwb/AngleMeasurement;

    invoke-virtual {v1}, Landroid/uwb/AngleOfArrivalMeasurement;->getAzimuth()Landroid/uwb/AngleMeasurement;

    move-result-object v4

    invoke-static {v3, v4}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Landroid/uwb/AngleOfArrivalMeasurement;->mAltitudeAngleMeasurement:Landroid/uwb/AngleMeasurement;

    invoke-virtual {v1}, Landroid/uwb/AngleOfArrivalMeasurement;->getAltitude()Landroid/uwb/AngleMeasurement;

    move-result-object v4

    invoke-static {v3, v4}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_1
    move v0, v2

    :goto_0
    return v0

    :cond_2
    return v2
.end method

.method public getAltitude()Landroid/uwb/AngleMeasurement;
    .locals 1

    iget-object v0, p0, Landroid/uwb/AngleOfArrivalMeasurement;->mAltitudeAngleMeasurement:Landroid/uwb/AngleMeasurement;

    return-object v0
.end method

.method public getAzimuth()Landroid/uwb/AngleMeasurement;
    .locals 1

    iget-object v0, p0, Landroid/uwb/AngleOfArrivalMeasurement;->mAzimuthAngleMeasurement:Landroid/uwb/AngleMeasurement;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Landroid/uwb/AngleOfArrivalMeasurement;->mAzimuthAngleMeasurement:Landroid/uwb/AngleMeasurement;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Landroid/uwb/AngleOfArrivalMeasurement;->mAltitudeAngleMeasurement:Landroid/uwb/AngleMeasurement;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AngleOfArrivalMeasurement[azimuth: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/uwb/AngleOfArrivalMeasurement;->mAzimuthAngleMeasurement:Landroid/uwb/AngleMeasurement;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", altitude: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/uwb/AngleOfArrivalMeasurement;->mAltitudeAngleMeasurement:Landroid/uwb/AngleMeasurement;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Landroid/uwb/AngleOfArrivalMeasurement;->mAzimuthAngleMeasurement:Landroid/uwb/AngleMeasurement;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Landroid/uwb/AngleOfArrivalMeasurement;->mAltitudeAngleMeasurement:Landroid/uwb/AngleMeasurement;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
