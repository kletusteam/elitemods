.class public final Landroid/uwb/RangingMeasurement;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation runtime Landroid/annotation/SystemApi;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/uwb/RangingMeasurement$Builder;,
        Landroid/uwb/RangingMeasurement$MeasurementFocus;,
        Landroid/uwb/RangingMeasurement$LineOfSight;,
        Landroid/uwb/RangingMeasurement$Status;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Landroid/uwb/RangingMeasurement;",
            ">;"
        }
    .end annotation
.end field

.field public static final LOS:I = 0x0

.field public static final LOS_UNDETERMINED:I = 0xff

.field public static final MEASUREMENT_FOCUS_ANGLE_OF_ARRIVAL_AZIMUTH:I = 0x2

.field public static final MEASUREMENT_FOCUS_ANGLE_OF_ARRIVAL_ELEVATION:I = 0x3

.field public static final MEASUREMENT_FOCUS_NONE:I = 0x0

.field public static final MEASUREMENT_FOCUS_RANGE:I = 0x1

.field public static final NLOS:I = 0x1

.field public static final RANGING_STATUS_FAILURE_OUT_OF_RANGE:I = 0x1

.field public static final RANGING_STATUS_FAILURE_UNKNOWN_ERROR:I = -0x1

.field public static final RANGING_STATUS_SUCCESS:I = 0x0

.field public static final RSSI_MAX:I = -0x1

.field public static final RSSI_MIN:I = -0x7f

.field public static final RSSI_UNKNOWN:I = -0x80


# instance fields
.field private final mAngleOfArrivalMeasurement:Landroid/uwb/AngleOfArrivalMeasurement;

.field private final mDestinationAngleOfArrivalMeasurement:Landroid/uwb/AngleOfArrivalMeasurement;

.field private final mDistanceMeasurement:Landroid/uwb/DistanceMeasurement;

.field private final mElapsedRealtimeNanos:J

.field private final mLineOfSight:I

.field private final mMeasurementFocus:I

.field private final mRemoteDeviceAddress:Landroid/uwb/UwbAddress;

.field private final mRssiDbm:I

.field private final mStatus:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/uwb/RangingMeasurement$1;

    invoke-direct {v0}, Landroid/uwb/RangingMeasurement$1;-><init>()V

    sput-object v0, Landroid/uwb/RangingMeasurement;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/uwb/UwbAddress;IJLandroid/uwb/DistanceMeasurement;Landroid/uwb/AngleOfArrivalMeasurement;Landroid/uwb/AngleOfArrivalMeasurement;III)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/uwb/RangingMeasurement;->mRemoteDeviceAddress:Landroid/uwb/UwbAddress;

    iput p2, p0, Landroid/uwb/RangingMeasurement;->mStatus:I

    iput-wide p3, p0, Landroid/uwb/RangingMeasurement;->mElapsedRealtimeNanos:J

    iput-object p5, p0, Landroid/uwb/RangingMeasurement;->mDistanceMeasurement:Landroid/uwb/DistanceMeasurement;

    iput-object p6, p0, Landroid/uwb/RangingMeasurement;->mAngleOfArrivalMeasurement:Landroid/uwb/AngleOfArrivalMeasurement;

    iput-object p7, p0, Landroid/uwb/RangingMeasurement;->mDestinationAngleOfArrivalMeasurement:Landroid/uwb/AngleOfArrivalMeasurement;

    iput p8, p0, Landroid/uwb/RangingMeasurement;->mLineOfSight:I

    iput p9, p0, Landroid/uwb/RangingMeasurement;->mMeasurementFocus:I

    iput p10, p0, Landroid/uwb/RangingMeasurement;->mRssiDbm:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/uwb/UwbAddress;IJLandroid/uwb/DistanceMeasurement;Landroid/uwb/AngleOfArrivalMeasurement;Landroid/uwb/AngleOfArrivalMeasurement;IIILandroid/uwb/RangingMeasurement-IA;)V
    .locals 0

    invoke-direct/range {p0 .. p10}, Landroid/uwb/RangingMeasurement;-><init>(Landroid/uwb/UwbAddress;IJLandroid/uwb/DistanceMeasurement;Landroid/uwb/AngleOfArrivalMeasurement;Landroid/uwb/AngleOfArrivalMeasurement;III)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Landroid/uwb/RangingMeasurement;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    move-object v1, p1

    check-cast v1, Landroid/uwb/RangingMeasurement;

    iget-object v3, p0, Landroid/uwb/RangingMeasurement;->mRemoteDeviceAddress:Landroid/uwb/UwbAddress;

    invoke-virtual {v1}, Landroid/uwb/RangingMeasurement;->getRemoteDeviceAddress()Landroid/uwb/UwbAddress;

    move-result-object v4

    invoke-static {v3, v4}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget v3, p0, Landroid/uwb/RangingMeasurement;->mStatus:I

    invoke-virtual {v1}, Landroid/uwb/RangingMeasurement;->getStatus()I

    move-result v4

    if-ne v3, v4, :cond_1

    iget-wide v3, p0, Landroid/uwb/RangingMeasurement;->mElapsedRealtimeNanos:J

    invoke-virtual {v1}, Landroid/uwb/RangingMeasurement;->getElapsedRealtimeNanos()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-nez v3, :cond_1

    iget-object v3, p0, Landroid/uwb/RangingMeasurement;->mDistanceMeasurement:Landroid/uwb/DistanceMeasurement;

    invoke-virtual {v1}, Landroid/uwb/RangingMeasurement;->getDistanceMeasurement()Landroid/uwb/DistanceMeasurement;

    move-result-object v4

    invoke-static {v3, v4}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Landroid/uwb/RangingMeasurement;->mAngleOfArrivalMeasurement:Landroid/uwb/AngleOfArrivalMeasurement;

    invoke-virtual {v1}, Landroid/uwb/RangingMeasurement;->getAngleOfArrivalMeasurement()Landroid/uwb/AngleOfArrivalMeasurement;

    move-result-object v4

    invoke-static {v3, v4}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Landroid/uwb/RangingMeasurement;->mDestinationAngleOfArrivalMeasurement:Landroid/uwb/AngleOfArrivalMeasurement;

    invoke-virtual {v1}, Landroid/uwb/RangingMeasurement;->getDestinationAngleOfArrivalMeasurement()Landroid/uwb/AngleOfArrivalMeasurement;

    move-result-object v4

    invoke-static {v3, v4}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget v3, p0, Landroid/uwb/RangingMeasurement;->mLineOfSight:I

    invoke-virtual {v1}, Landroid/uwb/RangingMeasurement;->getLineOfSight()I

    move-result v4

    if-ne v3, v4, :cond_1

    iget v3, p0, Landroid/uwb/RangingMeasurement;->mMeasurementFocus:I

    invoke-virtual {v1}, Landroid/uwb/RangingMeasurement;->getMeasurementFocus()I

    move-result v4

    if-ne v3, v4, :cond_1

    iget v3, p0, Landroid/uwb/RangingMeasurement;->mRssiDbm:I

    invoke-virtual {v1}, Landroid/uwb/RangingMeasurement;->getRssiDbm()I

    move-result v4

    if-ne v3, v4, :cond_1

    goto :goto_0

    :cond_1
    move v0, v2

    :goto_0
    return v0

    :cond_2
    return v2
.end method

.method public getAngleOfArrivalMeasurement()Landroid/uwb/AngleOfArrivalMeasurement;
    .locals 1

    iget-object v0, p0, Landroid/uwb/RangingMeasurement;->mAngleOfArrivalMeasurement:Landroid/uwb/AngleOfArrivalMeasurement;

    return-object v0
.end method

.method public getDestinationAngleOfArrivalMeasurement()Landroid/uwb/AngleOfArrivalMeasurement;
    .locals 1

    iget-object v0, p0, Landroid/uwb/RangingMeasurement;->mDestinationAngleOfArrivalMeasurement:Landroid/uwb/AngleOfArrivalMeasurement;

    return-object v0
.end method

.method public getDistanceMeasurement()Landroid/uwb/DistanceMeasurement;
    .locals 1

    iget-object v0, p0, Landroid/uwb/RangingMeasurement;->mDistanceMeasurement:Landroid/uwb/DistanceMeasurement;

    return-object v0
.end method

.method public getElapsedRealtimeNanos()J
    .locals 2

    iget-wide v0, p0, Landroid/uwb/RangingMeasurement;->mElapsedRealtimeNanos:J

    return-wide v0
.end method

.method public getLineOfSight()I
    .locals 1

    iget v0, p0, Landroid/uwb/RangingMeasurement;->mLineOfSight:I

    return v0
.end method

.method public getMeasurementFocus()I
    .locals 1

    iget v0, p0, Landroid/uwb/RangingMeasurement;->mMeasurementFocus:I

    return v0
.end method

.method public getRemoteDeviceAddress()Landroid/uwb/UwbAddress;
    .locals 1

    iget-object v0, p0, Landroid/uwb/RangingMeasurement;->mRemoteDeviceAddress:Landroid/uwb/UwbAddress;

    return-object v0
.end method

.method public getRssiDbm()I
    .locals 1

    iget v0, p0, Landroid/uwb/RangingMeasurement;->mRssiDbm:I

    return v0
.end method

.method public getStatus()I
    .locals 1

    iget v0, p0, Landroid/uwb/RangingMeasurement;->mStatus:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Landroid/uwb/RangingMeasurement;->mRemoteDeviceAddress:Landroid/uwb/UwbAddress;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget v1, p0, Landroid/uwb/RangingMeasurement;->mStatus:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-wide v1, p0, Landroid/uwb/RangingMeasurement;->mElapsedRealtimeNanos:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Landroid/uwb/RangingMeasurement;->mDistanceMeasurement:Landroid/uwb/DistanceMeasurement;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Landroid/uwb/RangingMeasurement;->mAngleOfArrivalMeasurement:Landroid/uwb/AngleOfArrivalMeasurement;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iget-object v1, p0, Landroid/uwb/RangingMeasurement;->mDestinationAngleOfArrivalMeasurement:Landroid/uwb/AngleOfArrivalMeasurement;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    iget v1, p0, Landroid/uwb/RangingMeasurement;->mLineOfSight:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x6

    aput-object v1, v0, v2

    iget v1, p0, Landroid/uwb/RangingMeasurement;->mMeasurementFocus:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x7

    aput-object v1, v0, v2

    iget v1, p0, Landroid/uwb/RangingMeasurement;->mRssiDbm:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x8

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RangingMeasurement[distance measurement: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/uwb/RangingMeasurement;->mDistanceMeasurement:Landroid/uwb/DistanceMeasurement;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", aoa measurement: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/uwb/RangingMeasurement;->mAngleOfArrivalMeasurement:Landroid/uwb/AngleOfArrivalMeasurement;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dest aoa measurement: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/uwb/RangingMeasurement;->mDestinationAngleOfArrivalMeasurement:Landroid/uwb/AngleOfArrivalMeasurement;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lineOfSight: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/uwb/RangingMeasurement;->mLineOfSight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", rssiDbm: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/uwb/RangingMeasurement;->mRssiDbm:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget-object v0, p0, Landroid/uwb/RangingMeasurement;->mRemoteDeviceAddress:Landroid/uwb/UwbAddress;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget v0, p0, Landroid/uwb/RangingMeasurement;->mStatus:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-wide v0, p0, Landroid/uwb/RangingMeasurement;->mElapsedRealtimeNanos:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Landroid/uwb/RangingMeasurement;->mDistanceMeasurement:Landroid/uwb/DistanceMeasurement;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Landroid/uwb/RangingMeasurement;->mAngleOfArrivalMeasurement:Landroid/uwb/AngleOfArrivalMeasurement;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Landroid/uwb/RangingMeasurement;->mDestinationAngleOfArrivalMeasurement:Landroid/uwb/AngleOfArrivalMeasurement;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget v0, p0, Landroid/uwb/RangingMeasurement;->mLineOfSight:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Landroid/uwb/RangingMeasurement;->mMeasurementFocus:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Landroid/uwb/RangingMeasurement;->mRssiDbm:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
