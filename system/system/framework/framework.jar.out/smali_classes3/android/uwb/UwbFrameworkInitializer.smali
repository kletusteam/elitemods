.class public Landroid/uwb/UwbFrameworkInitializer;
.super Ljava/lang/Object;


# annotations
.annotation runtime Landroid/annotation/SystemApi;
    client = .enum Landroid/annotation/SystemApi$Client;->MODULE_LIBRARIES:Landroid/annotation/SystemApi$Client;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$registerServiceWrappers$0(Landroid/content/Context;Landroid/os/IBinder;)Landroid/uwb/UwbManager;
    .locals 2

    invoke-static {p1}, Landroid/uwb/IUwbAdapter$Stub;->asInterface(Landroid/os/IBinder;)Landroid/uwb/IUwbAdapter;

    move-result-object v0

    new-instance v1, Landroid/uwb/UwbManager;

    invoke-direct {v1, p0, v0}, Landroid/uwb/UwbManager;-><init>(Landroid/content/Context;Landroid/uwb/IUwbAdapter;)V

    return-object v1
.end method

.method public static registerServiceWrappers()V
    .locals 3

    const-class v0, Landroid/uwb/UwbManager;

    new-instance v1, Landroid/uwb/UwbFrameworkInitializer$$ExternalSyntheticLambda0;

    invoke-direct {v1}, Landroid/uwb/UwbFrameworkInitializer$$ExternalSyntheticLambda0;-><init>()V

    const-string/jumbo v2, "uwb"

    invoke-static {v2, v0, v1}, Landroid/app/SystemServiceRegistry;->registerContextAwareService(Ljava/lang/String;Ljava/lang/Class;Landroid/app/SystemServiceRegistry$ContextAwareServiceProducerWithBinder;)V

    return-void
.end method
