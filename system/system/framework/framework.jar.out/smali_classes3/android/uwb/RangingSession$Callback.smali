.class public interface abstract Landroid/uwb/RangingSession$Callback;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/uwb/RangingSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callback"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/uwb/RangingSession$Callback$DataFailureReason;,
        Landroid/uwb/RangingSession$Callback$ControleeFailureReason;,
        Landroid/uwb/RangingSession$Callback$Reason;
    }
.end annotation


# static fields
.field public static final CONTROLEE_FAILURE_REASON_MAX_CONTROLEE_REACHED:I = 0x0

.field public static final DATA_FAILURE_REASON_DATA_SIZE_TOO_LARGE:I = 0xa

.field public static final REASON_BAD_PARAMETERS:I = 0x3

.field public static final REASON_GENERIC_ERROR:I = 0x4

.field public static final REASON_LOCAL_REQUEST:I = 0x1

.field public static final REASON_MAX_RR_RETRY_REACHED:I = 0x9

.field public static final REASON_MAX_SESSIONS_REACHED:I = 0x5

.field public static final REASON_PROTOCOL_SPECIFIC_ERROR:I = 0x7

.field public static final REASON_REMOTE_REQUEST:I = 0x2

.field public static final REASON_SERVICE_CONNECTION_FAILURE:I = 0xb

.field public static final REASON_SERVICE_DISCOVERY_FAILURE:I = 0xa

.field public static final REASON_SE_INTERACTION_FAILURE:I = 0xd

.field public static final REASON_SE_NOT_SUPPORTED:I = 0xc

.field public static final REASON_SYSTEM_POLICY:I = 0x6

.field public static final REASON_UNKNOWN:I


# virtual methods
.method public abstract onClosed(ILandroid/os/PersistableBundle;)V
.end method

.method public onControleeAddFailed(ILandroid/os/PersistableBundle;)V
    .locals 0

    return-void
.end method

.method public onControleeAdded(Landroid/os/PersistableBundle;)V
    .locals 0

    return-void
.end method

.method public onControleeRemoveFailed(ILandroid/os/PersistableBundle;)V
    .locals 0

    return-void
.end method

.method public onControleeRemoved(Landroid/os/PersistableBundle;)V
    .locals 0

    return-void
.end method

.method public onDataReceiveFailed(Landroid/uwb/UwbAddress;ILandroid/os/PersistableBundle;)V
    .locals 0

    return-void
.end method

.method public onDataReceived(Landroid/uwb/UwbAddress;Landroid/os/PersistableBundle;[B)V
    .locals 0

    return-void
.end method

.method public onDataSendFailed(Landroid/uwb/UwbAddress;ILandroid/os/PersistableBundle;)V
    .locals 0

    return-void
.end method

.method public onDataSent(Landroid/uwb/UwbAddress;Landroid/os/PersistableBundle;)V
    .locals 0

    return-void
.end method

.method public abstract onOpenFailed(ILandroid/os/PersistableBundle;)V
.end method

.method public abstract onOpened(Landroid/uwb/RangingSession;)V
.end method

.method public onPauseFailed(ILandroid/os/PersistableBundle;)V
    .locals 0

    return-void
.end method

.method public onPaused(Landroid/os/PersistableBundle;)V
    .locals 0

    return-void
.end method

.method public abstract onReconfigureFailed(ILandroid/os/PersistableBundle;)V
.end method

.method public abstract onReconfigured(Landroid/os/PersistableBundle;)V
.end method

.method public abstract onReportReceived(Landroid/uwb/RangingReport;)V
.end method

.method public onResumeFailed(ILandroid/os/PersistableBundle;)V
    .locals 0

    return-void
.end method

.method public onResumed(Landroid/os/PersistableBundle;)V
    .locals 0

    return-void
.end method

.method public onServiceConnected(Landroid/os/PersistableBundle;)V
    .locals 0

    return-void
.end method

.method public onServiceDiscovered(Landroid/os/PersistableBundle;)V
    .locals 0

    return-void
.end method

.method public abstract onStartFailed(ILandroid/os/PersistableBundle;)V
.end method

.method public abstract onStarted(Landroid/os/PersistableBundle;)V
.end method

.method public abstract onStopFailed(ILandroid/os/PersistableBundle;)V
.end method

.method public abstract onStopped(ILandroid/os/PersistableBundle;)V
.end method
