.class public final Landroid/uwb/DistanceMeasurement;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation runtime Landroid/annotation/SystemApi;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/uwb/DistanceMeasurement$Builder;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Landroid/uwb/DistanceMeasurement;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mConfidenceLevel:D

.field private final mErrorMeters:D

.field private final mMeters:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/uwb/DistanceMeasurement$1;

    invoke-direct {v0}, Landroid/uwb/DistanceMeasurement$1;-><init>()V

    sput-object v0, Landroid/uwb/DistanceMeasurement;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(DDD)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Landroid/uwb/DistanceMeasurement;->mMeters:D

    iput-wide p3, p0, Landroid/uwb/DistanceMeasurement;->mErrorMeters:D

    iput-wide p5, p0, Landroid/uwb/DistanceMeasurement;->mConfidenceLevel:D

    return-void
.end method

.method synthetic constructor <init>(DDDLandroid/uwb/DistanceMeasurement-IA;)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Landroid/uwb/DistanceMeasurement;-><init>(DDD)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Landroid/uwb/DistanceMeasurement;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    move-object v1, p1

    check-cast v1, Landroid/uwb/DistanceMeasurement;

    iget-wide v3, p0, Landroid/uwb/DistanceMeasurement;->mMeters:D

    invoke-virtual {v1}, Landroid/uwb/DistanceMeasurement;->getMeters()D

    move-result-wide v5

    cmpl-double v3, v3, v5

    if-nez v3, :cond_1

    iget-wide v3, p0, Landroid/uwb/DistanceMeasurement;->mErrorMeters:D

    invoke-virtual {v1}, Landroid/uwb/DistanceMeasurement;->getErrorMeters()D

    move-result-wide v5

    cmpl-double v3, v3, v5

    if-nez v3, :cond_1

    iget-wide v3, p0, Landroid/uwb/DistanceMeasurement;->mConfidenceLevel:D

    invoke-virtual {v1}, Landroid/uwb/DistanceMeasurement;->getConfidenceLevel()D

    move-result-wide v5

    cmpl-double v3, v3, v5

    if-nez v3, :cond_1

    goto :goto_0

    :cond_1
    move v0, v2

    :goto_0
    return v0

    :cond_2
    return v2
.end method

.method public getConfidenceLevel()D
    .locals 2

    iget-wide v0, p0, Landroid/uwb/DistanceMeasurement;->mConfidenceLevel:D

    return-wide v0
.end method

.method public getErrorMeters()D
    .locals 2

    iget-wide v0, p0, Landroid/uwb/DistanceMeasurement;->mErrorMeters:D

    return-wide v0
.end method

.method public getMeters()D
    .locals 2

    iget-wide v0, p0, Landroid/uwb/DistanceMeasurement;->mMeters:D

    return-wide v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    iget-wide v1, p0, Landroid/uwb/DistanceMeasurement;->mMeters:D

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-wide v1, p0, Landroid/uwb/DistanceMeasurement;->mErrorMeters:D

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-wide v1, p0, Landroid/uwb/DistanceMeasurement;->mConfidenceLevel:D

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DistanceMeasurement[meters: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Landroid/uwb/DistanceMeasurement;->mMeters:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", errorMeters: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Landroid/uwb/DistanceMeasurement;->mErrorMeters:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", confidenceLevel: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Landroid/uwb/DistanceMeasurement;->mConfidenceLevel:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget-wide v0, p0, Landroid/uwb/DistanceMeasurement;->mMeters:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    iget-wide v0, p0, Landroid/uwb/DistanceMeasurement;->mErrorMeters:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    iget-wide v0, p0, Landroid/uwb/DistanceMeasurement;->mConfidenceLevel:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    return-void
.end method
