.class public interface abstract annotation Landroid/uwb/RangingChangeReason;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final BAD_PARAMETERS:I = 0x6

.field public static final LOCAL_API:I = 0x1

.field public static final MAX_RR_RETRY_REACHED:I = 0x7

.field public static final MAX_SESSIONS_REACHED:I = 0x2

.field public static final PROTOCOL_SPECIFIC:I = 0x5

.field public static final REMOTE_REQUEST:I = 0x4

.field public static final SYSTEM_POLICY:I = 0x3

.field public static final UNKNOWN:I
