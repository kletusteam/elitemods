.class public final Landroid/uwb/UwbManager;
.super Ljava/lang/Object;


# annotations
.annotation runtime Landroid/annotation/SystemApi;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/uwb/UwbManager$SendVendorUciStatus;,
        Landroid/uwb/UwbManager$RemoveProfileAdf;,
        Landroid/uwb/UwbManager$RemoveServiceProfile;,
        Landroid/uwb/UwbManager$UwbVendorUciCallback;,
        Landroid/uwb/UwbManager$AdfProvisionStateCallback;,
        Landroid/uwb/UwbManager$AdapterStateCallback;
    }
.end annotation


# static fields
.field public static final REMOVE_PROFILE_ADF_ERROR_INTERNAL:I = 0x2

.field public static final REMOVE_PROFILE_ADF_ERROR_UNKNOWN_SERVICE:I = 0x1

.field public static final REMOVE_PROFILE_ADF_SUCCESS:I = 0x0

.field public static final REMOVE_SERVICE_PROFILE_ERROR_INTERNAL:I = 0x2

.field public static final REMOVE_SERVICE_PROFILE_ERROR_UNKNOWN_SERVICE:I = 0x1

.field public static final REMOVE_SERVICE_PROFILE_SUCCESS:I = 0x0

.field public static final SEND_VENDOR_UCI_ERROR_HW:I = 0x1

.field public static final SEND_VENDOR_UCI_ERROR_INVALID_ARGS:I = 0x3

.field public static final SEND_VENDOR_UCI_ERROR_INVALID_GID:I = 0x4

.field public static final SEND_VENDOR_UCI_ERROR_OFF:I = 0x2

.field public static final SEND_VENDOR_UCI_SUCCESS:I = 0x0

.field private static final TAG:Ljava/lang/String; = "UwbManager"


# instance fields
.field private final mAdapterStateListener:Landroid/uwb/AdapterStateListener;

.field private final mContext:Landroid/content/Context;

.field private final mRangingManager:Landroid/uwb/RangingManager;

.field private final mUwbAdapter:Landroid/uwb/IUwbAdapter;

.field private final mUwbVendorUciCallbackListener:Landroid/uwb/UwbVendorUciCallbackListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/uwb/IUwbAdapter;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/uwb/UwbManager;->mContext:Landroid/content/Context;

    iput-object p2, p0, Landroid/uwb/UwbManager;->mUwbAdapter:Landroid/uwb/IUwbAdapter;

    new-instance v0, Landroid/uwb/AdapterStateListener;

    invoke-direct {v0, p2}, Landroid/uwb/AdapterStateListener;-><init>(Landroid/uwb/IUwbAdapter;)V

    iput-object v0, p0, Landroid/uwb/UwbManager;->mAdapterStateListener:Landroid/uwb/AdapterStateListener;

    new-instance v0, Landroid/uwb/RangingManager;

    invoke-direct {v0, p2}, Landroid/uwb/RangingManager;-><init>(Landroid/uwb/IUwbAdapter;)V

    iput-object v0, p0, Landroid/uwb/UwbManager;->mRangingManager:Landroid/uwb/RangingManager;

    new-instance v0, Landroid/uwb/UwbVendorUciCallbackListener;

    invoke-direct {v0, p2}, Landroid/uwb/UwbVendorUciCallbackListener;-><init>(Landroid/uwb/IUwbAdapter;)V

    iput-object v0, p0, Landroid/uwb/UwbManager;->mUwbVendorUciCallbackListener:Landroid/uwb/UwbVendorUciCallbackListener;

    return-void
.end method

.method private elapsedRealtimeResolutionNanosInternal(Ljava/lang/String;)J
    .locals 2

    :try_start_0
    iget-object v0, p0, Landroid/uwb/UwbManager;->mUwbAdapter:Landroid/uwb/IUwbAdapter;

    invoke-interface {v0, p1}, Landroid/uwb/IUwbAdapter;->getTimestampResolutionNanos(Ljava/lang/String;)J

    move-result-wide v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-wide v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1
.end method

.method private getSpecificationInfoInternal(Ljava/lang/String;)Landroid/os/PersistableBundle;
    .locals 2

    :try_start_0
    iget-object v0, p0, Landroid/uwb/UwbManager;->mUwbAdapter:Landroid/uwb/IUwbAdapter;

    invoke-interface {v0, p1}, Landroid/uwb/IUwbAdapter;->getSpecificationInfo(Ljava/lang/String;)Landroid/os/PersistableBundle;

    move-result-object v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1
.end method

.method private openRangingSessionInternal(Landroid/os/PersistableBundle;Ljava/util/concurrent/Executor;Landroid/uwb/RangingSession$Callback;Ljava/lang/String;)Landroid/os/CancellationSignal;
    .locals 6

    iget-object v0, p0, Landroid/uwb/UwbManager;->mRangingManager:Landroid/uwb/RangingManager;

    iget-object v1, p0, Landroid/uwb/UwbManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getAttributionSource()Landroid/content/AttributionSource;

    move-result-object v1

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/uwb/RangingManager;->openSession(Landroid/content/AttributionSource;Landroid/os/PersistableBundle;Ljava/util/concurrent/Executor;Landroid/uwb/RangingSession$Callback;Ljava/lang/String;)Landroid/os/CancellationSignal;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public addServiceProfile(Landroid/os/PersistableBundle;)Landroid/os/PersistableBundle;
    .locals 2

    :try_start_0
    iget-object v0, p0, Landroid/uwb/UwbManager;->mUwbAdapter:Landroid/uwb/IUwbAdapter;

    invoke-interface {v0, p1}, Landroid/uwb/IUwbAdapter;->addServiceProfile(Landroid/os/PersistableBundle;)Landroid/os/PersistableBundle;

    move-result-object v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1
.end method

.method public elapsedRealtimeResolutionNanos()J
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/uwb/UwbManager;->elapsedRealtimeResolutionNanosInternal(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public elapsedRealtimeResolutionNanos(Ljava/lang/String;)J
    .locals 2

    invoke-static {p1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Landroid/uwb/UwbManager;->elapsedRealtimeResolutionNanosInternal(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getAdapterState()I
    .locals 1

    iget-object v0, p0, Landroid/uwb/UwbManager;->mAdapterStateListener:Landroid/uwb/AdapterStateListener;

    invoke-virtual {v0}, Landroid/uwb/AdapterStateListener;->getAdapterState()I

    move-result v0

    return v0
.end method

.method public getAdfCertificateInfo(Landroid/os/PersistableBundle;)Landroid/os/PersistableBundle;
    .locals 2

    :try_start_0
    iget-object v0, p0, Landroid/uwb/UwbManager;->mUwbAdapter:Landroid/uwb/IUwbAdapter;

    invoke-interface {v0, p1}, Landroid/uwb/IUwbAdapter;->getAdfCertificateAndInfo(Landroid/os/PersistableBundle;)Landroid/os/PersistableBundle;

    move-result-object v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1
.end method

.method public getAdfProvisioningAuthorities(Landroid/os/PersistableBundle;)Landroid/os/PersistableBundle;
    .locals 2

    :try_start_0
    iget-object v0, p0, Landroid/uwb/UwbManager;->mUwbAdapter:Landroid/uwb/IUwbAdapter;

    invoke-interface {v0, p1}, Landroid/uwb/IUwbAdapter;->getAdfProvisioningAuthorities(Landroid/os/PersistableBundle;)Landroid/os/PersistableBundle;

    move-result-object v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1
.end method

.method public getAllServiceProfiles()Landroid/os/PersistableBundle;
    .locals 2

    :try_start_0
    iget-object v0, p0, Landroid/uwb/UwbManager;->mUwbAdapter:Landroid/uwb/IUwbAdapter;

    invoke-interface {v0}, Landroid/uwb/IUwbAdapter;->getAllServiceProfiles()Landroid/os/PersistableBundle;

    move-result-object v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1
.end method

.method public getChipInfos()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/os/PersistableBundle;",
            ">;"
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Landroid/uwb/UwbManager;->mUwbAdapter:Landroid/uwb/IUwbAdapter;

    invoke-interface {v0}, Landroid/uwb/IUwbAdapter;->getChipInfos()Ljava/util/List;

    move-result-object v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1
.end method

.method public getDefaultChipId()Ljava/lang/String;
    .locals 2

    :try_start_0
    iget-object v0, p0, Landroid/uwb/UwbManager;->mUwbAdapter:Landroid/uwb/IUwbAdapter;

    invoke-interface {v0}, Landroid/uwb/IUwbAdapter;->getDefaultChipId()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1
.end method

.method public getSpecificationInfo()Landroid/os/PersistableBundle;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/uwb/UwbManager;->getSpecificationInfoInternal(Ljava/lang/String;)Landroid/os/PersistableBundle;

    move-result-object v0

    return-object v0
.end method

.method public getSpecificationInfo(Ljava/lang/String;)Landroid/os/PersistableBundle;
    .locals 1

    invoke-static {p1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Landroid/uwb/UwbManager;->getSpecificationInfoInternal(Ljava/lang/String;)Landroid/os/PersistableBundle;

    move-result-object v0

    return-object v0
.end method

.method public isUwbEnabled()Z
    .locals 3

    invoke-virtual {p0}, Landroid/uwb/UwbManager;->getAdapterState()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method public openRangingSession(Landroid/os/PersistableBundle;Ljava/util/concurrent/Executor;Landroid/uwb/RangingSession$Callback;)Landroid/os/CancellationSignal;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/uwb/UwbManager;->openRangingSessionInternal(Landroid/os/PersistableBundle;Ljava/util/concurrent/Executor;Landroid/uwb/RangingSession$Callback;Ljava/lang/String;)Landroid/os/CancellationSignal;

    move-result-object v0

    return-object v0
.end method

.method public openRangingSession(Landroid/os/PersistableBundle;Ljava/util/concurrent/Executor;Landroid/uwb/RangingSession$Callback;Ljava/lang/String;)Landroid/os/CancellationSignal;
    .locals 1

    invoke-static {p4}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/uwb/UwbManager;->openRangingSessionInternal(Landroid/os/PersistableBundle;Ljava/util/concurrent/Executor;Landroid/uwb/RangingSession$Callback;Ljava/lang/String;)Landroid/os/CancellationSignal;

    move-result-object v0

    return-object v0
.end method

.method public provisionProfileAdfByScript(Landroid/os/PersistableBundle;Ljava/util/concurrent/Executor;Landroid/uwb/UwbManager$AdfProvisionStateCallback;)V
    .locals 3

    if-eqz p2, :cond_1

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/uwb/UwbManager$AdfProvisionStateCallback;->getProxy()Landroid/uwb/UwbManager$AdfProvisionStateCallback$AdfProvisionStateCallbackProxy;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/uwb/UwbManager$AdfProvisionStateCallback$AdfProvisionStateCallbackProxy;->initProxy(Ljava/util/concurrent/Executor;Landroid/uwb/UwbManager$AdfProvisionStateCallback;)V

    :try_start_0
    iget-object v1, p0, Landroid/uwb/UwbManager;->mUwbAdapter:Landroid/uwb/IUwbAdapter;

    invoke-interface {v1, p1, v0}, Landroid/uwb/IUwbAdapter;->provisionProfileAdfByScript(Landroid/os/PersistableBundle;Landroid/uwb/IUwbAdfProvisionStateCallbacks;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    nop

    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v2

    throw v2

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "callback must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "executor must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public registerAdapterStateCallback(Ljava/util/concurrent/Executor;Landroid/uwb/UwbManager$AdapterStateCallback;)V
    .locals 1

    iget-object v0, p0, Landroid/uwb/UwbManager;->mAdapterStateListener:Landroid/uwb/AdapterStateListener;

    invoke-virtual {v0, p1, p2}, Landroid/uwb/AdapterStateListener;->register(Ljava/util/concurrent/Executor;Landroid/uwb/UwbManager$AdapterStateCallback;)V

    return-void
.end method

.method public registerUwbVendorUciCallback(Ljava/util/concurrent/Executor;Landroid/uwb/UwbManager$UwbVendorUciCallback;)V
    .locals 1

    iget-object v0, p0, Landroid/uwb/UwbManager;->mUwbVendorUciCallbackListener:Landroid/uwb/UwbVendorUciCallbackListener;

    invoke-virtual {v0, p1, p2}, Landroid/uwb/UwbVendorUciCallbackListener;->register(Ljava/util/concurrent/Executor;Landroid/uwb/UwbManager$UwbVendorUciCallback;)V

    return-void
.end method

.method public removeProfileAdf(Landroid/os/PersistableBundle;)I
    .locals 2

    :try_start_0
    iget-object v0, p0, Landroid/uwb/UwbManager;->mUwbAdapter:Landroid/uwb/IUwbAdapter;

    invoke-interface {v0, p1}, Landroid/uwb/IUwbAdapter;->removeProfileAdf(Landroid/os/PersistableBundle;)I

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1
.end method

.method public removeServiceProfile(Landroid/os/PersistableBundle;)I
    .locals 2

    :try_start_0
    iget-object v0, p0, Landroid/uwb/UwbManager;->mUwbAdapter:Landroid/uwb/IUwbAdapter;

    invoke-interface {v0, p1}, Landroid/uwb/IUwbAdapter;->removeServiceProfile(Landroid/os/PersistableBundle;)I

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1
.end method

.method public sendVendorUciMessage(II[B)I
    .locals 2

    :try_start_0
    iget-object v0, p0, Landroid/uwb/UwbManager;->mUwbAdapter:Landroid/uwb/IUwbAdapter;

    invoke-interface {v0, p1, p2, p3}, Landroid/uwb/IUwbAdapter;->sendVendorUciMessage(II[B)I

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1
.end method

.method public setUwbEnabled(Z)V
    .locals 1

    iget-object v0, p0, Landroid/uwb/UwbManager;->mAdapterStateListener:Landroid/uwb/AdapterStateListener;

    invoke-virtual {v0, p1}, Landroid/uwb/AdapterStateListener;->setEnabled(Z)V

    return-void
.end method

.method public unregisterAdapterStateCallback(Landroid/uwb/UwbManager$AdapterStateCallback;)V
    .locals 1

    iget-object v0, p0, Landroid/uwb/UwbManager;->mAdapterStateListener:Landroid/uwb/AdapterStateListener;

    invoke-virtual {v0, p1}, Landroid/uwb/AdapterStateListener;->unregister(Landroid/uwb/UwbManager$AdapterStateCallback;)V

    return-void
.end method

.method public unregisterUwbVendorUciCallback(Landroid/uwb/UwbManager$UwbVendorUciCallback;)V
    .locals 1

    iget-object v0, p0, Landroid/uwb/UwbManager;->mUwbVendorUciCallbackListener:Landroid/uwb/UwbVendorUciCallbackListener;

    invoke-virtual {v0, p1}, Landroid/uwb/UwbVendorUciCallbackListener;->unregister(Landroid/uwb/UwbManager$UwbVendorUciCallback;)V

    return-void
.end method
