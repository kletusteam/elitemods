.class Landroid/uwb/AngleOfArrivalMeasurement$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/uwb/AngleOfArrivalMeasurement;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Landroid/uwb/AngleOfArrivalMeasurement;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Landroid/uwb/AngleOfArrivalMeasurement;
    .locals 2

    new-instance v0, Landroid/uwb/AngleOfArrivalMeasurement$Builder;

    const-class v1, Landroid/uwb/AngleMeasurement;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/uwb/AngleMeasurement;

    invoke-direct {v0, v1}, Landroid/uwb/AngleOfArrivalMeasurement$Builder;-><init>(Landroid/uwb/AngleMeasurement;)V

    const-class v1, Landroid/uwb/AngleMeasurement;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/uwb/AngleMeasurement;

    invoke-virtual {v0, v1}, Landroid/uwb/AngleOfArrivalMeasurement$Builder;->setAltitude(Landroid/uwb/AngleMeasurement;)Landroid/uwb/AngleOfArrivalMeasurement$Builder;

    invoke-virtual {v0}, Landroid/uwb/AngleOfArrivalMeasurement$Builder;->build()Landroid/uwb/AngleOfArrivalMeasurement;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Landroid/uwb/AngleOfArrivalMeasurement$1;->createFromParcel(Landroid/os/Parcel;)Landroid/uwb/AngleOfArrivalMeasurement;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Landroid/uwb/AngleOfArrivalMeasurement;
    .locals 1

    new-array v0, p1, [Landroid/uwb/AngleOfArrivalMeasurement;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Landroid/uwb/AngleOfArrivalMeasurement$1;->newArray(I)[Landroid/uwb/AngleOfArrivalMeasurement;

    move-result-object p1

    return-object p1
.end method
