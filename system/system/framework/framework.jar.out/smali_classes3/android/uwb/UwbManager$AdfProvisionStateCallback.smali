.class public abstract Landroid/uwb/UwbManager$AdfProvisionStateCallback;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/uwb/UwbManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "AdfProvisionStateCallback"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/uwb/UwbManager$AdfProvisionStateCallback$AdfProvisionStateCallbackProxy;,
        Landroid/uwb/UwbManager$AdfProvisionStateCallback$Reason;
    }
.end annotation


# static fields
.field public static final REASON_INVALID_OID:I = 0x1

.field public static final REASON_SE_FAILURE:I = 0x2

.field public static final REASON_UNKNOWN:I = 0x3


# instance fields
.field private final mAdfProvisionStateCallbackProxy:Landroid/uwb/UwbManager$AdfProvisionStateCallback$AdfProvisionStateCallbackProxy;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/uwb/UwbManager$AdfProvisionStateCallback$AdfProvisionStateCallbackProxy;

    invoke-direct {v0}, Landroid/uwb/UwbManager$AdfProvisionStateCallback$AdfProvisionStateCallbackProxy;-><init>()V

    iput-object v0, p0, Landroid/uwb/UwbManager$AdfProvisionStateCallback;->mAdfProvisionStateCallbackProxy:Landroid/uwb/UwbManager$AdfProvisionStateCallback$AdfProvisionStateCallbackProxy;

    return-void
.end method


# virtual methods
.method getProxy()Landroid/uwb/UwbManager$AdfProvisionStateCallback$AdfProvisionStateCallbackProxy;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/uwb/UwbManager$AdfProvisionStateCallback;->mAdfProvisionStateCallbackProxy:Landroid/uwb/UwbManager$AdfProvisionStateCallback$AdfProvisionStateCallbackProxy;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method public abstract onProfileAdfsProvisionFailed(ILandroid/os/PersistableBundle;)V
.end method

.method public abstract onProfileAdfsProvisioned(Landroid/os/PersistableBundle;)V
.end method
