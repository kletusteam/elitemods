.class public interface abstract annotation Landroid/uwb/StateChangeReason;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final ALL_SESSIONS_CLOSED:I = 0x2

.field public static final SESSION_STARTED:I = 0x1

.field public static final SYSTEM_BOOT:I = 0x4

.field public static final SYSTEM_POLICY:I = 0x3

.field public static final UNKNOWN:I
