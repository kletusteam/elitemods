.class public final Landroid/uwb/RangingMeasurement$Builder;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/uwb/RangingMeasurement;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private mAngleOfArrivalMeasurement:Landroid/uwb/AngleOfArrivalMeasurement;

.field private mDestinationAngleOfArrivalMeasurement:Landroid/uwb/AngleOfArrivalMeasurement;

.field private mDistanceMeasurement:Landroid/uwb/DistanceMeasurement;

.field private mElapsedRealtimeNanos:J

.field private mLineOfSight:I

.field private mMeasurementFocus:I

.field private mRemoteDeviceAddress:Landroid/uwb/UwbAddress;

.field private mRssiDbm:I

.field private mStatus:I


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/uwb/RangingMeasurement$Builder;->mRemoteDeviceAddress:Landroid/uwb/UwbAddress;

    const/4 v1, -0x1

    iput v1, p0, Landroid/uwb/RangingMeasurement$Builder;->mStatus:I

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Landroid/uwb/RangingMeasurement$Builder;->mElapsedRealtimeNanos:J

    iput-object v0, p0, Landroid/uwb/RangingMeasurement$Builder;->mDistanceMeasurement:Landroid/uwb/DistanceMeasurement;

    iput-object v0, p0, Landroid/uwb/RangingMeasurement$Builder;->mAngleOfArrivalMeasurement:Landroid/uwb/AngleOfArrivalMeasurement;

    iput-object v0, p0, Landroid/uwb/RangingMeasurement$Builder;->mDestinationAngleOfArrivalMeasurement:Landroid/uwb/AngleOfArrivalMeasurement;

    const/16 v0, 0xff

    iput v0, p0, Landroid/uwb/RangingMeasurement$Builder;->mLineOfSight:I

    const/4 v0, 0x0

    iput v0, p0, Landroid/uwb/RangingMeasurement$Builder;->mMeasurementFocus:I

    const/16 v0, -0x80

    iput v0, p0, Landroid/uwb/RangingMeasurement$Builder;->mRssiDbm:I

    return-void
.end method


# virtual methods
.method public build()Landroid/uwb/RangingMeasurement;
    .locals 13

    iget v0, p0, Landroid/uwb/RangingMeasurement$Builder;->mStatus:I

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/uwb/RangingMeasurement$Builder;->mDistanceMeasurement:Landroid/uwb/DistanceMeasurement;

    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/uwb/RangingMeasurement$Builder;->mAngleOfArrivalMeasurement:Landroid/uwb/AngleOfArrivalMeasurement;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Angle of Arrival must be null if ranging is not successful"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Distance Measurement must be null if ranging is not successful"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    :goto_0
    iget-object v0, p0, Landroid/uwb/RangingMeasurement$Builder;->mRemoteDeviceAddress:Landroid/uwb/UwbAddress;

    if-eqz v0, :cond_4

    iget-wide v0, p0, Landroid/uwb/RangingMeasurement$Builder;->mElapsedRealtimeNanos:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_3

    new-instance v0, Landroid/uwb/RangingMeasurement;

    iget-object v2, p0, Landroid/uwb/RangingMeasurement$Builder;->mRemoteDeviceAddress:Landroid/uwb/UwbAddress;

    iget v3, p0, Landroid/uwb/RangingMeasurement$Builder;->mStatus:I

    iget-wide v4, p0, Landroid/uwb/RangingMeasurement$Builder;->mElapsedRealtimeNanos:J

    iget-object v6, p0, Landroid/uwb/RangingMeasurement$Builder;->mDistanceMeasurement:Landroid/uwb/DistanceMeasurement;

    iget-object v7, p0, Landroid/uwb/RangingMeasurement$Builder;->mAngleOfArrivalMeasurement:Landroid/uwb/AngleOfArrivalMeasurement;

    iget-object v8, p0, Landroid/uwb/RangingMeasurement$Builder;->mDestinationAngleOfArrivalMeasurement:Landroid/uwb/AngleOfArrivalMeasurement;

    iget v9, p0, Landroid/uwb/RangingMeasurement$Builder;->mLineOfSight:I

    iget v10, p0, Landroid/uwb/RangingMeasurement$Builder;->mMeasurementFocus:I

    iget v11, p0, Landroid/uwb/RangingMeasurement$Builder;->mRssiDbm:I

    const/4 v12, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v12}, Landroid/uwb/RangingMeasurement;-><init>(Landroid/uwb/UwbAddress;IJLandroid/uwb/DistanceMeasurement;Landroid/uwb/AngleOfArrivalMeasurement;Landroid/uwb/AngleOfArrivalMeasurement;IIILandroid/uwb/RangingMeasurement-IA;)V

    return-object v0

    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "elapsedRealtimeNanos must be >=0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Landroid/uwb/RangingMeasurement$Builder;->mElapsedRealtimeNanos:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No remote device address was set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setAngleOfArrivalMeasurement(Landroid/uwb/AngleOfArrivalMeasurement;)Landroid/uwb/RangingMeasurement$Builder;
    .locals 0

    iput-object p1, p0, Landroid/uwb/RangingMeasurement$Builder;->mAngleOfArrivalMeasurement:Landroid/uwb/AngleOfArrivalMeasurement;

    return-object p0
.end method

.method public setDestinationAngleOfArrivalMeasurement(Landroid/uwb/AngleOfArrivalMeasurement;)Landroid/uwb/RangingMeasurement$Builder;
    .locals 0

    iput-object p1, p0, Landroid/uwb/RangingMeasurement$Builder;->mDestinationAngleOfArrivalMeasurement:Landroid/uwb/AngleOfArrivalMeasurement;

    return-object p0
.end method

.method public setDistanceMeasurement(Landroid/uwb/DistanceMeasurement;)Landroid/uwb/RangingMeasurement$Builder;
    .locals 0

    iput-object p1, p0, Landroid/uwb/RangingMeasurement$Builder;->mDistanceMeasurement:Landroid/uwb/DistanceMeasurement;

    return-object p0
.end method

.method public setElapsedRealtimeNanos(J)Landroid/uwb/RangingMeasurement$Builder;
    .locals 2

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    iput-wide p1, p0, Landroid/uwb/RangingMeasurement$Builder;->mElapsedRealtimeNanos:J

    return-object p0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "elapsedRealtimeNanos must be >= 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setLineOfSight(I)Landroid/uwb/RangingMeasurement$Builder;
    .locals 0

    iput p1, p0, Landroid/uwb/RangingMeasurement$Builder;->mLineOfSight:I

    return-object p0
.end method

.method public setMeasurementFocus(I)Landroid/uwb/RangingMeasurement$Builder;
    .locals 0

    iput p1, p0, Landroid/uwb/RangingMeasurement$Builder;->mMeasurementFocus:I

    return-object p0
.end method

.method public setRemoteDeviceAddress(Landroid/uwb/UwbAddress;)Landroid/uwb/RangingMeasurement$Builder;
    .locals 0

    iput-object p1, p0, Landroid/uwb/RangingMeasurement$Builder;->mRemoteDeviceAddress:Landroid/uwb/UwbAddress;

    return-object p0
.end method

.method public setRssiDbm(I)Landroid/uwb/RangingMeasurement$Builder;
    .locals 3

    const/16 v0, -0x80

    if-eq p1, v0, :cond_1

    const/16 v0, -0x7f

    if-lt p1, v0, :cond_0

    const/4 v0, -0x1

    if-gt p1, v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid rssiDbm: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    iput p1, p0, Landroid/uwb/RangingMeasurement$Builder;->mRssiDbm:I

    return-object p0
.end method

.method public setStatus(I)Landroid/uwb/RangingMeasurement$Builder;
    .locals 0

    iput p1, p0, Landroid/uwb/RangingMeasurement$Builder;->mStatus:I

    return-object p0
.end method
