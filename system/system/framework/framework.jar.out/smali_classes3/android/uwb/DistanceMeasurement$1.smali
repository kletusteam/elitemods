.class Landroid/uwb/DistanceMeasurement$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/uwb/DistanceMeasurement;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Landroid/uwb/DistanceMeasurement;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Landroid/uwb/DistanceMeasurement;
    .locals 3

    new-instance v0, Landroid/uwb/DistanceMeasurement$Builder;

    invoke-direct {v0}, Landroid/uwb/DistanceMeasurement$Builder;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/uwb/DistanceMeasurement$Builder;->setMeters(D)Landroid/uwb/DistanceMeasurement$Builder;

    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/uwb/DistanceMeasurement$Builder;->setErrorMeters(D)Landroid/uwb/DistanceMeasurement$Builder;

    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/uwb/DistanceMeasurement$Builder;->setConfidenceLevel(D)Landroid/uwb/DistanceMeasurement$Builder;

    invoke-virtual {v0}, Landroid/uwb/DistanceMeasurement$Builder;->build()Landroid/uwb/DistanceMeasurement;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Landroid/uwb/DistanceMeasurement$1;->createFromParcel(Landroid/os/Parcel;)Landroid/uwb/DistanceMeasurement;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Landroid/uwb/DistanceMeasurement;
    .locals 1

    new-array v0, p1, [Landroid/uwb/DistanceMeasurement;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Landroid/uwb/DistanceMeasurement$1;->newArray(I)[Landroid/uwb/DistanceMeasurement;

    move-result-object p1

    return-object p1
.end method
