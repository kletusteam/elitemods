.class public Landroid/uwb/AdapterStateListener;
.super Landroid/uwb/IUwbAdapterStateCallbacks$Stub;


# static fields
.field private static final TAG:Ljava/lang/String; = "Uwb.StateListener"


# instance fields
.field private final mAdapter:Landroid/uwb/IUwbAdapter;

.field private mAdapterState:I

.field private mAdapterStateChangeReason:I

.field private final mCallbackMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Landroid/uwb/UwbManager$AdapterStateCallback;",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private mIsRegistered:Z


# direct methods
.method public constructor <init>(Landroid/uwb/IUwbAdapter;)V
    .locals 2

    invoke-direct {p0}, Landroid/uwb/IUwbAdapterStateCallbacks$Stub;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/uwb/AdapterStateListener;->mIsRegistered:Z

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Landroid/uwb/AdapterStateListener;->mCallbackMap:Ljava/util/Map;

    const/4 v1, 0x4

    iput v1, p0, Landroid/uwb/AdapterStateListener;->mAdapterStateChangeReason:I

    iput v0, p0, Landroid/uwb/AdapterStateListener;->mAdapterState:I

    iput-object p1, p0, Landroid/uwb/AdapterStateListener;->mAdapter:Landroid/uwb/IUwbAdapter;

    return-void
.end method

.method private static convertToState(I)I
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    return v0

    :pswitch_0
    const/4 v0, 0x2

    return v0

    :pswitch_1
    const/4 v0, 0x1

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static convertToStateChangedReason(I)I
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x4

    return v0

    :pswitch_0
    const/4 v0, 0x3

    return v0

    :pswitch_1
    const/4 v0, 0x2

    return v0

    :pswitch_2
    const/4 v0, 0x1

    return v0

    :pswitch_3
    const/4 v0, 0x0

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private sendCurrentState(Landroid/uwb/UwbManager$AdapterStateCallback;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Landroid/uwb/AdapterStateListener;->mCallbackMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    new-instance v3, Landroid/uwb/AdapterStateListener$$ExternalSyntheticLambda0;

    invoke-direct {v3, p0, p1}, Landroid/uwb/AdapterStateListener$$ExternalSyntheticLambda0;-><init>(Landroid/uwb/AdapterStateListener;Landroid/uwb/UwbManager$AdapterStateCallback;)V

    invoke-interface {v0, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    nop

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v3

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    nop

    throw v3

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public getAdapterState()I
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Landroid/uwb/AdapterStateListener;->mAdapter:Landroid/uwb/IUwbAdapter;

    invoke-interface {v0}, Landroid/uwb/IUwbAdapter;->getAdapterState()I

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Uwb.StateListener"

    const-string v2, "Failed to get adapter state"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1

    :goto_0
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method synthetic lambda$sendCurrentState$0$android-uwb-AdapterStateListener(Landroid/uwb/UwbManager$AdapterStateCallback;)V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    iget v1, p0, Landroid/uwb/AdapterStateListener;->mAdapterStateChangeReason:I

    goto/32 :goto_2

    nop

    :goto_1
    iget v0, p0, Landroid/uwb/AdapterStateListener;->mAdapterState:I

    goto/32 :goto_0

    nop

    :goto_2
    invoke-interface {p1, v0, v1}, Landroid/uwb/UwbManager$AdapterStateCallback;->onStateChanged(II)V

    goto/32 :goto_3

    nop

    :goto_3
    return-void
.end method

.method public onAdapterStateChanged(II)V
    .locals 4

    monitor-enter p0

    nop

    :try_start_0
    invoke-static {p2}, Landroid/uwb/AdapterStateListener;->convertToStateChangedReason(I)I

    move-result v0

    invoke-static {p1}, Landroid/uwb/AdapterStateListener;->convertToState(I)I

    move-result v1

    iput v0, p0, Landroid/uwb/AdapterStateListener;->mAdapterStateChangeReason:I

    iput v1, p0, Landroid/uwb/AdapterStateListener;->mAdapterState:I

    iget-object v2, p0, Landroid/uwb/AdapterStateListener;->mCallbackMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/uwb/UwbManager$AdapterStateCallback;

    invoke-direct {p0, v3}, Landroid/uwb/AdapterStateListener;->sendCurrentState(Landroid/uwb/UwbManager$AdapterStateCallback;)V

    goto :goto_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public register(Ljava/util/concurrent/Executor;Landroid/uwb/UwbManager$AdapterStateCallback;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Landroid/uwb/AdapterStateListener;->mCallbackMap:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    iget-object v0, p0, Landroid/uwb/AdapterStateListener;->mCallbackMap:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v0, p0, Landroid/uwb/AdapterStateListener;->mIsRegistered:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :try_start_1
    iget-object v0, p0, Landroid/uwb/AdapterStateListener;->mAdapter:Landroid/uwb/IUwbAdapter;

    invoke-interface {v0, p0}, Landroid/uwb/IUwbAdapter;->registerAdapterStateCallbacks(Landroid/uwb/IUwbAdapterStateCallbacks;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/uwb/AdapterStateListener;->mIsRegistered:Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "Uwb.StateListener"

    const-string v2, "Failed to register adapter state callback"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1

    :cond_1
    invoke-direct {p0, p2}, Landroid/uwb/AdapterStateListener;->sendCurrentState(Landroid/uwb/UwbManager$AdapterStateCallback;)V

    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public setEnabled(Z)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Landroid/uwb/AdapterStateListener;->mAdapter:Landroid/uwb/IUwbAdapter;

    invoke-interface {v0, p1}, Landroid/uwb/IUwbAdapter;->setEnabled(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    nop

    :try_start_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Uwb.StateListener"

    const-string v2, "Failed to set adapter state"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1

    :goto_0
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public unregister(Landroid/uwb/UwbManager$AdapterStateCallback;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Landroid/uwb/AdapterStateListener;->mCallbackMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    iget-object v0, p0, Landroid/uwb/AdapterStateListener;->mCallbackMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Landroid/uwb/AdapterStateListener;->mCallbackMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Landroid/uwb/AdapterStateListener;->mIsRegistered:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :try_start_1
    iget-object v0, p0, Landroid/uwb/AdapterStateListener;->mAdapter:Landroid/uwb/IUwbAdapter;

    invoke-interface {v0, p0}, Landroid/uwb/IUwbAdapter;->unregisterAdapterStateCallbacks(Landroid/uwb/IUwbAdapterStateCallbacks;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    nop

    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, Landroid/uwb/AdapterStateListener;->mIsRegistered:Z

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Uwb.StateListener"

    const-string v2, "Failed to unregister AdapterStateCallback with service"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/os/RemoteException;->rethrowFromSystemServer()Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1

    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method
