.class public final Landroid/uwb/UwbProtoEnums;
.super Ljava/lang/Object;


# static fields
.field public static final BAD_PARAMS:I = 0x3

.field public static final CCC:I = 0x2

.field public static final COUNT_UNKNOWN:I = 0x0

.field public static final CUSTOMIZED:I = 0x3

.field public static final DURATION_UNKNOWN:I = 0x0

.field public static final DYNAMIC:I = 0x2

.field public static final FIRA:I = 0x1

.field public static final FIVE_TO_TWENTY:I = 0x3

.field public static final GENERAL_FAILURE:I = 0x2

.field public static final LOS:I = 0x2

.field public static final MORE_THAN_FIVE_HUNDRED:I = 0x6

.field public static final MORE_THAN_ONE_HOUR:I = 0x6

.field public static final NLOS:I = 0x1

.field public static final NLOS_UNKNOWN:I = 0x0

.field public static final ONE_HUNDRED_TO_FIVE_HUNDRED:I = 0x5

.field public static final ONE_TO_FIVE:I = 0x2

.field public static final ONE_TO_TEN_MIN:I = 0x4

.field public static final ONE_TO_TEN_SEC:I = 0x2

.field public static final PROVISIONED:I = 0x3

.field public static final PROVISION_FAILED:I = 0x8

.field public static final REJECTED:I = 0x4

.field public static final SERVICE_NOT_FOUND:I = 0x7

.field public static final SESSION_DUPLICATE:I = 0x5

.field public static final SESSION_EXCEEDED:I = 0x6

.field public static final STATIC:I = 0x1

.field public static final STATUS_UNKNOWN:I = 0x0

.field public static final SUCCESS:I = 0x1

.field public static final TEN_MIN_TO_ONE_HOUR:I = 0x5

.field public static final TEN_SEC_TO_ONE_MIN:I = 0x3

.field public static final TWENTY_TO_ONE_HUNDRED:I = 0x4

.field public static final UNKNOWN:I = 0x0

.field public static final UNKNOWN_STS:I = 0x0

.field public static final WITHIN_ONE_SEC:I = 0x1

.field public static final ZERO:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
