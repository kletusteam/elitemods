.class final Landroid/service/dreams/DreamService$DreamActivityCallback;
.super Landroid/os/Binder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/service/dreams/DreamService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "DreamActivityCallback"
.end annotation


# instance fields
.field private final mActivityDreamToken:Landroid/os/IBinder;

.field final synthetic this$0:Landroid/service/dreams/DreamService;


# direct methods
.method constructor <init>(Landroid/service/dreams/DreamService;Landroid/os/IBinder;)V
    .locals 0

    iput-object p1, p0, Landroid/service/dreams/DreamService$DreamActivityCallback;->this$0:Landroid/service/dreams/DreamService;

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    iput-object p2, p0, Landroid/service/dreams/DreamService$DreamActivityCallback;->mActivityDreamToken:Landroid/os/IBinder;

    return-void
.end method


# virtual methods
.method onActivityCreated(Landroid/service/dreams/DreamActivity;)V
    .locals 2

    goto/32 :goto_8

    nop

    :goto_0
    invoke-static {}, Landroid/service/dreams/DreamService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_10

    nop

    :goto_1
    if-eqz v0, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_9

    nop

    :goto_2
    invoke-static {v0, p1}, Landroid/service/dreams/DreamService;->-$$Nest$fputmActivity(Landroid/service/dreams/DreamService;Landroid/app/Activity;)V

    goto/32 :goto_5

    nop

    :goto_3
    goto :goto_19

    :goto_4
    goto/32 :goto_21

    nop

    :goto_5
    iget-object v0, p0, Landroid/service/dreams/DreamService$DreamActivityCallback;->this$0:Landroid/service/dreams/DreamService;

    goto/32 :goto_1d

    nop

    :goto_6
    invoke-virtual {p1}, Landroid/service/dreams/DreamActivity;->finishAndRemoveTask()V

    :goto_7
    goto/32 :goto_16

    nop

    :goto_8
    iget-object v0, p0, Landroid/service/dreams/DreamService$DreamActivityCallback;->mActivityDreamToken:Landroid/os/IBinder;

    goto/32 :goto_b

    nop

    :goto_9
    invoke-virtual {p1}, Landroid/service/dreams/DreamActivity;->finishAndRemoveTask()V

    :goto_a
    goto/32 :goto_11

    nop

    :goto_b
    iget-object v1, p0, Landroid/service/dreams/DreamService$DreamActivityCallback;->this$0:Landroid/service/dreams/DreamService;

    goto/32 :goto_17

    nop

    :goto_c
    invoke-static {v0}, Landroid/service/dreams/DreamService;->-$$Nest$fgetmFinished(Landroid/service/dreams/DreamService;)Z

    move-result v0

    goto/32 :goto_20

    nop

    :goto_d
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_1e

    nop

    :goto_e
    invoke-static {v0}, Landroid/service/dreams/DreamService;->-$$Nest$fgetmActivity(Landroid/service/dreams/DreamService;)Landroid/app/Activity;

    move-result-object v0

    goto/32 :goto_f

    nop

    :goto_f
    if-nez v0, :cond_1

    goto/32 :goto_12

    :cond_1
    goto/32 :goto_0

    nop

    :goto_10
    const-string v1, "A DreamActivity has already been started, finishing latest DreamActivity"

    goto/32 :goto_14

    nop

    :goto_11
    return-void

    :goto_12
    goto/32 :goto_1b

    nop

    :goto_13
    invoke-static {}, Landroid/service/dreams/DreamService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_1a

    nop

    :goto_14
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_1c

    nop

    :goto_15
    iget-object v0, p0, Landroid/service/dreams/DreamService$DreamActivityCallback;->this$0:Landroid/service/dreams/DreamService;

    goto/32 :goto_c

    nop

    :goto_16
    return-void

    :goto_17
    invoke-static {v1}, Landroid/service/dreams/DreamService;->-$$Nest$fgetmDreamToken(Landroid/service/dreams/DreamService;)Landroid/os/IBinder;

    move-result-object v1

    goto/32 :goto_1f

    nop

    :goto_18
    return-void

    :goto_19
    goto/32 :goto_13

    nop

    :goto_1a
    const-string v1, "DreamActivity was created after the dream was finished or a new dream started, finishing DreamActivity"

    goto/32 :goto_d

    nop

    :goto_1b
    iget-object v0, p0, Landroid/service/dreams/DreamService$DreamActivityCallback;->this$0:Landroid/service/dreams/DreamService;

    goto/32 :goto_2

    nop

    :goto_1c
    invoke-virtual {p1}, Landroid/service/dreams/DreamActivity;->isFinishing()Z

    move-result v0

    goto/32 :goto_1

    nop

    :goto_1d
    invoke-virtual {p1}, Landroid/service/dreams/DreamActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    goto/32 :goto_23

    nop

    :goto_1e
    invoke-virtual {p1}, Landroid/service/dreams/DreamActivity;->isFinishing()Z

    move-result v0

    goto/32 :goto_22

    nop

    :goto_1f
    if-eq v0, v1, :cond_2

    goto/32 :goto_19

    :cond_2
    goto/32 :goto_15

    nop

    :goto_20
    if-nez v0, :cond_3

    goto/32 :goto_4

    :cond_3
    goto/32 :goto_3

    nop

    :goto_21
    iget-object v0, p0, Landroid/service/dreams/DreamService$DreamActivityCallback;->this$0:Landroid/service/dreams/DreamService;

    goto/32 :goto_e

    nop

    :goto_22
    if-eqz v0, :cond_4

    goto/32 :goto_7

    :cond_4
    goto/32 :goto_6

    nop

    :goto_23
    invoke-static {v0, v1}, Landroid/service/dreams/DreamService;->-$$Nest$monWindowCreated(Landroid/service/dreams/DreamService;Landroid/view/Window;)V

    goto/32 :goto_18

    nop
.end method
