.class Landroid/service/media/MediaBrowserService$4;
.super Landroid/service/media/MediaBrowserService$Result;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/service/media/MediaBrowserService;->performLoadItem(Ljava/lang/String;Landroid/service/media/MediaBrowserService$ConnectionRecord;Landroid/os/ResultReceiver;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/service/media/MediaBrowserService$Result<",
        "Landroid/media/browse/MediaBrowser$MediaItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Landroid/service/media/MediaBrowserService;

.field final synthetic val$connection:Landroid/service/media/MediaBrowserService$ConnectionRecord;

.field final synthetic val$itemId:Ljava/lang/String;

.field final synthetic val$receiver:Landroid/os/ResultReceiver;


# direct methods
.method constructor <init>(Landroid/service/media/MediaBrowserService;Ljava/lang/Object;Landroid/service/media/MediaBrowserService$ConnectionRecord;Ljava/lang/String;Landroid/os/ResultReceiver;)V
    .locals 0

    iput-object p1, p0, Landroid/service/media/MediaBrowserService$4;->this$0:Landroid/service/media/MediaBrowserService;

    iput-object p3, p0, Landroid/service/media/MediaBrowserService$4;->val$connection:Landroid/service/media/MediaBrowserService$ConnectionRecord;

    iput-object p4, p0, Landroid/service/media/MediaBrowserService$4;->val$itemId:Ljava/lang/String;

    iput-object p5, p0, Landroid/service/media/MediaBrowserService$4;->val$receiver:Landroid/os/ResultReceiver;

    invoke-direct {p0, p1, p2}, Landroid/service/media/MediaBrowserService$Result;-><init>(Landroid/service/media/MediaBrowserService;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method onResultSent(Landroid/media/browse/MediaBrowser$MediaItem;I)V
    .locals 3

    goto/32 :goto_f

    nop

    :goto_0
    invoke-static {v0}, Landroid/service/media/MediaBrowserService;->-$$Nest$fgetmConnections(Landroid/service/media/MediaBrowserService;)Landroid/util/ArrayMap;

    move-result-object v0

    goto/32 :goto_16

    nop

    :goto_1
    invoke-virtual {v0, v1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_2
    const/4 v2, 0x0

    goto/32 :goto_a

    nop

    :goto_3
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto/32 :goto_6

    nop

    :goto_4
    iget-object v1, p0, Landroid/service/media/MediaBrowserService$4;->val$connection:Landroid/service/media/MediaBrowserService$ConnectionRecord;

    goto/32 :goto_5

    nop

    :goto_5
    if-ne v0, v1, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_c

    nop

    :goto_6
    const-string/jumbo v1, "media_item"

    goto/32 :goto_13

    nop

    :goto_7
    invoke-virtual {v0, v1, v2}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    goto/32 :goto_14

    nop

    :goto_8
    iget-object v0, p0, Landroid/service/media/MediaBrowserService$4;->val$receiver:Landroid/os/ResultReceiver;

    goto/32 :goto_17

    nop

    :goto_9
    and-int/lit8 v0, p2, 0x2

    goto/32 :goto_11

    nop

    :goto_a
    invoke-virtual {v1, v2, v0}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    goto/32 :goto_e

    nop

    :goto_b
    const/4 v2, 0x0

    goto/32 :goto_7

    nop

    :goto_c
    return-void

    :goto_d
    goto/32 :goto_9

    nop

    :goto_e
    return-void

    :goto_f
    iget-object v0, p0, Landroid/service/media/MediaBrowserService$4;->this$0:Landroid/service/media/MediaBrowserService;

    goto/32 :goto_0

    nop

    :goto_10
    new-instance v0, Landroid/os/Bundle;

    goto/32 :goto_3

    nop

    :goto_11
    if-nez v0, :cond_1

    goto/32 :goto_15

    :cond_1
    goto/32 :goto_8

    nop

    :goto_12
    iget-object v1, v1, Landroid/service/media/MediaBrowserService$ConnectionRecord;->callbacks:Landroid/service/media/IMediaBrowserServiceCallbacks;

    goto/32 :goto_18

    nop

    :goto_13
    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto/32 :goto_19

    nop

    :goto_14
    return-void

    :goto_15
    goto/32 :goto_10

    nop

    :goto_16
    iget-object v1, p0, Landroid/service/media/MediaBrowserService$4;->val$connection:Landroid/service/media/MediaBrowserService$ConnectionRecord;

    goto/32 :goto_12

    nop

    :goto_17
    const/4 v1, -0x1

    goto/32 :goto_b

    nop

    :goto_18
    invoke-interface {v1}, Landroid/service/media/IMediaBrowserServiceCallbacks;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    goto/32 :goto_1

    nop

    :goto_19
    iget-object v1, p0, Landroid/service/media/MediaBrowserService$4;->val$receiver:Landroid/os/ResultReceiver;

    goto/32 :goto_2

    nop
.end method

.method bridge synthetic onResultSent(Ljava/lang/Object;I)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p0, p1, p2}, Landroid/service/media/MediaBrowserService$4;->onResultSent(Landroid/media/browse/MediaBrowser$MediaItem;I)V

    goto/32 :goto_2

    nop

    :goto_1
    check-cast p1, Landroid/media/browse/MediaBrowser$MediaItem;

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method
