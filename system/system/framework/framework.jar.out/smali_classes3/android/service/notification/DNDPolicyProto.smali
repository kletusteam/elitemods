.class public final Landroid/service/notification/DNDPolicyProto;
.super Ljava/lang/Object;


# static fields
.field public static final ALARMS:J = 0x10e00000007L

.field public static final ALLOW_CALLS_FROM:J = 0x10e00000011L

.field public static final ALLOW_CONVERSATIONS_FROM:J = 0x10e00000013L

.field public static final ALLOW_MESSAGES_FROM:J = 0x10e00000012L

.field public static final AMBIENT:J = 0x10e0000000fL

.field public static final BADGE:J = 0x10e0000000eL

.field public static final CALLS:J = 0x10e00000001L

.field public static final CONVERSATIONS:J = 0x10e00000004L

.field public static final CONV_ANYONE:I = 0x1

.field public static final CONV_IMPORTANT:I = 0x2

.field public static final CONV_NONE:I = 0x3

.field public static final CONV_UNSET:I = 0x0

.field public static final EVENTS:J = 0x10e00000006L

.field public static final FULLSCREEN:J = 0x10e0000000aL

.field public static final LIGHTS:J = 0x10e0000000bL

.field public static final MEDIA:J = 0x10e00000008L

.field public static final MESSAGES:J = 0x10e00000003L

.field public static final NOTIFICATION_LIST:J = 0x10e00000010L

.field public static final PEEK:J = 0x10e0000000cL

.field public static final PEOPLE_ANYONE:I = 0x1

.field public static final PEOPLE_CONTACTS:I = 0x2

.field public static final PEOPLE_NONE:I = 0x4

.field public static final PEOPLE_STARRED:I = 0x3

.field public static final PEOPLE_UNSET:I = 0x0

.field public static final REMINDERS:J = 0x10e00000005L

.field public static final REPEAT_CALLERS:J = 0x10e00000002L

.field public static final STATE_ALLOW:I = 0x1

.field public static final STATE_DISALLOW:I = 0x2

.field public static final STATE_UNSET:I = 0x0

.field public static final STATUS_BAR:J = 0x10e0000000dL

.field public static final SYSTEM:J = 0x10e00000009L


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
