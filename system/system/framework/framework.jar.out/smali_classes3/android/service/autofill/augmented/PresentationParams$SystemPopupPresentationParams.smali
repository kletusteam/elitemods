.class public final Landroid/service/autofill/augmented/PresentationParams$SystemPopupPresentationParams;
.super Landroid/service/autofill/augmented/PresentationParams;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/service/autofill/augmented/PresentationParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SystemPopupPresentationParams"
.end annotation


# instance fields
.field private final mSuggestionArea:Landroid/service/autofill/augmented/PresentationParams$Area;


# direct methods
.method public constructor <init>(Landroid/service/autofill/augmented/AugmentedAutofillService$AutofillProxy;Landroid/graphics/Rect;)V
    .locals 1

    invoke-direct {p0}, Landroid/service/autofill/augmented/PresentationParams;-><init>()V

    new-instance v0, Landroid/service/autofill/augmented/PresentationParams$SystemPopupPresentationParams$1;

    invoke-direct {v0, p0, p1, p2}, Landroid/service/autofill/augmented/PresentationParams$SystemPopupPresentationParams$1;-><init>(Landroid/service/autofill/augmented/PresentationParams$SystemPopupPresentationParams;Landroid/service/autofill/augmented/AugmentedAutofillService$AutofillProxy;Landroid/graphics/Rect;)V

    iput-object v0, p0, Landroid/service/autofill/augmented/PresentationParams$SystemPopupPresentationParams;->mSuggestionArea:Landroid/service/autofill/augmented/PresentationParams$Area;

    return-void
.end method


# virtual methods
.method dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_3

    nop

    :goto_1
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_5

    nop

    :goto_2
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    goto/32 :goto_4

    nop

    :goto_3
    iget-object v0, p0, Landroid/service/autofill/augmented/PresentationParams$SystemPopupPresentationParams;->mSuggestionArea:Landroid/service/autofill/augmented/PresentationParams$Area;

    goto/32 :goto_2

    nop

    :goto_4
    return-void

    :goto_5
    const-string v0, "area: "

    goto/32 :goto_0

    nop
.end method

.method public getSuggestionArea()Landroid/service/autofill/augmented/PresentationParams$Area;
    .locals 1

    iget-object v0, p0, Landroid/service/autofill/augmented/PresentationParams$SystemPopupPresentationParams;->mSuggestionArea:Landroid/service/autofill/augmented/PresentationParams$Area;

    return-object v0
.end method
