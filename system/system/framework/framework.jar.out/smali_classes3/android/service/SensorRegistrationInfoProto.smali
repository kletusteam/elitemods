.class public final Landroid/service/SensorRegistrationInfoProto;
.super Ljava/lang/Object;


# static fields
.field public static final ACTIVATED:J = 0x10800000008L

.field public static final MAX_REPORT_LATENCY_US:J = 0x10300000007L

.field public static final PACKAGE_NAME:J = 0x10900000003L

.field public static final PID:J = 0x10500000004L

.field public static final SAMPLING_RATE_US:J = 0x10300000006L

.field public static final SENSOR_HANDLE:J = 0x10500000002L

.field public static final TIMESTAMP_SEC:J = 0x10300000001L

.field public static final UID:J = 0x10500000005L


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
