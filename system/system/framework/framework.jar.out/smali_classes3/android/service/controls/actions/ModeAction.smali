.class public final Landroid/service/controls/actions/ModeAction;
.super Landroid/service/controls/actions/ControlAction;


# static fields
.field private static final KEY_MODE:Ljava/lang/String; = "key_mode"

.field private static final TYPE:I = 0x4


# instance fields
.field private final mNewMode:I


# direct methods
.method constructor <init>(Landroid/os/Bundle;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/service/controls/actions/ControlAction;-><init>(Landroid/os/Bundle;)V

    const-string/jumbo v0, "key_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Landroid/service/controls/actions/ModeAction;->mNewMode:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/service/controls/actions/ModeAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p3}, Landroid/service/controls/actions/ControlAction;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput p2, p0, Landroid/service/controls/actions/ModeAction;->mNewMode:I

    return-void
.end method


# virtual methods
.method public getActionType()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method getDataBundle()Landroid/os/Bundle;
    .locals 3

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0

    :goto_2
    invoke-super {p0}, Landroid/service/controls/actions/ControlAction;->getDataBundle()Landroid/os/Bundle;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_3
    iget v1, p0, Landroid/service/controls/actions/ModeAction;->mNewMode:I

    goto/32 :goto_4

    nop

    :goto_4
    const-string/jumbo v2, "key_mode"

    goto/32 :goto_0

    nop
.end method

.method public getNewMode()I
    .locals 1

    iget v0, p0, Landroid/service/controls/actions/ModeAction;->mNewMode:I

    return v0
.end method
