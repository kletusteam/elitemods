.class public final Landroid/service/controls/actions/BooleanAction;
.super Landroid/service/controls/actions/ControlAction;


# static fields
.field private static final KEY_NEW_STATE:Ljava/lang/String; = "key_new_state"

.field private static final TYPE:I = 0x1


# instance fields
.field private final mNewState:Z


# direct methods
.method constructor <init>(Landroid/os/Bundle;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/service/controls/actions/ControlAction;-><init>(Landroid/os/Bundle;)V

    const-string/jumbo v0, "key_new_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Landroid/service/controls/actions/BooleanAction;->mNewState:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/service/controls/actions/BooleanAction;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p3}, Landroid/service/controls/actions/ControlAction;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean p2, p0, Landroid/service/controls/actions/BooleanAction;->mNewState:Z

    return-void
.end method


# virtual methods
.method public getActionType()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method getDataBundle()Landroid/os/Bundle;
    .locals 3

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    invoke-super {p0}, Landroid/service/controls/actions/ControlAction;->getDataBundle()Landroid/os/Bundle;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_2
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/32 :goto_0

    nop

    :goto_3
    const-string/jumbo v2, "key_new_state"

    goto/32 :goto_2

    nop

    :goto_4
    iget-boolean v1, p0, Landroid/service/controls/actions/BooleanAction;->mNewState:Z

    goto/32 :goto_3

    nop
.end method

.method public getNewState()Z
    .locals 1

    iget-boolean v0, p0, Landroid/service/controls/actions/BooleanAction;->mNewState:Z

    return v0
.end method
