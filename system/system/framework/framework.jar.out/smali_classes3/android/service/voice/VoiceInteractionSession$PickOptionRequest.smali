.class public final Landroid/service/voice/VoiceInteractionSession$PickOptionRequest;
.super Landroid/service/voice/VoiceInteractionSession$Request;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/service/voice/VoiceInteractionSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PickOptionRequest"
.end annotation


# instance fields
.field final mOptions:[Landroid/app/VoiceInteractor$PickOptionRequest$Option;

.field final mPrompt:Landroid/app/VoiceInteractor$Prompt;


# direct methods
.method constructor <init>(Ljava/lang/String;ILcom/android/internal/app/IVoiceInteractorCallback;Landroid/service/voice/VoiceInteractionSession;Landroid/app/VoiceInteractor$Prompt;[Landroid/app/VoiceInteractor$PickOptionRequest$Option;Landroid/os/Bundle;)V
    .locals 6

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Landroid/service/voice/VoiceInteractionSession$Request;-><init>(Ljava/lang/String;ILcom/android/internal/app/IVoiceInteractorCallback;Landroid/service/voice/VoiceInteractionSession;Landroid/os/Bundle;)V

    iput-object p5, p0, Landroid/service/voice/VoiceInteractionSession$PickOptionRequest;->mPrompt:Landroid/app/VoiceInteractor$Prompt;

    iput-object p6, p0, Landroid/service/voice/VoiceInteractionSession$PickOptionRequest;->mOptions:[Landroid/app/VoiceInteractor$PickOptionRequest$Option;

    return-void
.end method


# virtual methods
.method dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 4

    goto/32 :goto_24

    nop

    :goto_0
    if-gtz v2, :cond_0

    goto/32 :goto_34

    :cond_0
    goto/32 :goto_1b

    nop

    :goto_1
    const-string v2, "  #"

    goto/32 :goto_23

    nop

    :goto_2
    const-string v2, ":"

    goto/32 :goto_17

    nop

    :goto_3
    if-nez v2, :cond_1

    goto/32 :goto_28

    :cond_1
    goto/32 :goto_14

    nop

    :goto_4
    iget-object v0, p0, Landroid/service/voice/VoiceInteractionSession$PickOptionRequest;->mPrompt:Landroid/app/VoiceInteractor$Prompt;

    goto/32 :goto_25

    nop

    :goto_5
    iget-object v0, p0, Landroid/service/voice/VoiceInteractionSession$PickOptionRequest;->mOptions:[Landroid/app/VoiceInteractor$PickOptionRequest$Option;

    goto/32 :goto_36

    nop

    :goto_6
    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_2b

    nop

    :goto_7
    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/32 :goto_3b

    nop

    :goto_8
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_40

    nop

    :goto_9
    array-length v2, v1

    goto/32 :goto_15

    nop

    :goto_a
    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    goto/32 :goto_8

    nop

    :goto_b
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_2c

    nop

    :goto_c
    iget-object v1, p0, Landroid/service/voice/VoiceInteractionSession$PickOptionRequest;->mOptions:[Landroid/app/VoiceInteractor$PickOptionRequest$Option;

    goto/32 :goto_9

    nop

    :goto_d
    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(I)V

    goto/32 :goto_f

    nop

    :goto_e
    invoke-virtual {v1}, Landroid/app/VoiceInteractor$PickOptionRequest$Option;->getLabel()Ljava/lang/CharSequence;

    move-result-object v2

    goto/32 :goto_a

    nop

    :goto_f
    invoke-virtual {v1}, Landroid/app/VoiceInteractor$PickOptionRequest$Option;->countSynonyms()I

    move-result v2

    goto/32 :goto_0

    nop

    :goto_10
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_18

    nop

    :goto_11
    invoke-virtual {v1}, Landroid/app/VoiceInteractor$PickOptionRequest$Option;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    goto/32 :goto_27

    nop

    :goto_12
    const-string v3, ": "

    goto/32 :goto_b

    nop

    :goto_13
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    goto/32 :goto_2

    nop

    :goto_14
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_35

    nop

    :goto_15
    if-lt v0, v2, :cond_2

    goto/32 :goto_38

    :cond_2
    goto/32 :goto_3a

    nop

    :goto_16
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_3e

    nop

    :goto_17
    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/32 :goto_10

    nop

    :goto_18
    const-string v2, "    mLabel="

    goto/32 :goto_26

    nop

    :goto_19
    const-string v3, "      #"

    goto/32 :goto_29

    nop

    :goto_1a
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    goto/32 :goto_31

    nop

    :goto_1b
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_2d

    nop

    :goto_1c
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/32 :goto_1e

    nop

    :goto_1d
    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    goto/32 :goto_12

    nop

    :goto_1e
    const/4 v0, 0x0

    :goto_1f
    goto/32 :goto_c

    nop

    :goto_20
    invoke-virtual {v1}, Landroid/app/VoiceInteractor$PickOptionRequest$Option;->countSynonyms()I

    move-result v3

    goto/32 :goto_2a

    nop

    :goto_21
    const-string/jumbo v0, "mPrompt="

    goto/32 :goto_22

    nop

    :goto_22
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_4

    nop

    :goto_23
    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_13

    nop

    :goto_24
    invoke-super {p0, p1, p2, p3, p4}, Landroid/service/voice/VoiceInteractionSession$Request;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    goto/32 :goto_2f

    nop

    :goto_25
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    goto/32 :goto_5

    nop

    :goto_26
    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_e

    nop

    :goto_27
    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :goto_28
    goto/32 :goto_39

    nop

    :goto_29
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_1d

    nop

    :goto_2a
    if-lt v2, v3, :cond_3

    goto/32 :goto_34

    :cond_3
    goto/32 :goto_2e

    nop

    :goto_2b
    invoke-virtual {v1}, Landroid/app/VoiceInteractor$PickOptionRequest$Option;->getIndex()I

    move-result v2

    goto/32 :goto_d

    nop

    :goto_2c
    invoke-virtual {v1, v2}, Landroid/app/VoiceInteractor$PickOptionRequest$Option;->getSynonymAt(I)Ljava/lang/CharSequence;

    move-result-object v3

    goto/32 :goto_1a

    nop

    :goto_2d
    const-string v2, "    Synonyms:"

    goto/32 :goto_7

    nop

    :goto_2e
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_19

    nop

    :goto_2f
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_21

    nop

    :goto_30
    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_11

    nop

    :goto_31
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_33

    nop

    :goto_32
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_1

    nop

    :goto_33
    goto :goto_3c

    :goto_34
    goto/32 :goto_3f

    nop

    :goto_35
    const-string v2, "    mExtras="

    goto/32 :goto_30

    nop

    :goto_36
    if-nez v0, :cond_4

    goto/32 :goto_38

    :cond_4
    goto/32 :goto_16

    nop

    :goto_37
    goto/16 :goto_1f

    :goto_38
    goto/32 :goto_3d

    nop

    :goto_39
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_37

    nop

    :goto_3a
    aget-object v1, v1, v0

    goto/32 :goto_32

    nop

    :goto_3b
    const/4 v2, 0x0

    :goto_3c
    goto/32 :goto_20

    nop

    :goto_3d
    return-void

    :goto_3e
    const-string v0, "Options:"

    goto/32 :goto_1c

    nop

    :goto_3f
    invoke-virtual {v1}, Landroid/app/VoiceInteractor$PickOptionRequest$Option;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    goto/32 :goto_3

    nop

    :goto_40
    const-string v2, "    mIndex="

    goto/32 :goto_6

    nop
.end method

.method public getOptions()[Landroid/app/VoiceInteractor$PickOptionRequest$Option;
    .locals 1

    iget-object v0, p0, Landroid/service/voice/VoiceInteractionSession$PickOptionRequest;->mOptions:[Landroid/app/VoiceInteractor$PickOptionRequest$Option;

    return-object v0
.end method

.method public getPrompt()Ljava/lang/CharSequence;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Landroid/service/voice/VoiceInteractionSession$PickOptionRequest;->mPrompt:Landroid/app/VoiceInteractor$Prompt;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/VoiceInteractor$Prompt;->getVoicePromptAt(I)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getVoicePrompt()Landroid/app/VoiceInteractor$Prompt;
    .locals 1

    iget-object v0, p0, Landroid/service/voice/VoiceInteractionSession$PickOptionRequest;->mPrompt:Landroid/app/VoiceInteractor$Prompt;

    return-object v0
.end method

.method public sendIntermediatePickOptionResult([Landroid/app/VoiceInteractor$PickOptionRequest$Option;Landroid/os/Bundle;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Landroid/service/voice/VoiceInteractionSession$PickOptionRequest;->sendPickOptionResult(Z[Landroid/app/VoiceInteractor$PickOptionRequest$Option;Landroid/os/Bundle;)V

    return-void
.end method

.method sendPickOptionResult(Z[Landroid/app/VoiceInteractor$PickOptionRequest$Option;Landroid/os/Bundle;)V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    if-nez p1, :cond_0

    goto/32 :goto_1

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/service/voice/VoiceInteractionSession$PickOptionRequest;->finishRequest()V

    :goto_1
    iget-object v0, p0, Landroid/service/voice/VoiceInteractionSession$PickOptionRequest;->mCallback:Lcom/android/internal/app/IVoiceInteractorCallback;

    iget-object v1, p0, Landroid/service/voice/VoiceInteractionSession$PickOptionRequest;->mInterface:Lcom/android/internal/app/IVoiceInteractorRequest;

    invoke-interface {v0, v1, p1, p2, p3}, Lcom/android/internal/app/IVoiceInteractorCallback;->deliverPickOptionResult(Lcom/android/internal/app/IVoiceInteractorRequest;Z[Landroid/app/VoiceInteractor$PickOptionRequest$Option;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_3

    nop

    :goto_2
    return-void

    :goto_3
    goto :goto_4

    :catch_0
    move-exception v0

    :goto_4
    goto/32 :goto_2

    nop
.end method

.method public sendPickOptionResult([Landroid/app/VoiceInteractor$PickOptionRequest$Option;Landroid/os/Bundle;)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1, p2}, Landroid/service/voice/VoiceInteractionSession$PickOptionRequest;->sendPickOptionResult(Z[Landroid/app/VoiceInteractor$PickOptionRequest$Option;Landroid/os/Bundle;)V

    return-void
.end method
