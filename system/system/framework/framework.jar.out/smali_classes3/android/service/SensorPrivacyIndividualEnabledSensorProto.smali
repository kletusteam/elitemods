.class public final Landroid/service/SensorPrivacyIndividualEnabledSensorProto;
.super Ljava/lang/Object;


# static fields
.field public static final CAMERA:I = 0x2

.field public static final DISABLED:I = 0x2

.field public static final ENABLED:I = 0x1

.field public static final HARDWARE:I = 0x2

.field public static final IS_ENABLED:J = 0x10800000002L

.field public static final LAST_CHANGE:J = 0x10300000003L

.field public static final MICROPHONE:I = 0x1

.field public static final SENSOR:J = 0x10e00000001L

.field public static final SOFTWARE:I = 0x1

.field public static final STATE_TYPE:J = 0x10e00000005L

.field public static final TOGGLE_TYPE:J = 0x10e00000004L

.field public static final UNKNOWN:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
