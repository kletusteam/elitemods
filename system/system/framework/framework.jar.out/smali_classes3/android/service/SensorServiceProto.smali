.class public final Landroid/service/SensorServiceProto;
.super Ljava/lang/Object;


# static fields
.field public static final ACTIVE_CONNECTIONS:J = 0x20b0000000dL

.field public static final ACTIVE_SENSORS:J = 0x20b00000006L

.field public static final CURRENT_TIME_MS:J = 0x10300000001L

.field public static final DIRECT_CONNECTIONS:J = 0x20b0000000eL

.field public static final FUSION_STATE:J = 0x10b00000004L

.field public static final INIT_STATUS:J = 0x11100000010L

.field public static final OPERATING_MODE:J = 0x10e0000000aL

.field public static final OP_MODE_DATA_INJECTION:I = 0x3

.field public static final OP_MODE_NORMAL:I = 0x1

.field public static final OP_MODE_RESTRICTED:I = 0x2

.field public static final OP_MODE_UNKNOWN:I = 0x0

.field public static final PREVIOUS_REGISTRATIONS:J = 0x20b0000000fL

.field public static final SENSORS:J = 0x10b00000003L

.field public static final SENSOR_DEVICE:J = 0x10b00000002L

.field public static final SENSOR_EVENTS:J = 0x10b00000005L

.field public static final SENSOR_PRIVACY:J = 0x1080000000cL

.field public static final SOCKET_BUFFER_SIZE:J = 0x10500000007L

.field public static final SOCKET_BUFFER_SIZE_IN_EVENTS:J = 0x10500000008L

.field public static final WAKE_LOCK_ACQUIRED:J = 0x10800000009L

.field public static final WHITELISTED_PACKAGE:J = 0x1090000000bL


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
