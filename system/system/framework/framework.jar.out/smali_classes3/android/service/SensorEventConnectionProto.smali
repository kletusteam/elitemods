.class public final Landroid/service/SensorEventConnectionProto;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/service/SensorEventConnectionProto$FlushInfoProto;
    }
.end annotation


# static fields
.field public static final CACHE_SIZE:J = 0x10500000005L

.field public static final EVENTS_CACHE:J = 0x1050000000aL

.field public static final EVENTS_DROPPED:J = 0x1050000000bL

.field public static final EVENTS_RECEIVED:J = 0x10500000008L

.field public static final EVENTS_SENT:J = 0x10500000009L

.field public static final FLUSH_INFOS:J = 0x20b00000007L

.field public static final MAX_CACHE_SIZE:J = 0x10500000006L

.field public static final OPERATING_MODE:J = 0x10e00000001L

.field public static final OP_MODE_DATA_INJECTION:I = 0x3

.field public static final OP_MODE_NORMAL:I = 0x1

.field public static final OP_MODE_RESTRICTED:I = 0x2

.field public static final OP_MODE_UNKNOWN:I = 0x0

.field public static final PACKAGE_NAME:J = 0x10900000002L

.field public static final TOTAL_ACKS_NEEDED:J = 0x1050000000cL

.field public static final TOTAL_ACKS_RECEIVED:J = 0x1050000000dL

.field public static final UID:J = 0x10500000004L

.field public static final WAKE_LOCK_REF_COUNT:J = 0x10500000003L


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
