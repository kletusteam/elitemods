.class public Landroid/service/games/GameService;
.super Landroid/app/Service;


# annotations
.annotation runtime Landroid/annotation/SystemApi;
.end annotation


# static fields
.field public static final ACTION_GAME_SERVICE:Ljava/lang/String; = "android.service.games.action.GAME_SERVICE"

.field public static final SERVICE_META_DATA:Ljava/lang/String; = "android.game_service"

.field private static final TAG:Ljava/lang/String; = "GameService"


# instance fields
.field private mGameManagerService:Landroid/app/IGameManagerService;

.field private final mGameManagerServiceDeathRecipient:Landroid/os/IBinder$DeathRecipient;

.field private mGameServiceController:Landroid/service/games/IGameServiceController;

.field private final mInterface:Landroid/service/games/IGameService;


# direct methods
.method static bridge synthetic -$$Nest$mdoOnConnected(Landroid/service/games/GameService;Landroid/service/games/IGameServiceController;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/service/games/GameService;->doOnConnected(Landroid/service/games/IGameServiceController;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Landroid/service/games/GameService$1;

    invoke-direct {v0, p0}, Landroid/service/games/GameService$1;-><init>(Landroid/service/games/GameService;)V

    iput-object v0, p0, Landroid/service/games/GameService;->mInterface:Landroid/service/games/IGameService;

    new-instance v0, Landroid/service/games/GameService$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Landroid/service/games/GameService$$ExternalSyntheticLambda0;-><init>(Landroid/service/games/GameService;)V

    iput-object v0, p0, Landroid/service/games/GameService;->mGameManagerServiceDeathRecipient:Landroid/os/IBinder$DeathRecipient;

    return-void
.end method

.method private doOnConnected(Landroid/service/games/IGameServiceController;)V
    .locals 3

    nop

    const-string v0, "game"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/IGameManagerService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/IGameManagerService;

    move-result-object v0

    iput-object v0, p0, Landroid/service/games/GameService;->mGameManagerService:Landroid/app/IGameManagerService;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    iget-object v0, p0, Landroid/service/games/GameService;->mGameManagerService:Landroid/app/IGameManagerService;

    invoke-interface {v0}, Landroid/app/IGameManagerService;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Landroid/service/games/GameService;->mGameManagerServiceDeathRecipient:Landroid/os/IBinder$DeathRecipient;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "GameService"

    const-string v2, "Unable to link to death with system service"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iput-object p1, p0, Landroid/service/games/GameService;->mGameServiceController:Landroid/service/games/IGameServiceController;

    invoke-virtual {p0}, Landroid/service/games/GameService;->onConnected()V

    return-void
.end method


# virtual methods
.method public final createGameSession(I)V
    .locals 3

    iget-object v0, p0, Landroid/service/games/GameService;->mGameServiceController:Landroid/service/games/IGameServiceController;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-interface {v0, p1}, Landroid/service/games/IGameServiceController;->createGameSession(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "GameService"

    const-string v2, "Request for game session failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can not call before connected()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method synthetic lambda$new$0$android-service-games-GameService()V
    .locals 2

    goto/32 :goto_4

    nop

    :goto_0
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_2

    nop

    :goto_1
    invoke-direct {v1}, Landroid/service/games/GameService$$ExternalSyntheticLambda1;-><init>()V

    goto/32 :goto_6

    nop

    :goto_2
    invoke-static {}, Landroid/os/Handler;->getMain()Landroid/os/Handler;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_3
    invoke-virtual {v0, v1}, Landroid/os/Handler;->executeOrSendMessage(Landroid/os/Message;)Z

    goto/32 :goto_8

    nop

    :goto_4
    const-string v0, "GameService"

    goto/32 :goto_5

    nop

    :goto_5
    const-string v1, "System service binder died. Shutting down"

    goto/32 :goto_0

    nop

    :goto_6
    invoke-static {v1, p0}, Lcom/android/internal/util/function/pooled/PooledLambda;->obtainMessage(Ljava/util/function/Consumer;Ljava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    goto/32 :goto_3

    nop

    :goto_7
    new-instance v1, Landroid/service/games/GameService$$ExternalSyntheticLambda1;

    goto/32 :goto_1

    nop

    :goto_8
    return-void
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.service.games.action.GAME_SERVICE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/service/games/GameService;->mInterface:Landroid/service/games/IGameService;

    invoke-interface {v0}, Landroid/service/games/IGameService;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public onConnected()V
    .locals 0

    return-void
.end method

.method public onDisconnected()V
    .locals 0

    return-void
.end method

.method public onGameStarted(Landroid/service/games/GameStartedEvent;)V
    .locals 0

    return-void
.end method
