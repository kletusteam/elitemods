.class public abstract Landroid/service/dataloader/DataLoaderService;
.super Landroid/app/Service;


# annotations
.annotation runtime Landroid/annotation/SystemApi;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/service/dataloader/DataLoaderService$FileSystemConnector;,
        Landroid/service/dataloader/DataLoaderService$DataLoaderBinderService;,
        Landroid/service/dataloader/DataLoaderService$DataLoader;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "DataLoaderService"


# instance fields
.field private final mBinder:Landroid/service/dataloader/DataLoaderService$DataLoaderBinderService;


# direct methods
.method static bridge synthetic -$$Nest$mnativeCreateDataLoader(Landroid/service/dataloader/DataLoaderService;ILandroid/content/pm/FileSystemControlParcel;Landroid/content/pm/DataLoaderParamsParcel;Landroid/content/pm/IDataLoaderStatusListener;)Z
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/service/dataloader/DataLoaderService;->nativeCreateDataLoader(ILandroid/content/pm/FileSystemControlParcel;Landroid/content/pm/DataLoaderParamsParcel;Landroid/content/pm/IDataLoaderStatusListener;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mnativeDestroyDataLoader(Landroid/service/dataloader/DataLoaderService;I)Z
    .locals 0

    invoke-direct {p0, p1}, Landroid/service/dataloader/DataLoaderService;->nativeDestroyDataLoader(I)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mnativePrepareImage(Landroid/service/dataloader/DataLoaderService;I[Landroid/content/pm/InstallationFileParcel;[Ljava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/service/dataloader/DataLoaderService;->nativePrepareImage(I[Landroid/content/pm/InstallationFileParcel;[Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mnativeStartDataLoader(Landroid/service/dataloader/DataLoaderService;I)Z
    .locals 0

    invoke-direct {p0, p1}, Landroid/service/dataloader/DataLoaderService;->nativeStartDataLoader(I)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mnativeStopDataLoader(Landroid/service/dataloader/DataLoaderService;I)Z
    .locals 0

    invoke-direct {p0, p1}, Landroid/service/dataloader/DataLoaderService;->nativeStopDataLoader(I)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$smnativeWriteData(JLjava/lang/String;JJLandroid/os/ParcelFileDescriptor;)V
    .locals 0

    invoke-static/range {p0 .. p7}, Landroid/service/dataloader/DataLoaderService;->nativeWriteData(JLjava/lang/String;JJLandroid/os/ParcelFileDescriptor;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Landroid/service/dataloader/DataLoaderService$DataLoaderBinderService;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroid/service/dataloader/DataLoaderService$DataLoaderBinderService;-><init>(Landroid/service/dataloader/DataLoaderService;Landroid/service/dataloader/DataLoaderService$DataLoaderBinderService-IA;)V

    iput-object v0, p0, Landroid/service/dataloader/DataLoaderService;->mBinder:Landroid/service/dataloader/DataLoaderService$DataLoaderBinderService;

    return-void
.end method

.method private native nativeCreateDataLoader(ILandroid/content/pm/FileSystemControlParcel;Landroid/content/pm/DataLoaderParamsParcel;Landroid/content/pm/IDataLoaderStatusListener;)Z
.end method

.method private native nativeDestroyDataLoader(I)Z
.end method

.method private native nativePrepareImage(I[Landroid/content/pm/InstallationFileParcel;[Ljava/lang/String;)Z
.end method

.method private native nativeStartDataLoader(I)Z
.end method

.method private native nativeStopDataLoader(I)Z
.end method

.method private static native nativeWriteData(JLjava/lang/String;JJLandroid/os/ParcelFileDescriptor;)V
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Landroid/service/dataloader/DataLoaderService;->mBinder:Landroid/service/dataloader/DataLoaderService$DataLoaderBinderService;

    return-object v0
.end method

.method public onCreateDataLoader(Landroid/content/pm/DataLoaderParams;)Landroid/service/dataloader/DataLoaderService$DataLoader;
    .locals 1
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method
