.class public Landroid/service/wallpaper/WallpaperService$Engine;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/service/wallpaper/WallpaperService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Engine"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/service/wallpaper/WallpaperService$Engine$WallpaperInputEventReceiver;
    }
.end annotation


# instance fields
.field mBbqSurfaceControl:Landroid/view/SurfaceControl;

.field mBlastBufferQueue:Landroid/graphics/BLASTBufferQueue;

.field mCaller:Lcom/android/internal/os/HandlerCaller;

.field private final mClockFunction:Ljava/util/function/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/function/Supplier<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field mConnection:Landroid/service/wallpaper/IWallpaperConnection;

.field mCreated:Z

.field mCurHeight:I

.field mCurWidth:I

.field mCurWindowFlags:I

.field mCurWindowPrivateFlags:I

.field private mDefaultDimAmount:F

.field mDestroyed:Z

.field final mDispatchedContentInsets:Landroid/graphics/Rect;

.field mDispatchedDisplayCutout:Landroid/view/DisplayCutout;

.field final mDispatchedStableInsets:Landroid/graphics/Rect;

.field private mDisplay:Landroid/view/Display;

.field private mDisplayContext:Landroid/content/Context;

.field private mDisplayInstallOrientation:I

.field private final mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

.field private mDisplayState:I

.field mDrawingAllowed:Z

.field mFixedSizeAllowed:Z

.field mFormat:I

.field private mFrozenRequested:Z

.field private final mHandler:Landroid/os/Handler;

.field mHeight:I

.field mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

.field mInitializing:Z

.field mInputEventReceiver:Landroid/service/wallpaper/WallpaperService$Engine$WallpaperInputEventReceiver;

.field final mInsetsState:Landroid/view/InsetsState;

.field mIsCreating:Z

.field mIsInAmbientMode:Z

.field private mLastColorInvalidation:J

.field mLastScreenshot:Landroid/graphics/Bitmap;

.field private final mLastSurfaceSize:Landroid/graphics/Point;

.field mLastWindowPage:I

.field final mLayout:Landroid/view/WindowManager$LayoutParams;

.field final mLocalColorAreas:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field final mLocalColorsToAdd:Landroid/util/ArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArraySet<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field final mLock:Ljava/lang/Object;

.field final mMergedConfiguration:Landroid/util/MergedConfiguration;

.field private final mNotifyColorsChanged:Ljava/lang/Runnable;

.field mOffsetMessageEnqueued:Z

.field mOffsetsChanged:Z

.field mPendingMove:Landroid/view/MotionEvent;

.field mPendingSync:Z

.field mPendingXOffset:F

.field mPendingXOffsetStep:F

.field mPendingYOffset:F

.field mPendingYOffsetStep:F

.field mPreviewSurfacePosition:Landroid/graphics/Rect;

.field private mPreviousWallpaperDimAmount:F

.field mReportedVisible:Z

.field final mRequestedVisibilities:Landroid/view/InsetsVisibilities;

.field private mResetWindowPages:Z

.field private mScreenshotSize:Landroid/graphics/Point;

.field private mScreenshotSurfaceControl:Landroid/view/SurfaceControl;

.field mSession:Landroid/view/IWindowSession;

.field mShouldDim:Z

.field mShouldDimByDefault:Z

.field mSurfaceControl:Landroid/view/SurfaceControl;

.field mSurfaceCreated:Z

.field final mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

.field private final mSurfaceSize:Landroid/graphics/Point;

.field final mSyncSeqIdBundle:Landroid/os/Bundle;

.field final mTempControls:[Landroid/view/InsetsSourceControl;

.field private final mTempRect:Landroid/graphics/Rect;

.field private final mTmpMatrix:Landroid/graphics/Matrix;

.field private final mTmpValues:[F

.field mType:I

.field mVisible:Z

.field private mWallpaperDimAmount:F

.field mWidth:I

.field final mWinFrames:Landroid/window/ClientWindowFrames;

.field final mWindow:Lcom/android/internal/view/BaseIWindow;

.field mWindowFlags:I

.field private final mWindowLayout:Landroid/view/WindowLayout;

.field mWindowPages:[Landroid/service/wallpaper/EngineWindowPage;

.field mWindowPrivateFlags:I

.field mWindowToken:Landroid/os/IBinder;

.field mZoom:F

.field final synthetic this$0:Landroid/service/wallpaper/WallpaperService;


# direct methods
.method static bridge synthetic -$$Nest$fgetmDisplay(Landroid/service/wallpaper/WallpaperService$Engine;)Landroid/view/Display;
    .locals 0

    iget-object p0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDisplay:Landroid/view/Display;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDisplayState(Landroid/service/wallpaper/WallpaperService$Engine;)I
    .locals 0

    iget p0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDisplayState:I

    return p0
.end method

.method static bridge synthetic -$$Nest$mdispatchPointer(Landroid/service/wallpaper/WallpaperService$Engine;Landroid/view/MotionEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->dispatchPointer(Landroid/view/MotionEvent;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mscalePreview(Landroid/service/wallpaper/WallpaperService$Engine;Landroid/graphics/Rect;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->scalePreview(Landroid/graphics/Rect;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetPrimaryWallpaperColors(Landroid/service/wallpaper/WallpaperService$Engine;Landroid/app/WallpaperColors;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->setPrimaryWallpaperColors(Landroid/app/WallpaperColors;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateConfiguration(Landroid/service/wallpaper/WallpaperService$Engine;Landroid/util/MergedConfiguration;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->updateConfiguration(Landroid/util/MergedConfiguration;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateWallpaperDimming(Landroid/service/wallpaper/WallpaperService$Engine;F)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->updateWallpaperDimming(F)V

    return-void
.end method

.method public constructor <init>(Landroid/service/wallpaper/WallpaperService;)V
    .locals 2

    new-instance v0, Landroid/service/wallpaper/WallpaperService$Engine$$ExternalSyntheticLambda1;

    invoke-direct {v0}, Landroid/service/wallpaper/WallpaperService$Engine$$ExternalSyntheticLambda1;-><init>()V

    invoke-static {}, Landroid/os/Handler;->getMain()Landroid/os/Handler;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Landroid/service/wallpaper/WallpaperService$Engine;-><init>(Landroid/service/wallpaper/WallpaperService;Ljava/util/function/Supplier;Landroid/os/Handler;)V

    return-void
.end method

.method public constructor <init>(Landroid/service/wallpaper/WallpaperService;Ljava/util/function/Supplier;Landroid/os/Handler;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/function/Supplier<",
            "Ljava/lang/Long;",
            ">;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->this$0:Landroid/service/wallpaper/WallpaperService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/ArraySet;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/util/ArraySet;-><init>(I)V

    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLocalColorAreas:Landroid/util/ArraySet;

    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0, v1}, Landroid/util/ArraySet;-><init>(I)V

    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLocalColorsToAdd:Landroid/util/ArraySet;

    const/4 v0, 0x0

    new-array v1, v0, [Landroid/service/wallpaper/EngineWindowPage;

    iput-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPages:[Landroid/service/wallpaper/EngineWindowPage;

    const/4 v1, -0x1

    iput v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLastWindowPage:I

    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mInitializing:Z

    iput-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mFrozenRequested:Z

    const/4 v1, 0x0

    iput v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mZoom:F

    const/16 v1, 0x10

    iput v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowFlags:I

    const v2, 0x2000004

    iput v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPrivateFlags:I

    iput v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWindowFlags:I

    iput v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWindowPrivateFlags:I

    new-instance v1, Landroid/window/ClientWindowFrames;

    invoke-direct {v1}, Landroid/window/ClientWindowFrames;-><init>()V

    iput-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWinFrames:Landroid/window/ClientWindowFrames;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDispatchedContentInsets:Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDispatchedStableInsets:Landroid/graphics/Rect;

    sget-object v1, Landroid/view/DisplayCutout;->NO_CUTOUT:Landroid/view/DisplayCutout;

    iput-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDispatchedDisplayCutout:Landroid/view/DisplayCutout;

    new-instance v1, Landroid/view/InsetsState;

    invoke-direct {v1}, Landroid/view/InsetsState;-><init>()V

    iput-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mInsetsState:Landroid/view/InsetsState;

    new-instance v1, Landroid/view/InsetsVisibilities;

    invoke-direct {v1}, Landroid/view/InsetsVisibilities;-><init>()V

    iput-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mRequestedVisibilities:Landroid/view/InsetsVisibilities;

    new-array v0, v0, [Landroid/view/InsetsSourceControl;

    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mTempControls:[Landroid/view/InsetsSourceControl;

    new-instance v0, Landroid/util/MergedConfiguration;

    invoke-direct {v0}, Landroid/util/MergedConfiguration;-><init>()V

    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mMergedConfiguration:Landroid/util/MergedConfiguration;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSyncSeqIdBundle:Landroid/os/Bundle;

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceSize:Landroid/graphics/Point;

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLastSurfaceSize:Landroid/graphics/Point;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mTmpMatrix:Landroid/graphics/Matrix;

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mTmpValues:[F

    new-instance v0, Landroid/view/WindowLayout;

    invoke-direct {v0}, Landroid/view/WindowLayout;-><init>()V

    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowLayout:Landroid/view/WindowLayout;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mTempRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLock:Ljava/lang/Object;

    new-instance v0, Landroid/service/wallpaper/WallpaperService$Engine$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0}, Landroid/service/wallpaper/WallpaperService$Engine$$ExternalSyntheticLambda2;-><init>(Landroid/service/wallpaper/WallpaperService$Engine;)V

    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mNotifyColorsChanged:Ljava/lang/Runnable;

    const v0, 0x3d4ccccd    # 0.05f

    iput v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWallpaperDimAmount:F

    iput v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPreviousWallpaperDimAmount:F

    iput v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDefaultDimAmount:F

    new-instance v0, Landroid/view/SurfaceControl;

    invoke-direct {v0}, Landroid/view/SurfaceControl;-><init>()V

    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceControl:Landroid/view/SurfaceControl;

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenshotSize:Landroid/graphics/Point;

    new-instance v0, Landroid/service/wallpaper/WallpaperService$Engine$1;

    invoke-direct {v0, p0}, Landroid/service/wallpaper/WallpaperService$Engine$1;-><init>(Landroid/service/wallpaper/WallpaperService$Engine;)V

    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    new-instance v0, Landroid/service/wallpaper/WallpaperService$Engine$2;

    invoke-direct {v0, p0}, Landroid/service/wallpaper/WallpaperService$Engine$2;-><init>(Landroid/service/wallpaper/WallpaperService$Engine;)V

    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindow:Lcom/android/internal/view/BaseIWindow;

    new-instance v0, Landroid/service/wallpaper/WallpaperService$Engine$4;

    invoke-direct {v0, p0}, Landroid/service/wallpaper/WallpaperService$Engine$4;-><init>(Landroid/service/wallpaper/WallpaperService$Engine;)V

    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    iput-object p2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mClockFunction:Ljava/util/function/Supplier;

    iput-object p3, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private cleanUpScreenshotSurfaceControl()V
    .locals 2

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenshotSurfaceControl:Landroid/view/SurfaceControl;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/view/SurfaceControl$Transaction;

    invoke-direct {v0}, Landroid/view/SurfaceControl$Transaction;-><init>()V

    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenshotSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Transaction;->remove(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    move-result-object v0

    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mBbqSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/SurfaceControl$Transaction;->apply()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenshotSurfaceControl:Landroid/view/SurfaceControl;

    :cond_0
    return-void
.end method

.method private dispatchPointer(Landroid/view/MotionEvent;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->isTouchEvent()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    iput-object p1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingMove:Landroid/view/MotionEvent;

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingMove:Landroid/view/MotionEvent;

    :goto_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCaller:Lcom/android/internal/os/HandlerCaller;

    const/16 v1, 0x2738

    invoke-virtual {v0, v1, p1}, Lcom/android/internal/os/HandlerCaller;->obtainMessageO(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCaller:Lcom/android/internal/os/HandlerCaller;

    invoke-virtual {v1, v0}, Lcom/android/internal/os/HandlerCaller;->sendMessage(Landroid/os/Message;)V

    goto :goto_1

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->recycle()V

    :goto_1
    return-void
.end method

.method private fixRect(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 2

    iget v0, p2, Landroid/graphics/Rect;->left:I

    iget v1, p2, Landroid/graphics/Rect;->right:I

    if-ge v0, v1, :cond_1

    iget v0, p2, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget v0, p2, Landroid/graphics/Rect;->left:I

    if-lez v0, :cond_0

    goto :goto_0

    :cond_0
    iget v0, p2, Landroid/graphics/Rect;->left:I

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x0

    :goto_1
    iput v0, p2, Landroid/graphics/Rect;->left:I

    iget v0, p2, Landroid/graphics/Rect;->left:I

    iget v1, p2, Landroid/graphics/Rect;->right:I

    if-ge v0, v1, :cond_3

    iget v0, p2, Landroid/graphics/Rect;->right:I

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-le v0, v1, :cond_2

    goto :goto_2

    :cond_2
    iget v0, p2, Landroid/graphics/Rect;->right:I

    goto :goto_3

    :cond_3
    :goto_2
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    :goto_3
    iput v0, p2, Landroid/graphics/Rect;->right:I

    return-object p2
.end method

.method private freeze()V
    .locals 1

    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mReportedVisible:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDestroyed:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->showScreenshotOfWallpaper()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/service/wallpaper/WallpaperService$Engine;->doVisibilityChanged(Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mVisible:Z

    return-void

    :cond_2
    :goto_0
    return-void
.end method

.method private generateSubRect(Landroid/graphics/RectF;II)Landroid/graphics/RectF;
    .locals 7

    int-to-float v0, p2

    int-to-float v1, p3

    div-float/2addr v0, v1

    add-int/lit8 v1, p2, 0x1

    int-to-float v1, v1

    int-to-float v2, p3

    div-float/2addr v1, v2

    iget v2, p1, Landroid/graphics/RectF;->left:F

    iget v3, p1, Landroid/graphics/RectF;->right:F

    cmpg-float v4, v2, v0

    if-gez v4, :cond_0

    move v2, v0

    :cond_0
    cmpl-float v4, v3, v1

    if-lez v4, :cond_1

    move v3, v1

    :cond_1
    int-to-float v4, p3

    mul-float/2addr v4, v2

    const/high16 v5, 0x3f800000    # 1.0f

    rem-float/2addr v4, v5

    int-to-float v2, p3

    mul-float/2addr v2, v3

    rem-float/2addr v2, v5

    const/4 v3, 0x0

    cmpl-float v3, v2, v3

    if-nez v3, :cond_2

    const/high16 v2, 0x3f800000    # 1.0f

    :cond_2
    new-instance v3, Landroid/graphics/RectF;

    iget v5, p1, Landroid/graphics/RectF;->top:F

    iget v6, p1, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v3, v4, v5, v2, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v3
.end method

.method private getOrCreateBLASTSurface(III)Landroid/view/Surface;
    .locals 8

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mBlastBufferQueue:Landroid/graphics/BLASTBufferQueue;

    if-nez v1, :cond_0

    new-instance v1, Landroid/graphics/BLASTBufferQueue;

    iget-object v4, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mBbqSurfaceControl:Landroid/view/SurfaceControl;

    const-string v3, "Wallpaper"

    move-object v2, v1

    move v5, p1

    move v6, p2

    move v7, p3

    invoke-direct/range {v2 .. v7}, Landroid/graphics/BLASTBufferQueue;-><init>(Ljava/lang/String;Landroid/view/SurfaceControl;III)V

    iput-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mBlastBufferQueue:Landroid/graphics/BLASTBufferQueue;

    invoke-virtual {v1}, Landroid/graphics/BLASTBufferQueue;->createSurface()Landroid/view/Surface;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mBbqSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v1, v2, p1, p2, p3}, Landroid/graphics/BLASTBufferQueue;->update(Landroid/view/SurfaceControl;III)V

    :goto_0
    return-object v0
.end method

.method private getRectFPage(Landroid/graphics/RectF;F)I
    .locals 4

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->this$0:Landroid/service/wallpaper/WallpaperService;

    invoke-static {v0, p1}, Landroid/service/wallpaper/WallpaperService;->-$$Nest$misValid(Landroid/service/wallpaper/WallpaperService;Landroid/graphics/RectF;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-direct {p0, p2}, Landroid/service/wallpaper/WallpaperService$Engine;->validStep(F)Z

    move-result v0

    if-nez v0, :cond_1

    return v1

    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    div-float/2addr v0, p2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    int-to-float v2, v0

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    if-ne v1, v0, :cond_2

    add-int/lit8 v2, v0, -0x1

    return v2

    :cond_2
    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPages:[Landroid/service/wallpaper/EngineWindowPage;

    array-length v3, v2

    if-ne v1, v3, :cond_3

    array-length v2, v2

    add-int/lit8 v1, v2, -0x1

    :cond_3
    return v1
.end method

.method private initWindowPages([Landroid/service/wallpaper/EngineWindowPage;F)V
    .locals 4

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    new-instance v1, Landroid/service/wallpaper/EngineWindowPage;

    invoke-direct {v1}, Landroid/service/wallpaper/EngineWindowPage;-><init>()V

    aput-object v1, p1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLocalColorAreas:Landroid/util/ArraySet;

    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLocalColorsToAdd:Landroid/util/ArraySet;

    invoke-virtual {v0, v1}, Landroid/util/ArraySet;->addAll(Landroid/util/ArraySet;)V

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLocalColorsToAdd:Landroid/util/ArraySet;

    invoke-virtual {v0}, Landroid/util/ArraySet;->clear()V

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLocalColorAreas:Landroid/util/ArraySet;

    invoke-virtual {v0}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->this$0:Landroid/service/wallpaper/WallpaperService;

    invoke-static {v2, v1}, Landroid/service/wallpaper/WallpaperService;->-$$Nest$misValid(Landroid/service/wallpaper/WallpaperService;Landroid/graphics/RectF;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLocalColorAreas:Landroid/util/ArraySet;

    invoke-virtual {v2, v1}, Landroid/util/ArraySet;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-direct {p0, v1, p2}, Landroid/service/wallpaper/WallpaperService$Engine;->getRectFPage(Landroid/graphics/RectF;F)I

    move-result v2

    aget-object v3, p1, v2

    invoke-virtual {v3, v1}, Landroid/service/wallpaper/EngineWindowPage;->addArea(Landroid/graphics/RectF;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method private processLocalColors(FF)V
    .locals 10

    invoke-virtual {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->supportsLocalColorExtraction()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    rem-float v0, p1, p2

    const v1, 0x3d4ccccd    # 0.05f

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_3

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    invoke-virtual {v0}, Lcom/android/internal/view/BaseSurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Surface;->isValid()Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    invoke-direct {p0, p2}, Landroid/service/wallpaper/WallpaperService$Engine;->validStep(F)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 p1, 0x0

    const/high16 p2, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/high16 v0, 0x3f800000    # 1.0f

    div-float v1, v0, p2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    int-to-float v2, v1

    div-float p2, v0, v2

    add-int/lit8 v0, v1, -0x1

    int-to-float v0, v0

    int-to-float v2, v1

    div-float/2addr v0, v2

    mul-float/2addr p1, v0

    div-float v2, p1, p2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    move v0, v2

    :goto_0
    move v7, p2

    move v8, p1

    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mHandler:Landroid/os/Handler;

    new-instance v9, Landroid/service/wallpaper/WallpaperService$Engine$$ExternalSyntheticLambda5;

    move-object v3, v9

    move-object v4, p0

    move v5, v0

    move v6, v1

    invoke-direct/range {v3 .. v8}, Landroid/service/wallpaper/WallpaperService$Engine$$ExternalSyntheticLambda5;-><init>(Landroid/service/wallpaper/WallpaperService$Engine;IIFF)V

    invoke-virtual {v2, v9}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :cond_3
    :goto_1
    return-void
.end method

.method private reposition()V
    .locals 7

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPreviewSurfacePosition:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mTmpMatrix:Landroid/graphics/Matrix;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPreviewSurfacePosition:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    invoke-virtual {v1, v0, v2}, Landroid/graphics/Matrix;->setTranslate(FF)V

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mTmpMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPreviewSurfacePosition:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWidth:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPreviewSurfacePosition:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurHeight:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mTmpMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mTmpValues:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    new-instance v0, Landroid/view/SurfaceControl$Transaction;

    invoke-direct {v0}, Landroid/view/SurfaceControl$Transaction;-><init>()V

    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceControl:Landroid/view/SurfaceControl;

    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPreviewSurfacePosition:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iget-object v3, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPreviewSurfacePosition:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/SurfaceControl$Transaction;->setPosition(Landroid/view/SurfaceControl;FF)Landroid/view/SurfaceControl$Transaction;

    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceControl:Landroid/view/SurfaceControl;

    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mTmpValues:[F

    const/4 v3, 0x0

    aget v3, v1, v3

    const/4 v4, 0x3

    aget v4, v1, v4

    const/4 v5, 0x1

    aget v5, v1, v5

    const/4 v6, 0x4

    aget v6, v1, v6

    move-object v1, v0

    invoke-virtual/range {v1 .. v6}, Landroid/view/SurfaceControl$Transaction;->setMatrix(Landroid/view/SurfaceControl;FFFF)Landroid/view/SurfaceControl$Transaction;

    invoke-virtual {v0}, Landroid/view/SurfaceControl$Transaction;->apply()V

    return-void
.end method

.method private resetWindowPages()V
    .locals 4

    invoke-virtual {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->supportsLocalColorExtraction()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mResetWindowPages:Z

    if-nez v0, :cond_1

    return-void

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mResetWindowPages:Z

    const/4 v0, -0x1

    iput v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLastWindowPage:I

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPages:[Landroid/service/wallpaper/EngineWindowPage;

    array-length v2, v1

    if-ge v0, v2, :cond_2

    aget-object v1, v1, v0

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/service/wallpaper/EngineWindowPage;->setLastUpdateTime(J)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private scalePreview(Landroid/graphics/Rect;)V
    .locals 1

    invoke-virtual {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->isPreview()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPreviewSurfacePosition:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPreviewSurfacePosition:Landroid/graphics/Rect;

    if-eqz v0, :cond_3

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    iput-object p1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPreviewSurfacePosition:Landroid/graphics/Rect;

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v0}, Landroid/view/SurfaceControl;->isValid()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->reposition()V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0, v0}, Landroid/service/wallpaper/WallpaperService$Engine;->updateSurface(ZZZ)V

    :cond_3
    :goto_0
    return-void
.end method

.method private setPrimaryWallpaperColors(Landroid/app/WallpaperColors;)V
    .locals 4

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/app/WallpaperColors;->getColorHints()I

    move-result v0

    and-int/lit8 v1, v0, 0x1

    if-nez v1, :cond_1

    and-int/lit8 v1, v0, 0x2

    if-nez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mShouldDimByDefault:Z

    iget-boolean v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mShouldDim:Z

    if-eq v1, v2, :cond_2

    iget v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWallpaperDimAmount:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    iput-boolean v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mShouldDim:Z

    invoke-direct {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->updateSurfaceDimming()V

    :cond_2
    return-void
.end method

.method private showScreenshotOfWallpaper()Z
    .locals 7

    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDestroyed:Z

    const/4 v1, 0x0

    if-nez v0, :cond_4

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceControl:Landroid/view/SurfaceControl;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/view/SurfaceControl;->isValid()Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_0

    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceSize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget-object v3, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceSize:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    invoke-direct {v0, v1, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v2

    const-string v3, "WallpaperService"

    if-eqz v2, :cond_1

    const-string v2, "Failed to screenshot wallpaper: surface bounds are empty"

    invoke-static {v3, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_1
    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenshotSurfaceControl:Landroid/view/SurfaceControl;

    if-eqz v2, :cond_2

    const-string v2, "Screenshot is unexpectedly not null"

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->cleanUpScreenshotSurfaceControl()V

    :cond_2
    new-instance v2, Landroid/view/SurfaceControl$LayerCaptureArgs$Builder;

    iget-object v4, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceControl:Landroid/view/SurfaceControl;

    invoke-direct {v2, v4}, Landroid/view/SurfaceControl$LayerCaptureArgs$Builder;-><init>(Landroid/view/SurfaceControl;)V

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v2, v4, v5}, Landroid/view/SurfaceControl$LayerCaptureArgs$Builder;->setUid(J)Landroid/view/SurfaceControl$CaptureArgs$Builder;

    move-result-object v2

    check-cast v2, Landroid/view/SurfaceControl$LayerCaptureArgs$Builder;

    invoke-virtual {v2, v1}, Landroid/view/SurfaceControl$LayerCaptureArgs$Builder;->setChildrenOnly(Z)Landroid/view/SurfaceControl$LayerCaptureArgs$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/SurfaceControl$LayerCaptureArgs$Builder;->setSourceCrop(Landroid/graphics/Rect;)Landroid/view/SurfaceControl$CaptureArgs$Builder;

    move-result-object v2

    check-cast v2, Landroid/view/SurfaceControl$LayerCaptureArgs$Builder;

    invoke-virtual {v2}, Landroid/view/SurfaceControl$LayerCaptureArgs$Builder;->build()Landroid/view/SurfaceControl$LayerCaptureArgs;

    move-result-object v2

    invoke-static {v2}, Landroid/view/SurfaceControl;->captureLayers(Landroid/view/SurfaceControl$LayerCaptureArgs;)Landroid/view/SurfaceControl$ScreenshotHardwareBuffer;

    move-result-object v2

    if-nez v2, :cond_3

    const-string v4, "Failed to screenshot wallpaper: screenshotBuffer is null"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_3
    invoke-virtual {v2}, Landroid/view/SurfaceControl$ScreenshotHardwareBuffer;->getHardwareBuffer()Landroid/hardware/HardwareBuffer;

    move-result-object v1

    new-instance v3, Landroid/view/SurfaceControl$Transaction;

    invoke-direct {v3}, Landroid/view/SurfaceControl$Transaction;-><init>()V

    new-instance v4, Landroid/view/SurfaceControl$Builder;

    invoke-direct {v4}, Landroid/view/SurfaceControl$Builder;-><init>()V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Wallpaper snapshot for engine "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/SurfaceControl$Builder;->setName(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/hardware/HardwareBuffer;->getFormat()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/SurfaceControl$Builder;->setFormat(I)Landroid/view/SurfaceControl$Builder;

    move-result-object v4

    iget-object v5, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v4, v5}, Landroid/view/SurfaceControl$Builder;->setParent(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Builder;

    move-result-object v4

    invoke-virtual {v2}, Landroid/view/SurfaceControl$ScreenshotHardwareBuffer;->containsSecureLayers()Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/SurfaceControl$Builder;->setSecure(Z)Landroid/view/SurfaceControl$Builder;

    move-result-object v4

    const-string v5, "WallpaperService.Engine.showScreenshotOfWallpaper"

    invoke-virtual {v4, v5}, Landroid/view/SurfaceControl$Builder;->setCallsite(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/SurfaceControl$Builder;->setBLASTLayer()Landroid/view/SurfaceControl$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/SurfaceControl$Builder;->build()Landroid/view/SurfaceControl;

    move-result-object v4

    iput-object v4, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenshotSurfaceControl:Landroid/view/SurfaceControl;

    iget-object v4, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenshotSize:Landroid/graphics/Point;

    iget-object v5, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceSize:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    iget-object v6, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceSize:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Point;->set(II)V

    iget-object v4, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenshotSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v3, v4, v1}, Landroid/view/SurfaceControl$Transaction;->setBuffer(Landroid/view/SurfaceControl;Landroid/hardware/HardwareBuffer;)Landroid/view/SurfaceControl$Transaction;

    iget-object v4, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenshotSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v2}, Landroid/view/SurfaceControl$ScreenshotHardwareBuffer;->getColorSpace()Landroid/graphics/ColorSpace;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/view/SurfaceControl$Transaction;->setColorSpace(Landroid/view/SurfaceControl;Landroid/graphics/ColorSpace;)Landroid/view/SurfaceControl$Transaction;

    iget-object v4, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenshotSurfaceControl:Landroid/view/SurfaceControl;

    const v5, 0x7fffffff

    invoke-virtual {v3, v4, v5}, Landroid/view/SurfaceControl$Transaction;->setLayer(Landroid/view/SurfaceControl;I)Landroid/view/SurfaceControl$Transaction;

    iget-object v4, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenshotSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v3, v4}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    iget-object v4, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mBbqSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v3, v4}, Landroid/view/SurfaceControl$Transaction;->hide(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;

    invoke-virtual {v3}, Landroid/view/SurfaceControl$Transaction;->apply()V

    const/4 v4, 0x1

    return v4

    :cond_4
    :goto_0
    return v1
.end method

.method private unfreeze()V
    .locals 1

    invoke-direct {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->cleanUpScreenshotSurfaceControl()V

    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mVisible:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/service/wallpaper/WallpaperService$Engine;->doVisibilityChanged(Z)V

    :cond_0
    return-void
.end method

.method private updateConfiguration(Landroid/util/MergedConfiguration;)V
    .locals 1

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mMergedConfiguration:Landroid/util/MergedConfiguration;

    invoke-virtual {v0, p1}, Landroid/util/MergedConfiguration;->setTo(Landroid/util/MergedConfiguration;)V

    return-void
.end method

.method private updateFrozenState(Z)V
    .locals 1

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    iget-object v0, v0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mWallpaperManager:Landroid/app/WallpaperManager;

    invoke-virtual {v0}, Landroid/app/WallpaperManager;->getWallpaperInfo()Landroid/app/WallpaperInfo;

    move-result-object v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    return-void

    :cond_0
    iput-boolean p1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mFrozenRequested:Z

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenshotSurfaceControl:Landroid/view/SurfaceControl;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-ne p1, v0, :cond_2

    return-void

    :cond_2
    if-eqz p1, :cond_3

    invoke-direct {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->freeze()V

    goto :goto_1

    :cond_3
    invoke-direct {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->unfreeze()V

    :goto_1
    return-void
.end method

.method private updatePageColors(Landroid/service/wallpaper/EngineWindowPage;IIF)V
    .locals 17

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    const-string v3, "WallpaperService"

    invoke-virtual/range {p1 .. p1}, Landroid/service/wallpaper/EngineWindowPage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v0, "WallpaperService#updatePageColors"

    invoke-static {v0}, Landroid/os/Trace;->beginSection(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/service/wallpaper/EngineWindowPage;->getAreas()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/graphics/RectF;

    if-nez v5, :cond_1

    goto :goto_0

    :cond_1
    move/from16 v6, p2

    move/from16 v7, p3

    invoke-direct {v1, v5, v6, v7}, Landroid/service/wallpaper/WallpaperService$Engine;->generateSubRect(Landroid/graphics/RectF;II)Landroid/graphics/RectF;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Landroid/service/wallpaper/EngineWindowPage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v10, v8, Landroid/graphics/RectF;->left:F

    mul-float/2addr v0, v10

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v10

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v11, v8, Landroid/graphics/RectF;->top:F

    mul-float/2addr v0, v11

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v11

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v12

    mul-float/2addr v0, v12

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v12

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v13

    mul-float/2addr v0, v13

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v13

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/service/wallpaper/EngineWindowPage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0, v10, v11, v12, v13}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-object v14, v0

    nop

    iget v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWallpaperDimAmount:F

    invoke-static {v14, v0}, Landroid/app/WallpaperColors;->fromBitmap(Landroid/graphics/Bitmap;F)Landroid/app/WallpaperColors;

    move-result-object v15

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->recycle()V

    move-object/from16 v16, v4

    invoke-virtual {v2, v5}, Landroid/service/wallpaper/EngineWindowPage;->getColors(Landroid/graphics/RectF;)Landroid/app/WallpaperColors;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v15, v4}, Landroid/app/WallpaperColors;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    invoke-virtual {v2, v5, v15}, Landroid/service/wallpaper/EngineWindowPage;->addWallpaperColors(Landroid/graphics/RectF;Landroid/app/WallpaperColors;)V

    :try_start_1
    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mConnection:Landroid/service/wallpaper/IWallpaperConnection;

    iget-object v2, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mDisplayContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getDisplayId()I

    move-result v2

    invoke-interface {v0, v5, v15, v2}, Landroid/service/wallpaper/IWallpaperConnection;->onLocalWallpaperColorsChanged(Landroid/graphics/RectF;Landroid/app/WallpaperColors;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v2, "Error calling Connection.onLocalWallpaperColorsChanged"

    invoke-static {v3, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_3
    :goto_1
    move-object/from16 v2, p1

    move-object/from16 v4, v16

    goto/16 :goto_0

    :catch_1
    move-exception v0

    move-object/from16 v16, v4

    const-string v2, "Error creating page local color bitmap"

    invoke-static {v3, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object/from16 v2, p1

    move-object/from16 v4, v16

    goto/16 :goto_0

    :cond_4
    move/from16 v6, p2

    move/from16 v7, p3

    invoke-static {}, Landroid/os/Trace;->endSection()V

    return-void
.end method

.method private updateSurfaceDimming()V
    .locals 6

    invoke-static {}, Landroid/service/wallpaper/WallpaperService;->-$$Nest$sfgetENABLE_WALLPAPER_DIMMING()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mBbqSurfaceControl:Landroid/view/SurfaceControl;

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    new-instance v0, Landroid/view/SurfaceControl$Transaction;

    invoke-direct {v0}, Landroid/view/SurfaceControl$Transaction;-><init>()V

    invoke-virtual {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->isPreview()Z

    move-result v1

    const/4 v2, 0x1

    const-string v3, "WallpaperService"

    const/4 v4, 0x0

    if-nez v1, :cond_1

    iget-boolean v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mShouldDim:Z

    if-nez v1, :cond_2

    :cond_1
    iget v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPreviousWallpaperDimAmount:F

    iget v5, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWallpaperDimAmount:F

    cmpl-float v1, v1, v5

    if-eqz v1, :cond_3

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Setting wallpaper dimming: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v5, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWallpaperDimAmount:F

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x2

    new-array v1, v1, [F

    iget v3, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPreviousWallpaperDimAmount:F

    aput v3, v1, v4

    iget v3, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWallpaperDimAmount:F

    aput v3, v1, v2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    new-instance v2, Landroid/service/wallpaper/WallpaperService$Engine$$ExternalSyntheticLambda3;

    invoke-direct {v2, p0, v0}, Landroid/service/wallpaper/WallpaperService$Engine$$ExternalSyntheticLambda3;-><init>(Landroid/service/wallpaper/WallpaperService$Engine;Landroid/view/SurfaceControl$Transaction;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v2, Landroid/service/wallpaper/WallpaperService$Engine$3;

    invoke-direct {v2, p0}, Landroid/service/wallpaper/WallpaperService$Engine$3;-><init>(Landroid/service/wallpaper/WallpaperService$Engine;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    :cond_3
    const-string v1, "Setting wallpaper dimming: 0"

    invoke-static {v3, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mBbqSurfaceControl:Landroid/view/SurfaceControl;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v3}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/SurfaceControl$Transaction;->apply()V

    invoke-virtual {p0, v4, v4, v2}, Landroid/service/wallpaper/WallpaperService$Engine;->updateSurface(ZZZ)V

    :goto_0
    iget v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWallpaperDimAmount:F

    iput v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPreviousWallpaperDimAmount:F

    return-void

    :cond_4
    :goto_1
    return-void
.end method

.method private updateWallpaperDimming(F)V
    .locals 1

    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWallpaperDimAmount:F

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDefaultDimAmount:F

    invoke-static {v0, p1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWallpaperDimAmount:F

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mShouldDimByDefault:Z

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mShouldDim:Z

    invoke-direct {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->updateSurfaceDimming()V

    return-void
.end method

.method private validStep(F)Z
    .locals 4

    invoke-static {}, Landroid/service/wallpaper/WallpaperService;->-$$Nest$sfgetPROHIBITED_STEPS()Ljava/util/List;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    float-to-double v0, p1

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    float-to-double v0, p1

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public addLocalColorsAreas(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/graphics/RectF;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->supportsLocalColorExtraction()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mHandler:Landroid/os/Handler;

    new-instance v1, Landroid/service/wallpaper/WallpaperService$Engine$$ExternalSyntheticLambda6;

    invoke-direct {v1, p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine$$ExternalSyntheticLambda6;-><init>(Landroid/service/wallpaper/WallpaperService$Engine;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method attach(Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;)V
    .locals 3

    goto/32 :goto_2d

    nop

    :goto_0
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    goto/32 :goto_9

    nop

    :goto_1
    const/16 v1, 0x7dd

    goto/32 :goto_2e

    nop

    :goto_2
    invoke-virtual {v0}, Landroid/view/Display;->getState()I

    move-result v0

    goto/32 :goto_1c

    nop

    :goto_3
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    goto/32 :goto_a

    nop

    :goto_4
    invoke-virtual {p0, v0}, Landroid/service/wallpaper/WallpaperService$Engine;->onCreate(Landroid/view/SurfaceHolder;)V

    goto/32 :goto_23

    nop

    :goto_5
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindow:Lcom/android/internal/view/BaseIWindow;

    goto/32 :goto_c

    nop

    :goto_6
    invoke-virtual {p0, v0, v0, v0}, Landroid/service/wallpaper/WallpaperService$Engine;->updateSurface(ZZZ)V

    goto/32 :goto_8

    nop

    :goto_7
    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowToken:Landroid/os/IBinder;

    goto/32 :goto_3

    nop

    :goto_8
    return-void

    :goto_9
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->this$0:Landroid/service/wallpaper/WallpaperService;

    goto/32 :goto_2b

    nop

    :goto_a
    invoke-virtual {v0}, Lcom/android/internal/view/BaseSurfaceHolder;->setSizeFromLayout()V

    goto/32 :goto_34

    nop

    :goto_b
    invoke-virtual {v0}, Landroid/view/Display;->getInstallOrientation()I

    move-result v0

    goto/32 :goto_36

    nop

    :goto_c
    invoke-virtual {v1, v0}, Lcom/android/internal/view/BaseIWindow;->setSession(Landroid/view/IWindowSession;)V

    goto/32 :goto_0

    nop

    :goto_d
    iget-object v0, v0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    goto/32 :goto_31

    nop

    :goto_e
    invoke-virtual {v0, v1, v2}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V

    goto/32 :goto_2a

    nop

    :goto_f
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDisplay:Landroid/view/Display;

    goto/32 :goto_b

    nop

    :goto_10
    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCaller:Lcom/android/internal/os/HandlerCaller;

    goto/32 :goto_13

    nop

    :goto_11
    invoke-virtual {v1, v0}, Landroid/service/wallpaper/WallpaperService;->createDisplayContext(Landroid/view/Display;)Landroid/content/Context;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_12
    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDisplayContext:Landroid/content/Context;

    goto/32 :goto_25

    nop

    :goto_13
    invoke-virtual {v2}, Lcom/android/internal/os/HandlerCaller;->getHandler()Landroid/os/Handler;

    move-result-object v2

    goto/32 :goto_e

    nop

    :goto_14
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->this$0:Landroid/service/wallpaper/WallpaperService;

    goto/32 :goto_11

    nop

    :goto_15
    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCaller:Lcom/android/internal/os/HandlerCaller;

    goto/32 :goto_32

    nop

    :goto_16
    iput-object p1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    goto/32 :goto_1d

    nop

    :goto_17
    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mConnection:Landroid/service/wallpaper/IWallpaperConnection;

    goto/32 :goto_37

    nop

    :goto_18
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->createWindowContext(ILandroid/os/Bundle;)Landroid/content/Context;

    move-result-object v0

    goto/32 :goto_12

    nop

    :goto_19
    iget-object v0, v0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mDisplay:Landroid/view/Display;

    goto/32 :goto_35

    nop

    :goto_1a
    iput-object v1, v0, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    goto/32 :goto_29

    nop

    :goto_1b
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v0

    goto/32 :goto_20

    nop

    :goto_1c
    iput v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDisplayState:I

    goto/32 :goto_f

    nop

    :goto_1d
    invoke-static {p1}, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->-$$Nest$fgetmCaller(Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;)Lcom/android/internal/os/HandlerCaller;

    move-result-object v0

    goto/32 :goto_15

    nop

    :goto_1e
    invoke-static {}, Landroid/view/WindowManagerGlobal;->getWindowSession()Landroid/view/IWindowSession;

    move-result-object v0

    goto/32 :goto_2f

    nop

    :goto_1f
    iput v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPreviousWallpaperDimAmount:F

    goto/32 :goto_28

    nop

    :goto_20
    iput v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDefaultDimAmount:F

    goto/32 :goto_38

    nop

    :goto_21
    if-nez v0, :cond_0

    goto/32 :goto_27

    :cond_0
    goto/32 :goto_26

    nop

    :goto_22
    iput-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mInitializing:Z

    goto/32 :goto_1e

    nop

    :goto_23
    const/4 v0, 0x0

    goto/32 :goto_30

    nop

    :goto_24
    const v1, 0x10500e6

    goto/32 :goto_1b

    nop

    :goto_25
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    goto/32 :goto_24

    nop

    :goto_26
    return-void

    :goto_27
    goto/32 :goto_16

    nop

    :goto_28
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDisplay:Landroid/view/Display;

    goto/32 :goto_2

    nop

    :goto_29
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    goto/32 :goto_d

    nop

    :goto_2a
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    goto/32 :goto_19

    nop

    :goto_2b
    invoke-virtual {v1}, Landroid/service/wallpaper/WallpaperService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_1a

    nop

    :goto_2c
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    goto/32 :goto_4

    nop

    :goto_2d
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDestroyed:Z

    goto/32 :goto_21

    nop

    :goto_2e
    const/4 v2, 0x0

    goto/32 :goto_18

    nop

    :goto_2f
    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSession:Landroid/view/IWindowSession;

    goto/32 :goto_5

    nop

    :goto_30
    iput-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mInitializing:Z

    goto/32 :goto_33

    nop

    :goto_31
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    goto/32 :goto_10

    nop

    :goto_32
    iget-object v0, p1, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mConnection:Landroid/service/wallpaper/IWallpaperConnection;

    goto/32 :goto_17

    nop

    :goto_33
    iput-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mReportedVisible:Z

    goto/32 :goto_6

    nop

    :goto_34
    const/4 v0, 0x1

    goto/32 :goto_22

    nop

    :goto_35
    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDisplay:Landroid/view/Display;

    goto/32 :goto_14

    nop

    :goto_36
    iput v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDisplayInstallOrientation:I

    goto/32 :goto_2c

    nop

    :goto_37
    iget-object v0, p1, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mWindowToken:Landroid/os/IBinder;

    goto/32 :goto_7

    nop

    :goto_38
    iput v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWallpaperDimAmount:F

    goto/32 :goto_1f

    nop
.end method

.method detach()V
    .locals 3

    goto/32 :goto_1

    nop

    :goto_0
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    goto/32 :goto_8

    nop

    :goto_1
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDestroyed:Z

    goto/32 :goto_a

    nop

    :goto_2
    goto :goto_3

    :catch_0
    move-exception v0

    :goto_3
    goto/32 :goto_0

    nop

    :goto_4
    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    goto/32 :goto_6

    nop

    :goto_5
    return-void

    :goto_6
    iput-boolean v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCreated:Z

    :goto_7
    goto/32 :goto_5

    nop

    :goto_8
    iget-object v0, v0, Lcom/android/internal/view/BaseSurfaceHolder;->mSurface:Landroid/view/Surface;

    goto/32 :goto_4

    nop

    :goto_9
    if-nez v0, :cond_0

    goto/32 :goto_1d

    :cond_0
    goto/32 :goto_1e

    nop

    :goto_a
    if-nez v0, :cond_1

    goto/32 :goto_1a

    :cond_1
    goto/32 :goto_19

    nop

    :goto_b
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mVisible:Z

    goto/32 :goto_13

    nop

    :goto_c
    iput-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDestroyed:Z

    goto/32 :goto_11

    nop

    :goto_d
    invoke-virtual {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->onDestroy()V

    goto/32 :goto_15

    nop

    :goto_e
    invoke-static {p0}, Landroid/animation/AnimationHandler;->removeRequestor(Ljava/lang/Object;)V

    goto/32 :goto_1f

    nop

    :goto_f
    if-nez v0, :cond_2

    goto/32 :goto_7

    :cond_2
    :try_start_0
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mInputEventReceiver:Landroid/service/wallpaper/WallpaperService$Engine$WallpaperInputEventReceiver;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/service/wallpaper/WallpaperService$Engine$WallpaperInputEventReceiver;->dispose()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mInputEventReceiver:Landroid/service/wallpaper/WallpaperService$Engine$WallpaperInputEventReceiver;

    :cond_3
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSession:Landroid/view/IWindowSession;

    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindow:Lcom/android/internal/view/BaseIWindow;

    invoke-interface {v0, v2}, Landroid/view/IWindowSession;->remove(Landroid/view/IWindow;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_2

    nop

    :goto_10
    invoke-virtual {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->reportSurfaceDestroyed()V

    goto/32 :goto_d

    nop

    :goto_11
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    goto/32 :goto_20

    nop

    :goto_12
    iget-object v0, v0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    goto/32 :goto_16

    nop

    :goto_13
    const/4 v1, 0x0

    goto/32 :goto_9

    nop

    :goto_14
    if-nez v0, :cond_4

    goto/32 :goto_18

    :cond_4
    goto/32 :goto_1b

    nop

    :goto_15
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCreated:Z

    goto/32 :goto_f

    nop

    :goto_16
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    goto/32 :goto_17

    nop

    :goto_17
    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->unregisterDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;)V

    :goto_18
    goto/32 :goto_b

    nop

    :goto_19
    return-void

    :goto_1a
    goto/32 :goto_e

    nop

    :goto_1b
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    goto/32 :goto_12

    nop

    :goto_1c
    invoke-virtual {p0, v1}, Landroid/service/wallpaper/WallpaperService$Engine;->onVisibilityChanged(Z)V

    :goto_1d
    goto/32 :goto_10

    nop

    :goto_1e
    iput-boolean v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mVisible:Z

    goto/32 :goto_1c

    nop

    :goto_1f
    const/4 v0, 0x1

    goto/32 :goto_c

    nop

    :goto_20
    iget-object v0, v0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    goto/32 :goto_14

    nop
.end method

.method public doAmbientModeChanged(ZJ)V
    .locals 1

    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDestroyed:Z

    if-nez v0, :cond_0

    iput-boolean p1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIsInAmbientMode:Z

    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCreated:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2, p3}, Landroid/service/wallpaper/WallpaperService$Engine;->onAmbientModeChanged(ZJ)V

    :cond_0
    return-void
.end method

.method doCommand(Landroid/service/wallpaper/WallpaperService$WallpaperCommand;)V
    .locals 8

    goto/32 :goto_11

    nop

    :goto_0
    const-string v1, "android.wallpaper.freeze"

    goto/32 :goto_1c

    nop

    :goto_1
    iget v5, p1, Landroid/service/wallpaper/WallpaperService$WallpaperCommand;->z:I

    goto/32 :goto_1a

    nop

    :goto_2
    invoke-direct {p0, v0}, Landroid/service/wallpaper/WallpaperService$Engine;->updateFrozenState(Z)V

    :goto_3
    goto/32 :goto_c

    nop

    :goto_4
    iget-boolean v1, p1, Landroid/service/wallpaper/WallpaperService$WallpaperCommand;->sync:Z

    goto/32 :goto_1f

    nop

    :goto_5
    iget-object v0, p1, Landroid/service/wallpaper/WallpaperService$WallpaperCommand;->action:Ljava/lang/String;

    goto/32 :goto_b

    nop

    :goto_6
    const-string v1, "android.wallpaper.unfreeze"

    goto/32 :goto_7

    nop

    :goto_7
    if-eqz v0, :cond_0

    goto/32 :goto_1e

    :cond_0
    goto/32 :goto_5

    nop

    :goto_8
    return-void

    :goto_9
    iget-boolean v7, p1, Landroid/service/wallpaper/WallpaperService$WallpaperCommand;->sync:Z

    goto/32 :goto_20

    nop

    :goto_a
    xor-int/lit8 v0, v0, 0x1

    goto/32 :goto_2

    nop

    :goto_b
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_1d

    nop

    :goto_c
    iget-object v2, p1, Landroid/service/wallpaper/WallpaperService$WallpaperCommand;->action:Ljava/lang/String;

    goto/32 :goto_d

    nop

    :goto_d
    iget v3, p1, Landroid/service/wallpaper/WallpaperService$WallpaperCommand;->x:I

    goto/32 :goto_10

    nop

    :goto_e
    iget-object v0, p1, Landroid/service/wallpaper/WallpaperService$WallpaperCommand;->action:Ljava/lang/String;

    goto/32 :goto_0

    nop

    :goto_f
    invoke-virtual/range {v1 .. v7}, Landroid/service/wallpaper/WallpaperService$Engine;->onCommand(Ljava/lang/String;IIILandroid/os/Bundle;Z)Landroid/os/Bundle;

    move-result-object v0

    goto/32 :goto_13

    nop

    :goto_10
    iget v4, p1, Landroid/service/wallpaper/WallpaperService$WallpaperCommand;->y:I

    goto/32 :goto_1

    nop

    :goto_11
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDestroyed:Z

    goto/32 :goto_17

    nop

    :goto_12
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_a

    nop

    :goto_13
    goto :goto_19

    :goto_14
    goto/32 :goto_18

    nop

    :goto_15
    goto :goto_16

    :catch_0
    move-exception v1

    :goto_16
    goto/32 :goto_8

    nop

    :goto_17
    if-eqz v0, :cond_1

    goto/32 :goto_14

    :cond_1
    goto/32 :goto_e

    nop

    :goto_18
    const/4 v0, 0x0

    :goto_19
    goto/32 :goto_4

    nop

    :goto_1a
    iget-object v6, p1, Landroid/service/wallpaper/WallpaperService$WallpaperCommand;->extras:Landroid/os/Bundle;

    goto/32 :goto_9

    nop

    :goto_1b
    iget-object v0, p1, Landroid/service/wallpaper/WallpaperService$WallpaperCommand;->action:Ljava/lang/String;

    goto/32 :goto_12

    nop

    :goto_1c
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_6

    nop

    :goto_1d
    if-nez v0, :cond_2

    goto/32 :goto_3

    :cond_2
    :goto_1e
    goto/32 :goto_1b

    nop

    :goto_1f
    if-nez v1, :cond_3

    goto/32 :goto_16

    :cond_3
    :try_start_0
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSession:Landroid/view/IWindowSession;

    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindow:Lcom/android/internal/view/BaseIWindow;

    invoke-virtual {v2}, Lcom/android/internal/view/BaseIWindow;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/view/IWindowSession;->wallpaperCommandComplete(Landroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_15

    nop

    :goto_20
    move-object v1, p0

    goto/32 :goto_f

    nop
.end method

.method doDesiredSizeChanged(II)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    goto/32 :goto_4

    nop

    :goto_1
    const/4 v0, 0x1

    goto/32 :goto_6

    nop

    :goto_2
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDestroyed:Z

    goto/32 :goto_a

    nop

    :goto_3
    iput p1, v0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mReqWidth:I

    goto/32 :goto_0

    nop

    :goto_4
    iput p2, v0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mReqHeight:I

    goto/32 :goto_9

    nop

    :goto_5
    return-void

    :goto_6
    invoke-virtual {p0, v0}, Landroid/service/wallpaper/WallpaperService$Engine;->doOffsetsChanged(Z)V

    :goto_7
    goto/32 :goto_5

    nop

    :goto_8
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    goto/32 :goto_3

    nop

    :goto_9
    invoke-virtual {p0, p1, p2}, Landroid/service/wallpaper/WallpaperService$Engine;->onDesiredSizeChanged(II)V

    goto/32 :goto_1

    nop

    :goto_a
    if-eqz v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_8

    nop
.end method

.method doDisplayPaddingChanged(Landroid/graphics/Rect;)V
    .locals 2

    goto/32 :goto_b

    nop

    :goto_0
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    goto/32 :goto_a

    nop

    :goto_1
    const/4 v1, 0x0

    goto/32 :goto_7

    nop

    :goto_2
    iget-object v0, v0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mDisplayPadding:Landroid/graphics/Rect;

    goto/32 :goto_4

    nop

    :goto_3
    if-eqz v0, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_0

    nop

    :goto_4
    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto/32 :goto_6

    nop

    :goto_5
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    goto/32 :goto_2

    nop

    :goto_6
    const/4 v0, 0x1

    goto/32 :goto_1

    nop

    :goto_7
    invoke-virtual {p0, v0, v1, v1}, Landroid/service/wallpaper/WallpaperService$Engine;->updateSurface(ZZZ)V

    :goto_8
    goto/32 :goto_d

    nop

    :goto_9
    if-eqz v0, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_5

    nop

    :goto_a
    iget-object v0, v0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mDisplayPadding:Landroid/graphics/Rect;

    goto/32 :goto_c

    nop

    :goto_b
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDestroyed:Z

    goto/32 :goto_3

    nop

    :goto_c
    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_9

    nop

    :goto_d
    return-void
.end method

.method doOffsetsChanged(Z)V
    .locals 13

    goto/32 :goto_28

    nop

    :goto_0
    return-void

    :goto_1
    goto/32 :goto_1a

    nop

    :goto_2
    const/high16 v3, 0x3f000000    # 0.5f

    goto/32 :goto_18

    nop

    :goto_3
    move v7, v2

    :goto_4
    goto/32 :goto_24

    nop

    :goto_5
    goto :goto_6

    :catch_0
    move-exception v0

    :goto_6
    goto/32 :goto_2f

    nop

    :goto_7
    if-nez v11, :cond_0

    goto/32 :goto_6

    :cond_0
    :try_start_0
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSession:Landroid/view/IWindowSession;

    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindow:Lcom/android/internal/view/BaseIWindow;

    invoke-virtual {v2}, Lcom/android/internal/view/BaseIWindow;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-interface {v0, v2}, Landroid/view/IWindowSession;->wallpaperOffsetsComplete(Landroid/os/IBinder;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_5

    nop

    :goto_8
    add-float/2addr v4, v3

    goto/32 :goto_33

    nop

    :goto_9
    goto :goto_4

    :goto_a
    goto/32 :goto_3

    nop

    :goto_b
    throw v1

    :goto_c
    if-nez v0, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_0

    nop

    :goto_d
    monitor-enter v0

    :try_start_1
    iget v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingXOffset:F

    iget v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingYOffset:F

    move v9, v2

    iget v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingXOffsetStep:F

    move v10, v2

    iget v6, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingYOffsetStep:F

    iget-boolean v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingSync:Z

    move v11, v2

    const/4 v2, 0x0

    iput-boolean v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingSync:Z

    iput-boolean v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mOffsetMessageEnqueued:Z

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/32 :goto_2d

    nop

    :goto_e
    iput-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mOffsetsChanged:Z

    :goto_f
    goto/32 :goto_7

    nop

    :goto_10
    mul-float/2addr v4, v1

    goto/32 :goto_8

    nop

    :goto_11
    move v3, v1

    goto/32 :goto_37

    nop

    :goto_12
    iget v0, v0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mReqWidth:I

    goto/32 :goto_21

    nop

    :goto_13
    invoke-virtual/range {v2 .. v8}, Landroid/service/wallpaper/WallpaperService$Engine;->onOffsetsChanged(FFFFII)V

    goto/32 :goto_16

    nop

    :goto_14
    float-to-int v2, v2

    goto/32 :goto_38

    nop

    :goto_15
    move v5, v10

    goto/32 :goto_13

    nop

    :goto_16
    goto :goto_f

    :goto_17
    goto/32 :goto_30

    nop

    :goto_18
    if-gtz v0, :cond_2

    goto/32 :goto_a

    :cond_2
    goto/32 :goto_23

    nop

    :goto_19
    if-eqz v0, :cond_3

    goto/32 :goto_36

    :cond_3
    goto/32 :goto_35

    nop

    :goto_1a
    if-eqz p1, :cond_4

    goto/32 :goto_36

    :cond_4
    goto/32 :goto_31

    nop

    :goto_1b
    mul-float/2addr v2, v9

    goto/32 :goto_27

    nop

    :goto_1c
    int-to-float v2, v12

    goto/32 :goto_1b

    nop

    :goto_1d
    if-gtz v12, :cond_5

    goto/32 :goto_39

    :cond_5
    goto/32 :goto_1c

    nop

    :goto_1e
    if-nez v0, :cond_6

    goto/32 :goto_f

    :cond_6
    goto/32 :goto_32

    nop

    :goto_1f
    if-nez v0, :cond_7

    goto/32 :goto_17

    :cond_7
    goto/32 :goto_20

    nop

    :goto_20
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    goto/32 :goto_12

    nop

    :goto_21
    iget v3, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWidth:I

    goto/32 :goto_22

    nop

    :goto_22
    sub-int/2addr v0, v3

    goto/32 :goto_2

    nop

    :goto_23
    int-to-float v4, v0

    goto/32 :goto_10

    nop

    :goto_24
    iget-object v4, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    goto/32 :goto_2a

    nop

    :goto_25
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLock:Ljava/lang/Object;

    goto/32 :goto_d

    nop

    :goto_26
    move v8, v2

    goto/32 :goto_29

    nop

    :goto_27
    add-float/2addr v2, v3

    goto/32 :goto_14

    nop

    :goto_28
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDestroyed:Z

    goto/32 :goto_c

    nop

    :goto_29
    move-object v2, p0

    goto/32 :goto_11

    nop

    :goto_2a
    iget v4, v4, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mReqHeight:I

    goto/32 :goto_2e

    nop

    :goto_2b
    sub-int v12, v4, v5

    goto/32 :goto_1d

    nop

    :goto_2c
    return-void

    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/32 :goto_b

    nop

    :goto_2d
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceCreated:Z

    goto/32 :goto_1e

    nop

    :goto_2e
    iget v5, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurHeight:I

    goto/32 :goto_2b

    nop

    :goto_2f
    invoke-direct {p0, v1, v10}, Landroid/service/wallpaper/WallpaperService$Engine;->processLocalColors(FF)V

    goto/32 :goto_2c

    nop

    :goto_30
    const/4 v0, 0x1

    goto/32 :goto_e

    nop

    :goto_31
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mOffsetsChanged:Z

    goto/32 :goto_19

    nop

    :goto_32
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mReportedVisible:Z

    goto/32 :goto_1f

    nop

    :goto_33
    float-to-int v4, v4

    goto/32 :goto_34

    nop

    :goto_34
    neg-int v4, v4

    goto/32 :goto_3a

    nop

    :goto_35
    return-void

    :goto_36
    goto/32 :goto_25

    nop

    :goto_37
    move v4, v9

    goto/32 :goto_15

    nop

    :goto_38
    neg-int v2, v2

    :goto_39
    goto/32 :goto_26

    nop

    :goto_3a
    move v7, v4

    goto/32 :goto_9

    nop
.end method

.method doVisibilityChanged(Z)V
    .locals 2

    goto/32 :goto_d

    nop

    :goto_0
    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingXOffset:F

    goto/32 :goto_a

    nop

    :goto_1
    return-void

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_0

    nop

    :goto_3
    iput-boolean p1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mVisible:Z

    goto/32 :goto_b

    nop

    :goto_4
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mReportedVisible:Z

    goto/32 :goto_2

    nop

    :goto_5
    goto :goto_8

    :goto_6
    goto/32 :goto_7

    nop

    :goto_7
    invoke-static {p1, p0}, Landroid/animation/AnimationHandler;->requestAnimatorsEnabled(ZLjava/lang/Object;)V

    :goto_8
    goto/32 :goto_1

    nop

    :goto_9
    if-eqz v0, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_3

    nop

    :goto_a
    iget v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingXOffsetStep:F

    goto/32 :goto_c

    nop

    :goto_b
    invoke-virtual {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->reportVisibility()V

    goto/32 :goto_4

    nop

    :goto_c
    invoke-direct {p0, v0, v1}, Landroid/service/wallpaper/WallpaperService$Engine;->processLocalColors(FF)V

    goto/32 :goto_5

    nop

    :goto_d
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDestroyed:Z

    goto/32 :goto_9

    nop
.end method

.method protected dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mInitializing="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mInitializing:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string v0, " mDestroyed="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDestroyed:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mVisible="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mVisible:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string v0, " mReportedVisible="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mReportedVisible:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mDisplay="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDisplay:Landroid/view/Display;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mCreated="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCreated:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string v0, " mSurfaceCreated="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceCreated:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string v0, " mIsCreating="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIsCreating:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string v0, " mDrawingAllowed="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDrawingAllowed:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mWidth="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWidth:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    const-string v0, " mCurWidth="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWidth:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    const-string v0, " mHeight="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mHeight:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    const-string v0, " mCurHeight="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurHeight:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mType="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mType:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    const-string v0, " mWindowFlags="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowFlags:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    const-string v0, " mCurWindowFlags="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWindowFlags:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mWindowPrivateFlags="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPrivateFlags:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    const-string v0, " mCurWindowPrivateFlags="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWindowPrivateFlags:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mWinFrames="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWinFrames:Landroid/window/ClientWindowFrames;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mConfiguration="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mMergedConfiguration:Landroid/util/MergedConfiguration;

    invoke-virtual {v0}, Landroid/util/MergedConfiguration;->getMergedConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mLayout="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mZoom="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mZoom:F

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(F)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v0, "mPreviewSurfacePosition="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPreviewSurfacePosition:Landroid/graphics/Rect;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v1, "mPendingXOffset="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingXOffset:F

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(F)V

    const-string v1, " mPendingXOffset="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingXOffset:F

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(F)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v1, "mPendingXOffsetStep="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingXOffsetStep:F

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(F)V

    const-string v1, " mPendingXOffsetStep="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingXOffsetStep:F

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(F)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v1, "mOffsetMessageEnqueued="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mOffsetMessageEnqueued:Z

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Z)V

    const-string v1, " mPendingSync="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingSync:Z

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Z)V

    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingMove:Landroid/view/MotionEvent;

    if-eqz v1, :cond_0

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v1, "mPendingMove="

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingMove:Landroid/view/MotionEvent;

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getDesiredMinimumHeight()I
    .locals 1

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    iget v0, v0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mReqHeight:I

    return v0
.end method

.method public getDesiredMinimumWidth()I
    .locals 1

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    iget v0, v0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mReqWidth:I

    return v0
.end method

.method public getDisplayContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDisplayContext:Landroid/content/Context;

    return-object v0
.end method

.method public getSurfaceHolder()Landroid/view/SurfaceHolder;
    .locals 1

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    return-object v0
.end method

.method public getZoom()F
    .locals 1

    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mZoom:F

    return v0
.end method

.method public isInAmbientMode()Z
    .locals 1
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIsInAmbientMode:Z

    return v0
.end method

.method public isPreview()Z
    .locals 1

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    iget-boolean v0, v0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mIsPreview:Z

    return v0
.end method

.method public isVisible()Z
    .locals 1

    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mReportedVisible:Z

    return v0
.end method

.method synthetic lambda$addLocalColorsAreas$3$android-service-wallpaper-WallpaperService$Engine(Ljava/util/List;)V
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    invoke-direct {p0, v0, v1}, Landroid/service/wallpaper/WallpaperService$Engine;->processLocalColors(FF)V

    goto/32 :goto_3

    nop

    :goto_1
    iget v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingYOffset:F

    goto/32 :goto_0

    nop

    :goto_2
    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingXOffset:F

    goto/32 :goto_1

    nop

    :goto_3
    return-void

    :goto_4
    invoke-virtual {v0, p1}, Landroid/util/ArraySet;->addAll(Ljava/util/Collection;)Z

    goto/32 :goto_2

    nop

    :goto_5
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLocalColorsToAdd:Landroid/util/ArraySet;

    goto/32 :goto_4

    nop
.end method

.method synthetic lambda$processLocalColors$1$android-service-wallpaper-WallpaperService$Engine(IIFF)V
    .locals 7

    goto/32 :goto_9

    nop

    :goto_0
    array-length v1, v1

    goto/32 :goto_2e

    nop

    :goto_1
    invoke-virtual {v3, v2}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    goto/32 :goto_28

    nop

    :goto_2
    invoke-virtual {v1}, Landroid/util/ArraySet;->size()I

    move-result v1

    goto/32 :goto_6

    nop

    :goto_3
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPages:[Landroid/service/wallpaper/EngineWindowPage;

    goto/32 :goto_18

    nop

    :goto_4
    invoke-virtual {p0, v1, v0, p2, p3}, Landroid/service/wallpaper/WallpaperService$Engine;->updatePage(Landroid/service/wallpaper/EngineWindowPage;IIF)V

    goto/32 :goto_33

    nop

    :goto_5
    move v0, p1

    goto/32 :goto_3

    nop

    :goto_6
    if-nez v1, :cond_0

    goto/32 :goto_24

    :cond_0
    goto/32 :goto_1c

    nop

    :goto_7
    new-array v1, p2, [Landroid/service/wallpaper/EngineWindowPage;

    goto/32 :goto_a

    nop

    :goto_8
    invoke-direct {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->resetWindowPages()V

    goto/32 :goto_5

    nop

    :goto_9
    const-string v0, "WallpaperService#processLocalColors"

    goto/32 :goto_1f

    nop

    :goto_a
    iput-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPages:[Landroid/service/wallpaper/EngineWindowPage;

    goto/32 :goto_25

    nop

    :goto_b
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPages:[Landroid/service/wallpaper/EngineWindowPage;

    goto/32 :goto_31

    nop

    :goto_c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_11

    nop

    :goto_d
    iget-object v3, p0, Landroid/service/wallpaper/WallpaperService$Engine;->this$0:Landroid/service/wallpaper/WallpaperService;

    goto/32 :goto_30

    nop

    :goto_e
    aget-object v1, v1, v0

    goto/32 :goto_4

    nop

    :goto_f
    if-nez v2, :cond_1

    goto/32 :goto_2d

    :cond_1
    goto/32 :goto_c

    nop

    :goto_10
    invoke-virtual {v4, v5, v6}, Landroid/service/wallpaper/EngineWindowPage;->setLastUpdateTime(J)V

    goto/32 :goto_2b

    nop

    :goto_11
    check-cast v2, Landroid/graphics/RectF;

    goto/32 :goto_d

    nop

    :goto_12
    iget-object v4, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPages:[Landroid/service/wallpaper/EngineWindowPage;

    goto/32 :goto_1e

    nop

    :goto_13
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLocalColorsToAdd:Landroid/util/ArraySet;

    goto/32 :goto_23

    nop

    :goto_14
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    goto/32 :goto_f

    nop

    :goto_15
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLocalColorsToAdd:Landroid/util/ArraySet;

    goto/32 :goto_2

    nop

    :goto_16
    add-int/lit8 v0, v2, -0x1

    :goto_17
    goto/32 :goto_e

    nop

    :goto_18
    array-length v2, v1

    goto/32 :goto_1d

    nop

    :goto_19
    array-length v2, v1

    goto/32 :goto_16

    nop

    :goto_1a
    invoke-virtual {v1}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1b
    goto/32 :goto_14

    nop

    :goto_1c
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLocalColorsToAdd:Landroid/util/ArraySet;

    goto/32 :goto_1a

    nop

    :goto_1d
    if-nez v2, :cond_2

    goto/32 :goto_2f

    :cond_2
    goto/32 :goto_0

    nop

    :goto_1e
    aget-object v4, v4, v3

    goto/32 :goto_22

    nop

    :goto_1f
    invoke-static {v0}, Landroid/os/Trace;->beginSection(Ljava/lang/String;)V

    goto/32 :goto_8

    nop

    :goto_20
    return-void

    :goto_21
    iget-object v3, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLocalColorAreas:Landroid/util/ArraySet;

    goto/32 :goto_1

    nop

    :goto_22
    const-wide/16 v5, 0x0

    goto/32 :goto_10

    nop

    :goto_23
    invoke-virtual {v1}, Landroid/util/ArraySet;->clear()V

    :goto_24
    goto/32 :goto_b

    nop

    :goto_25
    invoke-direct {p0, v1, p3}, Landroid/service/wallpaper/WallpaperService$Engine;->initWindowPages([Landroid/service/wallpaper/EngineWindowPage;F)V

    :goto_26
    goto/32 :goto_15

    nop

    :goto_27
    if-ge v0, v2, :cond_3

    goto/32 :goto_17

    :cond_3
    goto/32 :goto_19

    nop

    :goto_28
    invoke-direct {p0, v2, p3}, Landroid/service/wallpaper/WallpaperService$Engine;->getRectFPage(Landroid/graphics/RectF;F)I

    move-result v3

    goto/32 :goto_12

    nop

    :goto_29
    goto :goto_1b

    :goto_2a
    goto/32 :goto_21

    nop

    :goto_2b
    invoke-virtual {v4, v2}, Landroid/service/wallpaper/EngineWindowPage;->removeColor(Landroid/graphics/RectF;)V

    goto/32 :goto_2c

    nop

    :goto_2c
    goto :goto_1b

    :goto_2d
    goto/32 :goto_13

    nop

    :goto_2e
    if-ne v1, p2, :cond_4

    goto/32 :goto_26

    :cond_4
    :goto_2f
    goto/32 :goto_7

    nop

    :goto_30
    invoke-static {v3, v2}, Landroid/service/wallpaper/WallpaperService;->-$$Nest$misValid(Landroid/service/wallpaper/WallpaperService;Landroid/graphics/RectF;)Z

    move-result v3

    goto/32 :goto_32

    nop

    :goto_31
    array-length v2, v1

    goto/32 :goto_27

    nop

    :goto_32
    if-eqz v3, :cond_5

    goto/32 :goto_2a

    :cond_5
    goto/32 :goto_29

    nop

    :goto_33
    invoke-static {}, Landroid/os/Trace;->endSection()V

    goto/32 :goto_20

    nop
.end method

.method synthetic lambda$removeLocalColorsAreas$4$android-service-wallpaper-WallpaperService$Engine(Ljava/util/List;)V
    .locals 5

    goto/32 :goto_1c

    nop

    :goto_0
    if-lt v2, v3, :cond_0

    goto/32 :goto_19

    :cond_0
    goto/32 :goto_2

    nop

    :goto_1
    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPages:[Landroid/service/wallpaper/EngineWindowPage;

    goto/32 :goto_e

    nop

    :goto_2
    iget-object v3, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPages:[Landroid/service/wallpaper/EngineWindowPage;

    goto/32 :goto_8

    nop

    :goto_3
    const/4 v1, 0x0

    :goto_4
    goto/32 :goto_1

    nop

    :goto_5
    invoke-direct {p0, v0}, Landroid/service/wallpaper/WallpaperService$Engine;->validStep(F)Z

    move-result v1

    goto/32 :goto_f

    nop

    :goto_6
    return-void

    :goto_7
    goto/32 :goto_3

    nop

    :goto_8
    aget-object v3, v3, v1

    goto/32 :goto_c

    nop

    :goto_9
    const/4 v2, 0x0

    :goto_a
    goto/32 :goto_11

    nop

    :goto_b
    invoke-virtual {v1, p1}, Landroid/util/ArraySet;->removeAll(Ljava/util/Collection;)Z

    goto/32 :goto_14

    nop

    :goto_c
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_12

    nop

    :goto_d
    return-void

    :goto_e
    array-length v2, v2

    goto/32 :goto_10

    nop

    :goto_f
    if-eqz v1, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_6

    nop

    :goto_10
    if-lt v1, v2, :cond_2

    goto/32 :goto_1b

    :cond_2
    goto/32 :goto_9

    nop

    :goto_11
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    goto/32 :goto_0

    nop

    :goto_12
    check-cast v4, Landroid/graphics/RectF;

    goto/32 :goto_16

    nop

    :goto_13
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_1a

    nop

    :goto_14
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLocalColorAreas:Landroid/util/ArraySet;

    goto/32 :goto_1d

    nop

    :goto_15
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLocalColorsToAdd:Landroid/util/ArraySet;

    goto/32 :goto_b

    nop

    :goto_16
    invoke-virtual {v3, v4}, Landroid/service/wallpaper/EngineWindowPage;->removeArea(Landroid/graphics/RectF;)V

    goto/32 :goto_17

    nop

    :goto_17
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_18

    nop

    :goto_18
    goto :goto_a

    :goto_19
    goto/32 :goto_13

    nop

    :goto_1a
    goto :goto_4

    :goto_1b
    goto/32 :goto_d

    nop

    :goto_1c
    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingXOffsetStep:F

    goto/32 :goto_15

    nop

    :goto_1d
    invoke-virtual {v1, p1}, Landroid/util/ArraySet;->removeAll(Ljava/util/Collection;)Z

    goto/32 :goto_5

    nop
.end method

.method synthetic lambda$updatePage$2$android-service-wallpaper-WallpaperService$Engine(Landroid/service/wallpaper/EngineWindowPage;IIFLandroid/graphics/Bitmap;JI)V
    .locals 3

    goto/32 :goto_8

    nop

    :goto_0
    invoke-virtual {p1, p6, p7}, Landroid/service/wallpaper/EngineWindowPage;->setLastUpdateTime(J)V

    goto/32 :goto_4

    nop

    :goto_1
    invoke-virtual {p1, v1}, Landroid/service/wallpaper/EngineWindowPage;->setBitmap(Landroid/graphics/Bitmap;)V

    goto/32 :goto_3

    nop

    :goto_2
    invoke-static {v0, v1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    goto/32 :goto_6

    nop

    :goto_3
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLastScreenshot:Landroid/graphics/Bitmap;

    goto/32 :goto_10

    nop

    :goto_4
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/service/wallpaper/WallpaperService$Engine;->updatePageColors(Landroid/service/wallpaper/EngineWindowPage;IIF)V

    :goto_5
    goto/32 :goto_9

    nop

    :goto_6
    if-eqz v2, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_d

    nop

    :goto_7
    invoke-virtual {p1}, Landroid/service/wallpaper/EngineWindowPage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    goto/32 :goto_12

    nop

    :goto_8
    invoke-static {}, Landroid/os/Trace;->endSection()V

    goto/32 :goto_13

    nop

    :goto_9
    return-void

    :goto_a
    goto :goto_5

    :goto_b
    goto/32 :goto_f

    nop

    :goto_c
    invoke-virtual {p1, p5}, Landroid/service/wallpaper/EngineWindowPage;->setBitmap(Landroid/graphics/Bitmap;)V

    goto/32 :goto_0

    nop

    :goto_d
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/service/wallpaper/WallpaperService$Engine;->updatePageColors(Landroid/service/wallpaper/EngineWindowPage;IIF)V

    :goto_e
    goto/32 :goto_a

    nop

    :goto_f
    iput-object p5, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLastScreenshot:Landroid/graphics/Bitmap;

    goto/32 :goto_c

    nop

    :goto_10
    if-nez v1, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_14

    nop

    :goto_11
    if-eqz v2, :cond_2

    goto/32 :goto_e

    :cond_2
    goto/32 :goto_2

    nop

    :goto_12
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLastScreenshot:Landroid/graphics/Bitmap;

    goto/32 :goto_1

    nop

    :goto_13
    if-nez p8, :cond_3

    goto/32 :goto_b

    :cond_3
    goto/32 :goto_7

    nop

    :goto_14
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    goto/32 :goto_11

    nop
.end method

.method synthetic lambda$updateSurfaceDimming$0$android-service-wallpaper-WallpaperService$Engine(Landroid/view/SurfaceControl$Transaction;Landroid/animation/ValueAnimator;)V
    .locals 3

    goto/32 :goto_1

    nop

    :goto_0
    if-nez v1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_a

    nop

    :goto_1
    invoke-virtual {p2}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_2
    sub-float/2addr v2, v0

    goto/32 :goto_9

    nop

    :goto_3
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mBbqSurfaceControl:Landroid/view/SurfaceControl;

    goto/32 :goto_0

    nop

    :goto_4
    check-cast v0, Ljava/lang/Float;

    goto/32 :goto_7

    nop

    :goto_5
    invoke-virtual {v1}, Landroid/view/SurfaceControl$Transaction;->apply()V

    :goto_6
    goto/32 :goto_8

    nop

    :goto_7
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto/32 :goto_3

    nop

    :goto_8
    return-void

    :goto_9
    invoke-virtual {p1, v1, v2}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;

    move-result-object v1

    goto/32 :goto_5

    nop

    :goto_a
    const/high16 v2, 0x3f800000    # 1.0f

    goto/32 :goto_2

    nop
.end method

.method public notifyColorsChanged()V
    .locals 6

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mClockFunction:Ljava/util/function/Supplier;

    invoke-interface {v0}, Ljava/util/function/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-wide v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLastColorInvalidation:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x3e8

    cmp-long v2, v2, v4

    const-string v3, "WallpaperService"

    if-gez v2, :cond_1

    const-string v2, "This call has been deferred. You should only call notifyColorsChanged() once every 1.0 seconds."

    invoke-static {v3, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mNotifyColorsChanged:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->hasCallbacks(Ljava/lang/Runnable;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mNotifyColorsChanged:Ljava/lang/Runnable;

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void

    :cond_1
    iput-wide v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLastColorInvalidation:J

    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mNotifyColorsChanged:Ljava/lang/Runnable;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :try_start_0
    invoke-virtual {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->onComputeColors()Landroid/app/WallpaperColors;

    move-result-object v2

    iget-object v4, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mConnection:Landroid/service/wallpaper/IWallpaperConnection;

    if-eqz v4, :cond_2

    iget-object v5, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDisplay:Landroid/view/Display;

    invoke-virtual {v5}, Landroid/view/Display;->getDisplayId()I

    move-result v5

    invoke-interface {v4, v2, v5}, Landroid/service/wallpaper/IWallpaperConnection;->onWallpaperColorsChanged(Landroid/app/WallpaperColors;I)V

    goto :goto_0

    :cond_2
    const-string v4, "Can\'t notify system because wallpaper connection was not established."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const/4 v4, 0x1

    iput-boolean v4, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mResetWindowPages:Z

    iget v4, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingXOffset:F

    iget v5, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingXOffsetStep:F

    invoke-direct {p0, v4, v5}, Landroid/service/wallpaper/WallpaperService$Engine;->processLocalColors(FF)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    const-string v4, "Can\'t notify system because wallpaper connection was lost."

    invoke-static {v3, v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_1
    return-void
.end method

.method public notifyLocalColorsChanged(Ljava/util/List;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/graphics/RectF;",
            ">;",
            "Ljava/util/List<",
            "Landroid/app/WallpaperColors;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/WallpaperColors;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/RectF;

    if-eqz v1, :cond_1

    if-nez v2, :cond_0

    goto :goto_1

    :cond_0
    :try_start_0
    iget-object v3, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mConnection:Landroid/service/wallpaper/IWallpaperConnection;

    iget-object v4, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDisplayContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getDisplayId()I

    move-result v4

    invoke-interface {v3, v2, v1, v4}, Landroid/service/wallpaper/IWallpaperConnection;->onLocalWallpaperColorsChanged(Landroid/graphics/RectF;Landroid/app/WallpaperColors;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v3

    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    iget-object v0, v0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mWallpaperManager:Landroid/app/WallpaperManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/WallpaperManager;->getWallpaperColors(I)Landroid/app/WallpaperColors;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/service/wallpaper/WallpaperService$Engine;->setPrimaryWallpaperColors(Landroid/app/WallpaperColors;)V

    return-void
.end method

.method public onAmbientModeChanged(ZJ)V
    .locals 0
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    return-void
.end method

.method public onApplyWindowInsets(Landroid/view/WindowInsets;)V
    .locals 0

    return-void
.end method

.method public onCommand(Ljava/lang/String;IIILandroid/os/Bundle;Z)Landroid/os/Bundle;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onComputeColors()Landroid/app/WallpaperColors;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate(Landroid/view/SurfaceHolder;)V
    .locals 0

    return-void
.end method

.method public onDesiredSizeChanged(II)V
    .locals 0

    return-void
.end method

.method public onDestroy()V
    .locals 0

    return-void
.end method

.method public onOffsetsChanged(FFFFII)V
    .locals 0

    return-void
.end method

.method public onSurfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0

    return-void
.end method

.method public onSurfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 0

    return-void
.end method

.method public onSurfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0

    return-void
.end method

.method public onSurfaceRedrawNeeded(Landroid/view/SurfaceHolder;)V
    .locals 0

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 0

    return-void
.end method

.method public onVisibilityChanged(Z)V
    .locals 0

    return-void
.end method

.method public onZoomChanged(F)V
    .locals 0

    return-void
.end method

.method public removeLocalColorsAreas(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/graphics/RectF;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->supportsLocalColorExtraction()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mHandler:Landroid/os/Handler;

    new-instance v1, Landroid/service/wallpaper/WallpaperService$Engine$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine$$ExternalSyntheticLambda4;-><init>(Landroid/service/wallpaper/WallpaperService$Engine;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public reportEngineShown(Z)V
    .locals 5

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    iget-boolean v0, v0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mShownReported:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/16 v0, 0x27a6

    if-nez p1, :cond_1

    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCaller:Lcom/android/internal/os/HandlerCaller;

    invoke-virtual {v1, v0}, Lcom/android/internal/os/HandlerCaller;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCaller:Lcom/android/internal/os/HandlerCaller;

    invoke-virtual {v2, v0}, Lcom/android/internal/os/HandlerCaller;->removeMessages(I)V

    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCaller:Lcom/android/internal/os/HandlerCaller;

    invoke-virtual {v0, v1}, Lcom/android/internal/os/HandlerCaller;->sendMessage(Landroid/os/Message;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCaller:Lcom/android/internal/os/HandlerCaller;

    invoke-virtual {v1, v0}, Lcom/android/internal/os/HandlerCaller;->hasMessages(I)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCaller:Lcom/android/internal/os/HandlerCaller;

    invoke-virtual {v1, v0}, Lcom/android/internal/os/HandlerCaller;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCaller:Lcom/android/internal/os/HandlerCaller;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v3, 0x5

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/internal/os/HandlerCaller;->sendMessageDelayed(Landroid/os/Message;J)V

    :cond_2
    :goto_0
    return-void
.end method

.method reportSurfaceDestroyed()V
    .locals 5

    goto/32 :goto_15

    nop

    :goto_0
    iput-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceCreated:Z

    goto/32 :goto_8

    nop

    :goto_1
    invoke-virtual {v1}, Lcom/android/internal/view/BaseSurfaceHolder;->ungetCallbacks()V

    goto/32 :goto_12

    nop

    :goto_2
    invoke-interface {v3, v4}, Landroid/view/SurfaceHolder$Callback;->surfaceDestroyed(Landroid/view/SurfaceHolder;)V

    goto/32 :goto_14

    nop

    :goto_3
    invoke-virtual {p0, v0}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceDestroyed(Landroid/view/SurfaceHolder;)V

    :goto_4
    goto/32 :goto_b

    nop

    :goto_5
    const/4 v0, 0x0

    goto/32 :goto_0

    nop

    :goto_6
    if-lt v0, v2, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_7

    nop

    :goto_7
    aget-object v3, v1, v0

    goto/32 :goto_13

    nop

    :goto_8
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    goto/32 :goto_1

    nop

    :goto_9
    array-length v2, v1

    :goto_a
    goto/32 :goto_6

    nop

    :goto_b
    return-void

    :goto_c
    goto :goto_a

    :goto_d
    goto/32 :goto_10

    nop

    :goto_e
    if-nez v1, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_9

    nop

    :goto_f
    if-nez v0, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_5

    nop

    :goto_10
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    goto/32 :goto_3

    nop

    :goto_11
    invoke-virtual {v1}, Lcom/android/internal/view/BaseSurfaceHolder;->getCallbacks()[Landroid/view/SurfaceHolder$Callback;

    move-result-object v1

    goto/32 :goto_e

    nop

    :goto_12
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    goto/32 :goto_11

    nop

    :goto_13
    iget-object v4, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    goto/32 :goto_2

    nop

    :goto_14
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_c

    nop

    :goto_15
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceCreated:Z

    goto/32 :goto_f

    nop
.end method

.method reportVisibility()V
    .locals 4

    goto/32 :goto_4

    nop

    :goto_0
    return-void

    :goto_1
    if-nez v1, :cond_0

    goto/32 :goto_24

    :cond_0
    goto/32 :goto_23

    nop

    :goto_2
    return-void

    :goto_3
    goto/32 :goto_10

    nop

    :goto_4
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenshotSurfaceControl:Landroid/view/SurfaceControl;

    goto/32 :goto_9

    nop

    :goto_5
    move v0, v3

    goto/32 :goto_19

    nop

    :goto_6
    move v0, v1

    goto/32 :goto_17

    nop

    :goto_7
    iget-boolean v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mReportedVisible:Z

    goto/32 :goto_12

    nop

    :goto_8
    if-ne v0, v3, :cond_1

    goto/32 :goto_1a

    :cond_1
    goto/32 :goto_5

    nop

    :goto_9
    if-nez v0, :cond_2

    goto/32 :goto_3

    :cond_2
    goto/32 :goto_e

    nop

    :goto_a
    if-ne v2, v0, :cond_3

    goto/32 :goto_16

    :cond_3
    goto/32 :goto_14

    nop

    :goto_b
    if-eqz v0, :cond_4

    goto/32 :goto_16

    :cond_4
    goto/32 :goto_20

    nop

    :goto_c
    if-nez v0, :cond_5

    goto/32 :goto_2a

    :cond_5
    goto/32 :goto_1b

    nop

    :goto_d
    iget-boolean v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mFrozenRequested:Z

    goto/32 :goto_1

    nop

    :goto_e
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mVisible:Z

    goto/32 :goto_11

    nop

    :goto_f
    invoke-virtual {p0, v0}, Landroid/service/wallpaper/WallpaperService$Engine;->onVisibilityChanged(Z)V

    goto/32 :goto_7

    nop

    :goto_10
    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDestroyed:Z

    goto/32 :goto_b

    nop

    :goto_11
    if-nez v0, :cond_6

    goto/32 :goto_3

    :cond_6
    goto/32 :goto_2

    nop

    :goto_12
    if-nez v1, :cond_7

    goto/32 :goto_24

    :cond_7
    goto/32 :goto_d

    nop

    :goto_13
    const/4 v3, 0x1

    goto/32 :goto_21

    nop

    :goto_14
    iput-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mReportedVisible:Z

    goto/32 :goto_c

    nop

    :goto_15
    invoke-static {v0, p0}, Landroid/animation/AnimationHandler;->requestAnimatorsEnabled(ZLjava/lang/Object;)V

    :goto_16
    goto/32 :goto_0

    nop

    :goto_17
    goto :goto_28

    :goto_18
    goto/32 :goto_27

    nop

    :goto_19
    goto :goto_26

    :goto_1a
    goto/32 :goto_25

    nop

    :goto_1b
    invoke-virtual {p0, v1}, Landroid/service/wallpaper/WallpaperService$Engine;->doOffsetsChanged(Z)V

    goto/32 :goto_29

    nop

    :goto_1c
    iget-boolean v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mVisible:Z

    goto/32 :goto_13

    nop

    :goto_1d
    iget-boolean v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mReportedVisible:Z

    goto/32 :goto_a

    nop

    :goto_1e
    if-eqz v0, :cond_8

    goto/32 :goto_18

    :cond_8
    goto/32 :goto_6

    nop

    :goto_1f
    iput v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDisplayState:I

    goto/32 :goto_1c

    nop

    :goto_20
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDisplay:Landroid/view/Display;

    goto/32 :goto_22

    nop

    :goto_21
    if-nez v2, :cond_9

    goto/32 :goto_1a

    :cond_9
    goto/32 :goto_8

    nop

    :goto_22
    const/4 v1, 0x0

    goto/32 :goto_1e

    nop

    :goto_23
    invoke-direct {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->freeze()V

    :goto_24
    goto/32 :goto_15

    nop

    :goto_25
    move v0, v1

    :goto_26
    goto/32 :goto_1d

    nop

    :goto_27
    invoke-virtual {v0}, Landroid/view/Display;->getState()I

    move-result v0

    :goto_28
    goto/32 :goto_1f

    nop

    :goto_29
    invoke-virtual {p0, v3, v1, v1}, Landroid/service/wallpaper/WallpaperService$Engine;->updateSurface(ZZZ)V

    :goto_2a
    goto/32 :goto_f

    nop
.end method

.method scaleAndCropScreenshot()V
    .locals 11

    goto/32 :goto_18

    nop

    :goto_0
    iget v3, v3, Landroid/graphics/Point;->y:I

    goto/32 :goto_f

    nop

    :goto_1
    div-int/lit8 v7, v1, 0x2

    goto/32 :goto_6

    nop

    :goto_2
    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceSize:Landroid/graphics/Point;

    goto/32 :goto_33

    nop

    :goto_3
    invoke-virtual {v2, v3, v4}, Landroid/view/SurfaceControl$Transaction;->setWindowCrop(Landroid/view/SurfaceControl;Landroid/graphics/Rect;)Landroid/view/SurfaceControl$Transaction;

    move-result-object v2

    goto/32 :goto_2c

    nop

    :goto_4
    iget-object v3, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceSize:Landroid/graphics/Point;

    goto/32 :goto_20

    nop

    :goto_5
    mul-float/2addr v1, v0

    goto/32 :goto_2a

    nop

    :goto_6
    iget-object v9, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenshotSize:Landroid/graphics/Point;

    goto/32 :goto_11

    nop

    :goto_7
    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_42

    nop

    :goto_8
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    goto/32 :goto_27

    nop

    :goto_9
    iget v10, v10, Landroid/graphics/Point;->y:I

    goto/32 :goto_53

    nop

    :goto_a
    iget-object v3, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenshotSize:Landroid/graphics/Point;

    goto/32 :goto_0

    nop

    :goto_b
    iget v0, v0, Landroid/graphics/Point;->y:I

    goto/32 :goto_23

    nop

    :goto_c
    iget v2, v2, Landroid/graphics/Point;->y:I

    goto/32 :goto_28

    nop

    :goto_d
    invoke-direct {v4, v5, v6, v7, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    goto/32 :goto_3

    nop

    :goto_e
    iget-object v10, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenshotSize:Landroid/graphics/Point;

    goto/32 :goto_9

    nop

    :goto_f
    int-to-float v3, v3

    goto/32 :goto_4f

    nop

    :goto_10
    invoke-virtual/range {v2 .. v7}, Landroid/view/SurfaceControl$Transaction;->setMatrix(Landroid/view/SurfaceControl;FFFF)Landroid/view/SurfaceControl$Transaction;

    move-result-object v2

    goto/32 :goto_54

    nop

    :goto_11
    iget v9, v9, Landroid/graphics/Point;->x:I

    goto/32 :goto_1b

    nop

    :goto_12
    div-int/lit8 v5, v5, 0x2

    goto/32 :goto_3b

    nop

    :goto_13
    goto/16 :goto_4e

    :goto_14
    goto/32 :goto_47

    nop

    :goto_15
    invoke-virtual {v2, v3, v4, v5}, Landroid/view/SurfaceControl$Transaction;->setPosition(Landroid/view/SurfaceControl;FF)Landroid/view/SurfaceControl$Transaction;

    move-result-object v2

    goto/32 :goto_48

    nop

    :goto_16
    iget v1, v1, Landroid/graphics/Point;->x:I

    goto/32 :goto_40

    nop

    :goto_17
    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenshotSize:Landroid/graphics/Point;

    goto/32 :goto_c

    nop

    :goto_18
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenshotSurfaceControl:Landroid/view/SurfaceControl;

    goto/32 :goto_30

    nop

    :goto_19
    return-void

    :goto_1a
    goto/32 :goto_3a

    nop

    :goto_1b
    add-int/2addr v7, v9

    goto/32 :goto_38

    nop

    :goto_1c
    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenshotSize:Landroid/graphics/Point;

    goto/32 :goto_3e

    nop

    :goto_1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_2f

    nop

    :goto_1e
    int-to-float v2, v2

    goto/32 :goto_a

    nop

    :goto_1f
    new-instance v4, Landroid/graphics/Rect;

    goto/32 :goto_52

    nop

    :goto_20
    iget v3, v3, Landroid/graphics/Point;->y:I

    goto/32 :goto_4b

    nop

    :goto_21
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenshotSize:Landroid/graphics/Point;

    goto/32 :goto_b

    nop

    :goto_22
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_2b

    nop

    :goto_23
    if-lez v0, :cond_0

    goto/32 :goto_14

    :cond_0
    goto/32 :goto_13

    nop

    :goto_24
    if-gtz v0, :cond_1

    goto/32 :goto_4e

    :cond_1
    goto/32 :goto_21

    nop

    :goto_25
    iget v1, v1, Landroid/graphics/Point;->x:I

    goto/32 :goto_46

    nop

    :goto_26
    div-float/2addr v1, v2

    goto/32 :goto_36

    nop

    :goto_27
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenshotSize:Landroid/graphics/Point;

    goto/32 :goto_16

    nop

    :goto_28
    int-to-float v2, v2

    goto/32 :goto_57

    nop

    :goto_29
    float-to-int v2, v2

    goto/32 :goto_4

    nop

    :goto_2a
    float-to-int v1, v1

    goto/32 :goto_2

    nop

    :goto_2b
    const-string v1, "Unexpected screenshot size: "

    goto/32 :goto_3d

    nop

    :goto_2c
    iget-object v3, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenshotSurfaceControl:Landroid/view/SurfaceControl;

    goto/32 :goto_45

    nop

    :goto_2d
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_22

    nop

    :goto_2e
    new-instance v2, Landroid/view/SurfaceControl$Transaction;

    goto/32 :goto_35

    nop

    :goto_2f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_49

    nop

    :goto_30
    if-eqz v0, :cond_2

    goto/32 :goto_1a

    :cond_2
    goto/32 :goto_19

    nop

    :goto_31
    int-to-float v4, v4

    goto/32 :goto_55

    nop

    :goto_32
    const/4 v5, 0x0

    goto/32 :goto_56

    nop

    :goto_33
    iget v2, v2, Landroid/graphics/Point;->x:I

    goto/32 :goto_4c

    nop

    :goto_34
    move v4, v0

    goto/32 :goto_3c

    nop

    :goto_35
    invoke-direct {v2}, Landroid/view/SurfaceControl$Transaction;-><init>()V

    goto/32 :goto_39

    nop

    :goto_36
    iget-object v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceSize:Landroid/graphics/Point;

    goto/32 :goto_44

    nop

    :goto_37
    div-int/lit8 v4, v4, 0x2

    goto/32 :goto_31

    nop

    :goto_38
    div-int/lit8 v9, v8, 0x2

    goto/32 :goto_e

    nop

    :goto_39
    iget-object v3, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenshotSurfaceControl:Landroid/view/SurfaceControl;

    goto/32 :goto_32

    nop

    :goto_3a
    iget-object v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenshotSize:Landroid/graphics/Point;

    goto/32 :goto_43

    nop

    :goto_3b
    int-to-float v5, v5

    goto/32 :goto_15

    nop

    :goto_3c
    move v7, v0

    goto/32 :goto_10

    nop

    :goto_3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_41

    nop

    :goto_3e
    iget v2, v2, Landroid/graphics/Point;->x:I

    goto/32 :goto_3f

    nop

    :goto_3f
    int-to-float v2, v2

    goto/32 :goto_26

    nop

    :goto_40
    int-to-float v1, v1

    goto/32 :goto_5

    nop

    :goto_41
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenshotSize:Landroid/graphics/Point;

    goto/32 :goto_1d

    nop

    :goto_42
    return-void

    :goto_43
    iget v0, v0, Landroid/graphics/Point;->x:I

    goto/32 :goto_24

    nop

    :goto_44
    iget v2, v2, Landroid/graphics/Point;->y:I

    goto/32 :goto_1e

    nop

    :goto_45
    neg-int v4, v1

    goto/32 :goto_37

    nop

    :goto_46
    int-to-float v1, v1

    goto/32 :goto_1c

    nop

    :goto_47
    const/high16 v0, 0x3f800000    # 1.0f

    goto/32 :goto_50

    nop

    :goto_48
    invoke-virtual {v2}, Landroid/view/SurfaceControl$Transaction;->apply()V

    goto/32 :goto_4d

    nop

    :goto_49
    const-string v1, "WallpaperService"

    goto/32 :goto_7

    nop

    :goto_4a
    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    goto/32 :goto_8

    nop

    :goto_4b
    sub-int v8, v2, v3

    goto/32 :goto_2e

    nop

    :goto_4c
    sub-int/2addr v1, v2

    goto/32 :goto_17

    nop

    :goto_4d
    return-void

    :goto_4e
    goto/32 :goto_2d

    nop

    :goto_4f
    div-float/2addr v2, v3

    goto/32 :goto_4a

    nop

    :goto_50
    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceSize:Landroid/graphics/Point;

    goto/32 :goto_25

    nop

    :goto_51
    div-int/lit8 v6, v8, 0x2

    goto/32 :goto_1

    nop

    :goto_52
    div-int/lit8 v5, v1, 0x2

    goto/32 :goto_51

    nop

    :goto_53
    add-int/2addr v9, v10

    goto/32 :goto_d

    nop

    :goto_54
    iget-object v3, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mScreenshotSurfaceControl:Landroid/view/SurfaceControl;

    goto/32 :goto_1f

    nop

    :goto_55
    neg-int v5, v8

    goto/32 :goto_12

    nop

    :goto_56
    const/4 v6, 0x0

    goto/32 :goto_34

    nop

    :goto_57
    mul-float/2addr v2, v0

    goto/32 :goto_29

    nop
.end method

.method public setCreated(Z)V
    .locals 0

    iput-boolean p1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCreated:Z

    return-void
.end method

.method public setFixedSizeAllowed(Z)V
    .locals 0

    iput-boolean p1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mFixedSizeAllowed:Z

    return-void
.end method

.method public setOffsetNotificationsEnabled(Z)V
    .locals 1

    if-eqz p1, :cond_0

    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPrivateFlags:I

    or-int/lit8 v0, v0, 0x4

    goto :goto_0

    :cond_0
    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPrivateFlags:I

    and-int/lit8 v0, v0, -0x5

    :goto_0
    iput v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPrivateFlags:I

    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCreated:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0, v0}, Landroid/service/wallpaper/WallpaperService$Engine;->updateSurface(ZZZ)V

    :cond_1
    return-void
.end method

.method public setShowForAllUsers(Z)V
    .locals 1

    if-eqz p1, :cond_0

    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPrivateFlags:I

    or-int/lit8 v0, v0, 0x10

    goto :goto_0

    :cond_0
    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPrivateFlags:I

    and-int/lit8 v0, v0, -0x11

    :goto_0
    iput v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPrivateFlags:I

    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCreated:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0, v0}, Landroid/service/wallpaper/WallpaperService$Engine;->updateSurface(ZZZ)V

    :cond_1
    return-void
.end method

.method public setTouchEventsEnabled(Z)V
    .locals 1

    if-eqz p1, :cond_0

    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowFlags:I

    and-int/lit8 v0, v0, -0x11

    goto :goto_0

    :cond_0
    iget v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowFlags:I

    or-int/lit8 v0, v0, 0x10

    :goto_0
    iput v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowFlags:I

    iget-boolean v0, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mCreated:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0, v0}, Landroid/service/wallpaper/WallpaperService$Engine;->updateSurface(ZZZ)V

    :cond_1
    return-void
.end method

.method public setZoom(F)V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mIsInAmbientMode:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    iput v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mZoom:F

    :cond_0
    iget v2, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mZoom:F

    invoke-static {p1, v2}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_1

    iput p1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mZoom:F

    const/4 v0, 0x1

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    iget-boolean v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mDestroyed:Z

    if-nez v1, :cond_2

    iget v1, p0, Landroid/service/wallpaper/WallpaperService$Engine;->mZoom:F

    invoke-virtual {p0, v1}, Landroid/service/wallpaper/WallpaperService$Engine;->onZoomChanged(F)V

    :cond_2
    return-void

    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public shouldWaitForEngineShown()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public shouldZoomOutWallpaper()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public supportsLocalColorExtraction()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method updatePage(Landroid/service/wallpaper/EngineWindowPage;IIF)V
    .locals 22

    goto/32 :goto_65

    nop

    :goto_0
    move-wide/from16 v17, v12

    goto/32 :goto_1b

    nop

    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/service/wallpaper/EngineWindowPage;->getLastUpdateTime()J

    move-result-wide v0

    goto/32 :goto_c

    nop

    :goto_2
    move v7, v0

    goto/32 :goto_15

    nop

    :goto_3
    invoke-direct/range {v0 .. v8}, Landroid/service/wallpaper/WallpaperService$Engine$$ExternalSyntheticLambda0;-><init>(Landroid/service/wallpaper/WallpaperService$Engine;Landroid/service/wallpaper/EngineWindowPage;IIFLandroid/graphics/Bitmap;J)V

    goto/32 :goto_42

    nop

    :goto_4
    float-to-int v8, v0

    goto/32 :goto_7

    nop

    :goto_5
    invoke-virtual {v0}, Lcom/android/internal/view/BaseSurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v14

    goto/32 :goto_25

    nop

    :goto_6
    move-object/from16 v1, p0

    goto/32 :goto_18

    nop

    :goto_7
    iget-object v0, v9, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceSize:Landroid/graphics/Point;

    goto/32 :goto_34

    nop

    :goto_8
    move v15, v8

    :goto_9
    goto/32 :goto_64

    nop

    :goto_a
    cmp-long v0, v12, v2

    goto/32 :goto_52

    nop

    :goto_b
    mul-float v0, v0, v16

    goto/32 :goto_26

    nop

    :goto_c
    sub-long v12, v10, v0

    goto/32 :goto_a

    nop

    :goto_d
    const-string v1, " "

    goto/32 :goto_16

    nop

    :goto_e
    if-eqz v0, :cond_0

    goto/32 :goto_44

    :cond_0
    goto/32 :goto_43

    nop

    :goto_f
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    goto/32 :goto_63

    nop

    :goto_10
    int-to-float v1, v7

    goto/32 :goto_5a

    nop

    :goto_11
    iget-object v0, v9, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceSize:Landroid/graphics/Point;

    goto/32 :goto_56

    nop

    :goto_12
    if-nez v15, :cond_1

    goto/32 :goto_4a

    :cond_1
    goto/32 :goto_11

    nop

    :goto_13
    move-object v12, v3

    goto/32 :goto_3d

    nop

    :goto_14
    int-to-float v0, v0

    goto/32 :goto_b

    nop

    :goto_15
    const/high16 v0, 0x42800000    # 64.0f

    goto/32 :goto_10

    nop

    :goto_16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_23

    nop

    :goto_17
    move-wide v7, v10

    goto/32 :goto_3

    nop

    :goto_18
    move-object/from16 v2, p1

    goto/32 :goto_5c

    nop

    :goto_19
    if-gt v0, v1, :cond_2

    goto/32 :goto_2f

    :cond_2
    goto/32 :goto_5e

    nop

    :goto_1a
    if-gtz v8, :cond_3

    goto/32 :goto_37

    :cond_3
    goto/32 :goto_2b

    nop

    :goto_1b
    move/from16 v19, v15

    goto/32 :goto_40

    nop

    :goto_1c
    const-string v2, "WallpaperService"

    goto/32 :goto_3b

    nop

    :goto_1d
    return-void

    :goto_1e
    goto/32 :goto_35

    nop

    :goto_1f
    iget v0, v0, Landroid/graphics/Point;->y:I

    :goto_20
    goto/32 :goto_2

    nop

    :goto_21
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_1c

    nop

    :goto_22
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_59

    nop

    :goto_23
    move/from16 v1, v21

    goto/32 :goto_5d

    nop

    :goto_24
    move-wide/from16 v17, v12

    goto/32 :goto_39

    nop

    :goto_25
    invoke-virtual {v14}, Landroid/view/Surface;->isValid()Z

    move-result v0

    goto/32 :goto_e

    nop

    :goto_26
    float-to-int v5, v0

    goto/32 :goto_1a

    nop

    :goto_27
    goto/16 :goto_9

    :goto_28
    goto/32 :goto_f

    nop

    :goto_29
    move/from16 v20, v7

    goto/32 :goto_0

    nop

    :goto_2a
    move v15, v8

    goto/32 :goto_17

    nop

    :goto_2b
    if-lez v5, :cond_4

    goto/32 :goto_28

    :cond_4
    goto/32 :goto_53

    nop

    :goto_2c
    iget-object v0, v9, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceSize:Landroid/graphics/Point;

    goto/32 :goto_1f

    nop

    :goto_2d
    iget v0, v0, Landroid/graphics/Point;->x:I

    goto/32 :goto_48

    nop

    :goto_2e
    goto/16 :goto_60

    :goto_2f
    goto/32 :goto_5f

    nop

    :goto_30
    iget v1, v1, Landroid/graphics/Point;->y:I

    goto/32 :goto_19

    nop

    :goto_31
    mul-float v0, v0, v16

    goto/32 :goto_4

    nop

    :goto_32
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    goto/32 :goto_47

    nop

    :goto_33
    move v15, v0

    goto/32 :goto_12

    nop

    :goto_34
    iget v0, v0, Landroid/graphics/Point;->y:I

    goto/32 :goto_14

    nop

    :goto_35
    iget-object v0, v9, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    goto/32 :goto_5

    nop

    :goto_36
    return-void

    :goto_37
    goto/32 :goto_61

    nop

    :goto_38
    move v15, v5

    goto/32 :goto_51

    nop

    :goto_39
    move/from16 v19, v15

    goto/32 :goto_8

    nop

    :goto_3a
    move/from16 v21, v15

    goto/32 :goto_2a

    nop

    :goto_3b
    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_4e

    nop

    :goto_3c
    move-object v0, v3

    goto/32 :goto_6

    nop

    :goto_3d
    move/from16 v3, p2

    goto/32 :goto_62

    nop

    :goto_3e
    iget-object v0, v9, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceSize:Landroid/graphics/Point;

    goto/32 :goto_2d

    nop

    :goto_3f
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_d

    nop

    :goto_40
    move v15, v8

    goto/32 :goto_27

    nop

    :goto_41
    iget-object v0, v9, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceSize:Landroid/graphics/Point;

    goto/32 :goto_4f

    nop

    :goto_42
    iget-object v0, v9, Landroid/service/wallpaper/WallpaperService$Engine;->mHandler:Landroid/os/Handler;

    goto/32 :goto_50

    nop

    :goto_43
    return-void

    :goto_44
    goto/32 :goto_41

    nop

    :goto_45
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_3f

    nop

    :goto_46
    invoke-static {v0}, Landroid/os/Trace;->beginSection(Ljava/lang/String;)V

    goto/32 :goto_4b

    nop

    :goto_47
    const-wide/32 v2, 0xea60

    goto/32 :goto_54

    nop

    :goto_48
    int-to-float v0, v0

    goto/32 :goto_31

    nop

    :goto_49
    goto/16 :goto_20

    :goto_4a
    goto/32 :goto_2c

    nop

    :goto_4b
    new-instance v3, Landroid/service/wallpaper/WallpaperService$Engine$$ExternalSyntheticLambda0;

    goto/32 :goto_3c

    nop

    :goto_4c
    move/from16 v4, p3

    goto/32 :goto_58

    nop

    :goto_4d
    const-string v0, "WallpaperService#pixelCopy"

    goto/32 :goto_46

    nop

    :goto_4e
    return-void

    :goto_4f
    iget v0, v0, Landroid/graphics/Point;->x:I

    goto/32 :goto_5b

    nop

    :goto_50
    invoke-static {v14, v13, v12, v0}, Landroid/view/PixelCopy;->request(Landroid/view/Surface;Landroid/graphics/Bitmap;Landroid/view/PixelCopy$OnPixelCopyFinishedListener;Landroid/os/Handler;)V

    goto/32 :goto_36

    nop

    :goto_51
    move/from16 v5, p4

    goto/32 :goto_55

    nop

    :goto_52
    if-ltz v0, :cond_5

    goto/32 :goto_1e

    :cond_5
    goto/32 :goto_1d

    nop

    :goto_53
    move/from16 v21, v5

    goto/32 :goto_29

    nop

    :goto_54
    sub-long v10, v0, v2

    goto/32 :goto_1

    nop

    :goto_55
    move/from16 v20, v7

    goto/32 :goto_3a

    nop

    :goto_56
    iget v0, v0, Landroid/graphics/Point;->x:I

    goto/32 :goto_49

    nop

    :goto_57
    move-object v6, v4

    goto/32 :goto_4d

    nop

    :goto_58
    move/from16 v19, v15

    goto/32 :goto_38

    nop

    :goto_59
    const-string/jumbo v1, "wrong width and height values of bitmap "

    goto/32 :goto_45

    nop

    :goto_5a
    div-float v16, v0, v1

    goto/32 :goto_3e

    nop

    :goto_5b
    iget-object v1, v9, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceSize:Landroid/graphics/Point;

    goto/32 :goto_30

    nop

    :goto_5c
    move-wide/from16 v17, v12

    goto/32 :goto_13

    nop

    :goto_5d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_21

    nop

    :goto_5e
    const/4 v0, 0x1

    goto/32 :goto_2e

    nop

    :goto_5f
    const/4 v0, 0x0

    :goto_60
    goto/32 :goto_33

    nop

    :goto_61
    move/from16 v21, v5

    goto/32 :goto_66

    nop

    :goto_62
    move-object v13, v4

    goto/32 :goto_4c

    nop

    :goto_63
    invoke-static {v8, v5, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    goto/32 :goto_57

    nop

    :goto_64
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_22

    nop

    :goto_65
    move-object/from16 v9, p0

    goto/32 :goto_32

    nop

    :goto_66
    move/from16 v20, v7

    goto/32 :goto_24

    nop
.end method

.method updateSurface(ZZZ)V
    .locals 50

    goto/32 :goto_be

    nop

    :goto_0
    iget v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWindowPrivateFlags:I

    goto/32 :goto_10d

    nop

    :goto_1
    move/from16 v36, v9

    goto/32 :goto_f8

    nop

    :goto_2
    move/from16 v27, v5

    goto/32 :goto_180

    nop

    :goto_3
    return-void

    :goto_4
    goto/16 :goto_93

    :goto_5
    goto/32 :goto_1

    nop

    :goto_6
    move/from16 v21, v4

    goto/32 :goto_50

    nop

    :goto_7
    move/from16 v27, v5

    goto/32 :goto_fa

    nop

    :goto_8
    if-eq v3, v0, :cond_0

    goto/32 :goto_164

    :cond_0
    goto/32 :goto_19e

    nop

    :goto_9
    const/4 v0, 0x1

    :goto_a
    goto/32 :goto_18b

    nop

    :goto_b
    move v11, v12

    goto/32 :goto_bc

    nop

    :goto_c
    move/from16 v21, v4

    goto/32 :goto_d5

    nop

    :goto_d
    const/16 v27, 0x0

    goto/32 :goto_90

    nop

    :goto_e
    move/from16 v27, v5

    goto/32 :goto_44

    nop

    :goto_f
    iget v14, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowFlags:I

    goto/32 :goto_b3

    nop

    :goto_10
    move/from16 v24, v7

    goto/32 :goto_ce

    nop

    :goto_11
    move/from16 v25, v8

    :try_start_0
    iget-object v8, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mTempControls:[Landroid/view/InsetsSourceControl;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_f

    goto/32 :goto_7a

    nop

    :goto_12
    const/4 v2, 0x0

    :try_start_1
    iput-boolean v2, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mIsCreating:Z

    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceCreated:Z

    if-eqz v4, :cond_1

    invoke-direct/range {p0 .. p0}, Landroid/service/wallpaper/WallpaperService$Engine;->resetWindowPages()V

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSession:Landroid/view/IWindowSession;

    iget-object v2, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWindow:Lcom/android/internal/view/BaseIWindow;

    const v5, 0x7fffffff

    const/4 v6, 0x0

    invoke-interface {v0, v2, v6, v5}, Landroid/view/IWindowSession;->finishDrawing(Landroid/view/IWindow;Landroid/view/SurfaceControl$Transaction;I)V

    iget v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingXOffset:F

    iget v2, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingXOffsetStep:F

    invoke-direct {v1, v0, v2}, Landroid/service/wallpaper/WallpaperService$Engine;->processLocalColors(FF)V

    :cond_1
    invoke-direct/range {p0 .. p0}, Landroid/service/wallpaper/WallpaperService$Engine;->reposition()V

    invoke-virtual/range {p0 .. p0}, Landroid/service/wallpaper/WallpaperService$Engine;->shouldWaitForEngineShown()Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/service/wallpaper/WallpaperService$Engine;->reportEngineShown(Z)V

    nop

    move v11, v12

    move/from16 v12, v17

    goto/16 :goto_93

    :catchall_0
    move-exception v0

    move/from16 v30, v7

    move-object/from16 v31, v8

    goto :goto_13

    :catchall_1
    move-exception v0

    move/from16 v29, v6

    move/from16 v30, v7

    move-object/from16 v31, v8

    goto :goto_13

    :catchall_2
    move-exception v0

    move/from16 v28, v2

    move/from16 v29, v6

    move/from16 v30, v7

    move-object/from16 v31, v8

    goto :goto_13

    :catchall_3
    move-exception v0

    move/from16 v28, v2

    move/from16 v27, v5

    move/from16 v29, v6

    move/from16 v30, v7

    move-object/from16 v31, v8

    move/from16 v4, p3

    :goto_13
    const/4 v2, 0x0

    iput-boolean v2, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mIsCreating:Z

    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceCreated:Z

    if-eqz v4, :cond_2

    invoke-direct/range {p0 .. p0}, Landroid/service/wallpaper/WallpaperService$Engine;->resetWindowPages()V

    iget-object v2, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSession:Landroid/view/IWindowSession;

    iget-object v5, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWindow:Lcom/android/internal/view/BaseIWindow;

    const v6, 0x7fffffff

    const/4 v7, 0x0

    invoke-interface {v2, v5, v7, v6}, Landroid/view/IWindowSession;->finishDrawing(Landroid/view/IWindow;Landroid/view/SurfaceControl$Transaction;I)V

    iget v2, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingXOffset:F

    iget v5, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mPendingXOffsetStep:F

    invoke-direct {v1, v2, v5}, Landroid/service/wallpaper/WallpaperService$Engine;->processLocalColors(FF)V

    :cond_2
    invoke-direct/range {p0 .. p0}, Landroid/service/wallpaper/WallpaperService$Engine;->reposition()V

    invoke-virtual/range {p0 .. p0}, Landroid/service/wallpaper/WallpaperService$Engine;->shouldWaitForEngineShown()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/service/wallpaper/WallpaperService$Engine;->reportEngineShown(Z)V

    nop

    throw v0
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    goto/32 :goto_10c

    nop

    :goto_14
    goto/16 :goto_4b

    :goto_15
    goto/32 :goto_198

    nop

    :goto_16
    goto/16 :goto_49

    :goto_17
    goto/32 :goto_48

    nop

    :goto_18
    invoke-virtual {v4}, Lcom/android/internal/view/BaseSurfaceHolder;->getRequestedHeight()I

    move-result v4

    goto/32 :goto_161

    nop

    :goto_19
    move/from16 v24, v7

    goto/32 :goto_19c

    nop

    :goto_1a
    const/16 v19, 0x0

    goto/32 :goto_72

    nop

    :goto_1b
    move/from16 v25, v8

    goto/32 :goto_88

    nop

    :goto_1c
    move/from16 v20, v3

    goto/32 :goto_25

    nop

    :goto_1d
    if-eqz v4, :cond_3

    goto/32 :goto_eb

    :cond_3
    :try_start_2
    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    iget-object v0, v0, Landroid/view/WindowManager$LayoutParams;->surfaceInsets:Landroid/graphics/Rect;

    iget-object v2, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    iget-object v2, v2, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mDisplayPadding:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    goto/32 :goto_53

    nop

    :goto_1e
    move v0, v6

    :goto_1f
    goto/32 :goto_162

    nop

    :goto_20
    move/from16 v20, v3

    goto/32 :goto_bf

    nop

    :goto_21
    move/from16 v12, v17

    goto/32 :goto_9a

    nop

    :goto_22
    move/from16 v30, v7

    goto/32 :goto_d3

    nop

    :goto_23
    goto/16 :goto_93

    :goto_24
    :try_start_3
    iput v3, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWidth:I

    iput v5, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mHeight:I

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    invoke-virtual {v0}, Lcom/android/internal/view/BaseSurfaceHolder;->getRequestedFormat()I

    move-result v0

    iput v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mFormat:I

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    invoke-virtual {v0}, Lcom/android/internal/view/BaseSurfaceHolder;->getRequestedType()I

    move-result v0

    iput v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mType:I

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    iput v10, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    iput v10, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    iget v15, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mFormat:I

    iput v15, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    iget v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowFlags:I

    iput v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWindowFlags:I

    iget-object v15, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    or-int/lit16 v0, v0, 0x200

    const/high16 v16, 0x10000

    or-int v0, v0, v16

    or-int/lit16 v0, v0, 0x100

    or-int/lit8 v0, v0, 0x8

    iput v0, v15, Landroid/view/WindowManager$LayoutParams;->flags:I

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mMergedConfiguration:Landroid/util/MergedConfiguration;

    invoke-virtual {v0}, Landroid/util/MergedConfiguration;->getMergedConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    move-object v15, v0

    iget-object v0, v15, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/app/WindowConfiguration;->getMaxBounds()Landroid/graphics/Rect;

    move-result-object v18
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_d

    goto/32 :goto_6a

    nop

    :goto_25
    move/from16 v21, v4

    goto/32 :goto_6f

    nop

    :goto_26
    goto/16 :goto_b9

    :goto_27
    goto/32 :goto_125

    nop

    :goto_28
    move/from16 v26, v12

    goto/32 :goto_16b

    nop

    :goto_29
    move/from16 v35, v14

    goto/32 :goto_166

    nop

    :goto_2a
    move v5, v4

    goto/32 :goto_d6

    nop

    :goto_2b
    goto/16 :goto_93

    :cond_4
    :goto_2c
    :try_start_4
    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLastSurfaceSize:Landroid/graphics/Point;

    iget-object v7, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceSize:Landroid/graphics/Point;

    invoke-virtual {v0, v7}, Landroid/graphics/Point;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_8

    goto/32 :goto_a4

    nop

    :goto_2d
    move/from16 v11, v33

    goto/32 :goto_148

    nop

    :goto_2e
    move/from16 v28, v2

    goto/32 :goto_14e

    nop

    :goto_2f
    move/from16 v11, v33

    goto/32 :goto_8e

    nop

    :goto_30
    move/from16 v21, v4

    goto/32 :goto_c6

    nop

    :goto_31
    move/from16 v25, v8

    goto/32 :goto_63

    nop

    :goto_32
    if-eqz v25, :cond_5

    goto/32 :goto_13a

    :cond_5
    goto/32 :goto_96

    nop

    :goto_33
    if-eqz v0, :cond_6

    goto/32 :goto_102

    :cond_6
    goto/32 :goto_101

    nop

    :goto_34
    move/from16 v21, v4

    goto/32 :goto_4c

    nop

    :goto_35
    move/from16 v27, v5

    goto/32 :goto_64

    nop

    :goto_36
    move/from16 v28, v2

    goto/32 :goto_b5

    nop

    :goto_37
    move/from16 v20, v3

    goto/32 :goto_bb

    nop

    :goto_38
    move v7, v0

    goto/32 :goto_42

    nop

    :goto_39
    move/from16 v24, v7

    goto/32 :goto_1b

    nop

    :goto_3a
    move-object/from16 v17, v0

    :try_start_5
    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mDispatchedContentInsets:Landroid/graphics/Rect;

    invoke-virtual {v0, v13}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_9

    goto/32 :goto_124

    nop

    :goto_3b
    move/from16 v20, v3

    goto/32 :goto_114

    nop

    :goto_3c
    goto/16 :goto_15e

    :goto_3d
    goto/32 :goto_15d

    nop

    :goto_3e
    xor-int/2addr v0, v6

    goto/32 :goto_10a

    nop

    :goto_3f
    move/from16 v35, v14

    goto/32 :goto_184

    nop

    :goto_40
    if-eqz p2, :cond_7

    goto/32 :goto_13a

    :cond_7
    goto/32 :goto_4e

    nop

    :goto_41
    iget-object v9, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    goto/32 :goto_9d

    nop

    :goto_42
    iget-boolean v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceCreated:Z

    goto/32 :goto_3e

    nop

    :goto_43
    if-eqz v0, :cond_8

    goto/32 :goto_3d

    :cond_8
    goto/32 :goto_158

    nop

    :goto_44
    move/from16 v26, v12

    goto/32 :goto_122

    nop

    :goto_45
    move/from16 v27, v5

    goto/32 :goto_f7

    nop

    :goto_46
    move/from16 v20, v3

    goto/32 :goto_c

    nop

    :goto_47
    move/from16 v27, v5

    goto/32 :goto_10

    nop

    :goto_48
    const/4 v0, 0x0

    :goto_49
    goto/32 :goto_11b

    nop

    :goto_4a
    move v4, v0

    :goto_4b
    goto/32 :goto_d0

    nop

    :goto_4c
    move/from16 v27, v5

    goto/32 :goto_115

    nop

    :goto_4d
    move v11, v12

    goto/32 :goto_83

    nop

    :goto_4e
    if-eqz v24, :cond_9

    goto/32 :goto_13a

    :cond_9
    goto/32 :goto_32

    nop

    :goto_4f
    goto :goto_58

    :cond_a
    goto/32 :goto_d1

    nop

    :goto_50
    move/from16 v27, v5

    goto/32 :goto_120

    nop

    :goto_51
    move/from16 v24, v7

    goto/32 :goto_10e

    nop

    :goto_52
    move/from16 v27, v5

    goto/32 :goto_84

    nop

    :goto_53
    goto/16 :goto_ec

    :catch_1
    move-exception v0

    goto/32 :goto_15b

    nop

    :goto_54
    return-void

    :catch_2
    move-exception v0

    goto/32 :goto_f1

    nop

    :goto_55
    if-lez v3, :cond_b

    goto/32 :goto_e0

    :cond_b
    goto/32 :goto_62

    nop

    :goto_56
    move/from16 v12, v26

    goto/32 :goto_13c

    nop

    :goto_57
    move/from16 v27, v5

    :goto_58
    goto/32 :goto_126

    nop

    :goto_59
    move v0, v10

    :goto_5a
    goto/32 :goto_9b

    nop

    :goto_5b
    goto/16 :goto_131

    :goto_5c
    goto/32 :goto_130

    nop

    :goto_5d
    if-eqz p1, :cond_c

    goto/32 :goto_24

    :cond_c
    goto/32 :goto_12a

    nop

    :goto_5e
    move/from16 v33, v11

    :try_start_6
    iget-object v11, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_11

    goto/32 :goto_d

    nop

    :goto_5f
    move/from16 v21, v4

    goto/32 :goto_160

    nop

    :goto_60
    goto/16 :goto_147

    :goto_61
    goto/32 :goto_146

    nop

    :goto_62
    const/4 v3, -0x1

    goto/32 :goto_df

    nop

    :goto_63
    move/from16 v36, v9

    goto/32 :goto_175

    nop

    :goto_64
    move/from16 v12, v26

    goto/32 :goto_16f

    nop

    :goto_65
    move v0, v3

    goto/32 :goto_168

    nop

    :goto_66
    goto/16 :goto_93

    :catch_3
    move-exception v0

    goto/32 :goto_c5

    nop

    :goto_67
    iget-boolean v0, v0, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mShownReported:Z

    goto/32 :goto_33

    nop

    :goto_68
    move/from16 v36, v9

    goto/32 :goto_10b

    nop

    :goto_69
    move-object/from16 v0, v26

    goto/32 :goto_13e

    nop

    :goto_6a
    const/4 v0, -0x1

    goto/32 :goto_8

    nop

    :goto_6b
    if-nez v12, :cond_d

    goto/32 :goto_8c

    :cond_d
    goto/32 :goto_8b

    nop

    :goto_6c
    move/from16 v2, v17

    goto/32 :goto_116

    nop

    :goto_6d
    move/from16 v21, v4

    goto/32 :goto_150

    nop

    :goto_6e
    move/from16 v4, p3

    goto/32 :goto_163

    nop

    :goto_6f
    move/from16 v27, v5

    goto/32 :goto_a8

    nop

    :goto_70
    move/from16 v27, v5

    goto/32 :goto_c0

    nop

    :goto_71
    invoke-virtual {v13}, Lcom/android/internal/view/BaseSurfaceHolder;->getRequestedType()I

    move-result v13

    goto/32 :goto_86

    nop

    :goto_72
    move/from16 v21, v4

    :try_start_7
    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    invoke-virtual {v0}, Lcom/android/internal/view/BaseSurfaceHolder;->ungetCallbacks()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    goto/32 :goto_142

    nop

    :goto_73
    move-object/from16 v31, v8

    :try_start_8
    iget v8, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mCurHeight:I

    invoke-interface {v6, v0, v2, v7, v8}, Landroid/view/SurfaceHolder$Callback;->surfaceChanged(Landroid/view/SurfaceHolder;III)V

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p3

    move/from16 v2, v26

    move/from16 v7, v30

    move-object/from16 v8, v31

    goto/16 :goto_7d

    :catchall_4
    move-exception v0

    move-object/from16 v31, v8

    goto/16 :goto_13

    :cond_e
    move-object/from16 p3, v0

    move/from16 v30, v7

    move-object/from16 v31, v8

    goto :goto_74

    :cond_f
    move-object/from16 p3, v0

    move/from16 v30, v7

    move-object/from16 v31, v8

    :goto_74
    if-eqz v17, :cond_10

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mDispatchedContentInsets:Landroid/graphics/Rect;

    invoke-virtual {v0, v13}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mDispatchedStableInsets:Landroid/graphics/Rect;

    invoke-virtual {v0, v14}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iput-object v3, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mDispatchedDisplayCutout:Landroid/view/DisplayCutout;

    invoke-virtual {v1, v11}, Landroid/service/wallpaper/WallpaperService$Engine;->onApplyWindowInsets(Landroid/view/WindowInsets;)V

    goto :goto_75

    :catchall_5
    move-exception v0

    goto/16 :goto_13

    :cond_10
    :goto_75
    if-eqz v4, :cond_12

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    invoke-virtual {v1, v0}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceRedrawNeeded(Landroid/view/SurfaceHolder;)V

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    invoke-virtual {v0}, Lcom/android/internal/view/BaseSurfaceHolder;->getCallbacks()[Landroid/view/SurfaceHolder$Callback;

    move-result-object v0

    if-eqz v0, :cond_12

    array-length v2, v0

    const/4 v5, 0x0

    :goto_76
    if-ge v5, v2, :cond_12

    aget-object v6, v0, v5

    instance-of v7, v6, Landroid/view/SurfaceHolder$Callback2;

    if-eqz v7, :cond_11

    move-object v7, v6

    check-cast v7, Landroid/view/SurfaceHolder$Callback2;

    iget-object v8, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    invoke-interface {v7, v8}, Landroid/view/SurfaceHolder$Callback2;->surfaceRedrawNeeded(Landroid/view/SurfaceHolder;)V

    :cond_11
    add-int/lit8 v5, v5, 0x1

    goto :goto_76

    :cond_12
    if-eqz v19, :cond_14

    iget-boolean v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mReportedVisible:Z

    if-nez v0, :cond_14

    iget-boolean v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mIsCreating:Z

    if-eqz v0, :cond_13

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/service/wallpaper/WallpaperService$Engine;->onVisibilityChanged(Z)V

    :cond_13
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/service/wallpaper/WallpaperService$Engine;->onVisibilityChanged(Z)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_5

    :cond_14
    goto/32 :goto_12

    nop

    :goto_77
    goto/16 :goto_58

    :catchall_6
    move-exception v0

    goto/32 :goto_45

    nop

    :goto_78
    or-int v17, v19, v0

    :try_start_9
    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    invoke-virtual {v0, v7, v10}, Lcom/android/internal/view/BaseSurfaceHolder;->setSurfaceFrameSize(II)V

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    iget-object v0, v0, Lcom/android/internal/view/BaseSurfaceHolder;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    iget-object v0, v0, Lcom/android/internal/view/BaseSurfaceHolder;->mSurface:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->isValid()Z

    move-result v0
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_4

    goto/32 :goto_192

    nop

    :goto_79
    move/from16 v25, v8

    goto/32 :goto_183

    nop

    :goto_7a
    move/from16 v26, v12

    :try_start_a
    iget-object v12, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSyncSeqIdBundle:Landroid/os/Bundle;

    move-object/from16 v37, v2

    move-object/from16 v38, v6

    move-object/from16 v39, v9

    move/from16 v40, v10

    move/from16 v41, v11

    move-object/from16 v44, v13

    move-object/from16 v45, v14

    move-object/from16 v46, v0

    move-object/from16 v47, v7

    move-object/from16 v48, v8

    move-object/from16 v49, v12

    invoke-interface/range {v37 .. v49}, Landroid/view/IWindowSession;->relayout(Landroid/view/IWindow;Landroid/view/WindowManager$LayoutParams;IIIILandroid/window/ClientWindowFrames;Landroid/util/MergedConfiguration;Landroid/view/SurfaceControl;Landroid/view/InsetsState;[Landroid/view/InsetsSourceControl;Landroid/os/Bundle;)I

    move-result v0

    move v2, v0

    :goto_7b
    iget v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mDisplayInstallOrientation:I

    iget-object v6, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mDisplay:Landroid/view/Display;

    invoke-virtual {v6}, Landroid/view/Display;->getRotation()I

    move-result v6

    add-int/2addr v0, v6

    rem-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Landroid/view/SurfaceControl;->rotationToBufferTransform(I)I

    move-result v0

    move v6, v0

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v0, v6}, Landroid/view/SurfaceControl;->setTransformHint(I)V

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    iget v7, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWidth:I

    iget v8, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mHeight:I

    iget-object v9, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWinFrames:Landroid/window/ClientWindowFrames;

    iget-object v9, v9, Landroid/window/ClientWindowFrames;->frame:Landroid/graphics/Rect;

    const/16 v22, 0x0

    iget-object v10, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceSize:Landroid/graphics/Point;

    move-object/from16 v17, v0

    move/from16 v19, v7

    move/from16 v20, v8

    move-object/from16 v21, v9

    move-object/from16 v23, v10

    invoke-static/range {v17 .. v23}, Landroid/view/WindowLayout;->computeSurfaceSize(Landroid/view/WindowManager$LayoutParams;Landroid/graphics/Rect;IILandroid/graphics/Rect;ZLandroid/graphics/Point;)V

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v0}, Landroid/view/SurfaceControl;->isValid()Z

    move-result v0
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_a} :catch_8

    goto/32 :goto_167

    nop

    :goto_7c
    move/from16 v29, v6

    :try_start_b
    iget v6, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mCurHeight:I

    invoke-virtual {v1, v0, v5, v2, v6}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceChanged(Landroid/view/SurfaceHolder;III)V

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    invoke-virtual {v0}, Lcom/android/internal/view/BaseSurfaceHolder;->getCallbacks()[Landroid/view/SurfaceHolder$Callback;

    move-result-object v0

    if-eqz v0, :cond_f

    array-length v2, v0

    const/4 v5, 0x0

    :goto_7d
    if-ge v5, v2, :cond_e

    aget-object v6, v0, v5

    move-object/from16 p3, v0

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    move/from16 v26, v2

    iget v2, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mFormat:I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/32 :goto_10f

    nop

    :goto_7e
    move v11, v12

    goto/32 :goto_8d

    nop

    :goto_7f
    goto/16 :goto_93

    :goto_80
    goto/32 :goto_1a

    nop

    :goto_81
    move/from16 v4, p3

    goto/32 :goto_4

    nop

    :goto_82
    if-ne v0, v5, :cond_15

    goto/32 :goto_137

    :cond_15
    goto/32 :goto_136

    nop

    :goto_83
    move/from16 v12, v17

    goto/32 :goto_c7

    nop

    :goto_84
    move/from16 v24, v7

    goto/32 :goto_121

    nop

    :goto_85
    move/from16 v4, p3

    goto/32 :goto_c2

    nop

    :goto_86
    if-ne v0, v13, :cond_16

    goto/32 :goto_5c

    :cond_16
    goto/32 :goto_12c

    nop

    :goto_87
    move v11, v12

    goto/32 :goto_a3

    nop

    :goto_88
    move/from16 v26, v12

    goto/32 :goto_85

    nop

    :goto_89
    move/from16 v12, v19

    goto/32 :goto_aa

    nop

    :goto_8a
    move/from16 v35, v14

    goto/32 :goto_6e

    nop

    :goto_8b
    goto/16 :goto_13a

    :goto_8c
    goto/32 :goto_36

    nop

    :goto_8d
    move/from16 v12, v17

    goto/32 :goto_b2

    nop

    :goto_8e
    move/from16 v4, p3

    goto/32 :goto_19b

    nop

    :goto_8f
    move v10, v7

    goto/32 :goto_e8

    nop

    :goto_90
    move/from16 v34, v13

    :try_start_c
    iget-object v13, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mDisplay:Landroid/view/Display;

    invoke-virtual {v13}, Landroid/view/Display;->getDisplayId()I

    move-result v28

    iget-object v13, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mRequestedVisibilities:Landroid/view/InsetsVisibilities;
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_c} :catch_10

    goto/32 :goto_cb

    nop

    :goto_91
    move/from16 v4, p3

    goto/32 :goto_da

    nop

    :goto_92
    move/from16 v4, p3

    :goto_93
    goto/32 :goto_3

    nop

    :goto_94
    xor-int/2addr v0, v6

    goto/32 :goto_38

    nop

    :goto_95
    if-eqz v13, :cond_17

    goto/32 :goto_24

    :cond_17
    goto/32 :goto_ca

    nop

    :goto_96
    if-eqz v36, :cond_18

    goto/32 :goto_13a

    :cond_18
    goto/32 :goto_6b

    nop

    :goto_97
    if-nez v0, :cond_19

    goto/32 :goto_191

    :cond_19
    goto/32 :goto_190

    nop

    :goto_98
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_69

    nop

    :goto_99
    move v0, v6

    goto/32 :goto_a6

    nop

    :goto_9a
    goto/16 :goto_93

    :catch_4
    move-exception v0

    goto/32 :goto_127

    nop

    :goto_9b
    move v9, v0

    goto/32 :goto_af

    nop

    :goto_9c
    if-ne v0, v7, :cond_1a

    goto/32 :goto_db

    :cond_1a
    goto/32 :goto_13b

    nop

    :goto_9d
    invoke-virtual {v9}, Lcom/android/internal/view/BaseSurfaceHolder;->getRequestedFormat()I

    move-result v9

    goto/32 :goto_15a

    nop

    :goto_9e
    move v7, v0

    :goto_9f
    :try_start_d
    iget v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWidth:I
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_d} :catch_8

    goto/32 :goto_9c

    nop

    :goto_a0
    iget-object v13, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    goto/32 :goto_71

    nop

    :goto_a1
    move/from16 v20, v3

    goto/32 :goto_34

    nop

    :goto_a2
    goto/16 :goto_173

    :catchall_7
    move-exception v0

    goto/32 :goto_107

    nop

    :goto_a3
    move/from16 v12, v19

    goto/32 :goto_dc

    nop

    :goto_a4
    if-eqz v0, :cond_1b

    goto/32 :goto_a5

    :cond_1b
    :try_start_e
    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLastSurfaceSize:Landroid/graphics/Point;

    iget-object v7, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceSize:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->x:I

    iget-object v8, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceSize:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v7, v8}, Landroid/graphics/Point;->set(II)V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_e} :catch_a

    :goto_a5
    :try_start_f
    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWinFrames:Landroid/window/ClientWindowFrames;

    iget-object v0, v0, Landroid/window/ClientWindowFrames;->frame:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget-object v7, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWinFrames:Landroid/window/ClientWindowFrames;

    iget-object v7, v7, Landroid/window/ClientWindowFrames;->frame:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v7

    iget-object v8, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mInsetsState:Landroid/view/InsetsState;

    invoke-virtual {v8}, Landroid/view/InsetsState;->getDisplayCutout()Landroid/view/DisplayCutout;

    move-result-object v8

    new-instance v9, Landroid/graphics/Rect;

    iget-object v10, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWinFrames:Landroid/window/ClientWindowFrames;

    iget-object v10, v10, Landroid/window/ClientWindowFrames;->frame:Landroid/graphics/Rect;

    invoke-direct {v9, v10}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iget-object v10, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mInsetsState:Landroid/view/InsetsState;

    invoke-virtual {v10}, Landroid/view/InsetsState;->getDisplayFrame()Landroid/graphics/Rect;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    iget-object v10, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mInsetsState:Landroid/view/InsetsState;

    const/16 v39, 0x0

    invoke-virtual {v15}, Landroid/content/res/Configuration;->isScreenRound()Z

    move-result v40

    const/16 v41, 0x0

    iget-object v11, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    iget v11, v11, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    iget-object v12, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    iget v12, v12, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/16 v44, 0x0

    iget-object v13, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    iget v13, v13, Landroid/view/WindowManager$LayoutParams;->type:I

    invoke-virtual/range {v16 .. v16}, Landroid/app/WindowConfiguration;->getWindowingMode()I

    move-result v46

    const/16 v47, 0x0

    move-object/from16 v37, v10

    move-object/from16 v38, v9

    move/from16 v42, v11

    move/from16 v43, v12

    move/from16 v45, v13

    invoke-virtual/range {v37 .. v47}, Landroid/view/InsetsState;->calculateInsets(Landroid/graphics/Rect;Landroid/view/InsetsState;ZZIIIIILandroid/util/SparseIntArray;)Landroid/view/WindowInsets;

    move-result-object v10
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_f} :catch_8

    goto/32 :goto_c8

    nop

    :goto_a6
    goto/16 :goto_5a

    :goto_a7
    goto/32 :goto_59

    nop

    :goto_a8
    move/from16 v24, v7

    goto/32 :goto_31

    nop

    :goto_a9
    move-object/from16 v3, v17

    :try_start_10
    invoke-virtual {v0, v3}, Landroid/view/DisplayCutout;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_10 .. :try_end_10} :catch_7

    goto/32 :goto_43

    nop

    :goto_aa
    goto/16 :goto_93

    :catch_5
    move-exception v0

    goto/32 :goto_20

    nop

    :goto_ab
    move/from16 v28, v2

    :try_start_11
    iget v2, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWidth:I
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    goto/32 :goto_7c

    nop

    :goto_ac
    move/from16 v27, v5

    goto/32 :goto_14f

    nop

    :goto_ad
    move/from16 v30, v7

    goto/32 :goto_196

    nop

    :goto_ae
    goto/16 :goto_178

    :catch_6
    move-exception v0

    goto/32 :goto_46

    nop

    :goto_af
    iget v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWidth:I

    goto/32 :goto_13f

    nop

    :goto_b0
    move/from16 v21, v4

    goto/32 :goto_2

    nop

    :goto_b1
    move/from16 v21, v4

    goto/32 :goto_7

    nop

    :goto_b2
    goto/16 :goto_93

    :catch_7
    move-exception v0

    goto/32 :goto_b0

    nop

    :goto_b3
    if-eq v0, v14, :cond_1c

    goto/32 :goto_b9

    :cond_1c
    goto/32 :goto_0

    nop

    :goto_b4
    and-int/lit8 v0, v2, 0x1

    goto/32 :goto_97

    nop

    :goto_b5
    move/from16 v29, v6

    goto/32 :goto_ad

    nop

    :goto_b6
    goto/16 :goto_100

    :goto_b7
    goto/32 :goto_ff

    nop

    :goto_b8
    goto/16 :goto_f3

    :goto_b9
    goto/32 :goto_f2

    nop

    :goto_ba
    const/16 v19, 0x1

    :try_start_12
    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    iget v5, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mFormat:I
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_2

    goto/32 :goto_ab

    nop

    :goto_bb
    move/from16 v21, v4

    goto/32 :goto_52

    nop

    :goto_bc
    move/from16 v12, v26

    goto/32 :goto_d7

    nop

    :goto_bd
    iget v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWindowFlags:I

    goto/32 :goto_f

    nop

    :goto_be
    move-object/from16 v1, p0

    goto/32 :goto_103

    nop

    :goto_bf
    move/from16 v21, v4

    goto/32 :goto_ac

    nop

    :goto_c0
    move/from16 v25, v8

    goto/32 :goto_14c

    nop

    :goto_c1
    move/from16 v27, v5

    goto/32 :goto_4d

    nop

    :goto_c2
    move/from16 v11, v33

    goto/32 :goto_152

    nop

    :goto_c3
    if-eqz v11, :cond_1d

    goto/32 :goto_24

    :cond_1d
    goto/32 :goto_95

    nop

    :goto_c4
    invoke-virtual {v3}, Lcom/android/internal/view/BaseSurfaceHolder;->getRequestedWidth()I

    move-result v3

    goto/32 :goto_55

    nop

    :goto_c5
    move/from16 v20, v3

    goto/32 :goto_17a

    nop

    :goto_c6
    move/from16 v27, v5

    goto/32 :goto_108

    nop

    :goto_c7
    move/from16 v4, p3

    goto/32 :goto_7f

    nop

    :goto_c8
    if-eqz v4, :cond_1e

    goto/32 :goto_129

    :cond_1e
    :try_start_13
    iget-object v11, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    iget-object v11, v11, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mDisplayPadding:Landroid/graphics/Rect;

    iget v12, v11, Landroid/graphics/Rect;->left:I

    iget v13, v11, Landroid/graphics/Rect;->right:I

    add-int/2addr v12, v13

    add-int/2addr v0, v12

    iget v12, v11, Landroid/graphics/Rect;->top:I

    iget v13, v11, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v12, v13

    add-int/2addr v7, v12

    iget v12, v11, Landroid/graphics/Rect;->left:I

    neg-int v12, v12

    iget v13, v11, Landroid/graphics/Rect;->top:I

    neg-int v13, v13

    iget v14, v11, Landroid/graphics/Rect;->right:I

    neg-int v14, v14

    move/from16 v17, v0

    iget v0, v11, Landroid/graphics/Rect;->bottom:I

    neg-int v0, v0

    invoke-virtual {v10, v12, v13, v14, v0}, Landroid/view/WindowInsets;->insetUnchecked(IIII)Landroid/view/WindowInsets;

    move-result-object v0
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_13 .. :try_end_13} :catch_a

    goto/32 :goto_cf

    nop

    :goto_c9
    if-lt v5, v4, :cond_1f

    goto/32 :goto_f5

    :cond_1f
    :try_start_14
    aget-object v26, v0, v5

    move-object/from16 v28, v26

    move-object/from16 v26, v0

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    move/from16 v29, v4

    move-object/from16 v4, v28

    invoke-interface {v4, v0}, Landroid/view/SurfaceHolder$Callback;->surfaceCreated(Landroid/view/SurfaceHolder;)V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_7

    goto/32 :goto_98

    nop

    :goto_ca
    if-eqz v14, :cond_20

    goto/32 :goto_24

    :cond_20
    goto/32 :goto_12f

    nop

    :goto_cb
    move/from16 v35, v14

    :try_start_15
    iget-object v14, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mInsetsState:Landroid/view/InsetsState;
    :try_end_15
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_15} :catch_b

    goto/32 :goto_fb

    nop

    :goto_cc
    move/from16 v34, v13

    goto/32 :goto_8a

    nop

    :goto_cd
    move/from16 v21, v4

    goto/32 :goto_47

    nop

    :goto_ce
    move/from16 v25, v8

    goto/32 :goto_132

    nop

    :goto_cf
    move-object v10, v0

    goto/32 :goto_18d

    nop

    :goto_d0
    iget-boolean v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mCreated:Z

    goto/32 :goto_1a0

    nop

    :goto_d1
    move-object/from16 v26, v0

    goto/32 :goto_16c

    nop

    :goto_d2
    const/4 v0, 0x1

    goto/32 :goto_16

    nop

    :goto_d3
    move-object/from16 v31, v8

    goto/32 :goto_186

    nop

    :goto_d4
    const/4 v4, 0x1

    :try_start_16
    iput-boolean v4, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mIsCreating:Z

    const/16 v19, 0x1

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    invoke-virtual {v1, v0}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceCreated(Landroid/view/SurfaceHolder;)V

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    invoke-virtual {v0}, Lcom/android/internal/view/BaseSurfaceHolder;->getCallbacks()[Landroid/view/SurfaceHolder$Callback;

    move-result-object v0

    if-eqz v0, :cond_a

    array-length v4, v0
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_6

    goto/32 :goto_113

    nop

    :goto_d5
    move/from16 v27, v5

    goto/32 :goto_e9

    nop

    :goto_d6
    move v4, v0

    goto/32 :goto_14

    nop

    :goto_d7
    goto/16 :goto_93

    :catch_8
    move-exception v0

    goto/32 :goto_a1

    nop

    :goto_d8
    return-void

    :goto_d9
    goto/32 :goto_fd

    nop

    :goto_da
    goto/16 :goto_93

    :goto_db
    goto/32 :goto_177

    nop

    :goto_dc
    goto/16 :goto_93

    :catch_9
    move-exception v0

    goto/32 :goto_11e

    nop

    :goto_dd
    move/from16 v4, p3

    goto/32 :goto_199

    nop

    :goto_de
    if-nez v0, :cond_21

    goto/32 :goto_d9

    :cond_21
    goto/32 :goto_134

    nop

    :goto_df
    goto/16 :goto_a

    :goto_e0
    goto/32 :goto_9

    nop

    :goto_e1
    move/from16 v35, v14

    goto/32 :goto_92

    nop

    :goto_e2
    move/from16 v20, v3

    goto/32 :goto_b1

    nop

    :goto_e3
    if-eqz v9, :cond_22

    goto/32 :goto_24

    :cond_22
    goto/32 :goto_c3

    nop

    :goto_e4
    move/from16 v20, v3

    goto/32 :goto_6d

    nop

    :goto_e5
    move/from16 v20, v3

    goto/32 :goto_cd

    nop

    :goto_e6
    const/4 v12, 0x1

    :try_start_17
    iput v10, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mCurHeight:I
    :try_end_17
    .catch Landroid/os/RemoteException; {:try_start_17 .. :try_end_17} :catch_6

    :goto_e7
    :try_start_18
    invoke-virtual {v11}, Landroid/view/WindowInsets;->getSystemWindowInsets()Landroid/graphics/Insets;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Insets;->toRect()Landroid/graphics/Rect;

    move-result-object v0

    move-object v13, v0

    invoke-virtual {v11}, Landroid/view/WindowInsets;->getStableInsets()Landroid/graphics/Insets;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Insets;->toRect()Landroid/graphics/Rect;

    move-result-object v0

    move-object v14, v0

    invoke-virtual {v11}, Landroid/view/WindowInsets;->getDisplayCutout()Landroid/view/DisplayCutout;

    move-result-object v0
    :try_end_18
    .catch Landroid/os/RemoteException; {:try_start_18 .. :try_end_18} :catch_9

    goto/32 :goto_11f

    nop

    :goto_e8
    move/from16 v7, v17

    goto/32 :goto_128

    nop

    :goto_e9
    move v11, v12

    goto/32 :goto_17b

    nop

    :goto_ea
    goto/16 :goto_93

    :goto_eb
    :try_start_19
    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    iget-object v0, v0, Landroid/view/WindowManager$LayoutParams;->surfaceInsets:Landroid/graphics/Rect;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/graphics/Rect;->set(IIII)V

    :goto_ec
    const/4 v0, 0x0

    sget-boolean v2, Landroid/view/ViewRootImpl;->LOCAL_LAYOUT:Z
    :try_end_19
    .catch Landroid/os/RemoteException; {:try_start_19 .. :try_end_19} :catch_c

    goto/32 :goto_f6

    nop

    :goto_ed
    move/from16 v4, p3

    goto/32 :goto_7e

    nop

    :goto_ee
    goto/16 :goto_2c

    :catch_a
    move-exception v0

    goto/32 :goto_3b

    nop

    :goto_ef
    goto/16 :goto_141

    :catch_b
    move-exception v0

    goto/32 :goto_185

    nop

    :goto_f0
    move-object v11, v10

    goto/32 :goto_112

    nop

    :goto_f1
    move/from16 v21, v4

    goto/32 :goto_c1

    nop

    :goto_f2
    move v0, v6

    :goto_f3
    goto/32 :goto_181

    nop

    :goto_f4
    goto/16 :goto_13

    :goto_f5
    goto/32 :goto_157

    nop

    :goto_f6
    if-nez v2, :cond_23

    goto/32 :goto_117

    :cond_23
    :try_start_1a
    iget-object v2, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v2}, Landroid/view/SurfaceControl;->isValid()Z

    move-result v2

    if-nez v2, :cond_24

    iget-object v2, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSession:Landroid/view/IWindowSession;

    iget-object v6, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWindow:Lcom/android/internal/view/BaseIWindow;

    iget-object v9, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    const/16 v27, 0x0

    iget-object v10, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mMergedConfiguration:Landroid/util/MergedConfiguration;

    iget-object v11, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceControl:Landroid/view/SurfaceControl;

    iget-object v13, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mInsetsState:Landroid/view/InsetsState;

    iget-object v14, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mTempControls:[Landroid/view/InsetsSourceControl;

    move-object/from16 v24, v2

    move-object/from16 v25, v6

    move-object/from16 v26, v9

    move-object/from16 v28, v10

    move-object/from16 v29, v11

    move-object/from16 v30, v13

    move-object/from16 v31, v14

    invoke-interface/range {v24 .. v31}, Landroid/view/IWindowSession;->updateVisibility(Landroid/view/IWindow;Landroid/view/WindowManager$LayoutParams;ILandroid/util/MergedConfiguration;Landroid/view/SurfaceControl;Landroid/view/InsetsState;[Landroid/view/InsetsSourceControl;)I

    move-result v2

    move v0, v2

    :cond_24
    iget-object v2, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mTempRect:Landroid/graphics/Rect;

    iget-object v6, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mInsetsState:Landroid/view/InsetsState;

    invoke-virtual {v6, v2}, Landroid/view/InsetsState;->getDisplayCutoutSafe(Landroid/graphics/Rect;)V

    iget-object v6, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowLayout:Landroid/view/WindowLayout;

    iget-object v9, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    iget-object v10, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mInsetsState:Landroid/view/InsetsState;

    invoke-virtual/range {v16 .. v16}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;

    move-result-object v41

    invoke-virtual/range {v16 .. v16}, Landroid/app/WindowConfiguration;->getWindowingMode()I

    move-result v42

    iget v11, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWidth:I

    iget v13, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mHeight:I

    iget-object v14, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mRequestedVisibilities:Landroid/view/InsetsVisibilities;

    const/16 v46, 0x0

    const/high16 v47, 0x3f800000    # 1.0f

    move/from16 v17, v0

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWinFrames:Landroid/window/ClientWindowFrames;

    move-object/from16 v37, v6

    move-object/from16 v38, v9

    move-object/from16 v39, v10

    move-object/from16 v40, v2

    move/from16 v43, v11

    move/from16 v44, v13

    move-object/from16 v45, v14

    move-object/from16 v48, v0

    invoke-virtual/range {v37 .. v48}, Landroid/view/WindowLayout;->computeFrames(Landroid/view/WindowManager$LayoutParams;Landroid/view/InsetsState;Landroid/graphics/Rect;Landroid/graphics/Rect;IIILandroid/view/InsetsVisibilities;Landroid/graphics/Rect;FLandroid/window/ClientWindowFrames;)V

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSession:Landroid/view/IWindowSession;

    iget-object v6, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWindow:Lcom/android/internal/view/BaseIWindow;

    iget-object v9, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    const/16 v27, 0x0

    iget-object v10, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWinFrames:Landroid/window/ClientWindowFrames;

    iget v11, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWidth:I

    iget v13, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mHeight:I

    move-object/from16 v24, v0

    move-object/from16 v25, v6

    move-object/from16 v26, v9

    move-object/from16 v28, v10

    move/from16 v29, v11

    move/from16 v30, v13

    invoke-interface/range {v24 .. v30}, Landroid/view/IWindowSession;->updateLayout(Landroid/view/IWindow;Landroid/view/WindowManager$LayoutParams;ILandroid/window/ClientWindowFrames;II)V
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_1a .. :try_end_1a} :catch_1

    goto/32 :goto_12b

    nop

    :goto_f7
    move/from16 v4, p3

    goto/32 :goto_118

    nop

    :goto_f8
    move/from16 v33, v11

    goto/32 :goto_151

    nop

    :goto_f9
    move/from16 v27, v5

    goto/32 :goto_39

    nop

    :goto_fa
    move/from16 v24, v7

    goto/32 :goto_123

    nop

    :goto_fb
    move/from16 v36, v9

    :try_start_1b
    iget-object v9, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mTempControls:[Landroid/view/InsetsSourceControl;

    move-object/from16 v24, v10

    move-object/from16 v25, v0

    move-object/from16 v26, v11

    move-object/from16 v29, v13

    move-object/from16 v30, v6

    move-object/from16 v31, v14

    move-object/from16 v32, v9

    invoke-interface/range {v24 .. v32}, Landroid/view/IWindowSession;->addToDisplay(Landroid/view/IWindow;Landroid/view/WindowManager$LayoutParams;IILandroid/view/InsetsVisibilities;Landroid/view/InputChannel;Landroid/view/InsetsState;[Landroid/view/InsetsSourceControl;)I

    move-result v0

    if-gez v0, :cond_25

    const-string v0, "Failed to add window while updating wallpaper surface."

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_25
    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSession:Landroid/view/IWindowSession;

    iget-object v2, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWindow:Lcom/android/internal/view/BaseIWindow;

    invoke-virtual/range {p0 .. p0}, Landroid/service/wallpaper/WallpaperService$Engine;->shouldZoomOutWallpaper()Z

    move-result v9

    invoke-interface {v0, v2, v9}, Landroid/view/IWindowSession;->setShouldZoomOutWallpaper(Landroid/os/IBinder;Z)V

    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mCreated:Z

    new-instance v0, Landroid/service/wallpaper/WallpaperService$Engine$WallpaperInputEventReceiver;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, Landroid/service/wallpaper/WallpaperService$Engine$WallpaperInputEventReceiver;-><init>(Landroid/service/wallpaper/WallpaperService$Engine;Landroid/view/InputChannel;Landroid/os/Looper;)V

    iput-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mInputEventReceiver:Landroid/service/wallpaper/WallpaperService$Engine$WallpaperInputEventReceiver;
    :try_end_1b
    .catch Landroid/os/RemoteException; {:try_start_1b .. :try_end_1b} :catch_1

    goto/32 :goto_ef

    nop

    :goto_fc
    move v0, v10

    goto/32 :goto_104

    nop

    :goto_fd
    const/4 v0, 0x0

    goto/32 :goto_195

    nop

    :goto_fe
    move/from16 v33, v11

    goto/32 :goto_28

    nop

    :goto_ff
    move-object v0, v8

    :goto_100
    goto/32 :goto_3a

    nop

    :goto_101
    goto/16 :goto_24

    :goto_102
    goto/32 :goto_1c

    nop

    :goto_103
    iget-boolean v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mDestroyed:Z

    goto/32 :goto_153

    nop

    :goto_104
    goto/16 :goto_1f

    :goto_105
    goto/32 :goto_1e

    nop

    :goto_106
    move/from16 v4, p3

    goto/32 :goto_23

    nop

    :goto_107
    move/from16 v4, p3

    goto/32 :goto_2e

    nop

    :goto_108
    move/from16 v4, p3

    goto/32 :goto_b

    nop

    :goto_109
    if-eqz v8, :cond_26

    goto/32 :goto_24

    :cond_26
    goto/32 :goto_e3

    nop

    :goto_10a
    move v8, v0

    goto/32 :goto_110

    nop

    :goto_10b
    move/from16 v34, v13

    goto/32 :goto_176

    nop

    :goto_10c
    move v11, v12

    goto/32 :goto_21

    nop

    :goto_10d
    iget v14, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPrivateFlags:I

    goto/32 :goto_1a1

    nop

    :goto_10e
    move/from16 v25, v8

    goto/32 :goto_18c

    nop

    :goto_10f
    move/from16 v30, v7

    :try_start_1c
    iget v7, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWidth:I
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_4

    goto/32 :goto_73

    nop

    :goto_110
    iget v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mFormat:I

    goto/32 :goto_41

    nop

    :goto_111
    if-ne v0, v9, :cond_27

    goto/32 :goto_a7

    :cond_27
    goto/32 :goto_99

    nop

    :goto_112
    move v10, v7

    goto/32 :goto_9e

    nop

    :goto_113
    move/from16 v27, v5

    goto/32 :goto_172

    nop

    :goto_114
    move/from16 v21, v4

    goto/32 :goto_35

    nop

    :goto_115
    move/from16 v4, p3

    goto/32 :goto_56

    nop

    :goto_116
    goto/16 :goto_7b

    :goto_117
    :try_start_1d
    iget-object v2, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSession:Landroid/view/IWindowSession;

    iget-object v6, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWindow:Lcom/android/internal/view/BaseIWindow;

    iget-object v9, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    iget v10, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWidth:I

    iget v11, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mHeight:I

    const/16 v42, 0x0

    const/16 v43, 0x0

    iget-object v13, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWinFrames:Landroid/window/ClientWindowFrames;

    iget-object v14, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mMergedConfiguration:Landroid/util/MergedConfiguration;

    move/from16 v17, v0

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceControl:Landroid/view/SurfaceControl;
    :try_end_1d
    .catch Landroid/os/RemoteException; {:try_start_1d .. :try_end_1d} :catch_c

    goto/32 :goto_155

    nop

    :goto_118
    move/from16 v28, v2

    goto/32 :goto_14b

    nop

    :goto_119
    move/from16 v25, v8

    goto/32 :goto_135

    nop

    :goto_11a
    iget v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mHeight:I

    goto/32 :goto_82

    nop

    :goto_11b
    or-int v19, v26, v0

    :try_start_1e
    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mDispatchedStableInsets:Landroid/graphics/Rect;

    invoke-virtual {v0, v14}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_28

    const/4 v0, 0x1

    goto :goto_11c

    :cond_28
    const/4 v0, 0x0

    :goto_11c
    or-int v19, v19, v0

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mDispatchedDisplayCutout:Landroid/view/DisplayCutout;
    :try_end_1e
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_1e} :catch_5

    goto/32 :goto_169

    nop

    :goto_11d
    goto/16 :goto_93

    :catch_c
    move-exception v0

    goto/32 :goto_156

    nop

    :goto_11e
    move/from16 v20, v3

    goto/32 :goto_30

    nop

    :goto_11f
    if-nez v0, :cond_29

    goto/32 :goto_b7

    :cond_29
    :try_start_1f
    invoke-virtual {v11}, Landroid/view/WindowInsets;->getDisplayCutout()Landroid/view/DisplayCutout;

    move-result-object v0
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_1f .. :try_end_1f} :catch_6

    goto/32 :goto_b6

    nop

    :goto_120
    move/from16 v24, v7

    goto/32 :goto_119

    nop

    :goto_121
    move/from16 v25, v8

    goto/32 :goto_174

    nop

    :goto_122
    move/from16 v4, p3

    goto/32 :goto_19d

    nop

    :goto_123
    move/from16 v25, v8

    goto/32 :goto_2d

    nop

    :goto_124
    if-eqz v0, :cond_2a

    goto/32 :goto_17

    :cond_2a
    goto/32 :goto_d2

    nop

    :goto_125
    move v0, v10

    goto/32 :goto_b8

    nop

    :goto_126
    if-eqz v24, :cond_2b

    goto/32 :goto_61

    :cond_2b
    goto/32 :goto_b4

    nop

    :goto_127
    move/from16 v21, v4

    goto/32 :goto_19f

    nop

    :goto_128
    goto/16 :goto_9f

    :goto_129
    goto/32 :goto_65

    nop

    :goto_12a
    if-eqz v7, :cond_2c

    goto/32 :goto_24

    :cond_2c
    goto/32 :goto_109

    nop

    :goto_12b
    move/from16 v24, v7

    goto/32 :goto_79

    nop

    :goto_12c
    move v0, v6

    goto/32 :goto_5b

    nop

    :goto_12d
    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    goto/32 :goto_67

    nop

    :goto_12e
    move/from16 v24, v7

    goto/32 :goto_188

    nop

    :goto_12f
    if-eqz p3, :cond_2d

    goto/32 :goto_24

    :cond_2d
    goto/32 :goto_12d

    nop

    :goto_130
    move v0, v10

    :goto_131
    goto/32 :goto_182

    nop

    :goto_132
    move/from16 v36, v9

    goto/32 :goto_fe

    nop

    :goto_133
    move/from16 v33, v11

    goto/32 :goto_17d

    nop

    :goto_134
    const-string v0, "Ignoring updateSurface due to destroyed"

    goto/32 :goto_145

    nop

    :goto_135
    move/from16 v11, v33

    goto/32 :goto_dd

    nop

    :goto_136
    goto/16 :goto_105

    :goto_137
    goto/32 :goto_fc

    nop

    :goto_138
    move/from16 v4, p3

    goto/32 :goto_2b

    nop

    :goto_139
    goto/16 :goto_74

    :goto_13a
    goto/32 :goto_ba

    nop

    :goto_13b
    const/4 v12, 0x1

    :try_start_20
    iput v7, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWidth:I
    :try_end_20
    .catch Landroid/os/RemoteException; {:try_start_20 .. :try_end_20} :catch_6

    goto/32 :goto_ae

    nop

    :goto_13c
    move/from16 v11, v33

    goto/32 :goto_17e

    nop

    :goto_13d
    move/from16 v4, p3

    goto/32 :goto_18e

    nop

    :goto_13e
    move/from16 v4, v29

    goto/32 :goto_a2

    nop

    :goto_13f
    if-eq v0, v3, :cond_2e

    goto/32 :goto_105

    :cond_2e
    goto/32 :goto_11a

    nop

    :goto_140
    move/from16 v35, v14

    :goto_141
    :try_start_21
    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    iget-object v0, v0, Lcom/android/internal/view/BaseSurfaceHolder;->mSurfaceLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mDrawingAllowed:Z
    :try_end_21
    .catch Landroid/os/RemoteException; {:try_start_21 .. :try_end_21} :catch_c

    goto/32 :goto_1d

    nop

    :goto_142
    if-nez v25, :cond_2f

    goto/32 :goto_187

    :cond_2f
    goto/32 :goto_d4

    nop

    :goto_143
    iget-boolean v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mCreated:Z

    goto/32 :goto_193

    nop

    :goto_144
    const/4 v0, 0x0

    goto/32 :goto_60

    nop

    :goto_145
    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_d8

    nop

    :goto_146
    const/4 v0, 0x1

    :goto_147
    goto/32 :goto_16a

    nop

    :goto_148
    move/from16 v4, p3

    goto/32 :goto_194

    nop

    :goto_149
    move/from16 v21, v4

    goto/32 :goto_14a

    nop

    :goto_14a
    move/from16 v27, v5

    goto/32 :goto_12e

    nop

    :goto_14b
    move/from16 v29, v6

    goto/32 :goto_22

    nop

    :goto_14c
    move/from16 v26, v12

    goto/32 :goto_13d

    nop

    :goto_14d
    move/from16 v4, p3

    goto/32 :goto_ea

    nop

    :goto_14e
    move/from16 v29, v6

    goto/32 :goto_171

    nop

    :goto_14f
    move/from16 v4, p3

    goto/32 :goto_87

    nop

    :goto_150
    move/from16 v27, v5

    goto/32 :goto_19

    nop

    :goto_151
    move/from16 v34, v13

    goto/32 :goto_140

    nop

    :goto_152
    goto/16 :goto_93

    :catch_d
    move-exception v0

    goto/32 :goto_e5

    nop

    :goto_153
    const-string v2, "WallpaperService"

    goto/32 :goto_de

    nop

    :goto_154
    goto/16 :goto_165

    :catch_e
    move-exception v0

    goto/32 :goto_37

    nop

    :goto_155
    move/from16 v24, v7

    :try_start_22
    iget-object v7, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mInsetsState:Landroid/view/InsetsState;
    :try_end_22
    .catch Landroid/os/RemoteException; {:try_start_22 .. :try_end_22} :catch_3

    goto/32 :goto_11

    nop

    :goto_156
    move/from16 v20, v3

    goto/32 :goto_17f

    nop

    :goto_157
    move-object/from16 v26, v0

    goto/32 :goto_4f

    nop

    :goto_158
    const/4 v0, 0x1

    goto/32 :goto_3c

    nop

    :goto_159
    move v12, v0

    goto/32 :goto_19a

    nop

    :goto_15a
    const/4 v10, 0x0

    goto/32 :goto_111

    nop

    :goto_15b
    move/from16 v20, v3

    goto/32 :goto_5f

    nop

    :goto_15c
    const/4 v4, -0x1

    goto/32 :goto_2a

    nop

    :goto_15d
    const/4 v0, 0x0

    :goto_15e
    goto/32 :goto_78

    nop

    :goto_15f
    move/from16 v20, v3

    goto/32 :goto_16d

    nop

    :goto_160
    move/from16 v27, v5

    goto/32 :goto_51

    nop

    :goto_161
    if-lez v4, :cond_30

    goto/32 :goto_15

    :cond_30
    goto/32 :goto_15c

    nop

    :goto_162
    move v11, v0

    goto/32 :goto_143

    nop

    :goto_163
    goto/16 :goto_93

    :goto_164
    nop

    :try_start_23
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    int-to-float v6, v3

    div-float/2addr v0, v6

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Rect;->height()I

    move-result v6

    int-to-float v6, v6

    int-to-float v10, v5

    div-float/2addr v6, v10

    invoke-static {v0, v6}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iget-object v6, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    int-to-float v10, v3

    mul-float/2addr v10, v0

    const/high16 v17, 0x3f000000    # 0.5f

    add-float v10, v10, v17

    float-to-int v10, v10

    iput v10, v6, Landroid/view/WindowManager$LayoutParams;->width:I

    iget-object v6, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    int-to-float v10, v5

    mul-float/2addr v10, v0

    add-float v10, v10, v17

    float-to-int v10, v10

    iput v10, v6, Landroid/view/WindowManager$LayoutParams;->height:I

    iget-object v6, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    iget v10, v6, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v10, v10, 0x4000

    iput v10, v6, Landroid/view/WindowManager$LayoutParams;->flags:I

    :goto_165
    iget v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowPrivateFlags:I

    iput v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mCurWindowPrivateFlags:I

    iget-object v6, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    iput v0, v6, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    iget v6, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mType:I

    iput v6, v0, Landroid/view/WindowManager$LayoutParams;->memoryType:I

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    iget-object v6, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWindowToken:Landroid/os/IBinder;

    iput-object v6, v0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    iget-boolean v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mCreated:Z
    :try_end_23
    .catch Landroid/os/RemoteException; {:try_start_23 .. :try_end_23} :catch_d

    goto/32 :goto_17c

    nop

    :goto_166
    move/from16 v20, v3

    goto/32 :goto_6

    nop

    :goto_167
    if-nez v0, :cond_31

    goto/32 :goto_2c

    :cond_31
    :try_start_24
    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mBbqSurfaceControl:Landroid/view/SurfaceControl;

    if-nez v0, :cond_32

    new-instance v0, Landroid/view/SurfaceControl$Builder;

    invoke-direct {v0}, Landroid/view/SurfaceControl$Builder;-><init>()V

    const-string v7, "Wallpaper BBQ wrapper"

    invoke-virtual {v0, v7}, Landroid/view/SurfaceControl$Builder;->setName(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;

    move-result-object v0

    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Landroid/view/SurfaceControl$Builder;->setHidden(Z)Landroid/view/SurfaceControl$Builder;

    move-result-object v0

    const/4 v7, 0x2

    const/16 v8, 0x7dd

    invoke-virtual {v0, v7, v8}, Landroid/view/SurfaceControl$Builder;->setMetadata(II)Landroid/view/SurfaceControl$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/SurfaceControl$Builder;->setBLASTLayer()Landroid/view/SurfaceControl$Builder;

    move-result-object v0

    iget-object v7, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v0, v7}, Landroid/view/SurfaceControl$Builder;->setParent(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Builder;

    move-result-object v0

    const-string v7, "Wallpaper#relayout"

    invoke-virtual {v0, v7}, Landroid/view/SurfaceControl$Builder;->setCallsite(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/SurfaceControl$Builder;->build()Landroid/view/SurfaceControl;

    move-result-object v0

    iput-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mBbqSurfaceControl:Landroid/view/SurfaceControl;

    :cond_32
    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mBbqSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {v0, v6}, Landroid/view/SurfaceControl;->setTransformHint(I)V

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceSize:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v7, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceSize:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->y:I

    iget v8, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mFormat:I

    invoke-direct {v1, v0, v7, v8}, Landroid/service/wallpaper/WallpaperService$Engine;->getOrCreateBLASTSurface(III)Landroid/view/Surface;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v7, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    iget-object v7, v7, Lcom/android/internal/view/BaseSurfaceHolder;->mSurface:Landroid/view/Surface;

    invoke-virtual {v7, v0}, Landroid/view/Surface;->transferFrom(Landroid/view/Surface;)V
    :try_end_24
    .catch Landroid/os/RemoteException; {:try_start_24 .. :try_end_24} :catch_a

    goto/32 :goto_ee

    nop

    :goto_168
    move v7, v5

    goto/32 :goto_f0

    nop

    :goto_169
    move/from16 v20, v3

    goto/32 :goto_a9

    nop

    :goto_16a
    or-int v4, p3, v0

    goto/32 :goto_40

    nop

    :goto_16b
    move/from16 v34, v13

    goto/32 :goto_e1

    nop

    :goto_16c
    move/from16 v27, v5

    goto/32 :goto_77

    nop

    :goto_16d
    move/from16 v21, v4

    goto/32 :goto_e

    nop

    :goto_16e
    move/from16 v35, v14

    goto/32 :goto_106

    nop

    :goto_16f
    move/from16 v11, v33

    goto/32 :goto_138

    nop

    :goto_170
    move/from16 v36, v9

    goto/32 :goto_29

    nop

    :goto_171
    move/from16 v30, v7

    goto/32 :goto_189

    nop

    :goto_172
    const/4 v5, 0x0

    :goto_173
    goto/32 :goto_c9

    nop

    :goto_174
    move/from16 v36, v9

    goto/32 :goto_cc

    nop

    :goto_175
    move/from16 v34, v13

    goto/32 :goto_16e

    nop

    :goto_176
    move/from16 v35, v14

    goto/32 :goto_e4

    nop

    :goto_177
    move/from16 v12, v33

    :goto_178
    :try_start_25
    iget v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mCurHeight:I
    :try_end_25
    .catch Landroid/os/RemoteException; {:try_start_25 .. :try_end_25} :catch_9

    goto/32 :goto_179

    nop

    :goto_179
    if-ne v0, v10, :cond_33

    goto/32 :goto_e7

    :cond_33
    goto/32 :goto_e6

    nop

    :goto_17a
    move/from16 v21, v4

    goto/32 :goto_70

    nop

    :goto_17b
    move/from16 v12, v26

    goto/32 :goto_91

    nop

    :goto_17c
    if-eqz v0, :cond_34

    goto/32 :goto_5

    :cond_34
    :try_start_26
    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->this$0:Landroid/service/wallpaper/WallpaperService;

    sget-object v6, Lcom/android/internal/R$styleable;->Window:[I

    invoke-virtual {v0, v6}, Landroid/service/wallpaper/WallpaperService;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    iget-object v6, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    iget-object v10, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mIWallpaperEngine:Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;

    iget v10, v10, Landroid/service/wallpaper/WallpaperService$IWallpaperEngineWrapper;->mWindowType:I

    iput v10, v6, Landroid/view/WindowManager$LayoutParams;->type:I

    iget-object v6, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    const v10, 0x800033

    iput v10, v6, Landroid/view/WindowManager$LayoutParams;->gravity:I

    iget-object v6, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    const/4 v10, 0x0

    invoke-virtual {v6, v10}, Landroid/view/WindowManager$LayoutParams;->setFitInsetsTypes(I)V

    iget-object v6, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    iget-object v10, v1, Landroid/service/wallpaper/WallpaperService$Engine;->this$0:Landroid/service/wallpaper/WallpaperService;

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v6, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    const v10, 0x1030310

    iput v10, v6, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    new-instance v6, Landroid/view/InputChannel;

    invoke-direct {v6}, Landroid/view/InputChannel;-><init>()V

    iget-object v10, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSession:Landroid/view/IWindowSession;

    move-object/from16 v17, v0

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mWindow:Lcom/android/internal/view/BaseIWindow;
    :try_end_26
    .catch Landroid/os/RemoteException; {:try_start_26 .. :try_end_26} :catch_12

    goto/32 :goto_5e

    nop

    :goto_17d
    move/from16 v34, v13

    goto/32 :goto_3f

    nop

    :goto_17e
    goto/16 :goto_93

    :catch_f
    move-exception v0

    goto/32 :goto_15f

    nop

    :goto_17f
    move/from16 v21, v4

    goto/32 :goto_f9

    nop

    :goto_180
    move/from16 v4, p3

    goto/32 :goto_18f

    nop

    :goto_181
    move v14, v0

    goto/32 :goto_5d

    nop

    :goto_182
    move v13, v0

    goto/32 :goto_bd

    nop

    :goto_183
    move/from16 v26, v12

    goto/32 :goto_6c

    nop

    :goto_184
    move/from16 v20, v3

    goto/32 :goto_149

    nop

    :goto_185
    move/from16 v36, v9

    goto/32 :goto_e2

    nop

    :goto_186
    goto/16 :goto_13

    :goto_187
    goto/32 :goto_57

    nop

    :goto_188
    move/from16 v25, v8

    goto/32 :goto_81

    nop

    :goto_189
    move-object/from16 v31, v8

    goto/32 :goto_f4

    nop

    :goto_18a
    move v5, v4

    goto/32 :goto_4a

    nop

    :goto_18b
    iget-object v4, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    goto/32 :goto_18

    nop

    :goto_18c
    move/from16 v11, v33

    goto/32 :goto_14d

    nop

    :goto_18d
    move-object v11, v10

    goto/32 :goto_8f

    nop

    :goto_18e
    move/from16 v11, v33

    goto/32 :goto_11d

    nop

    :goto_18f
    move v11, v12

    goto/32 :goto_89

    nop

    :goto_190
    goto/16 :goto_61

    :goto_191
    goto/32 :goto_144

    nop

    :goto_192
    if-eqz v0, :cond_35

    goto/32 :goto_80

    :cond_35
    :try_start_27
    invoke-virtual/range {p0 .. p0}, Landroid/service/wallpaper/WallpaperService$Engine;->reportSurfaceDestroyed()V
    :try_end_27
    .catch Landroid/os/RemoteException; {:try_start_27 .. :try_end_27} :catch_2

    goto/32 :goto_54

    nop

    :goto_193
    xor-int/2addr v0, v6

    goto/32 :goto_159

    nop

    :goto_194
    goto/16 :goto_93

    :catch_10
    move-exception v0

    goto/32 :goto_170

    nop

    :goto_195
    iget-object v3, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mSurfaceHolder:Lcom/android/internal/view/BaseSurfaceHolder;

    goto/32 :goto_c4

    nop

    :goto_196
    move-object/from16 v31, v8

    goto/32 :goto_139

    nop

    :goto_197
    move/from16 v36, v9

    goto/32 :goto_133

    nop

    :goto_198
    const/4 v0, 0x1

    goto/32 :goto_18a

    nop

    :goto_199
    goto/16 :goto_93

    :catch_11
    move-exception v0

    goto/32 :goto_68

    nop

    :goto_19a
    iget v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mType:I

    goto/32 :goto_a0

    nop

    :goto_19b
    goto/16 :goto_93

    :catch_12
    move-exception v0

    goto/32 :goto_197

    nop

    :goto_19c
    move/from16 v25, v8

    goto/32 :goto_2f

    nop

    :goto_19d
    move/from16 v11, v33

    goto/32 :goto_66

    nop

    :goto_19e
    if-eq v5, v0, :cond_36

    goto/32 :goto_164

    :cond_36
    :try_start_28
    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    iget-object v0, v1, Landroid/service/wallpaper/WallpaperService$Engine;->mLayout:Landroid/view/WindowManager$LayoutParams;

    iget v6, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v6, v6, -0x4001

    iput v6, v0, Landroid/view/WindowManager$LayoutParams;->flags:I
    :try_end_28
    .catch Landroid/os/RemoteException; {:try_start_28 .. :try_end_28} :catch_e

    goto/32 :goto_154

    nop

    :goto_19f
    move/from16 v27, v5

    goto/32 :goto_ed

    nop

    :goto_1a0
    const/4 v6, 0x1

    goto/32 :goto_94

    nop

    :goto_1a1
    if-ne v0, v14, :cond_37

    goto/32 :goto_27

    :cond_37
    goto/32 :goto_26

    nop
.end method
