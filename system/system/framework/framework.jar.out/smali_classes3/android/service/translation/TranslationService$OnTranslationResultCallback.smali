.class public interface abstract Landroid/service/translation/TranslationService$OnTranslationResultCallback;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/service/translation/TranslationService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnTranslationResultCallback"
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# virtual methods
.method public abstract onError()V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract onTranslationSuccess(Landroid/view/translation/TranslationResponse;)V
.end method
