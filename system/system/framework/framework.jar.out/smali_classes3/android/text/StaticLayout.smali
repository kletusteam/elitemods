.class public Landroid/text/StaticLayout;
.super Landroid/text/Layout;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/text/StaticLayout$LineBreaks;,
        Landroid/text/StaticLayout$Builder;
    }
.end annotation


# static fields
.field private static final CHAR_NEW_LINE:C = '\n'

.field private static final COLUMNS_ELLIPSIZE:I = 0x7

.field private static final COLUMNS_NORMAL:I = 0x5

.field private static final DEFAULT_MAX_LINE_HEIGHT:I = -0x1

.field private static final DESCENT:I = 0x2

.field private static final DIR:I = 0x0

.field private static final DIR_SHIFT:I = 0x1e

.field private static final ELLIPSIS_COUNT:I = 0x6

.field private static final ELLIPSIS_START:I = 0x5

.field private static final END_HYPHEN_MASK:I = 0x7

.field private static final EXTRA:I = 0x3

.field private static final EXTRA_ROUNDING:D = 0.5

.field private static final HYPHEN:I = 0x4

.field private static final HYPHEN_MASK:I = 0xff

.field private static final START:I = 0x0

.field private static final START_HYPHEN_BITS_SHIFT:I = 0x3

.field private static final START_HYPHEN_MASK:I = 0x18

.field private static final START_MASK:I = 0x1fffffff

.field private static final TAB:I = 0x0

.field private static final TAB_INCREMENT:F = 20.0f

.field private static final TAB_MASK:I = 0x20000000

.field static final TAG:Ljava/lang/String; = "StaticLayout"

.field private static final TOP:I = 0x1


# instance fields
.field private mBottomPadding:I

.field private mColumns:I

.field private mEllipsized:Z

.field private mEllipsizedWidth:I

.field private mFallbackLineSpacing:Z

.field private mLeftIndents:[I

.field private mLineCount:I

.field private mLineDirections:[Landroid/text/Layout$Directions;

.field private mLines:[I

.field private mMaxLineHeight:I

.field private mMaximumVisibleLineCount:I

.field private mRightIndents:[I

.field private mTopPadding:I


# direct methods
.method private constructor <init>(Landroid/text/StaticLayout$Builder;)V
    .locals 10

    invoke-static {p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmEllipsize(Landroid/text/StaticLayout$Builder;)Landroid/text/TextUtils$TruncateAt;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmText(Landroid/text/StaticLayout$Builder;)Ljava/lang/CharSequence;

    move-result-object v0

    move-object v3, v0

    goto :goto_0

    :cond_0
    invoke-static {p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmText(Landroid/text/StaticLayout$Builder;)Ljava/lang/CharSequence;

    move-result-object v0

    instance-of v0, v0, Landroid/text/Spanned;

    if-eqz v0, :cond_1

    new-instance v0, Landroid/text/Layout$SpannedEllipsizer;

    invoke-static {p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmText(Landroid/text/StaticLayout$Builder;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/Layout$SpannedEllipsizer;-><init>(Ljava/lang/CharSequence;)V

    move-object v3, v0

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/text/Layout$Ellipsizer;

    invoke-static {p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmText(Landroid/text/StaticLayout$Builder;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/Layout$Ellipsizer;-><init>(Ljava/lang/CharSequence;)V

    move-object v3, v0

    :goto_0
    invoke-static {p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmPaint(Landroid/text/StaticLayout$Builder;)Landroid/text/TextPaint;

    move-result-object v4

    invoke-static {p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmWidth(Landroid/text/StaticLayout$Builder;)I

    move-result v5

    invoke-static {p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmAlignment(Landroid/text/StaticLayout$Builder;)Landroid/text/Layout$Alignment;

    move-result-object v6

    invoke-static {p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmTextDir(Landroid/text/StaticLayout$Builder;)Landroid/text/TextDirectionHeuristic;

    move-result-object v7

    invoke-static {p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmSpacingMult(Landroid/text/StaticLayout$Builder;)F

    move-result v8

    invoke-static {p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmSpacingAdd(Landroid/text/StaticLayout$Builder;)F

    move-result v9

    move-object v2, p0

    invoke-direct/range {v2 .. v9}, Landroid/text/Layout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/TextDirectionHeuristic;FF)V

    const/4 v0, -0x1

    iput v0, p0, Landroid/text/StaticLayout;->mMaxLineHeight:I

    const v0, 0x7fffffff

    iput v0, p0, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    invoke-static {p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmEllipsize(Landroid/text/StaticLayout$Builder;)Landroid/text/TextUtils$TruncateAt;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/text/StaticLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Layout$Ellipsizer;

    iput-object p0, v0, Landroid/text/Layout$Ellipsizer;->mLayout:Landroid/text/Layout;

    invoke-static {p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmEllipsizedWidth(Landroid/text/StaticLayout$Builder;)I

    move-result v1

    iput v1, v0, Landroid/text/Layout$Ellipsizer;->mWidth:I

    invoke-static {p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmEllipsize(Landroid/text/StaticLayout$Builder;)Landroid/text/TextUtils$TruncateAt;

    move-result-object v1

    iput-object v1, v0, Landroid/text/Layout$Ellipsizer;->mMethod:Landroid/text/TextUtils$TruncateAt;

    invoke-static {p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmEllipsizedWidth(Landroid/text/StaticLayout$Builder;)I

    move-result v1

    iput v1, p0, Landroid/text/StaticLayout;->mEllipsizedWidth:I

    const/4 v1, 0x7

    iput v1, p0, Landroid/text/StaticLayout;->mColumns:I

    goto :goto_1

    :cond_2
    const/4 v0, 0x5

    iput v0, p0, Landroid/text/StaticLayout;->mColumns:I

    invoke-static {p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmWidth(Landroid/text/StaticLayout$Builder;)I

    move-result v0

    iput v0, p0, Landroid/text/StaticLayout;->mEllipsizedWidth:I

    :goto_1
    const-class v0, Landroid/text/Layout$Directions;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/internal/util/ArrayUtils;->newUnpaddedArray(Ljava/lang/Class;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/Layout$Directions;

    iput-object v0, p0, Landroid/text/StaticLayout;->mLineDirections:[Landroid/text/Layout$Directions;

    iget v0, p0, Landroid/text/StaticLayout;->mColumns:I

    mul-int/2addr v0, v1

    invoke-static {v0}, Lcom/android/internal/util/ArrayUtils;->newUnpaddedIntArray(I)[I

    move-result-object v0

    iput-object v0, p0, Landroid/text/StaticLayout;->mLines:[I

    invoke-static {p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmMaxLines(Landroid/text/StaticLayout$Builder;)I

    move-result v0

    iput v0, p0, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    invoke-static {p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmLeftIndents(Landroid/text/StaticLayout$Builder;)[I

    move-result-object v0

    iput-object v0, p0, Landroid/text/StaticLayout;->mLeftIndents:[I

    invoke-static {p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmRightIndents(Landroid/text/StaticLayout$Builder;)[I

    move-result-object v0

    iput-object v0, p0, Landroid/text/StaticLayout;->mRightIndents:[I

    invoke-static {p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmJustificationMode(Landroid/text/StaticLayout$Builder;)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/text/StaticLayout;->setJustificationMode(I)V

    invoke-static {p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmIncludePad(Landroid/text/StaticLayout$Builder;)Z

    move-result v0

    invoke-static {p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmIncludePad(Landroid/text/StaticLayout$Builder;)Z

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, Landroid/text/StaticLayout;->generate(Landroid/text/StaticLayout$Builder;ZZ)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/text/StaticLayout$Builder;Landroid/text/StaticLayout-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/text/StaticLayout;-><init>(Landroid/text/StaticLayout$Builder;)V

    return-void
.end method

.method constructor <init>(Ljava/lang/CharSequence;)V
    .locals 7

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Landroid/text/Layout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FF)V

    const/4 v0, -0x1

    iput v0, p0, Landroid/text/StaticLayout;->mMaxLineHeight:I

    const v0, 0x7fffffff

    iput v0, p0, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    const/4 v0, 0x7

    iput v0, p0, Landroid/text/StaticLayout;->mColumns:I

    const-class v0, Landroid/text/Layout$Directions;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/internal/util/ArrayUtils;->newUnpaddedArray(Ljava/lang/Class;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/Layout$Directions;

    iput-object v0, p0, Landroid/text/StaticLayout;->mLineDirections:[Landroid/text/Layout$Directions;

    iget v0, p0, Landroid/text/StaticLayout;->mColumns:I

    mul-int/2addr v0, v1

    invoke-static {v0}, Lcom/android/internal/util/ArrayUtils;->newUnpaddedIntArray(I)[I

    move-result-object v0

    iput-object v0, p0, Landroid/text/StaticLayout;->mLines:[I

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V
    .locals 12
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    invoke-direct/range {v0 .. v11}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V
    .locals 14
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    sget-object v7, Landroid/text/TextDirectionHeuristics;->FIRSTSTRONG_LTR:Landroid/text/TextDirectionHeuristic;

    const v13, 0x7fffffff

    move-object v0, p0

    move-object v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move-object/from16 v11, p10

    move/from16 v12, p11

    invoke-direct/range {v0 .. v13}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/TextDirectionHeuristic;FFZLandroid/text/TextUtils$TruncateAt;II)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/TextDirectionHeuristic;FFZLandroid/text/TextUtils$TruncateAt;II)V
    .locals 14
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    move-object v8, p0

    move-object v9, p1

    move-object/from16 v10, p11

    move/from16 v11, p12

    move/from16 v12, p13

    if-nez v10, :cond_0

    move-object v1, v9

    goto :goto_0

    :cond_0
    instance-of v0, v9, Landroid/text/Spanned;

    if-eqz v0, :cond_1

    new-instance v0, Landroid/text/Layout$SpannedEllipsizer;

    invoke-direct {v0, p1}, Landroid/text/Layout$SpannedEllipsizer;-><init>(Ljava/lang/CharSequence;)V

    move-object v1, v0

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/text/Layout$Ellipsizer;

    invoke-direct {v0, p1}, Landroid/text/Layout$Ellipsizer;-><init>(Ljava/lang/CharSequence;)V

    move-object v1, v0

    :goto_0
    move-object v0, p0

    move-object/from16 v2, p4

    move/from16 v3, p5

    move-object/from16 v4, p6

    move-object/from16 v5, p7

    move/from16 v6, p8

    move/from16 v7, p9

    invoke-direct/range {v0 .. v7}, Landroid/text/Layout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/TextDirectionHeuristic;FF)V

    const/4 v0, -0x1

    iput v0, v8, Landroid/text/StaticLayout;->mMaxLineHeight:I

    const v0, 0x7fffffff

    iput v0, v8, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    invoke-static/range {p1 .. p5}, Landroid/text/StaticLayout$Builder;->obtain(Ljava/lang/CharSequence;IILandroid/text/TextPaint;I)Landroid/text/StaticLayout$Builder;

    move-result-object v0

    move-object/from16 v1, p6

    invoke-virtual {v0, v1}, Landroid/text/StaticLayout$Builder;->setAlignment(Landroid/text/Layout$Alignment;)Landroid/text/StaticLayout$Builder;

    move-result-object v0

    move-object/from16 v2, p7

    invoke-virtual {v0, v2}, Landroid/text/StaticLayout$Builder;->setTextDirection(Landroid/text/TextDirectionHeuristic;)Landroid/text/StaticLayout$Builder;

    move-result-object v0

    move/from16 v3, p8

    move/from16 v4, p9

    invoke-virtual {v0, v4, v3}, Landroid/text/StaticLayout$Builder;->setLineSpacing(FF)Landroid/text/StaticLayout$Builder;

    move-result-object v0

    move/from16 v5, p10

    invoke-virtual {v0, v5}, Landroid/text/StaticLayout$Builder;->setIncludePad(Z)Landroid/text/StaticLayout$Builder;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/text/StaticLayout$Builder;->setEllipsizedWidth(I)Landroid/text/StaticLayout$Builder;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/text/StaticLayout$Builder;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)Landroid/text/StaticLayout$Builder;

    move-result-object v0

    invoke-virtual {v0, v12}, Landroid/text/StaticLayout$Builder;->setMaxLines(I)Landroid/text/StaticLayout$Builder;

    move-result-object v0

    if-eqz v10, :cond_2

    invoke-virtual {p0}, Landroid/text/StaticLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    check-cast v6, Landroid/text/Layout$Ellipsizer;

    iput-object v8, v6, Landroid/text/Layout$Ellipsizer;->mLayout:Landroid/text/Layout;

    iput v11, v6, Landroid/text/Layout$Ellipsizer;->mWidth:I

    iput-object v10, v6, Landroid/text/Layout$Ellipsizer;->mMethod:Landroid/text/TextUtils$TruncateAt;

    iput v11, v8, Landroid/text/StaticLayout;->mEllipsizedWidth:I

    const/4 v7, 0x7

    iput v7, v8, Landroid/text/StaticLayout;->mColumns:I

    move/from16 v6, p5

    goto :goto_1

    :cond_2
    const/4 v6, 0x5

    iput v6, v8, Landroid/text/StaticLayout;->mColumns:I

    move/from16 v6, p5

    iput v6, v8, Landroid/text/StaticLayout;->mEllipsizedWidth:I

    :goto_1
    const-class v7, Landroid/text/Layout$Directions;

    const/4 v13, 0x2

    invoke-static {v7, v13}, Lcom/android/internal/util/ArrayUtils;->newUnpaddedArray(Ljava/lang/Class;I)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Landroid/text/Layout$Directions;

    iput-object v7, v8, Landroid/text/StaticLayout;->mLineDirections:[Landroid/text/Layout$Directions;

    iget v7, v8, Landroid/text/StaticLayout;->mColumns:I

    mul-int/2addr v7, v13

    invoke-static {v7}, Lcom/android/internal/util/ArrayUtils;->newUnpaddedIntArray(I)[I

    move-result-object v7

    iput-object v7, v8, Landroid/text/StaticLayout;->mLines:[I

    iput v12, v8, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    invoke-static {v0}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmIncludePad(Landroid/text/StaticLayout$Builder;)Z

    move-result v7

    invoke-static {v0}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmIncludePad(Landroid/text/StaticLayout$Builder;)Z

    move-result v13

    invoke-virtual {p0, v0, v7, v13}, Landroid/text/StaticLayout;->generate(Landroid/text/StaticLayout$Builder;ZZ)V

    invoke-static {v0}, Landroid/text/StaticLayout$Builder;->-$$Nest$smrecycle(Landroid/text/StaticLayout$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V
    .locals 10
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move v5, p3

    move-object v6, p4

    move v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    invoke-direct/range {v0 .. v9}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    return-void
.end method

.method private calculateEllipsis(IILandroid/text/MeasuredParagraph;IFLandroid/text/TextUtils$TruncateAt;IFLandroid/text/TextPaint;Z)V
    .locals 18

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p6

    move/from16 v3, p7

    invoke-direct {v0, v3}, Landroid/text/StaticLayout;->getTotalInsets(I)F

    move-result v4

    sub-float v4, p5, v4

    cmpg-float v5, p8, v4

    const/4 v6, 0x5

    if-gtz v5, :cond_0

    if-nez p10, :cond_0

    iget-object v5, v0, Landroid/text/StaticLayout;->mLines:[I

    iget v7, v0, Landroid/text/StaticLayout;->mColumns:I

    mul-int v8, v7, v3

    add-int/2addr v8, v6

    const/4 v6, 0x0

    aput v6, v5, v8

    mul-int/2addr v7, v3

    add-int/lit8 v7, v7, 0x6

    aput v6, v5, v7

    return-void

    :cond_0
    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->getEllipsisString(Landroid/text/TextUtils$TruncateAt;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v7, p9

    invoke-virtual {v7, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v5

    const/4 v8, 0x0

    const/4 v9, 0x0

    sub-int v10, p2, p1

    sget-object v11, Landroid/text/TextUtils$TruncateAt;->START:Landroid/text/TextUtils$TruncateAt;

    const/4 v12, 0x0

    const-string v13, "StaticLayout"

    const/4 v14, 0x1

    if-ne v2, v11, :cond_4

    iget v11, v0, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    if-ne v11, v14, :cond_3

    const/4 v11, 0x0

    move v13, v10

    :goto_0
    if-lez v13, :cond_2

    add-int/lit8 v15, v13, -0x1

    add-int v15, v15, p1

    sub-int v15, v15, p4

    invoke-virtual {v1, v15}, Landroid/text/MeasuredParagraph;->getCharWidthAt(I)F

    move-result v15

    add-float v16, v15, v11

    add-float v16, v16, v5

    cmpl-float v16, v16, v4

    if-lez v16, :cond_1

    :goto_1
    if-ge v13, v10, :cond_2

    add-int v16, v13, p1

    sub-int v14, v16, p4

    invoke-virtual {v1, v14}, Landroid/text/MeasuredParagraph;->getCharWidthAt(I)F

    move-result v14

    cmpl-float v14, v14, v12

    if-nez v14, :cond_2

    add-int/lit8 v13, v13, 0x1

    const/4 v14, 0x1

    goto :goto_1

    :cond_1
    add-float/2addr v11, v15

    add-int/lit8 v13, v13, -0x1

    const/4 v14, 0x1

    goto :goto_0

    :cond_2
    const/4 v8, 0x0

    move v9, v13

    goto/16 :goto_9

    :cond_3
    invoke-static {v13, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_f

    const-string v11, "Start Ellipsis only supported with one line"

    invoke-static {v13, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_9

    :cond_4
    sget-object v11, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    if-eq v2, v11, :cond_b

    sget-object v11, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    if-eq v2, v11, :cond_b

    sget-object v11, Landroid/text/TextUtils$TruncateAt;->END_SMALL:Landroid/text/TextUtils$TruncateAt;

    if-ne v2, v11, :cond_5

    goto/16 :goto_6

    :cond_5
    iget v11, v0, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    const/4 v14, 0x1

    if-ne v11, v14, :cond_a

    const/4 v11, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move v15, v10

    sub-float v16, v4, v5

    const/high16 v17, 0x40000000    # 2.0f

    div-float v16, v16, v17

    nop

    :goto_2
    if-lez v15, :cond_7

    add-int/lit8 v17, v15, -0x1

    add-int v17, v17, p1

    sub-int v6, v17, p4

    invoke-virtual {v1, v6}, Landroid/text/MeasuredParagraph;->getCharWidthAt(I)F

    move-result v6

    add-float v17, v6, v13

    cmpl-float v17, v17, v16

    if-lez v17, :cond_6

    :goto_3
    if-ge v15, v10, :cond_7

    add-int v17, v15, p1

    sub-int v12, v17, p4

    invoke-virtual {v1, v12}, Landroid/text/MeasuredParagraph;->getCharWidthAt(I)F

    move-result v12

    const/16 v17, 0x0

    cmpl-float v12, v12, v17

    if-nez v12, :cond_7

    add-int/lit8 v15, v15, 0x1

    move/from16 v12, v17

    goto :goto_3

    :cond_6
    move/from16 v17, v12

    add-float/2addr v13, v6

    add-int/lit8 v15, v15, -0x1

    const/4 v6, 0x5

    goto :goto_2

    :cond_7
    sub-float v6, v4, v5

    sub-float/2addr v6, v13

    const/4 v12, 0x0

    :goto_4
    if-ge v12, v15, :cond_9

    add-int v14, v12, p1

    sub-int v14, v14, p4

    invoke-virtual {v1, v14}, Landroid/text/MeasuredParagraph;->getCharWidthAt(I)F

    move-result v14

    add-float v17, v14, v11

    cmpl-float v17, v17, v6

    if-lez v17, :cond_8

    goto :goto_5

    :cond_8
    add-float/2addr v11, v14

    add-int/lit8 v12, v12, 0x1

    goto :goto_4

    :cond_9
    :goto_5
    move v8, v12

    sub-int v9, v15, v12

    goto :goto_9

    :cond_a
    const/4 v6, 0x5

    invoke-static {v13, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_f

    const-string v6, "Middle Ellipsis only supported with one line"

    invoke-static {v13, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    :cond_b
    :goto_6
    const/4 v6, 0x0

    const/4 v11, 0x0

    :goto_7
    if-ge v11, v10, :cond_d

    add-int v12, v11, p1

    sub-int v12, v12, p4

    invoke-virtual {v1, v12}, Landroid/text/MeasuredParagraph;->getCharWidthAt(I)F

    move-result v12

    add-float v13, v12, v6

    add-float/2addr v13, v5

    cmpl-float v13, v13, v4

    if-lez v13, :cond_c

    goto :goto_8

    :cond_c
    add-float/2addr v6, v12

    add-int/lit8 v11, v11, 0x1

    goto :goto_7

    :cond_d
    :goto_8
    move v8, v11

    sub-int v9, v10, v11

    if-eqz p10, :cond_e

    if-nez v9, :cond_e

    if-lez v10, :cond_e

    add-int/lit8 v8, v10, -0x1

    const/4 v9, 0x1

    :cond_e
    nop

    :cond_f
    :goto_9
    const/4 v6, 0x1

    iput-boolean v6, v0, Landroid/text/StaticLayout;->mEllipsized:Z

    iget-object v6, v0, Landroid/text/StaticLayout;->mLines:[I

    iget v11, v0, Landroid/text/StaticLayout;->mColumns:I

    mul-int v12, v11, v3

    const/4 v13, 0x5

    add-int/2addr v12, v13

    aput v8, v6, v12

    mul-int/2addr v11, v3

    add-int/lit8 v11, v11, 0x6

    aput v9, v6, v11

    return-void
.end method

.method private static getBaseHyphenationFrequency(I)I
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    return v0

    :pswitch_0
    const/4 v0, 0x2

    return v0

    :pswitch_1
    const/4 v0, 0x1

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getTotalInsets(I)F
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/text/StaticLayout;->mLeftIndents:[I

    if-eqz v1, :cond_0

    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    aget v0, v1, v2

    :cond_0
    iget-object v1, p0, Landroid/text/StaticLayout;->mRightIndents:[I

    if-eqz v1, :cond_1

    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    aget v1, v1, v2

    add-int/2addr v0, v1

    :cond_1
    int-to-float v1, v0

    return v1
.end method

.method private out(Ljava/lang/CharSequence;IIIIIIIFF[Landroid/text/style/LineHeightSpan;[ILandroid/graphics/Paint$FontMetricsInt;ZIZLandroid/text/MeasuredParagraph;IZZZ[CILandroid/text/TextUtils$TruncateAt;FFLandroid/text/TextPaint;Z)I
    .locals 29

    move-object/from16 v11, p0

    move/from16 v12, p2

    move/from16 v13, p3

    move-object/from16 v14, p11

    move-object/from16 v15, p13

    move/from16 v9, p18

    move/from16 v8, p23

    move-object/from16 v10, p24

    iget v7, v11, Landroid/text/StaticLayout;->mLineCount:I

    iget v0, v11, Landroid/text/StaticLayout;->mColumns:I

    mul-int v16, v7, v0

    add-int v0, v16, v0

    const/4 v6, 0x1

    add-int/lit8 v5, v0, 0x1

    iget-object v0, v11, Landroid/text/StaticLayout;->mLines:[I

    invoke-virtual/range {p17 .. p17}, Landroid/text/MeasuredParagraph;->getParagraphDir()I

    move-result v17

    array-length v1, v0

    const/4 v4, 0x0

    if-lt v5, v1, :cond_0

    invoke-static {v5}, Lcom/android/internal/util/GrowingArrayUtils;->growSize(I)I

    move-result v1

    invoke-static {v1}, Lcom/android/internal/util/ArrayUtils;->newUnpaddedIntArray(I)[I

    move-result-object v1

    array-length v2, v0

    invoke-static {v0, v4, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, v11, Landroid/text/StaticLayout;->mLines:[I

    move-object v0, v1

    move-object/from16 v18, v0

    goto :goto_0

    :cond_0
    move-object/from16 v18, v0

    :goto_0
    iget-object v0, v11, Landroid/text/StaticLayout;->mLineDirections:[Landroid/text/Layout$Directions;

    array-length v0, v0

    if-lt v7, v0, :cond_1

    const-class v0, Landroid/text/Layout$Directions;

    invoke-static {v7}, Lcom/android/internal/util/GrowingArrayUtils;->growSize(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/internal/util/ArrayUtils;->newUnpaddedArray(Ljava/lang/Class;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/Layout$Directions;

    iget-object v1, v11, Landroid/text/StaticLayout;->mLineDirections:[Landroid/text/Layout$Directions;

    array-length v2, v1

    invoke-static {v1, v4, v0, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v0, v11, Landroid/text/StaticLayout;->mLineDirections:[Landroid/text/Layout$Directions;

    :cond_1
    if-eqz v14, :cond_4

    move/from16 v3, p4

    iput v3, v15, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    move/from16 v2, p5

    iput v2, v15, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    move/from16 v1, p6

    iput v1, v15, Landroid/graphics/Paint$FontMetricsInt;->top:I

    move/from16 v0, p7

    iput v0, v15, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    const/16 v19, 0x0

    move/from16 v8, v19

    :goto_1
    array-length v4, v14

    if-ge v8, v4, :cond_3

    aget-object v4, v14, v8

    instance-of v4, v4, Landroid/text/style/LineHeightSpan$WithDensity;

    if-eqz v4, :cond_2

    aget-object v4, v14, v8

    check-cast v4, Landroid/text/style/LineHeightSpan$WithDensity;

    aget v20, p12, v8

    move-object v0, v4

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    const/16 v19, 0x0

    move/from16 v4, v20

    move/from16 v20, v5

    move/from16 v5, p8

    move v12, v6

    move-object/from16 v6, p13

    move/from16 v21, v7

    move-object/from16 v7, p27

    invoke-interface/range {v0 .. v7}, Landroid/text/style/LineHeightSpan$WithDensity;->chooseHeight(Ljava/lang/CharSequence;IIIILandroid/graphics/Paint$FontMetricsInt;Landroid/text/TextPaint;)V

    goto :goto_2

    :cond_2
    move/from16 v20, v5

    move v12, v6

    move/from16 v21, v7

    const/16 v19, 0x0

    aget-object v0, v14, v8

    aget v4, p12, v8

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v5, p8

    move-object/from16 v6, p13

    invoke-interface/range {v0 .. v6}, Landroid/text/style/LineHeightSpan;->chooseHeight(Ljava/lang/CharSequence;IIIILandroid/graphics/Paint$FontMetricsInt;)V

    :goto_2
    add-int/lit8 v8, v8, 0x1

    move/from16 v3, p4

    move/from16 v2, p5

    move/from16 v1, p6

    move/from16 v0, p7

    move v6, v12

    move/from16 v4, v19

    move/from16 v5, v20

    move/from16 v7, v21

    move/from16 v12, p2

    goto :goto_1

    :cond_3
    move/from16 v20, v5

    move v12, v6

    move/from16 v21, v7

    const/16 v19, 0x0

    iget v0, v15, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    iget v1, v15, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    iget v2, v15, Landroid/graphics/Paint$FontMetricsInt;->top:I

    iget v3, v15, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    move/from16 v22, v0

    move/from16 v23, v1

    move/from16 v24, v2

    move/from16 v25, v3

    goto :goto_3

    :cond_4
    move/from16 v19, v4

    move/from16 v20, v5

    move v12, v6

    move/from16 v21, v7

    move/from16 v22, p4

    move/from16 v23, p5

    move/from16 v24, p6

    move/from16 v25, p7

    :goto_3
    if-nez v21, :cond_5

    move v6, v12

    goto :goto_4

    :cond_5
    move/from16 v6, v19

    :goto_4
    move/from16 v26, v6

    add-int/lit8 v7, v21, 0x1

    iget v0, v11, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    if-ne v7, v0, :cond_6

    move v6, v12

    goto :goto_5

    :cond_6
    move/from16 v6, v19

    :goto_5
    move/from16 v27, v6

    if-eqz v10, :cond_f

    if-eqz p28, :cond_7

    iget v1, v11, Landroid/text/StaticLayout;->mLineCount:I

    add-int/2addr v1, v12

    if-ne v1, v0, :cond_7

    move v1, v12

    goto :goto_6

    :cond_7
    move/from16 v1, v19

    :goto_6
    move-object v8, v10

    move v10, v1

    if-ne v0, v12, :cond_8

    if-nez p28, :cond_9

    :cond_8
    if-eqz v26, :cond_a

    if-nez p28, :cond_a

    :cond_9
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    if-ne v8, v0, :cond_c

    :cond_a
    if-nez v26, :cond_d

    if-nez v27, :cond_b

    if-nez p28, :cond_d

    :cond_b
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    if-ne v8, v0, :cond_d

    :cond_c
    move v6, v12

    goto :goto_7

    :cond_d
    move/from16 v6, v19

    :goto_7
    move/from16 v28, v6

    if-eqz v28, :cond_e

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    move-object/from16 v3, p17

    move/from16 v4, p23

    move/from16 v5, p25

    move-object/from16 v6, p24

    move/from16 v7, v21

    move/from16 v12, p23

    move/from16 v8, p26

    move v14, v9

    move-object/from16 v9, p27

    invoke-direct/range {v0 .. v10}, Landroid/text/StaticLayout;->calculateEllipsis(IILandroid/text/MeasuredParagraph;IFLandroid/text/TextUtils$TruncateAt;IFLandroid/text/TextPaint;Z)V

    goto :goto_8

    :cond_e
    move/from16 v12, p23

    move v14, v9

    iget-object v0, v11, Landroid/text/StaticLayout;->mLines:[I

    iget v1, v11, Landroid/text/StaticLayout;->mColumns:I

    mul-int v7, v1, v21

    add-int/lit8 v7, v7, 0x5

    aput v19, v0, v7

    mul-int v1, v1, v21

    add-int/lit8 v1, v1, 0x6

    aput v19, v0, v1

    goto :goto_8

    :cond_f
    move/from16 v12, p23

    move v14, v9

    :goto_8
    iget-boolean v0, v11, Landroid/text/StaticLayout;->mEllipsized:Z

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    move-object/from16 v1, p1

    move/from16 v2, p2

    const/4 v3, 0x1

    goto :goto_a

    :cond_10
    if-eq v12, v14, :cond_11

    if-lez v14, :cond_11

    add-int/lit8 v0, v14, -0x1

    move-object/from16 v1, p1

    invoke-interface {v1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v2, 0xa

    if-ne v0, v2, :cond_12

    const/4 v6, 0x1

    goto :goto_9

    :cond_11
    move-object/from16 v1, p1

    :cond_12
    move/from16 v6, v19

    :goto_9
    move v0, v6

    if-ne v13, v14, :cond_13

    if-nez v0, :cond_13

    const/4 v2, 0x1

    move v0, v2

    const/4 v3, 0x1

    move/from16 v2, p2

    goto :goto_a

    :cond_13
    move/from16 v2, p2

    const/4 v3, 0x1

    if-ne v2, v14, :cond_14

    if-eqz v0, :cond_14

    const/4 v4, 0x1

    move v0, v4

    goto :goto_a

    :cond_14
    const/4 v4, 0x0

    move v0, v4

    :goto_a
    if-eqz v26, :cond_16

    if-eqz p20, :cond_15

    sub-int v4, v24, v22

    iput v4, v11, Landroid/text/StaticLayout;->mTopPadding:I

    :cond_15
    if-eqz p19, :cond_16

    move/from16 v22, v24

    :cond_16
    if-eqz v0, :cond_18

    if-eqz p20, :cond_17

    sub-int v4, v25, v23

    iput v4, v11, Landroid/text/StaticLayout;->mBottomPadding:I

    :cond_17
    if-eqz p19, :cond_18

    move/from16 v23, v25

    :cond_18
    if-eqz p16, :cond_1b

    if-nez p21, :cond_19

    if-nez v0, :cond_1b

    :cond_19
    sub-int v4, v23, v22

    int-to-float v4, v4

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float v5, p9, v5

    mul-float/2addr v4, v5

    add-float v4, v4, p10

    float-to-double v4, v4

    const-wide/16 v6, 0x0

    cmpl-double v6, v4, v6

    const-wide/high16 v7, 0x3fe0000000000000L    # 0.5

    if-ltz v6, :cond_1a

    add-double/2addr v7, v4

    double-to-int v6, v7

    goto :goto_b

    :cond_1a
    neg-double v9, v4

    add-double/2addr v9, v7

    double-to-int v6, v9

    neg-int v6, v6

    :goto_b
    goto :goto_c

    :cond_1b
    const/4 v6, 0x0

    :goto_c
    add-int/lit8 v4, v16, 0x0

    aput v2, v18, v4

    add-int/lit8 v4, v16, 0x1

    aput p8, v18, v4

    add-int/lit8 v4, v16, 0x2

    add-int v5, v23, v6

    aput v5, v18, v4

    add-int/lit8 v4, v16, 0x3

    aput v6, v18, v4

    iget-boolean v4, v11, Landroid/text/StaticLayout;->mEllipsized:Z

    if-nez v4, :cond_1d

    if-eqz v27, :cond_1d

    if-eqz p19, :cond_1c

    move/from16 v4, v25

    goto :goto_d

    :cond_1c
    move/from16 v4, v23

    :goto_d
    sub-int v5, v4, v22

    add-int v5, p8, v5

    iput v5, v11, Landroid/text/StaticLayout;->mMaxLineHeight:I

    :cond_1d
    sub-int v4, v23, v22

    add-int/2addr v4, v6

    add-int v4, p8, v4

    iget v5, v11, Landroid/text/StaticLayout;->mColumns:I

    add-int v7, v16, v5

    add-int/lit8 v7, v7, 0x0

    aput v13, v18, v7

    add-int v5, v16, v5

    add-int/2addr v5, v3

    aput v4, v18, v5

    add-int/lit8 v5, v16, 0x0

    aget v7, v18, v5

    if-eqz p14, :cond_1e

    const/high16 v8, 0x20000000

    move/from16 v19, v8

    :cond_1e
    or-int v7, v7, v19

    aput v7, v18, v5

    add-int/lit8 v5, v16, 0x4

    aput p15, v18, v5

    add-int/lit8 v5, v16, 0x0

    aget v7, v18, v5

    shl-int/lit8 v8, v17, 0x1e

    or-int/2addr v7, v8

    aput v7, v18, v5

    iget-object v5, v11, Landroid/text/StaticLayout;->mLineDirections:[Landroid/text/Layout$Directions;

    sub-int v7, v2, v12

    sub-int v8, v13, v12

    move-object/from16 v9, p17

    invoke-virtual {v9, v7, v8}, Landroid/text/MeasuredParagraph;->getDirections(II)Landroid/text/Layout$Directions;

    move-result-object v7

    aput-object v7, v5, v21

    iget v5, v11, Landroid/text/StaticLayout;->mLineCount:I

    add-int/2addr v5, v3

    iput v5, v11, Landroid/text/StaticLayout;->mLineCount:I

    return v4
.end method

.method static packHyphenEdit(II)I
    .locals 1

    shl-int/lit8 v0, p0, 0x3

    or-int/2addr v0, p1

    return v0
.end method

.method static unpackEndHyphenEdit(I)I
    .locals 1

    and-int/lit8 v0, p0, 0x7

    return v0
.end method

.method static unpackStartHyphenEdit(I)I
    .locals 1

    and-int/lit8 v0, p0, 0x18

    shr-int/lit8 v0, v0, 0x3

    return v0
.end method


# virtual methods
.method generate(Landroid/text/StaticLayout$Builder;ZZ)V
    .locals 87

    goto/32 :goto_25d

    nop

    :goto_0
    move-object v4, v9

    goto/32 :goto_15f

    nop

    :goto_1
    move/from16 v36, p2

    goto/32 :goto_73

    nop

    :goto_2
    move-object/from16 v56, v8

    goto/32 :goto_c1

    nop

    :goto_3
    move/from16 v1, v28

    goto/32 :goto_1c6

    nop

    :goto_4
    invoke-virtual {v0, v1}, Landroid/graphics/text/LineBreaker$Builder;->setBreakStrategy(I)Landroid/graphics/text/LineBreaker$Builder;

    move-result-object v0

    goto/32 :goto_1cb

    nop

    :goto_5
    move-object/from16 v80, v31

    goto/32 :goto_db

    nop

    :goto_6
    invoke-static/range {p1 .. p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmTextDir(Landroid/text/StaticLayout$Builder;)Landroid/text/TextDirectionHeuristic;

    move-result-object v8

    goto/32 :goto_116

    nop

    :goto_7
    add-int/lit8 v7, v14, -0x1

    goto/32 :goto_2eb

    nop

    :goto_8
    move/from16 v17, v35

    goto/32 :goto_b0

    nop

    :goto_9
    move-object/from16 v71, v15

    goto/32 :goto_242

    nop

    :goto_a
    goto/16 :goto_2f6

    :goto_b
    goto/32 :goto_331

    nop

    :goto_c
    goto/16 :goto_217

    :goto_d
    goto/32 :goto_216

    nop

    :goto_e
    move-object/from16 v79, v8

    goto/32 :goto_2b5

    nop

    :goto_f
    move-object/from16 v45, v21

    goto/32 :goto_1df

    nop

    :goto_10
    const/4 v7, 0x0

    goto/32 :goto_233

    nop

    :goto_11
    move/from16 v9, v17

    goto/32 :goto_98

    nop

    :goto_12
    aget-object v11, v8, v9

    goto/32 :goto_2aa

    nop

    :goto_13
    const/4 v1, 0x0

    goto/32 :goto_34a

    nop

    :goto_14
    move/from16 v54, v0

    goto/32 :goto_71

    nop

    :goto_15
    move/from16 v10, v48

    goto/32 :goto_32d

    nop

    :goto_16
    move/from16 v31, v27

    :goto_17
    goto/32 :goto_1d

    nop

    :goto_18
    const-class v8, Landroid/text/style/LineHeightSpan;

    goto/32 :goto_39c

    nop

    :goto_19
    move v11, v3

    goto/32 :goto_145

    nop

    :goto_1a
    iget-boolean v4, v13, Landroid/text/StaticLayout;->mFallbackLineSpacing:Z

    goto/32 :goto_fa

    nop

    :goto_1b
    move-object/from16 v42, v18

    goto/32 :goto_8b

    nop

    :goto_1c
    move-object v4, v13

    goto/32 :goto_a7

    nop

    :goto_1d
    move v12, v0

    goto/32 :goto_7c

    nop

    :goto_1e
    goto/16 :goto_2d4

    :goto_1f
    goto/32 :goto_2e7

    nop

    :goto_20
    move v2, v4

    goto/32 :goto_150

    nop

    :goto_21
    const/16 v19, 0x0

    goto/32 :goto_30a

    nop

    :goto_22
    move v14, v0

    goto/32 :goto_37b

    nop

    :goto_23
    move-object/from16 v11, v79

    goto/32 :goto_2f5

    nop

    :goto_24
    if-ne v5, v10, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_b2

    nop

    :goto_25
    move-object/from16 v21, v45

    goto/32 :goto_21b

    nop

    :goto_26
    aput v6, v3, v4

    goto/32 :goto_143

    nop

    :goto_27
    invoke-static {v12, v5, v5, v11, v13}, Landroid/text/MeasuredParagraph;->buildForBidi(Ljava/lang/CharSequence;IILandroid/text/TextDirectionHeuristic;Landroid/text/MeasuredParagraph;)Landroid/text/MeasuredParagraph;

    move-result-object v13

    goto/32 :goto_112

    nop

    :goto_28
    move-object/from16 v71, v15

    goto/32 :goto_2a3

    nop

    :goto_29
    goto/16 :goto_2f6

    :goto_2a
    goto/32 :goto_23

    nop

    :goto_2b
    return-void

    :goto_2c
    goto/32 :goto_f8

    nop

    :goto_2d
    move/from16 v20, v5

    goto/32 :goto_391

    nop

    :goto_2e
    move-object/from16 v26, v2

    goto/32 :goto_1ac

    nop

    :goto_2f
    invoke-virtual {v1, v2}, Landroid/text/PrecomputedText$Params$Builder;->setLineBreakConfig(Landroid/graphics/text/LineBreakConfig;)Landroid/text/PrecomputedText$Params$Builder;

    move-result-object v1

    goto/32 :goto_254

    nop

    :goto_30
    if-nez v1, :cond_1

    goto/32 :goto_5d

    :cond_1
    goto/32 :goto_5c

    nop

    :goto_31
    move-object/from16 v79, v30

    goto/32 :goto_292

    nop

    :goto_32
    move-object/from16 v2, v32

    goto/32 :goto_1a4

    nop

    :goto_33
    move-object/from16 v12, v85

    goto/32 :goto_289

    nop

    :goto_34
    const/16 v20, 0x1

    goto/32 :goto_256

    nop

    :goto_35
    array-length v9, v8

    goto/32 :goto_225

    nop

    :goto_36
    array-length v3, v2

    goto/32 :goto_1b1

    nop

    :goto_37
    move/from16 v17, v12

    goto/32 :goto_2fd

    nop

    :goto_38
    move v11, v3

    goto/32 :goto_11

    nop

    :goto_39
    move/from16 v21, v49

    goto/32 :goto_358

    nop

    :goto_3a
    invoke-static/range {p1 .. p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmFallbackLineSpacing(Landroid/text/StaticLayout$Builder;)Z

    move-result v0

    goto/32 :goto_1bd

    nop

    :goto_3b
    array-length v0, v0

    :goto_3c
    goto/32 :goto_241

    nop

    :goto_3d
    move/from16 v74, v62

    goto/32 :goto_14f

    nop

    :goto_3e
    if-eqz v2, :cond_2

    goto/32 :goto_234

    :cond_2
    goto/32 :goto_10

    nop

    :goto_3f
    move/from16 v9, v24

    goto/32 :goto_250

    nop

    :goto_40
    move-object/from16 v9, v31

    goto/32 :goto_13a

    nop

    :goto_41
    move-object/from16 v71, v15

    goto/32 :goto_25a

    nop

    :goto_42
    aget v4, v45, v14

    goto/32 :goto_1ba

    nop

    :goto_43
    if-lt v11, v13, :cond_3

    goto/32 :goto_2a

    :cond_3
    nop

    goto/32 :goto_333

    nop

    :goto_44
    const/4 v6, 0x1

    goto/32 :goto_87

    nop

    :goto_45
    move/from16 v82, v10

    goto/32 :goto_1c

    nop

    :goto_46
    move v8, v14

    goto/32 :goto_252

    nop

    :goto_47
    iget v0, v9, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    goto/32 :goto_279

    nop

    :goto_48
    move-object/from16 v15, v71

    goto/32 :goto_126

    nop

    :goto_49
    move/from16 v6, v68

    goto/32 :goto_11f

    nop

    :goto_4a
    move-object/from16 v31, v80

    goto/32 :goto_2d7

    nop

    :goto_4b
    add-int v1, v5, v19

    goto/32 :goto_314

    nop

    :goto_4c
    aget v7, v42, v7

    :goto_4d
    goto/32 :goto_13d

    nop

    :goto_4e
    goto/16 :goto_f2

    :goto_4f
    goto/32 :goto_6a

    nop

    :goto_50
    add-int/2addr v6, v7

    goto/32 :goto_26

    nop

    :goto_51
    const/4 v0, 0x0

    goto/32 :goto_77

    nop

    :goto_52
    move-object/from16 v12, v35

    goto/32 :goto_1e

    nop

    :goto_53
    move/from16 v50, v23

    goto/32 :goto_142

    nop

    :goto_54
    move-object/from16 v18, v2

    goto/32 :goto_80

    nop

    :goto_55
    if-eqz v6, :cond_4

    goto/32 :goto_131

    :cond_4
    goto/32 :goto_396

    nop

    :goto_56
    aget v6, v6, v4

    goto/32 :goto_18f

    nop

    :goto_57
    move-object/from16 v30, v79

    goto/32 :goto_9a

    nop

    :goto_58
    move-object/from16 v85, v35

    goto/32 :goto_37a

    nop

    :goto_59
    if-lt v8, v9, :cond_5

    goto/32 :goto_1f

    :cond_5
    goto/32 :goto_2db

    nop

    :goto_5a
    int-to-float v1, v9

    goto/32 :goto_2fc

    nop

    :goto_5b
    move/from16 v0, v50

    goto/32 :goto_3c0

    nop

    :goto_5c
    goto/16 :goto_220

    :goto_5d
    goto/32 :goto_51

    nop

    :goto_5e
    if-nez v54, :cond_6

    goto/32 :goto_253

    :cond_6
    goto/32 :goto_265

    nop

    :goto_5f
    invoke-static/range {p1 .. p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmLineBreakConfig(Landroid/text/StaticLayout$Builder;)Landroid/graphics/text/LineBreakConfig;

    move-result-object v2

    goto/32 :goto_22b

    nop

    :goto_60
    iget v14, v9, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    :goto_61
    goto/32 :goto_47

    nop

    :goto_62
    const/16 v22, 0x0

    goto/32 :goto_1e7

    nop

    :goto_63
    move-object v13, v4

    goto/32 :goto_102

    nop

    :goto_64
    if-eqz v1, :cond_7

    goto/32 :goto_c5

    :cond_7
    goto/32 :goto_2c3

    nop

    :goto_65
    invoke-static/range {p1 .. p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmHyphenationFrequency(Landroid/text/StaticLayout$Builder;)I

    move-result v2

    goto/32 :goto_1f2

    nop

    :goto_66
    invoke-static {v2, v12}, Ljava/lang/Math;->max(II)I

    move-result v2

    :goto_67
    goto/32 :goto_1e5

    nop

    :goto_68
    iget v0, v9, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    goto/32 :goto_86

    nop

    :goto_69
    invoke-interface {v11}, Landroid/text/style/LeadingMarginSpan$LeadingMarginSpan2;->getLeadingMarginLineCount()I

    move-result v12

    goto/32 :goto_66

    nop

    :goto_6a
    move/from16 v63, v1

    goto/32 :goto_f1

    nop

    :goto_6b
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    goto/32 :goto_16e

    nop

    :goto_6c
    iget v13, v4, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    goto/32 :goto_43

    nop

    :goto_6d
    const/16 v13, 0xa

    goto/32 :goto_1db

    nop

    :goto_6e
    goto/16 :goto_313

    :goto_6f
    goto/32 :goto_176

    nop

    :goto_70
    invoke-virtual/range {v32 .. v32}, Landroid/text/PrecomputedText;->getParagraphInfo()[Landroid/text/PrecomputedText$ParagraphInfo;

    move-result-object v28

    goto/32 :goto_1ab

    nop

    :goto_71
    if-gtz v6, :cond_8

    goto/32 :goto_253

    :cond_8
    goto/32 :goto_33c

    nop

    :goto_72
    move/from16 v35, v5

    goto/32 :goto_1

    nop

    :goto_73
    move/from16 v37, p3

    goto/32 :goto_382

    nop

    :goto_74
    move-object/from16 v6, v43

    goto/32 :goto_191

    nop

    :goto_75
    move/from16 v82, v10

    goto/32 :goto_320

    nop

    :goto_76
    move/from16 v59, v32

    goto/32 :goto_57

    nop

    :goto_77
    move-object v7, v0

    goto/32 :goto_21f

    nop

    :goto_78
    move/from16 v5, v82

    goto/32 :goto_361

    nop

    :goto_79
    const/16 v45, 0x0

    goto/32 :goto_105

    nop

    :goto_7a
    array-length v9, v1

    goto/32 :goto_259

    nop

    :goto_7b
    move-object/from16 v18, v12

    goto/32 :goto_12b

    nop

    :goto_7c
    move-object v13, v4

    goto/32 :goto_2e6

    nop

    :goto_7d
    move-object/from16 v30, v79

    goto/32 :goto_34b

    nop

    :goto_7e
    move/from16 v10, v83

    goto/32 :goto_24

    nop

    :goto_7f
    move-object/from16 v3, v50

    goto/32 :goto_229

    nop

    :goto_80
    move-object/from16 v55, v7

    goto/32 :goto_2

    nop

    :goto_81
    iget v15, v3, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    goto/32 :goto_2e2

    nop

    :goto_82
    move-object/from16 v34, v1

    :goto_83
    goto/32 :goto_1c4

    nop

    :goto_84
    goto/16 :goto_17

    :goto_85
    goto/32 :goto_3d0

    nop

    :goto_86
    if-lt v0, v7, :cond_9

    goto/32 :goto_1d4

    :cond_9
    goto/32 :goto_1d3

    nop

    :goto_87
    goto/16 :goto_e7

    :goto_88
    goto/32 :goto_3ca

    nop

    :goto_89
    move-object/from16 v2, v32

    goto/32 :goto_aa

    nop

    :goto_8a
    add-int/lit8 v2, v6, -0x1

    goto/32 :goto_21a

    nop

    :goto_8b
    move-object/from16 v43, v19

    goto/32 :goto_1b0

    nop

    :goto_8c
    move/from16 v72, v19

    goto/32 :goto_12c

    nop

    :goto_8d
    move/from16 v76, v25

    goto/32 :goto_1a7

    nop

    :goto_8e
    move/from16 v22, v5

    goto/32 :goto_1d9

    nop

    :goto_8f
    const/16 v29, 0x0

    goto/32 :goto_a4

    nop

    :goto_90
    const/4 v9, 0x0

    :goto_91
    goto/32 :goto_2ce

    nop

    :goto_92
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :goto_93
    move-object v1, v9

    goto/32 :goto_23f

    nop

    :goto_94
    move/from16 v6, v25

    goto/32 :goto_1e0

    nop

    :goto_95
    move/from16 v68, v22

    goto/32 :goto_fe

    nop

    :goto_96
    move-object/from16 v1, v36

    goto/32 :goto_fb

    nop

    :goto_97
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_321

    nop

    :goto_98
    move-object/from16 v37, v25

    :goto_99
    goto/32 :goto_3c7

    nop

    :goto_9a
    move/from16 v32, v83

    goto/32 :goto_181

    nop

    :goto_9b
    move-object v1, v12

    goto/32 :goto_2e5

    nop

    :goto_9c
    invoke-interface {v11, v12}, Landroid/text/style/LeadingMarginSpan;->getLeadingMargin(Z)I

    move-result v11

    goto/32 :goto_186

    nop

    :goto_9d
    move-object/from16 v36, v1

    goto/32 :goto_287

    nop

    :goto_9e
    move/from16 v9, v17

    goto/32 :goto_16b

    nop

    :goto_9f
    invoke-static/range {p1 .. p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmLineBreakConfig(Landroid/text/StaticLayout$Builder;)Landroid/graphics/text/LineBreakConfig;

    move-result-object v31

    goto/32 :goto_339

    nop

    :goto_a0
    move-object/from16 v44, v14

    goto/32 :goto_197

    nop

    :goto_a1
    move/from16 v50, v5

    goto/32 :goto_215

    nop

    :goto_a2
    move/from16 v76, v25

    goto/32 :goto_2be

    nop

    :goto_a3
    const/16 v28, 0x0

    goto/32 :goto_c3

    nop

    :goto_a4
    const/16 v31, 0x0

    goto/32 :goto_1ad

    nop

    :goto_a5
    move v6, v1

    goto/32 :goto_32c

    nop

    :goto_a6
    add-int/lit8 v29, v19, 0x1

    goto/32 :goto_f3

    nop

    :goto_a7
    move/from16 v67, v14

    goto/32 :goto_28

    nop

    :goto_a8
    iget v15, v3, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    goto/32 :goto_114

    nop

    :goto_a9
    iput v0, v9, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    goto/32 :goto_17e

    nop

    :goto_aa
    invoke-static {v2, v1}, Landroid/text/PrecomputedText;->create(Ljava/lang/CharSequence;Landroid/text/PrecomputedText$Params;)Landroid/text/PrecomputedText;

    move-result-object v2

    goto/32 :goto_194

    nop

    :goto_ab
    invoke-virtual {v1, v8}, Landroid/text/PrecomputedText$Params$Builder;->setTextDirection(Landroid/text/TextDirectionHeuristic;)Landroid/text/PrecomputedText$Params$Builder;

    move-result-object v1

    goto/32 :goto_3c4

    nop

    :goto_ac
    aget v0, v53, v67

    goto/32 :goto_28d

    nop

    :goto_ad
    move-object/from16 v13, v81

    goto/32 :goto_2b3

    nop

    :goto_ae
    move/from16 v19, v3

    goto/32 :goto_345

    nop

    :goto_af
    invoke-virtual {v3}, Landroid/text/MeasuredParagraph;->getChars()[C

    move-result-object v38

    goto/32 :goto_1ec

    nop

    :goto_b0
    move/from16 v8, v56

    goto/32 :goto_317

    nop

    :goto_b1
    move-object/from16 v12, v36

    goto/32 :goto_ad

    nop

    :goto_b2
    add-int/lit8 v11, v5, -0x1

    goto/32 :goto_33

    nop

    :goto_b3
    move v1, v6

    goto/32 :goto_17d

    nop

    :goto_b4
    move-object v11, v9

    goto/32 :goto_2f3

    nop

    :goto_b5
    aput v24, v1, v9

    :goto_b6
    goto/32 :goto_3a7

    nop

    :goto_b7
    move-object/from16 v79, v30

    goto/32 :goto_5

    nop

    :goto_b8
    goto/16 :goto_140

    :goto_b9
    goto/32 :goto_13f

    nop

    :goto_ba
    add-int/lit8 v7, v2, -0x1

    goto/32 :goto_4c

    nop

    :goto_bb
    invoke-virtual {v0, v1}, Landroid/graphics/text/LineBreaker$Builder;->setHyphenationFrequency(I)Landroid/graphics/text/LineBreaker$Builder;

    move-result-object v0

    goto/32 :goto_23a

    nop

    :goto_bc
    move/from16 v78, v7

    goto/32 :goto_184

    nop

    :goto_bd
    move-object/from16 v52, v7

    goto/32 :goto_20f

    nop

    :goto_be
    move/from16 v74, v4

    goto/32 :goto_2b9

    nop

    :goto_bf
    move/from16 v25, v70

    goto/32 :goto_154

    nop

    :goto_c0
    add-int/lit8 v8, v8, 0x1

    goto/32 :goto_33d

    nop

    :goto_c1
    move v8, v14

    :goto_c2
    goto/32 :goto_13e

    nop

    :goto_c3
    move-object/from16 v30, v8

    goto/32 :goto_2d3

    nop

    :goto_c4
    goto/16 :goto_2f2

    :goto_c5
    goto/32 :goto_2f1

    nop

    :goto_c6
    move/from16 v23, v0

    goto/32 :goto_374

    nop

    :goto_c7
    goto/16 :goto_1ea

    :goto_c8
    goto/32 :goto_1e9

    nop

    :goto_c9
    iget v2, v9, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    goto/32 :goto_278

    nop

    :goto_ca
    move/from16 v7, v64

    goto/32 :goto_31

    nop

    :goto_cb
    move/from16 v6, v77

    goto/32 :goto_101

    nop

    :goto_cc
    move/from16 v26, v47

    goto/32 :goto_214

    nop

    :goto_cd
    move/from16 v76, v25

    goto/32 :goto_385

    nop

    :goto_ce
    const/16 v20, 0x0

    goto/32 :goto_1aa

    nop

    :goto_cf
    move-object/from16 v26, v66

    goto/32 :goto_261

    nop

    :goto_d0
    move/from16 v70, v33

    goto/32 :goto_3b1

    nop

    :goto_d1
    move/from16 v24, v60

    goto/32 :goto_255

    nop

    :goto_d2
    move-object/from16 v50, v3

    goto/32 :goto_365

    nop

    :goto_d3
    move/from16 v22, v15

    goto/32 :goto_10f

    nop

    :goto_d4
    mul-int/lit8 v0, v19, 0x4

    goto/32 :goto_334

    nop

    :goto_d5
    move-object/from16 v8, v45

    goto/32 :goto_123

    nop

    :goto_d6
    move/from16 v59, v12

    goto/32 :goto_58

    nop

    :goto_d7
    iput v0, v9, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    goto/32 :goto_a6

    nop

    :goto_d8
    const/16 v25, 0x0

    goto/32 :goto_1b6

    nop

    :goto_d9
    if-lt v0, v1, :cond_a

    goto/32 :goto_338

    :cond_a
    goto/32 :goto_337

    nop

    :goto_da
    move/from16 v20, v58

    goto/32 :goto_d1

    nop

    :goto_db
    move/from16 v83, v32

    goto/32 :goto_1ae

    nop

    :goto_dc
    invoke-virtual {v0, v7}, Landroid/graphics/text/LineBreaker$Builder;->setIndents([I)Landroid/graphics/text/LineBreaker$Builder;

    move-result-object v0

    goto/32 :goto_161

    nop

    :goto_dd
    aput v2, v44, v0

    goto/32 :goto_239

    nop

    :goto_de
    const/16 v21, 0x0

    goto/32 :goto_8e

    nop

    :goto_df
    iget-object v0, v13, Landroid/text/StaticLayout;->mLeftIndents:[I

    goto/32 :goto_17f

    nop

    :goto_e0
    move-object/from16 v52, v7

    goto/32 :goto_113

    nop

    :goto_e1
    invoke-interface {v11, v12}, Landroid/text/style/LeadingMarginSpan;->getLeadingMargin(Z)I

    move-result v11

    goto/32 :goto_352

    nop

    :goto_e2
    move-object/from16 v79, v30

    goto/32 :goto_2cb

    nop

    :goto_e3
    move/from16 v82, v10

    goto/32 :goto_15

    nop

    :goto_e4
    goto/16 :goto_1f0

    :goto_e5
    goto/32 :goto_1ef

    nop

    :goto_e6
    const/4 v4, 0x0

    :goto_e7
    goto/32 :goto_122

    nop

    :goto_e8
    move/from16 v70, v33

    goto/32 :goto_247

    nop

    :goto_e9
    move/from16 v40, v10

    goto/32 :goto_160

    nop

    :goto_ea
    invoke-static/range {p1 .. p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmHyphenationFrequency(Landroid/text/StaticLayout$Builder;)I

    move-result v30

    goto/32 :goto_9f

    nop

    :goto_eb
    move/from16 v77, v6

    goto/32 :goto_1f1

    nop

    :goto_ec
    move-object v4, v13

    goto/32 :goto_41

    nop

    :goto_ed
    move/from16 v41, v0

    goto/32 :goto_1b

    nop

    :goto_ee
    move v5, v2

    goto/32 :goto_363

    nop

    :goto_ef
    aput-boolean v1, v51, v2

    goto/32 :goto_273

    nop

    :goto_f0
    move/from16 v25, v76

    goto/32 :goto_cb

    nop

    :goto_f1
    move/from16 v64, v2

    :goto_f2
    goto/32 :goto_10e

    nop

    :goto_f3
    iget v0, v9, Landroid/graphics/Paint$FontMetricsInt;->top:I

    goto/32 :goto_d9

    nop

    :goto_f4
    invoke-static/range {p1 .. p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmBreakStrategy(Landroid/text/StaticLayout$Builder;)I

    move-result v2

    goto/32 :goto_309

    nop

    :goto_f5
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    goto/32 :goto_128

    nop

    :goto_f6
    add-int/lit8 v7, v7, 0x1

    goto/32 :goto_118

    nop

    :goto_f7
    invoke-direct {v0}, Landroid/graphics/text/LineBreaker$Builder;-><init>()V

    goto/32 :goto_3c3

    nop

    :goto_f8
    move v11, v0

    goto/32 :goto_3b3

    nop

    :goto_f9
    move-object/from16 v65, v18

    goto/32 :goto_2ab

    nop

    :goto_fa
    if-nez v4, :cond_b

    goto/32 :goto_c8

    :cond_b
    goto/32 :goto_42

    nop

    :goto_fb
    move/from16 v0, v41

    goto/32 :goto_201

    nop

    :goto_fc
    move/from16 v68, v22

    goto/32 :goto_1de

    nop

    :goto_fd
    add-int/2addr v3, v5

    goto/32 :goto_370

    nop

    :goto_fe
    move-object/from16 v66, v26

    goto/32 :goto_3a9

    nop

    :goto_ff
    move v2, v10

    goto/32 :goto_17a

    nop

    :goto_100
    const/16 v19, 0x0

    goto/32 :goto_ce

    nop

    :goto_101
    move/from16 v7, v78

    goto/32 :goto_14c

    nop

    :goto_102
    move v10, v5

    goto/32 :goto_96

    nop

    :goto_103
    move/from16 v16, v6

    :goto_104
    goto/32 :goto_31c

    nop

    :goto_105
    move-object/from16 v17, p0

    goto/32 :goto_7b

    nop

    :goto_106
    move/from16 v32, v59

    goto/32 :goto_350

    nop

    :goto_107
    const/16 v43, 0x0

    goto/32 :goto_79

    nop

    :goto_108
    move/from16 v32, v83

    goto/32 :goto_34f

    nop

    :goto_109
    move/from16 v59, v12

    goto/32 :goto_200

    nop

    :goto_10a
    array-length v2, v7

    goto/32 :goto_212

    nop

    :goto_10b
    move-object/from16 v81, v9

    goto/32 :goto_3c2

    nop

    :goto_10c
    int-to-float v14, v14

    goto/32 :goto_1cd

    nop

    :goto_10d
    invoke-virtual {v13, v12}, Landroid/text/StaticLayout;->getLineTop(I)I

    move-result v12

    goto/32 :goto_26b

    nop

    :goto_10e
    aget-boolean v0, v51, v14

    goto/32 :goto_378

    nop

    :goto_10f
    iget v15, v3, Landroid/graphics/Paint$FontMetricsInt;->top:I

    goto/32 :goto_208

    nop

    :goto_110
    goto/16 :goto_1c5

    :goto_111
    goto/32 :goto_3cc

    nop

    :goto_112
    move-object/from16 v34, v13

    goto/32 :goto_2fb

    nop

    :goto_113
    const/4 v14, 0x0

    goto/32 :goto_1bf

    nop

    :goto_114
    move/from16 v21, v15

    goto/32 :goto_303

    nop

    :goto_115
    move-object v0, v12

    goto/32 :goto_3b9

    nop

    :goto_116
    invoke-static/range {p1 .. p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmSpacingMult(Landroid/text/StaticLayout$Builder;)F

    move-result v47

    goto/32 :goto_280

    nop

    :goto_117
    move-object/from16 v0, p0

    goto/32 :goto_2b7

    nop

    :goto_118
    move-object/from16 v8, v56

    goto/32 :goto_22f

    nop

    :goto_119
    add-int/lit8 v0, v0, 0x2

    goto/32 :goto_3d2

    nop

    :goto_11a
    move-object/from16 v37, v25

    goto/32 :goto_168

    nop

    :goto_11b
    move-object/from16 v18, v2

    goto/32 :goto_221

    nop

    :goto_11c
    move/from16 v33, v21

    goto/32 :goto_117

    nop

    :goto_11d
    move-object/from16 v21, v2

    goto/32 :goto_366

    nop

    :goto_11e
    move/from16 v5, v75

    goto/32 :goto_136

    nop

    :goto_11f
    move v7, v2

    goto/32 :goto_157

    nop

    :goto_120
    invoke-static/range {p1 .. p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmEllipsizedWidth(Landroid/text/StaticLayout$Builder;)I

    move-result v0

    goto/32 :goto_2bb

    nop

    :goto_121
    const/4 v6, 0x1

    goto/32 :goto_1e2

    nop

    :goto_122
    if-lt v4, v0, :cond_c

    goto/32 :goto_88

    :cond_c
    goto/32 :goto_3b8

    nop

    :goto_123
    goto/16 :goto_203

    :goto_124
    goto/32 :goto_23c

    nop

    :goto_125
    invoke-virtual {v0, v1}, Landroid/graphics/text/LineBreaker$Builder;->setJustificationMode(I)Landroid/graphics/text/LineBreaker$Builder;

    move-result-object v0

    goto/32 :goto_dc

    nop

    :goto_126
    move-object/from16 v8, v79

    goto/32 :goto_1c1

    nop

    :goto_127
    new-instance v0, Landroid/graphics/text/LineBreaker$ParagraphConstraints;

    goto/32 :goto_12e

    nop

    :goto_128
    move-object/from16 v2, v34

    goto/32 :goto_2e1

    nop

    :goto_129
    const/16 v17, 0x0

    goto/32 :goto_258

    nop

    :goto_12a
    move-object/from16 v44, v20

    goto/32 :goto_17b

    nop

    :goto_12b
    move/from16 v19, v5

    goto/32 :goto_2d

    nop

    :goto_12c
    move/from16 v3, v72

    goto/32 :goto_3d

    nop

    :goto_12d
    move/from16 v77, v6

    goto/32 :goto_bc

    nop

    :goto_12e
    invoke-direct {v0}, Landroid/graphics/text/LineBreaker$ParagraphConstraints;-><init>()V

    goto/32 :goto_318

    nop

    :goto_12f
    invoke-static/range {p1 .. p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmEnd(Landroid/text/StaticLayout$Builder;)I

    move-result v10

    goto/32 :goto_1e4

    nop

    :goto_130
    goto/16 :goto_30f

    :goto_131
    goto/32 :goto_8a

    nop

    :goto_132
    const-class v14, Landroid/text/style/LeadingMarginSpan;

    goto/32 :goto_383

    nop

    :goto_133
    move/from16 v64, v2

    goto/32 :goto_4e

    nop

    :goto_134
    move/from16 v59, v12

    goto/32 :goto_302

    nop

    :goto_135
    const/4 v3, 0x0

    goto/32 :goto_2bf

    nop

    :goto_136
    move/from16 v25, v76

    goto/32 :goto_1a9

    nop

    :goto_137
    move-object/from16 v8, v21

    goto/32 :goto_1f7

    nop

    :goto_138
    invoke-direct/range {v0 .. v5}, Landroid/text/PrecomputedText$Params;-><init>(Landroid/text/TextPaint;Landroid/graphics/text/LineBreakConfig;Landroid/text/TextDirectionHeuristic;II)V

    goto/32 :goto_301

    nop

    :goto_139
    invoke-virtual {v14, v3}, Landroid/text/TextPaint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    goto/32 :goto_a8

    nop

    :goto_13a
    move/from16 v11, v32

    goto/32 :goto_52

    nop

    :goto_13b
    const/16 v22, 0x0

    goto/32 :goto_33a

    nop

    :goto_13c
    move-object/from16 v65, v18

    goto/32 :goto_a2

    nop

    :goto_13d
    move-object/from16 v56, v8

    goto/32 :goto_35f

    nop

    :goto_13e
    move v0, v5

    goto/32 :goto_1b4

    nop

    :goto_13f
    move-object v8, v1

    :goto_140
    goto/32 :goto_1dd

    nop

    :goto_141
    move-object/from16 v55, v7

    goto/32 :goto_e

    nop

    :goto_142
    move/from16 v60, v24

    goto/32 :goto_183

    nop

    :goto_143
    add-int/lit8 v4, v4, 0x1

    goto/32 :goto_16f

    nop

    :goto_144
    if-nez v8, :cond_d

    goto/32 :goto_2c

    :cond_d
    goto/32 :goto_2b

    nop

    :goto_145
    move-object/from16 v37, v8

    goto/32 :goto_9e

    nop

    :goto_146
    move-object/from16 v34, v1

    goto/32 :goto_177

    nop

    :goto_147
    move/from16 v33, v16

    goto/32 :goto_72

    nop

    :goto_148
    if-lt v0, v8, :cond_e

    goto/32 :goto_1c7

    :cond_e
    goto/32 :goto_343

    nop

    :goto_149
    move/from16 v56, v8

    goto/32 :goto_38c

    nop

    :goto_14a
    const/4 v12, 0x0

    goto/32 :goto_e1

    nop

    :goto_14b
    move/from16 v33, v70

    goto/32 :goto_39b

    nop

    :goto_14c
    move-object/from16 v31, v80

    goto/32 :goto_390

    nop

    :goto_14d
    const/high16 v0, 0x3f800000    # 1.0f

    goto/32 :goto_26a

    nop

    :goto_14e
    if-nez v34, :cond_f

    goto/32 :goto_111

    :cond_f
    goto/32 :goto_f5

    nop

    :goto_14f
    move/from16 v62, v4

    goto/32 :goto_1ff

    nop

    :goto_150
    move/from16 v63, v1

    goto/32 :goto_394

    nop

    :goto_151
    invoke-virtual {v2, v8, v1}, Landroid/graphics/text/LineBreaker$ParagraphConstraints;->setTabStops([FF)V

    nop

    goto/32 :goto_277

    nop

    :goto_152
    array-length v8, v3

    goto/32 :goto_199

    nop

    :goto_153
    invoke-static {v15, v5, v4, v2}, Landroid/text/StaticLayout;->getParagraphSpans(Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_1f5

    nop

    :goto_154
    move-object/from16 v27, v80

    goto/32 :goto_245

    nop

    :goto_155
    invoke-virtual {v2, v1}, Landroid/graphics/text/LineBreaker$ParagraphConstraints;->setWidth(F)V

    goto/32 :goto_2f4

    nop

    :goto_156
    invoke-virtual {v1}, Landroid/text/AutoGrowArray$IntArray;->getRawArray()[I

    move-result-object v40

    goto/32 :goto_5a

    nop

    :goto_157
    move v2, v4

    :goto_158
    goto/32 :goto_a1

    nop

    :goto_159
    aget v26, v43, v67

    goto/32 :goto_11c

    nop

    :goto_15a
    iget v0, v13, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    goto/32 :goto_198

    nop

    :goto_15b
    move-object/from16 v30, v56

    goto/32 :goto_295

    nop

    :goto_15c
    move-object/from16 v50, v3

    goto/32 :goto_63

    nop

    :goto_15d
    move-object/from16 v6, v19

    goto/32 :goto_2d8

    nop

    :goto_15e
    move/from16 v20, p3

    goto/32 :goto_39

    nop

    :goto_15f
    move-object/from16 v50, v5

    goto/32 :goto_1f4

    nop

    :goto_160
    move-object/from16 v41, v65

    goto/32 :goto_179

    nop

    :goto_161
    invoke-virtual {v0}, Landroid/graphics/text/LineBreaker$Builder;->build()Landroid/graphics/text/LineBreaker;

    move-result-object v6

    goto/32 :goto_127

    nop

    :goto_162
    const/4 v0, -0x1

    :goto_163
    goto/32 :goto_3b2

    nop

    :goto_164
    move-object/from16 v32, v1

    goto/32 :goto_304

    nop

    :goto_165
    mul-int/lit8 v0, v19, 0x4

    goto/32 :goto_34

    nop

    :goto_166
    if-gt v4, v2, :cond_10

    goto/32 :goto_285

    :cond_10
    goto/32 :goto_20

    nop

    :goto_167
    move/from16 v59, v12

    goto/32 :goto_35d

    nop

    :goto_168
    goto/16 :goto_99

    :goto_169
    goto/32 :goto_20c

    nop

    :goto_16a
    move-object/from16 v23, v53

    goto/32 :goto_373

    nop

    :goto_16b
    goto/16 :goto_99

    :goto_16c
    goto/32 :goto_19b

    nop

    :goto_16d
    const/4 v13, 0x0

    goto/32 :goto_27

    nop

    :goto_16e
    invoke-static {v7, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    goto/32 :goto_e4

    nop

    :goto_16f
    const/4 v7, 0x0

    goto/32 :goto_19c

    nop

    :goto_170
    const/4 v7, 0x0

    goto/32 :goto_35e

    nop

    :goto_171
    move-object/from16 v9, v50

    goto/32 :goto_286

    nop

    :goto_172
    move/from16 v69, v20

    goto/32 :goto_1b2

    nop

    :goto_173
    move-object/from16 v34, v1

    :goto_174
    goto/32 :goto_3a0

    nop

    :goto_175
    iget-boolean v8, v4, Landroid/text/StaticLayout;->mEllipsized:Z

    goto/32 :goto_144

    nop

    :goto_176
    move-object/from16 v26, v2

    goto/32 :goto_ed

    nop

    :goto_177
    const/4 v1, 0x1

    goto/32 :goto_2a7

    nop

    :goto_178
    const/4 v0, 0x0

    goto/32 :goto_18e

    nop

    :goto_179
    move/from16 v42, v70

    goto/32 :goto_a0

    nop

    :goto_17a
    move-object v3, v8

    goto/32 :goto_291

    nop

    :goto_17b
    move-object/from16 v45, v21

    goto/32 :goto_17c

    nop

    :goto_17c
    move-object/from16 v51, v22

    goto/32 :goto_6e

    nop

    :goto_17d
    move/from16 v27, v31

    goto/32 :goto_8

    nop

    :goto_17e
    mul-int/lit8 v0, v19, 0x4

    goto/32 :goto_119

    nop

    :goto_17f
    if-eqz v0, :cond_11

    goto/32 :goto_220

    :cond_11
    goto/32 :goto_187

    nop

    :goto_180
    move/from16 v67, v14

    goto/32 :goto_9

    nop

    :goto_181
    goto/16 :goto_37c

    :goto_182
    goto/32 :goto_2cd

    nop

    :goto_183
    move/from16 v1, v28

    goto/32 :goto_3b4

    nop

    :goto_184
    move-object v3, v9

    goto/32 :goto_75

    nop

    :goto_185
    const/4 v0, 0x0

    goto/32 :goto_2e0

    nop

    :goto_186
    sub-int/2addr v3, v11

    goto/32 :goto_275

    nop

    :goto_187
    iget-object v1, v13, Landroid/text/StaticLayout;->mRightIndents:[I

    goto/32 :goto_30

    nop

    :goto_188
    move/from16 v8, v60

    goto/32 :goto_10b

    nop

    :goto_189
    invoke-virtual {v2, v1, v12}, Landroid/graphics/text/LineBreaker$ParagraphConstraints;->setIndent(FI)V

    goto/32 :goto_2ea

    nop

    :goto_18a
    move-object/from16 v11, v79

    goto/32 :goto_2ef

    nop

    :goto_18b
    if-lt v2, v14, :cond_12

    goto/32 :goto_1ee

    :cond_12
    goto/32 :goto_351

    nop

    :goto_18c
    goto/16 :goto_323

    :goto_18d
    goto/32 :goto_3a2

    nop

    :goto_18e
    cmpl-float v0, v48, v0

    goto/32 :goto_231

    nop

    :goto_18f
    aput v6, v3, v4

    goto/32 :goto_276

    nop

    :goto_190
    move/from16 v32, v11

    goto/32 :goto_297

    nop

    :goto_191
    move-object/from16 v7, v44

    goto/32 :goto_d5

    nop

    :goto_192
    move/from16 v9, v47

    goto/32 :goto_e3

    nop

    :goto_193
    move-object/from16 v50, v5

    goto/32 :goto_392

    nop

    :goto_194
    invoke-virtual {v2}, Landroid/text/PrecomputedText;->getParagraphInfo()[Landroid/text/PrecomputedText$ParagraphInfo;

    move-result-object v28

    goto/32 :goto_213

    nop

    :goto_195
    aget v0, v40, v0

    goto/32 :goto_a9

    nop

    :goto_196
    array-length v14, v2

    goto/32 :goto_38b

    nop

    :goto_197
    invoke-direct/range {v17 .. v45}, Landroid/text/StaticLayout;->out(Ljava/lang/CharSequence;IIIIIIIFF[Landroid/text/style/LineHeightSpan;[ILandroid/graphics/Paint$FontMetricsInt;ZIZLandroid/text/MeasuredParagraph;IZZZ[CILandroid/text/TextUtils$TruncateAt;FFLandroid/text/TextPaint;Z)I

    move-result v9

    goto/32 :goto_29

    nop

    :goto_198
    iget v2, v13, Landroid/text/StaticLayout;->mLineCount:I

    goto/32 :goto_39d

    nop

    :goto_199
    const/4 v14, 0x0

    goto/32 :goto_388

    nop

    :goto_19a
    move/from16 v27, v9

    goto/32 :goto_171

    nop

    :goto_19b
    move-object/from16 v30, v8

    goto/32 :goto_33f

    nop

    :goto_19c
    goto/16 :goto_3cb

    :goto_19d
    goto/32 :goto_249

    nop

    :goto_19e
    goto/16 :goto_230

    :goto_19f
    goto/32 :goto_3e

    nop

    :goto_1a0
    invoke-virtual {v1, v0}, Landroid/graphics/text/LineBreaker$Result;->getLineWidth(I)F

    move-result v2

    goto/32 :goto_31d

    nop

    :goto_1a1
    if-nez v3, :cond_13

    goto/32 :goto_e5

    :cond_13
    goto/32 :goto_2b1

    nop

    :goto_1a2
    move/from16 v56, v8

    goto/32 :goto_2df

    nop

    :goto_1a3
    invoke-virtual {v1, v0}, Landroid/graphics/text/LineBreaker$Result;->getStartLineHyphenEdit(I)I

    move-result v2

    goto/32 :goto_1d0

    nop

    :goto_1a4
    goto/16 :goto_2ad

    :pswitch_0
    goto/32 :goto_70

    nop

    :goto_1a5
    if-lt v4, v1, :cond_14

    goto/32 :goto_19d

    :cond_14
    goto/32 :goto_1d6

    nop

    :goto_1a6
    if-gtz v3, :cond_15

    goto/32 :goto_b9

    :cond_15
    goto/32 :goto_36

    nop

    :goto_1a7
    move-object/from16 v66, v26

    goto/32 :goto_b7

    nop

    :goto_1a8
    move/from16 v32, v11

    goto/32 :goto_367

    nop

    :goto_1a9
    move/from16 v6, v77

    goto/32 :goto_4a

    nop

    :goto_1aa
    const/16 v21, 0x0

    goto/32 :goto_62

    nop

    :goto_1ab
    goto/16 :goto_2ad

    :pswitch_1
    goto/32 :goto_3a5

    nop

    :goto_1ac
    new-array v2, v0, [I

    goto/32 :goto_11b

    nop

    :goto_1ad
    const/16 v32, 0x0

    goto/32 :goto_32e

    nop

    :goto_1ae
    move/from16 v70, v33

    goto/32 :goto_395

    nop

    :goto_1af
    move/from16 v5, v62

    goto/32 :goto_26f

    nop

    :goto_1b0
    move-object/from16 v44, v20

    goto/32 :goto_f

    nop

    :goto_1b1
    new-array v3, v3, [F

    goto/32 :goto_3ad

    nop

    :goto_1b2
    move/from16 v68, v22

    goto/32 :goto_8d

    nop

    :goto_1b3
    move/from16 v2, v68

    goto/32 :goto_288

    nop

    :goto_1b4
    const/4 v1, 0x0

    goto/32 :goto_207

    nop

    :goto_1b5
    iget v9, v4, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    goto/32 :goto_296

    nop

    :goto_1b6
    move-object/from16 v36, v1

    goto/32 :goto_2dc

    nop

    :goto_1b7
    move-object/from16 v61, v3

    goto/32 :goto_236

    nop

    :goto_1b8
    const/4 v12, 0x1

    goto/32 :goto_9c

    nop

    :goto_1b9
    const/4 v2, 0x1

    goto/32 :goto_375

    nop

    :goto_1ba
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    goto/32 :goto_300

    nop

    :goto_1bb
    invoke-virtual {v1, v0}, Landroid/graphics/text/LineBreaker$Result;->getLineAscent(I)F

    move-result v2

    goto/32 :goto_dd

    nop

    :goto_1bc
    move/from16 v9, v17

    goto/32 :goto_11a

    nop

    :goto_1bd
    iput-boolean v0, v13, Landroid/text/StaticLayout;->mFallbackLineSpacing:Z

    goto/32 :goto_2b6

    nop

    :goto_1be
    move/from16 v73, v3

    goto/32 :goto_8c

    nop

    :goto_1bf
    move-object/from16 v7, v31

    goto/32 :goto_1f9

    nop

    :goto_1c0
    move-object/from16 v35, v85

    goto/32 :goto_324

    nop

    :goto_1c1
    move-object/from16 v9, v80

    goto/32 :goto_2ff

    nop

    :goto_1c2
    move/from16 v0, v74

    goto/32 :goto_78

    nop

    :goto_1c3
    if-lt v9, v11, :cond_16

    goto/32 :goto_2a6

    :cond_16
    goto/32 :goto_12

    nop

    :goto_1c4
    const/4 v0, 0x0

    :goto_1c5
    goto/32 :goto_14

    nop

    :goto_1c6
    goto/16 :goto_219

    :goto_1c7
    goto/32 :goto_315

    nop

    :goto_1c8
    move-object/from16 v65, v34

    goto/32 :goto_7f

    nop

    :goto_1c9
    aget v7, v7, v4

    goto/32 :goto_50

    nop

    :goto_1ca
    move/from16 v5, v75

    goto/32 :goto_f0

    nop

    :goto_1cb
    invoke-static/range {p1 .. p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmHyphenationFrequency(Landroid/text/StaticLayout$Builder;)I

    move-result v1

    goto/32 :goto_20e

    nop

    :goto_1cc
    move/from16 v82, v10

    goto/32 :goto_109

    nop

    :goto_1cd
    aput v14, v3, v8

    goto/32 :goto_c0

    nop

    :goto_1ce
    move-object/from16 v19, v43

    goto/32 :goto_3ac

    nop

    :goto_1cf
    move/from16 v62, v4

    goto/32 :goto_1a

    nop

    :goto_1d0
    move/from16 v25, v6

    goto/32 :goto_298

    nop

    :goto_1d1
    invoke-static/range {p1 .. p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmBreakStrategy(Landroid/text/StaticLayout$Builder;)I

    move-result v29

    goto/32 :goto_ea

    nop

    :goto_1d2
    move/from16 v18, v82

    goto/32 :goto_3bc

    nop

    :goto_1d3
    iget v7, v9, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    :goto_1d4
    goto/32 :goto_1d5

    nop

    :goto_1d5
    iget v0, v9, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    goto/32 :goto_335

    nop

    :goto_1d6
    aget v6, v3, v4

    goto/32 :goto_27c

    nop

    :goto_1d7
    goto/16 :goto_38f

    :goto_1d8
    goto/32 :goto_2f7

    nop

    :goto_1d9
    move-object/from16 v57, v12

    goto/32 :goto_348

    nop

    :goto_1da
    const/16 v28, 0x0

    goto/32 :goto_8f

    nop

    :goto_1db
    if-eq v11, v13, :cond_17

    goto/32 :goto_290

    :cond_17
    goto/32 :goto_28f

    nop

    :goto_1dc
    move/from16 v33, v0

    goto/32 :goto_271

    nop

    :goto_1dd
    aget-object v1, v7, v6

    goto/32 :goto_344

    nop

    :goto_1de
    move/from16 v76, v25

    goto/32 :goto_3a3

    nop

    :goto_1df
    move-object/from16 v51, v22

    goto/32 :goto_312

    nop

    :goto_1e0
    goto/16 :goto_28c

    :goto_1e1
    goto/32 :goto_222

    nop

    :goto_1e2
    if-lt v0, v6, :cond_18

    goto/32 :goto_36a

    :cond_18
    goto/32 :goto_3cd

    nop

    :goto_1e3
    move-object/from16 v30, v3

    goto/32 :goto_147

    nop

    :goto_1e4
    invoke-static/range {p1 .. p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmPaint(Landroid/text/StaticLayout$Builder;)Landroid/text/TextPaint;

    move-result-object v9

    goto/32 :goto_210

    nop

    :goto_1e5
    add-int/lit8 v8, v8, 0x1

    goto/32 :goto_40

    nop

    :goto_1e6
    move v0, v11

    goto/32 :goto_134

    nop

    :goto_1e7
    const/16 v23, 0x0

    goto/32 :goto_380

    nop

    :goto_1e8
    move/from16 v32, v59

    goto/32 :goto_1dc

    nop

    :goto_1e9
    move v4, v0

    :goto_1ea
    nop

    goto/32 :goto_311

    nop

    :goto_1eb
    iget-boolean v0, v13, Landroid/text/StaticLayout;->mFallbackLineSpacing:Z

    goto/32 :goto_24d

    nop

    :goto_1ec
    invoke-virtual {v3}, Landroid/text/MeasuredParagraph;->getSpanEndCache()Landroid/text/AutoGrowArray$IntArray;

    move-result-object v1

    goto/32 :goto_2fa

    nop

    :goto_1ed
    goto/16 :goto_3c6

    :goto_1ee
    goto/32 :goto_2c8

    nop

    :goto_1ef
    move v3, v7

    :goto_1f0
    nop

    goto/32 :goto_1cf

    nop

    :goto_1f1
    move/from16 v78, v7

    goto/32 :goto_243

    nop

    :goto_1f2
    invoke-virtual {v1, v2}, Landroid/text/PrecomputedText$Params$Builder;->setHyphenationFrequency(I)Landroid/text/PrecomputedText$Params$Builder;

    move-result-object v1

    goto/32 :goto_ab

    nop

    :goto_1f3
    instance-of v0, v12, Landroid/text/PrecomputedText;

    goto/32 :goto_2a8

    nop

    :goto_1f4
    move/from16 v5, v29

    goto/32 :goto_272

    nop

    :goto_1f5
    check-cast v2, [Landroid/text/style/TabStopSpan;

    goto/32 :goto_306

    nop

    :goto_1f6
    check-cast v8, [Landroid/text/style/LineHeightSpan;

    goto/32 :goto_35

    nop

    :goto_1f7
    move-object/from16 v51, v22

    goto/32 :goto_23e

    nop

    :goto_1f8
    move v5, v10

    goto/32 :goto_283

    nop

    :goto_1f9
    invoke-virtual/range {v0 .. v7}, Landroid/text/PrecomputedText;->checkResultUsable(IILandroid/text/TextDirectionHeuristic;Landroid/text/TextPaint;IILandroid/graphics/text/LineBreakConfig;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/32 :goto_32

    nop

    :goto_1fa
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_94

    nop

    :goto_1fb
    const/16 v25, 0x0

    goto/32 :goto_df

    nop

    :goto_1fc
    move-object/from16 v53, v2

    goto/32 :goto_2c2

    nop

    :goto_1fd
    move-object/from16 v20, v2

    goto/32 :goto_3d7

    nop

    :goto_1fe
    move-object/from16 v4, p0

    goto/32 :goto_34d

    nop

    :goto_1ff
    move/from16 v4, v73

    goto/32 :goto_29e

    nop

    :goto_200
    move-object v4, v13

    goto/32 :goto_37f

    nop

    :goto_201
    move-object/from16 v18, v42

    goto/32 :goto_1ce

    nop

    :goto_202
    move/from16 v9, v24

    :goto_203
    goto/32 :goto_7e

    nop

    :goto_204
    iget v0, v13, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    goto/32 :goto_121

    nop

    :goto_205
    const/16 v28, 0x0

    goto/32 :goto_262

    nop

    :goto_206
    array-length v9, v8

    goto/32 :goto_30c

    nop

    :goto_207
    const/4 v2, 0x0

    goto/32 :goto_170

    nop

    :goto_208
    move/from16 v23, v15

    goto/32 :goto_81

    nop

    :goto_209
    goto/16 :goto_347

    :goto_20a
    goto/32 :goto_346

    nop

    :goto_20b
    move v11, v3

    goto/32 :goto_1bc

    nop

    :goto_20c
    if-nez v1, :cond_19

    goto/32 :goto_22d

    :cond_19
    goto/32 :goto_7a

    nop

    :goto_20d
    move/from16 v31, v27

    goto/32 :goto_84

    nop

    :goto_20e
    invoke-static {v1}, Landroid/text/StaticLayout;->getBaseHyphenationFrequency(I)I

    move-result v1

    goto/32 :goto_bb

    nop

    :goto_20f
    move-object/from16 v34, v14

    goto/32 :goto_328

    nop

    :goto_210
    invoke-static/range {p1 .. p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmWidth(Landroid/text/StaticLayout$Builder;)I

    move-result v46

    goto/32 :goto_6

    nop

    :goto_211
    move/from16 v0, v84

    goto/32 :goto_25f

    nop

    :goto_212
    if-lt v6, v2, :cond_1a

    goto/32 :goto_1d8

    :cond_1a
    goto/32 :goto_55

    nop

    :goto_213
    goto/16 :goto_2ad

    :pswitch_2
    goto/32 :goto_23b

    nop

    :goto_214
    move/from16 v27, v48

    goto/32 :goto_1e3

    nop

    :goto_215
    add-int/lit8 v14, v67, 0x1

    goto/32 :goto_1fe

    nop

    :goto_216
    move/from16 v28, v22

    :goto_217
    goto/32 :goto_ae

    nop

    :goto_218
    move/from16 v0, v21

    :goto_219
    goto/32 :goto_148

    nop

    :goto_21a
    aget-object v2, v7, v2

    goto/32 :goto_30e

    nop

    :goto_21b
    move-object/from16 v22, v51

    goto/32 :goto_16a

    nop

    :goto_21c
    invoke-virtual {v3, v7}, Landroid/text/MeasuredParagraph;->getCharWidthAt(I)F

    move-result v8

    goto/32 :goto_35a

    nop

    :goto_21d
    invoke-static/range {p1 .. p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmStart(Landroid/text/StaticLayout$Builder;)I

    move-result v11

    goto/32 :goto_12f

    nop

    :goto_21e
    if-lt v0, v14, :cond_1b

    goto/32 :goto_6f

    :cond_1b
    goto/32 :goto_3d5

    nop

    :goto_21f
    goto/16 :goto_24a

    :goto_220
    goto/32 :goto_340

    nop

    :goto_221
    new-array v2, v0, [F

    goto/32 :goto_27f

    nop

    :goto_222
    move/from16 v25, v6

    goto/32 :goto_15a

    nop

    :goto_223
    move-object/from16 v35, v12

    goto/32 :goto_1b8

    nop

    :goto_224
    move-object/from16 v2, v18

    goto/32 :goto_15d

    nop

    :goto_225
    if-eqz v9, :cond_1c

    goto/32 :goto_169

    :cond_1c
    goto/32 :goto_d8

    nop

    :goto_226
    aget v3, v42, v14

    goto/32 :goto_fd

    nop

    :goto_227
    goto/16 :goto_174

    :goto_228
    goto/32 :goto_173

    nop

    :goto_229
    move-object/from16 v57, v51

    goto/32 :goto_224

    nop

    :goto_22a
    if-lt v0, v14, :cond_1d

    goto/32 :goto_1e1

    :cond_1d
    goto/32 :goto_3ce

    nop

    :goto_22b
    invoke-static/range {p1 .. p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmBreakStrategy(Landroid/text/StaticLayout$Builder;)I

    move-result v4

    goto/32 :goto_31b

    nop

    :goto_22c
    if-lt v9, v11, :cond_1e

    goto/32 :goto_30d

    :cond_1e
    :goto_22d
    goto/32 :goto_206

    nop

    :goto_22e
    move-object/from16 v31, v9

    goto/32 :goto_379

    nop

    :goto_22f
    goto/16 :goto_4d

    :goto_230
    goto/32 :goto_274

    nop

    :goto_231
    if-nez v0, :cond_1f

    goto/32 :goto_18d

    :cond_1f
    goto/32 :goto_18c

    nop

    :goto_232
    aget v7, v43, v2

    goto/32 :goto_260

    nop

    :goto_233
    goto/16 :goto_4d

    :goto_234
    goto/32 :goto_ba

    nop

    :goto_235
    new-instance v0, Landroid/graphics/text/LineBreaker$Builder;

    goto/32 :goto_f7

    nop

    :goto_236
    add-int v3, v5, v19

    goto/32 :goto_24f

    nop

    :goto_237
    move-object/from16 v2, v42

    goto/32 :goto_74

    nop

    :goto_238
    move/from16 v11, v32

    goto/32 :goto_32b

    nop

    :goto_239
    invoke-virtual {v1, v0}, Landroid/graphics/text/LineBreaker$Result;->getLineDescent(I)F

    move-result v2

    goto/32 :goto_2fe

    nop

    :goto_23a
    invoke-static/range {p1 .. p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmJustificationMode(Landroid/text/StaticLayout$Builder;)I

    move-result v1

    goto/32 :goto_125

    nop

    :goto_23b
    move-object/from16 v2, v32

    goto/32 :goto_266

    nop

    :goto_23c
    add-int/lit8 v6, v76, 0x1

    goto/32 :goto_15c

    nop

    :goto_23d
    move/from16 v28, v20

    goto/32 :goto_c

    nop

    :goto_23e
    move-object/from16 v53, v23

    goto/32 :goto_202

    nop

    :goto_23f
    move-object v3, v8

    goto/32 :goto_138

    nop

    :goto_240
    move-object v14, v4

    goto/32 :goto_0

    nop

    :goto_241
    iget-object v1, v13, Landroid/text/StaticLayout;->mRightIndents:[I

    goto/32 :goto_64

    nop

    :goto_242
    move-object/from16 v65, v18

    goto/32 :goto_172

    nop

    :goto_243
    move-object v3, v9

    goto/32 :goto_45

    nop

    :goto_244
    move-object/from16 v35, v12

    goto/32 :goto_18

    nop

    :goto_245
    invoke-direct/range {v0 .. v28}, Landroid/text/StaticLayout;->out(Ljava/lang/CharSequence;IIIIIIIFF[Landroid/text/style/LineHeightSpan;[ILandroid/graphics/Paint$FontMetricsInt;ZIZLandroid/text/MeasuredParagraph;IZZZ[CILandroid/text/TextUtils$TruncateAt;FFLandroid/text/TextPaint;Z)I

    move-result v60

    goto/32 :goto_282

    nop

    :goto_246
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto/32 :goto_3a8

    nop

    :goto_247
    move-object/from16 v85, v35

    goto/32 :goto_2da

    nop

    :goto_248
    move v7, v4

    goto/32 :goto_39a

    nop

    :goto_249
    move-object v7, v3

    :goto_24a
    goto/32 :goto_235

    nop

    :goto_24b
    move-object/from16 v36, v1

    goto/32 :goto_2d1

    nop

    :goto_24c
    move/from16 v0, v17

    goto/32 :goto_38e

    nop

    :goto_24d
    if-nez v0, :cond_20

    goto/32 :goto_4f

    :cond_20
    goto/32 :goto_384

    nop

    :goto_24e
    move-object/from16 v18, v2

    goto/32 :goto_3c5

    nop

    :goto_24f
    if-le v3, v11, :cond_21

    goto/32 :goto_182

    :cond_21
    goto/32 :goto_226

    nop

    :goto_250
    move-object/from16 v1, v36

    goto/32 :goto_3a4

    nop

    :goto_251
    move-object/from16 v79, v30

    goto/32 :goto_3b0

    nop

    :goto_252
    goto/16 :goto_c2

    :goto_253
    goto/32 :goto_54

    nop

    :goto_254
    invoke-virtual {v1}, Landroid/text/PrecomputedText$Params$Builder;->build()Landroid/text/PrecomputedText$Params;

    move-result-object v1

    goto/32 :goto_89

    nop

    :goto_255
    move-object/from16 v18, v65

    goto/32 :goto_cf

    nop

    :goto_256
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_195

    nop

    :goto_257
    iget v14, v13, Landroid/text/StaticLayout;->mLineCount:I

    goto/32 :goto_37

    nop

    :goto_258
    const/16 v18, 0x0

    goto/32 :goto_100

    nop

    :goto_259
    array-length v11, v8

    goto/32 :goto_22c

    nop

    :goto_25a
    move-object/from16 v66, v26

    goto/32 :goto_308

    nop

    :goto_25b
    iput v0, v9, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    goto/32 :goto_d4

    nop

    :goto_25c
    iget v1, v3, Landroid/graphics/Paint$FontMetricsInt;->top:I

    goto/32 :goto_36c

    nop

    :goto_25d
    move-object/from16 v13, p0

    goto/32 :goto_381

    nop

    :goto_25e
    move-object v15, v2

    goto/32 :goto_ff

    nop

    :goto_25f
    if-lt v5, v0, :cond_22

    goto/32 :goto_36e

    :cond_22
    goto/32 :goto_36f

    nop

    :goto_260
    add-float/2addr v0, v7

    goto/32 :goto_393

    nop

    :goto_261
    move/from16 v21, v67

    goto/32 :goto_263

    nop

    :goto_262
    instance-of v0, v12, Landroid/text/Spanned;

    goto/32 :goto_135

    nop

    :goto_263
    move/from16 v33, v70

    goto/32 :goto_376

    nop

    :goto_264
    move/from16 v4, v74

    goto/32 :goto_1ca

    nop

    :goto_265
    const/4 v0, 0x0

    goto/32 :goto_13

    nop

    :goto_266
    goto/16 :goto_2ad

    :goto_267
    goto/32 :goto_2d0

    nop

    :goto_268
    move/from16 v41, v0

    goto/32 :goto_1fc

    nop

    :goto_269
    move/from16 v32, v59

    goto/32 :goto_27a

    nop

    :goto_26a
    cmpl-float v0, v47, v0

    goto/32 :goto_2bc

    nop

    :goto_26b
    aput v12, v1, v9

    goto/32 :goto_354

    nop

    :goto_26c
    move/from16 v86, v18

    goto/32 :goto_24e

    nop

    :goto_26d
    aput v7, v42, v2

    goto/32 :goto_3cf

    nop

    :goto_26e
    move/from16 v14, v68

    goto/32 :goto_294

    nop

    :goto_26f
    move/from16 v77, v6

    goto/32 :goto_cd

    nop

    :goto_270
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    goto/32 :goto_329

    nop

    :goto_271
    move/from16 v56, v8

    goto/32 :goto_1e6

    nop

    :goto_272
    move-object/from16 v51, v6

    goto/32 :goto_27d

    nop

    :goto_273
    move v14, v6

    goto/32 :goto_46

    nop

    :goto_274
    aget-boolean v7, v51, v2

    goto/32 :goto_2c1

    nop

    :goto_275
    aget-object v11, v14, v8

    goto/32 :goto_14a

    nop

    :goto_276
    add-int/lit8 v4, v4, 0x1

    goto/32 :goto_44

    nop

    :goto_277
    invoke-virtual {v3}, Landroid/text/MeasuredParagraph;->getMeasuredText()Landroid/graphics/text/MeasuredText;

    move-result-object v1

    goto/32 :goto_257

    nop

    :goto_278
    move/from16 v0, v21

    goto/32 :goto_397

    nop

    :goto_279
    if-gt v0, v2, :cond_23

    goto/32 :goto_398

    :cond_23
    goto/32 :goto_c9

    nop

    :goto_27a
    move/from16 v33, v0

    goto/32 :goto_149

    nop

    :goto_27b
    move-object v13, v4

    goto/32 :goto_b3

    nop

    :goto_27c
    iget-object v7, v13, Landroid/text/StaticLayout;->mRightIndents:[I

    goto/32 :goto_1c9

    nop

    :goto_27d
    move-object/from16 v26, v14

    goto/32 :goto_29f

    nop

    :goto_27e
    iget v6, v3, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    goto/32 :goto_248

    nop

    :goto_27f
    move-object/from16 v19, v2

    goto/32 :goto_29a

    nop

    :goto_280
    invoke-static/range {p1 .. p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmSpacingAdd(Landroid/text/StaticLayout$Builder;)F

    move-result v48

    goto/32 :goto_120

    nop

    :goto_281
    move/from16 v17, v46

    goto/32 :goto_2c5

    nop

    :goto_282
    move/from16 v5, v72

    goto/32 :goto_211

    nop

    :goto_283
    move/from16 v83, v11

    goto/32 :goto_2f0

    nop

    :goto_284
    goto/16 :goto_f2

    :goto_285
    goto/32 :goto_2a9

    nop

    :goto_286
    iput v0, v9, Landroid/graphics/Paint$FontMetricsInt;->top:I

    goto/32 :goto_165

    nop

    :goto_287
    move v12, v2

    goto/32 :goto_38

    nop

    :goto_288
    move/from16 v4, v68

    goto/32 :goto_49

    nop

    :goto_289
    invoke-interface {v12, v11}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v11

    goto/32 :goto_6d

    nop

    :goto_28a
    move/from16 v17, v35

    goto/32 :goto_5b

    nop

    :goto_28b
    const/4 v0, 0x0

    :goto_28c
    goto/32 :goto_22a

    nop

    :goto_28d
    move-object/from16 v71, v15

    goto/32 :goto_353

    nop

    :goto_28e
    move-object/from16 v30, v56

    goto/32 :goto_269

    nop

    :goto_28f
    goto/16 :goto_332

    :goto_290
    goto/32 :goto_18a

    nop

    :goto_291
    move-object/from16 v34, v14

    goto/32 :goto_240

    nop

    :goto_292
    move-object/from16 v30, v56

    goto/32 :goto_2e9

    nop

    :goto_293
    add-int/lit8 v2, v6, -0x1

    goto/32 :goto_ef

    nop

    :goto_294
    move/from16 v33, v70

    goto/32 :goto_48

    nop

    :goto_295
    const/16 v68, 0x0

    goto/32 :goto_389

    nop

    :goto_296
    if-ge v8, v9, :cond_24

    goto/32 :goto_2c

    :cond_24
    goto/32 :goto_175

    nop

    :goto_297
    move/from16 v35, v17

    goto/32 :goto_1c2

    nop

    :goto_298
    invoke-virtual {v1, v0}, Landroid/graphics/text/LineBreaker$Result;->getEndLineHyphenEdit(I)I

    move-result v6

    goto/32 :goto_325

    nop

    :goto_299
    move/from16 v75, v5

    goto/32 :goto_33b

    nop

    :goto_29a
    new-array v2, v0, [F

    goto/32 :goto_1fd

    nop

    :goto_29b
    iget v11, v4, Landroid/text/StaticLayout;->mLineCount:I

    goto/32 :goto_6c

    nop

    :goto_29c
    move/from16 v1, v68

    goto/32 :goto_1b3

    nop

    :goto_29d
    if-lt v12, v4, :cond_25

    goto/32 :goto_34c

    :cond_25
    goto/32 :goto_371

    nop

    :goto_29e
    move/from16 v75, v5

    goto/32 :goto_1af

    nop

    :goto_29f
    const/4 v14, 0x1

    goto/32 :goto_360

    nop

    :goto_2a0
    move-object v2, v0

    goto/32 :goto_1f3

    nop

    :goto_2a1
    move-object v0, v3

    :goto_2a2
    goto/32 :goto_2a0

    nop

    :goto_2a3
    move-object/from16 v65, v18

    goto/32 :goto_2f9

    nop

    :goto_2a4
    invoke-virtual {v1}, Landroid/graphics/text/LineBreaker$Result;->getLineCount()I

    move-result v14

    goto/32 :goto_21e

    nop

    :goto_2a5
    goto/16 :goto_91

    :goto_2a6
    goto/32 :goto_24b

    nop

    :goto_2a7
    if-eq v0, v1, :cond_26

    goto/32 :goto_83

    :cond_26
    goto/32 :goto_270

    nop

    :goto_2a8
    if-nez v0, :cond_27

    goto/32 :goto_267

    :cond_27
    goto/32 :goto_9b

    nop

    :goto_2a9
    move/from16 v63, v1

    goto/32 :goto_133

    nop

    :goto_2aa
    invoke-interface {v15, v11}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v11

    goto/32 :goto_310

    nop

    :goto_2ab
    move/from16 v69, v20

    goto/32 :goto_95

    nop

    :goto_2ac
    move-object v15, v2

    :goto_2ad
    goto/32 :goto_362

    nop

    :goto_2ae
    move/from16 v74, v4

    goto/32 :goto_299

    nop

    :goto_2af
    move/from16 v2, v50

    goto/32 :goto_1be

    nop

    :goto_2b0
    instance-of v11, v9, Landroid/text/style/LeadingMarginSpan$LeadingMarginSpan2;

    goto/32 :goto_2dd

    nop

    :goto_2b1
    aget v3, v44, v14

    goto/32 :goto_6b

    nop

    :goto_2b2
    new-instance v6, Landroid/text/PrecomputedText$Params;

    goto/32 :goto_5f

    nop

    :goto_2b3
    move-object/from16 v17, v61

    goto/32 :goto_1d2

    nop

    :goto_2b4
    check-cast v14, [Landroid/text/style/LeadingMarginSpan;

    goto/32 :goto_a3

    nop

    :goto_2b5
    move-object/from16 v80, v9

    goto/32 :goto_1f8

    nop

    :goto_2b6
    const/16 v24, 0x0

    goto/32 :goto_14d

    nop

    :goto_2b7
    move-object/from16 v1, v35

    goto/32 :goto_2af

    nop

    :goto_2b8
    aput-boolean v2, v51, v0

    nop

    goto/32 :goto_1a3

    nop

    :goto_2b9
    move/from16 v75, v5

    goto/32 :goto_eb

    nop

    :goto_2ba
    move-object/from16 v22, v2

    goto/32 :goto_3a6

    nop

    :goto_2bb
    int-to-float v15, v0

    goto/32 :goto_38a

    nop

    :goto_2bc
    if-eqz v0, :cond_28

    goto/32 :goto_323

    :cond_28
    goto/32 :goto_178

    nop

    :goto_2bd
    iput v7, v13, Landroid/text/StaticLayout;->mLineCount:I

    goto/32 :goto_2de

    nop

    :goto_2be
    move-object/from16 v66, v26

    goto/32 :goto_251

    nop

    :goto_2bf
    if-nez v0, :cond_29

    goto/32 :goto_35c

    :cond_29
    goto/32 :goto_115

    nop

    :goto_2c0
    move-object v9, v3

    goto/32 :goto_27b

    nop

    :goto_2c1
    or-int/2addr v1, v7

    goto/32 :goto_97

    nop

    :goto_2c2
    move-object/from16 v42, v18

    goto/32 :goto_368

    nop

    :goto_2c3
    move v1, v7

    goto/32 :goto_c4

    nop

    :goto_2c4
    add-int/lit8 v2, v6, -0x1

    goto/32 :goto_7

    nop

    :goto_2c5
    const/16 v25, 0x0

    goto/32 :goto_307

    nop

    :goto_2c6
    move-object/from16 v3, v50

    goto/32 :goto_15b

    nop

    :goto_2c7
    if-lt v14, v8, :cond_2a

    goto/32 :goto_85

    :cond_2a
    goto/32 :goto_386

    nop

    :goto_2c8
    move-object/from16 v55, v7

    goto/32 :goto_2ec

    nop

    :goto_2c9
    move v0, v7

    goto/32 :goto_356

    nop

    :goto_2ca
    invoke-virtual {v1, v0}, Landroid/graphics/text/LineBreaker$Result;->hasLineTab(I)Z

    move-result v2

    goto/32 :goto_2b8

    nop

    :goto_2cb
    move-object/from16 v80, v31

    goto/32 :goto_3bb

    nop

    :goto_2cc
    if-lt v7, v8, :cond_2b

    goto/32 :goto_230

    :cond_2b
    goto/32 :goto_21c

    nop

    :goto_2cd
    move/from16 v74, v4

    goto/32 :goto_3be

    nop

    :goto_2ce
    array-length v11, v8

    goto/32 :goto_1c3

    nop

    :goto_2cf
    move-object/from16 v26, v66

    goto/32 :goto_26e

    nop

    :goto_2d0
    move-object/from16 v26, v4

    goto/32 :goto_193

    nop

    :goto_2d1
    move v12, v2

    goto/32 :goto_19

    nop

    :goto_2d2
    mul-int/lit8 v20, v19, 0x4

    goto/32 :goto_13b

    nop

    :goto_2d3
    move/from16 v8, v28

    :goto_2d4
    goto/32 :goto_22e

    nop

    :goto_2d5
    move/from16 v83, v32

    goto/32 :goto_d0

    nop

    :goto_2d6
    move-object/from16 v7, v28

    goto/32 :goto_209

    nop

    :goto_2d7
    move/from16 v10, v82

    goto/32 :goto_1c0

    nop

    :goto_2d8
    move-object/from16 v7, v20

    goto/32 :goto_137

    nop

    :goto_2d9
    move-object/from16 v24, v65

    goto/32 :goto_bf

    nop

    :goto_2da
    move-object/from16 v30, v56

    goto/32 :goto_1e8

    nop

    :goto_2db
    aget-object v9, v14, v8

    goto/32 :goto_305

    nop

    :goto_2dc
    move v12, v2

    goto/32 :goto_20b

    nop

    :goto_2dd
    if-nez v11, :cond_2c

    goto/32 :goto_67

    :cond_2c
    goto/32 :goto_b4

    nop

    :goto_2de
    iput-boolean v7, v13, Landroid/text/StaticLayout;->mEllipsized:Z

    goto/32 :goto_204

    nop

    :goto_2df
    move/from16 v31, v9

    goto/32 :goto_190

    nop

    :goto_2e0
    move v6, v0

    goto/32 :goto_24c

    nop

    :goto_2e1
    if-ne v2, v0, :cond_2d

    goto/32 :goto_228

    :cond_2d
    goto/32 :goto_372

    nop

    :goto_2e2
    move/from16 v24, v15

    goto/32 :goto_1da

    nop

    :goto_2e3
    const/4 v14, 0x0

    goto/32 :goto_2ac

    nop

    :goto_2e4
    move/from16 v31, v27

    goto/32 :goto_192

    nop

    :goto_2e5
    check-cast v1, Landroid/text/PrecomputedText;

    goto/32 :goto_1d1

    nop

    :goto_2e6
    move/from16 v19, v29

    goto/32 :goto_2ee

    nop

    :goto_2e7
    move/from16 v32, v11

    goto/32 :goto_244

    nop

    :goto_2e8
    if-eq v2, v7, :cond_2e

    goto/32 :goto_19f

    :cond_2e
    goto/32 :goto_232

    nop

    :goto_2e9
    move/from16 v56, v8

    goto/32 :goto_188

    nop

    :goto_2ea
    const/high16 v1, 0x41a00000    # 20.0f

    goto/32 :goto_151

    nop

    :goto_2eb
    aget v7, v42, v7

    goto/32 :goto_26d

    nop

    :goto_2ec
    move-object/from16 v56, v8

    goto/32 :goto_2c4

    nop

    :goto_2ed
    move/from16 v23, v75

    goto/32 :goto_2d9

    nop

    :goto_2ee
    move/from16 v9, v31

    goto/32 :goto_238

    nop

    :goto_2ef
    move-object/from16 v14, v80

    goto/32 :goto_a

    nop

    :goto_2f0
    move-object/from16 v85, v12

    goto/32 :goto_ec

    nop

    :goto_2f1
    array-length v1, v1

    :goto_2f2
    goto/32 :goto_246

    nop

    :goto_2f3
    check-cast v11, Landroid/text/style/LeadingMarginSpan$LeadingMarginSpan2;

    nop

    goto/32 :goto_69

    nop

    :goto_2f4
    int-to-float v1, v11

    goto/32 :goto_189

    nop

    :goto_2f5
    move-object/from16 v14, v80

    :goto_2f6
    goto/32 :goto_92

    nop

    :goto_2f7
    move/from16 v76, v6

    goto/32 :goto_141

    nop

    :goto_2f8
    aget-object v11, v14, v8

    goto/32 :goto_223

    nop

    :goto_2f9
    move/from16 v69, v20

    goto/32 :goto_fc

    nop

    :goto_2fa
    invoke-virtual {v1}, Landroid/text/AutoGrowArray$IntArray;->getRawArray()[I

    move-result-object v39

    goto/32 :goto_377

    nop

    :goto_2fb
    move-object/from16 v14, v80

    goto/32 :goto_139

    nop

    :goto_2fc
    move-object/from16 v2, v26

    goto/32 :goto_155

    nop

    :goto_2fd
    move-object/from16 v12, v51

    goto/32 :goto_3aa

    nop

    :goto_2fe
    aput v2, v45, v0

    goto/32 :goto_2ca

    nop

    :goto_2ff
    move/from16 v11, v83

    goto/32 :goto_34e

    nop

    :goto_300
    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    goto/32 :goto_c7

    nop

    :goto_301
    invoke-static {v12, v0, v11, v10, v14}, Landroid/text/PrecomputedText;->createMeasuredParagraphs(Ljava/lang/CharSequence;Landroid/text/PrecomputedText$Params;IIZ)[Landroid/text/PrecomputedText$ParagraphInfo;

    move-result-object v28

    goto/32 :goto_2d6

    nop

    :goto_302
    move/from16 v35, v17

    goto/32 :goto_16

    nop

    :goto_303
    iget v15, v3, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    goto/32 :goto_d3

    nop

    :goto_304
    move v1, v11

    goto/32 :goto_3ba

    nop

    :goto_305
    move/from16 v32, v11

    goto/32 :goto_2f8

    nop

    :goto_306
    array-length v3, v2

    goto/32 :goto_1a6

    nop

    :goto_307
    if-nez v15, :cond_2f

    goto/32 :goto_16c

    :cond_2f
    goto/32 :goto_132

    nop

    :goto_308
    move/from16 v70, v33

    goto/32 :goto_1c8

    nop

    :goto_309
    invoke-virtual {v1, v2}, Landroid/text/PrecomputedText$Params$Builder;->setBreakStrategy(I)Landroid/text/PrecomputedText$Params$Builder;

    move-result-object v1

    goto/32 :goto_65

    nop

    :goto_30a
    const/16 v20, 0x0

    goto/32 :goto_de

    nop

    :goto_30b
    aget v0, v40, v0

    goto/32 :goto_d7

    nop

    :goto_30c
    invoke-static {v9}, Lcom/android/internal/util/ArrayUtils;->newUnpaddedIntArray(I)[I

    move-result-object v1

    :goto_30d
    goto/32 :goto_90

    nop

    :goto_30e
    iget v2, v2, Landroid/text/PrecomputedText$ParagraphInfo;->paragraphEnd:I

    :goto_30f
    goto/32 :goto_ee

    nop

    :goto_310
    if-lt v11, v5, :cond_30

    goto/32 :goto_355

    :cond_30
    goto/32 :goto_336

    nop

    :goto_311
    move/from16 v21, v0

    goto/32 :goto_1eb

    nop

    :goto_312
    move-object/from16 v53, v23

    :goto_313
    goto/32 :goto_28b

    nop

    :goto_314
    if-lt v1, v12, :cond_31

    goto/32 :goto_316

    :cond_31
    goto/32 :goto_341

    nop

    :goto_315
    move/from16 v28, v1

    :goto_316
    goto/32 :goto_53

    nop

    :goto_317
    move/from16 v12, v59

    goto/32 :goto_319

    nop

    :goto_318
    move-object v4, v0

    goto/32 :goto_205

    nop

    :goto_319
    move-object/from16 v3, v61

    goto/32 :goto_3af

    nop

    :goto_31a
    invoke-static/range {p1 .. p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmAddLastLineLineSpacing(Landroid/text/StaticLayout$Builder;)Z

    move-result v49

    goto/32 :goto_129

    nop

    :goto_31b
    invoke-static/range {p1 .. p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmHyphenationFrequency(Landroid/text/StaticLayout$Builder;)I

    move-result v5

    goto/32 :goto_3bf

    nop

    :goto_31c
    invoke-static/range {p1 .. p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmFontMetricsInt(Landroid/text/StaticLayout$Builder;)Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v5

    goto/32 :goto_1fb

    nop

    :goto_31d
    aput v2, v43, v0

    goto/32 :goto_1bb

    nop

    :goto_31e
    move-object v1, v3

    goto/32 :goto_3d8

    nop

    :goto_31f
    move-object/from16 v8, v56

    goto/32 :goto_1ed

    nop

    :goto_320
    move-object v4, v13

    goto/32 :goto_180

    nop

    :goto_321
    move-object/from16 v7, v55

    goto/32 :goto_31f

    nop

    :goto_322
    goto/16 :goto_104

    :goto_323
    goto/32 :goto_103

    nop

    :goto_324
    move-object/from16 v56, v30

    goto/32 :goto_76

    nop

    :goto_325
    invoke-static {v2, v6}, Landroid/text/StaticLayout;->packHyphenEdit(II)I

    move-result v2

    goto/32 :goto_3d1

    nop

    :goto_326
    move-object/from16 v26, v66

    goto/32 :goto_327

    nop

    :goto_327
    move/from16 v22, v68

    goto/32 :goto_39f

    nop

    :goto_328
    move/from16 v33, v15

    goto/32 :goto_2e3

    nop

    :goto_329
    if-ne v2, v0, :cond_32

    goto/32 :goto_83

    :cond_32
    goto/32 :goto_227

    nop

    :goto_32a
    iget v4, v3, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    goto/32 :goto_27e

    nop

    :goto_32b
    move/from16 v14, v33

    goto/32 :goto_28a

    nop

    :goto_32c
    move/from16 v1, v86

    goto/32 :goto_36d

    nop

    :goto_32d
    move/from16 v84, v11

    goto/32 :goto_32f

    nop

    :goto_32e
    const/16 v39, 0x0

    goto/32 :goto_107

    nop

    :goto_32f
    move/from16 v83, v32

    goto/32 :goto_106

    nop

    :goto_330
    if-nez v15, :cond_33

    goto/32 :goto_b9

    :cond_33
    goto/32 :goto_3c8

    nop

    :goto_331
    move-object/from16 v12, v85

    :goto_332
    goto/32 :goto_29b

    nop

    :goto_333
    move-object/from16 v11, v79

    goto/32 :goto_16d

    nop

    :goto_334
    add-int/lit8 v0, v0, 0x3

    goto/32 :goto_30b

    nop

    :goto_335
    if-gt v0, v14, :cond_34

    goto/32 :goto_61

    :cond_34
    goto/32 :goto_60

    nop

    :goto_336
    invoke-virtual {v13, v11}, Landroid/text/StaticLayout;->getLineForOffset(I)I

    move-result v12

    goto/32 :goto_10d

    nop

    :goto_337
    iget v1, v9, Landroid/graphics/Paint$FontMetricsInt;->top:I

    :goto_338
    goto/32 :goto_68

    nop

    :goto_339
    move-object v0, v1

    goto/32 :goto_164

    nop

    :goto_33a
    add-int/lit8 v20, v20, 0x0

    goto/32 :goto_c6

    nop

    :goto_33b
    move/from16 v77, v6

    goto/32 :goto_1cc

    nop

    :goto_33c
    if-lt v6, v14, :cond_35

    goto/32 :goto_253

    :cond_35
    goto/32 :goto_5e

    nop

    :goto_33d
    goto/16 :goto_3ae

    :goto_33e
    goto/32 :goto_152

    nop

    :goto_33f
    move-object/from16 v31, v9

    goto/32 :goto_1a8

    nop

    :goto_340
    if-eqz v0, :cond_36

    goto/32 :goto_357

    :cond_36
    goto/32 :goto_2c9

    nop

    :goto_341
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_3

    nop

    :goto_342
    move/from16 v4, v74

    goto/32 :goto_11e

    nop

    :goto_343
    aget v19, v42, v0

    goto/32 :goto_364

    nop

    :goto_344
    iget-object v3, v1, Landroid/text/PrecomputedText$ParagraphInfo;->measured:Landroid/text/MeasuredParagraph;

    goto/32 :goto_af

    nop

    :goto_345
    iget-boolean v3, v13, Landroid/text/StaticLayout;->mFallbackLineSpacing:Z

    goto/32 :goto_1a1

    nop

    :goto_346
    move-object/from16 v7, v28

    :goto_347
    goto/32 :goto_185

    nop

    :goto_348
    move/from16 v12, v22

    :goto_349
    goto/32 :goto_29d

    nop

    :goto_34a
    add-int/lit8 v18, v6, -0x1

    goto/32 :goto_26c

    nop

    :goto_34b
    goto/16 :goto_349

    :goto_34c
    goto/32 :goto_36b

    nop

    :goto_34d
    iget v8, v4, Landroid/text/StaticLayout;->mLineCount:I

    goto/32 :goto_1b5

    nop

    :goto_34e
    move-object/from16 v12, v85

    goto/32 :goto_1d7

    nop

    :goto_34f
    move-object/from16 v35, v85

    goto/32 :goto_d2

    nop

    :goto_350
    move-object/from16 v11, v37

    goto/32 :goto_d6

    nop

    :goto_351
    move-object/from16 v55, v7

    goto/32 :goto_3b5

    nop

    :goto_352
    sub-int v17, v17, v11

    goto/32 :goto_2b0

    nop

    :goto_353
    move/from16 v70, v33

    goto/32 :goto_3d3

    nop

    :goto_354
    goto/16 :goto_b6

    :goto_355
    goto/32 :goto_b5

    nop

    :goto_356
    goto/16 :goto_3c

    :goto_357
    goto/32 :goto_3b

    nop

    :goto_358
    move-object/from16 v22, v38

    goto/32 :goto_2ed

    nop

    :goto_359
    invoke-direct {v1, v9}, Landroid/text/PrecomputedText$Params$Builder;-><init>(Landroid/text/TextPaint;)V

    goto/32 :goto_f4

    nop

    :goto_35a
    add-float/2addr v0, v8

    goto/32 :goto_f6

    nop

    :goto_35b
    goto/16 :goto_2a2

    :goto_35c
    goto/32 :goto_2a1

    nop

    :goto_35d
    move/from16 v35, v17

    goto/32 :goto_20d

    nop

    :goto_35e
    const/4 v14, 0x0

    goto/32 :goto_21

    nop

    :goto_35f
    aget v8, v42, v2

    goto/32 :goto_2cc

    nop

    :goto_360
    move/from16 v6, v30

    goto/32 :goto_e0

    nop

    :goto_361
    if-eq v0, v5, :cond_37

    goto/32 :goto_124

    :cond_37
    goto/32 :goto_3f

    nop

    :goto_362
    if-eqz v28, :cond_38

    goto/32 :goto_20a

    :cond_38
    goto/32 :goto_2b2

    nop

    :goto_363
    aget-object v2, v7, v6

    goto/32 :goto_3c9

    nop

    :goto_364
    move/from16 v28, v1

    goto/32 :goto_4b

    nop

    :goto_365
    move-object/from16 v56, v30

    goto/32 :goto_3a1

    nop

    :goto_366
    new-array v2, v0, [Z

    goto/32 :goto_2ba

    nop

    :goto_367
    move-object/from16 v35, v12

    goto/32 :goto_9d

    nop

    :goto_368
    move-object/from16 v43, v19

    goto/32 :goto_12a

    nop

    :goto_369
    goto/16 :goto_163

    :goto_36a
    goto/32 :goto_162

    nop

    :goto_36b
    move/from16 v23, v0

    goto/32 :goto_37d

    nop

    :goto_36c
    iget v2, v3, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    goto/32 :goto_32a

    nop

    :goto_36d
    goto/16 :goto_158

    :goto_36e
    goto/32 :goto_3d4

    nop

    :goto_36f
    move-object/from16 v3, v81

    goto/32 :goto_25c

    nop

    :goto_370
    if-lt v3, v10, :cond_39

    goto/32 :goto_d

    :cond_39
    goto/32 :goto_23d

    nop

    :goto_371
    add-int/lit8 v58, v20, 0x1

    goto/32 :goto_3ab

    nop

    :goto_372
    iget v0, v13, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    goto/32 :goto_146

    nop

    :goto_373
    move-object/from16 v7, v55

    goto/32 :goto_39e

    nop

    :goto_374
    aget v0, v40, v20

    goto/32 :goto_19a

    nop

    :goto_375
    move/from16 v3, v46

    goto/32 :goto_281

    nop

    :goto_376
    move-object/from16 v15, v71

    goto/32 :goto_264

    nop

    :goto_377
    invoke-virtual {v3}, Landroid/text/MeasuredParagraph;->getFontMetrics()Landroid/text/AutoGrowArray$IntArray;

    move-result-object v1

    goto/32 :goto_156

    nop

    :goto_378
    move/from16 v67, v14

    goto/32 :goto_f9

    nop

    :goto_379
    array-length v9, v14

    goto/32 :goto_59

    nop

    :goto_37a
    move/from16 v35, v17

    goto/32 :goto_b1

    nop

    :goto_37b
    move/from16 v0, v86

    :goto_37c
    goto/32 :goto_2c7

    nop

    :goto_37d
    move-object/from16 v61, v3

    goto/32 :goto_2ae

    nop

    :goto_37e
    aget v11, v39, v20

    goto/32 :goto_2d2

    nop

    :goto_37f
    move-object/from16 v71, v15

    goto/32 :goto_13c

    nop

    :goto_380
    const/4 v7, 0x0

    goto/32 :goto_2bd

    nop

    :goto_381
    invoke-static/range {p1 .. p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmText(Landroid/text/StaticLayout$Builder;)Ljava/lang/CharSequence;

    move-result-object v12

    goto/32 :goto_21d

    nop

    :goto_382
    move/from16 v38, v49

    goto/32 :goto_e9

    nop

    :goto_383
    invoke-static {v15, v5, v4, v14}, Landroid/text/StaticLayout;->getParagraphSpans(Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v14

    goto/32 :goto_2b4

    nop

    :goto_384
    if-lt v3, v1, :cond_3a

    goto/32 :goto_3b7

    :cond_3a
    goto/32 :goto_3b6

    nop

    :goto_385
    move/from16 v6, v63

    goto/32 :goto_399

    nop

    :goto_386
    aget v19, v42, v14

    goto/32 :goto_1b7

    nop

    :goto_387
    move-object/from16 v34, v65

    goto/32 :goto_2cf

    nop

    :goto_388
    invoke-static {v3, v14, v8}, Ljava/util/Arrays;->sort([FII)V

    goto/32 :goto_31e

    nop

    :goto_389
    const/16 v69, 0x1

    goto/32 :goto_1a2

    nop

    :goto_38a
    invoke-static/range {p1 .. p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmEllipsize(Landroid/text/StaticLayout$Builder;)Landroid/text/TextUtils$TruncateAt;

    move-result-object v14

    goto/32 :goto_31a

    nop

    :goto_38b
    if-lt v8, v14, :cond_3b

    goto/32 :goto_33e

    :cond_3b
    goto/32 :goto_3bd

    nop

    :goto_38c
    move v0, v11

    goto/32 :goto_167

    nop

    :goto_38d
    aput v2, v42, v0

    goto/32 :goto_1a0

    nop

    :goto_38e
    move-object/from16 v1, v25

    :goto_38f
    goto/32 :goto_10a

    nop

    :goto_390
    move/from16 v10, v82

    goto/32 :goto_108

    nop

    :goto_391
    move/from16 v25, v9

    goto/32 :goto_cc

    nop

    :goto_392
    move-object/from16 v51, v6

    goto/32 :goto_bd

    nop

    :goto_393
    move-object/from16 v56, v8

    goto/32 :goto_19e

    nop

    :goto_394
    move/from16 v64, v2

    goto/32 :goto_284

    nop

    :goto_395
    move-object/from16 v85, v35

    goto/32 :goto_28e

    nop

    :goto_396
    move v2, v11

    goto/32 :goto_130

    nop

    :goto_397
    goto/16 :goto_219

    :goto_398
    goto/32 :goto_218

    nop

    :goto_399
    move/from16 v78, v7

    goto/32 :goto_ca

    nop

    :goto_39a
    move/from16 v86, v6

    goto/32 :goto_a5

    nop

    :goto_39b
    move-object/from16 v15, v71

    goto/32 :goto_342

    nop

    :goto_39c
    invoke-static {v15, v5, v4, v8}, Landroid/text/StaticLayout;->getParagraphSpans(Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v8

    goto/32 :goto_1f6

    nop

    :goto_39d
    sub-int v6, v0, v2

    goto/32 :goto_14e

    nop

    :goto_39e
    move-object/from16 v51, v57

    goto/32 :goto_387

    nop

    :goto_39f
    move/from16 v20, v69

    goto/32 :goto_14b

    nop

    :goto_3a0
    const/4 v0, 0x1

    goto/32 :goto_110

    nop

    :goto_3a1
    move-object/from16 v3, v61

    goto/32 :goto_7d

    nop

    :goto_3a2
    move/from16 v16, v7

    goto/32 :goto_322

    nop

    :goto_3a3
    move-object/from16 v66, v26

    goto/32 :goto_e2

    nop

    :goto_3a4
    move/from16 v0, v41

    goto/32 :goto_237

    nop

    :goto_3a5
    new-instance v1, Landroid/text/PrecomputedText$Params$Builder;

    goto/32 :goto_359

    nop

    :goto_3a6
    new-array v2, v0, [I

    goto/32 :goto_268

    nop

    :goto_3a7
    add-int/lit8 v9, v9, 0x1

    goto/32 :goto_2a5

    nop

    :goto_3a8
    new-array v3, v2, [I

    goto/32 :goto_e6

    nop

    :goto_3a9
    move v14, v0

    goto/32 :goto_ac

    nop

    :goto_3aa
    invoke-virtual {v12, v1, v2, v14}, Landroid/graphics/text/LineBreaker;->computeLineBreaks(Landroid/graphics/text/MeasuredText;Landroid/graphics/text/LineBreaker$ParagraphConstraints;I)Landroid/graphics/text/LineBreaker$Result;

    move-result-object v1

    goto/32 :goto_2a4

    nop

    :goto_3ab
    move/from16 v59, v11

    goto/32 :goto_37e

    nop

    :goto_3ac
    move-object/from16 v20, v44

    goto/32 :goto_25

    nop

    :goto_3ad
    const/4 v8, 0x0

    :goto_3ae
    goto/32 :goto_196

    nop

    :goto_3af
    move-object/from16 v18, v65

    goto/32 :goto_326

    nop

    :goto_3b0
    move-object/from16 v80, v31

    goto/32 :goto_2d5

    nop

    :goto_3b1
    move-object/from16 v85, v35

    goto/32 :goto_2c6

    nop

    :goto_3b2
    iput v0, v13, Landroid/text/StaticLayout;->mMaxLineHeight:I

    goto/32 :goto_3a

    nop

    :goto_3b3
    move v0, v1

    goto/32 :goto_2c0

    nop

    :goto_3b4
    move/from16 v86, v14

    goto/32 :goto_22

    nop

    :goto_3b5
    add-int/lit8 v7, v14, -0x1

    goto/32 :goto_2e8

    nop

    :goto_3b6
    move v1, v3

    :goto_3b7
    goto/32 :goto_166

    nop

    :goto_3b8
    iget-object v6, v13, Landroid/text/StaticLayout;->mLeftIndents:[I

    goto/32 :goto_56

    nop

    :goto_3b9
    check-cast v0, Landroid/text/Spanned;

    goto/32 :goto_35b

    nop

    :goto_3ba
    move/from16 v33, v15

    goto/32 :goto_25e

    nop

    :goto_3bb
    move/from16 v83, v32

    goto/32 :goto_e8

    nop

    :goto_3bc
    move/from16 v19, p2

    goto/32 :goto_15e

    nop

    :goto_3bd
    aget-object v14, v2, v8

    goto/32 :goto_3c1

    nop

    :goto_3be
    move/from16 v75, v5

    goto/32 :goto_12d

    nop

    :goto_3bf
    move-object v0, v6

    goto/32 :goto_93

    nop

    :goto_3c0
    move/from16 v8, v56

    goto/32 :goto_da

    nop

    :goto_3c1
    invoke-interface {v14}, Landroid/text/style/TabStopSpan;->getTabStop()I

    move-result v14

    goto/32 :goto_10c

    nop

    :goto_3c2
    move-object/from16 v80, v31

    goto/32 :goto_2e4

    nop

    :goto_3c3
    invoke-static/range {p1 .. p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmBreakStrategy(Landroid/text/StaticLayout$Builder;)I

    move-result v1

    goto/32 :goto_4

    nop

    :goto_3c4
    invoke-static/range {p1 .. p1}, Landroid/text/StaticLayout$Builder;->-$$Nest$fgetmLineBreakConfig(Landroid/text/StaticLayout$Builder;)Landroid/graphics/text/LineBreakConfig;

    move-result-object v2

    goto/32 :goto_2f

    nop

    :goto_3c5
    move/from16 v2, v86

    :goto_3c6
    goto/32 :goto_18b

    nop

    :goto_3c7
    const/4 v1, 0x0

    goto/32 :goto_330

    nop

    :goto_3c8
    const-class v2, Landroid/text/style/TabStopSpan;

    goto/32 :goto_153

    nop

    :goto_3c9
    iget v4, v2, Landroid/text/PrecomputedText$ParagraphInfo;->paragraphEnd:I

    goto/32 :goto_1b9

    nop

    :goto_3ca
    const/4 v4, 0x0

    :goto_3cb
    goto/32 :goto_1a5

    nop

    :goto_3cc
    move-object/from16 v2, v34

    goto/32 :goto_82

    nop

    :goto_3cd
    move v0, v7

    goto/32 :goto_369

    nop

    :goto_3ce
    invoke-virtual {v1, v0}, Landroid/graphics/text/LineBreaker$Result;->getLineBreakOffset(I)I

    move-result v2

    goto/32 :goto_38d

    nop

    :goto_3cf
    add-int/lit8 v2, v6, -0x1

    goto/32 :goto_3d6

    nop

    :goto_3d0
    move-object/from16 v61, v3

    goto/32 :goto_be

    nop

    :goto_3d1
    aput v2, v53, v0

    goto/32 :goto_1fa

    nop

    :goto_3d2
    aget v0, v40, v0

    goto/32 :goto_25b

    nop

    :goto_3d3
    move v15, v0

    goto/32 :goto_159

    nop

    :goto_3d4
    move-object/from16 v3, v81

    goto/32 :goto_29c

    nop

    :goto_3d5
    move v0, v14

    goto/32 :goto_2e

    nop

    :goto_3d6
    aput v0, v43, v2

    goto/32 :goto_293

    nop

    :goto_3d7
    new-array v2, v0, [F

    goto/32 :goto_11d

    nop

    :goto_3d8
    move-object v8, v1

    goto/32 :goto_b8

    nop
.end method

.method public getBottomPadding()I
    .locals 1

    iget v0, p0, Landroid/text/StaticLayout;->mBottomPadding:I

    return v0
.end method

.method public getEllipsisCount(I)I
    .locals 2

    iget v0, p0, Landroid/text/StaticLayout;->mColumns:I

    const/4 v1, 0x7

    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v1, p0, Landroid/text/StaticLayout;->mLines:[I

    mul-int/2addr v0, p1

    add-int/lit8 v0, v0, 0x6

    aget v0, v1, v0

    return v0
.end method

.method public getEllipsisStart(I)I
    .locals 2

    iget v0, p0, Landroid/text/StaticLayout;->mColumns:I

    const/4 v1, 0x7

    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v1, p0, Landroid/text/StaticLayout;->mLines:[I

    mul-int/2addr v0, p1

    add-int/lit8 v0, v0, 0x5

    aget v0, v1, v0

    return v0
.end method

.method public getEllipsizedWidth()I
    .locals 1

    iget v0, p0, Landroid/text/StaticLayout;->mEllipsizedWidth:I

    return v0
.end method

.method public getEndHyphenEdit(I)I
    .locals 2

    iget-object v0, p0, Landroid/text/StaticLayout;->mLines:[I

    iget v1, p0, Landroid/text/StaticLayout;->mColumns:I

    mul-int/2addr v1, p1

    add-int/lit8 v1, v1, 0x4

    aget v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    invoke-static {v0}, Landroid/text/StaticLayout;->unpackEndHyphenEdit(I)I

    move-result v0

    return v0
.end method

.method public getHeight(Z)I
    .locals 4

    const/4 v0, -0x1

    if-eqz p1, :cond_0

    iget v1, p0, Landroid/text/StaticLayout;->mLineCount:I

    iget v2, p0, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    if-le v1, v2, :cond_0

    iget v1, p0, Landroid/text/StaticLayout;->mMaxLineHeight:I

    if-ne v1, v0, :cond_0

    const/4 v1, 0x5

    const-string v2, "StaticLayout"

    invoke-static {v2, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "maxLineHeight should not be -1.  maxLines:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " lineCount:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Landroid/text/StaticLayout;->mLineCount:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-eqz p1, :cond_1

    iget v1, p0, Landroid/text/StaticLayout;->mLineCount:I

    iget v2, p0, Landroid/text/StaticLayout;->mMaximumVisibleLineCount:I

    if-le v1, v2, :cond_1

    iget v1, p0, Landroid/text/StaticLayout;->mMaxLineHeight:I

    if-eq v1, v0, :cond_1

    goto :goto_0

    :cond_1
    invoke-super {p0}, Landroid/text/Layout;->getHeight()I

    move-result v1

    :goto_0
    return v1
.end method

.method public getIndentAdjust(ILandroid/text/Layout$Alignment;)I
    .locals 4

    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_LEFT:Landroid/text/Layout$Alignment;

    const/4 v1, 0x0

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Landroid/text/StaticLayout;->mLeftIndents:[I

    if-nez v0, :cond_0

    return v1

    :cond_0
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    aget v0, v0, v1

    return v0

    :cond_1
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_RIGHT:Landroid/text/Layout$Alignment;

    if-ne p2, v0, :cond_3

    iget-object v0, p0, Landroid/text/StaticLayout;->mRightIndents:[I

    if-nez v0, :cond_2

    return v1

    :cond_2
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    aget v0, v0, v1

    neg-int v0, v0

    return v0

    :cond_3
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    if-ne p2, v0, :cond_6

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/text/StaticLayout;->mLeftIndents:[I

    if-eqz v1, :cond_4

    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    aget v0, v1, v2

    :cond_4
    const/4 v1, 0x0

    iget-object v2, p0, Landroid/text/StaticLayout;->mRightIndents:[I

    if-eqz v2, :cond_5

    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    invoke-static {p1, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    aget v1, v2, v3

    :cond_5
    sub-int v2, v0, v1

    shr-int/lit8 v2, v2, 0x1

    return v2

    :cond_6
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unhandled alignment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public getLineContainsTab(I)Z
    .locals 3

    iget-object v0, p0, Landroid/text/StaticLayout;->mLines:[I

    iget v1, p0, Landroid/text/StaticLayout;->mColumns:I

    mul-int/2addr v1, p1

    const/4 v2, 0x0

    add-int/2addr v1, v2

    aget v0, v0, v1

    const/high16 v1, 0x20000000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method public getLineCount()I
    .locals 1

    iget v0, p0, Landroid/text/StaticLayout;->mLineCount:I

    return v0
.end method

.method public getLineDescent(I)I
    .locals 2

    iget-object v0, p0, Landroid/text/StaticLayout;->mLines:[I

    iget v1, p0, Landroid/text/StaticLayout;->mColumns:I

    mul-int/2addr v1, p1

    add-int/lit8 v1, v1, 0x2

    aget v0, v0, v1

    return v0
.end method

.method public final getLineDirections(I)Landroid/text/Layout$Directions;
    .locals 1

    invoke-virtual {p0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v0

    if-gt p1, v0, :cond_0

    iget-object v0, p0, Landroid/text/StaticLayout;->mLineDirections:[Landroid/text/Layout$Directions;

    aget-object v0, v0, p1

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0
.end method

.method public getLineExtra(I)I
    .locals 2

    iget-object v0, p0, Landroid/text/StaticLayout;->mLines:[I

    iget v1, p0, Landroid/text/StaticLayout;->mColumns:I

    mul-int/2addr v1, p1

    add-int/lit8 v1, v1, 0x3

    aget v0, v0, v1

    return v0
.end method

.method public getLineForVertical(I)I
    .locals 6

    iget v0, p0, Landroid/text/StaticLayout;->mLineCount:I

    const/4 v1, -0x1

    iget-object v2, p0, Landroid/text/StaticLayout;->mLines:[I

    :goto_0
    sub-int v3, v0, v1

    const/4 v4, 0x1

    if-le v3, v4, :cond_1

    add-int v3, v0, v1

    shr-int/2addr v3, v4

    iget v5, p0, Landroid/text/StaticLayout;->mColumns:I

    mul-int/2addr v5, v3

    add-int/2addr v5, v4

    aget v4, v2, v5

    if-le v4, p1, :cond_0

    move v0, v3

    goto :goto_0

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    if-gez v1, :cond_2

    const/4 v3, 0x0

    return v3

    :cond_2
    return v1
.end method

.method public getLineStart(I)I
    .locals 2

    iget-object v0, p0, Landroid/text/StaticLayout;->mLines:[I

    iget v1, p0, Landroid/text/StaticLayout;->mColumns:I

    mul-int/2addr v1, p1

    add-int/lit8 v1, v1, 0x0

    aget v0, v0, v1

    const v1, 0x1fffffff

    and-int/2addr v0, v1

    return v0
.end method

.method public getLineTop(I)I
    .locals 2

    iget-object v0, p0, Landroid/text/StaticLayout;->mLines:[I

    iget v1, p0, Landroid/text/StaticLayout;->mColumns:I

    mul-int/2addr v1, p1

    add-int/lit8 v1, v1, 0x1

    aget v0, v0, v1

    return v0
.end method

.method public getParagraphDirection(I)I
    .locals 2

    iget-object v0, p0, Landroid/text/StaticLayout;->mLines:[I

    iget v1, p0, Landroid/text/StaticLayout;->mColumns:I

    mul-int/2addr v1, p1

    add-int/lit8 v1, v1, 0x0

    aget v0, v0, v1

    shr-int/lit8 v0, v0, 0x1e

    return v0
.end method

.method public getStartHyphenEdit(I)I
    .locals 2

    iget-object v0, p0, Landroid/text/StaticLayout;->mLines:[I

    iget v1, p0, Landroid/text/StaticLayout;->mColumns:I

    mul-int/2addr v1, p1

    add-int/lit8 v1, v1, 0x4

    aget v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    invoke-static {v0}, Landroid/text/StaticLayout;->unpackStartHyphenEdit(I)I

    move-result v0

    return v0
.end method

.method public getTopPadding()I
    .locals 1

    iget v0, p0, Landroid/text/StaticLayout;->mTopPadding:I

    return v0
.end method

.method public isFallbackLineSpacingEnabled()Z
    .locals 1

    iget-boolean v0, p0, Landroid/text/StaticLayout;->mFallbackLineSpacing:Z

    return v0
.end method
