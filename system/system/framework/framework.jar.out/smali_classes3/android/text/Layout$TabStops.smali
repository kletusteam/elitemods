.class public Landroid/text/Layout$TabStops;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/text/Layout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TabStops"
.end annotation


# instance fields
.field private mIncrement:F

.field private mNumStops:I

.field private mStops:[F


# direct methods
.method public constructor <init>(F[Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1, p2}, Landroid/text/Layout$TabStops;->reset(F[Ljava/lang/Object;)V

    return-void
.end method

.method public static nextDefaultStop(FF)F
    .locals 1

    add-float v0, p0, p1

    div-float/2addr v0, p1

    float-to-int v0, v0

    int-to-float v0, v0

    mul-float/2addr v0, p1

    return v0
.end method


# virtual methods
.method nextTab(F)F
    .locals 5

    goto/32 :goto_7

    nop

    :goto_0
    if-gtz v0, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_10

    nop

    :goto_1
    cmpl-float v4, v3, p1

    goto/32 :goto_5

    nop

    :goto_2
    invoke-static {p1, v1}, Landroid/text/Layout$TabStops;->nextDefaultStop(FF)F

    move-result v1

    goto/32 :goto_8

    nop

    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_9

    nop

    :goto_4
    iget v1, p0, Landroid/text/Layout$TabStops;->mIncrement:F

    goto/32 :goto_2

    nop

    :goto_5
    if-gtz v4, :cond_1

    goto/32 :goto_f

    :cond_1
    goto/32 :goto_e

    nop

    :goto_6
    if-lt v2, v0, :cond_2

    goto/32 :goto_a

    :cond_2
    goto/32 :goto_d

    nop

    :goto_7
    iget v0, p0, Landroid/text/Layout$TabStops;->mNumStops:I

    goto/32 :goto_0

    nop

    :goto_8
    return v1

    :goto_9
    goto :goto_c

    :goto_a
    goto/32 :goto_4

    nop

    :goto_b
    const/4 v2, 0x0

    :goto_c
    goto/32 :goto_6

    nop

    :goto_d
    aget v3, v1, v2

    goto/32 :goto_1

    nop

    :goto_e
    return v3

    :goto_f
    goto/32 :goto_3

    nop

    :goto_10
    iget-object v1, p0, Landroid/text/Layout$TabStops;->mStops:[F

    goto/32 :goto_b

    nop
.end method

.method reset(F[Ljava/lang/Object;)V
    .locals 9

    goto/32 :goto_14

    nop

    :goto_0
    if-nez p2, :cond_0

    goto/32 :goto_f

    :cond_0
    goto/32 :goto_7

    nop

    :goto_1
    if-eq v0, v6, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_33

    nop

    :goto_2
    const/16 v6, 0xa

    goto/32 :goto_11

    nop

    :goto_3
    const/4 v3, 0x0

    goto/32 :goto_1a

    nop

    :goto_4
    add-int/lit8 v7, v7, 0x1

    goto/32 :goto_17

    nop

    :goto_5
    add-int/lit8 v4, v4, 0x1

    goto/32 :goto_27

    nop

    :goto_6
    int-to-float v7, v7

    goto/32 :goto_21

    nop

    :goto_7
    iget-object v1, p0, Landroid/text/Layout$TabStops;->mStops:[F

    goto/32 :goto_15

    nop

    :goto_8
    aget v8, v1, v7

    goto/32 :goto_1d

    nop

    :goto_9
    move v0, v6

    :goto_a
    goto/32 :goto_5

    nop

    :goto_b
    move-object v1, v6

    :goto_c
    goto/32 :goto_10

    nop

    :goto_d
    move-object v7, v5

    goto/32 :goto_2a

    nop

    :goto_e
    iput-object v1, p0, Landroid/text/Layout$TabStops;->mStops:[F

    :goto_f
    goto/32 :goto_24

    nop

    :goto_10
    add-int/lit8 v6, v0, 0x1

    goto/32 :goto_d

    nop

    :goto_11
    new-array v1, v6, [F

    goto/32 :goto_30

    nop

    :goto_12
    if-gt v0, v2, :cond_2

    goto/32 :goto_2f

    :cond_2
    goto/32 :goto_2e

    nop

    :goto_13
    aget-object v5, p2, v4

    goto/32 :goto_2c

    nop

    :goto_14
    iput p1, p0, Landroid/text/Layout$TabStops;->mIncrement:F

    goto/32 :goto_2b

    nop

    :goto_15
    array-length v2, p2

    goto/32 :goto_3

    nop

    :goto_16
    if-eqz v1, :cond_3

    goto/32 :goto_31

    :cond_3
    goto/32 :goto_2

    nop

    :goto_17
    goto :goto_26

    :goto_18
    goto/32 :goto_b

    nop

    :goto_19
    invoke-interface {v7}, Landroid/text/style/TabStopSpan;->getTabStop()I

    move-result v7

    goto/32 :goto_6

    nop

    :goto_1a
    move v4, v3

    :goto_1b
    goto/32 :goto_29

    nop

    :goto_1c
    if-ne v1, v2, :cond_4

    goto/32 :goto_f

    :cond_4
    goto/32 :goto_e

    nop

    :goto_1d
    aput v8, v6, v7

    goto/32 :goto_4

    nop

    :goto_1e
    if-nez v6, :cond_5

    goto/32 :goto_a

    :cond_5
    goto/32 :goto_16

    nop

    :goto_1f
    iget-object v2, p0, Landroid/text/Layout$TabStops;->mStops:[F

    goto/32 :goto_1c

    nop

    :goto_20
    return-void

    :goto_21
    aput v7, v1, v0

    goto/32 :goto_9

    nop

    :goto_22
    if-lt v7, v0, :cond_6

    goto/32 :goto_18

    :cond_6
    goto/32 :goto_8

    nop

    :goto_23
    new-array v6, v6, [F

    goto/32 :goto_25

    nop

    :goto_24
    iput v0, p0, Landroid/text/Layout$TabStops;->mNumStops:I

    goto/32 :goto_20

    nop

    :goto_25
    const/4 v7, 0x0

    :goto_26
    goto/32 :goto_22

    nop

    :goto_27
    goto :goto_1b

    :goto_28
    goto/32 :goto_32

    nop

    :goto_29
    if-lt v4, v2, :cond_7

    goto/32 :goto_28

    :cond_7
    goto/32 :goto_13

    nop

    :goto_2a
    check-cast v7, Landroid/text/style/TabStopSpan;

    goto/32 :goto_19

    nop

    :goto_2b
    const/4 v0, 0x0

    goto/32 :goto_0

    nop

    :goto_2c
    instance-of v6, v5, Landroid/text/style/TabStopSpan;

    goto/32 :goto_1e

    nop

    :goto_2d
    array-length v6, v1

    goto/32 :goto_1

    nop

    :goto_2e
    invoke-static {v1, v3, v0}, Ljava/util/Arrays;->sort([FII)V

    :goto_2f
    goto/32 :goto_1f

    nop

    :goto_30
    goto/16 :goto_c

    :goto_31
    goto/32 :goto_2d

    nop

    :goto_32
    const/4 v2, 0x1

    goto/32 :goto_12

    nop

    :goto_33
    mul-int/lit8 v6, v0, 0x2

    goto/32 :goto_23

    nop
.end method
