.class public interface abstract Landroid/text/style/LineHeightSpan;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/text/style/ParagraphStyle;
.implements Landroid/text/style/WrapTogetherSpan;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/text/style/LineHeightSpan$Standard;,
        Landroid/text/style/LineHeightSpan$WithDensity;
    }
.end annotation


# virtual methods
.method public abstract chooseHeight(Ljava/lang/CharSequence;IIIILandroid/graphics/Paint$FontMetricsInt;)V
.end method
