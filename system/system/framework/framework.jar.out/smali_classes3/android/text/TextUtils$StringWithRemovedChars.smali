.class Landroid/text/TextUtils$StringWithRemovedChars;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/text/TextUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "StringWithRemovedChars"
.end annotation


# instance fields
.field private final mOriginal:Ljava/lang/String;

.field private mRemovedChars:Ljava/util/BitSet;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/text/TextUtils$StringWithRemovedChars;->mOriginal:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method codePointAt(I)I
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return v0

    :goto_1
    invoke-virtual {v0, p1}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, Landroid/text/TextUtils$StringWithRemovedChars;->mOriginal:Ljava/lang/String;

    goto/32 :goto_1

    nop
.end method

.method length()I
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Landroid/text/TextUtils$StringWithRemovedChars;->mOriginal:Ljava/lang/String;

    goto/32 :goto_0

    nop

    :goto_2
    return v0
.end method

.method removeAllCharAfter(I)V
    .locals 2

    goto/32 :goto_b

    nop

    :goto_0
    iput-object v0, p0, Landroid/text/TextUtils$StringWithRemovedChars;->mRemovedChars:Ljava/util/BitSet;

    :goto_1
    goto/32 :goto_9

    nop

    :goto_2
    iget-object v1, p0, Landroid/text/TextUtils$StringWithRemovedChars;->mOriginal:Ljava/lang/String;

    goto/32 :goto_5

    nop

    :goto_3
    new-instance v0, Ljava/util/BitSet;

    goto/32 :goto_2

    nop

    :goto_4
    invoke-virtual {v0, p1, v1}, Ljava/util/BitSet;->set(II)V

    goto/32 :goto_6

    nop

    :goto_5
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    goto/32 :goto_8

    nop

    :goto_6
    return-void

    :goto_7
    iget-object v1, p0, Landroid/text/TextUtils$StringWithRemovedChars;->mOriginal:Ljava/lang/String;

    goto/32 :goto_c

    nop

    :goto_8
    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    goto/32 :goto_0

    nop

    :goto_9
    iget-object v0, p0, Landroid/text/TextUtils$StringWithRemovedChars;->mRemovedChars:Ljava/util/BitSet;

    goto/32 :goto_7

    nop

    :goto_a
    if-eqz v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_3

    nop

    :goto_b
    iget-object v0, p0, Landroid/text/TextUtils$StringWithRemovedChars;->mRemovedChars:Ljava/util/BitSet;

    goto/32 :goto_a

    nop

    :goto_c
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    goto/32 :goto_4

    nop
.end method

.method removeAllCharBefore(I)V
    .locals 2

    goto/32 :goto_6

    nop

    :goto_0
    new-instance v0, Ljava/util/BitSet;

    goto/32 :goto_1

    nop

    :goto_1
    iget-object v1, p0, Landroid/text/TextUtils$StringWithRemovedChars;->mOriginal:Ljava/lang/String;

    goto/32 :goto_5

    nop

    :goto_2
    if-eqz v0, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_0

    nop

    :goto_3
    const/4 v1, 0x0

    goto/32 :goto_7

    nop

    :goto_4
    return-void

    :goto_5
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    goto/32 :goto_a

    nop

    :goto_6
    iget-object v0, p0, Landroid/text/TextUtils$StringWithRemovedChars;->mRemovedChars:Ljava/util/BitSet;

    goto/32 :goto_2

    nop

    :goto_7
    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(II)V

    goto/32 :goto_4

    nop

    :goto_8
    iput-object v0, p0, Landroid/text/TextUtils$StringWithRemovedChars;->mRemovedChars:Ljava/util/BitSet;

    :goto_9
    goto/32 :goto_b

    nop

    :goto_a
    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    goto/32 :goto_8

    nop

    :goto_b
    iget-object v0, p0, Landroid/text/TextUtils$StringWithRemovedChars;->mRemovedChars:Ljava/util/BitSet;

    goto/32 :goto_3

    nop
.end method

.method removeRange(II)V
    .locals 2

    goto/32 :goto_7

    nop

    :goto_0
    iput-object v0, p0, Landroid/text/TextUtils$StringWithRemovedChars;->mRemovedChars:Ljava/util/BitSet;

    :goto_1
    goto/32 :goto_4

    nop

    :goto_2
    iget-object v1, p0, Landroid/text/TextUtils$StringWithRemovedChars;->mOriginal:Ljava/lang/String;

    goto/32 :goto_a

    nop

    :goto_3
    return-void

    :goto_4
    iget-object v0, p0, Landroid/text/TextUtils$StringWithRemovedChars;->mRemovedChars:Ljava/util/BitSet;

    goto/32 :goto_6

    nop

    :goto_5
    new-instance v0, Ljava/util/BitSet;

    goto/32 :goto_2

    nop

    :goto_6
    invoke-virtual {v0, p1, p2}, Ljava/util/BitSet;->set(II)V

    goto/32 :goto_3

    nop

    :goto_7
    iget-object v0, p0, Landroid/text/TextUtils$StringWithRemovedChars;->mRemovedChars:Ljava/util/BitSet;

    goto/32 :goto_8

    nop

    :goto_8
    if-eqz v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_5

    nop

    :goto_9
    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    goto/32 :goto_0

    nop

    :goto_a
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    goto/32 :goto_9

    nop
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Landroid/text/TextUtils$StringWithRemovedChars;->mRemovedChars:Ljava/util/BitSet;

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/text/TextUtils$StringWithRemovedChars;->mOriginal:Ljava/lang/String;

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Landroid/text/TextUtils$StringWithRemovedChars;->mOriginal:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Landroid/text/TextUtils$StringWithRemovedChars;->mOriginal:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_2

    iget-object v2, p0, Landroid/text/TextUtils$StringWithRemovedChars;->mRemovedChars:Ljava/util/BitSet;

    invoke-virtual {v2, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Landroid/text/TextUtils$StringWithRemovedChars;->mOriginal:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
