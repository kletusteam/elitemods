.class public Landroid/text/BoringLayout;
.super Landroid/text/Layout;

# interfaces
.implements Landroid/text/TextUtils$EllipsizeCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/text/BoringLayout$Metrics;
    }
.end annotation


# instance fields
.field mBottom:I

.field private mBottomPadding:I

.field mDesc:I

.field private mDirect:Ljava/lang/String;

.field private mEllipsizedCount:I

.field private mEllipsizedStart:I

.field private mEllipsizedWidth:I

.field private mMax:F

.field private mPaint:Landroid/graphics/Paint;

.field private mTopPadding:I

.field private mUseFallbackLineSpacing:Z


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;Z)V
    .locals 10

    move-object v8, p0

    invoke-direct/range {p0 .. p6}, Landroid/text/Layout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FF)V

    move v9, p3

    iput v9, v8, Landroid/text/BoringLayout;->mEllipsizedWidth:I

    const/4 v0, 0x0

    iput v0, v8, Landroid/text/BoringLayout;->mEllipsizedStart:I

    iput v0, v8, Landroid/text/BoringLayout;->mEllipsizedCount:I

    iput-boolean v0, v8, Landroid/text/BoringLayout;->mUseFallbackLineSpacing:Z

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object/from16 v4, p7

    move/from16 v5, p8

    invoke-virtual/range {v0 .. v7}, Landroid/text/BoringLayout;->init(Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/Layout$Alignment;Landroid/text/BoringLayout$Metrics;ZZZ)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;ZLandroid/text/TextUtils$TruncateAt;I)V
    .locals 12

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    move-object/from16 v9, p9

    move/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Landroid/text/BoringLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;ZLandroid/text/TextUtils$TruncateAt;IZ)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;ZLandroid/text/TextUtils$TruncateAt;IZ)V
    .locals 14

    move-object v8, p0

    move-object/from16 v9, p9

    move/from16 v10, p10

    invoke-direct/range {p0 .. p6}, Landroid/text/Layout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FF)V

    if-eqz v9, :cond_1

    sget-object v0, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    if-ne v9, v0, :cond_0

    goto :goto_0

    :cond_0
    int-to-float v2, v10

    const/4 v4, 0x1

    move-object v0, p1

    move-object/from16 v1, p2

    move-object/from16 v3, p9

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;ZLandroid/text/TextUtils$EllipsizeCallback;)Ljava/lang/CharSequence;

    move-result-object v1

    move-object v0, p0

    move-object/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    invoke-virtual/range {v0 .. v6}, Landroid/text/BoringLayout;->replaceWith(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FF)V

    iput v10, v8, Landroid/text/BoringLayout;->mEllipsizedWidth:I

    const/4 v0, 0x0

    move/from16 v11, p3

    move v12, v0

    goto :goto_1

    :cond_1
    :goto_0
    move/from16 v11, p3

    iput v11, v8, Landroid/text/BoringLayout;->mEllipsizedWidth:I

    const/4 v0, 0x0

    iput v0, v8, Landroid/text/BoringLayout;->mEllipsizedStart:I

    iput v0, v8, Landroid/text/BoringLayout;->mEllipsizedCount:I

    const/4 v0, 0x1

    move v12, v0

    :goto_1
    move/from16 v13, p11

    iput-boolean v13, v8, Landroid/text/BoringLayout;->mUseFallbackLineSpacing:Z

    invoke-virtual {p0}, Landroid/text/BoringLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    move-object v0, p0

    move-object/from16 v2, p2

    move-object/from16 v3, p4

    move-object/from16 v4, p7

    move/from16 v5, p8

    move v6, v12

    move/from16 v7, p11

    invoke-virtual/range {v0 .. v7}, Landroid/text/BoringLayout;->init(Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/Layout$Alignment;Landroid/text/BoringLayout$Metrics;ZZZ)V

    return-void
.end method

.method private static hasAnyInterestingChars(Ljava/lang/CharSequence;I)Z
    .locals 8

    const/16 v0, 0x1f4

    const/16 v1, 0x1f4

    invoke-static {v1}, Landroid/text/TextUtils;->obtain(I)[C

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x0

    if-ge v2, p1, :cond_3

    add-int/lit16 v4, v2, 0x1f4

    :try_start_0
    invoke-static {v4, p1}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-static {p0, v2, v4, v1, v3}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    sub-int v3, v4, v2

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v3, :cond_2

    aget-char v6, v1, v5

    const/16 v7, 0xa

    if-eq v6, v7, :cond_1

    const/16 v7, 0x9

    if-eq v6, v7, :cond_1

    invoke-static {v6}, Landroid/text/TextUtils;->couldAffectRtl(C)Z

    move-result v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v7, :cond_0

    goto :goto_2

    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_1
    :goto_2
    nop

    invoke-static {v1}, Landroid/text/TextUtils;->recycle([C)V

    const/4 v7, 0x1

    return v7

    :cond_2
    add-int/lit16 v2, v2, 0x1f4

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-static {v1}, Landroid/text/TextUtils;->recycle([C)V

    throw v2

    :cond_3
    nop

    invoke-static {v1}, Landroid/text/TextUtils;->recycle([C)V

    return v3
.end method

.method public static isBoring(Ljava/lang/CharSequence;Landroid/text/TextPaint;)Landroid/text/BoringLayout$Metrics;
    .locals 2

    sget-object v0, Landroid/text/TextDirectionHeuristics;->FIRSTSTRONG_LTR:Landroid/text/TextDirectionHeuristic;

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Landroid/text/BoringLayout;->isBoring(Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextDirectionHeuristic;Landroid/text/BoringLayout$Metrics;)Landroid/text/BoringLayout$Metrics;

    move-result-object v0

    return-object v0
.end method

.method public static isBoring(Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/BoringLayout$Metrics;)Landroid/text/BoringLayout$Metrics;
    .locals 1

    sget-object v0, Landroid/text/TextDirectionHeuristics;->FIRSTSTRONG_LTR:Landroid/text/TextDirectionHeuristic;

    invoke-static {p0, p1, v0, p2}, Landroid/text/BoringLayout;->isBoring(Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextDirectionHeuristic;Landroid/text/BoringLayout$Metrics;)Landroid/text/BoringLayout$Metrics;

    move-result-object v0

    return-object v0
.end method

.method public static isBoring(Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextDirectionHeuristic;Landroid/text/BoringLayout$Metrics;)Landroid/text/BoringLayout$Metrics;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0, p3}, Landroid/text/BoringLayout;->isBoring(Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextDirectionHeuristic;ZLandroid/text/BoringLayout$Metrics;)Landroid/text/BoringLayout$Metrics;

    move-result-object v0

    return-object v0
.end method

.method public static isBoring(Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/TextDirectionHeuristic;ZLandroid/text/BoringLayout$Metrics;)Landroid/text/BoringLayout$Metrics;
    .locals 16

    move-object/from16 v12, p0

    move-object/from16 v13, p2

    invoke-interface/range {p0 .. p0}, Ljava/lang/CharSequence;->length()I

    move-result v14

    invoke-static {v12, v14}, Landroid/text/BoringLayout;->hasAnyInterestingChars(Ljava/lang/CharSequence;I)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    :cond_0
    const/4 v0, 0x0

    if-eqz v13, :cond_1

    invoke-interface {v13, v12, v0, v14}, Landroid/text/TextDirectionHeuristic;->isRtl(Ljava/lang/CharSequence;II)Z

    move-result v2

    if-eqz v2, :cond_1

    return-object v1

    :cond_1
    instance-of v2, v12, Landroid/text/Spanned;

    if-eqz v2, :cond_2

    move-object v2, v12

    check-cast v2, Landroid/text/Spanned;

    const-class v3, Landroid/text/style/ParagraphStyle;

    invoke-interface {v2, v0, v14, v3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    array-length v3, v0

    if-lez v3, :cond_2

    return-object v1

    :cond_2
    move-object/from16 v0, p4

    if-nez v0, :cond_3

    new-instance v1, Landroid/text/BoringLayout$Metrics;

    invoke-direct {v1}, Landroid/text/BoringLayout$Metrics;-><init>()V

    move-object v0, v1

    move-object v15, v0

    goto :goto_0

    :cond_3
    invoke-static {v0}, Landroid/text/BoringLayout$Metrics;->-$$Nest$mreset(Landroid/text/BoringLayout$Metrics;)V

    move-object v15, v0

    :goto_0
    invoke-static {}, Landroid/text/TextLine;->obtain()Landroid/text/TextLine;

    move-result-object v11

    const/4 v3, 0x0

    const/4 v5, 0x1

    sget-object v6, Landroid/text/Layout;->DIRS_ALL_LEFT_TO_RIGHT:Landroid/text/Layout$Directions;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v0, v11

    move-object/from16 v1, p1

    move-object/from16 v2, p0

    move v4, v14

    move-object v12, v11

    move/from16 v11, p3

    invoke-virtual/range {v0 .. v11}, Landroid/text/TextLine;->set(Landroid/text/TextPaint;Ljava/lang/CharSequence;IIILandroid/text/Layout$Directions;ZLandroid/text/Layout$TabStops;IIZ)V

    invoke-virtual {v12, v15}, Landroid/text/TextLine;->metrics(Landroid/graphics/Paint$FontMetricsInt;)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, v15, Landroid/text/BoringLayout$Metrics;->width:I

    invoke-static {v12}, Landroid/text/TextLine;->recycle(Landroid/text/TextLine;)Landroid/text/TextLine;

    return-object v15
.end method

.method public static make(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;Z)Landroid/text/BoringLayout;
    .locals 10

    new-instance v9, Landroid/text/BoringLayout;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Landroid/text/BoringLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;Z)V

    return-object v9
.end method

.method public static make(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;ZLandroid/text/TextUtils$TruncateAt;I)Landroid/text/BoringLayout;
    .locals 12

    new-instance v11, Landroid/text/BoringLayout;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p8

    move/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Landroid/text/BoringLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;ZLandroid/text/TextUtils$TruncateAt;I)V

    return-object v11
.end method

.method public static make(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/BoringLayout$Metrics;ZLandroid/text/TextUtils$TruncateAt;IZ)Landroid/text/BoringLayout;
    .locals 13

    new-instance v12, Landroid/text/BoringLayout;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object/from16 v4, p3

    move-object/from16 v7, p4

    move/from16 v8, p5

    move-object/from16 v9, p6

    move/from16 v10, p7

    move/from16 v11, p8

    invoke-direct/range {v0 .. v11}, Landroid/text/BoringLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;ZLandroid/text/TextUtils$TruncateAt;IZ)V

    return-object v12
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;I)V
    .locals 4

    iget-object v0, p0, Landroid/text/BoringLayout;->mDirect:Ljava/lang/String;

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    const/4 v1, 0x0

    iget v2, p0, Landroid/text/BoringLayout;->mBottom:I

    iget v3, p0, Landroid/text/BoringLayout;->mDesc:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Landroid/text/BoringLayout;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_0

    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;I)V

    :goto_0
    return-void
.end method

.method public ellipsized(II)V
    .locals 1

    iput p1, p0, Landroid/text/BoringLayout;->mEllipsizedStart:I

    sub-int v0, p2, p1

    iput v0, p0, Landroid/text/BoringLayout;->mEllipsizedCount:I

    return-void
.end method

.method public getBottomPadding()I
    .locals 1

    iget v0, p0, Landroid/text/BoringLayout;->mBottomPadding:I

    return v0
.end method

.method public getEllipsisCount(I)I
    .locals 1

    iget v0, p0, Landroid/text/BoringLayout;->mEllipsizedCount:I

    return v0
.end method

.method public getEllipsisStart(I)I
    .locals 1

    iget v0, p0, Landroid/text/BoringLayout;->mEllipsizedStart:I

    return v0
.end method

.method public getEllipsizedWidth()I
    .locals 1

    iget v0, p0, Landroid/text/BoringLayout;->mEllipsizedWidth:I

    return v0
.end method

.method public getHeight()I
    .locals 1

    iget v0, p0, Landroid/text/BoringLayout;->mBottom:I

    return v0
.end method

.method public getLineContainsTab(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getLineCount()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getLineDescent(I)I
    .locals 1

    iget v0, p0, Landroid/text/BoringLayout;->mDesc:I

    return v0
.end method

.method public final getLineDirections(I)Landroid/text/Layout$Directions;
    .locals 1

    sget-object v0, Landroid/text/Layout;->DIRS_ALL_LEFT_TO_RIGHT:Landroid/text/Layout$Directions;

    return-object v0
.end method

.method public getLineMax(I)F
    .locals 1

    iget v0, p0, Landroid/text/BoringLayout;->mMax:F

    return v0
.end method

.method public getLineStart(I)I
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/text/BoringLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    return v0
.end method

.method public getLineTop(I)I
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget v0, p0, Landroid/text/BoringLayout;->mBottom:I

    return v0
.end method

.method public getLineWidth(I)F
    .locals 1

    if-nez p1, :cond_0

    iget v0, p0, Landroid/text/BoringLayout;->mMax:F

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getParagraphDirection(I)I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getTopPadding()I
    .locals 1

    iget v0, p0, Landroid/text/BoringLayout;->mTopPadding:I

    return v0
.end method

.method init(Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/Layout$Alignment;Landroid/text/BoringLayout$Metrics;ZZZ)V
    .locals 20

    goto/32 :goto_52

    nop

    :goto_0
    const/4 v9, 0x0

    goto/32 :goto_46

    nop

    :goto_1
    move-object/from16 v1, p4

    goto/32 :goto_31

    nop

    :goto_2
    move/from16 v11, v18

    goto/32 :goto_39

    nop

    :goto_3
    iget v2, v1, Landroid/text/BoringLayout$Metrics;->width:I

    goto/32 :goto_18

    nop

    :goto_4
    iget v2, v1, Landroid/text/BoringLayout$Metrics;->top:I

    goto/32 :goto_49

    nop

    :goto_5
    goto :goto_11

    :goto_6
    goto/32 :goto_44

    nop

    :goto_7
    move v11, v2

    :goto_8
    goto/32 :goto_1e

    nop

    :goto_9
    add-int v17, v4, v2

    goto/32 :goto_53

    nop

    :goto_a
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    move-result v6

    goto/32 :goto_30

    nop

    :goto_b
    if-nez p5, :cond_0

    goto/32 :goto_1c

    :cond_0
    goto/32 :goto_28

    nop

    :goto_c
    move-object/from16 v10, v16

    goto/32 :goto_3e

    nop

    :goto_d
    iget v2, v1, Landroid/text/BoringLayout$Metrics;->descent:I

    goto/32 :goto_4e

    nop

    :goto_e
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_50

    nop

    :goto_f
    iput v3, v0, Landroid/text/BoringLayout;->mMax:F

    goto/32 :goto_10

    nop

    :goto_10
    invoke-static {v2}, Landroid/text/TextLine;->recycle(Landroid/text/TextLine;)Landroid/text/TextLine;

    :goto_11
    goto/32 :goto_12

    nop

    :goto_12
    if-nez p5, :cond_1

    goto/32 :goto_2e

    :cond_1
    goto/32 :goto_4

    nop

    :goto_13
    instance-of v2, v14, Ljava/lang/String;

    goto/32 :goto_3d

    nop

    :goto_14
    if-nez p6, :cond_2

    goto/32 :goto_6

    :cond_2
    goto/32 :goto_3

    nop

    :goto_15
    iput v2, v0, Landroid/text/BoringLayout;->mMax:F

    goto/32 :goto_23

    nop

    :goto_16
    move-object/from16 v12, p2

    goto/32 :goto_17

    nop

    :goto_17
    iput-object v12, v0, Landroid/text/BoringLayout;->mPaint:Landroid/graphics/Paint;

    goto/32 :goto_b

    nop

    :goto_18
    int-to-float v2, v2

    goto/32 :goto_15

    nop

    :goto_19
    iput v3, v0, Landroid/text/BoringLayout;->mDesc:I

    goto/32 :goto_4d

    nop

    :goto_1a
    iget v2, v1, Landroid/text/BoringLayout$Metrics;->bottom:I

    goto/32 :goto_22

    nop

    :goto_1b
    goto :goto_8

    :goto_1c
    goto/32 :goto_d

    nop

    :goto_1d
    move/from16 v13, p7

    goto/32 :goto_3c

    nop

    :goto_1e
    iput v11, v0, Landroid/text/BoringLayout;->mBottom:I

    goto/32 :goto_14

    nop

    :goto_1f
    if-eq v13, v2, :cond_3

    goto/32 :goto_21

    :cond_3
    goto/32 :goto_e

    nop

    :goto_20
    move-object/from16 v13, p3

    :goto_21
    goto/32 :goto_4b

    nop

    :goto_22
    iget v3, v1, Landroid/text/BoringLayout$Metrics;->descent:I

    goto/32 :goto_3b

    nop

    :goto_23
    move/from16 v16, v11

    goto/32 :goto_5

    nop

    :goto_24
    move-object/from16 v2, v19

    goto/32 :goto_3a

    nop

    :goto_25
    goto/16 :goto_4c

    :goto_26
    goto/32 :goto_20

    nop

    :goto_27
    iget v3, v1, Landroid/text/BoringLayout$Metrics;->top:I

    goto/32 :goto_33

    nop

    :goto_28
    iget v2, v1, Landroid/text/BoringLayout$Metrics;->bottom:I

    goto/32 :goto_27

    nop

    :goto_29
    sub-int/2addr v2, v3

    goto/32 :goto_35

    nop

    :goto_2a
    move-object/from16 v3, p2

    goto/32 :goto_34

    nop

    :goto_2b
    move-object/from16 v4, p1

    goto/32 :goto_38

    nop

    :goto_2c
    sub-int/2addr v2, v3

    goto/32 :goto_51

    nop

    :goto_2d
    iput v2, v0, Landroid/text/BoringLayout;->mBottomPadding:I

    :goto_2e
    goto/32 :goto_43

    nop

    :goto_2f
    if-nez v2, :cond_4

    goto/32 :goto_26

    :cond_4
    goto/32 :goto_37

    nop

    :goto_30
    const/4 v7, 0x1

    goto/32 :goto_48

    nop

    :goto_31
    move-object/from16 v14, p1

    goto/32 :goto_13

    nop

    :goto_32
    iget v2, v0, Landroid/text/BoringLayout;->mEllipsizedCount:I

    goto/32 :goto_9

    nop

    :goto_33
    sub-int/2addr v2, v3

    goto/32 :goto_40

    nop

    :goto_34
    move/from16 v18, v4

    goto/32 :goto_2b

    nop

    :goto_35
    iget v3, v1, Landroid/text/BoringLayout$Metrics;->descent:I

    goto/32 :goto_45

    nop

    :goto_36
    iget v4, v0, Landroid/text/BoringLayout;->mEllipsizedStart:I

    goto/32 :goto_32

    nop

    :goto_37
    sget-object v2, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    goto/32 :goto_41

    nop

    :goto_38
    move-object/from16 v19, v10

    goto/32 :goto_c

    nop

    :goto_39
    move/from16 v12, v17

    goto/32 :goto_1d

    nop

    :goto_3a
    invoke-virtual {v2, v15}, Landroid/text/TextLine;->metrics(Landroid/graphics/Paint$FontMetricsInt;)F

    move-result v3

    goto/32 :goto_47

    nop

    :goto_3b
    sub-int/2addr v2, v3

    goto/32 :goto_2d

    nop

    :goto_3c
    invoke-virtual/range {v2 .. v13}, Landroid/text/TextLine;->set(Landroid/text/TextPaint;Ljava/lang/CharSequence;IIILandroid/text/Layout$Directions;ZLandroid/text/Layout$TabStops;IIZ)V

    goto/32 :goto_24

    nop

    :goto_3d
    const/4 v15, 0x0

    goto/32 :goto_2f

    nop

    :goto_3e
    move/from16 v16, v11

    goto/32 :goto_2

    nop

    :goto_3f
    const/4 v5, 0x0

    goto/32 :goto_a

    nop

    :goto_40
    iget v3, v1, Landroid/text/BoringLayout$Metrics;->bottom:I

    goto/32 :goto_19

    nop

    :goto_41
    move-object/from16 v13, p3

    goto/32 :goto_1f

    nop

    :goto_42
    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    goto/32 :goto_4f

    nop

    :goto_43
    return-void

    :goto_44
    invoke-static {}, Landroid/text/TextLine;->obtain()Landroid/text/TextLine;

    move-result-object v10

    goto/32 :goto_3f

    nop

    :goto_45
    iput v3, v0, Landroid/text/BoringLayout;->mDesc:I

    goto/32 :goto_7

    nop

    :goto_46
    const/16 v16, 0x0

    goto/32 :goto_36

    nop

    :goto_47
    float-to-double v3, v3

    goto/32 :goto_42

    nop

    :goto_48
    sget-object v8, Landroid/text/Layout;->DIRS_ALL_LEFT_TO_RIGHT:Landroid/text/Layout$Directions;

    goto/32 :goto_0

    nop

    :goto_49
    iget v3, v1, Landroid/text/BoringLayout$Metrics;->ascent:I

    goto/32 :goto_2c

    nop

    :goto_4a
    int-to-float v3, v3

    goto/32 :goto_f

    nop

    :goto_4b
    iput-object v15, v0, Landroid/text/BoringLayout;->mDirect:Ljava/lang/String;

    :goto_4c
    goto/32 :goto_16

    nop

    :goto_4d
    move v11, v2

    goto/32 :goto_1b

    nop

    :goto_4e
    iget v3, v1, Landroid/text/BoringLayout$Metrics;->ascent:I

    goto/32 :goto_29

    nop

    :goto_4f
    double-to-int v3, v3

    goto/32 :goto_4a

    nop

    :goto_50
    iput-object v2, v0, Landroid/text/BoringLayout;->mDirect:Ljava/lang/String;

    goto/32 :goto_25

    nop

    :goto_51
    iput v2, v0, Landroid/text/BoringLayout;->mTopPadding:I

    goto/32 :goto_1a

    nop

    :goto_52
    move-object/from16 v0, p0

    goto/32 :goto_1

    nop

    :goto_53
    move-object v2, v10

    goto/32 :goto_2a

    nop
.end method

.method public isFallbackLineSpacingEnabled()Z
    .locals 1

    iget-boolean v0, p0, Landroid/text/BoringLayout;->mUseFallbackLineSpacing:Z

    return v0
.end method

.method public replaceOrMake(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;Z)Landroid/text/BoringLayout;
    .locals 10

    move-object v8, p0

    invoke-virtual/range {p0 .. p6}, Landroid/text/BoringLayout;->replaceWith(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FF)V

    move v9, p3

    iput v9, v8, Landroid/text/BoringLayout;->mEllipsizedWidth:I

    const/4 v0, 0x0

    iput v0, v8, Landroid/text/BoringLayout;->mEllipsizedStart:I

    iput v0, v8, Landroid/text/BoringLayout;->mEllipsizedCount:I

    iput-boolean v0, v8, Landroid/text/BoringLayout;->mUseFallbackLineSpacing:Z

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object/from16 v4, p7

    move/from16 v5, p8

    invoke-virtual/range {v0 .. v7}, Landroid/text/BoringLayout;->init(Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/Layout$Alignment;Landroid/text/BoringLayout$Metrics;ZZZ)V

    return-object v8
.end method

.method public replaceOrMake(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;ZLandroid/text/TextUtils$TruncateAt;I)Landroid/text/BoringLayout;
    .locals 10

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object/from16 v5, p7

    move/from16 v6, p8

    move-object/from16 v7, p9

    move/from16 v8, p10

    invoke-virtual/range {v0 .. v9}, Landroid/text/BoringLayout;->replaceOrMake(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/BoringLayout$Metrics;ZLandroid/text/TextUtils$TruncateAt;IZ)Landroid/text/BoringLayout;

    move-result-object v0

    return-object v0
.end method

.method public replaceOrMake(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;Landroid/text/BoringLayout$Metrics;ZLandroid/text/TextUtils$TruncateAt;IZ)Landroid/text/BoringLayout;
    .locals 14

    move-object v8, p0

    move-object/from16 v9, p7

    move/from16 v10, p8

    if-eqz v9, :cond_1

    sget-object v0, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    if-ne v9, v0, :cond_0

    goto :goto_0

    :cond_0
    int-to-float v2, v10

    const/4 v4, 0x1

    move-object v0, p1

    move-object/from16 v1, p2

    move-object/from16 v3, p7

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;ZLandroid/text/TextUtils$EllipsizeCallback;)Ljava/lang/CharSequence;

    move-result-object v1

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    move-object v0, p0

    move-object/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v4, p4

    invoke-virtual/range {v0 .. v6}, Landroid/text/BoringLayout;->replaceWith(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FF)V

    iput v10, v8, Landroid/text/BoringLayout;->mEllipsizedWidth:I

    const/4 v0, 0x0

    move/from16 v11, p3

    move v12, v0

    goto :goto_1

    :cond_1
    :goto_0
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v4, p4

    invoke-virtual/range {v0 .. v6}, Landroid/text/BoringLayout;->replaceWith(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FF)V

    move/from16 v11, p3

    iput v11, v8, Landroid/text/BoringLayout;->mEllipsizedWidth:I

    const/4 v0, 0x0

    iput v0, v8, Landroid/text/BoringLayout;->mEllipsizedStart:I

    iput v0, v8, Landroid/text/BoringLayout;->mEllipsizedCount:I

    const/4 v0, 0x1

    move v12, v0

    :goto_1
    move/from16 v13, p9

    iput-boolean v13, v8, Landroid/text/BoringLayout;->mUseFallbackLineSpacing:Z

    invoke-virtual {p0}, Landroid/text/BoringLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    move-object v0, p0

    move-object/from16 v2, p2

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move/from16 v5, p6

    move v6, v12

    move/from16 v7, p9

    invoke-virtual/range {v0 .. v7}, Landroid/text/BoringLayout;->init(Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/text/Layout$Alignment;Landroid/text/BoringLayout$Metrics;ZZZ)V

    return-object v8
.end method
