.class public Landroid/text/BidiFormatter$DirectionalityEstimator;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/text/BidiFormatter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DirectionalityEstimator"
.end annotation


# static fields
.field private static final DIR_TYPE_CACHE:[B

.field private static final DIR_TYPE_CACHE_SIZE:I = 0x700


# instance fields
.field private charIndex:I

.field private final isHtml:Z

.field private lastChar:C

.field private final length:I

.field private final text:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x700

    new-array v1, v0, [B

    sput-object v1, Landroid/text/BidiFormatter$DirectionalityEstimator;->DIR_TYPE_CACHE:[B

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    sget-object v2, Landroid/text/BidiFormatter$DirectionalityEstimator;->DIR_TYPE_CACHE:[B

    invoke-static {v1}, Ljava/lang/Character;->getDirectionality(I)B

    move-result v3

    aput-byte v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method constructor <init>(Ljava/lang/CharSequence;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->text:Ljava/lang/CharSequence;

    iput-boolean p2, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->isHtml:Z

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    iput v0, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->length:I

    return-void
.end method

.method private static getCachedDirectionality(C)B
    .locals 1

    const/16 v0, 0x700

    if-ge p0, v0, :cond_0

    sget-object v0, Landroid/text/BidiFormatter$DirectionalityEstimator;->DIR_TYPE_CACHE:[B

    aget-byte v0, v0, p0

    goto :goto_0

    :cond_0
    invoke-static {p0}, Landroid/text/BidiFormatter$DirectionalityEstimator;->getDirectionality(I)B

    move-result v0

    :goto_0
    return v0
.end method

.method public static getDirectionality(I)B
    .locals 1

    invoke-static {p0}, Ljava/lang/Character;->getDirectionality(I)B

    move-result v0

    return v0
.end method

.method private skipEntityBackward()B
    .locals 4

    iget v0, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    :cond_0
    iget v1, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    const/16 v2, 0x3b

    if-lez v1, :cond_2

    iget-object v3, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->text:Ljava/lang/CharSequence;

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    invoke-interface {v3, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    iput-char v1, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->lastChar:C

    const/16 v3, 0x26

    if-ne v1, v3, :cond_1

    const/16 v1, 0xc

    return v1

    :cond_1
    if-ne v1, v2, :cond_0

    :cond_2
    iput v0, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    iput-char v2, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->lastChar:C

    const/16 v1, 0xd

    return v1
.end method

.method private skipEntityForward()B
    .locals 3

    :goto_0
    iget v0, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    iget v1, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->length:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->text:Ljava/lang/CharSequence;

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    invoke-interface {v1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    iput-char v0, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->lastChar:C

    const/16 v1, 0x3b

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/16 v0, 0xc

    return v0
.end method

.method private skipTagBackward()B
    .locals 4

    iget v0, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    :cond_0
    :goto_0
    iget v1, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    const/16 v2, 0x3e

    if-lez v1, :cond_5

    iget-object v3, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->text:Ljava/lang/CharSequence;

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    invoke-interface {v3, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    iput-char v1, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->lastChar:C

    const/16 v3, 0x3c

    if-ne v1, v3, :cond_1

    const/16 v1, 0xc

    return v1

    :cond_1
    if-ne v1, v2, :cond_2

    goto :goto_2

    :cond_2
    const/16 v2, 0x22

    if-eq v1, v2, :cond_3

    const/16 v2, 0x27

    if-ne v1, v2, :cond_0

    :cond_3
    iget-char v1, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->lastChar:C

    :goto_1
    iget v2, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    if-lez v2, :cond_4

    iget-object v3, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->text:Ljava/lang/CharSequence;

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    invoke-interface {v3, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    iput-char v2, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->lastChar:C

    if-eq v2, v1, :cond_4

    goto :goto_1

    :cond_4
    goto :goto_0

    :cond_5
    :goto_2
    iput v0, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    iput-char v2, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->lastChar:C

    const/16 v1, 0xd

    return v1
.end method

.method private skipTagForward()B
    .locals 5

    iget v0, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    :cond_0
    :goto_0
    iget v1, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    iget v2, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->length:I

    if-ge v1, v2, :cond_4

    iget-object v2, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->text:Ljava/lang/CharSequence;

    add-int/lit8 v3, v1, 0x1

    iput v3, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    invoke-interface {v2, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    iput-char v1, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->lastChar:C

    const/16 v2, 0x3e

    if-ne v1, v2, :cond_1

    const/16 v1, 0xc

    return v1

    :cond_1
    const/16 v2, 0x22

    if-eq v1, v2, :cond_2

    const/16 v2, 0x27

    if-ne v1, v2, :cond_0

    :cond_2
    iget-char v1, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->lastChar:C

    :goto_1
    iget v2, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    iget v3, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->length:I

    if-ge v2, v3, :cond_3

    iget-object v3, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->text:Ljava/lang/CharSequence;

    add-int/lit8 v4, v2, 0x1

    iput v4, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    invoke-interface {v3, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    iput-char v2, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->lastChar:C

    if-eq v2, v1, :cond_3

    goto :goto_1

    :cond_3
    goto :goto_0

    :cond_4
    iput v0, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    const/16 v1, 0x3c

    iput-char v1, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->lastChar:C

    const/16 v1, 0xd

    return v1
.end method


# virtual methods
.method dirTypeBackward()B
    .locals 3

    goto/32 :goto_5

    nop

    :goto_0
    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    goto/32 :goto_1f

    nop

    :goto_1
    iget-char v0, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->lastChar:C

    goto/32 :goto_16

    nop

    :goto_2
    const/16 v2, 0x3e

    goto/32 :goto_13

    nop

    :goto_3
    iget v1, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    goto/32 :goto_6

    nop

    :goto_4
    invoke-static {v0}, Ljava/lang/Character;->isLowSurrogate(C)Z

    move-result v0

    goto/32 :goto_19

    nop

    :goto_5
    iget-object v0, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->text:Ljava/lang/CharSequence;

    goto/32 :goto_b

    nop

    :goto_6
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v2

    goto/32 :goto_12

    nop

    :goto_7
    iput v0, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    goto/32 :goto_1

    nop

    :goto_8
    invoke-direct {p0}, Landroid/text/BidiFormatter$DirectionalityEstimator;->skipTagBackward()B

    move-result v0

    goto/32 :goto_9

    nop

    :goto_9
    goto :goto_15

    :goto_a
    goto/32 :goto_1c

    nop

    :goto_b
    iget v1, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    goto/32 :goto_1a

    nop

    :goto_c
    iget v0, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    goto/32 :goto_11

    nop

    :goto_d
    invoke-static {v0, v1}, Ljava/lang/Character;->codePointBefore(Ljava/lang/CharSequence;I)I

    move-result v0

    goto/32 :goto_3

    nop

    :goto_e
    iget-boolean v1, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->isHtml:Z

    goto/32 :goto_1d

    nop

    :goto_f
    iput v1, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    goto/32 :goto_1b

    nop

    :goto_10
    if-eq v1, v2, :cond_0

    goto/32 :goto_15

    :cond_0
    goto/32 :goto_14

    nop

    :goto_11
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_7

    nop

    :goto_12
    sub-int/2addr v1, v2

    goto/32 :goto_f

    nop

    :goto_13
    if-eq v1, v2, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_8

    nop

    :goto_14
    invoke-direct {p0}, Landroid/text/BidiFormatter$DirectionalityEstimator;->skipEntityBackward()B

    move-result v0

    :goto_15
    goto/32 :goto_18

    nop

    :goto_16
    invoke-static {v0}, Landroid/text/BidiFormatter$DirectionalityEstimator;->getCachedDirectionality(C)B

    move-result v0

    goto/32 :goto_e

    nop

    :goto_17
    iget-char v1, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->lastChar:C

    goto/32 :goto_2

    nop

    :goto_18
    return v0

    :goto_19
    if-nez v0, :cond_2

    goto/32 :goto_21

    :cond_2
    goto/32 :goto_22

    nop

    :goto_1a
    add-int/lit8 v1, v1, -0x1

    goto/32 :goto_0

    nop

    :goto_1b
    invoke-static {v0}, Landroid/text/BidiFormatter$DirectionalityEstimator;->getDirectionality(I)B

    move-result v1

    goto/32 :goto_20

    nop

    :goto_1c
    const/16 v2, 0x3b

    goto/32 :goto_10

    nop

    :goto_1d
    if-nez v1, :cond_3

    goto/32 :goto_15

    :cond_3
    goto/32 :goto_17

    nop

    :goto_1e
    iget v1, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    goto/32 :goto_d

    nop

    :goto_1f
    iput-char v0, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->lastChar:C

    goto/32 :goto_4

    nop

    :goto_20
    return v1

    :goto_21
    goto/32 :goto_c

    nop

    :goto_22
    iget-object v0, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->text:Ljava/lang/CharSequence;

    goto/32 :goto_1e

    nop
.end method

.method dirTypeForward()B
    .locals 3

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->text:Ljava/lang/CharSequence;

    goto/32 :goto_6

    nop

    :goto_1
    iget-char v0, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->lastChar:C

    goto/32 :goto_a

    nop

    :goto_2
    iput-char v0, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->lastChar:C

    goto/32 :goto_5

    nop

    :goto_3
    return v0

    :goto_4
    iget v0, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    goto/32 :goto_16

    nop

    :goto_5
    invoke-static {v0}, Ljava/lang/Character;->isHighSurrogate(C)Z

    move-result v0

    goto/32 :goto_20

    nop

    :goto_6
    iget v1, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    goto/32 :goto_e

    nop

    :goto_7
    invoke-static {v0, v1}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v0

    goto/32 :goto_b

    nop

    :goto_8
    return v1

    :goto_9
    goto/32 :goto_4

    nop

    :goto_a
    invoke-static {v0}, Landroid/text/BidiFormatter$DirectionalityEstimator;->getCachedDirectionality(C)B

    move-result v0

    goto/32 :goto_d

    nop

    :goto_b
    iget v1, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    goto/32 :goto_15

    nop

    :goto_c
    iput v1, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    goto/32 :goto_f

    nop

    :goto_d
    iget-boolean v1, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->isHtml:Z

    goto/32 :goto_1e

    nop

    :goto_e
    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    goto/32 :goto_2

    nop

    :goto_f
    invoke-static {v0}, Landroid/text/BidiFormatter$DirectionalityEstimator;->getDirectionality(I)B

    move-result v1

    goto/32 :goto_8

    nop

    :goto_10
    iget-char v1, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->lastChar:C

    goto/32 :goto_19

    nop

    :goto_11
    iput v0, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    goto/32 :goto_1

    nop

    :goto_12
    add-int/2addr v1, v2

    goto/32 :goto_c

    nop

    :goto_13
    if-eq v1, v2, :cond_0

    goto/32 :goto_18

    :cond_0
    goto/32 :goto_1a

    nop

    :goto_14
    iget-object v0, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->text:Ljava/lang/CharSequence;

    goto/32 :goto_21

    nop

    :goto_15
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v2

    goto/32 :goto_12

    nop

    :goto_16
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_11

    nop

    :goto_17
    goto :goto_1d

    :goto_18
    goto/32 :goto_1f

    nop

    :goto_19
    const/16 v2, 0x3c

    goto/32 :goto_13

    nop

    :goto_1a
    invoke-direct {p0}, Landroid/text/BidiFormatter$DirectionalityEstimator;->skipTagForward()B

    move-result v0

    goto/32 :goto_17

    nop

    :goto_1b
    if-eq v1, v2, :cond_1

    goto/32 :goto_1d

    :cond_1
    goto/32 :goto_1c

    nop

    :goto_1c
    invoke-direct {p0}, Landroid/text/BidiFormatter$DirectionalityEstimator;->skipEntityForward()B

    move-result v0

    :goto_1d
    goto/32 :goto_3

    nop

    :goto_1e
    if-nez v1, :cond_2

    goto/32 :goto_1d

    :cond_2
    goto/32 :goto_10

    nop

    :goto_1f
    const/16 v2, 0x26

    goto/32 :goto_1b

    nop

    :goto_20
    if-nez v0, :cond_3

    goto/32 :goto_9

    :cond_3
    goto/32 :goto_14

    nop

    :goto_21
    iget v1, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    goto/32 :goto_7

    nop
.end method

.method getEntryDir()I
    .locals 8

    goto/32 :goto_c

    nop

    :goto_0
    if-eqz v1, :cond_0

    goto/32 :goto_13

    :cond_0
    goto/32 :goto_12

    nop

    :goto_1
    const/4 v2, -0x1

    goto/32 :goto_2f

    nop

    :goto_2
    goto :goto_17

    :sswitch_0
    goto/32 :goto_3a

    nop

    :goto_3
    return v7

    :goto_4
    goto/32 :goto_39

    nop

    :goto_5
    const/4 v7, 0x1

    goto/32 :goto_18

    nop

    :goto_6
    iget v4, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    goto/32 :goto_31

    nop

    :goto_7
    const/4 v1, 0x0

    goto/32 :goto_3b

    nop

    :goto_8
    const/4 v2, 0x1

    goto/32 :goto_2

    nop

    :goto_9
    if-eq v3, v1, :cond_1

    goto/32 :goto_1b

    :cond_1
    goto/32 :goto_1a

    nop

    :goto_a
    if-nez v2, :cond_2

    goto/32 :goto_36

    :cond_2
    goto/32 :goto_35

    nop

    :goto_b
    goto :goto_17

    :sswitch_1
    goto/32 :goto_24

    nop

    :goto_c
    const/4 v0, 0x0

    goto/32 :goto_2e

    nop

    :goto_d
    invoke-virtual {p0}, Landroid/text/BidiFormatter$DirectionalityEstimator;->dirTypeBackward()B

    move-result v4

    packed-switch v4, :pswitch_data_0

    goto/32 :goto_19

    nop

    :goto_e
    goto :goto_17

    :sswitch_2
    goto/32 :goto_0

    nop

    :goto_f
    if-eqz v3, :cond_3

    goto/32 :goto_2d

    :cond_3
    goto/32 :goto_2c

    nop

    :goto_10
    goto :goto_17

    :goto_11
    goto/32 :goto_f

    nop

    :goto_12
    return v6

    :goto_13
    goto/32 :goto_33

    nop

    :goto_14
    goto/16 :goto_36

    :goto_15
    goto/32 :goto_2b

    nop

    :goto_16
    const/4 v3, 0x0

    :goto_17
    goto/32 :goto_29

    nop

    :goto_18
    if-lt v4, v5, :cond_4

    goto/32 :goto_11

    :cond_4
    goto/32 :goto_25

    nop

    :goto_19
    goto :goto_20

    :pswitch_0
    goto/32 :goto_32

    nop

    :goto_1a
    return v6

    :goto_1b
    goto/32 :goto_1f

    nop

    :goto_1c
    move v3, v1

    goto/32 :goto_e

    nop

    :goto_1d
    if-eqz v1, :cond_5

    goto/32 :goto_38

    :cond_5
    goto/32 :goto_37

    nop

    :goto_1e
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_8

    nop

    :goto_1f
    add-int/lit8 v1, v1, -0x1

    nop

    :goto_20
    goto/32 :goto_14

    nop

    :goto_21
    const/4 v2, 0x0

    goto/32 :goto_22

    nop

    :goto_22
    goto :goto_17

    :sswitch_3
    goto/32 :goto_1e

    nop

    :goto_23
    goto :goto_20

    :pswitch_1
    goto/32 :goto_9

    nop

    :goto_24
    add-int/lit8 v1, v1, -0x1

    goto/32 :goto_21

    nop

    :goto_25
    if-eqz v3, :cond_6

    goto/32 :goto_11

    :cond_6
    goto/32 :goto_3c

    nop

    :goto_26
    goto :goto_17

    :sswitch_4
    goto/32 :goto_1d

    nop

    :goto_27
    if-eq v3, v1, :cond_7

    goto/32 :goto_4

    :cond_7
    goto/32 :goto_3

    nop

    :goto_28
    const/4 v6, -0x1

    goto/32 :goto_5

    nop

    :goto_29
    iget v4, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    goto/32 :goto_34

    nop

    :goto_2a
    move v3, v1

    goto/32 :goto_b

    nop

    :goto_2b
    return v0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_4
        0x2 -> :sswitch_4
        0x9 -> :sswitch_5
        0xe -> :sswitch_0
        0xf -> :sswitch_0
        0x10 -> :sswitch_3
        0x11 -> :sswitch_3
        0x12 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0xe
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
    .end packed-switch

    :goto_2c
    return v0

    :goto_2d
    goto/32 :goto_a

    nop

    :goto_2e
    iput v0, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    goto/32 :goto_7

    nop

    :goto_2f
    goto/16 :goto_17

    :sswitch_5
    goto/32 :goto_26

    nop

    :goto_30
    goto/16 :goto_20

    :pswitch_2
    goto/32 :goto_27

    nop

    :goto_31
    if-gtz v4, :cond_8

    goto/32 :goto_15

    :cond_8
    goto/32 :goto_d

    nop

    :goto_32
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_30

    nop

    :goto_33
    move v3, v1

    goto/32 :goto_10

    nop

    :goto_34
    iget v5, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->length:I

    goto/32 :goto_28

    nop

    :goto_35
    return v2

    :goto_36
    goto/32 :goto_6

    nop

    :goto_37
    return v7

    :goto_38
    goto/32 :goto_1c

    nop

    :goto_39
    add-int/lit8 v1, v1, -0x1

    goto/32 :goto_23

    nop

    :goto_3a
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_1

    nop

    :goto_3b
    const/4 v2, 0x0

    goto/32 :goto_16

    nop

    :goto_3c
    invoke-virtual {p0}, Landroid/text/BidiFormatter$DirectionalityEstimator;->dirTypeForward()B

    move-result v4

    sparse-switch v4, :sswitch_data_0

    goto/32 :goto_2a

    nop
.end method

.method getExitDir()I
    .locals 5

    goto/32 :goto_27

    nop

    :goto_0
    if-gtz v2, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_24

    nop

    :goto_1
    goto/16 :goto_1a

    :sswitch_0
    goto/32 :goto_d

    nop

    :goto_2
    const/4 v4, -0x1

    sparse-switch v2, :sswitch_data_0

    goto/32 :goto_8

    nop

    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_1b

    nop

    :goto_4
    const/4 v2, 0x0

    goto/32 :goto_e

    nop

    :goto_5
    if-eq v1, v0, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_a

    nop

    :goto_6
    goto/16 :goto_1a

    :goto_7
    goto/32 :goto_4

    nop

    :goto_8
    if-eqz v1, :cond_2

    goto/32 :goto_1a

    :cond_2
    goto/32 :goto_17

    nop

    :goto_9
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_1

    nop

    :goto_a
    return v3

    :goto_b
    goto/32 :goto_9

    nop

    :goto_c
    move v1, v0

    goto/32 :goto_f

    nop

    :goto_d
    if-eq v1, v0, :cond_3

    goto/32 :goto_26

    :cond_3
    goto/32 :goto_25

    nop

    :goto_e
    return v2

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_3
        0x2 -> :sswitch_3
        0x9 -> :sswitch_2
        0xe -> :sswitch_0
        0xf -> :sswitch_0
        0x10 -> :sswitch_4
        0x11 -> :sswitch_4
        0x12 -> :sswitch_5
    .end sparse-switch

    :goto_f
    goto :goto_1a

    :sswitch_1
    goto/32 :goto_18

    nop

    :goto_10
    return v3

    :goto_11
    goto/32 :goto_28

    nop

    :goto_12
    iget v2, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    goto/32 :goto_0

    nop

    :goto_13
    iput v0, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->charIndex:I

    goto/32 :goto_1c

    nop

    :goto_14
    goto :goto_1a

    :sswitch_2
    goto/32 :goto_15

    nop

    :goto_15
    goto :goto_1a

    :sswitch_3
    goto/32 :goto_20

    nop

    :goto_16
    if-eqz v1, :cond_4

    goto/32 :goto_1a

    :cond_4
    goto/32 :goto_23

    nop

    :goto_17
    move v1, v0

    goto/32 :goto_1d

    nop

    :goto_18
    if-eqz v0, :cond_5

    goto/32 :goto_22

    :cond_5
    goto/32 :goto_21

    nop

    :goto_19
    const/4 v1, 0x0

    :goto_1a
    goto/32 :goto_12

    nop

    :goto_1b
    goto :goto_1a

    :sswitch_4
    goto/32 :goto_5

    nop

    :goto_1c
    const/4 v0, 0x0

    goto/32 :goto_19

    nop

    :goto_1d
    goto :goto_1a

    :sswitch_5
    goto/32 :goto_3

    nop

    :goto_1e
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_14

    nop

    :goto_1f
    const/4 v3, 0x1

    goto/32 :goto_2

    nop

    :goto_20
    if-eqz v0, :cond_6

    goto/32 :goto_11

    :cond_6
    goto/32 :goto_10

    nop

    :goto_21
    return v4

    :goto_22
    goto/32 :goto_16

    nop

    :goto_23
    move v1, v0

    goto/32 :goto_6

    nop

    :goto_24
    invoke-virtual {p0}, Landroid/text/BidiFormatter$DirectionalityEstimator;->dirTypeBackward()B

    move-result v2

    goto/32 :goto_1f

    nop

    :goto_25
    return v4

    :goto_26
    goto/32 :goto_1e

    nop

    :goto_27
    iget v0, p0, Landroid/text/BidiFormatter$DirectionalityEstimator;->length:I

    goto/32 :goto_13

    nop

    :goto_28
    if-eqz v1, :cond_7

    goto/32 :goto_1a

    :cond_7
    goto/32 :goto_c

    nop
.end method
