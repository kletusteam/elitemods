.class public Landroid/text/TextLine;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/text/TextLine$DecorationInfo;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final TAB_CHAR:C = '\t'

.field private static final TAB_INCREMENT:I = 0x14

.field private static final sCached:[Landroid/text/TextLine;


# instance fields
.field private final mActivePaint:Landroid/text/TextPaint;

.field private mAddedWidthForJustify:F

.field private final mCharacterStyleSpanSet:Landroid/text/SpanSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/text/SpanSet<",
            "Landroid/text/style/CharacterStyle;",
            ">;"
        }
    .end annotation
.end field

.field private mChars:[C

.field private mCharsValid:Z

.field private mComputed:Landroid/text/PrecomputedText;

.field private final mDecorationInfo:Landroid/text/TextLine$DecorationInfo;

.field private final mDecorations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/text/TextLine$DecorationInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mDir:I

.field private mDirections:Landroid/text/Layout$Directions;

.field private mEllipsisEnd:I

.field private mEllipsisStart:I

.field private mHasTabs:Z

.field private mIsJustifying:Z

.field private mLen:I

.field private final mMetricAffectingSpanSpanSet:Landroid/text/SpanSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/text/SpanSet<",
            "Landroid/text/style/MetricAffectingSpan;",
            ">;"
        }
    .end annotation
.end field

.field private mPaint:Landroid/text/TextPaint;

.field private final mReplacementSpanSpanSet:Landroid/text/SpanSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/text/SpanSet<",
            "Landroid/text/style/ReplacementSpan;",
            ">;"
        }
    .end annotation
.end field

.field private mSpanned:Landroid/text/Spanned;

.field private mStart:I

.field private mTabs:Landroid/text/Layout$TabStops;

.field private mText:Ljava/lang/CharSequence;

.field private mUseFallbackExtent:Z

.field private final mWorkPaint:Landroid/text/TextPaint;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x3

    new-array v0, v0, [Landroid/text/TextLine;

    sput-object v0, Landroid/text/TextLine;->sCached:[Landroid/text/TextLine;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/text/TextLine;->mUseFallbackExtent:Z

    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, Landroid/text/TextLine;->mWorkPaint:Landroid/text/TextPaint;

    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, Landroid/text/TextLine;->mActivePaint:Landroid/text/TextPaint;

    new-instance v0, Landroid/text/SpanSet;

    const-class v1, Landroid/text/style/MetricAffectingSpan;

    invoke-direct {v0, v1}, Landroid/text/SpanSet;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Landroid/text/TextLine;->mMetricAffectingSpanSpanSet:Landroid/text/SpanSet;

    new-instance v0, Landroid/text/SpanSet;

    const-class v1, Landroid/text/style/CharacterStyle;

    invoke-direct {v0, v1}, Landroid/text/SpanSet;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Landroid/text/TextLine;->mCharacterStyleSpanSet:Landroid/text/SpanSet;

    new-instance v0, Landroid/text/SpanSet;

    const-class v1, Landroid/text/style/ReplacementSpan;

    invoke-direct {v0, v1}, Landroid/text/SpanSet;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Landroid/text/TextLine;->mReplacementSpanSpanSet:Landroid/text/SpanSet;

    new-instance v0, Landroid/text/TextLine$DecorationInfo;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/text/TextLine$DecorationInfo;-><init>(Landroid/text/TextLine$DecorationInfo-IA;)V

    iput-object v0, p0, Landroid/text/TextLine;->mDecorationInfo:Landroid/text/TextLine$DecorationInfo;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/text/TextLine;->mDecorations:Ljava/util/ArrayList;

    return-void
.end method

.method private adjustEndHyphenEdit(II)I
    .locals 1

    iget v0, p0, Landroid/text/TextLine;->mLen:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    move v0, p2

    :goto_0
    return v0
.end method

.method private adjustStartHyphenEdit(II)I
    .locals 1

    if-lez p1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    move v0, p2

    :goto_0
    return v0
.end method

.method private charAt(I)C
    .locals 2

    iget-boolean v0, p0, Landroid/text/TextLine;->mCharsValid:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/text/TextLine;->mChars:[C

    aget-char v0, v0, p1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Landroid/text/TextLine;->mText:Ljava/lang/CharSequence;

    iget v1, p0, Landroid/text/TextLine;->mStart:I

    add-int/2addr v1, p1

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    :goto_0
    return v0
.end method

.method private countStretchableSpaces(II)I
    .locals 4

    const/4 v0, 0x0

    move v1, p1

    :goto_0
    if-ge v1, p2, :cond_2

    iget-boolean v2, p0, Landroid/text/TextLine;->mCharsValid:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/text/TextLine;->mChars:[C

    aget-char v2, v2, v1

    goto :goto_1

    :cond_0
    iget-object v2, p0, Landroid/text/TextLine;->mText:Ljava/lang/CharSequence;

    iget v3, p0, Landroid/text/TextLine;->mStart:I

    add-int/2addr v3, v1

    invoke-interface {v2, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    :goto_1
    invoke-direct {p0, v2}, Landroid/text/TextLine;->isStretchableWhitespace(I)Z

    move-result v3

    if-eqz v3, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return v0
.end method

.method private drawRun(Landroid/graphics/Canvas;IIZFIIIZ)F
    .locals 16

    move-object/from16 v13, p0

    iget v0, v13, Landroid/text/TextLine;->mDir:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    move/from16 v14, p4

    if-ne v1, v14, :cond_1

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p3

    move/from16 v4, p4

    invoke-direct/range {v0 .. v5}, Landroid/text/TextLine;->measureRun(IIIZLandroid/graphics/Paint$FontMetricsInt;)F

    move-result v0

    neg-float v15, v0

    const/4 v6, 0x0

    add-float v7, p5, v15

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v0, p0

    move-object/from16 v5, p1

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    invoke-direct/range {v0 .. v12}, Landroid/text/TextLine;->handleRun(IIIZLandroid/graphics/Canvas;Landroid/text/TextShaper$GlyphsConsumer;FIIILandroid/graphics/Paint$FontMetricsInt;Z)F

    return v15

    :cond_1
    const/4 v6, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p3

    move/from16 v4, p4

    move-object/from16 v5, p1

    move/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    move/from16 v12, p9

    invoke-direct/range {v0 .. v12}, Landroid/text/TextLine;->handleRun(IIIZLandroid/graphics/Canvas;Landroid/text/TextShaper$GlyphsConsumer;FIIILandroid/graphics/Paint$FontMetricsInt;Z)F

    move-result v0

    return v0
.end method

.method private static drawStroke(Landroid/text/TextPaint;Landroid/graphics/Canvas;IFFFFF)V
    .locals 12

    move-object v6, p0

    iget v0, v6, Landroid/text/TextPaint;->baselineShift:I

    int-to-float v0, v0

    add-float v0, p7, v0

    add-float v7, v0, p3

    invoke-virtual {p0}, Landroid/text/TextPaint;->getColor()I

    move-result v8

    invoke-virtual {p0}, Landroid/text/TextPaint;->getStyle()Landroid/graphics/Paint$Style;

    move-result-object v9

    invoke-virtual {p0}, Landroid/text/TextPaint;->isAntiAlias()Z

    move-result v10

    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p0, v0}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    move v11, p2

    invoke-virtual {p0, p2}, Landroid/text/TextPaint;->setColor(I)V

    add-float v4, v7, p4

    move-object v0, p1

    move/from16 v1, p5

    move v2, v7

    move/from16 v3, p6

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    invoke-virtual {p0, v9}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {p0, v8}, Landroid/text/TextPaint;->setColor(I)V

    invoke-virtual {p0, v10}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    return-void
.end method

.method private drawTextRun(Landroid/graphics/Canvas;Landroid/text/TextPaint;IIIIZFI)V
    .locals 22

    move-object/from16 v0, p0

    move/from16 v11, p9

    iget-boolean v1, v0, Landroid/text/TextLine;->mCharsValid:Z

    if-eqz v1, :cond_0

    sub-int v12, p4, p3

    sub-int v13, p6, p5

    iget-object v2, v0, Landroid/text/TextLine;->mChars:[C

    int-to-float v8, v11

    move-object/from16 v1, p1

    move/from16 v3, p3

    move v4, v12

    move/from16 v5, p5

    move v6, v13

    move/from16 v7, p8

    move/from16 v9, p7

    move-object/from16 v10, p2

    invoke-virtual/range {v1 .. v10}, Landroid/graphics/Canvas;->drawTextRun([CIIIIFFZLandroid/graphics/Paint;)V

    goto :goto_0

    :cond_0
    iget v1, v0, Landroid/text/TextLine;->mStart:I

    iget-object v13, v0, Landroid/text/TextLine;->mText:Ljava/lang/CharSequence;

    add-int v14, v1, p3

    add-int v15, v1, p4

    add-int v16, v1, p5

    add-int v17, v1, p6

    int-to-float v2, v11

    move-object/from16 v12, p1

    move/from16 v18, p8

    move/from16 v19, v2

    move/from16 v20, p7

    move-object/from16 v21, p2

    invoke-virtual/range {v12 .. v21}, Landroid/graphics/Canvas;->drawTextRun(Ljava/lang/CharSequence;IIIIFFZLandroid/graphics/Paint;)V

    :goto_0
    return-void
.end method

.method private static equalAttributes(Landroid/text/TextPaint;Landroid/text/TextPaint;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/text/TextPaint;->getColorFilter()Landroid/graphics/ColorFilter;

    move-result-object v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getColorFilter()Landroid/graphics/ColorFilter;

    move-result-object v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->getMaskFilter()Landroid/graphics/MaskFilter;

    move-result-object v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getMaskFilter()Landroid/graphics/MaskFilter;

    move-result-object v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->getShader()Landroid/graphics/Shader;

    move-result-object v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getShader()Landroid/graphics/Shader;

    move-result-object v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->getXfermode()Landroid/graphics/Xfermode;

    move-result-object v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getXfermode()Landroid/graphics/Xfermode;

    move-result-object v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->getTextLocales()Landroid/os/LocaleList;

    move-result-object v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getTextLocales()Landroid/os/LocaleList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/LocaleList;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->getFontFeatureSettings()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getFontFeatureSettings()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->getFontVariationSettings()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getFontVariationSettings()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->getShadowLayerRadius()F

    move-result v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getShadowLayerRadius()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->getShadowLayerDx()F

    move-result v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getShadowLayerDx()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->getShadowLayerDy()F

    move-result v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getShadowLayerDy()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->getShadowLayerColor()I

    move-result v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getShadowLayerColor()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->getFlags()I

    move-result v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getFlags()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->getHinting()I

    move-result v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getHinting()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->getStyle()Landroid/graphics/Paint$Style;

    move-result-object v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getStyle()Landroid/graphics/Paint$Style;

    move-result-object v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->getColor()I

    move-result v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getColor()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->getStrokeWidth()F

    move-result v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getStrokeWidth()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->getStrokeMiter()F

    move-result v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getStrokeMiter()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->getStrokeCap()Landroid/graphics/Paint$Cap;

    move-result-object v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getStrokeCap()Landroid/graphics/Paint$Cap;

    move-result-object v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->getStrokeJoin()Landroid/graphics/Paint$Join;

    move-result-object v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getStrokeJoin()Landroid/graphics/Paint$Join;

    move-result-object v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->getTextAlign()Landroid/graphics/Paint$Align;

    move-result-object v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getTextAlign()Landroid/graphics/Paint$Align;

    move-result-object v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->isElegantTextHeight()Z

    move-result v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->isElegantTextHeight()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->getTextSize()F

    move-result v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getTextSize()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->getTextScaleX()F

    move-result v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getTextScaleX()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->getTextSkewX()F

    move-result v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getTextSkewX()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->getLetterSpacing()F

    move-result v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getLetterSpacing()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->getWordSpacing()F

    move-result v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getWordSpacing()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->getStartHyphenEdit()I

    move-result v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getStartHyphenEdit()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/text/TextPaint;->getEndHyphenEdit()I

    move-result v0

    invoke-virtual {p1}, Landroid/text/TextPaint;->getEndHyphenEdit()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget v0, p0, Landroid/text/TextPaint;->bgColor:I

    iget v1, p1, Landroid/text/TextPaint;->bgColor:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Landroid/text/TextPaint;->baselineShift:I

    iget v1, p1, Landroid/text/TextPaint;->baselineShift:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Landroid/text/TextPaint;->linkColor:I

    iget v1, p1, Landroid/text/TextPaint;->linkColor:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Landroid/text/TextPaint;->drawableState:[I

    iget-object v1, p1, Landroid/text/TextPaint;->drawableState:[I

    if-ne v0, v1, :cond_0

    iget v0, p0, Landroid/text/TextPaint;->density:F

    iget v1, p1, Landroid/text/TextPaint;->density:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p0, Landroid/text/TextPaint;->underlineColor:I

    iget v1, p1, Landroid/text/TextPaint;->underlineColor:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Landroid/text/TextPaint;->underlineThickness:F

    iget v1, p1, Landroid/text/TextPaint;->underlineThickness:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private static expandMetricsFromPaint(Landroid/graphics/Paint$FontMetricsInt;Landroid/text/TextPaint;)V
    .locals 11

    iget v6, p0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    iget v7, p0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    iget v8, p0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    iget v9, p0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iget v10, p0, Landroid/graphics/Paint$FontMetricsInt;->leading:I

    invoke-virtual {p1, p0}, Landroid/text/TextPaint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    move-object v0, p0

    move v1, v6

    move v2, v7

    move v3, v8

    move v4, v9

    move v5, v10

    invoke-static/range {v0 .. v5}, Landroid/text/TextLine;->updateMetrics(Landroid/graphics/Paint$FontMetricsInt;IIIII)V

    return-void
.end method

.method private expandMetricsFromPaint(Landroid/text/TextPaint;IIIIZLandroid/graphics/Paint$FontMetricsInt;)V
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v9, p7

    iget v10, v9, Landroid/graphics/Paint$FontMetricsInt;->top:I

    iget v11, v9, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    iget v12, v9, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    iget v13, v9, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iget v14, v9, Landroid/graphics/Paint$FontMetricsInt;->leading:I

    sub-int v15, p3, p2

    sub-int v16, p5, p4

    iget-boolean v1, v0, Landroid/text/TextLine;->mCharsValid:Z

    if-eqz v1, :cond_0

    iget-object v2, v0, Landroid/text/TextLine;->mChars:[C

    move-object/from16 v1, p1

    move/from16 v3, p2

    move v4, v15

    move/from16 v5, p4

    move/from16 v6, v16

    move/from16 v7, p6

    move-object/from16 v8, p7

    invoke-virtual/range {v1 .. v8}, Landroid/text/TextPaint;->getFontMetricsInt([CIIIIZLandroid/graphics/Paint$FontMetricsInt;)V

    goto :goto_0

    :cond_0
    iget-object v1, v0, Landroid/text/TextLine;->mComputed:Landroid/text/PrecomputedText;

    if-nez v1, :cond_1

    iget-object v2, v0, Landroid/text/TextLine;->mText:Ljava/lang/CharSequence;

    iget v1, v0, Landroid/text/TextLine;->mStart:I

    add-int v3, v1, p2

    add-int v5, v1, p4

    move-object/from16 v1, p1

    move v4, v15

    move/from16 v6, v16

    move/from16 v7, p6

    move-object/from16 v8, p7

    invoke-virtual/range {v1 .. v8}, Landroid/text/TextPaint;->getFontMetricsInt(Ljava/lang/CharSequence;IIIIZLandroid/graphics/Paint$FontMetricsInt;)V

    goto :goto_0

    :cond_1
    iget v2, v0, Landroid/text/TextLine;->mStart:I

    add-int v3, v2, p2

    add-int v2, v2, p3

    invoke-virtual {v1, v3, v2, v9}, Landroid/text/PrecomputedText;->getFontMetricsInt(IILandroid/graphics/Paint$FontMetricsInt;)V

    :goto_0
    move-object/from16 v1, p7

    move v2, v10

    move v3, v11

    move v4, v12

    move v5, v13

    move v6, v14

    invoke-static/range {v1 .. v6}, Landroid/text/TextLine;->updateMetrics(Landroid/graphics/Paint$FontMetricsInt;IIIII)V

    return-void
.end method

.method private extractDecorationInfo(Landroid/text/TextPaint;Landroid/text/TextLine$DecorationInfo;)V
    .locals 2

    invoke-virtual {p1}, Landroid/text/TextPaint;->isStrikeThruText()Z

    move-result v0

    iput-boolean v0, p2, Landroid/text/TextLine$DecorationInfo;->isStrikeThruText:Z

    iget-boolean v0, p2, Landroid/text/TextLine$DecorationInfo;->isStrikeThruText:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v1}, Landroid/text/TextPaint;->setStrikeThruText(Z)V

    :cond_0
    invoke-virtual {p1}, Landroid/text/TextPaint;->isUnderlineText()Z

    move-result v0

    iput-boolean v0, p2, Landroid/text/TextLine$DecorationInfo;->isUnderlineText:Z

    iget-boolean v0, p2, Landroid/text/TextLine$DecorationInfo;->isUnderlineText:Z

    if-eqz v0, :cond_1

    invoke-virtual {p1, v1}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    :cond_1
    iget v0, p1, Landroid/text/TextPaint;->underlineColor:I

    iput v0, p2, Landroid/text/TextLine$DecorationInfo;->underlineColor:I

    iget v0, p1, Landroid/text/TextPaint;->underlineThickness:F

    iput v0, p2, Landroid/text/TextLine$DecorationInfo;->underlineThickness:F

    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0}, Landroid/text/TextPaint;->setUnderlineText(IF)V

    return-void
.end method

.method private getOffsetBeforeAfter(IIIZIZ)I
    .locals 19

    move-object/from16 v0, p0

    move/from16 v1, p3

    move/from16 v9, p5

    if-ltz p1, :cond_d

    const/4 v2, 0x0

    if-eqz p6, :cond_0

    iget v3, v0, Landroid/text/TextLine;->mLen:I

    goto :goto_0

    :cond_0
    move v3, v2

    :goto_0
    if-ne v9, v3, :cond_1

    goto/16 :goto_9

    :cond_1
    iget-object v15, v0, Landroid/text/TextLine;->mWorkPaint:Landroid/text/TextPaint;

    iget-object v3, v0, Landroid/text/TextLine;->mPaint:Landroid/text/TextPaint;

    invoke-virtual {v15, v3}, Landroid/text/TextPaint;->set(Landroid/text/TextPaint;)V

    iget-boolean v3, v0, Landroid/text/TextLine;->mIsJustifying:Z

    if-eqz v3, :cond_2

    iget v3, v0, Landroid/text/TextLine;->mAddedWidthForJustify:F

    invoke-virtual {v15, v3}, Landroid/text/TextPaint;->setWordSpacing(F)V

    :cond_2
    move/from16 v3, p2

    iget-object v4, v0, Landroid/text/TextLine;->mSpanned:Landroid/text/Spanned;

    if-eqz v4, :cond_a

    move/from16 v14, p2

    if-ne v14, v1, :cond_3

    goto :goto_6

    :cond_3
    if-eqz p6, :cond_4

    add-int/lit8 v4, v9, 0x1

    goto :goto_1

    :cond_4
    move v4, v9

    :goto_1
    iget v5, v0, Landroid/text/TextLine;->mStart:I

    add-int/2addr v5, v1

    :goto_2
    iget-object v6, v0, Landroid/text/TextLine;->mSpanned:Landroid/text/Spanned;

    iget v7, v0, Landroid/text/TextLine;->mStart:I

    add-int/2addr v7, v3

    const-class v8, Landroid/text/style/MetricAffectingSpan;

    invoke-interface {v6, v7, v5, v8}, Landroid/text/Spanned;->nextSpanTransition(IILjava/lang/Class;)I

    move-result v6

    iget v7, v0, Landroid/text/TextLine;->mStart:I

    sub-int/2addr v6, v7

    if-lt v6, v4, :cond_9

    nop

    iget-object v8, v0, Landroid/text/TextLine;->mSpanned:Landroid/text/Spanned;

    add-int v10, v7, v3

    add-int/2addr v7, v6

    const-class v11, Landroid/text/style/MetricAffectingSpan;

    invoke-interface {v8, v10, v7, v11}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Landroid/text/style/MetricAffectingSpan;

    iget-object v8, v0, Landroid/text/TextLine;->mSpanned:Landroid/text/Spanned;

    const-class v10, Landroid/text/style/MetricAffectingSpan;

    invoke-static {v7, v8, v10}, Landroid/text/TextUtils;->removeEmptySpans([Ljava/lang/Object;Landroid/text/Spanned;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v8

    move-object v7, v8

    check-cast v7, [Landroid/text/style/MetricAffectingSpan;

    array-length v8, v7

    if-lez v8, :cond_8

    const/4 v8, 0x0

    const/4 v10, 0x0

    :goto_3
    array-length v11, v7

    if-ge v10, v11, :cond_6

    aget-object v11, v7, v10

    instance-of v12, v11, Landroid/text/style/ReplacementSpan;

    if-eqz v12, :cond_5

    move-object v8, v11

    check-cast v8, Landroid/text/style/ReplacementSpan;

    goto :goto_4

    :cond_5
    invoke-virtual {v11, v15}, Landroid/text/style/MetricAffectingSpan;->updateMeasureState(Landroid/text/TextPaint;)V

    :goto_4
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    :cond_6
    if-eqz v8, :cond_8

    if-eqz p6, :cond_7

    move v2, v6

    goto :goto_5

    :cond_7
    move v2, v3

    :goto_5
    return v2

    :cond_8
    move/from16 v17, v3

    move/from16 v18, v6

    goto :goto_7

    :cond_9
    move v3, v6

    goto :goto_2

    :cond_a
    move/from16 v14, p2

    :goto_6
    move/from16 v6, p3

    move/from16 v17, v3

    move/from16 v18, v6

    :goto_7
    if-eqz p6, :cond_b

    goto :goto_8

    :cond_b
    const/4 v2, 0x2

    :goto_8
    move v8, v2

    iget-boolean v2, v0, Landroid/text/TextLine;->mCharsValid:Z

    if-eqz v2, :cond_c

    iget-object v3, v0, Landroid/text/TextLine;->mChars:[C

    sub-int v5, v18, v17

    move-object v2, v15

    move/from16 v4, v17

    move/from16 v6, p4

    move/from16 v7, p5

    invoke-virtual/range {v2 .. v8}, Landroid/text/TextPaint;->getTextRunCursor([CIIZII)I

    move-result v2

    return v2

    :cond_c
    iget-object v11, v0, Landroid/text/TextLine;->mText:Ljava/lang/CharSequence;

    iget v2, v0, Landroid/text/TextLine;->mStart:I

    add-int v12, v2, v17

    add-int v13, v2, v18

    add-int/2addr v2, v9

    move-object v10, v15

    move/from16 v14, p4

    move-object v3, v15

    move v15, v2

    move/from16 v16, v8

    invoke-virtual/range {v10 .. v16}, Landroid/text/TextPaint;->getTextRunCursor(Ljava/lang/CharSequence;IIZII)I

    move-result v2

    iget v4, v0, Landroid/text/TextLine;->mStart:I

    sub-int/2addr v2, v4

    return v2

    :cond_d
    :goto_9
    if-eqz p6, :cond_e

    iget-object v2, v0, Landroid/text/TextLine;->mText:Ljava/lang/CharSequence;

    iget v3, v0, Landroid/text/TextLine;->mStart:I

    add-int/2addr v3, v9

    invoke-static {v2, v3}, Landroid/text/TextUtils;->getOffsetAfter(Ljava/lang/CharSequence;I)I

    move-result v2

    iget v3, v0, Landroid/text/TextLine;->mStart:I

    sub-int/2addr v2, v3

    return v2

    :cond_e
    iget-object v2, v0, Landroid/text/TextLine;->mText:Ljava/lang/CharSequence;

    iget v3, v0, Landroid/text/TextLine;->mStart:I

    add-int/2addr v3, v9

    invoke-static {v2, v3}, Landroid/text/TextUtils;->getOffsetBefore(Ljava/lang/CharSequence;I)I

    move-result v2

    iget v3, v0, Landroid/text/TextLine;->mStart:I

    sub-int/2addr v2, v3

    return v2
.end method

.method private getRunAdvance(Landroid/text/TextPaint;IIIIZI)F
    .locals 11

    move-object v0, p0

    iget-boolean v1, v0, Landroid/text/TextLine;->mCharsValid:Z

    if-eqz v1, :cond_0

    iget-object v2, v0, Landroid/text/TextLine;->mChars:[C

    move-object v1, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    invoke-virtual/range {v1 .. v8}, Landroid/text/TextPaint;->getRunAdvance([CIIIIZI)F

    move-result v1

    return v1

    :cond_0
    iget v1, v0, Landroid/text/TextLine;->mStart:I

    iget-object v2, v0, Landroid/text/TextLine;->mComputed:Landroid/text/PrecomputedText;

    if-nez v2, :cond_1

    iget-object v4, v0, Landroid/text/TextLine;->mText:Ljava/lang/CharSequence;

    add-int v5, v1, p2

    add-int v6, v1, p3

    add-int v7, v1, p4

    add-int v8, v1, p5

    add-int v10, v1, p7

    move-object v3, p1

    move/from16 v9, p6

    invoke-virtual/range {v3 .. v10}, Landroid/text/TextPaint;->getRunAdvance(Ljava/lang/CharSequence;IIIIZI)F

    move-result v2

    return v2

    :cond_1
    add-int v3, p2, v1

    add-int v4, p3, v1

    invoke-virtual {v2, v3, v4}, Landroid/text/PrecomputedText;->getWidth(II)F

    move-result v2

    return v2
.end method

.method private handleReplacement(Landroid/text/style/ReplacementSpan;Landroid/text/TextPaint;IIZLandroid/graphics/Canvas;FIIILandroid/graphics/Paint$FontMetricsInt;Z)F
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v11, p11

    const/4 v7, 0x0

    iget v1, v0, Landroid/text/TextLine;->mStart:I

    add-int v12, v1, p3

    add-int v13, v1, p4

    if-nez p12, :cond_1

    if-eqz p6, :cond_0

    if-eqz p5, :cond_0

    goto :goto_1

    :cond_0
    :goto_0
    move v14, v7

    goto :goto_4

    :cond_1
    :goto_1
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    if-eqz v11, :cond_2

    const/4 v6, 0x1

    goto :goto_2

    :cond_2
    const/4 v6, 0x0

    :goto_2
    move v8, v6

    if-eqz v8, :cond_3

    iget v1, v11, Landroid/graphics/Paint$FontMetricsInt;->top:I

    iget v2, v11, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    iget v3, v11, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    iget v4, v11, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iget v5, v11, Landroid/graphics/Paint$FontMetricsInt;->leading:I

    move v9, v1

    move v10, v2

    move v14, v3

    move v15, v4

    move/from16 v16, v5

    goto :goto_3

    :cond_3
    move v9, v1

    move v10, v2

    move v14, v3

    move v15, v4

    move/from16 v16, v5

    :goto_3
    iget-object v3, v0, Landroid/text/TextLine;->mText:Ljava/lang/CharSequence;

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move v4, v12

    move v5, v13

    move-object/from16 v6, p11

    invoke-virtual/range {v1 .. v6}, Landroid/text/style/ReplacementSpan;->getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I

    move-result v1

    int-to-float v7, v1

    if-eqz v8, :cond_0

    move-object/from16 v1, p11

    move v2, v9

    move v3, v10

    move v4, v14

    move v5, v15

    move/from16 v6, v16

    invoke-static/range {v1 .. v6}, Landroid/text/TextLine;->updateMetrics(Landroid/graphics/Paint$FontMetricsInt;IIIII)V

    goto :goto_0

    :goto_4
    if-eqz p6, :cond_5

    if-eqz p5, :cond_4

    sub-float v1, p7, v14

    move v15, v1

    goto :goto_5

    :cond_4
    move/from16 v15, p7

    :goto_5
    iget-object v3, v0, Landroid/text/TextLine;->mText:Ljava/lang/CharSequence;

    move-object/from16 v1, p1

    move-object/from16 v2, p6

    move v4, v12

    move v5, v13

    move v6, v15

    move/from16 v7, p8

    move/from16 v8, p9

    move/from16 v9, p10

    move-object/from16 v10, p2

    invoke-virtual/range {v1 .. v10}, Landroid/text/style/ReplacementSpan;->draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V

    goto :goto_6

    :cond_5
    move/from16 v15, p7

    :goto_6
    if-eqz p5, :cond_6

    neg-float v1, v14

    goto :goto_7

    :cond_6
    move v1, v14

    :goto_7
    return v1
.end method

.method private handleRun(IIIZLandroid/graphics/Canvas;Landroid/text/TextShaper$GlyphsConsumer;FIIILandroid/graphics/Paint$FontMetricsInt;Z)F
    .locals 34

    move-object/from16 v15, p0

    move/from16 v14, p1

    move/from16 v13, p2

    move/from16 v12, p3

    move-object/from16 v11, p11

    if-lt v13, v14, :cond_1b

    if-gt v13, v12, :cond_1b

    if-ne v14, v13, :cond_1

    iget-object v0, v15, Landroid/text/TextLine;->mWorkPaint:Landroid/text/TextPaint;

    iget-object v1, v15, Landroid/text/TextLine;->mPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->set(Landroid/text/TextPaint;)V

    if-eqz v11, :cond_0

    invoke-static {v11, v0}, Landroid/text/TextLine;->expandMetricsFromPaint(Landroid/graphics/Paint$FontMetricsInt;Landroid/text/TextPaint;)V

    :cond_0
    const/4 v1, 0x0

    return v1

    :cond_1
    iget-object v0, v15, Landroid/text/TextLine;->mSpanned:Landroid/text/Spanned;

    const/16 v17, 0x0

    const/16 v18, 0x1

    if-nez v0, :cond_2

    const/4 v0, 0x0

    move/from16 v19, v0

    goto :goto_2

    :cond_2
    iget-object v1, v15, Landroid/text/TextLine;->mMetricAffectingSpanSpanSet:Landroid/text/SpanSet;

    iget v2, v15, Landroid/text/TextLine;->mStart:I

    add-int v3, v2, v14

    add-int/2addr v2, v12

    invoke-virtual {v1, v0, v3, v2}, Landroid/text/SpanSet;->init(Landroid/text/Spanned;II)V

    iget-object v0, v15, Landroid/text/TextLine;->mCharacterStyleSpanSet:Landroid/text/SpanSet;

    iget-object v1, v15, Landroid/text/TextLine;->mSpanned:Landroid/text/Spanned;

    iget v2, v15, Landroid/text/TextLine;->mStart:I

    add-int v3, v2, v14

    add-int/2addr v2, v12

    invoke-virtual {v0, v1, v3, v2}, Landroid/text/SpanSet;->init(Landroid/text/Spanned;II)V

    iget-object v0, v15, Landroid/text/TextLine;->mMetricAffectingSpanSpanSet:Landroid/text/SpanSet;

    iget v0, v0, Landroid/text/SpanSet;->numberOfSpans:I

    if-nez v0, :cond_4

    iget-object v0, v15, Landroid/text/TextLine;->mCharacterStyleSpanSet:Landroid/text/SpanSet;

    iget v0, v0, Landroid/text/SpanSet;->numberOfSpans:I

    if-eqz v0, :cond_3

    goto :goto_0

    :cond_3
    move/from16 v0, v17

    goto :goto_1

    :cond_4
    :goto_0
    move/from16 v0, v18

    :goto_1
    move/from16 v19, v0

    :goto_2
    if-nez v19, :cond_5

    iget-object v10, v15, Landroid/text/TextLine;->mWorkPaint:Landroid/text/TextPaint;

    move-object v1, v10

    iget-object v0, v15, Landroid/text/TextLine;->mPaint:Landroid/text/TextPaint;

    invoke-virtual {v10, v0}, Landroid/text/TextPaint;->set(Landroid/text/TextPaint;)V

    invoke-virtual {v10}, Landroid/text/TextPaint;->getStartHyphenEdit()I

    move-result v0

    invoke-direct {v15, v14, v0}, Landroid/text/TextLine;->adjustStartHyphenEdit(II)I

    move-result v0

    invoke-virtual {v10, v0}, Landroid/text/TextPaint;->setStartHyphenEdit(I)V

    invoke-virtual {v10}, Landroid/text/TextPaint;->getEndHyphenEdit()I

    move-result v0

    invoke-direct {v15, v12, v0}, Landroid/text/TextLine;->adjustEndHyphenEdit(II)I

    move-result v0

    invoke-virtual {v10, v0}, Landroid/text/TextPaint;->setEndHyphenEdit(I)V

    const/16 v16, 0x0

    move-object/from16 v0, p0

    move/from16 v2, p1

    move/from16 v3, p3

    move/from16 v4, p1

    move/from16 v5, p3

    move/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move/from16 v9, p7

    move-object/from16 v17, v10

    move/from16 v10, p8

    move/from16 v11, p9

    move/from16 v12, p10

    move-object/from16 v13, p11

    move/from16 v14, p12

    move/from16 v15, p2

    invoke-direct/range {v0 .. v16}, Landroid/text/TextLine;->handleText(Landroid/text/TextPaint;IIIIZLandroid/graphics/Canvas;Landroid/text/TextShaper$GlyphsConsumer;FIIILandroid/graphics/Paint$FontMetricsInt;ZILjava/util/ArrayList;)F

    move-result v0

    return v0

    :cond_5
    move/from16 v20, p7

    move/from16 v0, p1

    move/from16 v13, p7

    move v14, v0

    :goto_3
    move/from16 v15, p2

    if-ge v14, v15, :cond_1a

    move-object/from16 v12, p0

    iget-object v11, v12, Landroid/text/TextLine;->mWorkPaint:Landroid/text/TextPaint;

    iget-object v0, v12, Landroid/text/TextLine;->mPaint:Landroid/text/TextPaint;

    invoke-virtual {v11, v0}, Landroid/text/TextPaint;->set(Landroid/text/TextPaint;)V

    iget-object v0, v12, Landroid/text/TextLine;->mMetricAffectingSpanSpanSet:Landroid/text/SpanSet;

    iget v1, v12, Landroid/text/TextLine;->mStart:I

    add-int v2, v1, v14

    move/from16 v10, p3

    add-int/2addr v1, v10

    invoke-virtual {v0, v2, v1}, Landroid/text/SpanSet;->getNextTransition(II)I

    move-result v0

    iget v1, v12, Landroid/text/TextLine;->mStart:I

    sub-int v9, v0, v1

    invoke-static {v9, v15}, Ljava/lang/Math;->min(II)I

    move-result v8

    const/4 v0, 0x0

    const/4 v1, 0x0

    move-object/from16 v21, v0

    :goto_4
    iget-object v0, v12, Landroid/text/TextLine;->mMetricAffectingSpanSpanSet:Landroid/text/SpanSet;

    iget v0, v0, Landroid/text/SpanSet;->numberOfSpans:I

    if-ge v1, v0, :cond_b

    iget-object v0, v12, Landroid/text/TextLine;->mMetricAffectingSpanSpanSet:Landroid/text/SpanSet;

    iget-object v0, v0, Landroid/text/SpanSet;->spanStarts:[I

    aget v0, v0, v1

    iget v2, v12, Landroid/text/TextLine;->mStart:I

    add-int/2addr v2, v8

    if-ge v0, v2, :cond_a

    iget-object v0, v12, Landroid/text/TextLine;->mMetricAffectingSpanSpanSet:Landroid/text/SpanSet;

    iget-object v0, v0, Landroid/text/SpanSet;->spanEnds:[I

    aget v0, v0, v1

    iget v2, v12, Landroid/text/TextLine;->mStart:I

    add-int v3, v2, v14

    if-gt v0, v3, :cond_6

    goto :goto_7

    :cond_6
    iget v0, v12, Landroid/text/TextLine;->mEllipsisStart:I

    add-int/2addr v2, v0

    iget-object v0, v12, Landroid/text/TextLine;->mMetricAffectingSpanSpanSet:Landroid/text/SpanSet;

    iget-object v0, v0, Landroid/text/SpanSet;->spanStarts:[I

    aget v0, v0, v1

    if-gt v2, v0, :cond_7

    iget-object v0, v12, Landroid/text/TextLine;->mMetricAffectingSpanSpanSet:Landroid/text/SpanSet;

    iget-object v0, v0, Landroid/text/SpanSet;->spanEnds:[I

    aget v0, v0, v1

    iget v2, v12, Landroid/text/TextLine;->mStart:I

    iget v3, v12, Landroid/text/TextLine;->mEllipsisEnd:I

    add-int/2addr v2, v3

    if-gt v0, v2, :cond_7

    move/from16 v0, v18

    goto :goto_5

    :cond_7
    move/from16 v0, v17

    :goto_5
    iget-object v2, v12, Landroid/text/TextLine;->mMetricAffectingSpanSpanSet:Landroid/text/SpanSet;

    iget-object v2, v2, Landroid/text/SpanSet;->spans:[Ljava/lang/Object;

    check-cast v2, [Landroid/text/style/MetricAffectingSpan;

    aget-object v2, v2, v1

    instance-of v3, v2, Landroid/text/style/ReplacementSpan;

    if-eqz v3, :cond_9

    if-nez v0, :cond_8

    move-object v3, v2

    check-cast v3, Landroid/text/style/ReplacementSpan;

    goto :goto_6

    :cond_8
    const/4 v3, 0x0

    :goto_6
    move-object/from16 v21, v3

    goto :goto_7

    :cond_9
    invoke-virtual {v2, v11}, Landroid/text/style/MetricAffectingSpan;->updateDrawState(Landroid/text/TextPaint;)V

    :cond_a
    :goto_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_b
    if-eqz v21, :cond_e

    if-nez p12, :cond_d

    if-ge v8, v15, :cond_c

    goto :goto_8

    :cond_c
    move/from16 v16, v17

    goto :goto_9

    :cond_d
    :goto_8
    move/from16 v16, v18

    :goto_9
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object v2, v11

    move v3, v14

    move v4, v8

    move/from16 v5, p4

    move-object/from16 v6, p5

    move v7, v13

    move/from16 p7, v8

    move/from16 v8, p8

    move/from16 v22, v9

    move/from16 v9, p9

    move/from16 v10, p10

    move-object v15, v11

    move-object/from16 v11, p11

    move-object/from16 v23, v15

    move-object v15, v12

    move/from16 v12, v16

    invoke-direct/range {v0 .. v12}, Landroid/text/TextLine;->handleReplacement(Landroid/text/style/ReplacementSpan;Landroid/text/TextPaint;IIZLandroid/graphics/Canvas;FIIILandroid/graphics/Paint$FontMetricsInt;Z)F

    move-result v0

    add-float/2addr v13, v0

    move/from16 v33, v14

    goto/16 :goto_13

    :cond_e
    move/from16 p7, v8

    move/from16 v22, v9

    move-object/from16 v23, v11

    move-object v15, v12

    iget-object v12, v15, Landroid/text/TextLine;->mActivePaint:Landroid/text/TextPaint;

    iget-object v0, v15, Landroid/text/TextLine;->mPaint:Landroid/text/TextPaint;

    invoke-virtual {v12, v0}, Landroid/text/TextPaint;->set(Landroid/text/TextPaint;)V

    move v0, v14

    move/from16 v1, p7

    iget-object v11, v15, Landroid/text/TextLine;->mDecorationInfo:Landroid/text/TextLine$DecorationInfo;

    iget-object v2, v15, Landroid/text/TextLine;->mDecorations:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    move v2, v14

    move v10, v1

    move v9, v2

    move/from16 v24, v13

    move v13, v0

    :goto_a
    move/from16 v8, p7

    if-ge v9, v8, :cond_17

    iget-object v0, v15, Landroid/text/TextLine;->mCharacterStyleSpanSet:Landroid/text/SpanSet;

    iget v1, v15, Landroid/text/TextLine;->mStart:I

    add-int v2, v1, v9

    add-int v1, v1, v22

    invoke-virtual {v0, v2, v1}, Landroid/text/SpanSet;->getNextTransition(II)I

    move-result v0

    iget v1, v15, Landroid/text/TextLine;->mStart:I

    sub-int v7, v0, v1

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v25

    iget-object v0, v15, Landroid/text/TextLine;->mPaint:Landroid/text/TextPaint;

    move-object/from16 v1, v23

    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->set(Landroid/text/TextPaint;)V

    const/4 v0, 0x0

    :goto_b
    iget-object v2, v15, Landroid/text/TextLine;->mCharacterStyleSpanSet:Landroid/text/SpanSet;

    iget v2, v2, Landroid/text/SpanSet;->numberOfSpans:I

    if-ge v0, v2, :cond_11

    iget-object v2, v15, Landroid/text/TextLine;->mCharacterStyleSpanSet:Landroid/text/SpanSet;

    iget-object v2, v2, Landroid/text/SpanSet;->spanStarts:[I

    aget v2, v2, v0

    iget v3, v15, Landroid/text/TextLine;->mStart:I

    add-int v3, v3, v25

    if-ge v2, v3, :cond_10

    iget-object v2, v15, Landroid/text/TextLine;->mCharacterStyleSpanSet:Landroid/text/SpanSet;

    iget-object v2, v2, Landroid/text/SpanSet;->spanEnds:[I

    aget v2, v2, v0

    iget v3, v15, Landroid/text/TextLine;->mStart:I

    add-int/2addr v3, v9

    if-gt v2, v3, :cond_f

    goto :goto_c

    :cond_f
    iget-object v2, v15, Landroid/text/TextLine;->mCharacterStyleSpanSet:Landroid/text/SpanSet;

    iget-object v2, v2, Landroid/text/SpanSet;->spans:[Ljava/lang/Object;

    check-cast v2, [Landroid/text/style/CharacterStyle;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Landroid/text/style/CharacterStyle;->updateDrawState(Landroid/text/TextPaint;)V

    :cond_10
    :goto_c
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    :cond_11
    invoke-direct {v15, v1, v11}, Landroid/text/TextLine;->extractDecorationInfo(Landroid/text/TextPaint;Landroid/text/TextLine$DecorationInfo;)V

    if-ne v9, v14, :cond_12

    invoke-virtual {v12, v1}, Landroid/text/TextPaint;->set(Landroid/text/TextPaint;)V

    move/from16 p7, v7

    move/from16 v27, v8

    move/from16 v28, v9

    move/from16 v29, v10

    move-object/from16 v30, v11

    move/from16 v32, v13

    move/from16 v33, v14

    move-object v14, v1

    move-object v13, v12

    move-object v12, v15

    goto/16 :goto_f

    :cond_12
    invoke-static {v1, v12}, Landroid/text/TextLine;->equalAttributes(Landroid/text/TextPaint;Landroid/text/TextPaint;)Z

    move-result v0

    if-nez v0, :cond_15

    iget-object v0, v15, Landroid/text/TextLine;->mPaint:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getStartHyphenEdit()I

    move-result v0

    invoke-direct {v15, v13, v0}, Landroid/text/TextLine;->adjustStartHyphenEdit(II)I

    move-result v0

    invoke-virtual {v12, v0}, Landroid/text/TextPaint;->setStartHyphenEdit(I)V

    iget-object v0, v15, Landroid/text/TextLine;->mPaint:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getEndHyphenEdit()I

    move-result v0

    invoke-direct {v15, v10, v0}, Landroid/text/TextLine;->adjustEndHyphenEdit(II)I

    move-result v0

    invoke-virtual {v12, v0}, Landroid/text/TextPaint;->setEndHyphenEdit(I)V

    if-nez p12, :cond_14

    move/from16 v0, p2

    move-object v6, v1

    if-ge v10, v0, :cond_13

    goto :goto_d

    :cond_13
    move/from16 v23, v17

    goto :goto_e

    :cond_14
    move/from16 v0, p2

    move-object v6, v1

    :goto_d
    move/from16 v23, v18

    :goto_e
    invoke-static {v10, v8}, Ljava/lang/Math;->min(II)I

    move-result v1

    move v4, v0

    move-object v5, v15

    move v15, v1

    iget-object v0, v5, Landroid/text/TextLine;->mDecorations:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    move-object v1, v12

    move v2, v13

    move v3, v10

    move v4, v14

    move/from16 v5, v22

    move-object/from16 v26, v6

    move/from16 v6, p4

    move/from16 p7, v7

    move-object/from16 v7, p5

    move/from16 v27, v8

    move-object/from16 v8, p6

    move/from16 v28, v9

    move/from16 v9, v24

    move/from16 v29, v10

    move/from16 v10, p8

    move-object/from16 v30, v11

    move/from16 v11, p9

    move-object/from16 v31, v12

    move/from16 v12, p10

    move/from16 v32, v13

    move-object/from16 v13, p11

    move/from16 v33, v14

    move/from16 v14, v23

    invoke-direct/range {v0 .. v16}, Landroid/text/TextLine;->handleText(Landroid/text/TextPaint;IIIIZLandroid/graphics/Canvas;Landroid/text/TextShaper$GlyphsConsumer;FIIILandroid/graphics/Paint$FontMetricsInt;ZILjava/util/ArrayList;)F

    move-result v0

    add-float v24, v24, v0

    move/from16 v0, v28

    move-object/from16 v14, v26

    move-object/from16 v13, v31

    invoke-virtual {v13, v14}, Landroid/text/TextPaint;->set(Landroid/text/TextPaint;)V

    move-object/from16 v12, p0

    iget-object v1, v12, Landroid/text/TextLine;->mDecorations:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    move/from16 v32, v0

    goto :goto_f

    :cond_15
    move/from16 p7, v7

    move/from16 v27, v8

    move/from16 v28, v9

    move/from16 v29, v10

    move-object/from16 v30, v11

    move/from16 v32, v13

    move/from16 v33, v14

    move-object v14, v1

    move-object v13, v12

    move-object v12, v15

    :goto_f
    move/from16 v10, p7

    invoke-virtual/range {v30 .. v30}, Landroid/text/TextLine$DecorationInfo;->hasDecoration()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-virtual/range {v30 .. v30}, Landroid/text/TextLine$DecorationInfo;->copyInfo()Landroid/text/TextLine$DecorationInfo;

    move-result-object v0

    move/from16 v2, v28

    iput v2, v0, Landroid/text/TextLine$DecorationInfo;->start:I

    move/from16 v1, p7

    iput v1, v0, Landroid/text/TextLine$DecorationInfo;->end:I

    iget-object v3, v12, Landroid/text/TextLine;->mDecorations:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_10

    :cond_16
    move/from16 v1, p7

    move/from16 v2, v28

    :goto_10
    move v9, v1

    move-object v15, v12

    move-object v12, v13

    move-object/from16 v23, v14

    move/from16 p7, v27

    move-object/from16 v11, v30

    move/from16 v13, v32

    move/from16 v14, v33

    goto/16 :goto_a

    :cond_17
    move/from16 v27, v8

    move v2, v9

    move/from16 v29, v10

    move-object/from16 v30, v11

    move/from16 v32, v13

    move/from16 v33, v14

    move-object/from16 v14, v23

    move-object v13, v12

    move-object v12, v15

    iget-object v0, v12, Landroid/text/TextLine;->mPaint:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getStartHyphenEdit()I

    move-result v0

    move/from16 v11, v32

    invoke-direct {v12, v11, v0}, Landroid/text/TextLine;->adjustStartHyphenEdit(II)I

    move-result v0

    invoke-virtual {v13, v0}, Landroid/text/TextPaint;->setStartHyphenEdit(I)V

    iget-object v0, v12, Landroid/text/TextLine;->mPaint:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getEndHyphenEdit()I

    move-result v0

    invoke-direct {v12, v10, v0}, Landroid/text/TextLine;->adjustEndHyphenEdit(II)I

    move-result v0

    invoke-virtual {v13, v0}, Landroid/text/TextPaint;->setEndHyphenEdit(I)V

    if-nez p12, :cond_19

    move/from16 v9, p2

    if-ge v10, v9, :cond_18

    goto :goto_11

    :cond_18
    move/from16 v23, v17

    goto :goto_12

    :cond_19
    move/from16 v9, p2

    :goto_11
    move/from16 v23, v18

    :goto_12
    move/from16 v8, v27

    invoke-static {v10, v8}, Ljava/lang/Math;->min(II)I

    move-result v15

    iget-object v0, v12, Landroid/text/TextLine;->mDecorations:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    move-object v1, v13

    move v2, v11

    move v3, v10

    move/from16 v4, v33

    move/from16 v5, v22

    move/from16 v6, p4

    move-object/from16 v7, p5

    move/from16 v25, v8

    move-object/from16 v8, p6

    move/from16 v9, v24

    move/from16 v26, v10

    move/from16 v10, p8

    move/from16 v32, v11

    move/from16 v11, p9

    move/from16 v12, p10

    move-object/from16 v27, v13

    move-object/from16 v13, p11

    move-object/from16 v28, v14

    move/from16 v14, v23

    invoke-direct/range {v0 .. v16}, Landroid/text/TextLine;->handleText(Landroid/text/TextPaint;IIIIZLandroid/graphics/Canvas;Landroid/text/TextShaper$GlyphsConsumer;FIIILandroid/graphics/Paint$FontMetricsInt;ZILjava/util/ArrayList;)F

    move-result v0

    add-float v24, v24, v0

    move/from16 v13, v24

    :goto_13
    move/from16 v14, v22

    goto/16 :goto_3

    :cond_1a
    sub-float v0, v13, v20

    return v0

    :cond_1b
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "measureLimit ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v2, p2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ") is out of start ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v3, p1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ") and limit ("

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v4, p3

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ") bounds"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private handleText(Landroid/text/TextPaint;IIIIZLandroid/graphics/Canvas;Landroid/text/TextShaper$GlyphsConsumer;FIIILandroid/graphics/Paint$FontMetricsInt;ZILjava/util/ArrayList;)F
    .locals 28
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/text/TextPaint;",
            "IIIIZ",
            "Landroid/graphics/Canvas;",
            "Landroid/text/TextShaper$GlyphsConsumer;",
            "FIII",
            "Landroid/graphics/Paint$FontMetricsInt;",
            "ZI",
            "Ljava/util/ArrayList<",
            "Landroid/text/TextLine$DecorationInfo;",
            ">;)F"
        }
    .end annotation

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    move/from16 v12, p2

    move/from16 v13, p11

    move-object/from16 v14, p13

    move-object/from16 v15, p16

    iget-boolean v0, v10, Landroid/text/TextLine;->mIsJustifying:Z

    if-eqz v0, :cond_0

    iget v0, v10, Landroid/text/TextLine;->mAddedWidthForJustify:F

    invoke-virtual {v11, v0}, Landroid/text/TextPaint;->setWordSpacing(F)V

    :cond_0
    if-eqz v14, :cond_1

    invoke-static {v14, v11}, Landroid/text/TextLine;->expandMetricsFromPaint(Landroid/graphics/Paint$FontMetricsInt;Landroid/text/TextPaint;)V

    :cond_1
    move/from16 v9, p3

    if-ne v9, v12, :cond_2

    const/4 v0, 0x0

    return v0

    :cond_2
    const/4 v8, 0x0

    if-nez v15, :cond_3

    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    invoke-virtual/range {p16 .. p16}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    move v7, v0

    if-nez p14, :cond_6

    if-nez p7, :cond_4

    if-eqz p8, :cond_5

    :cond_4
    iget v0, v11, Landroid/text/TextPaint;->bgColor:I

    if-nez v0, :cond_6

    if-nez v7, :cond_6

    if-eqz p6, :cond_5

    goto :goto_1

    :cond_5
    move v12, v7

    goto :goto_2

    :cond_6
    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move v12, v7

    move/from16 v7, p15

    invoke-direct/range {v0 .. v7}, Landroid/text/TextLine;->getRunAdvance(Landroid/text/TextPaint;IIIIZI)F

    move-result v8

    :goto_2
    if-eqz p6, :cond_7

    sub-float v0, p9, v8

    move/from16 v1, p9

    move/from16 v16, v0

    move/from16 v17, v1

    goto :goto_3

    :cond_7
    move/from16 v0, p9

    add-float v1, p9, v8

    move/from16 v16, v0

    move/from16 v17, v1

    :goto_3
    if-eqz p8, :cond_8

    move-object/from16 v0, p0

    move-object/from16 v1, p8

    move-object/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v18, v8

    move/from16 v8, v16

    invoke-direct/range {v0 .. v8}, Landroid/text/TextLine;->shapeTextRun(Landroid/text/TextShaper$GlyphsConsumer;Landroid/text/TextPaint;IIIIZF)V

    goto :goto_4

    :cond_8
    move/from16 v18, v8

    :goto_4
    iget-boolean v0, v10, Landroid/text/TextLine;->mUseFallbackExtent:Z

    if-eqz v0, :cond_9

    if-eqz v14, :cond_9

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p13

    invoke-direct/range {v0 .. v7}, Landroid/text/TextLine;->expandMetricsFromPaint(Landroid/text/TextPaint;IIIIZLandroid/graphics/Paint$FontMetricsInt;)V

    :cond_9
    if-eqz p7, :cond_11

    iget v0, v11, Landroid/text/TextPaint;->bgColor:I

    if-eqz v0, :cond_a

    invoke-virtual/range {p1 .. p1}, Landroid/text/TextPaint;->getColor()I

    move-result v6

    invoke-virtual/range {p1 .. p1}, Landroid/text/TextPaint;->getStyle()Landroid/graphics/Paint$Style;

    move-result-object v7

    iget v0, v11, Landroid/text/TextPaint;->bgColor:I

    invoke-virtual {v11, v0}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v11, v0}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    move/from16 v8, p10

    int-to-float v2, v8

    move/from16 v5, p12

    int-to-float v4, v5

    move-object/from16 v0, p7

    move/from16 v1, v16

    move/from16 v3, v17

    move-object/from16 v5, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    invoke-virtual {v11, v7}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {v11, v6}, Landroid/text/TextPaint;->setColor(I)V

    goto :goto_5

    :cond_a
    move/from16 v8, p10

    :goto_5
    iget v0, v11, Landroid/text/TextPaint;->baselineShift:I

    add-int v19, v13, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    move-object/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, v16

    move/from16 v9, v19

    invoke-direct/range {v0 .. v9}, Landroid/text/TextLine;->drawTextRun(Landroid/graphics/Canvas;Landroid/text/TextPaint;IIIIZFI)V

    if-eqz v12, :cond_10

    const/4 v0, 0x0

    move v8, v0

    :goto_6
    if-ge v8, v12, :cond_f

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/text/TextLine$DecorationInfo;

    iget v0, v9, Landroid/text/TextLine$DecorationInfo;->start:I

    move/from16 v19, v12

    move/from16 v12, p2

    invoke-static {v0, v12}, Ljava/lang/Math;->max(II)I

    move-result v20

    iget v0, v9, Landroid/text/TextLine$DecorationInfo;->end:I

    move/from16 v7, p15

    invoke-static {v0, v7}, Ljava/lang/Math;->min(II)I

    move-result v21

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, v20

    invoke-direct/range {v0 .. v7}, Landroid/text/TextLine;->getRunAdvance(Landroid/text/TextPaint;IIIIZI)F

    move-result v22

    move/from16 v7, v21

    invoke-direct/range {v0 .. v7}, Landroid/text/TextLine;->getRunAdvance(Landroid/text/TextPaint;IIIIZI)F

    move-result v23

    if-eqz p6, :cond_b

    sub-float v0, v17, v23

    sub-float v1, v17, v22

    move/from16 v24, v0

    move/from16 v25, v1

    goto :goto_7

    :cond_b
    add-float v0, v16, v22

    add-float v1, v16, v23

    move/from16 v24, v0

    move/from16 v25, v1

    :goto_7
    iget v0, v9, Landroid/text/TextLine$DecorationInfo;->underlineColor:I

    if-eqz v0, :cond_c

    iget v2, v9, Landroid/text/TextLine$DecorationInfo;->underlineColor:I

    invoke-virtual/range {p1 .. p1}, Landroid/text/TextPaint;->getUnderlinePosition()F

    move-result v3

    iget v4, v9, Landroid/text/TextLine$DecorationInfo;->underlineThickness:F

    int-to-float v7, v13

    move-object/from16 v0, p1

    move-object/from16 v1, p7

    move/from16 v5, v24

    move/from16 v6, v25

    invoke-static/range {v0 .. v7}, Landroid/text/TextLine;->drawStroke(Landroid/text/TextPaint;Landroid/graphics/Canvas;IFFFFF)V

    :cond_c
    iget-boolean v0, v9, Landroid/text/TextLine$DecorationInfo;->isUnderlineText:Z

    const/high16 v7, 0x3f800000    # 1.0f

    if-eqz v0, :cond_d

    nop

    invoke-virtual/range {p1 .. p1}, Landroid/text/TextPaint;->getUnderlineThickness()F

    move-result v0

    invoke-static {v0, v7}, Ljava/lang/Math;->max(FF)F

    move-result v26

    invoke-virtual/range {p1 .. p1}, Landroid/text/TextPaint;->getColor()I

    move-result v2

    invoke-virtual/range {p1 .. p1}, Landroid/text/TextPaint;->getUnderlinePosition()F

    move-result v3

    int-to-float v6, v13

    move-object/from16 v0, p1

    move-object/from16 v1, p7

    move/from16 v4, v26

    move/from16 v5, v24

    move/from16 v27, v6

    move/from16 v6, v25

    move v10, v7

    move/from16 v7, v27

    invoke-static/range {v0 .. v7}, Landroid/text/TextLine;->drawStroke(Landroid/text/TextPaint;Landroid/graphics/Canvas;IFFFFF)V

    goto :goto_8

    :cond_d
    move v10, v7

    :goto_8
    iget-boolean v0, v9, Landroid/text/TextLine$DecorationInfo;->isStrikeThruText:Z

    if-eqz v0, :cond_e

    nop

    invoke-virtual/range {p1 .. p1}, Landroid/text/TextPaint;->getStrikeThruThickness()F

    move-result v0

    invoke-static {v0, v10}, Ljava/lang/Math;->max(FF)F

    move-result v10

    invoke-virtual/range {p1 .. p1}, Landroid/text/TextPaint;->getColor()I

    move-result v2

    invoke-virtual/range {p1 .. p1}, Landroid/text/TextPaint;->getStrikeThruPosition()F

    move-result v3

    int-to-float v7, v13

    move-object/from16 v0, p1

    move-object/from16 v1, p7

    move v4, v10

    move/from16 v5, v24

    move/from16 v6, v25

    invoke-static/range {v0 .. v7}, Landroid/text/TextLine;->drawStroke(Landroid/text/TextPaint;Landroid/graphics/Canvas;IFFFFF)V

    :cond_e
    add-int/lit8 v8, v8, 0x1

    move-object/from16 v10, p0

    move/from16 v12, v19

    goto/16 :goto_6

    :cond_f
    move/from16 v19, v12

    move/from16 v12, p2

    goto :goto_9

    :cond_10
    move/from16 v19, v12

    move/from16 v12, p2

    goto :goto_9

    :cond_11
    move/from16 v19, v12

    move/from16 v12, p2

    :goto_9
    if-eqz p6, :cond_12

    move/from16 v8, v18

    neg-float v0, v8

    goto :goto_a

    :cond_12
    move/from16 v8, v18

    move v0, v8

    :goto_a
    return v0
.end method

.method public static isLineEndSpace(C)Z
    .locals 1

    const/16 v0, 0x20

    if-eq p0, v0, :cond_2

    const/16 v0, 0x9

    if-eq p0, v0, :cond_2

    const/16 v0, 0x1680

    if-eq p0, v0, :cond_2

    const/16 v0, 0x2000

    if-gt v0, p0, :cond_0

    const/16 v0, 0x200a

    if-gt p0, v0, :cond_0

    const/16 v0, 0x2007

    if-ne p0, v0, :cond_2

    :cond_0
    const/16 v0, 0x205f

    if-eq p0, v0, :cond_2

    const/16 v0, 0x3000

    if-ne p0, v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private isStretchableWhitespace(I)Z
    .locals 1

    const/16 v0, 0x20

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private measureRun(IIIZLandroid/graphics/Paint$FontMetricsInt;)F
    .locals 13

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move-object/from16 v11, p5

    invoke-direct/range {v0 .. v12}, Landroid/text/TextLine;->handleRun(IIIZLandroid/graphics/Canvas;Landroid/text/TextShaper$GlyphsConsumer;FIIILandroid/graphics/Paint$FontMetricsInt;Z)F

    move-result v0

    return v0
.end method

.method public static obtain()Landroid/text/TextLine;
    .locals 5

    sget-object v0, Landroid/text/TextLine;->sCached:[Landroid/text/TextLine;

    monitor-enter v0

    :try_start_0
    array-length v1, v0

    :cond_0
    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_1

    sget-object v2, Landroid/text/TextLine;->sCached:[Landroid/text/TextLine;

    aget-object v3, v2, v1

    if-eqz v3, :cond_0

    nop

    const/4 v4, 0x0

    aput-object v4, v2, v1

    monitor-exit v0

    return-object v3

    :cond_1
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v0, Landroid/text/TextLine;

    invoke-direct {v0}, Landroid/text/TextLine;-><init>()V

    return-object v0

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public static recycle(Landroid/text/TextLine;)Landroid/text/TextLine;
    .locals 5

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/text/TextLine;->mText:Ljava/lang/CharSequence;

    iput-object v0, p0, Landroid/text/TextLine;->mPaint:Landroid/text/TextPaint;

    iput-object v0, p0, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    iput-object v0, p0, Landroid/text/TextLine;->mSpanned:Landroid/text/Spanned;

    iput-object v0, p0, Landroid/text/TextLine;->mTabs:Landroid/text/Layout$TabStops;

    iput-object v0, p0, Landroid/text/TextLine;->mChars:[C

    iput-object v0, p0, Landroid/text/TextLine;->mComputed:Landroid/text/PrecomputedText;

    const/4 v1, 0x0

    iput-boolean v1, p0, Landroid/text/TextLine;->mUseFallbackExtent:Z

    iget-object v1, p0, Landroid/text/TextLine;->mMetricAffectingSpanSpanSet:Landroid/text/SpanSet;

    invoke-virtual {v1}, Landroid/text/SpanSet;->recycle()V

    iget-object v1, p0, Landroid/text/TextLine;->mCharacterStyleSpanSet:Landroid/text/SpanSet;

    invoke-virtual {v1}, Landroid/text/SpanSet;->recycle()V

    iget-object v1, p0, Landroid/text/TextLine;->mReplacementSpanSpanSet:Landroid/text/SpanSet;

    invoke-virtual {v1}, Landroid/text/SpanSet;->recycle()V

    sget-object v1, Landroid/text/TextLine;->sCached:[Landroid/text/TextLine;

    monitor-enter v1

    const/4 v2, 0x0

    :goto_0
    :try_start_0
    sget-object v3, Landroid/text/TextLine;->sCached:[Landroid/text/TextLine;

    array-length v4, v3

    if-ge v2, v4, :cond_1

    aget-object v4, v3, v2

    if-nez v4, :cond_0

    aput-object p0, v3, v2

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private shapeRun(Landroid/text/TextShaper$GlyphsConsumer;IIZFZ)F
    .locals 16

    move-object/from16 v13, p0

    iget v0, v13, Landroid/text/TextLine;->mDir:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    move/from16 v14, p4

    if-ne v1, v14, :cond_1

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p3

    move/from16 v4, p4

    invoke-direct/range {v0 .. v5}, Landroid/text/TextLine;->measureRun(IIIZLandroid/graphics/Paint$FontMetricsInt;)F

    move-result v0

    neg-float v15, v0

    add-float v7, p5, v15

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v0, p0

    move-object/from16 v6, p1

    invoke-direct/range {v0 .. v12}, Landroid/text/TextLine;->handleRun(IIIZLandroid/graphics/Canvas;Landroid/text/TextShaper$GlyphsConsumer;FIIILandroid/graphics/Paint$FontMetricsInt;Z)F

    return v15

    :cond_1
    const/4 v5, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p3

    move/from16 v4, p4

    move-object/from16 v6, p1

    move/from16 v7, p5

    move/from16 v12, p6

    invoke-direct/range {v0 .. v12}, Landroid/text/TextLine;->handleRun(IIIZLandroid/graphics/Canvas;Landroid/text/TextShaper$GlyphsConsumer;FIIILandroid/graphics/Paint$FontMetricsInt;Z)F

    move-result v0

    return v0
.end method

.method private shapeTextRun(Landroid/text/TextShaper$GlyphsConsumer;Landroid/text/TextPaint;IIIIZF)V
    .locals 13

    move-object v0, p0

    move/from16 v10, p3

    sub-int v11, p4, v10

    sub-int v12, p6, p5

    iget-boolean v1, v0, Landroid/text/TextLine;->mCharsValid:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Landroid/text/TextLine;->mChars:[C

    const/4 v7, 0x0

    move/from16 v2, p3

    move v3, v11

    move/from16 v4, p5

    move v5, v12

    move/from16 v6, p8

    move/from16 v8, p7

    move-object v9, p2

    invoke-static/range {v1 .. v9}, Landroid/graphics/text/TextRunShaper;->shapeTextRun([CIIIIFFZLandroid/graphics/Paint;)Landroid/graphics/text/PositionedGlyphs;

    move-result-object v1

    goto :goto_0

    :cond_0
    iget-object v1, v0, Landroid/text/TextLine;->mText:Ljava/lang/CharSequence;

    iget v2, v0, Landroid/text/TextLine;->mStart:I

    add-int v3, v2, v10

    add-int v4, v2, p5

    const/4 v7, 0x0

    move v2, v3

    move v3, v11

    move v5, v12

    move/from16 v6, p8

    move/from16 v8, p7

    move-object v9, p2

    invoke-static/range {v1 .. v9}, Landroid/graphics/text/TextRunShaper;->shapeTextRun(Ljava/lang/CharSequence;IIIIFFZLandroid/graphics/Paint;)Landroid/graphics/text/PositionedGlyphs;

    move-result-object v1

    :goto_0
    move-object v2, p1

    move-object v3, p2

    invoke-interface {p1, v10, v11, v1, p2}, Landroid/text/TextShaper$GlyphsConsumer;->accept(IILandroid/graphics/text/PositionedGlyphs;Landroid/text/TextPaint;)V

    return-void
.end method

.method static updateMetrics(Landroid/graphics/Paint$FontMetricsInt;IIIII)V
    .locals 1

    iget v0, p0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    iget v0, p0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    iget v0, p0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    invoke-static {v0, p3}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    iget v0, p0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    invoke-static {v0, p4}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iget v0, p0, Landroid/graphics/Paint$FontMetricsInt;->leading:I

    invoke-static {v0, p5}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Landroid/graphics/Paint$FontMetricsInt;->leading:I

    return-void
.end method


# virtual methods
.method draw(Landroid/graphics/Canvas;FIII)V
    .locals 20

    goto/32 :goto_1a

    nop

    :goto_0
    iget-object v1, v10, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    goto/32 :goto_5a

    nop

    :goto_1
    iget v2, v10, Landroid/text/TextLine;->mLen:I

    goto/32 :goto_5d

    nop

    :goto_2
    iget-object v1, v10, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    goto/32 :goto_2f

    nop

    :goto_3
    move v1, v13

    goto/32 :goto_2d

    nop

    :goto_4
    iget v1, v10, Landroid/text/TextLine;->mLen:I

    goto/32 :goto_4b

    nop

    :goto_5
    move/from16 v17, v9

    :goto_6
    goto/32 :goto_31

    nop

    :goto_7
    move v11, v9

    goto/32 :goto_8

    nop

    :goto_8
    move/from16 v9, v18

    goto/32 :goto_35

    nop

    :goto_9
    add-int/lit8 v0, v11, -0x1

    goto/32 :goto_3b

    nop

    :goto_a
    goto/16 :goto_48

    :goto_b
    goto/32 :goto_50

    nop

    :goto_c
    const/4 v0, 0x0

    goto/32 :goto_20

    nop

    :goto_d
    return-void

    :goto_e
    move/from16 v7, p4

    goto/32 :goto_42

    nop

    :goto_f
    move/from16 v18, v0

    goto/32 :goto_59

    nop

    :goto_10
    int-to-float v0, v0

    goto/32 :goto_30

    nop

    :goto_11
    const/4 v0, 0x1

    :goto_12
    goto/32 :goto_f

    nop

    :goto_13
    move-object/from16 v1, p1

    goto/32 :goto_2c

    nop

    :goto_14
    if-le v9, v14, :cond_0

    goto/32 :goto_5f

    :cond_0
    goto/32 :goto_3e

    nop

    :goto_15
    const/4 v1, 0x0

    goto/32 :goto_47

    nop

    :goto_16
    goto/16 :goto_33

    :goto_17
    goto/32 :goto_34

    nop

    :goto_18
    move/from16 v11, v19

    goto/32 :goto_a

    nop

    :goto_19
    move/from16 v6, p3

    goto/32 :goto_e

    nop

    :goto_1a
    move-object/from16 v10, p0

    goto/32 :goto_26

    nop

    :goto_1b
    move/from16 v19, v11

    goto/32 :goto_45

    nop

    :goto_1c
    move v9, v2

    :goto_1d
    goto/32 :goto_14

    nop

    :goto_1e
    if-ne v11, v14, :cond_1

    goto/32 :goto_56

    :cond_1
    goto/32 :goto_53

    nop

    :goto_1f
    int-to-float v1, v0

    goto/32 :goto_10

    nop

    :goto_20
    goto :goto_12

    :goto_21
    goto/32 :goto_11

    nop

    :goto_22
    move/from16 v16, v0

    goto/32 :goto_5b

    nop

    :goto_23
    move/from16 v19, v11

    goto/32 :goto_41

    nop

    :goto_24
    invoke-virtual {v1, v12}, Landroid/text/Layout$Directions;->getRunStart(I)I

    move-result v13

    goto/32 :goto_4

    nop

    :goto_25
    move v11, v9

    goto/32 :goto_32

    nop

    :goto_26
    const/4 v0, 0x0

    goto/32 :goto_2

    nop

    :goto_27
    mul-float/2addr v1, v0

    goto/32 :goto_55

    nop

    :goto_28
    move/from16 v0, v16

    goto/32 :goto_18

    nop

    :goto_29
    add-float v5, p2, v16

    goto/32 :goto_9

    nop

    :goto_2a
    move/from16 v19, v11

    goto/32 :goto_7

    nop

    :goto_2b
    add-float v16, v16, v0

    goto/32 :goto_1e

    nop

    :goto_2c
    move/from16 v2, v17

    goto/32 :goto_4a

    nop

    :goto_2d
    iget-boolean v2, v10, Landroid/text/TextLine;->mHasTabs:Z

    goto/32 :goto_2e

    nop

    :goto_2e
    if-nez v2, :cond_2

    goto/32 :goto_3d

    :cond_2
    goto/32 :goto_5c

    nop

    :goto_2f
    invoke-virtual {v1}, Landroid/text/Layout$Directions;->getRunCount()I

    move-result v11

    goto/32 :goto_15

    nop

    :goto_30
    mul-float v0, v0, v16

    goto/32 :goto_37

    nop

    :goto_31
    add-int/lit8 v9, v11, 0x1

    goto/32 :goto_39

    nop

    :goto_32
    goto/16 :goto_6

    :goto_33
    goto/32 :goto_29

    nop

    :goto_34
    move/from16 v19, v11

    goto/32 :goto_25

    nop

    :goto_35
    invoke-direct/range {v0 .. v9}, Landroid/text/TextLine;->drawRun(Landroid/graphics/Canvas;IIZFIIIZ)F

    move-result v0

    goto/32 :goto_2b

    nop

    :goto_36
    add-int/lit8 v12, v12, 0x1

    goto/32 :goto_28

    nop

    :goto_37
    invoke-virtual {v10, v0}, Landroid/text/TextLine;->nextTab(F)F

    move-result v0

    goto/32 :goto_27

    nop

    :goto_38
    iget v0, v10, Landroid/text/TextLine;->mLen:I

    goto/32 :goto_3a

    nop

    :goto_39
    move/from16 v11, v19

    goto/32 :goto_5e

    nop

    :goto_3a
    if-ne v9, v0, :cond_3

    goto/32 :goto_40

    :cond_3
    goto/32 :goto_3f

    nop

    :goto_3b
    if-eq v12, v0, :cond_4

    goto/32 :goto_21

    :cond_4
    goto/32 :goto_38

    nop

    :goto_3c
    goto :goto_4e

    :goto_3d
    goto/32 :goto_4d

    nop

    :goto_3e
    if-ne v9, v14, :cond_5

    goto/32 :goto_33

    :cond_5
    goto/32 :goto_58

    nop

    :goto_3f
    goto/16 :goto_21

    :goto_40
    goto/32 :goto_c

    nop

    :goto_41
    move v11, v9

    goto/32 :goto_36

    nop

    :goto_42
    move/from16 v8, p5

    goto/32 :goto_2a

    nop

    :goto_43
    add-int/2addr v1, v13

    goto/32 :goto_1

    nop

    :goto_44
    add-int/lit8 v9, v11, 0x1

    goto/32 :goto_5

    nop

    :goto_45
    goto/16 :goto_51

    :goto_46
    goto/32 :goto_0

    nop

    :goto_47
    move v12, v1

    :goto_48
    goto/32 :goto_54

    nop

    :goto_49
    move v4, v15

    goto/32 :goto_19

    nop

    :goto_4a
    move v3, v9

    goto/32 :goto_49

    nop

    :goto_4b
    if-gt v13, v1, :cond_6

    goto/32 :goto_46

    :cond_6
    goto/32 :goto_1b

    nop

    :goto_4c
    iget-object v1, v10, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    goto/32 :goto_52

    nop

    :goto_4d
    move v2, v14

    :goto_4e
    goto/32 :goto_22

    nop

    :goto_4f
    iget-object v1, v10, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    goto/32 :goto_24

    nop

    :goto_50
    move/from16 v19, v11

    :goto_51
    goto/32 :goto_d

    nop

    :goto_52
    invoke-virtual {v1, v12}, Landroid/text/Layout$Directions;->isRunRtl(I)Z

    move-result v15

    goto/32 :goto_3

    nop

    :goto_53
    iget v0, v10, Landroid/text/TextLine;->mDir:I

    goto/32 :goto_1f

    nop

    :goto_54
    if-lt v12, v11, :cond_7

    goto/32 :goto_b

    :cond_7
    goto/32 :goto_4f

    nop

    :goto_55
    move/from16 v16, v1

    :goto_56
    goto/32 :goto_44

    nop

    :goto_57
    if-eq v0, v1, :cond_8

    goto/32 :goto_17

    :cond_8
    goto/32 :goto_16

    nop

    :goto_58
    invoke-direct {v10, v9}, Landroid/text/TextLine;->charAt(I)C

    move-result v0

    goto/32 :goto_60

    nop

    :goto_59
    move-object/from16 v0, p0

    goto/32 :goto_13

    nop

    :goto_5a
    invoke-virtual {v1, v12}, Landroid/text/Layout$Directions;->getRunLength(I)I

    move-result v1

    goto/32 :goto_43

    nop

    :goto_5b
    move/from16 v17, v1

    goto/32 :goto_1c

    nop

    :goto_5c
    move v2, v13

    goto/32 :goto_3c

    nop

    :goto_5d
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v14

    goto/32 :goto_4c

    nop

    :goto_5e
    goto/16 :goto_1d

    :goto_5f
    goto/32 :goto_23

    nop

    :goto_60
    const/16 v1, 0x9

    goto/32 :goto_57

    nop
.end method

.method getOffsetToLeftRightOf(IZ)I
    .locals 27

    goto/32 :goto_10e

    nop

    :goto_0
    goto/16 :goto_63

    :goto_1
    goto/32 :goto_80

    nop

    :goto_2
    move/from16 v6, v16

    goto/32 :goto_c7

    nop

    :goto_3
    aget v14, v6, v13

    goto/32 :goto_2c

    nop

    :goto_4
    if-gez v5, :cond_0

    goto/32 :goto_1e

    :cond_0
    goto/32 :goto_ed

    nop

    :goto_5
    and-int/lit8 v1, v19, 0x3f

    goto/32 :goto_f5

    nop

    :goto_6
    move/from16 v20, v1

    :goto_7
    goto/32 :goto_30

    nop

    :goto_8
    if-gt v5, v11, :cond_1

    goto/32 :goto_65

    :cond_1
    goto/32 :goto_64

    nop

    :goto_9
    move v3, v13

    goto/32 :goto_f2

    nop

    :goto_a
    add-int/lit8 v2, v8, -0x1

    goto/32 :goto_2e

    nop

    :goto_b
    const/4 v1, 0x0

    :goto_c
    goto/32 :goto_40

    nop

    :goto_d
    goto/16 :goto_93

    :goto_e
    goto/32 :goto_92

    nop

    :goto_f
    if-eq v0, v12, :cond_2

    goto/32 :goto_126

    :cond_2
    goto/32 :goto_d9

    nop

    :goto_10
    move v13, v2

    goto/32 :goto_59

    nop

    :goto_11
    array-length v4, v6

    goto/32 :goto_bf

    nop

    :goto_12
    if-eq v6, v0, :cond_3

    goto/32 :goto_ef

    :cond_3
    nop

    goto/32 :goto_23

    nop

    :goto_13
    move/from16 v26, v5

    goto/32 :goto_8d

    nop

    :goto_14
    move/from16 v2, v20

    goto/32 :goto_6a

    nop

    :goto_15
    if-ge v2, v14, :cond_4

    goto/32 :goto_8a

    :cond_4
    goto/32 :goto_77

    nop

    :goto_16
    move/from16 v22, v4

    goto/32 :goto_47

    nop

    :goto_17
    goto/16 :goto_109

    :goto_18
    goto/32 :goto_108

    nop

    :goto_19
    move v4, v0

    goto/32 :goto_aa

    nop

    :goto_1a
    if-eq v8, v1, :cond_5

    goto/32 :goto_4f

    :cond_5
    goto/32 :goto_d4

    nop

    :goto_1b
    const/4 v1, 0x1

    goto/32 :goto_55

    nop

    :goto_1c
    move/from16 v23, v16

    goto/32 :goto_13e

    nop

    :goto_1d
    goto/16 :goto_5d

    :goto_1e
    goto/32 :goto_13

    nop

    :goto_1f
    const/4 v0, 0x0

    :goto_20
    goto/32 :goto_115

    nop

    :goto_21
    goto/16 :goto_b3

    :goto_22
    goto/32 :goto_b2

    nop

    :goto_23
    if-nez v21, :cond_6

    goto/32 :goto_13f

    :cond_6
    goto/32 :goto_1c

    nop

    :goto_24
    move v8, v6

    goto/32 :goto_25

    nop

    :goto_25
    const/4 v1, -0x1

    goto/32 :goto_1a

    nop

    :goto_26
    move v14, v3

    goto/32 :goto_83

    nop

    :goto_27
    ushr-int/lit8 v1, v1, 0x1a

    goto/32 :goto_132

    nop

    :goto_28
    move/from16 v2, v16

    goto/32 :goto_135

    nop

    :goto_29
    add-int v16, v10, v1

    goto/32 :goto_d2

    nop

    :goto_2a
    const/4 v12, -0x1

    goto/32 :goto_f

    nop

    :goto_2b
    move/from16 v19, v14

    goto/32 :goto_7d

    nop

    :goto_2c
    add-int/2addr v14, v10

    goto/32 :goto_15

    nop

    :goto_2d
    array-length v14, v6

    goto/32 :goto_a3

    nop

    :goto_2e
    const/16 v18, 0x0

    goto/32 :goto_96

    nop

    :goto_2f
    move/from16 v1, v19

    goto/32 :goto_11a

    nop

    :goto_30
    add-int/lit8 v4, v4, 0x2

    goto/32 :goto_81

    nop

    :goto_31
    move/from16 v20, v1

    goto/32 :goto_f3

    nop

    :goto_32
    const/4 v0, -0x1

    goto/32 :goto_12

    nop

    :goto_33
    move/from16 v20, v1

    :goto_34
    goto/32 :goto_dc

    nop

    :goto_35
    move/from16 v24, v3

    goto/32 :goto_4a

    nop

    :goto_36
    goto/16 :goto_134

    :goto_37
    goto/32 :goto_133

    nop

    :goto_38
    add-int v1, v16, v1

    goto/32 :goto_e3

    nop

    :goto_39
    iget v0, v7, Landroid/text/TextLine;->mDir:I

    goto/32 :goto_2a

    nop

    :goto_3a
    add-int v12, v14, v19

    goto/32 :goto_f8

    nop

    :goto_3b
    return v0

    :goto_3c
    goto/32 :goto_127

    nop

    :goto_3d
    move v15, v0

    goto/32 :goto_90

    nop

    :goto_3e
    move v0, v13

    goto/32 :goto_21

    nop

    :goto_3f
    iget v11, v7, Landroid/text/TextLine;->mLen:I

    goto/32 :goto_39

    nop

    :goto_40
    move v3, v1

    goto/32 :goto_b7

    nop

    :goto_41
    add-int/lit8 v19, v13, 0x1

    goto/32 :goto_12a

    nop

    :goto_42
    if-nez v21, :cond_7

    goto/32 :goto_11e

    :cond_7
    goto/32 :goto_10c

    nop

    :goto_43
    move/from16 v5, v23

    goto/32 :goto_110

    nop

    :goto_44
    move v1, v10

    :goto_45
    goto/32 :goto_13c

    nop

    :goto_46
    move-object/from16 v23, v8

    goto/32 :goto_ad

    nop

    :goto_47
    move-object v8, v6

    goto/32 :goto_2

    nop

    :goto_48
    move/from16 v26, v5

    goto/32 :goto_43

    nop

    :goto_49
    if-ne v5, v0, :cond_8

    goto/32 :goto_103

    :cond_8
    goto/32 :goto_73

    nop

    :goto_4a
    move/from16 v25, v4

    goto/32 :goto_95

    nop

    :goto_4b
    and-int/lit8 v1, v4, 0x1

    goto/32 :goto_114

    nop

    :goto_4c
    const/4 v3, 0x0

    goto/32 :goto_f9

    nop

    :goto_4d
    move v1, v11

    goto/32 :goto_c2

    nop

    :goto_4e
    goto :goto_5d

    :goto_4f
    goto/32 :goto_c5

    nop

    :goto_50
    if-nez v21, :cond_9

    goto/32 :goto_d1

    :cond_9
    goto/32 :goto_6f

    nop

    :goto_51
    if-lt v5, v1, :cond_a

    goto/32 :goto_1e

    :cond_a
    goto/32 :goto_fb

    nop

    :goto_52
    move/from16 v20, v1

    :goto_53
    goto/32 :goto_12c

    nop

    :goto_54
    const/4 v3, 0x1

    goto/32 :goto_2f

    nop

    :goto_55
    goto/16 :goto_c

    :goto_56
    goto/32 :goto_b

    nop

    :goto_57
    aget v19, v6, v19

    goto/32 :goto_137

    nop

    :goto_58
    and-int v5, v5, v17

    goto/32 :goto_ba

    nop

    :goto_59
    move v14, v3

    goto/32 :goto_9f

    nop

    :goto_5a
    move v14, v3

    goto/32 :goto_16

    nop

    :goto_5b
    aget v1, v8, v1

    goto/32 :goto_118

    nop

    :goto_5c
    move v6, v8

    :goto_5d
    goto/32 :goto_113

    nop

    :goto_5e
    move/from16 v4, v24

    goto/32 :goto_48

    nop

    :goto_5f
    goto/16 :goto_e8

    :goto_60
    goto/32 :goto_31

    nop

    :goto_61
    move v12, v0

    goto/32 :goto_f1

    nop

    :goto_62
    const/4 v4, 0x0

    :goto_63
    goto/32 :goto_ac

    nop

    :goto_64
    move v5, v11

    :goto_65
    goto/32 :goto_c4

    nop

    :goto_66
    aget v5, v6, v5

    goto/32 :goto_58

    nop

    :goto_67
    goto/16 :goto_97

    :goto_68
    goto/32 :goto_33

    nop

    :goto_69
    move/from16 v8, p1

    goto/32 :goto_7b

    nop

    :goto_6a
    move/from16 v19, v3

    goto/32 :goto_9

    nop

    :goto_6b
    const/4 v0, 0x0

    :goto_6c
    goto/32 :goto_136

    nop

    :goto_6d
    move/from16 v20, v1

    goto/32 :goto_85

    nop

    :goto_6e
    aget v2, v6, v2

    goto/32 :goto_9e

    nop

    :goto_6f
    move/from16 v1, v16

    goto/32 :goto_d0

    nop

    :goto_70
    const/4 v1, -0x2

    :goto_71
    goto/32 :goto_104

    nop

    :goto_72
    move/from16 v3, v19

    goto/32 :goto_12b

    nop

    :goto_73
    and-int/lit8 v0, v12, 0x1

    goto/32 :goto_fd

    nop

    :goto_74
    move-object/from16 v0, p0

    goto/32 :goto_ae

    nop

    :goto_75
    move v4, v13

    goto/32 :goto_f6

    nop

    :goto_76
    move/from16 v20, v1

    goto/32 :goto_12f

    nop

    :goto_77
    add-int/lit8 v19, v13, 0x1

    goto/32 :goto_57

    nop

    :goto_78
    if-lt v0, v12, :cond_b

    goto/32 :goto_b5

    :cond_b
    goto/32 :goto_50

    nop

    :goto_79
    move/from16 v20, v1

    goto/32 :goto_89

    nop

    :goto_7a
    const/4 v0, 0x0

    goto/32 :goto_ca

    nop

    :goto_7b
    move/from16 v9, p2

    goto/32 :goto_cb

    nop

    :goto_7c
    move-object/from16 v0, p0

    goto/32 :goto_88

    nop

    :goto_7d
    move v5, v12

    goto/32 :goto_54

    nop

    :goto_7e
    move/from16 v20, v1

    goto/32 :goto_10

    nop

    :goto_7f
    move-object v8, v6

    goto/32 :goto_8f

    nop

    :goto_80
    move v12, v0

    goto/32 :goto_d5

    nop

    :goto_81
    move/from16 v1, v20

    goto/32 :goto_af

    nop

    :goto_82
    move v6, v1

    goto/32 :goto_1d

    nop

    :goto_83
    move v13, v5

    goto/32 :goto_138

    nop

    :goto_84
    move v5, v4

    goto/32 :goto_e1

    nop

    :goto_85
    move v12, v0

    goto/32 :goto_26

    nop

    :goto_86
    goto/16 :goto_e0

    :goto_87
    goto/32 :goto_df

    nop

    :goto_88
    move v1, v5

    goto/32 :goto_28

    nop

    :goto_89
    goto/16 :goto_53

    :goto_8a
    goto/32 :goto_52

    nop

    :goto_8b
    move-object v8, v6

    :goto_8c
    goto/32 :goto_e9

    nop

    :goto_8d
    move-object/from16 v23, v8

    goto/32 :goto_24

    nop

    :goto_8e
    move/from16 v1, v20

    goto/32 :goto_128

    nop

    :goto_8f
    move/from16 v6, v16

    goto/32 :goto_a7

    nop

    :goto_90
    iget-object v0, v7, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    goto/32 :goto_be

    nop

    :goto_91
    move/from16 v22, v5

    goto/32 :goto_8b

    nop

    :goto_92
    move/from16 v1, v20

    :goto_93
    goto/32 :goto_c0

    nop

    :goto_94
    if-eq v8, v10, :cond_c

    goto/32 :goto_c8

    :cond_c
    goto/32 :goto_da

    nop

    :goto_95
    move/from16 v26, v5

    goto/32 :goto_46

    nop

    :goto_96
    move/from16 v13, v18

    :goto_97
    goto/32 :goto_2d

    nop

    :goto_98
    goto/16 :goto_de

    :goto_99
    goto/32 :goto_eb

    nop

    :goto_9a
    move v1, v11

    goto/32 :goto_db

    nop

    :goto_9b
    move/from16 v22, v5

    goto/32 :goto_fa

    nop

    :goto_9c
    if-eq v8, v0, :cond_d

    goto/32 :goto_de

    :cond_d
    goto/32 :goto_bd

    nop

    :goto_9d
    iget v1, v7, Landroid/text/TextLine;->mLen:I

    goto/32 :goto_ab

    nop

    :goto_9e
    ushr-int/lit8 v2, v2, 0x1a

    goto/32 :goto_f0

    nop

    :goto_9f
    move/from16 v22, v4

    goto/32 :goto_7f

    nop

    :goto_a0
    move/from16 v23, v19

    :goto_a1
    goto/32 :goto_7c

    nop

    :goto_a2
    if-eq v9, v15, :cond_e

    goto/32 :goto_120

    :cond_e
    goto/32 :goto_13a

    nop

    :goto_a3
    if-lt v13, v14, :cond_f

    goto/32 :goto_68

    :cond_f
    goto/32 :goto_3

    nop

    :goto_a4
    move v2, v11

    goto/32 :goto_fc

    nop

    :goto_a5
    move v12, v11

    :goto_a6
    goto/32 :goto_d8

    nop

    :goto_a7
    goto/16 :goto_ea

    :goto_a8
    goto/32 :goto_62

    nop

    :goto_a9
    if-nez v3, :cond_10

    goto/32 :goto_22

    :cond_10
    goto/32 :goto_3e

    nop

    :goto_aa
    if-eq v9, v4, :cond_11

    goto/32 :goto_ff

    :cond_11
    goto/32 :goto_b9

    nop

    :goto_ab
    const/4 v2, 0x1

    goto/32 :goto_130

    nop

    :goto_ac
    array-length v5, v6

    goto/32 :goto_10f

    nop

    :goto_ad
    move v8, v6

    goto/32 :goto_10d

    nop

    :goto_ae
    move v1, v5

    goto/32 :goto_14

    nop

    :goto_af
    const/4 v12, -0x1

    goto/32 :goto_0

    nop

    :goto_b0
    invoke-direct/range {v0 .. v6}, Landroid/text/TextLine;->getOffsetBeforeAfter(IIIZIZ)I

    move-result v6

    goto/32 :goto_42

    nop

    :goto_b1
    ushr-int/lit8 v19, v19, 0x1a

    goto/32 :goto_12e

    nop

    :goto_b2
    move/from16 v0, v20

    :goto_b3
    goto/32 :goto_9c

    nop

    :goto_b4
    goto/16 :goto_5d

    :goto_b5
    goto/32 :goto_5c

    nop

    :goto_b6
    aget v1, v8, v1

    goto/32 :goto_27

    nop

    :goto_b7
    if-eq v9, v3, :cond_12

    goto/32 :goto_87

    :cond_12
    goto/32 :goto_13d

    nop

    :goto_b8
    move v13, v5

    goto/32 :goto_84

    nop

    :goto_b9
    const/4 v0, 0x1

    goto/32 :goto_fe

    nop

    :goto_ba
    add-int/2addr v5, v1

    goto/32 :goto_8

    nop

    :goto_bb
    move v6, v1

    goto/32 :goto_4e

    nop

    :goto_bc
    if-nez v19, :cond_13

    goto/32 :goto_e

    :cond_13
    goto/32 :goto_d3

    nop

    :goto_bd
    if-ne v3, v14, :cond_14

    goto/32 :goto_99

    :cond_14
    goto/32 :goto_98

    nop

    :goto_be
    iget-object v6, v0, Landroid/text/Layout$Directions;->mDirections:[I

    goto/32 :goto_7a

    nop

    :goto_bf
    move v12, v0

    goto/32 :goto_7e

    nop

    :goto_c0
    if-ne v0, v1, :cond_15

    goto/32 :goto_3c

    :cond_15
    goto/32 :goto_3b

    nop

    :goto_c1
    move-object/from16 v8, v23

    goto/32 :goto_ee

    nop

    :goto_c2
    goto/16 :goto_45

    :goto_c3
    goto/32 :goto_44

    nop

    :goto_c4
    if-lt v8, v5, :cond_16

    goto/32 :goto_60

    :cond_16
    goto/32 :goto_123

    nop

    :goto_c5
    if-le v8, v11, :cond_17

    goto/32 :goto_b5

    :cond_17
    goto/32 :goto_129

    nop

    :goto_c6
    if-eq v8, v11, :cond_18

    goto/32 :goto_a8

    :cond_18
    goto/32 :goto_11

    nop

    :goto_c7
    goto/16 :goto_ea

    :goto_c8
    goto/32 :goto_c6

    nop

    :goto_c9
    add-int/lit8 v5, v4, 0x1

    goto/32 :goto_66

    nop

    :goto_ca
    move v1, v10

    goto/32 :goto_a4

    nop

    :goto_cb
    const/4 v10, 0x0

    goto/32 :goto_3f

    nop

    :goto_cc
    if-eq v8, v1, :cond_19

    goto/32 :goto_e2

    :cond_19
    goto/32 :goto_a

    nop

    :goto_cd
    move v13, v2

    goto/32 :goto_12d

    nop

    :goto_ce
    if-eq v6, v0, :cond_1a

    goto/32 :goto_5d

    :cond_1a
    goto/32 :goto_124

    nop

    :goto_cf
    if-ge v8, v1, :cond_1b

    goto/32 :goto_122

    :cond_1b
    goto/32 :goto_c9

    nop

    :goto_d0
    goto/16 :goto_107

    :goto_d1
    goto/32 :goto_106

    nop

    :goto_d2
    add-int/lit8 v1, v5, 0x1

    goto/32 :goto_5b

    nop

    :goto_d3
    move v1, v13

    goto/32 :goto_d

    nop

    :goto_d4
    if-nez v0, :cond_1c

    goto/32 :goto_131

    :cond_1c
    goto/32 :goto_9d

    nop

    :goto_d5
    move/from16 v20, v1

    goto/32 :goto_cd

    nop

    :goto_d6
    add-int v1, v10, v5

    goto/32 :goto_cf

    nop

    :goto_d7
    move/from16 v12, v25

    goto/32 :goto_c1

    nop

    :goto_d8
    if-lt v2, v12, :cond_1d

    goto/32 :goto_11b

    :cond_1d
    goto/32 :goto_41

    nop

    :goto_d9
    const/4 v0, 0x1

    goto/32 :goto_125

    nop

    :goto_da
    const/4 v4, -0x2

    goto/32 :goto_61

    nop

    :goto_db
    move/from16 v19, v1

    goto/32 :goto_17

    nop

    :goto_dc
    move v12, v0

    goto/32 :goto_76

    nop

    :goto_dd
    goto/16 :goto_8c

    :goto_de
    goto/32 :goto_74

    nop

    :goto_df
    const/4 v1, 0x0

    :goto_e0
    goto/32 :goto_105

    nop

    :goto_e1
    goto :goto_e8

    :goto_e2
    goto/32 :goto_6d

    nop

    :goto_e3
    if-gt v1, v11, :cond_1e

    goto/32 :goto_18

    :cond_1e
    goto/32 :goto_9a

    nop

    :goto_e4
    invoke-direct/range {v0 .. v6}, Landroid/text/TextLine;->getOffsetBeforeAfter(IIIZIZ)I

    move-result v0

    goto/32 :goto_bc

    nop

    :goto_e5
    goto/16 :goto_71

    :goto_e6
    goto/32 :goto_70

    nop

    :goto_e7
    move v5, v4

    :goto_e8
    goto/32 :goto_10a

    nop

    :goto_e9
    move/from16 v6, v16

    :goto_ea
    goto/32 :goto_a2

    nop

    :goto_eb
    move/from16 v22, v5

    goto/32 :goto_119

    nop

    :goto_ec
    move-object v8, v6

    goto/32 :goto_f4

    nop

    :goto_ed
    array-length v1, v8

    goto/32 :goto_51

    nop

    :goto_ee
    goto :goto_ea

    :goto_ef
    goto/32 :goto_35

    nop

    :goto_f0
    and-int/lit8 v0, v2, 0x3f

    goto/32 :goto_cc

    nop

    :goto_f1
    move/from16 v20, v1

    goto/32 :goto_f7

    nop

    :goto_f2
    move/from16 v21, v4

    goto/32 :goto_9b

    nop

    :goto_f3
    move v2, v5

    goto/32 :goto_121

    nop

    :goto_f4
    move/from16 v6, v19

    goto/32 :goto_e4

    nop

    :goto_f5
    if-lt v1, v0, :cond_1f

    goto/32 :goto_53

    :cond_1f
    goto/32 :goto_75

    nop

    :goto_f6
    move v0, v1

    goto/32 :goto_2b

    nop

    :goto_f7
    move v13, v2

    goto/32 :goto_5a

    nop

    :goto_f8
    if-gt v12, v11, :cond_20

    goto/32 :goto_a6

    :cond_20
    goto/32 :goto_a5

    nop

    :goto_f9
    const v17, 0x3ffffff

    goto/32 :goto_94

    nop

    :goto_fa
    move/from16 v5, p1

    goto/32 :goto_ec

    nop

    :goto_fb
    aget v1, v8, v5

    goto/32 :goto_29

    nop

    :goto_fc
    const/16 v16, -0x1

    goto/32 :goto_4c

    nop

    :goto_fd
    if-nez v0, :cond_21

    goto/32 :goto_37

    :cond_21
    goto/32 :goto_139

    nop

    :goto_fe
    goto/16 :goto_20

    :goto_ff
    goto/32 :goto_1f

    nop

    :goto_100
    const/4 v1, 0x2

    goto/32 :goto_e5

    nop

    :goto_101
    aget v5, v6, v4

    goto/32 :goto_d6

    nop

    :goto_102
    goto/16 :goto_ea

    :goto_103
    goto/32 :goto_91

    nop

    :goto_104
    add-int v5, v22, v1

    goto/32 :goto_4

    nop

    :goto_105
    move/from16 v21, v1

    goto/32 :goto_32

    nop

    :goto_106
    move/from16 v1, v19

    :goto_107
    goto/32 :goto_82

    nop

    :goto_108
    move/from16 v19, v1

    :goto_109
    goto/32 :goto_11c

    nop

    :goto_10a
    array-length v0, v6

    goto/32 :goto_49

    nop

    :goto_10b
    move/from16 v6, v21

    goto/32 :goto_b0

    nop

    :goto_10c
    move/from16 v0, v19

    goto/32 :goto_11d

    nop

    :goto_10d
    move/from16 v0, v25

    goto/32 :goto_78

    nop

    :goto_10e
    move-object/from16 v7, p0

    goto/32 :goto_69

    nop

    :goto_10f
    if-lt v4, v5, :cond_22

    goto/32 :goto_1

    :cond_22
    goto/32 :goto_101

    nop

    :goto_110
    move-object/from16 v23, v8

    goto/32 :goto_13b

    nop

    :goto_111
    const/4 v0, 0x0

    :goto_112
    goto/32 :goto_3d

    nop

    :goto_113
    return v6

    :goto_114
    if-nez v1, :cond_23

    goto/32 :goto_56

    :cond_23
    goto/32 :goto_1b

    nop

    :goto_115
    move v3, v0

    goto/32 :goto_a9

    nop

    :goto_116
    move/from16 v0, v16

    :goto_117
    goto/32 :goto_ce

    nop

    :goto_118
    and-int v1, v1, v17

    goto/32 :goto_38

    nop

    :goto_119
    move-object v8, v6

    goto/32 :goto_dd

    nop

    :goto_11a
    goto/16 :goto_34

    :goto_11b
    goto/32 :goto_79

    nop

    :goto_11c
    add-int/lit8 v1, v5, 0x1

    goto/32 :goto_b6

    nop

    :goto_11d
    goto :goto_117

    :goto_11e
    goto/32 :goto_116

    nop

    :goto_11f
    goto/16 :goto_6c

    :goto_120
    goto/32 :goto_6b

    nop

    :goto_121
    goto/16 :goto_7

    :goto_122
    goto/32 :goto_6

    nop

    :goto_123
    add-int/lit8 v2, v4, 0x1

    goto/32 :goto_6e

    nop

    :goto_124
    move/from16 v22, v26

    goto/32 :goto_d7

    nop

    :goto_125
    goto :goto_112

    :goto_126
    goto/32 :goto_111

    nop

    :goto_127
    move v6, v0

    goto/32 :goto_102

    nop

    :goto_128
    const/4 v12, -0x1

    goto/32 :goto_67

    nop

    :goto_129
    if-nez v0, :cond_24

    goto/32 :goto_c3

    :cond_24
    goto/32 :goto_4d

    nop

    :goto_12a
    aget v19, v6, v19

    goto/32 :goto_b1

    nop

    :goto_12b
    move/from16 v25, v4

    goto/32 :goto_5e

    nop

    :goto_12c
    add-int/lit8 v13, v13, 0x2

    goto/32 :goto_8e

    nop

    :goto_12d
    move v14, v3

    goto/32 :goto_e7

    nop

    :goto_12e
    move/from16 v20, v1

    goto/32 :goto_5

    nop

    :goto_12f
    move v14, v3

    goto/32 :goto_b8

    nop

    :goto_130
    add-int/2addr v1, v2

    :goto_131
    goto/32 :goto_bb

    nop

    :goto_132
    and-int/lit8 v4, v1, 0x3f

    goto/32 :goto_4b

    nop

    :goto_133
    const/4 v0, 0x0

    :goto_134
    goto/32 :goto_19

    nop

    :goto_135
    move/from16 v24, v3

    goto/32 :goto_72

    nop

    :goto_136
    if-nez v0, :cond_25

    goto/32 :goto_e6

    :cond_25
    goto/32 :goto_100

    nop

    :goto_137
    and-int v19, v19, v17

    goto/32 :goto_3a

    nop

    :goto_138
    move v5, v4

    goto/32 :goto_5f

    nop

    :goto_139
    const/4 v0, 0x1

    goto/32 :goto_36

    nop

    :goto_13a
    const/4 v0, 0x1

    goto/32 :goto_11f

    nop

    :goto_13b
    move v8, v6

    goto/32 :goto_10b

    nop

    :goto_13c
    move v6, v1

    goto/32 :goto_b4

    nop

    :goto_13d
    const/4 v1, 0x1

    goto/32 :goto_86

    nop

    :goto_13e
    goto/16 :goto_a1

    :goto_13f
    goto/32 :goto_a0

    nop
.end method

.method public justify(F)V
    .locals 6

    iget v0, p0, Landroid/text/TextLine;->mLen:I

    :goto_0
    const/4 v1, 0x1

    if-lez v0, :cond_0

    iget-object v2, p0, Landroid/text/TextLine;->mText:Ljava/lang/CharSequence;

    iget v3, p0, Landroid/text/TextLine;->mStart:I

    add-int/2addr v3, v0

    sub-int/2addr v3, v1

    invoke-interface {v2, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-static {v2}, Landroid/text/TextLine;->isLineEndSpace(C)Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    invoke-direct {p0, v2, v0}, Landroid/text/TextLine;->countStretchableSpaces(II)I

    move-result v3

    if-nez v3, :cond_1

    return-void

    :cond_1
    const/4 v4, 0x0

    invoke-virtual {p0, v0, v2, v4}, Landroid/text/TextLine;->measure(IZLandroid/graphics/Paint$FontMetricsInt;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    sub-float v4, p1, v2

    int-to-float v5, v3

    div-float/2addr v4, v5

    iput v4, p0, Landroid/text/TextLine;->mAddedWidthForJustify:F

    iput-boolean v1, p0, Landroid/text/TextLine;->mIsJustifying:Z

    return-void
.end method

.method public measure(IZLandroid/graphics/Paint$FontMetricsInt;)F
    .locals 20

    move-object/from16 v6, p0

    move/from16 v7, p1

    iget v0, v6, Landroid/text/TextLine;->mLen:I

    if-gt v7, v0, :cond_11

    if-eqz p2, :cond_0

    add-int/lit8 v0, v7, -0x1

    goto :goto_0

    :cond_0
    move v0, v7

    :goto_0
    move v8, v0

    if-gez v8, :cond_1

    const/4 v0, 0x0

    return v0

    :cond_1
    const/4 v0, 0x0

    const/4 v1, 0x0

    move v9, v1

    :goto_1
    iget-object v1, v6, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    invoke-virtual {v1}, Landroid/text/Layout$Directions;->getRunCount()I

    move-result v1

    if-ge v9, v1, :cond_10

    iget-object v1, v6, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    invoke-virtual {v1, v9}, Landroid/text/Layout$Directions;->getRunStart(I)I

    move-result v10

    iget v1, v6, Landroid/text/TextLine;->mLen:I

    if-le v10, v1, :cond_2

    goto/16 :goto_7

    :cond_2
    iget-object v1, v6, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    invoke-virtual {v1, v9}, Landroid/text/Layout$Directions;->getRunLength(I)I

    move-result v1

    add-int/2addr v1, v10

    iget v2, v6, Landroid/text/TextLine;->mLen:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v11

    iget-object v1, v6, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    invoke-virtual {v1, v9}, Landroid/text/Layout$Directions;->isRunRtl(I)Z

    move-result v12

    move v1, v10

    iget-boolean v2, v6, Landroid/text/TextLine;->mHasTabs:Z

    if-eqz v2, :cond_3

    move v2, v10

    goto :goto_2

    :cond_3
    move v2, v11

    :goto_2
    move v13, v0

    move v14, v1

    move v15, v2

    :goto_3
    if-gt v15, v11, :cond_f

    if-eq v15, v11, :cond_4

    invoke-direct {v6, v15}, Landroid/text/TextLine;->charAt(I)C

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_e

    :cond_4
    const/4 v0, 0x0

    const/4 v1, 0x1

    if-lt v8, v14, :cond_5

    if-ge v8, v15, :cond_5

    move v2, v1

    goto :goto_4

    :cond_5
    move v2, v0

    :goto_4
    move/from16 v16, v2

    iget v2, v6, Landroid/text/TextLine;->mDir:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_6

    move v2, v1

    goto :goto_5

    :cond_6
    move v2, v0

    :goto_5
    if-ne v2, v12, :cond_7

    move v0, v1

    :cond_7
    move/from16 v17, v0

    if-eqz v16, :cond_8

    if-eqz v17, :cond_8

    move-object/from16 v0, p0

    move v1, v14

    move/from16 v2, p1

    move v3, v15

    move v4, v12

    move-object/from16 v5, p3

    invoke-direct/range {v0 .. v5}, Landroid/text/TextLine;->measureRun(IIIZLandroid/graphics/Paint$FontMetricsInt;)F

    move-result v0

    add-float/2addr v0, v13

    return v0

    :cond_8
    move-object/from16 v0, p0

    move v1, v14

    move v2, v15

    move v3, v15

    move v4, v12

    move-object/from16 v5, p3

    invoke-direct/range {v0 .. v5}, Landroid/text/TextLine;->measureRun(IIIZLandroid/graphics/Paint$FontMetricsInt;)F

    move-result v5

    if-eqz v17, :cond_9

    move v0, v5

    goto :goto_6

    :cond_9
    neg-float v0, v5

    :goto_6
    add-float/2addr v13, v0

    if-eqz v16, :cond_a

    const/16 v18, 0x0

    move-object/from16 v0, p0

    move v1, v14

    move/from16 v2, p1

    move v3, v15

    move v4, v12

    move/from16 v19, v5

    move-object/from16 v5, v18

    invoke-direct/range {v0 .. v5}, Landroid/text/TextLine;->measureRun(IIIZLandroid/graphics/Paint$FontMetricsInt;)F

    move-result v0

    add-float/2addr v0, v13

    return v0

    :cond_a
    move/from16 v19, v5

    if-eq v15, v11, :cond_d

    if-ne v7, v15, :cond_b

    return v13

    :cond_b
    iget v0, v6, Landroid/text/TextLine;->mDir:I

    int-to-float v1, v0

    int-to-float v0, v0

    mul-float/2addr v0, v13

    invoke-virtual {v6, v0}, Landroid/text/TextLine;->nextTab(F)F

    move-result v0

    mul-float/2addr v1, v0

    if-ne v8, v15, :cond_c

    return v1

    :cond_c
    move v13, v1

    :cond_d
    add-int/lit8 v0, v15, 0x1

    move v14, v0

    :cond_e
    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    :cond_f
    add-int/lit8 v9, v9, 0x1

    move v0, v13

    goto/16 :goto_1

    :cond_10
    :goto_7
    return v0

    :cond_11
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "offset("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") should be less than line limit("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v6, Landroid/text/TextLine;->mLen:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public measureAllOffsets([ZLandroid/graphics/Paint$FontMetricsInt;)[F
    .locals 22

    move-object/from16 v6, p0

    iget v0, v6, Landroid/text/TextLine;->mLen:I

    add-int/lit8 v1, v0, 0x1

    new-array v7, v1, [F

    const/4 v8, 0x1

    add-int/2addr v0, v8

    new-array v9, v0, [I

    const/4 v0, 0x0

    :goto_0
    array-length v1, v9

    if-ge v0, v1, :cond_1

    aget-boolean v1, p1, v0

    if-eqz v1, :cond_0

    add-int/lit8 v1, v0, -0x1

    goto :goto_1

    :cond_0
    move v1, v0

    :goto_1
    aput v1, v9, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v10, 0x0

    aget v0, v9, v10

    if-gez v0, :cond_2

    const/4 v0, 0x0

    aput v0, v7, v10

    :cond_2
    const/4 v0, 0x0

    const/4 v1, 0x0

    move v11, v1

    :goto_2
    iget-object v1, v6, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    invoke-virtual {v1}, Landroid/text/Layout$Directions;->getRunCount()I

    move-result v1

    if-ge v11, v1, :cond_12

    iget-object v1, v6, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    invoke-virtual {v1, v11}, Landroid/text/Layout$Directions;->getRunStart(I)I

    move-result v12

    iget v1, v6, Landroid/text/TextLine;->mLen:I

    if-le v12, v1, :cond_3

    goto/16 :goto_e

    :cond_3
    iget-object v1, v6, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    invoke-virtual {v1, v11}, Landroid/text/Layout$Directions;->getRunLength(I)I

    move-result v1

    add-int/2addr v1, v12

    iget v2, v6, Landroid/text/TextLine;->mLen:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v13

    iget-object v1, v6, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    invoke-virtual {v1, v11}, Landroid/text/Layout$Directions;->isRunRtl(I)Z

    move-result v14

    move v1, v12

    iget-boolean v2, v6, Landroid/text/TextLine;->mHasTabs:Z

    if-eqz v2, :cond_4

    move v2, v12

    goto :goto_3

    :cond_4
    move v2, v13

    :goto_3
    move v15, v0

    move v5, v1

    move v4, v2

    :goto_4
    if-gt v4, v13, :cond_11

    if-eq v4, v13, :cond_6

    invoke-direct {v6, v4}, Landroid/text/TextLine;->charAt(I)C

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_5

    goto :goto_5

    :cond_5
    move v8, v4

    goto/16 :goto_d

    :cond_6
    :goto_5
    move/from16 v16, v15

    iget v0, v6, Landroid/text/TextLine;->mDir:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_7

    move v0, v8

    goto :goto_6

    :cond_7
    move v0, v10

    :goto_6
    if-ne v0, v14, :cond_8

    move v0, v8

    goto :goto_7

    :cond_8
    move v0, v10

    :goto_7
    move/from16 v17, v0

    move-object/from16 v0, p0

    move v1, v5

    move v2, v4

    move v3, v4

    move v8, v4

    move v4, v14

    move/from16 v18, v5

    move-object/from16 v5, p2

    invoke-direct/range {v0 .. v5}, Landroid/text/TextLine;->measureRun(IIIZLandroid/graphics/Paint$FontMetricsInt;)F

    move-result v4

    if-eqz v17, :cond_9

    move v0, v4

    goto :goto_8

    :cond_9
    neg-float v0, v4

    :goto_8
    add-float/2addr v15, v0

    if-eqz v17, :cond_a

    move/from16 v0, v16

    goto :goto_9

    :cond_a
    move v0, v15

    :goto_9
    move/from16 v19, v0

    if-eqz v17, :cond_b

    move-object/from16 v5, p2

    goto :goto_a

    :cond_b
    const/4 v0, 0x0

    move-object v5, v0

    :goto_a
    move/from16 v0, v18

    move v3, v0

    :goto_b
    if-gt v3, v8, :cond_d

    iget v0, v6, Landroid/text/TextLine;->mLen:I

    if-gt v3, v0, :cond_d

    aget v0, v9, v3

    move/from16 v2, v18

    if-lt v0, v2, :cond_c

    aget v0, v9, v3

    if-ge v0, v8, :cond_c

    nop

    move-object/from16 v0, p0

    move v1, v2

    move/from16 v18, v2

    move v2, v3

    move/from16 v20, v3

    move v3, v8

    move/from16 v21, v4

    move v4, v14

    invoke-direct/range {v0 .. v5}, Landroid/text/TextLine;->measureRun(IIIZLandroid/graphics/Paint$FontMetricsInt;)F

    move-result v0

    add-float v0, v19, v0

    aput v0, v7, v20

    goto :goto_c

    :cond_c
    move/from16 v18, v2

    move/from16 v20, v3

    move/from16 v21, v4

    :goto_c
    add-int/lit8 v3, v20, 0x1

    move/from16 v4, v21

    goto :goto_b

    :cond_d
    move/from16 v20, v3

    move/from16 v21, v4

    if-eq v8, v13, :cond_10

    aget v0, v9, v8

    if-ne v0, v8, :cond_e

    aput v15, v7, v8

    :cond_e
    iget v0, v6, Landroid/text/TextLine;->mDir:I

    int-to-float v1, v0

    int-to-float v0, v0

    mul-float/2addr v0, v15

    invoke-virtual {v6, v0}, Landroid/text/TextLine;->nextTab(F)F

    move-result v0

    mul-float/2addr v1, v0

    add-int/lit8 v4, v8, 0x1

    aget v0, v9, v4

    if-ne v0, v8, :cond_f

    add-int/lit8 v4, v8, 0x1

    aput v1, v7, v4

    :cond_f
    move v15, v1

    :cond_10
    add-int/lit8 v4, v8, 0x1

    move v5, v4

    :goto_d
    add-int/lit8 v4, v8, 0x1

    const/4 v8, 0x1

    goto/16 :goto_4

    :cond_11
    move v8, v4

    move/from16 v18, v5

    add-int/lit8 v11, v11, 0x1

    move v0, v15

    const/4 v8, 0x1

    goto/16 :goto_2

    :cond_12
    :goto_e
    iget v1, v6, Landroid/text/TextLine;->mLen:I

    aget v2, v9, v1

    if-ne v2, v1, :cond_13

    aput v0, v7, v1

    :cond_13
    return-object v7
.end method

.method public metrics(Landroid/graphics/Paint$FontMetricsInt;)F
    .locals 2

    iget v0, p0, Landroid/text/TextLine;->mLen:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p1}, Landroid/text/TextLine;->measure(IZLandroid/graphics/Paint$FontMetricsInt;)F

    move-result v0

    return v0
.end method

.method nextTab(F)F
    .locals 1

    goto/32 :goto_5

    nop

    :goto_0
    const/high16 v0, 0x41a00000    # 20.0f

    goto/32 :goto_1

    nop

    :goto_1
    invoke-static {p1, v0}, Landroid/text/Layout$TabStops;->nextDefaultStop(FF)F

    move-result v0

    goto/32 :goto_6

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_7

    nop

    :goto_3
    return v0

    :goto_4
    goto/32 :goto_0

    nop

    :goto_5
    iget-object v0, p0, Landroid/text/TextLine;->mTabs:Landroid/text/Layout$TabStops;

    goto/32 :goto_2

    nop

    :goto_6
    return v0

    :goto_7
    invoke-virtual {v0, p1}, Landroid/text/Layout$TabStops;->nextTab(F)F

    move-result v0

    goto/32 :goto_3

    nop
.end method

.method public set(Landroid/text/TextPaint;Ljava/lang/CharSequence;IIILandroid/text/Layout$Directions;ZLandroid/text/Layout$TabStops;IIZ)V
    .locals 18

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move-object/from16 v5, p6

    move/from16 v6, p9

    move/from16 v7, p10

    iput-object v1, v0, Landroid/text/TextLine;->mPaint:Landroid/text/TextPaint;

    iput-object v2, v0, Landroid/text/TextLine;->mText:Ljava/lang/CharSequence;

    iput v3, v0, Landroid/text/TextLine;->mStart:I

    sub-int v8, v4, v3

    iput v8, v0, Landroid/text/TextLine;->mLen:I

    move/from16 v8, p5

    iput v8, v0, Landroid/text/TextLine;->mDir:I

    iput-object v5, v0, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    move/from16 v9, p11

    iput-boolean v9, v0, Landroid/text/TextLine;->mUseFallbackExtent:Z

    if-eqz v5, :cond_c

    move/from16 v10, p7

    iput-boolean v10, v0, Landroid/text/TextLine;->mHasTabs:Z

    const/4 v11, 0x0

    iput-object v11, v0, Landroid/text/TextLine;->mSpanned:Landroid/text/Spanned;

    const/4 v12, 0x0

    instance-of v13, v2, Landroid/text/Spanned;

    const/4 v15, 0x0

    if-eqz v13, :cond_1

    move-object v13, v2

    check-cast v13, Landroid/text/Spanned;

    iput-object v13, v0, Landroid/text/TextLine;->mSpanned:Landroid/text/Spanned;

    iget-object v14, v0, Landroid/text/TextLine;->mReplacementSpanSpanSet:Landroid/text/SpanSet;

    invoke-virtual {v14, v13, v3, v4}, Landroid/text/SpanSet;->init(Landroid/text/Spanned;II)V

    iget-object v13, v0, Landroid/text/TextLine;->mReplacementSpanSpanSet:Landroid/text/SpanSet;

    iget v13, v13, Landroid/text/SpanSet;->numberOfSpans:I

    if-lez v13, :cond_0

    const/4 v13, 0x1

    goto :goto_0

    :cond_0
    move v13, v15

    :goto_0
    move v12, v13

    :cond_1
    iput-object v11, v0, Landroid/text/TextLine;->mComputed:Landroid/text/PrecomputedText;

    instance-of v13, v2, Landroid/text/PrecomputedText;

    if-eqz v13, :cond_2

    move-object v13, v2

    check-cast v13, Landroid/text/PrecomputedText;

    iput-object v13, v0, Landroid/text/TextLine;->mComputed:Landroid/text/PrecomputedText;

    invoke-virtual {v13}, Landroid/text/PrecomputedText;->getParams()Landroid/text/PrecomputedText$Params;

    move-result-object v13

    invoke-virtual {v13}, Landroid/text/PrecomputedText$Params;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v13

    invoke-virtual {v13, v1}, Landroid/text/TextPaint;->equalsForTextMeasurement(Landroid/graphics/Paint;)Z

    move-result v13

    if-nez v13, :cond_2

    iput-object v11, v0, Landroid/text/TextLine;->mComputed:Landroid/text/PrecomputedText;

    :cond_2
    iput-boolean v12, v0, Landroid/text/TextLine;->mCharsValid:Z

    if-eqz v12, :cond_9

    iget-object v11, v0, Landroid/text/TextLine;->mChars:[C

    if-eqz v11, :cond_3

    array-length v11, v11

    iget v13, v0, Landroid/text/TextLine;->mLen:I

    if-ge v11, v13, :cond_4

    :cond_3
    iget v11, v0, Landroid/text/TextLine;->mLen:I

    invoke-static {v11}, Lcom/android/internal/util/ArrayUtils;->newUnpaddedCharArray(I)[C

    move-result-object v11

    iput-object v11, v0, Landroid/text/TextLine;->mChars:[C

    :cond_4
    iget-object v11, v0, Landroid/text/TextLine;->mChars:[C

    invoke-static {v2, v3, v4, v11, v15}, Landroid/text/TextUtils;->getChars(Ljava/lang/CharSequence;II[CI)V

    if-eqz v12, :cond_9

    iget-object v11, v0, Landroid/text/TextLine;->mChars:[C

    move/from16 v13, p3

    :goto_1
    if-ge v13, v4, :cond_9

    iget-object v14, v0, Landroid/text/TextLine;->mReplacementSpanSpanSet:Landroid/text/SpanSet;

    invoke-virtual {v14, v13, v4}, Landroid/text/SpanSet;->getNextTransition(II)I

    move-result v14

    iget-object v15, v0, Landroid/text/TextLine;->mReplacementSpanSpanSet:Landroid/text/SpanSet;

    invoke-virtual {v15, v13, v14}, Landroid/text/SpanSet;->hasSpansIntersecting(II)Z

    move-result v15

    if-eqz v15, :cond_7

    sub-int v15, v13, v3

    if-ge v15, v7, :cond_6

    sub-int v15, v14, v3

    if-gt v15, v6, :cond_5

    goto :goto_2

    :cond_5
    const/16 v16, 0x1

    goto :goto_4

    :cond_6
    :goto_2
    sub-int v15, v13, v3

    const v17, 0xfffc

    aput-char v17, v11, v15

    sub-int v15, v13, v3

    const/16 v16, 0x1

    add-int/lit8 v15, v15, 0x1

    sub-int v1, v14, v3

    :goto_3
    if-ge v15, v1, :cond_8

    const v17, 0xfeff

    aput-char v17, v11, v15

    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    :cond_7
    const/16 v16, 0x1

    :cond_8
    :goto_4
    move v13, v14

    move-object/from16 v1, p1

    const/4 v15, 0x0

    goto :goto_1

    :cond_9
    move-object/from16 v1, p8

    iput-object v1, v0, Landroid/text/TextLine;->mTabs:Landroid/text/Layout$TabStops;

    const/4 v11, 0x0

    iput v11, v0, Landroid/text/TextLine;->mAddedWidthForJustify:F

    const/4 v11, 0x0

    iput-boolean v11, v0, Landroid/text/TextLine;->mIsJustifying:Z

    if-eq v6, v7, :cond_a

    move v13, v6

    goto :goto_5

    :cond_a
    move v13, v11

    :goto_5
    iput v13, v0, Landroid/text/TextLine;->mEllipsisStart:I

    if-eq v6, v7, :cond_b

    move v15, v7

    goto :goto_6

    :cond_b
    move v15, v11

    :goto_6
    iput v15, v0, Landroid/text/TextLine;->mEllipsisEnd:I

    return-void

    :cond_c
    move/from16 v10, p7

    move-object/from16 v1, p8

    new-instance v11, Ljava/lang/IllegalArgumentException;

    const-string v12, "Directions cannot be null"

    invoke-direct {v11, v12}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v11
.end method

.method shape(Landroid/text/TextShaper$GlyphsConsumer;)V
    .locals 18

    goto/32 :goto_0

    nop

    :goto_0
    move-object/from16 v7, p0

    goto/32 :goto_1b

    nop

    :goto_1
    goto/16 :goto_30

    :goto_2
    goto/32 :goto_49

    nop

    :goto_3
    goto :goto_15

    :goto_4
    goto/32 :goto_23

    nop

    :goto_5
    move v8, v6

    goto/32 :goto_3c

    nop

    :goto_6
    invoke-virtual {v1, v10}, Landroid/text/Layout$Directions;->isRunRtl(I)Z

    move-result v13

    goto/32 :goto_57

    nop

    :goto_7
    int-to-float v1, v0

    goto/32 :goto_4a

    nop

    :goto_8
    if-eq v0, v1, :cond_0

    goto/32 :goto_41

    :cond_0
    goto/32 :goto_40

    nop

    :goto_9
    move/from16 v17, v8

    goto/32 :goto_16

    nop

    :goto_a
    invoke-virtual {v1, v10}, Landroid/text/Layout$Directions;->getRunStart(I)I

    move-result v11

    goto/32 :goto_e

    nop

    :goto_b
    add-int/lit8 v6, v8, 0x1

    goto/32 :goto_35

    nop

    :goto_c
    move v3, v6

    goto/32 :goto_10

    nop

    :goto_d
    move v0, v14

    goto/32 :goto_11

    nop

    :goto_e
    iget v1, v7, Landroid/text/TextLine;->mLen:I

    goto/32 :goto_50

    nop

    :goto_f
    if-lt v10, v9, :cond_1

    goto/32 :goto_47

    :cond_1
    goto/32 :goto_32

    nop

    :goto_10
    move v4, v13

    goto/32 :goto_59

    nop

    :goto_11
    move/from16 v8, v17

    goto/32 :goto_46

    nop

    :goto_12
    if-nez v2, :cond_2

    goto/32 :goto_53

    :cond_2
    goto/32 :goto_22

    nop

    :goto_13
    invoke-virtual {v7, v0}, Landroid/text/TextLine;->nextTab(F)F

    move-result v0

    goto/32 :goto_17

    nop

    :goto_14
    goto/16 :goto_3f

    :goto_15
    goto/32 :goto_3e

    nop

    :goto_16
    move v8, v6

    goto/32 :goto_18

    nop

    :goto_17
    mul-float/2addr v1, v0

    goto/32 :goto_39

    nop

    :goto_18
    goto :goto_28

    :goto_19
    goto/32 :goto_29

    nop

    :goto_1a
    invoke-direct {v7, v6}, Landroid/text/TextLine;->charAt(I)C

    move-result v0

    goto/32 :goto_42

    nop

    :goto_1b
    const/4 v0, 0x0

    goto/32 :goto_1d

    nop

    :goto_1c
    move v2, v15

    goto/32 :goto_c

    nop

    :goto_1d
    const/4 v8, 0x0

    goto/32 :goto_58

    nop

    :goto_1e
    move v2, v12

    :goto_1f
    goto/32 :goto_55

    nop

    :goto_20
    if-le v6, v12, :cond_3

    goto/32 :goto_4e

    :cond_3
    goto/32 :goto_44

    nop

    :goto_21
    const/4 v1, 0x0

    goto/32 :goto_33

    nop

    :goto_22
    move v2, v11

    goto/32 :goto_52

    nop

    :goto_23
    const/4 v0, 0x0

    goto/32 :goto_14

    nop

    :goto_24
    iget-boolean v2, v7, Landroid/text/TextLine;->mHasTabs:Z

    goto/32 :goto_12

    nop

    :goto_25
    iget v0, v7, Landroid/text/TextLine;->mLen:I

    goto/32 :goto_4f

    nop

    :goto_26
    move v8, v6

    goto/32 :goto_2b

    nop

    :goto_27
    move v15, v6

    :goto_28
    goto/32 :goto_b

    nop

    :goto_29
    add-float v5, v8, v14

    goto/32 :goto_45

    nop

    :goto_2a
    if-eq v10, v0, :cond_4

    goto/32 :goto_15

    :cond_4
    goto/32 :goto_25

    nop

    :goto_2b
    add-int/lit8 v10, v10, 0x1

    goto/32 :goto_d

    nop

    :goto_2c
    invoke-virtual {v1}, Landroid/text/Layout$Directions;->getRunCount()I

    move-result v9

    goto/32 :goto_21

    nop

    :goto_2d
    invoke-virtual {v1, v10}, Landroid/text/Layout$Directions;->getRunLength(I)I

    move-result v1

    goto/32 :goto_54

    nop

    :goto_2e
    move/from16 v17, v8

    goto/32 :goto_1

    nop

    :goto_2f
    move/from16 v17, v8

    :goto_30
    goto/32 :goto_3b

    nop

    :goto_31
    move-object/from16 v0, p0

    goto/32 :goto_4c

    nop

    :goto_32
    iget-object v1, v7, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    goto/32 :goto_a

    nop

    :goto_33
    move v10, v1

    :goto_34
    goto/32 :goto_f

    nop

    :goto_35
    move/from16 v8, v17

    goto/32 :goto_4d

    nop

    :goto_36
    iget v0, v7, Landroid/text/TextLine;->mDir:I

    goto/32 :goto_7

    nop

    :goto_37
    iget-object v1, v7, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    goto/32 :goto_6

    nop

    :goto_38
    move/from16 v17, v8

    goto/32 :goto_26

    nop

    :goto_39
    move v14, v1

    :goto_3a
    goto/32 :goto_5e

    nop

    :goto_3b
    return-void

    :goto_3c
    move/from16 v6, v16

    goto/32 :goto_56

    nop

    :goto_3d
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v12

    goto/32 :goto_37

    nop

    :goto_3e
    const/4 v0, 0x1

    :goto_3f
    goto/32 :goto_4b

    nop

    :goto_40
    goto/16 :goto_19

    :goto_41
    goto/32 :goto_9

    nop

    :goto_42
    const/16 v1, 0x9

    goto/32 :goto_8

    nop

    :goto_43
    move v15, v1

    goto/32 :goto_5a

    nop

    :goto_44
    if-ne v6, v12, :cond_5

    goto/32 :goto_19

    :cond_5
    goto/32 :goto_1a

    nop

    :goto_45
    add-int/lit8 v0, v9, -0x1

    goto/32 :goto_2a

    nop

    :goto_46
    goto :goto_34

    :goto_47
    goto/32 :goto_2f

    nop

    :goto_48
    mul-float/2addr v0, v14

    goto/32 :goto_13

    nop

    :goto_49
    iget-object v1, v7, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    goto/32 :goto_2d

    nop

    :goto_4a
    int-to-float v0, v0

    goto/32 :goto_48

    nop

    :goto_4b
    move/from16 v16, v0

    goto/32 :goto_31

    nop

    :goto_4c
    move-object/from16 v1, p1

    goto/32 :goto_1c

    nop

    :goto_4d
    goto :goto_5b

    :goto_4e
    goto/32 :goto_38

    nop

    :goto_4f
    if-ne v6, v0, :cond_6

    goto/32 :goto_4

    :cond_6
    goto/32 :goto_3

    nop

    :goto_50
    if-gt v11, v1, :cond_7

    goto/32 :goto_2

    :cond_7
    goto/32 :goto_2e

    nop

    :goto_51
    if-ne v8, v12, :cond_8

    goto/32 :goto_3a

    :cond_8
    goto/32 :goto_36

    nop

    :goto_52
    goto/16 :goto_1f

    :goto_53
    goto/32 :goto_1e

    nop

    :goto_54
    add-int/2addr v1, v11

    goto/32 :goto_5c

    nop

    :goto_55
    move v14, v0

    goto/32 :goto_43

    nop

    :goto_56
    invoke-direct/range {v0 .. v6}, Landroid/text/TextLine;->shapeRun(Landroid/text/TextShaper$GlyphsConsumer;IIZFZ)F

    move-result v0

    goto/32 :goto_5d

    nop

    :goto_57
    move v1, v11

    goto/32 :goto_24

    nop

    :goto_58
    iget-object v1, v7, Landroid/text/TextLine;->mDirections:Landroid/text/Layout$Directions;

    goto/32 :goto_2c

    nop

    :goto_59
    move/from16 v17, v8

    goto/32 :goto_5

    nop

    :goto_5a
    move v6, v2

    :goto_5b
    goto/32 :goto_20

    nop

    :goto_5c
    iget v2, v7, Landroid/text/TextLine;->mLen:I

    goto/32 :goto_3d

    nop

    :goto_5d
    add-float/2addr v14, v0

    goto/32 :goto_51

    nop

    :goto_5e
    add-int/lit8 v6, v8, 0x1

    goto/32 :goto_27

    nop
.end method
