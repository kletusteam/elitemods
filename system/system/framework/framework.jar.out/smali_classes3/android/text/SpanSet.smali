.class public Landroid/text/SpanSet;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final classType:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "+TE;>;"
        }
    .end annotation
.end field

.field numberOfSpans:I

.field spanEnds:[I

.field spanFlags:[I

.field spanStarts:[I

.field spans:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TE;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+TE;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/text/SpanSet;->classType:Ljava/lang/Class;

    const/4 v0, 0x0

    iput v0, p0, Landroid/text/SpanSet;->numberOfSpans:I

    return-void
.end method


# virtual methods
.method getNextTransition(II)I
    .locals 3

    goto/32 :goto_b

    nop

    :goto_0
    if-lt v0, v1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_6

    nop

    :goto_1
    move p2, v2

    :goto_2
    goto/32 :goto_d

    nop

    :goto_3
    goto :goto_c

    :goto_4
    goto/32 :goto_8

    nop

    :goto_5
    iget v1, p0, Landroid/text/SpanSet;->numberOfSpans:I

    goto/32 :goto_0

    nop

    :goto_6
    iget-object v1, p0, Landroid/text/SpanSet;->spanStarts:[I

    goto/32 :goto_12

    nop

    :goto_7
    if-lt v1, p2, :cond_1

    goto/32 :goto_11

    :cond_1
    goto/32 :goto_10

    nop

    :goto_8
    return p2

    :goto_9
    iget-object v2, p0, Landroid/text/SpanSet;->spanEnds:[I

    goto/32 :goto_a

    nop

    :goto_a
    aget v2, v2, v0

    goto/32 :goto_e

    nop

    :goto_b
    const/4 v0, 0x0

    :goto_c
    goto/32 :goto_5

    nop

    :goto_d
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_3

    nop

    :goto_e
    if-gt v1, p1, :cond_2

    goto/32 :goto_11

    :cond_2
    goto/32 :goto_7

    nop

    :goto_f
    if-lt v2, p2, :cond_3

    goto/32 :goto_2

    :cond_3
    goto/32 :goto_1

    nop

    :goto_10
    move p2, v1

    :goto_11
    goto/32 :goto_13

    nop

    :goto_12
    aget v1, v1, v0

    goto/32 :goto_9

    nop

    :goto_13
    if-gt v2, p1, :cond_4

    goto/32 :goto_2

    :cond_4
    goto/32 :goto_f

    nop
.end method

.method public hasSpansIntersecting(II)Z
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Landroid/text/SpanSet;->numberOfSpans:I

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Landroid/text/SpanSet;->spanStarts:[I

    aget v1, v1, v0

    if-ge v1, p2, :cond_1

    iget-object v1, p0, Landroid/text/SpanSet;->spanEnds:[I

    aget v1, v1, v0

    if-gt v1, p1, :cond_0

    goto :goto_1

    :cond_0
    const/4 v1, 0x1

    return v1

    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method public init(Landroid/text/Spanned;II)V
    .locals 10

    iget-object v0, p0, Landroid/text/SpanSet;->classType:Ljava/lang/Class;

    invoke-interface {p1, p2, p3, v0}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    array-length v1, v0

    if-lez v1, :cond_1

    iget-object v2, p0, Landroid/text/SpanSet;->spans:[Ljava/lang/Object;

    if-eqz v2, :cond_0

    array-length v2, v2

    if-ge v2, v1, :cond_1

    :cond_0
    iget-object v2, p0, Landroid/text/SpanSet;->classType:Ljava/lang/Class;

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/Object;

    iput-object v2, p0, Landroid/text/SpanSet;->spans:[Ljava/lang/Object;

    new-array v2, v1, [I

    iput-object v2, p0, Landroid/text/SpanSet;->spanStarts:[I

    new-array v2, v1, [I

    iput-object v2, p0, Landroid/text/SpanSet;->spanEnds:[I

    new-array v2, v1, [I

    iput-object v2, p0, Landroid/text/SpanSet;->spanFlags:[I

    :cond_1
    iget v2, p0, Landroid/text/SpanSet;->numberOfSpans:I

    const/4 v3, 0x0

    iput v3, p0, Landroid/text/SpanSet;->numberOfSpans:I

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_3

    aget-object v4, v0, v3

    invoke-interface {p1, v4}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    invoke-interface {p1, v4}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    if-ne v5, v6, :cond_2

    goto :goto_1

    :cond_2
    invoke-interface {p1, v4}, Landroid/text/Spanned;->getSpanFlags(Ljava/lang/Object;)I

    move-result v7

    iget-object v8, p0, Landroid/text/SpanSet;->spans:[Ljava/lang/Object;

    iget v9, p0, Landroid/text/SpanSet;->numberOfSpans:I

    aput-object v4, v8, v9

    iget-object v8, p0, Landroid/text/SpanSet;->spanStarts:[I

    aput v5, v8, v9

    iget-object v8, p0, Landroid/text/SpanSet;->spanEnds:[I

    aput v6, v8, v9

    iget-object v8, p0, Landroid/text/SpanSet;->spanFlags:[I

    aput v7, v8, v9

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Landroid/text/SpanSet;->numberOfSpans:I

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    iget v3, p0, Landroid/text/SpanSet;->numberOfSpans:I

    if-ge v3, v2, :cond_4

    iget-object v4, p0, Landroid/text/SpanSet;->spans:[Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v4, v3, v2, v5}, Ljava/util/Arrays;->fill([Ljava/lang/Object;IILjava/lang/Object;)V

    :cond_4
    return-void
.end method

.method public recycle()V
    .locals 4

    iget-object v0, p0, Landroid/text/SpanSet;->spans:[Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    iget v2, p0, Landroid/text/SpanSet;->numberOfSpans:I

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Ljava/util/Arrays;->fill([Ljava/lang/Object;IILjava/lang/Object;)V

    :cond_0
    return-void
.end method
