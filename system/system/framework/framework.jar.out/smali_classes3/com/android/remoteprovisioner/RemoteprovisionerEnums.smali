.class public final Lcom/android/remoteprovisioner/RemoteprovisionerEnums;
.super Ljava/lang/Object;


# static fields
.field public static final FETCH_GEEK_HTTP_ERROR:I = 0x16

.field public static final FETCH_GEEK_IO_EXCEPTION:I = 0x15

.field public static final FETCH_GEEK_TIMED_OUT:I = 0x14

.field public static final GENERATE_CSR_FAILED:I = 0xb

.field public static final GENERATE_KEYPAIR_FAILED:I = 0xa

.field public static final GET_POOL_STATUS_FAILED:I = 0xc

.field public static final INSERT_CHAIN_INTO_POOL_FAILED:I = 0xd

.field public static final INTERNAL_ERROR:I = 0x4

.field public static final INTERRUPTED:I = 0x7

.field public static final KEYS_SUCCESSFULLY_PROVISIONED:I = 0x1

.field public static final NO_NETWORK_CONNECTIVITY:I = 0x5

.field public static final NO_PROVISIONING_NEEDED:I = 0x2

.field public static final OUT_OF_ERROR_BUDGET:I = 0x6

.field public static final PROVISIONING_DISABLED:I = 0x3

.field public static final REMOTE_KEY_PROVISIONING_STATUS_UNKNOWN:I = 0x0

.field public static final SIGN_CERTS_DEVICE_NOT_REGISTERED:I = 0x21

.field public static final SIGN_CERTS_HTTP_ERROR:I = 0x20

.field public static final SIGN_CERTS_IO_EXCEPTION:I = 0x1f

.field public static final SIGN_CERTS_TIMED_OUT:I = 0x1e


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
