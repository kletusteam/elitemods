.class Lcom/android/internal/widget/ChildHelper;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/widget/ChildHelper$Callback;,
        Lcom/android/internal/widget/ChildHelper$Bucket;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "ChildrenHelper"


# instance fields
.field final mBucket:Lcom/android/internal/widget/ChildHelper$Bucket;

.field final mCallback:Lcom/android/internal/widget/ChildHelper$Callback;

.field final mHiddenViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/android/internal/widget/ChildHelper$Callback;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/internal/widget/ChildHelper;->mCallback:Lcom/android/internal/widget/ChildHelper$Callback;

    new-instance v0, Lcom/android/internal/widget/ChildHelper$Bucket;

    invoke-direct {v0}, Lcom/android/internal/widget/ChildHelper$Bucket;-><init>()V

    iput-object v0, p0, Lcom/android/internal/widget/ChildHelper;->mBucket:Lcom/android/internal/widget/ChildHelper$Bucket;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/internal/widget/ChildHelper;->mHiddenViews:Ljava/util/List;

    return-void
.end method

.method private getOffset(I)I
    .locals 5

    const/4 v0, -0x1

    if-gez p1, :cond_0

    return v0

    :cond_0
    iget-object v1, p0, Lcom/android/internal/widget/ChildHelper;->mCallback:Lcom/android/internal/widget/ChildHelper$Callback;

    invoke-interface {v1}, Lcom/android/internal/widget/ChildHelper$Callback;->getChildCount()I

    move-result v1

    move v2, p1

    :goto_0
    if-ge v2, v1, :cond_3

    iget-object v3, p0, Lcom/android/internal/widget/ChildHelper;->mBucket:Lcom/android/internal/widget/ChildHelper$Bucket;

    invoke-virtual {v3, v2}, Lcom/android/internal/widget/ChildHelper$Bucket;->countOnesBefore(I)I

    move-result v3

    sub-int v4, v2, v3

    sub-int v4, p1, v4

    if-nez v4, :cond_2

    :goto_1
    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper;->mBucket:Lcom/android/internal/widget/ChildHelper$Bucket;

    invoke-virtual {v0, v2}, Lcom/android/internal/widget/ChildHelper$Bucket;->get(I)Z

    move-result v0

    if-eqz v0, :cond_1

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    return v2

    :cond_2
    add-int/2addr v2, v4

    goto :goto_0

    :cond_3
    return v0
.end method

.method private hideViewInternal(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper;->mHiddenViews:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper;->mCallback:Lcom/android/internal/widget/ChildHelper$Callback;

    invoke-interface {v0, p1}, Lcom/android/internal/widget/ChildHelper$Callback;->onEnteredHiddenState(Landroid/view/View;)V

    return-void
.end method

.method private unhideViewInternal(Landroid/view/View;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper;->mHiddenViews:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper;->mCallback:Lcom/android/internal/widget/ChildHelper$Callback;

    invoke-interface {v0, p1}, Lcom/android/internal/widget/ChildHelper$Callback;->onLeftHiddenState(Landroid/view/View;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method addView(Landroid/view/View;IZ)V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    invoke-direct {p0, p2}, Lcom/android/internal/widget/ChildHelper;->getOffset(I)I

    move-result v0

    :goto_1
    goto/32 :goto_9

    nop

    :goto_2
    if-ltz p2, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_7

    nop

    :goto_3
    invoke-direct {p0, p1}, Lcom/android/internal/widget/ChildHelper;->hideViewInternal(Landroid/view/View;)V

    :goto_4
    goto/32 :goto_6

    nop

    :goto_5
    if-nez p3, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_3

    nop

    :goto_6
    iget-object v1, p0, Lcom/android/internal/widget/ChildHelper;->mCallback:Lcom/android/internal/widget/ChildHelper$Callback;

    goto/32 :goto_8

    nop

    :goto_7
    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper;->mCallback:Lcom/android/internal/widget/ChildHelper$Callback;

    goto/32 :goto_d

    nop

    :goto_8
    invoke-interface {v1, p1, v0}, Lcom/android/internal/widget/ChildHelper$Callback;->addView(Landroid/view/View;I)V

    goto/32 :goto_a

    nop

    :goto_9
    iget-object v1, p0, Lcom/android/internal/widget/ChildHelper;->mBucket:Lcom/android/internal/widget/ChildHelper$Bucket;

    goto/32 :goto_e

    nop

    :goto_a
    return-void

    :goto_b
    goto :goto_1

    :goto_c
    goto/32 :goto_0

    nop

    :goto_d
    invoke-interface {v0}, Lcom/android/internal/widget/ChildHelper$Callback;->getChildCount()I

    move-result v0

    goto/32 :goto_b

    nop

    :goto_e
    invoke-virtual {v1, v0, p3}, Lcom/android/internal/widget/ChildHelper$Bucket;->insert(IZ)V

    goto/32 :goto_5

    nop
.end method

.method addView(Landroid/view/View;Z)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p0, p1, v0, p2}, Lcom/android/internal/widget/ChildHelper;->addView(Landroid/view/View;IZ)V

    goto/32 :goto_2

    nop

    :goto_1
    const/4 v0, -0x1

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)V
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    invoke-interface {v1, p1, v0, p3}, Lcom/android/internal/widget/ChildHelper$Callback;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper;->mCallback:Lcom/android/internal/widget/ChildHelper$Callback;

    goto/32 :goto_3

    nop

    :goto_2
    return-void

    :goto_3
    invoke-interface {v0}, Lcom/android/internal/widget/ChildHelper$Callback;->getChildCount()I

    move-result v0

    goto/32 :goto_a

    nop

    :goto_4
    invoke-virtual {v1, v0, p4}, Lcom/android/internal/widget/ChildHelper$Bucket;->insert(IZ)V

    goto/32 :goto_c

    nop

    :goto_5
    if-ltz p2, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_1

    nop

    :goto_6
    invoke-direct {p0, p2}, Lcom/android/internal/widget/ChildHelper;->getOffset(I)I

    move-result v0

    :goto_7
    goto/32 :goto_9

    nop

    :goto_8
    iget-object v1, p0, Lcom/android/internal/widget/ChildHelper;->mCallback:Lcom/android/internal/widget/ChildHelper$Callback;

    goto/32 :goto_0

    nop

    :goto_9
    iget-object v1, p0, Lcom/android/internal/widget/ChildHelper;->mBucket:Lcom/android/internal/widget/ChildHelper$Bucket;

    goto/32 :goto_4

    nop

    :goto_a
    goto :goto_7

    :goto_b
    goto/32 :goto_6

    nop

    :goto_c
    if-nez p4, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_d

    nop

    :goto_d
    invoke-direct {p0, p1}, Lcom/android/internal/widget/ChildHelper;->hideViewInternal(Landroid/view/View;)V

    :goto_e
    goto/32 :goto_8

    nop
.end method

.method detachViewFromParent(I)V
    .locals 2

    goto/32 :goto_4

    nop

    :goto_0
    invoke-interface {v1, v0}, Lcom/android/internal/widget/ChildHelper$Callback;->detachViewFromParent(I)V

    goto/32 :goto_5

    nop

    :goto_1
    iget-object v1, p0, Lcom/android/internal/widget/ChildHelper;->mBucket:Lcom/android/internal/widget/ChildHelper$Bucket;

    goto/32 :goto_3

    nop

    :goto_2
    iget-object v1, p0, Lcom/android/internal/widget/ChildHelper;->mCallback:Lcom/android/internal/widget/ChildHelper$Callback;

    goto/32 :goto_0

    nop

    :goto_3
    invoke-virtual {v1, v0}, Lcom/android/internal/widget/ChildHelper$Bucket;->remove(I)Z

    goto/32 :goto_2

    nop

    :goto_4
    invoke-direct {p0, p1}, Lcom/android/internal/widget/ChildHelper;->getOffset(I)I

    move-result v0

    goto/32 :goto_1

    nop

    :goto_5
    return-void
.end method

.method findHiddenNonRemovedView(I)Landroid/view/View;
    .locals 5

    goto/32 :goto_13

    nop

    :goto_0
    invoke-virtual {v3}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isInvalid()Z

    move-result v4

    goto/32 :goto_11

    nop

    :goto_1
    goto :goto_5

    :goto_2
    goto/32 :goto_12

    nop

    :goto_3
    if-eqz v4, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_9

    nop

    :goto_4
    const/4 v1, 0x0

    :goto_5
    goto/32 :goto_f

    nop

    :goto_6
    if-eq v4, p1, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_0

    nop

    :goto_7
    invoke-virtual {v3}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->getLayoutPosition()I

    move-result v4

    goto/32 :goto_6

    nop

    :goto_8
    invoke-interface {v3, v2}, Lcom/android/internal/widget/ChildHelper$Callback;->getChildViewHolder(Landroid/view/View;)Lcom/android/internal/widget/RecyclerView$ViewHolder;

    move-result-object v3

    goto/32 :goto_7

    nop

    :goto_9
    return-object v2

    :goto_a
    goto/32 :goto_10

    nop

    :goto_b
    iget-object v3, p0, Lcom/android/internal/widget/ChildHelper;->mCallback:Lcom/android/internal/widget/ChildHelper$Callback;

    goto/32 :goto_8

    nop

    :goto_c
    iget-object v2, p0, Lcom/android/internal/widget/ChildHelper;->mHiddenViews:Ljava/util/List;

    goto/32 :goto_16

    nop

    :goto_d
    check-cast v2, Landroid/view/View;

    goto/32 :goto_b

    nop

    :goto_e
    return-object v1

    :goto_f
    if-lt v1, v0, :cond_2

    goto/32 :goto_2

    :cond_2
    goto/32 :goto_c

    nop

    :goto_10
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_1

    nop

    :goto_11
    if-eqz v4, :cond_3

    goto/32 :goto_a

    :cond_3
    goto/32 :goto_15

    nop

    :goto_12
    const/4 v1, 0x0

    goto/32 :goto_e

    nop

    :goto_13
    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper;->mHiddenViews:Ljava/util/List;

    goto/32 :goto_14

    nop

    :goto_14
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto/32 :goto_4

    nop

    :goto_15
    invoke-virtual {v3}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isRemoved()Z

    move-result v4

    goto/32 :goto_3

    nop

    :goto_16
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_d

    nop
.end method

.method getChildAt(I)Landroid/view/View;
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    return-object v1

    :goto_1
    invoke-direct {p0, p1}, Lcom/android/internal/widget/ChildHelper;->getOffset(I)I

    move-result v0

    goto/32 :goto_2

    nop

    :goto_2
    iget-object v1, p0, Lcom/android/internal/widget/ChildHelper;->mCallback:Lcom/android/internal/widget/ChildHelper$Callback;

    goto/32 :goto_3

    nop

    :goto_3
    invoke-interface {v1, v0}, Lcom/android/internal/widget/ChildHelper$Callback;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    goto/32 :goto_0

    nop
.end method

.method getChildCount()I
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper;->mCallback:Lcom/android/internal/widget/ChildHelper$Callback;

    goto/32 :goto_2

    nop

    :goto_1
    sub-int/2addr v0, v1

    goto/32 :goto_4

    nop

    :goto_2
    invoke-interface {v0}, Lcom/android/internal/widget/ChildHelper$Callback;->getChildCount()I

    move-result v0

    goto/32 :goto_5

    nop

    :goto_3
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    goto/32 :goto_1

    nop

    :goto_4
    return v0

    :goto_5
    iget-object v1, p0, Lcom/android/internal/widget/ChildHelper;->mHiddenViews:Ljava/util/List;

    goto/32 :goto_3

    nop
.end method

.method getUnfilteredChildAt(I)Landroid/view/View;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper;->mCallback:Lcom/android/internal/widget/ChildHelper$Callback;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-interface {v0, p1}, Lcom/android/internal/widget/ChildHelper$Callback;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto/32 :goto_0

    nop
.end method

.method getUnfilteredChildCount()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper;->mCallback:Lcom/android/internal/widget/ChildHelper$Callback;

    goto/32 :goto_2

    nop

    :goto_1
    return v0

    :goto_2
    invoke-interface {v0}, Lcom/android/internal/widget/ChildHelper$Callback;->getChildCount()I

    move-result v0

    goto/32 :goto_1

    nop
.end method

.method hide(Landroid/view/View;)V
    .locals 4

    goto/32 :goto_1

    nop

    :goto_0
    invoke-direct {p0, p1}, Lcom/android/internal/widget/ChildHelper;->hideViewInternal(Landroid/view/View;)V

    goto/32 :goto_b

    nop

    :goto_1
    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper;->mCallback:Lcom/android/internal/widget/ChildHelper$Callback;

    goto/32 :goto_4

    nop

    :goto_2
    throw v1

    :goto_3
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_5

    nop

    :goto_4
    invoke-interface {v0, p1}, Lcom/android/internal/widget/ChildHelper$Callback;->indexOfChild(Landroid/view/View;)I

    move-result v0

    goto/32 :goto_d

    nop

    :goto_5
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_6

    nop

    :goto_6
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_2

    nop

    :goto_7
    new-instance v1, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_a

    nop

    :goto_8
    iget-object v1, p0, Lcom/android/internal/widget/ChildHelper;->mBucket:Lcom/android/internal/widget/ChildHelper$Bucket;

    goto/32 :goto_e

    nop

    :goto_9
    const-string/jumbo v3, "view is not a child, cannot hide "

    goto/32 :goto_10

    nop

    :goto_a
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_f

    nop

    :goto_b
    return-void

    :goto_c
    goto/32 :goto_7

    nop

    :goto_d
    if-gez v0, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_8

    nop

    :goto_e
    invoke-virtual {v1, v0}, Lcom/android/internal/widget/ChildHelper$Bucket;->set(I)V

    goto/32 :goto_0

    nop

    :goto_f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_9

    nop

    :goto_10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_3

    nop
.end method

.method indexOfChild(Landroid/view/View;)I
    .locals 3

    goto/32 :goto_4

    nop

    :goto_0
    invoke-interface {v0, p1}, Lcom/android/internal/widget/ChildHelper$Callback;->indexOfChild(Landroid/view/View;)I

    move-result v0

    goto/32 :goto_e

    nop

    :goto_1
    return v1

    :goto_2
    goto/32 :goto_3

    nop

    :goto_3
    iget-object v1, p0, Lcom/android/internal/widget/ChildHelper;->mBucket:Lcom/android/internal/widget/ChildHelper$Bucket;

    goto/32 :goto_9

    nop

    :goto_4
    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper;->mCallback:Lcom/android/internal/widget/ChildHelper$Callback;

    goto/32 :goto_0

    nop

    :goto_5
    iget-object v2, p0, Lcom/android/internal/widget/ChildHelper;->mBucket:Lcom/android/internal/widget/ChildHelper$Bucket;

    goto/32 :goto_6

    nop

    :goto_6
    invoke-virtual {v2, v0}, Lcom/android/internal/widget/ChildHelper$Bucket;->get(I)Z

    move-result v2

    goto/32 :goto_c

    nop

    :goto_7
    if-eq v0, v1, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_a

    nop

    :goto_8
    return v1

    :goto_9
    invoke-virtual {v1, v0}, Lcom/android/internal/widget/ChildHelper$Bucket;->countOnesBefore(I)I

    move-result v1

    goto/32 :goto_d

    nop

    :goto_a
    return v1

    :goto_b
    goto/32 :goto_5

    nop

    :goto_c
    if-nez v2, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_1

    nop

    :goto_d
    sub-int v1, v0, v1

    goto/32 :goto_8

    nop

    :goto_e
    const/4 v1, -0x1

    goto/32 :goto_7

    nop
.end method

.method isHidden(Landroid/view/View;)Z
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_1

    nop

    :goto_1
    return v0

    :goto_2
    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper;->mHiddenViews:Ljava/util/List;

    goto/32 :goto_0

    nop
.end method

.method removeAllViewsUnfiltered()V
    .locals 3

    goto/32 :goto_f

    nop

    :goto_0
    iget-object v2, p0, Lcom/android/internal/widget/ChildHelper;->mHiddenViews:Ljava/util/List;

    goto/32 :goto_11

    nop

    :goto_1
    add-int/lit8 v0, v0, -0x1

    :goto_2
    goto/32 :goto_8

    nop

    :goto_3
    invoke-virtual {v0}, Lcom/android/internal/widget/ChildHelper$Bucket;->reset()V

    goto/32 :goto_c

    nop

    :goto_4
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto/32 :goto_1

    nop

    :goto_5
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_12

    nop

    :goto_6
    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto/32 :goto_5

    nop

    :goto_7
    invoke-interface {v0}, Lcom/android/internal/widget/ChildHelper$Callback;->removeAllViews()V

    goto/32 :goto_e

    nop

    :goto_8
    if-gez v0, :cond_0

    goto/32 :goto_13

    :cond_0
    goto/32 :goto_d

    nop

    :goto_9
    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper;->mCallback:Lcom/android/internal/widget/ChildHelper$Callback;

    goto/32 :goto_7

    nop

    :goto_a
    iget-object v1, p0, Lcom/android/internal/widget/ChildHelper;->mHiddenViews:Ljava/util/List;

    goto/32 :goto_6

    nop

    :goto_b
    invoke-interface {v1, v2}, Lcom/android/internal/widget/ChildHelper$Callback;->onLeftHiddenState(Landroid/view/View;)V

    goto/32 :goto_a

    nop

    :goto_c
    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper;->mHiddenViews:Ljava/util/List;

    goto/32 :goto_4

    nop

    :goto_d
    iget-object v1, p0, Lcom/android/internal/widget/ChildHelper;->mCallback:Lcom/android/internal/widget/ChildHelper$Callback;

    goto/32 :goto_0

    nop

    :goto_e
    return-void

    :goto_f
    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper;->mBucket:Lcom/android/internal/widget/ChildHelper$Bucket;

    goto/32 :goto_3

    nop

    :goto_10
    check-cast v2, Landroid/view/View;

    goto/32 :goto_b

    nop

    :goto_11
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_10

    nop

    :goto_12
    goto :goto_2

    :goto_13
    goto/32 :goto_9

    nop
.end method

.method removeView(Landroid/view/View;)V
    .locals 2

    goto/32 :goto_a

    nop

    :goto_0
    invoke-interface {v0, p1}, Lcom/android/internal/widget/ChildHelper$Callback;->indexOfChild(Landroid/view/View;)I

    move-result v0

    goto/32 :goto_c

    nop

    :goto_1
    invoke-direct {p0, p1}, Lcom/android/internal/widget/ChildHelper;->unhideViewInternal(Landroid/view/View;)Z

    :goto_2
    goto/32 :goto_8

    nop

    :goto_3
    if-nez v1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop

    :goto_4
    return-void

    :goto_5
    goto/32 :goto_b

    nop

    :goto_6
    invoke-virtual {v1, v0}, Lcom/android/internal/widget/ChildHelper$Bucket;->remove(I)Z

    move-result v1

    goto/32 :goto_3

    nop

    :goto_7
    invoke-interface {v1, v0}, Lcom/android/internal/widget/ChildHelper$Callback;->removeViewAt(I)V

    goto/32 :goto_9

    nop

    :goto_8
    iget-object v1, p0, Lcom/android/internal/widget/ChildHelper;->mCallback:Lcom/android/internal/widget/ChildHelper$Callback;

    goto/32 :goto_7

    nop

    :goto_9
    return-void

    :goto_a
    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper;->mCallback:Lcom/android/internal/widget/ChildHelper$Callback;

    goto/32 :goto_0

    nop

    :goto_b
    iget-object v1, p0, Lcom/android/internal/widget/ChildHelper;->mBucket:Lcom/android/internal/widget/ChildHelper$Bucket;

    goto/32 :goto_6

    nop

    :goto_c
    if-ltz v0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_4

    nop
.end method

.method removeViewAt(I)V
    .locals 3

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-direct {p0, p1}, Lcom/android/internal/widget/ChildHelper;->getOffset(I)I

    move-result v0

    goto/32 :goto_d

    nop

    :goto_2
    if-nez v2, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_3

    nop

    :goto_3
    invoke-direct {p0, v1}, Lcom/android/internal/widget/ChildHelper;->unhideViewInternal(Landroid/view/View;)Z

    :goto_4
    goto/32 :goto_6

    nop

    :goto_5
    invoke-interface {v2, v0}, Lcom/android/internal/widget/ChildHelper$Callback;->removeViewAt(I)V

    goto/32 :goto_0

    nop

    :goto_6
    iget-object v2, p0, Lcom/android/internal/widget/ChildHelper;->mCallback:Lcom/android/internal/widget/ChildHelper$Callback;

    goto/32 :goto_5

    nop

    :goto_7
    iget-object v2, p0, Lcom/android/internal/widget/ChildHelper;->mBucket:Lcom/android/internal/widget/ChildHelper$Bucket;

    goto/32 :goto_c

    nop

    :goto_8
    invoke-interface {v1, v0}, Lcom/android/internal/widget/ChildHelper$Callback;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    goto/32 :goto_b

    nop

    :goto_9
    return-void

    :goto_a
    goto/32 :goto_7

    nop

    :goto_b
    if-eqz v1, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_9

    nop

    :goto_c
    invoke-virtual {v2, v0}, Lcom/android/internal/widget/ChildHelper$Bucket;->remove(I)Z

    move-result v2

    goto/32 :goto_2

    nop

    :goto_d
    iget-object v1, p0, Lcom/android/internal/widget/ChildHelper;->mCallback:Lcom/android/internal/widget/ChildHelper$Callback;

    goto/32 :goto_8

    nop
.end method

.method removeViewIfHidden(Landroid/view/View;)Z
    .locals 3

    goto/32 :goto_c

    nop

    :goto_0
    return v1

    :goto_1
    invoke-interface {v0, p1}, Lcom/android/internal/widget/ChildHelper$Callback;->indexOfChild(Landroid/view/View;)I

    move-result v0

    goto/32 :goto_f

    nop

    :goto_2
    invoke-direct {p0, p1}, Lcom/android/internal/widget/ChildHelper;->unhideViewInternal(Landroid/view/View;)Z

    goto/32 :goto_5

    nop

    :goto_3
    if-nez v2, :cond_0

    goto/32 :goto_13

    :cond_0
    goto/32 :goto_9

    nop

    :goto_4
    invoke-virtual {v2, v0}, Lcom/android/internal/widget/ChildHelper$Bucket;->get(I)Z

    move-result v2

    goto/32 :goto_3

    nop

    :goto_5
    return v1

    :goto_6
    goto/32 :goto_a

    nop

    :goto_7
    invoke-interface {v2, v0}, Lcom/android/internal/widget/ChildHelper$Callback;->removeViewAt(I)V

    goto/32 :goto_12

    nop

    :goto_8
    const/4 v1, 0x0

    goto/32 :goto_0

    nop

    :goto_9
    iget-object v2, p0, Lcom/android/internal/widget/ChildHelper;->mBucket:Lcom/android/internal/widget/ChildHelper$Bucket;

    goto/32 :goto_10

    nop

    :goto_a
    iget-object v2, p0, Lcom/android/internal/widget/ChildHelper;->mBucket:Lcom/android/internal/widget/ChildHelper$Bucket;

    goto/32 :goto_4

    nop

    :goto_b
    invoke-direct {p0, p1}, Lcom/android/internal/widget/ChildHelper;->unhideViewInternal(Landroid/view/View;)Z

    goto/32 :goto_d

    nop

    :goto_c
    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper;->mCallback:Lcom/android/internal/widget/ChildHelper$Callback;

    goto/32 :goto_1

    nop

    :goto_d
    iget-object v2, p0, Lcom/android/internal/widget/ChildHelper;->mCallback:Lcom/android/internal/widget/ChildHelper$Callback;

    goto/32 :goto_7

    nop

    :goto_e
    if-eq v0, v2, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_2

    nop

    :goto_f
    const/4 v1, 0x1

    goto/32 :goto_11

    nop

    :goto_10
    invoke-virtual {v2, v0}, Lcom/android/internal/widget/ChildHelper$Bucket;->remove(I)Z

    goto/32 :goto_b

    nop

    :goto_11
    const/4 v2, -0x1

    goto/32 :goto_e

    nop

    :goto_12
    return v1

    :goto_13
    goto/32 :goto_8

    nop
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/internal/widget/ChildHelper;->mBucket:Lcom/android/internal/widget/ChildHelper$Bucket;

    invoke-virtual {v1}, Lcom/android/internal/widget/ChildHelper$Bucket;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", hidden list:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/internal/widget/ChildHelper;->mHiddenViews:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method unhide(Landroid/view/View;)V
    .locals 4

    goto/32 :goto_4

    nop

    :goto_0
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_10

    nop

    :goto_1
    invoke-virtual {v1, v0}, Lcom/android/internal/widget/ChildHelper$Bucket;->get(I)Z

    move-result v1

    goto/32 :goto_a

    nop

    :goto_2
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_f

    nop

    :goto_3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1a

    nop

    :goto_4
    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper;->mCallback:Lcom/android/internal/widget/ChildHelper$Callback;

    goto/32 :goto_15

    nop

    :goto_5
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_17

    nop

    :goto_6
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_18

    nop

    :goto_7
    iget-object v1, p0, Lcom/android/internal/widget/ChildHelper;->mBucket:Lcom/android/internal/widget/ChildHelper$Bucket;

    goto/32 :goto_1

    nop

    :goto_8
    new-instance v1, Ljava/lang/RuntimeException;

    goto/32 :goto_1d

    nop

    :goto_9
    if-gez v0, :cond_0

    goto/32 :goto_11

    :cond_0
    goto/32 :goto_7

    nop

    :goto_a
    if-nez v1, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_19

    nop

    :goto_b
    invoke-direct {p0, p1}, Lcom/android/internal/widget/ChildHelper;->unhideViewInternal(Landroid/view/View;)Z

    goto/32 :goto_d

    nop

    :goto_c
    const-string/jumbo v3, "view is not a child, cannot hide "

    goto/32 :goto_13

    nop

    :goto_d
    return-void

    :goto_e
    goto/32 :goto_8

    nop

    :goto_f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_1c

    nop

    :goto_10
    throw v1

    :goto_11
    goto/32 :goto_14

    nop

    :goto_12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_5

    nop

    :goto_13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_2

    nop

    :goto_14
    new-instance v1, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_6

    nop

    :goto_15
    invoke-interface {v0, p1}, Lcom/android/internal/widget/ChildHelper$Callback;->indexOfChild(Landroid/view/View;)I

    move-result v0

    goto/32 :goto_9

    nop

    :goto_16
    invoke-virtual {v1, v0}, Lcom/android/internal/widget/ChildHelper$Bucket;->clear(I)V

    goto/32 :goto_b

    nop

    :goto_17
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_0

    nop

    :goto_18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_c

    nop

    :goto_19
    iget-object v1, p0, Lcom/android/internal/widget/ChildHelper;->mBucket:Lcom/android/internal/widget/ChildHelper$Bucket;

    goto/32 :goto_16

    nop

    :goto_1a
    const-string/jumbo v3, "trying to unhide a view that was not hidden"

    goto/32 :goto_12

    nop

    :goto_1b
    throw v1

    :goto_1c
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_1b

    nop

    :goto_1d
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_3

    nop
.end method
