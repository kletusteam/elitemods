.class public abstract Lcom/android/internal/widget/RecyclerView$ViewHolder;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/RecyclerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ViewHolder"
.end annotation


# static fields
.field static final FLAG_ADAPTER_FULLUPDATE:I = 0x400

.field static final FLAG_ADAPTER_POSITION_UNKNOWN:I = 0x200

.field static final FLAG_APPEARED_IN_PRE_LAYOUT:I = 0x1000

.field static final FLAG_BOUNCED_FROM_HIDDEN_LIST:I = 0x2000

.field static final FLAG_BOUND:I = 0x1

.field static final FLAG_IGNORE:I = 0x80

.field static final FLAG_INVALID:I = 0x4

.field static final FLAG_MOVED:I = 0x800

.field static final FLAG_NOT_RECYCLABLE:I = 0x10

.field static final FLAG_REMOVED:I = 0x8

.field static final FLAG_RETURNED_FROM_SCRAP:I = 0x20

.field static final FLAG_TMP_DETACHED:I = 0x100

.field static final FLAG_UPDATE:I = 0x2

.field private static final FULLUPDATE_PAYLOADS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field static final PENDING_ACCESSIBILITY_STATE_NOT_SET:I = -0x1


# instance fields
.field public final itemView:Landroid/view/View;

.field private mFlags:I

.field private mInChangeScrap:Z

.field private mIsRecyclableCount:I

.field mItemId:J

.field mItemViewType:I

.field mNestedRecyclerView:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/android/internal/widget/RecyclerView;",
            ">;"
        }
    .end annotation
.end field

.field mOldPosition:I

.field mOwnerRecyclerView:Lcom/android/internal/widget/RecyclerView;

.field mPayloads:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field mPendingAccessibilityState:I

.field mPosition:I

.field mPreLayoutPosition:I

.field private mScrapContainer:Lcom/android/internal/widget/RecyclerView$Recycler;

.field mShadowedHolder:Lcom/android/internal/widget/RecyclerView$ViewHolder;

.field mShadowingHolder:Lcom/android/internal/widget/RecyclerView$ViewHolder;

.field mUnmodifiedPayloads:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mWasImportantForAccessibilityBeforeHidden:I


# direct methods
.method static bridge synthetic -$$Nest$fgetmFlags(Lcom/android/internal/widget/RecyclerView$ViewHolder;)I
    .locals 0

    iget p0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmInChangeScrap(Lcom/android/internal/widget/RecyclerView$ViewHolder;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mInChangeScrap:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmInChangeScrap(Lcom/android/internal/widget/RecyclerView$ViewHolder;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mInChangeScrap:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmScrapContainer(Lcom/android/internal/widget/RecyclerView$ViewHolder;Lcom/android/internal/widget/RecyclerView$Recycler;)V
    .locals 0

    iput-object p1, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mScrapContainer:Lcom/android/internal/widget/RecyclerView$Recycler;

    return-void
.end method

.method static bridge synthetic -$$Nest$mdoesTransientStatePreventRecycling(Lcom/android/internal/widget/RecyclerView$ViewHolder;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->doesTransientStatePreventRecycling()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$monEnteredHiddenState(Lcom/android/internal/widget/RecyclerView$ViewHolder;Lcom/android/internal/widget/RecyclerView;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->onEnteredHiddenState(Lcom/android/internal/widget/RecyclerView;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monLeftHiddenState(Lcom/android/internal/widget/RecyclerView$ViewHolder;Lcom/android/internal/widget/RecyclerView;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->onLeftHiddenState(Lcom/android/internal/widget/RecyclerView;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mshouldBeKeptAsChild(Lcom/android/internal/widget/RecyclerView$ViewHolder;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->shouldBeKeptAsChild()Z

    move-result p0

    return p0
.end method

.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    sput-object v0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->FULLUPDATE_PAYLOADS:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPosition:I

    iput v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mOldPosition:I

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mItemId:J

    iput v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mItemViewType:I

    iput v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPreLayoutPosition:I

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mShadowedHolder:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    iput-object v1, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mShadowingHolder:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    iput-object v1, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPayloads:Ljava/util/List;

    iput-object v1, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mUnmodifiedPayloads:Ljava/util/List;

    const/4 v2, 0x0

    iput v2, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mIsRecyclableCount:I

    iput-object v1, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mScrapContainer:Lcom/android/internal/widget/RecyclerView$Recycler;

    iput-boolean v2, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mInChangeScrap:Z

    iput v2, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mWasImportantForAccessibilityBeforeHidden:I

    iput v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPendingAccessibilityState:I

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "itemView may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private createPayloadsIfNeeded()V
    .locals 1

    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPayloads:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPayloads:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mUnmodifiedPayloads:Ljava/util/List;

    :cond_0
    return-void
.end method

.method private doesTransientStatePreventRecycling()Z
    .locals 1

    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    and-int/lit8 v0, v0, 0x10

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->hasTransientState()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private onEnteredHiddenState(Lcom/android/internal/widget/RecyclerView;)V
    .locals 1

    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getImportantForAccessibility()I

    move-result v0

    iput v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mWasImportantForAccessibilityBeforeHidden:I

    const/4 v0, 0x4

    invoke-virtual {p1, p0, v0}, Lcom/android/internal/widget/RecyclerView;->setChildImportantForAccessibilityInternal(Lcom/android/internal/widget/RecyclerView$ViewHolder;I)Z

    return-void
.end method

.method private onLeftHiddenState(Lcom/android/internal/widget/RecyclerView;)V
    .locals 1

    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mWasImportantForAccessibilityBeforeHidden:I

    invoke-virtual {p1, p0, v0}, Lcom/android/internal/widget/RecyclerView;->setChildImportantForAccessibilityInternal(Lcom/android/internal/widget/RecyclerView$ViewHolder;I)Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mWasImportantForAccessibilityBeforeHidden:I

    return-void
.end method

.method private shouldBeKeptAsChild()Z
    .locals 1

    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method addChangePayload(Ljava/lang/Object;)V
    .locals 2

    goto/32 :goto_6

    nop

    :goto_0
    and-int/2addr v0, v1

    goto/32 :goto_1

    nop

    :goto_1
    if-eqz v0, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_c

    nop

    :goto_2
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->addFlags(I)V

    goto/32 :goto_7

    nop

    :goto_3
    iget v1, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    goto/32 :goto_0

    nop

    :goto_4
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPayloads:Ljava/util/List;

    goto/32 :goto_9

    nop

    :goto_5
    return-void

    :goto_6
    const/16 v0, 0x400

    goto/32 :goto_b

    nop

    :goto_7
    goto :goto_a

    :goto_8
    goto/32 :goto_3

    nop

    :goto_9
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_a
    goto/32 :goto_5

    nop

    :goto_b
    if-eqz p1, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_2

    nop

    :goto_c
    invoke-direct {p0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->createPayloadsIfNeeded()V

    goto/32 :goto_4

    nop
.end method

.method addFlags(I)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    or-int/2addr v0, p1

    goto/32 :goto_3

    nop

    :goto_3
    iput v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    goto/32 :goto_1

    nop
.end method

.method clearOldPosition()V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    iput v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPreLayoutPosition:I

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    const/4 v0, -0x1

    goto/32 :goto_3

    nop

    :goto_3
    iput v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mOldPosition:I

    goto/32 :goto_0

    nop
.end method

.method clearPayload()V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    return-void

    :goto_1
    invoke-interface {v0}, Ljava/util/List;->clear()V

    :goto_2
    goto/32 :goto_7

    nop

    :goto_3
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPayloads:Ljava/util/List;

    goto/32 :goto_5

    nop

    :goto_4
    iput v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    goto/32 :goto_0

    nop

    :goto_5
    if-nez v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop

    :goto_6
    and-int/lit16 v0, v0, -0x401

    goto/32 :goto_4

    nop

    :goto_7
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    goto/32 :goto_6

    nop
.end method

.method clearReturnedFromScrapFlag()V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    return-void

    :goto_1
    and-int/lit8 v0, v0, -0x21

    goto/32 :goto_2

    nop

    :goto_2
    iput v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    goto/32 :goto_0

    nop

    :goto_3
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    goto/32 :goto_1

    nop
.end method

.method clearTmpDetachFlag()V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    goto/32 :goto_1

    nop

    :goto_1
    and-int/lit16 v0, v0, -0x101

    goto/32 :goto_2

    nop

    :goto_2
    iput v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    goto/32 :goto_3

    nop

    :goto_3
    return-void
.end method

.method flagRemovedAndOffsetPosition(IIZ)V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    invoke-virtual {p0, p2, p3}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->offsetPosition(IZ)V

    goto/32 :goto_4

    nop

    :goto_1
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->addFlags(I)V

    goto/32 :goto_0

    nop

    :goto_2
    return-void

    :goto_3
    const/16 v0, 0x8

    goto/32 :goto_1

    nop

    :goto_4
    iput p1, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPosition:I

    goto/32 :goto_2

    nop
.end method

.method public final getAdapterPosition()I
    .locals 1

    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mOwnerRecyclerView:Lcom/android/internal/widget/RecyclerView;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    return v0

    :cond_0
    invoke-virtual {v0, p0}, Lcom/android/internal/widget/RecyclerView;->getAdapterPositionFor(Lcom/android/internal/widget/RecyclerView$ViewHolder;)I

    move-result v0

    return v0
.end method

.method public final getItemId()J
    .locals 2

    iget-wide v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mItemId:J

    return-wide v0
.end method

.method public final getItemViewType()I
    .locals 1

    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mItemViewType:I

    return v0
.end method

.method public final getLayoutPosition()I
    .locals 2

    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPreLayoutPosition:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPosition:I

    :cond_0
    return v0
.end method

.method public final getOldPosition()I
    .locals 1

    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mOldPosition:I

    return v0
.end method

.method public final getPosition()I
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPreLayoutPosition:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPosition:I

    :cond_0
    return v0
.end method

.method getUnmodifiedPayloads()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    goto/32 :goto_c

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPayloads:Ljava/util/List;

    goto/32 :goto_4

    nop

    :goto_1
    and-int/lit16 v0, v0, 0x400

    goto/32 :goto_7

    nop

    :goto_2
    return-object v0

    :goto_3
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mUnmodifiedPayloads:Ljava/util/List;

    goto/32 :goto_f

    nop

    :goto_4
    if-nez v0, :cond_0

    goto/32 :goto_10

    :cond_0
    goto/32 :goto_8

    nop

    :goto_5
    return-object v0

    :goto_6
    goto/32 :goto_b

    nop

    :goto_7
    if-eqz v0, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_0

    nop

    :goto_8
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto/32 :goto_d

    nop

    :goto_9
    goto :goto_10

    :goto_a
    goto/32 :goto_3

    nop

    :goto_b
    sget-object v0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->FULLUPDATE_PAYLOADS:Ljava/util/List;

    goto/32 :goto_2

    nop

    :goto_c
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    goto/32 :goto_1

    nop

    :goto_d
    if-eqz v0, :cond_2

    goto/32 :goto_a

    :cond_2
    goto/32 :goto_9

    nop

    :goto_e
    sget-object v0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->FULLUPDATE_PAYLOADS:Ljava/util/List;

    goto/32 :goto_5

    nop

    :goto_f
    return-object v0

    :goto_10
    goto/32 :goto_e

    nop
.end method

.method hasAnyOfTheFlags(I)Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    goto/32 :goto_5

    nop

    :goto_1
    const/4 v0, 0x0

    :goto_2
    goto/32 :goto_4

    nop

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_6

    nop

    :goto_4
    return v0

    :goto_5
    and-int/2addr v0, p1

    goto/32 :goto_3

    nop

    :goto_6
    const/4 v0, 0x1

    goto/32 :goto_7

    nop

    :goto_7
    goto :goto_2

    :goto_8
    goto/32 :goto_1

    nop
.end method

.method isAdapterPositionUnknown()Z
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop

    :goto_1
    goto :goto_6

    :goto_2
    goto/32 :goto_b

    nop

    :goto_3
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    goto/32 :goto_9

    nop

    :goto_4
    if-eqz v0, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_a

    nop

    :goto_5
    goto :goto_8

    :goto_6
    goto/32 :goto_7

    nop

    :goto_7
    const/4 v0, 0x1

    :goto_8
    goto/32 :goto_c

    nop

    :goto_9
    and-int/lit16 v0, v0, 0x200

    goto/32 :goto_4

    nop

    :goto_a
    invoke-virtual {p0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isInvalid()Z

    move-result v0

    goto/32 :goto_0

    nop

    :goto_b
    const/4 v0, 0x0

    goto/32 :goto_5

    nop

    :goto_c
    return v0
.end method

.method isBound()Z
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_3

    nop

    :goto_1
    and-int/2addr v0, v1

    goto/32 :goto_0

    nop

    :goto_2
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    goto/32 :goto_5

    nop

    :goto_3
    goto :goto_7

    :goto_4
    goto/32 :goto_6

    nop

    :goto_5
    const/4 v1, 0x1

    goto/32 :goto_1

    nop

    :goto_6
    const/4 v1, 0x0

    :goto_7
    goto/32 :goto_8

    nop

    :goto_8
    return v1
.end method

.method isInvalid()Z
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    goto :goto_8

    :goto_1
    goto/32 :goto_7

    nop

    :goto_2
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    goto/32 :goto_3

    nop

    :goto_3
    and-int/lit8 v0, v0, 0x4

    goto/32 :goto_6

    nop

    :goto_4
    return v0

    :goto_5
    const/4 v0, 0x1

    goto/32 :goto_0

    nop

    :goto_6
    if-nez v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_5

    nop

    :goto_7
    const/4 v0, 0x0

    :goto_8
    goto/32 :goto_4

    nop
.end method

.method public final isRecyclable()Z
    .locals 1

    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    and-int/lit8 v0, v0, 0x10

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->hasTransientState()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method isRemoved()Z
    .locals 1

    goto/32 :goto_6

    nop

    :goto_0
    goto :goto_4

    :goto_1
    goto/32 :goto_3

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_5

    nop

    :goto_3
    const/4 v0, 0x0

    :goto_4
    goto/32 :goto_8

    nop

    :goto_5
    const/4 v0, 0x1

    goto/32 :goto_0

    nop

    :goto_6
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    goto/32 :goto_7

    nop

    :goto_7
    and-int/lit8 v0, v0, 0x8

    goto/32 :goto_2

    nop

    :goto_8
    return v0
.end method

.method isScrap()Z
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_3

    nop

    :goto_1
    const/4 v0, 0x0

    :goto_2
    goto/32 :goto_5

    nop

    :goto_3
    const/4 v0, 0x1

    goto/32 :goto_6

    nop

    :goto_4
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mScrapContainer:Lcom/android/internal/widget/RecyclerView$Recycler;

    goto/32 :goto_0

    nop

    :goto_5
    return v0

    :goto_6
    goto :goto_2

    :goto_7
    goto/32 :goto_1

    nop
.end method

.method isTmpDetached()Z
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    return v0

    :goto_1
    and-int/lit16 v0, v0, 0x100

    goto/32 :goto_8

    nop

    :goto_2
    const/4 v0, 0x0

    :goto_3
    goto/32 :goto_0

    nop

    :goto_4
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    goto/32 :goto_1

    nop

    :goto_5
    goto :goto_3

    :goto_6
    goto/32 :goto_2

    nop

    :goto_7
    const/4 v0, 0x1

    goto/32 :goto_5

    nop

    :goto_8
    if-nez v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_7

    nop
.end method

.method isUpdated()Z
    .locals 1

    goto/32 :goto_6

    nop

    :goto_0
    return v0

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_5

    nop

    :goto_2
    and-int/lit8 v0, v0, 0x2

    goto/32 :goto_1

    nop

    :goto_3
    const/4 v0, 0x0

    :goto_4
    goto/32 :goto_0

    nop

    :goto_5
    const/4 v0, 0x1

    goto/32 :goto_7

    nop

    :goto_6
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    goto/32 :goto_2

    nop

    :goto_7
    goto :goto_4

    :goto_8
    goto/32 :goto_3

    nop
.end method

.method needsUpdate()Z
    .locals 1

    goto/32 :goto_7

    nop

    :goto_0
    and-int/lit8 v0, v0, 0x2

    goto/32 :goto_6

    nop

    :goto_1
    const/4 v0, 0x0

    :goto_2
    goto/32 :goto_8

    nop

    :goto_3
    goto :goto_2

    :goto_4
    goto/32 :goto_1

    nop

    :goto_5
    const/4 v0, 0x1

    goto/32 :goto_3

    nop

    :goto_6
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_5

    nop

    :goto_7
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    goto/32 :goto_0

    nop

    :goto_8
    return v0
.end method

.method offsetPosition(IZ)V
    .locals 2

    goto/32 :goto_d

    nop

    :goto_0
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPreLayoutPosition:I

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_17

    nop

    :goto_2
    add-int/2addr v0, p1

    goto/32 :goto_f

    nop

    :goto_3
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_6

    nop

    :goto_4
    return-void

    :goto_5
    const/4 v1, 0x1

    goto/32 :goto_7

    nop

    :goto_6
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    goto/32 :goto_16

    nop

    :goto_7
    iput-boolean v1, v0, Lcom/android/internal/widget/RecyclerView$LayoutParams;->mInsetsDirty:Z

    :goto_8
    goto/32 :goto_4

    nop

    :goto_9
    if-eq v0, v1, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_1a

    nop

    :goto_a
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPosition:I

    goto/32 :goto_18

    nop

    :goto_b
    iput v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mOldPosition:I

    :goto_c
    goto/32 :goto_1b

    nop

    :goto_d
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mOldPosition:I

    goto/32 :goto_11

    nop

    :goto_e
    add-int/2addr v0, p1

    goto/32 :goto_12

    nop

    :goto_f
    iput v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPreLayoutPosition:I

    :goto_10
    goto/32 :goto_14

    nop

    :goto_11
    const/4 v1, -0x1

    goto/32 :goto_9

    nop

    :goto_12
    iput v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPosition:I

    goto/32 :goto_3

    nop

    :goto_13
    if-eq v0, v1, :cond_1

    goto/32 :goto_19

    :cond_1
    goto/32 :goto_a

    nop

    :goto_14
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPosition:I

    goto/32 :goto_e

    nop

    :goto_15
    if-nez p2, :cond_2

    goto/32 :goto_10

    :cond_2
    goto/32 :goto_0

    nop

    :goto_16
    if-nez v0, :cond_3

    goto/32 :goto_8

    :cond_3
    goto/32 :goto_1

    nop

    :goto_17
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    goto/32 :goto_1c

    nop

    :goto_18
    iput v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPreLayoutPosition:I

    :goto_19
    goto/32 :goto_15

    nop

    :goto_1a
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPosition:I

    goto/32 :goto_b

    nop

    :goto_1b
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPreLayoutPosition:I

    goto/32 :goto_13

    nop

    :goto_1c
    check-cast v0, Lcom/android/internal/widget/RecyclerView$LayoutParams;

    goto/32 :goto_5

    nop
.end method

.method resetInternal()V
    .locals 4

    goto/32 :goto_f

    nop

    :goto_0
    const/4 v1, -0x1

    goto/32 :goto_1

    nop

    :goto_1
    iput v1, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPosition:I

    goto/32 :goto_d

    nop

    :goto_2
    invoke-virtual {p0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->clearPayload()V

    goto/32 :goto_10

    nop

    :goto_3
    invoke-static {p0}, Lcom/android/internal/widget/RecyclerView;->clearNestedRecyclerViewIfNotNested(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_4

    nop

    :goto_4
    return-void

    :goto_5
    iput-wide v2, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mItemId:J

    goto/32 :goto_e

    nop

    :goto_6
    const/4 v2, 0x0

    goto/32 :goto_9

    nop

    :goto_7
    iput v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    goto/32 :goto_0

    nop

    :goto_8
    iput-object v2, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mShadowingHolder:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_2

    nop

    :goto_9
    iput-object v2, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mShadowedHolder:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_8

    nop

    :goto_a
    iput v1, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPendingAccessibilityState:I

    goto/32 :goto_3

    nop

    :goto_b
    iput v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mIsRecyclableCount:I

    goto/32 :goto_6

    nop

    :goto_c
    const-wide/16 v2, -0x1

    goto/32 :goto_5

    nop

    :goto_d
    iput v1, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mOldPosition:I

    goto/32 :goto_c

    nop

    :goto_e
    iput v1, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPreLayoutPosition:I

    goto/32 :goto_b

    nop

    :goto_f
    const/4 v0, 0x0

    goto/32 :goto_7

    nop

    :goto_10
    iput v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mWasImportantForAccessibilityBeforeHidden:I

    goto/32 :goto_a

    nop
.end method

.method saveOldPosition()V
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    iput v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mOldPosition:I

    :goto_1
    goto/32 :goto_3

    nop

    :goto_2
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPosition:I

    goto/32 :goto_0

    nop

    :goto_3
    return-void

    :goto_4
    if-eq v0, v1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_2

    nop

    :goto_5
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mOldPosition:I

    goto/32 :goto_6

    nop

    :goto_6
    const/4 v1, -0x1

    goto/32 :goto_4

    nop
.end method

.method setFlags(II)V
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    return-void

    :goto_1
    and-int v1, p1, p2

    goto/32 :goto_5

    nop

    :goto_2
    iput v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    goto/32 :goto_0

    nop

    :goto_3
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    goto/32 :goto_4

    nop

    :goto_4
    not-int v1, p2

    goto/32 :goto_6

    nop

    :goto_5
    or-int/2addr v0, v1

    goto/32 :goto_2

    nop

    :goto_6
    and-int/2addr v0, v1

    goto/32 :goto_1

    nop
.end method

.method public final setIsRecyclable(Z)V
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mIsRecyclableCount:I

    if-eqz p1, :cond_0

    sub-int/2addr v1, v0

    goto :goto_0

    :cond_0
    add-int/2addr v1, v0

    :goto_0
    iput v1, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mIsRecyclableCount:I

    if-gez v1, :cond_1

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mIsRecyclableCount:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "isRecyclable decremented below 0: unmatched pair of setIsRecyable() calls for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "View"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    if-nez p1, :cond_2

    if-ne v1, v0, :cond_2

    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    goto :goto_1

    :cond_2
    if-eqz p1, :cond_3

    if-nez v1, :cond_3

    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    :cond_3
    :goto_1
    return-void
.end method

.method setScrapContainer(Lcom/android/internal/widget/RecyclerView$Recycler;Z)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iput-object p1, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mScrapContainer:Lcom/android/internal/widget/RecyclerView$Recycler;

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    iput-boolean p2, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mInChangeScrap:Z

    goto/32 :goto_1

    nop
.end method

.method shouldIgnore()Z
    .locals 1

    goto/32 :goto_7

    nop

    :goto_0
    goto :goto_6

    :goto_1
    goto/32 :goto_5

    nop

    :goto_2
    const/4 v0, 0x1

    goto/32 :goto_0

    nop

    :goto_3
    return v0

    :goto_4
    and-int/lit16 v0, v0, 0x80

    goto/32 :goto_8

    nop

    :goto_5
    const/4 v0, 0x0

    :goto_6
    goto/32 :goto_3

    nop

    :goto_7
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    goto/32 :goto_4

    nop

    :goto_8
    if-nez v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_2

    nop
.end method

.method stopIgnoring()V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    and-int/lit16 v0, v0, -0x81

    goto/32 :goto_1

    nop

    :goto_1
    iput v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    goto/32 :goto_3

    nop

    :goto_2
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    goto/32 :goto_0

    nop

    :goto_3
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ViewHolder{"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " position="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPosition:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mItemId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", oldPos="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mOldPosition:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", pLpos:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPreLayoutPosition:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isScrap()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, " scrap "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mInChangeScrap:Z

    if-eqz v2, :cond_0

    const-string v2, "[changeScrap]"

    goto :goto_0

    :cond_0
    const-string v2, "[attachedScrap]"

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {p0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isInvalid()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, " invalid"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {p0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isBound()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, " unbound"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    invoke-virtual {p0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->needsUpdate()Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, " update"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    invoke-virtual {p0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isRemoved()Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, " removed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    invoke-virtual {p0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->shouldIgnore()Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, " ignored"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    invoke-virtual {p0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isTmpDetached()Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, " tmpDetached"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    invoke-virtual {p0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isRecyclable()Z

    move-result v1

    if-nez v1, :cond_8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " not recyclable("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mIsRecyclableCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    invoke-virtual {p0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isAdapterPositionUnknown()Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, " undefined adapter position"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_9
    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_a

    const-string v1, " no parent"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_a
    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method unScrap()V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {v0, p0}, Lcom/android/internal/widget/RecyclerView$Recycler;->unscrapView(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mScrapContainer:Lcom/android/internal/widget/RecyclerView$Recycler;

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method wasReturnedFromScrap()Z
    .locals 1

    goto/32 :goto_8

    nop

    :goto_0
    return v0

    :goto_1
    const/4 v0, 0x1

    goto/32 :goto_5

    nop

    :goto_2
    const/4 v0, 0x0

    :goto_3
    goto/32 :goto_0

    nop

    :goto_4
    and-int/lit8 v0, v0, 0x20

    goto/32 :goto_7

    nop

    :goto_5
    goto :goto_3

    :goto_6
    goto/32 :goto_2

    nop

    :goto_7
    if-nez v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_1

    nop

    :goto_8
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mFlags:I

    goto/32 :goto_4

    nop
.end method
