.class public Lcom/android/internal/widget/helper/ItemTouchHelper;
.super Lcom/android/internal/widget/RecyclerView$ItemDecoration;

# interfaces
.implements Lcom/android/internal/widget/RecyclerView$OnChildAttachStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/widget/helper/ItemTouchHelper$RecoverAnimation;,
        Lcom/android/internal/widget/helper/ItemTouchHelper$ItemTouchHelperGestureListener;,
        Lcom/android/internal/widget/helper/ItemTouchHelper$SimpleCallback;,
        Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;,
        Lcom/android/internal/widget/helper/ItemTouchHelper$ViewDropHandler;
    }
.end annotation


# static fields
.field static final ACTION_MODE_DRAG_MASK:I = 0xff0000

.field private static final ACTION_MODE_IDLE_MASK:I = 0xff

.field static final ACTION_MODE_SWIPE_MASK:I = 0xff00

.field public static final ACTION_STATE_DRAG:I = 0x2

.field public static final ACTION_STATE_IDLE:I = 0x0

.field public static final ACTION_STATE_SWIPE:I = 0x1

.field static final ACTIVE_POINTER_ID_NONE:I = -0x1

.field public static final ANIMATION_TYPE_DRAG:I = 0x8

.field public static final ANIMATION_TYPE_SWIPE_CANCEL:I = 0x4

.field public static final ANIMATION_TYPE_SWIPE_SUCCESS:I = 0x2

.field static final DEBUG:Z = false

.field static final DIRECTION_FLAG_COUNT:I = 0x8

.field public static final DOWN:I = 0x2

.field public static final END:I = 0x20

.field public static final LEFT:I = 0x4

.field private static final PIXELS_PER_SECOND:I = 0x3e8

.field public static final RIGHT:I = 0x8

.field public static final START:I = 0x10

.field static final TAG:Ljava/lang/String; = "ItemTouchHelper"

.field public static final UP:I = 0x1


# instance fields
.field mActionState:I

.field mActivePointerId:I

.field mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

.field private mChildDrawingOrderCallback:Lcom/android/internal/widget/RecyclerView$ChildDrawingOrderCallback;

.field private mDistances:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mDragScrollStartTimeInMs:J

.field mDx:F

.field mDy:F

.field mGestureDetector:Landroid/view/GestureDetector;

.field mInitialTouchX:F

.field mInitialTouchY:F

.field mMaxSwipeVelocity:F

.field private final mOnItemTouchListener:Lcom/android/internal/widget/RecyclerView$OnItemTouchListener;

.field mOverdrawChild:Landroid/view/View;

.field mOverdrawChildPosition:I

.field final mPendingCleanup:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field mRecoverAnimations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/internal/widget/helper/ItemTouchHelper$RecoverAnimation;",
            ">;"
        }
    .end annotation
.end field

.field mRecyclerView:Lcom/android/internal/widget/RecyclerView;

.field final mScrollRunnable:Ljava/lang/Runnable;

.field mSelected:Lcom/android/internal/widget/RecyclerView$ViewHolder;

.field mSelectedFlags:I

.field mSelectedStartX:F

.field mSelectedStartY:F

.field private mSlop:I

.field private mSwapTargets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/internal/widget/RecyclerView$ViewHolder;",
            ">;"
        }
    .end annotation
.end field

.field mSwipeEscapeVelocity:F

.field private final mTmpPosition:[F

.field private mTmpRect:Landroid/graphics/Rect;

.field mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method public constructor <init>(Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;)V
    .locals 3

    invoke-direct {p0}, Lcom/android/internal/widget/RecyclerView$ItemDecoration;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mPendingCleanup:Ljava/util/List;

    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mTmpPosition:[F

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelected:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mActivePointerId:I

    const/4 v2, 0x0

    iput v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mActionState:I

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecoverAnimations:Ljava/util/List;

    new-instance v2, Lcom/android/internal/widget/helper/ItemTouchHelper$1;

    invoke-direct {v2, p0}, Lcom/android/internal/widget/helper/ItemTouchHelper$1;-><init>(Lcom/android/internal/widget/helper/ItemTouchHelper;)V

    iput-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mScrollRunnable:Ljava/lang/Runnable;

    iput-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mChildDrawingOrderCallback:Lcom/android/internal/widget/RecyclerView$ChildDrawingOrderCallback;

    iput-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mOverdrawChild:Landroid/view/View;

    iput v1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mOverdrawChildPosition:I

    new-instance v0, Lcom/android/internal/widget/helper/ItemTouchHelper$2;

    invoke-direct {v0, p0}, Lcom/android/internal/widget/helper/ItemTouchHelper$2;-><init>(Lcom/android/internal/widget/helper/ItemTouchHelper;)V

    iput-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mOnItemTouchListener:Lcom/android/internal/widget/RecyclerView$OnItemTouchListener;

    iput-object p1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    return-void
.end method

.method private addChildDrawingOrderCallback()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mChildDrawingOrderCallback:Lcom/android/internal/widget/RecyclerView$ChildDrawingOrderCallback;

    if-nez v0, :cond_1

    new-instance v0, Lcom/android/internal/widget/helper/ItemTouchHelper$5;

    invoke-direct {v0, p0}, Lcom/android/internal/widget/helper/ItemTouchHelper$5;-><init>(Lcom/android/internal/widget/helper/ItemTouchHelper;)V

    iput-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mChildDrawingOrderCallback:Lcom/android/internal/widget/RecyclerView$ChildDrawingOrderCallback;

    :cond_1
    iget-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    iget-object v1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mChildDrawingOrderCallback:Lcom/android/internal/widget/RecyclerView$ChildDrawingOrderCallback;

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/RecyclerView;->setChildDrawingOrderCallback(Lcom/android/internal/widget/RecyclerView$ChildDrawingOrderCallback;)V

    return-void
.end method

.method private checkHorizontalSwipe(Lcom/android/internal/widget/RecyclerView$ViewHolder;I)I
    .locals 8

    and-int/lit8 v0, p2, 0xc

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDx:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    const/16 v2, 0x8

    const/4 v3, 0x4

    if-lez v0, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v3

    :goto_0
    iget-object v4, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v4, :cond_2

    iget v5, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mActivePointerId:I

    const/4 v6, -0x1

    if-le v5, v6, :cond_2

    const/16 v5, 0x3e8

    iget-object v6, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    iget v7, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mMaxSwipeVelocity:F

    invoke-virtual {v6, v7}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->getSwipeVelocityThreshold(F)F

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    iget-object v4, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    iget v5, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mActivePointerId:I

    invoke-virtual {v4, v5}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v4

    iget-object v5, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    iget v6, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mActivePointerId:I

    invoke-virtual {v5, v6}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v5

    cmpl-float v1, v4, v1

    if-lez v1, :cond_1

    goto :goto_1

    :cond_1
    move v2, v3

    :goto_1
    move v1, v2

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v2

    and-int v3, v1, p2

    if-eqz v3, :cond_2

    if-ne v0, v1, :cond_2

    iget-object v3, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    iget v6, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSwipeEscapeVelocity:F

    invoke-virtual {v3, v6}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->getSwipeEscapeVelocity(F)F

    move-result v3

    cmpl-float v3, v2, v3

    if-ltz v3, :cond_2

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v3, v2, v3

    if-lez v3, :cond_2

    return v1

    :cond_2
    iget-object v1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    invoke-virtual {v1}, Lcom/android/internal/widget/RecyclerView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    invoke-virtual {v2, p1}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->getSwipeThreshold(Lcom/android/internal/widget/RecyclerView$ViewHolder;)F

    move-result v2

    mul-float/2addr v1, v2

    and-int v2, p2, v0

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDx:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, v1

    if-lez v2, :cond_3

    return v0

    :cond_3
    const/4 v0, 0x0

    return v0
.end method

.method private checkVerticalSwipe(Lcom/android/internal/widget/RecyclerView$ViewHolder;I)I
    .locals 8

    and-int/lit8 v0, p2, 0x3

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDy:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-lez v0, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v3

    :goto_0
    iget-object v4, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v4, :cond_2

    iget v5, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mActivePointerId:I

    const/4 v6, -0x1

    if-le v5, v6, :cond_2

    const/16 v5, 0x3e8

    iget-object v6, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    iget v7, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mMaxSwipeVelocity:F

    invoke-virtual {v6, v7}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->getSwipeVelocityThreshold(F)F

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    iget-object v4, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    iget v5, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mActivePointerId:I

    invoke-virtual {v4, v5}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v4

    iget-object v5, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    iget v6, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mActivePointerId:I

    invoke-virtual {v5, v6}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v5

    cmpl-float v1, v5, v1

    if-lez v1, :cond_1

    goto :goto_1

    :cond_1
    move v2, v3

    :goto_1
    move v1, v2

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v2

    and-int v3, v1, p2

    if-eqz v3, :cond_2

    if-ne v1, v0, :cond_2

    iget-object v3, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    iget v6, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSwipeEscapeVelocity:F

    invoke-virtual {v3, v6}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->getSwipeEscapeVelocity(F)F

    move-result v3

    cmpl-float v3, v2, v3

    if-ltz v3, :cond_2

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v3, v2, v3

    if-lez v3, :cond_2

    return v1

    :cond_2
    iget-object v1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    invoke-virtual {v1}, Lcom/android/internal/widget/RecyclerView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    invoke-virtual {v2, p1}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->getSwipeThreshold(Lcom/android/internal/widget/RecyclerView$ViewHolder;)F

    move-result v2

    mul-float/2addr v1, v2

    and-int v2, p2, v0

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDy:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, v1

    if-lez v2, :cond_3

    return v0

    :cond_3
    const/4 v0, 0x0

    return v0
.end method

.method private destroyCallbacks()V
    .locals 6

    iget-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    invoke-virtual {v0, p0}, Lcom/android/internal/widget/RecyclerView;->removeItemDecoration(Lcom/android/internal/widget/RecyclerView$ItemDecoration;)V

    iget-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    iget-object v1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mOnItemTouchListener:Lcom/android/internal/widget/RecyclerView$OnItemTouchListener;

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/RecyclerView;->removeOnItemTouchListener(Lcom/android/internal/widget/RecyclerView$OnItemTouchListener;)V

    iget-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    invoke-virtual {v0, p0}, Lcom/android/internal/widget/RecyclerView;->removeOnChildAttachStateChangeListener(Lcom/android/internal/widget/RecyclerView$OnChildAttachStateChangeListener;)V

    iget-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecoverAnimations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    :goto_0
    if-ltz v1, :cond_0

    iget-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecoverAnimations:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/internal/widget/helper/ItemTouchHelper$RecoverAnimation;

    iget-object v3, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    iget-object v4, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    iget-object v5, v2, Lcom/android/internal/widget/helper/ItemTouchHelper$RecoverAnimation;->mViewHolder:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    invoke-virtual {v3, v4, v5}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->clearView(Lcom/android/internal/widget/RecyclerView;Lcom/android/internal/widget/RecyclerView$ViewHolder;)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecoverAnimations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mOverdrawChild:Landroid/view/View;

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mOverdrawChildPosition:I

    invoke-direct {p0}, Lcom/android/internal/widget/helper/ItemTouchHelper;->releaseVelocityTracker()V

    return-void
.end method

.method private findSwapTargets(Lcom/android/internal/widget/RecyclerView$ViewHolder;)Ljava/util/List;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/internal/widget/RecyclerView$ViewHolder;",
            ")",
            "Ljava/util/List<",
            "Lcom/android/internal/widget/RecyclerView$ViewHolder;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget-object v2, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSwapTargets:Ljava/util/List;

    if-nez v2, :cond_0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSwapTargets:Ljava/util/List;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDistances:Ljava/util/List;

    goto :goto_0

    :cond_0
    invoke-interface {v2}, Ljava/util/List;->clear()V

    iget-object v2, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDistances:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    :goto_0
    iget-object v2, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    invoke-virtual {v2}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->getBoundingBoxMargin()I

    move-result v2

    iget v3, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelectedStartX:F

    iget v4, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDx:F

    add-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    sub-int/2addr v3, v2

    iget v4, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelectedStartY:F

    iget v5, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDy:F

    add-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    sub-int/2addr v4, v2

    iget-object v5, v1, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v5, v3

    mul-int/lit8 v6, v2, 0x2

    add-int/2addr v5, v6

    iget-object v6, v1, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    add-int/2addr v6, v4

    mul-int/lit8 v7, v2, 0x2

    add-int/2addr v6, v7

    add-int v7, v3, v5

    div-int/lit8 v7, v7, 0x2

    add-int v8, v4, v6

    div-int/lit8 v8, v8, 0x2

    iget-object v9, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    invoke-virtual {v9}, Lcom/android/internal/widget/RecyclerView;->getLayoutManager()Lcom/android/internal/widget/RecyclerView$LayoutManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/internal/widget/RecyclerView$LayoutManager;->getChildCount()I

    move-result v10

    const/4 v11, 0x0

    :goto_1
    if-ge v11, v10, :cond_8

    invoke-virtual {v9, v11}, Lcom/android/internal/widget/RecyclerView$LayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v12

    iget-object v13, v1, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    if-ne v12, v13, :cond_1

    move/from16 v19, v2

    move/from16 v16, v3

    goto/16 :goto_3

    :cond_1
    invoke-virtual {v12}, Landroid/view/View;->getBottom()I

    move-result v13

    if-lt v13, v4, :cond_7

    invoke-virtual {v12}, Landroid/view/View;->getTop()I

    move-result v13

    if-gt v13, v6, :cond_7

    invoke-virtual {v12}, Landroid/view/View;->getRight()I

    move-result v13

    if-lt v13, v3, :cond_6

    invoke-virtual {v12}, Landroid/view/View;->getLeft()I

    move-result v13

    if-le v13, v5, :cond_2

    move/from16 v19, v2

    move/from16 v16, v3

    goto/16 :goto_3

    :cond_2
    iget-object v13, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    invoke-virtual {v13, v12}, Lcom/android/internal/widget/RecyclerView;->getChildViewHolder(Landroid/view/View;)Lcom/android/internal/widget/RecyclerView$ViewHolder;

    move-result-object v13

    iget-object v14, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    iget-object v15, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    iget-object v1, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelected:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    invoke-virtual {v14, v15, v1, v13}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->canDropOver(Lcom/android/internal/widget/RecyclerView;Lcom/android/internal/widget/RecyclerView$ViewHolder;Lcom/android/internal/widget/RecyclerView$ViewHolder;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v12}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {v12}, Landroid/view/View;->getRight()I

    move-result v14

    add-int/2addr v1, v14

    div-int/lit8 v1, v1, 0x2

    sub-int v1, v7, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    invoke-virtual {v12}, Landroid/view/View;->getTop()I

    move-result v14

    invoke-virtual {v12}, Landroid/view/View;->getBottom()I

    move-result v15

    add-int/2addr v14, v15

    div-int/lit8 v14, v14, 0x2

    sub-int v14, v8, v14

    invoke-static {v14}, Ljava/lang/Math;->abs(I)I

    move-result v14

    mul-int v15, v1, v1

    mul-int v16, v14, v14

    add-int v15, v15, v16

    const/16 v16, 0x0

    move/from16 v17, v1

    iget-object v1, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSwapTargets:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/16 v18, 0x0

    move/from16 v19, v2

    move/from16 v2, v16

    move/from16 v16, v3

    move/from16 v3, v18

    :goto_2
    if-ge v3, v1, :cond_3

    move/from16 v18, v1

    iget-object v1, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDistances:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-le v15, v1, :cond_4

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v3, v3, 0x1

    move/from16 v1, v18

    goto :goto_2

    :cond_3
    move/from16 v18, v1

    :cond_4
    iget-object v1, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSwapTargets:Ljava/util/List;

    invoke-interface {v1, v2, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object v1, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDistances:Ljava/util/List;

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_3

    :cond_5
    move/from16 v19, v2

    move/from16 v16, v3

    goto :goto_3

    :cond_6
    move/from16 v19, v2

    move/from16 v16, v3

    goto :goto_3

    :cond_7
    move/from16 v19, v2

    move/from16 v16, v3

    :goto_3
    add-int/lit8 v11, v11, 0x1

    move-object/from16 v1, p1

    move/from16 v3, v16

    move/from16 v2, v19

    goto/16 :goto_1

    :cond_8
    iget-object v1, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSwapTargets:Ljava/util/List;

    return-object v1
.end method

.method private findSwipedView(Landroid/view/MotionEvent;)Lcom/android/internal/widget/RecyclerView$ViewHolder;
    .locals 9

    iget-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView;->getLayoutManager()Lcom/android/internal/widget/RecyclerView$LayoutManager;

    move-result-object v0

    iget v1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mActivePointerId:I

    const/4 v2, 0x0

    const/4 v3, -0x1

    if-ne v1, v3, :cond_0

    return-object v2

    :cond_0
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    iget v4, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mInitialTouchX:F

    sub-float/2addr v3, v4

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    iget v5, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mInitialTouchY:F

    sub-float/2addr v4, v5

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v6

    iget v7, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSlop:I

    int-to-float v8, v7

    cmpg-float v8, v5, v8

    if-gez v8, :cond_1

    int-to-float v7, v7

    cmpg-float v7, v6, v7

    if-gez v7, :cond_1

    return-object v2

    :cond_1
    cmpl-float v7, v5, v6

    if-lez v7, :cond_2

    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView$LayoutManager;->canScrollHorizontally()Z

    move-result v7

    if-eqz v7, :cond_2

    return-object v2

    :cond_2
    cmpl-float v7, v6, v5

    if-lez v7, :cond_3

    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView$LayoutManager;->canScrollVertically()Z

    move-result v7

    if-eqz v7, :cond_3

    return-object v2

    :cond_3
    invoke-virtual {p0, p1}, Lcom/android/internal/widget/helper/ItemTouchHelper;->findChildView(Landroid/view/MotionEvent;)Landroid/view/View;

    move-result-object v7

    if-nez v7, :cond_4

    return-object v2

    :cond_4
    iget-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    invoke-virtual {v2, v7}, Lcom/android/internal/widget/RecyclerView;->getChildViewHolder(Landroid/view/View;)Lcom/android/internal/widget/RecyclerView$ViewHolder;

    move-result-object v2

    return-object v2
.end method

.method private getSelectedDxDy([F)V
    .locals 3

    iget v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelectedFlags:I

    and-int/lit8 v0, v0, 0xc

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelectedStartX:F

    iget v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDx:F

    add-float/2addr v0, v2

    iget-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelected:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    iget-object v2, v2, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v0, v2

    aput v0, p1, v1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelected:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v0

    aput v0, p1, v1

    :goto_0
    iget v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelectedFlags:I

    and-int/lit8 v0, v0, 0x3

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelectedStartY:F

    iget v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDy:F

    add-float/2addr v0, v2

    iget-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelected:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    iget-object v2, v2, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v0, v2

    aput v0, p1, v1

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelected:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v0

    aput v0, p1, v1

    :goto_1
    return-void
.end method

.method private static hitTest(Landroid/view/View;FFFF)Z
    .locals 1

    cmpl-float v0, p1, p3

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, p3

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    cmpl-float v0, p2, p4

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, p4

    cmpg-float v0, p2, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private initGestureDetector()V
    .locals 3

    iget-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mGestureDetector:Landroid/view/GestureDetector;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    invoke-virtual {v1}, Lcom/android/internal/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/android/internal/widget/helper/ItemTouchHelper$ItemTouchHelperGestureListener;

    invoke-direct {v2, p0}, Lcom/android/internal/widget/helper/ItemTouchHelper$ItemTouchHelperGestureListener;-><init>(Lcom/android/internal/widget/helper/ItemTouchHelper;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mGestureDetector:Landroid/view/GestureDetector;

    return-void
.end method

.method private releaseVelocityTracker()V
    .locals 1

    iget-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_0
    return-void
.end method

.method private setupCallbacks()V
    .locals 3

    iget-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSlop:I

    iget-object v1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    invoke-virtual {v1, p0}, Lcom/android/internal/widget/RecyclerView;->addItemDecoration(Lcom/android/internal/widget/RecyclerView$ItemDecoration;)V

    iget-object v1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    iget-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mOnItemTouchListener:Lcom/android/internal/widget/RecyclerView$OnItemTouchListener;

    invoke-virtual {v1, v2}, Lcom/android/internal/widget/RecyclerView;->addOnItemTouchListener(Lcom/android/internal/widget/RecyclerView$OnItemTouchListener;)V

    iget-object v1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    invoke-virtual {v1, p0}, Lcom/android/internal/widget/RecyclerView;->addOnChildAttachStateChangeListener(Lcom/android/internal/widget/RecyclerView$OnChildAttachStateChangeListener;)V

    invoke-direct {p0}, Lcom/android/internal/widget/helper/ItemTouchHelper;->initGestureDetector()V

    return-void
.end method

.method private swipeIfNecessary(Lcom/android/internal/widget/RecyclerView$ViewHolder;)I
    .locals 7

    iget v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mActionState:I

    const/4 v1, 0x0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    iget-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    invoke-virtual {v0, v2, p1}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->getMovementFlags(Lcom/android/internal/widget/RecyclerView;Lcom/android/internal/widget/RecyclerView$ViewHolder;)I

    move-result v0

    iget-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    iget-object v3, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    invoke-virtual {v3}, Lcom/android/internal/widget/RecyclerView;->getLayoutDirection()I

    move-result v3

    invoke-virtual {v2, v0, v3}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->convertToAbsoluteDirection(II)I

    move-result v2

    const v3, 0xff00

    and-int v4, v2, v3

    shr-int/lit8 v4, v4, 0x8

    if-nez v4, :cond_1

    return v1

    :cond_1
    and-int/2addr v3, v0

    shr-int/lit8 v3, v3, 0x8

    iget v5, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDx:F

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v6, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDy:F

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpl-float v5, v5, v6

    if-lez v5, :cond_4

    invoke-direct {p0, p1, v4}, Lcom/android/internal/widget/helper/ItemTouchHelper;->checkHorizontalSwipe(Lcom/android/internal/widget/RecyclerView$ViewHolder;I)I

    move-result v5

    move v6, v5

    if-lez v5, :cond_3

    and-int v1, v3, v6

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    invoke-virtual {v1}, Lcom/android/internal/widget/RecyclerView;->getLayoutDirection()I

    move-result v1

    invoke-static {v6, v1}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->convertToRelativeDirection(II)I

    move-result v1

    return v1

    :cond_2
    return v6

    :cond_3
    invoke-direct {p0, p1, v4}, Lcom/android/internal/widget/helper/ItemTouchHelper;->checkVerticalSwipe(Lcom/android/internal/widget/RecyclerView$ViewHolder;I)I

    move-result v5

    move v6, v5

    if-lez v5, :cond_7

    return v6

    :cond_4
    invoke-direct {p0, p1, v4}, Lcom/android/internal/widget/helper/ItemTouchHelper;->checkVerticalSwipe(Lcom/android/internal/widget/RecyclerView$ViewHolder;I)I

    move-result v5

    move v6, v5

    if-lez v5, :cond_5

    return v6

    :cond_5
    invoke-direct {p0, p1, v4}, Lcom/android/internal/widget/helper/ItemTouchHelper;->checkHorizontalSwipe(Lcom/android/internal/widget/RecyclerView$ViewHolder;I)I

    move-result v5

    move v6, v5

    if-lez v5, :cond_7

    and-int v1, v3, v6

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    invoke-virtual {v1}, Lcom/android/internal/widget/RecyclerView;->getLayoutDirection()I

    move-result v1

    invoke-static {v6, v1}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->convertToRelativeDirection(II)I

    move-result v1

    return v1

    :cond_6
    return v6

    :cond_7
    return v1
.end method


# virtual methods
.method public attachToRecyclerView(Lcom/android/internal/widget/RecyclerView;)V
    .locals 2

    iget-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/internal/widget/helper/ItemTouchHelper;->destroyCallbacks()V

    :cond_1
    iput-object p1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/android/internal/widget/RecyclerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x105017b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSwipeEscapeVelocity:F

    const v1, 0x105017a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mMaxSwipeVelocity:F

    invoke-direct {p0}, Lcom/android/internal/widget/helper/ItemTouchHelper;->setupCallbacks()V

    :cond_2
    return-void
.end method

.method checkSelectForSwipe(ILandroid/view/MotionEvent;I)Z
    .locals 16

    goto/32 :goto_21

    nop

    :goto_0
    const/4 v2, 0x2

    goto/32 :goto_45

    nop

    :goto_1
    return v3

    :goto_2
    goto/32 :goto_2b

    nop

    :goto_3
    if-ltz v14, :cond_0

    goto/32 :goto_26

    :cond_0
    goto/32 :goto_9

    nop

    :goto_4
    cmpg-float v14, v11, v15

    goto/32 :goto_47

    nop

    :goto_5
    if-ltz v15, :cond_1

    goto/32 :goto_1e

    :cond_1
    goto/32 :goto_53

    nop

    :goto_6
    move/from16 v4, p1

    :goto_7
    goto/32 :goto_11

    nop

    :goto_8
    iput v15, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDx:F

    goto/32 :goto_55

    nop

    :goto_9
    and-int/lit8 v14, v7, 0x4

    goto/32 :goto_52

    nop

    :goto_a
    cmpg-float v15, v12, v15

    goto/32 :goto_5

    nop

    :goto_b
    if-eqz v2, :cond_2

    goto/32 :goto_2

    :cond_2
    goto/32 :goto_1

    nop

    :goto_c
    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v13

    goto/32 :goto_43

    nop

    :goto_d
    return v3

    :goto_e
    goto/32 :goto_19

    nop

    :goto_f
    iput v3, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mActivePointerId:I

    goto/32 :goto_28

    nop

    :goto_10
    if-gtz v14, :cond_3

    goto/32 :goto_4d

    :cond_3
    goto/32 :goto_22

    nop

    :goto_11
    return v3

    :goto_12
    if-eq v2, v5, :cond_4

    goto/32 :goto_e

    :cond_4
    goto/32 :goto_d

    nop

    :goto_13
    iget-object v7, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_14

    nop

    :goto_14
    invoke-virtual {v6, v7, v2}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->getAbsoluteMovementFlags(Lcom/android/internal/widget/RecyclerView;Lcom/android/internal/widget/RecyclerView$ViewHolder;)I

    move-result v6

    goto/32 :goto_42

    nop

    :goto_15
    invoke-virtual/range {p2 .. p3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v9

    goto/32 :goto_24

    nop

    :goto_16
    sub-float v11, v9, v11

    goto/32 :goto_18

    nop

    :goto_17
    const/4 v15, 0x0

    goto/32 :goto_27

    nop

    :goto_18
    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v12

    goto/32 :goto_c

    nop

    :goto_19
    invoke-direct {v0, v1}, Lcom/android/internal/widget/helper/ItemTouchHelper;->findSwipedView(Landroid/view/MotionEvent;)Lcom/android/internal/widget/RecyclerView$ViewHolder;

    move-result-object v2

    goto/32 :goto_b

    nop

    :goto_1a
    iget-object v2, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    goto/32 :goto_41

    nop

    :goto_1b
    goto/16 :goto_7

    :goto_1c
    goto/32 :goto_4b

    nop

    :goto_1d
    return v3

    :goto_1e
    goto/32 :goto_51

    nop

    :goto_1f
    and-int/2addr v7, v6

    goto/32 :goto_57

    nop

    :goto_20
    if-ltz v14, :cond_5

    goto/32 :goto_1e

    :cond_5
    goto/32 :goto_1d

    nop

    :goto_21
    move-object/from16 v0, p0

    goto/32 :goto_31

    nop

    :goto_22
    and-int/lit8 v14, v7, 0x8

    goto/32 :goto_4e

    nop

    :goto_23
    if-eqz v2, :cond_6

    goto/32 :goto_1c

    :cond_6
    goto/32 :goto_1b

    nop

    :goto_24
    iget v10, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mInitialTouchX:F

    goto/32 :goto_2a

    nop

    :goto_25
    return v3

    :goto_26
    goto/32 :goto_30

    nop

    :goto_27
    if-gtz v14, :cond_7

    goto/32 :goto_3e

    :cond_7
    goto/32 :goto_44

    nop

    :goto_28
    invoke-virtual {v0, v2, v5}, Lcom/android/internal/widget/helper/ItemTouchHelper;->select(Lcom/android/internal/widget/RecyclerView$ViewHolder;I)V

    goto/32 :goto_4f

    nop

    :goto_29
    const/4 v3, 0x0

    goto/32 :goto_46

    nop

    :goto_2a
    sub-float v10, v8, v10

    goto/32 :goto_40

    nop

    :goto_2b
    iget-object v6, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    goto/32 :goto_13

    nop

    :goto_2c
    invoke-virtual/range {p2 .. p3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v8

    goto/32 :goto_15

    nop

    :goto_2d
    if-gtz v14, :cond_8

    goto/32 :goto_4d

    :cond_8
    goto/32 :goto_3b

    nop

    :goto_2e
    invoke-virtual {v2}, Lcom/android/internal/widget/RecyclerView;->getScrollState()I

    move-result v2

    goto/32 :goto_54

    nop

    :goto_2f
    and-int/lit8 v14, v7, 0x1

    goto/32 :goto_37

    nop

    :goto_30
    cmpl-float v14, v10, v15

    goto/32 :goto_10

    nop

    :goto_31
    move-object/from16 v1, p2

    goto/32 :goto_3f

    nop

    :goto_32
    if-ne v5, v2, :cond_9

    goto/32 :goto_7

    :cond_9
    goto/32 :goto_1a

    nop

    :goto_33
    return v3

    :goto_34
    goto/32 :goto_2c

    nop

    :goto_35
    return v3

    :goto_36
    goto/32 :goto_4a

    nop

    :goto_37
    if-eqz v14, :cond_a

    goto/32 :goto_36

    :cond_a
    goto/32 :goto_35

    nop

    :goto_38
    if-eq v4, v2, :cond_b

    goto/32 :goto_7

    :cond_b
    goto/32 :goto_49

    nop

    :goto_39
    if-eqz v14, :cond_c

    goto/32 :goto_4d

    :cond_c
    goto/32 :goto_4c

    nop

    :goto_3a
    int-to-float v15, v14

    goto/32 :goto_a

    nop

    :goto_3b
    and-int/lit8 v14, v7, 0x2

    goto/32 :goto_39

    nop

    :goto_3c
    iput v15, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDy:F

    goto/32 :goto_8

    nop

    :goto_3d
    return v3

    :goto_3e
    goto/32 :goto_4

    nop

    :goto_3f
    iget-object v2, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelected:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_29

    nop

    :goto_40
    iget v11, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mInitialTouchY:F

    goto/32 :goto_16

    nop

    :goto_41
    invoke-virtual {v2}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->isItemViewSwipeEnabled()Z

    move-result v2

    goto/32 :goto_23

    nop

    :goto_42
    const v7, 0xff00

    goto/32 :goto_1f

    nop

    :goto_43
    iget v14, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSlop:I

    goto/32 :goto_3a

    nop

    :goto_44
    cmpg-float v14, v10, v15

    goto/32 :goto_3

    nop

    :goto_45
    move/from16 v4, p1

    goto/32 :goto_38

    nop

    :goto_46
    if-eqz v2, :cond_d

    goto/32 :goto_50

    :cond_d
    goto/32 :goto_0

    nop

    :goto_47
    if-ltz v14, :cond_e

    goto/32 :goto_36

    :cond_e
    goto/32 :goto_2f

    nop

    :goto_48
    if-eqz v7, :cond_f

    goto/32 :goto_34

    :cond_f
    goto/32 :goto_33

    nop

    :goto_49
    iget v5, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mActionState:I

    goto/32 :goto_32

    nop

    :goto_4a
    cmpl-float v14, v11, v15

    goto/32 :goto_2d

    nop

    :goto_4b
    iget-object v2, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_2e

    nop

    :goto_4c
    return v3

    :goto_4d
    goto/32 :goto_3c

    nop

    :goto_4e
    if-eqz v14, :cond_10

    goto/32 :goto_4d

    :cond_10
    goto/32 :goto_3d

    nop

    :goto_4f
    return v5

    :goto_50
    goto/32 :goto_6

    nop

    :goto_51
    cmpl-float v14, v12, v13

    goto/32 :goto_17

    nop

    :goto_52
    if-eqz v14, :cond_11

    goto/32 :goto_26

    :cond_11
    goto/32 :goto_25

    nop

    :goto_53
    int-to-float v14, v14

    goto/32 :goto_56

    nop

    :goto_54
    const/4 v5, 0x1

    goto/32 :goto_12

    nop

    :goto_55
    invoke-virtual {v1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    goto/32 :goto_f

    nop

    :goto_56
    cmpg-float v14, v13, v14

    goto/32 :goto_20

    nop

    :goto_57
    shr-int/lit8 v7, v7, 0x8

    goto/32 :goto_48

    nop
.end method

.method endRecoverAnimation(Lcom/android/internal/widget/RecyclerView$ViewHolder;Z)I
    .locals 4

    goto/32 :goto_5

    nop

    :goto_0
    iget-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecoverAnimations:Ljava/util/List;

    goto/32 :goto_13

    nop

    :goto_1
    iget v3, v2, Lcom/android/internal/widget/helper/ItemTouchHelper$RecoverAnimation;->mAnimationType:I

    goto/32 :goto_19

    nop

    :goto_2
    iget-object v3, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecoverAnimations:Ljava/util/List;

    goto/32 :goto_6

    nop

    :goto_3
    if-gez v1, :cond_0

    goto/32 :goto_16

    :cond_0
    goto/32 :goto_0

    nop

    :goto_4
    check-cast v2, Lcom/android/internal/widget/helper/ItemTouchHelper$RecoverAnimation;

    goto/32 :goto_e

    nop

    :goto_5
    iget-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecoverAnimations:Ljava/util/List;

    goto/32 :goto_a

    nop

    :goto_6
    invoke-interface {v3, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto/32 :goto_1

    nop

    :goto_7
    if-eq v3, p1, :cond_1

    goto/32 :goto_1a

    :cond_1
    goto/32 :goto_8

    nop

    :goto_8
    iget-boolean v3, v2, Lcom/android/internal/widget/helper/ItemTouchHelper$RecoverAnimation;->mOverridden:Z

    goto/32 :goto_f

    nop

    :goto_9
    return v1

    :goto_a
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto/32 :goto_b

    nop

    :goto_b
    add-int/lit8 v1, v0, -0x1

    :goto_c
    goto/32 :goto_3

    nop

    :goto_d
    if-eqz v3, :cond_2

    goto/32 :goto_11

    :cond_2
    goto/32 :goto_10

    nop

    :goto_e
    iget-object v3, v2, Lcom/android/internal/widget/helper/ItemTouchHelper$RecoverAnimation;->mViewHolder:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_7

    nop

    :goto_f
    or-int/2addr v3, p2

    goto/32 :goto_18

    nop

    :goto_10
    invoke-virtual {v2}, Lcom/android/internal/widget/helper/ItemTouchHelper$RecoverAnimation;->cancel()V

    :goto_11
    goto/32 :goto_2

    nop

    :goto_12
    const/4 v1, 0x0

    goto/32 :goto_9

    nop

    :goto_13
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_4

    nop

    :goto_14
    add-int/lit8 v1, v1, -0x1

    goto/32 :goto_15

    nop

    :goto_15
    goto :goto_c

    :goto_16
    goto/32 :goto_12

    nop

    :goto_17
    iget-boolean v3, v2, Lcom/android/internal/widget/helper/ItemTouchHelper$RecoverAnimation;->mEnded:Z

    goto/32 :goto_d

    nop

    :goto_18
    iput-boolean v3, v2, Lcom/android/internal/widget/helper/ItemTouchHelper$RecoverAnimation;->mOverridden:Z

    goto/32 :goto_17

    nop

    :goto_19
    return v3

    :goto_1a
    goto/32 :goto_14

    nop
.end method

.method findAnimation(Landroid/view/MotionEvent;)Lcom/android/internal/widget/helper/ItemTouchHelper$RecoverAnimation;
    .locals 5

    goto/32 :goto_2

    nop

    :goto_0
    goto :goto_a

    :goto_1
    goto/32 :goto_16

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecoverAnimations:Ljava/util/List;

    goto/32 :goto_8

    nop

    :goto_3
    const/4 v1, 0x0

    goto/32 :goto_b

    nop

    :goto_4
    if-gez v2, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_f

    nop

    :goto_5
    return-object v3

    :goto_6
    goto/32 :goto_15

    nop

    :goto_7
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_e

    nop

    :goto_8
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    goto/32 :goto_3

    nop

    :goto_9
    add-int/lit8 v2, v2, -0x1

    :goto_a
    goto/32 :goto_4

    nop

    :goto_b
    if-nez v0, :cond_1

    goto/32 :goto_13

    :cond_1
    goto/32 :goto_12

    nop

    :goto_c
    if-eq v4, v0, :cond_2

    goto/32 :goto_6

    :cond_2
    goto/32 :goto_5

    nop

    :goto_d
    iget-object v4, v4, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_c

    nop

    :goto_e
    check-cast v3, Lcom/android/internal/widget/helper/ItemTouchHelper$RecoverAnimation;

    goto/32 :goto_14

    nop

    :goto_f
    iget-object v3, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecoverAnimations:Ljava/util/List;

    goto/32 :goto_7

    nop

    :goto_10
    invoke-virtual {p0, p1}, Lcom/android/internal/widget/helper/ItemTouchHelper;->findChildView(Landroid/view/MotionEvent;)Landroid/view/View;

    move-result-object v0

    goto/32 :goto_17

    nop

    :goto_11
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    goto/32 :goto_9

    nop

    :goto_12
    return-object v1

    :goto_13
    goto/32 :goto_10

    nop

    :goto_14
    iget-object v4, v3, Lcom/android/internal/widget/helper/ItemTouchHelper$RecoverAnimation;->mViewHolder:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_d

    nop

    :goto_15
    add-int/lit8 v2, v2, -0x1

    goto/32 :goto_0

    nop

    :goto_16
    return-object v1

    :goto_17
    iget-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecoverAnimations:Ljava/util/List;

    goto/32 :goto_11

    nop
.end method

.method findChildView(Landroid/view/MotionEvent;)Landroid/view/View;
    .locals 7

    goto/32 :goto_18

    nop

    :goto_0
    return-object v4

    :goto_1
    goto/32 :goto_1c

    nop

    :goto_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    goto/32 :goto_5

    nop

    :goto_3
    goto :goto_11

    :goto_4
    goto/32 :goto_15

    nop

    :goto_5
    iget-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelected:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_1f

    nop

    :goto_6
    iget-object v3, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecoverAnimations:Ljava/util/List;

    goto/32 :goto_d

    nop

    :goto_7
    iget-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecoverAnimations:Ljava/util/List;

    goto/32 :goto_1e

    nop

    :goto_8
    invoke-static {v4, v0, v1, v5, v6}, Lcom/android/internal/widget/helper/ItemTouchHelper;->hitTest(Landroid/view/View;FFFF)Z

    move-result v5

    goto/32 :goto_12

    nop

    :goto_9
    iget-object v2, v2, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_22

    nop

    :goto_a
    invoke-static {v2, v0, v1, v3, v4}, Lcom/android/internal/widget/helper/ItemTouchHelper;->hitTest(Landroid/view/View;FFFF)Z

    move-result v3

    goto/32 :goto_f

    nop

    :goto_b
    iget-object v4, v3, Lcom/android/internal/widget/helper/ItemTouchHelper$RecoverAnimation;->mViewHolder:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_16

    nop

    :goto_c
    iget v5, v3, Lcom/android/internal/widget/helper/ItemTouchHelper$RecoverAnimation;->mX:F

    goto/32 :goto_19

    nop

    :goto_d
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_e

    nop

    :goto_e
    check-cast v3, Lcom/android/internal/widget/helper/ItemTouchHelper$RecoverAnimation;

    goto/32 :goto_b

    nop

    :goto_f
    if-nez v3, :cond_0

    goto/32 :goto_1b

    :cond_0
    goto/32 :goto_1a

    nop

    :goto_10
    add-int/lit8 v2, v2, -0x1

    :goto_11
    goto/32 :goto_1d

    nop

    :goto_12
    if-nez v5, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_0

    nop

    :goto_13
    iget v4, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDx:F

    goto/32 :goto_24

    nop

    :goto_14
    invoke-virtual {v2, v0, v1}, Lcom/android/internal/widget/RecyclerView;->findChildViewUnder(FF)Landroid/view/View;

    move-result-object v2

    goto/32 :goto_23

    nop

    :goto_15
    iget-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_14

    nop

    :goto_16
    iget-object v4, v4, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_c

    nop

    :goto_17
    iget v5, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDy:F

    goto/32 :goto_21

    nop

    :goto_18
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    goto/32 :goto_2

    nop

    :goto_19
    iget v6, v3, Lcom/android/internal/widget/helper/ItemTouchHelper$RecoverAnimation;->mY:F

    goto/32 :goto_8

    nop

    :goto_1a
    return-object v2

    :goto_1b
    goto/32 :goto_7

    nop

    :goto_1c
    add-int/lit8 v2, v2, -0x1

    goto/32 :goto_3

    nop

    :goto_1d
    if-gez v2, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_6

    nop

    :goto_1e
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    goto/32 :goto_10

    nop

    :goto_1f
    if-nez v2, :cond_3

    goto/32 :goto_1b

    :cond_3
    goto/32 :goto_9

    nop

    :goto_20
    iget v4, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelectedStartY:F

    goto/32 :goto_17

    nop

    :goto_21
    add-float/2addr v4, v5

    goto/32 :goto_a

    nop

    :goto_22
    iget v3, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelectedStartX:F

    goto/32 :goto_13

    nop

    :goto_23
    return-object v2

    :goto_24
    add-float/2addr v3, v4

    goto/32 :goto_20

    nop
.end method

.method public getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Lcom/android/internal/widget/RecyclerView;Lcom/android/internal/widget/RecyclerView$State;)V
    .locals 0

    invoke-virtual {p1}, Landroid/graphics/Rect;->setEmpty()V

    return-void
.end method

.method hasRunningRecoverAnim()Z
    .locals 3

    goto/32 :goto_1

    nop

    :goto_0
    iget-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecoverAnimations:Ljava/util/List;

    goto/32 :goto_5

    nop

    :goto_1
    iget-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecoverAnimations:Ljava/util/List;

    goto/32 :goto_9

    nop

    :goto_2
    goto :goto_10

    :goto_3
    goto/32 :goto_a

    nop

    :goto_4
    check-cast v2, Lcom/android/internal/widget/helper/ItemTouchHelper$RecoverAnimation;

    goto/32 :goto_c

    nop

    :goto_5
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_4

    nop

    :goto_6
    return v2

    :goto_7
    goto/32 :goto_8

    nop

    :goto_8
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_2

    nop

    :goto_9
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto/32 :goto_f

    nop

    :goto_a
    const/4 v1, 0x0

    goto/32 :goto_e

    nop

    :goto_b
    if-eqz v2, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_11

    nop

    :goto_c
    iget-boolean v2, v2, Lcom/android/internal/widget/helper/ItemTouchHelper$RecoverAnimation;->mEnded:Z

    goto/32 :goto_b

    nop

    :goto_d
    if-lt v1, v0, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_0

    nop

    :goto_e
    return v1

    :goto_f
    const/4 v1, 0x0

    :goto_10
    goto/32 :goto_d

    nop

    :goto_11
    const/4 v2, 0x1

    goto/32 :goto_6

    nop
.end method

.method moveIfNecessary(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V
    .locals 17

    goto/32 :goto_14

    nop

    :goto_0
    mul-float/2addr v2, v10

    goto/32 :goto_3d

    nop

    :goto_1
    float-to-int v12, v1

    goto/32 :goto_3b

    nop

    :goto_2
    invoke-interface {v1}, Ljava/util/List;->clear()V

    goto/32 :goto_37

    nop

    :goto_3
    invoke-virtual {v14}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v15

    goto/32 :goto_44

    nop

    :goto_4
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    goto/32 :goto_2e

    nop

    :goto_5
    float-to-int v11, v1

    goto/32 :goto_7

    nop

    :goto_6
    sub-int v1, v11, v1

    goto/32 :goto_4

    nop

    :goto_7
    iget v1, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelectedStartY:F

    goto/32 :goto_4c

    nop

    :goto_8
    cmpg-float v1, v1, v2

    goto/32 :goto_39

    nop

    :goto_9
    iget-object v2, v9, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_c

    nop

    :goto_a
    iget-object v1, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    goto/32 :goto_43

    nop

    :goto_b
    move-object v5, v14

    goto/32 :goto_1c

    nop

    :goto_c
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    goto/32 :goto_1f

    nop

    :goto_d
    iget-object v1, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_3c

    nop

    :goto_e
    invoke-virtual/range {v1 .. v8}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->onMoved(Lcom/android/internal/widget/RecyclerView;Lcom/android/internal/widget/RecyclerView$ViewHolder;ILcom/android/internal/widget/RecyclerView$ViewHolder;III)V

    :goto_f
    goto/32 :goto_1d

    nop

    :goto_10
    iget-object v1, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSwapTargets:Ljava/util/List;

    goto/32 :goto_1b

    nop

    :goto_11
    add-float/2addr v1, v2

    goto/32 :goto_1

    nop

    :goto_12
    iget v1, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mActionState:I

    goto/32 :goto_3a

    nop

    :goto_13
    iget-object v1, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDistances:Ljava/util/List;

    goto/32 :goto_2

    nop

    :goto_14
    move-object/from16 v0, p0

    goto/32 :goto_34

    nop

    :goto_15
    add-float/2addr v1, v2

    goto/32 :goto_5

    nop

    :goto_16
    return-void

    :goto_17
    goto/32 :goto_a

    nop

    :goto_18
    sub-int v1, v12, v1

    goto/32 :goto_4a

    nop

    :goto_19
    mul-float/2addr v2, v10

    goto/32 :goto_8

    nop

    :goto_1a
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    goto/32 :goto_18

    nop

    :goto_1b
    invoke-interface {v1}, Ljava/util/List;->clear()V

    goto/32 :goto_13

    nop

    :goto_1c
    move v6, v15

    goto/32 :goto_24

    nop

    :goto_1d
    return-void

    :goto_1e
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    goto/32 :goto_2b

    nop

    :goto_1f
    int-to-float v2, v2

    goto/32 :goto_19

    nop

    :goto_20
    iget v1, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelectedStartX:F

    goto/32 :goto_3e

    nop

    :goto_21
    if-nez v1, :cond_0

    goto/32 :goto_26

    :cond_0
    goto/32 :goto_25

    nop

    :goto_22
    iget-object v2, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_30

    nop

    :goto_23
    if-ne v1, v2, :cond_1

    goto/32 :goto_40

    :cond_1
    goto/32 :goto_3f

    nop

    :goto_24
    move v7, v11

    goto/32 :goto_36

    nop

    :goto_25
    return-void

    :goto_26
    goto/32 :goto_12

    nop

    :goto_27
    iget-object v1, v9, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_47

    nop

    :goto_28
    if-eqz v1, :cond_2

    goto/32 :goto_17

    :cond_2
    goto/32 :goto_16

    nop

    :goto_29
    if-ltz v1, :cond_3

    goto/32 :goto_32

    :cond_3
    goto/32 :goto_31

    nop

    :goto_2a
    invoke-virtual {v1, v2, v9, v14}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->onMove(Lcom/android/internal/widget/RecyclerView;Lcom/android/internal/widget/RecyclerView$ViewHolder;Lcom/android/internal/widget/RecyclerView$ViewHolder;)Z

    move-result v1

    goto/32 :goto_41

    nop

    :goto_2b
    int-to-float v2, v2

    goto/32 :goto_0

    nop

    :goto_2c
    iget-object v2, v9, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_1e

    nop

    :goto_2d
    invoke-direct/range {p0 .. p1}, Lcom/android/internal/widget/helper/ItemTouchHelper;->findSwapTargets(Lcom/android/internal/widget/RecyclerView$ViewHolder;)Ljava/util/List;

    move-result-object v13

    goto/32 :goto_46

    nop

    :goto_2e
    int-to-float v1, v1

    goto/32 :goto_2c

    nop

    :goto_2f
    iget-object v1, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    goto/32 :goto_48

    nop

    :goto_30
    move-object/from16 v3, p1

    goto/32 :goto_35

    nop

    :goto_31
    return-void

    :goto_32
    goto/32 :goto_2d

    nop

    :goto_33
    iget-object v2, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_2a

    nop

    :goto_34
    move-object/from16 v9, p1

    goto/32 :goto_d

    nop

    :goto_35
    move/from16 v4, v16

    goto/32 :goto_b

    nop

    :goto_36
    move v8, v12

    goto/32 :goto_e

    nop

    :goto_37
    return-void

    :goto_38
    goto/32 :goto_3

    nop

    :goto_39
    if-ltz v1, :cond_4

    goto/32 :goto_32

    :cond_4
    goto/32 :goto_27

    nop

    :goto_3a
    const/4 v2, 0x2

    goto/32 :goto_23

    nop

    :goto_3b
    iget-object v1, v9, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_1a

    nop

    :goto_3c
    invoke-virtual {v1}, Lcom/android/internal/widget/RecyclerView;->isLayoutRequested()Z

    move-result v1

    goto/32 :goto_21

    nop

    :goto_3d
    cmpg-float v1, v1, v2

    goto/32 :goto_29

    nop

    :goto_3e
    iget v2, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDx:F

    goto/32 :goto_15

    nop

    :goto_3f
    return-void

    :goto_40
    goto/32 :goto_2f

    nop

    :goto_41
    if-nez v1, :cond_5

    goto/32 :goto_f

    :cond_5
    goto/32 :goto_49

    nop

    :goto_42
    iget-object v1, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    goto/32 :goto_33

    nop

    :goto_43
    invoke-virtual {v1, v9, v13, v11, v12}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->chooseDropTarget(Lcom/android/internal/widget/RecyclerView$ViewHolder;Ljava/util/List;II)Lcom/android/internal/widget/RecyclerView$ViewHolder;

    move-result-object v14

    goto/32 :goto_4b

    nop

    :goto_44
    invoke-virtual/range {p1 .. p1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v16

    goto/32 :goto_42

    nop

    :goto_45
    int-to-float v1, v1

    goto/32 :goto_9

    nop

    :goto_46
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v1

    goto/32 :goto_28

    nop

    :goto_47
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    goto/32 :goto_6

    nop

    :goto_48
    invoke-virtual {v1, v9}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->getMoveThreshold(Lcom/android/internal/widget/RecyclerView$ViewHolder;)F

    move-result v10

    goto/32 :goto_20

    nop

    :goto_49
    iget-object v1, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    goto/32 :goto_22

    nop

    :goto_4a
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    goto/32 :goto_45

    nop

    :goto_4b
    if-eqz v14, :cond_6

    goto/32 :goto_38

    :cond_6
    goto/32 :goto_10

    nop

    :goto_4c
    iget v2, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDy:F

    goto/32 :goto_11

    nop
.end method

.method obtainVelocityTracker()V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    goto/32 :goto_3

    nop

    :goto_1
    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    :goto_2
    goto/32 :goto_6

    nop

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop

    :goto_4
    iput-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    goto/32 :goto_5

    nop

    :goto_5
    return-void

    :goto_6
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    goto/32 :goto_4

    nop
.end method

.method public onChildViewAttachedToWindow(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public onChildViewDetachedFromWindow(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p0, p1}, Lcom/android/internal/widget/helper/ItemTouchHelper;->removeChildDrawingOrderCallbackIfNecessary(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    invoke-virtual {v0, p1}, Lcom/android/internal/widget/RecyclerView;->getChildViewHolder(Landroid/view/View;)Lcom/android/internal/widget/RecyclerView$ViewHolder;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelected:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    if-ne v0, v1, :cond_1

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/android/internal/widget/helper/ItemTouchHelper;->select(Lcom/android/internal/widget/RecyclerView$ViewHolder;I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v0, v2}, Lcom/android/internal/widget/helper/ItemTouchHelper;->endRecoverAnimation(Lcom/android/internal/widget/RecyclerView$ViewHolder;Z)I

    iget-object v1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mPendingCleanup:Ljava/util/List;

    iget-object v2, v0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-interface {v1, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    iget-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    invoke-virtual {v1, v2, v0}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->clearView(Lcom/android/internal/widget/RecyclerView;Lcom/android/internal/widget/RecyclerView$ViewHolder;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;Lcom/android/internal/widget/RecyclerView;Lcom/android/internal/widget/RecyclerView$State;)V
    .locals 10

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mOverdrawChildPosition:I

    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelected:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mTmpPosition:[F

    invoke-direct {p0, v2}, Lcom/android/internal/widget/helper/ItemTouchHelper;->getSelectedDxDy([F)V

    iget-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mTmpPosition:[F

    const/4 v3, 0x0

    aget v0, v2, v3

    const/4 v3, 0x1

    aget v1, v2, v3

    :cond_0
    iget-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    iget-object v5, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelected:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    iget-object v6, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecoverAnimations:Ljava/util/List;

    iget v7, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mActionState:I

    move-object v3, p1

    move-object v4, p2

    move v8, v0

    move v9, v1

    invoke-virtual/range {v2 .. v9}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->onDraw(Landroid/graphics/Canvas;Lcom/android/internal/widget/RecyclerView;Lcom/android/internal/widget/RecyclerView$ViewHolder;Ljava/util/List;IFF)V

    return-void
.end method

.method public onDrawOver(Landroid/graphics/Canvas;Lcom/android/internal/widget/RecyclerView;Lcom/android/internal/widget/RecyclerView$State;)V
    .locals 10

    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelected:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mTmpPosition:[F

    invoke-direct {p0, v2}, Lcom/android/internal/widget/helper/ItemTouchHelper;->getSelectedDxDy([F)V

    iget-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mTmpPosition:[F

    const/4 v3, 0x0

    aget v0, v2, v3

    const/4 v3, 0x1

    aget v1, v2, v3

    :cond_0
    iget-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    iget-object v5, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelected:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    iget-object v6, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecoverAnimations:Ljava/util/List;

    iget v7, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mActionState:I

    move-object v3, p1

    move-object v4, p2

    move v8, v0

    move v9, v1

    invoke-virtual/range {v2 .. v9}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->onDrawOver(Landroid/graphics/Canvas;Lcom/android/internal/widget/RecyclerView;Lcom/android/internal/widget/RecyclerView$ViewHolder;Ljava/util/List;IFF)V

    return-void
.end method

.method postDispatchSwipe(Lcom/android/internal/widget/helper/ItemTouchHelper$RecoverAnimation;I)V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_2

    nop

    :goto_2
    new-instance v1, Lcom/android/internal/widget/helper/ItemTouchHelper$4;

    goto/32 :goto_4

    nop

    :goto_3
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/RecyclerView;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_0

    nop

    :goto_4
    invoke-direct {v1, p0, p1, p2}, Lcom/android/internal/widget/helper/ItemTouchHelper$4;-><init>(Lcom/android/internal/widget/helper/ItemTouchHelper;Lcom/android/internal/widget/helper/ItemTouchHelper$RecoverAnimation;I)V

    goto/32 :goto_3

    nop
.end method

.method removeChildDrawingOrderCallbackIfNecessary(Landroid/view/View;)V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    iput-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mOverdrawChild:Landroid/view/View;

    goto/32 :goto_8

    nop

    :goto_1
    iget-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mOverdrawChild:Landroid/view/View;

    goto/32 :goto_5

    nop

    :goto_2
    invoke-virtual {v1, v0}, Lcom/android/internal/widget/RecyclerView;->setChildDrawingOrderCallback(Lcom/android/internal/widget/RecyclerView$ChildDrawingOrderCallback;)V

    :goto_3
    goto/32 :goto_9

    nop

    :goto_4
    if-nez v1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_6

    nop

    :goto_5
    if-eq p1, v0, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_7

    nop

    :goto_6
    iget-object v1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_2

    nop

    :goto_7
    const/4 v0, 0x0

    goto/32 :goto_0

    nop

    :goto_8
    iget-object v1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mChildDrawingOrderCallback:Lcom/android/internal/widget/RecyclerView$ChildDrawingOrderCallback;

    goto/32 :goto_4

    nop

    :goto_9
    return-void
.end method

.method scrollIfNecessary()Z
    .locals 23

    goto/32 :goto_40

    nop

    :goto_0
    iget-object v10, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelected:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_2b

    nop

    :goto_1
    sub-int/2addr v11, v12

    goto/32 :goto_2e

    nop

    :goto_2
    iget v2, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelectedStartY:F

    goto/32 :goto_55

    nop

    :goto_3
    if-ltz v12, :cond_0

    goto/32 :goto_80

    :cond_0
    goto/32 :goto_35

    nop

    :goto_4
    invoke-virtual {v11}, Lcom/android/internal/widget/RecyclerView;->getHeight()I

    move-result v11

    goto/32 :goto_16

    nop

    :goto_5
    iget-object v1, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelected:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_39

    nop

    :goto_6
    if-eqz v7, :cond_1

    goto/32 :goto_21

    :cond_1
    goto/32 :goto_86

    nop

    :goto_7
    if-gtz v10, :cond_2

    goto/32 :goto_84

    :cond_2
    goto/32 :goto_53

    nop

    :goto_8
    iget-object v9, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mTmpRect:Landroid/graphics/Rect;

    goto/32 :goto_45

    nop

    :goto_9
    if-nez v9, :cond_3

    goto/32 :goto_8f

    :cond_3
    goto/32 :goto_6a

    nop

    :goto_a
    invoke-virtual {v11}, Lcom/android/internal/widget/RecyclerView;->getPaddingTop()I

    move-result v11

    goto/32 :goto_94

    nop

    :goto_b
    sub-int v9, v2, v9

    goto/32 :goto_67

    nop

    :goto_c
    iget v11, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDy:F

    goto/32 :goto_1f

    nop

    :goto_d
    invoke-virtual {v10}, Landroid/view/View;->getHeight()I

    move-result v10

    goto/32 :goto_8b

    nop

    :goto_e
    invoke-virtual {v10}, Landroid/view/View;->getHeight()I

    move-result v18

    goto/32 :goto_4e

    nop

    :goto_f
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v11

    goto/32 :goto_4c

    nop

    :goto_10
    iget-object v12, v12, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_69

    nop

    :goto_11
    if-ltz v13, :cond_4

    goto/32 :goto_5e

    :cond_4
    goto/32 :goto_3a

    nop

    :goto_12
    if-eqz v1, :cond_5

    goto/32 :goto_4b

    :cond_5
    goto/32 :goto_8a

    nop

    :goto_13
    add-float/2addr v9, v11

    goto/32 :goto_24

    nop

    :goto_14
    iput-wide v3, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDragScrollStartTimeInMs:J

    goto/32 :goto_70

    nop

    :goto_15
    iget-object v9, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelected:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_5c

    nop

    :goto_16
    iget-object v12, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_1e

    nop

    :goto_17
    cmp-long v1, v7, v3

    goto/32 :goto_12

    nop

    :goto_18
    iget-object v11, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_4

    nop

    :goto_19
    return v2

    :goto_1a
    if-nez v8, :cond_6

    goto/32 :goto_74

    :cond_6
    goto/32 :goto_73

    nop

    :goto_1b
    invoke-virtual {v2}, Lcom/android/internal/widget/RecyclerView;->getWidth()I

    move-result v13

    goto/32 :goto_31

    nop

    :goto_1c
    const-wide/high16 v3, -0x8000000000000000L

    goto/32 :goto_75

    nop

    :goto_1d
    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    goto/32 :goto_20

    nop

    :goto_1e
    invoke-virtual {v12}, Lcom/android/internal/widget/RecyclerView;->getPaddingBottom()I

    move-result v12

    goto/32 :goto_1

    nop

    :goto_1f
    cmpg-float v12, v11, v10

    goto/32 :goto_3

    nop

    :goto_20
    iput-object v7, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mTmpRect:Landroid/graphics/Rect;

    :goto_21
    goto/32 :goto_4f

    nop

    :goto_22
    const/4 v10, 0x0

    goto/32 :goto_9

    nop

    :goto_23
    iget-object v13, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mTmpRect:Landroid/graphics/Rect;

    goto/32 :goto_2a

    nop

    :goto_24
    float-to-int v9, v9

    goto/32 :goto_42

    nop

    :goto_25
    cmpl-float v12, v12, v10

    goto/32 :goto_87

    nop

    :goto_26
    sub-int v11, v9, v11

    goto/32 :goto_93

    nop

    :goto_27
    invoke-virtual/range {v9 .. v15}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->interpolateOutOfBoundsScroll(Lcom/android/internal/widget/RecyclerView;IIIJ)I

    move-result v7

    :goto_28
    goto/32 :goto_89

    nop

    :goto_29
    iget-object v9, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_0

    nop

    :goto_2a
    iget v13, v13, Landroid/graphics/Rect;->right:I

    goto/32 :goto_59

    nop

    :goto_2b
    iget-object v10, v10, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_e

    nop

    :goto_2c
    cmp-long v2, v9, v3

    goto/32 :goto_82

    nop

    :goto_2d
    iget-wide v9, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDragScrollStartTimeInMs:J

    goto/32 :goto_2c

    nop

    :goto_2e
    sub-int/2addr v10, v11

    goto/32 :goto_6e

    nop

    :goto_2f
    move/from16 v19, v8

    goto/32 :goto_81

    nop

    :goto_30
    if-eqz v7, :cond_7

    goto/32 :goto_63

    :cond_7
    goto/32 :goto_1a

    nop

    :goto_31
    move v12, v7

    goto/32 :goto_27

    nop

    :goto_32
    cmpl-float v10, v11, v10

    goto/32 :goto_7

    nop

    :goto_33
    move v8, v9

    goto/32 :goto_7f

    nop

    :goto_34
    sub-int/2addr v12, v13

    goto/32 :goto_88

    nop

    :goto_35
    if-ltz v9, :cond_8

    goto/32 :goto_80

    :cond_8
    goto/32 :goto_33

    nop

    :goto_36
    move-wide v14, v7

    goto/32 :goto_7a

    nop

    :goto_37
    sub-long v7, v5, v7

    :goto_38
    goto/32 :goto_36

    nop

    :goto_39
    const/4 v2, 0x0

    goto/32 :goto_1c

    nop

    :goto_3a
    if-ltz v11, :cond_9

    goto/32 :goto_5e

    :cond_9
    goto/32 :goto_43

    nop

    :goto_3b
    sub-int/2addr v11, v12

    goto/32 :goto_6c

    nop

    :goto_3c
    iget-object v2, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    goto/32 :goto_29

    nop

    :goto_3d
    invoke-virtual {v2, v7, v8}, Lcom/android/internal/widget/RecyclerView;->scrollBy(II)V

    goto/32 :goto_54

    nop

    :goto_3e
    iget v11, v11, Landroid/graphics/Rect;->left:I

    goto/32 :goto_26

    nop

    :goto_3f
    sub-int/2addr v13, v2

    goto/32 :goto_34

    nop

    :goto_40
    move-object/from16 v0, p0

    goto/32 :goto_5

    nop

    :goto_41
    iget-object v2, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_3d

    nop

    :goto_42
    iget-object v11, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mTmpRect:Landroid/graphics/Rect;

    goto/32 :goto_3e

    nop

    :goto_43
    move v7, v11

    goto/32 :goto_5d

    nop

    :goto_44
    invoke-virtual {v2}, Lcom/android/internal/widget/RecyclerView;->getPaddingRight()I

    move-result v2

    goto/32 :goto_3f

    nop

    :goto_45
    iget v9, v9, Landroid/graphics/Rect;->top:I

    goto/32 :goto_b

    nop

    :goto_46
    add-int/2addr v12, v9

    goto/32 :goto_23

    nop

    :goto_47
    iget-object v7, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mTmpRect:Landroid/graphics/Rect;

    goto/32 :goto_6

    nop

    :goto_48
    invoke-virtual {v12}, Lcom/android/internal/widget/RecyclerView;->getPaddingLeft()I

    move-result v12

    goto/32 :goto_3b

    nop

    :goto_49
    iget-object v2, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelected:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_90

    nop

    :goto_4a
    goto :goto_38

    :goto_4b
    goto/32 :goto_37

    nop

    :goto_4c
    iget-object v2, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_1b

    nop

    :goto_4d
    iget-object v10, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mTmpRect:Landroid/graphics/Rect;

    goto/32 :goto_72

    nop

    :goto_4e
    iget-object v10, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_92

    nop

    :goto_4f
    const/4 v7, 0x0

    goto/32 :goto_68

    nop

    :goto_50
    iget-object v12, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelected:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_10

    nop

    :goto_51
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    goto/32 :goto_58

    nop

    :goto_52
    cmpg-float v13, v12, v10

    goto/32 :goto_11

    nop

    :goto_53
    iget-object v10, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelected:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_7b

    nop

    :goto_54
    const/4 v2, 0x1

    goto/32 :goto_19

    nop

    :goto_55
    iget v9, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDy:F

    goto/32 :goto_8c

    nop

    :goto_56
    const/4 v2, 0x0

    goto/32 :goto_62

    nop

    :goto_57
    move-object/from16 v16, v2

    goto/32 :goto_64

    nop

    :goto_58
    iget-wide v7, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDragScrollStartTimeInMs:J

    goto/32 :goto_17

    nop

    :goto_59
    add-int/2addr v12, v13

    goto/32 :goto_7c

    nop

    :goto_5a
    invoke-virtual {v13}, Lcom/android/internal/widget/RecyclerView;->getWidth()I

    move-result v13

    goto/32 :goto_91

    nop

    :goto_5b
    iput-wide v3, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDragScrollStartTimeInMs:J

    goto/32 :goto_56

    nop

    :goto_5c
    iget-object v9, v9, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_4d

    nop

    :goto_5d
    goto/16 :goto_8f

    :goto_5e
    goto/32 :goto_25

    nop

    :goto_5f
    invoke-virtual {v1}, Lcom/android/internal/widget/RecyclerView$LayoutManager;->canScrollVertically()Z

    move-result v2

    goto/32 :goto_7d

    nop

    :goto_60
    add-int/2addr v10, v11

    goto/32 :goto_18

    nop

    :goto_61
    if-nez v7, :cond_a

    goto/32 :goto_28

    :cond_a
    goto/32 :goto_6f

    nop

    :goto_62
    return v2

    :goto_63
    goto/32 :goto_2d

    nop

    :goto_64
    move-object/from16 v17, v9

    goto/32 :goto_2f

    nop

    :goto_65
    invoke-virtual/range {v16 .. v22}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->interpolateOutOfBoundsScroll(Lcom/android/internal/widget/RecyclerView;IIIJ)I

    move-result v8

    :goto_66
    goto/32 :goto_30

    nop

    :goto_67
    iget-object v11, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_a

    nop

    :goto_68
    const/4 v8, 0x0

    goto/32 :goto_15

    nop

    :goto_69
    invoke-virtual {v12}, Landroid/view/View;->getWidth()I

    move-result v12

    goto/32 :goto_46

    nop

    :goto_6a
    iget v9, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelectedStartX:F

    goto/32 :goto_79

    nop

    :goto_6b
    float-to-int v2, v2

    goto/32 :goto_8

    nop

    :goto_6c
    iget v12, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDx:F

    goto/32 :goto_52

    nop

    :goto_6d
    iget v11, v11, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_60

    nop

    :goto_6e
    if-gtz v10, :cond_b

    goto/32 :goto_84

    :cond_b
    goto/32 :goto_83

    nop

    :goto_6f
    iget-object v9, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    goto/32 :goto_76

    nop

    :goto_70
    return v2

    :goto_71
    goto/32 :goto_51

    nop

    :goto_72
    invoke-virtual {v1, v9, v10}, Lcom/android/internal/widget/RecyclerView$LayoutManager;->calculateItemDecorationsForChild(Landroid/view/View;Landroid/graphics/Rect;)V

    goto/32 :goto_8d

    nop

    :goto_73
    goto :goto_63

    :goto_74
    goto/32 :goto_5b

    nop

    :goto_75
    if-eqz v1, :cond_c

    goto/32 :goto_71

    :cond_c
    goto/32 :goto_14

    nop

    :goto_76
    iget-object v10, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_49

    nop

    :goto_77
    iput-wide v5, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDragScrollStartTimeInMs:J

    :goto_78
    goto/32 :goto_41

    nop

    :goto_79
    iget v11, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDx:F

    goto/32 :goto_13

    nop

    :goto_7a
    iget-object v1, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_7e

    nop

    :goto_7b
    iget-object v10, v10, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_d

    nop

    :goto_7c
    iget-object v13, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_5a

    nop

    :goto_7d
    if-nez v2, :cond_d

    goto/32 :goto_84

    :cond_d
    goto/32 :goto_2

    nop

    :goto_7e
    invoke-virtual {v1}, Lcom/android/internal/widget/RecyclerView;->getLayoutManager()Lcom/android/internal/widget/RecyclerView$LayoutManager;

    move-result-object v1

    goto/32 :goto_47

    nop

    :goto_7f
    goto :goto_84

    :goto_80
    goto/32 :goto_32

    nop

    :goto_81
    move-wide/from16 v21, v14

    goto/32 :goto_65

    nop

    :goto_82
    if-eqz v2, :cond_e

    goto/32 :goto_78

    :cond_e
    goto/32 :goto_77

    nop

    :goto_83
    move v8, v10

    :goto_84
    goto/32 :goto_61

    nop

    :goto_85
    iget-object v11, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mTmpRect:Landroid/graphics/Rect;

    goto/32 :goto_6d

    nop

    :goto_86
    new-instance v7, Landroid/graphics/Rect;

    goto/32 :goto_1d

    nop

    :goto_87
    if-gtz v12, :cond_f

    goto/32 :goto_8f

    :cond_f
    goto/32 :goto_50

    nop

    :goto_88
    if-gtz v12, :cond_10

    goto/32 :goto_8f

    :cond_10
    goto/32 :goto_8e

    nop

    :goto_89
    if-nez v8, :cond_11

    goto/32 :goto_66

    :cond_11
    goto/32 :goto_3c

    nop

    :goto_8a
    const-wide/16 v7, 0x0

    goto/32 :goto_4a

    nop

    :goto_8b
    add-int/2addr v10, v2

    goto/32 :goto_85

    nop

    :goto_8c
    add-float/2addr v2, v9

    goto/32 :goto_6b

    nop

    :goto_8d
    invoke-virtual {v1}, Lcom/android/internal/widget/RecyclerView$LayoutManager;->canScrollHorizontally()Z

    move-result v9

    goto/32 :goto_22

    nop

    :goto_8e
    move v7, v12

    :goto_8f
    goto/32 :goto_5f

    nop

    :goto_90
    iget-object v2, v2, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_f

    nop

    :goto_91
    iget-object v2, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_44

    nop

    :goto_92
    invoke-virtual {v10}, Lcom/android/internal/widget/RecyclerView;->getHeight()I

    move-result v20

    goto/32 :goto_57

    nop

    :goto_93
    iget-object v12, v0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_48

    nop

    :goto_94
    sub-int/2addr v9, v11

    goto/32 :goto_c

    nop
.end method

.method select(Lcom/android/internal/widget/RecyclerView$ViewHolder;I)V
    .locals 26

    goto/32 :goto_3f

    nop

    :goto_0
    invoke-virtual {v0, v1, v12}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->getAbsoluteMovementFlags(Lcom/android/internal/widget/RecyclerView;Lcom/android/internal/widget/RecyclerView$ViewHolder;)I

    move-result v0

    goto/32 :goto_ae

    nop

    :goto_1
    move v14, v10

    goto/32 :goto_4a

    nop

    :goto_2
    return-void

    :goto_3
    goto/32 :goto_6

    nop

    :goto_4
    const/16 v17, 0x0

    goto/32 :goto_af

    nop

    :goto_5
    iget-object v0, v12, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_6c

    nop

    :goto_6
    const-wide/high16 v0, -0x8000000000000000L

    goto/32 :goto_66

    nop

    :goto_7
    goto/16 :goto_97

    :goto_8
    goto/32 :goto_60

    nop

    :goto_9
    move/from16 v19, v0

    goto/32 :goto_31

    nop

    :goto_a
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/widget/helper/ItemTouchHelper;->releaseVelocityTracker()V

    sparse-switch v18, :sswitch_data_0

    goto/32 :goto_a7

    nop

    :goto_b
    invoke-virtual {v1}, Lcom/android/internal/widget/RecyclerView;->getLayoutManager()Lcom/android/internal/widget/RecyclerView$LayoutManager;

    move-result-object v1

    goto/32 :goto_68

    nop

    :goto_c
    move v3, v7

    goto/32 :goto_9a

    nop

    :goto_d
    const/4 v1, 0x0

    goto/32 :goto_1f

    nop

    :goto_e
    iget-object v0, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mTmpPosition:[F

    goto/32 :goto_a2

    nop

    :goto_f
    move v15, v1

    :goto_10
    goto/32 :goto_37

    nop

    :goto_11
    iget v1, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mActionState:I

    goto/32 :goto_2f

    nop

    :goto_12
    iput v0, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelectedStartY:F

    goto/32 :goto_53

    nop

    :goto_13
    shl-int v0, v15, v0

    goto/32 :goto_7b

    nop

    :goto_14
    iget v14, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mActionState:I

    goto/32 :goto_34

    nop

    :goto_15
    if-nez v0, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_83

    nop

    :goto_16
    move v14, v10

    goto/32 :goto_6a

    nop

    :goto_17
    move-object/from16 v24, v8

    goto/32 :goto_62

    nop

    :goto_18
    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    move-result v1

    goto/32 :goto_23

    nop

    :goto_19
    move/from16 v25, v14

    goto/32 :goto_16

    nop

    :goto_1a
    move v7, v0

    goto/32 :goto_ab

    nop

    :goto_1b
    new-instance v23, Lcom/android/internal/widget/helper/ItemTouchHelper$3;

    goto/32 :goto_24

    nop

    :goto_1c
    move/from16 v19, v0

    goto/32 :goto_51

    nop

    :goto_1d
    move/from16 v25, v14

    goto/32 :goto_29

    nop

    :goto_1e
    move-object/from16 v0, v24

    goto/32 :goto_7

    nop

    :goto_1f
    iget v0, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDy:F

    goto/32 :goto_87

    nop

    :goto_20
    goto/16 :goto_64

    :goto_21
    goto/32 :goto_9c

    nop

    :goto_22
    if-eq v14, v10, :cond_1

    goto/32 :goto_ac

    :cond_1
    goto/32 :goto_b5

    nop

    :goto_23
    iget-object v2, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_7c

    nop

    :goto_24
    move-object/from16 v0, v23

    goto/32 :goto_67

    nop

    :goto_25
    int-to-float v0, v0

    goto/32 :goto_a0

    nop

    :goto_26
    iput v0, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelectedFlags:I

    goto/32 :goto_8b

    nop

    :goto_27
    move v15, v7

    goto/32 :goto_b1

    nop

    :goto_28
    iget-object v1, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    goto/32 :goto_9b

    nop

    :goto_29
    move v14, v9

    goto/32 :goto_a3

    nop

    :goto_2a
    invoke-virtual {v1, v2, v3}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->onSelectedChanged(Lcom/android/internal/widget/RecyclerView$ViewHolder;I)V

    goto/32 :goto_3c

    nop

    :goto_2b
    mul-float/2addr v0, v2

    goto/32 :goto_b7

    nop

    :goto_2c
    iget v0, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mActionState:I

    goto/32 :goto_92

    nop

    :goto_2d
    move/from16 v6, v22

    goto/32 :goto_27

    nop

    :goto_2e
    const/4 v1, 0x0

    goto/32 :goto_8a

    nop

    :goto_2f
    mul-int/lit8 v1, v1, 0x8

    goto/32 :goto_a4

    nop

    :goto_30
    int-to-float v2, v2

    goto/32 :goto_65

    nop

    :goto_31
    move/from16 v20, v1

    goto/32 :goto_79

    nop

    :goto_32
    move/from16 v20, v1

    :goto_33
    goto/32 :goto_22

    nop

    :goto_34
    const/4 v15, 0x1

    goto/32 :goto_5d

    nop

    :goto_35
    iget-object v1, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_0

    nop

    :goto_36
    mul-int/lit8 v0, v13, 0x8

    goto/32 :goto_3e

    nop

    :goto_37
    invoke-interface {v0, v15}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :goto_38
    goto/32 :goto_71

    nop

    :goto_39
    iget-object v8, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelected:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_49

    nop

    :goto_3a
    goto :goto_43

    :goto_3b
    goto/32 :goto_42

    nop

    :goto_3c
    iget-object v1, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_a6

    nop

    :goto_3d
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    goto/32 :goto_25

    nop

    :goto_3e
    add-int/lit8 v0, v0, 0x8

    goto/32 :goto_13

    nop

    :goto_3f
    move-object/from16 v11, p0

    goto/32 :goto_52

    nop

    :goto_40
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    goto/32 :goto_61

    nop

    :goto_41
    if-gtz v18, :cond_2

    goto/32 :goto_56

    :cond_2
    goto/32 :goto_45

    nop

    :goto_42
    invoke-direct {v11, v8}, Lcom/android/internal/widget/helper/ItemTouchHelper;->swipeIfNecessary(Lcom/android/internal/widget/RecyclerView$ViewHolder;)I

    move-result v0

    :goto_43
    goto/32 :goto_7f

    nop

    :goto_44
    move/from16 v5, v21

    goto/32 :goto_2d

    nop

    :goto_45
    const/4 v0, 0x2

    goto/32 :goto_6d

    nop

    :goto_46
    move v7, v0

    :goto_47
    goto/32 :goto_b4

    nop

    :goto_48
    iget-object v0, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    goto/32 :goto_35

    nop

    :goto_49
    iget-object v0, v8, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_84

    nop

    :goto_4a
    move-object/from16 v10, v24

    goto/32 :goto_80

    nop

    :goto_4b
    iget-object v1, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    goto/32 :goto_6f

    nop

    :goto_4c
    if-eq v12, v0, :cond_3

    goto/32 :goto_3

    :cond_3
    goto/32 :goto_2c

    nop

    :goto_4d
    move/from16 v17, v0

    goto/32 :goto_1e

    nop

    :goto_4e
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_90

    nop

    :goto_4f
    const/4 v1, 0x0

    goto/32 :goto_a8

    nop

    :goto_50
    const/4 v9, 0x0

    goto/32 :goto_54

    nop

    :goto_51
    move/from16 v20, v1

    goto/32 :goto_b8

    nop

    :goto_52
    move-object/from16 v12, p1

    goto/32 :goto_aa

    nop

    :goto_53
    iput-object v12, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelected:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_58

    nop

    :goto_54
    if-nez v0, :cond_4

    goto/32 :goto_21

    :cond_4
    goto/32 :goto_39

    nop

    :goto_55
    goto :goto_47

    :goto_56
    goto/32 :goto_5c

    nop

    :goto_57
    iget-object v2, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_72

    nop

    :goto_58
    if-eq v13, v14, :cond_5

    goto/32 :goto_99

    :cond_5
    goto/32 :goto_82

    nop

    :goto_59
    sub-float v4, v19, v22

    goto/32 :goto_b6

    nop

    :goto_5a
    const/4 v0, 0x0

    goto/32 :goto_a1

    nop

    :goto_5b
    invoke-virtual {v11, v1}, Lcom/android/internal/widget/helper/ItemTouchHelper;->removeChildDrawingOrderCallbackIfNecessary(Landroid/view/View;)V

    goto/32 :goto_4b

    nop

    :goto_5c
    const/4 v0, 0x4

    goto/32 :goto_46

    nop

    :goto_5d
    invoke-virtual {v11, v12, v15}, Lcom/android/internal/widget/helper/ItemTouchHelper;->endRecoverAnimation(Lcom/android/internal/widget/RecyclerView$ViewHolder;Z)I

    goto/32 :goto_7e

    nop

    :goto_5e
    iget-object v2, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelected:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_8f

    nop

    :goto_5f
    invoke-direct {v11, v0}, Lcom/android/internal/widget/helper/ItemTouchHelper;->getSelectedDxDy([F)V

    goto/32 :goto_e

    nop

    :goto_60
    move-object/from16 v24, v8

    goto/32 :goto_19

    nop

    :goto_61
    int-to-float v0, v0

    goto/32 :goto_12

    nop

    :goto_62
    move/from16 v8, v19

    goto/32 :goto_1d

    nop

    :goto_63
    move v14, v10

    :goto_64
    goto/32 :goto_94

    nop

    :goto_65
    mul-float/2addr v1, v2

    goto/32 :goto_1c

    nop

    :goto_66
    iput-wide v0, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDragScrollStartTimeInMs:J

    goto/32 :goto_14

    nop

    :goto_67
    move-object/from16 v1, p0

    goto/32 :goto_8c

    nop

    :goto_68
    invoke-virtual {v1}, Lcom/android/internal/widget/RecyclerView$LayoutManager;->requestSimpleAnimationsInNextLayout()V

    :goto_69
    goto/32 :goto_28

    nop

    :goto_6a
    move-object/from16 v0, v24

    goto/32 :goto_85

    nop

    :goto_6b
    iget-object v0, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelected:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_4c

    nop

    :goto_6c
    iput-object v0, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mOverdrawChild:Landroid/view/View;

    goto/32 :goto_8d

    nop

    :goto_6d
    move v7, v0

    goto/32 :goto_55

    nop

    :goto_6e
    iget-object v1, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    goto/32 :goto_57

    nop

    :goto_6f
    iget-object v2, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_96

    nop

    :goto_70
    iget-object v0, v12, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_40

    nop

    :goto_71
    if-eqz v17, :cond_6

    goto/32 :goto_69

    :cond_6
    goto/32 :goto_7a

    nop

    :goto_72
    sub-float v3, v20, v21

    goto/32 :goto_59

    nop

    :goto_73
    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto/32 :goto_76

    nop

    :goto_74
    if-eq v13, v10, :cond_7

    goto/32 :goto_8e

    :cond_7
    goto/32 :goto_5

    nop

    :goto_75
    aget v22, v0, v15

    goto/32 :goto_1b

    nop

    :goto_76
    if-nez v0, :cond_8

    goto/32 :goto_38

    :cond_8
    goto/32 :goto_5e

    nop

    :goto_77
    const/4 v0, 0x0

    goto/32 :goto_9

    nop

    :goto_78
    invoke-virtual {v2}, Lcom/android/internal/widget/RecyclerView;->getHeight()I

    move-result v2

    goto/32 :goto_91

    nop

    :goto_79
    goto/16 :goto_33

    :sswitch_0
    goto/32 :goto_5a

    nop

    :goto_7a
    iget-object v1, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_b

    nop

    :goto_7b
    add-int/lit8 v16, v0, -0x1

    goto/32 :goto_4

    nop

    :goto_7c
    invoke-virtual {v2}, Lcom/android/internal/widget/RecyclerView;->getWidth()I

    move-result v2

    goto/32 :goto_30

    nop

    :goto_7d
    return-void

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_1
        0x4 -> :sswitch_0
        0x8 -> :sswitch_0
        0x10 -> :sswitch_0
        0x20 -> :sswitch_0
    .end sparse-switch

    :goto_7e
    iput v13, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mActionState:I

    goto/32 :goto_95

    nop

    :goto_7f
    move/from16 v18, v0

    goto/32 :goto_a

    nop

    :goto_80
    invoke-direct/range {v0 .. v10}, Lcom/android/internal/widget/helper/ItemTouchHelper$3;-><init>(Lcom/android/internal/widget/helper/ItemTouchHelper;Lcom/android/internal/widget/RecyclerView$ViewHolder;IIFFFFILcom/android/internal/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_6e

    nop

    :goto_81
    iget v3, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mActionState:I

    goto/32 :goto_2a

    nop

    :goto_82
    iget-object v0, v12, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_2e

    nop

    :goto_83
    if-eq v14, v10, :cond_9

    goto/32 :goto_3b

    :cond_9
    goto/32 :goto_93

    nop

    :goto_84
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto/32 :goto_15

    nop

    :goto_85
    iget-object v1, v0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_5b

    nop

    :goto_86
    invoke-virtual {v0, v1, v2}, Lcom/android/internal/widget/helper/ItemTouchHelper$RecoverAnimation;->setDuration(J)V

    goto/32 :goto_88

    nop

    :goto_87
    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    goto/32 :goto_9f

    nop

    :goto_88
    iget-object v3, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecoverAnimations:Ljava/util/List;

    goto/32 :goto_4e

    nop

    :goto_89
    const/4 v0, 0x1

    goto/32 :goto_4d

    nop

    :goto_8a
    invoke-virtual {v0, v1}, Landroid/view/View;->performHapticFeedback(I)Z

    goto/32 :goto_98

    nop

    :goto_8b
    iget-object v0, v12, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_3d

    nop

    :goto_8c
    move-object v2, v8

    goto/32 :goto_c

    nop

    :goto_8d
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/widget/helper/ItemTouchHelper;->addChildDrawingOrderCallback()V

    :goto_8e
    goto/32 :goto_36

    nop

    :goto_8f
    if-nez v2, :cond_a

    goto/32 :goto_9e

    :cond_a
    goto/32 :goto_b0

    nop

    :goto_90
    invoke-virtual {v0}, Lcom/android/internal/widget/helper/ItemTouchHelper$RecoverAnimation;->start()V

    goto/32 :goto_89

    nop

    :goto_91
    int-to-float v2, v2

    goto/32 :goto_2b

    nop

    :goto_92
    if-eq v13, v0, :cond_b

    goto/32 :goto_3

    :cond_b
    goto/32 :goto_2

    nop

    :goto_93
    move v0, v9

    goto/32 :goto_3a

    nop

    :goto_94
    if-nez v12, :cond_c

    goto/32 :goto_a9

    :cond_c
    goto/32 :goto_48

    nop

    :goto_95
    const/4 v10, 0x2

    goto/32 :goto_74

    nop

    :goto_96
    invoke-virtual {v1, v2, v0}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->clearView(Lcom/android/internal/widget/RecyclerView;Lcom/android/internal/widget/RecyclerView$ViewHolder;)V

    :goto_97
    goto/32 :goto_ad

    nop

    :goto_98
    goto :goto_b3

    :goto_99
    goto/32 :goto_4f

    nop

    :goto_9a
    move v4, v14

    goto/32 :goto_44

    nop

    :goto_9b
    iget-object v2, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelected:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_81

    nop

    :goto_9c
    move/from16 v25, v14

    goto/32 :goto_63

    nop

    :goto_9d
    goto/16 :goto_10

    :goto_9e
    goto/32 :goto_f

    nop

    :goto_9f
    iget-object v2, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_78

    nop

    :goto_a0
    iput v0, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelectedStartX:F

    goto/32 :goto_70

    nop

    :goto_a1
    iget v1, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDx:F

    goto/32 :goto_18

    nop

    :goto_a2
    aget v21, v0, v9

    goto/32 :goto_75

    nop

    :goto_a3
    move/from16 v9, v18

    goto/32 :goto_1

    nop

    :goto_a4
    shr-int/2addr v0, v1

    goto/32 :goto_26

    nop

    :goto_a5
    iget-object v0, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_73

    nop

    :goto_a6
    invoke-virtual {v1}, Lcom/android/internal/widget/RecyclerView;->invalidate()V

    goto/32 :goto_7d

    nop

    :goto_a7
    const/4 v1, 0x0

    goto/32 :goto_77

    nop

    :goto_a8
    goto :goto_b3

    :goto_a9
    goto/32 :goto_b2

    nop

    :goto_aa
    move/from16 v13, p2

    goto/32 :goto_6b

    nop

    :goto_ab
    goto/16 :goto_47

    :goto_ac
    goto/32 :goto_41

    nop

    :goto_ad
    const/4 v1, 0x0

    goto/32 :goto_b9

    nop

    :goto_ae
    and-int v0, v0, v16

    goto/32 :goto_11

    nop

    :goto_af
    iget-object v0, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelected:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_50

    nop

    :goto_b0
    const/4 v15, 0x1

    goto/32 :goto_9d

    nop

    :goto_b1
    move/from16 v7, v20

    goto/32 :goto_17

    nop

    :goto_b2
    const/4 v1, 0x0

    :goto_b3
    goto/32 :goto_a5

    nop

    :goto_b4
    iget-object v0, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mTmpPosition:[F

    goto/32 :goto_5f

    nop

    :goto_b5
    const/16 v0, 0x8

    goto/32 :goto_1a

    nop

    :goto_b6
    invoke-virtual {v1, v2, v15, v3, v4}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->getAnimationDuration(Lcom/android/internal/widget/RecyclerView;IFF)J

    move-result-wide v1

    goto/32 :goto_86

    nop

    :goto_b7
    move/from16 v19, v0

    goto/32 :goto_32

    nop

    :goto_b8
    goto/16 :goto_33

    :sswitch_1
    goto/32 :goto_d

    nop

    :goto_b9
    iput-object v1, v11, Lcom/android/internal/widget/helper/ItemTouchHelper;->mSelected:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_20

    nop
.end method

.method public startDrag(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V
    .locals 3

    iget-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    iget-object v1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    invoke-virtual {v0, v1, p1}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->hasDragFlag(Lcom/android/internal/widget/RecyclerView;Lcom/android/internal/widget/RecyclerView$ViewHolder;)Z

    move-result v0

    const-string v1, "ItemTouchHelper"

    if-nez v0, :cond_0

    const-string v0, "Start drag has been called but dragging is not enabled"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v0, p1, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    if-eq v0, v2, :cond_1

    const-string v0, "Start drag has been called with a view holder which is not a child of the RecyclerView which is controlled by this ItemTouchHelper."

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/internal/widget/helper/ItemTouchHelper;->obtainVelocityTracker()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDy:F

    iput v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDx:F

    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lcom/android/internal/widget/helper/ItemTouchHelper;->select(Lcom/android/internal/widget/RecyclerView$ViewHolder;I)V

    return-void
.end method

.method public startSwipe(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V
    .locals 3

    iget-object v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mCallback:Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;

    iget-object v1, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    invoke-virtual {v0, v1, p1}, Lcom/android/internal/widget/helper/ItemTouchHelper$Callback;->hasSwipeFlag(Lcom/android/internal/widget/RecyclerView;Lcom/android/internal/widget/RecyclerView$ViewHolder;)Z

    move-result v0

    const-string v1, "ItemTouchHelper"

    if-nez v0, :cond_0

    const-string v0, "Start swipe has been called but swiping is not enabled"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v0, p1, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mRecyclerView:Lcom/android/internal/widget/RecyclerView;

    if-eq v0, v2, :cond_1

    const-string v0, "Start swipe has been called with a view holder which is not a child of the RecyclerView controlled by this ItemTouchHelper."

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/internal/widget/helper/ItemTouchHelper;->obtainVelocityTracker()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDy:F

    iput v0, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDx:F

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/android/internal/widget/helper/ItemTouchHelper;->select(Lcom/android/internal/widget/RecyclerView$ViewHolder;I)V

    return-void
.end method

.method updateDxDy(Landroid/view/MotionEvent;II)V
    .locals 5

    goto/32 :goto_12

    nop

    :goto_0
    iget v3, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mInitialTouchY:F

    goto/32 :goto_1e

    nop

    :goto_1
    iget v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDx:F

    goto/32 :goto_9

    nop

    :goto_2
    invoke-static {v4, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    goto/32 :goto_1c

    nop

    :goto_3
    iput v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDx:F

    :goto_4
    goto/32 :goto_13

    nop

    :goto_5
    invoke-static {v4, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    goto/32 :goto_3

    nop

    :goto_6
    iput v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDx:F

    goto/32 :goto_0

    nop

    :goto_7
    and-int/lit8 v2, p2, 0x2

    goto/32 :goto_a

    nop

    :goto_8
    invoke-static {v4, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    goto/32 :goto_b

    nop

    :goto_9
    invoke-static {v4, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    goto/32 :goto_16

    nop

    :goto_a
    if-eqz v2, :cond_0

    goto/32 :goto_1d

    :cond_0
    goto/32 :goto_20

    nop

    :goto_b
    iput v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDy:F

    :goto_c
    goto/32 :goto_7

    nop

    :goto_d
    if-eqz v3, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_5

    nop

    :goto_e
    and-int/lit8 v3, p2, 0x4

    goto/32 :goto_1b

    nop

    :goto_f
    if-eqz v2, :cond_2

    goto/32 :goto_c

    :cond_2
    goto/32 :goto_11

    nop

    :goto_10
    if-eqz v2, :cond_3

    goto/32 :goto_17

    :cond_3
    goto/32 :goto_1

    nop

    :goto_11
    iget v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDy:F

    goto/32 :goto_8

    nop

    :goto_12
    invoke-virtual {p1, p3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    goto/32 :goto_1a

    nop

    :goto_13
    and-int/lit8 v2, p2, 0x8

    goto/32 :goto_10

    nop

    :goto_14
    and-int/lit8 v2, p2, 0x1

    goto/32 :goto_f

    nop

    :goto_15
    return-void

    :goto_16
    iput v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDx:F

    :goto_17
    goto/32 :goto_14

    nop

    :goto_18
    sub-float v2, v0, v2

    goto/32 :goto_6

    nop

    :goto_19
    iput v3, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDy:F

    goto/32 :goto_e

    nop

    :goto_1a
    invoke-virtual {p1, p3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    goto/32 :goto_1f

    nop

    :goto_1b
    const/4 v4, 0x0

    goto/32 :goto_d

    nop

    :goto_1c
    iput v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDy:F

    :goto_1d
    goto/32 :goto_15

    nop

    :goto_1e
    sub-float v3, v1, v3

    goto/32 :goto_19

    nop

    :goto_1f
    iget v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mInitialTouchX:F

    goto/32 :goto_18

    nop

    :goto_20
    iget v2, p0, Lcom/android/internal/widget/helper/ItemTouchHelper;->mDy:F

    goto/32 :goto_2

    nop
.end method
