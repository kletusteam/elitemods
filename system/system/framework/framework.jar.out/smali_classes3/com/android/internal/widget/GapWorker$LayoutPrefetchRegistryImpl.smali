.class Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/android/internal/widget/RecyclerView$LayoutManager$LayoutPrefetchRegistry;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/GapWorker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LayoutPrefetchRegistryImpl"
.end annotation


# instance fields
.field mCount:I

.field mPrefetchArray:[I

.field mPrefetchDx:I

.field mPrefetchDy:I


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addPosition(II)V
    .locals 5

    if-ltz p2, :cond_2

    iget v0, p0, Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;->mCount:I

    mul-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;->mPrefetchArray:[I

    if-nez v1, :cond_0

    const/4 v1, 0x4

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;->mPrefetchArray:[I

    const/4 v2, -0x1

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([II)V

    goto :goto_0

    :cond_0
    array-length v1, v1

    if-lt v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;->mPrefetchArray:[I

    mul-int/lit8 v2, v0, 0x2

    new-array v2, v2, [I

    iput-object v2, p0, Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;->mPrefetchArray:[I

    array-length v3, v1

    const/4 v4, 0x0

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;->mPrefetchArray:[I

    aput p1, v1, v0

    add-int/lit8 v2, v0, 0x1

    aput p2, v1, v2

    iget v1, p0, Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;->mCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;->mCount:I

    return-void

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Pixel distance must be non-negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method clearPrefetchPositions()V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;->mPrefetchArray:[I

    goto/32 :goto_4

    nop

    :goto_1
    return-void

    :goto_2
    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    :goto_3
    goto/32 :goto_1

    nop

    :goto_4
    if-nez v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_5

    nop

    :goto_5
    const/4 v1, -0x1

    goto/32 :goto_2

    nop
.end method

.method collectPrefetchPositionsFromView(Lcom/android/internal/widget/RecyclerView;Z)V
    .locals 4

    goto/32 :goto_4

    nop

    :goto_0
    iget v1, p0, Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;->mPrefetchDx:I

    goto/32 :goto_d

    nop

    :goto_1
    iget-object v0, p0, Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;->mPrefetchArray:[I

    goto/32 :goto_16

    nop

    :goto_2
    iget-object v0, p1, Lcom/android/internal/widget/RecyclerView;->mLayout:Lcom/android/internal/widget/RecyclerView$LayoutManager;

    goto/32 :goto_18

    nop

    :goto_3
    iget-object v1, p1, Lcom/android/internal/widget/RecyclerView;->mRecycler:Lcom/android/internal/widget/RecyclerView$Recycler;

    goto/32 :goto_1e

    nop

    :goto_4
    const/4 v0, 0x0

    goto/32 :goto_25

    nop

    :goto_5
    iget v2, v0, Lcom/android/internal/widget/RecyclerView$LayoutManager;->mPrefetchMaxCountObserved:I

    goto/32 :goto_8

    nop

    :goto_6
    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    :goto_7
    goto/32 :goto_2

    nop

    :goto_8
    if-gt v1, v2, :cond_0

    goto/32 :goto_1f

    :cond_0
    goto/32 :goto_20

    nop

    :goto_9
    invoke-virtual {v1}, Lcom/android/internal/widget/RecyclerView$Adapter;->getItemCount()I

    move-result v1

    goto/32 :goto_e

    nop

    :goto_a
    goto/16 :goto_23

    :goto_b
    goto/32 :goto_10

    nop

    :goto_c
    iget v1, p0, Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;->mCount:I

    goto/32 :goto_5

    nop

    :goto_d
    iget v2, p0, Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;->mPrefetchDy:I

    goto/32 :goto_11

    nop

    :goto_e
    invoke-virtual {v0, v1, p0}, Lcom/android/internal/widget/RecyclerView$LayoutManager;->collectInitialPrefetchPositions(ILcom/android/internal/widget/RecyclerView$LayoutManager$LayoutPrefetchRegistry;)V

    goto/32 :goto_a

    nop

    :goto_f
    invoke-virtual {v1}, Lcom/android/internal/widget/AdapterHelper;->hasPendingUpdates()Z

    move-result v1

    goto/32 :goto_24

    nop

    :goto_10
    invoke-virtual {p1}, Lcom/android/internal/widget/RecyclerView;->hasPendingAdapterUpdates()Z

    move-result v1

    goto/32 :goto_21

    nop

    :goto_11
    iget-object v3, p1, Lcom/android/internal/widget/RecyclerView;->mState:Lcom/android/internal/widget/RecyclerView$State;

    goto/32 :goto_22

    nop

    :goto_12
    if-nez v0, :cond_1

    goto/32 :goto_1f

    :cond_1
    goto/32 :goto_17

    nop

    :goto_13
    const/4 v1, -0x1

    goto/32 :goto_6

    nop

    :goto_14
    if-nez p2, :cond_2

    goto/32 :goto_b

    :cond_2
    goto/32 :goto_1d

    nop

    :goto_15
    if-nez v1, :cond_3

    goto/32 :goto_1f

    :cond_3
    goto/32 :goto_12

    nop

    :goto_16
    if-nez v0, :cond_4

    goto/32 :goto_7

    :cond_4
    goto/32 :goto_13

    nop

    :goto_17
    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView$LayoutManager;->isItemPrefetchEnabled()Z

    move-result v1

    goto/32 :goto_1c

    nop

    :goto_18
    iget-object v1, p1, Lcom/android/internal/widget/RecyclerView;->mAdapter:Lcom/android/internal/widget/RecyclerView$Adapter;

    goto/32 :goto_15

    nop

    :goto_19
    iput v1, v0, Lcom/android/internal/widget/RecyclerView$LayoutManager;->mPrefetchMaxCountObserved:I

    goto/32 :goto_1a

    nop

    :goto_1a
    iput-boolean p2, v0, Lcom/android/internal/widget/RecyclerView$LayoutManager;->mPrefetchMaxObservedInInitialPrefetch:Z

    goto/32 :goto_3

    nop

    :goto_1b
    iget-object v1, p1, Lcom/android/internal/widget/RecyclerView;->mAdapter:Lcom/android/internal/widget/RecyclerView$Adapter;

    goto/32 :goto_9

    nop

    :goto_1c
    if-nez v1, :cond_5

    goto/32 :goto_1f

    :cond_5
    goto/32 :goto_14

    nop

    :goto_1d
    iget-object v1, p1, Lcom/android/internal/widget/RecyclerView;->mAdapterHelper:Lcom/android/internal/widget/AdapterHelper;

    goto/32 :goto_f

    nop

    :goto_1e
    invoke-virtual {v1}, Lcom/android/internal/widget/RecyclerView$Recycler;->updateViewCacheSize()V

    :goto_1f
    goto/32 :goto_26

    nop

    :goto_20
    iget v1, p0, Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;->mCount:I

    goto/32 :goto_19

    nop

    :goto_21
    if-eqz v1, :cond_6

    goto/32 :goto_23

    :cond_6
    goto/32 :goto_0

    nop

    :goto_22
    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/android/internal/widget/RecyclerView$LayoutManager;->collectAdjacentPrefetchPositions(IILcom/android/internal/widget/RecyclerView$State;Lcom/android/internal/widget/RecyclerView$LayoutManager$LayoutPrefetchRegistry;)V

    :goto_23
    goto/32 :goto_c

    nop

    :goto_24
    if-eqz v1, :cond_7

    goto/32 :goto_23

    :cond_7
    goto/32 :goto_1b

    nop

    :goto_25
    iput v0, p0, Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;->mCount:I

    goto/32 :goto_1

    nop

    :goto_26
    return-void
.end method

.method lastPrefetchIncludedPosition(I)Z
    .locals 3

    goto/32 :goto_2

    nop

    :goto_0
    goto :goto_c

    :goto_1
    goto/32 :goto_f

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;->mPrefetchArray:[I

    goto/32 :goto_11

    nop

    :goto_3
    if-eq v2, p1, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_8

    nop

    :goto_4
    if-lt v1, v0, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_5

    nop

    :goto_5
    iget-object v2, p0, Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;->mPrefetchArray:[I

    goto/32 :goto_10

    nop

    :goto_6
    add-int/lit8 v1, v1, 0x2

    goto/32 :goto_0

    nop

    :goto_7
    mul-int/lit8 v0, v0, 0x2

    goto/32 :goto_b

    nop

    :goto_8
    const/4 v2, 0x1

    goto/32 :goto_9

    nop

    :goto_9
    return v2

    :goto_a
    goto/32 :goto_6

    nop

    :goto_b
    const/4 v1, 0x0

    :goto_c
    goto/32 :goto_4

    nop

    :goto_d
    return v0

    :goto_e
    iget v0, p0, Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;->mCount:I

    goto/32 :goto_7

    nop

    :goto_f
    const/4 v0, 0x0

    goto/32 :goto_d

    nop

    :goto_10
    aget v2, v2, v1

    goto/32 :goto_3

    nop

    :goto_11
    if-nez v0, :cond_2

    goto/32 :goto_1

    :cond_2
    goto/32 :goto_e

    nop
.end method

.method setPrefetchVector(II)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iput p1, p0, Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;->mPrefetchDx:I

    goto/32 :goto_1

    nop

    :goto_1
    iput p2, p0, Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;->mPrefetchDy:I

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method
