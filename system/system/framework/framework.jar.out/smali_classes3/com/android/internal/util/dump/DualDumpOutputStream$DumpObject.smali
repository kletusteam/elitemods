.class Lcom/android/internal/util/dump/DualDumpOutputStream$DumpObject;
.super Lcom/android/internal/util/dump/DualDumpOutputStream$Dumpable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/util/dump/DualDumpOutputStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DumpObject"
.end annotation


# instance fields
.field private final mSubObjects:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lcom/android/internal/util/dump/DualDumpOutputStream$Dumpable;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/internal/util/dump/DualDumpOutputStream$Dumpable;-><init>(Ljava/lang/String;Lcom/android/internal/util/dump/DualDumpOutputStream$Dumpable-IA;)V

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/internal/util/dump/DualDumpOutputStream$DumpObject;->mSubObjects:Ljava/util/LinkedHashMap;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lcom/android/internal/util/dump/DualDumpOutputStream$DumpObject-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/internal/util/dump/DualDumpOutputStream$DumpObject;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public add(Ljava/lang/String;Lcom/android/internal/util/dump/DualDumpOutputStream$Dumpable;)V
    .locals 3

    iget-object v0, p0, Lcom/android/internal/util/dump/DualDumpOutputStream$DumpObject;->mSubObjects:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    move-object v0, v1

    iget-object v1, p0, Lcom/android/internal/util/dump/DualDumpOutputStream$DumpObject;->mSubObjects:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method print(Landroid/util/IndentingPrintWriter;Z)V
    .locals 6

    goto/32 :goto_1a

    nop

    :goto_0
    check-cast v3, Lcom/android/internal/util/dump/DualDumpOutputStream$Dumpable;

    goto/32 :goto_3b

    nop

    :goto_1
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    goto/32 :goto_3c

    nop

    :goto_2
    invoke-virtual {p1, v0}, Landroid/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    goto/32 :goto_6

    nop

    :goto_3
    invoke-virtual {p1, v0}, Landroid/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    :goto_4
    goto/32 :goto_32

    nop

    :goto_5
    invoke-virtual {p1, v0}, Landroid/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    goto/32 :goto_26

    nop

    :goto_6
    return-void

    :goto_7
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_8
    const-string v5, "=["

    goto/32 :goto_2b

    nop

    :goto_9
    goto :goto_10

    :goto_a
    goto/32 :goto_21

    nop

    :goto_b
    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    goto/32 :goto_f

    nop

    :goto_c
    if-lt v4, v2, :cond_0

    goto/32 :goto_2d

    :cond_0
    goto/32 :goto_1

    nop

    :goto_d
    const-string v3, "]"

    goto/32 :goto_28

    nop

    :goto_e
    iget-object v0, p0, Lcom/android/internal/util/dump/DualDumpOutputStream$DumpObject;->mSubObjects:Ljava/util/LinkedHashMap;

    goto/32 :goto_b

    nop

    :goto_f
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_10
    goto/32 :goto_35

    nop

    :goto_11
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_34

    nop

    :goto_12
    invoke-virtual {v5, p1, v3}, Lcom/android/internal/util/dump/DualDumpOutputStream$Dumpable;->print(Landroid/util/IndentingPrintWriter;Z)V

    goto/32 :goto_3a

    nop

    :goto_13
    const/4 v3, 0x0

    goto/32 :goto_38

    nop

    :goto_14
    if-nez v1, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_22

    nop

    :goto_15
    const-string/jumbo v0, "{"

    goto/32 :goto_3

    nop

    :goto_16
    iget-object v1, p0, Lcom/android/internal/util/dump/DualDumpOutputStream$DumpObject;->name:Ljava/lang/String;

    goto/32 :goto_39

    nop

    :goto_17
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_2f

    nop

    :goto_18
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    goto/32 :goto_13

    nop

    :goto_19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_1a
    if-nez p2, :cond_2

    goto/32 :goto_27

    :cond_2
    goto/32 :goto_11

    nop

    :goto_1b
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_0

    nop

    :goto_1c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_8

    nop

    :goto_1d
    invoke-virtual {p1}, Landroid/util/IndentingPrintWriter;->decreaseIndent()Landroid/util/IndentingPrintWriter;

    goto/32 :goto_d

    nop

    :goto_1e
    invoke-virtual {p1, v4}, Landroid/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    goto/32 :goto_36

    nop

    :goto_1f
    const/4 v4, 0x0

    :goto_20
    goto/32 :goto_c

    nop

    :goto_21
    invoke-virtual {p1}, Landroid/util/IndentingPrintWriter;->decreaseIndent()Landroid/util/IndentingPrintWriter;

    goto/32 :goto_2e

    nop

    :goto_22
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_3d

    nop

    :goto_23
    iget-object v5, v5, Lcom/android/internal/util/dump/DualDumpOutputStream$Dumpable;->name:Ljava/lang/String;

    goto/32 :goto_1c

    nop

    :goto_24
    goto :goto_29

    :goto_25
    goto/32 :goto_30

    nop

    :goto_26
    goto/16 :goto_4

    :goto_27
    goto/32 :goto_15

    nop

    :goto_28
    invoke-virtual {p1, v3}, Landroid/util/IndentingPrintWriter;->println(Ljava/lang/String;)V

    :goto_29
    goto/32 :goto_9

    nop

    :goto_2a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_1e

    nop

    :goto_2b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_2a

    nop

    :goto_2c
    goto :goto_20

    :goto_2d
    goto/32 :goto_1d

    nop

    :goto_2e
    const-string/jumbo v0, "}"

    goto/32 :goto_2

    nop

    :goto_2f
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    goto/32 :goto_33

    nop

    :goto_30
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_17

    nop

    :goto_31
    const-string v1, "={"

    goto/32 :goto_19

    nop

    :goto_32
    invoke-virtual {p1}, Landroid/util/IndentingPrintWriter;->increaseIndent()Landroid/util/IndentingPrintWriter;

    goto/32 :goto_e

    nop

    :goto_33
    check-cast v5, Lcom/android/internal/util/dump/DualDumpOutputStream$Dumpable;

    goto/32 :goto_23

    nop

    :goto_34
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_16

    nop

    :goto_35
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_14

    nop

    :goto_36
    invoke-virtual {p1}, Landroid/util/IndentingPrintWriter;->increaseIndent()Landroid/util/IndentingPrintWriter;

    goto/32 :goto_1f

    nop

    :goto_37
    if-eq v2, v4, :cond_3

    goto/32 :goto_25

    :cond_3
    goto/32 :goto_1b

    nop

    :goto_38
    const/4 v4, 0x1

    goto/32 :goto_37

    nop

    :goto_39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_31

    nop

    :goto_3a
    add-int/lit8 v4, v4, 0x1

    goto/32 :goto_2c

    nop

    :goto_3b
    invoke-virtual {v3, p1, v4}, Lcom/android/internal/util/dump/DualDumpOutputStream$Dumpable;->print(Landroid/util/IndentingPrintWriter;Z)V

    goto/32 :goto_24

    nop

    :goto_3c
    check-cast v5, Lcom/android/internal/util/dump/DualDumpOutputStream$Dumpable;

    goto/32 :goto_12

    nop

    :goto_3d
    check-cast v1, Ljava/util/ArrayList;

    goto/32 :goto_18

    nop
.end method
