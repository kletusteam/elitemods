.class Lcom/android/internal/widget/ViewInfoStore;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/widget/ViewInfoStore$InfoRecord;,
        Lcom/android/internal/widget/ViewInfoStore$ProcessCallback;
    }
.end annotation


# static fields
.field private static final DEBUG:Z


# instance fields
.field final mLayoutHolderMap:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Lcom/android/internal/widget/RecyclerView$ViewHolder;",
            "Lcom/android/internal/widget/ViewInfoStore$InfoRecord;",
            ">;"
        }
    .end annotation
.end field

.field final mOldChangedHolders:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray<",
            "Lcom/android/internal/widget/RecyclerView$ViewHolder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/android/internal/widget/ViewInfoStore;->mLayoutHolderMap:Landroid/util/ArrayMap;

    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/internal/widget/ViewInfoStore;->mOldChangedHolders:Landroid/util/LongSparseArray;

    return-void
.end method

.method private popFromLayoutStep(Lcom/android/internal/widget/RecyclerView$ViewHolder;I)Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;
    .locals 4

    iget-object v0, p0, Lcom/android/internal/widget/ViewInfoStore;->mLayoutHolderMap:Landroid/util/ArrayMap;

    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->indexOfKey(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, 0x0

    if-gez v0, :cond_0

    return-object v1

    :cond_0
    iget-object v2, p0, Lcom/android/internal/widget/ViewInfoStore;->mLayoutHolderMap:Landroid/util/ArrayMap;

    invoke-virtual {v2, v0}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;

    if-eqz v2, :cond_4

    iget v3, v2, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->flags:I

    and-int/2addr v3, p2

    if-eqz v3, :cond_4

    iget v1, v2, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->flags:I

    not-int v3, p2

    and-int/2addr v1, v3

    iput v1, v2, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->flags:I

    const/4 v1, 0x4

    if-ne p2, v1, :cond_1

    iget-object v1, v2, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->preInfo:Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;

    goto :goto_0

    :cond_1
    const/16 v1, 0x8

    if-ne p2, v1, :cond_3

    iget-object v1, v2, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->postInfo:Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;

    :goto_0
    iget v3, v2, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->flags:I

    and-int/lit8 v3, v3, 0xc

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/android/internal/widget/ViewInfoStore;->mLayoutHolderMap:Landroid/util/ArrayMap;

    invoke-virtual {v3, v0}, Landroid/util/ArrayMap;->removeAt(I)Ljava/lang/Object;

    invoke-static {v2}, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->recycle(Lcom/android/internal/widget/ViewInfoStore$InfoRecord;)V

    :cond_2
    return-object v1

    :cond_3
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v3, "Must provide flag PRE or POST"

    invoke-direct {v1, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    return-object v1
.end method


# virtual methods
.method addToAppearedInPreLayoutHolders(Lcom/android/internal/widget/RecyclerView$ViewHolder;Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;)V
    .locals 2

    goto/32 :goto_8

    nop

    :goto_0
    or-int/lit8 v1, v1, 0x2

    goto/32 :goto_5

    nop

    :goto_1
    iget-object v1, p0, Lcom/android/internal/widget/ViewInfoStore;->mLayoutHolderMap:Landroid/util/ArrayMap;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {v1, p1, v0}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_3
    goto/32 :goto_4

    nop

    :goto_4
    iget v1, v0, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->flags:I

    goto/32 :goto_0

    nop

    :goto_5
    iput v1, v0, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->flags:I

    goto/32 :goto_7

    nop

    :goto_6
    invoke-static {}, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->obtain()Lcom/android/internal/widget/ViewInfoStore$InfoRecord;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_7
    iput-object p2, v0, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->preInfo:Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;

    goto/32 :goto_a

    nop

    :goto_8
    iget-object v0, p0, Lcom/android/internal/widget/ViewInfoStore;->mLayoutHolderMap:Landroid/util/ArrayMap;

    goto/32 :goto_9

    nop

    :goto_9
    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_b

    nop

    :goto_a
    return-void

    :goto_b
    check-cast v0, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;

    goto/32 :goto_c

    nop

    :goto_c
    if-eqz v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_6

    nop
.end method

.method addToDisappearedInLayout(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V
    .locals 2

    goto/32 :goto_a

    nop

    :goto_0
    iput v1, v0, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->flags:I

    goto/32 :goto_6

    nop

    :goto_1
    invoke-virtual {v1, p1, v0}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_2
    goto/32 :goto_4

    nop

    :goto_3
    if-eqz v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_7

    nop

    :goto_4
    iget v1, v0, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->flags:I

    goto/32 :goto_9

    nop

    :goto_5
    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_6
    return-void

    :goto_7
    invoke-static {}, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->obtain()Lcom/android/internal/widget/ViewInfoStore$InfoRecord;

    move-result-object v0

    goto/32 :goto_b

    nop

    :goto_8
    check-cast v0, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;

    goto/32 :goto_3

    nop

    :goto_9
    or-int/lit8 v1, v1, 0x1

    goto/32 :goto_0

    nop

    :goto_a
    iget-object v0, p0, Lcom/android/internal/widget/ViewInfoStore;->mLayoutHolderMap:Landroid/util/ArrayMap;

    goto/32 :goto_5

    nop

    :goto_b
    iget-object v1, p0, Lcom/android/internal/widget/ViewInfoStore;->mLayoutHolderMap:Landroid/util/ArrayMap;

    goto/32 :goto_1

    nop
.end method

.method addToOldChangeHolders(JLcom/android/internal/widget/RecyclerView$ViewHolder;)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {v0, p1, p2, p3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    iget-object v0, p0, Lcom/android/internal/widget/ViewInfoStore;->mOldChangedHolders:Landroid/util/LongSparseArray;

    goto/32 :goto_0

    nop
.end method

.method addToPostLayout(Lcom/android/internal/widget/RecyclerView$ViewHolder;Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;)V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/widget/ViewInfoStore;->mLayoutHolderMap:Landroid/util/ArrayMap;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_2
    invoke-virtual {v1, p1, v0}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_3
    goto/32 :goto_9

    nop

    :goto_4
    iput v1, v0, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->flags:I

    goto/32 :goto_5

    nop

    :goto_5
    return-void

    :goto_6
    if-eqz v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_c

    nop

    :goto_7
    or-int/lit8 v1, v1, 0x8

    goto/32 :goto_4

    nop

    :goto_8
    check-cast v0, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;

    goto/32 :goto_6

    nop

    :goto_9
    iput-object p2, v0, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->postInfo:Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;

    goto/32 :goto_b

    nop

    :goto_a
    iget-object v1, p0, Lcom/android/internal/widget/ViewInfoStore;->mLayoutHolderMap:Landroid/util/ArrayMap;

    goto/32 :goto_2

    nop

    :goto_b
    iget v1, v0, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->flags:I

    goto/32 :goto_7

    nop

    :goto_c
    invoke-static {}, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->obtain()Lcom/android/internal/widget/ViewInfoStore$InfoRecord;

    move-result-object v0

    goto/32 :goto_a

    nop
.end method

.method addToPreLayout(Lcom/android/internal/widget/RecyclerView$ViewHolder;Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;)V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/widget/ViewInfoStore;->mLayoutHolderMap:Landroid/util/ArrayMap;

    goto/32 :goto_2

    nop

    :goto_1
    invoke-static {}, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->obtain()Lcom/android/internal/widget/ViewInfoStore$InfoRecord;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_2
    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_3
    iget-object v1, p0, Lcom/android/internal/widget/ViewInfoStore;->mLayoutHolderMap:Landroid/util/ArrayMap;

    goto/32 :goto_4

    nop

    :goto_4
    invoke-virtual {v1, p1, v0}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_5
    goto/32 :goto_a

    nop

    :goto_6
    or-int/lit8 v1, v1, 0x4

    goto/32 :goto_7

    nop

    :goto_7
    iput v1, v0, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->flags:I

    goto/32 :goto_9

    nop

    :goto_8
    check-cast v0, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;

    goto/32 :goto_b

    nop

    :goto_9
    return-void

    :goto_a
    iput-object p2, v0, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->preInfo:Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;

    goto/32 :goto_c

    nop

    :goto_b
    if-eqz v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_1

    nop

    :goto_c
    iget v1, v0, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->flags:I

    goto/32 :goto_6

    nop
.end method

.method clear()V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/widget/ViewInfoStore;->mLayoutHolderMap:Landroid/util/ArrayMap;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {v0}, Landroid/util/ArrayMap;->clear()V

    goto/32 :goto_4

    nop

    :goto_2
    invoke-virtual {v0}, Landroid/util/LongSparseArray;->clear()V

    goto/32 :goto_3

    nop

    :goto_3
    return-void

    :goto_4
    iget-object v0, p0, Lcom/android/internal/widget/ViewInfoStore;->mOldChangedHolders:Landroid/util/LongSparseArray;

    goto/32 :goto_2

    nop
.end method

.method getFromOldChangeHolders(J)Lcom/android/internal/widget/RecyclerView$ViewHolder;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_1
    iget-object v0, p0, Lcom/android/internal/widget/ViewInfoStore;->mOldChangedHolders:Landroid/util/LongSparseArray;

    goto/32 :goto_0

    nop

    :goto_2
    return-object v0

    :goto_3
    check-cast v0, Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_2

    nop
.end method

.method isDisappearing(Lcom/android/internal/widget/RecyclerView$ViewHolder;)Z
    .locals 3

    goto/32 :goto_9

    nop

    :goto_0
    return v1

    :goto_1
    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_b

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_a

    nop

    :goto_3
    and-int/2addr v2, v1

    goto/32 :goto_c

    nop

    :goto_4
    goto :goto_8

    :goto_5
    goto/32 :goto_7

    nop

    :goto_6
    const/4 v1, 0x1

    goto/32 :goto_2

    nop

    :goto_7
    const/4 v1, 0x0

    :goto_8
    goto/32 :goto_0

    nop

    :goto_9
    iget-object v0, p0, Lcom/android/internal/widget/ViewInfoStore;->mLayoutHolderMap:Landroid/util/ArrayMap;

    goto/32 :goto_1

    nop

    :goto_a
    iget v2, v0, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->flags:I

    goto/32 :goto_3

    nop

    :goto_b
    check-cast v0, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;

    goto/32 :goto_6

    nop

    :goto_c
    if-nez v2, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_4

    nop
.end method

.method isInPreLayout(Lcom/android/internal/widget/RecyclerView$ViewHolder;)Z
    .locals 2

    goto/32 :goto_4

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_8

    nop

    :goto_1
    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_c

    nop

    :goto_2
    return v1

    :goto_3
    const/4 v1, 0x1

    goto/32 :goto_9

    nop

    :goto_4
    iget-object v0, p0, Lcom/android/internal/widget/ViewInfoStore;->mLayoutHolderMap:Landroid/util/ArrayMap;

    goto/32 :goto_1

    nop

    :goto_5
    if-nez v1, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_3

    nop

    :goto_6
    const/4 v1, 0x0

    :goto_7
    goto/32 :goto_2

    nop

    :goto_8
    iget v1, v0, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->flags:I

    goto/32 :goto_b

    nop

    :goto_9
    goto :goto_7

    :goto_a
    goto/32 :goto_6

    nop

    :goto_b
    and-int/lit8 v1, v1, 0x4

    goto/32 :goto_5

    nop

    :goto_c
    check-cast v0, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;

    goto/32 :goto_0

    nop
.end method

.method onDetach()V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-static {}, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->drainCache()V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method public onViewDetached(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/android/internal/widget/ViewInfoStore;->removeFromDisappearedInLayout(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V

    return-void
.end method

.method popFromPostLayout(Lcom/android/internal/widget/RecyclerView$ViewHolder;)Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    const/16 v0, 0x8

    goto/32 :goto_2

    nop

    :goto_2
    invoke-direct {p0, p1, v0}, Lcom/android/internal/widget/ViewInfoStore;->popFromLayoutStep(Lcom/android/internal/widget/RecyclerView$ViewHolder;I)Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;

    move-result-object v0

    goto/32 :goto_0

    nop
.end method

.method popFromPreLayout(Lcom/android/internal/widget/RecyclerView$ViewHolder;)Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x4

    goto/32 :goto_1

    nop

    :goto_1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/widget/ViewInfoStore;->popFromLayoutStep(Lcom/android/internal/widget/RecyclerView$ViewHolder;I)Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_2
    return-object v0
.end method

.method process(Lcom/android/internal/widget/ViewInfoStore$ProcessCallback;)V
    .locals 5

    goto/32 :goto_f

    nop

    :goto_0
    iget-object v4, v2, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->postInfo:Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;

    goto/32 :goto_c

    nop

    :goto_1
    if-nez v3, :cond_0

    goto/32 :goto_26

    :cond_0
    goto/32 :goto_37

    nop

    :goto_2
    iget v3, v2, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->flags:I

    goto/32 :goto_27

    nop

    :goto_3
    if-eqz v3, :cond_1

    goto/32 :goto_21

    :cond_1
    goto/32 :goto_7

    nop

    :goto_4
    and-int/lit8 v3, v3, 0x8

    goto/32 :goto_1

    nop

    :goto_5
    invoke-interface {p1, v1}, Lcom/android/internal/widget/ViewInfoStore$ProcessCallback;->unused(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_d

    nop

    :goto_6
    iget-object v3, v2, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->preInfo:Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;

    goto/32 :goto_45

    nop

    :goto_7
    invoke-interface {p1, v1}, Lcom/android/internal/widget/ViewInfoStore$ProcessCallback;->unused(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_20

    nop

    :goto_8
    and-int/lit8 v3, v3, 0x4

    goto/32 :goto_29

    nop

    :goto_9
    and-int/2addr v3, v4

    goto/32 :goto_2e

    nop

    :goto_a
    and-int/2addr v3, v4

    goto/32 :goto_32

    nop

    :goto_b
    iget-object v3, v2, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->preInfo:Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;

    goto/32 :goto_3

    nop

    :goto_c
    invoke-interface {p1, v1, v3, v4}, Lcom/android/internal/widget/ViewInfoStore$ProcessCallback;->processDisappeared(Lcom/android/internal/widget/RecyclerView$ViewHolder;Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;)V

    goto/32 :goto_30

    nop

    :goto_d
    goto/16 :goto_2d

    :goto_e
    goto/32 :goto_3f

    nop

    :goto_f
    iget-object v0, p0, Lcom/android/internal/widget/ViewInfoStore;->mLayoutHolderMap:Landroid/util/ArrayMap;

    goto/32 :goto_2f

    nop

    :goto_10
    invoke-interface {p1, v1, v3, v4}, Lcom/android/internal/widget/ViewInfoStore$ProcessCallback;->processPersistent(Lcom/android/internal/widget/RecyclerView$ViewHolder;Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;)V

    goto/32 :goto_46

    nop

    :goto_11
    invoke-virtual {v2, v0}, Landroid/util/ArrayMap;->removeAt(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_16

    nop

    :goto_12
    iget v3, v2, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->flags:I

    goto/32 :goto_4

    nop

    :goto_13
    invoke-static {v2}, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->recycle(Lcom/android/internal/widget/ViewInfoStore$InfoRecord;)V

    goto/32 :goto_1a

    nop

    :goto_14
    iget v3, v2, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->flags:I

    goto/32 :goto_17

    nop

    :goto_15
    iget v3, v2, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->flags:I

    goto/32 :goto_1f

    nop

    :goto_16
    check-cast v2, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;

    goto/32 :goto_2

    nop

    :goto_17
    const/16 v4, 0xe

    goto/32 :goto_9

    nop

    :goto_18
    goto/16 :goto_43

    :goto_19
    goto/32 :goto_2a

    nop

    :goto_1a
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_18

    nop

    :goto_1b
    iget-object v3, v2, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->preInfo:Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;

    goto/32 :goto_3c

    nop

    :goto_1c
    iget-object v3, v2, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->preInfo:Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;

    goto/32 :goto_1d

    nop

    :goto_1d
    const/4 v4, 0x0

    goto/32 :goto_39

    nop

    :goto_1e
    iget-object v4, v2, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->postInfo:Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;

    goto/32 :goto_28

    nop

    :goto_1f
    const/16 v4, 0xc

    goto/32 :goto_3d

    nop

    :goto_20
    goto :goto_2d

    :goto_21
    goto/32 :goto_3a

    nop

    :goto_22
    invoke-interface {p1, v1, v3, v4}, Lcom/android/internal/widget/ViewInfoStore$ProcessCallback;->processAppeared(Lcom/android/internal/widget/RecyclerView$ViewHolder;Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;)V

    goto/32 :goto_23

    nop

    :goto_23
    goto :goto_2d

    :goto_24
    goto/32 :goto_15

    nop

    :goto_25
    goto :goto_2d

    :goto_26
    goto/32 :goto_2c

    nop

    :goto_27
    const/4 v4, 0x3

    goto/32 :goto_a

    nop

    :goto_28
    invoke-interface {p1, v1, v3, v4}, Lcom/android/internal/widget/ViewInfoStore$ProcessCallback;->processAppeared(Lcom/android/internal/widget/RecyclerView$ViewHolder;Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;)V

    goto/32 :goto_25

    nop

    :goto_29
    if-nez v3, :cond_2

    goto/32 :goto_35

    :cond_2
    goto/32 :goto_1c

    nop

    :goto_2a
    return-void

    :goto_2b
    iget v3, v2, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->flags:I

    goto/32 :goto_8

    nop

    :goto_2c
    iget v3, v2, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->flags:I

    :goto_2d
    goto/32 :goto_13

    nop

    :goto_2e
    if-eq v3, v4, :cond_3

    goto/32 :goto_24

    :cond_3
    goto/32 :goto_1b

    nop

    :goto_2f
    invoke-virtual {v0}, Landroid/util/ArrayMap;->size()I

    move-result v0

    goto/32 :goto_42

    nop

    :goto_30
    goto :goto_2d

    :goto_31
    goto/32 :goto_14

    nop

    :goto_32
    if-eq v3, v4, :cond_4

    goto/32 :goto_e

    :cond_4
    goto/32 :goto_5

    nop

    :goto_33
    check-cast v1, Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_36

    nop

    :goto_34
    goto :goto_2d

    :goto_35
    goto/32 :goto_12

    nop

    :goto_36
    iget-object v2, p0, Lcom/android/internal/widget/ViewInfoStore;->mLayoutHolderMap:Landroid/util/ArrayMap;

    goto/32 :goto_11

    nop

    :goto_37
    iget-object v3, v2, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->preInfo:Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;

    goto/32 :goto_1e

    nop

    :goto_38
    if-eq v3, v4, :cond_5

    goto/32 :goto_47

    :cond_5
    goto/32 :goto_6

    nop

    :goto_39
    invoke-interface {p1, v1, v3, v4}, Lcom/android/internal/widget/ViewInfoStore$ProcessCallback;->processDisappeared(Lcom/android/internal/widget/RecyclerView$ViewHolder;Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;)V

    goto/32 :goto_34

    nop

    :goto_3a
    iget-object v3, v2, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->preInfo:Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;

    goto/32 :goto_0

    nop

    :goto_3b
    invoke-virtual {v1, v0}, Landroid/util/ArrayMap;->keyAt(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_33

    nop

    :goto_3c
    iget-object v4, v2, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->postInfo:Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;

    goto/32 :goto_22

    nop

    :goto_3d
    and-int/2addr v3, v4

    goto/32 :goto_38

    nop

    :goto_3e
    if-gez v0, :cond_6

    goto/32 :goto_19

    :cond_6
    goto/32 :goto_40

    nop

    :goto_3f
    iget v3, v2, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->flags:I

    goto/32 :goto_41

    nop

    :goto_40
    iget-object v1, p0, Lcom/android/internal/widget/ViewInfoStore;->mLayoutHolderMap:Landroid/util/ArrayMap;

    goto/32 :goto_3b

    nop

    :goto_41
    and-int/lit8 v3, v3, 0x1

    goto/32 :goto_44

    nop

    :goto_42
    add-int/lit8 v0, v0, -0x1

    :goto_43
    goto/32 :goto_3e

    nop

    :goto_44
    if-nez v3, :cond_7

    goto/32 :goto_31

    :cond_7
    goto/32 :goto_b

    nop

    :goto_45
    iget-object v4, v2, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->postInfo:Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;

    goto/32 :goto_10

    nop

    :goto_46
    goto/16 :goto_2d

    :goto_47
    goto/32 :goto_2b

    nop
.end method

.method removeFromDisappearedInLayout(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V
    .locals 2

    goto/32 :goto_6

    nop

    :goto_0
    iget v1, v0, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->flags:I

    goto/32 :goto_1

    nop

    :goto_1
    and-int/lit8 v1, v1, -0x2

    goto/32 :goto_2

    nop

    :goto_2
    iput v1, v0, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->flags:I

    goto/32 :goto_7

    nop

    :goto_3
    return-void

    :goto_4
    goto/32 :goto_0

    nop

    :goto_5
    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_6
    iget-object v0, p0, Lcom/android/internal/widget/ViewInfoStore;->mLayoutHolderMap:Landroid/util/ArrayMap;

    goto/32 :goto_5

    nop

    :goto_7
    return-void

    :goto_8
    check-cast v0, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;

    goto/32 :goto_9

    nop

    :goto_9
    if-eqz v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_3

    nop
.end method

.method removeViewHolder(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V
    .locals 2

    goto/32 :goto_15

    nop

    :goto_0
    invoke-virtual {v1, v0}, Landroid/util/LongSparseArray;->removeAt(I)V

    goto/32 :goto_9

    nop

    :goto_1
    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    goto/32 :goto_2

    nop

    :goto_2
    add-int/lit8 v0, v0, -0x1

    :goto_3
    goto/32 :goto_5

    nop

    :goto_4
    invoke-virtual {v1, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_5
    if-gez v0, :cond_0

    goto/32 :goto_11

    :cond_0
    goto/32 :goto_6

    nop

    :goto_6
    iget-object v1, p0, Lcom/android/internal/widget/ViewInfoStore;->mOldChangedHolders:Landroid/util/LongSparseArray;

    goto/32 :goto_4

    nop

    :goto_7
    iget-object v1, p0, Lcom/android/internal/widget/ViewInfoStore;->mOldChangedHolders:Landroid/util/LongSparseArray;

    goto/32 :goto_0

    nop

    :goto_8
    if-eq p1, v1, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_7

    nop

    :goto_9
    goto :goto_11

    :goto_a
    goto/32 :goto_13

    nop

    :goto_b
    return-void

    :goto_c
    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_d

    nop

    :goto_d
    check-cast v0, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;

    goto/32 :goto_12

    nop

    :goto_e
    invoke-static {v0}, Lcom/android/internal/widget/ViewInfoStore$InfoRecord;->recycle(Lcom/android/internal/widget/ViewInfoStore$InfoRecord;)V

    :goto_f
    goto/32 :goto_b

    nop

    :goto_10
    goto :goto_3

    :goto_11
    goto/32 :goto_14

    nop

    :goto_12
    if-nez v0, :cond_2

    goto/32 :goto_f

    :cond_2
    goto/32 :goto_e

    nop

    :goto_13
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_10

    nop

    :goto_14
    iget-object v0, p0, Lcom/android/internal/widget/ViewInfoStore;->mLayoutHolderMap:Landroid/util/ArrayMap;

    goto/32 :goto_c

    nop

    :goto_15
    iget-object v0, p0, Lcom/android/internal/widget/ViewInfoStore;->mOldChangedHolders:Landroid/util/LongSparseArray;

    goto/32 :goto_1

    nop
.end method
