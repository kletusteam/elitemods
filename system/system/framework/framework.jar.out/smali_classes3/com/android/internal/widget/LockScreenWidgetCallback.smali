.class public interface abstract Lcom/android/internal/widget/LockScreenWidgetCallback;
.super Ljava/lang/Object;


# virtual methods
.method public abstract isVisible(Landroid/view/View;)Z
.end method

.method public abstract requestHide(Landroid/view/View;)V
.end method

.method public abstract requestShow(Landroid/view/View;)V
.end method

.method public abstract userActivity(Landroid/view/View;)V
.end method
