.class Lcom/android/internal/widget/OpReorderer;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/widget/OpReorderer$Callback;
    }
.end annotation


# instance fields
.field final mCallback:Lcom/android/internal/widget/OpReorderer$Callback;


# direct methods
.method constructor <init>(Lcom/android/internal/widget/OpReorderer$Callback;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/internal/widget/OpReorderer;->mCallback:Lcom/android/internal/widget/OpReorderer$Callback;

    return-void
.end method

.method private getLastMoveOutOfOrder(Ljava/util/List;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/internal/widget/AdapterHelper$UpdateOp;",
            ">;)I"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ltz v1, :cond_2

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/internal/widget/AdapterHelper$UpdateOp;

    iget v3, v2, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->cmd:I

    const/16 v4, 0x8

    if-ne v3, v4, :cond_0

    if-eqz v0, :cond_1

    return v1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_2
    const/4 v1, -0x1

    return v1
.end method

.method private swapMoveAdd(Ljava/util/List;ILcom/android/internal/widget/AdapterHelper$UpdateOp;ILcom/android/internal/widget/AdapterHelper$UpdateOp;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/internal/widget/AdapterHelper$UpdateOp;",
            ">;I",
            "Lcom/android/internal/widget/AdapterHelper$UpdateOp;",
            "I",
            "Lcom/android/internal/widget/AdapterHelper$UpdateOp;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    iget v1, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    iget v2, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    if-ge v1, v2, :cond_0

    add-int/lit8 v0, v0, -0x1

    :cond_0
    iget v1, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    iget v2, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    if-ge v1, v2, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_1
    iget v1, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    iget v2, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    if-gt v1, v2, :cond_2

    iget v1, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    iget v2, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    add-int/2addr v1, v2

    iput v1, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    :cond_2
    iget v1, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    iget v2, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    if-gt v1, v2, :cond_3

    iget v1, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    iget v2, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    add-int/2addr v1, v2

    iput v1, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    :cond_3
    iget v1, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    add-int/2addr v1, v0

    iput v1, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    invoke-interface {p1, p2, p5}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1, p4, p3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private swapMoveOp(Ljava/util/List;II)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/internal/widget/AdapterHelper$UpdateOp;",
            ">;II)V"
        }
    .end annotation

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/widget/AdapterHelper$UpdateOp;

    invoke-interface {p1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/android/internal/widget/AdapterHelper$UpdateOp;

    iget v1, v7, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->cmd:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, v0

    move v5, p3

    move-object v6, v7

    invoke-virtual/range {v1 .. v6}, Lcom/android/internal/widget/OpReorderer;->swapMoveUpdate(Ljava/util/List;ILcom/android/internal/widget/AdapterHelper$UpdateOp;ILcom/android/internal/widget/AdapterHelper$UpdateOp;)V

    goto :goto_0

    :pswitch_2
    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, v0

    move v5, p3

    move-object v6, v7

    invoke-virtual/range {v1 .. v6}, Lcom/android/internal/widget/OpReorderer;->swapMoveRemove(Ljava/util/List;ILcom/android/internal/widget/AdapterHelper$UpdateOp;ILcom/android/internal/widget/AdapterHelper$UpdateOp;)V

    goto :goto_0

    :pswitch_3
    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, v0

    move v5, p3

    move-object v6, v7

    invoke-direct/range {v1 .. v6}, Lcom/android/internal/widget/OpReorderer;->swapMoveAdd(Ljava/util/List;ILcom/android/internal/widget/AdapterHelper$UpdateOp;ILcom/android/internal/widget/AdapterHelper$UpdateOp;)V

    nop

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method reorderOps(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/internal/widget/AdapterHelper$UpdateOp;",
            ">;)V"
        }
    .end annotation

    :goto_0
    goto/32 :goto_4

    nop

    :goto_1
    add-int/lit8 v0, v1, 0x1

    goto/32 :goto_8

    nop

    :goto_2
    return-void

    :goto_3
    move v1, v0

    goto/32 :goto_9

    nop

    :goto_4
    invoke-direct {p0, p1}, Lcom/android/internal/widget/OpReorderer;->getLastMoveOutOfOrder(Ljava/util/List;)I

    move-result v0

    goto/32 :goto_3

    nop

    :goto_5
    goto :goto_0

    :goto_6
    goto/32 :goto_2

    nop

    :goto_7
    if-ne v0, v2, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_1

    nop

    :goto_8
    invoke-direct {p0, p1, v1, v0}, Lcom/android/internal/widget/OpReorderer;->swapMoveOp(Ljava/util/List;II)V

    goto/32 :goto_5

    nop

    :goto_9
    const/4 v2, -0x1

    goto/32 :goto_7

    nop
.end method

.method swapMoveRemove(Ljava/util/List;ILcom/android/internal/widget/AdapterHelper$UpdateOp;ILcom/android/internal/widget/AdapterHelper$UpdateOp;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/internal/widget/AdapterHelper$UpdateOp;",
            ">;I",
            "Lcom/android/internal/widget/AdapterHelper$UpdateOp;",
            "I",
            "Lcom/android/internal/widget/AdapterHelper$UpdateOp;",
            ")V"
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    if-lt v2, v3, :cond_0

    goto/32 :goto_4e

    :cond_0
    goto/32 :goto_21

    nop

    :goto_1
    iget v3, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_4

    nop

    :goto_2
    const/4 v0, 0x0

    goto/32 :goto_28

    nop

    :goto_3
    add-int/2addr v5, v7

    goto/32 :goto_5f

    nop

    :goto_4
    iget v5, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_7

    nop

    :goto_5
    iget v4, v0, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_81

    nop

    :goto_6
    iget v3, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_2b

    nop

    :goto_7
    add-int/2addr v3, v5

    goto/32 :goto_26

    nop

    :goto_8
    if-ne v3, v4, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_99

    nop

    :goto_9
    if-gt v3, v4, :cond_2

    goto/32 :goto_17

    :cond_2
    goto/32 :goto_5e

    nop

    :goto_a
    invoke-interface {p1, p4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto/32 :goto_a2

    nop

    :goto_b
    goto/16 :goto_41

    :goto_c
    goto/32 :goto_40

    nop

    :goto_d
    if-ge v3, v4, :cond_3

    goto/32 :goto_6a

    :cond_3
    goto/32 :goto_47

    nop

    :goto_e
    if-eq v3, v5, :cond_4

    goto/32 :goto_8f

    :cond_4
    goto/32 :goto_a7

    nop

    :goto_f
    if-nez v0, :cond_5

    goto/32 :goto_30

    :cond_5
    goto/32 :goto_2f

    nop

    :goto_10
    iget v3, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_34

    nop

    :goto_11
    if-eq v3, v5, :cond_6

    goto/32 :goto_8f

    :cond_6
    goto/32 :goto_33

    nop

    :goto_12
    iget v4, v0, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_7b

    nop

    :goto_13
    sub-int/2addr v5, v6

    goto/32 :goto_11

    nop

    :goto_14
    iget v2, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_15

    nop

    :goto_15
    iget v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_2d

    nop

    :goto_16
    iput v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    :goto_17
    goto/32 :goto_52

    nop

    :goto_18
    return-void

    :goto_19
    goto/32 :goto_96

    nop

    :goto_1a
    iget v5, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_3b

    nop

    :goto_1b
    iget v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_9e

    nop

    :goto_1c
    iget v6, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_98

    nop

    :goto_1d
    if-ge v3, v4, :cond_7

    goto/32 :goto_7d

    :cond_7
    goto/32 :goto_a5

    nop

    :goto_1e
    if-lt v3, v5, :cond_8

    goto/32 :goto_7f

    :cond_8
    goto/32 :goto_1

    nop

    :goto_1f
    goto/16 :goto_3f

    :goto_20
    goto/32 :goto_6f

    nop

    :goto_21
    const/4 v2, 0x0

    goto/32 :goto_9b

    nop

    :goto_22
    iput v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    :goto_23
    goto/32 :goto_1b

    nop

    :goto_24
    iget v5, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_57

    nop

    :goto_25
    iget v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_80

    nop

    :goto_26
    iget v5, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_4c

    nop

    :goto_27
    if-gt v3, v4, :cond_9

    goto/32 :goto_23

    :cond_9
    goto/32 :goto_77

    nop

    :goto_28
    const/4 v1, 0x0

    goto/32 :goto_14

    nop

    :goto_29
    iget v4, v0, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_9

    nop

    :goto_2a
    iget v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_7a

    nop

    :goto_2b
    iget v5, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_1c

    nop

    :goto_2c
    iget v4, v0, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_97

    nop

    :goto_2d
    const/4 v4, 0x1

    goto/32 :goto_0

    nop

    :goto_2e
    iget v5, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_94

    nop

    :goto_2f
    invoke-interface {p1, p2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :goto_30
    goto/32 :goto_89

    nop

    :goto_31
    invoke-interface {v5, v6, v7, v3, v4}, Lcom/android/internal/widget/OpReorderer$Callback;->obtainUpdateOp(IIILjava/lang/Object;)Lcom/android/internal/widget/AdapterHelper$UpdateOp;

    move-result-object v0

    goto/32 :goto_68

    nop

    :goto_32
    if-eq v3, v5, :cond_a

    goto/32 :goto_8f

    :cond_a
    goto/32 :goto_6

    nop

    :goto_33
    const/4 v1, 0x1

    goto/32 :goto_4d

    nop

    :goto_34
    add-int/2addr v3, v4

    goto/32 :goto_49

    nop

    :goto_35
    iget v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_50

    nop

    :goto_36
    iput v3, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_a3

    nop

    :goto_37
    goto/16 :goto_6a

    :goto_38
    goto/32 :goto_44

    nop

    :goto_39
    iget v5, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_9c

    nop

    :goto_3a
    iget v3, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_95

    nop

    :goto_3b
    add-int/2addr v5, v4

    goto/32 :goto_32

    nop

    :goto_3c
    iget v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_5

    nop

    :goto_3d
    iput v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_37

    nop

    :goto_3e
    return-void

    :goto_3f
    goto/32 :goto_56

    nop

    :goto_40
    invoke-interface {p1, p4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :goto_41
    goto/32 :goto_f

    nop

    :goto_42
    const/4 v6, 0x2

    goto/32 :goto_78

    nop

    :goto_43
    iget v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_45

    nop

    :goto_44
    if-nez v0, :cond_b

    goto/32 :goto_65

    :cond_b
    goto/32 :goto_25

    nop

    :goto_45
    iget v5, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_42

    nop

    :goto_46
    iget-object v5, p0, Lcom/android/internal/widget/OpReorderer;->mCallback:Lcom/android/internal/widget/OpReorderer$Callback;

    goto/32 :goto_5c

    nop

    :goto_47
    iget v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_6d

    nop

    :goto_48
    iget v3, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_79

    nop

    :goto_49
    iput v3, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_90

    nop

    :goto_4a
    iput v3, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_1f

    nop

    :goto_4b
    invoke-interface {p1, p2, p5}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_35

    nop

    :goto_4c
    sub-int/2addr v3, v5

    goto/32 :goto_46

    nop

    :goto_4d
    goto/16 :goto_8f

    :goto_4e
    goto/32 :goto_a0

    nop

    :goto_4f
    iget v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_58

    nop

    :goto_50
    iget v4, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_8

    nop

    :goto_51
    if-ge v3, v4, :cond_c

    goto/32 :goto_86

    :cond_c
    goto/32 :goto_4f

    nop

    :goto_52
    iget v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_67

    nop

    :goto_53
    iget v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_12

    nop

    :goto_54
    iget v3, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_5b

    nop

    :goto_55
    iget-object v3, p0, Lcom/android/internal/widget/OpReorderer;->mCallback:Lcom/android/internal/widget/OpReorderer$Callback;

    goto/32 :goto_6b

    nop

    :goto_56
    iget v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_2e

    nop

    :goto_57
    iget v7, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_3

    nop

    :goto_58
    iget v4, v0, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_aa

    nop

    :goto_59
    iget v4, v0, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_75

    nop

    :goto_5a
    if-nez v1, :cond_d

    goto/32 :goto_19

    :cond_d
    goto/32 :goto_70

    nop

    :goto_5b
    sub-int/2addr v3, v4

    goto/32 :goto_36

    nop

    :goto_5c
    iget v7, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_60

    nop

    :goto_5d
    iget v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_66

    nop

    :goto_5e
    iget v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_2c

    nop

    :goto_5f
    if-lt v3, v5, :cond_e

    goto/32 :goto_3f

    :cond_e
    goto/32 :goto_54

    nop

    :goto_60
    add-int/2addr v7, v4

    goto/32 :goto_8c

    nop

    :goto_61
    iget v4, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_9a

    nop

    :goto_62
    iget v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_82

    nop

    :goto_63
    invoke-interface {p1, p4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto/32 :goto_55

    nop

    :goto_64
    iput v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    :goto_65
    goto/32 :goto_72

    nop

    :goto_66
    iget v4, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_ab

    nop

    :goto_67
    iget v4, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_27

    nop

    :goto_68
    iget v4, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_8a

    nop

    :goto_69
    iput v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    :goto_6a
    goto/32 :goto_4b

    nop

    :goto_6b
    invoke-interface {v3, p3}, Lcom/android/internal/widget/OpReorderer$Callback;->recycleUpdateOp(Lcom/android/internal/widget/AdapterHelper$UpdateOp;)V

    goto/32 :goto_18

    nop

    :goto_6c
    if-eq v3, v5, :cond_f

    goto/32 :goto_8f

    :cond_f
    goto/32 :goto_8e

    nop

    :goto_6d
    iget v4, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_87

    nop

    :goto_6e
    iget v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_29

    nop

    :goto_6f
    iget v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_24

    nop

    :goto_70
    invoke-interface {p1, p2, p5}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_63

    nop

    :goto_71
    iget v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_59

    nop

    :goto_72
    iget v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_92

    nop

    :goto_73
    iget v4, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_88

    nop

    :goto_74
    iget v5, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_a1

    nop

    :goto_75
    if-gt v3, v4, :cond_10

    goto/32 :goto_84

    :cond_10
    goto/32 :goto_62

    nop

    :goto_76
    iget v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_74

    nop

    :goto_77
    iget v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_61

    nop

    :goto_78
    if-lt v3, v5, :cond_11

    goto/32 :goto_20

    :cond_11
    goto/32 :goto_3a

    nop

    :goto_79
    if-eqz v3, :cond_12

    goto/32 :goto_a9

    :cond_12
    goto/32 :goto_a

    nop

    :goto_7a
    iget v4, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_d

    nop

    :goto_7b
    if-ge v3, v4, :cond_13

    goto/32 :goto_65

    :cond_13
    goto/32 :goto_3c

    nop

    :goto_7c
    iput v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    :goto_7d
    goto/32 :goto_2a

    nop

    :goto_7e
    iput v4, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    :goto_7f
    goto/32 :goto_5a

    nop

    :goto_80
    iget v4, v0, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_51

    nop

    :goto_81
    sub-int/2addr v3, v4

    goto/32 :goto_64

    nop

    :goto_82
    iget v4, v0, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_9f

    nop

    :goto_83
    iput v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    :goto_84
    goto/32 :goto_6e

    nop

    :goto_85
    iput v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    :goto_86
    goto/32 :goto_53

    nop

    :goto_87
    sub-int/2addr v3, v4

    goto/32 :goto_69

    nop

    :goto_88
    sub-int/2addr v3, v4

    goto/32 :goto_7c

    nop

    :goto_89
    return-void

    :goto_8a
    iget v5, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_8b

    nop

    :goto_8b
    sub-int/2addr v4, v5

    goto/32 :goto_7e

    nop

    :goto_8c
    const/4 v4, 0x0

    goto/32 :goto_31

    nop

    :goto_8d
    add-int/2addr v5, v7

    goto/32 :goto_1e

    nop

    :goto_8e
    const/4 v1, 0x1

    :goto_8f
    goto/32 :goto_43

    nop

    :goto_90
    goto :goto_7f

    :goto_91
    goto/32 :goto_76

    nop

    :goto_92
    iget v4, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_1d

    nop

    :goto_93
    iget v3, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_1a

    nop

    :goto_94
    if-le v3, v5, :cond_14

    goto/32 :goto_91

    :cond_14
    goto/32 :goto_10

    nop

    :goto_95
    sub-int/2addr v3, v4

    goto/32 :goto_4a

    nop

    :goto_96
    if-nez v2, :cond_15

    goto/32 :goto_38

    :cond_15
    goto/32 :goto_a4

    nop

    :goto_97
    sub-int/2addr v3, v4

    goto/32 :goto_16

    nop

    :goto_98
    sub-int/2addr v5, v6

    goto/32 :goto_6c

    nop

    :goto_99
    invoke-interface {p1, p4, p3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_b

    nop

    :goto_9a
    sub-int/2addr v3, v4

    goto/32 :goto_22

    nop

    :goto_9b
    iget v3, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_9d

    nop

    :goto_9c
    iget v6, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_13

    nop

    :goto_9d
    iget v5, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_e

    nop

    :goto_9e
    iget v4, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_a6

    nop

    :goto_9f
    sub-int/2addr v3, v4

    goto/32 :goto_83

    nop

    :goto_a0
    const/4 v2, 0x1

    goto/32 :goto_93

    nop

    :goto_a1
    iget v7, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_8d

    nop

    :goto_a2
    iget-object v3, p0, Lcom/android/internal/widget/OpReorderer;->mCallback:Lcom/android/internal/widget/OpReorderer$Callback;

    goto/32 :goto_a8

    nop

    :goto_a3
    iput v6, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->cmd:I

    goto/32 :goto_ac

    nop

    :goto_a4
    if-nez v0, :cond_16

    goto/32 :goto_17

    :cond_16
    goto/32 :goto_71

    nop

    :goto_a5
    iget v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_73

    nop

    :goto_a6
    if-gt v3, v4, :cond_17

    goto/32 :goto_6a

    :cond_17
    goto/32 :goto_5d

    nop

    :goto_a7
    iget v3, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_39

    nop

    :goto_a8
    invoke-interface {v3, p5}, Lcom/android/internal/widget/OpReorderer$Callback;->recycleUpdateOp(Lcom/android/internal/widget/AdapterHelper$UpdateOp;)V

    :goto_a9
    goto/32 :goto_3e

    nop

    :goto_aa
    sub-int/2addr v3, v4

    goto/32 :goto_85

    nop

    :goto_ab
    sub-int/2addr v3, v4

    goto/32 :goto_3d

    nop

    :goto_ac
    iput v4, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_48

    nop
.end method

.method swapMoveUpdate(Ljava/util/List;ILcom/android/internal/widget/AdapterHelper$UpdateOp;ILcom/android/internal/widget/AdapterHelper$UpdateOp;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/internal/widget/AdapterHelper$UpdateOp;",
            ">;I",
            "Lcom/android/internal/widget/AdapterHelper$UpdateOp;",
            "I",
            "Lcom/android/internal/widget/AdapterHelper$UpdateOp;",
            ")V"
        }
    .end annotation

    goto/32 :goto_34

    nop

    :goto_0
    add-int/2addr v2, v3

    goto/32 :goto_43

    nop

    :goto_1
    iget-object v2, p0, Lcom/android/internal/widget/OpReorderer;->mCallback:Lcom/android/internal/widget/OpReorderer$Callback;

    goto/32 :goto_44

    nop

    :goto_2
    iget v2, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_12

    nop

    :goto_3
    iget v2, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_2c

    nop

    :goto_4
    const/4 v5, 0x1

    goto/32 :goto_18

    nop

    :goto_5
    iput v2, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_2a

    nop

    :goto_6
    if-gtz v2, :cond_0

    goto/32 :goto_1b

    :cond_0
    goto/32 :goto_1f

    nop

    :goto_7
    invoke-interface {v3, v4, v6, v2, v5}, Lcom/android/internal/widget/OpReorderer$Callback;->obtainUpdateOp(IIILjava/lang/Object;)Lcom/android/internal/widget/AdapterHelper$UpdateOp;

    move-result-object v1

    goto/32 :goto_36

    nop

    :goto_8
    iget v2, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_3a

    nop

    :goto_9
    iget-object v6, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->payload:Ljava/lang/Object;

    goto/32 :goto_15

    nop

    :goto_a
    iget-object v2, p0, Lcom/android/internal/widget/OpReorderer;->mCallback:Lcom/android/internal/widget/OpReorderer$Callback;

    goto/32 :goto_1c

    nop

    :goto_b
    iget v3, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_30

    nop

    :goto_c
    iget-object v3, p0, Lcom/android/internal/widget/OpReorderer;->mCallback:Lcom/android/internal/widget/OpReorderer$Callback;

    goto/32 :goto_25

    nop

    :goto_d
    add-int/2addr v3, v6

    goto/32 :goto_3f

    nop

    :goto_e
    add-int/2addr v6, v5

    goto/32 :goto_17

    nop

    :goto_f
    iget v2, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_6

    nop

    :goto_10
    iput v2, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_1

    nop

    :goto_11
    return-void

    :goto_12
    iget v3, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_32

    nop

    :goto_13
    iget v2, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_24

    nop

    :goto_14
    iget v2, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_41

    nop

    :goto_15
    invoke-interface {v2, v4, v3, v5, v6}, Lcom/android/internal/widget/OpReorderer$Callback;->obtainUpdateOp(IIILjava/lang/Object;)Lcom/android/internal/widget/AdapterHelper$UpdateOp;

    move-result-object v0

    :goto_16
    goto/32 :goto_28

    nop

    :goto_17
    iget-object v5, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->payload:Ljava/lang/Object;

    goto/32 :goto_7

    nop

    :goto_18
    if-lt v2, v3, :cond_1

    goto/32 :goto_3c

    :cond_1
    goto/32 :goto_14

    nop

    :goto_19
    invoke-interface {p1, p4, p3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_f

    nop

    :goto_1a
    goto :goto_1d

    :goto_1b
    goto/32 :goto_3e

    nop

    :goto_1c
    invoke-interface {v2, p5}, Lcom/android/internal/widget/OpReorderer$Callback;->recycleUpdateOp(Lcom/android/internal/widget/AdapterHelper$UpdateOp;)V

    :goto_1d
    goto/32 :goto_33

    nop

    :goto_1e
    iget v3, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_21

    nop

    :goto_1f
    invoke-interface {p1, p2, p5}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_1a

    nop

    :goto_20
    iget v6, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_d

    nop

    :goto_21
    if-le v2, v3, :cond_2

    goto/32 :goto_2b

    :cond_2
    goto/32 :goto_8

    nop

    :goto_22
    invoke-interface {p1, p2, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :goto_23
    goto/32 :goto_11

    nop

    :goto_24
    sub-int/2addr v2, v5

    goto/32 :goto_10

    nop

    :goto_25
    iget v6, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_e

    nop

    :goto_26
    iput v2, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_3b

    nop

    :goto_27
    sub-int/2addr v2, v3

    goto/32 :goto_c

    nop

    :goto_28
    iget v2, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_1e

    nop

    :goto_29
    if-lt v2, v3, :cond_3

    goto/32 :goto_16

    :cond_3
    goto/32 :goto_13

    nop

    :goto_2a
    goto :goto_2f

    :goto_2b
    goto/32 :goto_39

    nop

    :goto_2c
    iget v3, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_0

    nop

    :goto_2d
    const/4 v1, 0x0

    goto/32 :goto_31

    nop

    :goto_2e
    iput v3, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    :goto_2f
    goto/32 :goto_19

    nop

    :goto_30
    const/4 v4, 0x4

    goto/32 :goto_4

    nop

    :goto_31
    iget v2, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_b

    nop

    :goto_32
    iget v6, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_40

    nop

    :goto_33
    if-nez v0, :cond_4

    goto/32 :goto_38

    :cond_4
    goto/32 :goto_37

    nop

    :goto_34
    const/4 v0, 0x0

    goto/32 :goto_2d

    nop

    :goto_35
    if-nez v1, :cond_5

    goto/32 :goto_23

    :cond_5
    goto/32 :goto_22

    nop

    :goto_36
    iget v3, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->itemCount:I

    goto/32 :goto_42

    nop

    :goto_37
    invoke-interface {p1, p2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :goto_38
    goto/32 :goto_35

    nop

    :goto_39
    iget v2, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_3d

    nop

    :goto_3a
    add-int/2addr v2, v5

    goto/32 :goto_5

    nop

    :goto_3b
    goto/16 :goto_16

    :goto_3c
    goto/32 :goto_2

    nop

    :goto_3d
    iget v3, p5, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_20

    nop

    :goto_3e
    invoke-interface {p1, p2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto/32 :goto_a

    nop

    :goto_3f
    if-lt v2, v3, :cond_6

    goto/32 :goto_2f

    :cond_6
    goto/32 :goto_3

    nop

    :goto_40
    add-int/2addr v3, v6

    goto/32 :goto_29

    nop

    :goto_41
    sub-int/2addr v2, v5

    goto/32 :goto_26

    nop

    :goto_42
    sub-int/2addr v3, v2

    goto/32 :goto_2e

    nop

    :goto_43
    iget v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_27

    nop

    :goto_44
    iget v3, p3, Lcom/android/internal/widget/AdapterHelper$UpdateOp;->positionStart:I

    goto/32 :goto_9

    nop
.end method
