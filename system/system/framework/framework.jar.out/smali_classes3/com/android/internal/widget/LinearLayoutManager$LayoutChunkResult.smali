.class public Lcom/android/internal/widget/LinearLayoutManager$LayoutChunkResult;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/LinearLayoutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "LayoutChunkResult"
.end annotation


# instance fields
.field public mConsumed:I

.field public mFinished:Z

.field public mFocusable:Z

.field public mIgnoreConsumed:Z


# direct methods
.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method resetInternal()V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_5

    nop

    :goto_2
    iput-boolean v0, p0, Lcom/android/internal/widget/LinearLayoutManager$LayoutChunkResult;->mFinished:Z

    goto/32 :goto_4

    nop

    :goto_3
    iput-boolean v0, p0, Lcom/android/internal/widget/LinearLayoutManager$LayoutChunkResult;->mFocusable:Z

    goto/32 :goto_0

    nop

    :goto_4
    iput-boolean v0, p0, Lcom/android/internal/widget/LinearLayoutManager$LayoutChunkResult;->mIgnoreConsumed:Z

    goto/32 :goto_3

    nop

    :goto_5
    iput v0, p0, Lcom/android/internal/widget/LinearLayoutManager$LayoutChunkResult;->mConsumed:I

    goto/32 :goto_2

    nop
.end method
