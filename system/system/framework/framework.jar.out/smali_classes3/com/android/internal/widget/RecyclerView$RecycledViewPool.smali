.class public Lcom/android/internal/widget/RecyclerView$RecycledViewPool;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/RecyclerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RecycledViewPool"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;
    }
.end annotation


# static fields
.field private static final DEFAULT_MAX_SCRAP:I = 0x5


# instance fields
.field private mAttachCount:I

.field mScrap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->mScrap:Landroid/util/SparseArray;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->mAttachCount:I

    return-void
.end method

.method private getScrapDataForType(I)Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;
    .locals 2

    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->mScrap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;

    if-nez v0, :cond_0

    new-instance v1, Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;

    invoke-direct {v1}, Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;-><init>()V

    move-object v0, v1

    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->mScrap:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_0
    return-object v0
.end method


# virtual methods
.method attach(Lcom/android/internal/widget/RecyclerView$Adapter;)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    iput v0, p0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->mAttachCount:I

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->mAttachCount:I

    goto/32 :goto_3

    nop

    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_0

    nop
.end method

.method public clear()V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->mScrap:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->mScrap:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;

    iget-object v2, v1, Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;->mScrapHeap:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method detach()V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    return-void

    :goto_1
    iput v0, p0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->mAttachCount:I

    goto/32 :goto_0

    nop

    :goto_2
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_1

    nop

    :goto_3
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->mAttachCount:I

    goto/32 :goto_2

    nop
.end method

.method factorInBindTime(IJ)V
    .locals 3

    goto/32 :goto_4

    nop

    :goto_0
    iget-wide v1, v0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;->mBindRunningAverageNs:J

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    invoke-virtual {p0, v1, v2, p2, p3}, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->runningAverage(JJ)J

    move-result-wide v1

    goto/32 :goto_3

    nop

    :goto_3
    iput-wide v1, v0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;->mBindRunningAverageNs:J

    goto/32 :goto_1

    nop

    :goto_4
    invoke-direct {p0, p1}, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->getScrapDataForType(I)Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;

    move-result-object v0

    goto/32 :goto_0

    nop
.end method

.method factorInCreateTime(IJ)V
    .locals 3

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p0, v1, v2, p2, p3}, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->runningAverage(JJ)J

    move-result-wide v1

    goto/32 :goto_2

    nop

    :goto_1
    invoke-direct {p0, p1}, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->getScrapDataForType(I)Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_2
    iput-wide v1, v0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;->mCreateRunningAverageNs:J

    goto/32 :goto_4

    nop

    :goto_3
    iget-wide v1, v0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;->mCreateRunningAverageNs:J

    goto/32 :goto_0

    nop

    :goto_4
    return-void
.end method

.method public getRecycledView(I)Lcom/android/internal/widget/RecyclerView$ViewHolder;
    .locals 3

    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->mScrap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;->mScrapHeap:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;->mScrapHeap:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/internal/widget/RecyclerView$ViewHolder;

    return-object v2

    :cond_0
    const/4 v1, 0x0

    return-object v1
.end method

.method public getRecycledViewCount(I)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->getScrapDataForType(I)Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;

    move-result-object v0

    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;->mScrapHeap:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method onAdapterChanged(Lcom/android/internal/widget/RecyclerView$Adapter;Lcom/android/internal/widget/RecyclerView$Adapter;Z)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    if-nez p1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_5

    nop

    :goto_2
    invoke-virtual {p0, p2}, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->attach(Lcom/android/internal/widget/RecyclerView$Adapter;)V

    :goto_3
    goto/32 :goto_0

    nop

    :goto_4
    iget v0, p0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->mAttachCount:I

    goto/32 :goto_7

    nop

    :goto_5
    invoke-virtual {p0}, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->detach()V

    :goto_6
    goto/32 :goto_a

    nop

    :goto_7
    if-eqz v0, :cond_1

    goto/32 :goto_9

    :cond_1
    goto/32 :goto_8

    nop

    :goto_8
    invoke-virtual {p0}, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->clear()V

    :goto_9
    goto/32 :goto_b

    nop

    :goto_a
    if-eqz p3, :cond_2

    goto/32 :goto_9

    :cond_2
    goto/32 :goto_4

    nop

    :goto_b
    if-nez p2, :cond_3

    goto/32 :goto_3

    :cond_3
    goto/32 :goto_2

    nop
.end method

.method public putRecycledView(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V
    .locals 4

    invoke-virtual {p1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->getScrapDataForType(I)Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;

    move-result-object v1

    iget-object v1, v1, Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;->mScrapHeap:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->mScrap:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;

    iget v2, v2, Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;->mMaxScrap:I

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gt v2, v3, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->resetInternal()V

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method runningAverage(JJ)J
    .locals 6

    goto/32 :goto_1

    nop

    :goto_0
    mul-long/2addr v2, v4

    goto/32 :goto_6

    nop

    :goto_1
    const-wide/16 v0, 0x0

    goto/32 :goto_2

    nop

    :goto_2
    cmp-long v0, p1, v0

    goto/32 :goto_a

    nop

    :goto_3
    return-wide v2

    :goto_4
    const-wide/16 v4, 0x3

    goto/32 :goto_0

    nop

    :goto_5
    add-long/2addr v2, v0

    goto/32 :goto_3

    nop

    :goto_6
    div-long v0, p3, v0

    goto/32 :goto_5

    nop

    :goto_7
    return-wide p3

    :goto_8
    goto/32 :goto_b

    nop

    :goto_9
    div-long v2, p1, v0

    goto/32 :goto_4

    nop

    :goto_a
    if-eqz v0, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_7

    nop

    :goto_b
    const-wide/16 v0, 0x4

    goto/32 :goto_9

    nop
.end method

.method public setMaxRecycledViews(II)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->getScrapDataForType(I)Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;

    move-result-object v0

    iput p2, v0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;->mMaxScrap:I

    iget-object v1, v0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;->mScrapHeap:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, p2, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method size()I
    .locals 4

    goto/32 :goto_8

    nop

    :goto_0
    add-int/2addr v0, v3

    :goto_1
    goto/32 :goto_5

    nop

    :goto_2
    goto :goto_e

    :goto_3
    goto/32 :goto_4

    nop

    :goto_4
    return v0

    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_2

    nop

    :goto_6
    iget-object v2, v2, Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;->mScrapHeap:Ljava/util/ArrayList;

    goto/32 :goto_9

    nop

    :goto_7
    iget-object v2, p0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->mScrap:Landroid/util/SparseArray;

    goto/32 :goto_b

    nop

    :goto_8
    const/4 v0, 0x0

    goto/32 :goto_d

    nop

    :goto_9
    if-nez v2, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_10

    nop

    :goto_a
    if-lt v1, v2, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_11

    nop

    :goto_b
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    goto/32 :goto_a

    nop

    :goto_c
    check-cast v2, Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;

    goto/32 :goto_6

    nop

    :goto_d
    const/4 v1, 0x0

    :goto_e
    goto/32 :goto_7

    nop

    :goto_f
    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_c

    nop

    :goto_10
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    goto/32 :goto_0

    nop

    :goto_11
    iget-object v2, p0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->mScrap:Landroid/util/SparseArray;

    goto/32 :goto_f

    nop
.end method

.method willBindInTime(IJJ)Z
    .locals 4

    goto/32 :goto_d

    nop

    :goto_0
    goto :goto_a

    :goto_1
    goto/32 :goto_e

    nop

    :goto_2
    const-wide/16 v2, 0x0

    goto/32 :goto_8

    nop

    :goto_3
    iget-wide v0, v0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;->mBindRunningAverageNs:J

    goto/32 :goto_2

    nop

    :goto_4
    add-long v2, p2, v0

    goto/32 :goto_5

    nop

    :goto_5
    cmp-long v2, v2, p4

    goto/32 :goto_6

    nop

    :goto_6
    if-ltz v2, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_0

    nop

    :goto_7
    return v2

    :goto_8
    cmp-long v2, v0, v2

    goto/32 :goto_f

    nop

    :goto_9
    goto :goto_c

    :goto_a
    goto/32 :goto_b

    nop

    :goto_b
    const/4 v2, 0x1

    :goto_c
    goto/32 :goto_7

    nop

    :goto_d
    invoke-direct {p0, p1}, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->getScrapDataForType(I)Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_e
    const/4 v2, 0x0

    goto/32 :goto_9

    nop

    :goto_f
    if-nez v2, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_4

    nop
.end method

.method willCreateInTime(IJJ)Z
    .locals 4

    goto/32 :goto_f

    nop

    :goto_0
    cmp-long v2, v0, v2

    goto/32 :goto_d

    nop

    :goto_1
    add-long v2, p2, v0

    goto/32 :goto_2

    nop

    :goto_2
    cmp-long v2, v2, p4

    goto/32 :goto_e

    nop

    :goto_3
    return v2

    :goto_4
    const/4 v2, 0x1

    :goto_5
    goto/32 :goto_3

    nop

    :goto_6
    const-wide/16 v2, 0x0

    goto/32 :goto_0

    nop

    :goto_7
    goto :goto_b

    :goto_8
    goto/32 :goto_9

    nop

    :goto_9
    const/4 v2, 0x0

    goto/32 :goto_a

    nop

    :goto_a
    goto :goto_5

    :goto_b
    goto/32 :goto_4

    nop

    :goto_c
    iget-wide v0, v0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;->mCreateRunningAverageNs:J

    goto/32 :goto_6

    nop

    :goto_d
    if-nez v2, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_1

    nop

    :goto_e
    if-ltz v2, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_7

    nop

    :goto_f
    invoke-direct {p0, p1}, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->getScrapDataForType(I)Lcom/android/internal/widget/RecyclerView$RecycledViewPool$ScrapData;

    move-result-object v0

    goto/32 :goto_c

    nop
.end method
