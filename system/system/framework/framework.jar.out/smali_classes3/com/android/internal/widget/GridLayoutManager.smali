.class public Lcom/android/internal/widget/GridLayoutManager;
.super Lcom/android/internal/widget/LinearLayoutManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/widget/GridLayoutManager$LayoutParams;,
        Lcom/android/internal/widget/GridLayoutManager$DefaultSpanSizeLookup;,
        Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field public static final DEFAULT_SPAN_COUNT:I = -0x1

.field private static final TAG:Ljava/lang/String; = "GridLayoutManager"


# instance fields
.field mCachedBorders:[I

.field final mDecorInsets:Landroid/graphics/Rect;

.field mPendingSpanCountChange:Z

.field final mPreLayoutSpanIndexCache:Landroid/util/SparseIntArray;

.field final mPreLayoutSpanSizeCache:Landroid/util/SparseIntArray;

.field mSet:[Landroid/view/View;

.field mSpanCount:I

.field mSpanSizeLookup:Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/internal/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mPendingSpanCountChange:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanCount:I

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mPreLayoutSpanSizeCache:Landroid/util/SparseIntArray;

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mPreLayoutSpanIndexCache:Landroid/util/SparseIntArray;

    new-instance v0, Lcom/android/internal/widget/GridLayoutManager$DefaultSpanSizeLookup;

    invoke-direct {v0}, Lcom/android/internal/widget/GridLayoutManager$DefaultSpanSizeLookup;-><init>()V

    iput-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanSizeLookup:Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mDecorInsets:Landroid/graphics/Rect;

    invoke-virtual {p0, p2}, Lcom/android/internal/widget/GridLayoutManager;->setSpanCount(I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IIZ)V
    .locals 1

    invoke-direct {p0, p1, p3, p4}, Lcom/android/internal/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mPendingSpanCountChange:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanCount:I

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mPreLayoutSpanSizeCache:Landroid/util/SparseIntArray;

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mPreLayoutSpanIndexCache:Landroid/util/SparseIntArray;

    new-instance v0, Lcom/android/internal/widget/GridLayoutManager$DefaultSpanSizeLookup;

    invoke-direct {v0}, Lcom/android/internal/widget/GridLayoutManager$DefaultSpanSizeLookup;-><init>()V

    iput-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanSizeLookup:Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mDecorInsets:Landroid/graphics/Rect;

    invoke-virtual {p0, p2}, Lcom/android/internal/widget/GridLayoutManager;->setSpanCount(I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 2

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/internal/widget/LinearLayoutManager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mPendingSpanCountChange:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanCount:I

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mPreLayoutSpanSizeCache:Landroid/util/SparseIntArray;

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mPreLayoutSpanIndexCache:Landroid/util/SparseIntArray;

    new-instance v0, Lcom/android/internal/widget/GridLayoutManager$DefaultSpanSizeLookup;

    invoke-direct {v0}, Lcom/android/internal/widget/GridLayoutManager$DefaultSpanSizeLookup;-><init>()V

    iput-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanSizeLookup:Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mDecorInsets:Landroid/graphics/Rect;

    invoke-static {p1, p2, p3, p4}, Lcom/android/internal/widget/GridLayoutManager;->getProperties(Landroid/content/Context;Landroid/util/AttributeSet;II)Lcom/android/internal/widget/RecyclerView$LayoutManager$Properties;

    move-result-object v0

    iget v1, v0, Lcom/android/internal/widget/RecyclerView$LayoutManager$Properties;->spanCount:I

    invoke-virtual {p0, v1}, Lcom/android/internal/widget/GridLayoutManager;->setSpanCount(I)V

    return-void
.end method

.method private assignSpans(Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;IIZ)V
    .locals 8

    if-eqz p5, :cond_0

    const/4 v0, 0x0

    move v1, p3

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    add-int/lit8 v0, p3, -0x1

    const/4 v1, -0x1

    const/4 v2, -0x1

    :goto_0
    const/4 v3, 0x0

    move v4, v0

    :goto_1
    if-eq v4, v1, :cond_1

    iget-object v5, p0, Lcom/android/internal/widget/GridLayoutManager;->mSet:[Landroid/view/View;

    aget-object v5, v5, v4

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;

    invoke-virtual {p0, v5}, Lcom/android/internal/widget/GridLayoutManager;->getPosition(Landroid/view/View;)I

    move-result v7

    invoke-direct {p0, p1, p2, v7}, Lcom/android/internal/widget/GridLayoutManager;->getSpanSize(Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;I)I

    move-result v7

    iput v7, v6, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->mSpanSize:I

    iput v3, v6, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->mSpanIndex:I

    iget v7, v6, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->mSpanSize:I

    add-int/2addr v3, v7

    add-int/2addr v4, v2

    goto :goto_1

    :cond_1
    return-void
.end method

.method private cachePreLayoutSpanMapping()V
    .locals 6

    invoke-virtual {p0}, Lcom/android/internal/widget/GridLayoutManager;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/internal/widget/GridLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;

    invoke-virtual {v2}, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->getViewLayoutPosition()I

    move-result v3

    iget-object v4, p0, Lcom/android/internal/widget/GridLayoutManager;->mPreLayoutSpanSizeCache:Landroid/util/SparseIntArray;

    invoke-virtual {v2}, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->getSpanSize()I

    move-result v5

    invoke-virtual {v4, v3, v5}, Landroid/util/SparseIntArray;->put(II)V

    iget-object v4, p0, Lcom/android/internal/widget/GridLayoutManager;->mPreLayoutSpanIndexCache:Landroid/util/SparseIntArray;

    invoke-virtual {v2}, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->getSpanIndex()I

    move-result v5

    invoke-virtual {v4, v3, v5}, Landroid/util/SparseIntArray;->put(II)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private calculateItemBorders(I)V
    .locals 2

    iget-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mCachedBorders:[I

    iget v1, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanCount:I

    invoke-static {v0, v1, p1}, Lcom/android/internal/widget/GridLayoutManager;->calculateItemBorders([III)[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mCachedBorders:[I

    return-void
.end method

.method static calculateItemBorders([III)[I
    .locals 7

    if-eqz p0, :cond_0

    array-length v0, p0

    add-int/lit8 v1, p1, 0x1

    if-ne v0, v1, :cond_0

    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    aget v0, p0, v0

    if-eq v0, p2, :cond_1

    :cond_0
    add-int/lit8 v0, p1, 0x1

    new-array p0, v0, [I

    :cond_1
    const/4 v0, 0x0

    aput v0, p0, v0

    div-int v0, p2, p1

    rem-int v1, p2, p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    :goto_0
    if-gt v4, p1, :cond_3

    move v5, v0

    add-int/2addr v3, v1

    if-lez v3, :cond_2

    sub-int v6, p1, v3

    if-ge v6, v1, :cond_2

    add-int/lit8 v5, v5, 0x1

    sub-int/2addr v3, p1

    :cond_2
    add-int/2addr v2, v5

    aput v2, p0, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    return-object p0
.end method

.method private clearPreLayoutSpanMappingCache()V
    .locals 1

    iget-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mPreLayoutSpanSizeCache:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    iget-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mPreLayoutSpanIndexCache:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    return-void
.end method

.method private ensureAnchorIsInCorrectSpan(Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;Lcom/android/internal/widget/LinearLayoutManager$AnchorInfo;I)V
    .locals 6

    const/4 v0, 0x1

    if-ne p4, v0, :cond_0

    move v1, v0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iget v2, p3, Lcom/android/internal/widget/LinearLayoutManager$AnchorInfo;->mPosition:I

    invoke-direct {p0, p1, p2, v2}, Lcom/android/internal/widget/GridLayoutManager;->getSpanIndex(Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;I)I

    move-result v2

    if-eqz v1, :cond_1

    :goto_1
    if-lez v2, :cond_3

    iget v3, p3, Lcom/android/internal/widget/LinearLayoutManager$AnchorInfo;->mPosition:I

    if-lez v3, :cond_3

    iget v3, p3, Lcom/android/internal/widget/LinearLayoutManager$AnchorInfo;->mPosition:I

    sub-int/2addr v3, v0

    iput v3, p3, Lcom/android/internal/widget/LinearLayoutManager$AnchorInfo;->mPosition:I

    iget v3, p3, Lcom/android/internal/widget/LinearLayoutManager$AnchorInfo;->mPosition:I

    invoke-direct {p0, p1, p2, v3}, Lcom/android/internal/widget/GridLayoutManager;->getSpanIndex(Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;I)I

    move-result v2

    goto :goto_1

    :cond_1
    invoke-virtual {p2}, Lcom/android/internal/widget/RecyclerView$State;->getItemCount()I

    move-result v3

    sub-int/2addr v3, v0

    iget v0, p3, Lcom/android/internal/widget/LinearLayoutManager$AnchorInfo;->mPosition:I

    move v4, v2

    :goto_2
    if-ge v0, v3, :cond_2

    add-int/lit8 v5, v0, 0x1

    invoke-direct {p0, p1, p2, v5}, Lcom/android/internal/widget/GridLayoutManager;->getSpanIndex(Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;I)I

    move-result v5

    if-le v5, v4, :cond_2

    add-int/lit8 v0, v0, 0x1

    move v4, v5

    goto :goto_2

    :cond_2
    iput v0, p3, Lcom/android/internal/widget/LinearLayoutManager$AnchorInfo;->mPosition:I

    :cond_3
    return-void
.end method

.method private ensureViewSet()V
    .locals 2

    iget-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mSet:[Landroid/view/View;

    if-eqz v0, :cond_0

    array-length v0, v0

    iget v1, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanCount:I

    if-eq v0, v1, :cond_1

    :cond_0
    iget v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanCount:I

    new-array v0, v0, [Landroid/view/View;

    iput-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mSet:[Landroid/view/View;

    :cond_1
    return-void
.end method

.method private getSpanGroupIndex(Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;I)I
    .locals 3

    invoke-virtual {p2}, Lcom/android/internal/widget/RecyclerView$State;->isPreLayout()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanSizeLookup:Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;

    iget v1, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanCount:I

    invoke-virtual {v0, p3, v1}, Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;->getSpanGroupIndex(II)I

    move-result v0

    return v0

    :cond_0
    invoke-virtual {p1, p3}, Lcom/android/internal/widget/RecyclerView$Recycler;->convertPreLayoutPositionToPostLayout(I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot find span size for pre layout position. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GridLayoutManager"

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    return v1

    :cond_1
    iget-object v1, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanSizeLookup:Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;

    iget v2, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanCount:I

    invoke-virtual {v1, v0, v2}, Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;->getSpanGroupIndex(II)I

    move-result v1

    return v1
.end method

.method private getSpanIndex(Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;I)I
    .locals 4

    invoke-virtual {p2}, Lcom/android/internal/widget/RecyclerView$State;->isPreLayout()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanSizeLookup:Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;

    iget v1, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanCount:I

    invoke-virtual {v0, p3, v1}, Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;->getCachedSpanIndex(II)I

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mPreLayoutSpanIndexCache:Landroid/util/SparseIntArray;

    const/4 v1, -0x1

    invoke-virtual {v0, p3, v1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    if-eq v0, v1, :cond_1

    return v0

    :cond_1
    invoke-virtual {p1, p3}, Lcom/android/internal/widget/RecyclerView$Recycler;->convertPreLayoutPositionToPostLayout(I)I

    move-result v2

    if-ne v2, v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "GridLayoutManager"

    invoke-static {v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    return v1

    :cond_2
    iget-object v1, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanSizeLookup:Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;

    iget v3, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanCount:I

    invoke-virtual {v1, v2, v3}, Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;->getCachedSpanIndex(II)I

    move-result v1

    return v1
.end method

.method private getSpanSize(Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;I)I
    .locals 4

    invoke-virtual {p2}, Lcom/android/internal/widget/RecyclerView$State;->isPreLayout()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanSizeLookup:Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;

    invoke-virtual {v0, p3}, Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;->getSpanSize(I)I

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mPreLayoutSpanSizeCache:Landroid/util/SparseIntArray;

    const/4 v1, -0x1

    invoke-virtual {v0, p3, v1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    if-eq v0, v1, :cond_1

    return v0

    :cond_1
    invoke-virtual {p1, p3}, Lcom/android/internal/widget/RecyclerView$Recycler;->convertPreLayoutPositionToPostLayout(I)I

    move-result v2

    if-ne v2, v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "GridLayoutManager"

    invoke-static {v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    return v1

    :cond_2
    iget-object v1, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanSizeLookup:Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;

    invoke-virtual {v1, v2}, Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;->getSpanSize(I)I

    move-result v1

    return v1
.end method

.method private guessMeasurement(FI)V
    .locals 2

    iget v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanCount:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/internal/widget/GridLayoutManager;->calculateItemBorders(I)V

    return-void
.end method

.method private measureChild(Landroid/view/View;IZ)V
    .locals 10

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;

    iget-object v1, v0, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->mDecorInsets:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->top:I

    iget v3, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v3

    iget v3, v0, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->topMargin:I

    add-int/2addr v2, v3

    iget v3, v0, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->bottomMargin:I

    add-int/2addr v2, v3

    iget v3, v1, Landroid/graphics/Rect;->left:I

    iget v4, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v4

    iget v4, v0, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->leftMargin:I

    add-int/2addr v3, v4

    iget v4, v0, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->rightMargin:I

    add-int/2addr v3, v4

    iget v4, v0, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->mSpanIndex:I

    iget v5, v0, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->mSpanSize:I

    invoke-virtual {p0, v4, v5}, Lcom/android/internal/widget/GridLayoutManager;->getSpaceForSpanRange(II)I

    move-result v4

    iget v5, p0, Lcom/android/internal/widget/GridLayoutManager;->mOrientation:I

    const/4 v6, 0x0

    const/4 v7, 0x1

    if-ne v5, v7, :cond_0

    iget v5, v0, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->width:I

    invoke-static {v4, p2, v3, v5, v6}, Lcom/android/internal/widget/GridLayoutManager;->getChildMeasureSpec(IIIIZ)I

    move-result v5

    iget-object v6, p0, Lcom/android/internal/widget/GridLayoutManager;->mOrientationHelper:Lcom/android/internal/widget/OrientationHelper;

    invoke-virtual {v6}, Lcom/android/internal/widget/OrientationHelper;->getTotalSpace()I

    move-result v6

    invoke-virtual {p0}, Lcom/android/internal/widget/GridLayoutManager;->getHeightMode()I

    move-result v8

    iget v9, v0, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->height:I

    invoke-static {v6, v8, v2, v9, v7}, Lcom/android/internal/widget/GridLayoutManager;->getChildMeasureSpec(IIIIZ)I

    move-result v6

    goto :goto_0

    :cond_0
    iget v5, v0, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->height:I

    invoke-static {v4, p2, v2, v5, v6}, Lcom/android/internal/widget/GridLayoutManager;->getChildMeasureSpec(IIIIZ)I

    move-result v6

    iget-object v5, p0, Lcom/android/internal/widget/GridLayoutManager;->mOrientationHelper:Lcom/android/internal/widget/OrientationHelper;

    invoke-virtual {v5}, Lcom/android/internal/widget/OrientationHelper;->getTotalSpace()I

    move-result v5

    invoke-virtual {p0}, Lcom/android/internal/widget/GridLayoutManager;->getWidthMode()I

    move-result v8

    iget v9, v0, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->width:I

    invoke-static {v5, v8, v3, v9, v7}, Lcom/android/internal/widget/GridLayoutManager;->getChildMeasureSpec(IIIIZ)I

    move-result v5

    :goto_0
    invoke-direct {p0, p1, v5, v6, p3}, Lcom/android/internal/widget/GridLayoutManager;->measureChildWithDecorationsAndMargin(Landroid/view/View;IIZ)V

    return-void
.end method

.method private measureChildWithDecorationsAndMargin(Landroid/view/View;IIZ)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/internal/widget/RecyclerView$LayoutParams;

    if-eqz p4, :cond_0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/android/internal/widget/GridLayoutManager;->shouldReMeasureChild(Landroid/view/View;IILcom/android/internal/widget/RecyclerView$LayoutParams;)Z

    move-result v1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/android/internal/widget/GridLayoutManager;->shouldMeasureChild(Landroid/view/View;IILcom/android/internal/widget/RecyclerView$LayoutParams;)Z

    move-result v1

    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {p1, p2, p3}, Landroid/view/View;->measure(II)V

    :cond_1
    return-void
.end method

.method private updateMeasurements()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/internal/widget/GridLayoutManager;->getOrientation()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/internal/widget/GridLayoutManager;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/internal/widget/GridLayoutManager;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/android/internal/widget/GridLayoutManager;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/android/internal/widget/GridLayoutManager;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/internal/widget/GridLayoutManager;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/android/internal/widget/GridLayoutManager;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    :goto_0
    invoke-direct {p0, v0}, Lcom/android/internal/widget/GridLayoutManager;->calculateItemBorders(I)V

    return-void
.end method


# virtual methods
.method public checkLayoutParams(Lcom/android/internal/widget/RecyclerView$LayoutParams;)Z
    .locals 1

    instance-of v0, p1, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;

    return v0
.end method

.method collectPrefetchPositionsForLayoutState(Lcom/android/internal/widget/RecyclerView$State;Lcom/android/internal/widget/LinearLayoutManager$LayoutState;Lcom/android/internal/widget/RecyclerView$LayoutManager$LayoutPrefetchRegistry;)V
    .locals 6

    goto/32 :goto_a

    nop

    :goto_0
    if-nez v2, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_11

    nop

    :goto_1
    iget v4, p2, Lcom/android/internal/widget/LinearLayoutManager$LayoutState;->mScrollingOffset:I

    goto/32 :goto_14

    nop

    :goto_2
    iget v5, p2, Lcom/android/internal/widget/LinearLayoutManager$LayoutState;->mItemDirection:I

    goto/32 :goto_16

    nop

    :goto_3
    iput v4, p2, Lcom/android/internal/widget/LinearLayoutManager$LayoutState;->mCurrentPosition:I

    nop

    goto/32 :goto_c

    nop

    :goto_4
    return-void

    :goto_5
    invoke-virtual {p2, p1}, Lcom/android/internal/widget/LinearLayoutManager$LayoutState;->hasMore(Lcom/android/internal/widget/RecyclerView$State;)Z

    move-result v2

    goto/32 :goto_0

    nop

    :goto_6
    goto :goto_e

    :goto_7
    goto/32 :goto_4

    nop

    :goto_8
    invoke-interface {p3, v2, v3}, Lcom/android/internal/widget/RecyclerView$LayoutManager$LayoutPrefetchRegistry;->addPosition(II)V

    goto/32 :goto_13

    nop

    :goto_9
    const/4 v3, 0x0

    goto/32 :goto_1

    nop

    :goto_a
    iget v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanCount:I

    goto/32 :goto_d

    nop

    :goto_b
    iget v4, p2, Lcom/android/internal/widget/LinearLayoutManager$LayoutState;->mCurrentPosition:I

    goto/32 :goto_2

    nop

    :goto_c
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_6

    nop

    :goto_d
    const/4 v1, 0x0

    :goto_e
    goto/32 :goto_f

    nop

    :goto_f
    iget v2, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanCount:I

    goto/32 :goto_10

    nop

    :goto_10
    if-lt v1, v2, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_5

    nop

    :goto_11
    if-gtz v0, :cond_2

    goto/32 :goto_7

    :cond_2
    goto/32 :goto_12

    nop

    :goto_12
    iget v2, p2, Lcom/android/internal/widget/LinearLayoutManager$LayoutState;->mCurrentPosition:I

    goto/32 :goto_9

    nop

    :goto_13
    iget-object v3, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanSizeLookup:Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;

    goto/32 :goto_17

    nop

    :goto_14
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    goto/32 :goto_8

    nop

    :goto_15
    sub-int/2addr v0, v3

    goto/32 :goto_b

    nop

    :goto_16
    add-int/2addr v4, v5

    goto/32 :goto_3

    nop

    :goto_17
    invoke-virtual {v3, v2}, Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;->getSpanSize(I)I

    move-result v3

    goto/32 :goto_15

    nop
.end method

.method findReferenceChild(Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;III)Landroid/view/View;
    .locals 10

    goto/32 :goto_1a

    nop

    :goto_0
    return-object v5

    :goto_1
    invoke-virtual {p0, v5}, Lcom/android/internal/widget/GridLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {p0, v6}, Lcom/android/internal/widget/GridLayoutManager;->getPosition(Landroid/view/View;)I

    move-result v7

    goto/32 :goto_24

    nop

    :goto_3
    if-nez v9, :cond_0

    goto/32 :goto_28

    :cond_0
    goto/32 :goto_21

    nop

    :goto_4
    if-eqz v1, :cond_1

    goto/32 :goto_2a

    :cond_1
    goto/32 :goto_29

    nop

    :goto_5
    move-object v5, v0

    :goto_6
    goto/32 :goto_0

    nop

    :goto_7
    invoke-direct {p0, p1, p2, v7}, Lcom/android/internal/widget/GridLayoutManager;->getSpanIndex(Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;I)I

    move-result v8

    goto/32 :goto_10

    nop

    :goto_8
    move-object v5, v1

    goto/32 :goto_1c

    nop

    :goto_9
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    goto/32 :goto_15

    nop

    :goto_a
    move v5, p3

    :goto_b
    goto/32 :goto_16

    nop

    :goto_c
    goto :goto_b

    :goto_d
    goto/32 :goto_2c

    nop

    :goto_e
    goto/16 :goto_2a

    :goto_f
    goto/32 :goto_9

    nop

    :goto_10
    if-nez v8, :cond_2

    goto/32 :goto_f

    :cond_2
    goto/32 :goto_e

    nop

    :goto_11
    iget-object v2, p0, Lcom/android/internal/widget/GridLayoutManager;->mOrientationHelper:Lcom/android/internal/widget/OrientationHelper;

    goto/32 :goto_12

    nop

    :goto_12
    invoke-virtual {v2}, Lcom/android/internal/widget/OrientationHelper;->getStartAfterPadding()I

    move-result v2

    goto/32 :goto_2d

    nop

    :goto_13
    const/4 v1, 0x0

    goto/32 :goto_11

    nop

    :goto_14
    invoke-virtual {v9}, Lcom/android/internal/widget/RecyclerView$LayoutParams;->isItemRemoved()Z

    move-result v9

    goto/32 :goto_3

    nop

    :goto_15
    check-cast v9, Lcom/android/internal/widget/RecyclerView$LayoutParams;

    goto/32 :goto_14

    nop

    :goto_16
    if-ne v5, p4, :cond_3

    goto/32 :goto_d

    :cond_3
    goto/32 :goto_1

    nop

    :goto_17
    const/4 v4, -0x1

    :goto_18
    goto/32 :goto_a

    nop

    :goto_19
    invoke-virtual {v3}, Lcom/android/internal/widget/OrientationHelper;->getEndAfterPadding()I

    move-result v3

    goto/32 :goto_33

    nop

    :goto_1a
    invoke-virtual {p0}, Lcom/android/internal/widget/GridLayoutManager;->ensureLayoutState()V

    goto/32 :goto_1b

    nop

    :goto_1b
    const/4 v0, 0x0

    goto/32 :goto_13

    nop

    :goto_1c
    goto :goto_6

    :goto_1d
    goto/32 :goto_5

    nop

    :goto_1e
    iget-object v9, p0, Lcom/android/internal/widget/GridLayoutManager;->mOrientationHelper:Lcom/android/internal/widget/OrientationHelper;

    goto/32 :goto_35

    nop

    :goto_1f
    goto :goto_23

    :goto_20
    goto/32 :goto_22

    nop

    :goto_21
    if-eqz v0, :cond_4

    goto/32 :goto_2a

    :cond_4
    goto/32 :goto_31

    nop

    :goto_22
    return-object v6

    :goto_23
    goto/32 :goto_4

    nop

    :goto_24
    if-gez v7, :cond_5

    goto/32 :goto_2a

    :cond_5
    goto/32 :goto_2b

    nop

    :goto_25
    iget-object v9, p0, Lcom/android/internal/widget/GridLayoutManager;->mOrientationHelper:Lcom/android/internal/widget/OrientationHelper;

    goto/32 :goto_32

    nop

    :goto_26
    add-int/2addr v5, v4

    goto/32 :goto_c

    nop

    :goto_27
    goto :goto_2a

    :goto_28
    goto/32 :goto_1e

    nop

    :goto_29
    move-object v1, v6

    :goto_2a
    goto/32 :goto_26

    nop

    :goto_2b
    if-lt v7, p5, :cond_6

    goto/32 :goto_2a

    :cond_6
    goto/32 :goto_7

    nop

    :goto_2c
    if-nez v1, :cond_7

    goto/32 :goto_1d

    :cond_7
    goto/32 :goto_8

    nop

    :goto_2d
    iget-object v3, p0, Lcom/android/internal/widget/GridLayoutManager;->mOrientationHelper:Lcom/android/internal/widget/OrientationHelper;

    goto/32 :goto_19

    nop

    :goto_2e
    goto :goto_18

    :goto_2f
    goto/32 :goto_17

    nop

    :goto_30
    const/4 v4, 0x1

    goto/32 :goto_2e

    nop

    :goto_31
    move-object v0, v6

    goto/32 :goto_27

    nop

    :goto_32
    invoke-virtual {v9, v6}, Lcom/android/internal/widget/OrientationHelper;->getDecoratedEnd(Landroid/view/View;)I

    move-result v9

    goto/32 :goto_36

    nop

    :goto_33
    if-gt p4, p3, :cond_8

    goto/32 :goto_2f

    :cond_8
    goto/32 :goto_30

    nop

    :goto_34
    if-lt v9, v3, :cond_9

    goto/32 :goto_23

    :cond_9
    goto/32 :goto_25

    nop

    :goto_35
    invoke-virtual {v9, v6}, Lcom/android/internal/widget/OrientationHelper;->getDecoratedStart(Landroid/view/View;)I

    move-result v9

    goto/32 :goto_34

    nop

    :goto_36
    if-lt v9, v2, :cond_a

    goto/32 :goto_20

    :cond_a
    goto/32 :goto_1f

    nop
.end method

.method public generateDefaultLayoutParams()Lcom/android/internal/widget/RecyclerView$LayoutParams;
    .locals 3

    iget v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mOrientation:I

    const/4 v1, -0x2

    const/4 v2, -0x1

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;

    invoke-direct {v0, v1, v2}, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;-><init>(II)V

    return-object v0

    :cond_0
    new-instance v0, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;

    invoke-direct {v0, v2, v1}, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/android/internal/widget/RecyclerView$LayoutParams;
    .locals 1

    new-instance v0, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;

    invoke-direct {v0, p1, p2}, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/android/internal/widget/RecyclerView$LayoutParams;
    .locals 2

    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;

    move-object v1, p1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, v1}, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    return-object v0

    :cond_0
    new-instance v0, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;

    invoke-direct {v0, p1}, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public getColumnCountForAccessibility(Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;)I
    .locals 2

    iget v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mOrientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanCount:I

    return v0

    :cond_0
    invoke-virtual {p2}, Lcom/android/internal/widget/RecyclerView$State;->getItemCount()I

    move-result v0

    if-ge v0, v1, :cond_1

    const/4 v0, 0x0

    return v0

    :cond_1
    invoke-virtual {p2}, Lcom/android/internal/widget/RecyclerView$State;->getItemCount()I

    move-result v0

    sub-int/2addr v0, v1

    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/widget/GridLayoutManager;->getSpanGroupIndex(Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;I)I

    move-result v0

    add-int/2addr v0, v1

    return v0
.end method

.method public getRowCountForAccessibility(Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;)I
    .locals 2

    iget v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mOrientation:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanCount:I

    return v0

    :cond_0
    invoke-virtual {p2}, Lcom/android/internal/widget/RecyclerView$State;->getItemCount()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    const/4 v0, 0x0

    return v0

    :cond_1
    invoke-virtual {p2}, Lcom/android/internal/widget/RecyclerView$State;->getItemCount()I

    move-result v0

    sub-int/2addr v0, v1

    invoke-direct {p0, p1, p2, v0}, Lcom/android/internal/widget/GridLayoutManager;->getSpanGroupIndex(Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;I)I

    move-result v0

    add-int/2addr v0, v1

    return v0
.end method

.method getSpaceForSpanRange(II)I
    .locals 3

    goto/32 :goto_e

    nop

    :goto_0
    const/4 v1, 0x1

    goto/32 :goto_c

    nop

    :goto_1
    sub-int/2addr v1, p1

    goto/32 :goto_b

    nop

    :goto_2
    iget v1, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanCount:I

    goto/32 :goto_4

    nop

    :goto_3
    iget-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mCachedBorders:[I

    goto/32 :goto_5

    nop

    :goto_4
    sub-int v2, v1, p1

    goto/32 :goto_8

    nop

    :goto_5
    add-int v1, p1, p2

    goto/32 :goto_13

    nop

    :goto_6
    return v2

    :goto_7
    goto/32 :goto_3

    nop

    :goto_8
    aget v2, v0, v2

    goto/32 :goto_1

    nop

    :goto_9
    aget v0, v0, p1

    goto/32 :goto_14

    nop

    :goto_a
    if-nez v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_10

    nop

    :goto_b
    sub-int/2addr v1, p2

    goto/32 :goto_12

    nop

    :goto_c
    if-eq v0, v1, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_11

    nop

    :goto_d
    sub-int/2addr v2, v0

    goto/32 :goto_6

    nop

    :goto_e
    iget v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mOrientation:I

    goto/32 :goto_0

    nop

    :goto_f
    return v1

    :goto_10
    iget-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mCachedBorders:[I

    goto/32 :goto_2

    nop

    :goto_11
    invoke-virtual {p0}, Lcom/android/internal/widget/GridLayoutManager;->isLayoutRTL()Z

    move-result v0

    goto/32 :goto_a

    nop

    :goto_12
    aget v0, v0, v1

    goto/32 :goto_d

    nop

    :goto_13
    aget v1, v0, v1

    goto/32 :goto_9

    nop

    :goto_14
    sub-int/2addr v1, v0

    goto/32 :goto_f

    nop
.end method

.method public getSpanCount()I
    .locals 1

    iget v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanCount:I

    return v0
.end method

.method public getSpanSizeLookup()Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;
    .locals 1

    iget-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanSizeLookup:Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;

    return-object v0
.end method

.method layoutChunk(Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;Lcom/android/internal/widget/LinearLayoutManager$LayoutState;Lcom/android/internal/widget/LinearLayoutManager$LayoutChunkResult;)V
    .locals 25

    goto/32 :goto_1a

    nop

    :goto_0
    move/from16 v23, v1

    :goto_1
    goto/32 :goto_57

    nop

    :goto_2
    iget v12, v2, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->height:I

    goto/32 :goto_11a

    nop

    :goto_3
    const/4 v0, 0x0

    goto/32 :goto_a4

    nop

    :goto_4
    move v3, v12

    goto/32 :goto_78

    nop

    :goto_5
    goto :goto_1

    :goto_6
    goto/32 :goto_32

    nop

    :goto_7
    iget-object v3, v2, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->mDecorInsets:Landroid/graphics/Rect;

    goto/32 :goto_70

    nop

    :goto_8
    const/high16 v3, 0x40000000    # 2.0f

    goto/32 :goto_47

    nop

    :goto_9
    goto/16 :goto_22

    :goto_a
    goto/32 :goto_44

    nop

    :goto_b
    move/from16 v13, v21

    goto/32 :goto_154

    nop

    :goto_c
    invoke-virtual {v11}, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->isItemChanged()Z

    move-result v0

    goto/32 :goto_134

    nop

    :goto_d
    check-cast v4, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;

    goto/32 :goto_15c

    nop

    :goto_e
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    goto/32 :goto_125

    nop

    :goto_f
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/GridLayoutManager;->getPaddingTop()I

    move-result v0

    goto/32 :goto_1d

    nop

    :goto_10
    iget v5, v9, Lcom/android/internal/widget/LinearLayoutManager$LayoutState;->mLayoutDirection:I

    goto/32 :goto_171

    nop

    :goto_11
    iget v0, v9, Lcom/android/internal/widget/LinearLayoutManager$LayoutState;->mOffset:I

    goto/32 :goto_117

    nop

    :goto_12
    move/from16 v4, v17

    goto/32 :goto_172

    nop

    :goto_13
    iget-object v3, v6, Lcom/android/internal/widget/GridLayoutManager;->mOrientationHelper:Lcom/android/internal/widget/OrientationHelper;

    goto/32 :goto_147

    nop

    :goto_14
    move/from16 v12, v18

    goto/32 :goto_11d

    nop

    :goto_15
    move/from16 v1, v19

    goto/32 :goto_16c

    nop

    :goto_16
    const/4 v5, 0x0

    goto/32 :goto_36

    nop

    :goto_17
    move v4, v1

    :goto_18
    goto/32 :goto_bf

    nop

    :goto_19
    invoke-virtual {v1, v8}, Lcom/android/internal/widget/OrientationHelper;->getDecoratedMeasurementInOther(Landroid/view/View;)I

    move-result v1

    goto/32 :goto_66

    nop

    :goto_1a
    move-object/from16 v6, p0

    goto/32 :goto_16f

    nop

    :goto_1b
    move v2, v12

    goto/32 :goto_20

    nop

    :goto_1c
    check-cast v11, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;

    goto/32 :goto_e1

    nop

    :goto_1d
    iget-object v1, v6, Lcom/android/internal/widget/GridLayoutManager;->mCachedBorders:[I

    goto/32 :goto_41

    nop

    :goto_1e
    iget v3, v2, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->mSpanSize:I

    goto/32 :goto_28

    nop

    :goto_1f
    if-lt v1, v14, :cond_0

    goto/32 :goto_158

    :cond_0
    goto/32 :goto_b5

    nop

    :goto_20
    move/from16 v0, v20

    goto/32 :goto_f1

    nop

    :goto_21
    invoke-static {v3, v11, v5, v12, v2}, Lcom/android/internal/widget/GridLayoutManager;->getChildMeasureSpec(IIIIZ)I

    move-result v12

    :goto_22
    goto/32 :goto_177

    nop

    :goto_23
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/widget/GridLayoutManager;->updateMeasurements()V

    :goto_24
    goto/32 :goto_29

    nop

    :goto_25
    iget v7, v2, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->topMargin:I

    goto/32 :goto_3e

    nop

    :goto_26
    move v4, v0

    goto/32 :goto_9b

    nop

    :goto_27
    iget-object v2, v6, Lcom/android/internal/widget/GridLayoutManager;->mSet:[Landroid/view/View;

    goto/32 :goto_fa

    nop

    :goto_28
    invoke-virtual {v6, v8, v3}, Lcom/android/internal/widget/GridLayoutManager;->getSpaceForSpanRange(II)I

    move-result v3

    goto/32 :goto_174

    nop

    :goto_29
    iget v0, v9, Lcom/android/internal/widget/LinearLayoutManager$LayoutState;->mItemDirection:I

    goto/32 :goto_7c

    nop

    :goto_2a
    if-eqz v0, :cond_1

    goto/32 :goto_5b

    :cond_1
    goto/32 :goto_c

    nop

    :goto_2b
    throw v3

    :goto_2c
    goto/32 :goto_15a

    nop

    :goto_2d
    iget-object v12, v6, Lcom/android/internal/widget/GridLayoutManager;->mCachedBorders:[I

    goto/32 :goto_ad

    nop

    :goto_2e
    iget v1, v11, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->mSpanIndex:I

    goto/32 :goto_180

    nop

    :goto_2f
    move v4, v0

    goto/32 :goto_113

    nop

    :goto_30
    const/4 v12, 0x0

    goto/32 :goto_f8

    nop

    :goto_31
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    goto/32 :goto_102

    nop

    :goto_32
    move/from16 v20, v0

    goto/32 :goto_11e

    nop

    :goto_33
    if-lt v0, v14, :cond_2

    goto/32 :goto_16a

    :cond_2
    goto/32 :goto_62

    nop

    :goto_34
    iget v13, v6, Lcom/android/internal/widget/GridLayoutManager;->mSpanCount:I

    goto/32 :goto_da

    nop

    :goto_35
    const/high16 v11, 0x40000000    # 2.0f

    goto/32 :goto_30

    nop

    :goto_36
    move v7, v5

    :goto_37
    goto/32 :goto_ec

    nop

    :goto_38
    invoke-static {v8, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    goto/32 :goto_2

    nop

    :goto_39
    iget v8, v3, Landroid/graphics/Rect;->right:I

    goto/32 :goto_9a

    nop

    :goto_3a
    iget v1, v4, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->mSpanSize:I

    goto/32 :goto_116

    nop

    :goto_3b
    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    goto/32 :goto_45

    nop

    :goto_3c
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    goto/32 :goto_e

    nop

    :goto_3d
    const/4 v2, 0x0

    goto/32 :goto_21

    nop

    :goto_3e
    add-int/2addr v5, v7

    goto/32 :goto_bb

    nop

    :goto_3f
    if-nez v16, :cond_3

    goto/32 :goto_a2

    :cond_3
    goto/32 :goto_146

    nop

    :goto_40
    if-ltz v2, :cond_4

    goto/32 :goto_87

    :cond_4
    goto/32 :goto_86

    nop

    :goto_41
    iget v5, v11, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->mSpanIndex:I

    goto/32 :goto_138

    nop

    :goto_42
    move/from16 v19, v5

    goto/32 :goto_d6

    nop

    :goto_43
    move-object/from16 v8, p2

    goto/32 :goto_14

    nop

    :goto_44
    move/from16 v18, v12

    goto/32 :goto_164

    nop

    :goto_45
    const-string v13, " spans but GridLayoutManager has only "

    goto/32 :goto_112

    nop

    :goto_46
    iget-boolean v1, v10, Lcom/android/internal/widget/LinearLayoutManager$LayoutChunkResult;->mFocusable:Z

    goto/32 :goto_a3

    nop

    :goto_47
    const/4 v4, 0x1

    goto/32 :goto_143

    nop

    :goto_48
    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    goto/32 :goto_d9

    nop

    :goto_49
    move-object v1, v8

    goto/32 :goto_ac

    nop

    :goto_4a
    sub-int/2addr v2, v1

    goto/32 :goto_40

    nop

    :goto_4b
    iget-object v0, v6, Lcom/android/internal/widget/GridLayoutManager;->mOrientationHelper:Lcom/android/internal/widget/OrientationHelper;

    goto/32 :goto_185

    nop

    :goto_4c
    move/from16 v22, v11

    goto/32 :goto_ff

    nop

    :goto_4d
    move/from16 v18, v12

    goto/32 :goto_35

    nop

    :goto_4e
    if-nez v15, :cond_5

    goto/32 :goto_114

    :cond_5
    goto/32 :goto_b

    nop

    :goto_4f
    iget v8, v2, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->leftMargin:I

    goto/32 :goto_71

    nop

    :goto_50
    if-eq v5, v7, :cond_6

    goto/32 :goto_fd

    :cond_6
    goto/32 :goto_94

    nop

    :goto_51
    move/from16 v18, v12

    goto/32 :goto_69

    nop

    :goto_52
    move/from16 v4, v24

    goto/32 :goto_cb

    nop

    :goto_53
    invoke-virtual {v6, v2, v13}, Lcom/android/internal/widget/GridLayoutManager;->addDisappearingView(Landroid/view/View;I)V

    :goto_54
    goto/32 :goto_12d

    nop

    :goto_55
    invoke-direct {v6, v2, v11, v13}, Lcom/android/internal/widget/GridLayoutManager;->measureChild(Landroid/view/View;IZ)V

    goto/32 :goto_13

    nop

    :goto_56
    if-eqz v4, :cond_7

    goto/32 :goto_10e

    :cond_7
    goto/32 :goto_13c

    nop

    :goto_57
    move-object/from16 v0, p0

    goto/32 :goto_49

    nop

    :goto_58
    invoke-virtual {v9, v7}, Lcom/android/internal/widget/LinearLayoutManager$LayoutState;->next(Lcom/android/internal/widget/RecyclerView$Recycler;)Landroid/view/View;

    move-result-object v3

    goto/32 :goto_96

    nop

    :goto_59
    move-object v11, v5

    goto/32 :goto_1c

    nop

    :goto_5a
    goto/16 :goto_182

    :goto_5b
    goto/32 :goto_8c

    nop

    :goto_5c
    move-object/from16 v0, p0

    goto/32 :goto_109

    nop

    :goto_5d
    if-nez v0, :cond_8

    goto/32 :goto_2c

    :cond_8
    goto/32 :goto_c4

    nop

    :goto_5e
    move v0, v3

    :goto_5f
    goto/32 :goto_120

    nop

    :goto_60
    iput v4, v10, Lcom/android/internal/widget/LinearLayoutManager$LayoutChunkResult;->mConsumed:I

    goto/32 :goto_f0

    nop

    :goto_61
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_156

    nop

    :goto_62
    iget-object v1, v6, Lcom/android/internal/widget/GridLayoutManager;->mSet:[Landroid/view/View;

    goto/32 :goto_c7

    nop

    :goto_63
    move/from16 v19, v5

    goto/32 :goto_b8

    nop

    :goto_64
    add-int/2addr v5, v7

    goto/32 :goto_121

    nop

    :goto_65
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    goto/32 :goto_31

    nop

    :goto_66
    add-int/2addr v1, v0

    goto/32 :goto_a7

    nop

    :goto_67
    iget-object v12, v6, Lcom/android/internal/widget/GridLayoutManager;->mSet:[Landroid/view/View;

    goto/32 :goto_15f

    nop

    :goto_68
    invoke-virtual {v3, v2}, Lcom/android/internal/widget/OrientationHelper;->getDecoratedMeasurement(Landroid/view/View;)I

    move-result v3

    goto/32 :goto_11f

    nop

    :goto_69
    const/high16 v11, 0x40000000    # 2.0f

    :goto_6a
    goto/32 :goto_107

    nop

    :goto_6b
    iget-object v1, v6, Lcom/android/internal/widget/GridLayoutManager;->mOrientationHelper:Lcom/android/internal/widget/OrientationHelper;

    goto/32 :goto_e6

    nop

    :goto_6c
    move v1, v3

    :goto_6d
    goto/32 :goto_12a

    nop

    :goto_6e
    add-int/2addr v1, v0

    goto/32 :goto_17a

    nop

    :goto_6f
    const/4 v0, 0x0

    goto/32 :goto_15

    nop

    :goto_70
    move/from16 v19, v5

    goto/32 :goto_ca

    nop

    :goto_71
    add-int/2addr v7, v8

    goto/32 :goto_184

    nop

    :goto_72
    move/from16 v18, v12

    goto/32 :goto_60

    nop

    :goto_73
    mul-float v13, v13, v19

    goto/32 :goto_a9

    nop

    :goto_74
    move v0, v13

    :goto_75
    goto/32 :goto_159

    nop

    :goto_76
    if-eqz v3, :cond_9

    goto/32 :goto_17c

    :cond_9
    goto/32 :goto_141

    nop

    :goto_77
    move-object/from16 v7, p1

    goto/32 :goto_43

    nop

    :goto_78
    move/from16 v24, v4

    goto/32 :goto_ea

    nop

    :goto_79
    invoke-direct {v6, v7, v8, v0}, Lcom/android/internal/widget/GridLayoutManager;->getSpanSize(Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;I)I

    move-result v1

    goto/32 :goto_7a

    nop

    :goto_7a
    iget v3, v6, Lcom/android/internal/widget/GridLayoutManager;->mSpanCount:I

    goto/32 :goto_ed

    nop

    :goto_7b
    move/from16 v21, v1

    goto/32 :goto_10f

    nop

    :goto_7c
    if-eq v0, v14, :cond_a

    goto/32 :goto_99

    :cond_a
    goto/32 :goto_eb

    nop

    :goto_7d
    or-int/2addr v1, v2

    goto/32 :goto_d1

    nop

    :goto_7e
    goto/16 :goto_9c

    :goto_7f
    goto/32 :goto_26

    nop

    :goto_80
    const/4 v1, 0x0

    :goto_81
    goto/32 :goto_1f

    nop

    :goto_82
    goto/16 :goto_a0

    :goto_83
    goto/32 :goto_9f

    nop

    :goto_84
    move-object/from16 v21, v3

    goto/32 :goto_1e

    nop

    :goto_85
    if-ne v2, v4, :cond_b

    goto/32 :goto_bd

    :cond_b
    goto/32 :goto_175

    nop

    :goto_86
    goto/16 :goto_2c

    :goto_87
    goto/32 :goto_58

    nop

    :goto_88
    invoke-static {v12, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    goto/32 :goto_fb

    nop

    :goto_89
    iget v2, v6, Lcom/android/internal/widget/GridLayoutManager;->mSpanCount:I

    goto/32 :goto_119

    nop

    :goto_8a
    iget v0, v6, Lcom/android/internal/widget/GridLayoutManager;->mSpanCount:I

    goto/32 :goto_8b

    nop

    :goto_8b
    move/from16 v21, v1

    goto/32 :goto_2e

    nop

    :goto_8c
    const/4 v0, 0x1

    goto/32 :goto_181

    nop

    :goto_8d
    iget-object v0, v6, Lcom/android/internal/widget/GridLayoutManager;->mSet:[Landroid/view/View;

    goto/32 :goto_b7

    nop

    :goto_8e
    const/4 v2, 0x0

    goto/32 :goto_178

    nop

    :goto_8f
    aget v0, v0, v1

    goto/32 :goto_122

    nop

    :goto_90
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/GridLayoutManager;->isLayoutRTL()Z

    move-result v5

    goto/32 :goto_13d

    nop

    :goto_91
    new-instance v3, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_b4

    nop

    :goto_92
    if-gtz v0, :cond_c

    goto/32 :goto_123

    :cond_c
    goto/32 :goto_a6

    nop

    :goto_93
    move v14, v4

    goto/32 :goto_12

    nop

    :goto_94
    iget v3, v9, Lcom/android/internal/widget/LinearLayoutManager$LayoutState;->mOffset:I

    goto/32 :goto_d8

    nop

    :goto_95
    const/4 v1, 0x0

    goto/32 :goto_8e

    nop

    :goto_96
    if-eqz v3, :cond_d

    goto/32 :goto_9e

    :cond_d
    goto/32 :goto_9d

    nop

    :goto_97
    iget-object v1, v6, Lcom/android/internal/widget/GridLayoutManager;->mOrientationHelper:Lcom/android/internal/widget/OrientationHelper;

    goto/32 :goto_19

    nop

    :goto_98
    goto/16 :goto_75

    :goto_99
    goto/32 :goto_74

    nop

    :goto_9a
    add-int/2addr v7, v8

    goto/32 :goto_4f

    nop

    :goto_9b
    move/from16 v17, v1

    :goto_9c
    goto/32 :goto_140

    nop

    :goto_9d
    goto/16 :goto_2c

    :goto_9e
    goto/32 :goto_13f

    nop

    :goto_9f
    move v0, v13

    :goto_a0
    goto/32 :goto_b1

    nop

    :goto_a1
    goto/16 :goto_54

    :goto_a2
    goto/32 :goto_53

    nop

    :goto_a3
    invoke-virtual {v8}, Landroid/view/View;->hasFocusable()Z

    move-result v2

    goto/32 :goto_7d

    nop

    :goto_a4
    const/4 v1, 0x0

    goto/32 :goto_89

    nop

    :goto_a5
    iget v5, v11, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->mSpanIndex:I

    goto/32 :goto_b0

    nop

    :goto_a6
    iget-object v0, v6, Lcom/android/internal/widget/GridLayoutManager;->mCachedBorders:[I

    goto/32 :goto_115

    nop

    :goto_a7
    move v12, v0

    goto/32 :goto_0

    nop

    :goto_a8
    move/from16 v5, v16

    goto/32 :goto_c8

    nop

    :goto_a9
    move/from16 v19, v1

    goto/32 :goto_3a

    nop

    :goto_aa
    move v0, v13

    :goto_ab
    goto/32 :goto_e9

    nop

    :goto_ac
    move/from16 v2, v20

    goto/32 :goto_4

    nop

    :goto_ad
    move/from16 v20, v0

    goto/32 :goto_8a

    nop

    :goto_ae
    move/from16 v20, v0

    goto/32 :goto_de

    nop

    :goto_af
    move v12, v2

    goto/32 :goto_14a

    nop

    :goto_b0
    aget v1, v1, v5

    goto/32 :goto_b9

    nop

    :goto_b1
    move v15, v0

    goto/32 :goto_cf

    nop

    :goto_b2
    invoke-virtual {v6, v2, v13}, Lcom/android/internal/widget/GridLayoutManager;->addView(Landroid/view/View;I)V

    goto/32 :goto_17b

    nop

    :goto_b3
    add-int v2, v3, v4

    goto/32 :goto_111

    nop

    :goto_b4
    new-instance v12, Ljava/lang/StringBuilder;

    goto/32 :goto_165

    nop

    :goto_b5
    iget-object v2, v6, Lcom/android/internal/widget/GridLayoutManager;->mSet:[Landroid/view/View;

    goto/32 :goto_16e

    nop

    :goto_b6
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/GridLayoutManager;->getPaddingLeft()I

    move-result v5

    goto/32 :goto_2d

    nop

    :goto_b7
    const/4 v1, 0x0

    goto/32 :goto_48

    nop

    :goto_b8
    move/from16 v22, v11

    goto/32 :goto_72

    nop

    :goto_b9
    add-int/2addr v0, v1

    goto/32 :goto_6b

    nop

    :goto_ba
    iget v8, v2, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->width:I

    goto/32 :goto_4d

    nop

    :goto_bb
    iget v7, v2, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->bottomMargin:I

    goto/32 :goto_64

    nop

    :goto_bc
    goto/16 :goto_6a

    :goto_bd
    goto/32 :goto_42

    nop

    :goto_be
    const-string v13, "Item at position "

    goto/32 :goto_65

    nop

    :goto_bf
    const/4 v0, 0x0

    :goto_c0
    goto/32 :goto_33

    nop

    :goto_c1
    if-gtz v1, :cond_e

    goto/32 :goto_168

    :cond_e
    goto/32 :goto_155

    nop

    :goto_c2
    if-gt v3, v1, :cond_f

    goto/32 :goto_6d

    :cond_f
    goto/32 :goto_6c

    nop

    :goto_c3
    move/from16 v13, v21

    goto/32 :goto_17

    nop

    :goto_c4
    if-gtz v2, :cond_10

    goto/32 :goto_2c

    :cond_10
    goto/32 :goto_cd

    nop

    :goto_c5
    iget-object v3, v9, Lcom/android/internal/widget/LinearLayoutManager$LayoutState;->mScrapList:Ljava/util/List;

    goto/32 :goto_76

    nop

    :goto_c6
    const/4 v0, 0x1

    goto/32 :goto_5a

    nop

    :goto_c7
    aget-object v1, v1, v0

    goto/32 :goto_e0

    nop

    :goto_c8
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/widget/GridLayoutManager;->assignSpans(Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;IIZ)V

    goto/32 :goto_6f

    nop

    :goto_c9
    move/from16 v21, v5

    goto/32 :goto_d3

    nop

    :goto_ca
    iget v5, v3, Landroid/graphics/Rect;->top:I

    goto/32 :goto_100

    nop

    :goto_cb
    goto/16 :goto_37

    :goto_cc
    goto/32 :goto_f5

    nop

    :goto_cd
    iget v0, v9, Lcom/android/internal/widget/LinearLayoutManager$LayoutState;->mCurrentPosition:I

    goto/32 :goto_79

    nop

    :goto_ce
    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/widget/GridLayoutManager;->layoutDecoratedWithMargins(Landroid/view/View;IIII)V

    goto/32 :goto_ef

    nop

    :goto_cf
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/GridLayoutManager;->getChildCount()I

    move-result v0

    goto/32 :goto_92

    nop

    :goto_d0
    const/16 v19, 0x0

    goto/32 :goto_10b

    nop

    :goto_d1
    iput-boolean v1, v10, Lcom/android/internal/widget/LinearLayoutManager$LayoutChunkResult;->mFocusable:Z

    goto/32 :goto_12b

    nop

    :goto_d2
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    goto/32 :goto_59

    nop

    :goto_d3
    goto/16 :goto_1

    :goto_d4
    goto/32 :goto_ae

    nop

    :goto_d5
    iget v3, v9, Lcom/android/internal/widget/LinearLayoutManager$LayoutState;->mCurrentPosition:I

    goto/32 :goto_11c

    nop

    :goto_d6
    move/from16 v22, v11

    goto/32 :goto_51

    nop

    :goto_d7
    move/from16 v3, v23

    goto/32 :goto_52

    nop

    :goto_d8
    sub-int v2, v3, v4

    goto/32 :goto_fc

    nop

    :goto_d9
    return-void

    :goto_da
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    goto/32 :goto_144

    nop

    :goto_db
    add-int/2addr v5, v0

    goto/32 :goto_4b

    nop

    :goto_dc
    invoke-virtual {v13, v2}, Lcom/android/internal/widget/OrientationHelper;->getDecoratedMeasurementInOther(Landroid/view/View;)I

    move-result v13

    goto/32 :goto_128

    nop

    :goto_dd
    if-eq v5, v8, :cond_11

    goto/32 :goto_e4

    :cond_11
    goto/32 :goto_170

    nop

    :goto_de
    move/from16 v21, v1

    goto/32 :goto_135

    nop

    :goto_df
    move-object/from16 v10, p4

    goto/32 :goto_106

    nop

    :goto_e0
    iget-object v2, v6, Lcom/android/internal/widget/GridLayoutManager;->mOrientationHelper:Lcom/android/internal/widget/OrientationHelper;

    goto/32 :goto_15d

    nop

    :goto_e1
    iget v5, v6, Lcom/android/internal/widget/GridLayoutManager;->mOrientation:I

    goto/32 :goto_14d

    nop

    :goto_e2
    const/4 v8, 0x1

    goto/32 :goto_dd

    nop

    :goto_e3
    goto/16 :goto_118

    :goto_e4
    goto/32 :goto_10

    nop

    :goto_e5
    move/from16 v11, v22

    goto/32 :goto_169

    nop

    :goto_e6
    invoke-virtual {v1, v8}, Lcom/android/internal/widget/OrientationHelper;->getDecoratedMeasurementInOther(Landroid/view/View;)I

    move-result v1

    goto/32 :goto_6e

    nop

    :goto_e7
    const/4 v13, 0x0

    goto/32 :goto_153

    nop

    :goto_e8
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    goto/32 :goto_3b

    nop

    :goto_e9
    move v5, v0

    goto/32 :goto_186

    nop

    :goto_ea
    move/from16 v4, v21

    goto/32 :goto_137

    nop

    :goto_eb
    move v0, v14

    goto/32 :goto_98

    nop

    :goto_ec
    if-lt v7, v14, :cond_12

    goto/32 :goto_cc

    :cond_12
    goto/32 :goto_145

    nop

    :goto_ed
    if-le v1, v3, :cond_13

    goto/32 :goto_14f

    :cond_13
    goto/32 :goto_4a

    nop

    :goto_ee
    const/high16 v12, 0x40000000    # 2.0f

    goto/32 :goto_e7

    nop

    :goto_ef
    invoke-virtual {v11}, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->isItemRemoved()Z

    move-result v0

    goto/32 :goto_2a

    nop

    :goto_f0
    const/4 v0, 0x0

    goto/32 :goto_95

    nop

    :goto_f1
    move/from16 v1, v21

    goto/32 :goto_d7

    nop

    :goto_f2
    goto/16 :goto_5b

    :goto_f3
    goto/32 :goto_c6

    nop

    :goto_f4
    invoke-virtual {v6, v2}, Lcom/android/internal/widget/GridLayoutManager;->addView(Landroid/view/View;)V

    goto/32 :goto_104

    nop

    :goto_f5
    move/from16 v20, v0

    goto/32 :goto_17d

    nop

    :goto_f6
    move/from16 v23, v3

    goto/32 :goto_5

    nop

    :goto_f7
    sub-int v0, v5, v0

    goto/32 :goto_162

    nop

    :goto_f8
    invoke-static {v3, v11, v7, v8, v12}, Lcom/android/internal/widget/GridLayoutManager;->getChildMeasureSpec(IIIIZ)I

    move-result v8

    goto/32 :goto_124

    nop

    :goto_f9
    move-object/from16 v2, p2

    goto/32 :goto_14c

    nop

    :goto_fa
    aget-object v2, v2, v0

    goto/32 :goto_c5

    nop

    :goto_fb
    move-object/from16 v23, v2

    goto/32 :goto_150

    nop

    :goto_fc
    goto/16 :goto_118

    :goto_fd
    goto/32 :goto_11b

    nop

    :goto_fe
    aget-object v8, v5, v7

    goto/32 :goto_d2

    nop

    :goto_ff
    const/4 v11, 0x1

    goto/32 :goto_13e

    nop

    :goto_100
    iget v7, v3, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_17e

    nop

    :goto_101
    add-int v3, v2, v4

    goto/32 :goto_e3

    nop

    :goto_102
    const-string v13, " requires "

    goto/32 :goto_e8

    nop

    :goto_103
    move v0, v14

    goto/32 :goto_82

    nop

    :goto_104
    goto/16 :goto_54

    :goto_105
    goto/32 :goto_b2

    nop

    :goto_106
    iget-object v0, v6, Lcom/android/internal/widget/GridLayoutManager;->mOrientationHelper:Lcom/android/internal/widget/OrientationHelper;

    goto/32 :goto_110

    nop

    :goto_107
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_77

    nop

    :goto_108
    if-eq v5, v12, :cond_14

    goto/32 :goto_6

    :cond_14
    goto/32 :goto_90

    nop

    :goto_109
    move-object/from16 v1, p1

    goto/32 :goto_f9

    nop

    :goto_10a
    iget-object v3, v6, Lcom/android/internal/widget/GridLayoutManager;->mOrientationHelper:Lcom/android/internal/widget/OrientationHelper;

    goto/32 :goto_68

    nop

    :goto_10b
    const/16 v20, 0x0

    goto/32 :goto_5c

    nop

    :goto_10c
    add-int/lit8 v4, v4, 0x1

    goto/32 :goto_152

    nop

    :goto_10d
    return-void

    :goto_10e
    goto/32 :goto_d0

    nop

    :goto_10f
    move v12, v2

    goto/32 :goto_f6

    nop

    :goto_110
    invoke-virtual {v0}, Lcom/android/internal/widget/OrientationHelper;->getModeInOther()I

    move-result v11

    goto/32 :goto_ee

    nop

    :goto_111
    move v4, v0

    goto/32 :goto_133

    nop

    :goto_112
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    goto/32 :goto_34

    nop

    :goto_113
    goto/16 :goto_18

    :goto_114
    goto/32 :goto_c3

    nop

    :goto_115
    iget v1, v6, Lcom/android/internal/widget/GridLayoutManager;->mSpanCount:I

    goto/32 :goto_8f

    nop

    :goto_116
    int-to-float v1, v1

    goto/32 :goto_176

    nop

    :goto_117
    add-int v1, v0, v4

    :goto_118
    goto/32 :goto_16

    nop

    :goto_119
    if-eqz v16, :cond_15

    goto/32 :goto_7f

    :cond_15
    goto/32 :goto_d5

    nop

    :goto_11a
    move-object/from16 v23, v2

    goto/32 :goto_3d

    nop

    :goto_11b
    iget v2, v9, Lcom/android/internal/widget/LinearLayoutManager$LayoutState;->mOffset:I

    goto/32 :goto_101

    nop

    :goto_11c
    invoke-direct {v6, v7, v8, v3}, Lcom/android/internal/widget/GridLayoutManager;->getSpanIndex(Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;I)I

    move-result v3

    goto/32 :goto_142

    nop

    :goto_11d
    move/from16 v5, v19

    goto/32 :goto_e5

    nop

    :goto_11e
    move/from16 v21, v1

    goto/32 :goto_f

    nop

    :goto_11f
    if-gt v3, v0, :cond_16

    goto/32 :goto_5f

    :cond_16
    goto/32 :goto_5e

    nop

    :goto_120
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_157

    nop

    :goto_121
    iget v7, v3, Landroid/graphics/Rect;->left:I

    goto/32 :goto_39

    nop

    :goto_122
    goto/16 :goto_ab

    :goto_123
    goto/32 :goto_aa

    nop

    :goto_124
    sub-int v12, v4, v5

    goto/32 :goto_88

    nop

    :goto_125
    invoke-direct {v3, v12}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_2b

    nop

    :goto_126
    add-int/2addr v7, v8

    goto/32 :goto_151

    nop

    :goto_127
    invoke-direct {v6, v1, v8, v12, v2}, Lcom/android/internal/widget/GridLayoutManager;->measureChildWithDecorationsAndMargin(Landroid/view/View;IIZ)V

    goto/32 :goto_bc

    nop

    :goto_128
    int-to-float v13, v13

    goto/32 :goto_73

    nop

    :goto_129
    add-int/2addr v0, v1

    goto/32 :goto_97

    nop

    :goto_12a
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    goto/32 :goto_d

    nop

    :goto_12b
    add-int/lit8 v7, v7, 0x1

    goto/32 :goto_1b

    nop

    :goto_12c
    move-object/from16 v9, p3

    goto/32 :goto_df

    nop

    :goto_12d
    iget-object v3, v6, Lcom/android/internal/widget/GridLayoutManager;->mDecorInsets:Landroid/graphics/Rect;

    goto/32 :goto_163

    nop

    :goto_12e
    if-lt v0, v14, :cond_17

    goto/32 :goto_149

    :cond_17
    goto/32 :goto_27

    nop

    :goto_12f
    if-lt v4, v0, :cond_18

    goto/32 :goto_2c

    :cond_18
    goto/32 :goto_179

    nop

    :goto_130
    goto/16 :goto_118

    :goto_131
    goto/32 :goto_11

    nop

    :goto_132
    invoke-direct {v6, v7, v8, v4}, Lcom/android/internal/widget/GridLayoutManager;->getSpanSize(Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;I)I

    move-result v4

    goto/32 :goto_b3

    nop

    :goto_133
    move/from16 v17, v1

    goto/32 :goto_7e

    nop

    :goto_134
    if-nez v0, :cond_19

    goto/32 :goto_f3

    :cond_19
    goto/32 :goto_f2

    nop

    :goto_135
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/GridLayoutManager;->getPaddingLeft()I

    move-result v0

    goto/32 :goto_15e

    nop

    :goto_136
    check-cast v2, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;

    goto/32 :goto_7

    nop

    :goto_137
    move/from16 v5, v23

    goto/32 :goto_ce

    nop

    :goto_138
    aget v1, v1, v5

    goto/32 :goto_129

    nop

    :goto_139
    cmpl-float v1, v13, v5

    goto/32 :goto_c1

    nop

    :goto_13a
    sub-int v0, v1, v4

    goto/32 :goto_130

    nop

    :goto_13b
    const/4 v7, -0x1

    goto/32 :goto_e2

    nop

    :goto_13c
    iput-boolean v14, v10, Lcom/android/internal/widget/LinearLayoutManager$LayoutChunkResult;->mFinished:Z

    goto/32 :goto_10d

    nop

    :goto_13d
    if-nez v5, :cond_1a

    goto/32 :goto_d4

    :cond_1a
    goto/32 :goto_b6

    nop

    :goto_13e
    if-eq v8, v11, :cond_1b

    goto/32 :goto_a

    :cond_1b
    goto/32 :goto_ba

    nop

    :goto_13f
    add-int v17, v17, v1

    goto/32 :goto_67

    nop

    :goto_140
    iget v0, v6, Lcom/android/internal/widget/GridLayoutManager;->mSpanCount:I

    goto/32 :goto_12f

    nop

    :goto_141
    if-nez v16, :cond_1c

    goto/32 :goto_105

    :cond_1c
    goto/32 :goto_f4

    nop

    :goto_142
    iget v4, v9, Lcom/android/internal/widget/LinearLayoutManager$LayoutState;->mCurrentPosition:I

    goto/32 :goto_132

    nop

    :goto_143
    invoke-direct {v6, v2, v3, v4}, Lcom/android/internal/widget/GridLayoutManager;->measureChild(Landroid/view/View;IZ)V

    goto/32 :goto_10a

    nop

    :goto_144
    const-string v13, " spans."

    goto/32 :goto_3c

    nop

    :goto_145
    iget-object v5, v6, Lcom/android/internal/widget/GridLayoutManager;->mSet:[Landroid/view/View;

    goto/32 :goto_fe

    nop

    :goto_146
    invoke-virtual {v6, v2}, Lcom/android/internal/widget/GridLayoutManager;->addDisappearingView(Landroid/view/View;)V

    goto/32 :goto_a1

    nop

    :goto_147
    invoke-virtual {v3, v2}, Lcom/android/internal/widget/OrientationHelper;->getDecoratedMeasurement(Landroid/view/View;)I

    move-result v3

    goto/32 :goto_c2

    nop

    :goto_148
    goto/16 :goto_16d

    :goto_149
    goto/32 :goto_4e

    nop

    :goto_14a
    move/from16 v23, v3

    goto/32 :goto_c9

    nop

    :goto_14b
    const/4 v0, 0x0

    goto/32 :goto_80

    nop

    :goto_14c
    move v3, v4

    goto/32 :goto_93

    nop

    :goto_14d
    const/4 v12, 0x1

    goto/32 :goto_108

    nop

    :goto_14e
    goto/16 :goto_9c

    :goto_14f
    goto/32 :goto_91

    nop

    :goto_150
    const/4 v2, 0x0

    goto/32 :goto_9

    nop

    :goto_151
    iget v8, v2, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->mSpanIndex:I

    goto/32 :goto_84

    nop

    :goto_152
    const/high16 v12, 0x40000000    # 2.0f

    goto/32 :goto_14e

    nop

    :goto_153
    const/4 v14, 0x1

    goto/32 :goto_173

    nop

    :goto_154
    invoke-direct {v6, v5, v13}, Lcom/android/internal/widget/GridLayoutManager;->guessMeasurement(FI)V

    goto/32 :goto_14b

    nop

    :goto_155
    move v1, v13

    goto/32 :goto_167

    nop

    :goto_156
    move/from16 v1, v19

    goto/32 :goto_161

    nop

    :goto_157
    goto/16 :goto_81

    :goto_158
    goto/32 :goto_2f

    nop

    :goto_159
    move/from16 v16, v0

    goto/32 :goto_3

    nop

    :goto_15a
    move v12, v2

    goto/32 :goto_56

    nop

    :goto_15b
    iget v1, v9, Lcom/android/internal/widget/LinearLayoutManager$LayoutState;->mOffset:I

    goto/32 :goto_13a

    nop

    :goto_15c
    const/high16 v19, 0x3f800000    # 1.0f

    goto/32 :goto_16b

    nop

    :goto_15d
    invoke-virtual {v2, v1}, Lcom/android/internal/widget/OrientationHelper;->getDecoratedMeasurement(Landroid/view/View;)I

    move-result v2

    goto/32 :goto_85

    nop

    :goto_15e
    iget-object v1, v6, Lcom/android/internal/widget/GridLayoutManager;->mCachedBorders:[I

    goto/32 :goto_a5

    nop

    :goto_15f
    aput-object v3, v12, v4

    nop

    goto/32 :goto_10c

    nop

    :goto_160
    iget v5, v6, Lcom/android/internal/widget/GridLayoutManager;->mOrientation:I

    goto/32 :goto_13b

    nop

    :goto_161
    const/4 v13, 0x0

    goto/32 :goto_148

    nop

    :goto_162
    move/from16 v20, v0

    goto/32 :goto_af

    nop

    :goto_163
    invoke-virtual {v6, v2, v3}, Lcom/android/internal/widget/GridLayoutManager;->calculateItemDecorationsForChild(Landroid/view/View;Landroid/graphics/Rect;)V

    goto/32 :goto_55

    nop

    :goto_164
    const/high16 v11, 0x40000000    # 2.0f

    goto/32 :goto_183

    nop

    :goto_165
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_be

    nop

    :goto_166
    aget v0, v12, v0

    goto/32 :goto_db

    nop

    :goto_167
    move v5, v1

    :goto_168
    goto/32 :goto_61

    nop

    :goto_169
    goto/16 :goto_c0

    :goto_16a
    goto/32 :goto_63

    nop

    :goto_16b
    iget-object v13, v6, Lcom/android/internal/widget/GridLayoutManager;->mOrientationHelper:Lcom/android/internal/widget/OrientationHelper;

    goto/32 :goto_dc

    nop

    :goto_16c
    move/from16 v5, v20

    :goto_16d
    goto/32 :goto_12e

    nop

    :goto_16e
    aget-object v2, v2, v1

    goto/32 :goto_8

    nop

    :goto_16f
    move-object/from16 v7, p1

    goto/32 :goto_17f

    nop

    :goto_170
    iget v5, v9, Lcom/android/internal/widget/LinearLayoutManager$LayoutState;->mLayoutDirection:I

    goto/32 :goto_50

    nop

    :goto_171
    if-eq v5, v7, :cond_1d

    goto/32 :goto_131

    :cond_1d
    goto/32 :goto_15b

    nop

    :goto_172
    move/from16 v21, v5

    goto/32 :goto_a8

    nop

    :goto_173
    if-ne v11, v12, :cond_1e

    goto/32 :goto_83

    :cond_1e
    goto/32 :goto_103

    nop

    :goto_174
    iget v8, v6, Lcom/android/internal/widget/GridLayoutManager;->mOrientation:I

    goto/32 :goto_4c

    nop

    :goto_175
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    goto/32 :goto_136

    nop

    :goto_176
    div-float/2addr v13, v1

    goto/32 :goto_139

    nop

    :goto_177
    const/4 v2, 0x1

    goto/32 :goto_127

    nop

    :goto_178
    const/4 v3, 0x0

    goto/32 :goto_160

    nop

    :goto_179
    invoke-virtual {v9, v8}, Lcom/android/internal/widget/LinearLayoutManager$LayoutState;->hasMore(Lcom/android/internal/widget/RecyclerView$State;)Z

    move-result v0

    goto/32 :goto_5d

    nop

    :goto_17a
    move/from16 v20, v0

    goto/32 :goto_7b

    nop

    :goto_17b
    goto/16 :goto_54

    :goto_17c
    goto/32 :goto_3f

    nop

    :goto_17d
    move/from16 v21, v1

    goto/32 :goto_8d

    nop

    :goto_17e
    add-int/2addr v5, v7

    goto/32 :goto_25

    nop

    :goto_17f
    move-object/from16 v8, p2

    goto/32 :goto_12c

    nop

    :goto_180
    sub-int/2addr v0, v1

    goto/32 :goto_166

    nop

    :goto_181
    iput-boolean v0, v10, Lcom/android/internal/widget/LinearLayoutManager$LayoutChunkResult;->mIgnoreConsumed:Z

    :goto_182
    goto/32 :goto_46

    nop

    :goto_183
    sub-int v8, v4, v7

    goto/32 :goto_38

    nop

    :goto_184
    iget v8, v2, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->rightMargin:I

    goto/32 :goto_126

    nop

    :goto_185
    invoke-virtual {v0, v8}, Lcom/android/internal/widget/OrientationHelper;->getDecoratedMeasurementInOther(Landroid/view/View;)I

    move-result v0

    goto/32 :goto_f7

    nop

    :goto_186
    if-nez v15, :cond_1f

    goto/32 :goto_24

    :cond_1f
    goto/32 :goto_23

    nop
.end method

.method onAnchorReady(Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;Lcom/android/internal/widget/LinearLayoutManager$AnchorInfo;I)V
    .locals 1

    goto/32 :goto_6

    nop

    :goto_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/internal/widget/GridLayoutManager;->ensureAnchorIsInCorrectSpan(Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;Lcom/android/internal/widget/LinearLayoutManager$AnchorInfo;I)V

    :goto_1
    goto/32 :goto_7

    nop

    :goto_2
    if-eqz v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_0

    nop

    :goto_3
    return-void

    :goto_4
    if-gtz v0, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_9

    nop

    :goto_5
    invoke-direct {p0}, Lcom/android/internal/widget/GridLayoutManager;->updateMeasurements()V

    goto/32 :goto_8

    nop

    :goto_6
    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/internal/widget/LinearLayoutManager;->onAnchorReady(Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;Lcom/android/internal/widget/LinearLayoutManager$AnchorInfo;I)V

    goto/32 :goto_5

    nop

    :goto_7
    invoke-direct {p0}, Lcom/android/internal/widget/GridLayoutManager;->ensureViewSet()V

    goto/32 :goto_3

    nop

    :goto_8
    invoke-virtual {p2}, Lcom/android/internal/widget/RecyclerView$State;->getItemCount()I

    move-result v0

    goto/32 :goto_4

    nop

    :goto_9
    invoke-virtual {p2}, Lcom/android/internal/widget/RecyclerView$State;->isPreLayout()Z

    move-result v0

    goto/32 :goto_2

    nop
.end method

.method public onInitializeAccessibilityNodeInfoForItem(Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 9

    invoke-virtual {p3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v1, v0, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;

    if-nez v1, :cond_0

    invoke-super {p0, p3, p4}, Lcom/android/internal/widget/LinearLayoutManager;->onInitializeAccessibilityNodeInfoForItem(Landroid/view/View;Landroid/view/accessibility/AccessibilityNodeInfo;)V

    return-void

    :cond_0
    move-object v1, v0

    check-cast v1, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;

    invoke-virtual {v1}, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->getViewLayoutPosition()I

    move-result v2

    invoke-direct {p0, p1, p2, v2}, Lcom/android/internal/widget/GridLayoutManager;->getSpanGroupIndex(Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;I)I

    move-result v2

    iget v3, p0, Lcom/android/internal/widget/GridLayoutManager;->mOrientation:I

    if-nez v3, :cond_1

    nop

    invoke-virtual {v1}, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->getSpanIndex()I

    move-result v3

    invoke-virtual {v1}, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->getSpanSize()I

    move-result v4

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    move v5, v2

    invoke-static/range {v3 .. v8}, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;->obtain(IIIIZZ)Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;

    move-result-object v3

    invoke-virtual {p4, v3}, Landroid/view/accessibility/AccessibilityNodeInfo;->setCollectionItemInfo(Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;)V

    goto :goto_0

    :cond_1
    const/4 v4, 0x1

    invoke-virtual {v1}, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->getSpanIndex()I

    move-result v5

    invoke-virtual {v1}, Lcom/android/internal/widget/GridLayoutManager$LayoutParams;->getSpanSize()I

    move-result v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    move v3, v2

    invoke-static/range {v3 .. v8}, Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;->obtain(IIIIZZ)Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;

    move-result-object v3

    invoke-virtual {p4, v3}, Landroid/view/accessibility/AccessibilityNodeInfo;->setCollectionItemInfo(Landroid/view/accessibility/AccessibilityNodeInfo$CollectionItemInfo;)V

    :goto_0
    return-void
.end method

.method public onItemsAdded(Lcom/android/internal/widget/RecyclerView;II)V
    .locals 1

    iget-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanSizeLookup:Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;

    invoke-virtual {v0}, Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;->invalidateSpanIndexCache()V

    return-void
.end method

.method public onItemsChanged(Lcom/android/internal/widget/RecyclerView;)V
    .locals 1

    iget-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanSizeLookup:Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;

    invoke-virtual {v0}, Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;->invalidateSpanIndexCache()V

    return-void
.end method

.method public onItemsMoved(Lcom/android/internal/widget/RecyclerView;III)V
    .locals 1

    iget-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanSizeLookup:Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;

    invoke-virtual {v0}, Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;->invalidateSpanIndexCache()V

    return-void
.end method

.method public onItemsRemoved(Lcom/android/internal/widget/RecyclerView;II)V
    .locals 1

    iget-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanSizeLookup:Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;

    invoke-virtual {v0}, Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;->invalidateSpanIndexCache()V

    return-void
.end method

.method public onItemsUpdated(Lcom/android/internal/widget/RecyclerView;IILjava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanSizeLookup:Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;

    invoke-virtual {v0}, Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;->invalidateSpanIndexCache()V

    return-void
.end method

.method public onLayoutChildren(Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;)V
    .locals 1

    invoke-virtual {p2}, Lcom/android/internal/widget/RecyclerView$State;->isPreLayout()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/internal/widget/GridLayoutManager;->cachePreLayoutSpanMapping()V

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/android/internal/widget/LinearLayoutManager;->onLayoutChildren(Lcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;)V

    invoke-direct {p0}, Lcom/android/internal/widget/GridLayoutManager;->clearPreLayoutSpanMappingCache()V

    return-void
.end method

.method public onLayoutCompleted(Lcom/android/internal/widget/RecyclerView$State;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/internal/widget/LinearLayoutManager;->onLayoutCompleted(Lcom/android/internal/widget/RecyclerView$State;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mPendingSpanCountChange:Z

    return-void
.end method

.method public scrollHorizontallyBy(ILcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;)I
    .locals 1

    invoke-direct {p0}, Lcom/android/internal/widget/GridLayoutManager;->updateMeasurements()V

    invoke-direct {p0}, Lcom/android/internal/widget/GridLayoutManager;->ensureViewSet()V

    invoke-super {p0, p1, p2, p3}, Lcom/android/internal/widget/LinearLayoutManager;->scrollHorizontallyBy(ILcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;)I

    move-result v0

    return v0
.end method

.method public scrollVerticallyBy(ILcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;)I
    .locals 1

    invoke-direct {p0}, Lcom/android/internal/widget/GridLayoutManager;->updateMeasurements()V

    invoke-direct {p0}, Lcom/android/internal/widget/GridLayoutManager;->ensureViewSet()V

    invoke-super {p0, p1, p2, p3}, Lcom/android/internal/widget/LinearLayoutManager;->scrollVerticallyBy(ILcom/android/internal/widget/RecyclerView$Recycler;Lcom/android/internal/widget/RecyclerView$State;)I

    move-result v0

    return v0
.end method

.method public setMeasuredDimension(Landroid/graphics/Rect;II)V
    .locals 7

    iget-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mCachedBorders:[I

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2, p3}, Lcom/android/internal/widget/LinearLayoutManager;->setMeasuredDimension(Landroid/graphics/Rect;II)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/internal/widget/GridLayoutManager;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/internal/widget/GridLayoutManager;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/android/internal/widget/GridLayoutManager;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/internal/widget/GridLayoutManager;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    iget v2, p0, Lcom/android/internal/widget/GridLayoutManager;->mOrientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    add-int/2addr v2, v1

    invoke-virtual {p0}, Lcom/android/internal/widget/GridLayoutManager;->getMinimumHeight()I

    move-result v4

    invoke-static {p3, v2, v4}, Lcom/android/internal/widget/GridLayoutManager;->chooseSize(III)I

    move-result v4

    iget-object v5, p0, Lcom/android/internal/widget/GridLayoutManager;->mCachedBorders:[I

    array-length v6, v5

    sub-int/2addr v6, v3

    aget v3, v5, v6

    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/android/internal/widget/GridLayoutManager;->getMinimumWidth()I

    move-result v5

    invoke-static {p2, v3, v5}, Lcom/android/internal/widget/GridLayoutManager;->chooseSize(III)I

    move-result v2

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v2

    add-int/2addr v2, v0

    invoke-virtual {p0}, Lcom/android/internal/widget/GridLayoutManager;->getMinimumWidth()I

    move-result v4

    invoke-static {p2, v2, v4}, Lcom/android/internal/widget/GridLayoutManager;->chooseSize(III)I

    move-result v4

    iget-object v5, p0, Lcom/android/internal/widget/GridLayoutManager;->mCachedBorders:[I

    array-length v6, v5

    sub-int/2addr v6, v3

    aget v3, v5, v6

    add-int/2addr v3, v1

    invoke-virtual {p0}, Lcom/android/internal/widget/GridLayoutManager;->getMinimumHeight()I

    move-result v5

    invoke-static {p3, v3, v5}, Lcom/android/internal/widget/GridLayoutManager;->chooseSize(III)I

    move-result v3

    move v2, v4

    move v4, v3

    :goto_0
    invoke-virtual {p0, v2, v4}, Lcom/android/internal/widget/GridLayoutManager;->setMeasuredDimension(II)V

    return-void
.end method

.method public setSpanCount(I)V
    .locals 3

    iget v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanCount:I

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mPendingSpanCountChange:Z

    if-lt p1, v0, :cond_1

    iput p1, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanCount:I

    iget-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanSizeLookup:Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;

    invoke-virtual {v0}, Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;->invalidateSpanIndexCache()V

    invoke-virtual {p0}, Lcom/android/internal/widget/GridLayoutManager;->requestLayout()V

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Span count should be at least 1. Provided "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setSpanSizeLookup(Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;)V
    .locals 0

    iput-object p1, p0, Lcom/android/internal/widget/GridLayoutManager;->mSpanSizeLookup:Lcom/android/internal/widget/GridLayoutManager$SpanSizeLookup;

    return-void
.end method

.method public setStackFromEnd(Z)V
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-super {p0, v0}, Lcom/android/internal/widget/LinearLayoutManager;->setStackFromEnd(Z)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "GridLayoutManager does not support stack from end. Consider using reverse layout"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public supportsPredictiveItemAnimations()Z
    .locals 1

    iget-object v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mPendingSavedState:Lcom/android/internal/widget/LinearLayoutManager$SavedState;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/internal/widget/GridLayoutManager;->mPendingSpanCountChange:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
