.class public final Lcom/android/internal/widget/RecyclerView$Recycler;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/RecyclerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "Recycler"
.end annotation


# static fields
.field static final DEFAULT_CACHE_SIZE:I = 0x2


# instance fields
.field final mAttachedScrap:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/internal/widget/RecyclerView$ViewHolder;",
            ">;"
        }
    .end annotation
.end field

.field final mCachedViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/internal/widget/RecyclerView$ViewHolder;",
            ">;"
        }
    .end annotation
.end field

.field mChangedScrap:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/internal/widget/RecyclerView$ViewHolder;",
            ">;"
        }
    .end annotation
.end field

.field mRecyclerPool:Lcom/android/internal/widget/RecyclerView$RecycledViewPool;

.field private mRequestedCacheMax:I

.field private final mUnmodifiableAttachedScrap:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/internal/widget/RecyclerView$ViewHolder;",
            ">;"
        }
    .end annotation
.end field

.field private mViewCacheExtension:Lcom/android/internal/widget/RecyclerView$ViewCacheExtension;

.field mViewCacheMax:I

.field final synthetic this$0:Lcom/android/internal/widget/RecyclerView;


# direct methods
.method public constructor <init>(Lcom/android/internal/widget/RecyclerView;)V
    .locals 2

    iput-object p1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mAttachedScrap:Ljava/util/ArrayList;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mChangedScrap:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    nop

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mUnmodifiableAttachedScrap:Ljava/util/List;

    const/4 v0, 0x2

    iput v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mRequestedCacheMax:I

    iput v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mViewCacheMax:I

    return-void
.end method

.method private attachAccessibilityDelegate(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView;->isAccessibilityEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getImportantForAccessibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setImportantForAccessibility(I)V

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getAccessibilityDelegate()Landroid/view/View$AccessibilityDelegate;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mAccessibilityDelegate:Lcom/android/internal/widget/RecyclerViewAccessibilityDelegate;

    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerViewAccessibilityDelegate;->getItemDelegate()Landroid/view/View$AccessibilityDelegate;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    :cond_1
    return-void
.end method

.method private invalidateDisplayListInt(Landroid/view/ViewGroup;Z)V
    .locals 4

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    :goto_0
    if-ltz v0, :cond_1

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v3, v2, Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    move-object v3, v2

    check-cast v3, Landroid/view/ViewGroup;

    invoke-direct {p0, v3, v1}, Lcom/android/internal/widget/RecyclerView$Recycler;->invalidateDisplayListInt(Landroid/view/ViewGroup;Z)V

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    if-nez p2, :cond_2

    return-void

    :cond_2
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    :goto_1
    return-void
.end method

.method private invalidateDisplayListInt(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V
    .locals 2

    iget-object v0, p1, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/internal/widget/RecyclerView$Recycler;->invalidateDisplayListInt(Landroid/view/ViewGroup;Z)V

    :cond_0
    return-void
.end method

.method private tryBindViewHolderByDeadline(Lcom/android/internal/widget/RecyclerView$ViewHolder;IIJ)Z
    .locals 9

    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    iput-object v0, p1, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mOwnerRecyclerView:Lcom/android/internal/widget/RecyclerView;

    invoke-virtual {p1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v0

    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    invoke-virtual {v1}, Lcom/android/internal/widget/RecyclerView;->getNanoTime()J

    move-result-wide v7

    const-wide v1, 0x7fffffffffffffffL

    cmp-long v1, p4, v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mRecyclerPool:Lcom/android/internal/widget/RecyclerView$RecycledViewPool;

    move v2, v0

    move-wide v3, v7

    move-wide v5, p4

    invoke-virtual/range {v1 .. v6}, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->willBindInTime(IJJ)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    return v1

    :cond_0
    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    iget-object v1, v1, Lcom/android/internal/widget/RecyclerView;->mAdapter:Lcom/android/internal/widget/RecyclerView$Adapter;

    invoke-virtual {v1, p1, p2}, Lcom/android/internal/widget/RecyclerView$Adapter;->bindViewHolder(Lcom/android/internal/widget/RecyclerView$ViewHolder;I)V

    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    invoke-virtual {v1}, Lcom/android/internal/widget/RecyclerView;->getNanoTime()J

    move-result-wide v1

    iget-object v3, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mRecyclerPool:Lcom/android/internal/widget/RecyclerView$RecycledViewPool;

    invoke-virtual {p1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v4

    sub-long v5, v1, v7

    invoke-virtual {v3, v4, v5, v6}, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->factorInBindTime(IJ)V

    iget-object v3, p1, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-direct {p0, v3}, Lcom/android/internal/widget/RecyclerView$Recycler;->attachAccessibilityDelegate(Landroid/view/View;)V

    iget-object v3, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    iget-object v3, v3, Lcom/android/internal/widget/RecyclerView;->mState:Lcom/android/internal/widget/RecyclerView$State;

    invoke-virtual {v3}, Lcom/android/internal/widget/RecyclerView$State;->isPreLayout()Z

    move-result v3

    if-eqz v3, :cond_1

    iput p3, p1, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPreLayoutPosition:I

    :cond_1
    const/4 v3, 0x1

    return v3
.end method


# virtual methods
.method addViewHolderToRecycledViewPool(Lcom/android/internal/widget/RecyclerView$ViewHolder;Z)V
    .locals 2

    goto/32 :goto_9

    nop

    :goto_0
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->putRecycledView(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_5

    nop

    :goto_1
    invoke-virtual {p0}, Lcom/android/internal/widget/RecyclerView$Recycler;->getRecycledViewPool()Lcom/android/internal/widget/RecyclerView$RecycledViewPool;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_2
    iput-object v1, p1, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mOwnerRecyclerView:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_1

    nop

    :goto_3
    iget-object v0, p1, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_6

    nop

    :goto_4
    if-nez p2, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_7

    nop

    :goto_5
    return-void

    :goto_6
    const/4 v1, 0x0

    goto/32 :goto_a

    nop

    :goto_7
    invoke-virtual {p0, p1}, Lcom/android/internal/widget/RecyclerView$Recycler;->dispatchViewRecycled(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V

    :goto_8
    goto/32 :goto_2

    nop

    :goto_9
    invoke-static {p1}, Lcom/android/internal/widget/RecyclerView;->clearNestedRecyclerViewIfNotNested(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_3

    nop

    :goto_a
    invoke-virtual {v0, v1}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    goto/32 :goto_4

    nop
.end method

.method public bindViewToPosition(Landroid/view/View;I)V
    .locals 8

    invoke-static {p1}, Lcom/android/internal/widget/RecyclerView;->getChildViewHolderInt(Landroid/view/View;)Lcom/android/internal/widget/RecyclerView$ViewHolder;

    move-result-object v6

    if-eqz v6, :cond_4

    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mAdapterHelper:Lcom/android/internal/widget/AdapterHelper;

    invoke-virtual {v0, p2}, Lcom/android/internal/widget/AdapterHelper;->findPositionOffset(I)I

    move-result v7

    if-ltz v7, :cond_3

    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mAdapter:Lcom/android/internal/widget/RecyclerView$Adapter;

    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView$Adapter;->getItemCount()I

    move-result v0

    if-ge v7, v0, :cond_3

    const-wide v4, 0x7fffffffffffffffL

    move-object v0, p0

    move-object v1, v6

    move v2, v7

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/internal/widget/RecyclerView$Recycler;->tryBindViewHolderByDeadline(Lcom/android/internal/widget/RecyclerView$ViewHolder;IIJ)Z

    iget-object v0, v6, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    invoke-virtual {v1}, Lcom/android/internal/widget/RecyclerView;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/android/internal/widget/RecyclerView$LayoutParams;

    iget-object v2, v6, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    invoke-virtual {v1, v0}, Lcom/android/internal/widget/RecyclerView;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    invoke-virtual {v1, v0}, Lcom/android/internal/widget/RecyclerView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/android/internal/widget/RecyclerView$LayoutParams;

    iget-object v2, v6, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_1
    move-object v1, v0

    check-cast v1, Lcom/android/internal/widget/RecyclerView$LayoutParams;

    :goto_0
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/android/internal/widget/RecyclerView$LayoutParams;->mInsetsDirty:Z

    iput-object v6, v1, Lcom/android/internal/widget/RecyclerView$LayoutParams;->mViewHolder:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    iget-object v3, v6, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-nez v3, :cond_2

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    iput-boolean v2, v1, Lcom/android/internal/widget/RecyclerView$LayoutParams;->mPendingInvalidate:Z

    return-void

    :cond_3
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Inconsistency detected. Invalid item position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(offset:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ").state:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    iget-object v2, v2, Lcom/android/internal/widget/RecyclerView;->mState:Lcom/android/internal/widget/RecyclerView$State;

    invoke-virtual {v2}, Lcom/android/internal/widget/RecyclerView$State;->getItemCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The view does not have a ViewHolder. You cannot pass arbitrary views to this method, they should be created by the Adapter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mAttachedScrap:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {p0}, Lcom/android/internal/widget/RecyclerView$Recycler;->recycleAndClearCachedViews()V

    return-void
.end method

.method clearOldPositions()V
    .locals 5

    goto/32 :goto_2

    nop

    :goto_0
    const/4 v3, 0x0

    :goto_1
    goto/32 :goto_1b

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_f

    nop

    :goto_3
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_13

    nop

    :goto_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    goto/32 :goto_0

    nop

    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_7

    nop

    :goto_6
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_e

    nop

    :goto_7
    goto :goto_a

    :goto_8
    goto/32 :goto_20

    nop

    :goto_9
    const/4 v1, 0x0

    :goto_a
    goto/32 :goto_12

    nop

    :goto_b
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_c

    nop

    :goto_c
    goto :goto_1a

    :goto_d
    goto/32 :goto_1d

    nop

    :goto_e
    check-cast v3, Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_21

    nop

    :goto_f
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_9

    nop

    :goto_10
    iget-object v4, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mChangedScrap:Ljava/util/ArrayList;

    goto/32 :goto_23

    nop

    :goto_11
    invoke-virtual {v2}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->clearOldPosition()V

    goto/32 :goto_5

    nop

    :goto_12
    if-lt v1, v0, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_24

    nop

    :goto_13
    check-cast v2, Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_11

    nop

    :goto_14
    add-int/lit8 v3, v3, 0x1

    goto/32 :goto_15

    nop

    :goto_15
    goto :goto_1

    :goto_16
    goto/32 :goto_17

    nop

    :goto_17
    return-void

    :goto_18
    check-cast v4, Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_22

    nop

    :goto_19
    const/4 v2, 0x0

    :goto_1a
    goto/32 :goto_25

    nop

    :goto_1b
    if-lt v3, v2, :cond_1

    goto/32 :goto_16

    :cond_1
    goto/32 :goto_10

    nop

    :goto_1c
    if-nez v2, :cond_2

    goto/32 :goto_16

    :cond_2
    goto/32 :goto_4

    nop

    :goto_1d
    iget-object v2, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mChangedScrap:Ljava/util/ArrayList;

    goto/32 :goto_1c

    nop

    :goto_1e
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_19

    nop

    :goto_1f
    iget-object v3, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mAttachedScrap:Ljava/util/ArrayList;

    goto/32 :goto_6

    nop

    :goto_20
    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mAttachedScrap:Ljava/util/ArrayList;

    goto/32 :goto_1e

    nop

    :goto_21
    invoke-virtual {v3}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->clearOldPosition()V

    goto/32 :goto_b

    nop

    :goto_22
    invoke-virtual {v4}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->clearOldPosition()V

    goto/32 :goto_14

    nop

    :goto_23
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_18

    nop

    :goto_24
    iget-object v2, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_3

    nop

    :goto_25
    if-lt v2, v1, :cond_3

    goto/32 :goto_d

    :cond_3
    goto/32 :goto_1f

    nop
.end method

.method clearScrap()V
    .locals 1

    goto/32 :goto_6

    nop

    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :goto_1
    goto/32 :goto_5

    nop

    :goto_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto/32 :goto_4

    nop

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_0

    nop

    :goto_4
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mChangedScrap:Ljava/util/ArrayList;

    goto/32 :goto_3

    nop

    :goto_5
    return-void

    :goto_6
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mAttachedScrap:Ljava/util/ArrayList;

    goto/32 :goto_2

    nop
.end method

.method public convertPreLayoutPositionToPostLayout(I)I
    .locals 3

    if-ltz p1, :cond_1

    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mState:Lcom/android/internal/widget/RecyclerView$State;

    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView$State;->getItemCount()I

    move-result v0

    if-ge p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mState:Lcom/android/internal/widget/RecyclerView$State;

    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView$State;->isPreLayout()Z

    move-result v0

    if-nez v0, :cond_0

    return p1

    :cond_0
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mAdapterHelper:Lcom/android/internal/widget/AdapterHelper;

    invoke-virtual {v0, p1}, Lcom/android/internal/widget/AdapterHelper;->findPositionOffset(I)I

    move-result v0

    return v0

    :cond_1
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "invalid position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". State item count is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    iget-object v2, v2, Lcom/android/internal/widget/RecyclerView;->mState:Lcom/android/internal/widget/RecyclerView$State;

    invoke-virtual {v2}, Lcom/android/internal/widget/RecyclerView$State;->getItemCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method dispatchViewRecycled(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V
    .locals 1

    goto/32 :goto_d

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_15

    :cond_0
    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mViewInfoStore:Lcom/android/internal/widget/ViewInfoStore;

    goto/32 :goto_12

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_4

    nop

    :goto_3
    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mAdapter:Lcom/android/internal/widget/RecyclerView$Adapter;

    goto/32 :goto_0

    nop

    :goto_4
    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mAdapter:Lcom/android/internal/widget/RecyclerView$Adapter;

    goto/32 :goto_14

    nop

    :goto_5
    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mRecyclerListener:Lcom/android/internal/widget/RecyclerView$RecyclerListener;

    goto/32 :goto_9

    nop

    :goto_6
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_1

    nop

    :goto_7
    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mState:Lcom/android/internal/widget/RecyclerView$State;

    goto/32 :goto_e

    nop

    :goto_8
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_10

    nop

    :goto_9
    if-nez v0, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_8

    nop

    :goto_a
    invoke-interface {v0, p1}, Lcom/android/internal/widget/RecyclerView$RecyclerListener;->onViewRecycled(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V

    :goto_b
    goto/32 :goto_11

    nop

    :goto_c
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_7

    nop

    :goto_d
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_5

    nop

    :goto_e
    if-nez v0, :cond_2

    goto/32 :goto_13

    :cond_2
    goto/32 :goto_6

    nop

    :goto_f
    return-void

    :goto_10
    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mRecyclerListener:Lcom/android/internal/widget/RecyclerView$RecyclerListener;

    goto/32 :goto_a

    nop

    :goto_11
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_3

    nop

    :goto_12
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/ViewInfoStore;->removeViewHolder(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V

    :goto_13
    goto/32 :goto_f

    nop

    :goto_14
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/RecyclerView$Adapter;->onViewRecycled(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V

    :goto_15
    goto/32 :goto_c

    nop
.end method

.method getChangedScrapViewForPosition(I)Lcom/android/internal/widget/RecyclerView$ViewHolder;
    .locals 10

    goto/32 :goto_29

    nop

    :goto_0
    invoke-virtual {v7, v3}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->addFlags(I)V

    goto/32 :goto_a

    nop

    :goto_1
    iget-object v7, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mChangedScrap:Ljava/util/ArrayList;

    goto/32 :goto_23

    nop

    :goto_2
    invoke-virtual {v7}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->getItemId()J

    move-result-wide v8

    goto/32 :goto_d

    nop

    :goto_3
    invoke-virtual {v4}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->getLayoutPosition()I

    move-result v5

    goto/32 :goto_e

    nop

    :goto_4
    invoke-virtual {v4, v0}, Lcom/android/internal/widget/RecyclerView$Adapter;->getItemId(I)J

    move-result-wide v4

    goto/32 :goto_25

    nop

    :goto_5
    if-eqz v5, :cond_0

    goto/32 :goto_2e

    :cond_0
    goto/32 :goto_3

    nop

    :goto_6
    invoke-virtual {v4}, Lcom/android/internal/widget/RecyclerView$Adapter;->getItemCount()I

    move-result v4

    goto/32 :goto_2b

    nop

    :goto_7
    add-int/lit8 v6, v6, 0x1

    goto/32 :goto_19

    nop

    :goto_8
    iget-object v4, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_30

    nop

    :goto_9
    if-nez v0, :cond_1

    goto/32 :goto_33

    :cond_1
    goto/32 :goto_21

    nop

    :goto_a
    return-object v7

    :goto_b
    goto/32 :goto_7

    nop

    :goto_c
    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView$Adapter;->hasStableIds()Z

    move-result v0

    goto/32 :goto_11

    nop

    :goto_d
    cmp-long v8, v8, v4

    goto/32 :goto_28

    nop

    :goto_e
    if-eq v5, p1, :cond_2

    goto/32 :goto_2e

    :cond_2
    goto/32 :goto_36

    nop

    :goto_f
    const/4 v0, 0x0

    :goto_10
    goto/32 :goto_17

    nop

    :goto_11
    if-nez v0, :cond_3

    goto/32 :goto_1a

    :cond_3
    goto/32 :goto_37

    nop

    :goto_12
    iget-object v4, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_27

    nop

    :goto_13
    goto :goto_10

    :goto_14
    goto/32 :goto_20

    nop

    :goto_15
    if-gtz v0, :cond_4

    goto/32 :goto_1a

    :cond_4
    goto/32 :goto_12

    nop

    :goto_16
    invoke-virtual {v0, p1}, Lcom/android/internal/widget/AdapterHelper;->findPositionOffset(I)I

    move-result v0

    goto/32 :goto_15

    nop

    :goto_17
    const/16 v3, 0x20

    goto/32 :goto_24

    nop

    :goto_18
    if-lt v6, v2, :cond_5

    goto/32 :goto_1a

    :cond_5
    goto/32 :goto_1

    nop

    :goto_19
    goto :goto_26

    :goto_1a
    goto/32 :goto_32

    nop

    :goto_1b
    invoke-virtual {v4}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->wasReturnedFromScrap()Z

    move-result v5

    goto/32 :goto_5

    nop

    :goto_1c
    if-eqz v0, :cond_6

    goto/32 :goto_1e

    :cond_6
    goto/32 :goto_1d

    nop

    :goto_1d
    goto :goto_33

    :goto_1e
    goto/32 :goto_f

    nop

    :goto_1f
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_13

    nop

    :goto_20
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_2a

    nop

    :goto_21
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_39

    nop

    :goto_22
    return-object v1

    :goto_23
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    goto/32 :goto_35

    nop

    :goto_24
    if-lt v0, v2, :cond_7

    goto/32 :goto_14

    :cond_7
    goto/32 :goto_2c

    nop

    :goto_25
    const/4 v6, 0x0

    :goto_26
    goto/32 :goto_18

    nop

    :goto_27
    iget-object v4, v4, Lcom/android/internal/widget/RecyclerView;->mAdapter:Lcom/android/internal/widget/RecyclerView$Adapter;

    goto/32 :goto_6

    nop

    :goto_28
    if-eqz v8, :cond_8

    goto/32 :goto_b

    :cond_8
    goto/32 :goto_0

    nop

    :goto_29
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mChangedScrap:Ljava/util/ArrayList;

    goto/32 :goto_3a

    nop

    :goto_2a
    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mAdapter:Lcom/android/internal/widget/RecyclerView$Adapter;

    goto/32 :goto_c

    nop

    :goto_2b
    if-lt v0, v4, :cond_9

    goto/32 :goto_1a

    :cond_9
    goto/32 :goto_8

    nop

    :goto_2c
    iget-object v4, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mChangedScrap:Ljava/util/ArrayList;

    goto/32 :goto_2f

    nop

    :goto_2d
    return-object v4

    :goto_2e
    goto/32 :goto_1f

    nop

    :goto_2f
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_38

    nop

    :goto_30
    iget-object v4, v4, Lcom/android/internal/widget/RecyclerView;->mAdapter:Lcom/android/internal/widget/RecyclerView$Adapter;

    goto/32 :goto_4

    nop

    :goto_31
    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mAdapterHelper:Lcom/android/internal/widget/AdapterHelper;

    goto/32 :goto_16

    nop

    :goto_32
    return-object v1

    :goto_33
    goto/32 :goto_22

    nop

    :goto_34
    if-eqz v8, :cond_a

    goto/32 :goto_b

    :cond_a
    goto/32 :goto_2

    nop

    :goto_35
    check-cast v7, Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_3b

    nop

    :goto_36
    invoke-virtual {v4, v3}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->addFlags(I)V

    goto/32 :goto_2d

    nop

    :goto_37
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_31

    nop

    :goto_38
    check-cast v4, Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_1b

    nop

    :goto_39
    move v2, v0

    goto/32 :goto_1c

    nop

    :goto_3a
    const/4 v1, 0x0

    goto/32 :goto_9

    nop

    :goto_3b
    invoke-virtual {v7}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->wasReturnedFromScrap()Z

    move-result v8

    goto/32 :goto_34

    nop
.end method

.method getRecycledViewPool()Lcom/android/internal/widget/RecyclerView$RecycledViewPool;
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    iput-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mRecyclerPool:Lcom/android/internal/widget/RecyclerView$RecycledViewPool;

    :goto_1
    goto/32 :goto_3

    nop

    :goto_2
    if-eqz v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_6

    nop

    :goto_3
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mRecyclerPool:Lcom/android/internal/widget/RecyclerView$RecycledViewPool;

    goto/32 :goto_7

    nop

    :goto_4
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mRecyclerPool:Lcom/android/internal/widget/RecyclerView$RecycledViewPool;

    goto/32 :goto_2

    nop

    :goto_5
    invoke-direct {v0}, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;-><init>()V

    goto/32 :goto_0

    nop

    :goto_6
    new-instance v0, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;

    goto/32 :goto_5

    nop

    :goto_7
    return-object v0
.end method

.method getScrapCount()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mAttachedScrap:Ljava/util/ArrayList;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_2

    nop

    :goto_2
    return v0
.end method

.method public getScrapList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/android/internal/widget/RecyclerView$ViewHolder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mUnmodifiableAttachedScrap:Ljava/util/List;

    return-object v0
.end method

.method getScrapOrCachedViewForId(JIZ)Lcom/android/internal/widget/RecyclerView$ViewHolder;
    .locals 7

    goto/32 :goto_22

    nop

    :goto_0
    if-nez v3, :cond_0

    goto/32 :goto_17

    :cond_0
    goto/32 :goto_3f

    nop

    :goto_1
    if-eqz p4, :cond_1

    goto/32 :goto_45

    :cond_1
    goto/32 :goto_2f

    nop

    :goto_2
    iget-object v3, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_24

    nop

    :goto_3
    const/4 v5, 0x0

    goto/32 :goto_39

    nop

    :goto_4
    const/16 v3, 0x20

    goto/32 :goto_37

    nop

    :goto_5
    iget-object v4, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_3c

    nop

    :goto_6
    if-eqz v3, :cond_2

    goto/32 :goto_2b

    :cond_2
    goto/32 :goto_1c

    nop

    :goto_7
    cmp-long v3, v3, p1

    goto/32 :goto_31

    nop

    :goto_8
    if-gez v2, :cond_3

    goto/32 :goto_21

    :cond_3
    goto/32 :goto_5

    nop

    :goto_9
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_10

    nop

    :goto_a
    invoke-virtual {v2}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->wasReturnedFromScrap()Z

    move-result v3

    goto/32 :goto_6

    nop

    :goto_b
    const/4 v3, 0x0

    goto/32 :goto_8

    nop

    :goto_c
    invoke-virtual {v2}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->getItemId()J

    move-result-wide v3

    goto/32 :goto_7

    nop

    :goto_d
    if-eqz v5, :cond_4

    goto/32 :goto_14

    :cond_4
    goto/32 :goto_1e

    nop

    :goto_e
    return-object v4

    :goto_f
    goto/32 :goto_35

    nop

    :goto_10
    add-int/lit8 v1, v0, -0x1

    :goto_11
    goto/32 :goto_2d

    nop

    :goto_12
    check-cast v4, Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_43

    nop

    :goto_13
    return-object v3

    :goto_14
    goto/32 :goto_34

    nop

    :goto_15
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_27

    nop

    :goto_16
    invoke-virtual {v2, v3, v4}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->setFlags(II)V

    :goto_17
    goto/32 :goto_3a

    nop

    :goto_18
    cmp-long v5, v5, p1

    goto/32 :goto_d

    nop

    :goto_19
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_40

    nop

    :goto_1a
    goto :goto_11

    :goto_1b
    goto/32 :goto_3e

    nop

    :goto_1c
    invoke-virtual {v2}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v3

    goto/32 :goto_3d

    nop

    :goto_1d
    add-int/lit8 v1, v1, -0x1

    goto/32 :goto_1a

    nop

    :goto_1e
    invoke-virtual {v4}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v5

    goto/32 :goto_30

    nop

    :goto_1f
    if-eqz v3, :cond_5

    goto/32 :goto_17

    :cond_5
    goto/32 :goto_32

    nop

    :goto_20
    goto :goto_28

    :goto_21
    goto/32 :goto_25

    nop

    :goto_22
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mAttachedScrap:Ljava/util/ArrayList;

    goto/32 :goto_9

    nop

    :goto_23
    iget-object v3, v3, Lcom/android/internal/widget/RecyclerView;->mState:Lcom/android/internal/widget/RecyclerView$State;

    goto/32 :goto_42

    nop

    :goto_24
    iget-object v4, v2, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_3

    nop

    :goto_25
    return-object v3

    :goto_26
    iget-object v3, v2, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_2a

    nop

    :goto_27
    add-int/lit8 v2, v1, -0x1

    :goto_28
    goto/32 :goto_b

    nop

    :goto_29
    iget-object v2, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mAttachedScrap:Ljava/util/ArrayList;

    goto/32 :goto_19

    nop

    :goto_2a
    invoke-virtual {p0, v3}, Lcom/android/internal/widget/RecyclerView$Recycler;->quickRecycleScrapView(Landroid/view/View;)V

    :goto_2b
    goto/32 :goto_1d

    nop

    :goto_2c
    const/16 v4, 0xe

    goto/32 :goto_16

    nop

    :goto_2d
    if-gez v1, :cond_6

    goto/32 :goto_1b

    :cond_6
    goto/32 :goto_29

    nop

    :goto_2e
    iget-object v3, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mAttachedScrap:Ljava/util/ArrayList;

    goto/32 :goto_36

    nop

    :goto_2f
    iget-object v3, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_44

    nop

    :goto_30
    if-eq p3, v5, :cond_7

    goto/32 :goto_f

    :cond_7
    goto/32 :goto_1

    nop

    :goto_31
    if-eqz v3, :cond_8

    goto/32 :goto_2b

    :cond_8
    goto/32 :goto_a

    nop

    :goto_32
    const/4 v3, 0x2

    goto/32 :goto_2c

    nop

    :goto_33
    if-eqz p4, :cond_9

    goto/32 :goto_2b

    :cond_9
    goto/32 :goto_2e

    nop

    :goto_34
    add-int/lit8 v2, v2, -0x1

    goto/32 :goto_20

    nop

    :goto_35
    if-eqz p4, :cond_a

    goto/32 :goto_14

    :cond_a
    goto/32 :goto_38

    nop

    :goto_36
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto/32 :goto_2

    nop

    :goto_37
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->addFlags(I)V

    goto/32 :goto_41

    nop

    :goto_38
    invoke-virtual {p0, v2}, Lcom/android/internal/widget/RecyclerView$Recycler;->recycleCachedViewAt(I)V

    goto/32 :goto_13

    nop

    :goto_39
    invoke-virtual {v3, v4, v5}, Lcom/android/internal/widget/RecyclerView;->removeDetachedView(Landroid/view/View;Z)V

    goto/32 :goto_26

    nop

    :goto_3a
    return-object v2

    :goto_3b
    goto/32 :goto_33

    nop

    :goto_3c
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_12

    nop

    :goto_3d
    if-eq p3, v3, :cond_b

    goto/32 :goto_3b

    :cond_b
    goto/32 :goto_4

    nop

    :goto_3e
    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_15

    nop

    :goto_3f
    iget-object v3, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_23

    nop

    :goto_40
    check-cast v2, Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_c

    nop

    :goto_41
    invoke-virtual {v2}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isRemoved()Z

    move-result v3

    goto/32 :goto_0

    nop

    :goto_42
    invoke-virtual {v3}, Lcom/android/internal/widget/RecyclerView$State;->isPreLayout()Z

    move-result v3

    goto/32 :goto_1f

    nop

    :goto_43
    invoke-virtual {v4}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->getItemId()J

    move-result-wide v5

    goto/32 :goto_18

    nop

    :goto_44
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :goto_45
    goto/32 :goto_e

    nop
.end method

.method getScrapOrHiddenOrCachedHolderForPosition(IZ)Lcom/android/internal/widget/RecyclerView$ViewHolder;
    .locals 7

    goto/32 :goto_28

    nop

    :goto_0
    invoke-virtual {v2}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isRemoved()Z

    move-result v3

    goto/32 :goto_42

    nop

    :goto_1
    invoke-virtual {v3}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->getLayoutPosition()I

    move-result v4

    goto/32 :goto_2b

    nop

    :goto_2
    check-cast v3, Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_23

    nop

    :goto_3
    invoke-virtual {v2}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isInvalid()Z

    move-result v3

    goto/32 :goto_30

    nop

    :goto_4
    if-eqz v3, :cond_0

    goto/32 :goto_43

    :cond_0
    goto/32 :goto_0

    nop

    :goto_5
    if-eqz v3, :cond_1

    goto/32 :goto_40

    :cond_1
    goto/32 :goto_a

    nop

    :goto_6
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_26

    nop

    :goto_7
    if-eq v3, p1, :cond_2

    goto/32 :goto_40

    :cond_2
    goto/32 :goto_3

    nop

    :goto_8
    new-instance v5, Ljava/lang/StringBuilder;

    goto/32 :goto_35

    nop

    :goto_9
    iget-boolean v3, v3, Lcom/android/internal/widget/RecyclerView$State;->mInPreLayout:Z

    goto/32 :goto_4

    nop

    :goto_a
    invoke-virtual {v2}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->getLayoutPosition()I

    move-result v3

    goto/32 :goto_7

    nop

    :goto_b
    iget-object v3, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_4a

    nop

    :goto_c
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_29

    nop

    :goto_d
    iget-object v3, v3, Lcom/android/internal/widget/RecyclerView;->mState:Lcom/android/internal/widget/RecyclerView$State;

    goto/32 :goto_9

    nop

    :goto_e
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/32 :goto_1c

    nop

    :goto_f
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_14

    nop

    :goto_10
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_1d

    nop

    :goto_11
    iget-object v4, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_18

    nop

    :goto_12
    return-object v3

    :goto_13
    goto/32 :goto_f

    nop

    :goto_14
    goto :goto_2a

    :goto_15
    goto/32 :goto_47

    nop

    :goto_16
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/32 :goto_e

    nop

    :goto_17
    const/16 v4, 0x2020

    goto/32 :goto_34

    nop

    :goto_18
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :goto_19
    goto/32 :goto_12

    nop

    :goto_1a
    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_21

    nop

    :goto_1b
    iget-object v2, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mAttachedScrap:Ljava/util/ArrayList;

    goto/32 :goto_6

    nop

    :goto_1c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_49

    nop

    :goto_1d
    const/4 v1, 0x0

    :goto_1e
    goto/32 :goto_4c

    nop

    :goto_1f
    return-object v2

    :goto_20
    goto/32 :goto_24

    nop

    :goto_21
    iget-object v1, v1, Lcom/android/internal/widget/RecyclerView;->mChildHelper:Lcom/android/internal/widget/ChildHelper;

    goto/32 :goto_27

    nop

    :goto_22
    iget-object v3, v3, Lcom/android/internal/widget/RecyclerView;->mChildHelper:Lcom/android/internal/widget/ChildHelper;

    goto/32 :goto_48

    nop

    :goto_23
    invoke-virtual {v3}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isInvalid()Z

    move-result v4

    goto/32 :goto_45

    nop

    :goto_24
    new-instance v4, Ljava/lang/IllegalStateException;

    goto/32 :goto_8

    nop

    :goto_25
    if-eqz p2, :cond_3

    goto/32 :goto_4f

    :cond_3
    goto/32 :goto_1a

    nop

    :goto_26
    check-cast v2, Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_2d

    nop

    :goto_27
    invoke-virtual {v1, p1}, Lcom/android/internal/widget/ChildHelper;->findHiddenNonRemovedView(I)Landroid/view/View;

    move-result-object v1

    goto/32 :goto_50

    nop

    :goto_28
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mAttachedScrap:Ljava/util/ArrayList;

    goto/32 :goto_10

    nop

    :goto_29
    const/4 v2, 0x0

    :goto_2a
    goto/32 :goto_37

    nop

    :goto_2b
    if-eq v4, p1, :cond_4

    goto/32 :goto_13

    :cond_4
    goto/32 :goto_3c

    nop

    :goto_2c
    if-ne v3, v4, :cond_5

    goto/32 :goto_20

    :cond_5
    goto/32 :goto_4d

    nop

    :goto_2d
    invoke-virtual {v2}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->wasReturnedFromScrap()Z

    move-result v3

    goto/32 :goto_5

    nop

    :goto_2e
    invoke-virtual {v4, v3}, Lcom/android/internal/widget/ChildHelper;->detachViewFromParent(I)V

    goto/32 :goto_3e

    nop

    :goto_2f
    iget-object v3, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_52

    nop

    :goto_30
    if-eqz v3, :cond_6

    goto/32 :goto_40

    :cond_6
    goto/32 :goto_39

    nop

    :goto_31
    goto :goto_1e

    :goto_32
    goto/32 :goto_25

    nop

    :goto_33
    const/4 v4, -0x1

    goto/32 :goto_2c

    nop

    :goto_34
    invoke-virtual {v2, v4}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->addFlags(I)V

    goto/32 :goto_1f

    nop

    :goto_35
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_44

    nop

    :goto_36
    iget-object v4, v4, Lcom/android/internal/widget/RecyclerView;->mChildHelper:Lcom/android/internal/widget/ChildHelper;

    goto/32 :goto_2e

    nop

    :goto_37
    if-lt v2, v1, :cond_7

    goto/32 :goto_15

    :cond_7
    goto/32 :goto_2f

    nop

    :goto_38
    const/16 v3, 0x20

    goto/32 :goto_41

    nop

    :goto_39
    iget-object v3, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_d

    nop

    :goto_3a
    return-object v2

    :goto_3b
    invoke-static {v1}, Lcom/android/internal/widget/RecyclerView;->getChildViewHolderInt(Landroid/view/View;)Lcom/android/internal/widget/RecyclerView$ViewHolder;

    move-result-object v2

    goto/32 :goto_b

    nop

    :goto_3c
    if-eqz p2, :cond_8

    goto/32 :goto_19

    :cond_8
    goto/32 :goto_11

    nop

    :goto_3d
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_31

    nop

    :goto_3e
    invoke-virtual {p0, v1}, Lcom/android/internal/widget/RecyclerView$Recycler;->scrapView(Landroid/view/View;)V

    goto/32 :goto_17

    nop

    :goto_3f
    return-object v2

    :goto_40
    goto/32 :goto_3d

    nop

    :goto_41
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->addFlags(I)V

    goto/32 :goto_3f

    nop

    :goto_42
    if-eqz v3, :cond_9

    goto/32 :goto_40

    :cond_9
    :goto_43
    goto/32 :goto_38

    nop

    :goto_44
    const-string/jumbo v6, "layout index should not be -1 after unhiding a view:"

    goto/32 :goto_16

    nop

    :goto_45
    if-eqz v4, :cond_a

    goto/32 :goto_13

    :cond_a
    goto/32 :goto_1

    nop

    :goto_46
    invoke-virtual {v3, v1}, Lcom/android/internal/widget/ChildHelper;->unhide(Landroid/view/View;)V

    goto/32 :goto_4b

    nop

    :goto_47
    const/4 v2, 0x0

    goto/32 :goto_3a

    nop

    :goto_48
    invoke-virtual {v3, v1}, Lcom/android/internal/widget/ChildHelper;->indexOfChild(Landroid/view/View;)I

    move-result v3

    goto/32 :goto_33

    nop

    :goto_49
    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_4e

    nop

    :goto_4a
    iget-object v3, v3, Lcom/android/internal/widget/RecyclerView;->mChildHelper:Lcom/android/internal/widget/ChildHelper;

    goto/32 :goto_46

    nop

    :goto_4b
    iget-object v3, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_22

    nop

    :goto_4c
    if-lt v1, v0, :cond_b

    goto/32 :goto_32

    :cond_b
    goto/32 :goto_1b

    nop

    :goto_4d
    iget-object v4, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_36

    nop

    :goto_4e
    throw v4

    :goto_4f
    goto/32 :goto_51

    nop

    :goto_50
    if-nez v1, :cond_c

    goto/32 :goto_4f

    :cond_c
    goto/32 :goto_3b

    nop

    :goto_51
    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_c

    nop

    :goto_52
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_2

    nop
.end method

.method getScrapViewAt(I)Landroid/view/View;
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_1
    check-cast v0, Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_4

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mAttachedScrap:Ljava/util/ArrayList;

    goto/32 :goto_0

    nop

    :goto_3
    return-object v0

    :goto_4
    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_3

    nop
.end method

.method public getViewForPosition(I)Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/internal/widget/RecyclerView$Recycler;->getViewForPosition(IZ)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method getViewForPosition(IZ)Landroid/view/View;
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    const-wide v0, 0x7fffffffffffffffL

    goto/32 :goto_3

    nop

    :goto_1
    return-object v0

    :goto_2
    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_1

    nop

    :goto_3
    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/android/internal/widget/RecyclerView$Recycler;->tryGetViewHolderForPositionByDeadline(IZJ)Lcom/android/internal/widget/RecyclerView$ViewHolder;

    move-result-object v0

    goto/32 :goto_2

    nop
.end method

.method markItemDecorInsetsDirty()V
    .locals 5

    goto/32 :goto_1

    nop

    :goto_0
    iget-object v2, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_7

    nop

    :goto_1
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_10

    nop

    :goto_2
    check-cast v3, Lcom/android/internal/widget/RecyclerView$LayoutParams;

    goto/32 :goto_6

    nop

    :goto_3
    iput-boolean v4, v3, Lcom/android/internal/widget/RecyclerView$LayoutParams;->mInsetsDirty:Z

    :goto_4
    goto/32 :goto_5

    nop

    :goto_5
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_11

    nop

    :goto_6
    if-nez v3, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_a

    nop

    :goto_7
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_e

    nop

    :goto_8
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    goto/32 :goto_2

    nop

    :goto_9
    return-void

    :goto_a
    const/4 v4, 0x1

    goto/32 :goto_3

    nop

    :goto_b
    const/4 v1, 0x0

    :goto_c
    goto/32 :goto_d

    nop

    :goto_d
    if-lt v1, v0, :cond_1

    goto/32 :goto_12

    :cond_1
    goto/32 :goto_0

    nop

    :goto_e
    check-cast v2, Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_f

    nop

    :goto_f
    iget-object v3, v2, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_8

    nop

    :goto_10
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_b

    nop

    :goto_11
    goto :goto_c

    :goto_12
    goto/32 :goto_9

    nop
.end method

.method markKnownViewsInvalid()V
    .locals 4

    goto/32 :goto_17

    nop

    :goto_0
    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView$Adapter;->hasStableIds()Z

    move-result v0

    goto/32 :goto_9

    nop

    :goto_1
    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mAdapter:Lcom/android/internal/widget/RecyclerView$Adapter;

    goto/32 :goto_0

    nop

    :goto_2
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->addFlags(I)V

    goto/32 :goto_13

    nop

    :goto_3
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_18

    nop

    :goto_4
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_15

    nop

    :goto_5
    if-nez v2, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_a

    nop

    :goto_6
    const/4 v1, 0x0

    :goto_7
    goto/32 :goto_12

    nop

    :goto_8
    iget-object v2, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_3

    nop

    :goto_9
    if-nez v0, :cond_1

    goto/32 :goto_11

    :cond_1
    goto/32 :goto_4

    nop

    :goto_a
    const/4 v3, 0x6

    goto/32 :goto_2

    nop

    :goto_b
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->addChangePayload(Ljava/lang/Object;)V

    :goto_c
    goto/32 :goto_1c

    nop

    :goto_d
    goto :goto_7

    :goto_e
    goto/32 :goto_10

    nop

    :goto_f
    return-void

    :goto_10
    goto :goto_1a

    :goto_11
    goto/32 :goto_19

    nop

    :goto_12
    if-lt v1, v0, :cond_2

    goto/32 :goto_e

    :cond_2
    goto/32 :goto_8

    nop

    :goto_13
    const/4 v3, 0x0

    goto/32 :goto_b

    nop

    :goto_14
    if-nez v0, :cond_3

    goto/32 :goto_11

    :cond_3
    goto/32 :goto_16

    nop

    :goto_15
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_6

    nop

    :goto_16
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_1

    nop

    :goto_17
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_1b

    nop

    :goto_18
    check-cast v2, Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_5

    nop

    :goto_19
    invoke-virtual {p0}, Lcom/android/internal/widget/RecyclerView$Recycler;->recycleAndClearCachedViews()V

    :goto_1a
    goto/32 :goto_f

    nop

    :goto_1b
    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mAdapter:Lcom/android/internal/widget/RecyclerView$Adapter;

    goto/32 :goto_14

    nop

    :goto_1c
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_d

    nop
.end method

.method offsetPositionRecordsForInsert(II)V
    .locals 4

    goto/32 :goto_9

    nop

    :goto_0
    if-ge v3, p1, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_7

    nop

    :goto_1
    return-void

    :goto_2
    goto :goto_e

    :goto_3
    goto/32 :goto_1

    nop

    :goto_4
    invoke-virtual {v2, p2, v3}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->offsetPosition(IZ)V

    :goto_5
    goto/32 :goto_10

    nop

    :goto_6
    if-nez v2, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_b

    nop

    :goto_7
    const/4 v3, 0x1

    goto/32 :goto_4

    nop

    :goto_8
    check-cast v2, Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_6

    nop

    :goto_9
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_11

    nop

    :goto_a
    iget-object v2, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_f

    nop

    :goto_b
    iget v3, v2, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPosition:I

    goto/32 :goto_0

    nop

    :goto_c
    if-lt v1, v0, :cond_2

    goto/32 :goto_3

    :cond_2
    goto/32 :goto_a

    nop

    :goto_d
    const/4 v1, 0x0

    :goto_e
    goto/32 :goto_c

    nop

    :goto_f
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_8

    nop

    :goto_10
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_2

    nop

    :goto_11
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_d

    nop
.end method

.method offsetPositionRecordsForMove(II)V
    .locals 8

    goto/32 :goto_c

    nop

    :goto_0
    const/4 v4, 0x0

    :goto_1
    goto/32 :goto_6

    nop

    :goto_2
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    goto/32 :goto_15

    nop

    :goto_3
    iget-object v5, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_2

    nop

    :goto_4
    return-void

    :goto_5
    move v0, p1

    goto/32 :goto_16

    nop

    :goto_6
    if-lt v4, v3, :cond_0

    goto/32 :goto_f

    :cond_0
    goto/32 :goto_3

    nop

    :goto_7
    iget v6, v5, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPosition:I

    goto/32 :goto_13

    nop

    :goto_8
    goto/16 :goto_24

    :goto_9
    goto/32 :goto_22

    nop

    :goto_a
    const/4 v2, 0x1

    :goto_b
    goto/32 :goto_21

    nop

    :goto_c
    if-lt p1, p2, :cond_1

    goto/32 :goto_1a

    :cond_1
    goto/32 :goto_5

    nop

    :goto_d
    add-int/lit8 v4, v4, 0x1

    goto/32 :goto_e

    nop

    :goto_e
    goto :goto_1

    :goto_f
    goto/32 :goto_4

    nop

    :goto_10
    move v1, p1

    goto/32 :goto_a

    nop

    :goto_11
    if-eq v6, p1, :cond_2

    goto/32 :goto_18

    :cond_2
    goto/32 :goto_25

    nop

    :goto_12
    if-ge v6, v0, :cond_3

    goto/32 :goto_24

    :cond_3
    goto/32 :goto_7

    nop

    :goto_13
    if-gt v6, v1, :cond_4

    goto/32 :goto_9

    :cond_4
    goto/32 :goto_8

    nop

    :goto_14
    iget v6, v5, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPosition:I

    goto/32 :goto_12

    nop

    :goto_15
    check-cast v5, Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_1e

    nop

    :goto_16
    move v1, p2

    goto/32 :goto_1c

    nop

    :goto_17
    goto :goto_24

    :goto_18
    goto/32 :goto_23

    nop

    :goto_19
    goto :goto_b

    :goto_1a
    goto/32 :goto_1b

    nop

    :goto_1b
    move v0, p2

    goto/32 :goto_10

    nop

    :goto_1c
    const/4 v2, -0x1

    goto/32 :goto_19

    nop

    :goto_1d
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    goto/32 :goto_0

    nop

    :goto_1e
    if-nez v5, :cond_5

    goto/32 :goto_24

    :cond_5
    goto/32 :goto_14

    nop

    :goto_1f
    const/4 v7, 0x0

    goto/32 :goto_11

    nop

    :goto_20
    invoke-virtual {v5, v6, v7}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->offsetPosition(IZ)V

    goto/32 :goto_17

    nop

    :goto_21
    iget-object v3, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_1d

    nop

    :goto_22
    iget v6, v5, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPosition:I

    goto/32 :goto_1f

    nop

    :goto_23
    invoke-virtual {v5, v2, v7}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->offsetPosition(IZ)V

    :goto_24
    goto/32 :goto_d

    nop

    :goto_25
    sub-int v6, p2, p1

    goto/32 :goto_20

    nop
.end method

.method offsetPositionRecordsForRemove(IIZ)V
    .locals 5

    goto/32 :goto_b

    nop

    :goto_0
    goto :goto_d

    :goto_1
    goto/32 :goto_a

    nop

    :goto_2
    if-ge v4, p1, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_19

    nop

    :goto_3
    invoke-virtual {v3, v4, p3}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->offsetPosition(IZ)V

    goto/32 :goto_0

    nop

    :goto_4
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_f

    nop

    :goto_5
    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_4

    nop

    :goto_6
    iget v4, v3, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPosition:I

    goto/32 :goto_17

    nop

    :goto_7
    invoke-virtual {v3, v4}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->addFlags(I)V

    goto/32 :goto_c

    nop

    :goto_8
    return-void

    :goto_9
    if-nez v3, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_6

    nop

    :goto_a
    iget v4, v3, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPosition:I

    goto/32 :goto_2

    nop

    :goto_b
    add-int v0, p1, p2

    goto/32 :goto_5

    nop

    :goto_c
    invoke-virtual {p0, v2}, Lcom/android/internal/widget/RecyclerView$Recycler;->recycleCachedViewAt(I)V

    :goto_d
    goto/32 :goto_12

    nop

    :goto_e
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_11

    nop

    :goto_f
    add-int/lit8 v2, v1, -0x1

    :goto_10
    goto/32 :goto_18

    nop

    :goto_11
    check-cast v3, Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_9

    nop

    :goto_12
    add-int/lit8 v2, v2, -0x1

    goto/32 :goto_14

    nop

    :goto_13
    neg-int v4, p2

    goto/32 :goto_3

    nop

    :goto_14
    goto :goto_10

    :goto_15
    goto/32 :goto_8

    nop

    :goto_16
    iget-object v3, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_e

    nop

    :goto_17
    if-ge v4, v0, :cond_2

    goto/32 :goto_1

    :cond_2
    goto/32 :goto_13

    nop

    :goto_18
    if-gez v2, :cond_3

    goto/32 :goto_15

    :cond_3
    goto/32 :goto_16

    nop

    :goto_19
    const/16 v4, 0x8

    goto/32 :goto_7

    nop
.end method

.method onAdapterChanged(Lcom/android/internal/widget/RecyclerView$Adapter;Lcom/android/internal/widget/RecyclerView$Adapter;Z)V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    invoke-virtual {p0}, Lcom/android/internal/widget/RecyclerView$Recycler;->getRecycledViewPool()Lcom/android/internal/widget/RecyclerView$RecycledViewPool;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->onAdapterChanged(Lcom/android/internal/widget/RecyclerView$Adapter;Lcom/android/internal/widget/RecyclerView$Adapter;Z)V

    goto/32 :goto_1

    nop

    :goto_3
    invoke-virtual {p0}, Lcom/android/internal/widget/RecyclerView$Recycler;->clear()V

    goto/32 :goto_0

    nop
.end method

.method quickRecycleScrapView(Landroid/view/View;)V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    invoke-static {v0, v1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->-$$Nest$fputmScrapContainer(Lcom/android/internal/widget/RecyclerView$ViewHolder;Lcom/android/internal/widget/RecyclerView$Recycler;)V

    goto/32 :goto_6

    nop

    :goto_1
    invoke-static {p1}, Lcom/android/internal/widget/RecyclerView;->getChildViewHolderInt(Landroid/view/View;)Lcom/android/internal/widget/RecyclerView$ViewHolder;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_2
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/RecyclerView$Recycler;->recycleViewHolderInternal(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_4

    nop

    :goto_3
    invoke-static {v0, v1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->-$$Nest$fputmInChangeScrap(Lcom/android/internal/widget/RecyclerView$ViewHolder;Z)V

    goto/32 :goto_7

    nop

    :goto_4
    return-void

    :goto_5
    const/4 v1, 0x0

    goto/32 :goto_0

    nop

    :goto_6
    const/4 v1, 0x0

    goto/32 :goto_3

    nop

    :goto_7
    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->clearReturnedFromScrapFlag()V

    goto/32 :goto_2

    nop
.end method

.method recycleAndClearCachedViews()V
    .locals 2

    goto/32 :goto_9

    nop

    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    goto/32 :goto_c

    nop

    :goto_1
    return-void

    :goto_2
    iget-object v1, v1, Lcom/android/internal/widget/RecyclerView;->mPrefetchRegistry:Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;

    goto/32 :goto_a

    nop

    :goto_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_4

    nop

    :goto_4
    add-int/lit8 v1, v0, -0x1

    :goto_5
    goto/32 :goto_6

    nop

    :goto_6
    if-gez v1, :cond_0

    goto/32 :goto_f

    :cond_0
    goto/32 :goto_11

    nop

    :goto_7
    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_0

    nop

    :goto_8
    add-int/lit8 v1, v1, -0x1

    goto/32 :goto_e

    nop

    :goto_9
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_3

    nop

    :goto_a
    invoke-virtual {v1}, Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;->clearPrefetchPositions()V

    :goto_b
    goto/32 :goto_1

    nop

    :goto_c
    invoke-static {}, Lcom/android/internal/widget/RecyclerView;->-$$Nest$sfgetALLOW_THREAD_GAP_WORK()Z

    move-result v1

    goto/32 :goto_10

    nop

    :goto_d
    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_2

    nop

    :goto_e
    goto :goto_5

    :goto_f
    goto/32 :goto_7

    nop

    :goto_10
    if-nez v1, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_d

    nop

    :goto_11
    invoke-virtual {p0, v1}, Lcom/android/internal/widget/RecyclerView$Recycler;->recycleCachedViewAt(I)V

    goto/32 :goto_8

    nop
.end method

.method recycleCachedViewAt(I)V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_2
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto/32 :goto_4

    nop

    :goto_3
    check-cast v0, Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_7

    nop

    :goto_4
    return-void

    :goto_5
    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_2

    nop

    :goto_6
    invoke-virtual {p0, v0, v1}, Lcom/android/internal/widget/RecyclerView$Recycler;->addViewHolderToRecycledViewPool(Lcom/android/internal/widget/RecyclerView$ViewHolder;Z)V

    goto/32 :goto_5

    nop

    :goto_7
    const/4 v1, 0x1

    goto/32 :goto_6

    nop
.end method

.method public recycleView(Landroid/view/View;)V
    .locals 3

    invoke-static {p1}, Lcom/android/internal/widget/RecyclerView;->getChildViewHolderInt(Landroid/view/View;)Lcom/android/internal/widget/RecyclerView$ViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isTmpDetached()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Lcom/android/internal/widget/RecyclerView;->removeDetachedView(Landroid/view/View;Z)V

    :cond_0
    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isScrap()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->unScrap()V

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->wasReturnedFromScrap()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->clearReturnedFromScrapFlag()V

    :cond_2
    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/RecyclerView$Recycler;->recycleViewHolderInternal(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V

    return-void
.end method

.method recycleViewHolderInternal(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V
    .locals 10

    goto/32 :goto_6a

    nop

    :goto_0
    const-string v1, "Trying to recycle an ignored view holder. You should first call stopIgnoringView(view) before calling recycle."

    goto/32 :goto_3c

    nop

    :goto_1
    invoke-static {}, Lcom/android/internal/widget/RecyclerView;->-$$Nest$sfgetALLOW_THREAD_GAP_WORK()Z

    move-result v7

    goto/32 :goto_35

    nop

    :goto_2
    goto/16 :goto_39

    :goto_3
    goto/32 :goto_19

    nop

    :goto_4
    move v1, v2

    :goto_5
    goto/32 :goto_46

    nop

    :goto_6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_5f

    nop

    :goto_7
    iget-object v3, v3, Lcom/android/internal/widget/RecyclerView;->mAdapter:Lcom/android/internal/widget/RecyclerView$Adapter;

    goto/32 :goto_3a

    nop

    :goto_8
    invoke-virtual {p0, v1}, Lcom/android/internal/widget/RecyclerView$Recycler;->recycleCachedViewAt(I)V

    goto/32 :goto_59

    nop

    :goto_9
    move v3, v1

    :goto_a
    goto/32 :goto_5b

    nop

    :goto_b
    if-nez v3, :cond_0

    goto/32 :goto_1d

    :cond_0
    goto/32 :goto_55

    nop

    :goto_c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_13

    nop

    :goto_d
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto/32 :goto_17

    nop

    :goto_e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_2e

    nop

    :goto_f
    new-instance v3, Ljava/lang/StringBuilder;

    goto/32 :goto_5e

    nop

    :goto_10
    const/4 v2, 0x1

    goto/32 :goto_18

    nop

    :goto_11
    if-eqz v9, :cond_1

    goto/32 :goto_2c

    :cond_1
    goto/32 :goto_2b

    nop

    :goto_12
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_69

    nop

    :goto_13
    const-string v2, "Tmp detached view should be removed from RecyclerView before it can be recycled: "

    goto/32 :goto_20

    nop

    :goto_14
    iget-object v8, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_70

    nop

    :goto_15
    iget v8, p1, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPosition:I

    goto/32 :goto_5d

    nop

    :goto_16
    check-cast v8, Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_36

    nop

    :goto_17
    if-nez v0, :cond_2

    goto/32 :goto_76

    :cond_2
    goto/32 :goto_75

    nop

    :goto_18
    if-eqz v0, :cond_3

    goto/32 :goto_3e

    :cond_3
    goto/32 :goto_6f

    nop

    :goto_19
    add-int/lit8 v1, v7, 0x1

    :goto_1a
    goto/32 :goto_33

    nop

    :goto_1b
    move v1, v6

    goto/32 :goto_1

    nop

    :goto_1c
    goto :goto_a

    :goto_1d
    goto/32 :goto_9

    nop

    :goto_1e
    const/4 v5, 0x0

    goto/32 :goto_61

    nop

    :goto_1f
    iget v7, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mViewCacheMax:I

    goto/32 :goto_31

    nop

    :goto_20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_32

    nop

    :goto_21
    if-gtz v6, :cond_4

    goto/32 :goto_58

    :cond_4
    goto/32 :goto_2f

    nop

    :goto_22
    invoke-virtual {p1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isTmpDetached()Z

    move-result v0

    goto/32 :goto_40

    nop

    :goto_23
    invoke-virtual {p1, v6}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->hasAnyOfTheFlags(I)Z

    move-result v6

    goto/32 :goto_72

    nop

    :goto_24
    invoke-virtual {v9, v8}, Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;->lastPrefetchIncludedPosition(I)Z

    move-result v9

    goto/32 :goto_11

    nop

    :goto_25
    iget-object v9, v9, Lcom/android/internal/widget/RecyclerView;->mPrefetchRegistry:Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;

    goto/32 :goto_24

    nop

    :goto_26
    if-nez v4, :cond_5

    goto/32 :goto_5

    :cond_5
    goto/32 :goto_4

    nop

    :goto_27
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_c

    nop

    :goto_28
    add-int/lit8 v7, v7, -0x1

    goto/32 :goto_2

    nop

    :goto_29
    invoke-virtual {p0, p1, v2}, Lcom/android/internal/widget/RecyclerView$Recycler;->addViewHolderToRecycledViewPool(Lcom/android/internal/widget/RecyclerView$ViewHolder;Z)V

    goto/32 :goto_4b

    nop

    :goto_2a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_27

    nop

    :goto_2b
    goto/16 :goto_3

    :goto_2c
    nop

    goto/32 :goto_28

    nop

    :goto_2d
    if-eqz v4, :cond_6

    goto/32 :goto_4c

    :cond_6
    goto/32 :goto_29

    nop

    :goto_2e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_3d

    nop

    :goto_2f
    const/16 v6, 0x20e

    goto/32 :goto_23

    nop

    :goto_30
    new-instance v0, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_0

    nop

    :goto_31
    if-ge v6, v7, :cond_7

    goto/32 :goto_5a

    :cond_7
    goto/32 :goto_4d

    nop

    :goto_32
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_e

    nop

    :goto_33
    iget-object v7, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_7c

    nop

    :goto_34
    iget-object v6, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_54

    nop

    :goto_35
    if-nez v7, :cond_8

    goto/32 :goto_1a

    :cond_8
    goto/32 :goto_44

    nop

    :goto_36
    iget v8, v8, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPosition:I

    goto/32 :goto_77

    nop

    :goto_37
    invoke-virtual {p1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->shouldIgnore()Z

    move-result v0

    goto/32 :goto_73

    nop

    :goto_38
    add-int/lit8 v7, v6, -0x1

    :goto_39
    goto/32 :goto_49

    nop

    :goto_3a
    if-nez v3, :cond_9

    goto/32 :goto_1d

    :cond_9
    goto/32 :goto_4e

    nop

    :goto_3b
    iget-object v7, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_6e

    nop

    :goto_3c
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_52

    nop

    :goto_3d
    throw v0

    :goto_3e
    goto/32 :goto_51

    nop

    :goto_3f
    if-eqz v4, :cond_a

    goto/32 :goto_68

    :cond_a
    goto/32 :goto_4a

    nop

    :goto_40
    if-eqz v0, :cond_b

    goto/32 :goto_53

    :cond_b
    goto/32 :goto_37

    nop

    :goto_41
    if-nez v6, :cond_c

    goto/32 :goto_4c

    :cond_c
    :goto_42
    goto/32 :goto_62

    nop

    :goto_43
    iget-object v3, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_78

    nop

    :goto_44
    if-gtz v6, :cond_d

    goto/32 :goto_1a

    :cond_d
    goto/32 :goto_3b

    nop

    :goto_45
    invoke-virtual {v4}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    goto/32 :goto_26

    nop

    :goto_46
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_64

    nop

    :goto_47
    invoke-static {p1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->-$$Nest$mdoesTransientStatePreventRecycling(Lcom/android/internal/widget/RecyclerView$ViewHolder;)Z

    move-result v0

    goto/32 :goto_7a

    nop

    :goto_48
    const/4 v1, 0x0

    goto/32 :goto_67

    nop

    :goto_49
    if-gez v7, :cond_e

    goto/32 :goto_3

    :cond_e
    goto/32 :goto_14

    nop

    :goto_4a
    if-eqz v5, :cond_f

    goto/32 :goto_68

    :cond_f
    goto/32 :goto_4f

    nop

    :goto_4b
    const/4 v5, 0x1

    :goto_4c
    goto/32 :goto_65

    nop

    :goto_4d
    if-gtz v6, :cond_10

    goto/32 :goto_5a

    :cond_10
    goto/32 :goto_8

    nop

    :goto_4e
    if-nez v0, :cond_11

    goto/32 :goto_1d

    :cond_11
    goto/32 :goto_43

    nop

    :goto_4f
    if-nez v0, :cond_12

    goto/32 :goto_68

    :cond_12
    goto/32 :goto_48

    nop

    :goto_50
    invoke-virtual {v3, p1}, Lcom/android/internal/widget/RecyclerView$Adapter;->onFailedToRecycleView(Lcom/android/internal/widget/RecyclerView$ViewHolder;)Z

    move-result v3

    goto/32 :goto_b

    nop

    :goto_51
    new-instance v0, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_f

    nop

    :goto_52
    throw v0

    :goto_53
    goto/32 :goto_2a

    nop

    :goto_54
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    goto/32 :goto_1f

    nop

    :goto_55
    move v3, v2

    goto/32 :goto_1c

    nop

    :goto_56
    const-string v4, "Scrapped or attached views may not be recycled. isScrap:"

    goto/32 :goto_6

    nop

    :goto_57
    const/4 v4, 0x1

    :goto_58
    goto/32 :goto_2d

    nop

    :goto_59
    add-int/lit8 v6, v6, -0x1

    :goto_5a
    goto/32 :goto_1b

    nop

    :goto_5b
    const/4 v4, 0x0

    goto/32 :goto_1e

    nop

    :goto_5c
    const/4 v1, 0x0

    goto/32 :goto_10

    nop

    :goto_5d
    invoke-virtual {v7, v8}, Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;->lastPrefetchIncludedPosition(I)Z

    move-result v7

    goto/32 :goto_79

    nop

    :goto_5e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_56

    nop

    :goto_5f
    invoke-virtual {p1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isScrap()Z

    move-result v4

    goto/32 :goto_7b

    nop

    :goto_60
    invoke-virtual {p1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isRecyclable()Z

    move-result v6

    goto/32 :goto_41

    nop

    :goto_61
    if-eqz v3, :cond_13

    goto/32 :goto_42

    :cond_13
    goto/32 :goto_60

    nop

    :goto_62
    iget v6, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mViewCacheMax:I

    goto/32 :goto_21

    nop

    :goto_63
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_66

    nop

    :goto_64
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_12

    nop

    :goto_65
    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_6b

    nop

    :goto_66
    iget-object v4, p1, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_45

    nop

    :goto_67
    iput-object v1, p1, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mOwnerRecyclerView:Lcom/android/internal/widget/RecyclerView;

    :goto_68
    goto/32 :goto_6c

    nop

    :goto_69
    throw v0

    :goto_6a
    invoke-virtual {p1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isScrap()Z

    move-result v0

    goto/32 :goto_5c

    nop

    :goto_6b
    iget-object v1, v1, Lcom/android/internal/widget/RecyclerView;->mViewInfoStore:Lcom/android/internal/widget/ViewInfoStore;

    goto/32 :goto_71

    nop

    :goto_6c
    return-void

    :goto_6d
    goto/32 :goto_30

    nop

    :goto_6e
    iget-object v7, v7, Lcom/android/internal/widget/RecyclerView;->mPrefetchRegistry:Lcom/android/internal/widget/GapWorker$LayoutPrefetchRegistryImpl;

    goto/32 :goto_15

    nop

    :goto_6f
    iget-object v0, p1, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_d

    nop

    :goto_70
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    goto/32 :goto_16

    nop

    :goto_71
    invoke-virtual {v1, p1}, Lcom/android/internal/widget/ViewInfoStore;->removeViewHolder(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_3f

    nop

    :goto_72
    if-eqz v6, :cond_14

    goto/32 :goto_58

    :cond_14
    goto/32 :goto_34

    nop

    :goto_73
    if-eqz v0, :cond_15

    goto/32 :goto_6d

    :cond_15
    nop

    goto/32 :goto_47

    nop

    :goto_74
    const-string v4, " isAttached:"

    goto/32 :goto_63

    nop

    :goto_75
    goto/16 :goto_3e

    :goto_76
    goto/32 :goto_22

    nop

    :goto_77
    iget-object v9, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_25

    nop

    :goto_78
    iget-object v3, v3, Lcom/android/internal/widget/RecyclerView;->mAdapter:Lcom/android/internal/widget/RecyclerView$Adapter;

    goto/32 :goto_50

    nop

    :goto_79
    if-eqz v7, :cond_16

    goto/32 :goto_1a

    :cond_16
    goto/32 :goto_38

    nop

    :goto_7a
    iget-object v3, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_7

    nop

    :goto_7b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_74

    nop

    :goto_7c
    invoke-virtual {v7, v1, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto/32 :goto_57

    nop
.end method

.method recycleViewInternal(Landroid/view/View;)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/RecyclerView$Recycler;->recycleViewHolderInternal(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_2

    nop

    :goto_1
    invoke-static {p1}, Lcom/android/internal/widget/RecyclerView;->getChildViewHolderInt(Landroid/view/View;)Lcom/android/internal/widget/RecyclerView$ViewHolder;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method scrapView(Landroid/view/View;)V
    .locals 3

    goto/32 :goto_11

    nop

    :goto_0
    iput-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mChangedScrap:Ljava/util/ArrayList;

    :goto_1
    goto/32 :goto_29

    nop

    :goto_2
    invoke-virtual {v1}, Lcom/android/internal/widget/RecyclerView$Adapter;->hasStableIds()Z

    move-result v1

    goto/32 :goto_18

    nop

    :goto_3
    const/4 v1, 0x0

    goto/32 :goto_2a

    nop

    :goto_4
    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mChangedScrap:Ljava/util/ArrayList;

    goto/32 :goto_2b

    nop

    :goto_5
    if-nez v1, :cond_0

    goto/32 :goto_1b

    :cond_0
    goto/32 :goto_1a

    nop

    :goto_6
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->hasAnyOfTheFlags(I)Z

    move-result v1

    goto/32 :goto_e

    nop

    :goto_7
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_1d

    nop

    :goto_8
    const-string v2, "Called scrap view with an invalid view. Invalid views cannot be reused from scrap, they should rebound from recycler pool."

    goto/32 :goto_7

    nop

    :goto_9
    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_25

    nop

    :goto_a
    const/16 v1, 0xc

    goto/32 :goto_6

    nop

    :goto_b
    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_20

    nop

    :goto_c
    goto/16 :goto_27

    :goto_d
    goto/32 :goto_14

    nop

    :goto_e
    if-eqz v1, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_16

    nop

    :goto_f
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_0

    nop

    :goto_10
    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mAttachedScrap:Ljava/util/ArrayList;

    goto/32 :goto_26

    nop

    :goto_11
    invoke-static {p1}, Lcom/android/internal/widget/RecyclerView;->getChildViewHolderInt(Landroid/view/View;)Lcom/android/internal/widget/RecyclerView$ViewHolder;

    move-result-object v0

    goto/32 :goto_a

    nop

    :goto_12
    if-nez v1, :cond_2

    goto/32 :goto_1e

    :cond_2
    goto/32 :goto_28

    nop

    :goto_13
    return-void

    :goto_14
    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isInvalid()Z

    move-result v1

    goto/32 :goto_12

    nop

    :goto_15
    if-eqz v1, :cond_3

    goto/32 :goto_1e

    :cond_3
    goto/32 :goto_b

    nop

    :goto_16
    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isUpdated()Z

    move-result v1

    goto/32 :goto_24

    nop

    :goto_17
    invoke-virtual {v0, p0, v1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->setScrapContainer(Lcom/android/internal/widget/RecyclerView$Recycler;Z)V

    goto/32 :goto_1f

    nop

    :goto_18
    if-nez v1, :cond_4

    goto/32 :goto_23

    :cond_4
    goto/32 :goto_22

    nop

    :goto_19
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_c

    nop

    :goto_1a
    goto :goto_d

    :goto_1b
    goto/32 :goto_4

    nop

    :goto_1c
    new-instance v1, Ljava/util/ArrayList;

    goto/32 :goto_f

    nop

    :goto_1d
    throw v1

    :goto_1e
    goto/32 :goto_3

    nop

    :goto_1f
    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mChangedScrap:Ljava/util/ArrayList;

    goto/32 :goto_19

    nop

    :goto_20
    iget-object v1, v1, Lcom/android/internal/widget/RecyclerView;->mAdapter:Lcom/android/internal/widget/RecyclerView$Adapter;

    goto/32 :goto_2

    nop

    :goto_21
    new-instance v1, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_8

    nop

    :goto_22
    goto :goto_1e

    :goto_23
    goto/32 :goto_21

    nop

    :goto_24
    if-nez v1, :cond_5

    goto/32 :goto_d

    :cond_5
    goto/32 :goto_9

    nop

    :goto_25
    invoke-virtual {v1, v0}, Lcom/android/internal/widget/RecyclerView;->canReuseUpdatedViewHolder(Lcom/android/internal/widget/RecyclerView$ViewHolder;)Z

    move-result v1

    goto/32 :goto_5

    nop

    :goto_26
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_27
    goto/32 :goto_13

    nop

    :goto_28
    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isRemoved()Z

    move-result v1

    goto/32 :goto_15

    nop

    :goto_29
    const/4 v1, 0x1

    goto/32 :goto_17

    nop

    :goto_2a
    invoke-virtual {v0, p0, v1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->setScrapContainer(Lcom/android/internal/widget/RecyclerView$Recycler;Z)V

    goto/32 :goto_10

    nop

    :goto_2b
    if-eqz v1, :cond_6

    goto/32 :goto_1

    :cond_6
    goto/32 :goto_1c

    nop
.end method

.method setAdapterPositionsAsUnknown()V
    .locals 4

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_5

    nop

    :goto_1
    iget-object v2, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_3

    nop

    :goto_3
    check-cast v2, Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_4

    nop

    :goto_4
    if-nez v2, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_8

    nop

    :goto_5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_e

    nop

    :goto_6
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_b

    nop

    :goto_7
    if-lt v1, v0, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_1

    nop

    :goto_8
    const/16 v3, 0x200

    goto/32 :goto_9

    nop

    :goto_9
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->addFlags(I)V

    :goto_a
    goto/32 :goto_6

    nop

    :goto_b
    goto :goto_f

    :goto_c
    goto/32 :goto_d

    nop

    :goto_d
    return-void

    :goto_e
    const/4 v1, 0x0

    :goto_f
    goto/32 :goto_7

    nop
.end method

.method setRecycledViewPool(Lcom/android/internal/widget/RecyclerView$RecycledViewPool;)V
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_5

    nop

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->detach()V

    :goto_3
    goto/32 :goto_8

    nop

    :goto_4
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mRecyclerPool:Lcom/android/internal/widget/RecyclerView$RecycledViewPool;

    goto/32 :goto_1

    nop

    :goto_5
    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView;->getAdapter()Lcom/android/internal/widget/RecyclerView$Adapter;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_6
    return-void

    :goto_7
    if-nez p1, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_0

    nop

    :goto_8
    iput-object p1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mRecyclerPool:Lcom/android/internal/widget/RecyclerView$RecycledViewPool;

    goto/32 :goto_7

    nop

    :goto_9
    invoke-virtual {p1, v0}, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->attach(Lcom/android/internal/widget/RecyclerView$Adapter;)V

    :goto_a
    goto/32 :goto_6

    nop
.end method

.method setViewCacheExtension(Lcom/android/internal/widget/RecyclerView$ViewCacheExtension;)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iput-object p1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mViewCacheExtension:Lcom/android/internal/widget/RecyclerView$ViewCacheExtension;

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method public setViewCacheSize(I)V
    .locals 0

    iput p1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mRequestedCacheMax:I

    invoke-virtual {p0}, Lcom/android/internal/widget/RecyclerView$Recycler;->updateViewCacheSize()V

    return-void
.end method

.method tryGetViewHolderForPositionByDeadline(IZJ)Lcom/android/internal/widget/RecyclerView$ViewHolder;
    .locals 17

    goto/32 :goto_103

    nop

    :goto_0
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/widget/RecyclerView$Recycler;->tryBindViewHolderByDeadline(Lcom/android/internal/widget/RecyclerView$ViewHolder;IIJ)Z

    move-result v13

    :goto_1
    goto/32 :goto_59

    nop

    :goto_2
    if-nez v4, :cond_0

    goto/32 :goto_34

    :cond_0
    goto/32 :goto_72

    nop

    :goto_3
    const-string v2, "). Item count:"

    goto/32 :goto_31

    nop

    :goto_4
    invoke-static {v12}, Lcom/android/internal/widget/RecyclerView$ItemAnimator;->buildAdapterChangeFlagsForAnimations(Lcom/android/internal/widget/RecyclerView$ViewHolder;)I

    move-result v0

    goto/32 :goto_e9

    nop

    :goto_5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_f3

    nop

    :goto_6
    invoke-virtual {v1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->clearReturnedFromScrapFlag()V

    :goto_7
    goto/32 :goto_6e

    nop

    :goto_8
    iget-object v5, v5, Lcom/android/internal/widget/RecyclerView;->mState:Lcom/android/internal/widget/RecyclerView$State;

    goto/32 :goto_77

    nop

    :goto_9
    iput-object v12, v1, Lcom/android/internal/widget/RecyclerView$LayoutParams;->mViewHolder:Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_f0

    nop

    :goto_a
    check-cast v1, Lcom/android/internal/widget/RecyclerView$LayoutParams;

    :goto_b
    goto/32 :goto_9

    nop

    :goto_c
    iget-object v3, v3, Lcom/android/internal/widget/RecyclerView;->mAdapter:Lcom/android/internal/widget/RecyclerView$Adapter;

    goto/32 :goto_60

    nop

    :goto_d
    const/4 v0, 0x0

    goto/32 :goto_cc

    nop

    :goto_e
    iget-object v3, v1, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_a4

    nop

    :goto_f
    invoke-virtual {v12, v0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->hasAnyOfTheFlags(I)Z

    move-result v1

    goto/32 :goto_19

    nop

    :goto_10
    if-eqz v1, :cond_1

    goto/32 :goto_24

    :cond_1
    goto/32 :goto_a9

    nop

    :goto_11
    invoke-virtual/range {p0 .. p1}, Lcom/android/internal/widget/RecyclerView$Recycler;->getChangedScrapViewForPosition(I)Lcom/android/internal/widget/RecyclerView$ViewHolder;

    move-result-object v1

    goto/32 :goto_26

    nop

    :goto_12
    iput-boolean v9, v1, Lcom/android/internal/widget/RecyclerView$LayoutParams;->mPendingInvalidate:Z

    goto/32 :goto_f8

    nop

    :goto_13
    if-eqz v1, :cond_2

    goto/32 :goto_9d

    :cond_2
    goto/32 :goto_c5

    nop

    :goto_14
    iget-object v2, v2, Lcom/android/internal/widget/RecyclerView;->mState:Lcom/android/internal/widget/RecyclerView$State;

    goto/32 :goto_44

    nop

    :goto_15
    invoke-virtual {v1, v0}, Lcom/android/internal/widget/RecyclerView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    goto/32 :goto_109

    nop

    :goto_16
    throw v0

    :goto_17
    if-gez v2, :cond_3

    goto/32 :goto_af

    :cond_3
    goto/32 :goto_63

    nop

    :goto_18
    iget-object v0, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_ed

    nop

    :goto_19
    if-nez v1, :cond_4

    goto/32 :goto_41

    :cond_4
    goto/32 :goto_b9

    nop

    :goto_1a
    invoke-virtual {v4}, Lcom/android/internal/widget/RecyclerView$Adapter;->hasStableIds()Z

    move-result v4

    goto/32 :goto_e6

    nop

    :goto_1b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_42

    nop

    :goto_1c
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_1b

    nop

    :goto_1d
    iget-object v11, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->mRecyclerPool:Lcom/android/internal/widget/RecyclerView$RecycledViewPool;

    goto/32 :goto_b1

    nop

    :goto_1e
    invoke-virtual/range {v11 .. v16}, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->willCreateInTime(IJJ)Z

    move-result v11

    goto/32 :goto_fc

    nop

    :goto_1f
    iget-object v2, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_40

    nop

    :goto_20
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_d1

    nop

    :goto_21
    move v11, v0

    goto/32 :goto_be

    nop

    :goto_22
    invoke-virtual {v6, v1}, Lcom/android/internal/widget/RecyclerView$Recycler;->validateViewHolderForOffsetPosition(Lcom/android/internal/widget/RecyclerView$ViewHolder;)Z

    move-result v2

    goto/32 :goto_79

    nop

    :goto_23
    throw v3

    :goto_24
    goto/32 :goto_21

    nop

    :goto_25
    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/32 :goto_99

    nop

    :goto_26
    if-nez v1, :cond_5

    goto/32 :goto_ef

    :cond_5
    goto/32 :goto_7d

    nop

    :goto_27
    if-nez v0, :cond_6

    goto/32 :goto_8a

    :cond_6
    goto/32 :goto_98

    nop

    :goto_28
    move v11, v0

    goto/32 :goto_86

    nop

    :goto_29
    new-instance v5, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_5b

    nop

    :goto_2a
    invoke-direct {v12, v11}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    goto/32 :goto_de

    nop

    :goto_2b
    if-nez v2, :cond_7

    goto/32 :goto_d6

    :cond_7
    goto/32 :goto_11

    nop

    :goto_2c
    const/4 v13, 0x0

    goto/32 :goto_18

    nop

    :goto_2d
    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mState:Lcom/android/internal/widget/RecyclerView$State;

    goto/32 :goto_b6

    nop

    :goto_2e
    if-eqz v5, :cond_8

    goto/32 :goto_62

    :cond_8
    goto/32 :goto_61

    nop

    :goto_2f
    move v11, v0

    goto/32 :goto_4c

    nop

    :goto_30
    if-nez v1, :cond_9

    goto/32 :goto_90

    :cond_9
    goto/32 :goto_100

    nop

    :goto_31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_b7

    nop

    :goto_32
    if-nez v0, :cond_a

    goto/32 :goto_c3

    :cond_a
    goto/32 :goto_ec

    nop

    :goto_33
    throw v5

    :goto_34
    goto/32 :goto_53

    nop

    :goto_35
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    goto/32 :goto_c4

    nop

    :goto_36
    iget-object v1, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_57

    nop

    :goto_37
    iget-object v2, v12, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_25

    nop

    :goto_38
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_dc

    nop

    :goto_39
    iget-object v0, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_c0

    nop

    :goto_3a
    goto/16 :goto_9d

    :goto_3b
    goto/32 :goto_9c

    nop

    :goto_3c
    move-wide/from16 v4, p3

    goto/32 :goto_0

    nop

    :goto_3d
    iget-object v0, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_2d

    nop

    :goto_3e
    iget-object v4, v4, Lcom/android/internal/widget/RecyclerView;->mAdapter:Lcom/android/internal/widget/RecyclerView$Adapter;

    goto/32 :goto_64

    nop

    :goto_3f
    invoke-virtual {v1, v0}, Lcom/android/internal/widget/RecyclerView;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v1

    goto/32 :goto_aa

    nop

    :goto_40
    invoke-virtual {v2, v12, v1}, Lcom/android/internal/widget/RecyclerView;->recordAnimationInfoIfBouncedHiddenView(Lcom/android/internal/widget/RecyclerView$ViewHolder;Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;)V

    :goto_41
    goto/32 :goto_2c

    nop

    :goto_42
    const-string v5, "Inconsistency detected. Invalid item position "

    goto/32 :goto_ca

    nop

    :goto_43
    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mState:Lcom/android/internal/widget/RecyclerView$State;

    goto/32 :goto_b8

    nop

    :goto_44
    invoke-virtual {v2}, Lcom/android/internal/widget/RecyclerView$State;->getItemCount()I

    move-result v2

    goto/32 :goto_b2

    nop

    :goto_45
    const/4 v2, 0x4

    goto/32 :goto_ba

    nop

    :goto_46
    cmp-long v11, p3, v11

    goto/32 :goto_cf

    nop

    :goto_47
    goto/16 :goto_bf

    :goto_48
    goto/32 :goto_2f

    nop

    :goto_49
    iget-boolean v0, v0, Lcom/android/internal/widget/RecyclerView$State;->mRunSimpleAnimations:Z

    goto/32 :goto_9f

    nop

    :goto_4a
    invoke-direct {v5, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_68

    nop

    :goto_4b
    invoke-virtual {v1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->wasReturnedFromScrap()Z

    move-result v2

    goto/32 :goto_cd

    nop

    :goto_4c
    move-object v12, v1

    goto/32 :goto_ae

    nop

    :goto_4d
    invoke-virtual {v1}, Lcom/android/internal/widget/RecyclerView;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    goto/32 :goto_c1

    nop

    :goto_4e
    iput v7, v12, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPreLayoutPosition:I

    goto/32 :goto_89

    nop

    :goto_4f
    invoke-virtual {v12}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isInvalid()Z

    move-result v0

    goto/32 :goto_c2

    nop

    :goto_50
    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView$State;->isPreLayout()Z

    move-result v0

    goto/32 :goto_27

    nop

    :goto_51
    invoke-virtual {v3}, Lcom/android/internal/widget/RecyclerView$Adapter;->getItemCount()I

    move-result v3

    goto/32 :goto_6c

    nop

    :goto_52
    iget-object v0, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_fa

    nop

    :goto_53
    if-eqz v1, :cond_b

    goto/32 :goto_90

    :cond_b
    goto/32 :goto_84

    nop

    :goto_54
    invoke-virtual {v1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->shouldIgnore()Z

    move-result v5

    goto/32 :goto_2e

    nop

    :goto_55
    iget-object v11, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_d3

    nop

    :goto_56
    iget-object v4, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_3e

    nop

    :goto_57
    iget-object v1, v1, Lcom/android/internal/widget/RecyclerView;->mItemAnimator:Lcom/android/internal/widget/RecyclerView$ItemAnimator;

    goto/32 :goto_95

    nop

    :goto_58
    invoke-virtual {v12}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->getUnmodifiedPayloads()Ljava/util/List;

    move-result-object v3

    goto/32 :goto_106

    nop

    :goto_59
    iget-object v0, v12, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_35

    nop

    :goto_5a
    move-object/from16 v0, p0

    goto/32 :goto_93

    nop

    :goto_5b
    const-string v9, "getViewForPositionAndType returned a view that is ignored. You must call stopIgnoring before returning this view."

    goto/32 :goto_4a

    nop

    :goto_5c
    move v2, v10

    :goto_5d
    goto/32 :goto_d5

    nop

    :goto_5e
    if-eqz v8, :cond_c

    goto/32 :goto_6f

    :cond_c
    goto/32 :goto_45

    nop

    :goto_5f
    if-nez v1, :cond_d

    goto/32 :goto_9d

    :cond_d
    goto/32 :goto_22

    nop

    :goto_60
    invoke-virtual {v3, v2}, Lcom/android/internal/widget/RecyclerView$Adapter;->getItemViewType(I)I

    move-result v3

    goto/32 :goto_82

    nop

    :goto_61
    goto/16 :goto_34

    :goto_62
    goto/32 :goto_29

    nop

    :goto_63
    iget-object v3, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_67

    nop

    :goto_64
    invoke-virtual {v4, v2}, Lcom/android/internal/widget/RecyclerView$Adapter;->getItemId(I)J

    move-result-wide v4

    goto/32 :goto_88

    nop

    :goto_65
    invoke-static {}, Lcom/android/internal/widget/RecyclerView;->-$$Nest$sfgetALLOW_THREAD_GAP_WORK()Z

    move-result v11

    goto/32 :goto_73

    nop

    :goto_66
    new-instance v5, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_96

    nop

    :goto_67
    iget-object v3, v3, Lcom/android/internal/widget/RecyclerView;->mAdapter:Lcom/android/internal/widget/RecyclerView$Adapter;

    goto/32 :goto_51

    nop

    :goto_68
    throw v5

    :goto_69
    goto/32 :goto_66

    nop

    :goto_6a
    invoke-virtual {v5, v4}, Lcom/android/internal/widget/RecyclerView;->getChildViewHolder(Landroid/view/View;)Lcom/android/internal/widget/RecyclerView$ViewHolder;

    move-result-object v1

    goto/32 :goto_6d

    nop

    :goto_6b
    invoke-virtual {v2, v7}, Lcom/android/internal/widget/AdapterHelper;->findPositionOffset(I)I

    move-result v2

    goto/32 :goto_17

    nop

    :goto_6c
    if-lt v2, v3, :cond_e

    goto/32 :goto_af

    :cond_e
    goto/32 :goto_dd

    nop

    :goto_6d
    if-nez v1, :cond_f

    goto/32 :goto_69

    :cond_f
    goto/32 :goto_54

    nop

    :goto_6e
    invoke-virtual {v6, v1}, Lcom/android/internal/widget/RecyclerView$Recycler;->recycleViewHolderInternal(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V

    :goto_6f
    goto/32 :goto_fb

    nop

    :goto_70
    if-nez v4, :cond_10

    goto/32 :goto_90

    :cond_10
    goto/32 :goto_8f

    nop

    :goto_71
    invoke-virtual {v11}, Lcom/android/internal/widget/RecyclerView;->getNanoTime()J

    move-result-wide v11

    goto/32 :goto_a1

    nop

    :goto_72
    iget-object v5, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_6a

    nop

    :goto_73
    if-nez v11, :cond_11

    goto/32 :goto_df

    :cond_11
    goto/32 :goto_94

    nop

    :goto_74
    const-string v5, "(offset:"

    goto/32 :goto_104

    nop

    :goto_75
    const/4 v0, 0x1

    :goto_76
    goto/32 :goto_fe

    nop

    :goto_77
    invoke-virtual {v5}, Lcom/android/internal/widget/RecyclerView$State;->getItemCount()I

    move-result v5

    goto/32 :goto_a0

    nop

    :goto_78
    invoke-virtual {v0, v7}, Lcom/android/internal/widget/AdapterHelper;->findPositionOffset(I)I

    move-result v14

    goto/32 :goto_5a

    nop

    :goto_79
    if-eqz v2, :cond_12

    goto/32 :goto_3b

    :cond_12
    goto/32 :goto_5e

    nop

    :goto_7a
    move-object v1, v0

    goto/32 :goto_a

    nop

    :goto_7b
    iget-object v2, v12, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_ff

    nop

    :goto_7c
    iget-object v5, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_8

    nop

    :goto_7d
    move v2, v9

    goto/32 :goto_ee

    nop

    :goto_7e
    iget-object v12, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_8d

    nop

    :goto_7f
    const/4 v10, 0x0

    goto/32 :goto_2b

    nop

    :goto_80
    sub-long v14, v11, v4

    goto/32 :goto_85

    nop

    :goto_81
    if-nez v13, :cond_13

    goto/32 :goto_c7

    :cond_13
    goto/32 :goto_c6

    nop

    :goto_82
    iget-object v4, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_e2

    nop

    :goto_83
    iget-object v1, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_3f

    nop

    :goto_84
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/widget/RecyclerView$Recycler;->getRecycledViewPool()Lcom/android/internal/widget/RecyclerView$RecycledViewPool;

    move-result-object v4

    goto/32 :goto_a5

    nop

    :goto_85
    invoke-virtual {v13, v3, v14, v15}, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->factorInCreateTime(IJ)V

    goto/32 :goto_28

    nop

    :goto_86
    move-object v12, v1

    goto/32 :goto_47

    nop

    :goto_87
    if-nez v2, :cond_14

    goto/32 :goto_d8

    :cond_14
    goto/32 :goto_10e

    nop

    :goto_88
    invoke-virtual {v6, v4, v5, v3, v8}, Lcom/android/internal/widget/RecyclerView$Recycler;->getScrapOrCachedViewForId(JIZ)Lcom/android/internal/widget/RecyclerView$ViewHolder;

    move-result-object v1

    goto/32 :goto_e0

    nop

    :goto_89
    goto/16 :goto_1

    :goto_8a
    goto/32 :goto_db

    nop

    :goto_8b
    if-eqz v0, :cond_15

    goto/32 :goto_41

    :cond_15
    goto/32 :goto_a2

    nop

    :goto_8c
    invoke-virtual {v4, v6, v7, v3}, Lcom/android/internal/widget/RecyclerView$ViewCacheExtension;->getViewForPositionAndType(Lcom/android/internal/widget/RecyclerView$Recycler;II)Landroid/view/View;

    move-result-object v4

    goto/32 :goto_2

    nop

    :goto_8d
    invoke-virtual {v11, v12, v3}, Lcom/android/internal/widget/RecyclerView$Adapter;->createViewHolder(Landroid/view/ViewGroup;I)Lcom/android/internal/widget/RecyclerView$ViewHolder;

    move-result-object v1

    goto/32 :goto_65

    nop

    :goto_8e
    iget-object v1, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_15

    nop

    :goto_8f
    invoke-direct {v6, v1}, Lcom/android/internal/widget/RecyclerView$Recycler;->invalidateDisplayListInt(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V

    :goto_90
    goto/32 :goto_a6

    nop

    :goto_91
    goto/16 :goto_b

    :goto_92
    goto/32 :goto_7a

    nop

    :goto_93
    move-object v1, v12

    goto/32 :goto_b3

    nop

    :goto_94
    iget-object v11, v1, Lcom/android/internal/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_d0

    nop

    :goto_95
    iget-object v2, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_10a

    nop

    :goto_96
    const-string v9, "getViewForPositionAndType returned a view which does not have a ViewHolder"

    goto/32 :goto_10b

    nop

    :goto_97
    sget-boolean v4, Lcom/android/internal/widget/RecyclerView;->FORCE_INVALIDATE_DISPLAY_LIST:Z

    goto/32 :goto_70

    nop

    :goto_98
    invoke-virtual {v12}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isBound()Z

    move-result v0

    goto/32 :goto_bc

    nop

    :goto_99
    goto/16 :goto_b

    :goto_9a
    goto/32 :goto_83

    nop

    :goto_9b
    move-wide v13, v4

    goto/32 :goto_107

    nop

    :goto_9c
    const/4 v0, 0x1

    :goto_9d
    goto/32 :goto_10

    nop

    :goto_9e
    invoke-virtual {v1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isScrap()Z

    move-result v2

    goto/32 :goto_87

    nop

    :goto_9f
    if-nez v0, :cond_16

    goto/32 :goto_41

    :cond_16
    nop

    goto/32 :goto_4

    nop

    :goto_a0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_38

    nop

    :goto_a1
    iget-object v13, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->mRecyclerPool:Lcom/android/internal/widget/RecyclerView$RecycledViewPool;

    goto/32 :goto_80

    nop

    :goto_a2
    const/16 v0, 0x2000

    goto/32 :goto_f

    nop

    :goto_a3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_20

    nop

    :goto_a4
    invoke-virtual {v2, v3, v10}, Lcom/android/internal/widget/RecyclerView;->removeDetachedView(Landroid/view/View;Z)V

    goto/32 :goto_e7

    nop

    :goto_a5
    invoke-virtual {v4, v3}, Lcom/android/internal/widget/RecyclerView$RecycledViewPool;->getRecycledView(I)Lcom/android/internal/widget/RecyclerView$ViewHolder;

    move-result-object v1

    goto/32 :goto_30

    nop

    :goto_a6
    if-eqz v1, :cond_17

    goto/32 :goto_48

    :cond_17
    goto/32 :goto_ad

    nop

    :goto_a7
    move v9, v10

    :goto_a8
    goto/32 :goto_12

    nop

    :goto_a9
    iget-object v2, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_fd

    nop

    :goto_aa
    if-eqz v1, :cond_18

    goto/32 :goto_92

    :cond_18
    goto/32 :goto_8e

    nop

    :goto_ab
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_e1

    nop

    :goto_ac
    invoke-virtual {v4}, Lcom/android/internal/widget/RecyclerView;->getNanoTime()J

    move-result-wide v4

    goto/32 :goto_f7

    nop

    :goto_ad
    iget-object v4, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_ac

    nop

    :goto_ae
    goto :goto_bf

    :goto_af
    goto/32 :goto_c8

    nop

    :goto_b0
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_5

    nop

    :goto_b1
    move v12, v3

    goto/32 :goto_9b

    nop

    :goto_b2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_ea

    nop

    :goto_b3
    move v2, v14

    goto/32 :goto_f4

    nop

    :goto_b4
    invoke-virtual {v2}, Lcom/android/internal/widget/RecyclerView$State;->isPreLayout()Z

    move-result v2

    goto/32 :goto_102

    nop

    :goto_b5
    if-nez v11, :cond_19

    goto/32 :goto_41

    :cond_19
    goto/32 :goto_101

    nop

    :goto_b6
    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView$State;->getItemCount()I

    move-result v0

    goto/32 :goto_e8

    nop

    :goto_b7
    iget-object v2, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_14

    nop

    :goto_b8
    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView$State;->isPreLayout()Z

    move-result v0

    goto/32 :goto_8b

    nop

    :goto_b9
    invoke-virtual {v12, v10, v0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->setFlags(II)V

    goto/32 :goto_39

    nop

    :goto_ba
    invoke-virtual {v1, v2}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->addFlags(I)V

    goto/32 :goto_9e

    nop

    :goto_bb
    move/from16 v8, p2

    goto/32 :goto_f5

    nop

    :goto_bc
    if-nez v0, :cond_1a

    goto/32 :goto_8a

    :cond_1a
    goto/32 :goto_4e

    nop

    :goto_bd
    iget-object v11, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_71

    nop

    :goto_be
    move-object v12, v1

    :goto_bf
    goto/32 :goto_b5

    nop

    :goto_c0
    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mState:Lcom/android/internal/widget/RecyclerView$State;

    goto/32 :goto_49

    nop

    :goto_c1
    check-cast v1, Lcom/android/internal/widget/RecyclerView$LayoutParams;

    goto/32 :goto_37

    nop

    :goto_c2
    if-nez v0, :cond_1b

    goto/32 :goto_1

    :cond_1b
    :goto_c3
    goto/32 :goto_52

    nop

    :goto_c4
    if-eqz v0, :cond_1c

    goto/32 :goto_9a

    :cond_1c
    goto/32 :goto_108

    nop

    :goto_c5
    invoke-virtual/range {p0 .. p2}, Lcom/android/internal/widget/RecyclerView$Recycler;->getScrapOrHiddenOrCachedHolderForPosition(IZ)Lcom/android/internal/widget/RecyclerView$ViewHolder;

    move-result-object v1

    goto/32 :goto_5f

    nop

    :goto_c6
    goto/16 :goto_a8

    :goto_c7
    goto/32 :goto_a7

    nop

    :goto_c8
    new-instance v3, Ljava/lang/IndexOutOfBoundsException;

    goto/32 :goto_1c

    nop

    :goto_c9
    iget-object v4, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->mViewCacheExtension:Lcom/android/internal/widget/RecyclerView$ViewCacheExtension;

    goto/32 :goto_10c

    nop

    :goto_ca
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_d2

    nop

    :goto_cb
    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_16

    nop

    :goto_cc
    const/4 v1, 0x0

    goto/32 :goto_e3

    nop

    :goto_cd
    if-nez v2, :cond_1d

    goto/32 :goto_7

    :cond_1d
    goto/32 :goto_6

    nop

    :goto_ce
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_3

    nop

    :goto_cf
    if-nez v11, :cond_1e

    goto/32 :goto_da

    :cond_1e
    goto/32 :goto_1d

    nop

    :goto_d0
    invoke-static {v11}, Lcom/android/internal/widget/RecyclerView;->findNestedRecyclerView(Landroid/view/View;)Lcom/android/internal/widget/RecyclerView;

    move-result-object v11

    goto/32 :goto_f2

    nop

    :goto_d1
    const-string v2, "("

    goto/32 :goto_105

    nop

    :goto_d2
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_74

    nop

    :goto_d3
    iget-object v11, v11, Lcom/android/internal/widget/RecyclerView;->mAdapter:Lcom/android/internal/widget/RecyclerView$Adapter;

    goto/32 :goto_7e

    nop

    :goto_d4
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_7c

    nop

    :goto_d5
    move v0, v2

    :goto_d6
    goto/32 :goto_13

    nop

    :goto_d7
    goto/16 :goto_7

    :goto_d8
    goto/32 :goto_4b

    nop

    :goto_d9
    return-object v9

    :goto_da
    goto/32 :goto_55

    nop

    :goto_db
    invoke-virtual {v12}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isBound()Z

    move-result v0

    goto/32 :goto_32

    nop

    :goto_dc
    invoke-direct {v3, v4}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_23

    nop

    :goto_dd
    iget-object v3, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_c

    nop

    :goto_de
    iput-object v12, v1, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mNestedRecyclerView:Ljava/lang/ref/WeakReference;

    :goto_df
    goto/32 :goto_bd

    nop

    :goto_e0
    if-nez v1, :cond_1f

    goto/32 :goto_76

    :cond_1f
    goto/32 :goto_f6

    nop

    :goto_e1
    const-string v5, ").state:"

    goto/32 :goto_d4

    nop

    :goto_e2
    iget-object v4, v4, Lcom/android/internal/widget/RecyclerView;->mAdapter:Lcom/android/internal/widget/RecyclerView$Adapter;

    goto/32 :goto_1a

    nop

    :goto_e3
    iget-object v2, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_eb

    nop

    :goto_e4
    move/from16 v7, p1

    goto/32 :goto_bb

    nop

    :goto_e5
    if-eqz v0, :cond_20

    goto/32 :goto_c3

    :cond_20
    goto/32 :goto_4f

    nop

    :goto_e6
    if-nez v4, :cond_21

    goto/32 :goto_76

    :cond_21
    goto/32 :goto_56

    nop

    :goto_e7
    invoke-virtual {v1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->unScrap()V

    goto/32 :goto_d7

    nop

    :goto_e8
    if-lt v7, v0, :cond_22

    goto/32 :goto_f9

    :cond_22
    goto/32 :goto_d

    nop

    :goto_e9
    or-int/lit16 v0, v0, 0x1000

    goto/32 :goto_36

    nop

    :goto_ea
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_cb

    nop

    :goto_eb
    iget-object v2, v2, Lcom/android/internal/widget/RecyclerView;->mState:Lcom/android/internal/widget/RecyclerView$State;

    goto/32 :goto_b4

    nop

    :goto_ec
    invoke-virtual {v12}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->needsUpdate()Z

    move-result v0

    goto/32 :goto_e5

    nop

    :goto_ed
    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mState:Lcom/android/internal/widget/RecyclerView$State;

    goto/32 :goto_50

    nop

    :goto_ee
    goto/16 :goto_5d

    :goto_ef
    goto/32 :goto_5c

    nop

    :goto_f0
    if-nez v11, :cond_23

    goto/32 :goto_c7

    :cond_23
    goto/32 :goto_81

    nop

    :goto_f1
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    goto/32 :goto_b0

    nop

    :goto_f2
    if-nez v11, :cond_24

    goto/32 :goto_df

    :cond_24
    goto/32 :goto_10d

    nop

    :goto_f3
    const-string v2, "Invalid item position "

    goto/32 :goto_a3

    nop

    :goto_f4
    move/from16 v3, p1

    goto/32 :goto_3c

    nop

    :goto_f5
    if-gez v7, :cond_25

    goto/32 :goto_f9

    :cond_25
    goto/32 :goto_3d

    nop

    :goto_f6
    iput v2, v1, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPosition:I

    goto/32 :goto_75

    nop

    :goto_f7
    const-wide v11, 0x7fffffffffffffffL

    goto/32 :goto_46

    nop

    :goto_f8
    return-object v12

    :goto_f9
    goto/32 :goto_f1

    nop

    :goto_fa
    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mAdapterHelper:Lcom/android/internal/widget/AdapterHelper;

    goto/32 :goto_78

    nop

    :goto_fb
    const/4 v1, 0x0

    goto/32 :goto_3a

    nop

    :goto_fc
    if-eqz v11, :cond_26

    goto/32 :goto_da

    :cond_26
    goto/32 :goto_10f

    nop

    :goto_fd
    iget-object v2, v2, Lcom/android/internal/widget/RecyclerView;->mAdapterHelper:Lcom/android/internal/widget/AdapterHelper;

    goto/32 :goto_6b

    nop

    :goto_fe
    if-eqz v1, :cond_27

    goto/32 :goto_34

    :cond_27
    goto/32 :goto_c9

    nop

    :goto_ff
    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/32 :goto_91

    nop

    :goto_100
    invoke-virtual {v1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->resetInternal()V

    goto/32 :goto_97

    nop

    :goto_101
    iget-object v0, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_43

    nop

    :goto_102
    const/4 v9, 0x1

    goto/32 :goto_7f

    nop

    :goto_103
    move-object/from16 v6, p0

    goto/32 :goto_e4

    nop

    :goto_104
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_ab

    nop

    :goto_105
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_ce

    nop

    :goto_106
    invoke-virtual {v1, v2, v12, v0, v3}, Lcom/android/internal/widget/RecyclerView$ItemAnimator;->recordPreLayoutInformation(Lcom/android/internal/widget/RecyclerView$State;Lcom/android/internal/widget/RecyclerView$ViewHolder;ILjava/util/List;)Lcom/android/internal/widget/RecyclerView$ItemAnimator$ItemHolderInfo;

    move-result-object v1

    goto/32 :goto_1f

    nop

    :goto_107
    move-wide/from16 v15, p3

    goto/32 :goto_1e

    nop

    :goto_108
    iget-object v1, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_4d

    nop

    :goto_109
    check-cast v1, Lcom/android/internal/widget/RecyclerView$LayoutParams;

    goto/32 :goto_7b

    nop

    :goto_10a
    iget-object v2, v2, Lcom/android/internal/widget/RecyclerView;->mState:Lcom/android/internal/widget/RecyclerView$State;

    goto/32 :goto_58

    nop

    :goto_10b
    invoke-direct {v5, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_33

    nop

    :goto_10c
    if-nez v4, :cond_28

    goto/32 :goto_34

    :cond_28
    nop

    goto/32 :goto_8c

    nop

    :goto_10d
    new-instance v12, Ljava/lang/ref/WeakReference;

    goto/32 :goto_2a

    nop

    :goto_10e
    iget-object v2, v6, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_e

    nop

    :goto_10f
    const/4 v9, 0x0

    goto/32 :goto_d9

    nop
.end method

.method unscrapView(Lcom/android/internal/widget/RecyclerView$ViewHolder;)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    invoke-static {p1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->-$$Nest$fgetmInChangeScrap(Lcom/android/internal/widget/RecyclerView$ViewHolder;)Z

    move-result v0

    goto/32 :goto_a

    nop

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_9

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mChangedScrap:Ljava/util/ArrayList;

    goto/32 :goto_c

    nop

    :goto_3
    invoke-virtual {p1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->clearReturnedFromScrapFlag()V

    goto/32 :goto_8

    nop

    :goto_4
    goto :goto_e

    :goto_5
    goto/32 :goto_6

    nop

    :goto_6
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mAttachedScrap:Ljava/util/ArrayList;

    goto/32 :goto_d

    nop

    :goto_7
    const/4 v0, 0x0

    goto/32 :goto_b

    nop

    :goto_8
    return-void

    :goto_9
    invoke-static {p1, v0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->-$$Nest$fputmInChangeScrap(Lcom/android/internal/widget/RecyclerView$ViewHolder;Z)V

    goto/32 :goto_3

    nop

    :goto_a
    if-nez v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_2

    nop

    :goto_b
    invoke-static {p1, v0}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->-$$Nest$fputmScrapContainer(Lcom/android/internal/widget/RecyclerView$ViewHolder;Lcom/android/internal/widget/RecyclerView$Recycler;)V

    goto/32 :goto_1

    nop

    :goto_c
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/32 :goto_4

    nop

    :goto_d
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :goto_e
    goto/32 :goto_7

    nop
.end method

.method updateViewCacheSize()V
    .locals 4

    goto/32 :goto_1

    nop

    :goto_0
    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mLayout:Lcom/android/internal/widget/RecyclerView$LayoutManager;

    goto/32 :goto_c

    nop

    :goto_1
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_14

    nop

    :goto_2
    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_e

    nop

    :goto_3
    iget v3, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mViewCacheMax:I

    goto/32 :goto_12

    nop

    :goto_4
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_0

    nop

    :goto_5
    add-int/lit8 v1, v1, -0x1

    :goto_6
    goto/32 :goto_d

    nop

    :goto_7
    iget v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mRequestedCacheMax:I

    goto/32 :goto_8

    nop

    :goto_8
    add-int/2addr v1, v0

    goto/32 :goto_1a

    nop

    :goto_9
    iget-object v2, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_15

    nop

    :goto_a
    add-int/lit8 v1, v1, -0x1

    goto/32 :goto_16

    nop

    :goto_b
    if-nez v0, :cond_0

    goto/32 :goto_19

    :cond_0
    goto/32 :goto_4

    nop

    :goto_c
    iget v0, v0, Lcom/android/internal/widget/RecyclerView$LayoutManager;->mPrefetchMaxCountObserved:I

    goto/32 :goto_18

    nop

    :goto_d
    if-gez v1, :cond_1

    goto/32 :goto_17

    :cond_1
    goto/32 :goto_9

    nop

    :goto_e
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_5

    nop

    :goto_f
    return-void

    :goto_10
    const/4 v0, 0x0

    :goto_11
    goto/32 :goto_7

    nop

    :goto_12
    if-gt v2, v3, :cond_2

    goto/32 :goto_17

    :cond_2
    goto/32 :goto_13

    nop

    :goto_13
    invoke-virtual {p0, v1}, Lcom/android/internal/widget/RecyclerView$Recycler;->recycleCachedViewAt(I)V

    goto/32 :goto_a

    nop

    :goto_14
    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mLayout:Lcom/android/internal/widget/RecyclerView$LayoutManager;

    goto/32 :goto_b

    nop

    :goto_15
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    goto/32 :goto_3

    nop

    :goto_16
    goto :goto_6

    :goto_17
    goto/32 :goto_f

    nop

    :goto_18
    goto :goto_11

    :goto_19
    goto/32 :goto_10

    nop

    :goto_1a
    iput v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mViewCacheMax:I

    goto/32 :goto_2

    nop
.end method

.method validateViewHolderForOffsetPosition(Lcom/android/internal/widget/RecyclerView$ViewHolder;)Z
    .locals 7

    goto/32 :goto_2d

    nop

    :goto_0
    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView$State;->isPreLayout()Z

    move-result v0

    goto/32 :goto_34

    nop

    :goto_1
    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mState:Lcom/android/internal/widget/RecyclerView$State;

    goto/32 :goto_22

    nop

    :goto_2
    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mAdapter:Lcom/android/internal/widget/RecyclerView$Adapter;

    goto/32 :goto_2e

    nop

    :goto_3
    return v1

    :goto_4
    goto/32 :goto_1e

    nop

    :goto_5
    const/4 v2, 0x1

    goto/32 :goto_21

    nop

    :goto_6
    if-lt v0, v1, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_31

    nop

    :goto_7
    if-nez v0, :cond_1

    goto/32 :goto_20

    :cond_1
    goto/32 :goto_f

    nop

    :goto_8
    const-string v2, "Inconsistency detected. Invalid view holder adapter position"

    goto/32 :goto_16

    nop

    :goto_9
    iget v0, p1, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPosition:I

    goto/32 :goto_23

    nop

    :goto_a
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_27

    nop

    :goto_b
    return v2

    :goto_c
    goto/32 :goto_1a

    nop

    :goto_d
    if-ne v0, v2, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_3

    nop

    :goto_e
    if-gez v0, :cond_3

    goto/32 :goto_c

    :cond_3
    goto/32 :goto_9

    nop

    :goto_f
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_1

    nop

    :goto_10
    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_24

    nop

    :goto_11
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_29

    nop

    :goto_12
    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView$Adapter;->hasStableIds()Z

    move-result v0

    goto/32 :goto_5

    nop

    :goto_13
    invoke-virtual {p1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v2

    goto/32 :goto_d

    nop

    :goto_14
    cmp-long v0, v3, v5

    goto/32 :goto_1d

    nop

    :goto_15
    iget-object v1, v1, Lcom/android/internal/widget/RecyclerView;->mAdapter:Lcom/android/internal/widget/RecyclerView$Adapter;

    goto/32 :goto_33

    nop

    :goto_16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_11

    nop

    :goto_17
    invoke-virtual {v0, v2}, Lcom/android/internal/widget/RecyclerView$Adapter;->getItemViewType(I)I

    move-result v0

    goto/32 :goto_13

    nop

    :goto_18
    if-eqz v0, :cond_4

    goto/32 :goto_4

    :cond_4
    goto/32 :goto_a

    nop

    :goto_19
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_2

    nop

    :goto_1a
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    goto/32 :goto_2c

    nop

    :goto_1b
    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mAdapter:Lcom/android/internal/widget/RecyclerView$Adapter;

    goto/32 :goto_12

    nop

    :goto_1c
    invoke-virtual {v0, v5}, Lcom/android/internal/widget/RecyclerView$Adapter;->getItemId(I)J

    move-result-wide v5

    goto/32 :goto_14

    nop

    :goto_1d
    if-eqz v0, :cond_5

    goto/32 :goto_30

    :cond_5
    goto/32 :goto_2f

    nop

    :goto_1e
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_1b

    nop

    :goto_1f
    return v0

    :goto_20
    goto/32 :goto_35

    nop

    :goto_21
    if-nez v0, :cond_6

    goto/32 :goto_26

    :cond_6
    goto/32 :goto_2b

    nop

    :goto_22
    invoke-virtual {v0}, Lcom/android/internal/widget/RecyclerView$State;->isPreLayout()Z

    move-result v0

    goto/32 :goto_1f

    nop

    :goto_23
    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_15

    nop

    :goto_24
    throw v0

    :goto_25
    return v1

    :goto_26
    goto/32 :goto_b

    nop

    :goto_27
    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mAdapter:Lcom/android/internal/widget/RecyclerView$Adapter;

    goto/32 :goto_28

    nop

    :goto_28
    iget v2, p1, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPosition:I

    goto/32 :goto_17

    nop

    :goto_29
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_10

    nop

    :goto_2a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_8

    nop

    :goto_2b
    invoke-virtual {p1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->getItemId()J

    move-result-wide v3

    goto/32 :goto_19

    nop

    :goto_2c
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_2a

    nop

    :goto_2d
    invoke-virtual {p1}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->isRemoved()Z

    move-result v0

    goto/32 :goto_7

    nop

    :goto_2e
    iget v5, p1, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPosition:I

    goto/32 :goto_1c

    nop

    :goto_2f
    move v1, v2

    :goto_30
    goto/32 :goto_25

    nop

    :goto_31
    iget-object v0, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->this$0:Lcom/android/internal/widget/RecyclerView;

    goto/32 :goto_32

    nop

    :goto_32
    iget-object v0, v0, Lcom/android/internal/widget/RecyclerView;->mState:Lcom/android/internal/widget/RecyclerView$State;

    goto/32 :goto_0

    nop

    :goto_33
    invoke-virtual {v1}, Lcom/android/internal/widget/RecyclerView$Adapter;->getItemCount()I

    move-result v1

    goto/32 :goto_6

    nop

    :goto_34
    const/4 v1, 0x0

    goto/32 :goto_18

    nop

    :goto_35
    iget v0, p1, Lcom/android/internal/widget/RecyclerView$ViewHolder;->mPosition:I

    goto/32 :goto_e

    nop
.end method

.method viewRangeUpdate(II)V
    .locals 6

    goto/32 :goto_15

    nop

    :goto_0
    if-eqz v3, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_6

    nop

    :goto_1
    if-ge v4, p1, :cond_1

    goto/32 :goto_12

    :cond_1
    goto/32 :goto_a

    nop

    :goto_2
    const/4 v5, 0x2

    goto/32 :goto_5

    nop

    :goto_3
    return-void

    :goto_4
    iget-object v1, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_e

    nop

    :goto_5
    invoke-virtual {v3, v5}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->addFlags(I)V

    goto/32 :goto_11

    nop

    :goto_6
    goto :goto_12

    :goto_7
    goto/32 :goto_8

    nop

    :goto_8
    invoke-virtual {v3}, Lcom/android/internal/widget/RecyclerView$ViewHolder;->getLayoutPosition()I

    move-result v4

    goto/32 :goto_1

    nop

    :goto_9
    add-int/lit8 v2, v2, -0x1

    goto/32 :goto_13

    nop

    :goto_a
    if-lt v4, v0, :cond_2

    goto/32 :goto_12

    :cond_2
    goto/32 :goto_2

    nop

    :goto_b
    check-cast v3, Lcom/android/internal/widget/RecyclerView$ViewHolder;

    goto/32 :goto_0

    nop

    :goto_c
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_b

    nop

    :goto_d
    iget-object v3, p0, Lcom/android/internal/widget/RecyclerView$Recycler;->mCachedViews:Ljava/util/ArrayList;

    goto/32 :goto_c

    nop

    :goto_e
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_f

    nop

    :goto_f
    add-int/lit8 v2, v1, -0x1

    :goto_10
    goto/32 :goto_16

    nop

    :goto_11
    invoke-virtual {p0, v2}, Lcom/android/internal/widget/RecyclerView$Recycler;->recycleCachedViewAt(I)V

    :goto_12
    goto/32 :goto_9

    nop

    :goto_13
    goto :goto_10

    :goto_14
    goto/32 :goto_3

    nop

    :goto_15
    add-int v0, p1, p2

    goto/32 :goto_4

    nop

    :goto_16
    if-gez v2, :cond_3

    goto/32 :goto_14

    :cond_3
    goto/32 :goto_d

    nop
.end method
