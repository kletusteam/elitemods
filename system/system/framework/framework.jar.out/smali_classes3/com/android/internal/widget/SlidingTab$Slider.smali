.class Lcom/android/internal/widget/SlidingTab$Slider;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/SlidingTab;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Slider"
.end annotation


# static fields
.field public static final ALIGN_BOTTOM:I = 0x3

.field public static final ALIGN_LEFT:I = 0x0

.field public static final ALIGN_RIGHT:I = 0x1

.field public static final ALIGN_TOP:I = 0x2

.field public static final ALIGN_UNKNOWN:I = 0x4

.field private static final STATE_ACTIVE:I = 0x2

.field private static final STATE_NORMAL:I = 0x0

.field private static final STATE_PRESSED:I = 0x1


# instance fields
.field private alignment:I

.field private alignment_value:I

.field private currentState:I

.field private final tab:Landroid/widget/ImageView;

.field private final target:Landroid/widget/ImageView;

.field private final text:Landroid/widget/TextView;


# direct methods
.method static bridge synthetic -$$Nest$fgettab(Lcom/android/internal/widget/SlidingTab$Slider;)Landroid/widget/ImageView;
    .locals 0

    iget-object p0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgettext(Lcom/android/internal/widget/SlidingTab$Slider;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    return-object p0
.end method

.method constructor <init>(Landroid/view/ViewGroup;III)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->currentState:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment:I

    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    invoke-virtual {v1, p2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x2

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v2, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    const/4 v5, -0x1

    invoke-direct {v4, v3, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v2, p3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x10303d6

    invoke-virtual {v2, v4, v5}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    new-instance v4, Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    invoke-virtual {v4, p4}, Landroid/widget/ImageView;->setImageResource(I)V

    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v5, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public getTabHeight()I
    .locals 1

    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public getTabWidth()I
    .locals 1

    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v0

    return v0
.end method

.method hide()V
    .locals 8

    goto/32 :goto_17

    nop

    :goto_0
    int-to-float v5, v0

    goto/32 :goto_1f

    nop

    :goto_1
    goto/16 :goto_3a

    :goto_2
    goto/32 :goto_15

    nop

    :goto_3
    const/4 v5, 0x4

    goto/32 :goto_4

    nop

    :goto_4
    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/32 :goto_6

    nop

    :goto_5
    new-instance v4, Landroid/view/animation/TranslateAnimation;

    goto/32 :goto_0

    nop

    :goto_6
    return-void

    :goto_7
    if-nez v0, :cond_0

    goto/32 :goto_3a

    :cond_0
    goto/32 :goto_a

    nop

    :goto_8
    goto/16 :goto_2c

    :goto_9
    goto/32 :goto_2b

    nop

    :goto_a
    if-eq v0, v2, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_1

    nop

    :goto_b
    const/4 v2, 0x1

    goto/32 :goto_7

    nop

    :goto_c
    iget-object v2, p0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    goto/32 :goto_3

    nop

    :goto_d
    invoke-virtual {v4}, Landroid/widget/ImageView;->getLeft()I

    move-result v4

    goto/32 :goto_f

    nop

    :goto_e
    if-nez v3, :cond_2

    goto/32 :goto_9

    :cond_2
    goto/32 :goto_28

    nop

    :goto_f
    sub-int/2addr v0, v4

    goto/32 :goto_8

    nop

    :goto_10
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    goto/32 :goto_c

    nop

    :goto_11
    invoke-virtual {v4}, Landroid/widget/ImageView;->getRight()I

    move-result v4

    goto/32 :goto_25

    nop

    :goto_12
    sub-int/2addr v1, v4

    :goto_13
    nop

    goto/32 :goto_5

    nop

    :goto_14
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_11

    nop

    :goto_15
    move v3, v1

    goto/32 :goto_39

    nop

    :goto_16
    if-nez v3, :cond_3

    goto/32 :goto_3c

    :cond_3
    goto/32 :goto_3b

    nop

    :goto_17
    iget v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment:I

    goto/32 :goto_35

    nop

    :goto_18
    iget v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment_value:I

    goto/32 :goto_19

    nop

    :goto_19
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_1a

    nop

    :goto_1a
    invoke-virtual {v4}, Landroid/widget/ImageView;->getTop()I

    move-result v4

    goto/32 :goto_12

    nop

    :goto_1b
    invoke-direct {v4, v7, v5, v7, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    goto/32 :goto_20

    nop

    :goto_1c
    move v3, v2

    :goto_1d
    goto/32 :goto_e

    nop

    :goto_1e
    iget-object v2, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    goto/32 :goto_10

    nop

    :goto_1f
    int-to-float v6, v1

    goto/32 :goto_31

    nop

    :goto_20
    const-wide/16 v5, 0xfa

    goto/32 :goto_2e

    nop

    :goto_21
    iget v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment:I

    goto/32 :goto_2f

    nop

    :goto_22
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_2d

    nop

    :goto_23
    goto :goto_13

    :goto_24
    goto/32 :goto_18

    nop

    :goto_25
    sub-int/2addr v0, v4

    goto/32 :goto_26

    nop

    :goto_26
    goto :goto_2c

    :goto_27
    goto/32 :goto_37

    nop

    :goto_28
    if-eqz v0, :cond_4

    goto/32 :goto_27

    :cond_4
    goto/32 :goto_36

    nop

    :goto_29
    invoke-virtual {v4, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    goto/32 :goto_2a

    nop

    :goto_2a
    iget-object v2, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_32

    nop

    :goto_2b
    move v0, v1

    :goto_2c
    nop

    goto/32 :goto_16

    nop

    :goto_2d
    invoke-virtual {v4}, Landroid/widget/ImageView;->getBottom()I

    move-result v4

    goto/32 :goto_38

    nop

    :goto_2e
    invoke-virtual {v4, v5, v6}, Landroid/view/animation/Animation;->setDuration(J)V

    goto/32 :goto_29

    nop

    :goto_2f
    const/4 v4, 0x2

    goto/32 :goto_34

    nop

    :goto_30
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_d

    nop

    :goto_31
    const/4 v7, 0x0

    goto/32 :goto_1b

    nop

    :goto_32
    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto/32 :goto_1e

    nop

    :goto_33
    iget v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment_value:I

    goto/32 :goto_22

    nop

    :goto_34
    if-eq v1, v4, :cond_5

    goto/32 :goto_24

    :cond_5
    goto/32 :goto_33

    nop

    :goto_35
    const/4 v1, 0x0

    goto/32 :goto_b

    nop

    :goto_36
    iget v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment_value:I

    goto/32 :goto_14

    nop

    :goto_37
    iget v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment_value:I

    goto/32 :goto_30

    nop

    :goto_38
    sub-int/2addr v1, v4

    goto/32 :goto_23

    nop

    :goto_39
    goto/16 :goto_1d

    :goto_3a
    goto/32 :goto_1c

    nop

    :goto_3b
    goto/16 :goto_13

    :goto_3c
    goto/32 :goto_21

    nop
.end method

.method public hideTarget()V
    .locals 2

    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method layout(IIIII)V
    .locals 24

    goto/32 :goto_1d

    nop

    :goto_0
    iget-object v7, v0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_29

    nop

    :goto_1
    mul-float v10, v10, v17

    goto/32 :goto_87

    nop

    :goto_2
    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v10

    goto/32 :goto_9e

    nop

    :goto_3
    move/from16 v2, p3

    goto/32 :goto_50

    nop

    :goto_4
    sub-int v10, v13, v8

    goto/32 :goto_9d

    nop

    :goto_5
    move/from16 v3, p3

    goto/32 :goto_20

    nop

    :goto_6
    div-int/lit8 v3, v3, 0x2

    goto/32 :goto_37

    nop

    :goto_7
    move-object/from16 v19, v6

    goto/32 :goto_74

    nop

    :goto_8
    mul-float v1, v1, v16

    goto/32 :goto_48

    nop

    :goto_9
    add-int v12, v10, v11

    goto/32 :goto_86

    nop

    :goto_a
    add-int v12, v13, v8

    goto/32 :goto_9b

    nop

    :goto_b
    move/from16 v2, p3

    goto/32 :goto_66

    nop

    :goto_c
    sub-int v12, v3, v1

    goto/32 :goto_59

    nop

    :goto_d
    iput v2, v0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment_value:I

    :goto_e
    nop

    :goto_f
    goto/32 :goto_18

    nop

    :goto_10
    move/from16 v14, v23

    goto/32 :goto_1e

    nop

    :goto_11
    div-int/2addr v1, v3

    goto/32 :goto_5c

    nop

    :goto_12
    iput v2, v0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment_value:I

    goto/32 :goto_3

    nop

    :goto_13
    if-nez v5, :cond_0

    goto/32 :goto_1a

    :cond_0
    goto/32 :goto_2d

    nop

    :goto_14
    invoke-virtual {v3, v2, v10, v4, v12}, Landroid/widget/ImageView;->layout(IIII)V

    goto/32 :goto_2c

    nop

    :goto_15
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    goto/32 :goto_9f

    nop

    :goto_16
    div-int/lit8 v15, v21, 0x2

    goto/32 :goto_88

    nop

    :goto_17
    iget-object v2, v0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    goto/32 :goto_91

    nop

    :goto_18
    return-void

    :goto_19
    goto/16 :goto_f

    :goto_1a
    goto/32 :goto_2f

    nop

    :goto_1b
    move/from16 v22, v15

    :goto_1c
    goto/32 :goto_33

    nop

    :goto_1d
    move-object/from16 v0, p0

    goto/32 :goto_41

    nop

    :goto_1e
    const/4 v15, 0x0

    goto/32 :goto_22

    nop

    :goto_1f
    move/from16 v21, v12

    goto/32 :goto_0

    nop

    :goto_20
    move/from16 v4, p4

    goto/32 :goto_64

    nop

    :goto_21
    invoke-virtual {v9}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v9

    goto/32 :goto_2

    nop

    :goto_22
    invoke-virtual {v3, v15, v10, v14, v12}, Landroid/widget/ImageView;->layout(IIII)V

    goto/32 :goto_76

    nop

    :goto_23
    move/from16 v2, p2

    goto/32 :goto_5

    nop

    :goto_24
    move/from16 v14, v23

    goto/32 :goto_79

    nop

    :goto_25
    iput v4, v0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment_value:I

    goto/32 :goto_5a

    nop

    :goto_26
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setGravity(I)V

    goto/32 :goto_6a

    nop

    :goto_27
    iput v5, v0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment:I

    goto/32 :goto_34

    nop

    :goto_28
    move/from16 v15, v17

    goto/32 :goto_44

    nop

    :goto_29
    sub-int v12, v13, v8

    goto/32 :goto_2e

    nop

    :goto_2a
    float-to-int v14, v14

    goto/32 :goto_94

    nop

    :goto_2b
    iget-object v2, v0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    goto/32 :goto_7d

    nop

    :goto_2c
    iget-object v2, v0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    goto/32 :goto_43

    nop

    :goto_2d
    const/4 v3, 0x1

    goto/32 :goto_80

    nop

    :goto_2e
    invoke-virtual {v7, v9, v12, v6, v13}, Landroid/widget/ImageView;->layout(IIII)V

    goto/32 :goto_84

    nop

    :goto_2f
    move/from16 v23, v7

    goto/32 :goto_57

    nop

    :goto_30
    div-int/lit8 v17, v8, 0x2

    goto/32 :goto_67

    nop

    :goto_31
    const v17, 0x3eaaaaaa

    goto/32 :goto_8e

    nop

    :goto_32
    sub-int v3, v12, v10

    goto/32 :goto_85

    nop

    :goto_33
    sub-int v1, v13, v11

    goto/32 :goto_62

    nop

    :goto_34
    iget-object v6, v0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_58

    nop

    :goto_35
    if-eqz v5, :cond_1

    goto/32 :goto_99

    :cond_1
    goto/32 :goto_38

    nop

    :goto_36
    move/from16 v21, v12

    goto/32 :goto_6f

    nop

    :goto_37
    add-int v21, v12, v10

    goto/32 :goto_9c

    nop

    :goto_38
    iget-object v3, v0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_10

    nop

    :goto_39
    move/from16 v16, v10

    goto/32 :goto_5b

    nop

    :goto_3a
    move/from16 v15, v17

    goto/32 :goto_7c

    nop

    :goto_3b
    move/from16 v23, v7

    goto/32 :goto_1f

    nop

    :goto_3c
    invoke-virtual {v14, v9, v7, v6, v12}, Landroid/widget/TextView;->layout(IIII)V

    goto/32 :goto_40

    nop

    :goto_3d
    move/from16 v4, v22

    goto/32 :goto_7b

    nop

    :goto_3e
    rsub-int/lit8 v7, v13, 0x0

    goto/32 :goto_3c

    nop

    :goto_3f
    move/from16 v14, v23

    goto/32 :goto_19

    nop

    :goto_40
    iget-object v7, v0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    goto/32 :goto_73

    nop

    :goto_41
    move/from16 v1, p1

    goto/32 :goto_23

    nop

    :goto_42
    int-to-float v15, v12

    goto/32 :goto_31

    nop

    :goto_43
    add-int v3, v4, v4

    goto/32 :goto_81

    nop

    :goto_44
    invoke-virtual {v2, v15, v1, v3, v7}, Landroid/widget/ImageView;->layout(IIII)V

    goto/32 :goto_8b

    nop

    :goto_45
    div-int/lit8 v9, v18, 0x2

    goto/32 :goto_96

    nop

    :goto_46
    sub-int v18, v12, v7

    goto/32 :goto_7

    nop

    :goto_47
    const/16 v3, 0x30

    goto/32 :goto_7f

    nop

    :goto_48
    float-to-int v1, v1

    goto/32 :goto_4a

    nop

    :goto_49
    iget-object v14, v0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_97

    nop

    :goto_4a
    div-int/lit8 v16, v8, 0x2

    goto/32 :goto_82

    nop

    :goto_4b
    iput v2, v0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment_value:I

    goto/32 :goto_b

    nop

    :goto_4c
    move/from16 v16, v10

    goto/32 :goto_36

    nop

    :goto_4d
    invoke-virtual {v7, v9, v13, v6, v12}, Landroid/widget/TextView;->layout(IIII)V

    goto/32 :goto_55

    nop

    :goto_4e
    mul-float/2addr v14, v15

    goto/32 :goto_2a

    nop

    :goto_4f
    move/from16 v17, v14

    goto/32 :goto_1b

    nop

    :goto_50
    move/from16 v15, v17

    goto/32 :goto_51

    nop

    :goto_51
    move/from16 v4, v22

    goto/32 :goto_24

    nop

    :goto_52
    move-object/from16 v20, v9

    goto/32 :goto_45

    nop

    :goto_53
    add-int v14, v14, v16

    goto/32 :goto_42

    nop

    :goto_54
    iget-object v14, v0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    goto/32 :goto_78

    nop

    :goto_55
    iget-object v7, v0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    goto/32 :goto_9

    nop

    :goto_56
    sub-int v15, v15, v18

    goto/32 :goto_46

    nop

    :goto_57
    move/from16 v16, v10

    goto/32 :goto_70

    nop

    :goto_58
    invoke-virtual {v6}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    goto/32 :goto_15

    nop

    :goto_59
    sub-int v13, v4, v2

    goto/32 :goto_65

    nop

    :goto_5a
    move/from16 v2, p3

    goto/32 :goto_3a

    nop

    :goto_5b
    int-to-float v10, v13

    goto/32 :goto_1

    nop

    :goto_5c
    add-int v7, v1, v11

    goto/32 :goto_4

    nop

    :goto_5d
    move/from16 v22, v15

    goto/32 :goto_6c

    nop

    :goto_5e
    iget-object v9, v0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    goto/32 :goto_21

    nop

    :goto_5f
    move/from16 v2, p3

    goto/32 :goto_d

    nop

    :goto_60
    const/4 v12, 0x0

    goto/32 :goto_8c

    nop

    :goto_61
    const/4 v14, 0x2

    goto/32 :goto_9a

    nop

    :goto_62
    const/4 v3, 0x2

    goto/32 :goto_11

    nop

    :goto_63
    const v16, 0x3f2aaaab

    goto/32 :goto_8

    nop

    :goto_64
    move/from16 v5, p5

    goto/32 :goto_27

    nop

    :goto_65
    int-to-float v14, v12

    goto/32 :goto_72

    nop

    :goto_66
    move/from16 v4, v22

    goto/32 :goto_98

    nop

    :goto_67
    sub-int v10, v10, v17

    goto/32 :goto_77

    nop

    :goto_68
    invoke-virtual {v3, v2, v10, v15, v12}, Landroid/widget/TextView;->layout(IIII)V

    goto/32 :goto_17

    nop

    :goto_69
    div-int/lit8 v16, v7, 0x2

    goto/32 :goto_53

    nop

    :goto_6a
    iget-object v2, v0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    goto/32 :goto_8a

    nop

    :goto_6b
    iget-object v2, v0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    goto/32 :goto_47

    nop

    :goto_6c
    goto/16 :goto_1c

    :goto_6d
    goto/32 :goto_32

    nop

    :goto_6e
    add-int v12, v13, v13

    goto/32 :goto_4d

    nop

    :goto_6f
    move/from16 v17, v14

    goto/32 :goto_5d

    nop

    :goto_70
    move/from16 v21, v12

    goto/32 :goto_4f

    nop

    :goto_71
    move/from16 v14, v23

    goto/32 :goto_a0

    nop

    :goto_72
    const v15, 0x3f2aaaab

    goto/32 :goto_4e

    nop

    :goto_73
    add-int v12, v1, v11

    goto/32 :goto_8d

    nop

    :goto_74
    const/4 v6, 0x2

    goto/32 :goto_52

    nop

    :goto_75
    sub-int/2addr v1, v11

    goto/32 :goto_39

    nop

    :goto_76
    iget-object v3, v0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    goto/32 :goto_90

    nop

    :goto_77
    move/from16 v17, v14

    goto/32 :goto_61

    nop

    :goto_78
    move/from16 v23, v7

    goto/32 :goto_3e

    nop

    :goto_79
    goto/16 :goto_f

    :goto_7a
    goto/32 :goto_3b

    nop

    :goto_7b
    invoke-virtual {v2, v4, v1, v3, v7}, Landroid/widget/ImageView;->layout(IIII)V

    goto/32 :goto_6b

    nop

    :goto_7c
    move/from16 v4, v22

    goto/32 :goto_3f

    nop

    :goto_7d
    add-int v3, v22, v16

    goto/32 :goto_3d

    nop

    :goto_7e
    div-int/lit8 v18, v7, 0x2

    goto/32 :goto_56

    nop

    :goto_7f
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setGravity(I)V

    goto/32 :goto_5f

    nop

    :goto_80
    if-eq v5, v3, :cond_2

    goto/32 :goto_6d

    :cond_2
    goto/32 :goto_83

    nop

    :goto_81
    invoke-virtual {v2, v4, v10, v3, v12}, Landroid/widget/TextView;->layout(IIII)V

    goto/32 :goto_2b

    nop

    :goto_82
    add-int v1, v1, v16

    goto/32 :goto_75

    nop

    :goto_83
    move/from16 v23, v7

    goto/32 :goto_4c

    nop

    :goto_84
    iget-object v7, v0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    goto/32 :goto_6e

    nop

    :goto_85
    const/16 v18, 0x2

    goto/32 :goto_6

    nop

    :goto_86
    invoke-virtual {v7, v3, v10, v15, v12}, Landroid/widget/ImageView;->layout(IIII)V

    goto/32 :goto_25

    nop

    :goto_87
    float-to-int v10, v10

    goto/32 :goto_30

    nop

    :goto_88
    int-to-float v1, v13

    goto/32 :goto_63

    nop

    :goto_89
    sub-int v2, v21, v14

    goto/32 :goto_95

    nop

    :goto_8a
    add-int v3, v17, v16

    goto/32 :goto_28

    nop

    :goto_8b
    move/from16 v2, p1

    goto/32 :goto_4b

    nop

    :goto_8c
    invoke-virtual {v14, v9, v12, v6, v8}, Landroid/widget/ImageView;->layout(IIII)V

    goto/32 :goto_54

    nop

    :goto_8d
    invoke-virtual {v7, v3, v1, v15, v12}, Landroid/widget/ImageView;->layout(IIII)V

    goto/32 :goto_12

    nop

    :goto_8e
    mul-float v15, v15, v17

    goto/32 :goto_93

    nop

    :goto_8f
    move/from16 v2, p1

    goto/32 :goto_92

    nop

    :goto_90
    rsub-int/lit8 v2, v21, 0x0

    goto/32 :goto_68

    nop

    :goto_91
    const/4 v3, 0x5

    goto/32 :goto_26

    nop

    :goto_92
    move/from16 v15, v17

    goto/32 :goto_71

    nop

    :goto_93
    float-to-int v15, v15

    goto/32 :goto_7e

    nop

    :goto_94
    sub-int/2addr v14, v10

    goto/32 :goto_69

    nop

    :goto_95
    move/from16 v4, v21

    goto/32 :goto_14

    nop

    :goto_96
    add-int v6, v9, v7

    goto/32 :goto_13

    nop

    :goto_97
    move/from16 v21, v12

    goto/32 :goto_60

    nop

    :goto_98
    goto/16 :goto_e

    :goto_99
    goto/32 :goto_8f

    nop

    :goto_9a
    if-eq v5, v14, :cond_3

    goto/32 :goto_7a

    :cond_3
    goto/32 :goto_49

    nop

    :goto_9b
    div-int/2addr v12, v3

    goto/32 :goto_35

    nop

    :goto_9c
    move/from16 v22, v15

    goto/32 :goto_16

    nop

    :goto_9d
    div-int/2addr v10, v3

    goto/32 :goto_a

    nop

    :goto_9e
    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v11

    goto/32 :goto_c

    nop

    :goto_9f
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    goto/32 :goto_5e

    nop

    :goto_a0
    iget-object v3, v0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_89

    nop
.end method

.method public measure(II)V
    .locals 6

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    iget-object v2, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeSafeMeasureSpec(II)I

    move-result v4

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeSafeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/widget/ImageView;->measure(II)V

    iget-object v2, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeSafeMeasureSpec(II)I

    move-result v4

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeSafeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2, v4, v3}, Landroid/widget/TextView;->measure(II)V

    return-void
.end method

.method reset(Z)V
    .locals 8

    goto/32 :goto_3d

    nop

    :goto_0
    int-to-float v5, v1

    goto/32 :goto_c

    nop

    :goto_1
    iget-object v3, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_27

    nop

    :goto_2
    goto/16 :goto_4e

    :goto_3
    goto/32 :goto_4d

    nop

    :goto_4
    if-nez v2, :cond_0

    goto/32 :goto_4c

    :cond_0
    goto/32 :goto_1c

    nop

    :goto_5
    goto/16 :goto_32

    :goto_6
    goto/32 :goto_39

    nop

    :goto_7
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    goto/32 :goto_2b

    nop

    :goto_8
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->offsetTopAndBottom(I)V

    :goto_9
    goto/32 :goto_a

    nop

    :goto_a
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    goto/32 :goto_30

    nop

    :goto_b
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->offsetLeftAndRight(I)V

    goto/32 :goto_4f

    nop

    :goto_c
    int-to-float v6, v3

    goto/32 :goto_d

    nop

    :goto_d
    const/4 v7, 0x0

    goto/32 :goto_23

    nop

    :goto_e
    iget v3, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment_value:I

    goto/32 :goto_33

    nop

    :goto_f
    sub-int/2addr v1, v3

    goto/32 :goto_1f

    nop

    :goto_10
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_b

    nop

    :goto_11
    goto :goto_16

    :goto_12
    goto/32 :goto_15

    nop

    :goto_13
    const/4 v4, 0x2

    goto/32 :goto_55

    nop

    :goto_14
    iget v3, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment:I

    goto/32 :goto_13

    nop

    :goto_15
    move v2, v0

    :goto_16
    goto/32 :goto_25

    nop

    :goto_17
    goto :goto_2c

    :goto_18
    goto/32 :goto_49

    nop

    :goto_19
    const/4 v2, 0x4

    goto/32 :goto_57

    nop

    :goto_1a
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    goto/32 :goto_43

    nop

    :goto_1b
    invoke-virtual {v4}, Landroid/widget/ImageView;->getTop()I

    move-result v4

    goto/32 :goto_21

    nop

    :goto_1c
    move v3, v0

    goto/32 :goto_4b

    nop

    :goto_1d
    iget v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment:I

    goto/32 :goto_42

    nop

    :goto_1e
    iget-object v3, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_45

    nop

    :goto_1f
    goto/16 :goto_4e

    :goto_20
    goto/32 :goto_48

    nop

    :goto_21
    sub-int/2addr v3, v4

    goto/32 :goto_5

    nop

    :goto_22
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto/32 :goto_2d

    nop

    :goto_23
    invoke-direct {v4, v7, v5, v7, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    goto/32 :goto_26

    nop

    :goto_24
    new-instance v4, Landroid/view/animation/TranslateAnimation;

    goto/32 :goto_0

    nop

    :goto_25
    if-nez v2, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_3c

    nop

    :goto_26
    const-wide/16 v5, 0xfa

    goto/32 :goto_52

    nop

    :goto_27
    invoke-virtual {v3}, Landroid/widget/ImageView;->getRight()I

    move-result v3

    goto/32 :goto_2a

    nop

    :goto_28
    if-eq v1, v2, :cond_2

    goto/32 :goto_12

    :cond_2
    goto/32 :goto_11

    nop

    :goto_29
    const v3, 0x10303d6

    goto/32 :goto_47

    nop

    :goto_2a
    sub-int/2addr v1, v3

    goto/32 :goto_2

    nop

    :goto_2b
    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    :goto_2c
    goto/32 :goto_58

    nop

    :goto_2d
    iget-object v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    goto/32 :goto_34

    nop

    :goto_2e
    if-nez p1, :cond_3

    goto/32 :goto_18

    :cond_3
    goto/32 :goto_24

    nop

    :goto_2f
    invoke-virtual {v4}, Landroid/widget/ImageView;->getBottom()I

    move-result v4

    goto/32 :goto_31

    nop

    :goto_30
    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    goto/32 :goto_3a

    nop

    :goto_31
    sub-int/2addr v3, v4

    :goto_32
    nop

    goto/32 :goto_2e

    nop

    :goto_33
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_1b

    nop

    :goto_34
    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v2

    goto/32 :goto_29

    nop

    :goto_35
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    goto/32 :goto_40

    nop

    :goto_36
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_3b

    nop

    :goto_37
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_2f

    nop

    :goto_38
    invoke-virtual {p0, v0}, Lcom/android/internal/widget/SlidingTab$Slider;->setState(I)V

    goto/32 :goto_54

    nop

    :goto_39
    iget v3, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment_value:I

    goto/32 :goto_37

    nop

    :goto_3a
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_3e

    nop

    :goto_3b
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto/32 :goto_17

    nop

    :goto_3c
    if-eqz v1, :cond_4

    goto/32 :goto_20

    :cond_4
    goto/32 :goto_53

    nop

    :goto_3d
    const/4 v0, 0x0

    goto/32 :goto_38

    nop

    :goto_3e
    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    goto/32 :goto_7

    nop

    :goto_3f
    invoke-virtual {v4, v0}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    goto/32 :goto_1a

    nop

    :goto_40
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->offsetTopAndBottom(I)V

    goto/32 :goto_41

    nop

    :goto_41
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_8

    nop

    :goto_42
    const/4 v2, 0x1

    goto/32 :goto_46

    nop

    :goto_43
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    goto/32 :goto_36

    nop

    :goto_44
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/32 :goto_4a

    nop

    :goto_45
    invoke-virtual {v3}, Landroid/widget/ImageView;->getLeft()I

    move-result v3

    goto/32 :goto_f

    nop

    :goto_46
    if-nez v1, :cond_5

    goto/32 :goto_16

    :cond_5
    goto/32 :goto_28

    nop

    :goto_47
    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    goto/32 :goto_59

    nop

    :goto_48
    iget v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment_value:I

    goto/32 :goto_1

    nop

    :goto_49
    if-nez v2, :cond_6

    goto/32 :goto_50

    :cond_6
    goto/32 :goto_51

    nop

    :goto_4a
    iget-object v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    goto/32 :goto_19

    nop

    :goto_4b
    goto/16 :goto_32

    :goto_4c
    goto/32 :goto_14

    nop

    :goto_4d
    move v1, v0

    :goto_4e
    nop

    goto/32 :goto_4

    nop

    :goto_4f
    goto/16 :goto_9

    :goto_50
    goto/32 :goto_35

    nop

    :goto_51
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    goto/32 :goto_56

    nop

    :goto_52
    invoke-virtual {v4, v5, v6}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    goto/32 :goto_3f

    nop

    :goto_53
    iget v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment_value:I

    goto/32 :goto_1e

    nop

    :goto_54
    iget-object v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    goto/32 :goto_22

    nop

    :goto_55
    if-eq v3, v4, :cond_7

    goto/32 :goto_6

    :cond_7
    goto/32 :goto_e

    nop

    :goto_56
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->offsetLeftAndRight(I)V

    goto/32 :goto_10

    nop

    :goto_57
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/32 :goto_1d

    nop

    :goto_58
    return-void

    :goto_59
    iget-object v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_44

    nop
.end method

.method setBarBackgroundResource(I)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    goto/32 :goto_1

    nop
.end method

.method setHintText(I)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method setIcon(I)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_1

    nop
.end method

.method setState(I)V
    .locals 4

    goto/32 :goto_17

    nop

    :goto_0
    if-eq p1, v2, :cond_0

    goto/32 :goto_2b

    :cond_0
    goto/32 :goto_19

    nop

    :goto_1
    goto :goto_6

    :goto_2
    goto/32 :goto_5

    nop

    :goto_3
    iget-object v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_12

    nop

    :goto_4
    invoke-virtual {v1}, Landroid/widget/TextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto/32 :goto_9

    nop

    :goto_5
    move v3, v1

    :goto_6
    goto/32 :goto_25

    nop

    :goto_7
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_14

    nop

    :goto_8
    move v3, v2

    goto/32 :goto_1

    nop

    :goto_9
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :goto_a
    goto/32 :goto_33

    nop

    :goto_b
    const/4 v0, 0x2

    goto/32 :goto_10

    nop

    :goto_c
    aput v2, v0, v1

    goto/32 :goto_1e

    nop

    :goto_d
    const v3, 0x10303d5

    goto/32 :goto_1a

    nop

    :goto_e
    iget-object v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    goto/32 :goto_2c

    nop

    :goto_f
    new-array v0, v2, [I

    goto/32 :goto_13

    nop

    :goto_10
    if-eq p1, v0, :cond_1

    goto/32 :goto_24

    :cond_1
    goto/32 :goto_f

    nop

    :goto_11
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    goto/32 :goto_31

    nop

    :goto_12
    invoke-virtual {v1}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto/32 :goto_2e

    nop

    :goto_13
    const v2, 0x10100a2

    goto/32 :goto_c

    nop

    :goto_14
    if-eq p1, v2, :cond_2

    goto/32 :goto_2

    :cond_2
    goto/32 :goto_8

    nop

    :goto_15
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setPressed(Z)V

    goto/32 :goto_7

    nop

    :goto_16
    iput p1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->currentState:I

    goto/32 :goto_32

    nop

    :goto_17
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    goto/32 :goto_22

    nop

    :goto_18
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    goto/32 :goto_28

    nop

    :goto_19
    move v3, v2

    goto/32 :goto_2a

    nop

    :goto_1a
    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    goto/32 :goto_23

    nop

    :goto_1b
    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    :goto_1c
    goto/32 :goto_16

    nop

    :goto_1d
    invoke-virtual {v1}, Landroid/widget/TextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto/32 :goto_18

    nop

    :goto_1e
    iget-object v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    goto/32 :goto_1d

    nop

    :goto_1f
    const/4 v2, 0x1

    goto/32 :goto_0

    nop

    :goto_20
    move v3, v1

    :goto_21
    goto/32 :goto_15

    nop

    :goto_22
    const/4 v1, 0x0

    goto/32 :goto_1f

    nop

    :goto_23
    goto :goto_1c

    :goto_24
    goto/32 :goto_11

    nop

    :goto_25
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setPressed(Z)V

    goto/32 :goto_b

    nop

    :goto_26
    const v2, 0x10303d6

    goto/32 :goto_1b

    nop

    :goto_27
    invoke-virtual {v1}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto/32 :goto_29

    nop

    :goto_28
    if-nez v1, :cond_3

    goto/32 :goto_a

    :cond_3
    goto/32 :goto_2d

    nop

    :goto_29
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    goto/32 :goto_30

    nop

    :goto_2a
    goto :goto_21

    :goto_2b
    goto/32 :goto_20

    nop

    :goto_2c
    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v2

    goto/32 :goto_d

    nop

    :goto_2d
    iget-object v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    goto/32 :goto_4

    nop

    :goto_2e
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :goto_2f
    goto/32 :goto_e

    nop

    :goto_30
    if-nez v1, :cond_4

    goto/32 :goto_2f

    :cond_4
    goto/32 :goto_3

    nop

    :goto_31
    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_26

    nop

    :goto_32
    return-void

    :goto_33
    iget-object v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_27

    nop
.end method

.method setTabBackgroundResource(I)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/32 :goto_0

    nop
.end method

.method setTarget(I)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method

.method show(Z)V
    .locals 7

    goto/32 :goto_34

    nop

    :goto_0
    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto/32 :goto_19

    nop

    :goto_1
    move v0, v1

    :goto_2
    goto/32 :goto_13

    nop

    :goto_3
    goto :goto_2

    :goto_4
    goto/32 :goto_17

    nop

    :goto_5
    iget v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment:I

    goto/32 :goto_b

    nop

    :goto_6
    const/4 v3, 0x2

    goto/32 :goto_1a

    nop

    :goto_7
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_0

    nop

    :goto_8
    if-nez v0, :cond_0

    goto/32 :goto_26

    :cond_0
    goto/32 :goto_a

    nop

    :goto_9
    invoke-virtual {v3, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    goto/32 :goto_7

    nop

    :goto_a
    if-eq v0, v2, :cond_1

    goto/32 :goto_28

    :cond_1
    goto/32 :goto_27

    nop

    :goto_b
    const/4 v2, 0x1

    goto/32 :goto_8

    nop

    :goto_c
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/32 :goto_30

    nop

    :goto_d
    if-eqz v0, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_15

    nop

    :goto_e
    if-nez v2, :cond_3

    goto/32 :goto_2c

    :cond_3
    goto/32 :goto_d

    nop

    :goto_f
    return-void

    :goto_10
    const/4 v6, 0x0

    goto/32 :goto_2a

    nop

    :goto_11
    goto :goto_1f

    :goto_12
    goto/32 :goto_2e

    nop

    :goto_13
    if-nez v2, :cond_4

    goto/32 :goto_12

    :cond_4
    goto/32 :goto_11

    nop

    :goto_14
    iget-object v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_29

    nop

    :goto_15
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_1c

    nop

    :goto_16
    const-wide/16 v4, 0xfa

    goto/32 :goto_9

    nop

    :goto_17
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_31

    nop

    :goto_18
    int-to-float v4, v4

    goto/32 :goto_36

    nop

    :goto_19
    iget-object v4, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    goto/32 :goto_23

    nop

    :goto_1a
    if-eq v1, v3, :cond_5

    goto/32 :goto_21

    :cond_5
    goto/32 :goto_33

    nop

    :goto_1b
    new-instance v3, Landroid/view/animation/TranslateAnimation;

    goto/32 :goto_2f

    nop

    :goto_1c
    invoke-virtual {v0}, Landroid/widget/ImageView;->getWidth()I

    move-result v0

    goto/32 :goto_3

    nop

    :goto_1d
    if-nez p1, :cond_6

    goto/32 :goto_24

    :cond_6
    goto/32 :goto_5

    nop

    :goto_1e
    neg-int v1, v1

    :goto_1f
    goto/32 :goto_1b

    nop

    :goto_20
    goto :goto_1f

    :goto_21
    goto/32 :goto_14

    nop

    :goto_22
    neg-int v0, v0

    goto/32 :goto_2b

    nop

    :goto_23
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    :goto_24
    goto/32 :goto_f

    nop

    :goto_25
    move v2, v1

    :goto_26
    goto/32 :goto_e

    nop

    :goto_27
    goto :goto_26

    :goto_28
    goto/32 :goto_25

    nop

    :goto_29
    invoke-virtual {v1}, Landroid/widget/ImageView;->getHeight()I

    move-result v1

    goto/32 :goto_1e

    nop

    :goto_2a
    invoke-direct {v3, v4, v6, v5, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    goto/32 :goto_16

    nop

    :goto_2b
    goto/16 :goto_2

    :goto_2c
    goto/32 :goto_1

    nop

    :goto_2d
    invoke-virtual {v1}, Landroid/widget/ImageView;->getHeight()I

    move-result v1

    goto/32 :goto_20

    nop

    :goto_2e
    iget v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->alignment:I

    goto/32 :goto_6

    nop

    :goto_2f
    neg-int v4, v0

    goto/32 :goto_18

    nop

    :goto_30
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_32

    nop

    :goto_31
    invoke-virtual {v0}, Landroid/widget/ImageView;->getWidth()I

    move-result v0

    goto/32 :goto_22

    nop

    :goto_32
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/32 :goto_1d

    nop

    :goto_33
    iget-object v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    goto/32 :goto_2d

    nop

    :goto_34
    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    goto/32 :goto_37

    nop

    :goto_35
    int-to-float v5, v5

    goto/32 :goto_10

    nop

    :goto_36
    neg-int v5, v1

    goto/32 :goto_35

    nop

    :goto_37
    const/4 v1, 0x0

    goto/32 :goto_c

    nop
.end method

.method showTarget()V
    .locals 3

    goto/32 :goto_a

    nop

    :goto_0
    iget-object v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    goto/32 :goto_6

    nop

    :goto_1
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    goto/32 :goto_b

    nop

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto/32 :goto_0

    nop

    :goto_3
    const-wide/16 v1, 0x1f4

    goto/32 :goto_1

    nop

    :goto_4
    const/4 v1, 0x0

    goto/32 :goto_9

    nop

    :goto_5
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/32 :goto_8

    nop

    :goto_6
    const/4 v2, 0x0

    goto/32 :goto_5

    nop

    :goto_7
    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    goto/32 :goto_3

    nop

    :goto_8
    return-void

    :goto_9
    const/high16 v2, 0x3f800000    # 1.0f

    goto/32 :goto_7

    nop

    :goto_a
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    goto/32 :goto_4

    nop

    :goto_b
    iget-object v1, p0, Lcom/android/internal/widget/SlidingTab$Slider;->target:Landroid/widget/ImageView;

    goto/32 :goto_2

    nop
.end method

.method public startAnimation(Landroid/view/animation/Animation;Landroid/view/animation/Animation;)V
    .locals 1

    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public updateDrawableStates()V
    .locals 1

    iget v0, p0, Lcom/android/internal/widget/SlidingTab$Slider;->currentState:I

    invoke-virtual {p0, v0}, Lcom/android/internal/widget/SlidingTab$Slider;->setState(I)V

    return-void
.end method
