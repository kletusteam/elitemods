.class Lcom/android/internal/widget/ChildHelper$Bucket;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/widget/ChildHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Bucket"
.end annotation


# static fields
.field static final BITS_PER_WORD:I = 0x40

.field static final LAST_BIT:J = -0x8000000000000000L


# instance fields
.field mData:J

.field mNext:Lcom/android/internal/widget/ChildHelper$Bucket;


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mData:J

    return-void
.end method

.method private ensureNext()V
    .locals 1

    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mNext:Lcom/android/internal/widget/ChildHelper$Bucket;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/internal/widget/ChildHelper$Bucket;

    invoke-direct {v0}, Lcom/android/internal/widget/ChildHelper$Bucket;-><init>()V

    iput-object v0, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mNext:Lcom/android/internal/widget/ChildHelper$Bucket;

    :cond_0
    return-void
.end method


# virtual methods
.method clear(I)V
    .locals 4

    goto/32 :goto_0

    nop

    :goto_0
    const/16 v0, 0x40

    goto/32 :goto_a

    nop

    :goto_1
    iget-wide v0, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mData:J

    goto/32 :goto_9

    nop

    :goto_2
    not-long v2, v2

    goto/32 :goto_8

    nop

    :goto_3
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/ChildHelper$Bucket;->clear(I)V

    goto/32 :goto_b

    nop

    :goto_4
    if-nez v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_e

    nop

    :goto_5
    iput-wide v0, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mData:J

    :goto_6
    goto/32 :goto_d

    nop

    :goto_7
    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mNext:Lcom/android/internal/widget/ChildHelper$Bucket;

    goto/32 :goto_4

    nop

    :goto_8
    and-long/2addr v0, v2

    goto/32 :goto_5

    nop

    :goto_9
    const-wide/16 v2, 0x1

    goto/32 :goto_f

    nop

    :goto_a
    if-ge p1, v0, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_7

    nop

    :goto_b
    goto :goto_6

    :goto_c
    goto/32 :goto_1

    nop

    :goto_d
    return-void

    :goto_e
    add-int/lit8 v1, p1, -0x40

    goto/32 :goto_3

    nop

    :goto_f
    shl-long/2addr v2, p1

    goto/32 :goto_2

    nop
.end method

.method countOnesBefore(I)I
    .locals 6

    goto/32 :goto_e

    nop

    :goto_0
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/ChildHelper$Bucket;->countOnesBefore(I)I

    move-result v0

    goto/32 :goto_3

    nop

    :goto_1
    const-wide/16 v2, 0x1

    goto/32 :goto_6

    nop

    :goto_2
    if-lt p1, v1, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_f

    nop

    :goto_3
    iget-wide v1, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mData:J

    goto/32 :goto_1b

    nop

    :goto_4
    add-int/lit8 v1, p1, -0x40

    goto/32 :goto_0

    nop

    :goto_5
    iget-wide v0, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mData:J

    goto/32 :goto_14

    nop

    :goto_6
    if-eqz v0, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_d

    nop

    :goto_7
    return v0

    :goto_8
    goto/32 :goto_4

    nop

    :goto_9
    iget-wide v0, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mData:J

    goto/32 :goto_c

    nop

    :goto_a
    return v0

    :goto_b
    goto/32 :goto_2

    nop

    :goto_c
    shl-long v4, v2, p1

    goto/32 :goto_15

    nop

    :goto_d
    if-ge p1, v1, :cond_2

    goto/32 :goto_1d

    :cond_2
    goto/32 :goto_5

    nop

    :goto_e
    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mNext:Lcom/android/internal/widget/ChildHelper$Bucket;

    goto/32 :goto_1a

    nop

    :goto_f
    iget-wide v0, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mData:J

    goto/32 :goto_13

    nop

    :goto_10
    invoke-static {v0, v1}, Ljava/lang/Long;->bitCount(J)I

    move-result v0

    goto/32 :goto_7

    nop

    :goto_11
    add-int/2addr v0, v1

    goto/32 :goto_18

    nop

    :goto_12
    and-long/2addr v0, v4

    goto/32 :goto_10

    nop

    :goto_13
    shl-long v4, v2, p1

    goto/32 :goto_17

    nop

    :goto_14
    invoke-static {v0, v1}, Ljava/lang/Long;->bitCount(J)I

    move-result v0

    goto/32 :goto_1c

    nop

    :goto_15
    sub-long/2addr v4, v2

    goto/32 :goto_19

    nop

    :goto_16
    invoke-static {v0, v1}, Ljava/lang/Long;->bitCount(J)I

    move-result v0

    goto/32 :goto_a

    nop

    :goto_17
    sub-long/2addr v4, v2

    goto/32 :goto_12

    nop

    :goto_18
    return v0

    :goto_19
    and-long/2addr v0, v4

    goto/32 :goto_16

    nop

    :goto_1a
    const/16 v1, 0x40

    goto/32 :goto_1

    nop

    :goto_1b
    invoke-static {v1, v2}, Ljava/lang/Long;->bitCount(J)I

    move-result v1

    goto/32 :goto_11

    nop

    :goto_1c
    return v0

    :goto_1d
    goto/32 :goto_9

    nop
.end method

.method get(I)Z
    .locals 4

    goto/32 :goto_f

    nop

    :goto_0
    const-wide/16 v2, 0x1

    goto/32 :goto_3

    nop

    :goto_1
    if-ge p1, v0, :cond_0

    goto/32 :goto_14

    :cond_0
    goto/32 :goto_7

    nop

    :goto_2
    iget-wide v0, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mData:J

    goto/32 :goto_0

    nop

    :goto_3
    shl-long/2addr v2, p1

    goto/32 :goto_6

    nop

    :goto_4
    const/4 v0, 0x0

    :goto_5
    goto/32 :goto_e

    nop

    :goto_6
    and-long/2addr v0, v2

    goto/32 :goto_12

    nop

    :goto_7
    invoke-direct {p0}, Lcom/android/internal/widget/ChildHelper$Bucket;->ensureNext()V

    goto/32 :goto_11

    nop

    :goto_8
    const/4 v0, 0x1

    goto/32 :goto_c

    nop

    :goto_9
    add-int/lit8 v1, p1, -0x40

    goto/32 :goto_b

    nop

    :goto_a
    cmp-long v0, v0, v2

    goto/32 :goto_10

    nop

    :goto_b
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/ChildHelper$Bucket;->get(I)Z

    move-result v0

    goto/32 :goto_13

    nop

    :goto_c
    goto :goto_5

    :goto_d
    goto/32 :goto_4

    nop

    :goto_e
    return v0

    :goto_f
    const/16 v0, 0x40

    goto/32 :goto_1

    nop

    :goto_10
    if-nez v0, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_8

    nop

    :goto_11
    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mNext:Lcom/android/internal/widget/ChildHelper$Bucket;

    goto/32 :goto_9

    nop

    :goto_12
    const-wide/16 v2, 0x0

    goto/32 :goto_a

    nop

    :goto_13
    return v0

    :goto_14
    goto/32 :goto_2

    nop
.end method

.method insert(IZ)V
    .locals 11

    goto/32 :goto_10

    nop

    :goto_0
    invoke-direct {p0}, Lcom/android/internal/widget/ChildHelper$Bucket;->ensureNext()V

    goto/32 :goto_1c

    nop

    :goto_1
    const-wide/16 v5, 0x1

    goto/32 :goto_1e

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mNext:Lcom/android/internal/widget/ChildHelper$Bucket;

    goto/32 :goto_28

    nop

    :goto_3
    shl-long/2addr v0, v3

    goto/32 :goto_1b

    nop

    :goto_4
    const-wide/16 v4, 0x0

    goto/32 :goto_11

    nop

    :goto_5
    if-nez v2, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_26

    nop

    :goto_6
    goto/16 :goto_24

    :goto_7
    goto/32 :goto_23

    nop

    :goto_8
    sub-long/2addr v7, v5

    goto/32 :goto_2b

    nop

    :goto_9
    if-nez v3, :cond_1

    goto/32 :goto_15

    :cond_1
    :goto_a
    goto/32 :goto_0

    nop

    :goto_b
    const-wide/high16 v2, -0x8000000000000000L

    goto/32 :goto_2c

    nop

    :goto_c
    goto :goto_15

    :goto_d
    goto/32 :goto_22

    nop

    :goto_e
    and-long/2addr v0, v9

    goto/32 :goto_3

    nop

    :goto_f
    iput-wide v9, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mData:J

    goto/32 :goto_1f

    nop

    :goto_10
    const/16 v0, 0x40

    goto/32 :goto_1d

    nop

    :goto_11
    cmp-long v2, v2, v4

    goto/32 :goto_20

    nop

    :goto_12
    const/4 v4, 0x0

    goto/32 :goto_5

    nop

    :goto_13
    invoke-virtual {v0, v1, p2}, Lcom/android/internal/widget/ChildHelper$Bucket;->insert(IZ)V

    goto/32 :goto_c

    nop

    :goto_14
    invoke-virtual {v3, v4, v2}, Lcom/android/internal/widget/ChildHelper$Bucket;->insert(IZ)V

    :goto_15
    goto/32 :goto_19

    nop

    :goto_16
    goto :goto_2a

    :goto_17
    goto/32 :goto_29

    nop

    :goto_18
    not-long v9, v7

    goto/32 :goto_e

    nop

    :goto_19
    return-void

    :goto_1a
    iget-object v3, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mNext:Lcom/android/internal/widget/ChildHelper$Bucket;

    goto/32 :goto_9

    nop

    :goto_1b
    or-long v9, v5, v0

    goto/32 :goto_f

    nop

    :goto_1c
    iget-object v3, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mNext:Lcom/android/internal/widget/ChildHelper$Bucket;

    goto/32 :goto_14

    nop

    :goto_1d
    if-ge p1, v0, :cond_2

    goto/32 :goto_d

    :cond_2
    goto/32 :goto_27

    nop

    :goto_1e
    shl-long v7, v5, p1

    goto/32 :goto_8

    nop

    :goto_1f
    if-nez p2, :cond_3

    goto/32 :goto_17

    :cond_3
    goto/32 :goto_21

    nop

    :goto_20
    const/4 v3, 0x1

    goto/32 :goto_12

    nop

    :goto_21
    invoke-virtual {p0, p1}, Lcom/android/internal/widget/ChildHelper$Bucket;->set(I)V

    goto/32 :goto_16

    nop

    :goto_22
    iget-wide v0, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mData:J

    goto/32 :goto_b

    nop

    :goto_23
    move v2, v4

    :goto_24
    goto/32 :goto_1

    nop

    :goto_25
    if-eqz v2, :cond_4

    goto/32 :goto_a

    :cond_4
    goto/32 :goto_1a

    nop

    :goto_26
    move v2, v3

    goto/32 :goto_6

    nop

    :goto_27
    invoke-direct {p0}, Lcom/android/internal/widget/ChildHelper$Bucket;->ensureNext()V

    goto/32 :goto_2

    nop

    :goto_28
    add-int/lit8 v1, p1, -0x40

    goto/32 :goto_13

    nop

    :goto_29
    invoke-virtual {p0, p1}, Lcom/android/internal/widget/ChildHelper$Bucket;->clear(I)V

    :goto_2a
    goto/32 :goto_25

    nop

    :goto_2b
    and-long v5, v0, v7

    goto/32 :goto_18

    nop

    :goto_2c
    and-long/2addr v2, v0

    goto/32 :goto_4

    nop
.end method

.method remove(I)Z
    .locals 11

    goto/32 :goto_11

    nop

    :goto_0
    const-wide/16 v0, 0x1

    goto/32 :goto_1e

    nop

    :goto_1
    not-long v9, v2

    goto/32 :goto_1f

    nop

    :goto_2
    if-nez v7, :cond_0

    goto/32 :goto_1b

    :cond_0
    goto/32 :goto_6

    nop

    :goto_3
    and-long v6, v4, v2

    goto/32 :goto_15

    nop

    :goto_4
    if-ge p1, v0, :cond_1

    goto/32 :goto_18

    :cond_1
    goto/32 :goto_24

    nop

    :goto_5
    add-int/lit8 v1, p1, -0x40

    goto/32 :goto_c

    nop

    :goto_6
    invoke-virtual {v7, v8}, Lcom/android/internal/widget/ChildHelper$Bucket;->get(I)Z

    move-result v7

    goto/32 :goto_23

    nop

    :goto_7
    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mNext:Lcom/android/internal/widget/ChildHelper$Bucket;

    goto/32 :goto_5

    nop

    :goto_8
    iget-wide v4, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mData:J

    goto/32 :goto_3

    nop

    :goto_9
    move v6, v8

    :goto_a
    goto/32 :goto_21

    nop

    :goto_b
    const/16 v7, 0x3f

    goto/32 :goto_25

    nop

    :goto_c
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/ChildHelper$Bucket;->remove(I)Z

    move-result v0

    goto/32 :goto_17

    nop

    :goto_d
    goto :goto_a

    :goto_e
    goto/32 :goto_9

    nop

    :goto_f
    sub-long/2addr v2, v0

    goto/32 :goto_1c

    nop

    :goto_10
    return v6

    :goto_11
    const/16 v0, 0x40

    goto/32 :goto_4

    nop

    :goto_12
    and-long/2addr v4, v9

    goto/32 :goto_22

    nop

    :goto_13
    if-nez v6, :cond_2

    goto/32 :goto_e

    :cond_2
    goto/32 :goto_29

    nop

    :goto_14
    invoke-static {v4, v5, v7}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v4

    goto/32 :goto_28

    nop

    :goto_15
    const-wide/16 v8, 0x0

    goto/32 :goto_19

    nop

    :goto_16
    iput-wide v9, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mData:J

    goto/32 :goto_2a

    nop

    :goto_17
    return v0

    :goto_18
    goto/32 :goto_0

    nop

    :goto_19
    cmp-long v6, v6, v8

    goto/32 :goto_1d

    nop

    :goto_1a
    invoke-virtual {v7, v8}, Lcom/android/internal/widget/ChildHelper$Bucket;->remove(I)Z

    :goto_1b
    goto/32 :goto_10

    nop

    :goto_1c
    and-long v0, v4, v2

    goto/32 :goto_1

    nop

    :goto_1d
    const/4 v7, 0x1

    goto/32 :goto_20

    nop

    :goto_1e
    shl-long v2, v0, p1

    goto/32 :goto_8

    nop

    :goto_1f
    and-long/2addr v4, v9

    goto/32 :goto_14

    nop

    :goto_20
    const/4 v8, 0x0

    goto/32 :goto_13

    nop

    :goto_21
    not-long v9, v2

    goto/32 :goto_12

    nop

    :goto_22
    iput-wide v4, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mData:J

    goto/32 :goto_f

    nop

    :goto_23
    if-nez v7, :cond_3

    goto/32 :goto_26

    :cond_3
    goto/32 :goto_b

    nop

    :goto_24
    invoke-direct {p0}, Lcom/android/internal/widget/ChildHelper$Bucket;->ensureNext()V

    goto/32 :goto_7

    nop

    :goto_25
    invoke-virtual {p0, v7}, Lcom/android/internal/widget/ChildHelper$Bucket;->set(I)V

    :goto_26
    goto/32 :goto_27

    nop

    :goto_27
    iget-object v7, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mNext:Lcom/android/internal/widget/ChildHelper$Bucket;

    goto/32 :goto_1a

    nop

    :goto_28
    or-long v9, v0, v4

    goto/32 :goto_16

    nop

    :goto_29
    move v6, v7

    goto/32 :goto_d

    nop

    :goto_2a
    iget-object v7, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mNext:Lcom/android/internal/widget/ChildHelper$Bucket;

    goto/32 :goto_2

    nop
.end method

.method reset()V
    .locals 2

    goto/32 :goto_6

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mNext:Lcom/android/internal/widget/ChildHelper$Bucket;

    goto/32 :goto_5

    nop

    :goto_1
    iput-wide v0, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mData:J

    goto/32 :goto_0

    nop

    :goto_2
    invoke-virtual {v0}, Lcom/android/internal/widget/ChildHelper$Bucket;->reset()V

    :goto_3
    goto/32 :goto_4

    nop

    :goto_4
    return-void

    :goto_5
    if-nez v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_2

    nop

    :goto_6
    const-wide/16 v0, 0x0

    goto/32 :goto_1

    nop
.end method

.method set(I)V
    .locals 4

    goto/32 :goto_0

    nop

    :goto_0
    const/16 v0, 0x40

    goto/32 :goto_e

    nop

    :goto_1
    shl-long/2addr v2, p1

    goto/32 :goto_a

    nop

    :goto_2
    add-int/lit8 v1, p1, -0x40

    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/ChildHelper$Bucket;->set(I)V

    goto/32 :goto_8

    nop

    :goto_4
    return-void

    :goto_5
    invoke-direct {p0}, Lcom/android/internal/widget/ChildHelper$Bucket;->ensureNext()V

    goto/32 :goto_7

    nop

    :goto_6
    iget-wide v0, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mData:J

    goto/32 :goto_d

    nop

    :goto_7
    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mNext:Lcom/android/internal/widget/ChildHelper$Bucket;

    goto/32 :goto_2

    nop

    :goto_8
    goto :goto_c

    :goto_9
    goto/32 :goto_6

    nop

    :goto_a
    or-long/2addr v0, v2

    goto/32 :goto_b

    nop

    :goto_b
    iput-wide v0, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mData:J

    :goto_c
    goto/32 :goto_4

    nop

    :goto_d
    const-wide/16 v2, 0x1

    goto/32 :goto_1

    nop

    :goto_e
    if-ge p1, v0, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_5

    nop
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mNext:Lcom/android/internal/widget/ChildHelper$Bucket;

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mData:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toBinaryString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mNext:Lcom/android/internal/widget/ChildHelper$Bucket;

    invoke-virtual {v1}, Lcom/android/internal/widget/ChildHelper$Bucket;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "xx"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/internal/widget/ChildHelper$Bucket;->mData:J

    invoke-static {v1, v2}, Ljava/lang/Long;->toBinaryString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method
