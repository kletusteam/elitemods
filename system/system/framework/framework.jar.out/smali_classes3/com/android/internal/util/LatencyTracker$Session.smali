.class Lcom/android/internal/util/LatencyTracker$Session;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/util/LatencyTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Session"
.end annotation


# instance fields
.field private final mAction:I

.field private mEndRtc:J

.field private final mName:Ljava/lang/String;

.field private mStartRtc:J

.field private final mTag:Ljava/lang/String;

.field private mTimeoutRunnable:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(ILjava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/internal/util/LatencyTracker$Session;->mStartRtc:J

    iput-wide v0, p0, Lcom/android/internal/util/LatencyTracker$Session;->mEndRtc:J

    iput p1, p0, Lcom/android/internal/util/LatencyTracker$Session;->mAction:I

    iput-object p2, p0, Lcom/android/internal/util/LatencyTracker$Session;->mTag:Ljava/lang/String;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/internal/util/LatencyTracker;->-$$Nest$sfgetSTATSD_ACTION()[I

    move-result-object v0

    aget v0, v0, p1

    invoke-static {v0}, Lcom/android/internal/util/LatencyTracker;->getNameOfAction(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/android/internal/util/LatencyTracker;->-$$Nest$sfgetSTATSD_ACTION()[I

    move-result-object v1

    aget v1, v1, p1

    invoke-static {v1}, Lcom/android/internal/util/LatencyTracker;->getNameOfAction(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "::"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/android/internal/util/LatencyTracker$Session;->mName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method begin(Ljava/lang/Runnable;)V
    .locals 5

    goto/32 :goto_7

    nop

    :goto_0
    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    goto/32 :goto_8

    nop

    :goto_1
    iput-object p1, p0, Lcom/android/internal/util/LatencyTracker$Session;->mTimeoutRunnable:Ljava/lang/Runnable;

    goto/32 :goto_4

    nop

    :goto_2
    invoke-virtual {p0}, Lcom/android/internal/util/LatencyTracker$Session;->traceName()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_3
    const-wide/16 v1, 0x1000

    goto/32 :goto_5

    nop

    :goto_4
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    goto/32 :goto_d

    nop

    :goto_5
    const/4 v3, 0x0

    goto/32 :goto_9

    nop

    :goto_6
    return-void

    :goto_7
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    goto/32 :goto_c

    nop

    :goto_8
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/32 :goto_6

    nop

    :goto_9
    invoke-static {v1, v2, v0, v3}, Landroid/os/Trace;->asyncTraceBegin(JLjava/lang/String;I)V

    goto/32 :goto_1

    nop

    :goto_a
    const-wide/16 v3, 0xf

    goto/32 :goto_0

    nop

    :goto_b
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    goto/32 :goto_a

    nop

    :goto_c
    iput-wide v0, p0, Lcom/android/internal/util/LatencyTracker$Session;->mStartRtc:J

    goto/32 :goto_2

    nop

    :goto_d
    iget-object v1, p0, Lcom/android/internal/util/LatencyTracker$Session;->mTimeoutRunnable:Ljava/lang/Runnable;

    goto/32 :goto_b

    nop
.end method

.method cancel()V
    .locals 4

    goto/32 :goto_4

    nop

    :goto_0
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto/32 :goto_3

    nop

    :goto_1
    invoke-static {v1, v2, v0, v3}, Landroid/os/Trace;->asyncTraceEnd(JLjava/lang/String;I)V

    goto/32 :goto_6

    nop

    :goto_2
    const/4 v3, 0x0

    goto/32 :goto_1

    nop

    :goto_3
    const/4 v0, 0x0

    goto/32 :goto_9

    nop

    :goto_4
    invoke-virtual {p0}, Lcom/android/internal/util/LatencyTracker$Session;->traceName()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_5
    iget-object v1, p0, Lcom/android/internal/util/LatencyTracker$Session;->mTimeoutRunnable:Ljava/lang/Runnable;

    goto/32 :goto_0

    nop

    :goto_6
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_7
    const-wide/16 v1, 0x1000

    goto/32 :goto_2

    nop

    :goto_8
    return-void

    :goto_9
    iput-object v0, p0, Lcom/android/internal/util/LatencyTracker$Session;->mTimeoutRunnable:Ljava/lang/Runnable;

    goto/32 :goto_8

    nop
.end method

.method duration()I
    .locals 4

    goto/32 :goto_2

    nop

    :goto_0
    return v0

    :goto_1
    long-to-int v0, v0

    goto/32 :goto_0

    nop

    :goto_2
    iget-wide v0, p0, Lcom/android/internal/util/LatencyTracker$Session;->mEndRtc:J

    goto/32 :goto_4

    nop

    :goto_3
    sub-long/2addr v0, v2

    goto/32 :goto_1

    nop

    :goto_4
    iget-wide v2, p0, Lcom/android/internal/util/LatencyTracker$Session;->mStartRtc:J

    goto/32 :goto_3

    nop
.end method

.method end()V
    .locals 4

    goto/32 :goto_3

    nop

    :goto_0
    iget-object v1, p0, Lcom/android/internal/util/LatencyTracker$Session;->mTimeoutRunnable:Ljava/lang/Runnable;

    goto/32 :goto_9

    nop

    :goto_1
    return-void

    :goto_2
    const/4 v0, 0x0

    goto/32 :goto_6

    nop

    :goto_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    goto/32 :goto_a

    nop

    :goto_4
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_5
    const/4 v3, 0x0

    goto/32 :goto_7

    nop

    :goto_6
    iput-object v0, p0, Lcom/android/internal/util/LatencyTracker$Session;->mTimeoutRunnable:Ljava/lang/Runnable;

    goto/32 :goto_1

    nop

    :goto_7
    invoke-static {v1, v2, v0, v3}, Landroid/os/Trace;->asyncTraceEnd(JLjava/lang/String;I)V

    goto/32 :goto_4

    nop

    :goto_8
    const-wide/16 v1, 0x1000

    goto/32 :goto_5

    nop

    :goto_9
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto/32 :goto_2

    nop

    :goto_a
    iput-wide v0, p0, Lcom/android/internal/util/LatencyTracker$Session;->mEndRtc:J

    goto/32 :goto_b

    nop

    :goto_b
    invoke-virtual {p0}, Lcom/android/internal/util/LatencyTracker$Session;->traceName()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_8

    nop
.end method

.method name()Ljava/lang/String;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/util/LatencyTracker$Session;->mName:Ljava/lang/String;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method traceName()Ljava/lang/String;
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p0, Lcom/android/internal/util/LatencyTracker$Session;->mAction:I

    goto/32 :goto_1

    nop

    :goto_1
    iget-object v1, p0, Lcom/android/internal/util/LatencyTracker$Session;->mTag:Ljava/lang/String;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-static {v0, v1}, Lcom/android/internal/util/LatencyTracker;->-$$Nest$smgetTraceNameOfAction(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_3
    return-object v0
.end method
