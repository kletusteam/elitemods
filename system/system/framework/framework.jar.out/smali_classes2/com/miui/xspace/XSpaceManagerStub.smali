.class public Lcom/miui/xspace/XSpaceManagerStub;
.super Ljava/lang/Object;


# static fields
.field private static final TAG:Ljava/lang/String; = "XSpaceManagerStub"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/miui/xspace/XSpaceManagerStub;
    .locals 1

    const-class v0, Lcom/miui/xspace/XSpaceManagerStub;

    invoke-static {v0}, Lcom/miui/base/MiuiStubUtil;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/xspace/XSpaceManagerStub;

    return-object v0
.end method


# virtual methods
.method public belongToCrossXSpaceSecureSettings(ILjava/lang/String;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public belongToCrossXSpaceSettings(ILjava/lang/String;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public canCrossUser(II)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public checkAndResolve(Landroid/app/Activity;Landroid/content/Intent;Lcom/android/internal/app/AlertController$AlertParams;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getXSpaceIconMaskId()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getXSpaceUserFlag()I
    .locals 1

    const/high16 v0, 0x800000

    return v0
.end method

.method public getXSpaceUserId()I
    .locals 1

    const/16 v0, -0x2710

    return v0
.end method

.method public hookGetStorageVolume(Landroid/content/Context;Ljava/io/File;Landroid/os/storage/StorageVolume;)Landroid/os/storage/StorageVolume;
    .locals 0

    return-object p3
.end method

.method public isCrossUserIncomingUri(Landroid/content/Context;I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isCrossUserService(Landroid/content/pm/ServiceInfo;I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isUidBelongtoXSpace(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isXSpaceMatch(JZI)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isXSpaceUserHandle(Landroid/os/UserHandle;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isXSpaceUserId(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
