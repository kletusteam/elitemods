.class Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/telephony/gsm/SmsMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PduParser"
.end annotation


# instance fields
.field mCur:I

.field mPdu:[B

.field mUserData:[B

.field mUserDataHeader:Lcom/android/internal/telephony/SmsHeader;

.field mUserDataSeptetPadding:I


# direct methods
.method constructor <init>([B)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mPdu:[B

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    iput v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mUserDataSeptetPadding:I

    return-void
.end method


# virtual methods
.method constructUserData(ZZ)I
    .locals 9

    goto/32 :goto_1e

    nop

    :goto_0
    mul-int/lit8 v6, v6, 0x8

    goto/32 :goto_3b

    nop

    :goto_1
    sub-int/2addr v7, v6

    goto/32 :goto_36

    nop

    :goto_2
    if-nez p2, :cond_0

    goto/32 :goto_2a

    :cond_0
    goto/32 :goto_20

    nop

    :goto_3
    move v1, v5

    :goto_4
    goto/32 :goto_5

    nop

    :goto_5
    sub-int v1, v0, v1

    goto/32 :goto_26

    nop

    :goto_6
    add-int/lit8 v6, v2, 0x1

    goto/32 :goto_8

    nop

    :goto_7
    iput-object v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mUserDataHeader:Lcom/android/internal/telephony/SmsHeader;

    goto/32 :goto_22

    nop

    :goto_8
    aget-byte v2, v1, v2

    goto/32 :goto_3f

    nop

    :goto_9
    add-int/lit8 v2, v0, 0x1

    goto/32 :goto_18

    nop

    :goto_a
    goto/16 :goto_28

    :goto_b
    goto/32 :goto_27

    nop

    :goto_c
    move v7, v5

    :goto_d
    goto/32 :goto_41

    nop

    :goto_e
    array-length v1, v1

    goto/32 :goto_37

    nop

    :goto_f
    goto :goto_d

    :goto_10
    goto/32 :goto_c

    nop

    :goto_11
    rem-int/lit8 v7, v6, 0x7

    goto/32 :goto_32

    nop

    :goto_12
    const/4 v1, 0x0

    :goto_13
    goto/32 :goto_35

    nop

    :goto_14
    const/4 v3, 0x0

    goto/32 :goto_17

    nop

    :goto_15
    array-length v5, v5

    goto/32 :goto_16

    nop

    :goto_16
    return v5

    :goto_17
    const/4 v4, 0x0

    goto/32 :goto_19

    nop

    :goto_18
    aget-byte v0, v1, v0

    goto/32 :goto_3e

    nop

    :goto_19
    const/4 v5, 0x0

    goto/32 :goto_40

    nop

    :goto_1a
    iget-object v7, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mPdu:[B

    goto/32 :goto_33

    nop

    :goto_1b
    const/4 v7, 0x1

    goto/32 :goto_f

    nop

    :goto_1c
    goto :goto_4

    :goto_1d
    goto/32 :goto_3

    nop

    :goto_1e
    iget v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_25

    nop

    :goto_1f
    mul-int/lit8 v7, v3, 0x7

    goto/32 :goto_1

    nop

    :goto_20
    sub-int v6, v0, v3

    goto/32 :goto_2b

    nop

    :goto_21
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mPdu:[B

    goto/32 :goto_e

    nop

    :goto_22
    add-int v1, v6, v4

    goto/32 :goto_24

    nop

    :goto_23
    if-nez p1, :cond_1

    goto/32 :goto_1d

    :cond_1
    goto/32 :goto_34

    nop

    :goto_24
    add-int/lit8 v6, v4, 0x1

    goto/32 :goto_0

    nop

    :goto_25
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mPdu:[B

    goto/32 :goto_9

    nop

    :goto_26
    if-ltz v1, :cond_2

    goto/32 :goto_13

    :cond_2
    goto/32 :goto_12

    nop

    :goto_27
    move v5, v6

    :goto_28
    goto/32 :goto_29

    nop

    :goto_29
    return v5

    :goto_2a
    goto/32 :goto_3a

    nop

    :goto_2b
    if-ltz v6, :cond_3

    goto/32 :goto_b

    :cond_3
    goto/32 :goto_a

    nop

    :goto_2c
    goto :goto_13

    :goto_2d
    goto/32 :goto_23

    nop

    :goto_2e
    move v2, v1

    :goto_2f
    goto/32 :goto_42

    nop

    :goto_30
    invoke-static {v1, v6, v2, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto/32 :goto_31

    nop

    :goto_31
    invoke-static {v2}, Lcom/android/internal/telephony/SmsHeader;->fromByteArray([B)Lcom/android/internal/telephony/SmsHeader;

    move-result-object v1

    goto/32 :goto_7

    nop

    :goto_32
    if-gtz v7, :cond_4

    goto/32 :goto_10

    :cond_4
    goto/32 :goto_1b

    nop

    :goto_33
    array-length v8, v6

    goto/32 :goto_39

    nop

    :goto_34
    add-int/lit8 v1, v4, 0x1

    goto/32 :goto_1c

    nop

    :goto_35
    new-array v6, v1, [B

    goto/32 :goto_38

    nop

    :goto_36
    iput v7, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mUserDataSeptetPadding:I

    goto/32 :goto_2e

    nop

    :goto_37
    sub-int/2addr v1, v2

    goto/32 :goto_2c

    nop

    :goto_38
    iput-object v6, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mUserData:[B

    goto/32 :goto_1a

    nop

    :goto_39
    invoke-static {v7, v2, v6, v5, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto/32 :goto_3d

    nop

    :goto_3a
    iget-object v5, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mUserData:[B

    goto/32 :goto_15

    nop

    :goto_3b
    div-int/lit8 v3, v6, 0x7

    goto/32 :goto_11

    nop

    :goto_3c
    new-array v2, v4, [B

    goto/32 :goto_30

    nop

    :goto_3d
    iput v2, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_2

    nop

    :goto_3e
    and-int/lit16 v0, v0, 0xff

    goto/32 :goto_14

    nop

    :goto_3f
    and-int/lit16 v4, v2, 0xff

    goto/32 :goto_3c

    nop

    :goto_40
    if-nez p1, :cond_5

    goto/32 :goto_2f

    :cond_5
    goto/32 :goto_6

    nop

    :goto_41
    add-int/2addr v3, v7

    goto/32 :goto_1f

    nop

    :goto_42
    if-nez p2, :cond_6

    goto/32 :goto_2d

    :cond_6
    goto/32 :goto_21

    nop
.end method

.method getAddress()Lcom/android/internal/telephony/gsm/GsmSmsAddress;
    .locals 6

    goto/32 :goto_e

    nop

    :goto_0
    div-int/lit8 v3, v3, 0x2

    goto/32 :goto_b

    nop

    :goto_1
    iget v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_c

    nop

    :goto_2
    throw v4

    :goto_3
    aget-byte v2, v0, v1

    goto/32 :goto_8

    nop

    :goto_4
    move-object v0, v4

    nop

    goto/32 :goto_1

    nop

    :goto_5
    iput v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_d

    nop

    :goto_6
    iget v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_3

    nop

    :goto_7
    new-instance v4, Ljava/lang/RuntimeException;

    goto/32 :goto_10

    nop

    :goto_8
    and-int/lit16 v2, v2, 0xff

    goto/32 :goto_f

    nop

    :goto_9
    const/4 v1, 0x0

    goto/32 :goto_7

    nop

    :goto_a
    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_2

    nop

    :goto_b
    add-int/lit8 v3, v3, 0x2

    :try_start_0
    new-instance v4, Lcom/android/internal/telephony/gsm/GsmSmsAddress;

    invoke-direct {v4, v0, v1, v3}, Lcom/android/internal/telephony/gsm/GsmSmsAddress;-><init>([BII)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_4

    nop

    :goto_c
    add-int/2addr v1, v3

    goto/32 :goto_5

    nop

    :goto_d
    return-object v0

    :catch_0
    move-exception v0

    goto/32 :goto_9

    nop

    :goto_e
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mPdu:[B

    goto/32 :goto_6

    nop

    :goto_f
    add-int/lit8 v3, v2, 0x1

    goto/32 :goto_0

    nop

    :goto_10
    invoke-virtual {v0}, Ljava/text/ParseException;->getMessage()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_a

    nop
.end method

.method getByte()I
    .locals 3

    goto/32 :goto_2

    nop

    :goto_0
    iget v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_1

    nop

    :goto_1
    add-int/lit8 v2, v1, 0x1

    goto/32 :goto_5

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mPdu:[B

    goto/32 :goto_0

    nop

    :goto_3
    return v0

    :goto_4
    aget-byte v0, v0, v1

    goto/32 :goto_6

    nop

    :goto_5
    iput v2, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_4

    nop

    :goto_6
    and-int/lit16 v0, v0, 0xff

    goto/32 :goto_3

    nop
.end method

.method getSCAddress()Ljava/lang/String;
    .locals 4

    goto/32 :goto_5

    nop

    :goto_0
    iput v2, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_9

    nop

    :goto_1
    move-object v1, v2

    :goto_2
    goto/32 :goto_a

    nop

    :goto_3
    const/4 v1, 0x0

    goto/32 :goto_c

    nop

    :goto_4
    const/4 v2, 0x0

    goto/32 :goto_1

    nop

    :goto_5
    invoke-virtual {p0}, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->getByte()I

    move-result v0

    goto/32 :goto_f

    nop

    :goto_6
    invoke-static {v2, v3, v1}, Lcom/android/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/32 :goto_4

    nop

    :goto_7
    add-int/2addr v2, v0

    goto/32 :goto_0

    nop

    :goto_8
    const-string v2, "SmsMessage"

    goto/32 :goto_b

    nop

    :goto_9
    return-object v1

    :goto_a
    iget v2, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_7

    nop

    :goto_b
    const-string v3, "invalid SC address: "

    goto/32 :goto_6

    nop

    :goto_c
    goto :goto_2

    :goto_d
    :try_start_0
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mPdu:[B

    iget v2, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    const/4 v3, 0x2

    invoke-static {v1, v2, v0, v3}, Landroid/telephony/PhoneNumberUtils;->calledPartyBCDToString([BIII)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_e

    nop

    :goto_e
    goto :goto_2

    :catch_0
    move-exception v1

    goto/32 :goto_8

    nop

    :goto_f
    if-eqz v0, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_3

    nop
.end method

.method getSCTimestampMillis()J
    .locals 15

    goto/32 :goto_35

    nop

    :goto_0
    invoke-static {v2}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmBcdByteToInt(B)I

    move-result v9

    goto/32 :goto_21

    nop

    :goto_1
    invoke-static {v1}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmBcdByteToInt(B)I

    move-result v1

    goto/32 :goto_a

    nop

    :goto_2
    and-int/lit8 v3, v12, 0x8

    goto/32 :goto_30

    nop

    :goto_3
    return-wide v3

    :goto_4
    iput v4, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_16

    nop

    :goto_5
    goto :goto_1a

    :goto_6
    goto/32 :goto_19

    nop

    :goto_7
    add-int/lit8 v4, v3, 0x1

    goto/32 :goto_40

    nop

    :goto_8
    goto/16 :goto_44

    :goto_9
    goto/32 :goto_43

    nop

    :goto_a
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mPdu:[B

    goto/32 :goto_3a

    nop

    :goto_b
    invoke-static {v2}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmBcdByteToInt(B)I

    move-result v2

    goto/32 :goto_2

    nop

    :goto_c
    move v4, v8

    goto/32 :goto_45

    nop

    :goto_d
    add-int/lit8 v4, v3, 0x1

    goto/32 :goto_e

    nop

    :goto_e
    iput v4, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_25

    nop

    :goto_f
    iget v3, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_7

    nop

    :goto_10
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mPdu:[B

    goto/32 :goto_f

    nop

    :goto_11
    iget v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_12

    nop

    :goto_12
    add-int/lit8 v2, v1, 0x1

    goto/32 :goto_3b

    nop

    :goto_13
    iput v4, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_48

    nop

    :goto_14
    move v3, v2

    goto/32 :goto_5

    nop

    :goto_15
    iput v3, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_28

    nop

    :goto_16
    aget-byte v2, v2, v3

    goto/32 :goto_36

    nop

    :goto_17
    mul-long/2addr v6, v4

    goto/32 :goto_37

    nop

    :goto_18
    move v6, v10

    goto/32 :goto_49

    nop

    :goto_19
    neg-int v3, v2

    :goto_1a
    goto/32 :goto_3d

    nop

    :goto_1b
    add-int/lit8 v4, v3, 0x1

    goto/32 :goto_13

    nop

    :goto_1c
    iget-object v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mPdu:[B

    goto/32 :goto_23

    nop

    :goto_1d
    add-int/lit8 v4, v3, 0x1

    goto/32 :goto_4

    nop

    :goto_1e
    int-to-byte v2, v2

    goto/32 :goto_b

    nop

    :goto_1f
    move v3, v1

    goto/32 :goto_c

    nop

    :goto_20
    aget-byte v12, v2, v3

    goto/32 :goto_2e

    nop

    :goto_21
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mPdu:[B

    goto/32 :goto_2c

    nop

    :goto_22
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mPdu:[B

    goto/32 :goto_2d

    nop

    :goto_23
    iget v2, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_29

    nop

    :goto_24
    sub-long/2addr v4, v6

    goto/32 :goto_2b

    nop

    :goto_25
    aget-byte v2, v2, v3

    goto/32 :goto_47

    nop

    :goto_26
    mul-int/lit8 v2, v13, 0xf

    goto/32 :goto_34

    nop

    :goto_27
    invoke-static {v4, v5, v3}, Lcom/android/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/32 :goto_42

    nop

    :goto_28
    aget-byte v1, v1, v2

    goto/32 :goto_1

    nop

    :goto_29
    add-int/lit8 v3, v2, 0x1

    goto/32 :goto_15

    nop

    :goto_2a
    const/16 v2, 0x5a

    goto/32 :goto_4b

    nop

    :goto_2b
    const-wide/16 v6, 0x3e8

    goto/32 :goto_17

    nop

    :goto_2c
    iget v3, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_2f

    nop

    :goto_2d
    iget v3, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_1b

    nop

    :goto_2e
    and-int/lit8 v2, v12, -0x9

    goto/32 :goto_1e

    nop

    :goto_2f
    add-int/lit8 v4, v3, 0x1

    goto/32 :goto_3c

    nop

    :goto_30
    if-eqz v3, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_14

    nop

    :goto_31
    aget-byte v0, v0, v1

    goto/32 :goto_39

    nop

    :goto_32
    iget-object v2, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mPdu:[B

    goto/32 :goto_41

    nop

    :goto_33
    aget-byte v2, v2, v3

    goto/32 :goto_3e

    nop

    :goto_34
    mul-int/lit8 v14, v2, 0x3c

    goto/32 :goto_2a

    nop

    :goto_35
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mPdu:[B

    goto/32 :goto_11

    nop

    :goto_36
    invoke-static {v2}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmBcdByteToInt(B)I

    move-result v8

    goto/32 :goto_22

    nop

    :goto_37
    return-wide v6

    :catch_0
    move-exception v3

    goto/32 :goto_38

    nop

    :goto_38
    const-string v4, "SmsMessage"

    goto/32 :goto_4a

    nop

    :goto_39
    invoke-static {v0}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmBcdByteToInt(B)I

    move-result v0

    goto/32 :goto_1c

    nop

    :goto_3a
    iget v3, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_1d

    nop

    :goto_3b
    iput v2, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_31

    nop

    :goto_3c
    iput v4, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_33

    nop

    :goto_3d
    move v13, v3

    goto/32 :goto_26

    nop

    :goto_3e
    invoke-static {v2}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmBcdByteToInt(B)I

    move-result v10

    goto/32 :goto_32

    nop

    :goto_3f
    int-to-long v6, v14

    goto/32 :goto_24

    nop

    :goto_40
    iput v4, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_20

    nop

    :goto_41
    iget v3, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_d

    nop

    :goto_42
    const-wide/16 v3, 0x0

    goto/32 :goto_3

    nop

    :goto_43
    add-int/lit16 v2, v0, 0x7d0

    :goto_44
    goto/32 :goto_1f

    nop

    :goto_45
    move v5, v9

    goto/32 :goto_18

    nop

    :goto_46
    add-int/lit16 v2, v0, 0x76c

    goto/32 :goto_8

    nop

    :goto_47
    invoke-static {v2}, Lcom/android/internal/telephony/uicc/IccUtils;->gsmBcdByteToInt(B)I

    move-result v11

    goto/32 :goto_10

    nop

    :goto_48
    aget-byte v2, v2, v3

    goto/32 :goto_0

    nop

    :goto_49
    move v7, v11

    :try_start_0
    invoke-static/range {v2 .. v7}, Ljava/time/LocalDateTime;->of(IIIIII)Ljava/time/LocalDateTime;

    move-result-object v3

    sget-object v4, Ljava/time/ZoneOffset;->UTC:Ljava/time/ZoneOffset;

    invoke-virtual {v3, v4}, Ljava/time/LocalDateTime;->toEpochSecond(Ljava/time/ZoneOffset;)J

    move-result-wide v4
    :try_end_0
    .catch Ljava/time/DateTimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_3f

    nop

    :goto_4a
    const-string v5, "Invalid timestamp"

    goto/32 :goto_27

    nop

    :goto_4b
    if-ge v0, v2, :cond_1

    goto/32 :goto_9

    :cond_1
    goto/32 :goto_46

    nop
.end method

.method getUserData()[B
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mUserData:[B

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method getUserDataGSM7Bit(III)Ljava/lang/String;
    .locals 6

    goto/32 :goto_5

    nop

    :goto_0
    mul-int/lit8 v2, p1, 0x7

    goto/32 :goto_4

    nop

    :goto_1
    move v5, p3

    goto/32 :goto_a

    nop

    :goto_2
    return-object v0

    :goto_3
    move v4, p2

    goto/32 :goto_1

    nop

    :goto_4
    div-int/lit8 v2, v2, 0x8

    goto/32 :goto_b

    nop

    :goto_5
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mPdu:[B

    goto/32 :goto_7

    nop

    :goto_6
    iget v3, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mUserDataSeptetPadding:I

    goto/32 :goto_9

    nop

    :goto_7
    iget v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_6

    nop

    :goto_8
    iget v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_0

    nop

    :goto_9
    move v2, p1

    goto/32 :goto_3

    nop

    :goto_a
    invoke-static/range {v0 .. v5}, Lcom/android/internal/telephony/GsmAlphabet;->gsm7BitPackedToString([BIIIII)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_b
    add-int/2addr v1, v2

    goto/32 :goto_c

    nop

    :goto_c
    iput v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_2

    nop
.end method

.method getUserDataGSM8bit(I)Ljava/lang/String;
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mPdu:[B

    goto/32 :goto_5

    nop

    :goto_1
    invoke-static {v0, v1, p1}, Lcom/android/internal/telephony/GsmAlphabet;->gsm8BitUnpackedToString([BII)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_2
    add-int/2addr v1, p1

    goto/32 :goto_3

    nop

    :goto_3
    iput v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_4

    nop

    :goto_4
    return-object v0

    :goto_5
    iget v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_1

    nop

    :goto_6
    iget v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_2

    nop
.end method

.method getUserDataHeader()Lcom/android/internal/telephony/SmsHeader;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mUserDataHeader:Lcom/android/internal/telephony/SmsHeader;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method getUserDataKSC5601(I)Ljava/lang/String;
    .locals 4

    :try_start_0
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mPdu:[B

    iget v2, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    const-string v3, "KSC5601"

    invoke-direct {v0, v1, v2, p1, v3}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_8

    nop

    :goto_0
    return-object v0

    :goto_1
    invoke-static {v2, v3, v0}, Lcom/android/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/32 :goto_9

    nop

    :goto_2
    const-string v2, "SmsMessage"

    goto/32 :goto_5

    nop

    :goto_3
    iget v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_7

    nop

    :goto_4
    iput v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_0

    nop

    :goto_5
    const-string v3, "implausible UnsupportedEncodingException"

    goto/32 :goto_1

    nop

    :goto_6
    const-string v1, ""

    goto/32 :goto_2

    nop

    :goto_7
    add-int/2addr v1, p1

    goto/32 :goto_4

    nop

    :goto_8
    goto :goto_a

    :catch_0
    move-exception v0

    goto/32 :goto_6

    nop

    :goto_9
    move-object v0, v1

    :goto_a
    goto/32 :goto_3

    nop
.end method

.method getUserDataUCS2(I)Ljava/lang/String;
    .locals 4

    :try_start_0
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mPdu:[B

    iget v2, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    const-string v3, "utf-16"

    invoke-direct {v0, v1, v2, p1, v3}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_7

    nop

    :goto_0
    const-string v2, "SmsMessage"

    goto/32 :goto_3

    nop

    :goto_1
    iget v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_8

    nop

    :goto_2
    return-object v0

    :goto_3
    const-string v3, "implausible UnsupportedEncodingException"

    goto/32 :goto_4

    nop

    :goto_4
    invoke-static {v2, v3, v0}, Lcom/android/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/32 :goto_9

    nop

    :goto_5
    iput v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_2

    nop

    :goto_6
    const-string v1, ""

    goto/32 :goto_0

    nop

    :goto_7
    goto :goto_a

    :catch_0
    move-exception v0

    goto/32 :goto_6

    nop

    :goto_8
    add-int/2addr v1, p1

    goto/32 :goto_5

    nop

    :goto_9
    move-object v0, v1

    :goto_a
    goto/32 :goto_1

    nop
.end method

.method moreDataPresent()Z
    .locals 2

    goto/32 :goto_9

    nop

    :goto_0
    goto :goto_7

    :goto_1
    goto/32 :goto_6

    nop

    :goto_2
    iget v1, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mCur:I

    goto/32 :goto_5

    nop

    :goto_3
    return v0

    :goto_4
    array-length v0, v0

    goto/32 :goto_2

    nop

    :goto_5
    if-gt v0, v1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_8

    nop

    :goto_6
    const/4 v0, 0x0

    :goto_7
    goto/32 :goto_3

    nop

    :goto_8
    const/4 v0, 0x1

    goto/32 :goto_0

    nop

    :goto_9
    iget-object v0, p0, Lcom/android/internal/telephony/gsm/SmsMessage$PduParser;->mPdu:[B

    goto/32 :goto_4

    nop
.end method
