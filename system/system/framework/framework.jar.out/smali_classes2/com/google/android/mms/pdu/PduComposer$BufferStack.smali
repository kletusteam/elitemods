.class Lcom/google/android/mms/pdu/PduComposer$BufferStack;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/mms/pdu/PduComposer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BufferStack"
.end annotation


# instance fields
.field private stack:Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;

.field stackSize:I

.field final synthetic this$0:Lcom/google/android/mms/pdu/PduComposer;

.field private toCopy:Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;


# direct methods
.method private constructor <init>(Lcom/google/android/mms/pdu/PduComposer;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->this$0:Lcom/google/android/mms/pdu/PduComposer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->stack:Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;

    iput-object p1, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->toCopy:Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;

    const/4 p1, 0x0

    iput p1, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->stackSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/mms/pdu/PduComposer;Lcom/google/android/mms/pdu/PduComposer$BufferStack-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/mms/pdu/PduComposer$BufferStack;-><init>(Lcom/google/android/mms/pdu/PduComposer;)V

    return-void
.end method


# virtual methods
.method copy()V
    .locals 4

    goto/32 :goto_8

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_6

    nop

    :goto_1
    iget-object v1, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->toCopy:Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;

    goto/32 :goto_a

    nop

    :goto_2
    iget v2, v2, Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;->currentPosition:I

    goto/32 :goto_4

    nop

    :goto_3
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    goto/32 :goto_5

    nop

    :goto_4
    const/4 v3, 0x0

    goto/32 :goto_7

    nop

    :goto_5
    iget-object v2, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->toCopy:Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;

    goto/32 :goto_2

    nop

    :goto_6
    iput-object v0, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->toCopy:Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;

    goto/32 :goto_9

    nop

    :goto_7
    invoke-virtual {v0, v1, v3, v2}, Lcom/google/android/mms/pdu/PduComposer;->arraycopy([BII)V

    goto/32 :goto_0

    nop

    :goto_8
    iget-object v0, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->this$0:Lcom/google/android/mms/pdu/PduComposer;

    goto/32 :goto_1

    nop

    :goto_9
    return-void

    :goto_a
    iget-object v1, v1, Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;->currentMessage:Ljava/io/ByteArrayOutputStream;

    goto/32 :goto_3

    nop
.end method

.method mark()Lcom/google/android/mms/pdu/PduComposer$PositionMarker;
    .locals 3

    goto/32 :goto_2

    nop

    :goto_0
    iget-object v1, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->this$0:Lcom/google/android/mms/pdu/PduComposer;

    goto/32 :goto_5

    nop

    :goto_1
    iget v1, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->stackSize:I

    goto/32 :goto_7

    nop

    :goto_2
    new-instance v0, Lcom/google/android/mms/pdu/PduComposer$PositionMarker;

    goto/32 :goto_0

    nop

    :goto_3
    invoke-direct {v0, v1, v2}, Lcom/google/android/mms/pdu/PduComposer$PositionMarker;-><init>(Lcom/google/android/mms/pdu/PduComposer;Lcom/google/android/mms/pdu/PduComposer$PositionMarker-IA;)V

    goto/32 :goto_8

    nop

    :goto_4
    invoke-static {v0, v1}, Lcom/google/android/mms/pdu/PduComposer$PositionMarker;->-$$Nest$fputc_pos(Lcom/google/android/mms/pdu/PduComposer$PositionMarker;I)V

    goto/32 :goto_1

    nop

    :goto_5
    const/4 v2, 0x0

    goto/32 :goto_3

    nop

    :goto_6
    return-object v0

    :goto_7
    invoke-static {v0, v1}, Lcom/google/android/mms/pdu/PduComposer$PositionMarker;->-$$Nest$fputcurrentStackSize(Lcom/google/android/mms/pdu/PduComposer$PositionMarker;I)V

    goto/32 :goto_6

    nop

    :goto_8
    iget-object v1, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->this$0:Lcom/google/android/mms/pdu/PduComposer;

    goto/32 :goto_9

    nop

    :goto_9
    iget v1, v1, Lcom/google/android/mms/pdu/PduComposer;->mPosition:I

    goto/32 :goto_4

    nop
.end method

.method newbuf()V
    .locals 3

    goto/32 :goto_f

    nop

    :goto_0
    iput v2, v1, Lcom/google/android/mms/pdu/PduComposer;->mPosition:I

    goto/32 :goto_7

    nop

    :goto_1
    iput-object v0, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->stack:Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;

    goto/32 :goto_6

    nop

    :goto_2
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    goto/32 :goto_13

    nop

    :goto_3
    if-eqz v0, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_12

    nop

    :goto_4
    const/4 v2, 0x0

    goto/32 :goto_0

    nop

    :goto_5
    iget-object v1, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->this$0:Lcom/google/android/mms/pdu/PduComposer;

    goto/32 :goto_4

    nop

    :goto_6
    iget v1, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->stackSize:I

    goto/32 :goto_a

    nop

    :goto_7
    return-void

    :goto_8
    goto/32 :goto_1a

    nop

    :goto_9
    iget-object v1, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->stack:Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;

    goto/32 :goto_b

    nop

    :goto_a
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_1c

    nop

    :goto_b
    iput-object v1, v0, Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;->next:Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;

    goto/32 :goto_1

    nop

    :goto_c
    const/4 v1, 0x0

    goto/32 :goto_d

    nop

    :goto_d
    invoke-direct {v0, v1}, Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;-><init>(Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode-IA;)V

    goto/32 :goto_11

    nop

    :goto_e
    iput-object v2, v1, Lcom/google/android/mms/pdu/PduComposer;->mMessage:Ljava/io/ByteArrayOutputStream;

    goto/32 :goto_5

    nop

    :goto_f
    iget-object v0, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->toCopy:Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;

    goto/32 :goto_3

    nop

    :goto_10
    throw v0

    :goto_11
    iget-object v1, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->this$0:Lcom/google/android/mms/pdu/PduComposer;

    goto/32 :goto_1b

    nop

    :goto_12
    new-instance v0, Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;

    goto/32 :goto_c

    nop

    :goto_13
    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    goto/32 :goto_e

    nop

    :goto_14
    const-string v1, "BUG: Invalid newbuf() before copy()"

    goto/32 :goto_19

    nop

    :goto_15
    iput-object v1, v0, Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;->currentMessage:Ljava/io/ByteArrayOutputStream;

    goto/32 :goto_18

    nop

    :goto_16
    iput v1, v0, Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;->currentPosition:I

    goto/32 :goto_9

    nop

    :goto_17
    iget-object v1, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->this$0:Lcom/google/android/mms/pdu/PduComposer;

    goto/32 :goto_2

    nop

    :goto_18
    iget-object v1, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->this$0:Lcom/google/android/mms/pdu/PduComposer;

    goto/32 :goto_1d

    nop

    :goto_19
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_10

    nop

    :goto_1a
    new-instance v0, Ljava/lang/RuntimeException;

    goto/32 :goto_14

    nop

    :goto_1b
    iget-object v1, v1, Lcom/google/android/mms/pdu/PduComposer;->mMessage:Ljava/io/ByteArrayOutputStream;

    goto/32 :goto_15

    nop

    :goto_1c
    iput v1, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->stackSize:I

    goto/32 :goto_17

    nop

    :goto_1d
    iget v1, v1, Lcom/google/android/mms/pdu/PduComposer;->mPosition:I

    goto/32 :goto_16

    nop
.end method

.method pop()V
    .locals 4

    goto/32 :goto_c

    nop

    :goto_0
    iget v1, v1, Lcom/google/android/mms/pdu/PduComposer;->mPosition:I

    goto/32 :goto_1

    nop

    :goto_1
    iget-object v2, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->this$0:Lcom/google/android/mms/pdu/PduComposer;

    goto/32 :goto_15

    nop

    :goto_2
    iget-object v2, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->toCopy:Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;

    goto/32 :goto_5

    nop

    :goto_3
    iget v3, v3, Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;->currentPosition:I

    goto/32 :goto_17

    nop

    :goto_4
    iget-object v3, v3, Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;->currentMessage:Ljava/io/ByteArrayOutputStream;

    goto/32 :goto_f

    nop

    :goto_5
    iput-object v0, v2, Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;->currentMessage:Ljava/io/ByteArrayOutputStream;

    goto/32 :goto_14

    nop

    :goto_6
    iput-object v2, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->stack:Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;

    goto/32 :goto_16

    nop

    :goto_7
    iget-object v0, v0, Lcom/google/android/mms/pdu/PduComposer;->mMessage:Ljava/io/ByteArrayOutputStream;

    goto/32 :goto_12

    nop

    :goto_8
    iput v1, v2, Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;->currentPosition:I

    goto/32 :goto_11

    nop

    :goto_9
    iget-object v3, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->stack:Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;

    goto/32 :goto_3

    nop

    :goto_a
    add-int/lit8 v2, v2, -0x1

    goto/32 :goto_10

    nop

    :goto_b
    iget-object v2, v2, Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;->next:Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;

    goto/32 :goto_6

    nop

    :goto_c
    iget-object v0, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->this$0:Lcom/google/android/mms/pdu/PduComposer;

    goto/32 :goto_7

    nop

    :goto_d
    iput-object v2, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->toCopy:Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;

    goto/32 :goto_b

    nop

    :goto_e
    iget-object v2, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->this$0:Lcom/google/android/mms/pdu/PduComposer;

    goto/32 :goto_9

    nop

    :goto_f
    iput-object v3, v2, Lcom/google/android/mms/pdu/PduComposer;->mMessage:Ljava/io/ByteArrayOutputStream;

    goto/32 :goto_e

    nop

    :goto_10
    iput v2, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->stackSize:I

    goto/32 :goto_2

    nop

    :goto_11
    return-void

    :goto_12
    iget-object v1, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->this$0:Lcom/google/android/mms/pdu/PduComposer;

    goto/32 :goto_0

    nop

    :goto_13
    iget-object v2, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->stack:Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;

    goto/32 :goto_d

    nop

    :goto_14
    iget-object v2, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->toCopy:Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;

    goto/32 :goto_8

    nop

    :goto_15
    iget-object v3, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->stack:Lcom/google/android/mms/pdu/PduComposer$LengthRecordNode;

    goto/32 :goto_4

    nop

    :goto_16
    iget v2, p0, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->stackSize:I

    goto/32 :goto_a

    nop

    :goto_17
    iput v3, v2, Lcom/google/android/mms/pdu/PduComposer;->mPosition:I

    goto/32 :goto_13

    nop
.end method
