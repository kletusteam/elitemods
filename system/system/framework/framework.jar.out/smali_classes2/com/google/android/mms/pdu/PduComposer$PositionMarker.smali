.class Lcom/google/android/mms/pdu/PduComposer$PositionMarker;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/mms/pdu/PduComposer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PositionMarker"
.end annotation


# instance fields
.field private c_pos:I

.field private currentStackSize:I

.field final synthetic this$0:Lcom/google/android/mms/pdu/PduComposer;


# direct methods
.method static bridge synthetic -$$Nest$fputc_pos(Lcom/google/android/mms/pdu/PduComposer$PositionMarker;I)V
    .locals 0

    iput p1, p0, Lcom/google/android/mms/pdu/PduComposer$PositionMarker;->c_pos:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputcurrentStackSize(Lcom/google/android/mms/pdu/PduComposer$PositionMarker;I)V
    .locals 0

    iput p1, p0, Lcom/google/android/mms/pdu/PduComposer$PositionMarker;->currentStackSize:I

    return-void
.end method

.method private constructor <init>(Lcom/google/android/mms/pdu/PduComposer;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/mms/pdu/PduComposer$PositionMarker;->this$0:Lcom/google/android/mms/pdu/PduComposer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/mms/pdu/PduComposer;Lcom/google/android/mms/pdu/PduComposer$PositionMarker-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/mms/pdu/PduComposer$PositionMarker;-><init>(Lcom/google/android/mms/pdu/PduComposer;)V

    return-void
.end method


# virtual methods
.method getLength()I
    .locals 2

    goto/32 :goto_b

    nop

    :goto_0
    return v0

    :goto_1
    goto/32 :goto_9

    nop

    :goto_2
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_3

    nop

    :goto_3
    throw v0

    :goto_4
    iget-object v1, p0, Lcom/google/android/mms/pdu/PduComposer$PositionMarker;->this$0:Lcom/google/android/mms/pdu/PduComposer;

    goto/32 :goto_a

    nop

    :goto_5
    iget-object v0, p0, Lcom/google/android/mms/pdu/PduComposer$PositionMarker;->this$0:Lcom/google/android/mms/pdu/PduComposer;

    goto/32 :goto_c

    nop

    :goto_6
    sub-int/2addr v0, v1

    goto/32 :goto_0

    nop

    :goto_7
    iget v1, p0, Lcom/google/android/mms/pdu/PduComposer$PositionMarker;->c_pos:I

    goto/32 :goto_6

    nop

    :goto_8
    if-eq v0, v1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_5

    nop

    :goto_9
    new-instance v0, Ljava/lang/RuntimeException;

    goto/32 :goto_d

    nop

    :goto_a
    invoke-static {v1}, Lcom/google/android/mms/pdu/PduComposer;->-$$Nest$fgetmStack(Lcom/google/android/mms/pdu/PduComposer;)Lcom/google/android/mms/pdu/PduComposer$BufferStack;

    move-result-object v1

    goto/32 :goto_e

    nop

    :goto_b
    iget v0, p0, Lcom/google/android/mms/pdu/PduComposer$PositionMarker;->currentStackSize:I

    goto/32 :goto_4

    nop

    :goto_c
    iget v0, v0, Lcom/google/android/mms/pdu/PduComposer;->mPosition:I

    goto/32 :goto_7

    nop

    :goto_d
    const-string v1, "BUG: Invalid call to getLength()"

    goto/32 :goto_2

    nop

    :goto_e
    iget v1, v1, Lcom/google/android/mms/pdu/PduComposer$BufferStack;->stackSize:I

    goto/32 :goto_8

    nop
.end method
