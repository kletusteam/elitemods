.class public interface abstract Lcom/xiaomi/mirror/MiuiMirrorStub;
.super Ljava/lang/Object;


# static fields
.field public static final TAG:Ljava/lang/String; = "MiuiMirrorStub"


# direct methods
.method public static getInstance()Lcom/xiaomi/mirror/MiuiMirrorStub;
    .locals 1

    const-class v0, Lcom/xiaomi/mirror/MiuiMirrorStub;

    invoke-static {v0}, Lcom/miui/base/MiuiStubUtil;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/mirror/MiuiMirrorStub;

    return-object v0
.end method


# virtual methods
.method public cancelDragAndDrop(Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public cleanDragToken(Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public delegateMirrorDrag(Ljava/lang/Object;Landroid/view/ViewRootImpl;IILandroid/view/View$DragShadowBuilder;Landroid/graphics/Point;Landroid/content/ClipData;Ljava/lang/Object;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dispatchPointerEvent(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isModelSupport()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public notifyInputTypeAndPos(III)V
    .locals 0

    return-void
.end method

.method public notifyMoveTaskToBack(Landroid/os/IBinder;Z)V
    .locals 0

    return-void
.end method

.method public performMirrorMenuQuery(Landroid/view/View;Landroid/view/MotionEvent;ZZI)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public reportDragResult(ILandroid/view/IWindow;Z)V
    .locals 0

    return-void
.end method

.method public tryToTriggerSend(Landroid/content/Context;Landroid/view/KeyEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public updateShadow(Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;)V
    .locals 0

    return-void
.end method
