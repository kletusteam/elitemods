.class Landroid/media/Cea708CaptionTrack;
.super Landroid/media/SubtitleTrack;


# instance fields
.field private final mCCParser:Landroid/media/Cea708CCParser;

.field private final mRenderingWidget:Landroid/media/Cea708CCWidget;


# direct methods
.method constructor <init>(Landroid/media/Cea708CCWidget;Landroid/media/MediaFormat;)V
    .locals 1

    invoke-direct {p0, p2}, Landroid/media/SubtitleTrack;-><init>(Landroid/media/MediaFormat;)V

    iput-object p1, p0, Landroid/media/Cea708CaptionTrack;->mRenderingWidget:Landroid/media/Cea708CCWidget;

    new-instance v0, Landroid/media/Cea708CCParser;

    invoke-direct {v0, p1}, Landroid/media/Cea708CCParser;-><init>(Landroid/media/Cea708CCParser$DisplayListener;)V

    iput-object v0, p0, Landroid/media/Cea708CaptionTrack;->mCCParser:Landroid/media/Cea708CCParser;

    return-void
.end method


# virtual methods
.method public getRenderingWidget()Landroid/media/SubtitleTrack$RenderingWidget;
    .locals 1

    iget-object v0, p0, Landroid/media/Cea708CaptionTrack;->mRenderingWidget:Landroid/media/Cea708CCWidget;

    return-object v0
.end method

.method public onData([BZJ)V
    .locals 1

    iget-object v0, p0, Landroid/media/Cea708CaptionTrack;->mCCParser:Landroid/media/Cea708CCParser;

    invoke-virtual {v0, p1}, Landroid/media/Cea708CCParser;->parse([B)V

    return-void
.end method

.method public updateView(Ljava/util/Vector;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector<",
            "Landroid/media/SubtitleTrack$Cue;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
