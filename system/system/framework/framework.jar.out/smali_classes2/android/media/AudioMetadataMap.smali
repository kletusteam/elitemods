.class public interface abstract Landroid/media/AudioMetadataMap;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/media/AudioMetadataReadMap;


# virtual methods
.method public abstract remove(Landroid/media/AudioMetadata$Key;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/media/AudioMetadata$Key<",
            "TT;>;)TT;"
        }
    .end annotation
.end method

.method public abstract set(Landroid/media/AudioMetadata$Key;Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/media/AudioMetadata$Key<",
            "TT;>;TT;)TT;"
        }
    .end annotation
.end method
