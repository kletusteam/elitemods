.class public interface abstract Landroid/media/tv/tuner/filter/SharedFilterCallback;
.super Ljava/lang/Object;


# annotations
.annotation runtime Landroid/annotation/SystemApi;
.end annotation


# virtual methods
.method public abstract onFilterEvent(Landroid/media/tv/tuner/filter/SharedFilter;[Landroid/media/tv/tuner/filter/FilterEvent;)V
.end method

.method public abstract onFilterStatusChanged(Landroid/media/tv/tuner/filter/SharedFilter;I)V
.end method
