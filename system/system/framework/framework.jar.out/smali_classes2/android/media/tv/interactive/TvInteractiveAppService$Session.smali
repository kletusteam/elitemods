.class public abstract Landroid/media/tv/interactive/TvInteractiveAppService$Session;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/KeyEvent$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/tv/interactive/TvInteractiveAppService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Session"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDispatcherState:Landroid/view/KeyEvent$DispatcherState;

.field final mHandler:Landroid/os/Handler;

.field private final mLock:Ljava/lang/Object;

.field private mMediaFrame:Landroid/graphics/Rect;

.field private mMediaView:Landroid/view/View;

.field private mMediaViewCleanUpTask:Landroid/media/tv/interactive/TvInteractiveAppService$MediaViewCleanUpTask;

.field private mMediaViewContainer:Landroid/widget/FrameLayout;

.field private mMediaViewEnabled:Z

.field private final mPendingActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private mSessionCallback:Landroid/media/tv/interactive/ITvInteractiveAppSessionCallback;

.field private mSurface:Landroid/view/Surface;

.field private final mWindowManager:Landroid/view/WindowManager;

.field private mWindowParams:Landroid/view/WindowManager$LayoutParams;

.field private mWindowToken:Landroid/os/IBinder;


# direct methods
.method static bridge synthetic -$$Nest$fgetmMediaFrame(Landroid/media/tv/interactive/TvInteractiveAppService$Session;)Landroid/graphics/Rect;
    .locals 0

    iget-object p0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mMediaFrame:Landroid/graphics/Rect;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMediaViewEnabled(Landroid/media/tv/interactive/TvInteractiveAppService$Session;)Z
    .locals 0

    iget-boolean p0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mMediaViewEnabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSessionCallback(Landroid/media/tv/interactive/TvInteractiveAppService$Session;)Landroid/media/tv/interactive/ITvInteractiveAppSessionCallback;
    .locals 0

    iget-object p0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mSessionCallback:Landroid/media/tv/interactive/ITvInteractiveAppSessionCallback;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmWindowToken(Landroid/media/tv/interactive/TvInteractiveAppService$Session;)Landroid/os/IBinder;
    .locals 0

    iget-object p0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mWindowToken:Landroid/os/IBinder;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmMediaViewEnabled(Landroid/media/tv/interactive/TvInteractiveAppService$Session;Z)V
    .locals 0

    iput-boolean p1, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mMediaViewEnabled:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$minitialize(Landroid/media/tv/interactive/TvInteractiveAppService$Session;Landroid/media/tv/interactive/ITvInteractiveAppSessionCallback;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->initialize(Landroid/media/tv/interactive/ITvInteractiveAppSessionCallback;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/view/KeyEvent$DispatcherState;

    invoke-direct {v0}, Landroid/view/KeyEvent$DispatcherState;-><init>()V

    iput-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mDispatcherState:Landroid/view/KeyEvent$DispatcherState;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mLock:Ljava/lang/Object;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mPendingActions:Ljava/util/List;

    iput-object p1, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mContext:Landroid/content/Context;

    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mWindowManager:Landroid/view/WindowManager;

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V
    .locals 2

    iget-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mSessionCallback:Landroid/media/tv/interactive/ITvInteractiveAppSessionCallback;

    if-nez v1, :cond_0

    iget-object v1, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mPendingActions:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v1, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->isCurrentThread()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private initialize(Landroid/media/tv/interactive/ITvInteractiveAppSessionCallback;)V
    .locals 3

    iget-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iput-object p1, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mSessionCallback:Landroid/media/tv/interactive/ITvInteractiveAppSessionCallback;

    iget-object v1, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mPendingActions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mPendingActions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method createBiInteractiveApp(Landroid/net/Uri;Landroid/os/Bundle;)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0, p1, p2}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onCreateBiInteractiveAppRequest(Landroid/net/Uri;Landroid/os/Bundle;)V

    goto/32 :goto_0

    nop
.end method

.method createMediaView(Landroid/os/IBinder;Landroid/graphics/Rect;)V
    .locals 11

    goto/32 :goto_20

    nop

    :goto_0
    sub-int v4, v2, v4

    goto/32 :goto_e

    nop

    :goto_1
    or-int/lit8 v2, v2, 0x40

    goto/32 :goto_9

    nop

    :goto_2
    iget v2, p2, Landroid/graphics/Rect;->top:I

    goto/32 :goto_18

    nop

    :goto_3
    return-void

    :goto_4
    goto/32 :goto_26

    nop

    :goto_5
    move-object v2, v10

    goto/32 :goto_22

    nop

    :goto_6
    if-eqz v0, :cond_0

    goto/32 :goto_38

    :cond_0
    goto/32 :goto_37

    nop

    :goto_7
    if-eqz v0, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_3

    nop

    :goto_8
    const/4 v0, 0x0

    goto/32 :goto_32

    nop

    :goto_9
    iput v2, v10, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    goto/32 :goto_1b

    nop

    :goto_a
    iget-object v1, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mMediaView:Landroid/view/View;

    goto/32 :goto_24

    nop

    :goto_b
    iget-object v1, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mContext:Landroid/content/Context;

    goto/32 :goto_2b

    nop

    :goto_c
    iget v1, p2, Landroid/graphics/Rect;->left:I

    goto/32 :goto_46

    nop

    :goto_d
    invoke-direct/range {v2 .. v9}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIIIII)V

    goto/32 :goto_1e

    nop

    :goto_e
    iget v5, p2, Landroid/graphics/Rect;->left:I

    goto/32 :goto_1c

    nop

    :goto_f
    iget-boolean v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mMediaViewEnabled:Z

    goto/32 :goto_7

    nop

    :goto_10
    if-nez v2, :cond_2

    goto/32 :goto_3d

    :cond_2
    goto/32 :goto_14

    nop

    :goto_11
    iget v4, p2, Landroid/graphics/Rect;->top:I

    goto/32 :goto_0

    nop

    :goto_12
    iget v2, v10, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    goto/32 :goto_1

    nop

    :goto_13
    const v3, 0x800033

    goto/32 :goto_35

    nop

    :goto_14
    const/high16 v2, 0x1000000

    goto/32 :goto_3c

    nop

    :goto_15
    sub-int v3, v2, v3

    goto/32 :goto_34

    nop

    :goto_16
    const/4 v1, 0x1

    goto/32 :goto_2e

    nop

    :goto_17
    iget-object v2, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    goto/32 :goto_25

    nop

    :goto_18
    sub-int/2addr v1, v2

    goto/32 :goto_19

    nop

    :goto_19
    invoke-virtual {p0, v0, v1}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onMediaViewSizeChanged(II)V

    goto/32 :goto_f

    nop

    :goto_1a
    iget v1, p2, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_2

    nop

    :goto_1b
    iget-object v2, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    goto/32 :goto_13

    nop

    :goto_1c
    iget v6, p2, Landroid/graphics/Rect;->top:I

    goto/32 :goto_2c

    nop

    :goto_1d
    iget v0, p2, Landroid/graphics/Rect;->right:I

    goto/32 :goto_c

    nop

    :goto_1e
    iput-object v10, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    goto/32 :goto_12

    nop

    :goto_1f
    iput-object p2, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mMediaFrame:Landroid/graphics/Rect;

    goto/32 :goto_1d

    nop

    :goto_20
    iget-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mMediaViewContainer:Landroid/widget/FrameLayout;

    goto/32 :goto_43

    nop

    :goto_21
    iget-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mMediaViewCleanUpTask:Landroid/media/tv/interactive/TvInteractiveAppService$MediaViewCleanUpTask;

    goto/32 :goto_28

    nop

    :goto_22
    move v7, v0

    goto/32 :goto_23

    nop

    :goto_23
    move v8, v1

    goto/32 :goto_d

    nop

    :goto_24
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto/32 :goto_39

    nop

    :goto_25
    iput-object p1, v2, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    goto/32 :goto_3e

    nop

    :goto_26
    invoke-virtual {p0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onCreateMediaView()Landroid/view/View;

    move-result-object v0

    goto/32 :goto_40

    nop

    :goto_27
    new-instance v10, Landroid/view/WindowManager$LayoutParams;

    goto/32 :goto_42

    nop

    :goto_28
    if-nez v0, :cond_3

    goto/32 :goto_30

    :cond_3
    goto/32 :goto_16

    nop

    :goto_29
    iget-object v4, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    goto/32 :goto_36

    nop

    :goto_2a
    iget v3, p2, Landroid/graphics/Rect;->left:I

    goto/32 :goto_15

    nop

    :goto_2b
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_3b

    nop

    :goto_2c
    const/4 v9, -0x2

    goto/32 :goto_5

    nop

    :goto_2d
    const/4 v0, 0x0

    goto/32 :goto_2f

    nop

    :goto_2e
    invoke-virtual {v0, v1}, Landroid/media/tv/interactive/TvInteractiveAppService$MediaViewCleanUpTask;->cancel(Z)Z

    goto/32 :goto_2d

    nop

    :goto_2f
    iput-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mMediaViewCleanUpTask:Landroid/media/tv/interactive/TvInteractiveAppService$MediaViewCleanUpTask;

    :goto_30
    goto/32 :goto_31

    nop

    :goto_31
    new-instance v0, Landroid/widget/FrameLayout;

    goto/32 :goto_b

    nop

    :goto_32
    invoke-virtual {p0, v0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->removeMediaView(Z)V

    :goto_33
    goto/32 :goto_3f

    nop

    :goto_34
    iget v2, p2, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_11

    nop

    :goto_35
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    goto/32 :goto_17

    nop

    :goto_36
    invoke-interface {v2, v3, v4}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/32 :goto_45

    nop

    :goto_37
    return-void

    :goto_38
    goto/32 :goto_21

    nop

    :goto_39
    const/16 v0, 0x3e9

    goto/32 :goto_47

    nop

    :goto_3a
    iget-object v3, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mMediaViewContainer:Landroid/widget/FrameLayout;

    goto/32 :goto_29

    nop

    :goto_3b
    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    goto/32 :goto_41

    nop

    :goto_3c
    or-int/2addr v1, v2

    :goto_3d
    goto/32 :goto_27

    nop

    :goto_3e
    iget-object v2, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mWindowManager:Landroid/view/WindowManager;

    goto/32 :goto_3a

    nop

    :goto_3f
    iput-object p1, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mWindowToken:Landroid/os/IBinder;

    goto/32 :goto_1f

    nop

    :goto_40
    iput-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mMediaView:Landroid/view/View;

    goto/32 :goto_6

    nop

    :goto_41
    iput-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mMediaViewContainer:Landroid/widget/FrameLayout;

    goto/32 :goto_a

    nop

    :goto_42
    iget v2, p2, Landroid/graphics/Rect;->right:I

    goto/32 :goto_2a

    nop

    :goto_43
    if-nez v0, :cond_4

    goto/32 :goto_33

    :cond_4
    goto/32 :goto_8

    nop

    :goto_44
    invoke-static {}, Landroid/app/ActivityManager;->isHighEndGfx()Z

    move-result v2

    goto/32 :goto_10

    nop

    :goto_45
    return-void

    :goto_46
    sub-int/2addr v0, v1

    goto/32 :goto_1a

    nop

    :goto_47
    const/16 v1, 0x218

    goto/32 :goto_44

    nop
.end method

.method destroyBiInteractiveApp(Ljava/lang/String;)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0, p1}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onDestroyBiInteractiveAppRequest(Ljava/lang/String;)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method dispatchInputEvent(Landroid/view/InputEvent;Landroid/view/InputEventReceiver;)I
    .locals 4

    goto/32 :goto_2

    nop

    :goto_0
    return v1

    :goto_1
    goto/32 :goto_3

    nop

    :goto_2
    instance-of v0, p1, Landroid/view/KeyEvent;

    goto/32 :goto_1c

    nop

    :goto_3
    and-int/lit8 v3, v2, 0x4

    goto/32 :goto_b

    nop

    :goto_4
    move-object v0, p1

    goto/32 :goto_a

    nop

    :goto_5
    goto :goto_13

    :goto_6
    goto/32 :goto_18

    nop

    :goto_7
    if-nez v3, :cond_0

    goto/32 :goto_14

    :cond_0
    goto/32 :goto_0

    nop

    :goto_8
    return v1

    :goto_9
    goto/32 :goto_16

    nop

    :goto_a
    check-cast v0, Landroid/view/MotionEvent;

    goto/32 :goto_15

    nop

    :goto_b
    if-nez v3, :cond_1

    goto/32 :goto_9

    :cond_1
    goto/32 :goto_23

    nop

    :goto_c
    if-nez v3, :cond_2

    goto/32 :goto_1

    :cond_2
    goto/32 :goto_22

    nop

    :goto_d
    if-nez v0, :cond_3

    goto/32 :goto_13

    :cond_3
    goto/32 :goto_4

    nop

    :goto_e
    if-nez v3, :cond_4

    goto/32 :goto_14

    :cond_4
    goto/32 :goto_8

    nop

    :goto_f
    return v1

    :goto_10
    goto/32 :goto_5

    nop

    :goto_11
    invoke-virtual {v0, p0, v2, p0}, Landroid/view/KeyEvent;->dispatch(Landroid/view/KeyEvent$Callback;Landroid/view/KeyEvent$DispatcherState;Ljava/lang/Object;)Z

    move-result v2

    goto/32 :goto_1d

    nop

    :goto_12
    return v1

    :goto_13
    nop

    :goto_14
    goto/32 :goto_1f

    nop

    :goto_15
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getSource()I

    move-result v2

    goto/32 :goto_1a

    nop

    :goto_16
    invoke-virtual {p0, v0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    goto/32 :goto_1b

    nop

    :goto_17
    if-nez v0, :cond_5

    goto/32 :goto_6

    :cond_5
    goto/32 :goto_21

    nop

    :goto_18
    instance-of v0, p1, Landroid/view/MotionEvent;

    goto/32 :goto_d

    nop

    :goto_19
    iget-object v2, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mDispatcherState:Landroid/view/KeyEvent$DispatcherState;

    goto/32 :goto_11

    nop

    :goto_1a
    invoke-virtual {v0}, Landroid/view/MotionEvent;->isTouchEvent()Z

    move-result v3

    goto/32 :goto_c

    nop

    :goto_1b
    if-nez v3, :cond_6

    goto/32 :goto_14

    :cond_6
    goto/32 :goto_12

    nop

    :goto_1c
    const/4 v1, 0x1

    goto/32 :goto_17

    nop

    :goto_1d
    if-nez v2, :cond_7

    goto/32 :goto_10

    :cond_7
    goto/32 :goto_f

    nop

    :goto_1e
    return v0

    :goto_1f
    const/4 v0, 0x0

    goto/32 :goto_1e

    nop

    :goto_20
    check-cast v0, Landroid/view/KeyEvent;

    goto/32 :goto_19

    nop

    :goto_21
    move-object v0, p1

    goto/32 :goto_20

    nop

    :goto_22
    invoke-virtual {p0, v0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    goto/32 :goto_7

    nop

    :goto_23
    invoke-virtual {p0, v0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onTrackballEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    goto/32 :goto_e

    nop
.end method

.method dispatchSurfaceChanged(III)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0, p1, p2, p3}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onSurfaceChanged(III)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method public isMediaViewEnabled()Z
    .locals 1

    iget-boolean v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mMediaViewEnabled:Z

    return v0
.end method

.method public layoutSurface(IIII)V
    .locals 7

    if-gt p1, p3, :cond_0

    if-gt p2, p4, :cond_0

    new-instance v6, Landroid/media/tv/interactive/TvInteractiveAppService$Session$2;

    move-object v0, v6

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Landroid/media/tv/interactive/TvInteractiveAppService$Session$2;-><init>(Landroid/media/tv/interactive/TvInteractiveAppService$Session;IIII)V

    invoke-direct {p0, v6}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid parameter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method notifyAdResponse(Landroid/media/tv/AdResponse;)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0, p1}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onAdResponse(Landroid/media/tv/AdResponse;)V

    goto/32 :goto_0

    nop
.end method

.method public final notifyBiInteractiveAppCreated(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 1

    new-instance v0, Landroid/media/tv/interactive/TvInteractiveAppService$Session$15;

    invoke-direct {v0, p0, p2, p1}, Landroid/media/tv/interactive/TvInteractiveAppService$Session$15;-><init>(Landroid/media/tv/interactive/TvInteractiveAppService$Session;Ljava/lang/String;Landroid/net/Uri;)V

    invoke-direct {p0, v0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method notifyBroadcastInfoResponse(Landroid/media/tv/BroadcastInfoResponse;)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0, p1}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onBroadcastInfoResponse(Landroid/media/tv/BroadcastInfoResponse;)V

    goto/32 :goto_0

    nop
.end method

.method notifyContentAllowed()V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onContentAllowed()V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method notifyContentBlocked(Landroid/media/tv/TvContentRating;)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0, p1}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onContentBlocked(Landroid/media/tv/TvContentRating;)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method notifyError(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0, p1, p2}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onError(Ljava/lang/String;Landroid/os/Bundle;)V

    goto/32 :goto_0

    nop
.end method

.method public notifySessionStateChanged(II)V
    .locals 1

    new-instance v0, Landroid/media/tv/interactive/TvInteractiveAppService$Session$14;

    invoke-direct {v0, p0, p1, p2}, Landroid/media/tv/interactive/TvInteractiveAppService$Session$14;-><init>(Landroid/media/tv/interactive/TvInteractiveAppService$Session;II)V

    invoke-direct {p0, v0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method notifySignalStrength(I)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0, p1}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onSignalStrength(I)V

    goto/32 :goto_0

    nop
.end method

.method public final notifyTeletextAppStateChanged(I)V
    .locals 1

    new-instance v0, Landroid/media/tv/interactive/TvInteractiveAppService$Session$16;

    invoke-direct {v0, p0, p1}, Landroid/media/tv/interactive/TvInteractiveAppService$Session$16;-><init>(Landroid/media/tv/interactive/TvInteractiveAppService$Session;I)V

    invoke-direct {p0, v0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method notifyTrackSelected(ILjava/lang/String;)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0, p1, p2}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onTrackSelected(ILjava/lang/String;)V

    goto/32 :goto_0

    nop
.end method

.method notifyTracksChanged(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/media/tv/TvTrackInfo;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0, p1}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onTracksChanged(Ljava/util/List;)V

    goto/32 :goto_0

    nop
.end method

.method notifyTuned(Landroid/net/Uri;)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0, p1}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onTuned(Landroid/net/Uri;)V

    goto/32 :goto_0

    nop
.end method

.method notifyVideoAvailable()V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onVideoAvailable()V

    goto/32 :goto_0

    nop
.end method

.method notifyVideoUnavailable(I)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0, p1}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onVideoUnavailable(I)V

    goto/32 :goto_0

    nop
.end method

.method public onAdResponse(Landroid/media/tv/AdResponse;)V
    .locals 0

    return-void
.end method

.method public onBroadcastInfoResponse(Landroid/media/tv/BroadcastInfoResponse;)V
    .locals 0

    return-void
.end method

.method public onContentAllowed()V
    .locals 0

    return-void
.end method

.method public onContentBlocked(Landroid/media/tv/TvContentRating;)V
    .locals 0

    return-void
.end method

.method public onCreateBiInteractiveAppRequest(Landroid/net/Uri;Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public onCreateMediaView()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCurrentChannelLcn(I)V
    .locals 0

    return-void
.end method

.method public onCurrentChannelUri(Landroid/net/Uri;)V
    .locals 0

    return-void
.end method

.method public onCurrentTvInputId(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onDestroyBiInteractiveAppRequest(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onError(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onMediaViewSizeChanged(II)V
    .locals 0

    return-void
.end method

.method public abstract onRelease()V
.end method

.method public onResetInteractiveApp()V
    .locals 0

    return-void
.end method

.method public abstract onSetSurface(Landroid/view/Surface;)Z
.end method

.method public onSetTeletextAppEnabled(Z)V
    .locals 0

    return-void
.end method

.method public onSignalStrength(I)V
    .locals 0

    return-void
.end method

.method public onSigningResult(Ljava/lang/String;[B)V
    .locals 0

    return-void
.end method

.method public onStartInteractiveApp()V
    .locals 0

    return-void
.end method

.method public onStopInteractiveApp()V
    .locals 0

    return-void
.end method

.method public onStreamVolume(F)V
    .locals 0

    return-void
.end method

.method public onSurfaceChanged(III)V
    .locals 0

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onTrackInfoList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/media/tv/TvTrackInfo;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public onTrackSelected(ILjava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onTracksChanged(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/media/tv/TvTrackInfo;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public onTuned(Landroid/net/Uri;)V
    .locals 0

    return-void
.end method

.method public onVideoAvailable()V
    .locals 0

    return-void
.end method

.method public onVideoUnavailable(I)V
    .locals 0

    return-void
.end method

.method relayoutMediaView(Landroid/graphics/Rect;)V
    .locals 3

    goto/32 :goto_1b

    nop

    :goto_0
    iget-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    goto/32 :goto_e

    nop

    :goto_1
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_26

    nop

    :goto_2
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    goto/32 :goto_1f

    nop

    :goto_3
    if-ne v0, v1, :cond_0

    goto/32 :goto_10

    :cond_0
    :goto_4
    goto/32 :goto_a

    nop

    :goto_5
    goto/16 :goto_29

    :goto_6
    goto/32 :goto_15

    nop

    :goto_7
    iget-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    goto/32 :goto_1

    nop

    :goto_8
    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/32 :goto_28

    nop

    :goto_9
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    goto/32 :goto_20

    nop

    :goto_a
    iget v0, p1, Landroid/graphics/Rect;->right:I

    goto/32 :goto_16

    nop

    :goto_b
    iget-object v1, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mMediaViewContainer:Landroid/widget/FrameLayout;

    goto/32 :goto_2c

    nop

    :goto_c
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    goto/32 :goto_2f

    nop

    :goto_d
    sub-int/2addr v1, v2

    goto/32 :goto_2

    nop

    :goto_e
    iget v1, p1, Landroid/graphics/Rect;->top:I

    goto/32 :goto_9

    nop

    :goto_f
    invoke-virtual {p0, v0, v1}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onMediaViewSizeChanged(II)V

    :goto_10
    goto/32 :goto_1e

    nop

    :goto_11
    iget v2, p1, Landroid/graphics/Rect;->top:I

    goto/32 :goto_21

    nop

    :goto_12
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    goto/32 :goto_27

    nop

    :goto_13
    iget v2, p1, Landroid/graphics/Rect;->left:I

    goto/32 :goto_18

    nop

    :goto_14
    iget-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mMediaFrame:Landroid/graphics/Rect;

    goto/32 :goto_12

    nop

    :goto_15
    iget-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    goto/32 :goto_1d

    nop

    :goto_16
    iget v1, p1, Landroid/graphics/Rect;->left:I

    goto/32 :goto_19

    nop

    :goto_17
    iget v1, p1, Landroid/graphics/Rect;->right:I

    goto/32 :goto_13

    nop

    :goto_18
    sub-int/2addr v1, v2

    goto/32 :goto_24

    nop

    :goto_19
    sub-int/2addr v0, v1

    goto/32 :goto_2e

    nop

    :goto_1a
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    goto/32 :goto_c

    nop

    :goto_1b
    iget-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mMediaFrame:Landroid/graphics/Rect;

    goto/32 :goto_25

    nop

    :goto_1c
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    goto/32 :goto_0

    nop

    :goto_1d
    iget v1, p1, Landroid/graphics/Rect;->left:I

    goto/32 :goto_1c

    nop

    :goto_1e
    iput-object p1, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mMediaFrame:Landroid/graphics/Rect;

    goto/32 :goto_23

    nop

    :goto_1f
    iget-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mWindowManager:Landroid/view/WindowManager;

    goto/32 :goto_b

    nop

    :goto_20
    iget-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    goto/32 :goto_17

    nop

    :goto_21
    sub-int/2addr v1, v2

    goto/32 :goto_f

    nop

    :goto_22
    iget-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mMediaViewContainer:Landroid/widget/FrameLayout;

    goto/32 :goto_2a

    nop

    :goto_23
    iget-boolean v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mMediaViewEnabled:Z

    goto/32 :goto_2b

    nop

    :goto_24
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    goto/32 :goto_7

    nop

    :goto_25
    if-nez v0, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_1a

    nop

    :goto_26
    iget v2, p1, Landroid/graphics/Rect;->top:I

    goto/32 :goto_d

    nop

    :goto_27
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    goto/32 :goto_3

    nop

    :goto_28
    return-void

    :goto_29
    goto/32 :goto_2d

    nop

    :goto_2a
    if-eqz v0, :cond_2

    goto/32 :goto_6

    :cond_2
    goto/32 :goto_5

    nop

    :goto_2b
    if-nez v0, :cond_3

    goto/32 :goto_29

    :cond_3
    goto/32 :goto_22

    nop

    :goto_2c
    iget-object v2, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    goto/32 :goto_8

    nop

    :goto_2d
    return-void

    :goto_2e
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_11

    nop

    :goto_2f
    if-eq v0, v1, :cond_4

    goto/32 :goto_4

    :cond_4
    goto/32 :goto_14

    nop
.end method

.method release()V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    monitor-enter v0

    :try_start_0
    iput-object v1, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mSessionCallback:Landroid/media/tv/interactive/ITvInteractiveAppSessionCallback;

    iget-object v1, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mPendingActions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_3

    nop

    :goto_1
    invoke-virtual {p0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onRelease()V

    goto/32 :goto_4

    nop

    :goto_2
    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    goto/32 :goto_5

    nop

    :goto_3
    const/4 v0, 0x1

    goto/32 :goto_c

    nop

    :goto_4
    iget-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mSurface:Landroid/view/Surface;

    goto/32 :goto_a

    nop

    :goto_5
    iput-object v1, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mSurface:Landroid/view/Surface;

    :goto_6
    goto/32 :goto_9

    nop

    :goto_7
    throw v1

    :goto_8
    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/32 :goto_7

    nop

    :goto_9
    iget-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mLock:Ljava/lang/Object;

    goto/32 :goto_0

    nop

    :goto_a
    const/4 v1, 0x0

    goto/32 :goto_b

    nop

    :goto_b
    if-nez v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_2

    nop

    :goto_c
    invoke-virtual {p0, v0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->removeMediaView(Z)V

    goto/32 :goto_8

    nop
.end method

.method public removeBroadcastInfo(I)V
    .locals 1

    new-instance v0, Landroid/media/tv/interactive/TvInteractiveAppService$Session$4;

    invoke-direct {v0, p0, p1}, Landroid/media/tv/interactive/TvInteractiveAppService$Session$4;-><init>(Landroid/media/tv/interactive/TvInteractiveAppService$Session;I)V

    invoke-direct {p0, v0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method removeMediaView(Z)V
    .locals 3

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_6

    nop

    :goto_1
    iput-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    :goto_2
    goto/32 :goto_3

    nop

    :goto_3
    return-void

    :goto_4
    iput-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mMediaFrame:Landroid/graphics/Rect;

    :goto_5
    goto/32 :goto_e

    nop

    :goto_6
    if-nez p1, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_f

    nop

    :goto_7
    iget-object v2, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mMediaViewContainer:Landroid/widget/FrameLayout;

    goto/32 :goto_c

    nop

    :goto_8
    if-nez v1, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_10

    nop

    :goto_9
    iget-object v1, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mWindowManager:Landroid/view/WindowManager;

    goto/32 :goto_7

    nop

    :goto_a
    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    goto/32 :goto_b

    nop

    :goto_b
    iput-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mMediaView:Landroid/view/View;

    goto/32 :goto_9

    nop

    :goto_c
    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    goto/32 :goto_d

    nop

    :goto_d
    iput-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mMediaViewContainer:Landroid/widget/FrameLayout;

    goto/32 :goto_1

    nop

    :goto_e
    iget-object v1, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mMediaViewContainer:Landroid/widget/FrameLayout;

    goto/32 :goto_8

    nop

    :goto_f
    iput-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mWindowToken:Landroid/os/IBinder;

    goto/32 :goto_4

    nop

    :goto_10
    iget-object v2, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mMediaView:Landroid/view/View;

    goto/32 :goto_a

    nop
.end method

.method public requestAd(Landroid/media/tv/AdRequest;)V
    .locals 1

    new-instance v0, Landroid/media/tv/interactive/TvInteractiveAppService$Session$13;

    invoke-direct {v0, p0, p1}, Landroid/media/tv/interactive/TvInteractiveAppService$Session$13;-><init>(Landroid/media/tv/interactive/TvInteractiveAppService$Session;Landroid/media/tv/AdRequest;)V

    invoke-direct {p0, v0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public requestBroadcastInfo(Landroid/media/tv/BroadcastInfoRequest;)V
    .locals 1

    new-instance v0, Landroid/media/tv/interactive/TvInteractiveAppService$Session$3;

    invoke-direct {v0, p0, p1}, Landroid/media/tv/interactive/TvInteractiveAppService$Session$3;-><init>(Landroid/media/tv/interactive/TvInteractiveAppService$Session;Landroid/media/tv/BroadcastInfoRequest;)V

    invoke-direct {p0, v0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public requestCurrentChannelLcn()V
    .locals 1

    new-instance v0, Landroid/media/tv/interactive/TvInteractiveAppService$Session$8;

    invoke-direct {v0, p0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session$8;-><init>(Landroid/media/tv/interactive/TvInteractiveAppService$Session;)V

    invoke-direct {p0, v0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public requestCurrentChannelUri()V
    .locals 1

    new-instance v0, Landroid/media/tv/interactive/TvInteractiveAppService$Session$7;

    invoke-direct {v0, p0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session$7;-><init>(Landroid/media/tv/interactive/TvInteractiveAppService$Session;)V

    invoke-direct {p0, v0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public requestCurrentTvInputId()V
    .locals 1

    new-instance v0, Landroid/media/tv/interactive/TvInteractiveAppService$Session$11;

    invoke-direct {v0, p0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session$11;-><init>(Landroid/media/tv/interactive/TvInteractiveAppService$Session;)V

    invoke-direct {p0, v0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public requestSigning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 7

    new-instance v6, Landroid/media/tv/interactive/TvInteractiveAppService$Session$12;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Landroid/media/tv/interactive/TvInteractiveAppService$Session$12;-><init>(Landroid/media/tv/interactive/TvInteractiveAppService$Session;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V

    invoke-direct {p0, v6}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public requestStreamVolume()V
    .locals 1

    new-instance v0, Landroid/media/tv/interactive/TvInteractiveAppService$Session$9;

    invoke-direct {v0, p0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session$9;-><init>(Landroid/media/tv/interactive/TvInteractiveAppService$Session;)V

    invoke-direct {p0, v0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public requestTrackInfoList()V
    .locals 1

    new-instance v0, Landroid/media/tv/interactive/TvInteractiveAppService$Session$10;

    invoke-direct {v0, p0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session$10;-><init>(Landroid/media/tv/interactive/TvInteractiveAppService$Session;)V

    invoke-direct {p0, v0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method resetInteractiveApp()V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onResetInteractiveApp()V

    goto/32 :goto_0

    nop
.end method

.method scheduleMediaViewCleanup()V
    .locals 5

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mMediaViewContainer:Landroid/widget/FrameLayout;

    goto/32 :goto_4

    nop

    :goto_1
    invoke-virtual {v1, v2, v3}, Landroid/media/tv/interactive/TvInteractiveAppService$MediaViewCleanUpTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_2
    goto/32 :goto_8

    nop

    :goto_3
    iput-object v1, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mMediaViewCleanUpTask:Landroid/media/tv/interactive/TvInteractiveAppService$MediaViewCleanUpTask;

    goto/32 :goto_6

    nop

    :goto_4
    if-nez v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_c

    nop

    :goto_5
    invoke-direct {v1, v2}, Landroid/media/tv/interactive/TvInteractiveAppService$MediaViewCleanUpTask;-><init>(Landroid/media/tv/interactive/TvInteractiveAppService$MediaViewCleanUpTask-IA;)V

    goto/32 :goto_3

    nop

    :goto_6
    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    goto/32 :goto_7

    nop

    :goto_7
    const/4 v3, 0x1

    goto/32 :goto_d

    nop

    :goto_8
    return-void

    :goto_9
    const/4 v2, 0x0

    goto/32 :goto_5

    nop

    :goto_a
    const/4 v4, 0x0

    goto/32 :goto_b

    nop

    :goto_b
    aput-object v0, v3, v4

    goto/32 :goto_1

    nop

    :goto_c
    new-instance v1, Landroid/media/tv/interactive/TvInteractiveAppService$MediaViewCleanUpTask;

    goto/32 :goto_9

    nop

    :goto_d
    new-array v3, v3, [Landroid/view/View;

    goto/32 :goto_a

    nop
.end method

.method sendCurrentChannelLcn(I)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0, p1}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onCurrentChannelLcn(I)V

    goto/32 :goto_0

    nop
.end method

.method sendCurrentChannelUri(Landroid/net/Uri;)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0, p1}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onCurrentChannelUri(Landroid/net/Uri;)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method sendCurrentTvInputId(Ljava/lang/String;)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0, p1}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onCurrentTvInputId(Ljava/lang/String;)V

    goto/32 :goto_0

    nop
.end method

.method public sendPlaybackCommandRequest(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    new-instance v0, Landroid/media/tv/interactive/TvInteractiveAppService$Session$5;

    invoke-direct {v0, p0, p1, p2}, Landroid/media/tv/interactive/TvInteractiveAppService$Session$5;-><init>(Landroid/media/tv/interactive/TvInteractiveAppService$Session;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {p0, v0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method sendSigningResult(Ljava/lang/String;[B)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0, p1, p2}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onSigningResult(Ljava/lang/String;[B)V

    goto/32 :goto_0

    nop
.end method

.method sendStreamVolume(F)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0, p1}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onStreamVolume(F)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method sendTrackInfoList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/media/tv/TvTrackInfo;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0, p1}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onTrackInfoList(Ljava/util/List;)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method public setMediaViewEnabled(Z)V
    .locals 2

    iget-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mHandler:Landroid/os/Handler;

    new-instance v1, Landroid/media/tv/interactive/TvInteractiveAppService$Session$1;

    invoke-direct {v1, p0, p1}, Landroid/media/tv/interactive/TvInteractiveAppService$Session$1;-><init>(Landroid/media/tv/interactive/TvInteractiveAppService$Session;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method setSurface(Landroid/view/Surface;)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    :goto_1
    goto/32 :goto_3

    nop

    :goto_2
    invoke-virtual {p0, p1}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onSetSurface(Landroid/view/Surface;)Z

    goto/32 :goto_4

    nop

    :goto_3
    iput-object p1, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mSurface:Landroid/view/Surface;

    goto/32 :goto_5

    nop

    :goto_4
    iget-object v0, p0, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->mSurface:Landroid/view/Surface;

    goto/32 :goto_6

    nop

    :goto_5
    return-void

    :goto_6
    if-nez v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_0

    nop
.end method

.method setTeletextAppEnabled(Z)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0, p1}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onSetTeletextAppEnabled(Z)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method public setVideoBounds(Landroid/graphics/Rect;)V
    .locals 1

    new-instance v0, Landroid/media/tv/interactive/TvInteractiveAppService$Session$6;

    invoke-direct {v0, p0, p1}, Landroid/media/tv/interactive/TvInteractiveAppService$Session$6;-><init>(Landroid/media/tv/interactive/TvInteractiveAppService$Session;Landroid/graphics/Rect;)V

    invoke-direct {p0, v0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method startInteractiveApp()V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onStartInteractiveApp()V

    goto/32 :goto_0

    nop
.end method

.method stopInteractiveApp()V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0}, Landroid/media/tv/interactive/TvInteractiveAppService$Session;->onStopInteractiveApp()V

    goto/32 :goto_0

    nop
.end method
