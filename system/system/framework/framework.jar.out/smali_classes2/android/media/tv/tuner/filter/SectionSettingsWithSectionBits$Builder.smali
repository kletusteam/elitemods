.class public Landroid/media/tv/tuner/filter/SectionSettingsWithSectionBits$Builder;
.super Landroid/media/tv/tuner/filter/SectionSettings$Builder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/tv/tuner/filter/SectionSettingsWithSectionBits;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/media/tv/tuner/filter/SectionSettings$Builder<",
        "Landroid/media/tv/tuner/filter/SectionSettingsWithSectionBits$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private mFilter:[B

.field private mMask:[B

.field private mMode:[B


# direct methods
.method private constructor <init>(I)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/media/tv/tuner/filter/SectionSettings$Builder;-><init>(I)V

    const/4 v0, 0x0

    new-array v1, v0, [B

    iput-object v1, p0, Landroid/media/tv/tuner/filter/SectionSettingsWithSectionBits$Builder;->mFilter:[B

    new-array v1, v0, [B

    iput-object v1, p0, Landroid/media/tv/tuner/filter/SectionSettingsWithSectionBits$Builder;->mMask:[B

    new-array v0, v0, [B

    iput-object v0, p0, Landroid/media/tv/tuner/filter/SectionSettingsWithSectionBits$Builder;->mMode:[B

    return-void
.end method

.method synthetic constructor <init>(ILandroid/media/tv/tuner/filter/SectionSettingsWithSectionBits$Builder-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/media/tv/tuner/filter/SectionSettingsWithSectionBits$Builder;-><init>(I)V

    return-void
.end method


# virtual methods
.method public build()Landroid/media/tv/tuner/filter/SectionSettingsWithSectionBits;
    .locals 11

    new-instance v10, Landroid/media/tv/tuner/filter/SectionSettingsWithSectionBits;

    iget v1, p0, Landroid/media/tv/tuner/filter/SectionSettingsWithSectionBits$Builder;->mMainType:I

    iget-boolean v2, p0, Landroid/media/tv/tuner/filter/SectionSettingsWithSectionBits$Builder;->mCrcEnabled:Z

    iget-boolean v3, p0, Landroid/media/tv/tuner/filter/SectionSettingsWithSectionBits$Builder;->mIsRepeat:Z

    iget-boolean v4, p0, Landroid/media/tv/tuner/filter/SectionSettingsWithSectionBits$Builder;->mIsRaw:Z

    iget v5, p0, Landroid/media/tv/tuner/filter/SectionSettingsWithSectionBits$Builder;->mBitWidthOfLengthField:I

    iget-object v6, p0, Landroid/media/tv/tuner/filter/SectionSettingsWithSectionBits$Builder;->mFilter:[B

    iget-object v7, p0, Landroid/media/tv/tuner/filter/SectionSettingsWithSectionBits$Builder;->mMask:[B

    iget-object v8, p0, Landroid/media/tv/tuner/filter/SectionSettingsWithSectionBits$Builder;->mMode:[B

    const/4 v9, 0x0

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Landroid/media/tv/tuner/filter/SectionSettingsWithSectionBits;-><init>(IZZZI[B[B[BLandroid/media/tv/tuner/filter/SectionSettingsWithSectionBits-IA;)V

    return-object v10
.end method

.method bridge synthetic self()Landroid/media/tv/tuner/filter/SectionSettings$Builder;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    invoke-virtual {p0}, Landroid/media/tv/tuner/filter/SectionSettingsWithSectionBits$Builder;->self()Landroid/media/tv/tuner/filter/SectionSettingsWithSectionBits$Builder;

    move-result-object v0

    goto/32 :goto_0

    nop
.end method

.method self()Landroid/media/tv/tuner/filter/SectionSettingsWithSectionBits$Builder;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    return-object p0
.end method

.method public setFilter([B)Landroid/media/tv/tuner/filter/SectionSettingsWithSectionBits$Builder;
    .locals 0

    iput-object p1, p0, Landroid/media/tv/tuner/filter/SectionSettingsWithSectionBits$Builder;->mFilter:[B

    return-object p0
.end method

.method public setMask([B)Landroid/media/tv/tuner/filter/SectionSettingsWithSectionBits$Builder;
    .locals 0

    iput-object p1, p0, Landroid/media/tv/tuner/filter/SectionSettingsWithSectionBits$Builder;->mMask:[B

    return-object p0
.end method

.method public setMode([B)Landroid/media/tv/tuner/filter/SectionSettingsWithSectionBits$Builder;
    .locals 0

    iput-object p1, p0, Landroid/media/tv/tuner/filter/SectionSettingsWithSectionBits$Builder;->mMode:[B

    return-object p0
.end method
