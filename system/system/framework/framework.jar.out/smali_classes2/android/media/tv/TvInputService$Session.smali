.class public abstract Landroid/media/tv/TvInputService$Session;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/KeyEvent$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/tv/TvInputService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Session"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/tv/TvInputService$Session$TimeShiftPositionTrackingRunnable;
    }
.end annotation


# static fields
.field private static final POSITION_UPDATE_INTERVAL_MS:I = 0x3e8


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mCurrentPositionMs:J

.field private final mDispatcherState:Landroid/view/KeyEvent$DispatcherState;

.field final mHandler:Landroid/os/Handler;

.field private final mLock:Ljava/lang/Object;

.field private mOverlayFrame:Landroid/graphics/Rect;

.field private mOverlayView:Landroid/view/View;

.field private mOverlayViewCleanUpTask:Landroid/media/tv/TvInputService$OverlayViewCleanUpTask;

.field private mOverlayViewContainer:Landroid/widget/FrameLayout;

.field private mOverlayViewEnabled:Z

.field private final mPendingActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private mSessionCallback:Landroid/media/tv/ITvInputSessionCallback;

.field private mStartPositionMs:J

.field private mSurface:Landroid/view/Surface;

.field private final mTimeShiftPositionTrackingRunnable:Landroid/media/tv/TvInputService$Session$TimeShiftPositionTrackingRunnable;

.field private final mWindowManager:Landroid/view/WindowManager;

.field private mWindowParams:Landroid/view/WindowManager$LayoutParams;

.field private mWindowToken:Landroid/os/IBinder;


# direct methods
.method static bridge synthetic -$$Nest$fgetmCurrentPositionMs(Landroid/media/tv/TvInputService$Session;)J
    .locals 2

    iget-wide v0, p0, Landroid/media/tv/TvInputService$Session;->mCurrentPositionMs:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmOverlayFrame(Landroid/media/tv/TvInputService$Session;)Landroid/graphics/Rect;
    .locals 0

    iget-object p0, p0, Landroid/media/tv/TvInputService$Session;->mOverlayFrame:Landroid/graphics/Rect;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmOverlayViewEnabled(Landroid/media/tv/TvInputService$Session;)Z
    .locals 0

    iget-boolean p0, p0, Landroid/media/tv/TvInputService$Session;->mOverlayViewEnabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSessionCallback(Landroid/media/tv/TvInputService$Session;)Landroid/media/tv/ITvInputSessionCallback;
    .locals 0

    iget-object p0, p0, Landroid/media/tv/TvInputService$Session;->mSessionCallback:Landroid/media/tv/ITvInputSessionCallback;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmStartPositionMs(Landroid/media/tv/TvInputService$Session;)J
    .locals 2

    iget-wide v0, p0, Landroid/media/tv/TvInputService$Session;->mStartPositionMs:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmTimeShiftPositionTrackingRunnable(Landroid/media/tv/TvInputService$Session;)Landroid/media/tv/TvInputService$Session$TimeShiftPositionTrackingRunnable;
    .locals 0

    iget-object p0, p0, Landroid/media/tv/TvInputService$Session;->mTimeShiftPositionTrackingRunnable:Landroid/media/tv/TvInputService$Session$TimeShiftPositionTrackingRunnable;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmWindowToken(Landroid/media/tv/TvInputService$Session;)Landroid/os/IBinder;
    .locals 0

    iget-object p0, p0, Landroid/media/tv/TvInputService$Session;->mWindowToken:Landroid/os/IBinder;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmCurrentPositionMs(Landroid/media/tv/TvInputService$Session;J)V
    .locals 0

    iput-wide p1, p0, Landroid/media/tv/TvInputService$Session;->mCurrentPositionMs:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmOverlayViewEnabled(Landroid/media/tv/TvInputService$Session;Z)V
    .locals 0

    iput-boolean p1, p0, Landroid/media/tv/TvInputService$Session;->mOverlayViewEnabled:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmStartPositionMs(Landroid/media/tv/TvInputService$Session;J)V
    .locals 0

    iput-wide p1, p0, Landroid/media/tv/TvInputService$Session;->mStartPositionMs:J

    return-void
.end method

.method static bridge synthetic -$$Nest$minitialize(Landroid/media/tv/TvInputService$Session;Landroid/media/tv/ITvInputSessionCallback;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/media/tv/TvInputService$Session;->initialize(Landroid/media/tv/ITvInputSessionCallback;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mnotifyTimeShiftCurrentPositionChanged(Landroid/media/tv/TvInputService$Session;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/media/tv/TvInputService$Session;->notifyTimeShiftCurrentPositionChanged(J)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mnotifyTimeShiftStartPositionChanged(Landroid/media/tv/TvInputService$Session;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/media/tv/TvInputService$Session;->notifyTimeShiftStartPositionChanged(J)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/view/KeyEvent$DispatcherState;

    invoke-direct {v0}, Landroid/view/KeyEvent$DispatcherState;-><init>()V

    iput-object v0, p0, Landroid/media/tv/TvInputService$Session;->mDispatcherState:Landroid/view/KeyEvent$DispatcherState;

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Landroid/media/tv/TvInputService$Session;->mStartPositionMs:J

    iput-wide v0, p0, Landroid/media/tv/TvInputService$Session;->mCurrentPositionMs:J

    new-instance v0, Landroid/media/tv/TvInputService$Session$TimeShiftPositionTrackingRunnable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroid/media/tv/TvInputService$Session$TimeShiftPositionTrackingRunnable;-><init>(Landroid/media/tv/TvInputService$Session;Landroid/media/tv/TvInputService$Session$TimeShiftPositionTrackingRunnable-IA;)V

    iput-object v0, p0, Landroid/media/tv/TvInputService$Session;->mTimeShiftPositionTrackingRunnable:Landroid/media/tv/TvInputService$Session$TimeShiftPositionTrackingRunnable;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Landroid/media/tv/TvInputService$Session;->mLock:Ljava/lang/Object;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/media/tv/TvInputService$Session;->mPendingActions:Ljava/util/List;

    iput-object p1, p0, Landroid/media/tv/TvInputService$Session;->mContext:Landroid/content/Context;

    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Landroid/media/tv/TvInputService$Session;->mWindowManager:Landroid/view/WindowManager;

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Landroid/media/tv/TvInputService$Session;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V
    .locals 2

    iget-object v0, p0, Landroid/media/tv/TvInputService$Session;->mLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Landroid/media/tv/TvInputService$Session;->mSessionCallback:Landroid/media/tv/ITvInputSessionCallback;

    if-nez v1, :cond_0

    iget-object v1, p0, Landroid/media/tv/TvInputService$Session;->mPendingActions:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v1, p0, Landroid/media/tv/TvInputService$Session;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->isCurrentThread()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Landroid/media/tv/TvInputService$Session;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private initialize(Landroid/media/tv/ITvInputSessionCallback;)V
    .locals 3

    iget-object v0, p0, Landroid/media/tv/TvInputService$Session;->mLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iput-object p1, p0, Landroid/media/tv/TvInputService$Session;->mSessionCallback:Landroid/media/tv/ITvInputSessionCallback;

    iget-object v1, p0, Landroid/media/tv/TvInputService$Session;->mPendingActions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Landroid/media/tv/TvInputService$Session;->mPendingActions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private notifyTimeShiftCurrentPositionChanged(J)V
    .locals 1

    new-instance v0, Landroid/media/tv/TvInputService$Session$15;

    invoke-direct {v0, p0, p1, p2}, Landroid/media/tv/TvInputService$Session$15;-><init>(Landroid/media/tv/TvInputService$Session;J)V

    invoke-direct {p0, v0}, Landroid/media/tv/TvInputService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private notifyTimeShiftStartPositionChanged(J)V
    .locals 1

    new-instance v0, Landroid/media/tv/TvInputService$Session$14;

    invoke-direct {v0, p0, p1, p2}, Landroid/media/tv/TvInputService$Session$14;-><init>(Landroid/media/tv/TvInputService$Session;J)V

    invoke-direct {p0, v0}, Landroid/media/tv/TvInputService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method appPrivateCommand(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0, p1, p2}, Landroid/media/tv/TvInputService$Session;->onAppPrivateCommand(Ljava/lang/String;Landroid/os/Bundle;)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method createOverlayView(Landroid/os/IBinder;Landroid/graphics/Rect;)V
    .locals 11

    goto/32 :goto_1a

    nop

    :goto_0
    iput-object v0, p0, Landroid/media/tv/TvInputService$Session;->mOverlayViewCleanUpTask:Landroid/media/tv/TvInputService$OverlayViewCleanUpTask;

    :goto_1
    goto/32 :goto_33

    nop

    :goto_2
    iget-object v2, p0, Landroid/media/tv/TvInputService$Session;->mWindowManager:Landroid/view/WindowManager;

    goto/32 :goto_3a

    nop

    :goto_3
    iget v3, p2, Landroid/graphics/Rect;->left:I

    goto/32 :goto_2b

    nop

    :goto_4
    sub-int/2addr v0, v1

    goto/32 :goto_47

    nop

    :goto_5
    sub-int v4, v2, v4

    goto/32 :goto_26

    nop

    :goto_6
    iput-object p1, p0, Landroid/media/tv/TvInputService$Session;->mWindowToken:Landroid/os/IBinder;

    goto/32 :goto_45

    nop

    :goto_7
    iput-object v0, p0, Landroid/media/tv/TvInputService$Session;->mOverlayViewContainer:Landroid/widget/FrameLayout;

    goto/32 :goto_41

    nop

    :goto_8
    iget v2, v10, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    goto/32 :goto_34

    nop

    :goto_9
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_3d

    nop

    :goto_a
    iget-object v0, p0, Landroid/media/tv/TvInputService$Session;->mOverlayViewCleanUpTask:Landroid/media/tv/TvInputService$OverlayViewCleanUpTask;

    goto/32 :goto_46

    nop

    :goto_b
    return-void

    :goto_c
    goto/32 :goto_3f

    nop

    :goto_d
    sub-int/2addr v1, v2

    goto/32 :goto_35

    nop

    :goto_e
    iget v2, p2, Landroid/graphics/Rect;->top:I

    goto/32 :goto_d

    nop

    :goto_f
    iget-object v4, p0, Landroid/media/tv/TvInputService$Session;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    goto/32 :goto_13

    nop

    :goto_10
    invoke-virtual {p0, v0}, Landroid/media/tv/TvInputService$Session;->removeOverlayView(Z)V

    :goto_11
    goto/32 :goto_6

    nop

    :goto_12
    if-eqz v0, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_b

    nop

    :goto_13
    invoke-interface {v2, v3, v4}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/32 :goto_19

    nop

    :goto_14
    iput-object p1, v2, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    goto/32 :goto_2

    nop

    :goto_15
    const/4 v0, 0x0

    goto/32 :goto_10

    nop

    :goto_16
    move v8, v1

    goto/32 :goto_1c

    nop

    :goto_17
    const/16 v1, 0x218

    goto/32 :goto_3e

    nop

    :goto_18
    iget-object v2, p0, Landroid/media/tv/TvInputService$Session;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    goto/32 :goto_14

    nop

    :goto_19
    return-void

    :goto_1a
    iget-object v0, p0, Landroid/media/tv/TvInputService$Session;->mOverlayViewContainer:Landroid/widget/FrameLayout;

    goto/32 :goto_21

    nop

    :goto_1b
    move v7, v0

    goto/32 :goto_16

    nop

    :goto_1c
    invoke-direct/range {v2 .. v9}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIIIII)V

    goto/32 :goto_37

    nop

    :goto_1d
    iget v2, p2, Landroid/graphics/Rect;->right:I

    goto/32 :goto_3

    nop

    :goto_1e
    const/4 v0, 0x0

    goto/32 :goto_0

    nop

    :goto_1f
    const/4 v9, -0x2

    goto/32 :goto_28

    nop

    :goto_20
    iget-object v2, p0, Landroid/media/tv/TvInputService$Session;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    goto/32 :goto_2d

    nop

    :goto_21
    if-nez v0, :cond_1

    goto/32 :goto_11

    :cond_1
    goto/32 :goto_15

    nop

    :goto_22
    iput v2, v10, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    goto/32 :goto_20

    nop

    :goto_23
    or-int/2addr v1, v2

    :goto_24
    goto/32 :goto_2c

    nop

    :goto_25
    iget v2, p2, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_31

    nop

    :goto_26
    iget v5, p2, Landroid/graphics/Rect;->left:I

    goto/32 :goto_29

    nop

    :goto_27
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto/32 :goto_43

    nop

    :goto_28
    move-object v2, v10

    goto/32 :goto_1b

    nop

    :goto_29
    iget v6, p2, Landroid/graphics/Rect;->top:I

    goto/32 :goto_1f

    nop

    :goto_2a
    if-nez v2, :cond_2

    goto/32 :goto_24

    :cond_2
    goto/32 :goto_42

    nop

    :goto_2b
    sub-int v3, v2, v3

    goto/32 :goto_25

    nop

    :goto_2c
    new-instance v10, Landroid/view/WindowManager$LayoutParams;

    goto/32 :goto_1d

    nop

    :goto_2d
    const v3, 0x800033

    goto/32 :goto_40

    nop

    :goto_2e
    return-void

    :goto_2f
    goto/32 :goto_a

    nop

    :goto_30
    iput-object v0, p0, Landroid/media/tv/TvInputService$Session;->mOverlayView:Landroid/view/View;

    goto/32 :goto_3b

    nop

    :goto_31
    iget v4, p2, Landroid/graphics/Rect;->top:I

    goto/32 :goto_5

    nop

    :goto_32
    const/4 v1, 0x1

    goto/32 :goto_3c

    nop

    :goto_33
    new-instance v0, Landroid/widget/FrameLayout;

    goto/32 :goto_36

    nop

    :goto_34
    or-int/lit8 v2, v2, 0x40

    goto/32 :goto_22

    nop

    :goto_35
    invoke-virtual {p0, v0, v1}, Landroid/media/tv/TvInputService$Session;->onOverlayViewSizeChanged(II)V

    goto/32 :goto_39

    nop

    :goto_36
    iget-object v1, p0, Landroid/media/tv/TvInputService$Session;->mContext:Landroid/content/Context;

    goto/32 :goto_9

    nop

    :goto_37
    iput-object v10, p0, Landroid/media/tv/TvInputService$Session;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    goto/32 :goto_8

    nop

    :goto_38
    iget v0, p2, Landroid/graphics/Rect;->right:I

    goto/32 :goto_44

    nop

    :goto_39
    iget-boolean v0, p0, Landroid/media/tv/TvInputService$Session;->mOverlayViewEnabled:Z

    goto/32 :goto_12

    nop

    :goto_3a
    iget-object v3, p0, Landroid/media/tv/TvInputService$Session;->mOverlayViewContainer:Landroid/widget/FrameLayout;

    goto/32 :goto_f

    nop

    :goto_3b
    if-eqz v0, :cond_3

    goto/32 :goto_2f

    :cond_3
    goto/32 :goto_2e

    nop

    :goto_3c
    invoke-virtual {v0, v1}, Landroid/media/tv/TvInputService$OverlayViewCleanUpTask;->cancel(Z)Z

    goto/32 :goto_1e

    nop

    :goto_3d
    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    goto/32 :goto_7

    nop

    :goto_3e
    invoke-static {}, Landroid/app/ActivityManager;->isHighEndGfx()Z

    move-result v2

    goto/32 :goto_2a

    nop

    :goto_3f
    invoke-virtual {p0}, Landroid/media/tv/TvInputService$Session;->onCreateOverlayView()Landroid/view/View;

    move-result-object v0

    goto/32 :goto_30

    nop

    :goto_40
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    goto/32 :goto_18

    nop

    :goto_41
    iget-object v1, p0, Landroid/media/tv/TvInputService$Session;->mOverlayView:Landroid/view/View;

    goto/32 :goto_27

    nop

    :goto_42
    const/high16 v2, 0x1000000

    goto/32 :goto_23

    nop

    :goto_43
    const/16 v0, 0x3ec

    goto/32 :goto_17

    nop

    :goto_44
    iget v1, p2, Landroid/graphics/Rect;->left:I

    goto/32 :goto_4

    nop

    :goto_45
    iput-object p2, p0, Landroid/media/tv/TvInputService$Session;->mOverlayFrame:Landroid/graphics/Rect;

    goto/32 :goto_38

    nop

    :goto_46
    if-nez v0, :cond_4

    goto/32 :goto_1

    :cond_4
    goto/32 :goto_32

    nop

    :goto_47
    iget v1, p2, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_e

    nop
.end method

.method dispatchInputEvent(Landroid/view/InputEvent;Landroid/view/InputEventReceiver;)I
    .locals 7

    goto/32 :goto_46

    nop

    :goto_0
    iget-object v5, p0, Landroid/media/tv/TvInputService$Session;->mDispatcherState:Landroid/view/KeyEvent$DispatcherState;

    goto/32 :goto_42

    nop

    :goto_1
    goto :goto_b

    :goto_2
    goto/32 :goto_a

    nop

    :goto_3
    const/4 v2, -0x1

    goto/32 :goto_14

    nop

    :goto_4
    if-nez v6, :cond_0

    goto/32 :goto_2c

    :cond_0
    goto/32 :goto_17

    nop

    :goto_5
    if-nez v2, :cond_1

    goto/32 :goto_3f

    :cond_1
    goto/32 :goto_2a

    nop

    :goto_6
    if-eqz v2, :cond_2

    goto/32 :goto_48

    :cond_2
    goto/32 :goto_26

    nop

    :goto_7
    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getViewRootImpl()Landroid/view/ViewRootImpl;

    move-result-object v2

    goto/32 :goto_38

    nop

    :goto_8
    if-nez v2, :cond_3

    goto/32 :goto_1e

    :cond_3
    goto/32 :goto_4c

    nop

    :goto_9
    if-nez v2, :cond_4

    goto/32 :goto_15

    :cond_4
    goto/32 :goto_2e

    nop

    :goto_a
    move v5, v4

    :goto_b
    goto/32 :goto_1d

    nop

    :goto_c
    if-nez v5, :cond_5

    goto/32 :goto_e

    :cond_5
    goto/32 :goto_d

    nop

    :goto_d
    return v4

    :goto_e
    goto/32 :goto_41

    nop

    :goto_f
    invoke-static {v5}, Landroid/view/KeyEvent;->isMediaSessionKey(I)Z

    move-result v5

    goto/32 :goto_22

    nop

    :goto_10
    invoke-virtual {v2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v5

    goto/32 :goto_f

    nop

    :goto_11
    invoke-virtual {v2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v5

    goto/32 :goto_30

    nop

    :goto_12
    invoke-virtual {v2}, Landroid/view/MotionEvent;->getSource()I

    move-result v5

    goto/32 :goto_36

    nop

    :goto_13
    invoke-virtual {p0, v2}, Landroid/media/tv/TvInputService$Session;->onTrackballEvent(Landroid/view/MotionEvent;)Z

    move-result v6

    goto/32 :goto_37

    nop

    :goto_14
    return v2

    :goto_15
    goto/32 :goto_2d

    nop

    :goto_16
    instance-of v2, p1, Landroid/view/KeyEvent;

    goto/32 :goto_1b

    nop

    :goto_17
    return v4

    :goto_18
    goto/32 :goto_52

    nop

    :goto_19
    if-nez v6, :cond_6

    goto/32 :goto_18

    :cond_6
    goto/32 :goto_1a

    nop

    :goto_1a
    invoke-virtual {p0, v2}, Landroid/media/tv/TvInputService$Session;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v6

    goto/32 :goto_4

    nop

    :goto_1b
    const/4 v3, 0x0

    goto/32 :goto_32

    nop

    :goto_1c
    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getViewRootImpl()Landroid/view/ViewRootImpl;

    move-result-object v2

    goto/32 :goto_47

    nop

    :goto_1d
    move v1, v5

    :goto_1e
    goto/32 :goto_3e

    nop

    :goto_1f
    if-nez v1, :cond_7

    goto/32 :goto_51

    :cond_7
    goto/32 :goto_50

    nop

    :goto_20
    invoke-virtual {p0, v2}, Landroid/media/tv/TvInputService$Session;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v6

    goto/32 :goto_45

    nop

    :goto_21
    if-eq v5, v6, :cond_8

    goto/32 :goto_25

    :cond_8
    goto/32 :goto_24

    nop

    :goto_22
    if-eqz v5, :cond_9

    goto/32 :goto_2

    :cond_9
    goto/32 :goto_11

    nop

    :goto_23
    instance-of v2, p1, Landroid/view/MotionEvent;

    goto/32 :goto_8

    nop

    :goto_24
    goto/16 :goto_2

    :goto_25
    goto/32 :goto_4b

    nop

    :goto_26
    iget-object v2, p0, Landroid/media/tv/TvInputService$Session;->mOverlayViewContainer:Landroid/widget/FrameLayout;

    goto/32 :goto_1c

    nop

    :goto_27
    iget-object v2, p0, Landroid/media/tv/TvInputService$Session;->mOverlayViewContainer:Landroid/widget/FrameLayout;

    goto/32 :goto_44

    nop

    :goto_28
    iget-object v2, p0, Landroid/media/tv/TvInputService$Session;->mOverlayViewContainer:Landroid/widget/FrameLayout;

    goto/32 :goto_9

    nop

    :goto_29
    if-nez v2, :cond_a

    goto/32 :goto_4e

    :cond_a
    goto/32 :goto_27

    nop

    :goto_2a
    move-object v2, p1

    goto/32 :goto_3c

    nop

    :goto_2b
    return v4

    :goto_2c
    goto/32 :goto_28

    nop

    :goto_2d
    return v3

    :goto_2e
    invoke-virtual {v2}, Landroid/widget/FrameLayout;->isAttachedToWindow()Z

    move-result v2

    goto/32 :goto_3b

    nop

    :goto_2f
    if-nez v0, :cond_b

    goto/32 :goto_4e

    :cond_b
    goto/32 :goto_3a

    nop

    :goto_30
    const/16 v6, 0xde

    goto/32 :goto_21

    nop

    :goto_31
    invoke-virtual {v2}, Landroid/widget/FrameLayout;->hasFocusable()Z

    move-result v2

    goto/32 :goto_29

    nop

    :goto_32
    const/4 v4, 0x1

    goto/32 :goto_5

    nop

    :goto_33
    if-nez v6, :cond_c

    goto/32 :goto_35

    :cond_c
    goto/32 :goto_13

    nop

    :goto_34
    return v4

    :goto_35
    goto/32 :goto_20

    nop

    :goto_36
    invoke-virtual {v2}, Landroid/view/MotionEvent;->isTouchEvent()Z

    move-result v6

    goto/32 :goto_19

    nop

    :goto_37
    if-nez v6, :cond_d

    goto/32 :goto_2c

    :cond_d
    goto/32 :goto_34

    nop

    :goto_38
    invoke-virtual {v2, p1, p2}, Landroid/view/ViewRootImpl;->dispatchInputEvent(Landroid/view/InputEvent;Landroid/view/InputEventReceiver;)V

    goto/32 :goto_3

    nop

    :goto_39
    iget-object v2, p0, Landroid/media/tv/TvInputService$Session;->mOverlayViewContainer:Landroid/widget/FrameLayout;

    goto/32 :goto_7

    nop

    :goto_3a
    iget-object v2, p0, Landroid/media/tv/TvInputService$Session;->mOverlayViewContainer:Landroid/widget/FrameLayout;

    goto/32 :goto_31

    nop

    :goto_3b
    if-nez v2, :cond_e

    goto/32 :goto_15

    :cond_e
    goto/32 :goto_1f

    nop

    :goto_3c
    check-cast v2, Landroid/view/KeyEvent;

    goto/32 :goto_0

    nop

    :goto_3d
    iget-object v2, p0, Landroid/media/tv/TvInputService$Session;->mOverlayViewContainer:Landroid/widget/FrameLayout;

    goto/32 :goto_4f

    nop

    :goto_3e
    goto :goto_2c

    :goto_3f
    goto/32 :goto_23

    nop

    :goto_40
    const/4 v1, 0x0

    goto/32 :goto_16

    nop

    :goto_41
    invoke-virtual {v2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v5

    goto/32 :goto_43

    nop

    :goto_42
    invoke-virtual {v2, p0, v5, p0}, Landroid/view/KeyEvent;->dispatch(Landroid/view/KeyEvent$Callback;Landroid/view/KeyEvent$DispatcherState;Ljava/lang/Object;)Z

    move-result v5

    goto/32 :goto_c

    nop

    :goto_43
    invoke-static {v5}, Landroid/media/tv/TvInputService;->isNavigationKey(I)Z

    move-result v0

    goto/32 :goto_10

    nop

    :goto_44
    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getViewRootImpl()Landroid/view/ViewRootImpl;

    move-result-object v2

    goto/32 :goto_4a

    nop

    :goto_45
    if-nez v6, :cond_f

    goto/32 :goto_2c

    :cond_f
    goto/32 :goto_2b

    nop

    :goto_46
    const/4 v0, 0x0

    goto/32 :goto_40

    nop

    :goto_47
    invoke-virtual {v2, v4}, Landroid/view/ViewRootImpl;->windowFocusChanged(Z)V

    :goto_48
    goto/32 :goto_2f

    nop

    :goto_49
    check-cast v2, Landroid/view/MotionEvent;

    goto/32 :goto_12

    nop

    :goto_4a
    invoke-virtual {v2, p1}, Landroid/view/ViewRootImpl;->dispatchInputEvent(Landroid/view/InputEvent;)V

    goto/32 :goto_4d

    nop

    :goto_4b
    move v5, v3

    goto/32 :goto_1

    nop

    :goto_4c
    move-object v2, p1

    goto/32 :goto_49

    nop

    :goto_4d
    return v4

    :goto_4e
    goto/32 :goto_39

    nop

    :goto_4f
    invoke-virtual {v2}, Landroid/widget/FrameLayout;->hasWindowFocus()Z

    move-result v2

    goto/32 :goto_6

    nop

    :goto_50
    goto/16 :goto_15

    :goto_51
    goto/32 :goto_3d

    nop

    :goto_52
    and-int/lit8 v6, v5, 0x4

    goto/32 :goto_33

    nop
.end method

.method dispatchSurfaceChanged(III)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0, p1, p2, p3}, Landroid/media/tv/TvInputService$Session;->onSurfaceChanged(III)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method public layoutSurface(IIII)V
    .locals 7

    if-gt p1, p3, :cond_0

    if-gt p2, p4, :cond_0

    new-instance v6, Landroid/media/tv/TvInputService$Session$18;

    move-object v0, v6

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Landroid/media/tv/TvInputService$Session$18;-><init>(Landroid/media/tv/TvInputService$Session;IIII)V

    invoke-direct {p0, v6}, Landroid/media/tv/TvInputService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid parameter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public notifyAdResponse(Landroid/media/tv/AdResponse;)V
    .locals 1

    new-instance v0, Landroid/media/tv/TvInputService$Session$13;

    invoke-direct {v0, p0, p1}, Landroid/media/tv/TvInputService$Session$13;-><init>(Landroid/media/tv/TvInputService$Session;Landroid/media/tv/AdResponse;)V

    invoke-direct {p0, v0}, Landroid/media/tv/TvInputService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public notifyAitInfoUpdated(Landroid/media/tv/AitInfo;)V
    .locals 1

    new-instance v0, Landroid/media/tv/TvInputService$Session$16;

    invoke-direct {v0, p0, p1}, Landroid/media/tv/TvInputService$Session$16;-><init>(Landroid/media/tv/TvInputService$Session;Landroid/media/tv/AitInfo;)V

    invoke-direct {p0, v0}, Landroid/media/tv/TvInputService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public notifyBroadcastInfoResponse(Landroid/media/tv/BroadcastInfoResponse;)V
    .locals 1

    new-instance v0, Landroid/media/tv/TvInputService$Session$12;

    invoke-direct {v0, p0, p1}, Landroid/media/tv/TvInputService$Session$12;-><init>(Landroid/media/tv/TvInputService$Session;Landroid/media/tv/BroadcastInfoResponse;)V

    invoke-direct {p0, v0}, Landroid/media/tv/TvInputService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public notifyChannelRetuned(Landroid/net/Uri;)V
    .locals 1

    new-instance v0, Landroid/media/tv/TvInputService$Session$3;

    invoke-direct {v0, p0, p1}, Landroid/media/tv/TvInputService$Session$3;-><init>(Landroid/media/tv/TvInputService$Session;Landroid/net/Uri;)V

    invoke-direct {p0, v0}, Landroid/media/tv/TvInputService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public notifyContentAllowed()V
    .locals 1

    new-instance v0, Landroid/media/tv/TvInputService$Session$9;

    invoke-direct {v0, p0}, Landroid/media/tv/TvInputService$Session$9;-><init>(Landroid/media/tv/TvInputService$Session;)V

    invoke-direct {p0, v0}, Landroid/media/tv/TvInputService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public notifyContentBlocked(Landroid/media/tv/TvContentRating;)V
    .locals 1

    invoke-static {p1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Landroid/media/tv/TvInputService$Session$10;

    invoke-direct {v0, p0, p1}, Landroid/media/tv/TvInputService$Session$10;-><init>(Landroid/media/tv/TvInputService$Session;Landroid/media/tv/TvContentRating;)V

    invoke-direct {p0, v0}, Landroid/media/tv/TvInputService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public notifySessionEvent(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    invoke-static {p1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Landroid/media/tv/TvInputService$Session$2;

    invoke-direct {v0, p0, p1, p2}, Landroid/media/tv/TvInputService$Session$2;-><init>(Landroid/media/tv/TvInputService$Session;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {p0, v0}, Landroid/media/tv/TvInputService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public notifySignalStrength(I)V
    .locals 1

    new-instance v0, Landroid/media/tv/TvInputService$Session$17;

    invoke-direct {v0, p0, p1}, Landroid/media/tv/TvInputService$Session$17;-><init>(Landroid/media/tv/TvInputService$Session;I)V

    invoke-direct {p0, v0}, Landroid/media/tv/TvInputService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public notifyTimeShiftStatusChanged(I)V
    .locals 1

    new-instance v0, Landroid/media/tv/TvInputService$Session$11;

    invoke-direct {v0, p0, p1}, Landroid/media/tv/TvInputService$Session$11;-><init>(Landroid/media/tv/TvInputService$Session;I)V

    invoke-direct {p0, v0}, Landroid/media/tv/TvInputService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public notifyTrackSelected(ILjava/lang/String;)V
    .locals 1

    new-instance v0, Landroid/media/tv/TvInputService$Session$6;

    invoke-direct {v0, p0, p1, p2}, Landroid/media/tv/TvInputService$Session$6;-><init>(Landroid/media/tv/TvInputService$Session;ILjava/lang/String;)V

    invoke-direct {p0, v0}, Landroid/media/tv/TvInputService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public notifyTracksChanged(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/media/tv/TvTrackInfo;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v1, Landroid/media/tv/TvInputService$Session$5;

    invoke-direct {v1, p0, v0}, Landroid/media/tv/TvInputService$Session$5;-><init>(Landroid/media/tv/TvInputService$Session;Ljava/util/List;)V

    invoke-direct {p0, v1}, Landroid/media/tv/TvInputService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public notifyTuned(Landroid/net/Uri;)V
    .locals 1

    new-instance v0, Landroid/media/tv/TvInputService$Session$4;

    invoke-direct {v0, p0, p1}, Landroid/media/tv/TvInputService$Session$4;-><init>(Landroid/media/tv/TvInputService$Session;Landroid/net/Uri;)V

    invoke-direct {p0, v0}, Landroid/media/tv/TvInputService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public notifyVideoAvailable()V
    .locals 1

    new-instance v0, Landroid/media/tv/TvInputService$Session$7;

    invoke-direct {v0, p0}, Landroid/media/tv/TvInputService$Session$7;-><init>(Landroid/media/tv/TvInputService$Session;)V

    invoke-direct {p0, v0}, Landroid/media/tv/TvInputService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public notifyVideoUnavailable(I)V
    .locals 2

    if-ltz p1, :cond_0

    const/16 v0, 0x12

    if-le p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "notifyVideoUnavailable - unknown reason: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TvInputService"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v0, Landroid/media/tv/TvInputService$Session$8;

    invoke-direct {v0, p0, p1}, Landroid/media/tv/TvInputService$Session$8;-><init>(Landroid/media/tv/TvInputService$Session;I)V

    invoke-direct {p0, v0}, Landroid/media/tv/TvInputService$Session;->executeOrPostRunnableOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onAppPrivateCommand(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public onCreateOverlayView()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onOverlayViewSizeChanged(II)V
    .locals 0

    return-void
.end method

.method public abstract onRelease()V
.end method

.method public onRemoveBroadcastInfo(I)V
    .locals 0

    return-void
.end method

.method public onRequestAd(Landroid/media/tv/AdRequest;)V
    .locals 0

    return-void
.end method

.method public onRequestBroadcastInfo(Landroid/media/tv/BroadcastInfoRequest;)V
    .locals 0

    return-void
.end method

.method public onSelectTrack(ILjava/lang/String;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public abstract onSetCaptionEnabled(Z)V
.end method

.method public onSetInteractiveAppNotificationEnabled(Z)V
    .locals 0

    return-void
.end method

.method public onSetMain(Z)V
    .locals 0
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    return-void
.end method

.method public abstract onSetStreamVolume(F)V
.end method

.method public abstract onSetSurface(Landroid/view/Surface;)Z
.end method

.method public onSurfaceChanged(III)V
    .locals 0

    return-void
.end method

.method public onTimeShiftGetCurrentPosition()J
    .locals 2

    const-wide/high16 v0, -0x8000000000000000L

    return-wide v0
.end method

.method public onTimeShiftGetStartPosition()J
    .locals 2

    const-wide/high16 v0, -0x8000000000000000L

    return-wide v0
.end method

.method public onTimeShiftPause()V
    .locals 0

    return-void
.end method

.method public onTimeShiftPlay(Landroid/net/Uri;)V
    .locals 0

    return-void
.end method

.method public onTimeShiftResume()V
    .locals 0

    return-void
.end method

.method public onTimeShiftSeekTo(J)V
    .locals 0

    return-void
.end method

.method public onTimeShiftSetPlaybackParams(Landroid/media/PlaybackParams;)V
    .locals 0

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public abstract onTune(Landroid/net/Uri;)Z
.end method

.method public onTune(Landroid/net/Uri;Landroid/os/Bundle;)Z
    .locals 1

    invoke-virtual {p0, p1}, Landroid/media/tv/TvInputService$Session;->onTune(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public onUnblockContent(Landroid/media/tv/TvContentRating;)V
    .locals 0

    return-void
.end method

.method relayoutOverlayView(Landroid/graphics/Rect;)V
    .locals 3

    goto/32 :goto_11

    nop

    :goto_0
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    goto/32 :goto_a

    nop

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_17

    :cond_0
    goto/32 :goto_f

    nop

    :goto_2
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    goto/32 :goto_14

    nop

    :goto_3
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    goto/32 :goto_2a

    nop

    :goto_4
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_18

    nop

    :goto_5
    iget v1, p1, Landroid/graphics/Rect;->top:I

    goto/32 :goto_0

    nop

    :goto_6
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    goto/32 :goto_2c

    nop

    :goto_7
    return-void

    :goto_8
    goto/32 :goto_2e

    nop

    :goto_9
    iget-object v0, p0, Landroid/media/tv/TvInputService$Session;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    goto/32 :goto_5

    nop

    :goto_a
    iget-object v0, p0, Landroid/media/tv/TvInputService$Session;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    goto/32 :goto_d

    nop

    :goto_b
    if-nez v0, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_24

    nop

    :goto_c
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    goto/32 :goto_2d

    nop

    :goto_d
    iget v1, p1, Landroid/graphics/Rect;->right:I

    goto/32 :goto_2b

    nop

    :goto_e
    iput-object p1, p0, Landroid/media/tv/TvInputService$Session;->mOverlayFrame:Landroid/graphics/Rect;

    goto/32 :goto_15

    nop

    :goto_f
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    goto/32 :goto_6

    nop

    :goto_10
    iget-object v0, p0, Landroid/media/tv/TvInputService$Session;->mOverlayFrame:Landroid/graphics/Rect;

    goto/32 :goto_2

    nop

    :goto_11
    iget-object v0, p0, Landroid/media/tv/TvInputService$Session;->mOverlayFrame:Landroid/graphics/Rect;

    goto/32 :goto_1

    nop

    :goto_12
    iget-object v0, p0, Landroid/media/tv/TvInputService$Session;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    goto/32 :goto_25

    nop

    :goto_13
    iget v1, p1, Landroid/graphics/Rect;->left:I

    goto/32 :goto_20

    nop

    :goto_14
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    goto/32 :goto_16

    nop

    :goto_15
    iget-boolean v0, p0, Landroid/media/tv/TvInputService$Session;->mOverlayViewEnabled:Z

    goto/32 :goto_b

    nop

    :goto_16
    if-ne v0, v1, :cond_2

    goto/32 :goto_28

    :cond_2
    :goto_17
    goto/32 :goto_22

    nop

    :goto_18
    iget v2, p1, Landroid/graphics/Rect;->top:I

    goto/32 :goto_1f

    nop

    :goto_19
    iget-object v1, p0, Landroid/media/tv/TvInputService$Session;->mOverlayViewContainer:Landroid/widget/FrameLayout;

    goto/32 :goto_29

    nop

    :goto_1a
    sub-int/2addr v1, v2

    goto/32 :goto_27

    nop

    :goto_1b
    goto :goto_8

    :goto_1c
    goto/32 :goto_12

    nop

    :goto_1d
    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/32 :goto_7

    nop

    :goto_1e
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_21

    nop

    :goto_1f
    sub-int/2addr v1, v2

    goto/32 :goto_3

    nop

    :goto_20
    sub-int/2addr v0, v1

    goto/32 :goto_1e

    nop

    :goto_21
    iget v2, p1, Landroid/graphics/Rect;->top:I

    goto/32 :goto_1a

    nop

    :goto_22
    iget v0, p1, Landroid/graphics/Rect;->right:I

    goto/32 :goto_13

    nop

    :goto_23
    sub-int/2addr v1, v2

    goto/32 :goto_c

    nop

    :goto_24
    iget-object v0, p0, Landroid/media/tv/TvInputService$Session;->mOverlayViewContainer:Landroid/widget/FrameLayout;

    goto/32 :goto_2f

    nop

    :goto_25
    iget v1, p1, Landroid/graphics/Rect;->left:I

    goto/32 :goto_26

    nop

    :goto_26
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    goto/32 :goto_9

    nop

    :goto_27
    invoke-virtual {p0, v0, v1}, Landroid/media/tv/TvInputService$Session;->onOverlayViewSizeChanged(II)V

    :goto_28
    goto/32 :goto_e

    nop

    :goto_29
    iget-object v2, p0, Landroid/media/tv/TvInputService$Session;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    goto/32 :goto_1d

    nop

    :goto_2a
    iget-object v0, p0, Landroid/media/tv/TvInputService$Session;->mWindowManager:Landroid/view/WindowManager;

    goto/32 :goto_19

    nop

    :goto_2b
    iget v2, p1, Landroid/graphics/Rect;->left:I

    goto/32 :goto_23

    nop

    :goto_2c
    if-eq v0, v1, :cond_3

    goto/32 :goto_17

    :cond_3
    goto/32 :goto_10

    nop

    :goto_2d
    iget-object v0, p0, Landroid/media/tv/TvInputService$Session;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    goto/32 :goto_4

    nop

    :goto_2e
    return-void

    :goto_2f
    if-eqz v0, :cond_4

    goto/32 :goto_1c

    :cond_4
    goto/32 :goto_1b

    nop
.end method

.method release()V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0}, Landroid/media/tv/TvInputService$Session;->onRelease()V

    goto/32 :goto_7

    nop

    :goto_1
    iget-object v1, p0, Landroid/media/tv/TvInputService$Session;->mTimeShiftPositionTrackingRunnable:Landroid/media/tv/TvInputService$Session$TimeShiftPositionTrackingRunnable;

    goto/32 :goto_a

    nop

    :goto_2
    invoke-virtual {p0, v0}, Landroid/media/tv/TvInputService$Session;->removeOverlayView(Z)V

    goto/32 :goto_8

    nop

    :goto_3
    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    goto/32 :goto_d

    nop

    :goto_4
    throw v1

    :goto_5
    monitor-enter v0

    :try_start_0
    iput-object v1, p0, Landroid/media/tv/TvInputService$Session;->mSessionCallback:Landroid/media/tv/ITvInputSessionCallback;

    iget-object v1, p0, Landroid/media/tv/TvInputService$Session;->mPendingActions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_b

    nop

    :goto_6
    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/32 :goto_4

    nop

    :goto_7
    iget-object v0, p0, Landroid/media/tv/TvInputService$Session;->mSurface:Landroid/view/Surface;

    goto/32 :goto_c

    nop

    :goto_8
    iget-object v0, p0, Landroid/media/tv/TvInputService$Session;->mHandler:Landroid/os/Handler;

    goto/32 :goto_1

    nop

    :goto_9
    if-nez v0, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_3

    nop

    :goto_a
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto/32 :goto_6

    nop

    :goto_b
    const/4 v0, 0x1

    goto/32 :goto_2

    nop

    :goto_c
    const/4 v1, 0x0

    goto/32 :goto_9

    nop

    :goto_d
    iput-object v1, p0, Landroid/media/tv/TvInputService$Session;->mSurface:Landroid/view/Surface;

    :goto_e
    goto/32 :goto_f

    nop

    :goto_f
    iget-object v0, p0, Landroid/media/tv/TvInputService$Session;->mLock:Ljava/lang/Object;

    goto/32 :goto_5

    nop
.end method

.method removeBroadcastInfo(I)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0, p1}, Landroid/media/tv/TvInputService$Session;->onRemoveBroadcastInfo(I)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method removeOverlayView(Z)V
    .locals 3

    goto/32 :goto_a

    nop

    :goto_0
    iget-object v2, p0, Landroid/media/tv/TvInputService$Session;->mOverlayViewContainer:Landroid/widget/FrameLayout;

    goto/32 :goto_9

    nop

    :goto_1
    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    goto/32 :goto_5

    nop

    :goto_2
    iput-object v0, p0, Landroid/media/tv/TvInputService$Session;->mOverlayFrame:Landroid/graphics/Rect;

    :goto_3
    goto/32 :goto_d

    nop

    :goto_4
    iput-object v0, p0, Landroid/media/tv/TvInputService$Session;->mOverlayViewContainer:Landroid/widget/FrameLayout;

    goto/32 :goto_6

    nop

    :goto_5
    iput-object v0, p0, Landroid/media/tv/TvInputService$Session;->mOverlayView:Landroid/view/View;

    goto/32 :goto_10

    nop

    :goto_6
    iput-object v0, p0, Landroid/media/tv/TvInputService$Session;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    :goto_7
    goto/32 :goto_f

    nop

    :goto_8
    if-nez p1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_b

    nop

    :goto_9
    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    goto/32 :goto_4

    nop

    :goto_a
    const/4 v0, 0x0

    goto/32 :goto_8

    nop

    :goto_b
    iput-object v0, p0, Landroid/media/tv/TvInputService$Session;->mWindowToken:Landroid/os/IBinder;

    goto/32 :goto_2

    nop

    :goto_c
    if-nez v1, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_e

    nop

    :goto_d
    iget-object v1, p0, Landroid/media/tv/TvInputService$Session;->mOverlayViewContainer:Landroid/widget/FrameLayout;

    goto/32 :goto_c

    nop

    :goto_e
    iget-object v2, p0, Landroid/media/tv/TvInputService$Session;->mOverlayView:Landroid/view/View;

    goto/32 :goto_1

    nop

    :goto_f
    return-void

    :goto_10
    iget-object v1, p0, Landroid/media/tv/TvInputService$Session;->mWindowManager:Landroid/view/WindowManager;

    goto/32 :goto_0

    nop
.end method

.method requestAd(Landroid/media/tv/AdRequest;)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0, p1}, Landroid/media/tv/TvInputService$Session;->onRequestAd(Landroid/media/tv/AdRequest;)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method requestBroadcastInfo(Landroid/media/tv/BroadcastInfoRequest;)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0, p1}, Landroid/media/tv/TvInputService$Session;->onRequestBroadcastInfo(Landroid/media/tv/BroadcastInfoRequest;)V

    goto/32 :goto_0

    nop
.end method

.method scheduleOverlayViewCleanup()V
    .locals 5

    goto/32 :goto_4

    nop

    :goto_0
    const/4 v3, 0x1

    goto/32 :goto_c

    nop

    :goto_1
    aput-object v0, v3, v4

    goto/32 :goto_6

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_8

    nop

    :goto_3
    iput-object v1, p0, Landroid/media/tv/TvInputService$Session;->mOverlayViewCleanUpTask:Landroid/media/tv/TvInputService$OverlayViewCleanUpTask;

    goto/32 :goto_b

    nop

    :goto_4
    iget-object v0, p0, Landroid/media/tv/TvInputService$Session;->mOverlayViewContainer:Landroid/widget/FrameLayout;

    goto/32 :goto_2

    nop

    :goto_5
    const/4 v4, 0x0

    goto/32 :goto_1

    nop

    :goto_6
    invoke-virtual {v1, v2, v3}, Landroid/media/tv/TvInputService$OverlayViewCleanUpTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_7
    goto/32 :goto_a

    nop

    :goto_8
    new-instance v1, Landroid/media/tv/TvInputService$OverlayViewCleanUpTask;

    goto/32 :goto_d

    nop

    :goto_9
    invoke-direct {v1, v2}, Landroid/media/tv/TvInputService$OverlayViewCleanUpTask;-><init>(Landroid/media/tv/TvInputService$OverlayViewCleanUpTask-IA;)V

    goto/32 :goto_3

    nop

    :goto_a
    return-void

    :goto_b
    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    goto/32 :goto_0

    nop

    :goto_c
    new-array v3, v3, [Landroid/view/View;

    goto/32 :goto_5

    nop

    :goto_d
    const/4 v2, 0x0

    goto/32 :goto_9

    nop
.end method

.method selectTrack(ILjava/lang/String;)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0, p1, p2}, Landroid/media/tv/TvInputService$Session;->onSelectTrack(ILjava/lang/String;)Z

    goto/32 :goto_0

    nop
.end method

.method setCaptionEnabled(Z)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0, p1}, Landroid/media/tv/TvInputService$Session;->onSetCaptionEnabled(Z)V

    goto/32 :goto_0

    nop
.end method

.method setInteractiveAppNotificationEnabled(Z)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0, p1}, Landroid/media/tv/TvInputService$Session;->onSetInteractiveAppNotificationEnabled(Z)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method setMain(Z)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0, p1}, Landroid/media/tv/TvInputService$Session;->onSetMain(Z)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method public setOverlayViewEnabled(Z)V
    .locals 2

    iget-object v0, p0, Landroid/media/tv/TvInputService$Session;->mHandler:Landroid/os/Handler;

    new-instance v1, Landroid/media/tv/TvInputService$Session$1;

    invoke-direct {v1, p0, p1}, Landroid/media/tv/TvInputService$Session$1;-><init>(Landroid/media/tv/TvInputService$Session;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method setStreamVolume(F)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0, p1}, Landroid/media/tv/TvInputService$Session;->onSetStreamVolume(F)V

    goto/32 :goto_0

    nop
.end method

.method setSurface(Landroid/view/Surface;)V
    .locals 1

    goto/32 :goto_5

    nop

    :goto_0
    return-void

    :goto_1
    iput-object p1, p0, Landroid/media/tv/TvInputService$Session;->mSurface:Landroid/view/Surface;

    goto/32 :goto_0

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    :goto_4
    goto/32 :goto_1

    nop

    :goto_5
    invoke-virtual {p0, p1}, Landroid/media/tv/TvInputService$Session;->onSetSurface(Landroid/view/Surface;)Z

    goto/32 :goto_6

    nop

    :goto_6
    iget-object v0, p0, Landroid/media/tv/TvInputService$Session;->mSurface:Landroid/view/Surface;

    goto/32 :goto_2

    nop
.end method

.method timeShiftEnablePositionTracking(Z)V
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    iput-wide v0, p0, Landroid/media/tv/TvInputService$Session;->mCurrentPositionMs:J

    :goto_1
    goto/32 :goto_d

    nop

    :goto_2
    iget-object v1, p0, Landroid/media/tv/TvInputService$Session;->mTimeShiftPositionTrackingRunnable:Landroid/media/tv/TvInputService$Session$TimeShiftPositionTrackingRunnable;

    goto/32 :goto_6

    nop

    :goto_3
    if-nez p1, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_b

    nop

    :goto_4
    iput-wide v0, p0, Landroid/media/tv/TvInputService$Session;->mStartPositionMs:J

    goto/32 :goto_0

    nop

    :goto_5
    const-wide/high16 v0, -0x8000000000000000L

    goto/32 :goto_4

    nop

    :goto_6
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_7

    nop

    :goto_7
    goto :goto_1

    :goto_8
    goto/32 :goto_c

    nop

    :goto_9
    iget-object v1, p0, Landroid/media/tv/TvInputService$Session;->mTimeShiftPositionTrackingRunnable:Landroid/media/tv/TvInputService$Session$TimeShiftPositionTrackingRunnable;

    goto/32 :goto_a

    nop

    :goto_a
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto/32 :goto_5

    nop

    :goto_b
    iget-object v0, p0, Landroid/media/tv/TvInputService$Session;->mHandler:Landroid/os/Handler;

    goto/32 :goto_2

    nop

    :goto_c
    iget-object v0, p0, Landroid/media/tv/TvInputService$Session;->mHandler:Landroid/os/Handler;

    goto/32 :goto_9

    nop

    :goto_d
    return-void
.end method

.method timeShiftPause()V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0}, Landroid/media/tv/TvInputService$Session;->onTimeShiftPause()V

    goto/32 :goto_0

    nop
.end method

.method timeShiftPlay(Landroid/net/Uri;)V
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    iput-wide v0, p0, Landroid/media/tv/TvInputService$Session;->mCurrentPositionMs:J

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {p0, p1}, Landroid/media/tv/TvInputService$Session;->onTimeShiftPlay(Landroid/net/Uri;)V

    goto/32 :goto_2

    nop

    :goto_2
    return-void

    :goto_3
    const-wide/16 v0, 0x0

    goto/32 :goto_0

    nop
.end method

.method timeShiftResume()V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0}, Landroid/media/tv/TvInputService$Session;->onTimeShiftResume()V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method timeShiftSeekTo(J)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0, p1, p2}, Landroid/media/tv/TvInputService$Session;->onTimeShiftSeekTo(J)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method timeShiftSetPlaybackParams(Landroid/media/PlaybackParams;)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0, p1}, Landroid/media/tv/TvInputService$Session;->onTimeShiftSetPlaybackParams(Landroid/media/PlaybackParams;)V

    goto/32 :goto_0

    nop
.end method

.method tune(Landroid/net/Uri;Landroid/os/Bundle;)V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    const-wide/high16 v0, -0x8000000000000000L

    goto/32 :goto_2

    nop

    :goto_2
    iput-wide v0, p0, Landroid/media/tv/TvInputService$Session;->mCurrentPositionMs:J

    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {p0, p1, p2}, Landroid/media/tv/TvInputService$Session;->onTune(Landroid/net/Uri;Landroid/os/Bundle;)Z

    goto/32 :goto_0

    nop
.end method

.method unblockContent(Ljava/lang/String;)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {p0, v0}, Landroid/media/tv/TvInputService$Session;->onUnblockContent(Landroid/media/tv/TvContentRating;)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    invoke-static {p1}, Landroid/media/tv/TvContentRating;->unflattenFromString(Ljava/lang/String;)Landroid/media/tv/TvContentRating;

    move-result-object v0

    goto/32 :goto_0

    nop
.end method
