.class public interface abstract annotation Landroid/media/VolumeShaperOperationFlag;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final CREATE_IF_NECESSARY:I = 0x4

.field public static final DELAY:I = 0x3

.field public static final JOIN:I = 0x2

.field public static final REVERSE:I = 0x0

.field public static final TERMINATE:I = 0x1
