.class public Landroid/media/effect/EffectContext;
.super Ljava/lang/Object;


# instance fields
.field private final GL_STATE_ARRAYBUFFER:I

.field private final GL_STATE_COUNT:I

.field private final GL_STATE_FBO:I

.field private final GL_STATE_PROGRAM:I

.field private mFactory:Landroid/media/effect/EffectFactory;

.field mFilterContext:Landroid/filterfw/core/FilterContext;

.field private mOldState:[I


# direct methods
.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Landroid/media/effect/EffectContext;->GL_STATE_FBO:I

    const/4 v0, 0x1

    iput v0, p0, Landroid/media/effect/EffectContext;->GL_STATE_PROGRAM:I

    const/4 v0, 0x2

    iput v0, p0, Landroid/media/effect/EffectContext;->GL_STATE_ARRAYBUFFER:I

    const/4 v0, 0x3

    iput v0, p0, Landroid/media/effect/EffectContext;->GL_STATE_COUNT:I

    new-array v0, v0, [I

    iput-object v0, p0, Landroid/media/effect/EffectContext;->mOldState:[I

    new-instance v0, Landroid/filterfw/core/FilterContext;

    invoke-direct {v0}, Landroid/filterfw/core/FilterContext;-><init>()V

    iput-object v0, p0, Landroid/media/effect/EffectContext;->mFilterContext:Landroid/filterfw/core/FilterContext;

    new-instance v1, Landroid/filterfw/core/CachedFrameManager;

    invoke-direct {v1}, Landroid/filterfw/core/CachedFrameManager;-><init>()V

    invoke-virtual {v0, v1}, Landroid/filterfw/core/FilterContext;->setFrameManager(Landroid/filterfw/core/FrameManager;)V

    new-instance v0, Landroid/media/effect/EffectFactory;

    invoke-direct {v0, p0}, Landroid/media/effect/EffectFactory;-><init>(Landroid/media/effect/EffectContext;)V

    iput-object v0, p0, Landroid/media/effect/EffectContext;->mFactory:Landroid/media/effect/EffectFactory;

    return-void
.end method

.method public static createWithCurrentGlContext()Landroid/media/effect/EffectContext;
    .locals 1

    new-instance v0, Landroid/media/effect/EffectContext;

    invoke-direct {v0}, Landroid/media/effect/EffectContext;-><init>()V

    invoke-direct {v0}, Landroid/media/effect/EffectContext;->initInCurrentGlContext()V

    return-object v0
.end method

.method private initInCurrentGlContext()V
    .locals 2

    invoke-static {}, Landroid/filterfw/core/GLEnvironment;->isAnyContextActive()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/filterfw/core/GLEnvironment;

    invoke-direct {v0}, Landroid/filterfw/core/GLEnvironment;-><init>()V

    invoke-virtual {v0}, Landroid/filterfw/core/GLEnvironment;->initWithCurrentContext()V

    iget-object v1, p0, Landroid/media/effect/EffectContext;->mFilterContext:Landroid/filterfw/core/FilterContext;

    invoke-virtual {v1, v0}, Landroid/filterfw/core/FilterContext;->initGLEnvironment(Landroid/filterfw/core/GLEnvironment;)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempting to initialize EffectContext with no active GL context!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method final assertValidGLState()V
    .locals 3

    goto/32 :goto_12

    nop

    :goto_0
    throw v1

    :goto_1
    goto/32 :goto_2

    nop

    :goto_2
    new-instance v1, Ljava/lang/RuntimeException;

    goto/32 :goto_3

    nop

    :goto_3
    const-string v2, "Attempting to apply effect without valid GL context!"

    goto/32 :goto_f

    nop

    :goto_4
    throw v1

    :goto_5
    return-void

    :goto_6
    goto/32 :goto_d

    nop

    :goto_7
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_0

    nop

    :goto_8
    new-instance v1, Ljava/lang/RuntimeException;

    goto/32 :goto_c

    nop

    :goto_9
    invoke-virtual {v0}, Landroid/filterfw/core/FilterContext;->getGLEnvironment()Landroid/filterfw/core/GLEnvironment;

    move-result-object v0

    goto/32 :goto_e

    nop

    :goto_a
    goto :goto_6

    :goto_b
    goto/32 :goto_5

    nop

    :goto_c
    const-string v2, "Applying effect in wrong GL context!"

    goto/32 :goto_7

    nop

    :goto_d
    invoke-static {}, Landroid/filterfw/core/GLEnvironment;->isAnyContextActive()Z

    move-result v1

    goto/32 :goto_10

    nop

    :goto_e
    if-nez v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_13

    nop

    :goto_f
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_4

    nop

    :goto_10
    if-nez v1, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_8

    nop

    :goto_11
    if-eqz v1, :cond_2

    goto/32 :goto_b

    :cond_2
    goto/32 :goto_a

    nop

    :goto_12
    iget-object v0, p0, Landroid/media/effect/EffectContext;->mFilterContext:Landroid/filterfw/core/FilterContext;

    goto/32 :goto_9

    nop

    :goto_13
    invoke-virtual {v0}, Landroid/filterfw/core/GLEnvironment;->isContextActive()Z

    move-result v1

    goto/32 :goto_11

    nop
.end method

.method public getFactory()Landroid/media/effect/EffectFactory;
    .locals 1

    iget-object v0, p0, Landroid/media/effect/EffectContext;->mFactory:Landroid/media/effect/EffectFactory;

    return-object v0
.end method

.method public release()V
    .locals 1

    iget-object v0, p0, Landroid/media/effect/EffectContext;->mFilterContext:Landroid/filterfw/core/FilterContext;

    invoke-virtual {v0}, Landroid/filterfw/core/FilterContext;->tearDown()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/media/effect/EffectContext;->mFilterContext:Landroid/filterfw/core/FilterContext;

    return-void
.end method

.method final restoreGLState()V
    .locals 2

    goto/32 :goto_d

    nop

    :goto_0
    aget v0, v0, v1

    goto/32 :goto_b

    nop

    :goto_1
    invoke-static {v1, v0}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    goto/32 :goto_7

    nop

    :goto_2
    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    goto/32 :goto_9

    nop

    :goto_3
    const/4 v1, 0x1

    goto/32 :goto_6

    nop

    :goto_4
    const/4 v1, 0x2

    goto/32 :goto_0

    nop

    :goto_5
    const v1, 0x8d40

    goto/32 :goto_a

    nop

    :goto_6
    aget v0, v0, v1

    goto/32 :goto_2

    nop

    :goto_7
    return-void

    :goto_8
    const/4 v1, 0x0

    goto/32 :goto_e

    nop

    :goto_9
    iget-object v0, p0, Landroid/media/effect/EffectContext;->mOldState:[I

    goto/32 :goto_4

    nop

    :goto_a
    invoke-static {v1, v0}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    goto/32 :goto_c

    nop

    :goto_b
    const v1, 0x8892

    goto/32 :goto_1

    nop

    :goto_c
    iget-object v0, p0, Landroid/media/effect/EffectContext;->mOldState:[I

    goto/32 :goto_3

    nop

    :goto_d
    iget-object v0, p0, Landroid/media/effect/EffectContext;->mOldState:[I

    goto/32 :goto_8

    nop

    :goto_e
    aget v0, v0, v1

    goto/32 :goto_5

    nop
.end method

.method final saveGLState()V
    .locals 3

    goto/32 :goto_2

    nop

    :goto_0
    invoke-static {v1, v0, v2}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    goto/32 :goto_7

    nop

    :goto_1
    iget-object v0, p0, Landroid/media/effect/EffectContext;->mOldState:[I

    goto/32 :goto_5

    nop

    :goto_2
    iget-object v0, p0, Landroid/media/effect/EffectContext;->mOldState:[I

    goto/32 :goto_b

    nop

    :goto_3
    iget-object v0, p0, Landroid/media/effect/EffectContext;->mOldState:[I

    goto/32 :goto_a

    nop

    :goto_4
    invoke-static {v1, v0, v2}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    goto/32 :goto_1

    nop

    :goto_5
    const v1, 0x8b8d

    goto/32 :goto_8

    nop

    :goto_6
    const/4 v2, 0x0

    goto/32 :goto_4

    nop

    :goto_7
    return-void

    :goto_8
    const/4 v2, 0x1

    goto/32 :goto_9

    nop

    :goto_9
    invoke-static {v1, v0, v2}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    goto/32 :goto_3

    nop

    :goto_a
    const v1, 0x8894

    goto/32 :goto_c

    nop

    :goto_b
    const v1, 0x8ca6

    goto/32 :goto_6

    nop

    :goto_c
    const/4 v2, 0x2

    goto/32 :goto_0

    nop
.end method
