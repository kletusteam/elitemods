.class Landroid/media/MediaRouter$Static;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/hardware/display/DisplayManager$DisplayListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/MediaRouter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Static"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/MediaRouter$Static$Client;
    }
.end annotation


# instance fields
.field mActivelyScanningWifiDisplays:Z

.field final mAudioRoutesObserver:Landroid/media/IAudioRoutesObserver$Stub;

.field final mAudioService:Landroid/media/IAudioService;

.field mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

.field final mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Landroid/media/MediaRouter$CallbackInfo;",
            ">;"
        }
    .end annotation
.end field

.field final mCanConfigureWifiDisplays:Z

.field final mCategories:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/media/MediaRouter$RouteCategory;",
            ">;"
        }
    .end annotation
.end field

.field mClient:Landroid/media/IMediaRouterClient;

.field mClientState:Landroid/media/MediaRouterClientState;

.field final mCurAudioRoutesInfo:Landroid/media/AudioRoutesInfo;

.field mCurrentUserId:I

.field mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

.field mDiscoverRequestActiveScan:Z

.field mDiscoveryRequestRouteTypes:I

.field final mDisplayService:Landroid/hardware/display/DisplayManager;

.field final mHandler:Landroid/os/Handler;

.field mIsBluetoothA2dpOn:Z

.field final mMediaRouterService:Landroid/media/IMediaRouterService;

.field final mPackageName:Ljava/lang/String;

.field mPreviousActiveWifiDisplayAddress:Ljava/lang/String;

.field final mResources:Landroid/content/res/Resources;

.field final mRoutes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/media/MediaRouter$RouteInfo;",
            ">;"
        }
    .end annotation
.end field

.field mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

.field mStreamVolume:Landroid/util/SparseIntArray;

.field final mSystemCategory:Landroid/media/MediaRouter$RouteCategory;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Landroid/media/MediaRouter$Static;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/media/MediaRouter$Static;->mCategories:Ljava/util/ArrayList;

    new-instance v0, Landroid/media/AudioRoutesInfo;

    invoke-direct {v0}, Landroid/media/AudioRoutesInfo;-><init>()V

    iput-object v0, p0, Landroid/media/MediaRouter$Static;->mCurAudioRoutesInfo:Landroid/media/AudioRoutesInfo;

    const/4 v0, -0x1

    iput v0, p0, Landroid/media/MediaRouter$Static;->mCurrentUserId:I

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Landroid/media/MediaRouter$Static;->mStreamVolume:Landroid/util/SparseIntArray;

    new-instance v0, Landroid/media/MediaRouter$Static$1;

    invoke-direct {v0, p0}, Landroid/media/MediaRouter$Static$1;-><init>(Landroid/media/MediaRouter$Static;)V

    iput-object v0, p0, Landroid/media/MediaRouter$Static;->mAudioRoutesObserver:Landroid/media/IAudioRoutesObserver$Stub;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/media/MediaRouter$Static;->mPackageName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Landroid/media/MediaRouter$Static;->mResources:Landroid/content/res/Resources;

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Landroid/media/MediaRouter$Static;->mHandler:Landroid/os/Handler;

    const-string v0, "audio"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/media/IAudioService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IAudioService;

    move-result-object v1

    iput-object v1, p0, Landroid/media/MediaRouter$Static;->mAudioService:Landroid/media/IAudioService;

    const-string v1, "display"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/display/DisplayManager;

    iput-object v1, p0, Landroid/media/MediaRouter$Static;->mDisplayService:Landroid/hardware/display/DisplayManager;

    nop

    const-string v1, "media_router"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Landroid/media/IMediaRouterService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IMediaRouterService;

    move-result-object v1

    iput-object v1, p0, Landroid/media/MediaRouter$Static;->mMediaRouterService:Landroid/media/IMediaRouterService;

    new-instance v1, Landroid/media/MediaRouter$RouteCategory;

    const v2, 0x1040324

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Landroid/media/MediaRouter$RouteCategory;-><init>(IIZ)V

    iput-object v1, p0, Landroid/media/MediaRouter$Static;->mSystemCategory:Landroid/media/MediaRouter$RouteCategory;

    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/media/MediaRouter$RouteCategory;->mIsSystem:Z

    nop

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    const-string v5, "android.permission.CONFIGURE_WIFI_DISPLAY"

    invoke-virtual {p1, v5, v1, v3}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    move-result v1

    if-nez v1, :cond_0

    move v4, v2

    :cond_0
    iput-boolean v4, p0, Landroid/media/MediaRouter$Static;->mCanConfigureWifiDisplays:Z

    return-void
.end method

.method private updatePresentationDisplays(I)V
    .locals 4

    iget-object v0, p0, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    iget-object v2, p0, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v2}, Landroid/media/MediaRouter$RouteInfo;->updatePresentationDisplay()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v2, Landroid/media/MediaRouter$RouteInfo;->mPresentationDisplay:Landroid/view/Display;

    if-eqz v3, :cond_1

    iget-object v3, v2, Landroid/media/MediaRouter$RouteInfo;->mPresentationDisplay:Landroid/view/Display;

    invoke-virtual {v3}, Landroid/view/Display;->getDisplayId()I

    move-result v3

    if-ne v3, p1, :cond_1

    :cond_0
    invoke-static {v2}, Landroid/media/MediaRouter;->dispatchRoutePresentationDisplayChanged(Landroid/media/MediaRouter$RouteInfo;)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method findGlobalRoute(Ljava/lang/String;)Landroid/media/MediaRouter$RouteInfo;
    .locals 4

    goto/32 :goto_11

    nop

    :goto_0
    if-nez v3, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_9

    nop

    :goto_1
    iget-object v2, p0, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    goto/32 :goto_e

    nop

    :goto_2
    iget-object v3, v2, Landroid/media/MediaRouter$RouteInfo;->mGlobalRouteId:Ljava/lang/String;

    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    goto/32 :goto_0

    nop

    :goto_4
    check-cast v2, Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_2

    nop

    :goto_5
    return-object v1

    :goto_6
    const/4 v1, 0x0

    :goto_7
    goto/32 :goto_b

    nop

    :goto_8
    const/4 v1, 0x0

    goto/32 :goto_5

    nop

    :goto_9
    return-object v2

    :goto_a
    goto/32 :goto_d

    nop

    :goto_b
    if-lt v1, v0, :cond_1

    goto/32 :goto_10

    :cond_1
    goto/32 :goto_1

    nop

    :goto_c
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_6

    nop

    :goto_d
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_f

    nop

    :goto_e
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_4

    nop

    :goto_f
    goto :goto_7

    :goto_10
    goto/32 :goto_8

    nop

    :goto_11
    iget-object v0, p0, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    goto/32 :goto_c

    nop
.end method

.method public getAllPresentationDisplays()[Landroid/view/Display;
    .locals 3

    :try_start_0
    iget-object v0, p0, Landroid/media/MediaRouter$Static;->mDisplayService:Landroid/hardware/display/DisplayManager;

    const-string v1, "android.hardware.display.category.PRESENTATION"

    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->getDisplays(Ljava/lang/String;)[Landroid/view/Display;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "MediaRouter"

    const-string v2, "Unable to get displays."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x0

    return-object v1
.end method

.method getStreamVolume(I)I
    .locals 5

    goto/32 :goto_7

    nop

    :goto_0
    iget-object v1, p0, Landroid/media/MediaRouter$Static;->mStreamVolume:Landroid/util/SparseIntArray;

    goto/32 :goto_3

    nop

    :goto_1
    return v1

    :goto_2
    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Landroid/media/MediaRouter$Static;->mAudioService:Landroid/media/IAudioService;

    invoke-interface {v2, p1}, Landroid/media/IAudioService;->getStreamVolume(I)I

    move-result v2

    move v1, v2

    iget-object v2, p0, Landroid/media/MediaRouter$Static;->mStreamVolume:Landroid/util/SparseIntArray;

    invoke-virtual {v2, p1, v1}, Landroid/util/SparseIntArray;->put(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_b

    nop

    :goto_3
    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v1

    goto/32 :goto_1

    nop

    :goto_4
    return v1

    :goto_5
    goto/32 :goto_0

    nop

    :goto_6
    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    move-result v0

    goto/32 :goto_a

    nop

    :goto_7
    iget-object v0, p0, Landroid/media/MediaRouter$Static;->mStreamVolume:Landroid/util/SparseIntArray;

    goto/32 :goto_6

    nop

    :goto_8
    goto :goto_9

    :catch_0
    move-exception v2

    :try_start_1
    const-string v3, "MediaRouter"

    const-string v4, "Error getting local stream volume"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    nop

    :goto_9
    goto/32 :goto_4

    nop

    :goto_a
    if-ltz v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_2

    nop

    :goto_b
    return v1

    :catchall_0
    move-exception v2

    goto/32 :goto_8

    nop
.end method

.method handleGroupRouteSelected(Ljava/lang/String;)V
    .locals 5

    goto/32 :goto_17

    nop

    :goto_0
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_d

    nop

    :goto_1
    return-void

    :goto_2
    iget v2, v0, Landroid/media/MediaRouter$RouteInfo;->mSupportedTypes:I

    goto/32 :goto_8

    nop

    :goto_3
    move-object v0, v3

    :goto_4
    goto/32 :goto_10

    nop

    :goto_5
    iget-object v2, p0, Landroid/media/MediaRouter$Static;->mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_1c

    nop

    :goto_6
    const/4 v2, 0x0

    :goto_7
    goto/32 :goto_c

    nop

    :goto_8
    const/4 v3, 0x0

    goto/32 :goto_9

    nop

    :goto_9
    invoke-static {v2, v0, v3}, Landroid/media/MediaRouter;->selectRouteStatic(ILandroid/media/MediaRouter$RouteInfo;Z)V

    :goto_a
    goto/32 :goto_1

    nop

    :goto_b
    iget-object v1, p0, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    goto/32 :goto_11

    nop

    :goto_c
    if-lt v2, v1, :cond_0

    goto/32 :goto_13

    :cond_0
    goto/32 :goto_1b

    nop

    :goto_d
    check-cast v3, Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_f

    nop

    :goto_e
    if-nez v0, :cond_1

    goto/32 :goto_19

    :cond_1
    goto/32 :goto_1d

    nop

    :goto_f
    iget-object v4, v3, Landroid/media/MediaRouter$RouteInfo;->mGlobalRouteId:Ljava/lang/String;

    goto/32 :goto_16

    nop

    :goto_10
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_12

    nop

    :goto_11
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_6

    nop

    :goto_12
    goto :goto_7

    :goto_13
    goto/32 :goto_5

    nop

    :goto_14
    iget-object v0, p0, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    :goto_15
    goto/32 :goto_b

    nop

    :goto_16
    invoke-static {v4, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    goto/32 :goto_1a

    nop

    :goto_17
    invoke-virtual {p0}, Landroid/media/MediaRouter$Static;->isBluetoothA2dpOn()Z

    move-result v0

    goto/32 :goto_e

    nop

    :goto_18
    goto :goto_15

    :goto_19
    goto/32 :goto_14

    nop

    :goto_1a
    if-nez v4, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_3

    nop

    :goto_1b
    iget-object v3, p0, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    goto/32 :goto_0

    nop

    :goto_1c
    if-ne v0, v2, :cond_3

    goto/32 :goto_a

    :cond_3
    goto/32 :goto_2

    nop

    :goto_1d
    iget-object v0, p0, Landroid/media/MediaRouter$Static;->mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_18

    nop
.end method

.method isBluetoothA2dpOn()Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/media/MediaRouter$Static;->mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_2

    nop

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_8

    nop

    :goto_2
    if-nez v0, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_3

    nop

    :goto_3
    iget-boolean v0, p0, Landroid/media/MediaRouter$Static;->mIsBluetoothA2dpOn:Z

    goto/32 :goto_1

    nop

    :goto_4
    const/4 v0, 0x0

    :goto_5
    goto/32 :goto_9

    nop

    :goto_6
    goto :goto_5

    :goto_7
    goto/32 :goto_4

    nop

    :goto_8
    const/4 v0, 0x1

    goto/32 :goto_6

    nop

    :goto_9
    return v0
.end method

.method isPlaybackActive()Z
    .locals 3

    goto/32 :goto_7

    nop

    :goto_0
    const-string v1, "MediaRouter"

    goto/32 :goto_3

    nop

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_2

    nop

    :goto_2
    return v0

    :goto_3
    const-string v2, "Unable to retrieve playback active state."

    goto/32 :goto_4

    nop

    :goto_4
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_5
    goto/32 :goto_1

    nop

    :goto_6
    return v0

    :catch_0
    move-exception v0

    goto/32 :goto_0

    nop

    :goto_7
    iget-object v0, p0, Landroid/media/MediaRouter$Static;->mClient:Landroid/media/IMediaRouterClient;

    goto/32 :goto_8

    nop

    :goto_8
    if-nez v0, :cond_0

    goto/32 :goto_5

    :cond_0
    :try_start_0
    iget-object v1, p0, Landroid/media/MediaRouter$Static;->mMediaRouterService:Landroid/media/IMediaRouterService;

    invoke-interface {v1, v0}, Landroid/media/IMediaRouterService;->isPlaybackActive(Landroid/media/IMediaRouterClient;)Z

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_6

    nop
.end method

.method makeGlobalRoute(Landroid/media/MediaRouterClientState$RouteInfo;)Landroid/media/MediaRouter$RouteInfo;
    .locals 2

    goto/32 :goto_c

    nop

    :goto_0
    iget v1, p1, Landroid/media/MediaRouterClientState$RouteInfo;->volumeHandling:I

    goto/32 :goto_3

    nop

    :goto_1
    iput v1, v0, Landroid/media/MediaRouter$RouteInfo;->mVolumeMax:I

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v1, p1, Landroid/media/MediaRouterClientState$RouteInfo;->description:Ljava/lang/String;

    goto/32 :goto_f

    nop

    :goto_3
    iput v1, v0, Landroid/media/MediaRouter$RouteInfo;->mVolumeHandling:I

    goto/32 :goto_e

    nop

    :goto_4
    iget-boolean v1, p1, Landroid/media/MediaRouterClientState$RouteInfo;->enabled:Z

    goto/32 :goto_11

    nop

    :goto_5
    return-object v0

    :goto_6
    iget v1, p1, Landroid/media/MediaRouterClientState$RouteInfo;->volume:I

    goto/32 :goto_9

    nop

    :goto_7
    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->updatePresentationDisplay()Z

    goto/32 :goto_5

    nop

    :goto_8
    iget v1, p1, Landroid/media/MediaRouterClientState$RouteInfo;->supportedTypes:I

    goto/32 :goto_1e

    nop

    :goto_9
    iput v1, v0, Landroid/media/MediaRouter$RouteInfo;->mVolume:I

    goto/32 :goto_17

    nop

    :goto_a
    iput v1, v0, Landroid/media/MediaRouter$RouteInfo;->mPlaybackStream:I

    goto/32 :goto_6

    nop

    :goto_b
    invoke-virtual {v0, v1}, Landroid/media/MediaRouter$RouteInfo;->setRealStatusCode(I)Z

    goto/32 :goto_12

    nop

    :goto_c
    new-instance v0, Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_16

    nop

    :goto_d
    iget v1, p1, Landroid/media/MediaRouterClientState$RouteInfo;->playbackStream:I

    goto/32 :goto_a

    nop

    :goto_e
    iget v1, p1, Landroid/media/MediaRouterClientState$RouteInfo;->presentationDisplayId:I

    goto/32 :goto_15

    nop

    :goto_f
    iput-object v1, v0, Landroid/media/MediaRouter$RouteInfo;->mDescription:Ljava/lang/CharSequence;

    goto/32 :goto_8

    nop

    :goto_10
    iget v1, p1, Landroid/media/MediaRouterClientState$RouteInfo;->deviceType:I

    goto/32 :goto_1d

    nop

    :goto_11
    iput-boolean v1, v0, Landroid/media/MediaRouter$RouteInfo;->mEnabled:Z

    goto/32 :goto_1a

    nop

    :goto_12
    iget v1, p1, Landroid/media/MediaRouterClientState$RouteInfo;->playbackType:I

    goto/32 :goto_19

    nop

    :goto_13
    invoke-direct {v0, v1}, Landroid/media/MediaRouter$RouteInfo;-><init>(Landroid/media/MediaRouter$RouteCategory;)V

    goto/32 :goto_14

    nop

    :goto_14
    iget-object v1, p1, Landroid/media/MediaRouterClientState$RouteInfo;->id:Ljava/lang/String;

    goto/32 :goto_18

    nop

    :goto_15
    iput v1, v0, Landroid/media/MediaRouter$RouteInfo;->mPresentationDisplayId:I

    goto/32 :goto_7

    nop

    :goto_16
    iget-object v1, p0, Landroid/media/MediaRouter$Static;->mSystemCategory:Landroid/media/MediaRouter$RouteCategory;

    goto/32 :goto_13

    nop

    :goto_17
    iget v1, p1, Landroid/media/MediaRouterClientState$RouteInfo;->volumeMax:I

    goto/32 :goto_1

    nop

    :goto_18
    iput-object v1, v0, Landroid/media/MediaRouter$RouteInfo;->mGlobalRouteId:Ljava/lang/String;

    goto/32 :goto_1c

    nop

    :goto_19
    iput v1, v0, Landroid/media/MediaRouter$RouteInfo;->mPlaybackType:I

    goto/32 :goto_d

    nop

    :goto_1a
    iget v1, p1, Landroid/media/MediaRouterClientState$RouteInfo;->statusCode:I

    goto/32 :goto_b

    nop

    :goto_1b
    iput-object v1, v0, Landroid/media/MediaRouter$RouteInfo;->mName:Ljava/lang/CharSequence;

    goto/32 :goto_2

    nop

    :goto_1c
    iget-object v1, p1, Landroid/media/MediaRouterClientState$RouteInfo;->name:Ljava/lang/String;

    goto/32 :goto_1b

    nop

    :goto_1d
    iput v1, v0, Landroid/media/MediaRouter$RouteInfo;->mDeviceType:I

    goto/32 :goto_4

    nop

    :goto_1e
    iput v1, v0, Landroid/media/MediaRouter$RouteInfo;->mSupportedTypes:I

    goto/32 :goto_10

    nop
.end method

.method public onDisplayAdded(I)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/media/MediaRouter$Static;->updatePresentationDisplays(I)V

    return-void
.end method

.method public onDisplayChanged(I)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/media/MediaRouter$Static;->updatePresentationDisplays(I)V

    return-void
.end method

.method public onDisplayRemoved(I)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/media/MediaRouter$Static;->updatePresentationDisplays(I)V

    return-void
.end method

.method publishClientDiscoveryRequest()V
    .locals 4

    goto/32 :goto_5

    nop

    :goto_0
    const-string v2, "Unable to publish media router client discovery request."

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_3
    goto/32 :goto_1

    nop

    :goto_4
    const-string v1, "MediaRouter"

    goto/32 :goto_0

    nop

    :goto_5
    iget-object v0, p0, Landroid/media/MediaRouter$Static;->mClient:Landroid/media/IMediaRouterClient;

    goto/32 :goto_6

    nop

    :goto_6
    if-nez v0, :cond_0

    goto/32 :goto_3

    :cond_0
    :try_start_0
    iget-object v1, p0, Landroid/media/MediaRouter$Static;->mMediaRouterService:Landroid/media/IMediaRouterService;

    iget v2, p0, Landroid/media/MediaRouter$Static;->mDiscoveryRequestRouteTypes:I

    iget-boolean v3, p0, Landroid/media/MediaRouter$Static;->mDiscoverRequestActiveScan:Z

    invoke-interface {v1, v0, v2, v3}, Landroid/media/IMediaRouterService;->setDiscoveryRequest(Landroid/media/IMediaRouterClient;IZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_7

    nop

    :goto_7
    goto :goto_3

    :catch_0
    move-exception v0

    goto/32 :goto_4

    nop
.end method

.method publishClientSelectedRoute(Z)V
    .locals 3

    goto/32 :goto_2

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_6

    :cond_0
    :try_start_0
    iget-object v1, p0, Landroid/media/MediaRouter$Static;->mMediaRouterService:Landroid/media/IMediaRouterService;

    iget-object v2, p0, Landroid/media/MediaRouter$Static;->mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

    if-eqz v2, :cond_1

    iget-object v2, v2, Landroid/media/MediaRouter$RouteInfo;->mGlobalRouteId:Ljava/lang/String;

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    invoke-interface {v1, v0, v2, p1}, Landroid/media/IMediaRouterService;->setSelectedRoute(Landroid/media/IMediaRouterClient;Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_7

    nop

    :goto_2
    iget-object v0, p0, Landroid/media/MediaRouter$Static;->mClient:Landroid/media/IMediaRouterClient;

    goto/32 :goto_0

    nop

    :goto_3
    const-string v1, "MediaRouter"

    goto/32 :goto_4

    nop

    :goto_4
    const-string v2, "Unable to publish media router client selected route."

    goto/32 :goto_5

    nop

    :goto_5
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_6
    goto/32 :goto_8

    nop

    :goto_7
    goto :goto_6

    :catch_0
    move-exception v0

    goto/32 :goto_3

    nop

    :goto_8
    return-void
.end method

.method rebindAsUser(I)V
    .locals 4

    goto/32 :goto_f

    nop

    :goto_0
    const-string v2, "Unable to unregister media router client."

    goto/32 :goto_12

    nop

    :goto_1
    return-void

    :goto_2
    iput-object v0, p0, Landroid/media/MediaRouter$Static;->mClient:Landroid/media/IMediaRouterClient;

    :goto_3
    goto/32 :goto_a

    nop

    :goto_4
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_5
    goto/32 :goto_9

    nop

    :goto_6
    const/4 v0, 0x0

    goto/32 :goto_2

    nop

    :goto_7
    if-eqz v0, :cond_0

    goto/32 :goto_17

    :cond_0
    :goto_8
    goto/32 :goto_b

    nop

    :goto_9
    invoke-virtual {p0}, Landroid/media/MediaRouter$Static;->publishClientDiscoveryRequest()V

    goto/32 :goto_15

    nop

    :goto_a
    iput p1, p0, Landroid/media/MediaRouter$Static;->mCurrentUserId:I

    :try_start_0
    new-instance v0, Landroid/media/MediaRouter$Static$Client;

    invoke-direct {v0, p0}, Landroid/media/MediaRouter$Static$Client;-><init>(Landroid/media/MediaRouter$Static;)V

    iget-object v2, p0, Landroid/media/MediaRouter$Static;->mMediaRouterService:Landroid/media/IMediaRouterService;

    iget-object v3, p0, Landroid/media/MediaRouter$Static;->mPackageName:Ljava/lang/String;

    invoke-interface {v2, v0, v3, p1}, Landroid/media/IMediaRouterService;->registerClientAsUser(Landroid/media/IMediaRouterClient;Ljava/lang/String;I)V

    iput-object v0, p0, Landroid/media/MediaRouter$Static;->mClient:Landroid/media/IMediaRouterClient;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_c

    nop

    :goto_b
    iget-object v0, p0, Landroid/media/MediaRouter$Static;->mClient:Landroid/media/IMediaRouterClient;

    goto/32 :goto_11

    nop

    :goto_c
    goto :goto_5

    :catch_0
    move-exception v0

    goto/32 :goto_14

    nop

    :goto_d
    if-eq v0, p1, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_19

    nop

    :goto_e
    if-nez v0, :cond_2

    goto/32 :goto_3

    :cond_2
    :try_start_1
    iget-object v2, p0, Landroid/media/MediaRouter$Static;->mMediaRouterService:Landroid/media/IMediaRouterService;

    invoke-interface {v2, v0}, Landroid/media/IMediaRouterService;->unregisterClient(Landroid/media/IMediaRouterClient;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto/32 :goto_18

    nop

    :goto_f
    iget v0, p0, Landroid/media/MediaRouter$Static;->mCurrentUserId:I

    goto/32 :goto_d

    nop

    :goto_10
    iget-object v0, p0, Landroid/media/MediaRouter$Static;->mClient:Landroid/media/IMediaRouterClient;

    goto/32 :goto_7

    nop

    :goto_11
    const-string v1, "MediaRouter"

    goto/32 :goto_e

    nop

    :goto_12
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_13
    goto/32 :goto_6

    nop

    :goto_14
    const-string v2, "Unable to register media router client."

    goto/32 :goto_4

    nop

    :goto_15
    const/4 v0, 0x0

    goto/32 :goto_1a

    nop

    :goto_16
    invoke-virtual {p0}, Landroid/media/MediaRouter$Static;->updateClientState()V

    :goto_17
    goto/32 :goto_1

    nop

    :goto_18
    goto :goto_13

    :catch_1
    move-exception v0

    goto/32 :goto_0

    nop

    :goto_19
    if-gez p1, :cond_3

    goto/32 :goto_8

    :cond_3
    goto/32 :goto_10

    nop

    :goto_1a
    invoke-virtual {p0, v0}, Landroid/media/MediaRouter$Static;->publishClientSelectedRoute(Z)V

    goto/32 :goto_16

    nop
.end method

.method requestSetVolume(Landroid/media/MediaRouter$RouteInfo;I)V
    .locals 3

    goto/32 :goto_5

    nop

    :goto_0
    goto :goto_2

    :catch_0
    move-exception v0

    goto/32 :goto_4

    nop

    :goto_1
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_2
    goto/32 :goto_8

    nop

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_2

    :cond_0
    :try_start_0
    iget-object v1, p0, Landroid/media/MediaRouter$Static;->mMediaRouterService:Landroid/media/IMediaRouterService;

    iget-object v2, p1, Landroid/media/MediaRouter$RouteInfo;->mGlobalRouteId:Ljava/lang/String;

    invoke-interface {v1, v0, v2, p2}, Landroid/media/IMediaRouterService;->requestSetVolume(Landroid/media/IMediaRouterClient;Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_0

    nop

    :goto_4
    const-string v1, "MediaRouter"

    goto/32 :goto_7

    nop

    :goto_5
    iget-object v0, p1, Landroid/media/MediaRouter$RouteInfo;->mGlobalRouteId:Ljava/lang/String;

    goto/32 :goto_9

    nop

    :goto_6
    iget-object v0, p0, Landroid/media/MediaRouter$Static;->mClient:Landroid/media/IMediaRouterClient;

    goto/32 :goto_3

    nop

    :goto_7
    const-string v2, "Unable to request volume change."

    goto/32 :goto_1

    nop

    :goto_8
    return-void

    :goto_9
    if-nez v0, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_6

    nop
.end method

.method requestUpdateVolume(Landroid/media/MediaRouter$RouteInfo;I)V
    .locals 3

    goto/32 :goto_9

    nop

    :goto_0
    const-string v1, "MediaRouter"

    goto/32 :goto_7

    nop

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    :try_start_0
    iget-object v1, p0, Landroid/media/MediaRouter$Static;->mMediaRouterService:Landroid/media/IMediaRouterService;

    iget-object v2, p1, Landroid/media/MediaRouter$RouteInfo;->mGlobalRouteId:Ljava/lang/String;

    invoke-interface {v1, v0, v2, p2}, Landroid/media/IMediaRouterService;->requestUpdateVolume(Landroid/media/IMediaRouterClient;Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_8

    nop

    :goto_2
    return-void

    :goto_3
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_4
    goto/32 :goto_2

    nop

    :goto_5
    iget-object v0, p0, Landroid/media/MediaRouter$Static;->mClient:Landroid/media/IMediaRouterClient;

    goto/32 :goto_1

    nop

    :goto_6
    if-nez v0, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_5

    nop

    :goto_7
    const-string v2, "Unable to request volume change."

    goto/32 :goto_3

    nop

    :goto_8
    goto :goto_4

    :catch_0
    move-exception v0

    goto/32 :goto_0

    nop

    :goto_9
    iget-object v0, p1, Landroid/media/MediaRouter$RouteInfo;->mGlobalRouteId:Ljava/lang/String;

    goto/32 :goto_6

    nop
.end method

.method public setRouterGroupId(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Landroid/media/MediaRouter$Static;->mClient:Landroid/media/IMediaRouterClient;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Landroid/media/MediaRouter$Static;->mMediaRouterService:Landroid/media/IMediaRouterService;

    invoke-interface {v1, v0, p1}, Landroid/media/IMediaRouterService;->registerClientGroupId(Landroid/media/IMediaRouterClient;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "MediaRouter"

    const-string v2, "Unable to register group ID of the client."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    return-void
.end method

.method setSelectedRoute(Landroid/media/MediaRouter$RouteInfo;Z)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p0, p2}, Landroid/media/MediaRouter$Static;->publishClientSelectedRoute(Z)V

    goto/32 :goto_2

    nop

    :goto_1
    iput-object p1, p0, Landroid/media/MediaRouter$Static;->mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method startMonitoringRoutes(Landroid/content/Context;)V
    .locals 3

    goto/32 :goto_4

    nop

    :goto_0
    invoke-virtual {p0, v1}, Landroid/media/MediaRouter$Static;->rebindAsUser(I)V

    goto/32 :goto_a

    nop

    :goto_1
    goto :goto_2

    :catch_0
    move-exception v1

    :goto_2
    goto/32 :goto_17

    nop

    :goto_3
    iget-object v0, p0, Landroid/media/MediaRouter$Static;->mDisplayService:Landroid/hardware/display/DisplayManager;

    goto/32 :goto_20

    nop

    :goto_4
    new-instance v0, Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_2e

    nop

    :goto_5
    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    goto/32 :goto_31

    nop

    :goto_6
    iget-object v1, v1, Landroid/media/MediaRouter$Static;->mResources:Landroid/content/res/Resources;

    goto/32 :goto_d

    nop

    :goto_7
    new-instance v0, Landroid/media/MediaRouter$VolumeChangeReceiver;

    goto/32 :goto_33

    nop

    :goto_8
    invoke-direct {v0}, Landroid/media/MediaRouter$WifiDisplayStatusChangedReceiver;-><init>()V

    goto/32 :goto_27

    nop

    :goto_9
    const/4 v1, 0x0

    goto/32 :goto_28

    nop

    :goto_a
    iget-object v1, p0, Landroid/media/MediaRouter$Static;->mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_23

    nop

    :goto_b
    iput v1, v0, Landroid/media/MediaRouter$RouteInfo;->mNameResId:I

    goto/32 :goto_32

    nop

    :goto_c
    iget-object v0, p0, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_18

    nop

    :goto_d
    const v2, 0x1040325

    goto/32 :goto_35

    nop

    :goto_e
    const v1, 0x1040326

    goto/32 :goto_b

    nop

    :goto_f
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    goto/32 :goto_0

    nop

    :goto_10
    invoke-static {}, Landroid/media/MediaRouter;->selectDefaultRouteStatic()V

    :goto_11
    goto/32 :goto_21

    nop

    :goto_12
    invoke-virtual {p0, v0}, Landroid/media/MediaRouter$Static;->updateAudioRoutes(Landroid/media/AudioRoutesInfo;)V

    :goto_13
    goto/32 :goto_f

    nop

    :goto_14
    new-instance v0, Landroid/media/MediaRouter$WifiDisplayStatusChangedReceiver;

    goto/32 :goto_8

    nop

    :goto_15
    iput-object v0, p0, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_e

    nop

    :goto_16
    invoke-virtual {v0, p0, v1}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V

    goto/32 :goto_36

    nop

    :goto_17
    if-nez v0, :cond_0

    goto/32 :goto_13

    :cond_0
    goto/32 :goto_12

    nop

    :goto_18
    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->updatePresentationDisplay()Z

    goto/32 :goto_37

    nop

    :goto_19
    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    goto/32 :goto_1f

    nop

    :goto_1a
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_39

    nop

    :goto_1b
    iget-object v0, p0, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_3b

    nop

    :goto_1c
    iget-object v1, p0, Landroid/media/MediaRouter$Static;->mHandler:Landroid/os/Handler;

    goto/32 :goto_16

    nop

    :goto_1d
    iput v1, v0, Landroid/media/MediaRouter$RouteInfo;->mSupportedTypes:I

    goto/32 :goto_c

    nop

    :goto_1e
    invoke-direct {v0, v1}, Landroid/media/MediaRouter$RouteInfo;-><init>(Landroid/media/MediaRouter$RouteCategory;)V

    goto/32 :goto_15

    nop

    :goto_1f
    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto/32 :goto_7

    nop

    :goto_20
    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v0

    goto/32 :goto_2f

    nop

    :goto_21
    return-void

    :goto_22
    iput-object v1, v0, Landroid/media/MediaRouter$RouteInfo;->mGlobalRouteId:Ljava/lang/String;

    goto/32 :goto_2b

    nop

    :goto_23
    if-eqz v1, :cond_1

    goto/32 :goto_11

    :cond_1
    goto/32 :goto_10

    nop

    :goto_24
    if-nez v0, :cond_2

    goto/32 :goto_29

    :cond_2
    goto/32 :goto_34

    nop

    :goto_25
    move-object v0, v1

    goto/32 :goto_1

    nop

    :goto_26
    const-string v2, "android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED"

    goto/32 :goto_19

    nop

    :goto_27
    new-instance v1, Landroid/content/IntentFilter;

    goto/32 :goto_26

    nop

    :goto_28
    iput v1, v0, Landroid/media/MediaRouter$RouteInfo;->mVolumeHandling:I

    :goto_29
    goto/32 :goto_1b

    nop

    :goto_2a
    iget-object v0, p0, Landroid/media/MediaRouter$Static;->mDisplayService:Landroid/hardware/display/DisplayManager;

    goto/32 :goto_1c

    nop

    :goto_2b
    iget-object v0, p0, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_3a

    nop

    :goto_2c
    const-string v2, "android.media.VOLUME_CHANGED_ACTION"

    goto/32 :goto_5

    nop

    :goto_2d
    invoke-virtual {v0}, Landroid/media/AudioManager;->isVolumeFixed()Z

    move-result v0

    goto/32 :goto_24

    nop

    :goto_2e
    iget-object v1, p0, Landroid/media/MediaRouter$Static;->mSystemCategory:Landroid/media/MediaRouter$RouteCategory;

    goto/32 :goto_1e

    nop

    :goto_2f
    invoke-static {v0}, Landroid/media/MediaRouter;->updateWifiDisplayStatus(Landroid/hardware/display/WifiDisplayStatus;)V

    goto/32 :goto_14

    nop

    :goto_30
    const/4 v1, 0x3

    goto/32 :goto_1d

    nop

    :goto_31
    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto/32 :goto_2a

    nop

    :goto_32
    iget-object v0, p0, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_30

    nop

    :goto_33
    invoke-direct {v0}, Landroid/media/MediaRouter$VolumeChangeReceiver;-><init>()V

    goto/32 :goto_38

    nop

    :goto_34
    iget-object v0, p0, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_9

    nop

    :goto_35
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_22

    nop

    :goto_36
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Landroid/media/MediaRouter$Static;->mAudioService:Landroid/media/IAudioService;

    invoke-interface {v1}, Landroid/media/IAudioService;->isBluetoothA2dpOn()Z

    move-result v1

    iput-boolean v1, p0, Landroid/media/MediaRouter$Static;->mIsBluetoothA2dpOn:Z

    iget-object v1, p0, Landroid/media/MediaRouter$Static;->mAudioService:Landroid/media/IAudioService;

    iget-object v2, p0, Landroid/media/MediaRouter$Static;->mAudioRoutesObserver:Landroid/media/IAudioRoutesObserver$Stub;

    invoke-interface {v1, v2}, Landroid/media/IAudioService;->startWatchingRoutes(Landroid/media/IAudioRoutesObserver;)Landroid/media/AudioRoutesInfo;

    move-result-object v1
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_25

    nop

    :goto_37
    const-string v0, "audio"

    goto/32 :goto_1a

    nop

    :goto_38
    new-instance v1, Landroid/content/IntentFilter;

    goto/32 :goto_2c

    nop

    :goto_39
    check-cast v0, Landroid/media/AudioManager;

    goto/32 :goto_2d

    nop

    :goto_3a
    invoke-static {v0}, Landroid/media/MediaRouter;->addRouteStatic(Landroid/media/MediaRouter$RouteInfo;)V

    goto/32 :goto_3

    nop

    :goto_3b
    sget-object v1, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    goto/32 :goto_6

    nop
.end method

.method updateAudioRoutes(Landroid/media/AudioRoutesInfo;)V
    .locals 6

    goto/32 :goto_3a

    nop

    :goto_0
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_2e

    nop

    :goto_1
    iget-object v3, v3, Landroid/media/AudioRoutesInfo;->bluetoothName:Ljava/lang/CharSequence;

    goto/32 :goto_82

    nop

    :goto_2
    goto/16 :goto_4f

    :goto_3
    goto/32 :goto_78

    nop

    :goto_4
    invoke-virtual {v2}, Landroid/media/MediaRouter$RouteInfo;->isDefault()Z

    move-result v2

    goto/32 :goto_3e

    nop

    :goto_5
    iget-object v3, p0, Landroid/media/MediaRouter$Static;->mCurAudioRoutesInfo:Landroid/media/AudioRoutesInfo;

    goto/32 :goto_1

    nop

    :goto_6
    iget-object v2, p0, Landroid/media/MediaRouter$Static;->mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_13

    nop

    :goto_7
    if-nez v2, :cond_0

    goto/32 :goto_54

    :cond_0
    goto/32 :goto_3f

    nop

    :goto_8
    iput v4, v2, Landroid/media/MediaRouter$RouteInfo;->mSupportedTypes:I

    goto/32 :goto_72

    nop

    :goto_9
    goto/16 :goto_85

    :goto_a
    goto/32 :goto_3d

    nop

    :goto_b
    iget-object v2, p0, Landroid/media/MediaRouter$Static;->mCurAudioRoutesInfo:Landroid/media/AudioRoutesInfo;

    goto/32 :goto_7b

    nop

    :goto_c
    iput-object v2, p0, Landroid/media/MediaRouter$Static;->mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_20

    nop

    :goto_d
    if-nez v2, :cond_1

    goto/32 :goto_4f

    :cond_1
    :goto_e
    goto/32 :goto_81

    nop

    :goto_f
    iget v3, v3, Landroid/media/AudioRoutesInfo;->mainType:I

    goto/32 :goto_74

    nop

    :goto_10
    iput v3, v2, Landroid/media/AudioRoutesInfo;->mainType:I

    goto/32 :goto_49

    nop

    :goto_11
    iput-object v3, v2, Landroid/media/MediaRouter$RouteInfo;->mName:Ljava/lang/CharSequence;

    goto/32 :goto_50

    nop

    :goto_12
    if-eqz v1, :cond_2

    goto/32 :goto_3

    :cond_2
    goto/32 :goto_63

    nop

    :goto_13
    if-eqz v2, :cond_3

    goto/32 :goto_4c

    :cond_3
    goto/32 :goto_80

    nop

    :goto_14
    goto :goto_1a

    :goto_15
    goto/32 :goto_56

    nop

    :goto_16
    iget-object v3, p0, Landroid/media/MediaRouter$Static;->mCurAudioRoutesInfo:Landroid/media/AudioRoutesInfo;

    goto/32 :goto_f

    nop

    :goto_17
    iget-object v2, p0, Landroid/media/MediaRouter$Static;->mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_2f

    nop

    :goto_18
    sget-object v3, Landroid/media/MediaRouter;->sStatic:Landroid/media/MediaRouter$Static;

    goto/32 :goto_2a

    nop

    :goto_19
    invoke-static {v2}, Landroid/media/MediaRouter;->removeRouteStatic(Landroid/media/MediaRouter$RouteInfo;)V

    :goto_1a
    goto/32 :goto_3b

    nop

    :goto_1b
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    goto/32 :goto_60

    nop

    :goto_1c
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_6f

    nop

    :goto_1d
    const/4 v1, 0x1

    :goto_1e
    goto/32 :goto_42

    nop

    :goto_1f
    if-eqz v3, :cond_4

    goto/32 :goto_76

    :cond_4
    goto/32 :goto_75

    nop

    :goto_20
    invoke-static {v2}, Landroid/media/MediaRouter;->addRouteStatic(Landroid/media/MediaRouter$RouteInfo;)V

    goto/32 :goto_4b

    nop

    :goto_21
    if-eqz v2, :cond_5

    goto/32 :goto_85

    :cond_5
    goto/32 :goto_40

    nop

    :goto_22
    iget-object v3, p0, Landroid/media/MediaRouter$Static;->mSystemCategory:Landroid/media/MediaRouter$RouteCategory;

    goto/32 :goto_6b

    nop

    :goto_23
    invoke-static {v3, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_7d

    nop

    :goto_24
    if-nez v2, :cond_6

    goto/32 :goto_e

    :cond_6
    goto/32 :goto_4

    nop

    :goto_25
    if-ne v2, v3, :cond_7

    goto/32 :goto_43

    :cond_7
    goto/32 :goto_b

    nop

    :goto_26
    iget-object v2, p0, Landroid/media/MediaRouter$Static;->mCurAudioRoutesInfo:Landroid/media/AudioRoutesInfo;

    goto/32 :goto_5f

    nop

    :goto_27
    invoke-virtual {p0}, Landroid/media/MediaRouter$Static;->isBluetoothA2dpOn()Z

    move-result v3

    goto/32 :goto_52

    nop

    :goto_28
    const-string v3, "Audio routes updated: "

    goto/32 :goto_86

    nop

    :goto_29
    and-int/lit8 v2, v2, 0x8

    goto/32 :goto_7

    nop

    :goto_2a
    iget-object v3, v3, Landroid/media/MediaRouter$Static;->mResources:Landroid/content/res/Resources;

    goto/32 :goto_55

    nop

    :goto_2b
    const v5, 0x10401d7

    goto/32 :goto_1b

    nop

    :goto_2c
    const v2, 0x1040329

    :goto_2d
    goto/32 :goto_5d

    nop

    :goto_2e
    const-string v3, ", a2dp="

    goto/32 :goto_6d

    nop

    :goto_2f
    const/4 v3, 0x0

    goto/32 :goto_33

    nop

    :goto_30
    iget-object v3, p1, Landroid/media/AudioRoutesInfo;->bluetoothName:Ljava/lang/CharSequence;

    goto/32 :goto_41

    nop

    :goto_31
    const/4 v1, 0x0

    goto/32 :goto_77

    nop

    :goto_32
    iget-object v2, p0, Landroid/media/MediaRouter$Static;->mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_57

    nop

    :goto_33
    iput-object v3, p0, Landroid/media/MediaRouter$Static;->mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_19

    nop

    :goto_34
    iget v2, p1, Landroid/media/AudioRoutesInfo;->mainType:I

    goto/32 :goto_29

    nop

    :goto_35
    and-int/lit8 v3, v3, 0x13

    goto/32 :goto_67

    nop

    :goto_36
    const v2, 0x1040327

    goto/32 :goto_7e

    nop

    :goto_37
    iget-object v3, p1, Landroid/media/AudioRoutesInfo;->bluetoothName:Ljava/lang/CharSequence;

    goto/32 :goto_11

    nop

    :goto_38
    invoke-static {v3}, Landroid/media/MediaRouter;->dispatchRouteChanged(Landroid/media/MediaRouter$RouteInfo;)V

    goto/32 :goto_65

    nop

    :goto_39
    invoke-virtual {v2}, Landroid/media/MediaRouter$RouteInfo;->isBluetooth()Z

    move-result v2

    goto/32 :goto_d

    nop

    :goto_3a
    const/4 v0, 0x0

    goto/32 :goto_31

    nop

    :goto_3b
    const/4 v0, 0x1

    :goto_3c
    goto/32 :goto_6a

    nop

    :goto_3d
    iget v2, p1, Landroid/media/AudioRoutesInfo;->mainType:I

    goto/32 :goto_83

    nop

    :goto_3e
    if-eqz v2, :cond_8

    goto/32 :goto_e

    :cond_8
    goto/32 :goto_7c

    nop

    :goto_3f
    const v2, 0x1040328

    goto/32 :goto_53

    nop

    :goto_40
    iget v2, p1, Landroid/media/AudioRoutesInfo;->mainType:I

    goto/32 :goto_7a

    nop

    :goto_41
    iput-object v3, v2, Landroid/media/MediaRouter$RouteInfo;->mName:Ljava/lang/CharSequence;

    goto/32 :goto_32

    nop

    :goto_42
    const/4 v0, 0x1

    :goto_43
    goto/32 :goto_5a

    nop

    :goto_44
    if-nez v2, :cond_9

    goto/32 :goto_7f

    :cond_9
    goto/32 :goto_36

    nop

    :goto_45
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_6c

    nop

    :goto_46
    invoke-static {v4, v3, v2}, Landroid/media/MediaRouter;->selectRouteStatic(ILandroid/media/MediaRouter$RouteInfo;Z)V

    goto/32 :goto_2

    nop

    :goto_47
    return-void

    :goto_48
    iput-object v3, v2, Landroid/media/AudioRoutesInfo;->bluetoothName:Ljava/lang/CharSequence;

    goto/32 :goto_47

    nop

    :goto_49
    iget v2, p1, Landroid/media/AudioRoutesInfo;->mainType:I

    goto/32 :goto_69

    nop

    :goto_4a
    iput v3, v2, Landroid/media/MediaRouter$RouteInfo;->mDeviceType:I

    goto/32 :goto_18

    nop

    :goto_4b
    goto/16 :goto_1a

    :goto_4c
    goto/32 :goto_30

    nop

    :goto_4d
    const/4 v1, 0x0

    goto/32 :goto_66

    nop

    :goto_4e
    invoke-static {v4, v3, v2}, Landroid/media/MediaRouter;->selectRouteStatic(ILandroid/media/MediaRouter$RouteInfo;Z)V

    :goto_4f
    goto/32 :goto_26

    nop

    :goto_50
    iget-object v3, p0, Landroid/media/MediaRouter$Static;->mResources:Landroid/content/res/Resources;

    goto/32 :goto_2b

    nop

    :goto_51
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_5c

    nop

    :goto_52
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_51

    nop

    :goto_53
    goto/16 :goto_2d

    :goto_54
    goto/32 :goto_62

    nop

    :goto_55
    const v5, 0x10401d6

    goto/32 :goto_1c

    nop

    :goto_56
    iget-object v2, p0, Landroid/media/MediaRouter$Static;->mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_73

    nop

    :goto_57
    invoke-static {v2}, Landroid/media/MediaRouter;->dispatchRouteChanged(Landroid/media/MediaRouter$RouteInfo;)V

    goto/32 :goto_14

    nop

    :goto_58
    const v2, 0x104032a

    goto/32 :goto_70

    nop

    :goto_59
    if-eqz v2, :cond_a

    goto/32 :goto_3c

    :cond_a
    goto/32 :goto_4d

    nop

    :goto_5a
    iget-object v2, p1, Landroid/media/AudioRoutesInfo;->bluetoothName:Ljava/lang/CharSequence;

    goto/32 :goto_5

    nop

    :goto_5b
    const v2, 0x1040326

    goto/32 :goto_84

    nop

    :goto_5c
    const-string v3, "MediaRouter"

    goto/32 :goto_23

    nop

    :goto_5d
    iget-object v3, p0, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_6e

    nop

    :goto_5e
    if-nez v2, :cond_b

    goto/32 :goto_a

    :cond_b
    goto/32 :goto_9

    nop

    :goto_5f
    iget-object v3, p1, Landroid/media/AudioRoutesInfo;->bluetoothName:Ljava/lang/CharSequence;

    goto/32 :goto_48

    nop

    :goto_60
    iput-object v3, v2, Landroid/media/MediaRouter$RouteInfo;->mDescription:Ljava/lang/CharSequence;

    goto/32 :goto_8

    nop

    :goto_61
    if-nez v2, :cond_c

    goto/32 :goto_15

    :cond_c
    goto/32 :goto_6

    nop

    :goto_62
    iget v2, p1, Landroid/media/AudioRoutesInfo;->mainType:I

    goto/32 :goto_68

    nop

    :goto_63
    iget-object v3, p0, Landroid/media/MediaRouter$Static;->mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_1f

    nop

    :goto_64
    if-nez v2, :cond_d

    goto/32 :goto_71

    :cond_d
    goto/32 :goto_58

    nop

    :goto_65
    iget v3, p1, Landroid/media/AudioRoutesInfo;->mainType:I

    goto/32 :goto_35

    nop

    :goto_66
    iget-object v2, p1, Landroid/media/AudioRoutesInfo;->bluetoothName:Ljava/lang/CharSequence;

    goto/32 :goto_61

    nop

    :goto_67
    if-nez v3, :cond_e

    goto/32 :goto_1e

    :cond_e
    goto/32 :goto_1d

    nop

    :goto_68
    and-int/lit8 v2, v2, 0x10

    goto/32 :goto_64

    nop

    :goto_69
    and-int/lit8 v2, v2, 0x2

    goto/32 :goto_21

    nop

    :goto_6a
    if-nez v0, :cond_f

    goto/32 :goto_4f

    :cond_f
    goto/32 :goto_45

    nop

    :goto_6b
    invoke-direct {v2, v3}, Landroid/media/MediaRouter$RouteInfo;-><init>(Landroid/media/MediaRouter$RouteCategory;)V

    goto/32 :goto_37

    nop

    :goto_6c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_28

    nop

    :goto_6d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_27

    nop

    :goto_6e
    iput v2, v3, Landroid/media/MediaRouter$RouteInfo;->mNameResId:I

    goto/32 :goto_79

    nop

    :goto_6f
    iput-object v3, v2, Landroid/media/MediaRouter$RouteInfo;->mGlobalRouteId:Ljava/lang/String;

    goto/32 :goto_c

    nop

    :goto_70
    goto/16 :goto_2d

    :goto_71
    goto/32 :goto_5b

    nop

    :goto_72
    const/4 v3, 0x3

    goto/32 :goto_4a

    nop

    :goto_73
    if-nez v2, :cond_10

    goto/32 :goto_1a

    :cond_10
    goto/32 :goto_17

    nop

    :goto_74
    const/4 v4, 0x1

    goto/32 :goto_25

    nop

    :goto_75
    goto/16 :goto_3

    :goto_76
    goto/32 :goto_46

    nop

    :goto_77
    iget v2, p1, Landroid/media/AudioRoutesInfo;->mainType:I

    goto/32 :goto_16

    nop

    :goto_78
    iget-object v3, p0, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_4e

    nop

    :goto_79
    iget-object v3, p0, Landroid/media/MediaRouter$Static;->mDefaultAudioVideo:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_38

    nop

    :goto_7a
    and-int/2addr v2, v4

    goto/32 :goto_5e

    nop

    :goto_7b
    iget v3, p1, Landroid/media/AudioRoutesInfo;->mainType:I

    goto/32 :goto_10

    nop

    :goto_7c
    iget-object v2, p0, Landroid/media/MediaRouter$Static;->mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_39

    nop

    :goto_7d
    iget-object v2, p0, Landroid/media/MediaRouter$Static;->mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_24

    nop

    :goto_7e
    goto/16 :goto_2d

    :goto_7f
    goto/32 :goto_34

    nop

    :goto_80
    new-instance v2, Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_22

    nop

    :goto_81
    const/4 v2, 0x0

    goto/32 :goto_12

    nop

    :goto_82
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    goto/32 :goto_59

    nop

    :goto_83
    and-int/lit8 v2, v2, 0x4

    goto/32 :goto_44

    nop

    :goto_84
    goto/16 :goto_2d

    :goto_85
    goto/32 :goto_2c

    nop

    :goto_86
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_0

    nop
.end method

.method updateClientState()V
    .locals 8

    goto/32 :goto_1

    nop

    :goto_0
    if-nez v1, :cond_0

    goto/32 :goto_3d

    :cond_0
    goto/32 :goto_3c

    nop

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_13

    nop

    :goto_2
    const/4 v1, 0x0

    :goto_3
    goto/32 :goto_28

    nop

    :goto_4
    invoke-virtual {p0, v4, v3}, Landroid/media/MediaRouter$Static;->updateGlobalRoute(Landroid/media/MediaRouter$RouteInfo;Landroid/media/MediaRouterClientState$RouteInfo;)V

    :goto_5
    goto/32 :goto_36

    nop

    :goto_6
    iget-object v7, v6, Landroid/media/MediaRouterClientState$RouteInfo;->id:Ljava/lang/String;

    goto/32 :goto_44

    nop

    :goto_7
    const/4 v5, 0x0

    :goto_8
    goto/32 :goto_1d

    nop

    :goto_9
    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_a
    goto/32 :goto_12

    nop

    :goto_b
    goto :goto_a

    :catch_0
    move-exception v1

    goto/32 :goto_19

    nop

    :goto_c
    if-nez v0, :cond_1

    goto/32 :goto_11

    :cond_1
    goto/32 :goto_2c

    nop

    :goto_d
    invoke-static {v2}, Landroid/media/MediaRouter;->removeRouteStatic(Landroid/media/MediaRouter$RouteInfo;)V

    :goto_e
    nop

    :goto_f
    goto/32 :goto_34

    nop

    :goto_10
    goto :goto_3

    :goto_11
    goto/32 :goto_2

    nop

    :goto_12
    iget-object v1, p0, Landroid/media/MediaRouter$Static;->mClientState:Landroid/media/MediaRouterClientState;

    goto/32 :goto_0

    nop

    :goto_13
    iput-object v0, p0, Landroid/media/MediaRouter$Static;->mClientState:Landroid/media/MediaRouterClientState;

    goto/32 :goto_2d

    nop

    :goto_14
    goto :goto_5

    :goto_15
    goto/32 :goto_4

    nop

    :goto_16
    check-cast v6, Landroid/media/MediaRouterClientState$RouteInfo;

    goto/32 :goto_6

    nop

    :goto_17
    goto :goto_f

    :goto_18
    goto/32 :goto_1b

    nop

    :goto_19
    const-string v2, "MediaRouter"

    goto/32 :goto_3b

    nop

    :goto_1a
    invoke-virtual {v2}, Landroid/media/MediaRouter$RouteInfo;->isBluetooth()Z

    move-result v5

    goto/32 :goto_38

    nop

    :goto_1b
    if-nez v4, :cond_2

    goto/32 :goto_e

    :cond_2
    goto/32 :goto_7

    nop

    :goto_1c
    invoke-virtual {p0, v3}, Landroid/media/MediaRouter$Static;->makeGlobalRoute(Landroid/media/MediaRouterClientState$RouteInfo;)Landroid/media/MediaRouter$RouteInfo;

    move-result-object v4

    goto/32 :goto_32

    nop

    :goto_1d
    if-lt v5, v1, :cond_3

    goto/32 :goto_27

    :cond_3
    goto/32 :goto_33

    nop

    :goto_1e
    iget-object v4, v3, Landroid/media/MediaRouterClientState$RouteInfo;->id:Ljava/lang/String;

    goto/32 :goto_3e

    nop

    :goto_1f
    goto :goto_29

    :goto_20
    goto/32 :goto_35

    nop

    :goto_21
    if-eqz v5, :cond_4

    goto/32 :goto_f

    :cond_4
    goto/32 :goto_1a

    nop

    :goto_22
    iget-object v4, v2, Landroid/media/MediaRouter$RouteInfo;->mGlobalRouteId:Ljava/lang/String;

    goto/32 :goto_37

    nop

    :goto_23
    return-void

    :goto_24
    if-lt v2, v1, :cond_5

    goto/32 :goto_20

    :cond_5
    goto/32 :goto_41

    nop

    :goto_25
    if-nez v1, :cond_6

    goto/32 :goto_a

    :cond_6
    :try_start_0
    iget-object v2, p0, Landroid/media/MediaRouter$Static;->mMediaRouterService:Landroid/media/IMediaRouterService;

    invoke-interface {v2, v1}, Landroid/media/IMediaRouterService;->getState(Landroid/media/IMediaRouterClient;)Landroid/media/MediaRouterClientState;

    move-result-object v1

    iput-object v1, p0, Landroid/media/MediaRouter$Static;->mClientState:Landroid/media/MediaRouterClientState;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_b

    nop

    :goto_26
    goto/16 :goto_8

    :goto_27
    goto/32 :goto_d

    nop

    :goto_28
    const/4 v2, 0x0

    :goto_29
    goto/32 :goto_24

    nop

    :goto_2a
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_2b
    goto/32 :goto_3f

    nop

    :goto_2c
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_10

    nop

    :goto_2d
    iget-object v1, p0, Landroid/media/MediaRouter$Static;->mClient:Landroid/media/IMediaRouterClient;

    goto/32 :goto_25

    nop

    :goto_2e
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_40

    nop

    :goto_2f
    goto/16 :goto_f

    :goto_30
    goto/32 :goto_47

    nop

    :goto_31
    check-cast v3, Landroid/media/MediaRouterClientState$RouteInfo;

    goto/32 :goto_1e

    nop

    :goto_32
    invoke-static {v4}, Landroid/media/MediaRouter;->addRouteStatic(Landroid/media/MediaRouter$RouteInfo;)V

    goto/32 :goto_14

    nop

    :goto_33
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    goto/32 :goto_16

    nop

    :goto_34
    move v2, v3

    goto/32 :goto_45

    nop

    :goto_35
    iget-object v2, p0, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    goto/32 :goto_2a

    nop

    :goto_36
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_1f

    nop

    :goto_37
    invoke-virtual {v2}, Landroid/media/MediaRouter$RouteInfo;->isDefault()Z

    move-result v5

    goto/32 :goto_21

    nop

    :goto_38
    if-nez v5, :cond_7

    goto/32 :goto_18

    :cond_7
    goto/32 :goto_17

    nop

    :goto_39
    iget-object v2, p0, Landroid/media/MediaRouter$Static;->mRoutes:Ljava/util/ArrayList;

    goto/32 :goto_2e

    nop

    :goto_3a
    if-gtz v2, :cond_8

    goto/32 :goto_46

    :cond_8
    goto/32 :goto_39

    nop

    :goto_3b
    const-string v3, "Unable to retrieve media router client state."

    goto/32 :goto_9

    nop

    :goto_3c
    iget-object v0, v1, Landroid/media/MediaRouterClientState;->routes:Ljava/util/ArrayList;

    :goto_3d
    goto/32 :goto_c

    nop

    :goto_3e
    invoke-virtual {p0, v4}, Landroid/media/MediaRouter$Static;->findGlobalRoute(Ljava/lang/String;)Landroid/media/MediaRouter$RouteInfo;

    move-result-object v4

    goto/32 :goto_43

    nop

    :goto_3f
    add-int/lit8 v3, v2, -0x1

    goto/32 :goto_3a

    nop

    :goto_40
    check-cast v2, Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_22

    nop

    :goto_41
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_31

    nop

    :goto_42
    if-nez v7, :cond_9

    goto/32 :goto_30

    :cond_9
    goto/32 :goto_2f

    nop

    :goto_43
    if-eqz v4, :cond_a

    goto/32 :goto_15

    :cond_a
    goto/32 :goto_1c

    nop

    :goto_44
    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    goto/32 :goto_42

    nop

    :goto_45
    goto/16 :goto_2b

    :goto_46
    goto/32 :goto_23

    nop

    :goto_47
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_26

    nop
.end method

.method updateDiscoveryRequest()V
    .locals 10

    goto/32 :goto_39

    nop

    :goto_0
    or-int/2addr v0, v9

    :goto_1
    goto/32 :goto_50

    nop

    :goto_2
    return-void

    :goto_3
    and-int/lit8 v9, v9, 0x8

    goto/32 :goto_21

    nop

    :goto_4
    const/4 v2, 0x1

    goto/32 :goto_13

    nop

    :goto_5
    goto :goto_1

    :goto_6
    goto/32 :goto_47

    nop

    :goto_7
    iget v9, v8, Landroid/media/MediaRouter$CallbackInfo;->flags:I

    goto/32 :goto_3

    nop

    :goto_8
    if-nez v9, :cond_0

    goto/32 :goto_2b

    :cond_0
    goto/32 :goto_30

    nop

    :goto_9
    iget-object v8, p0, Landroid/media/MediaRouter$Static;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    goto/32 :goto_2d

    nop

    :goto_a
    iput v0, p0, Landroid/media/MediaRouter$Static;->mDiscoveryRequestRouteTypes:I

    goto/32 :goto_3e

    nop

    :goto_b
    const/4 v3, 0x0

    :goto_c
    goto/32 :goto_3a

    nop

    :goto_d
    iget-object v5, p0, Landroid/media/MediaRouter$Static;->mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_18

    nop

    :goto_e
    iput-boolean v7, p0, Landroid/media/MediaRouter$Static;->mActivelyScanningWifiDisplays:Z

    goto/32 :goto_3f

    nop

    :goto_f
    invoke-virtual {v5}, Landroid/hardware/display/DisplayManager;->startWifiDisplayScan()V

    goto/32 :goto_48

    nop

    :goto_10
    if-lt v5, v4, :cond_1

    goto/32 :goto_4e

    :cond_1
    goto/32 :goto_9

    nop

    :goto_11
    or-int/2addr v0, v1

    :goto_12
    goto/32 :goto_42

    nop

    :goto_13
    iget v7, v8, Landroid/media/MediaRouter$CallbackInfo;->type:I

    goto/32 :goto_1b

    nop

    :goto_14
    const/4 v3, 0x0

    goto/32 :goto_31

    nop

    :goto_15
    invoke-virtual {v5, v6}, Landroid/media/MediaRouter$RouteInfo;->matchesTypes(I)Z

    move-result v5

    goto/32 :goto_41

    nop

    :goto_16
    invoke-virtual {v5}, Landroid/hardware/display/DisplayManager;->stopWifiDisplayScan()V

    :goto_17
    goto/32 :goto_3b

    nop

    :goto_18
    if-nez v5, :cond_2

    goto/32 :goto_c

    :cond_2
    goto/32 :goto_15

    nop

    :goto_19
    invoke-virtual {p0}, Landroid/media/MediaRouter$Static;->publishClientDiscoveryRequest()V

    :goto_1a
    goto/32 :goto_2

    nop

    :goto_1b
    and-int/2addr v6, v7

    goto/32 :goto_38

    nop

    :goto_1c
    if-eqz v5, :cond_3

    goto/32 :goto_17

    :cond_3
    goto/32 :goto_e

    nop

    :goto_1d
    iget v9, v8, Landroid/media/MediaRouter$CallbackInfo;->type:I

    goto/32 :goto_4b

    nop

    :goto_1e
    if-eq v0, v5, :cond_4

    goto/32 :goto_25

    :cond_4
    goto/32 :goto_35

    nop

    :goto_1f
    or-int/2addr v0, v9

    goto/32 :goto_2a

    nop

    :goto_20
    and-int/2addr v7, v9

    goto/32 :goto_44

    nop

    :goto_21
    if-nez v9, :cond_5

    goto/32 :goto_6

    :cond_5
    goto/32 :goto_1d

    nop

    :goto_22
    const/4 v5, 0x0

    goto/32 :goto_34

    nop

    :goto_23
    if-eqz v0, :cond_6

    goto/32 :goto_3d

    :cond_6
    goto/32 :goto_3c

    nop

    :goto_24
    if-ne v2, v5, :cond_7

    goto/32 :goto_1a

    :cond_7
    :goto_25
    goto/32 :goto_a

    nop

    :goto_26
    const/4 v5, 0x0

    :goto_27
    goto/32 :goto_46

    nop

    :goto_28
    iget-object v5, p0, Landroid/media/MediaRouter$Static;->mDisplayService:Landroid/hardware/display/DisplayManager;

    goto/32 :goto_16

    nop

    :goto_29
    iget-boolean v5, p0, Landroid/media/MediaRouter$Static;->mActivelyScanningWifiDisplays:Z

    goto/32 :goto_32

    nop

    :goto_2a
    goto/16 :goto_1

    :goto_2b
    goto/32 :goto_7

    nop

    :goto_2c
    iget v9, v8, Landroid/media/MediaRouter$CallbackInfo;->flags:I

    goto/32 :goto_4c

    nop

    :goto_2d
    invoke-virtual {v8, v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    goto/32 :goto_4f

    nop

    :goto_2e
    const/4 v3, 0x1

    :goto_2f
    goto/32 :goto_43

    nop

    :goto_30
    iget v9, v8, Landroid/media/MediaRouter$CallbackInfo;->type:I

    goto/32 :goto_1f

    nop

    :goto_31
    iget-object v4, p0, Landroid/media/MediaRouter$Static;->mCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    goto/32 :goto_4a

    nop

    :goto_32
    if-nez v5, :cond_8

    goto/32 :goto_17

    :cond_8
    goto/32 :goto_22

    nop

    :goto_33
    const/4 v2, 0x0

    goto/32 :goto_14

    nop

    :goto_34
    iput-boolean v5, p0, Landroid/media/MediaRouter$Static;->mActivelyScanningWifiDisplays:Z

    goto/32 :goto_28

    nop

    :goto_35
    iget-boolean v5, p0, Landroid/media/MediaRouter$Static;->mDiscoverRequestActiveScan:Z

    goto/32 :goto_24

    nop

    :goto_36
    const/4 v1, 0x0

    goto/32 :goto_33

    nop

    :goto_37
    iget-boolean v5, p0, Landroid/media/MediaRouter$Static;->mActivelyScanningWifiDisplays:Z

    goto/32 :goto_1c

    nop

    :goto_38
    if-nez v6, :cond_9

    goto/32 :goto_2f

    :cond_9
    goto/32 :goto_2e

    nop

    :goto_39
    const/4 v0, 0x0

    goto/32 :goto_36

    nop

    :goto_3a
    if-nez v3, :cond_a

    goto/32 :goto_49

    :cond_a
    goto/32 :goto_37

    nop

    :goto_3b
    iget v5, p0, Landroid/media/MediaRouter$Static;->mDiscoveryRequestRouteTypes:I

    goto/32 :goto_1e

    nop

    :goto_3c
    if-nez v2, :cond_b

    goto/32 :goto_12

    :cond_b
    :goto_3d
    goto/32 :goto_11

    nop

    :goto_3e
    iput-boolean v2, p0, Landroid/media/MediaRouter$Static;->mDiscoverRequestActiveScan:Z

    goto/32 :goto_19

    nop

    :goto_3f
    iget-object v5, p0, Landroid/media/MediaRouter$Static;->mDisplayService:Landroid/hardware/display/DisplayManager;

    goto/32 :goto_f

    nop

    :goto_40
    const/4 v7, 0x1

    goto/32 :goto_10

    nop

    :goto_41
    if-nez v5, :cond_c

    goto/32 :goto_c

    :cond_c
    goto/32 :goto_b

    nop

    :goto_42
    iget-boolean v5, p0, Landroid/media/MediaRouter$Static;->mCanConfigureWifiDisplays:Z

    goto/32 :goto_45

    nop

    :goto_43
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_4d

    nop

    :goto_44
    if-nez v7, :cond_d

    goto/32 :goto_2f

    :cond_d
    goto/32 :goto_4

    nop

    :goto_45
    if-nez v5, :cond_e

    goto/32 :goto_17

    :cond_e
    goto/32 :goto_d

    nop

    :goto_46
    const/4 v6, 0x4

    goto/32 :goto_40

    nop

    :goto_47
    iget v9, v8, Landroid/media/MediaRouter$CallbackInfo;->type:I

    goto/32 :goto_0

    nop

    :goto_48
    goto/16 :goto_17

    :goto_49
    goto/32 :goto_29

    nop

    :goto_4a
    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v4

    goto/32 :goto_26

    nop

    :goto_4b
    or-int/2addr v1, v9

    goto/32 :goto_5

    nop

    :goto_4c
    and-int/lit8 v9, v9, 0x5

    goto/32 :goto_8

    nop

    :goto_4d
    goto/16 :goto_27

    :goto_4e
    goto/32 :goto_23

    nop

    :goto_4f
    check-cast v8, Landroid/media/MediaRouter$CallbackInfo;

    goto/32 :goto_2c

    nop

    :goto_50
    iget v9, v8, Landroid/media/MediaRouter$CallbackInfo;->flags:I

    goto/32 :goto_20

    nop
.end method

.method updateGlobalRoute(Landroid/media/MediaRouter$RouteInfo;Landroid/media/MediaRouterClientState$RouteInfo;)V
    .locals 6

    goto/32 :goto_4a

    nop

    :goto_0
    iget v5, p2, Landroid/media/MediaRouterClientState$RouteInfo;->statusCode:I

    goto/32 :goto_1e

    nop

    :goto_1
    if-ne v4, v5, :cond_0

    goto/32 :goto_3f

    :cond_0
    goto/32 :goto_54

    nop

    :goto_2
    iput v4, p1, Landroid/media/MediaRouter$RouteInfo;->mPresentationDisplayId:I

    goto/32 :goto_d

    nop

    :goto_3
    const/4 v0, 0x1

    :goto_4
    goto/32 :goto_1b

    nop

    :goto_5
    const/4 v2, 0x0

    goto/32 :goto_34

    nop

    :goto_6
    if-nez v0, :cond_1

    goto/32 :goto_3a

    :cond_1
    goto/32 :goto_39

    nop

    :goto_7
    if-eqz v3, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_57

    nop

    :goto_8
    iget v4, p1, Landroid/media/MediaRouter$RouteInfo;->mVolumeHandling:I

    goto/32 :goto_1f

    nop

    :goto_9
    iget v4, p2, Landroid/media/MediaRouterClientState$RouteInfo;->presentationDisplayId:I

    goto/32 :goto_2

    nop

    :goto_a
    iget v4, p1, Landroid/media/MediaRouter$RouteInfo;->mPlaybackStream:I

    goto/32 :goto_3b

    nop

    :goto_b
    return-void

    :goto_c
    iput-object v3, p1, Landroid/media/MediaRouter$RouteInfo;->mName:Ljava/lang/CharSequence;

    goto/32 :goto_5e

    nop

    :goto_d
    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->updatePresentationDisplay()Z

    goto/32 :goto_4e

    nop

    :goto_e
    invoke-static {p1}, Landroid/media/MediaRouter;->dispatchRoutePresentationDisplayChanged(Landroid/media/MediaRouter$RouteInfo;)V

    :goto_f
    goto/32 :goto_b

    nop

    :goto_10
    iget v4, p2, Landroid/media/MediaRouterClientState$RouteInfo;->supportedTypes:I

    goto/32 :goto_51

    nop

    :goto_11
    const/4 v1, 0x1

    :goto_12
    goto/32 :goto_5b

    nop

    :goto_13
    if-ne v4, v5, :cond_3

    goto/32 :goto_1d

    :cond_3
    goto/32 :goto_55

    nop

    :goto_14
    invoke-static {p1}, Landroid/media/MediaRouter;->dispatchRouteVolumeChanged(Landroid/media/MediaRouter$RouteInfo;)V

    :goto_15
    goto/32 :goto_4f

    nop

    :goto_16
    iget v4, p2, Landroid/media/MediaRouterClientState$RouteInfo;->supportedTypes:I

    goto/32 :goto_22

    nop

    :goto_17
    const/4 v0, 0x1

    :goto_18
    goto/32 :goto_a

    nop

    :goto_19
    const/4 v0, 0x1

    :goto_1a
    goto/32 :goto_45

    nop

    :goto_1b
    iget v3, p1, Landroid/media/MediaRouter$RouteInfo;->mSupportedTypes:I

    goto/32 :goto_16

    nop

    :goto_1c
    const/4 v0, 0x1

    :goto_1d
    goto/32 :goto_58

    nop

    :goto_1e
    if-ne v4, v5, :cond_4

    goto/32 :goto_32

    :cond_4
    goto/32 :goto_50

    nop

    :goto_1f
    iget v5, p2, Landroid/media/MediaRouterClientState$RouteInfo;->volumeHandling:I

    goto/32 :goto_1

    nop

    :goto_20
    const/4 v1, 0x0

    goto/32 :goto_5

    nop

    :goto_21
    iget-object v4, p2, Landroid/media/MediaRouterClientState$RouteInfo;->description:Ljava/lang/String;

    goto/32 :goto_2e

    nop

    :goto_22
    if-ne v3, v4, :cond_5

    goto/32 :goto_1a

    :cond_5
    goto/32 :goto_10

    nop

    :goto_23
    iput-object v3, p1, Landroid/media/MediaRouter$RouteInfo;->mDescription:Ljava/lang/CharSequence;

    goto/32 :goto_3

    nop

    :goto_24
    const/4 v0, 0x1

    :goto_25
    goto/32 :goto_47

    nop

    :goto_26
    if-ne v4, v5, :cond_6

    goto/32 :goto_18

    :cond_6
    goto/32 :goto_53

    nop

    :goto_27
    invoke-static {v3, v4}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    goto/32 :goto_41

    nop

    :goto_28
    iput v4, p1, Landroid/media/MediaRouter$RouteInfo;->mPlaybackType:I

    goto/32 :goto_17

    nop

    :goto_29
    iget-object v4, p2, Landroid/media/MediaRouterClientState$RouteInfo;->name:Ljava/lang/String;

    goto/32 :goto_27

    nop

    :goto_2a
    iget v4, p2, Landroid/media/MediaRouterClientState$RouteInfo;->volume:I

    goto/32 :goto_40

    nop

    :goto_2b
    iget v5, p2, Landroid/media/MediaRouterClientState$RouteInfo;->playbackType:I

    goto/32 :goto_26

    nop

    :goto_2c
    iget v5, p2, Landroid/media/MediaRouterClientState$RouteInfo;->presentationDisplayId:I

    goto/32 :goto_5c

    nop

    :goto_2d
    iput v4, p1, Landroid/media/MediaRouter$RouteInfo;->mVolumeMax:I

    goto/32 :goto_36

    nop

    :goto_2e
    invoke-static {v3, v4}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    goto/32 :goto_7

    nop

    :goto_2f
    iget v4, p1, Landroid/media/MediaRouter$RouteInfo;->mPresentationDisplayId:I

    goto/32 :goto_2c

    nop

    :goto_30
    iput-boolean v4, p1, Landroid/media/MediaRouter$RouteInfo;->mEnabled:Z

    goto/32 :goto_24

    nop

    :goto_31
    const/4 v0, 0x1

    :goto_32
    goto/32 :goto_52

    nop

    :goto_33
    iget v4, p2, Landroid/media/MediaRouterClientState$RouteInfo;->volumeMax:I

    goto/32 :goto_2d

    nop

    :goto_34
    iget-object v3, p1, Landroid/media/MediaRouter$RouteInfo;->mName:Ljava/lang/CharSequence;

    goto/32 :goto_29

    nop

    :goto_35
    iput v4, p1, Landroid/media/MediaRouter$RouteInfo;->mVolumeHandling:I

    goto/32 :goto_3d

    nop

    :goto_36
    const/4 v0, 0x1

    goto/32 :goto_42

    nop

    :goto_37
    iget v5, p2, Landroid/media/MediaRouterClientState$RouteInfo;->volumeMax:I

    goto/32 :goto_38

    nop

    :goto_38
    if-ne v4, v5, :cond_7

    goto/32 :goto_43

    :cond_7
    goto/32 :goto_33

    nop

    :goto_39
    invoke-static {p1, v3}, Landroid/media/MediaRouter;->dispatchRouteChanged(Landroid/media/MediaRouter$RouteInfo;I)V

    :goto_3a
    goto/32 :goto_5d

    nop

    :goto_3b
    iget v5, p2, Landroid/media/MediaRouterClientState$RouteInfo;->playbackStream:I

    goto/32 :goto_13

    nop

    :goto_3c
    iget-boolean v4, p2, Landroid/media/MediaRouterClientState$RouteInfo;->enabled:Z

    goto/32 :goto_30

    nop

    :goto_3d
    const/4 v0, 0x1

    goto/32 :goto_3e

    nop

    :goto_3e
    const/4 v1, 0x1

    :goto_3f
    goto/32 :goto_2f

    nop

    :goto_40
    iput v4, p1, Landroid/media/MediaRouter$RouteInfo;->mVolume:I

    goto/32 :goto_60

    nop

    :goto_41
    if-eqz v3, :cond_8

    goto/32 :goto_5f

    :cond_8
    goto/32 :goto_48

    nop

    :goto_42
    const/4 v1, 0x1

    :goto_43
    goto/32 :goto_8

    nop

    :goto_44
    iget v5, p2, Landroid/media/MediaRouterClientState$RouteInfo;->volume:I

    goto/32 :goto_5a

    nop

    :goto_45
    iget-boolean v4, p1, Landroid/media/MediaRouter$RouteInfo;->mEnabled:Z

    goto/32 :goto_46

    nop

    :goto_46
    iget-boolean v5, p2, Landroid/media/MediaRouterClientState$RouteInfo;->enabled:Z

    goto/32 :goto_56

    nop

    :goto_47
    invoke-static {p1}, Landroid/media/MediaRouter$RouteInfo;->-$$Nest$fgetmRealStatusCode(Landroid/media/MediaRouter$RouteInfo;)I

    move-result v4

    goto/32 :goto_0

    nop

    :goto_48
    iget-object v3, p2, Landroid/media/MediaRouterClientState$RouteInfo;->name:Ljava/lang/String;

    goto/32 :goto_c

    nop

    :goto_49
    iget-object v3, p1, Landroid/media/MediaRouter$RouteInfo;->mDescription:Ljava/lang/CharSequence;

    goto/32 :goto_21

    nop

    :goto_4a
    const/4 v0, 0x0

    goto/32 :goto_20

    nop

    :goto_4b
    const/4 v2, 0x1

    :goto_4c
    goto/32 :goto_6

    nop

    :goto_4d
    invoke-virtual {p1, v4}, Landroid/media/MediaRouter$RouteInfo;->setRealStatusCode(I)Z

    goto/32 :goto_31

    nop

    :goto_4e
    const/4 v0, 0x1

    goto/32 :goto_4b

    nop

    :goto_4f
    if-nez v2, :cond_9

    goto/32 :goto_f

    :cond_9
    goto/32 :goto_e

    nop

    :goto_50
    iget v4, p2, Landroid/media/MediaRouterClientState$RouteInfo;->statusCode:I

    goto/32 :goto_4d

    nop

    :goto_51
    iput v4, p1, Landroid/media/MediaRouter$RouteInfo;->mSupportedTypes:I

    goto/32 :goto_19

    nop

    :goto_52
    iget v4, p1, Landroid/media/MediaRouter$RouteInfo;->mPlaybackType:I

    goto/32 :goto_2b

    nop

    :goto_53
    iget v4, p2, Landroid/media/MediaRouterClientState$RouteInfo;->playbackType:I

    goto/32 :goto_28

    nop

    :goto_54
    iget v4, p2, Landroid/media/MediaRouterClientState$RouteInfo;->volumeHandling:I

    goto/32 :goto_35

    nop

    :goto_55
    iget v4, p2, Landroid/media/MediaRouterClientState$RouteInfo;->playbackStream:I

    goto/32 :goto_59

    nop

    :goto_56
    if-ne v4, v5, :cond_a

    goto/32 :goto_25

    :cond_a
    goto/32 :goto_3c

    nop

    :goto_57
    iget-object v3, p2, Landroid/media/MediaRouterClientState$RouteInfo;->description:Ljava/lang/String;

    goto/32 :goto_23

    nop

    :goto_58
    iget v4, p1, Landroid/media/MediaRouter$RouteInfo;->mVolume:I

    goto/32 :goto_44

    nop

    :goto_59
    iput v4, p1, Landroid/media/MediaRouter$RouteInfo;->mPlaybackStream:I

    goto/32 :goto_1c

    nop

    :goto_5a
    if-ne v4, v5, :cond_b

    goto/32 :goto_12

    :cond_b
    goto/32 :goto_2a

    nop

    :goto_5b
    iget v4, p1, Landroid/media/MediaRouter$RouteInfo;->mVolumeMax:I

    goto/32 :goto_37

    nop

    :goto_5c
    if-ne v4, v5, :cond_c

    goto/32 :goto_4c

    :cond_c
    goto/32 :goto_9

    nop

    :goto_5d
    if-nez v1, :cond_d

    goto/32 :goto_15

    :cond_d
    goto/32 :goto_14

    nop

    :goto_5e
    const/4 v0, 0x1

    :goto_5f
    goto/32 :goto_49

    nop

    :goto_60
    const/4 v0, 0x1

    goto/32 :goto_11

    nop
.end method
