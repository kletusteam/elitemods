.class public Landroid/media/MediaMetrics$Name;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/MediaMetrics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Name"
.end annotation


# static fields
.field public static final AUDIO:Ljava/lang/String; = "audio"

.field public static final AUDIO_BLUETOOTH:Ljava/lang/String; = "audio.bluetooth"

.field public static final AUDIO_DEVICE:Ljava/lang/String; = "audio.device"

.field public static final AUDIO_FOCUS:Ljava/lang/String; = "audio.focus"

.field public static final AUDIO_FORCE_USE:Ljava/lang/String; = "audio.forceUse"

.field public static final AUDIO_MIC:Ljava/lang/String; = "audio.mic"

.field public static final AUDIO_MODE:Ljava/lang/String; = "audio.mode"

.field public static final AUDIO_SERVICE:Ljava/lang/String; = "audio.service"

.field public static final AUDIO_VOLUME:Ljava/lang/String; = "audio.volume"

.field public static final AUDIO_VOLUME_EVENT:Ljava/lang/String; = "audio.volume.event"

.field public static final METRICS_MANAGER:Ljava/lang/String; = "metrics.manager"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
