.class public interface abstract Landroid/media/Spatializer$OnSpatializerOutputChangedListener;
.super Ljava/lang/Object;


# annotations
.annotation runtime Landroid/annotation/SystemApi;
    client = .enum Landroid/annotation/SystemApi$Client;->PRIVILEGED_APPS:Landroid/annotation/SystemApi$Client;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/Spatializer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnSpatializerOutputChangedListener"
.end annotation


# virtual methods
.method public abstract onSpatializerOutputChanged(Landroid/media/Spatializer;I)V
.end method
