.class public interface abstract Landroid/media/Spatializer$OnSpatializerStateChangedListener;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/Spatializer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnSpatializerStateChangedListener"
.end annotation


# virtual methods
.method public abstract onSpatializerAvailableChanged(Landroid/media/Spatializer;Z)V
.end method

.method public abstract onSpatializerEnabledChanged(Landroid/media/Spatializer;Z)V
.end method
