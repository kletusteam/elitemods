.class public interface abstract annotation Landroid/media/AudioFlag;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final AUDIBILITY_ENFORCED:I = 0x0

.field public static final BEACON:I = 0x3

.field public static final BYPASS_INTERRUPTION_POLICY:I = 0x6

.field public static final BYPASS_MUTE:I = 0x7

.field public static final CALL_REDIRECTION:I = 0x10

.field public static final CAPTURE_PRIVATE:I = 0xd

.field public static final CAR_VOIP:I = 0x11

.field public static final CONTENT_SPATIALIZED:I = 0xe

.field public static final DEEP_BUFFER:I = 0x9

.field public static final HW_AV_SYNC:I = 0x4

.field public static final HW_HOTWORD:I = 0x5

.field public static final INCALL_MUSIC:I = 0x12

.field public static final LOW_LATENCY:I = 0x8

.field public static final MUTE_HAPTIC:I = 0xb

.field public static final NEVER_SPATIALIZE:I = 0xf

.field public static final NO_MEDIA_PROJECTION:I = 0xa

.field public static final NO_SYSTEM_CAPTURE:I = 0xc

.field public static final SCO:I = 0x2

.field public static final SECURE:I = 0x1
