.class public interface abstract Landroid/media/MediaRecorder$OnEventListener;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/MediaRecorder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnEventListener"
.end annotation


# virtual methods
.method public abstract onAudioLevelsEvent(Landroid/media/MediaRecorder;II)V
.end method

.method public abstract onMicBlockingEvent(Landroid/media/MediaRecorder;II)V
.end method

.method public abstract onSourceLocalizationEvent(Landroid/media/MediaRecorder;IIII)V
.end method

.method public abstract onSourceSectorAzimuthEvent(Landroid/media/MediaRecorder;III)V
.end method

.method public abstract onSourceSectorWidthEvent(Landroid/media/MediaRecorder;III)V
.end method

.method public abstract onWindEvent(Landroid/media/MediaRecorder;IZI)V
.end method
