.class public interface abstract annotation Landroid/media/InterpolatorType;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final CUBIC:I = 0x2

.field public static final CUBIC_MONOTONIC:I = 0x3

.field public static final LINEAR:I = 0x1

.field public static final STEP:I
