.class public abstract Landroid/media/PlayerBase;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/PlayerBase$PlayerIdCard;,
        Landroid/media/PlayerBase$IPlayerWrapper;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final DEBUG_APP_OPS:Z = false

.field private static final TAG:Ljava/lang/String; = "PlayerBase"

.field private static sService:Landroid/media/IAudioService;


# instance fields
.field private mAppOps:Lcom/android/internal/app/IAppOpsService;

.field private mAppOpsCallback:Lcom/android/internal/app/IAppOpsCallback;

.field protected mAttributes:Landroid/media/AudioAttributes;

.field protected mAuxEffectSendLevel:F

.field private mDeviceId:I

.field private mHasAppOpsPlayAudio:Z

.field private final mImplType:I

.field protected mLeftVolume:F

.field private final mLock:Ljava/lang/Object;

.field private mPanMultiplierL:F

.field private mPanMultiplierR:F

.field protected mPlayerIId:I

.field protected mRightVolume:F

.field private mStartDelayMs:I

.field private mState:I

.field private mVolMultiplier:F


# direct methods
.method constructor <init>(Landroid/media/AudioAttributes;I)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Landroid/media/PlayerBase;->mLeftVolume:F

    iput v0, p0, Landroid/media/PlayerBase;->mRightVolume:F

    const/4 v1, 0x0

    iput v1, p0, Landroid/media/PlayerBase;->mAuxEffectSendLevel:F

    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Landroid/media/PlayerBase;->mLock:Ljava/lang/Object;

    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid/media/PlayerBase;->mHasAppOpsPlayAudio:Z

    const/4 v2, -0x1

    iput v2, p0, Landroid/media/PlayerBase;->mPlayerIId:I

    const/4 v2, 0x0

    iput v2, p0, Landroid/media/PlayerBase;->mStartDelayMs:I

    iput v0, p0, Landroid/media/PlayerBase;->mPanMultiplierL:F

    iput v0, p0, Landroid/media/PlayerBase;->mPanMultiplierR:F

    iput v0, p0, Landroid/media/PlayerBase;->mVolMultiplier:F

    if-eqz p1, :cond_0

    iput-object p1, p0, Landroid/media/PlayerBase;->mAttributes:Landroid/media/AudioAttributes;

    iput p2, p0, Landroid/media/PlayerBase;->mImplType:I

    iput v1, p0, Landroid/media/PlayerBase;->mState:I

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Illegal null AudioAttributes"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static deprecateStreamTypeForPlayback(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    const/16 v0, 0xa

    if-eq p0, v0, :cond_0

    const-string v0, "Use of stream types is deprecated for operations other than volume control"

    invoke-static {p1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "See the documentation of "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " for what to use instead with android.media.AudioAttributes to qualify your playback use case"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Use of STREAM_ACCESSIBILITY is reserved for volume control"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static getService()Landroid/media/IAudioService;
    .locals 2

    sget-object v0, Landroid/media/PlayerBase;->sService:Landroid/media/IAudioService;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "audio"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/media/IAudioService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IAudioService;

    move-result-object v1

    sput-object v1, Landroid/media/PlayerBase;->sService:Landroid/media/IAudioService;

    return-object v1
.end method

.method private updatePlayerVolume()V
    .locals 4

    iget-object v0, p0, Landroid/media/PlayerBase;->mLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget v1, p0, Landroid/media/PlayerBase;->mVolMultiplier:F

    iget v2, p0, Landroid/media/PlayerBase;->mLeftVolume:F

    mul-float/2addr v2, v1

    iget v3, p0, Landroid/media/PlayerBase;->mPanMultiplierL:F

    mul-float/2addr v2, v3

    iget v3, p0, Landroid/media/PlayerBase;->mRightVolume:F

    mul-float/2addr v1, v3

    iget v3, p0, Landroid/media/PlayerBase;->mPanMultiplierR:F

    mul-float/2addr v1, v3

    const/4 v3, 0x0

    invoke-virtual {p0, v3, v2, v1}, Landroid/media/PlayerBase;->playerSetVolume(ZFF)V

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private updateState(II)V
    .locals 5

    iget-object v0, p0, Landroid/media/PlayerBase;->mLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iput p1, p0, Landroid/media/PlayerBase;->mState:I

    iget v1, p0, Landroid/media/PlayerBase;->mPlayerIId:I

    iput p2, p0, Landroid/media/PlayerBase;->mDeviceId:I

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {}, Landroid/media/PlayerBase;->getService()Landroid/media/IAudioService;

    move-result-object v0

    invoke-interface {v0, v1, p1, p2}, Landroid/media/IAudioService;->playerEvent(III)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "PlayerBase"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error talking to audio service, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Landroid/media/AudioPlaybackConfiguration;->toLogFriendlyPlayerState(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " state will not be tracked for piid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void

    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method


# virtual methods
.method basePause()V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x3

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    const/4 v1, 0x0

    goto/32 :goto_3

    nop

    :goto_3
    invoke-direct {p0, v0, v1}, Landroid/media/PlayerBase;->updateState(II)V

    goto/32 :goto_1

    nop
.end method

.method protected baseRegisterPlayer(I)V
    .locals 5

    :try_start_0
    invoke-static {}, Landroid/media/PlayerBase;->getService()Landroid/media/IAudioService;

    move-result-object v0

    new-instance v1, Landroid/media/PlayerBase$PlayerIdCard;

    iget v2, p0, Landroid/media/PlayerBase;->mImplType:I

    iget-object v3, p0, Landroid/media/PlayerBase;->mAttributes:Landroid/media/AudioAttributes;

    new-instance v4, Landroid/media/PlayerBase$IPlayerWrapper;

    invoke-direct {v4, p0}, Landroid/media/PlayerBase$IPlayerWrapper;-><init>(Landroid/media/PlayerBase;)V

    invoke-direct {v1, v2, v3, v4, p1}, Landroid/media/PlayerBase$PlayerIdCard;-><init>(ILandroid/media/AudioAttributes;Landroid/media/IPlayer;I)V

    invoke-interface {v0, v1}, Landroid/media/IAudioService;->trackPlayer(Landroid/media/PlayerBase$PlayerIdCard;)I

    move-result v0

    iput v0, p0, Landroid/media/PlayerBase;->mPlayerIId:I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "PlayerBase"

    const-string v2, "Error talking to audio service, player will not be tracked"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method baseRelease()V
    .locals 4

    goto/32 :goto_4

    nop

    :goto_0
    goto :goto_6

    :catch_0
    move-exception v1

    goto/32 :goto_c

    nop

    :goto_1
    return-void

    :catchall_0
    move-exception v2

    :try_start_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_8

    nop

    :goto_2
    goto :goto_3

    :catch_1
    move-exception v1

    :goto_3
    goto/32 :goto_1

    nop

    :goto_4
    const/4 v0, 0x0

    goto/32 :goto_a

    nop

    :goto_5
    goto :goto_7

    :goto_6
    nop

    :goto_7
    :try_start_1
    iget-object v1, p0, Landroid/media/PlayerBase;->mAppOps:Lcom/android/internal/app/IAppOpsService;

    if-eqz v1, :cond_0

    iget-object v2, p0, Landroid/media/PlayerBase;->mAppOpsCallback:Lcom/android/internal/app/IAppOpsCallback;

    invoke-interface {v1, v2}, Lcom/android/internal/app/IAppOpsService;->stopWatchingMode(Lcom/android/internal/app/IAppOpsCallback;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    goto/32 :goto_2

    nop

    :goto_8
    throw v2

    :goto_9
    if-nez v0, :cond_1

    goto/32 :goto_6

    :cond_1
    :try_start_2
    invoke-static {}, Landroid/media/PlayerBase;->getService()Landroid/media/IAudioService;

    move-result-object v1

    iget v2, p0, Landroid/media/PlayerBase;->mPlayerIId:I

    invoke-interface {v1, v2}, Landroid/media/IAudioService;->releasePlayer(I)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto/32 :goto_0

    nop

    :goto_a
    iget-object v1, p0, Landroid/media/PlayerBase;->mLock:Ljava/lang/Object;

    goto/32 :goto_d

    nop

    :goto_b
    const-string v3, "Error talking to audio service, the player will still be tracked"

    goto/32 :goto_e

    nop

    :goto_c
    const-string v2, "PlayerBase"

    goto/32 :goto_b

    nop

    :goto_d
    monitor-enter v1

    :try_start_3
    iget v2, p0, Landroid/media/PlayerBase;->mState:I

    if-eqz v2, :cond_2

    const/4 v0, 0x1

    const/4 v2, 0x0

    iput v2, p0, Landroid/media/PlayerBase;->mState:I

    :cond_2
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/32 :goto_9

    nop

    :goto_e
    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/32 :goto_5

    nop
.end method

.method baseSetAuxEffectSendLevel(F)I
    .locals 2

    goto/32 :goto_4

    nop

    :goto_0
    monitor-enter v0

    :try_start_0
    iput p1, p0, Landroid/media/PlayerBase;->mAuxEffectSendLevel:F

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_1

    nop

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_5

    nop

    :goto_2
    throw v1

    :goto_3
    return v0

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/32 :goto_2

    nop

    :goto_4
    iget-object v0, p0, Landroid/media/PlayerBase;->mLock:Ljava/lang/Object;

    goto/32 :goto_0

    nop

    :goto_5
    invoke-virtual {p0, v0, p1}, Landroid/media/PlayerBase;->playerSetAuxEffectSendLevel(ZF)I

    move-result v0

    goto/32 :goto_3

    nop
.end method

.method baseSetPan(F)V
    .locals 4

    goto/32 :goto_a

    nop

    :goto_0
    throw v1

    :goto_1
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto/32 :goto_e

    nop

    :goto_2
    if-gez v3, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_4

    nop

    :goto_3
    invoke-direct {p0}, Landroid/media/PlayerBase;->updatePlayerVolume()V

    goto/32 :goto_8

    nop

    :goto_4
    sub-float v3, v1, v0

    :try_start_0
    iput v3, p0, Landroid/media/PlayerBase;->mPanMultiplierL:F

    iput v1, p0, Landroid/media/PlayerBase;->mPanMultiplierR:F

    goto :goto_6

    :goto_5
    iput v1, p0, Landroid/media/PlayerBase;->mPanMultiplierL:F

    add-float/2addr v1, v0

    iput v1, p0, Landroid/media/PlayerBase;->mPanMultiplierR:F

    :goto_6
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_3

    nop

    :goto_7
    const/high16 v1, 0x3f800000    # 1.0f

    goto/32 :goto_1

    nop

    :goto_8
    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/32 :goto_0

    nop

    :goto_9
    cmpl-float v3, v0, v3

    goto/32 :goto_2

    nop

    :goto_a
    const/high16 v0, -0x40800000    # -1.0f

    goto/32 :goto_c

    nop

    :goto_b
    const/4 v3, 0x0

    goto/32 :goto_9

    nop

    :goto_c
    invoke-static {v0, p1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    goto/32 :goto_7

    nop

    :goto_d
    monitor-enter v2

    goto/32 :goto_b

    nop

    :goto_e
    iget-object v2, p0, Landroid/media/PlayerBase;->mLock:Ljava/lang/Object;

    goto/32 :goto_d

    nop
.end method

.method baseSetStartDelayMs(I)V
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    throw v1

    :goto_1
    monitor-enter v0

    goto/32 :goto_2

    nop

    :goto_2
    const/4 v1, 0x0

    :try_start_0
    invoke-static {p1, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Landroid/media/PlayerBase;->mStartDelayMs:I

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_0

    nop

    :goto_3
    iget-object v0, p0, Landroid/media/PlayerBase;->mLock:Ljava/lang/Object;

    goto/32 :goto_1

    nop
.end method

.method baseSetVolume(FF)V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    invoke-direct {p0}, Landroid/media/PlayerBase;->updatePlayerVolume()V

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Landroid/media/PlayerBase;->mLock:Ljava/lang/Object;

    goto/32 :goto_3

    nop

    :goto_2
    return-void

    :catchall_0
    move-exception v1

    :try_start_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_4

    nop

    :goto_3
    monitor-enter v0

    :try_start_1
    iput p1, p0, Landroid/media/PlayerBase;->mLeftVolume:F

    iput p2, p0, Landroid/media/PlayerBase;->mRightVolume:F

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/32 :goto_0

    nop

    :goto_4
    throw v1
.end method

.method baseStart(I)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-direct {p0, v0, p1}, Landroid/media/PlayerBase;->updateState(II)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    const/4 v0, 0x2

    goto/32 :goto_0

    nop
.end method

.method baseStop()V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    const/4 v0, 0x4

    goto/32 :goto_3

    nop

    :goto_2
    invoke-direct {p0, v0, v1}, Landroid/media/PlayerBase;->updateState(II)V

    goto/32 :goto_0

    nop

    :goto_3
    const/4 v1, 0x0

    goto/32 :goto_2

    nop
.end method

.method baseUpdateAudioAttributes(Landroid/media/AudioAttributes;)V
    .locals 3

    goto/32 :goto_d

    nop

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_1
    goto/32 :goto_9

    nop

    :goto_2
    const-string v2, "Error talking to audio service, audio attributes will not be updated"

    goto/32 :goto_0

    nop

    :goto_3
    throw v0

    :goto_4
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_3

    nop

    :goto_5
    const-string v1, "PlayerBase"

    goto/32 :goto_2

    nop

    :goto_6
    const-string v1, "Illegal null AudioAttributes"

    goto/32 :goto_4

    nop

    :goto_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_6

    nop

    :goto_8
    goto :goto_1

    :catch_0
    move-exception v0

    goto/32 :goto_5

    nop

    :goto_9
    iget-object v0, p0, Landroid/media/PlayerBase;->mLock:Ljava/lang/Object;

    goto/32 :goto_a

    nop

    :goto_a
    monitor-enter v0

    :try_start_0
    iput-object p1, p0, Landroid/media/PlayerBase;->mAttributes:Landroid/media/AudioAttributes;

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_b

    nop

    :goto_b
    throw v1

    :goto_c
    goto/32 :goto_7

    nop

    :goto_d
    if-nez p1, :cond_0

    goto/32 :goto_c

    :cond_0
    :try_start_1
    invoke-static {}, Landroid/media/PlayerBase;->getService()Landroid/media/IAudioService;

    move-result-object v0

    iget v1, p0, Landroid/media/PlayerBase;->mPlayerIId:I

    invoke-interface {v0, v1, p1}, Landroid/media/IAudioService;->playerAttributes(ILandroid/media/AudioAttributes;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto/32 :goto_8

    nop
.end method

.method baseUpdateDeviceId(Landroid/media/AudioDeviceInfo;)V
    .locals 6

    goto/32 :goto_4

    nop

    :goto_0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_d

    nop

    :goto_1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_12

    nop

    :goto_2
    invoke-virtual {p1}, Landroid/media/AudioDeviceInfo;->getId()I

    move-result v0

    :goto_3
    goto/32 :goto_6

    nop

    :goto_4
    const/4 v0, 0x0

    goto/32 :goto_b

    nop

    :goto_5
    const-string v5, " device id will not be tracked for piid="

    goto/32 :goto_8

    nop

    :goto_6
    iget-object v1, p0, Landroid/media/PlayerBase;->mLock:Ljava/lang/Object;

    goto/32 :goto_e

    nop

    :goto_7
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_11

    nop

    :goto_8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_f

    nop

    :goto_9
    throw v2

    :goto_a
    return-void

    :catchall_0
    move-exception v2

    :try_start_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_9

    nop

    :goto_b
    if-nez p1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_2

    nop

    :goto_c
    goto :goto_13

    :catch_0
    move-exception v1

    goto/32 :goto_14

    nop

    :goto_d
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_5

    nop

    :goto_e
    monitor-enter v1

    :try_start_1
    iget v2, p0, Landroid/media/PlayerBase;->mPlayerIId:I

    iput v0, p0, Landroid/media/PlayerBase;->mDeviceId:I

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {}, Landroid/media/PlayerBase;->getService()Landroid/media/IAudioService;

    move-result-object v1

    const/4 v3, 0x5

    invoke-interface {v1, v2, v3, v0}, Landroid/media/IAudioService;->playerEvent(III)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto/32 :goto_c

    nop

    :goto_f
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_1

    nop

    :goto_10
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_7

    nop

    :goto_11
    const-string v5, "Error talking to audio service, "

    goto/32 :goto_0

    nop

    :goto_12
    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_13
    goto/32 :goto_a

    nop

    :goto_14
    const-string v3, "PlayerBase"

    goto/32 :goto_10

    nop
.end method

.method baseUpdateSessionId(I)V
    .locals 3

    :try_start_0
    invoke-static {}, Landroid/media/PlayerBase;->getService()Landroid/media/IAudioService;

    move-result-object v0

    iget v1, p0, Landroid/media/PlayerBase;->mPlayerIId:I

    invoke-interface {v0, v1, p1}, Landroid/media/IAudioService;->playerSessionId(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_4

    nop

    :goto_0
    const-string v1, "PlayerBase"

    goto/32 :goto_1

    nop

    :goto_1
    const-string v2, "Error talking to audio service, the session ID will not be updated"

    goto/32 :goto_2

    nop

    :goto_2
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_3
    goto/32 :goto_5

    nop

    :goto_4
    goto :goto_3

    :catch_0
    move-exception v0

    goto/32 :goto_0

    nop

    :goto_5
    return-void
.end method

.method protected getCurrentOpPackageName()Ljava/lang/String;
    .locals 1

    invoke-static {}, Landroid/app/ActivityThread;->currentOpPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->emptyIfNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPlayerIId()I
    .locals 2

    iget-object v0, p0, Landroid/media/PlayerBase;->mLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget v1, p0, Landroid/media/PlayerBase;->mPlayerIId:I

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected getStartDelayMs()I
    .locals 2

    iget-object v0, p0, Landroid/media/PlayerBase;->mLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget v1, p0, Landroid/media/PlayerBase;->mStartDelayMs:I

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected getVolMultiplier()F
    .locals 2

    iget-object v0, p0, Landroid/media/PlayerBase;->mLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget v1, p0, Landroid/media/PlayerBase;->mVolMultiplier:F

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method abstract playerApplyVolumeShaper(Landroid/media/VolumeShaper$Configuration;Landroid/media/VolumeShaper$Operation;)I
.end method

.method abstract playerGetVolumeShaperState(I)Landroid/media/VolumeShaper$State;
.end method

.method abstract playerPause()V
.end method

.method abstract playerSetAuxEffectSendLevel(ZF)I
.end method

.method abstract playerSetVolume(ZFF)V
.end method

.method abstract playerStart()V
.end method

.method abstract playerStop()V
.end method

.method public setStartDelayMs(I)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/media/PlayerBase;->baseSetStartDelayMs(I)V

    return-void
.end method

.method setVolumeMultiplier(F)V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    invoke-direct {p0}, Landroid/media/PlayerBase;->updatePlayerVolume()V

    goto/32 :goto_4

    nop

    :goto_1
    iget-object v0, p0, Landroid/media/PlayerBase;->mLock:Ljava/lang/Object;

    goto/32 :goto_2

    nop

    :goto_2
    monitor-enter v0

    :try_start_0
    iput p1, p0, Landroid/media/PlayerBase;->mVolMultiplier:F

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_0

    nop

    :goto_3
    throw v1

    :goto_4
    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/32 :goto_3

    nop
.end method
