.class Landroid/media/WebVttParser$3;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/media/WebVttParser$Phase;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/WebVttParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Landroid/media/WebVttParser;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Landroid/media/WebVttParser;

    return-void
.end method

.method constructor <init>(Landroid/media/WebVttParser;)V
    .locals 0

    iput-object p1, p0, Landroid/media/WebVttParser$3;->this$0:Landroid/media/WebVttParser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public parse(Ljava/lang/String;)V
    .locals 5

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/media/WebVttParser$3;->this$0:Landroid/media/WebVttParser;

    invoke-static {v0}, Landroid/media/WebVttParser;->-$$Nest$fgetmParseCueId(Landroid/media/WebVttParser;)Landroid/media/WebVttParser$Phase;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/media/WebVttParser;->-$$Nest$fputmPhase(Landroid/media/WebVttParser;Landroid/media/WebVttParser$Phase;)V

    goto :goto_0

    :cond_0
    const-string v0, "-->"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/media/WebVttParser$3;->this$0:Landroid/media/WebVttParser;

    invoke-static {v0}, Landroid/media/WebVttParser;->-$$Nest$fgetmParseCueTime(Landroid/media/WebVttParser;)Landroid/media/WebVttParser$Phase;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/media/WebVttParser;->-$$Nest$fputmPhase(Landroid/media/WebVttParser;Landroid/media/WebVttParser$Phase;)V

    iget-object v0, p0, Landroid/media/WebVttParser$3;->this$0:Landroid/media/WebVttParser;

    invoke-static {v0}, Landroid/media/WebVttParser;->-$$Nest$fgetmPhase(Landroid/media/WebVttParser;)Landroid/media/WebVttParser$Phase;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/media/WebVttParser$Phase;->parse(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/16 v0, 0x3a

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_3

    :cond_2
    iget-object v1, p0, Landroid/media/WebVttParser$3;->this$0:Landroid/media/WebVttParser;

    const-string v2, "meta data header has invalid format"

    invoke-static {v1, v2, p1}, Landroid/media/WebVttParser;->-$$Nest$mlog_warning(Landroid/media/WebVttParser;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Region"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p0, v2}, Landroid/media/WebVttParser$3;->parseRegion(Ljava/lang/String;)Landroid/media/TextTrackRegion;

    move-result-object v3

    iget-object v4, p0, Landroid/media/WebVttParser$3;->this$0:Landroid/media/WebVttParser;

    invoke-static {v4}, Landroid/media/WebVttParser;->-$$Nest$fgetmListener(Landroid/media/WebVttParser;)Landroid/media/WebVttCueListener;

    move-result-object v4

    invoke-interface {v4, v3}, Landroid/media/WebVttCueListener;->onRegionParsed(Landroid/media/TextTrackRegion;)V

    :cond_4
    :goto_0
    return-void
.end method

.method parseRegion(Ljava/lang/String;)Landroid/media/TextTrackRegion;
    .locals 22

    goto/32 :goto_c

    nop

    :goto_0
    if-nez v10, :cond_0

    goto/32 :goto_68

    :cond_0
    goto/32 :goto_e

    nop

    :goto_1
    move-object/from16 v3, p1

    goto/32 :goto_a0

    nop

    :goto_2
    invoke-static {v0, v11, v6, v10, v15}, Landroid/media/WebVttParser;->-$$Nest$mlog_warning(Landroid/media/WebVttParser;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_5d

    nop

    :goto_3
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v14

    goto/32 :goto_a

    nop

    :goto_4
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v16

    goto/32 :goto_3c

    nop

    :goto_5
    if-ltz v14, :cond_1

    goto/32 :goto_16

    :cond_1
    goto/32 :goto_a1

    nop

    :goto_6
    const/16 v20, 0x0

    goto/32 :goto_76

    nop

    :goto_7
    iget-object v10, v1, Landroid/media/WebVttParser$3;->this$0:Landroid/media/WebVttParser;

    goto/32 :goto_3

    nop

    :goto_8
    move/from16 v20, v6

    goto/32 :goto_31

    nop

    :goto_9
    const/16 v20, 0x0

    goto/32 :goto_67

    nop

    :goto_a
    const-string v11, "region setting"

    goto/32 :goto_87

    nop

    :goto_b
    const-string v0, "regionanchor"

    goto/32 :goto_3b

    nop

    :goto_c
    move-object/from16 v1, p0

    goto/32 :goto_5a

    nop

    :goto_d
    invoke-virtual {v15, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v14

    goto/32 :goto_5

    nop

    :goto_e
    const-string v10, ".*[^0-9].*"

    goto/32 :goto_30

    nop

    :goto_f
    move-object/from16 v15, v17

    goto/32 :goto_6a

    nop

    :goto_10
    move-object v6, v15

    goto/32 :goto_f

    nop

    :goto_11
    goto/16 :goto_2a

    :goto_12
    goto/32 :goto_6c

    nop

    :goto_13
    move-object/from16 v14, v16

    goto/32 :goto_10

    nop

    :goto_14
    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    goto/32 :goto_56

    nop

    :goto_15
    goto/16 :goto_2a

    :goto_16
    goto/32 :goto_93

    nop

    :goto_17
    move-object v0, v10

    goto/32 :goto_a5

    nop

    :goto_18
    if-nez v0, :cond_2

    goto/32 :goto_3f

    :cond_2
    goto/32 :goto_3e

    nop

    :goto_19
    const/16 v20, 0x0

    goto/32 :goto_11

    nop

    :goto_1a
    move-object/from16 v17, v14

    goto/32 :goto_13

    nop

    :goto_1b
    const/16 v20, 0x0

    goto/32 :goto_9a

    nop

    :goto_1c
    aget-object v8, v4, v7

    goto/32 :goto_53

    nop

    :goto_1d
    const-string v0, " +"

    goto/32 :goto_1

    nop

    :goto_1e
    const-string v0, "id"

    goto/32 :goto_23

    nop

    :goto_1f
    if-gtz v9, :cond_3

    goto/32 :goto_4c

    :cond_3
    goto/32 :goto_8a

    nop

    :goto_20
    invoke-static/range {v10 .. v15}, Landroid/media/WebVttParser;->-$$Nest$mlog_warning(Landroid/media/WebVttParser;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_4b

    nop

    :goto_21
    const-string v11, "region setting"

    goto/32 :goto_7d

    nop

    :goto_22
    const-string v0, "up"

    goto/32 :goto_54

    nop

    :goto_23
    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_86

    nop

    :goto_24
    move-object/from16 v13, v19

    goto/32 :goto_85

    nop

    :goto_25
    iput v13, v2, Landroid/media/TextTrackRegion;->mAnchorPointX:F

    goto/32 :goto_7f

    nop

    :goto_26
    const-string v0, "lines"

    goto/32 :goto_14

    nop

    :goto_27
    iget-object v10, v1, Landroid/media/WebVttParser$3;->this$0:Landroid/media/WebVttParser;

    goto/32 :goto_99

    nop

    :goto_28
    move/from16 v20, v6

    goto/32 :goto_2c

    nop

    :goto_29
    move/from16 v20, v6

    :goto_2a
    goto/32 :goto_42

    nop

    :goto_2b
    goto :goto_2a

    :catch_0
    move-exception v0

    goto/32 :goto_3d

    nop

    :goto_2c
    goto/16 :goto_2a

    :catch_1
    move-exception v0

    goto/32 :goto_39

    nop

    :goto_2d
    invoke-static {v10, v11, v6, v12, v15}, Landroid/media/WebVttParser;->-$$Nest$mlog_warning(Landroid/media/WebVttParser;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_2e
    goto/32 :goto_9

    nop

    :goto_2f
    add-int/lit8 v0, v14, 0x1

    goto/32 :goto_64

    nop

    :goto_30
    move-object/from16 v15, v17

    goto/32 :goto_7a

    nop

    :goto_31
    goto/16 :goto_2a

    :goto_32
    goto/32 :goto_9f

    nop

    :goto_33
    invoke-virtual {v6, v12}, Ljava/lang/String;->charAt(I)C

    move-result v10

    goto/32 :goto_58

    nop

    :goto_34
    if-nez v10, :cond_4

    goto/32 :goto_a3

    :cond_4
    goto/32 :goto_78

    nop

    :goto_35
    iput v13, v2, Landroid/media/TextTrackRegion;->mViewportAnchorPointX:F

    goto/32 :goto_8f

    nop

    :goto_36
    invoke-direct {v0}, Landroid/media/TextTrackRegion;-><init>()V

    goto/32 :goto_48

    nop

    :goto_37
    iput v0, v2, Landroid/media/TextTrackRegion;->mScrollValue:I

    goto/32 :goto_19

    nop

    :goto_38
    invoke-static/range {v10 .. v15}, Landroid/media/WebVttParser;->-$$Nest$mlog_warning(Landroid/media/WebVttParser;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_74

    nop

    :goto_39
    iget-object v10, v1, Landroid/media/WebVttParser$3;->this$0:Landroid/media/WebVttParser;

    goto/32 :goto_4

    nop

    :goto_3a
    move-object/from16 v17, v14

    goto/32 :goto_52

    nop

    :goto_3b
    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_5b

    nop

    :goto_3c
    const-string v11, "region setting"

    goto/32 :goto_6d

    nop

    :goto_3d
    const/4 v12, 0x0

    goto/32 :goto_71

    nop

    :goto_3e
    goto/16 :goto_62

    :goto_3f
    goto/32 :goto_55

    nop

    :goto_40
    const-string v10, "contains no comma"

    goto/32 :goto_2

    nop

    :goto_41
    move/from16 v6, v20

    goto/32 :goto_7b

    nop

    :goto_42
    add-int/lit8 v7, v7, 0x1

    goto/32 :goto_41

    nop

    :goto_43
    move-object v0, v10

    goto/32 :goto_7

    nop

    :goto_44
    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_18

    nop

    :goto_45
    move-object/from16 v15, v17

    goto/32 :goto_b

    nop

    :goto_46
    goto/16 :goto_90

    :goto_47
    goto/32 :goto_35

    nop

    :goto_48
    move-object v2, v0

    goto/32 :goto_1d

    nop

    :goto_49
    if-eq v10, v11, :cond_5

    goto/32 :goto_47

    :cond_5
    goto/32 :goto_25

    nop

    :goto_4a
    move-object/from16 v15, v17

    goto/32 :goto_20

    nop

    :goto_4b
    goto/16 :goto_2a

    :goto_4c
    goto/32 :goto_29

    nop

    :goto_4d
    move-object v12, v15

    goto/32 :goto_1a

    nop

    :goto_4e
    move v7, v6

    :goto_4f
    goto/32 :goto_5e

    nop

    :goto_50
    move-object v12, v6

    goto/32 :goto_4a

    nop

    :goto_51
    move/from16 v19, v14

    goto/32 :goto_82

    nop

    :goto_52
    move-object v6, v15

    goto/32 :goto_26

    nop

    :goto_53
    const/16 v0, 0x3d

    goto/32 :goto_5f

    nop

    :goto_54
    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_75

    nop

    :goto_55
    const-string v0, "scroll"

    goto/32 :goto_92

    nop

    :goto_56
    const-string v11, "region setting"

    goto/32 :goto_0

    nop

    :goto_57
    move-object/from16 v15, v18

    goto/32 :goto_38

    nop

    :goto_58
    const/16 v11, 0x72

    goto/32 :goto_49

    nop

    :goto_59
    invoke-static {v0, v11, v6, v10, v15}, Landroid/media/WebVttParser;->-$$Nest$mlog_warning(Landroid/media/WebVttParser;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_1b

    nop

    :goto_5a
    new-instance v0, Landroid/media/TextTrackRegion;

    goto/32 :goto_36

    nop

    :goto_5b
    if-eqz v0, :cond_6

    goto/32 :goto_62

    :cond_6
    goto/32 :goto_94

    nop

    :goto_5c
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v16

    goto/32 :goto_21

    nop

    :goto_5d
    const/16 v20, 0x0

    goto/32 :goto_15

    nop

    :goto_5e
    if-lt v7, v5, :cond_7

    goto/32 :goto_7c

    :cond_7
    goto/32 :goto_1c

    nop

    :goto_5f
    invoke-virtual {v8, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    goto/32 :goto_1f

    nop

    :goto_60
    move-object/from16 v16, v15

    goto/32 :goto_57

    nop

    :goto_61
    goto/16 :goto_2a

    :goto_62
    goto/32 :goto_79

    nop

    :goto_63
    const/4 v6, 0x0

    goto/32 :goto_4e

    nop

    :goto_64
    invoke-virtual {v15, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v18

    :try_start_0
    invoke-static/range {v17 .. v17}, Landroid/media/WebVttParser;->parseFloatPercentage(Ljava/lang/String;)F

    move-result v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_3

    goto/32 :goto_97

    nop

    :goto_65
    invoke-static {v10, v0, v6, v11, v15}, Landroid/media/WebVttParser;->-$$Nest$mlog_warning(Landroid/media/WebVttParser;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_9e

    nop

    :goto_66
    const-string v0, "width"

    goto/32 :goto_8c

    nop

    :goto_67
    goto/16 :goto_2a

    :goto_68
    goto/32 :goto_45

    nop

    :goto_69
    array-length v5, v4

    goto/32 :goto_63

    nop

    :goto_6a
    invoke-static/range {v10 .. v15}, Landroid/media/WebVttParser;->-$$Nest$mlog_warning(Landroid/media/WebVttParser;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_6

    nop

    :goto_6b
    if-nez v0, :cond_8

    goto/32 :goto_77

    :cond_8
    :try_start_1
    invoke-static {v14}, Landroid/media/WebVttParser;->parseFloatPercentage(Ljava/lang/String;)F

    move-result v0

    iput v0, v2, Landroid/media/TextTrackRegion;->mWidth:F
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    goto/32 :goto_28

    nop

    :goto_6c
    iget-object v0, v1, Landroid/media/WebVttParser$3;->this$0:Landroid/media/WebVttParser;

    goto/32 :goto_81

    nop

    :goto_6d
    const-string v13, "has invalid value"

    goto/32 :goto_4d

    nop

    :goto_6e
    invoke-virtual {v15, v10, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    goto/32 :goto_2f

    nop

    :goto_6f
    move-object v12, v6

    goto/32 :goto_89

    nop

    :goto_70
    const/16 v0, 0x12d

    goto/32 :goto_37

    nop

    :goto_71
    move-object v10, v0

    goto/32 :goto_17

    nop

    :goto_72
    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    goto/32 :goto_1e

    nop

    :goto_73
    goto/16 :goto_2e

    :catch_2
    move-exception v0

    goto/32 :goto_27

    nop

    :goto_74
    goto/16 :goto_2a

    :catch_3
    move-exception v0

    goto/32 :goto_51

    nop

    :goto_75
    if-nez v0, :cond_9

    goto/32 :goto_12

    :cond_9
    goto/32 :goto_70

    nop

    :goto_76
    goto/16 :goto_2a

    :goto_77
    goto/32 :goto_3a

    nop

    :goto_78
    iget-object v10, v1, Landroid/media/WebVttParser$3;->this$0:Landroid/media/WebVttParser;

    goto/32 :goto_91

    nop

    :goto_79
    const-string v0, ","

    goto/32 :goto_d

    nop

    :goto_7a
    invoke-virtual {v15, v10}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v10

    goto/32 :goto_34

    nop

    :goto_7b
    goto/16 :goto_4f

    :goto_7c
    goto/32 :goto_88

    nop

    :goto_7d
    const-string v19, "has invalid y component"

    goto/32 :goto_8d

    nop

    :goto_7e
    if-nez v0, :cond_a

    goto/32 :goto_9b

    :cond_a
    goto/32 :goto_22

    nop

    :goto_7f
    iput v0, v2, Landroid/media/TextTrackRegion;->mAnchorPointY:F

    goto/32 :goto_46

    nop

    :goto_80
    if-eq v9, v0, :cond_b

    goto/32 :goto_32

    :cond_b
    goto/32 :goto_8

    nop

    :goto_81
    const-string v10, "has invalid value"

    goto/32 :goto_59

    nop

    :goto_82
    move-object/from16 v16, v15

    goto/32 :goto_9c

    nop

    :goto_83
    move/from16 v20, v12

    goto/32 :goto_2b

    nop

    :goto_84
    const/4 v12, 0x0

    goto/32 :goto_33

    nop

    :goto_85
    move/from16 v19, v14

    goto/32 :goto_8b

    nop

    :goto_86
    if-nez v0, :cond_c

    goto/32 :goto_96

    :cond_c
    goto/32 :goto_a6

    nop

    :goto_87
    const-string v13, "has invalid x component"

    goto/32 :goto_50

    nop

    :goto_88
    return-object v2

    :goto_89
    move/from16 v21, v13

    goto/32 :goto_24

    nop

    :goto_8a
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v0

    goto/32 :goto_98

    nop

    :goto_8b
    move-object/from16 v14, v16

    goto/32 :goto_60

    nop

    :goto_8c
    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_6b

    nop

    :goto_8d
    move/from16 v20, v12

    goto/32 :goto_6f

    nop

    :goto_8e
    add-int/lit8 v0, v9, 0x1

    goto/32 :goto_72

    nop

    :goto_8f
    iput v0, v2, Landroid/media/TextTrackRegion;->mViewportAnchorPointY:F

    :goto_90
    goto/32 :goto_83

    nop

    :goto_91
    const-string v11, "contains an invalid character"

    goto/32 :goto_65

    nop

    :goto_92
    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_7e

    nop

    :goto_93
    const/4 v10, 0x0

    goto/32 :goto_6e

    nop

    :goto_94
    const-string v0, "viewportanchor"

    goto/32 :goto_44

    nop

    :goto_95
    goto/16 :goto_2a

    :goto_96
    goto/32 :goto_66

    nop

    :goto_97
    move v13, v0

    nop

    :try_start_2
    invoke-static/range {v18 .. v18}, Landroid/media/WebVttParser;->parseFloatPercentage(Ljava/lang/String;)F

    move-result v0
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0

    nop

    goto/32 :goto_84

    nop

    :goto_98
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_80

    nop

    :goto_99
    const-string v12, "is not numeric"

    goto/32 :goto_2d

    nop

    :goto_9a
    goto/16 :goto_2a

    :goto_9b
    goto/32 :goto_a7

    nop

    :goto_9c
    const/16 v20, 0x0

    goto/32 :goto_9d

    nop

    :goto_9d
    move-object v10, v0

    goto/32 :goto_43

    nop

    :goto_9e
    const/16 v20, 0x0

    goto/32 :goto_a2

    nop

    :goto_9f
    invoke-virtual {v8, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    goto/32 :goto_8e

    nop

    :goto_a0
    invoke-virtual {v3, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_69

    nop

    :goto_a1
    iget-object v0, v1, Landroid/media/WebVttParser$3;->this$0:Landroid/media/WebVttParser;

    goto/32 :goto_40

    nop

    :goto_a2
    goto/16 :goto_2a

    :goto_a3
    :try_start_3
    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v2, Landroid/media/TextTrackRegion;->mLines:I
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_2

    goto/32 :goto_73

    nop

    :goto_a4
    move/from16 v20, v6

    goto/32 :goto_95

    nop

    :goto_a5
    iget-object v10, v1, Landroid/media/WebVttParser$3;->this$0:Landroid/media/WebVttParser;

    goto/32 :goto_5c

    nop

    :goto_a6
    iput-object v14, v2, Landroid/media/TextTrackRegion;->mId:Ljava/lang/String;

    goto/32 :goto_a4

    nop

    :goto_a7
    const/16 v20, 0x0

    goto/32 :goto_61

    nop
.end method
