.class public interface abstract annotation Landroid/media/MediaResourceType;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final kBattery:I = 0x5

.field public static final kCpuBoost:I = 0x4

.field public static final kDrmSession:I = 0x6

.field public static final kGraphicMemory:I = 0x3

.field public static final kNonSecureCodec:I = 0x2

.field public static final kSecureCodec:I = 0x1

.field public static final kUnspecified:I
