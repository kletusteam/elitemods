.class Landroid/media/Cea608CCParser$CCMemory;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/Cea608CCParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CCMemory"
.end annotation


# instance fields
.field private final mBlankLine:Ljava/lang/String;

.field private mCol:I

.field private final mLines:[Landroid/media/Cea608CCParser$CCLineBuilder;

.field private mRow:I


# direct methods
.method static bridge synthetic -$$Nest$mmoveBaselineTo(Landroid/media/Cea608CCParser$CCMemory;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/media/Cea608CCParser$CCMemory;->moveBaselineTo(II)V

    return-void
.end method

.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x11

    new-array v0, v0, [Landroid/media/Cea608CCParser$CCLineBuilder;

    iput-object v0, p0, Landroid/media/Cea608CCParser$CCMemory;->mLines:[Landroid/media/Cea608CCParser$CCLineBuilder;

    const/16 v0, 0x22

    new-array v0, v0, [C

    const/16 v1, 0xa0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([CC)V

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    iput-object v1, p0, Landroid/media/Cea608CCParser$CCMemory;->mBlankLine:Ljava/lang/String;

    return-void
.end method

.method private static clamp(III)I
    .locals 1

    if-ge p0, p1, :cond_0

    move v0, p1

    goto :goto_0

    :cond_0
    if-le p0, p2, :cond_1

    move v0, p2

    goto :goto_0

    :cond_1
    move v0, p0

    :goto_0
    return v0
.end method

.method private getLineBuffer(I)Landroid/media/Cea608CCParser$CCLineBuilder;
    .locals 3

    iget-object v0, p0, Landroid/media/Cea608CCParser$CCMemory;->mLines:[Landroid/media/Cea608CCParser$CCLineBuilder;

    aget-object v1, v0, p1

    if-nez v1, :cond_0

    new-instance v1, Landroid/media/Cea608CCParser$CCLineBuilder;

    iget-object v2, p0, Landroid/media/Cea608CCParser$CCMemory;->mBlankLine:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/media/Cea608CCParser$CCLineBuilder;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, p1

    :cond_0
    iget-object v0, p0, Landroid/media/Cea608CCParser$CCMemory;->mLines:[Landroid/media/Cea608CCParser$CCLineBuilder;

    aget-object v0, v0, p1

    return-object v0
.end method

.method private moveBaselineTo(II)V
    .locals 5

    iget v0, p0, Landroid/media/Cea608CCParser$CCMemory;->mRow:I

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    move v1, p2

    if-ge p1, v1, :cond_1

    move v1, p1

    :cond_1
    if-ge v0, v1, :cond_2

    iget v1, p0, Landroid/media/Cea608CCParser$CCMemory;->mRow:I

    :cond_2
    if-ge p1, v0, :cond_3

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-ltz v0, :cond_4

    iget-object v2, p0, Landroid/media/Cea608CCParser$CCMemory;->mLines:[Landroid/media/Cea608CCParser$CCLineBuilder;

    sub-int v3, p1, v0

    iget v4, p0, Landroid/media/Cea608CCParser$CCMemory;->mRow:I

    sub-int/2addr v4, v0

    aget-object v4, v2, v4

    aput-object v4, v2, v3

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_4

    iget-object v2, p0, Landroid/media/Cea608CCParser$CCMemory;->mLines:[Landroid/media/Cea608CCParser$CCLineBuilder;

    sub-int v3, p1, v0

    iget v4, p0, Landroid/media/Cea608CCParser$CCMemory;->mRow:I

    sub-int/2addr v4, v0

    aget-object v4, v2, v4

    aput-object v4, v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    :goto_2
    sub-int v2, p1, p2

    const/4 v3, 0x0

    if-gt v0, v2, :cond_5

    iget-object v2, p0, Landroid/media/Cea608CCParser$CCMemory;->mLines:[Landroid/media/Cea608CCParser$CCLineBuilder;

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    add-int/lit8 v0, p1, 0x1

    :goto_3
    iget-object v2, p0, Landroid/media/Cea608CCParser$CCMemory;->mLines:[Landroid/media/Cea608CCParser$CCLineBuilder;

    array-length v4, v2

    if-ge v0, v4, :cond_6

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    return-void
.end method

.method private moveCursorByCol(I)V
    .locals 3

    iget v0, p0, Landroid/media/Cea608CCParser$CCMemory;->mCol:I

    add-int/2addr v0, p1

    const/4 v1, 0x1

    const/16 v2, 0x20

    invoke-static {v0, v1, v2}, Landroid/media/Cea608CCParser$CCMemory;->clamp(III)I

    move-result v0

    iput v0, p0, Landroid/media/Cea608CCParser$CCMemory;->mCol:I

    return-void
.end method

.method private moveCursorTo(II)V
    .locals 2

    const/4 v0, 0x1

    const/16 v1, 0xf

    invoke-static {p1, v0, v1}, Landroid/media/Cea608CCParser$CCMemory;->clamp(III)I

    move-result v1

    iput v1, p0, Landroid/media/Cea608CCParser$CCMemory;->mRow:I

    const/16 v1, 0x20

    invoke-static {p2, v0, v1}, Landroid/media/Cea608CCParser$CCMemory;->clamp(III)I

    move-result v0

    iput v0, p0, Landroid/media/Cea608CCParser$CCMemory;->mCol:I

    return-void
.end method

.method private moveCursorToRow(I)V
    .locals 2

    const/4 v0, 0x1

    const/16 v1, 0xf

    invoke-static {p1, v0, v1}, Landroid/media/Cea608CCParser$CCMemory;->clamp(III)I

    move-result v0

    iput v0, p0, Landroid/media/Cea608CCParser$CCMemory;->mRow:I

    return-void
.end method


# virtual methods
.method bs()V
    .locals 3

    goto/32 :goto_6

    nop

    :goto_0
    iget v1, p0, Landroid/media/Cea608CCParser$CCMemory;->mRow:I

    goto/32 :goto_d

    nop

    :goto_1
    return-void

    :goto_2
    iget-object v0, p0, Landroid/media/Cea608CCParser$CCMemory;->mLines:[Landroid/media/Cea608CCParser$CCLineBuilder;

    goto/32 :goto_0

    nop

    :goto_3
    const/16 v1, 0x20

    goto/32 :goto_4

    nop

    :goto_4
    invoke-virtual {v0, v1, v2}, Landroid/media/Cea608CCParser$CCLineBuilder;->setCharAt(IC)V

    :goto_5
    goto/32 :goto_1

    nop

    :goto_6
    const/4 v0, -0x1

    goto/32 :goto_11

    nop

    :goto_7
    if-nez v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_e

    nop

    :goto_8
    iget v0, p0, Landroid/media/Cea608CCParser$CCMemory;->mCol:I

    goto/32 :goto_f

    nop

    :goto_9
    const/16 v2, 0xa0

    goto/32 :goto_12

    nop

    :goto_a
    iget-object v0, p0, Landroid/media/Cea608CCParser$CCMemory;->mLines:[Landroid/media/Cea608CCParser$CCLineBuilder;

    goto/32 :goto_10

    nop

    :goto_b
    aget-object v0, v0, v1

    goto/32 :goto_7

    nop

    :goto_c
    if-eq v0, v1, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_2

    nop

    :goto_d
    aget-object v0, v0, v1

    goto/32 :goto_3

    nop

    :goto_e
    iget v1, p0, Landroid/media/Cea608CCParser$CCMemory;->mCol:I

    goto/32 :goto_9

    nop

    :goto_f
    const/16 v1, 0x1f

    goto/32 :goto_c

    nop

    :goto_10
    iget v1, p0, Landroid/media/Cea608CCParser$CCMemory;->mRow:I

    goto/32 :goto_b

    nop

    :goto_11
    invoke-direct {p0, v0}, Landroid/media/Cea608CCParser$CCMemory;->moveCursorByCol(I)V

    goto/32 :goto_a

    nop

    :goto_12
    invoke-virtual {v0, v1, v2}, Landroid/media/Cea608CCParser$CCLineBuilder;->setCharAt(IC)V

    goto/32 :goto_8

    nop
.end method

.method cr()V
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    invoke-direct {p0, v0, v1}, Landroid/media/Cea608CCParser$CCMemory;->moveCursorTo(II)V

    goto/32 :goto_2

    nop

    :goto_1
    add-int/2addr v0, v1

    goto/32 :goto_0

    nop

    :goto_2
    return-void

    :goto_3
    iget v0, p0, Landroid/media/Cea608CCParser$CCMemory;->mRow:I

    goto/32 :goto_4

    nop

    :goto_4
    const/4 v1, 0x1

    goto/32 :goto_1

    nop
.end method

.method der()V
    .locals 5

    goto/32 :goto_4

    nop

    :goto_0
    goto :goto_7

    :goto_1
    goto/32 :goto_17

    nop

    :goto_2
    iget-object v3, p0, Landroid/media/Cea608CCParser$CCMemory;->mLines:[Landroid/media/Cea608CCParser$CCLineBuilder;

    goto/32 :goto_d

    nop

    :goto_3
    return-void

    :goto_4
    iget-object v0, p0, Landroid/media/Cea608CCParser$CCMemory;->mLines:[Landroid/media/Cea608CCParser$CCLineBuilder;

    goto/32 :goto_25

    nop

    :goto_5
    aget-object v1, v1, v2

    goto/32 :goto_10

    nop

    :goto_6
    const/4 v0, 0x0

    :goto_7
    goto/32 :goto_24

    nop

    :goto_8
    const/16 v2, 0xa0

    goto/32 :goto_13

    nop

    :goto_9
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_0

    nop

    :goto_a
    iget v1, p0, Landroid/media/Cea608CCParser$CCMemory;->mCol:I

    :goto_b
    goto/32 :goto_2

    nop

    :goto_c
    iget-object v3, p0, Landroid/media/Cea608CCParser$CCMemory;->mLines:[Landroid/media/Cea608CCParser$CCLineBuilder;

    goto/32 :goto_14

    nop

    :goto_d
    iget v4, p0, Landroid/media/Cea608CCParser$CCMemory;->mRow:I

    goto/32 :goto_f

    nop

    :goto_e
    aget-object v0, v0, v1

    goto/32 :goto_1c

    nop

    :goto_f
    aget-object v3, v3, v4

    goto/32 :goto_1d

    nop

    :goto_10
    invoke-virtual {v1, v0}, Landroid/media/Cea608CCParser$CCLineBuilder;->charAt(I)C

    move-result v1

    goto/32 :goto_8

    nop

    :goto_11
    aput-object v2, v0, v1

    :goto_12
    goto/32 :goto_3

    nop

    :goto_13
    if-ne v1, v2, :cond_0

    goto/32 :goto_1b

    :cond_0
    goto/32 :goto_a

    nop

    :goto_14
    aget-object v3, v3, v1

    goto/32 :goto_20

    nop

    :goto_15
    iget v2, p0, Landroid/media/Cea608CCParser$CCMemory;->mRow:I

    goto/32 :goto_5

    nop

    :goto_16
    iget v1, p0, Landroid/media/Cea608CCParser$CCMemory;->mRow:I

    goto/32 :goto_19

    nop

    :goto_17
    iget-object v0, p0, Landroid/media/Cea608CCParser$CCMemory;->mLines:[Landroid/media/Cea608CCParser$CCLineBuilder;

    goto/32 :goto_16

    nop

    :goto_18
    if-lt v1, v3, :cond_1

    goto/32 :goto_23

    :cond_1
    goto/32 :goto_c

    nop

    :goto_19
    const/4 v2, 0x0

    goto/32 :goto_11

    nop

    :goto_1a
    return-void

    :goto_1b
    goto/32 :goto_9

    nop

    :goto_1c
    if-nez v0, :cond_2

    goto/32 :goto_12

    :cond_2
    goto/32 :goto_6

    nop

    :goto_1d
    invoke-virtual {v3}, Landroid/media/Cea608CCParser$CCLineBuilder;->length()I

    move-result v3

    goto/32 :goto_18

    nop

    :goto_1e
    iget-object v1, p0, Landroid/media/Cea608CCParser$CCMemory;->mLines:[Landroid/media/Cea608CCParser$CCLineBuilder;

    goto/32 :goto_15

    nop

    :goto_1f
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_22

    nop

    :goto_20
    invoke-virtual {v3, v1, v2}, Landroid/media/Cea608CCParser$CCLineBuilder;->setCharAt(IC)V

    goto/32 :goto_1f

    nop

    :goto_21
    if-lt v0, v1, :cond_3

    goto/32 :goto_1

    :cond_3
    goto/32 :goto_1e

    nop

    :goto_22
    goto/16 :goto_b

    :goto_23
    goto/32 :goto_1a

    nop

    :goto_24
    iget v1, p0, Landroid/media/Cea608CCParser$CCMemory;->mCol:I

    goto/32 :goto_21

    nop

    :goto_25
    iget v1, p0, Landroid/media/Cea608CCParser$CCMemory;->mRow:I

    goto/32 :goto_e

    nop
.end method

.method erase()V
    .locals 3

    goto/32 :goto_8

    nop

    :goto_0
    const/16 v0, 0xf

    goto/32 :goto_e

    nop

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_3

    nop

    :goto_2
    const/4 v0, 0x1

    goto/32 :goto_d

    nop

    :goto_3
    goto :goto_9

    :goto_4
    goto/32 :goto_0

    nop

    :goto_5
    if-lt v0, v2, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_b

    nop

    :goto_6
    return-void

    :goto_7
    iget-object v1, p0, Landroid/media/Cea608CCParser$CCMemory;->mLines:[Landroid/media/Cea608CCParser$CCLineBuilder;

    goto/32 :goto_c

    nop

    :goto_8
    const/4 v0, 0x0

    :goto_9
    goto/32 :goto_7

    nop

    :goto_a
    aput-object v2, v1, v0

    goto/32 :goto_1

    nop

    :goto_b
    const/4 v2, 0x0

    goto/32 :goto_a

    nop

    :goto_c
    array-length v2, v1

    goto/32 :goto_5

    nop

    :goto_d
    iput v0, p0, Landroid/media/Cea608CCParser$CCMemory;->mCol:I

    goto/32 :goto_6

    nop

    :goto_e
    iput v0, p0, Landroid/media/Cea608CCParser$CCMemory;->mRow:I

    goto/32 :goto_2

    nop
.end method

.method getStyledText(Landroid/view/accessibility/CaptioningManager$CaptionStyle;)[Landroid/text/SpannableStringBuilder;
    .locals 4

    goto/32 :goto_c

    nop

    :goto_0
    iget-object v3, p0, Landroid/media/Cea608CCParser$CCMemory;->mLines:[Landroid/media/Cea608CCParser$CCLineBuilder;

    goto/32 :goto_5

    nop

    :goto_1
    check-cast v1, [Landroid/text/SpannableStringBuilder;

    goto/32 :goto_b

    nop

    :goto_2
    const/4 v3, 0x0

    :goto_3
    goto/32 :goto_d

    nop

    :goto_4
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    goto/32 :goto_f

    nop

    :goto_5
    aget-object v3, v3, v2

    goto/32 :goto_6

    nop

    :goto_6
    if-nez v3, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_e

    nop

    :goto_7
    goto :goto_10

    :goto_8
    goto/32 :goto_15

    nop

    :goto_9
    goto :goto_3

    :goto_a
    goto/32 :goto_2

    nop

    :goto_b
    return-object v1

    :goto_c
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_13

    nop

    :goto_d
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_12

    nop

    :goto_e
    invoke-virtual {v3, p1}, Landroid/media/Cea608CCParser$CCLineBuilder;->getStyledText(Landroid/view/accessibility/CaptioningManager$CaptionStyle;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    goto/32 :goto_9

    nop

    :goto_f
    const/4 v2, 0x1

    :goto_10
    goto/32 :goto_14

    nop

    :goto_11
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_1

    nop

    :goto_12
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_7

    nop

    :goto_13
    const/16 v1, 0xf

    goto/32 :goto_4

    nop

    :goto_14
    if-le v2, v1, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_0

    nop

    :goto_15
    new-array v1, v1, [Landroid/text/SpannableStringBuilder;

    goto/32 :goto_11

    nop
.end method

.method rollUp(I)V
    .locals 6

    goto/32 :goto_25

    nop

    :goto_0
    aput-object v5, v4, v0

    goto/32 :goto_1c

    nop

    :goto_1
    goto/16 :goto_21

    :goto_2
    goto/32 :goto_19

    nop

    :goto_3
    if-lt v0, v5, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_17

    nop

    :goto_4
    const/4 v2, 0x1

    goto/32 :goto_15

    nop

    :goto_5
    const/4 v3, 0x0

    goto/32 :goto_18

    nop

    :goto_6
    iget-object v4, p0, Landroid/media/Cea608CCParser$CCMemory;->mLines:[Landroid/media/Cea608CCParser$CCLineBuilder;

    goto/32 :goto_10

    nop

    :goto_7
    if-lt v0, v4, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_6

    nop

    :goto_8
    iput v2, p0, Landroid/media/Cea608CCParser$CCMemory;->mCol:I

    goto/32 :goto_1d

    nop

    :goto_9
    iget v1, p0, Landroid/media/Cea608CCParser$CCMemory;->mRow:I

    goto/32 :goto_1f

    nop

    :goto_a
    goto :goto_1a

    :goto_b
    goto/32 :goto_8

    nop

    :goto_c
    const/4 v1, 0x1

    :goto_d
    goto/32 :goto_20

    nop

    :goto_e
    if-lt v1, v2, :cond_2

    goto/32 :goto_d

    :cond_2
    goto/32 :goto_c

    nop

    :goto_f
    iget v4, p0, Landroid/media/Cea608CCParser$CCMemory;->mRow:I

    goto/32 :goto_7

    nop

    :goto_10
    add-int/lit8 v5, v0, 0x1

    goto/32 :goto_14

    nop

    :goto_11
    aput-object v3, v1, v0

    goto/32 :goto_24

    nop

    :goto_12
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_a

    nop

    :goto_13
    iget-object v4, p0, Landroid/media/Cea608CCParser$CCMemory;->mLines:[Landroid/media/Cea608CCParser$CCLineBuilder;

    goto/32 :goto_16

    nop

    :goto_14
    aget-object v5, v4, v5

    goto/32 :goto_0

    nop

    :goto_15
    add-int/2addr v1, v2

    goto/32 :goto_e

    nop

    :goto_16
    array-length v5, v4

    goto/32 :goto_3

    nop

    :goto_17
    aput-object v3, v4, v0

    goto/32 :goto_12

    nop

    :goto_18
    if-le v0, v2, :cond_3

    goto/32 :goto_23

    :cond_3
    goto/32 :goto_1e

    nop

    :goto_19
    iget v0, p0, Landroid/media/Cea608CCParser$CCMemory;->mRow:I

    :goto_1a
    goto/32 :goto_13

    nop

    :goto_1b
    sub-int/2addr v1, p1

    goto/32 :goto_4

    nop

    :goto_1c
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_1

    nop

    :goto_1d
    return-void

    :goto_1e
    iget-object v1, p0, Landroid/media/Cea608CCParser$CCMemory;->mLines:[Landroid/media/Cea608CCParser$CCLineBuilder;

    goto/32 :goto_11

    nop

    :goto_1f
    sub-int v2, v1, p1

    goto/32 :goto_5

    nop

    :goto_20
    move v0, v1

    :goto_21
    goto/32 :goto_f

    nop

    :goto_22
    goto :goto_26

    :goto_23
    goto/32 :goto_1b

    nop

    :goto_24
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_22

    nop

    :goto_25
    const/4 v0, 0x0

    :goto_26
    goto/32 :goto_9

    nop
.end method

.method tab(I)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-direct {p0, p1}, Landroid/media/Cea608CCParser$CCMemory;->moveCursorByCol(I)V

    goto/32 :goto_0

    nop
.end method

.method writeMidRowCode(Landroid/media/Cea608CCParser$StyleCode;)V
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    invoke-direct {p0, v0}, Landroid/media/Cea608CCParser$CCMemory;->getLineBuffer(I)Landroid/media/Cea608CCParser$CCLineBuilder;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_1
    const/4 v0, 0x1

    goto/32 :goto_3

    nop

    :goto_2
    return-void

    :goto_3
    invoke-direct {p0, v0}, Landroid/media/Cea608CCParser$CCMemory;->moveCursorByCol(I)V

    goto/32 :goto_2

    nop

    :goto_4
    iget v1, p0, Landroid/media/Cea608CCParser$CCMemory;->mCol:I

    goto/32 :goto_6

    nop

    :goto_5
    iget v0, p0, Landroid/media/Cea608CCParser$CCMemory;->mRow:I

    goto/32 :goto_0

    nop

    :goto_6
    invoke-virtual {v0, v1, p1}, Landroid/media/Cea608CCParser$CCLineBuilder;->setMidRowAt(ILandroid/media/Cea608CCParser$StyleCode;)V

    goto/32 :goto_1

    nop
.end method

.method writePAC(Landroid/media/Cea608CCParser$PAC;)V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    const/4 v1, 0x1

    goto/32 :goto_c

    nop

    :goto_1
    invoke-virtual {p1}, Landroid/media/Cea608CCParser$PAC;->isIndentPAC()Z

    move-result v0

    goto/32 :goto_4

    nop

    :goto_2
    invoke-virtual {p1}, Landroid/media/Cea608CCParser$PAC;->getCol()I

    move-result v1

    goto/32 :goto_3

    nop

    :goto_3
    invoke-direct {p0, v0, v1}, Landroid/media/Cea608CCParser$CCMemory;->moveCursorTo(II)V

    goto/32 :goto_9

    nop

    :goto_4
    if-nez v0, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_8

    nop

    :goto_5
    invoke-virtual {p1}, Landroid/media/Cea608CCParser$PAC;->getRow()I

    move-result v0

    goto/32 :goto_0

    nop

    :goto_6
    iget v1, p0, Landroid/media/Cea608CCParser$CCMemory;->mCol:I

    goto/32 :goto_f

    nop

    :goto_7
    iget v0, p0, Landroid/media/Cea608CCParser$CCMemory;->mRow:I

    goto/32 :goto_e

    nop

    :goto_8
    invoke-virtual {p1}, Landroid/media/Cea608CCParser$PAC;->getRow()I

    move-result v0

    goto/32 :goto_2

    nop

    :goto_9
    goto :goto_d

    :goto_a
    goto/32 :goto_5

    nop

    :goto_b
    return-void

    :goto_c
    invoke-direct {p0, v0, v1}, Landroid/media/Cea608CCParser$CCMemory;->moveCursorTo(II)V

    :goto_d
    goto/32 :goto_7

    nop

    :goto_e
    invoke-direct {p0, v0}, Landroid/media/Cea608CCParser$CCMemory;->getLineBuffer(I)Landroid/media/Cea608CCParser$CCLineBuilder;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_f
    invoke-virtual {v0, v1, p1}, Landroid/media/Cea608CCParser$CCLineBuilder;->setPACAt(ILandroid/media/Cea608CCParser$PAC;)V

    goto/32 :goto_b

    nop
.end method

.method writeText(Ljava/lang/String;)V
    .locals 4

    goto/32 :goto_9

    nop

    :goto_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    goto/32 :goto_4

    nop

    :goto_1
    iget v2, p0, Landroid/media/Cea608CCParser$CCMemory;->mCol:I

    goto/32 :goto_0

    nop

    :goto_2
    const/4 v1, 0x1

    goto/32 :goto_8

    nop

    :goto_3
    iget v1, p0, Landroid/media/Cea608CCParser$CCMemory;->mRow:I

    goto/32 :goto_e

    nop

    :goto_4
    invoke-virtual {v1, v2, v3}, Landroid/media/Cea608CCParser$CCLineBuilder;->setCharAt(IC)V

    goto/32 :goto_2

    nop

    :goto_5
    if-lt v0, v1, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_3

    nop

    :goto_6
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_b

    nop

    :goto_7
    return-void

    :goto_8
    invoke-direct {p0, v1}, Landroid/media/Cea608CCParser$CCMemory;->moveCursorByCol(I)V

    goto/32 :goto_6

    nop

    :goto_9
    const/4 v0, 0x0

    :goto_a
    goto/32 :goto_d

    nop

    :goto_b
    goto :goto_a

    :goto_c
    goto/32 :goto_7

    nop

    :goto_d
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    goto/32 :goto_5

    nop

    :goto_e
    invoke-direct {p0, v1}, Landroid/media/Cea608CCParser$CCMemory;->getLineBuffer(I)Landroid/media/Cea608CCParser$CCLineBuilder;

    move-result-object v1

    goto/32 :goto_1

    nop
.end method
