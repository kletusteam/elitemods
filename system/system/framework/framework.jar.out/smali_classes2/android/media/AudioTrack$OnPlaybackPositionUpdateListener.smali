.class public interface abstract Landroid/media/AudioTrack$OnPlaybackPositionUpdateListener;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/AudioTrack;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnPlaybackPositionUpdateListener"
.end annotation


# virtual methods
.method public abstract onMarkerReached(Landroid/media/AudioTrack;)V
.end method

.method public abstract onPeriodicNotification(Landroid/media/AudioTrack;)V
.end method
