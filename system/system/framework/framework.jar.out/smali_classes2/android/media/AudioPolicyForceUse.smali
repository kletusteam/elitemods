.class public interface abstract annotation Landroid/media/AudioPolicyForceUse;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final COMMUNICATION:I = 0x0

.field public static final DOCK:I = 0x3

.field public static final ENCODED_SURROUND:I = 0x6

.field public static final HDMI_SYSTEM_AUDIO:I = 0x5

.field public static final LB_TEST:I = 0x8

.field public static final LOOPBACK:I = 0x9

.field public static final MEDIA:I = 0x1

.field public static final RECORD:I = 0x2

.field public static final SYSTEM:I = 0x4

.field public static final VIBRATE_RINGING:I = 0x7
