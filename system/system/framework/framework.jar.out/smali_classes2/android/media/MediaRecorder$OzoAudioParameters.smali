.class public final Landroid/media/MediaRecorder$OzoAudioParameters;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/MediaRecorder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OzoAudioParameters"
.end annotation


# static fields
.field public static final AUDIOLEVELS_NOTIFICATION:Ljava/lang/String; = "vendor.ozoaudio.audiolevels-notification.value"

.field public static final DEFAULT_AZIMUTH:D = 0.0

.field public static final DEFAULT_ELEVATION:D = 0.0

.field public static final DEFAULT_GAIN:D = 3.0

.field public static final DISABLED:Ljava/lang/String; = "off"

.field public static final ENABLED:Ljava/lang/String; = "on"

.field public static final FEAT_CUSTOM:Ljava/lang/String; = "custom"

.field public static final FEAT_FOCUS:Ljava/lang/String; = "focus"

.field public static final FEAT_FOCUSAZIMUTH:Ljava/lang/String; = "focus-azimuth"

.field public static final FEAT_FOCUSELEVATION:Ljava/lang/String; = "focus-elevation"

.field public static final FEAT_FOCUSHEIGHT:Ljava/lang/String; = "focus-height"

.field public static final FEAT_FOCUSWIDTH:Ljava/lang/String; = "focus-width"

.field public static final FEAT_NOISESUPPRESSION:Ljava/lang/String; = "ns"

.field public static final FEAT_WINDSCREEN:Ljava/lang/String; = "wnr"

.field public static final FEAT_ZOOM:Ljava/lang/String; = "zoom"

.field public static final FRAMING_NOTIFICATION:Ljava/lang/String; = "vendor.ozoaudio.ssloc-notification.value"

.field public static final GENERIC_PARAM:Ljava/lang/String; = "vendor.ozoaudio.generic.value"

.field public static final HIGH_GAIN:D = 4.0

.field public static final LOW_GAIN:D = 2.0

.field public static final MAX_GAIN:D = 5.0

.field public static final MICBLOCKING_NOTIFICATION:Ljava/lang/String; = "vendor.ozoaudio.micblocking-notification.value"

.field public static final MIN_GAIN:D = 1.0

.field public static final NS_SMART:Ljava/lang/String; = "smart"

.field public static final WINDSCREEN_NOTIFICATION:Ljava/lang/String; = "vendor.ozoaudio.wnr-notification.value"

.field public static final ZERO_GAIN:D


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
