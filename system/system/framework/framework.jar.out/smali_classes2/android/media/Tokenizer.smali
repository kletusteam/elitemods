.class Landroid/media/Tokenizer;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/Tokenizer$OnTokenListener;,
        Landroid/media/Tokenizer$TagTokenizer;,
        Landroid/media/Tokenizer$DataTokenizer;,
        Landroid/media/Tokenizer$TokenizerPhase;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Tokenizer"


# instance fields
.field private mDataTokenizer:Landroid/media/Tokenizer$TokenizerPhase;

.field private mHandledLen:I

.field private mLine:Ljava/lang/String;

.field private mListener:Landroid/media/Tokenizer$OnTokenListener;

.field private mPhase:Landroid/media/Tokenizer$TokenizerPhase;

.field private mTagTokenizer:Landroid/media/Tokenizer$TokenizerPhase;


# direct methods
.method static bridge synthetic -$$Nest$fgetmDataTokenizer(Landroid/media/Tokenizer;)Landroid/media/Tokenizer$TokenizerPhase;
    .locals 0

    iget-object p0, p0, Landroid/media/Tokenizer;->mDataTokenizer:Landroid/media/Tokenizer$TokenizerPhase;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandledLen(Landroid/media/Tokenizer;)I
    .locals 0

    iget p0, p0, Landroid/media/Tokenizer;->mHandledLen:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLine(Landroid/media/Tokenizer;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Landroid/media/Tokenizer;->mLine:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmListener(Landroid/media/Tokenizer;)Landroid/media/Tokenizer$OnTokenListener;
    .locals 0

    iget-object p0, p0, Landroid/media/Tokenizer;->mListener:Landroid/media/Tokenizer$OnTokenListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmTagTokenizer(Landroid/media/Tokenizer;)Landroid/media/Tokenizer$TokenizerPhase;
    .locals 0

    iget-object p0, p0, Landroid/media/Tokenizer;->mTagTokenizer:Landroid/media/Tokenizer$TokenizerPhase;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmHandledLen(Landroid/media/Tokenizer;I)V
    .locals 0

    iput p1, p0, Landroid/media/Tokenizer;->mHandledLen:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmPhase(Landroid/media/Tokenizer;Landroid/media/Tokenizer$TokenizerPhase;)V
    .locals 0

    iput-object p1, p0, Landroid/media/Tokenizer;->mPhase:Landroid/media/Tokenizer$TokenizerPhase;

    return-void
.end method

.method constructor <init>(Landroid/media/Tokenizer$OnTokenListener;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/media/Tokenizer$DataTokenizer;

    invoke-direct {v0, p0}, Landroid/media/Tokenizer$DataTokenizer;-><init>(Landroid/media/Tokenizer;)V

    iput-object v0, p0, Landroid/media/Tokenizer;->mDataTokenizer:Landroid/media/Tokenizer$TokenizerPhase;

    new-instance v0, Landroid/media/Tokenizer$TagTokenizer;

    invoke-direct {v0, p0}, Landroid/media/Tokenizer$TagTokenizer;-><init>(Landroid/media/Tokenizer;)V

    iput-object v0, p0, Landroid/media/Tokenizer;->mTagTokenizer:Landroid/media/Tokenizer$TokenizerPhase;

    invoke-virtual {p0}, Landroid/media/Tokenizer;->reset()V

    iput-object p1, p0, Landroid/media/Tokenizer;->mListener:Landroid/media/Tokenizer$OnTokenListener;

    return-void
.end method


# virtual methods
.method reset()V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v0, p0, Landroid/media/Tokenizer;->mDataTokenizer:Landroid/media/Tokenizer$TokenizerPhase;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-interface {v0}, Landroid/media/Tokenizer$TokenizerPhase;->start()Landroid/media/Tokenizer$TokenizerPhase;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_3
    iput-object v0, p0, Landroid/media/Tokenizer;->mPhase:Landroid/media/Tokenizer$TokenizerPhase;

    goto/32 :goto_0

    nop
.end method

.method tokenize(Ljava/lang/String;)V
    .locals 2

    goto/32 :goto_f

    nop

    :goto_0
    iget-object v1, p0, Landroid/media/Tokenizer;->mLine:Ljava/lang/String;

    goto/32 :goto_e

    nop

    :goto_1
    iget-object v0, p0, Landroid/media/Tokenizer;->mPhase:Landroid/media/Tokenizer$TokenizerPhase;

    goto/32 :goto_3

    nop

    :goto_2
    if-lt v0, v1, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_c

    nop

    :goto_3
    instance-of v0, v0, Landroid/media/Tokenizer$TagTokenizer;

    goto/32 :goto_4

    nop

    :goto_4
    if-eqz v0, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_d

    nop

    :goto_5
    return-void

    :goto_6
    invoke-interface {v0}, Landroid/media/Tokenizer$TokenizerPhase;->tokenize()V

    goto/32 :goto_11

    nop

    :goto_7
    iput-object p1, p0, Landroid/media/Tokenizer;->mLine:Ljava/lang/String;

    :goto_8
    goto/32 :goto_9

    nop

    :goto_9
    iget v0, p0, Landroid/media/Tokenizer;->mHandledLen:I

    goto/32 :goto_0

    nop

    :goto_a
    invoke-interface {v0}, Landroid/media/Tokenizer$OnTokenListener;->onLineEnd()V

    :goto_b
    goto/32 :goto_5

    nop

    :goto_c
    iget-object v0, p0, Landroid/media/Tokenizer;->mPhase:Landroid/media/Tokenizer$TokenizerPhase;

    goto/32 :goto_6

    nop

    :goto_d
    iget-object v0, p0, Landroid/media/Tokenizer;->mListener:Landroid/media/Tokenizer$OnTokenListener;

    goto/32 :goto_a

    nop

    :goto_e
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    goto/32 :goto_2

    nop

    :goto_f
    const/4 v0, 0x0

    goto/32 :goto_10

    nop

    :goto_10
    iput v0, p0, Landroid/media/Tokenizer;->mHandledLen:I

    goto/32 :goto_7

    nop

    :goto_11
    goto :goto_8

    :goto_12
    goto/32 :goto_1

    nop
.end method
