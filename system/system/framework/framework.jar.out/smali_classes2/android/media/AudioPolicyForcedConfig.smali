.class public interface abstract annotation Landroid/media/AudioPolicyForcedConfig;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final ANALOG_DOCK:I = 0x8

.field public static final BT_A2DP:I = 0x4

.field public static final BT_CAR_DOCK:I = 0x6

.field public static final BT_DESK_DOCK:I = 0x7

.field public static final BT_SCO:I = 0x3

.field public static final DIGITAL_DOCK:I = 0x9

.field public static final EARPIECE:I = 0x10

.field public static final ENCODED_SURROUND_ALWAYS:I = 0xe

.field public static final ENCODED_SURROUND_MANUAL:I = 0xf

.field public static final ENCODED_SURROUND_NEVER:I = 0xd

.field public static final HDMI_SYSTEM_AUDIO_ENFORCED:I = 0xc

.field public static final HEADPHONES:I = 0x2

.field public static final NONE:I = 0x0

.field public static final NO_BT_A2DP:I = 0xa

.field public static final SPEAKER:I = 0x1

.field public static final SYSTEM_ENFORCED:I = 0xb

.field public static final WIRED_ACCESSORY:I = 0x5
