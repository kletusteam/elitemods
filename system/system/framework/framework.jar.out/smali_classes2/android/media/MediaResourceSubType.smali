.class public interface abstract annotation Landroid/media/MediaResourceSubType;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final kAudioCodec:I = 0x1

.field public static final kImageCodec:I = 0x3

.field public static final kUnspecifiedSubType:I = 0x0

.field public static final kVideoCodec:I = 0x2
