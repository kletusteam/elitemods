.class final Landroid/media/MediaRouter$Static$Client;
.super Landroid/media/IMediaRouterClient$Stub;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/MediaRouter$Static;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "Client"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/media/MediaRouter$Static;


# direct methods
.method constructor <init>(Landroid/media/MediaRouter$Static;)V
    .locals 0

    iput-object p1, p0, Landroid/media/MediaRouter$Static$Client;->this$0:Landroid/media/MediaRouter$Static;

    invoke-direct {p0}, Landroid/media/IMediaRouterClient$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method synthetic lambda$onGroupRouteSelected$1$android-media-MediaRouter$Static$Client(Ljava/lang/String;)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    iget-object v0, p0, Landroid/media/MediaRouter$Static$Client;->this$0:Landroid/media/MediaRouter$Static;

    goto/32 :goto_3

    nop

    :goto_1
    return-void

    :goto_2
    iget-object v0, p0, Landroid/media/MediaRouter$Static$Client;->this$0:Landroid/media/MediaRouter$Static;

    goto/32 :goto_5

    nop

    :goto_3
    invoke-virtual {v0, p1}, Landroid/media/MediaRouter$Static;->handleGroupRouteSelected(Ljava/lang/String;)V

    :goto_4
    goto/32 :goto_1

    nop

    :goto_5
    iget-object v0, v0, Landroid/media/MediaRouter$Static;->mClient:Landroid/media/IMediaRouterClient;

    goto/32 :goto_6

    nop

    :goto_6
    if-eq p0, v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_0

    nop
.end method

.method synthetic lambda$onRestoreRoute$0$android-media-MediaRouter$Static$Client()V
    .locals 3

    goto/32 :goto_2e

    nop

    :goto_0
    iget-object v0, v0, Landroid/media/MediaRouter$Static;->mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_35

    nop

    :goto_1
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_1f

    nop

    :goto_2
    iget-object v0, p0, Landroid/media/MediaRouter$Static$Client;->this$0:Landroid/media/MediaRouter$Static;

    goto/32 :goto_d

    nop

    :goto_3
    iget-object v0, p0, Landroid/media/MediaRouter$Static$Client;->this$0:Landroid/media/MediaRouter$Static;

    goto/32 :goto_4

    nop

    :goto_4
    iget-object v0, v0, Landroid/media/MediaRouter$Static;->mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_34

    nop

    :goto_5
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_10

    nop

    :goto_6
    iget-object v0, v0, Landroid/media/MediaRouter$Static;->mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_13

    nop

    :goto_7
    if-eqz v0, :cond_0

    goto/32 :goto_1e

    :cond_0
    goto/32 :goto_1d

    nop

    :goto_8
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_2d

    nop

    :goto_9
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_31

    nop

    :goto_a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_26

    nop

    :goto_b
    iget-object v0, v0, Landroid/media/MediaRouter$Static;->mClient:Landroid/media/IMediaRouterClient;

    goto/32 :goto_1c

    nop

    :goto_c
    if-eqz v0, :cond_1

    goto/32 :goto_1e

    :cond_1
    goto/32 :goto_15

    nop

    :goto_d
    iget-object v0, v0, Landroid/media/MediaRouter$Static;->mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_24

    nop

    :goto_e
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_22

    nop

    :goto_f
    iget-object v0, v0, Landroid/media/MediaRouter$Static;->mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_14

    nop

    :goto_10
    goto/16 :goto_27

    :goto_11
    goto/32 :goto_21

    nop

    :goto_12
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_18

    nop

    :goto_13
    if-nez v0, :cond_2

    goto/32 :goto_11

    :cond_2
    goto/32 :goto_12

    nop

    :goto_14
    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->select()V

    goto/32 :goto_2f

    nop

    :goto_15
    iget-object v0, p0, Landroid/media/MediaRouter$Static$Client;->this$0:Landroid/media/MediaRouter$Static;

    goto/32 :goto_0

    nop

    :goto_16
    iget-object v0, p0, Landroid/media/MediaRouter$Static$Client;->this$0:Landroid/media/MediaRouter$Static;

    goto/32 :goto_f

    nop

    :goto_17
    if-nez v0, :cond_3

    goto/32 :goto_30

    :cond_3
    goto/32 :goto_2

    nop

    :goto_18
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1b

    nop

    :goto_19
    iget-object v2, p0, Landroid/media/MediaRouter$Static$Client;->this$0:Landroid/media/MediaRouter$Static;

    goto/32 :goto_8

    nop

    :goto_1a
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_19

    nop

    :goto_1b
    const-string v2, "onRestoreRoute() : selectedRoute="

    goto/32 :goto_1a

    nop

    :goto_1c
    if-eq p0, v0, :cond_4

    goto/32 :goto_30

    :cond_4
    goto/32 :goto_23

    nop

    :goto_1d
    goto :goto_30

    :goto_1e
    goto/32 :goto_3

    nop

    :goto_1f
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_a

    nop

    :goto_20
    return-void

    :goto_21
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_9

    nop

    :goto_22
    iget-object v2, p0, Landroid/media/MediaRouter$Static$Client;->this$0:Landroid/media/MediaRouter$Static;

    goto/32 :goto_1

    nop

    :goto_23
    iget-object v0, p0, Landroid/media/MediaRouter$Static$Client;->this$0:Landroid/media/MediaRouter$Static;

    goto/32 :goto_37

    nop

    :goto_24
    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->isDefault()Z

    move-result v0

    goto/32 :goto_c

    nop

    :goto_25
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_2c

    nop

    :goto_26
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_27
    goto/32 :goto_16

    nop

    :goto_28
    iget-object v0, p0, Landroid/media/MediaRouter$Static$Client;->this$0:Landroid/media/MediaRouter$Static;

    goto/32 :goto_6

    nop

    :goto_29
    const-string v2, ", a2dpRoute="

    goto/32 :goto_36

    nop

    :goto_2a
    const-string v1, "MediaRouter"

    goto/32 :goto_2b

    nop

    :goto_2b
    if-nez v0, :cond_5

    goto/32 :goto_11

    :cond_5
    goto/32 :goto_28

    nop

    :goto_2c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_2d
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_29

    nop

    :goto_2e
    iget-object v0, p0, Landroid/media/MediaRouter$Static$Client;->this$0:Landroid/media/MediaRouter$Static;

    goto/32 :goto_b

    nop

    :goto_2f
    return-void

    :goto_30
    goto/32 :goto_20

    nop

    :goto_31
    const-string v2, "onRestoreRoute() : route="

    goto/32 :goto_e

    nop

    :goto_32
    iget-object v2, v2, Landroid/media/MediaRouter$Static;->mBluetoothA2dpRoute:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_25

    nop

    :goto_33
    iget-object v2, p0, Landroid/media/MediaRouter$Static$Client;->this$0:Landroid/media/MediaRouter$Static;

    goto/32 :goto_32

    nop

    :goto_34
    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->isDefault()Z

    move-result v0

    goto/32 :goto_2a

    nop

    :goto_35
    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->isBluetooth()Z

    move-result v0

    goto/32 :goto_7

    nop

    :goto_36
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_33

    nop

    :goto_37
    iget-object v0, v0, Landroid/media/MediaRouter$Static;->mSelectedRoute:Landroid/media/MediaRouter$RouteInfo;

    goto/32 :goto_17

    nop
.end method

.method public onGroupRouteSelected(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Landroid/media/MediaRouter$Static$Client;->this$0:Landroid/media/MediaRouter$Static;

    iget-object v0, v0, Landroid/media/MediaRouter$Static;->mHandler:Landroid/os/Handler;

    new-instance v1, Landroid/media/MediaRouter$Static$Client$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Landroid/media/MediaRouter$Static$Client$$ExternalSyntheticLambda0;-><init>(Landroid/media/MediaRouter$Static$Client;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onRestoreRoute()V
    .locals 2

    iget-object v0, p0, Landroid/media/MediaRouter$Static$Client;->this$0:Landroid/media/MediaRouter$Static;

    iget-object v0, v0, Landroid/media/MediaRouter$Static;->mHandler:Landroid/os/Handler;

    new-instance v1, Landroid/media/MediaRouter$Static$Client$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Landroid/media/MediaRouter$Static$Client$$ExternalSyntheticLambda1;-><init>(Landroid/media/MediaRouter$Static$Client;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onStateChanged()V
    .locals 2

    iget-object v0, p0, Landroid/media/MediaRouter$Static$Client;->this$0:Landroid/media/MediaRouter$Static;

    iget-object v0, v0, Landroid/media/MediaRouter$Static;->mHandler:Landroid/os/Handler;

    new-instance v1, Landroid/media/MediaRouter$Static$Client$1;

    invoke-direct {v1, p0}, Landroid/media/MediaRouter$Static$Client$1;-><init>(Landroid/media/MediaRouter$Static$Client;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
