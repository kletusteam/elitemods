.class Landroid/media/MediaRecorder$EventHandler;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/MediaRecorder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventHandler"
.end annotation


# static fields
.field private static final MEDIA_RECORDER_AUDIO_ROUTING_CHANGED:I = 0x2710

.field private static final MEDIA_RECORDER_EVENT_ERROR:I = 0x1

.field private static final MEDIA_RECORDER_EVENT_INFO:I = 0x2

.field private static final MEDIA_RECORDER_EVENT_LIST_END:I = 0x63

.field private static final MEDIA_RECORDER_EVENT_LIST_START:I = 0x1

.field private static final MEDIA_RECORDER_OZOAUDIO_AUDIOLEVELS_EVENT:I = 0x7f100005

.field private static final MEDIA_RECORDER_OZOAUDIO_MICBLOCKING_EVENT:I = 0x7f100006

.field private static final MEDIA_RECORDER_OZOAUDIO_SSLOC_EVENT:I = 0x7f100002

.field private static final MEDIA_RECORDER_OZOAUDIO_SSLOC_SECTORS1_EVENT:I = 0x7f100003

.field private static final MEDIA_RECORDER_OZOAUDIO_SSLOC_SECTORS2_EVENT:I = 0x7f100004

.field private static final MEDIA_RECORDER_OZOAUDIO_WNR_EVENT:I = 0x7f100001

.field private static final MEDIA_RECORDER_TRACK_EVENT_ERROR:I = 0x64

.field private static final MEDIA_RECORDER_TRACK_EVENT_INFO:I = 0x65

.field private static final MEDIA_RECORDER_TRACK_EVENT_LIST_END:I = 0x3e8

.field private static final MEDIA_RECORDER_TRACK_EVENT_LIST_START:I = 0x64


# instance fields
.field private mMediaRecorder:Landroid/media/MediaRecorder;

.field final synthetic this$0:Landroid/media/MediaRecorder;


# direct methods
.method public constructor <init>(Landroid/media/MediaRecorder;Landroid/media/MediaRecorder;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Landroid/media/MediaRecorder$EventHandler;->this$0:Landroid/media/MediaRecorder;

    invoke-direct {p0, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p2, p0, Landroid/media/MediaRecorder$EventHandler;->mMediaRecorder:Landroid/media/MediaRecorder;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10

    iget-object v0, p0, Landroid/media/MediaRecorder$EventHandler;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-static {v0}, Landroid/media/MediaRecorder;->-$$Nest$fgetmNativeContext(Landroid/media/MediaRecorder;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-string v0, "MediaRecorder"

    const-string v1, "mediarecorder went away with unhandled events"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    const-string v0, "MediaRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown message type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :sswitch_0
    iget-object v0, p0, Landroid/media/MediaRecorder$EventHandler;->this$0:Landroid/media/MediaRecorder;

    invoke-static {v0}, Landroid/media/MediaRecorder;->-$$Nest$fgetmOnEventListener(Landroid/media/MediaRecorder;)Landroid/media/MediaRecorder$OnEventListener;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/media/MediaRecorder$EventHandler;->this$0:Landroid/media/MediaRecorder;

    invoke-static {v0}, Landroid/media/MediaRecorder;->-$$Nest$fgetmOnEventListener(Landroid/media/MediaRecorder;)Landroid/media/MediaRecorder$OnEventListener;

    move-result-object v0

    iget-object v1, p0, Landroid/media/MediaRecorder$EventHandler;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget v3, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v0, v1, v2, v3}, Landroid/media/MediaRecorder$OnEventListener;->onMicBlockingEvent(Landroid/media/MediaRecorder;II)V

    :cond_1
    return-void

    :sswitch_1
    iget-object v0, p0, Landroid/media/MediaRecorder$EventHandler;->this$0:Landroid/media/MediaRecorder;

    invoke-static {v0}, Landroid/media/MediaRecorder;->-$$Nest$fgetmOnEventListener(Landroid/media/MediaRecorder;)Landroid/media/MediaRecorder$OnEventListener;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/media/MediaRecorder$EventHandler;->this$0:Landroid/media/MediaRecorder;

    invoke-static {v0}, Landroid/media/MediaRecorder;->-$$Nest$fgetmOnEventListener(Landroid/media/MediaRecorder;)Landroid/media/MediaRecorder$OnEventListener;

    move-result-object v0

    iget-object v1, p0, Landroid/media/MediaRecorder$EventHandler;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget v3, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v0, v1, v2, v3}, Landroid/media/MediaRecorder$OnEventListener;->onAudioLevelsEvent(Landroid/media/MediaRecorder;II)V

    :cond_2
    return-void

    :sswitch_2
    iget-object v0, p0, Landroid/media/MediaRecorder$EventHandler;->this$0:Landroid/media/MediaRecorder;

    invoke-static {v0}, Landroid/media/MediaRecorder;->-$$Nest$fgetmOnEventListener(Landroid/media/MediaRecorder;)Landroid/media/MediaRecorder$OnEventListener;

    move-result-object v0

    if-eqz v0, :cond_3

    iget v0, p1, Landroid/os/Message;->arg1:I

    and-int/lit16 v0, v0, 0x1ff

    iget v1, p1, Landroid/os/Message;->arg1:I

    shr-int/lit8 v1, v1, 0x10

    and-int/lit16 v1, v1, 0x1ff

    iget v2, p1, Landroid/os/Message;->arg2:I

    iget-object v3, p0, Landroid/media/MediaRecorder$EventHandler;->this$0:Landroid/media/MediaRecorder;

    invoke-static {v3}, Landroid/media/MediaRecorder;->-$$Nest$fgetmOnEventListener(Landroid/media/MediaRecorder;)Landroid/media/MediaRecorder$OnEventListener;

    move-result-object v3

    iget-object v4, p0, Landroid/media/MediaRecorder$EventHandler;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-interface {v3, v4, v0, v1, v2}, Landroid/media/MediaRecorder$OnEventListener;->onSourceSectorWidthEvent(Landroid/media/MediaRecorder;III)V

    :cond_3
    return-void

    :sswitch_3
    iget-object v0, p0, Landroid/media/MediaRecorder$EventHandler;->this$0:Landroid/media/MediaRecorder;

    invoke-static {v0}, Landroid/media/MediaRecorder;->-$$Nest$fgetmOnEventListener(Landroid/media/MediaRecorder;)Landroid/media/MediaRecorder$OnEventListener;

    move-result-object v0

    if-eqz v0, :cond_4

    iget v0, p1, Landroid/os/Message;->arg1:I

    and-int/lit16 v0, v0, 0x1ff

    add-int/lit16 v0, v0, -0xb4

    iget v1, p1, Landroid/os/Message;->arg1:I

    shr-int/lit8 v1, v1, 0x10

    and-int/lit16 v1, v1, 0x1ff

    add-int/lit8 v1, v1, -0x5a

    iget v2, p1, Landroid/os/Message;->arg2:I

    iget-object v3, p0, Landroid/media/MediaRecorder$EventHandler;->this$0:Landroid/media/MediaRecorder;

    invoke-static {v3}, Landroid/media/MediaRecorder;->-$$Nest$fgetmOnEventListener(Landroid/media/MediaRecorder;)Landroid/media/MediaRecorder$OnEventListener;

    move-result-object v3

    iget-object v4, p0, Landroid/media/MediaRecorder$EventHandler;->mMediaRecorder:Landroid/media/MediaRecorder;

    invoke-interface {v3, v4, v0, v1, v2}, Landroid/media/MediaRecorder$OnEventListener;->onSourceSectorAzimuthEvent(Landroid/media/MediaRecorder;III)V

    :cond_4
    return-void

    :sswitch_4
    iget-object v0, p0, Landroid/media/MediaRecorder$EventHandler;->this$0:Landroid/media/MediaRecorder;

    invoke-static {v0}, Landroid/media/MediaRecorder;->-$$Nest$fgetmOnEventListener(Landroid/media/MediaRecorder;)Landroid/media/MediaRecorder$OnEventListener;

    move-result-object v0

    if-eqz v0, :cond_5

    iget v0, p1, Landroid/os/Message;->arg1:I

    shr-int/lit8 v0, v0, 0xf

    and-int/lit16 v0, v0, 0x1ff

    add-int/lit16 v0, v0, -0xb4

    iget v1, p1, Landroid/os/Message;->arg1:I

    shr-int/lit8 v1, v1, 0x7

    and-int/lit16 v1, v1, 0xff

    add-int/lit8 v7, v1, -0x5a

    iget v1, p1, Landroid/os/Message;->arg1:I

    and-int/lit8 v8, v1, 0x7f

    iget v9, p1, Landroid/os/Message;->arg2:I

    iget-object v1, p0, Landroid/media/MediaRecorder$EventHandler;->this$0:Landroid/media/MediaRecorder;

    invoke-static {v1}, Landroid/media/MediaRecorder;->-$$Nest$fgetmOnEventListener(Landroid/media/MediaRecorder;)Landroid/media/MediaRecorder$OnEventListener;

    move-result-object v1

    iget-object v2, p0, Landroid/media/MediaRecorder$EventHandler;->mMediaRecorder:Landroid/media/MediaRecorder;

    move v3, v0

    move v4, v7

    move v5, v8

    move v6, v9

    invoke-interface/range {v1 .. v6}, Landroid/media/MediaRecorder$OnEventListener;->onSourceLocalizationEvent(Landroid/media/MediaRecorder;IIII)V

    :cond_5
    return-void

    :sswitch_5
    iget-object v0, p0, Landroid/media/MediaRecorder$EventHandler;->this$0:Landroid/media/MediaRecorder;

    invoke-static {v0}, Landroid/media/MediaRecorder;->-$$Nest$fgetmOnEventListener(Landroid/media/MediaRecorder;)Landroid/media/MediaRecorder$OnEventListener;

    move-result-object v0

    if-eqz v0, :cond_7

    iget v0, p1, Landroid/os/Message;->arg2:I

    const/4 v1, 0x1

    shr-int/2addr v0, v1

    and-int/lit16 v0, v0, 0xff

    iget v2, p1, Landroid/os/Message;->arg2:I

    and-int/2addr v2, v1

    if-ne v2, v1, :cond_6

    goto :goto_0

    :cond_6
    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Landroid/media/MediaRecorder$EventHandler;->this$0:Landroid/media/MediaRecorder;

    invoke-static {v2}, Landroid/media/MediaRecorder;->-$$Nest$fgetmOnEventListener(Landroid/media/MediaRecorder;)Landroid/media/MediaRecorder$OnEventListener;

    move-result-object v2

    iget-object v3, p0, Landroid/media/MediaRecorder$EventHandler;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3, v4, v1, v0}, Landroid/media/MediaRecorder$OnEventListener;->onWindEvent(Landroid/media/MediaRecorder;IZI)V

    :cond_7
    return-void

    :sswitch_6
    invoke-static {}, Landroid/media/AudioManager;->resetAudioPortGeneration()I

    iget-object v0, p0, Landroid/media/MediaRecorder$EventHandler;->this$0:Landroid/media/MediaRecorder;

    invoke-static {v0}, Landroid/media/MediaRecorder;->-$$Nest$fgetmRoutingChangeListeners(Landroid/media/MediaRecorder;)Landroid/util/ArrayMap;

    move-result-object v0

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Landroid/media/MediaRecorder$EventHandler;->this$0:Landroid/media/MediaRecorder;

    invoke-static {v1}, Landroid/media/MediaRecorder;->-$$Nest$fgetmRoutingChangeListeners(Landroid/media/MediaRecorder;)Landroid/util/ArrayMap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/NativeRoutingEventHandlerDelegate;

    invoke-virtual {v2}, Landroid/media/NativeRoutingEventHandlerDelegate;->notifyClient()V

    goto :goto_1

    :cond_8
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :sswitch_7
    iget-object v0, p0, Landroid/media/MediaRecorder$EventHandler;->this$0:Landroid/media/MediaRecorder;

    invoke-static {v0}, Landroid/media/MediaRecorder;->-$$Nest$fgetmOnInfoListener(Landroid/media/MediaRecorder;)Landroid/media/MediaRecorder$OnInfoListener;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Landroid/media/MediaRecorder$EventHandler;->this$0:Landroid/media/MediaRecorder;

    invoke-static {v0}, Landroid/media/MediaRecorder;->-$$Nest$fgetmOnInfoListener(Landroid/media/MediaRecorder;)Landroid/media/MediaRecorder$OnInfoListener;

    move-result-object v0

    iget-object v1, p0, Landroid/media/MediaRecorder$EventHandler;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget v3, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v0, v1, v2, v3}, Landroid/media/MediaRecorder$OnInfoListener;->onInfo(Landroid/media/MediaRecorder;II)V

    :cond_9
    return-void

    :sswitch_8
    iget-object v0, p0, Landroid/media/MediaRecorder$EventHandler;->this$0:Landroid/media/MediaRecorder;

    invoke-static {v0}, Landroid/media/MediaRecorder;->-$$Nest$fgetmOnErrorListener(Landroid/media/MediaRecorder;)Landroid/media/MediaRecorder$OnErrorListener;

    move-result-object v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Landroid/media/MediaRecorder$EventHandler;->this$0:Landroid/media/MediaRecorder;

    invoke-static {v0}, Landroid/media/MediaRecorder;->-$$Nest$fgetmOnErrorListener(Landroid/media/MediaRecorder;)Landroid/media/MediaRecorder$OnErrorListener;

    move-result-object v0

    iget-object v1, p0, Landroid/media/MediaRecorder$EventHandler;->mMediaRecorder:Landroid/media/MediaRecorder;

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget v3, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v0, v1, v2, v3}, Landroid/media/MediaRecorder$OnErrorListener;->onError(Landroid/media/MediaRecorder;II)V

    :cond_a
    return-void

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_8
        0x2 -> :sswitch_7
        0x64 -> :sswitch_8
        0x65 -> :sswitch_7
        0x2710 -> :sswitch_6
        0x7f100001 -> :sswitch_5
        0x7f100002 -> :sswitch_4
        0x7f100003 -> :sswitch_3
        0x7f100004 -> :sswitch_2
        0x7f100005 -> :sswitch_1
        0x7f100006 -> :sswitch_0
    .end sparse-switch
.end method
