.class public interface abstract annotation Landroid/media/soundtrigger/RecognitionStatus;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final ABORTED:I = 0x1

.field public static final FAILURE:I = 0x2

.field public static final FIRST_SUCCESS:I = 0x4

.field public static final FORCED:I = 0x3

.field public static final INVALID:I = -0x1

.field public static final SS_ALL_FAILURE:I = 0x7

.field public static final SS_CNN_FAILURE:I = 0x5

.field public static final SS_VOP_FAILURE:I = 0x6

.field public static final SUCCESS:I
