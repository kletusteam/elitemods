.class public interface abstract annotation Landroid/media/soundtrigger/RecognitionMode;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final GENERIC_TRIGGER:I = 0x8

.field public static final USER_AUTHENTICATION:I = 0x4

.field public static final USER_IDENTIFICATION:I = 0x2

.field public static final VOICE_TRIGGER:I = 0x1
