.class public interface abstract annotation Landroid/media/soundtrigger/Status;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final DEAD_OBJECT:I = 0x4

.field public static final INTERNAL_ERROR:I = 0x5

.field public static final INVALID:I = -0x1

.field public static final OPERATION_NOT_SUPPORTED:I = 0x2

.field public static final RESOURCE_CONTENTION:I = 0x1

.field public static final SUCCESS:I = 0x0

.field public static final TEMPORARY_PERMISSION_DENIED:I = 0x3
