.class Landroid/media/Cea608CCParser$CCLineBuilder;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/Cea608CCParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CCLineBuilder"
.end annotation


# instance fields
.field private final mDisplayChars:Ljava/lang/StringBuilder;

.field private final mMidRowStyles:[Landroid/media/Cea608CCParser$StyleCode;

.field private final mPACStyles:[Landroid/media/Cea608CCParser$StyleCode;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroid/media/Cea608CCParser$CCLineBuilder;->mDisplayChars:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    new-array v1, v1, [Landroid/media/Cea608CCParser$StyleCode;

    iput-object v1, p0, Landroid/media/Cea608CCParser$CCLineBuilder;->mMidRowStyles:[Landroid/media/Cea608CCParser$StyleCode;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    new-array v0, v0, [Landroid/media/Cea608CCParser$StyleCode;

    iput-object v0, p0, Landroid/media/Cea608CCParser$CCLineBuilder;->mPACStyles:[Landroid/media/Cea608CCParser$StyleCode;

    return-void
.end method


# virtual methods
.method applyStyleSpan(Landroid/text/SpannableStringBuilder;Landroid/media/Cea608CCParser$StyleCode;II)V
    .locals 3

    goto/32 :goto_d

    nop

    :goto_0
    const/4 v2, 0x2

    goto/32 :goto_e

    nop

    :goto_1
    const/16 v1, 0x21

    goto/32 :goto_6

    nop

    :goto_2
    new-instance v0, Landroid/text/style/StyleSpan;

    goto/32 :goto_0

    nop

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_9

    nop

    :goto_4
    invoke-virtual {p1, v0, p3, p4, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :goto_5
    goto/32 :goto_b

    nop

    :goto_6
    if-nez v0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_2

    nop

    :goto_7
    invoke-virtual {p1, v0, p3, p4, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :goto_8
    goto/32 :goto_a

    nop

    :goto_9
    new-instance v0, Landroid/text/style/UnderlineSpan;

    goto/32 :goto_c

    nop

    :goto_a
    return-void

    :goto_b
    invoke-virtual {p2}, Landroid/media/Cea608CCParser$StyleCode;->isUnderline()Z

    move-result v0

    goto/32 :goto_3

    nop

    :goto_c
    invoke-direct {v0}, Landroid/text/style/UnderlineSpan;-><init>()V

    goto/32 :goto_7

    nop

    :goto_d
    invoke-virtual {p2}, Landroid/media/Cea608CCParser$StyleCode;->isItalics()Z

    move-result v0

    goto/32 :goto_1

    nop

    :goto_e
    invoke-direct {v0, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    goto/32 :goto_4

    nop
.end method

.method charAt(I)C
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Landroid/media/Cea608CCParser$CCLineBuilder;->mDisplayChars:Ljava/lang/StringBuilder;

    goto/32 :goto_0

    nop

    :goto_2
    return v0
.end method

.method getStyledText(Landroid/view/accessibility/CaptioningManager$CaptionStyle;)Landroid/text/SpannableStringBuilder;
    .locals 10

    goto/32 :goto_40

    nop

    :goto_0
    const/4 v1, -0x1

    :goto_1
    nop

    goto/32 :goto_19

    nop

    :goto_2
    if-eq v6, v7, :cond_0

    goto/32 :goto_3f

    :cond_0
    goto/32 :goto_2f

    nop

    :goto_3
    const/4 v2, 0x0

    goto/32 :goto_1b

    nop

    :goto_4
    iget-object v1, p0, Landroid/media/Cea608CCParser$CCLineBuilder;->mDisplayChars:Ljava/lang/StringBuilder;

    goto/32 :goto_17

    nop

    :goto_5
    add-int/lit8 v7, v2, 0x1

    :goto_6
    goto/32 :goto_1a

    nop

    :goto_7
    return-object v0

    :goto_8
    iget-object v8, p0, Landroid/media/Cea608CCParser$CCLineBuilder;->mDisplayChars:Ljava/lang/StringBuilder;

    goto/32 :goto_47

    nop

    :goto_9
    if-lt v2, v5, :cond_1

    goto/32 :goto_23

    :cond_1
    goto/32 :goto_35

    nop

    :goto_a
    iget-object v5, p0, Landroid/media/Cea608CCParser$CCLineBuilder;->mDisplayChars:Ljava/lang/StringBuilder;

    goto/32 :goto_1e

    nop

    :goto_b
    if-ltz v1, :cond_2

    goto/32 :goto_3d

    :cond_2
    :goto_c
    goto/32 :goto_3c

    nop

    :goto_d
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v6

    goto/32 :goto_2b

    nop

    :goto_e
    goto :goto_1

    :goto_f
    goto/32 :goto_42

    nop

    :goto_10
    const/16 v9, 0x21

    goto/32 :goto_14

    nop

    :goto_11
    invoke-virtual {p0, v0, v5, v3, v2}, Landroid/media/Cea608CCParser$CCLineBuilder;->applyStyleSpan(Landroid/text/SpannableStringBuilder;Landroid/media/Cea608CCParser$StyleCode;II)V

    :goto_12
    goto/32 :goto_24

    nop

    :goto_13
    if-nez v7, :cond_3

    goto/32 :goto_3d

    :cond_3
    goto/32 :goto_29

    nop

    :goto_14
    invoke-virtual {v0, v8, v6, v7, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/32 :goto_4a

    nop

    :goto_15
    if-eq v8, v7, :cond_4

    goto/32 :goto_2e

    :cond_4
    goto/32 :goto_48

    nop

    :goto_16
    aget-object v5, v6, v2

    goto/32 :goto_1f

    nop

    :goto_17
    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    goto/32 :goto_31

    nop

    :goto_18
    if-gez v3, :cond_5

    goto/32 :goto_12

    :cond_5
    goto/32 :goto_34

    nop

    :goto_19
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_22

    nop

    :goto_1a
    new-instance v8, Landroid/media/Cea608CCParser$MutableBackgroundColorSpan;

    goto/32 :goto_2a

    nop

    :goto_1b
    const/4 v3, -0x1

    goto/32 :goto_38

    nop

    :goto_1c
    iget-object v6, p0, Landroid/media/Cea608CCParser$CCLineBuilder;->mDisplayChars:Ljava/lang/StringBuilder;

    goto/32 :goto_d

    nop

    :goto_1d
    aget-object v7, v6, v2

    goto/32 :goto_13

    nop

    :goto_1e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    goto/32 :goto_9

    nop

    :goto_1f
    goto/16 :goto_3d

    :goto_20
    goto/32 :goto_30

    nop

    :goto_21
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v6

    goto/32 :goto_33

    nop

    :goto_22
    goto :goto_39

    :goto_23
    goto/32 :goto_7

    nop

    :goto_24
    move v3, v2

    :goto_25
    goto/32 :goto_41

    nop

    :goto_26
    add-int/lit8 v6, v1, -0x1

    :goto_27
    goto/32 :goto_8

    nop

    :goto_28
    move-object v4, v5

    goto/32 :goto_18

    nop

    :goto_29
    if-gez v3, :cond_6

    goto/32 :goto_c

    :cond_6
    goto/32 :goto_b

    nop

    :goto_2a
    iget v9, p1, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->backgroundColor:I

    goto/32 :goto_2c

    nop

    :goto_2b
    const/16 v7, 0x20

    goto/32 :goto_2

    nop

    :goto_2c
    invoke-direct {v8, v9}, Landroid/media/Cea608CCParser$MutableBackgroundColorSpan;-><init>(I)V

    goto/32 :goto_10

    nop

    :goto_2d
    goto/16 :goto_6

    :goto_2e
    goto/32 :goto_5

    nop

    :goto_2f
    move v6, v1

    goto/32 :goto_3e

    nop

    :goto_30
    iget-object v6, p0, Landroid/media/Cea608CCParser$CCLineBuilder;->mPACStyles:[Landroid/media/Cea608CCParser$StyleCode;

    goto/32 :goto_1d

    nop

    :goto_31
    const/4 v1, -0x1

    goto/32 :goto_3

    nop

    :goto_32
    if-ltz v1, :cond_7

    goto/32 :goto_1

    :cond_7
    goto/32 :goto_3b

    nop

    :goto_33
    const/16 v7, 0xa0

    goto/32 :goto_37

    nop

    :goto_34
    if-gez v1, :cond_8

    goto/32 :goto_12

    :cond_8
    goto/32 :goto_11

    nop

    :goto_35
    const/4 v5, 0x0

    goto/32 :goto_49

    nop

    :goto_36
    if-nez v7, :cond_9

    goto/32 :goto_20

    :cond_9
    goto/32 :goto_16

    nop

    :goto_37
    if-ne v6, v7, :cond_a

    goto/32 :goto_f

    :cond_a
    goto/32 :goto_32

    nop

    :goto_38
    const/4 v4, 0x0

    :goto_39
    goto/32 :goto_a

    nop

    :goto_3a
    aget-object v7, v6, v2

    goto/32 :goto_36

    nop

    :goto_3b
    move v1, v2

    goto/32 :goto_e

    nop

    :goto_3c
    aget-object v5, v6, v2

    :goto_3d
    goto/32 :goto_46

    nop

    :goto_3e
    goto :goto_27

    :goto_3f
    goto/32 :goto_26

    nop

    :goto_40
    new-instance v0, Landroid/text/SpannableStringBuilder;

    goto/32 :goto_4

    nop

    :goto_41
    iget-object v6, p0, Landroid/media/Cea608CCParser$CCLineBuilder;->mDisplayChars:Ljava/lang/StringBuilder;

    goto/32 :goto_21

    nop

    :goto_42
    if-gez v1, :cond_b

    goto/32 :goto_1

    :cond_b
    goto/32 :goto_1c

    nop

    :goto_43
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v8

    goto/32 :goto_15

    nop

    :goto_44
    invoke-virtual {p0, v0, v4, v3, v7}, Landroid/media/Cea608CCParser$CCLineBuilder;->applyStyleSpan(Landroid/text/SpannableStringBuilder;Landroid/media/Cea608CCParser$StyleCode;II)V

    :goto_45
    goto/32 :goto_0

    nop

    :goto_46
    if-nez v5, :cond_c

    goto/32 :goto_25

    :cond_c
    goto/32 :goto_28

    nop

    :goto_47
    add-int/lit8 v9, v2, -0x1

    goto/32 :goto_43

    nop

    :goto_48
    move v7, v2

    goto/32 :goto_2d

    nop

    :goto_49
    iget-object v6, p0, Landroid/media/Cea608CCParser$CCLineBuilder;->mMidRowStyles:[Landroid/media/Cea608CCParser$StyleCode;

    goto/32 :goto_3a

    nop

    :goto_4a
    if-gez v3, :cond_d

    goto/32 :goto_45

    :cond_d
    goto/32 :goto_44

    nop
.end method

.method length()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/media/Cea608CCParser$CCLineBuilder;->mDisplayChars:Ljava/lang/StringBuilder;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    goto/32 :goto_2

    nop

    :goto_2
    return v0
.end method

.method setCharAt(IC)V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/media/Cea608CCParser$CCLineBuilder;->mDisplayChars:Ljava/lang/StringBuilder;

    goto/32 :goto_5

    nop

    :goto_1
    const/4 v1, 0x0

    goto/32 :goto_4

    nop

    :goto_2
    return-void

    :goto_3
    iget-object v0, p0, Landroid/media/Cea608CCParser$CCLineBuilder;->mMidRowStyles:[Landroid/media/Cea608CCParser$StyleCode;

    goto/32 :goto_1

    nop

    :goto_4
    aput-object v1, v0, p1

    goto/32 :goto_2

    nop

    :goto_5
    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    goto/32 :goto_3

    nop
.end method

.method setMidRowAt(ILandroid/media/Cea608CCParser$StyleCode;)V
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    iget-object v0, p0, Landroid/media/Cea608CCParser$CCLineBuilder;->mMidRowStyles:[Landroid/media/Cea608CCParser$StyleCode;

    goto/32 :goto_4

    nop

    :goto_1
    return-void

    :goto_2
    const/16 v1, 0x20

    goto/32 :goto_5

    nop

    :goto_3
    iget-object v0, p0, Landroid/media/Cea608CCParser$CCLineBuilder;->mDisplayChars:Ljava/lang/StringBuilder;

    goto/32 :goto_2

    nop

    :goto_4
    aput-object p2, v0, p1

    goto/32 :goto_1

    nop

    :goto_5
    invoke-virtual {v0, p1, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    goto/32 :goto_0

    nop
.end method

.method setPACAt(ILandroid/media/Cea608CCParser$PAC;)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    aput-object p2, v0, p1

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Landroid/media/Cea608CCParser$CCLineBuilder;->mPACStyles:[Landroid/media/Cea608CCParser$StyleCode;

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method
