.class public interface abstract annotation Landroid/media/AudioIoConfigEvent;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final CLIENT_STARTED:I = 0x8

.field public static final INPUT_CLOSED:I = 0x6

.field public static final INPUT_CONFIG_CHANGED:I = 0x7

.field public static final INPUT_OPENED:I = 0x5

.field public static final INPUT_REGISTERED:I = 0x4

.field public static final OUTPUT_CLOSED:I = 0x2

.field public static final OUTPUT_CONFIG_CHANGED:I = 0x3

.field public static final OUTPUT_OPENED:I = 0x1

.field public static final OUTPUT_REGISTERED:I
