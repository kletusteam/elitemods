.class public interface abstract Landroid/media/ISpatializerHeadToSoundStagePoseCallback;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/ISpatializerHeadToSoundStagePoseCallback$Stub;,
        Landroid/media/ISpatializerHeadToSoundStagePoseCallback$Default;
    }
.end annotation


# static fields
.field public static final DESCRIPTOR:Ljava/lang/String; = "android.media.ISpatializerHeadToSoundStagePoseCallback"


# virtual methods
.method public abstract dispatchPoseChanged([F)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
