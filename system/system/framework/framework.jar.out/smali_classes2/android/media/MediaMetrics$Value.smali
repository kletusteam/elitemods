.class public Landroid/media/MediaMetrics$Value;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/MediaMetrics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Value"
.end annotation


# static fields
.field public static final CONNECT:Ljava/lang/String; = "connect"

.field public static final CONNECTED:Ljava/lang/String; = "connected"

.field public static final DISCONNECT:Ljava/lang/String; = "disconnect"

.field public static final DISCONNECTED:Ljava/lang/String; = "disconnected"

.field public static final DOWN:Ljava/lang/String; = "down"

.field public static final MUTE:Ljava/lang/String; = "mute"

.field public static final NO:Ljava/lang/String; = "no"

.field public static final OFF:Ljava/lang/String; = "off"

.field public static final ON:Ljava/lang/String; = "on"

.field public static final UNMUTE:Ljava/lang/String; = "unmute"

.field public static final UP:Ljava/lang/String; = "up"

.field public static final YES:Ljava/lang/String; = "yes"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
