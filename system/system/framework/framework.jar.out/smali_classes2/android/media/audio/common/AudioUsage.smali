.class public interface abstract annotation Landroid/media/audio/common/AudioUsage;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final ALARM:I = 0x4

.field public static final ANNOUNCEMENT:I = 0x3eb

.field public static final ASSISTANCE_ACCESSIBILITY:I = 0xb

.field public static final ASSISTANCE_NAVIGATION_GUIDANCE:I = 0xc

.field public static final ASSISTANCE_SONIFICATION:I = 0xd

.field public static final ASSISTANT:I = 0x10

.field public static final BLUETOOTH_SCO:I = 0x13

.field public static final CALL_ASSISTANT:I = 0x11

.field public static final EMERGENCY:I = 0x3e8

.field public static final ENFORCED_AUDIBLE:I = 0x12

.field public static final GAME:I = 0xe

.field public static final INVALID:I = -0x1

.field public static final MEDIA:I = 0x1

.field public static final NOTIFICATION:I = 0x5

.field public static final NOTIFICATION_EVENT:I = 0xa

.field public static final NOTIFICATION_TELEPHONY_RINGTONE:I = 0x6

.field public static final SAFETY:I = 0x3e9

.field public static final SYS_RESERVED_NOTIFICATION_COMMUNICATION_DELAYED:I = 0x9

.field public static final SYS_RESERVED_NOTIFICATION_COMMUNICATION_INSTANT:I = 0x8

.field public static final SYS_RESERVED_NOTIFICATION_COMMUNICATION_REQUEST:I = 0x7

.field public static final TTS:I = 0x14

.field public static final UNKNOWN:I = 0x0

.field public static final VEHICLE_STATUS:I = 0x3ea

.field public static final VIRTUAL_SOURCE:I = 0xf

.field public static final VOICE_COMMUNICATION:I = 0x2

.field public static final VOICE_COMMUNICATION_SIGNALLING:I = 0x3
