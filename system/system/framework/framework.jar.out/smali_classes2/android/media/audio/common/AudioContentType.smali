.class public interface abstract annotation Landroid/media/audio/common/AudioContentType;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final MOVIE:I = 0x3

.field public static final MUSIC:I = 0x2

.field public static final SONIFICATION:I = 0x4

.field public static final SPEECH:I = 0x1

.field public static final ULTRASOUND:I = 0x7cd

.field public static final UNKNOWN:I
