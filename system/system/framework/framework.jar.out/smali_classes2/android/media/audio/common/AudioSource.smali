.class public interface abstract annotation Landroid/media/audio/common/AudioSource;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final CAMCORDER:I = 0x5

.field public static final DEFAULT:I = 0x0

.field public static final ECHO_REFERENCE:I = 0x7cd

.field public static final FM_TUNER:I = 0x7ce

.field public static final HOTWORD:I = 0x7cf

.field public static final MIC:I = 0x1

.field public static final REMOTE_SUBMIX:I = 0x8

.field public static final SYS_RESERVED_INVALID:I = -0x1

.field public static final ULTRASOUND:I = 0x7d0

.field public static final UNPROCESSED:I = 0x9

.field public static final VOICE_CALL:I = 0x4

.field public static final VOICE_COMMUNICATION:I = 0x7

.field public static final VOICE_DOWNLINK:I = 0x3

.field public static final VOICE_PERFORMANCE:I = 0xa

.field public static final VOICE_RECOGNITION:I = 0x6

.field public static final VOICE_UPLINK:I = 0x2

.field public static final VOIP_CALL:I = 0xbb9

.field public static final VOIP_DOWNLINK:I = 0xbb8

.field public static final VOIP_UPLINK:I = 0xbb7
