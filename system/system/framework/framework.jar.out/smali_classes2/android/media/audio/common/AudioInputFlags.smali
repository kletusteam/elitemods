.class public interface abstract annotation Landroid/media/audio/common/AudioInputFlags;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final CAR_VOIP_TX:I = 0xa

.field public static final DIRECT:I = 0x7

.field public static final FAST:I = 0x0

.field public static final HW_AV_SYNC:I = 0x6

.field public static final HW_HOTWORD:I = 0x1

.field public static final INCALL_UPLINK_DOWNLINK:I = 0x9

.field public static final MMAP_NOIRQ:I = 0x4

.field public static final RAW:I = 0x2

.field public static final SYNC:I = 0x3

.field public static final ULTRASOUND:I = 0x8

.field public static final VOIP_RECORD:I = 0xb

.field public static final VOIP_TX:I = 0x5
