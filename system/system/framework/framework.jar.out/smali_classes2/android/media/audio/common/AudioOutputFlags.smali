.class public interface abstract annotation Landroid/media/audio/common/AudioOutputFlags;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final CAR_VOIP_RX:I = 0x13

.field public static final COMPRESS_OFFLOAD:I = 0x4

.field public static final DEEP_BUFFER:I = 0x3

.field public static final DIRECT:I = 0x0

.field public static final DIRECT_PCM:I = 0xb

.field public static final FAST:I = 0x2

.field public static final GAPLESS_OFFLOAD:I = 0xf

.field public static final HW_AV_SYNC:I = 0x6

.field public static final IEC958_NONAUDIO:I = 0xa

.field public static final INCALL_MUSIC:I = 0xe

.field public static final MMAP_NOIRQ:I = 0xc

.field public static final NON_BLOCKING:I = 0x5

.field public static final PRIMARY:I = 0x1

.field public static final RAW:I = 0x8

.field public static final SPATIALIZER:I = 0x10

.field public static final SYNC:I = 0x9

.field public static final TTS:I = 0x7

.field public static final ULTRASOUND:I = 0x11

.field public static final VIRTUAL_DEEP_BUFFER:I = 0x12

.field public static final VOIP_RX:I = 0xd
