.class public interface abstract annotation Landroid/media/audio/common/PcmType;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final DEFAULT:B = 0x0t

.field public static final FIXED_Q_8_24:B = 0x3t

.field public static final FLOAT_32_BIT:B = 0x4t

.field public static final INT_16_BIT:B = 0x1t

.field public static final INT_24_BIT:B = 0x5t

.field public static final INT_32_BIT:B = 0x2t

.field public static final UINT_8_BIT:B
