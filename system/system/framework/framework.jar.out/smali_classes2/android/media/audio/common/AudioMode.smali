.class public interface abstract annotation Landroid/media/audio/common/AudioMode;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final CALL_SCREEN:I = 0x4

.field public static final IN_CALL:I = 0x2

.field public static final IN_COMMUNICATION:I = 0x3

.field public static final NORMAL:I = 0x0

.field public static final RINGTONE:I = 0x1

.field public static final SYS_RESERVED_CALL_REDIRECT:I = 0x5

.field public static final SYS_RESERVED_COMMUNICATION_REDIRECT:I = 0x6

.field public static final SYS_RESERVED_CURRENT:I = -0x1

.field public static final SYS_RESERVED_INVALID:I = -0x2
