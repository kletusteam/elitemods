.class public interface abstract annotation Landroid/media/audio/common/AudioStreamType;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final ACCESSIBILITY:I = 0xa

.field public static final ALARM:I = 0x4

.field public static final ASSISTANT:I = 0xb

.field public static final BLUETOOTH_SCO:I = 0x6

.field public static final CALL_ASSISTANT:I = 0xe

.field public static final DTMF:I = 0x8

.field public static final ENFORCED_AUDIBLE:I = 0x7

.field public static final INVALID:I = -0x2

.field public static final MUSIC:I = 0x3

.field public static final NOTIFICATION:I = 0x5

.field public static final RING:I = 0x2

.field public static final SYSTEM:I = 0x1

.field public static final SYS_RESERVED_DEFAULT:I = -0x1

.field public static final SYS_RESERVED_PATCH:I = 0xd

.field public static final SYS_RESERVED_REROUTING:I = 0xc

.field public static final TTS:I = 0x9

.field public static final VOICE_CALL:I
