.class public interface abstract annotation Landroid/media/audio/common/AudioPortExt$Tag;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/media/audio/common/AudioPortExt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2609
    name = "Tag"
.end annotation


# static fields
.field public static final device:I = 0x1

.field public static final mix:I = 0x2

.field public static final session:I = 0x3

.field public static final unspecified:I
