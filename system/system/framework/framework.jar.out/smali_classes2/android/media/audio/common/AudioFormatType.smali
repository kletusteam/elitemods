.class public interface abstract annotation Landroid/media/audio/common/AudioFormatType;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final DEFAULT:B = 0x0t

.field public static final NON_PCM:B = 0x0t

.field public static final PCM:B = 0x1t

.field public static final SYS_RESERVED_INVALID:B = -0x1t
