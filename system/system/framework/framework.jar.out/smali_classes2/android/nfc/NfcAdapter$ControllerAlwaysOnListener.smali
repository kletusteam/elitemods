.class public interface abstract Landroid/nfc/NfcAdapter$ControllerAlwaysOnListener;
.super Ljava/lang/Object;


# annotations
.annotation runtime Landroid/annotation/SystemApi;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/nfc/NfcAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ControllerAlwaysOnListener"
.end annotation


# virtual methods
.method public abstract onControllerAlwaysOnChanged(Z)V
.end method
