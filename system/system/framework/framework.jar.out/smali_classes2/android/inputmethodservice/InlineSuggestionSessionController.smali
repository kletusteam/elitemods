.class Landroid/inputmethodservice/InlineSuggestionSessionController;
.super Ljava/lang/Object;


# static fields
.field private static final TAG:Ljava/lang/String; = "InlineSuggestionSessionController"


# instance fields
.field private final mHostInputTokenSupplier:Ljava/util/function/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/function/Supplier<",
            "Landroid/os/IBinder;",
            ">;"
        }
    .end annotation
.end field

.field private mImeClientFieldId:Landroid/view/autofill/AutofillId;

.field private mImeClientPackageName:Ljava/lang/String;

.field private mImeInputStarted:Z

.field private mImeInputViewStarted:Z

.field private final mMainThreadHandler:Landroid/os/Handler;

.field private final mRequestSupplier:Ljava/util/function/Function;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/function/Function<",
            "Landroid/os/Bundle;",
            "Landroid/view/inputmethod/InlineSuggestionsRequest;",
            ">;"
        }
    .end annotation
.end field

.field private final mResponseConsumer:Ljava/util/function/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/function/Consumer<",
            "Landroid/view/inputmethod/InlineSuggestionsResponse;",
            ">;"
        }
    .end annotation
.end field

.field private mSession:Landroid/inputmethodservice/InlineSuggestionSession;


# direct methods
.method constructor <init>(Ljava/util/function/Function;Ljava/util/function/Supplier;Ljava/util/function/Consumer;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/function/Function<",
            "Landroid/os/Bundle;",
            "Landroid/view/inputmethod/InlineSuggestionsRequest;",
            ">;",
            "Ljava/util/function/Supplier<",
            "Landroid/os/IBinder;",
            ">;",
            "Ljava/util/function/Consumer<",
            "Landroid/view/inputmethod/InlineSuggestionsResponse;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;Z)V

    iput-object v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mMainThreadHandler:Landroid/os/Handler;

    iput-object p1, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mRequestSupplier:Ljava/util/function/Function;

    iput-object p2, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mHostInputTokenSupplier:Ljava/util/function/Supplier;

    iput-object p3, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mResponseConsumer:Ljava/util/function/Consumer;

    return-void
.end method

.method private static match(Landroid/view/autofill/AutofillId;Landroid/view/autofill/AutofillId;)Z
    .locals 2

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroid/view/autofill/AutofillId;->getViewId()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/autofill/AutofillId;->getViewId()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private static match(Lcom/android/internal/view/InlineSuggestionsRequestInfo;Ljava/lang/String;Landroid/view/autofill/AutofillId;)Z
    .locals 2

    const/4 v0, 0x0

    if-eqz p0, :cond_2

    if-eqz p1, :cond_2

    if-nez p2, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Lcom/android/internal/view/InlineSuggestionsRequestInfo;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/android/internal/view/InlineSuggestionsRequestInfo;->getAutofillId()Landroid/view/autofill/AutofillId;

    move-result-object v1

    invoke-static {v1, p2}, Landroid/inputmethodservice/InlineSuggestionSessionController;->match(Landroid/view/autofill/AutofillId;Landroid/view/autofill/AutofillId;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    nop

    :goto_0
    return v0

    :cond_2
    :goto_1
    return v0
.end method


# virtual methods
.method match(Landroid/view/autofill/AutofillId;)Z
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-static {p1, v0}, Landroid/inputmethodservice/InlineSuggestionSessionController;->match(Landroid/view/autofill/AutofillId;Landroid/view/autofill/AutofillId;)Z

    move-result v0

    goto/32 :goto_1

    nop

    :goto_1
    return v0

    :goto_2
    iget-object v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mImeClientFieldId:Landroid/view/autofill/AutofillId;

    goto/32 :goto_0

    nop
.end method

.method match(Lcom/android/internal/view/InlineSuggestionsRequestInfo;)Z
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    invoke-static {p1, v0, v1}, Landroid/inputmethodservice/InlineSuggestionSessionController;->match(Lcom/android/internal/view/InlineSuggestionsRequestInfo;Ljava/lang/String;Landroid/view/autofill/AutofillId;)Z

    move-result v0

    goto/32 :goto_3

    nop

    :goto_1
    iget-object v1, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mImeClientFieldId:Landroid/view/autofill/AutofillId;

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mImeClientPackageName:Ljava/lang/String;

    goto/32 :goto_1

    nop

    :goto_3
    return v0
.end method

.method notifyOnFinishInput()V
    .locals 4

    goto/32 :goto_18

    nop

    :goto_0
    const-string v0, "notifyOnFinishInput"

    goto/32 :goto_4

    nop

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_7

    nop

    :goto_2
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    goto/32 :goto_d

    nop

    :goto_4
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_5
    goto/32 :goto_f

    nop

    :goto_6
    const-string v1, "InlineSuggestionSessionController"

    goto/32 :goto_b

    nop

    :goto_7
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_2

    nop

    :goto_8
    iput-boolean v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mImeInputStarted:Z

    goto/32 :goto_13

    nop

    :goto_9
    if-nez v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_17

    nop

    :goto_a
    iput-object v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mImeClientPackageName:Ljava/lang/String;

    goto/32 :goto_14

    nop

    :goto_b
    if-nez v0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_0

    nop

    :goto_c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_12

    nop

    :goto_d
    return-void

    :goto_e
    iput-boolean v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mImeInputViewStarted:Z

    goto/32 :goto_8

    nop

    :goto_f
    const/4 v0, 0x0

    goto/32 :goto_a

    nop

    :goto_10
    goto :goto_3

    :catch_0
    move-exception v0

    goto/32 :goto_19

    nop

    :goto_11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_1

    nop

    :goto_12
    const-string v3, "onInputMethodFinishInput() remote exception:"

    goto/32 :goto_11

    nop

    :goto_13
    iget-object v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mSession:Landroid/inputmethodservice/InlineSuggestionSession;

    goto/32 :goto_9

    nop

    :goto_14
    iput-object v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mImeClientFieldId:Landroid/view/autofill/AutofillId;

    goto/32 :goto_15

    nop

    :goto_15
    const/4 v0, 0x0

    goto/32 :goto_e

    nop

    :goto_16
    if-nez v0, :cond_2

    goto/32 :goto_3

    :cond_2
    :try_start_0
    iget-object v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mSession:Landroid/inputmethodservice/InlineSuggestionSession;

    invoke-virtual {v0}, Landroid/inputmethodservice/InlineSuggestionSession;->getRequestCallback()Lcom/android/internal/view/IInlineSuggestionsRequestCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/internal/view/IInlineSuggestionsRequestCallback;->onInputMethodFinishInput()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_10

    nop

    :goto_17
    invoke-virtual {v0}, Landroid/inputmethodservice/InlineSuggestionSession;->shouldSendImeStatus()Z

    move-result v0

    goto/32 :goto_16

    nop

    :goto_18
    sget-boolean v0, Landroid/inputmethodservice/InputMethodService;->DEBUG:Z

    goto/32 :goto_6

    nop

    :goto_19
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_c

    nop
.end method

.method notifyOnFinishInputView()V
    .locals 4

    goto/32 :goto_8

    nop

    :goto_0
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_13

    nop

    :goto_1
    goto :goto_3

    :catch_0
    move-exception v0

    goto/32 :goto_10

    nop

    :goto_2
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    goto/32 :goto_7

    nop

    :goto_4
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_5
    goto/32 :goto_9

    nop

    :goto_6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_2

    nop

    :goto_7
    return-void

    :goto_8
    sget-boolean v0, Landroid/inputmethodservice/InputMethodService;->DEBUG:Z

    goto/32 :goto_b

    nop

    :goto_9
    const/4 v0, 0x0

    goto/32 :goto_d

    nop

    :goto_a
    if-nez v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_12

    nop

    :goto_b
    const-string v1, "InlineSuggestionSessionController"

    goto/32 :goto_a

    nop

    :goto_c
    if-nez v0, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_f

    nop

    :goto_d
    iput-boolean v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mImeInputViewStarted:Z

    goto/32 :goto_15

    nop

    :goto_e
    const-string v3, "onInputMethodFinishInputView() remote exception:"

    goto/32 :goto_0

    nop

    :goto_f
    invoke-virtual {v0}, Landroid/inputmethodservice/InlineSuggestionSession;->shouldSendImeStatus()Z

    move-result v0

    goto/32 :goto_14

    nop

    :goto_10
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_11

    nop

    :goto_11
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_e

    nop

    :goto_12
    const-string v0, "notifyOnFinishInputView"

    goto/32 :goto_4

    nop

    :goto_13
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_6

    nop

    :goto_14
    if-nez v0, :cond_2

    goto/32 :goto_3

    :cond_2
    :try_start_0
    iget-object v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mSession:Landroid/inputmethodservice/InlineSuggestionSession;

    invoke-virtual {v0}, Landroid/inputmethodservice/InlineSuggestionSession;->getRequestCallback()Lcom/android/internal/view/IInlineSuggestionsRequestCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/internal/view/IInlineSuggestionsRequestCallback;->onInputMethodFinishInputView()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_1

    nop

    :goto_15
    iget-object v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mSession:Landroid/inputmethodservice/InlineSuggestionSession;

    goto/32 :goto_c

    nop
.end method

.method notifyOnShowInputRequested(Z)V
    .locals 4

    goto/32 :goto_7

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_f

    :cond_0
    goto/32 :goto_d

    nop

    :goto_1
    if-nez v0, :cond_1

    goto/32 :goto_4

    :cond_1
    :try_start_0
    iget-object v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mSession:Landroid/inputmethodservice/InlineSuggestionSession;

    invoke-virtual {v0}, Landroid/inputmethodservice/InlineSuggestionSession;->getRequestCallback()Lcom/android/internal/view/IInlineSuggestionsRequestCallback;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/internal/view/IInlineSuggestionsRequestCallback;->onInputMethodShowInputRequested(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_10

    nop

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_5

    nop

    :goto_3
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_4
    goto/32 :goto_a

    nop

    :goto_5
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_3

    nop

    :goto_6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_12

    nop

    :goto_7
    sget-boolean v0, Landroid/inputmethodservice/InputMethodService;->DEBUG:Z

    goto/32 :goto_11

    nop

    :goto_8
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_9
    if-nez v0, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_b

    nop

    :goto_a
    return-void

    :goto_b
    invoke-virtual {v0}, Landroid/inputmethodservice/InlineSuggestionSession;->shouldSendImeStatus()Z

    move-result v0

    goto/32 :goto_1

    nop

    :goto_c
    iget-object v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mSession:Landroid/inputmethodservice/InlineSuggestionSession;

    goto/32 :goto_9

    nop

    :goto_d
    const-string v0, "notifyShowInputRequested"

    goto/32 :goto_e

    nop

    :goto_e
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_f
    goto/32 :goto_c

    nop

    :goto_10
    goto :goto_4

    :catch_0
    move-exception v0

    goto/32 :goto_8

    nop

    :goto_11
    const-string v1, "InlineSuggestionSessionController"

    goto/32 :goto_0

    nop

    :goto_12
    const-string v3, "onInputMethodShowInputRequested() remote exception:"

    goto/32 :goto_13

    nop

    :goto_13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_2

    nop
.end method

.method notifyOnStartInput(Ljava/lang/String;Landroid/view/autofill/AutofillId;)V
    .locals 4

    goto/32 :goto_33

    nop

    :goto_0
    iput-object p1, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mImeClientPackageName:Ljava/lang/String;

    goto/32 :goto_30

    nop

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_26

    nop

    :goto_2
    return-void

    :goto_3
    goto/32 :goto_1d

    nop

    :goto_4
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_12

    nop

    :goto_5
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_21

    nop

    :goto_6
    goto :goto_3

    :goto_7
    goto/32 :goto_20

    nop

    :goto_8
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_9
    goto/16 :goto_2f

    :catch_0
    move-exception v0

    goto/32 :goto_4

    nop

    :goto_a
    invoke-virtual {v0}, Landroid/inputmethodservice/InlineSuggestionSession;->shouldSendImeStatus()Z

    move-result v0

    goto/32 :goto_2a

    nop

    :goto_b
    if-eqz v0, :cond_0

    goto/32 :goto_25

    :cond_0
    goto/32 :goto_29

    nop

    :goto_c
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_d
    goto/32 :goto_1c

    nop

    :goto_e
    invoke-virtual {v0}, Landroid/inputmethodservice/InlineSuggestionSession;->isCallbackInvoked()Z

    move-result v0

    goto/32 :goto_b

    nop

    :goto_f
    const-string v2, ", "

    goto/32 :goto_8

    nop

    :goto_10
    invoke-virtual {v0}, Landroid/inputmethodservice/InlineSuggestionSession;->getRequestInfo()Lcom/android/internal/view/InlineSuggestionsRequestInfo;

    move-result-object v0

    goto/32 :goto_17

    nop

    :goto_11
    const-string v3, "onInputMethodStartInput() remote exception:"

    goto/32 :goto_1e

    nop

    :goto_12
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_11

    nop

    :goto_13
    iget-object v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mSession:Landroid/inputmethodservice/InlineSuggestionSession;

    goto/32 :goto_e

    nop

    :goto_14
    iget-object v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mSession:Landroid/inputmethodservice/InlineSuggestionSession;

    goto/32 :goto_2d

    nop

    :goto_15
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_f

    nop

    :goto_16
    iget-object v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mSession:Landroid/inputmethodservice/InlineSuggestionSession;

    goto/32 :goto_a

    nop

    :goto_17
    invoke-virtual {p0, v0}, Landroid/inputmethodservice/InlineSuggestionSessionController;->match(Lcom/android/internal/view/InlineSuggestionsRequestInfo;)Z

    move-result v0

    goto/32 :goto_1f

    nop

    :goto_18
    if-nez v0, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_1

    nop

    :goto_19
    iget-object v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mSession:Landroid/inputmethodservice/InlineSuggestionSession;

    goto/32 :goto_28

    nop

    :goto_1a
    iput-boolean v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mImeInputStarted:Z

    goto/32 :goto_0

    nop

    :goto_1b
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_2b

    nop

    :goto_1c
    if-nez p1, :cond_2

    goto/32 :goto_3

    :cond_2
    goto/32 :goto_27

    nop

    :goto_1d
    return-void

    :goto_1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_1b

    nop

    :goto_1f
    if-nez v0, :cond_3

    goto/32 :goto_25

    :cond_3
    goto/32 :goto_14

    nop

    :goto_20
    const/4 v0, 0x1

    goto/32 :goto_1a

    nop

    :goto_21
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_c

    nop

    :goto_22
    invoke-virtual {v0, v2}, Landroid/inputmethodservice/InlineSuggestionSession;->consumeInlineSuggestionsResponse(Landroid/view/inputmethod/InlineSuggestionsResponse;)V

    goto/32 :goto_13

    nop

    :goto_23
    const-string v1, "InlineSuggestionSessionController"

    goto/32 :goto_18

    nop

    :goto_24
    goto :goto_2f

    :goto_25
    goto/32 :goto_16

    nop

    :goto_26
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_32

    nop

    :goto_27
    if-eqz p2, :cond_4

    goto/32 :goto_7

    :cond_4
    goto/32 :goto_6

    nop

    :goto_28
    if-nez v0, :cond_5

    goto/32 :goto_2f

    :cond_5
    goto/32 :goto_31

    nop

    :goto_29
    iget-object v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mSession:Landroid/inputmethodservice/InlineSuggestionSession;

    goto/32 :goto_10

    nop

    :goto_2a
    if-nez v0, :cond_6

    goto/32 :goto_2f

    :cond_6
    :try_start_0
    iget-object v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mSession:Landroid/inputmethodservice/InlineSuggestionSession;

    invoke-virtual {v0}, Landroid/inputmethodservice/InlineSuggestionSession;->getRequestCallback()Lcom/android/internal/view/IInlineSuggestionsRequestCallback;

    move-result-object v0

    iget-object v2, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mImeClientFieldId:Landroid/view/autofill/AutofillId;

    invoke-interface {v0, v2}, Lcom/android/internal/view/IInlineSuggestionsRequestCallback;->onInputMethodStartInput(Landroid/view/autofill/AutofillId;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_9

    nop

    :goto_2b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_2e

    nop

    :goto_2c
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_15

    nop

    :goto_2d
    invoke-virtual {v0}, Landroid/inputmethodservice/InlineSuggestionSession;->makeInlineSuggestionRequestUncheck()V

    goto/32 :goto_24

    nop

    :goto_2e
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2f
    goto/32 :goto_2

    nop

    :goto_30
    iput-object p2, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mImeClientFieldId:Landroid/view/autofill/AutofillId;

    goto/32 :goto_19

    nop

    :goto_31
    sget-object v2, Landroid/inputmethodservice/InlineSuggestionSession;->EMPTY_RESPONSE:Landroid/view/inputmethod/InlineSuggestionsResponse;

    goto/32 :goto_22

    nop

    :goto_32
    const-string v2, "notifyOnStartInput: "

    goto/32 :goto_2c

    nop

    :goto_33
    sget-boolean v0, Landroid/inputmethodservice/InputMethodService;->DEBUG:Z

    goto/32 :goto_23

    nop
.end method

.method notifyOnStartInputView()V
    .locals 4

    goto/32 :goto_8

    nop

    :goto_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mSession:Landroid/inputmethodservice/InlineSuggestionSession;

    goto/32 :goto_e

    nop

    :goto_2
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    goto/32 :goto_5

    nop

    :goto_4
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_0

    nop

    :goto_5
    return-void

    :goto_6
    const-string v1, "InlineSuggestionSessionController"

    goto/32 :goto_14

    nop

    :goto_7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_4

    nop

    :goto_8
    sget-boolean v0, Landroid/inputmethodservice/InputMethodService;->DEBUG:Z

    goto/32 :goto_6

    nop

    :goto_9
    if-nez v0, :cond_0

    goto/32 :goto_3

    :cond_0
    :try_start_0
    iget-object v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mSession:Landroid/inputmethodservice/InlineSuggestionSession;

    invoke-virtual {v0}, Landroid/inputmethodservice/InlineSuggestionSession;->getRequestCallback()Lcom/android/internal/view/IInlineSuggestionsRequestCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/internal/view/IInlineSuggestionsRequestCallback;->onInputMethodStartInputView()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_11

    nop

    :goto_a
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_b
    goto/32 :goto_c

    nop

    :goto_c
    const/4 v0, 0x1

    goto/32 :goto_12

    nop

    :goto_d
    const-string v0, "notifyOnStartInputView"

    goto/32 :goto_a

    nop

    :goto_e
    if-nez v0, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_10

    nop

    :goto_f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_15

    nop

    :goto_10
    invoke-virtual {v0}, Landroid/inputmethodservice/InlineSuggestionSession;->shouldSendImeStatus()Z

    move-result v0

    goto/32 :goto_9

    nop

    :goto_11
    goto :goto_3

    :catch_0
    move-exception v0

    goto/32 :goto_13

    nop

    :goto_12
    iput-boolean v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mImeInputViewStarted:Z

    goto/32 :goto_1

    nop

    :goto_13
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_f

    nop

    :goto_14
    if-nez v0, :cond_2

    goto/32 :goto_b

    :cond_2
    goto/32 :goto_d

    nop

    :goto_15
    const-string v3, "onInputMethodStartInputView() remote exception:"

    goto/32 :goto_7

    nop
.end method

.method onMakeInlineSuggestionsRequest(Lcom/android/internal/view/InlineSuggestionsRequestInfo;Lcom/android/internal/view/IInlineSuggestionsRequestCallback;)V
    .locals 10

    goto/32 :goto_16

    nop

    :goto_0
    move-object v2, v0

    goto/32 :goto_7

    nop

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_20

    :cond_0
    :try_start_0
    iget-object v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mSession:Landroid/inputmethodservice/InlineSuggestionSession;

    invoke-virtual {v0}, Landroid/inputmethodservice/InlineSuggestionSession;->getRequestCallback()Lcom/android/internal/view/IInlineSuggestionsRequestCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/internal/view/IInlineSuggestionsRequestCallback;->onInputMethodStartInputView()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_d

    nop

    :goto_2
    if-nez v2, :cond_1

    goto/32 :goto_20

    :cond_1
    goto/32 :goto_11

    nop

    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_9

    nop

    :goto_4
    iput-object v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mSession:Landroid/inputmethodservice/InlineSuggestionSession;

    goto/32 :goto_1e

    nop

    :goto_5
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_1f

    nop

    :goto_6
    if-nez v0, :cond_2

    goto/32 :goto_26

    :cond_2
    goto/32 :goto_25

    nop

    :goto_7
    move-object v3, p1

    goto/32 :goto_f

    nop

    :goto_8
    invoke-virtual {p0, v0}, Landroid/inputmethodservice/InlineSuggestionSessionController;->match(Lcom/android/internal/view/InlineSuggestionsRequestInfo;)Z

    move-result v0

    goto/32 :goto_27

    nop

    :goto_9
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_22

    nop

    :goto_a
    return-void

    :goto_b
    iget-object v6, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mHostInputTokenSupplier:Ljava/util/function/Supplier;

    goto/32 :goto_21

    nop

    :goto_c
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_24

    nop

    :goto_d
    goto :goto_20

    :catch_0
    move-exception v0

    goto/32 :goto_2a

    nop

    :goto_e
    iget-boolean v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mImeInputViewStarted:Z

    goto/32 :goto_1

    nop

    :goto_f
    move-object v4, p2

    goto/32 :goto_18

    nop

    :goto_10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_29

    nop

    :goto_11
    invoke-virtual {v0}, Landroid/inputmethodservice/InlineSuggestionSession;->getRequestInfo()Lcom/android/internal/view/InlineSuggestionsRequestInfo;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_12
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_5

    nop

    :goto_13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_12

    nop

    :goto_14
    const-string v1, "InlineSuggestionSessionController"

    goto/32 :goto_23

    nop

    :goto_15
    iget-object v5, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mRequestSupplier:Ljava/util/function/Function;

    goto/32 :goto_b

    nop

    :goto_16
    sget-boolean v0, Landroid/inputmethodservice/InputMethodService;->DEBUG:Z

    goto/32 :goto_14

    nop

    :goto_17
    invoke-direct/range {v2 .. v9}, Landroid/inputmethodservice/InlineSuggestionSession;-><init>(Lcom/android/internal/view/InlineSuggestionsRequestInfo;Lcom/android/internal/view/IInlineSuggestionsRequestCallback;Ljava/util/function/Function;Ljava/util/function/Supplier;Ljava/util/function/Consumer;Landroid/inputmethodservice/InlineSuggestionSessionController;Landroid/os/Handler;)V

    goto/32 :goto_4

    nop

    :goto_18
    move-object v8, p0

    goto/32 :goto_17

    nop

    :goto_19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_2b

    nop

    :goto_1a
    iget-object v9, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mMainThreadHandler:Landroid/os/Handler;

    goto/32 :goto_0

    nop

    :goto_1b
    iget-object v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mSession:Landroid/inputmethodservice/InlineSuggestionSession;

    goto/32 :goto_6

    nop

    :goto_1c
    iget-object v0, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mSession:Landroid/inputmethodservice/InlineSuggestionSession;

    goto/32 :goto_1d

    nop

    :goto_1d
    invoke-virtual {v0}, Landroid/inputmethodservice/InlineSuggestionSession;->makeInlineSuggestionRequestUncheck()V

    goto/32 :goto_e

    nop

    :goto_1e
    iget-boolean v2, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mImeInputStarted:Z

    goto/32 :goto_2

    nop

    :goto_1f
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_20
    goto/32 :goto_a

    nop

    :goto_21
    iget-object v7, p0, Landroid/inputmethodservice/InlineSuggestionSessionController;->mResponseConsumer:Ljava/util/function/Consumer;

    goto/32 :goto_1a

    nop

    :goto_22
    const-string v2, "onMakeInlineSuggestionsRequest: "

    goto/32 :goto_c

    nop

    :goto_23
    if-nez v0, :cond_3

    goto/32 :goto_2c

    :cond_3
    goto/32 :goto_3

    nop

    :goto_24
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_19

    nop

    :goto_25
    invoke-virtual {v0}, Landroid/inputmethodservice/InlineSuggestionSession;->invalidate()V

    :goto_26
    goto/32 :goto_28

    nop

    :goto_27
    if-nez v0, :cond_4

    goto/32 :goto_20

    :cond_4
    goto/32 :goto_1c

    nop

    :goto_28
    new-instance v0, Landroid/inputmethodservice/InlineSuggestionSession;

    goto/32 :goto_15

    nop

    :goto_29
    const-string v3, "onInputMethodStartInputView() remote exception:"

    goto/32 :goto_13

    nop

    :goto_2a
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_10

    nop

    :goto_2b
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2c
    goto/32 :goto_1b

    nop
.end method
