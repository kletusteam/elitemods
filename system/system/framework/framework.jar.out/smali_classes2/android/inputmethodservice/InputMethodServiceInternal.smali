.class interface abstract Landroid/inputmethodservice/InputMethodServiceInternal;
.super Ljava/lang/Object;


# virtual methods
.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public exposeContent(Landroid/view/inputmethod/InputContentInfo;Landroid/view/inputmethod/InputConnection;)V
    .locals 0

    return-void
.end method

.method public abstract getContext()Landroid/content/Context;
.end method

.method public notifyUserActionIfNecessary()V
    .locals 0

    return-void
.end method

.method public triggerServiceDump(Ljava/lang/String;[B)V
    .locals 0

    return-void
.end method
