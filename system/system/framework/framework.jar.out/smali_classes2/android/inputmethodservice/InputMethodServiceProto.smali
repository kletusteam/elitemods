.class public final Landroid/inputmethodservice/InputMethodServiceProto;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/inputmethodservice/InputMethodServiceProto$InsetsProto;
    }
.end annotation


# static fields
.field public static final CANDIDATES_VIEW_STARTED:J = 0x1080000000cL

.field public static final CANDIDATES_VISIBILITY:J = 0x10500000013L

.field public static final CONFIGURATION:J = 0x10900000007L

.field public static final DECOR_VIEW_VISIBLE:J = 0x10800000003L

.field public static final DECOR_VIEW_WAS_VISIBLE:J = 0x10800000004L

.field public static final EXTRACTED_TOKEN:J = 0x10500000017L

.field public static final EXTRACT_VIEW_HIDDEN:J = 0x10800000016L

.field public static final FULLSCREEN_APPLIED:J = 0x10800000014L

.field public static final INPUT_BINDING:J = 0x10900000009L

.field public static final INPUT_CONNECTION_CALL:J = 0x10b0000001cL

.field public static final INPUT_EDITOR_INFO:J = 0x10b0000000dL

.field public static final INPUT_STARTED:J = 0x1080000000aL

.field public static final INPUT_VIEW_STARTED:J = 0x1080000000bL

.field public static final IN_SHOW_WINDOW:J = 0x10800000006L

.field public static final IS_FULLSCREEN:J = 0x10800000015L

.field public static final IS_INPUT_VIEW_SHOWN:J = 0x10800000018L

.field public static final LAST_COMPUTED_INSETS:J = 0x10b0000001aL

.field public static final LAST_SHOW_INPUT_REQUESTED:J = 0x1080000000fL

.field public static final SETTINGS_OBSERVER:J = 0x1090000001bL

.field public static final SHOW_INPUT_FLAGS:J = 0x10500000012L

.field public static final SHOW_INPUT_REQUESTED:J = 0x1080000000eL

.field public static final SOFT_INPUT_WINDOW:J = 0x10b00000001L

.field public static final STATUS_ICON:J = 0x10500000019L

.field public static final TOKEN:J = 0x10900000008L

.field public static final VIEWS_CREATED:J = 0x10800000002L

.field public static final WINDOW_VISIBLE:J = 0x10800000005L


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
