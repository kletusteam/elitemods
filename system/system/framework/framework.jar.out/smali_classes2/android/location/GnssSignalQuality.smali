.class public interface abstract Landroid/location/GnssSignalQuality;
.super Ljava/lang/Object;


# static fields
.field public static final GNSS_SIGNAL_QUALITY_GOOD:I = 0x1

.field public static final GNSS_SIGNAL_QUALITY_POOR:I = 0x0

.field public static final GNSS_SIGNAL_QUALITY_UNKNOWN:I = -0x1

.field public static final NUM_GNSS_SIGNAL_QUALITY_LEVELS:I = 0x2
