.class Landroid/location/GnssStatus$GnssSvInfo;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/location/GnssStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GnssSvInfo"
.end annotation


# instance fields
.field private final mAzimuth:F

.field private final mBasebandCn0DbHz:F

.field private final mCarrierFrequency:F

.field private final mCn0DbHz:F

.field private final mElevation:F

.field private final mSvidWithFlags:I


# direct methods
.method static bridge synthetic -$$Nest$fgetmAzimuth(Landroid/location/GnssStatus$GnssSvInfo;)F
    .locals 0

    iget p0, p0, Landroid/location/GnssStatus$GnssSvInfo;->mAzimuth:F

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmBasebandCn0DbHz(Landroid/location/GnssStatus$GnssSvInfo;)F
    .locals 0

    iget p0, p0, Landroid/location/GnssStatus$GnssSvInfo;->mBasebandCn0DbHz:F

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmCarrierFrequency(Landroid/location/GnssStatus$GnssSvInfo;)F
    .locals 0

    iget p0, p0, Landroid/location/GnssStatus$GnssSvInfo;->mCarrierFrequency:F

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmCn0DbHz(Landroid/location/GnssStatus$GnssSvInfo;)F
    .locals 0

    iget p0, p0, Landroid/location/GnssStatus$GnssSvInfo;->mCn0DbHz:F

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmElevation(Landroid/location/GnssStatus$GnssSvInfo;)F
    .locals 0

    iget p0, p0, Landroid/location/GnssStatus$GnssSvInfo;->mElevation:F

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSvidWithFlags(Landroid/location/GnssStatus$GnssSvInfo;)I
    .locals 0

    iget p0, p0, Landroid/location/GnssStatus$GnssSvInfo;->mSvidWithFlags:I

    return p0
.end method

.method private constructor <init>(IIFFFZZZZFZF)V
    .locals 6

    move-object v0, p0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    shl-int/lit8 v1, p2, 0xc

    and-int/lit8 v2, p1, 0xf

    const/16 v3, 0x8

    shl-int/2addr v2, v3

    or-int/2addr v1, v2

    or-int/2addr v1, p6

    const/4 v2, 0x0

    if-eqz p7, :cond_0

    const/4 v4, 0x2

    goto :goto_0

    :cond_0
    move v4, v2

    :goto_0
    or-int/2addr v1, v4

    if-eqz p8, :cond_1

    const/4 v4, 0x4

    goto :goto_1

    :cond_1
    move v4, v2

    :goto_1
    or-int/2addr v1, v4

    if-eqz p9, :cond_2

    goto :goto_2

    :cond_2
    move v3, v2

    :goto_2
    or-int/2addr v1, v3

    if-eqz p11, :cond_3

    const/16 v2, 0x10

    :cond_3
    or-int/2addr v1, v2

    iput v1, v0, Landroid/location/GnssStatus$GnssSvInfo;->mSvidWithFlags:I

    move v1, p3

    iput v1, v0, Landroid/location/GnssStatus$GnssSvInfo;->mCn0DbHz:F

    move v2, p4

    iput v2, v0, Landroid/location/GnssStatus$GnssSvInfo;->mElevation:F

    move v3, p5

    iput v3, v0, Landroid/location/GnssStatus$GnssSvInfo;->mAzimuth:F

    const/4 v4, 0x0

    if-eqz p9, :cond_4

    move/from16 v5, p10

    goto :goto_3

    :cond_4
    move v5, v4

    :goto_3
    iput v5, v0, Landroid/location/GnssStatus$GnssSvInfo;->mCarrierFrequency:F

    if-eqz p11, :cond_5

    move/from16 v4, p12

    :cond_5
    iput v4, v0, Landroid/location/GnssStatus$GnssSvInfo;->mBasebandCn0DbHz:F

    return-void
.end method

.method synthetic constructor <init>(IIFFFZZZZFZFLandroid/location/GnssStatus$GnssSvInfo-IA;)V
    .locals 0

    invoke-direct/range {p0 .. p12}, Landroid/location/GnssStatus$GnssSvInfo;-><init>(IIFFFZZZZFZF)V

    return-void
.end method
