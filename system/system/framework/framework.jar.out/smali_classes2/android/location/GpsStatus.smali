.class public final Landroid/location/GpsStatus;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/location/GpsStatus$NmeaListener;,
        Landroid/location/GpsStatus$Listener;,
        Landroid/location/GpsStatus$SatelliteIterator;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final BEIDOU_SVID_OFFSET:I = 0xc8

.field private static final GLONASS_SVID_OFFSET:I = 0x40

.field public static final GPS_EVENT_FIRST_FIX:I = 0x3

.field public static final GPS_EVENT_SATELLITE_STATUS:I = 0x4

.field public static final GPS_EVENT_STARTED:I = 0x1

.field public static final GPS_EVENT_STOPPED:I = 0x2

.field private static final MAX_SATELLITES:I = 0xff

.field private static final SBAS_SVID_OFFSET:I = -0x57


# instance fields
.field private mSatelliteList:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable<",
            "Landroid/location/GpsSatellite;",
            ">;"
        }
    .end annotation
.end field

.field private final mSatellites:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Landroid/location/GpsSatellite;",
            ">;"
        }
    .end annotation
.end field

.field private mTimeToFirstFix:I


# direct methods
.method static bridge synthetic -$$Nest$fgetmSatellites(Landroid/location/GpsStatus;)Landroid/util/SparseArray;
    .locals 0

    iget-object p0, p0, Landroid/location/GpsStatus;->mSatellites:Landroid/util/SparseArray;

    return-object p0
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Landroid/location/GpsStatus;->mSatellites:Landroid/util/SparseArray;

    new-instance v0, Landroid/location/GpsStatus$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Landroid/location/GpsStatus$$ExternalSyntheticLambda0;-><init>(Landroid/location/GpsStatus;)V

    iput-object v0, p0, Landroid/location/GpsStatus;->mSatelliteList:Ljava/lang/Iterable;

    return-void
.end method

.method public static create(Landroid/location/GnssStatus;I)Landroid/location/GpsStatus;
    .locals 1

    new-instance v0, Landroid/location/GpsStatus;

    invoke-direct {v0}, Landroid/location/GpsStatus;-><init>()V

    invoke-virtual {v0, p0, p1}, Landroid/location/GpsStatus;->setStatus(Landroid/location/GnssStatus;I)V

    return-object v0
.end method

.method static createEmpty()Landroid/location/GpsStatus;
    .locals 1

    new-instance v0, Landroid/location/GpsStatus;

    invoke-direct {v0}, Landroid/location/GpsStatus;-><init>()V

    return-object v0
.end method


# virtual methods
.method public getMaxSatellites()I
    .locals 1

    iget-object v0, p0, Landroid/location/GpsStatus;->mSatellites:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    return v0
.end method

.method public getSatellites()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable<",
            "Landroid/location/GpsSatellite;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Landroid/location/GpsStatus;->mSatelliteList:Ljava/lang/Iterable;

    return-object v0
.end method

.method public getTimeToFirstFix()I
    .locals 1

    iget v0, p0, Landroid/location/GpsStatus;->mTimeToFirstFix:I

    return v0
.end method

.method synthetic lambda$new$0$android-location-GpsStatus()Ljava/util/Iterator;
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-direct {v0, p0}, Landroid/location/GpsStatus$SatelliteIterator;-><init>(Landroid/location/GpsStatus;)V

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0

    :goto_2
    new-instance v0, Landroid/location/GpsStatus$SatelliteIterator;

    goto/32 :goto_0

    nop
.end method

.method setStatus(Landroid/location/GnssStatus;I)V
    .locals 6

    goto/32 :goto_3f

    nop

    :goto_0
    iput v4, v3, Landroid/location/GpsSatellite;->mElevation:F

    goto/32 :goto_44

    nop

    :goto_1
    invoke-virtual {p1, v0}, Landroid/location/GnssStatus;->getSvid(I)I

    move-result v2

    goto/32 :goto_19

    nop

    :goto_2
    iput p2, p0, Landroid/location/GpsStatus;->mTimeToFirstFix:I

    goto/32 :goto_15

    nop

    :goto_3
    check-cast v3, Landroid/location/GpsSatellite;

    goto/32 :goto_2e

    nop

    :goto_4
    goto :goto_16

    :goto_5
    goto/32 :goto_27

    nop

    :goto_6
    iget-object v1, p0, Landroid/location/GpsStatus;->mSatellites:Landroid/util/SparseArray;

    goto/32 :goto_36

    nop

    :goto_7
    move-object v3, v5

    goto/32 :goto_1e

    nop

    :goto_8
    if-ne v1, v4, :cond_0

    goto/32 :goto_2a

    :cond_0
    goto/32 :goto_13

    nop

    :goto_9
    iput-boolean v2, v1, Landroid/location/GpsSatellite;->mValid:Z

    goto/32 :goto_11

    nop

    :goto_a
    if-lt v0, v1, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_38

    nop

    :goto_b
    if-eq v1, v3, :cond_2

    goto/32 :goto_1b

    :cond_2
    goto/32 :goto_39

    nop

    :goto_c
    const/4 v3, 0x5

    goto/32 :goto_20

    nop

    :goto_d
    if-ne v1, v3, :cond_3

    goto/32 :goto_2a

    :cond_3
    goto/32 :goto_29

    nop

    :goto_e
    invoke-virtual {p1, v0}, Landroid/location/GnssStatus;->hasEphemerisData(I)Z

    move-result v4

    goto/32 :goto_30

    nop

    :goto_f
    iget-object v1, p0, Landroid/location/GpsStatus;->mSatellites:Landroid/util/SparseArray;

    goto/32 :goto_3a

    nop

    :goto_10
    iget-object v3, p0, Landroid/location/GpsStatus;->mSatellites:Landroid/util/SparseArray;

    goto/32 :goto_22

    nop

    :goto_11
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_45

    nop

    :goto_12
    add-int/lit8 v2, v2, 0x40

    goto/32 :goto_2b

    nop

    :goto_13
    const/4 v3, 0x4

    goto/32 :goto_d

    nop

    :goto_14
    if-gt v2, v3, :cond_4

    goto/32 :goto_3e

    :cond_4
    goto/32 :goto_3d

    nop

    :goto_15
    const/4 v0, 0x0

    :goto_16
    goto/32 :goto_49

    nop

    :goto_17
    invoke-virtual {p1, v0}, Landroid/location/GnssStatus;->usedInFix(I)Z

    move-result v4

    goto/32 :goto_1c

    nop

    :goto_18
    if-lt v0, v1, :cond_5

    goto/32 :goto_46

    :cond_5
    goto/32 :goto_f

    nop

    :goto_19
    const/4 v3, 0x3

    goto/32 :goto_2f

    nop

    :goto_1a
    goto :goto_2a

    :goto_1b
    goto/32 :goto_8

    nop

    :goto_1c
    iput-boolean v4, v3, Landroid/location/GpsSatellite;->mUsedInFix:Z

    :goto_1d
    goto/32 :goto_32

    nop

    :goto_1e
    iget-object v5, p0, Landroid/location/GpsStatus;->mSatellites:Landroid/util/SparseArray;

    goto/32 :goto_42

    nop

    :goto_1f
    const/16 v3, 0xff

    goto/32 :goto_14

    nop

    :goto_20
    if-eq v1, v3, :cond_6

    goto/32 :goto_34

    :cond_6
    goto/32 :goto_28

    nop

    :goto_21
    invoke-direct {v5, v2}, Landroid/location/GpsSatellite;-><init>(I)V

    goto/32 :goto_7

    nop

    :goto_22
    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_3

    nop

    :goto_23
    invoke-virtual {p1, v0}, Landroid/location/GnssStatus;->hasAlmanacData(I)Z

    move-result v4

    goto/32 :goto_47

    nop

    :goto_24
    invoke-virtual {p1, v0}, Landroid/location/GnssStatus;->getElevationDegrees(I)F

    move-result v4

    goto/32 :goto_0

    nop

    :goto_25
    iput-boolean v4, v3, Landroid/location/GpsSatellite;->mValid:Z

    goto/32 :goto_41

    nop

    :goto_26
    new-instance v5, Landroid/location/GpsSatellite;

    goto/32 :goto_21

    nop

    :goto_27
    return-void

    :goto_28
    add-int/lit16 v2, v2, 0xc8

    goto/32 :goto_33

    nop

    :goto_29
    goto :goto_1d

    :goto_2a
    goto/32 :goto_31

    nop

    :goto_2b
    goto :goto_2a

    :goto_2c
    goto/32 :goto_c

    nop

    :goto_2d
    const/4 v2, 0x0

    goto/32 :goto_9

    nop

    :goto_2e
    if-eqz v3, :cond_7

    goto/32 :goto_43

    :cond_7
    goto/32 :goto_26

    nop

    :goto_2f
    const/4 v4, 0x1

    goto/32 :goto_3c

    nop

    :goto_30
    iput-boolean v4, v3, Landroid/location/GpsSatellite;->mHasEphemeris:Z

    goto/32 :goto_23

    nop

    :goto_31
    if-gtz v2, :cond_8

    goto/32 :goto_1d

    :cond_8
    goto/32 :goto_1f

    nop

    :goto_32
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_4

    nop

    :goto_33
    goto :goto_2a

    :goto_34
    goto/32 :goto_3b

    nop

    :goto_35
    iput v4, v3, Landroid/location/GpsSatellite;->mSnr:F

    goto/32 :goto_24

    nop

    :goto_36
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    goto/32 :goto_18

    nop

    :goto_37
    check-cast v1, Landroid/location/GpsSatellite;

    goto/32 :goto_2d

    nop

    :goto_38
    invoke-virtual {p1, v0}, Landroid/location/GnssStatus;->getConstellationType(I)I

    move-result v1

    goto/32 :goto_1

    nop

    :goto_39
    add-int/lit8 v2, v2, -0x57

    goto/32 :goto_1a

    nop

    :goto_3a
    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_37

    nop

    :goto_3b
    const/4 v3, 0x2

    goto/32 :goto_b

    nop

    :goto_3c
    if-eq v1, v3, :cond_9

    goto/32 :goto_2c

    :cond_9
    goto/32 :goto_12

    nop

    :goto_3d
    goto/16 :goto_1d

    :goto_3e
    goto/32 :goto_10

    nop

    :goto_3f
    const/4 v0, 0x0

    :goto_40
    goto/32 :goto_6

    nop

    :goto_41
    invoke-virtual {p1, v0}, Landroid/location/GnssStatus;->getCn0DbHz(I)F

    move-result v4

    goto/32 :goto_35

    nop

    :goto_42
    invoke-virtual {v5, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :goto_43
    goto/32 :goto_25

    nop

    :goto_44
    invoke-virtual {p1, v0}, Landroid/location/GnssStatus;->getAzimuthDegrees(I)F

    move-result v4

    goto/32 :goto_48

    nop

    :goto_45
    goto :goto_40

    :goto_46
    goto/32 :goto_2

    nop

    :goto_47
    iput-boolean v4, v3, Landroid/location/GpsSatellite;->mHasAlmanac:Z

    goto/32 :goto_17

    nop

    :goto_48
    iput v4, v3, Landroid/location/GpsSatellite;->mAzimuth:F

    goto/32 :goto_e

    nop

    :goto_49
    invoke-virtual {p1}, Landroid/location/GnssStatus;->getSatelliteCount()I

    move-result v1

    goto/32 :goto_a

    nop
.end method
