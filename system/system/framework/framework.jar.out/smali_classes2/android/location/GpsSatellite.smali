.class public final Landroid/location/GpsSatellite;
.super Ljava/lang/Object;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field mAzimuth:F

.field mElevation:F

.field mHasAlmanac:Z

.field mHasEphemeris:Z

.field mPrn:I

.field mSnr:F

.field mUsedInFix:Z

.field mValid:Z


# direct methods
.method constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Landroid/location/GpsSatellite;->mPrn:I

    return-void
.end method


# virtual methods
.method public getAzimuth()F
    .locals 1

    iget v0, p0, Landroid/location/GpsSatellite;->mAzimuth:F

    return v0
.end method

.method public getElevation()F
    .locals 1

    iget v0, p0, Landroid/location/GpsSatellite;->mElevation:F

    return v0
.end method

.method public getPrn()I
    .locals 1

    iget v0, p0, Landroid/location/GpsSatellite;->mPrn:I

    return v0
.end method

.method public getSnr()F
    .locals 1

    iget v0, p0, Landroid/location/GpsSatellite;->mSnr:F

    return v0
.end method

.method public hasAlmanac()Z
    .locals 1

    iget-boolean v0, p0, Landroid/location/GpsSatellite;->mHasAlmanac:Z

    return v0
.end method

.method public hasEphemeris()Z
    .locals 1

    iget-boolean v0, p0, Landroid/location/GpsSatellite;->mHasEphemeris:Z

    return v0
.end method

.method setStatus(Landroid/location/GpsSatellite;)V
    .locals 1

    goto/32 :goto_11

    nop

    :goto_0
    iget v0, p1, Landroid/location/GpsSatellite;->mAzimuth:F

    goto/32 :goto_f

    nop

    :goto_1
    iput-boolean v0, p0, Landroid/location/GpsSatellite;->mUsedInFix:Z

    goto/32 :goto_12

    nop

    :goto_2
    iput-boolean v0, p0, Landroid/location/GpsSatellite;->mValid:Z

    goto/32 :goto_4

    nop

    :goto_3
    iput-boolean v0, p0, Landroid/location/GpsSatellite;->mHasEphemeris:Z

    goto/32 :goto_a

    nop

    :goto_4
    goto :goto_10

    :goto_5
    goto/32 :goto_7

    nop

    :goto_6
    iput-boolean v0, p0, Landroid/location/GpsSatellite;->mHasAlmanac:Z

    goto/32 :goto_c

    nop

    :goto_7
    iget-boolean v0, p1, Landroid/location/GpsSatellite;->mValid:Z

    goto/32 :goto_b

    nop

    :goto_8
    iget v0, p1, Landroid/location/GpsSatellite;->mElevation:F

    goto/32 :goto_14

    nop

    :goto_9
    iget-boolean v0, p1, Landroid/location/GpsSatellite;->mHasEphemeris:Z

    goto/32 :goto_3

    nop

    :goto_a
    iget-boolean v0, p1, Landroid/location/GpsSatellite;->mHasAlmanac:Z

    goto/32 :goto_6

    nop

    :goto_b
    iput-boolean v0, p0, Landroid/location/GpsSatellite;->mValid:Z

    goto/32 :goto_9

    nop

    :goto_c
    iget-boolean v0, p1, Landroid/location/GpsSatellite;->mUsedInFix:Z

    goto/32 :goto_1

    nop

    :goto_d
    iput v0, p0, Landroid/location/GpsSatellite;->mSnr:F

    goto/32 :goto_8

    nop

    :goto_e
    const/4 v0, 0x0

    goto/32 :goto_2

    nop

    :goto_f
    iput v0, p0, Landroid/location/GpsSatellite;->mAzimuth:F

    :goto_10
    goto/32 :goto_13

    nop

    :goto_11
    if-eqz p1, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_e

    nop

    :goto_12
    iget v0, p1, Landroid/location/GpsSatellite;->mSnr:F

    goto/32 :goto_d

    nop

    :goto_13
    return-void

    :goto_14
    iput v0, p0, Landroid/location/GpsSatellite;->mElevation:F

    goto/32 :goto_0

    nop
.end method

.method public usedInFix()Z
    .locals 1

    iget-boolean v0, p0, Landroid/location/GpsSatellite;->mUsedInFix:Z

    return v0
.end method
