.class public interface abstract annotation Landroid/hardware/radio/data/ApnTypes;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/hardware/radio/data/ApnTypes$$;
    }
.end annotation


# static fields
.field public static final BIP:I = 0x2000

.field public static final CBS:I = 0x80

.field public static final DEFAULT:I = 0x1

.field public static final DUN:I = 0x8

.field public static final EMERGENCY:I = 0x200

.field public static final ENTERPRISE:I = 0x4000

.field public static final FOTA:I = 0x20

.field public static final HIPRI:I = 0x10

.field public static final IA:I = 0x100

.field public static final IMS:I = 0x40

.field public static final MCX:I = 0x400

.field public static final MMS:I = 0x2

.field public static final NONE:I = 0x0

.field public static final SUPL:I = 0x4

.field public static final VSIM:I = 0x1000

.field public static final XCAP:I = 0x800
