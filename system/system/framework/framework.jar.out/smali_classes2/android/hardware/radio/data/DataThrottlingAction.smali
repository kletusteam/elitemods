.class public interface abstract annotation Landroid/hardware/radio/data/DataThrottlingAction;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/hardware/radio/data/DataThrottlingAction$$;
    }
.end annotation


# static fields
.field public static final HOLD:B = 0x3t

.field public static final NO_DATA_THROTTLING:B = 0x0t

.field public static final THROTTLE_ANCHOR_CARRIER:B = 0x2t

.field public static final THROTTLE_SECONDARY_CARRIER:B = 0x1t
