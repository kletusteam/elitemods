.class public interface abstract annotation Landroid/hardware/radio/data/DataRequestReason;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/hardware/radio/data/DataRequestReason$$;
    }
.end annotation


# static fields
.field public static final HANDOVER:I = 0x3

.field public static final NORMAL:I = 0x1

.field public static final SHUTDOWN:I = 0x2
