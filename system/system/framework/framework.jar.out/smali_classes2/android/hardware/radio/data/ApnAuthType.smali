.class public interface abstract annotation Landroid/hardware/radio/data/ApnAuthType;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/hardware/radio/data/ApnAuthType$$;
    }
.end annotation


# static fields
.field public static final NO_PAP_CHAP:I = 0x2

.field public static final NO_PAP_NO_CHAP:I = 0x0

.field public static final PAP_CHAP:I = 0x3

.field public static final PAP_NO_CHAP:I = 0x1
