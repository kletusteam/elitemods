.class public interface abstract annotation Landroid/hardware/radio/sim/PinState;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/hardware/radio/sim/PinState$$;
    }
.end annotation


# static fields
.field public static final DISABLED:I = 0x3

.field public static final ENABLED_BLOCKED:I = 0x4

.field public static final ENABLED_NOT_VERIFIED:I = 0x1

.field public static final ENABLED_PERM_BLOCKED:I = 0x5

.field public static final ENABLED_VERIFIED:I = 0x2

.field public static final UNKNOWN:I
