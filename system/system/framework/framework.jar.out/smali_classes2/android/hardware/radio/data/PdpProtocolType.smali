.class public interface abstract annotation Landroid/hardware/radio/data/PdpProtocolType;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/hardware/radio/data/PdpProtocolType$$;
    }
.end annotation


# static fields
.field public static final IP:I = 0x0

.field public static final IPV4V6:I = 0x2

.field public static final IPV6:I = 0x1

.field public static final NON_IP:I = 0x4

.field public static final PPP:I = 0x3

.field public static final UNKNOWN:I = -0x1

.field public static final UNSTRUCTURED:I = 0x5
