.class public interface abstract annotation Landroid/hardware/radio/sim/SimLockMultiSimPolicy;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/hardware/radio/sim/SimLockMultiSimPolicy$$;
    }
.end annotation


# static fields
.field public static final NO_MULTISIM_POLICY:I = 0x0

.field public static final ONE_VALID_SIM_MUST_BE_PRESENT:I = 0x1
