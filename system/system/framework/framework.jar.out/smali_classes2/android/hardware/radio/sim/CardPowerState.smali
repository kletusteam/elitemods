.class public interface abstract annotation Landroid/hardware/radio/sim/CardPowerState;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/hardware/radio/sim/CardPowerState$$;
    }
.end annotation


# static fields
.field public static final POWER_DOWN:I = 0x0

.field public static final POWER_UP:I = 0x1

.field public static final POWER_UP_PASS_THROUGH:I = 0x2
