.class public interface abstract annotation Landroid/hardware/graphics/common/PixelFormat;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final BGRA_8888:I = 0x5

.field public static final BLOB:I = 0x21

.field public static final DEPTH_16:I = 0x30

.field public static final DEPTH_24:I = 0x31

.field public static final DEPTH_24_STENCIL_8:I = 0x32

.field public static final DEPTH_32F:I = 0x33

.field public static final DEPTH_32F_STENCIL_8:I = 0x34

.field public static final HSV_888:I = 0x37

.field public static final IMPLEMENTATION_DEFINED:I = 0x22

.field public static final RAW10:I = 0x25

.field public static final RAW12:I = 0x26

.field public static final RAW16:I = 0x20

.field public static final RAW_OPAQUE:I = 0x24

.field public static final RGBA_1010102:I = 0x2b

.field public static final RGBA_8888:I = 0x1

.field public static final RGBA_FP16:I = 0x16

.field public static final RGBX_8888:I = 0x2

.field public static final RGB_565:I = 0x4

.field public static final RGB_888:I = 0x3

.field public static final R_8:I = 0x38

.field public static final STENCIL_8:I = 0x35

.field public static final UNSPECIFIED:I = 0x0

.field public static final Y16:I = 0x20363159

.field public static final Y8:I = 0x20203859

.field public static final YCBCR_420_888:I = 0x23

.field public static final YCBCR_422_I:I = 0x14

.field public static final YCBCR_422_SP:I = 0x10

.field public static final YCBCR_P010:I = 0x36

.field public static final YCRCB_420_SP:I = 0x11

.field public static final YV12:I = 0x32315659
