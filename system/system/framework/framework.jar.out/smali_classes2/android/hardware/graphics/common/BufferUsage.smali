.class public interface abstract annotation Landroid/hardware/graphics/common/BufferUsage;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final CAMERA_INPUT:J = 0x40000L

.field public static final CAMERA_OUTPUT:J = 0x20000L

.field public static final COMPOSER_CLIENT_TARGET:J = 0x1000L

.field public static final COMPOSER_CURSOR:J = 0x8000L

.field public static final COMPOSER_OVERLAY:J = 0x800L

.field public static final CPU_READ_MASK:J = 0xfL

.field public static final CPU_READ_NEVER:J = 0x0L

.field public static final CPU_READ_OFTEN:J = 0x3L

.field public static final CPU_READ_RARELY:J = 0x2L

.field public static final CPU_WRITE_MASK:J = 0xf0L

.field public static final CPU_WRITE_NEVER:J = 0x0L

.field public static final CPU_WRITE_OFTEN:J = 0x30L

.field public static final CPU_WRITE_RARELY:J = 0x20L

.field public static final FRONT_BUFFER:J = 0x100000000L

.field public static final GPU_CUBE_MAP:J = 0x2000000L

.field public static final GPU_DATA_BUFFER:J = 0x1000000L

.field public static final GPU_MIPMAP_COMPLETE:J = 0x4000000L

.field public static final GPU_RENDER_TARGET:J = 0x200L

.field public static final GPU_TEXTURE:J = 0x100L

.field public static final HW_IMAGE_ENCODER:J = 0x8000000L

.field public static final PROTECTED:J = 0x4000L

.field public static final RENDERSCRIPT:J = 0x100000L

.field public static final SENSOR_DIRECT_DATA:J = 0x800000L

.field public static final VENDOR_MASK:J = -0x10000000L

.field public static final VENDOR_MASK_HI:J = -0x1000000000000L

.field public static final VIDEO_DECODER:J = 0x400000L

.field public static final VIDEO_ENCODER:J = 0x10000L
