.class public interface abstract annotation Landroid/hardware/graphics/common/ColorTransform;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final ARBITRARY_MATRIX:I = 0x1

.field public static final CORRECT_DEUTERANOPIA:I = 0x5

.field public static final CORRECT_PROTANOPIA:I = 0x4

.field public static final CORRECT_TRITANOPIA:I = 0x6

.field public static final GRAYSCALE:I = 0x3

.field public static final IDENTITY:I = 0x0

.field public static final VALUE_INVERSE:I = 0x2
