.class public interface abstract annotation Landroid/hardware/graphics/common/BlendMode;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final COVERAGE:I = 0x3

.field public static final INVALID:I = 0x0

.field public static final NONE:I = 0x1

.field public static final PREMULTIPLIED:I = 0x2
