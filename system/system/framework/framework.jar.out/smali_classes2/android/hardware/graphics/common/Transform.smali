.class public interface abstract annotation Landroid/hardware/graphics/common/Transform;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final FLIP_H:I = 0x1

.field public static final FLIP_V:I = 0x2

.field public static final NONE:I = 0x0

.field public static final ROT_180:I = 0x3

.field public static final ROT_270:I = 0x7

.field public static final ROT_90:I = 0x4
