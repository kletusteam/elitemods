.class public interface abstract annotation Landroid/hardware/graphics/common/Dataspace;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final ADOBE_RGB:I = 0x90b0000

.field public static final ARBITRARY:I = 0x1

.field public static final BT2020:I = 0x8c60000

.field public static final BT2020_HLG:I = 0xa060000

.field public static final BT2020_ITU:I = 0x10c60000

.field public static final BT2020_ITU_HLG:I = 0x12060000

.field public static final BT2020_ITU_PQ:I = 0x11c60000

.field public static final BT2020_LINEAR:I = 0x8460000

.field public static final BT2020_PQ:I = 0x9c60000

.field public static final BT601_525:I = 0x10c40000

.field public static final BT601_625:I = 0x10c20000

.field public static final BT709:I = 0x10c10000

.field public static final BT709_FULL_RANGE:I = 0x8c10000

.field public static final DCI_P3:I = 0x94a0000

.field public static final DCI_P3_LINEAR:I = 0x84a0000

.field public static final DEPTH:I = 0x1000

.field public static final DISPLAY_BT2020:I = 0x8860000

.field public static final DISPLAY_P3:I = 0x88a0000

.field public static final DISPLAY_P3_LINEAR:I = 0x84a0000

.field public static final DYNAMIC_DEPTH:I = 0x1002

.field public static final HEIF:I = 0x1004

.field public static final JFIF:I = 0x8c20000

.field public static final JPEG_APP_SEGMENTS:I = 0x1003

.field public static final RANGE_EXTENDED:I = 0x18000000

.field public static final RANGE_FULL:I = 0x8000000

.field public static final RANGE_LIMITED:I = 0x10000000

.field public static final RANGE_MASK:I = 0x38000000

.field public static final RANGE_SHIFT:I = 0x1b

.field public static final RANGE_UNSPECIFIED:I = 0x0

.field public static final SCRGB:I = 0x18810000

.field public static final SCRGB_LINEAR:I = 0x18410000

.field public static final SENSOR:I = 0x1001

.field public static final SRGB:I = 0x8810000

.field public static final SRGB_LINEAR:I = 0x8410000

.field public static final STANDARD_ADOBE_RGB:I = 0xb0000

.field public static final STANDARD_BT2020:I = 0x60000

.field public static final STANDARD_BT2020_CONSTANT_LUMINANCE:I = 0x70000

.field public static final STANDARD_BT470M:I = 0x80000

.field public static final STANDARD_BT601_525:I = 0x40000

.field public static final STANDARD_BT601_525_UNADJUSTED:I = 0x50000

.field public static final STANDARD_BT601_625:I = 0x20000

.field public static final STANDARD_BT601_625_UNADJUSTED:I = 0x30000

.field public static final STANDARD_BT709:I = 0x10000

.field public static final STANDARD_DCI_P3:I = 0xa0000

.field public static final STANDARD_FILM:I = 0x90000

.field public static final STANDARD_MASK:I = 0x3f0000

.field public static final STANDARD_SHIFT:I = 0x10

.field public static final STANDARD_UNSPECIFIED:I = 0x0

.field public static final TRANSFER_GAMMA2_2:I = 0x1000000

.field public static final TRANSFER_GAMMA2_6:I = 0x1400000

.field public static final TRANSFER_GAMMA2_8:I = 0x1800000

.field public static final TRANSFER_HLG:I = 0x2000000

.field public static final TRANSFER_LINEAR:I = 0x400000

.field public static final TRANSFER_MASK:I = 0x7c00000

.field public static final TRANSFER_SHIFT:I = 0x16

.field public static final TRANSFER_SMPTE_170M:I = 0xc00000

.field public static final TRANSFER_SRGB:I = 0x800000

.field public static final TRANSFER_ST2084:I = 0x1c00000

.field public static final TRANSFER_UNSPECIFIED:I

.field public static final UNKNOWN:I
