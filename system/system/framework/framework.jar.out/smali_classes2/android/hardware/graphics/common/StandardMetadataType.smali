.class public interface abstract annotation Landroid/hardware/graphics/common/StandardMetadataType;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final ALLOCATION_SIZE:J = 0xaL

.field public static final BLEND_MODE:J = 0x12L

.field public static final BUFFER_ID:J = 0x1L

.field public static final CHROMA_SITING:J = 0xeL

.field public static final COMPRESSION:J = 0xcL

.field public static final CROP:J = 0x10L

.field public static final CTA861_3:J = 0x14L

.field public static final DATASPACE:J = 0x11L

.field public static final HEIGHT:J = 0x4L

.field public static final INTERLACED:J = 0xdL

.field public static final INVALID:J = 0x0L

.field public static final LAYER_COUNT:J = 0x5L

.field public static final NAME:J = 0x2L

.field public static final PIXEL_FORMAT_FOURCC:J = 0x7L

.field public static final PIXEL_FORMAT_MODIFIER:J = 0x8L

.field public static final PIXEL_FORMAT_REQUESTED:J = 0x6L

.field public static final PLANE_LAYOUTS:J = 0xfL

.field public static final PROTECTED_CONTENT:J = 0xbL

.field public static final SMPTE2086:J = 0x13L

.field public static final SMPTE2094_10:J = 0x16L

.field public static final SMPTE2094_40:J = 0x15L

.field public static final USAGE:J = 0x9L

.field public static final WIDTH:J = 0x3L
