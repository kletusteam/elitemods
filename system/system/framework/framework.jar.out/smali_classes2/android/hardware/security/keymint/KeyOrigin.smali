.class public interface abstract annotation Landroid/hardware/security/keymint/KeyOrigin;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final DERIVED:I = 0x1

.field public static final GENERATED:I = 0x0

.field public static final IMPORTED:I = 0x2

.field public static final RESERVED:I = 0x3

.field public static final SECURELY_IMPORTED:I = 0x4
