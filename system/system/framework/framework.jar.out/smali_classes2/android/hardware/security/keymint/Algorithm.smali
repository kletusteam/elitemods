.class public interface abstract annotation Landroid/hardware/security/keymint/Algorithm;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final AES:I = 0x20

.field public static final EC:I = 0x3

.field public static final HMAC:I = 0x80

.field public static final RSA:I = 0x1

.field public static final TRIPLE_DES:I = 0x21
