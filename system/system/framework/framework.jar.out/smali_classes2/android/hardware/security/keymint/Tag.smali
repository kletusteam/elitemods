.class public interface abstract annotation Landroid/hardware/security/keymint/Tag;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final ACTIVE_DATETIME:I = 0x60000190

.field public static final ALGORITHM:I = 0x10000002

.field public static final ALLOW_WHILE_ON_BODY:I = 0x700001fa

.field public static final APPLICATION_DATA:I = -0x6ffffd44

.field public static final APPLICATION_ID:I = -0x6ffffda7

.field public static final ASSOCIATED_DATA:I = -0x6ffffc18

.field public static final ATTESTATION_APPLICATION_ID:I = -0x6ffffd3b

.field public static final ATTESTATION_CHALLENGE:I = -0x6ffffd3c

.field public static final ATTESTATION_ID_BRAND:I = -0x6ffffd3a

.field public static final ATTESTATION_ID_DEVICE:I = -0x6ffffd39

.field public static final ATTESTATION_ID_IMEI:I = -0x6ffffd36

.field public static final ATTESTATION_ID_MANUFACTURER:I = -0x6ffffd34

.field public static final ATTESTATION_ID_MEID:I = -0x6ffffd35

.field public static final ATTESTATION_ID_MODEL:I = -0x6ffffd33

.field public static final ATTESTATION_ID_PRODUCT:I = -0x6ffffd38

.field public static final ATTESTATION_ID_SERIAL:I = -0x6ffffd37

.field public static final AUTH_TIMEOUT:I = 0x300001f9

.field public static final BLOCK_MODE:I = 0x20000004

.field public static final BOOTLOADER_ONLY:I = 0x7000012e

.field public static final BOOT_PATCHLEVEL:I = 0x300002cf

.field public static final CALLER_NONCE:I = 0x70000007

.field public static final CERTIFICATE_NOT_AFTER:I = 0x600003f1

.field public static final CERTIFICATE_NOT_BEFORE:I = 0x600003f0

.field public static final CERTIFICATE_SERIAL:I = -0x7ffffc12

.field public static final CERTIFICATE_SUBJECT:I = -0x6ffffc11

.field public static final CONFIRMATION_TOKEN:I = -0x6ffffc13

.field public static final CREATION_DATETIME:I = 0x600002bd

.field public static final DEVICE_UNIQUE_ATTESTATION:I = 0x700002d0

.field public static final DIGEST:I = 0x20000005

.field public static final EARLY_BOOT_ONLY:I = 0x70000131

.field public static final EC_CURVE:I = 0x1000000a

.field public static final HARDWARE_TYPE:I = 0x10000130

.field public static final IDENTITY_CREDENTIAL_KEY:I = 0x700002d1

.field public static final INCLUDE_UNIQUE_ID:I = 0x700000ca

.field public static final INVALID:I = 0x0

.field public static final KEY_SIZE:I = 0x30000003

.field public static final MAC_LENGTH:I = 0x300003eb

.field public static final MAX_BOOT_LEVEL:I = 0x300003f2

.field public static final MAX_USES_PER_BOOT:I = 0x30000194

.field public static final MIN_MAC_LENGTH:I = 0x30000008

.field public static final MIN_SECONDS_BETWEEN_OPS:I = 0x30000193

.field public static final NONCE:I = -0x6ffffc17

.field public static final NO_AUTH_REQUIRED:I = 0x700001f7

.field public static final ORIGIN:I = 0x100002be

.field public static final ORIGINATION_EXPIRE_DATETIME:I = 0x60000191

.field public static final OS_PATCHLEVEL:I = 0x300002c2

.field public static final OS_VERSION:I = 0x300002c1

.field public static final PADDING:I = 0x20000006

.field public static final PURPOSE:I = 0x20000001

.field public static final RESET_SINCE_ID_ROTATION:I = 0x700003ec

.field public static final ROLLBACK_RESISTANCE:I = 0x7000012f

.field public static final ROOT_OF_TRUST:I = -0x6ffffd40

.field public static final RSA_OAEP_MGF_DIGEST:I = 0x200000cb

.field public static final RSA_PUBLIC_EXPONENT:I = 0x500000c8

.field public static final STORAGE_KEY:I = 0x700002d2

.field public static final TRUSTED_CONFIRMATION_REQUIRED:I = 0x700001fc

.field public static final TRUSTED_USER_PRESENCE_REQUIRED:I = 0x700001fb

.field public static final UNIQUE_ID:I = -0x6ffffd3d

.field public static final UNLOCKED_DEVICE_REQUIRED:I = 0x700001fd

.field public static final USAGE_COUNT_LIMIT:I = 0x30000195

.field public static final USAGE_EXPIRE_DATETIME:I = 0x60000192

.field public static final USER_AUTH_TYPE:I = 0x100001f8

.field public static final USER_ID:I = 0x300001f5

.field public static final USER_SECURE_ID:I = -0x5ffffe0a

.field public static final VENDOR_PATCHLEVEL:I = 0x300002ce
