.class public interface abstract annotation Landroid/hardware/security/keymint/SecurityLevel;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final KEYSTORE:I = 0x64

.field public static final SOFTWARE:I = 0x0

.field public static final STRONGBOX:I = 0x2

.field public static final TRUSTED_ENVIRONMENT:I = 0x1
