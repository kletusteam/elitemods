.class public interface abstract annotation Landroid/hardware/security/keymint/TagType;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final BIGNUM:I = -0x80000000

.field public static final BOOL:I = 0x70000000

.field public static final BYTES:I = -0x70000000

.field public static final DATE:I = 0x60000000

.field public static final ENUM:I = 0x10000000

.field public static final ENUM_REP:I = 0x20000000

.field public static final INVALID:I = 0x0

.field public static final UINT:I = 0x30000000

.field public static final UINT_REP:I = 0x40000000

.field public static final ULONG:I = 0x50000000

.field public static final ULONG_REP:I = -0x60000000
