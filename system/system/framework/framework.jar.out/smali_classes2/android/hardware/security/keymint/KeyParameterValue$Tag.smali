.class public interface abstract annotation Landroid/hardware/security/keymint/KeyParameterValue$Tag;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/security/keymint/KeyParameterValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2609
    name = "Tag"
.end annotation


# static fields
.field public static final algorithm:I = 0x1

.field public static final blob:I = 0xe

.field public static final blockMode:I = 0x2

.field public static final boolValue:I = 0xa

.field public static final dateTime:I = 0xd

.field public static final digest:I = 0x4

.field public static final ecCurve:I = 0x5

.field public static final hardwareAuthenticatorType:I = 0x8

.field public static final integer:I = 0xb

.field public static final invalid:I = 0x0

.field public static final keyPurpose:I = 0x7

.field public static final longInteger:I = 0xc

.field public static final origin:I = 0x6

.field public static final paddingMode:I = 0x3

.field public static final securityLevel:I = 0x9
