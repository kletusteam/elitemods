.class public interface abstract annotation Landroid/hardware/security/keymint/EcCurve;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final CURVE_25519:I = 0x4

.field public static final P_224:I = 0x0

.field public static final P_256:I = 0x1

.field public static final P_384:I = 0x2

.field public static final P_521:I = 0x3
