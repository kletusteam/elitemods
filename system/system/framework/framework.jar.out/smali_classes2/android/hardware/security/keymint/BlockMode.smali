.class public interface abstract annotation Landroid/hardware/security/keymint/BlockMode;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final CBC:I = 0x2

.field public static final CTR:I = 0x3

.field public static final ECB:I = 0x1

.field public static final GCM:I = 0x20
