.class public interface abstract annotation Landroid/hardware/security/keymint/Digest;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final MD5:I = 0x1

.field public static final NONE:I = 0x0

.field public static final SHA1:I = 0x2

.field public static final SHA_2_224:I = 0x3

.field public static final SHA_2_256:I = 0x4

.field public static final SHA_2_384:I = 0x5

.field public static final SHA_2_512:I = 0x6
