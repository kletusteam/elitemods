.class public interface abstract annotation Landroid/hardware/vibrator/Effect;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final CLICK:I = 0x0

.field public static final DOUBLE_CLICK:I = 0x1

.field public static final HEAVY_CLICK:I = 0x5

.field public static final POP:I = 0x4

.field public static final RINGTONE_1:I = 0x6

.field public static final RINGTONE_10:I = 0xf

.field public static final RINGTONE_11:I = 0x10

.field public static final RINGTONE_12:I = 0x11

.field public static final RINGTONE_13:I = 0x12

.field public static final RINGTONE_14:I = 0x13

.field public static final RINGTONE_15:I = 0x14

.field public static final RINGTONE_2:I = 0x7

.field public static final RINGTONE_3:I = 0x8

.field public static final RINGTONE_4:I = 0x9

.field public static final RINGTONE_5:I = 0xa

.field public static final RINGTONE_6:I = 0xb

.field public static final RINGTONE_7:I = 0xc

.field public static final RINGTONE_8:I = 0xd

.field public static final RINGTONE_9:I = 0xe

.field public static final TEXTURE_TICK:I = 0x15

.field public static final THUD:I = 0x3

.field public static final TICK:I = 0x2
