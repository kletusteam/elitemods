.class public interface abstract annotation Landroid/hardware/vibrator/CompositePrimitive;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final CLICK:I = 0x1

.field public static final LIGHT_TICK:I = 0x7

.field public static final LOW_TICK:I = 0x8

.field public static final NOOP:I = 0x0

.field public static final QUICK_FALL:I = 0x6

.field public static final QUICK_RISE:I = 0x4

.field public static final SLOW_RISE:I = 0x5

.field public static final SPIN:I = 0x3

.field public static final THUD:I = 0x2
