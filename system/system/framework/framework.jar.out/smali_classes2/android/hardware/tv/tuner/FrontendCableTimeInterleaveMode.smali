.class public interface abstract annotation Landroid/hardware/tv/tuner/FrontendCableTimeInterleaveMode;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final AUTO:I = 0x1

.field public static final INTERLEAVING_128_1_0:I = 0x2

.field public static final INTERLEAVING_128_1_1:I = 0x4

.field public static final INTERLEAVING_128_2:I = 0x80

.field public static final INTERLEAVING_128_3:I = 0x100

.field public static final INTERLEAVING_128_4:I = 0x200

.field public static final INTERLEAVING_16_8:I = 0x20

.field public static final INTERLEAVING_32_4:I = 0x10

.field public static final INTERLEAVING_64_2:I = 0x8

.field public static final INTERLEAVING_8_16:I = 0x40

.field public static final UNDEFINED:I
