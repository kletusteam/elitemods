.class public interface abstract annotation Landroid/hardware/tv/tuner/DemuxFilterEvent$Tag;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/tv/tuner/DemuxFilterEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2609
    name = "Tag"
.end annotation


# static fields
.field public static final download:I = 0x5

.field public static final ipPayload:I = 0x6

.field public static final media:I = 0x1

.field public static final mmtpRecord:I = 0x4

.field public static final monitorEvent:I = 0x8

.field public static final pes:I = 0x2

.field public static final section:I = 0x0

.field public static final startId:I = 0x9

.field public static final temi:I = 0x7

.field public static final tsRecord:I = 0x3
