.class public interface abstract annotation Landroid/hardware/tv/tuner/AudioStreamType;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final AAC:I = 0x6

.field public static final AAC_ADTS:I = 0x10

.field public static final AAC_HE_ADTS:I = 0x12

.field public static final AAC_HE_LATM:I = 0x13

.field public static final AAC_LATM:I = 0x11

.field public static final AC3:I = 0x7

.field public static final AC4:I = 0x9

.field public static final DRA:I = 0xf

.field public static final DTS:I = 0xa

.field public static final DTS_HD:I = 0xb

.field public static final EAC3:I = 0x8

.field public static final MP3:I = 0x2

.field public static final MPEG1:I = 0x3

.field public static final MPEG2:I = 0x4

.field public static final MPEGH:I = 0x5

.field public static final OPUS:I = 0xd

.field public static final PCM:I = 0x1

.field public static final UNDEFINED:I = 0x0

.field public static final VORBIS:I = 0xe

.field public static final WMA:I = 0xc
