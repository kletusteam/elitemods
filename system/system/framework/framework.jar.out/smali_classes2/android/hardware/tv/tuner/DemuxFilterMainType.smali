.class public interface abstract annotation Landroid/hardware/tv/tuner/DemuxFilterMainType;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final ALP:I = 0x10

.field public static final IP:I = 0x4

.field public static final MMTP:I = 0x2

.field public static final TLV:I = 0x8

.field public static final TS:I = 0x1

.field public static final UNDEFINED:I
