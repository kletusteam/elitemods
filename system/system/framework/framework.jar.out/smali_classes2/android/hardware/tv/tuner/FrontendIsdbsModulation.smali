.class public interface abstract annotation Landroid/hardware/tv/tuner/FrontendIsdbsModulation;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final AUTO:I = 0x1

.field public static final MOD_BPSK:I = 0x2

.field public static final MOD_QPSK:I = 0x4

.field public static final MOD_TC8PSK:I = 0x8

.field public static final UNDEFINED:I
