.class public interface abstract annotation Landroid/hardware/tv/tuner/DemuxFilterSubType$Tag;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/tv/tuner/DemuxFilterSubType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2609
    name = "Tag"
.end annotation


# static fields
.field public static final alpFilterType:I = 0x4

.field public static final ipFilterType:I = 0x2

.field public static final mmtpFilterType:I = 0x1

.field public static final tlvFilterType:I = 0x3

.field public static final tsFilterType:I
