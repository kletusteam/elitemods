.class public interface abstract annotation Landroid/hardware/tv/tuner/FrontendDvbsRolloff;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final ROLLOFF_0_10:I = 0x5

.field public static final ROLLOFF_0_15:I = 0x4

.field public static final ROLLOFF_0_20:I = 0x3

.field public static final ROLLOFF_0_25:I = 0x2

.field public static final ROLLOFF_0_35:I = 0x1

.field public static final ROLLOFF_0_5:I = 0x6

.field public static final UNDEFINED:I
