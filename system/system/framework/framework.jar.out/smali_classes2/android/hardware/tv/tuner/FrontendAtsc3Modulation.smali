.class public interface abstract annotation Landroid/hardware/tv/tuner/FrontendAtsc3Modulation;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final AUTO:I = 0x1

.field public static final MOD_1024QAM:I = 0x20

.field public static final MOD_16QAM:I = 0x4

.field public static final MOD_256QAM:I = 0x10

.field public static final MOD_4096QAM:I = 0x40

.field public static final MOD_64QAM:I = 0x8

.field public static final MOD_QPSK:I = 0x2

.field public static final UNDEFINED:I
