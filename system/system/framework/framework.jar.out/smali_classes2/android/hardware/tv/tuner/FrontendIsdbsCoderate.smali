.class public interface abstract annotation Landroid/hardware/tv/tuner/FrontendIsdbsCoderate;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final AUTO:I = 0x1

.field public static final CODERATE_1_2:I = 0x2

.field public static final CODERATE_2_3:I = 0x4

.field public static final CODERATE_3_4:I = 0x8

.field public static final CODERATE_5_6:I = 0x10

.field public static final CODERATE_7_8:I = 0x20

.field public static final UNDEFINED:I
