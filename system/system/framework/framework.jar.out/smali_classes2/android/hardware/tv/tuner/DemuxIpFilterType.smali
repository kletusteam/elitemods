.class public interface abstract annotation Landroid/hardware/tv/tuner/DemuxIpFilterType;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final IP:I = 0x4

.field public static final IP_PAYLOAD:I = 0x3

.field public static final NTP:I = 0x2

.field public static final PAYLOAD_THROUGH:I = 0x5

.field public static final SECTION:I = 0x1

.field public static final UNDEFINED:I
