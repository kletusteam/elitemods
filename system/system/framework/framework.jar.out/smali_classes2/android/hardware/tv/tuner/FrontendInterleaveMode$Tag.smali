.class public interface abstract annotation Landroid/hardware/tv/tuner/FrontendInterleaveMode$Tag;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/tv/tuner/FrontendInterleaveMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2609
    name = "Tag"
.end annotation


# static fields
.field public static final atsc3:I = 0x0

.field public static final dtmb:I = 0x2

.field public static final dvbc:I = 0x1

.field public static final isdbt:I = 0x3
