.class public interface abstract annotation Landroid/hardware/tv/tuner/FrontendType;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final ANALOG:I = 0x1

.field public static final ATSC:I = 0x2

.field public static final ATSC3:I = 0x3

.field public static final DTMB:I = 0xa

.field public static final DVBC:I = 0x4

.field public static final DVBS:I = 0x5

.field public static final DVBT:I = 0x6

.field public static final ISDBS:I = 0x7

.field public static final ISDBS3:I = 0x8

.field public static final ISDBT:I = 0x9

.field public static final UNDEFINED:I
