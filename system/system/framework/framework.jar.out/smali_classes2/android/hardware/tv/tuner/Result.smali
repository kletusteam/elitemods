.class public interface abstract annotation Landroid/hardware/tv/tuner/Result;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final INVALID_ARGUMENT:I = 0x4

.field public static final INVALID_STATE:I = 0x3

.field public static final NOT_INITIALIZED:I = 0x2

.field public static final OUT_OF_MEMORY:I = 0x5

.field public static final SUCCESS:I = 0x0

.field public static final UNAVAILABLE:I = 0x1

.field public static final UNKNOWN_ERROR:I = 0x6
