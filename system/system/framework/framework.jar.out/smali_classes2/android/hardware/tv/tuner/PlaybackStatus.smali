.class public interface abstract annotation Landroid/hardware/tv/tuner/PlaybackStatus;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final SPACE_ALMOST_EMPTY:I = 0x2

.field public static final SPACE_ALMOST_FULL:I = 0x4

.field public static final SPACE_EMPTY:I = 0x1

.field public static final SPACE_FULL:I = 0x8
