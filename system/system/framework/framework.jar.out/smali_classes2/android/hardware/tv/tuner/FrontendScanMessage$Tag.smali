.class public interface abstract annotation Landroid/hardware/tv/tuner/FrontendScanMessage$Tag;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/tv/tuner/FrontendScanMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2609
    name = "Tag"
.end annotation


# static fields
.field public static final analogType:I = 0x6

.field public static final annex:I = 0xd

.field public static final atsc3PlpInfos:I = 0xb

.field public static final dvbtCellIds:I = 0xf

.field public static final frequencies:I = 0x3

.field public static final groupIds:I = 0x8

.field public static final hierarchy:I = 0x5

.field public static final inputStreamIds:I = 0x9

.field public static final isEnd:I = 0x1

.field public static final isHighPriority:I = 0xe

.field public static final isLocked:I = 0x0

.field public static final modulation:I = 0xc

.field public static final plpIds:I = 0x7

.field public static final progressPercent:I = 0x2

.field public static final std:I = 0xa

.field public static final symbolRates:I = 0x4
