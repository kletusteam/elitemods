.class public interface abstract annotation Landroid/hardware/tv/tuner/FrontendStatusReadiness;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final STABLE:I = 0x3

.field public static final UNAVAILABLE:I = 0x1

.field public static final UNDEFINED:I = 0x0

.field public static final UNSTABLE:I = 0x2

.field public static final UNSUPPORTED:I = 0x4
