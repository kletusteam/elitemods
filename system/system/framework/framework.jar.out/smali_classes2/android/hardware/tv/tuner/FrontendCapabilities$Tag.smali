.class public interface abstract annotation Landroid/hardware/tv/tuner/FrontendCapabilities$Tag;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/tv/tuner/FrontendCapabilities;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2609
    name = "Tag"
.end annotation


# static fields
.field public static final analogCaps:I = 0x0

.field public static final atsc3Caps:I = 0x2

.field public static final atscCaps:I = 0x1

.field public static final dtmbCaps:I = 0x3

.field public static final dvbcCaps:I = 0x5

.field public static final dvbsCaps:I = 0x4

.field public static final dvbtCaps:I = 0x6

.field public static final isdbs3Caps:I = 0x8

.field public static final isdbsCaps:I = 0x7

.field public static final isdbtCaps:I = 0x9
