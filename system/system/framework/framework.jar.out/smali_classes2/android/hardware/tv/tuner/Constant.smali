.class public interface abstract annotation Landroid/hardware/tv/tuner/Constant;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final INVALID_AV_SYNC_ID:I = -0x1

.field public static final INVALID_FILTER_ID:I = -0x1

.field public static final INVALID_FIRST_MACROBLOCK_IN_SLICE:I = -0x1

.field public static final INVALID_FRONTEND_ID:I = -0x1

.field public static final INVALID_FRONTEND_SETTING_FREQUENCY:I = -0x1

.field public static final INVALID_IP_FILTER_CONTEXT_ID:I = -0x1

.field public static final INVALID_KEYTOKEN:I = 0x0

.field public static final INVALID_LNB_ID:I = -0x1

.field public static final INVALID_LTS_ID:I = -0x1

.field public static final INVALID_MMTP_RECORD_EVENT_MPT_SEQUENCE_NUM:I = -0x1

.field public static final INVALID_STREAM_ID:I = 0xffff

.field public static final INVALID_TABINFO_VERSION:I = -0x1

.field public static final INVALID_TS_PID:I = 0xffff
