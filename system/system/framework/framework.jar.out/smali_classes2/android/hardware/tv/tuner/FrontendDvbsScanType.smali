.class public interface abstract annotation Landroid/hardware/tv/tuner/FrontendDvbsScanType;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final DIRECT:I = 0x1

.field public static final DISEQC:I = 0x2

.field public static final JESS:I = 0x4

.field public static final UNDEFINED:I = 0x0

.field public static final UNICABLE:I = 0x3
