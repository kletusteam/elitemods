.class public interface abstract annotation Landroid/hardware/tv/tuner/DemuxScHevcIndex;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final AUD:I = 0x2

.field public static final SLICE_BLA_N_LP:I = 0x10

.field public static final SLICE_BLA_W_RADL:I = 0x8

.field public static final SLICE_CE_BLA_W_LP:I = 0x4

.field public static final SLICE_IDR_N_LP:I = 0x40

.field public static final SLICE_IDR_W_RADL:I = 0x20

.field public static final SLICE_TRAIL_CRA:I = 0x80

.field public static final SPS:I = 0x1

.field public static final UNDEFINED:I
