.class public interface abstract annotation Landroid/hardware/tv/tuner/FrontendDvbsModulation;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final AUTO:I = 0x1

.field public static final MOD_128APSK:I = 0x800

.field public static final MOD_16APSK:I = 0x100

.field public static final MOD_16PSK:I = 0x10

.field public static final MOD_16QAM:I = 0x8

.field public static final MOD_256APSK:I = 0x1000

.field public static final MOD_32APSK:I = 0x200

.field public static final MOD_32PSK:I = 0x20

.field public static final MOD_64APSK:I = 0x400

.field public static final MOD_8APSK:I = 0x80

.field public static final MOD_8PSK:I = 0x4

.field public static final MOD_ACM:I = 0x40

.field public static final MOD_QPSK:I = 0x2

.field public static final MOD_RESERVED:I = 0x2000

.field public static final UNDEFINED:I
