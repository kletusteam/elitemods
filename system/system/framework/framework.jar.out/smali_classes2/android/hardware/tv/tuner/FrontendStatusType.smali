.class public interface abstract annotation Landroid/hardware/tv/tuner/FrontendStatusType;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final AGC:I = 0xe

.field public static final ATSC3_ALL_PLP_INFO:I = 0x29

.field public static final ATSC3_PLP_INFO:I = 0x15

.field public static final BANDWIDTH:I = 0x19

.field public static final BER:I = 0x2

.field public static final BERS:I = 0x17

.field public static final CODERATES:I = 0x18

.field public static final DEMOD_LOCK:I = 0x0

.field public static final DVBT_CELL_IDS:I = 0x28

.field public static final EWBS:I = 0xd

.field public static final FEC:I = 0x8

.field public static final FREQ_OFFSET:I = 0x12

.field public static final GUARD_INTERVAL:I = 0x1a

.field public static final HIERARCHY:I = 0x13

.field public static final INTERLEAVINGS:I = 0x1e

.field public static final ISDBT_MODE:I = 0x25

.field public static final ISDBT_PARTIAL_RECEPTION_FLAG:I = 0x26

.field public static final ISDBT_SEGMENTS:I = 0x1f

.field public static final IS_LINEAR:I = 0x23

.field public static final IS_MISO:I = 0x22

.field public static final IS_SHORT_FRAMES:I = 0x24

.field public static final LAYER_ERROR:I = 0x10

.field public static final LNA:I = 0xf

.field public static final LNB_VOLTAGE:I = 0xb

.field public static final MER:I = 0x11

.field public static final MODULATION:I = 0x9

.field public static final MODULATIONS:I = 0x16

.field public static final PER:I = 0x3

.field public static final PLP_ID:I = 0xc

.field public static final PRE_BER:I = 0x4

.field public static final RF_LOCK:I = 0x14

.field public static final ROLL_OFF:I = 0x21

.field public static final SIGNAL_QUALITY:I = 0x5

.field public static final SIGNAL_STRENGTH:I = 0x6

.field public static final SNR:I = 0x1

.field public static final SPECTRAL:I = 0xa

.field public static final STREAM_ID_LIST:I = 0x27

.field public static final SYMBOL_RATE:I = 0x7

.field public static final T2_SYSTEM_ID:I = 0x1d

.field public static final TRANSMISSION_MODE:I = 0x1b

.field public static final TS_DATA_RATES:I = 0x20

.field public static final UEC:I = 0x1c
