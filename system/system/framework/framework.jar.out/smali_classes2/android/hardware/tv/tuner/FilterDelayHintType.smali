.class public interface abstract annotation Landroid/hardware/tv/tuner/FilterDelayHintType;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final DATA_SIZE_DELAY_IN_BYTES:I = 0x2

.field public static final INVALID:I = 0x0

.field public static final TIME_DELAY_IN_MS:I = 0x1
