.class public interface abstract annotation Landroid/hardware/tv/tuner/DemuxMmtpFilterType;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final AUDIO:I = 0x4

.field public static final DOWNLOAD:I = 0x7

.field public static final MMTP:I = 0x3

.field public static final PES:I = 0x2

.field public static final RECORD:I = 0x6

.field public static final SECTION:I = 0x1

.field public static final UNDEFINED:I = 0x0

.field public static final VIDEO:I = 0x5
