.class public interface abstract annotation Landroid/hardware/tv/tuner/FrontendDvbcBandwidth;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final BANDWIDTH_5MHZ:I = 0x1

.field public static final BANDWIDTH_6MHZ:I = 0x2

.field public static final BANDWIDTH_7MHZ:I = 0x4

.field public static final BANDWIDTH_8MHZ:I = 0x8

.field public static final UNDEFINED:I
