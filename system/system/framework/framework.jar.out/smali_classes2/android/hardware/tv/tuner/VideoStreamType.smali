.class public interface abstract annotation Landroid/hardware/tv/tuner/VideoStreamType;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final AV1:I = 0xa

.field public static final AVC:I = 0x5

.field public static final AVS:I = 0xb

.field public static final AVS2:I = 0xc

.field public static final HEVC:I = 0x6

.field public static final MPEG1:I = 0x2

.field public static final MPEG2:I = 0x3

.field public static final MPEG4P2:I = 0x4

.field public static final RESERVED:I = 0x1

.field public static final UNDEFINED:I = 0x0

.field public static final VC1:I = 0x7

.field public static final VP8:I = 0x8

.field public static final VP9:I = 0x9
