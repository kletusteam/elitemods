.class public interface abstract annotation Landroid/hardware/tv/tuner/DataFormat;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final ES:I = 0x2

.field public static final PES:I = 0x1

.field public static final SHV_TLV:I = 0x3

.field public static final TS:I = 0x0

.field public static final UNDEFINED:I = 0x4
