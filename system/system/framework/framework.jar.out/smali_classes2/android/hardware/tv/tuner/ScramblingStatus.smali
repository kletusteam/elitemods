.class public interface abstract annotation Landroid/hardware/tv/tuner/ScramblingStatus;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final NOT_SCRAMBLED:I = 0x2

.field public static final SCRAMBLED:I = 0x4

.field public static final UNKNOWN:I = 0x1
