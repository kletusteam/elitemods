.class public interface abstract annotation Landroid/hardware/tv/tuner/FrontendDtmbModulation;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final AUTO:I = 0x1

.field public static final CONSTELLATION_16QAM:I = 0x8

.field public static final CONSTELLATION_32QAM:I = 0x10

.field public static final CONSTELLATION_4QAM:I = 0x2

.field public static final CONSTELLATION_4QAM_NR:I = 0x4

.field public static final CONSTELLATION_64QAM:I = 0x20

.field public static final UNDEFINED:I
