.class public interface abstract annotation Landroid/hardware/tv/tuner/FrontendAnalogSifStandard;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final AUTO:I = 0x1

.field public static final BG:I = 0x2

.field public static final BG_A2:I = 0x4

.field public static final BG_NICAM:I = 0x8

.field public static final DK:I = 0x20

.field public static final DK1_A2:I = 0x40

.field public static final DK2_A2:I = 0x80

.field public static final DK3_A2:I = 0x100

.field public static final DK_NICAM:I = 0x200

.field public static final I:I = 0x10

.field public static final I_NICAM:I = 0x8000

.field public static final L:I = 0x400

.field public static final L_NICAM:I = 0x10000

.field public static final L_PRIME:I = 0x20000

.field public static final M:I = 0x800

.field public static final M_A2:I = 0x2000

.field public static final M_BTSC:I = 0x1000

.field public static final M_EIAJ:I = 0x4000

.field public static final UNDEFINED:I
