.class public interface abstract annotation Landroid/hardware/tv/tuner/FrontendStatus$Tag;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/tv/tuner/FrontendStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2609
    name = "Tag"
.end annotation


# static fields
.field public static final agc:I = 0xe

.field public static final allPlpInfo:I = 0x29

.field public static final bandwidth:I = 0x19

.field public static final ber:I = 0x2

.field public static final bers:I = 0x17

.field public static final codeRates:I = 0x18

.field public static final dvbtCellIds:I = 0x28

.field public static final freqOffset:I = 0x12

.field public static final hierarchy:I = 0x13

.field public static final innerFec:I = 0x8

.field public static final interleaving:I = 0x1e

.field public static final interval:I = 0x1a

.field public static final inversion:I = 0xa

.field public static final isDemodLocked:I = 0x0

.field public static final isEWBS:I = 0xd

.field public static final isLayerError:I = 0x10

.field public static final isLinear:I = 0x23

.field public static final isLnaOn:I = 0xf

.field public static final isMiso:I = 0x22

.field public static final isRfLocked:I = 0x14

.field public static final isShortFrames:I = 0x24

.field public static final isdbtMode:I = 0x25

.field public static final isdbtSegment:I = 0x1f

.field public static final lnbVoltage:I = 0xb

.field public static final mer:I = 0x11

.field public static final modulationStatus:I = 0x9

.field public static final modulations:I = 0x16

.field public static final partialReceptionFlag:I = 0x26

.field public static final per:I = 0x3

.field public static final plpId:I = 0xc

.field public static final plpInfo:I = 0x15

.field public static final preBer:I = 0x4

.field public static final rollOff:I = 0x21

.field public static final signalQuality:I = 0x5

.field public static final signalStrength:I = 0x6

.field public static final snr:I = 0x1

.field public static final streamIdList:I = 0x27

.field public static final symbolRate:I = 0x7

.field public static final systemId:I = 0x1d

.field public static final transmissionMode:I = 0x1b

.field public static final tsDataRate:I = 0x20

.field public static final uec:I = 0x1c
