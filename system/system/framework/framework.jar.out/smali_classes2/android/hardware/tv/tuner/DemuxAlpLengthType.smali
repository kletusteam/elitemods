.class public interface abstract annotation Landroid/hardware/tv/tuner/DemuxAlpLengthType;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final UNDEFINED:B = 0x0t

.field public static final WITHOUT_ADDITIONAL_HEADER:B = 0x1t

.field public static final WITH_ADDITIONAL_HEADER:B = 0x2t
