.class public interface abstract annotation Landroid/hardware/tv/tuner/FrontendScanMessageType;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final ANALOG_TYPE:I = 0x6

.field public static final ATSC3_PLP_INFO:I = 0xb

.field public static final DVBC_ANNEX:I = 0xd

.field public static final DVBT_CELL_IDS:I = 0xf

.field public static final END:I = 0x1

.field public static final FREQUENCY:I = 0x3

.field public static final GROUP_IDS:I = 0x8

.field public static final HIERARCHY:I = 0x5

.field public static final HIGH_PRIORITY:I = 0xe

.field public static final INPUT_STREAM_IDS:I = 0x9

.field public static final LOCKED:I = 0x0

.field public static final MODULATION:I = 0xc

.field public static final PLP_IDS:I = 0x7

.field public static final PROGRESS_PERCENT:I = 0x2

.field public static final STANDARD:I = 0xa

.field public static final SYMBOL_RATE:I = 0x4
