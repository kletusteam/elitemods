.class public interface abstract annotation Landroid/hardware/tv/tuner/FrontendAtsc3CodeRate;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final AUTO:I = 0x1

.field public static final CODERATE_10_15:I = 0x200

.field public static final CODERATE_11_15:I = 0x400

.field public static final CODERATE_12_15:I = 0x800

.field public static final CODERATE_13_15:I = 0x1000

.field public static final CODERATE_2_15:I = 0x2

.field public static final CODERATE_3_15:I = 0x4

.field public static final CODERATE_4_15:I = 0x8

.field public static final CODERATE_5_15:I = 0x10

.field public static final CODERATE_6_15:I = 0x20

.field public static final CODERATE_7_15:I = 0x40

.field public static final CODERATE_8_15:I = 0x80

.field public static final CODERATE_9_15:I = 0x100

.field public static final UNDEFINED:I
