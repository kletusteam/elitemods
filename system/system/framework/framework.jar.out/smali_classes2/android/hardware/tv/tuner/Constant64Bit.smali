.class public interface abstract annotation Landroid/hardware/tv/tuner/Constant64Bit;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final INVALID_AV_SYNC_ID_64BIT:J = -0x1L

.field public static final INVALID_FILTER_ID_64BIT:J = -0x1L

.field public static final INVALID_PRESENTATION_TIME_STAMP:J = -0x1L
