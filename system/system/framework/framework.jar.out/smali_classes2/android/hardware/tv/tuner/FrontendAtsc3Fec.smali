.class public interface abstract annotation Landroid/hardware/tv/tuner/FrontendAtsc3Fec;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final AUTO:I = 0x1

.field public static final BCH_LDPC_16K:I = 0x2

.field public static final BCH_LDPC_64K:I = 0x4

.field public static final CRC_LDPC_16K:I = 0x8

.field public static final CRC_LDPC_64K:I = 0x10

.field public static final LDPC_16K:I = 0x20

.field public static final LDPC_64K:I = 0x40

.field public static final UNDEFINED:I
