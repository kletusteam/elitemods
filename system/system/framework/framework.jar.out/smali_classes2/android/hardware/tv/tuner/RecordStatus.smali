.class public interface abstract annotation Landroid/hardware/tv/tuner/RecordStatus;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final DATA_READY:B = 0x1t

.field public static final HIGH_WATER:B = 0x4t

.field public static final LOW_WATER:B = 0x2t

.field public static final OVERFLOW:B = 0x8t
