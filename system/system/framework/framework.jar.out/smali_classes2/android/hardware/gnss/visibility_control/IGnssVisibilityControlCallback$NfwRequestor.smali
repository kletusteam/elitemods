.class public interface abstract annotation Landroid/hardware/gnss/visibility_control/IGnssVisibilityControlCallback$NfwRequestor;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/gnss/visibility_control/IGnssVisibilityControlCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2609
    name = "NfwRequestor"
.end annotation


# static fields
.field public static final AUTOMOBILE_CLIENT:I = 0x14

.field public static final CARRIER:I = 0x0

.field public static final GNSS_CHIPSET_VENDOR:I = 0xc

.field public static final MODEM_CHIPSET_VENDOR:I = 0xb

.field public static final OEM:I = 0xa

.field public static final OTHER_CHIPSET_VENDOR:I = 0xd

.field public static final OTHER_REQUESTOR:I = 0x64
