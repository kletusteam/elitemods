.class public interface abstract annotation Landroid/hardware/gnss/IGnss$GnssPositionMode;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/gnss/IGnss;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2609
    name = "GnssPositionMode"
.end annotation


# static fields
.field public static final MS_ASSISTED:I = 0x2

.field public static final MS_BASED:I = 0x1

.field public static final STANDALONE:I
