.class public interface abstract annotation Landroid/hardware/gnss/IGnssCallback$GnssStatusValue;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/gnss/IGnssCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2609
    name = "GnssStatusValue"
.end annotation


# static fields
.field public static final ENGINE_OFF:I = 0x4

.field public static final ENGINE_ON:I = 0x3

.field public static final NONE:I = 0x0

.field public static final SESSION_BEGIN:I = 0x1

.field public static final SESSION_END:I = 0x2
