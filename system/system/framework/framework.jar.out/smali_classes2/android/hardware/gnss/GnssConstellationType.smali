.class public interface abstract annotation Landroid/hardware/gnss/GnssConstellationType;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final BEIDOU:I = 0x5

.field public static final GALILEO:I = 0x6

.field public static final GLONASS:I = 0x3

.field public static final GPS:I = 0x1

.field public static final IRNSS:I = 0x7

.field public static final QZSS:I = 0x4

.field public static final SBAS:I = 0x2

.field public static final UNKNOWN:I
