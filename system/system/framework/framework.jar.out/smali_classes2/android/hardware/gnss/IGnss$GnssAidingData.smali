.class public interface abstract annotation Landroid/hardware/gnss/IGnss$GnssAidingData;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/gnss/IGnss;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2609
    name = "GnssAidingData"
.end annotation


# static fields
.field public static final ALL:I = 0xffff

.field public static final ALMANAC:I = 0x2

.field public static final CELLDB_INFO:I = 0x8000

.field public static final EPHEMERIS:I = 0x1

.field public static final HEALTH:I = 0x40

.field public static final IONO:I = 0x10

.field public static final POSITION:I = 0x4

.field public static final RTI:I = 0x400

.field public static final SADATA:I = 0x200

.field public static final SVDIR:I = 0x80

.field public static final SVSTEER:I = 0x100

.field public static final TIME:I = 0x8

.field public static final UTC:I = 0x20
