.class public interface abstract annotation Landroid/hardware/gnss/IGnssDebug$SatelliteEphemerisHealth;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/gnss/IGnssDebug;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2609
    name = "SatelliteEphemerisHealth"
.end annotation


# static fields
.field public static final BAD:I = 0x1

.field public static final GOOD:I = 0x0

.field public static final UNKNOWN:I = 0x2
