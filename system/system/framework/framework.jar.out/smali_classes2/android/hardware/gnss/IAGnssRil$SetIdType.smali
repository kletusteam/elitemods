.class public interface abstract annotation Landroid/hardware/gnss/IAGnssRil$SetIdType;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/gnss/IAGnssRil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2609
    name = "SetIdType"
.end annotation


# static fields
.field public static final IMSI:I = 0x1

.field public static final MSISDM:I = 0x2

.field public static final NONE:I
