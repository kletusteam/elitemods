.class public interface abstract annotation Landroid/hardware/gnss/IAGnssCallback$AGnssType;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/gnss/IAGnssCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2609
    name = "AGnssType"
.end annotation


# static fields
.field public static final C2K:I = 0x2

.field public static final SUPL:I = 0x1

.field public static final SUPL_EIMS:I = 0x3

.field public static final SUPL_IMS:I = 0x4
