.class public interface abstract annotation Landroid/hardware/gnss/IGnssNavigationMessageCallback$GnssNavigationMessage$GnssNavigationMessageType;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/gnss/IGnssNavigationMessageCallback$GnssNavigationMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2609
    name = "GnssNavigationMessageType"
.end annotation


# static fields
.field public static final BDS_CNAV1:I = 0x503

.field public static final BDS_CNAV2:I = 0x504

.field public static final BDS_D1:I = 0x501

.field public static final BDS_D2:I = 0x502

.field public static final GAL_F:I = 0x602

.field public static final GAL_I:I = 0x601

.field public static final GLO_L1CA:I = 0x301

.field public static final GPS_CNAV2:I = 0x104

.field public static final GPS_L1CA:I = 0x101

.field public static final GPS_L2CNAV:I = 0x102

.field public static final GPS_L5CNAV:I = 0x103

.field public static final IRN_L5CA:I = 0x701

.field public static final QZS_L1CA:I = 0x401

.field public static final SBS:I = 0x201

.field public static final UNKNOWN:I
