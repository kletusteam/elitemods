.class Landroid/hardware/hdmi/HdmiTimerRecordSources$TimeUnit;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/hdmi/HdmiTimerRecordSources;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TimeUnit"
.end annotation


# instance fields
.field final mHour:I

.field final mMinute:I


# direct methods
.method constructor <init>(II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Landroid/hardware/hdmi/HdmiTimerRecordSources$TimeUnit;->mHour:I

    iput p2, p0, Landroid/hardware/hdmi/HdmiTimerRecordSources$TimeUnit;->mMinute:I

    return-void
.end method

.method static toBcdByte(I)B
    .locals 3

    div-int/lit8 v0, p0, 0xa

    rem-int/lit8 v0, v0, 0xa

    rem-int/lit8 v1, p0, 0xa

    shl-int/lit8 v2, v0, 0x4

    or-int/2addr v2, v1

    int-to-byte v2, v2

    return v2
.end method


# virtual methods
.method toByteArray([BI)I
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    return v0

    :goto_1
    invoke-static {v1}, Landroid/hardware/hdmi/HdmiTimerRecordSources$TimeUnit;->toBcdByte(I)B

    move-result v1

    goto/32 :goto_8

    nop

    :goto_2
    iget v1, p0, Landroid/hardware/hdmi/HdmiTimerRecordSources$TimeUnit;->mMinute:I

    goto/32 :goto_1

    nop

    :goto_3
    invoke-static {v0}, Landroid/hardware/hdmi/HdmiTimerRecordSources$TimeUnit;->toBcdByte(I)B

    move-result v0

    goto/32 :goto_6

    nop

    :goto_4
    add-int/lit8 v0, p2, 0x1

    goto/32 :goto_2

    nop

    :goto_5
    iget v0, p0, Landroid/hardware/hdmi/HdmiTimerRecordSources$TimeUnit;->mHour:I

    goto/32 :goto_3

    nop

    :goto_6
    aput-byte v0, p1, p2

    goto/32 :goto_4

    nop

    :goto_7
    const/4 v0, 0x2

    goto/32 :goto_0

    nop

    :goto_8
    aput-byte v1, p1, v0

    goto/32 :goto_7

    nop
.end method
