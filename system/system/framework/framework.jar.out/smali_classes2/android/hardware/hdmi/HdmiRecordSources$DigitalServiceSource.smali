.class public final Landroid/hardware/hdmi/HdmiRecordSources$DigitalServiceSource;
.super Landroid/hardware/hdmi/HdmiRecordSources$RecordSource;


# annotations
.annotation runtime Landroid/annotation/SystemApi;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/hdmi/HdmiRecordSources;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DigitalServiceSource"
.end annotation


# static fields
.field private static final DIGITAL_SERVICE_IDENTIFIED_BY_CHANNEL:I = 0x1

.field private static final DIGITAL_SERVICE_IDENTIFIED_BY_DIGITAL_ID:I = 0x0

.field static final EXTRA_DATA_SIZE:I = 0x7


# instance fields
.field private final mBroadcastSystem:I

.field private final mIdentification:Landroid/hardware/hdmi/HdmiRecordSources$DigitalServiceIdentification;

.field private final mIdentificationMethod:I


# direct methods
.method private constructor <init>(IILandroid/hardware/hdmi/HdmiRecordSources$DigitalServiceIdentification;)V
    .locals 2

    const/4 v0, 0x2

    const/4 v1, 0x7

    invoke-direct {p0, v0, v1}, Landroid/hardware/hdmi/HdmiRecordSources$RecordSource;-><init>(II)V

    iput p1, p0, Landroid/hardware/hdmi/HdmiRecordSources$DigitalServiceSource;->mIdentificationMethod:I

    iput p2, p0, Landroid/hardware/hdmi/HdmiRecordSources$DigitalServiceSource;->mBroadcastSystem:I

    iput-object p3, p0, Landroid/hardware/hdmi/HdmiRecordSources$DigitalServiceSource;->mIdentification:Landroid/hardware/hdmi/HdmiRecordSources$DigitalServiceIdentification;

    return-void
.end method

.method synthetic constructor <init>(IILandroid/hardware/hdmi/HdmiRecordSources$DigitalServiceIdentification;Landroid/hardware/hdmi/HdmiRecordSources$DigitalServiceSource-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/hardware/hdmi/HdmiRecordSources$DigitalServiceSource;-><init>(IILandroid/hardware/hdmi/HdmiRecordSources$DigitalServiceIdentification;)V

    return-void
.end method


# virtual methods
.method extraParamToByteArray([BI)I
    .locals 3

    goto/32 :goto_b

    nop

    :goto_0
    and-int/lit8 v2, v2, 0x7f

    goto/32 :goto_1

    nop

    :goto_1
    or-int/2addr v0, v2

    goto/32 :goto_6

    nop

    :goto_2
    aput-byte v0, p1, p2

    goto/32 :goto_5

    nop

    :goto_3
    return v1

    :goto_4
    shl-int/2addr v0, v1

    goto/32 :goto_a

    nop

    :goto_5
    iget-object v0, p0, Landroid/hardware/hdmi/HdmiRecordSources$DigitalServiceSource;->mIdentification:Landroid/hardware/hdmi/HdmiRecordSources$DigitalServiceIdentification;

    goto/32 :goto_7

    nop

    :goto_6
    int-to-byte v0, v0

    goto/32 :goto_2

    nop

    :goto_7
    add-int/lit8 v2, p2, 0x1

    goto/32 :goto_9

    nop

    :goto_8
    const/4 v1, 0x7

    goto/32 :goto_4

    nop

    :goto_9
    invoke-interface {v0, p1, v2}, Landroid/hardware/hdmi/HdmiRecordSources$DigitalServiceIdentification;->toByteArray([BI)I

    goto/32 :goto_3

    nop

    :goto_a
    iget v2, p0, Landroid/hardware/hdmi/HdmiRecordSources$DigitalServiceSource;->mBroadcastSystem:I

    goto/32 :goto_0

    nop

    :goto_b
    iget v0, p0, Landroid/hardware/hdmi/HdmiRecordSources$DigitalServiceSource;->mIdentificationMethod:I

    goto/32 :goto_8

    nop
.end method
