.class public abstract Landroid/hardware/hdmi/HdmiRecordSources$RecordSource;
.super Ljava/lang/Object;


# annotations
.annotation runtime Landroid/annotation/SystemApi;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/hdmi/HdmiRecordSources;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "RecordSource"
.end annotation


# instance fields
.field final mExtraDataSize:I

.field final mSourceType:I


# direct methods
.method constructor <init>(II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Landroid/hardware/hdmi/HdmiRecordSources$RecordSource;->mSourceType:I

    iput p2, p0, Landroid/hardware/hdmi/HdmiRecordSources$RecordSource;->mExtraDataSize:I

    return-void
.end method


# virtual methods
.method abstract extraParamToByteArray([BI)I
.end method

.method final getDataSize(Z)I
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    return v0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    :goto_2
    goto/32 :goto_0

    nop

    :goto_3
    if-nez p1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop

    :goto_4
    iget v0, p0, Landroid/hardware/hdmi/HdmiRecordSources$RecordSource;->mExtraDataSize:I

    goto/32 :goto_3

    nop
.end method

.method final toByteArray(Z[BI)I
    .locals 2

    goto/32 :goto_7

    nop

    :goto_0
    return v0

    :goto_1
    iget v1, p0, Landroid/hardware/hdmi/HdmiRecordSources$RecordSource;->mSourceType:I

    goto/32 :goto_2

    nop

    :goto_2
    int-to-byte v1, v1

    goto/32 :goto_8

    nop

    :goto_3
    add-int/lit8 v0, p3, 0x1

    goto/32 :goto_1

    nop

    :goto_4
    invoke-virtual {p0, p2, p3}, Landroid/hardware/hdmi/HdmiRecordSources$RecordSource;->extraParamToByteArray([BI)I

    goto/32 :goto_9

    nop

    :goto_5
    move p3, v0

    :goto_6
    goto/32 :goto_4

    nop

    :goto_7
    if-nez p1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_3

    nop

    :goto_8
    aput-byte v1, p2, p3

    goto/32 :goto_5

    nop

    :goto_9
    invoke-virtual {p0, p1}, Landroid/hardware/hdmi/HdmiRecordSources$RecordSource;->getDataSize(Z)I

    move-result v0

    goto/32 :goto_0

    nop
.end method
