.class public final Landroid/hardware/hdmi/HdmiRecordSources$AnalogueServiceSource;
.super Landroid/hardware/hdmi/HdmiRecordSources$RecordSource;


# annotations
.annotation runtime Landroid/annotation/SystemApi;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/hdmi/HdmiRecordSources;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AnalogueServiceSource"
.end annotation


# static fields
.field static final EXTRA_DATA_SIZE:I = 0x4


# instance fields
.field private final mBroadcastSystem:I

.field private final mBroadcastType:I

.field private final mFrequency:I


# direct methods
.method private constructor <init>(III)V
    .locals 2

    const/4 v0, 0x3

    const/4 v1, 0x4

    invoke-direct {p0, v0, v1}, Landroid/hardware/hdmi/HdmiRecordSources$RecordSource;-><init>(II)V

    iput p1, p0, Landroid/hardware/hdmi/HdmiRecordSources$AnalogueServiceSource;->mBroadcastType:I

    iput p2, p0, Landroid/hardware/hdmi/HdmiRecordSources$AnalogueServiceSource;->mFrequency:I

    iput p3, p0, Landroid/hardware/hdmi/HdmiRecordSources$AnalogueServiceSource;->mBroadcastSystem:I

    return-void
.end method

.method synthetic constructor <init>(IIILandroid/hardware/hdmi/HdmiRecordSources$AnalogueServiceSource-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/hardware/hdmi/HdmiRecordSources$AnalogueServiceSource;-><init>(III)V

    return-void
.end method


# virtual methods
.method extraParamToByteArray([BI)I
    .locals 2

    goto/32 :goto_8

    nop

    :goto_0
    return v0

    :goto_1
    aput-byte v1, p1, v0

    goto/32 :goto_5

    nop

    :goto_2
    aput-byte v0, p1, p2

    goto/32 :goto_4

    nop

    :goto_3
    add-int/lit8 v1, p2, 0x1

    goto/32 :goto_a

    nop

    :goto_4
    iget v0, p0, Landroid/hardware/hdmi/HdmiRecordSources$AnalogueServiceSource;->mFrequency:I

    goto/32 :goto_9

    nop

    :goto_5
    const/4 v0, 0x4

    goto/32 :goto_0

    nop

    :goto_6
    add-int/lit8 v0, p2, 0x3

    goto/32 :goto_c

    nop

    :goto_7
    int-to-byte v1, v1

    goto/32 :goto_1

    nop

    :goto_8
    iget v0, p0, Landroid/hardware/hdmi/HdmiRecordSources$AnalogueServiceSource;->mBroadcastType:I

    goto/32 :goto_b

    nop

    :goto_9
    int-to-short v0, v0

    goto/32 :goto_3

    nop

    :goto_a
    invoke-static {v0, p1, v1}, Landroid/hardware/hdmi/HdmiRecordSources;->-$$Nest$smshortToByteArray(S[BI)I

    goto/32 :goto_6

    nop

    :goto_b
    int-to-byte v0, v0

    goto/32 :goto_2

    nop

    :goto_c
    iget v1, p0, Landroid/hardware/hdmi/HdmiRecordSources$AnalogueServiceSource;->mBroadcastSystem:I

    goto/32 :goto_7

    nop
.end method
