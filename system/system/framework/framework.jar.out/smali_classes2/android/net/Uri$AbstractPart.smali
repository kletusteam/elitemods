.class abstract Landroid/net/Uri$AbstractPart;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/Uri;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "AbstractPart"
.end annotation


# static fields
.field static final REPRESENTATION_DECODED:I = 0x2

.field static final REPRESENTATION_ENCODED:I = 0x1


# instance fields
.field volatile decoded:Ljava/lang/String;

.field volatile encoded:Ljava/lang/String;

.field private final mCanonicalRepresentation:I


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Landroid/net/Uri$NotCachedHolder;->NOT_CACHED:Ljava/lang/String;

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    iput v0, p0, Landroid/net/Uri$AbstractPart;->mCanonicalRepresentation:I

    iput-object p1, p0, Landroid/net/Uri$AbstractPart;->encoded:Ljava/lang/String;

    sget-object v0, Landroid/net/Uri$NotCachedHolder;->NOT_CACHED:Ljava/lang/String;

    iput-object v0, p0, Landroid/net/Uri$AbstractPart;->decoded:Ljava/lang/String;

    goto :goto_0

    :cond_0
    sget-object v0, Landroid/net/Uri$NotCachedHolder;->NOT_CACHED:Ljava/lang/String;

    if-eq p2, v0, :cond_1

    const/4 v0, 0x2

    iput v0, p0, Landroid/net/Uri$AbstractPart;->mCanonicalRepresentation:I

    sget-object v0, Landroid/net/Uri$NotCachedHolder;->NOT_CACHED:Ljava/lang/String;

    iput-object v0, p0, Landroid/net/Uri$AbstractPart;->encoded:Ljava/lang/String;

    iput-object p2, p0, Landroid/net/Uri$AbstractPart;->decoded:Ljava/lang/String;

    :goto_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Neither encoded nor decoded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method final getDecoded()Ljava/lang/String;
    .locals 2

    goto/32 :goto_e

    nop

    :goto_0
    invoke-static {v1}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_1

    nop

    :goto_1
    iput-object v1, p0, Landroid/net/Uri$AbstractPart;->decoded:Ljava/lang/String;

    :goto_2
    goto/32 :goto_10

    nop

    :goto_3
    goto :goto_2

    :goto_4
    goto/32 :goto_8

    nop

    :goto_5
    sget-object v1, Landroid/net/Uri$NotCachedHolder;->NOT_CACHED:Ljava/lang/String;

    goto/32 :goto_b

    nop

    :goto_6
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_f

    nop

    :goto_7
    const/4 v0, 0x1

    goto/32 :goto_9

    nop

    :goto_8
    iget-object v1, p0, Landroid/net/Uri$AbstractPart;->encoded:Ljava/lang/String;

    goto/32 :goto_0

    nop

    :goto_9
    goto :goto_d

    :goto_a
    goto/32 :goto_c

    nop

    :goto_b
    if-ne v0, v1, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_7

    nop

    :goto_c
    const/4 v0, 0x0

    :goto_d
    goto/32 :goto_6

    nop

    :goto_e
    iget-object v0, p0, Landroid/net/Uri$AbstractPart;->decoded:Ljava/lang/String;

    goto/32 :goto_5

    nop

    :goto_f
    iget-object v1, p0, Landroid/net/Uri$AbstractPart;->decoded:Ljava/lang/String;

    goto/32 :goto_3

    nop

    :goto_10
    return-object v1
.end method

.method abstract getEncoded()Ljava/lang/String;
.end method

.method final writeTo(Landroid/os/Parcel;)V
    .locals 4

    goto/32 :goto_f

    nop

    :goto_0
    if-ne v0, v1, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_15

    nop

    :goto_1
    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    goto/32 :goto_a

    nop

    :goto_2
    iget-object v0, p0, Landroid/net/Uri$AbstractPart;->decoded:Ljava/lang/String;

    :goto_3
    goto/32 :goto_23

    nop

    :goto_4
    const/4 v1, 0x1

    goto/32 :goto_c

    nop

    :goto_5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_16

    nop

    :goto_6
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_18

    nop

    :goto_7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_10

    nop

    :goto_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_1a

    nop

    :goto_9
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_1b

    nop

    :goto_a
    throw v1

    :goto_b
    goto/32 :goto_8

    nop

    :goto_c
    if-eq v0, v1, :cond_1

    goto/32 :goto_20

    :cond_1
    goto/32 :goto_19

    nop

    :goto_d
    const-string v2, "Unknown representation: "

    goto/32 :goto_5

    nop

    :goto_e
    iget v3, p0, Landroid/net/Uri$AbstractPart;->mCanonicalRepresentation:I

    goto/32 :goto_22

    nop

    :goto_f
    iget v0, p0, Landroid/net/Uri$AbstractPart;->mCanonicalRepresentation:I

    goto/32 :goto_4

    nop

    :goto_10
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_14

    nop

    :goto_11
    return-void

    :goto_12
    goto/32 :goto_13

    nop

    :goto_13
    new-instance v1, Ljava/lang/AssertionError;

    goto/32 :goto_9

    nop

    :goto_14
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_1d

    nop

    :goto_15
    iget v1, p0, Landroid/net/Uri$AbstractPart;->mCanonicalRepresentation:I

    goto/32 :goto_6

    nop

    :goto_16
    iget v2, p0, Landroid/net/Uri$AbstractPart;->mCanonicalRepresentation:I

    goto/32 :goto_7

    nop

    :goto_17
    const-string v3, ")"

    goto/32 :goto_1c

    nop

    :goto_18
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString8(Ljava/lang/String;)V

    goto/32 :goto_11

    nop

    :goto_19
    iget-object v0, p0, Landroid/net/Uri$AbstractPart;->encoded:Ljava/lang/String;

    goto/32 :goto_1f

    nop

    :goto_1a
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_1e

    nop

    :goto_1b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_26

    nop

    :goto_1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_24

    nop

    :goto_1d
    throw v0

    :goto_1e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_d

    nop

    :goto_1f
    goto/16 :goto_3

    :goto_20
    goto/32 :goto_21

    nop

    :goto_21
    const/4 v1, 0x2

    goto/32 :goto_27

    nop

    :goto_22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_17

    nop

    :goto_23
    sget-object v1, Landroid/net/Uri$NotCachedHolder;->NOT_CACHED:Ljava/lang/String;

    goto/32 :goto_0

    nop

    :goto_24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_1

    nop

    :goto_25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_e

    nop

    :goto_26
    const-string v3, "Canonical value not cached ("

    goto/32 :goto_25

    nop

    :goto_27
    if-eq v0, v1, :cond_2

    goto/32 :goto_b

    :cond_2
    goto/32 :goto_2

    nop
.end method
