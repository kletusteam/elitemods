.class public abstract Landroid/net/NetworkSpecifier;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public canBeSatisfiedBy(Landroid/net/NetworkSpecifier;)Z
    .locals 1
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public redact()Landroid/net/NetworkSpecifier;
    .locals 0
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    return-object p0
.end method
