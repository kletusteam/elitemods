.class public abstract Landroid/net/wifi/WifiNetworkScoreCache$CacheListener;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/wifi/WifiNetworkScoreCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "CacheListener"
.end annotation


# instance fields
.field private mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Landroid/net/wifi/WifiNetworkScoreCache$CacheListener;->mHandler:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public abstract networkCacheUpdated(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/net/ScoredNetwork;",
            ">;)V"
        }
    .end annotation
.end method

.method post(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/net/ScoredNetwork;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/net/wifi/WifiNetworkScoreCache$CacheListener;->mHandler:Landroid/os/Handler;

    goto/32 :goto_1

    nop

    :goto_1
    new-instance v1, Landroid/net/wifi/WifiNetworkScoreCache$CacheListener$1;

    goto/32 :goto_4

    nop

    :goto_2
    return-void

    :goto_3
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_2

    nop

    :goto_4
    invoke-direct {v1, p0, p1}, Landroid/net/wifi/WifiNetworkScoreCache$CacheListener$1;-><init>(Landroid/net/wifi/WifiNetworkScoreCache$CacheListener;Ljava/util/List;)V

    goto/32 :goto_3

    nop
.end method
