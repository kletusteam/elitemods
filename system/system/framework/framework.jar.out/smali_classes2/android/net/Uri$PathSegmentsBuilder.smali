.class Landroid/net/Uri$PathSegmentsBuilder;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/Uri;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PathSegmentsBuilder"
.end annotation


# instance fields
.field segments:[Ljava/lang/String;

.field size:I


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Landroid/net/Uri$PathSegmentsBuilder;->size:I

    return-void
.end method


# virtual methods
.method add(Ljava/lang/String;)V
    .locals 4

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/net/Uri$PathSegmentsBuilder;->segments:[Ljava/lang/String;

    goto/32 :goto_18

    nop

    :goto_1
    iput v2, p0, Landroid/net/Uri$PathSegmentsBuilder;->size:I

    goto/32 :goto_f

    nop

    :goto_2
    iget v1, p0, Landroid/net/Uri$PathSegmentsBuilder;->size:I

    goto/32 :goto_c

    nop

    :goto_3
    iput-object v0, p0, Landroid/net/Uri$PathSegmentsBuilder;->segments:[Ljava/lang/String;

    goto/32 :goto_6

    nop

    :goto_4
    if-eq v1, v2, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_11

    nop

    :goto_5
    const/4 v0, 0x4

    goto/32 :goto_17

    nop

    :goto_6
    goto :goto_9

    :goto_7
    goto/32 :goto_15

    nop

    :goto_8
    iput-object v1, p0, Landroid/net/Uri$PathSegmentsBuilder;->segments:[Ljava/lang/String;

    :goto_9
    goto/32 :goto_b

    nop

    :goto_a
    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto/32 :goto_8

    nop

    :goto_b
    iget-object v0, p0, Landroid/net/Uri$PathSegmentsBuilder;->segments:[Ljava/lang/String;

    goto/32 :goto_2

    nop

    :goto_c
    add-int/lit8 v2, v1, 0x1

    goto/32 :goto_1

    nop

    :goto_d
    mul-int/lit8 v1, v1, 0x2

    goto/32 :goto_12

    nop

    :goto_e
    array-length v2, v0

    goto/32 :goto_10

    nop

    :goto_f
    aput-object p1, v0, v1

    goto/32 :goto_13

    nop

    :goto_10
    const/4 v3, 0x0

    goto/32 :goto_a

    nop

    :goto_11
    array-length v1, v0

    goto/32 :goto_d

    nop

    :goto_12
    new-array v1, v1, [Ljava/lang/String;

    goto/32 :goto_e

    nop

    :goto_13
    return-void

    :goto_14
    array-length v2, v0

    goto/32 :goto_4

    nop

    :goto_15
    iget v1, p0, Landroid/net/Uri$PathSegmentsBuilder;->size:I

    goto/32 :goto_16

    nop

    :goto_16
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_14

    nop

    :goto_17
    new-array v0, v0, [Ljava/lang/String;

    goto/32 :goto_3

    nop

    :goto_18
    if-eqz v0, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_5

    nop
.end method

.method build()Landroid/net/Uri$PathSegments;
    .locals 4

    goto/32 :goto_3

    nop

    :goto_0
    iput-object v0, p0, Landroid/net/Uri$PathSegmentsBuilder;->segments:[Ljava/lang/String;

    goto/32 :goto_9

    nop

    :goto_1
    iput-object v0, p0, Landroid/net/Uri$PathSegmentsBuilder;->segments:[Ljava/lang/String;

    goto/32 :goto_5

    nop

    :goto_2
    if-eqz v0, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_4

    nop

    :goto_3
    iget-object v0, p0, Landroid/net/Uri$PathSegmentsBuilder;->segments:[Ljava/lang/String;

    goto/32 :goto_2

    nop

    :goto_4
    sget-object v0, Landroid/net/Uri$PathSegments;->EMPTY:Landroid/net/Uri$PathSegments;

    goto/32 :goto_7

    nop

    :goto_5
    throw v1

    :goto_6
    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Landroid/net/Uri$PathSegments;

    iget-object v2, p0, Landroid/net/Uri$PathSegmentsBuilder;->segments:[Ljava/lang/String;

    iget v3, p0, Landroid/net/Uri$PathSegmentsBuilder;->size:I

    invoke-direct {v1, v2, v3}, Landroid/net/Uri$PathSegments;-><init>([Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_0

    nop

    :goto_7
    return-object v0

    :goto_8
    goto/32 :goto_6

    nop

    :goto_9
    return-object v1

    :catchall_0
    move-exception v1

    goto/32 :goto_1

    nop
.end method
