.class Landroid/net/metrics/NetworkMetrics$Metrics;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/metrics/NetworkMetrics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Metrics"
.end annotation


# instance fields
.field public count:I

.field public max:D

.field public sum:D


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x1

    iput-wide v0, p0, Landroid/net/metrics/NetworkMetrics$Metrics;->max:D

    return-void
.end method


# virtual methods
.method average()D
    .locals 4

    goto/32 :goto_0

    nop

    :goto_0
    iget-wide v0, p0, Landroid/net/metrics/NetworkMetrics$Metrics;->sum:D

    goto/32 :goto_6

    nop

    :goto_1
    if-nez v2, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_2

    nop

    :goto_2
    const-wide/16 v0, 0x0

    :goto_3
    goto/32 :goto_7

    nop

    :goto_4
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    goto/32 :goto_1

    nop

    :goto_5
    div-double/2addr v0, v2

    goto/32 :goto_4

    nop

    :goto_6
    iget v2, p0, Landroid/net/metrics/NetworkMetrics$Metrics;->count:I

    goto/32 :goto_8

    nop

    :goto_7
    return-wide v0

    :goto_8
    int-to-double v2, v2

    goto/32 :goto_5

    nop
.end method

.method count(D)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p0, p1, p2, v0}, Landroid/net/metrics/NetworkMetrics$Metrics;->count(DI)V

    goto/32 :goto_2

    nop

    :goto_1
    const/4 v0, 0x1

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method count(DI)V
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    add-int/2addr v0, p3

    goto/32 :goto_9

    nop

    :goto_1
    iget-wide v0, p0, Landroid/net/metrics/NetworkMetrics$Metrics;->sum:D

    goto/32 :goto_2

    nop

    :goto_2
    add-double/2addr v0, p1

    goto/32 :goto_5

    nop

    :goto_3
    iget v0, p0, Landroid/net/metrics/NetworkMetrics$Metrics;->count:I

    goto/32 :goto_0

    nop

    :goto_4
    return-void

    :goto_5
    iput-wide v0, p0, Landroid/net/metrics/NetworkMetrics$Metrics;->sum:D

    goto/32 :goto_8

    nop

    :goto_6
    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    goto/32 :goto_7

    nop

    :goto_7
    iput-wide v0, p0, Landroid/net/metrics/NetworkMetrics$Metrics;->max:D

    goto/32 :goto_4

    nop

    :goto_8
    iget-wide v0, p0, Landroid/net/metrics/NetworkMetrics$Metrics;->max:D

    goto/32 :goto_6

    nop

    :goto_9
    iput v0, p0, Landroid/net/metrics/NetworkMetrics$Metrics;->count:I

    goto/32 :goto_1

    nop
.end method

.method merge(Landroid/net/metrics/NetworkMetrics$Metrics;)V
    .locals 4

    goto/32 :goto_6

    nop

    :goto_0
    iget-wide v0, p0, Landroid/net/metrics/NetworkMetrics$Metrics;->sum:D

    goto/32 :goto_5

    nop

    :goto_1
    iput-wide v0, p0, Landroid/net/metrics/NetworkMetrics$Metrics;->sum:D

    goto/32 :goto_2

    nop

    :goto_2
    iget-wide v0, p0, Landroid/net/metrics/NetworkMetrics$Metrics;->max:D

    goto/32 :goto_b

    nop

    :goto_3
    iput v0, p0, Landroid/net/metrics/NetworkMetrics$Metrics;->count:I

    goto/32 :goto_0

    nop

    :goto_4
    iput-wide v0, p0, Landroid/net/metrics/NetworkMetrics$Metrics;->max:D

    goto/32 :goto_c

    nop

    :goto_5
    iget-wide v2, p1, Landroid/net/metrics/NetworkMetrics$Metrics;->sum:D

    goto/32 :goto_9

    nop

    :goto_6
    iget v0, p0, Landroid/net/metrics/NetworkMetrics$Metrics;->count:I

    goto/32 :goto_a

    nop

    :goto_7
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    goto/32 :goto_4

    nop

    :goto_8
    add-int/2addr v0, v1

    goto/32 :goto_3

    nop

    :goto_9
    add-double/2addr v0, v2

    goto/32 :goto_1

    nop

    :goto_a
    iget v1, p1, Landroid/net/metrics/NetworkMetrics$Metrics;->count:I

    goto/32 :goto_8

    nop

    :goto_b
    iget-wide v2, p1, Landroid/net/metrics/NetworkMetrics$Metrics;->max:D

    goto/32 :goto_7

    nop

    :goto_c
    return-void
.end method
