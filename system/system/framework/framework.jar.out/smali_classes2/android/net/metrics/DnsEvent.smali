.class public final Landroid/net/metrics/DnsEvent;
.super Ljava/lang/Object;


# static fields
.field private static final SIZE_LIMIT:I = 0x4e20


# instance fields
.field public eventCount:I

.field public eventTypes:[B

.field public latenciesMs:[I

.field public latencySum:J

.field public final netId:I

.field public returnCodes:[B

.field public successCount:I

.field public final transports:J


# direct methods
.method public constructor <init>(IJI)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Landroid/net/metrics/DnsEvent;->latencySum:J

    iput p1, p0, Landroid/net/metrics/DnsEvent;->netId:I

    iput-wide p2, p0, Landroid/net/metrics/DnsEvent;->transports:J

    new-array v0, p4, [B

    iput-object v0, p0, Landroid/net/metrics/DnsEvent;->eventTypes:[B

    new-array v0, p4, [B

    iput-object v0, p0, Landroid/net/metrics/DnsEvent;->returnCodes:[B

    new-array v0, p4, [I

    iput-object v0, p0, Landroid/net/metrics/DnsEvent;->latenciesMs:[I

    return-void
.end method


# virtual methods
.method addResult(BBI)Z
    .locals 7

    goto/32 :goto_19

    nop

    :goto_0
    return v1

    :goto_1
    goto/32 :goto_8

    nop

    :goto_2
    if-eq v2, v3, :cond_0

    goto/32 :goto_f

    :cond_0
    goto/32 :goto_12

    nop

    :goto_3
    if-ge v2, v3, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_0

    nop

    :goto_4
    aput p3, v2, v3

    goto/32 :goto_18

    nop

    :goto_5
    if-nez v1, :cond_2

    goto/32 :goto_c

    :cond_2
    goto/32 :goto_2d

    nop

    :goto_6
    int-to-double v5, v2

    goto/32 :goto_7

    nop

    :goto_7
    mul-double/2addr v5, v3

    goto/32 :goto_25

    nop

    :goto_8
    iget-object v3, p0, Landroid/net/metrics/DnsEvent;->eventTypes:[B

    goto/32 :goto_21

    nop

    :goto_9
    iget v2, p0, Landroid/net/metrics/DnsEvent;->eventCount:I

    goto/32 :goto_d

    nop

    :goto_a
    const/4 v2, 0x3

    goto/32 :goto_10

    nop

    :goto_b
    iput v2, p0, Landroid/net/metrics/DnsEvent;->successCount:I

    :goto_c
    goto/32 :goto_27

    nop

    :goto_d
    const/16 v3, 0x4e20

    goto/32 :goto_3

    nop

    :goto_e
    invoke-virtual {p0, v2}, Landroid/net/metrics/DnsEvent;->resize(I)V

    :goto_f
    goto/32 :goto_20

    nop

    :goto_10
    invoke-virtual {p0, v2}, Landroid/net/metrics/DnsEvent;->resize(I)V

    goto/32 :goto_1b

    nop

    :goto_11
    int-to-long v4, p3

    goto/32 :goto_2c

    nop

    :goto_12
    const/4 v3, 0x2

    goto/32 :goto_14

    nop

    :goto_13
    aput-byte p2, v2, v3

    goto/32 :goto_1d

    nop

    :goto_14
    if-le v2, v3, :cond_3

    goto/32 :goto_1c

    :cond_3
    goto/32 :goto_a

    nop

    :goto_15
    goto :goto_2a

    :goto_16
    goto/32 :goto_29

    nop

    :goto_17
    if-eqz p2, :cond_4

    goto/32 :goto_16

    :cond_4
    goto/32 :goto_24

    nop

    :goto_18
    add-int/2addr v3, v0

    goto/32 :goto_1a

    nop

    :goto_19
    const/4 v0, 0x1

    goto/32 :goto_17

    nop

    :goto_1a
    iput v3, p0, Landroid/net/metrics/DnsEvent;->eventCount:I

    goto/32 :goto_26

    nop

    :goto_1b
    goto :goto_f

    :goto_1c
    goto/32 :goto_28

    nop

    :goto_1d
    iget-object v2, p0, Landroid/net/metrics/DnsEvent;->latenciesMs:[I

    goto/32 :goto_4

    nop

    :goto_1e
    iget-object v2, p0, Landroid/net/metrics/DnsEvent;->returnCodes:[B

    goto/32 :goto_13

    nop

    :goto_1f
    aput-byte p1, v2, v3

    goto/32 :goto_1e

    nop

    :goto_20
    iget-object v2, p0, Landroid/net/metrics/DnsEvent;->eventTypes:[B

    goto/32 :goto_23

    nop

    :goto_21
    array-length v3, v3

    goto/32 :goto_2

    nop

    :goto_22
    add-int/2addr v2, v0

    goto/32 :goto_b

    nop

    :goto_23
    iget v3, p0, Landroid/net/metrics/DnsEvent;->eventCount:I

    goto/32 :goto_1f

    nop

    :goto_24
    move v1, v0

    goto/32 :goto_15

    nop

    :goto_25
    double-to-int v2, v5

    goto/32 :goto_e

    nop

    :goto_26
    iget-wide v2, p0, Landroid/net/metrics/DnsEvent;->latencySum:J

    goto/32 :goto_11

    nop

    :goto_27
    return v1

    :goto_28
    const-wide v3, 0x3ff6666666666666L    # 1.4

    goto/32 :goto_6

    nop

    :goto_29
    const/4 v1, 0x0

    :goto_2a
    goto/32 :goto_9

    nop

    :goto_2b
    iput-wide v2, p0, Landroid/net/metrics/DnsEvent;->latencySum:J

    goto/32 :goto_5

    nop

    :goto_2c
    add-long/2addr v2, v4

    goto/32 :goto_2b

    nop

    :goto_2d
    iget v2, p0, Landroid/net/metrics/DnsEvent;->successCount:I

    goto/32 :goto_22

    nop
.end method

.method public resize(I)V
    .locals 1

    iget-object v0, p0, Landroid/net/metrics/DnsEvent;->eventTypes:[B

    invoke-static {v0, p1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    iput-object v0, p0, Landroid/net/metrics/DnsEvent;->eventTypes:[B

    iget-object v0, p0, Landroid/net/metrics/DnsEvent;->returnCodes:[B

    invoke-static {v0, p1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    iput-object v0, p0, Landroid/net/metrics/DnsEvent;->returnCodes:[B

    iget-object v0, p0, Landroid/net/metrics/DnsEvent;->latenciesMs:[I

    invoke-static {v0, p1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, Landroid/net/metrics/DnsEvent;->latenciesMs:[I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DnsEvent("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "netId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/net/metrics/DnsEvent;->netId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", transports="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x1

    new-array v2, v1, [J

    iget-wide v3, p0, Landroid/net/metrics/DnsEvent;->transports:J

    const/4 v5, 0x0

    aput-wide v3, v2, v5

    invoke-static {v2}, Ljava/util/BitSet;->valueOf([J)Ljava/util/BitSet;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-array v2, v1, [Ljava/lang/Object;

    iget v3, p0, Landroid/net/metrics/DnsEvent;->eventCount:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const-string v3, "%d events, "

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-array v2, v1, [Ljava/lang/Object;

    iget v3, p0, Landroid/net/metrics/DnsEvent;->successCount:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const-string v3, "%d success, "

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-array v1, v1, [Ljava/lang/Object;

    iget-wide v2, p0, Landroid/net/metrics/DnsEvent;->latencySum:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v5

    const-string v2, "%d ms)"

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
