.class public Landroid/net/PacProxyManager$PacProxyInstalledListenerProxy;
.super Landroid/net/IPacProxyInstalledListener$Stub;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/PacProxyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PacProxyInstalledListenerProxy"
.end annotation


# instance fields
.field private final mExecutor:Ljava/util/concurrent/Executor;

.field private final mListener:Landroid/net/PacProxyManager$PacProxyInstalledListener;

.field final synthetic this$0:Landroid/net/PacProxyManager;


# direct methods
.method constructor <init>(Landroid/net/PacProxyManager;Ljava/util/concurrent/Executor;Landroid/net/PacProxyManager$PacProxyInstalledListener;)V
    .locals 0

    iput-object p1, p0, Landroid/net/PacProxyManager$PacProxyInstalledListenerProxy;->this$0:Landroid/net/PacProxyManager;

    invoke-direct {p0}, Landroid/net/IPacProxyInstalledListener$Stub;-><init>()V

    iput-object p2, p0, Landroid/net/PacProxyManager$PacProxyInstalledListenerProxy;->mExecutor:Ljava/util/concurrent/Executor;

    iput-object p3, p0, Landroid/net/PacProxyManager$PacProxyInstalledListenerProxy;->mListener:Landroid/net/PacProxyManager$PacProxyInstalledListener;

    return-void
.end method


# virtual methods
.method synthetic lambda$onPacProxyInstalled$0$android-net-PacProxyManager$PacProxyInstalledListenerProxy(Landroid/net/Network;Landroid/net/ProxyInfo;)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v0, p0, Landroid/net/PacProxyManager$PacProxyInstalledListenerProxy;->mListener:Landroid/net/PacProxyManager$PacProxyInstalledListener;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-interface {v0, p1, p2}, Landroid/net/PacProxyManager$PacProxyInstalledListener;->onPacProxyInstalled(Landroid/net/Network;Landroid/net/ProxyInfo;)V

    goto/32 :goto_0

    nop
.end method

.method synthetic lambda$onPacProxyInstalled$1$android-net-PacProxyManager$PacProxyInstalledListenerProxy(Landroid/net/Network;Landroid/net/ProxyInfo;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    invoke-direct {v1, p0, p1, p2}, Landroid/net/PacProxyManager$PacProxyInstalledListenerProxy$$ExternalSyntheticLambda0;-><init>(Landroid/net/PacProxyManager$PacProxyInstalledListenerProxy;Landroid/net/Network;Landroid/net/ProxyInfo;)V

    goto/32 :goto_1

    nop

    :goto_1
    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto/32 :goto_4

    nop

    :goto_2
    iget-object v0, p0, Landroid/net/PacProxyManager$PacProxyInstalledListenerProxy;->mExecutor:Ljava/util/concurrent/Executor;

    goto/32 :goto_3

    nop

    :goto_3
    new-instance v1, Landroid/net/PacProxyManager$PacProxyInstalledListenerProxy$$ExternalSyntheticLambda0;

    goto/32 :goto_0

    nop

    :goto_4
    return-void
.end method

.method public onPacProxyInstalled(Landroid/net/Network;Landroid/net/ProxyInfo;)V
    .locals 1

    new-instance v0, Landroid/net/PacProxyManager$PacProxyInstalledListenerProxy$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0, p1, p2}, Landroid/net/PacProxyManager$PacProxyInstalledListenerProxy$$ExternalSyntheticLambda1;-><init>(Landroid/net/PacProxyManager$PacProxyInstalledListenerProxy;Landroid/net/Network;Landroid/net/ProxyInfo;)V

    invoke-static {v0}, Landroid/os/Binder;->withCleanCallingIdentity(Lcom/android/internal/util/FunctionalUtils$ThrowingRunnable;)V

    return-void
.end method
