.class public final Landroid/internal/modules/utils/build/UnboundedSdkLevel;
.super Ljava/lang/Object;


# static fields
.field private static final PREVIOUS_CODENAMES:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final sInstance:Landroid/internal/modules/utils/build/UnboundedSdkLevel;


# instance fields
.field private final mCodename:Ljava/lang/String;

.field private final mIsReleaseBuild:Z

.field private final mKnownCodenames:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mSdkInt:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Landroid/util/SparseArray;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    sput-object v0, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->PREVIOUS_CODENAMES:Landroid/util/SparseArray;

    const-string v1, "Q"

    invoke-static {v1}, Ljava/util/Set;->of(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v2

    const/16 v3, 0x1d

    invoke-virtual {v0, v3, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const-string v2, "R"

    invoke-static {v1, v2}, Ljava/util/Set;->of(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v3

    const/16 v4, 0x1e

    invoke-virtual {v0, v4, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const-string v3, "S"

    invoke-static {v1, v2, v3}, Ljava/util/Set;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v4

    const/16 v5, 0x1f

    invoke-virtual {v0, v5, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const-string v4, "Sv2"

    invoke-static {v1, v2, v3, v4}, Ljava/util/Set;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    const/16 v2, 0x20

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v1, Landroid/internal/modules/utils/build/UnboundedSdkLevel;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    sget-object v3, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    invoke-static {}, Landroid/internal/modules/utils/build/SdkLevel;->isAtLeastT()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v0, Landroid/os/Build$VERSION;->KNOWN_CODENAMES:Ljava/util/Set;

    goto :goto_0

    :cond_0
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    :goto_0
    invoke-direct {v1, v2, v3, v0}, Landroid/internal/modules/utils/build/UnboundedSdkLevel;-><init>(ILjava/lang/String;Ljava/util/Set;)V

    sput-object v1, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->sInstance:Landroid/internal/modules/utils/build/UnboundedSdkLevel;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->mSdkInt:I

    iput-object p2, p0, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->mCodename:Ljava/lang/String;

    const-string v0, "REL"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->mIsReleaseBuild:Z

    iput-object p3, p0, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->mKnownCodenames:Ljava/util/Set;

    return-void
.end method

.method public static isAtLeast(Ljava/lang/String;)Z
    .locals 1

    sget-object v0, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->sInstance:Landroid/internal/modules/utils/build/UnboundedSdkLevel;

    invoke-virtual {v0, p0}, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->isAtLeastInternal(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isAtMost(Ljava/lang/String;)Z
    .locals 1

    sget-object v0, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->sInstance:Landroid/internal/modules/utils/build/UnboundedSdkLevel;

    invoke-virtual {v0, p0}, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->isAtMostInternal(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private isCodename(Ljava/lang/String;)Z
    .locals 1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isUpperCase(C)Z

    move-result v0

    return v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method


# virtual methods
.method isAtLeastInternal(Ljava/lang/String;)Z
    .locals 4

    goto/32 :goto_22

    nop

    :goto_0
    return v1

    :goto_1
    if-ge v0, v3, :cond_0

    goto/32 :goto_f

    :cond_0
    goto/32 :goto_e

    nop

    :goto_2
    iget-boolean v0, p0, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->mIsReleaseBuild:Z

    goto/32 :goto_1d

    nop

    :goto_3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_c

    nop

    :goto_4
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    goto/32 :goto_1

    nop

    :goto_5
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_3

    nop

    :goto_6
    return v1

    :goto_7
    goto/32 :goto_1b

    nop

    :goto_8
    const-string v2, " must be recompiled with a finalized integer version."

    goto/32 :goto_26

    nop

    :goto_9
    goto/16 :goto_29

    :goto_a
    goto/32 :goto_28

    nop

    :goto_b
    iget v0, p0, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->mSdkInt:I

    goto/32 :goto_d

    nop

    :goto_c
    const-string v2, "Artifact with a known codename "

    goto/32 :goto_1a

    nop

    :goto_d
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    goto/32 :goto_2e

    nop

    :goto_e
    goto :goto_12

    :goto_f
    goto/32 :goto_11

    nop

    :goto_10
    new-instance v0, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_5

    nop

    :goto_11
    move v1, v2

    :goto_12
    goto/32 :goto_6

    nop

    :goto_13
    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_20

    nop

    :goto_14
    if-nez v0, :cond_1

    goto/32 :goto_16

    :cond_1
    goto/32 :goto_1f

    nop

    :goto_15
    return v0

    :goto_16
    goto/32 :goto_b

    nop

    :goto_17
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_18
    const/4 v2, 0x0

    goto/32 :goto_2a

    nop

    :goto_19
    invoke-direct {p0, p1}, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->isCodename(Ljava/lang/String;)Z

    move-result v0

    goto/32 :goto_27

    nop

    :goto_1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_17

    nop

    :goto_1b
    invoke-direct {p0, p1}, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->isCodename(Ljava/lang/String;)Z

    move-result v0

    goto/32 :goto_14

    nop

    :goto_1c
    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_15

    nop

    :goto_1d
    const/4 v1, 0x1

    goto/32 :goto_18

    nop

    :goto_1e
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_23

    nop

    :goto_1f
    iget-object v0, p0, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->mKnownCodenames:Ljava/util/Set;

    goto/32 :goto_1c

    nop

    :goto_20
    if-eqz v0, :cond_2

    goto/32 :goto_2c

    :cond_2
    goto/32 :goto_2b

    nop

    :goto_21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_1e

    nop

    :goto_22
    invoke-virtual {p0, p1}, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->removeFingerprint(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_2

    nop

    :goto_23
    throw v0

    :goto_24
    goto/32 :goto_2d

    nop

    :goto_25
    iget-object v0, p0, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->mKnownCodenames:Ljava/util/Set;

    goto/32 :goto_13

    nop

    :goto_26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_21

    nop

    :goto_27
    if-nez v0, :cond_3

    goto/32 :goto_24

    :cond_3
    goto/32 :goto_25

    nop

    :goto_28
    move v1, v2

    :goto_29
    goto/32 :goto_0

    nop

    :goto_2a
    if-nez v0, :cond_4

    goto/32 :goto_7

    :cond_4
    goto/32 :goto_19

    nop

    :goto_2b
    return v2

    :goto_2c
    goto/32 :goto_10

    nop

    :goto_2d
    iget v0, p0, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->mSdkInt:I

    goto/32 :goto_4

    nop

    :goto_2e
    if-ge v0, v3, :cond_5

    goto/32 :goto_a

    :cond_5
    goto/32 :goto_9

    nop
.end method

.method isAtMostInternal(Ljava/lang/String;)Z
    .locals 4

    goto/32 :goto_2f

    nop

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_21

    nop

    :goto_1
    const/4 v1, 0x0

    goto/32 :goto_1c

    nop

    :goto_2
    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_24

    nop

    :goto_3
    return v1

    :goto_4
    if-nez v0, :cond_0

    goto/32 :goto_1b

    :cond_0
    goto/32 :goto_25

    nop

    :goto_5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_1d

    nop

    :goto_6
    return v1

    :goto_7
    goto/32 :goto_2c

    nop

    :goto_8
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_b

    nop

    :goto_9
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_31

    nop

    :goto_a
    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_10

    nop

    :goto_b
    throw v0

    :goto_c
    goto/32 :goto_f

    nop

    :goto_d
    if-lt v0, v3, :cond_1

    goto/32 :goto_13

    :cond_1
    goto/32 :goto_12

    nop

    :goto_e
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    goto/32 :goto_d

    nop

    :goto_f
    iget v0, p0, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->mSdkInt:I

    goto/32 :goto_11

    nop

    :goto_10
    if-nez v0, :cond_2

    goto/32 :goto_2b

    :cond_2
    goto/32 :goto_18

    nop

    :goto_11
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    goto/32 :goto_1f

    nop

    :goto_12
    move v1, v2

    :goto_13
    goto/32 :goto_3

    nop

    :goto_14
    iget-object v0, p0, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->mKnownCodenames:Ljava/util/Set;

    goto/32 :goto_a

    nop

    :goto_15
    return v2

    :goto_16
    goto/32 :goto_27

    nop

    :goto_17
    const-string v2, "Artifact with a known codename "

    goto/32 :goto_0

    nop

    :goto_18
    iget-object v0, p0, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->mCodename:Ljava/lang/String;

    goto/32 :goto_28

    nop

    :goto_19
    invoke-direct {p0, p1}, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->isCodename(Ljava/lang/String;)Z

    move-result v0

    goto/32 :goto_20

    nop

    :goto_1a
    return v1

    :goto_1b
    goto/32 :goto_19

    nop

    :goto_1c
    const/4 v2, 0x1

    goto/32 :goto_4

    nop

    :goto_1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_1e
    if-nez v0, :cond_3

    goto/32 :goto_c

    :cond_3
    goto/32 :goto_26

    nop

    :goto_1f
    if-le v0, v3, :cond_4

    goto/32 :goto_23

    :cond_4
    goto/32 :goto_22

    nop

    :goto_20
    if-nez v0, :cond_5

    goto/32 :goto_7

    :cond_5
    goto/32 :goto_14

    nop

    :goto_21
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_30

    nop

    :goto_22
    move v1, v2

    :goto_23
    goto/32 :goto_1a

    nop

    :goto_24
    if-eqz v0, :cond_6

    goto/32 :goto_16

    :cond_6
    goto/32 :goto_15

    nop

    :goto_25
    invoke-direct {p0, p1}, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->isCodename(Ljava/lang/String;)Z

    move-result v0

    goto/32 :goto_1e

    nop

    :goto_26
    iget-object v0, p0, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->mKnownCodenames:Ljava/util/Set;

    goto/32 :goto_2

    nop

    :goto_27
    new-instance v0, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_9

    nop

    :goto_28
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_2a

    nop

    :goto_29
    iget-boolean v0, p0, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->mIsReleaseBuild:Z

    goto/32 :goto_1

    nop

    :goto_2a
    if-nez v0, :cond_7

    goto/32 :goto_2e

    :cond_7
    :goto_2b
    goto/32 :goto_2d

    nop

    :goto_2c
    iget v0, p0, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->mSdkInt:I

    goto/32 :goto_e

    nop

    :goto_2d
    move v1, v2

    :goto_2e
    goto/32 :goto_6

    nop

    :goto_2f
    invoke-virtual {p0, p1}, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->removeFingerprint(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_29

    nop

    :goto_30
    const-string v2, " must be recompiled with a finalized integer version."

    goto/32 :goto_5

    nop

    :goto_31
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_17

    nop
.end method

.method removeFingerprint(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    goto/32 :goto_7

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_5

    nop

    :goto_1
    return-object v1

    :goto_2
    goto/32 :goto_3

    nop

    :goto_3
    return-object p1

    :goto_4
    const/4 v1, -0x1

    goto/32 :goto_a

    nop

    :goto_5
    const/16 v0, 0x2e

    goto/32 :goto_6

    nop

    :goto_6
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    goto/32 :goto_4

    nop

    :goto_7
    invoke-direct {p0, p1}, Landroid/internal/modules/utils/build/UnboundedSdkLevel;->isCodename(Ljava/lang/String;)Z

    move-result v0

    goto/32 :goto_0

    nop

    :goto_8
    const/4 v1, 0x0

    goto/32 :goto_9

    nop

    :goto_9
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_1

    nop

    :goto_a
    if-ne v0, v1, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_8

    nop
.end method
