.class public final Landroid/internal/wifi/WifiAnnotations;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/internal/wifi/WifiAnnotations$Cipher;,
        Landroid/internal/wifi/WifiAnnotations$KeyMgmt;,
        Landroid/internal/wifi/WifiAnnotations$Protocol;,
        Landroid/internal/wifi/WifiAnnotations$WifiStandard;,
        Landroid/internal/wifi/WifiAnnotations$PreambleType;,
        Landroid/internal/wifi/WifiAnnotations$ChannelWidth;,
        Landroid/internal/wifi/WifiAnnotations$Bandwidth;,
        Landroid/internal/wifi/WifiAnnotations$WifiBandBasic;,
        Landroid/internal/wifi/WifiAnnotations$ScanType;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
