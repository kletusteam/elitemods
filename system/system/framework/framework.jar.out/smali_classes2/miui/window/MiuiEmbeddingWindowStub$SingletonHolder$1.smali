.class Lmiui/window/MiuiEmbeddingWindowStub$SingletonHolder$1;
.super Ljava/lang/Object;

# interfaces
.implements Lmiui/window/MiuiEmbeddingWindowStub;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/window/MiuiEmbeddingWindowStub$SingletonHolder;->getStub()Lmiui/window/MiuiEmbeddingWindowStub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mRemote:Lcom/miui/window/IMiuiEmbeddingWindow;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "miui_embedding_window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/window/IMiuiEmbeddingWindow$Stub;->asInterface(Landroid/os/IBinder;)Lcom/miui/window/IMiuiEmbeddingWindow;

    move-result-object v0

    iput-object v0, p0, Lmiui/window/MiuiEmbeddingWindowStub$SingletonHolder$1;->mRemote:Lcom/miui/window/IMiuiEmbeddingWindow;

    return-void
.end method


# virtual methods
.method public overrideDisplayRotation(Landroid/view/DisplayInfo;)Z
    .locals 3

    iget-object v0, p0, Lmiui/window/MiuiEmbeddingWindowStub$SingletonHolder$1;->mRemote:Lcom/miui/window/IMiuiEmbeddingWindow;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    :try_start_0
    invoke-static {}, Landroid/app/ActivityThread;->currentPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/miui/window/IMiuiEmbeddingWindow;->isUsingCameraWhenEmbedded(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput v1, p1, Landroid/view/DisplayInfo;->rotation:I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    return v0

    :cond_0
    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    :goto_0
    return v1
.end method
