.class public Lmiuix/settings/utils/ItemAnimation;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/preference/CustomUpdater$CustomReceiver;


# static fields
.field private static final sAnimatiom:Lmiuix/settings/utils/ItemAnimation;

.field private static final sKey:Ljava/lang/String; = "listview_animation"


# instance fields
.field private mAnimDuration:I

.field private mAnimEnable:Z

.field private mAnimType:I

.field private mInterpolatorType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmiuix/settings/utils/ItemAnimation;

    invoke-direct {v0}, Lmiuix/settings/utils/ItemAnimation;-><init>()V

    sput-object v0, Lmiuix/settings/utils/ItemAnimation;->sAnimatiom:Lmiuix/settings/utils/ItemAnimation;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Landroid/preference/CustomUpdater;->getInstance()Landroid/preference/CustomUpdater;

    move-result-object v0

    const-string v1, "listview_animation"

    invoke-virtual {v0, p0, v1}, Landroid/preference/CustomUpdater;->addCustomReceiver(Landroid/preference/CustomUpdater$CustomReceiver;Ljava/lang/String;)V

    const-string v0, "listview_animation"

    invoke-virtual {p0, v0}, Lmiuix/settings/utils/ItemAnimation;->onCustomChanged(Ljava/lang/String;)V

    return-void
.end method

.method public static declared-synchronized getInstance()Lmiuix/settings/utils/ItemAnimation;
    .locals 4

    const-class v2, Lmiuix/settings/utils/ItemAnimation;

    monitor-enter v2

    :try_start_0
    const-class v3, Lmiuix/settings/utils/ItemAnimation;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    sget-object v0, Lmiuix/settings/utils/ItemAnimation;->sAnimatiom:Lmiuix/settings/utils/ItemAnimation;

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v2

    return-object v0

    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private getInterpolator(Landroid/content/Context;)Landroid/view/animation/Interpolator;
    .locals 2

    iget v1, p0, Lmiuix/settings/utils/ItemAnimation;->mInterpolatorType:I

    packed-switch v1, :pswitch_data_0

    const v1, 0x10a000b

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    const v1, 0x10a0005

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const v1, 0x10a0006

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const v1, 0x10a0004

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const v1, 0x10a0007

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    const v1, 0x10a0008

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    const v1, 0x10a0009

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    goto :goto_0

    :pswitch_6
    const v1, 0x10a000a

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    goto :goto_0

    :pswitch_7
    const v1, 0x10a000c

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static playAnim(Ljava/lang/Object;)V
    .locals 7

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v0

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v1, v0, v3

    invoke-virtual {v1}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v5

    const-class v6, Landroid/view/View;

    if-ne v5, v6, :cond_1

    invoke-virtual {v1, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    new-instance v3, Lmiuix/settings/utils/ItemAnimation$1;

    invoke-direct {v3, v2}, Lmiuix/settings/utils/ItemAnimation$1;-><init>(Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    return-void

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :catch_0
    move-exception v3

    goto :goto_1
.end method


# virtual methods
.method public goAnim(Landroid/view/View;)V
    .locals 11

    const/4 v5, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v1, 0x3f000000    # 0.5f

    const/4 v4, 0x0

    if-eqz p1, :cond_0

    iget-boolean v3, p0, Lmiuix/settings/utils/ItemAnimation;->mAnimEnable:Z

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v5, :cond_2

    const/16 v10, 0x438

    :goto_1
    const/16 v9, 0xc8

    iget v3, p0, Lmiuix/settings/utils/ItemAnimation;->mAnimType:I

    packed-switch v3, :pswitch_data_0

    new-instance v0, Landroid/view/animation/ScaleAnimation;

    invoke-direct {v0, v1, v2, v1, v2}, Landroid/view/animation/ScaleAnimation;-><init>(FFFF)V

    :goto_2
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Lmiuix/settings/utils/ItemAnimation;->getInterpolator(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    iget v1, p0, Lmiuix/settings/utils/ItemAnimation;->mAnimDuration:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    :cond_2
    const/16 v10, 0x924

    goto :goto_1

    :pswitch_0
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    move v3, v1

    move v4, v2

    move v6, v2

    move v7, v5

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    goto :goto_2

    :pswitch_1
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    move v3, v1

    move v4, v2

    move v6, v1

    move v7, v5

    move v8, v1

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    goto :goto_2

    :pswitch_2
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v4, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    goto :goto_2

    :pswitch_3
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    neg-int v1, v9

    int-to-float v1, v1

    invoke-direct {v0, v4, v4, v1, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    goto :goto_2

    :pswitch_4
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    int-to-float v1, v9

    invoke-direct {v0, v4, v4, v1, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    goto :goto_2

    :pswitch_5
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    neg-int v1, v10

    int-to-float v1, v1

    invoke-direct {v0, v1, v4, v4, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    goto :goto_2

    :pswitch_6
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    int-to-float v1, v10

    invoke-direct {v0, v1, v4, v4, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    goto :goto_2

    :pswitch_7
    new-instance v0, Landroid/view/animation/RotateAnimation;

    const/high16 v3, 0x43340000    # 180.0f

    move-object v2, v0

    move v6, v1

    move v7, v5

    move v8, v1

    invoke-direct/range {v2 .. v8}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    goto :goto_2

    :pswitch_8
    new-instance v0, Landroid/view/animation/RotateAnimation;

    const/high16 v3, 0x43b40000    # 360.0f

    move-object v2, v0

    move v6, v1

    move v7, v5

    move v8, v1

    invoke-direct/range {v2 .. v8}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public onCustomChanged(Ljava/lang/String;)V
    .locals 3

    const/4 v0, 0x1

    const-string v1, "listview_animation"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "listview_animation"

    invoke-static {v1, v0}, Landroid/preference/SettingsEliteHelper;->getIntofSettings(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lmiuix/settings/utils/ItemAnimation;->mAnimType:I

    const-string v1, "listview_interpolator"

    invoke-static {v1, v0}, Landroid/preference/SettingsEliteHelper;->getIntofSettings(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lmiuix/settings/utils/ItemAnimation;->mInterpolatorType:I

    const-string v1, "listview_duration"

    const/16 v2, 0x12c

    invoke-static {v1, v2}, Landroid/preference/SettingsEliteHelper;->getIntofSettings(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lmiuix/settings/utils/ItemAnimation;->mAnimDuration:I

    iget v1, p0, Lmiuix/settings/utils/ItemAnimation;->mAnimType:I

    if-eqz v1, :cond_1

    :goto_0
    iput-boolean v0, p0, Lmiuix/settings/utils/ItemAnimation;->mAnimEnable:Z

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
