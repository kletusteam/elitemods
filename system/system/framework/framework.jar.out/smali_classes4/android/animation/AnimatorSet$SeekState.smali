.class Landroid/animation/AnimatorSet$SeekState;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/animation/AnimatorSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SeekState"
.end annotation


# instance fields
.field private mPlayTime:J

.field private mSeekingInReverse:Z

.field final synthetic this$0:Landroid/animation/AnimatorSet;


# direct methods
.method private constructor <init>(Landroid/animation/AnimatorSet;)V
    .locals 2

    iput-object p1, p0, Landroid/animation/AnimatorSet$SeekState;->this$0:Landroid/animation/AnimatorSet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/animation/AnimatorSet$SeekState;->mPlayTime:J

    const/4 p1, 0x0

    iput-boolean p1, p0, Landroid/animation/AnimatorSet$SeekState;->mSeekingInReverse:Z

    return-void
.end method

.method synthetic constructor <init>(Landroid/animation/AnimatorSet;Landroid/animation/AnimatorSet$SeekState-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/animation/AnimatorSet$SeekState;-><init>(Landroid/animation/AnimatorSet;)V

    return-void
.end method


# virtual methods
.method getPlayTime()J
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget-wide v0, p0, Landroid/animation/AnimatorSet$SeekState;->mPlayTime:J

    goto/32 :goto_1

    nop

    :goto_1
    return-wide v0
.end method

.method getPlayTimeNormalized()J
    .locals 4

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/animation/AnimatorSet$SeekState;->this$0:Landroid/animation/AnimatorSet;

    goto/32 :goto_9

    nop

    :goto_1
    sub-long/2addr v0, v2

    goto/32 :goto_2

    nop

    :goto_2
    return-wide v0

    :goto_3
    goto/32 :goto_a

    nop

    :goto_4
    iget-object v2, p0, Landroid/animation/AnimatorSet$SeekState;->this$0:Landroid/animation/AnimatorSet;

    goto/32 :goto_8

    nop

    :goto_5
    return-wide v0

    :goto_6
    if-nez v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_d

    nop

    :goto_7
    iget-wide v2, p0, Landroid/animation/AnimatorSet$SeekState;->mPlayTime:J

    goto/32 :goto_1

    nop

    :goto_8
    invoke-static {v2}, Landroid/animation/AnimatorSet;->-$$Nest$fgetmStartDelay(Landroid/animation/AnimatorSet;)J

    move-result-wide v2

    goto/32 :goto_b

    nop

    :goto_9
    invoke-static {v0}, Landroid/animation/AnimatorSet;->-$$Nest$fgetmReversing(Landroid/animation/AnimatorSet;)Z

    move-result v0

    goto/32 :goto_6

    nop

    :goto_a
    iget-wide v0, p0, Landroid/animation/AnimatorSet$SeekState;->mPlayTime:J

    goto/32 :goto_5

    nop

    :goto_b
    sub-long/2addr v0, v2

    goto/32 :goto_7

    nop

    :goto_c
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->getTotalDuration()J

    move-result-wide v0

    goto/32 :goto_4

    nop

    :goto_d
    iget-object v0, p0, Landroid/animation/AnimatorSet$SeekState;->this$0:Landroid/animation/AnimatorSet;

    goto/32 :goto_c

    nop
.end method

.method isActive()Z
    .locals 4

    goto/32 :goto_8

    nop

    :goto_0
    const/4 v0, 0x1

    goto/32 :goto_6

    nop

    :goto_1
    cmp-long v0, v0, v2

    goto/32 :goto_2

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_0

    nop

    :goto_3
    return v0

    :goto_4
    const/4 v0, 0x0

    :goto_5
    goto/32 :goto_3

    nop

    :goto_6
    goto :goto_5

    :goto_7
    goto/32 :goto_4

    nop

    :goto_8
    iget-wide v0, p0, Landroid/animation/AnimatorSet$SeekState;->mPlayTime:J

    goto/32 :goto_9

    nop

    :goto_9
    const-wide/16 v2, -0x1

    goto/32 :goto_1

    nop
.end method

.method reset()V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    iput-boolean v0, p0, Landroid/animation/AnimatorSet$SeekState;->mSeekingInReverse:Z

    goto/32 :goto_3

    nop

    :goto_1
    iput-wide v0, p0, Landroid/animation/AnimatorSet$SeekState;->mPlayTime:J

    goto/32 :goto_4

    nop

    :goto_2
    const-wide/16 v0, -0x1

    goto/32 :goto_1

    nop

    :goto_3
    return-void

    :goto_4
    const/4 v0, 0x0

    goto/32 :goto_0

    nop
.end method

.method setPlayTime(JZ)V
    .locals 4

    goto/32 :goto_9

    nop

    :goto_0
    iput-boolean p3, p0, Landroid/animation/AnimatorSet$SeekState;->mSeekingInReverse:Z

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_5

    nop

    :goto_3
    invoke-static {v2}, Landroid/animation/AnimatorSet;->-$$Nest$fgetmStartDelay(Landroid/animation/AnimatorSet;)J

    move-result-wide v2

    goto/32 :goto_b

    nop

    :goto_4
    iget-object v2, p0, Landroid/animation/AnimatorSet$SeekState;->this$0:Landroid/animation/AnimatorSet;

    goto/32 :goto_3

    nop

    :goto_5
    iget-object v0, p0, Landroid/animation/AnimatorSet$SeekState;->this$0:Landroid/animation/AnimatorSet;

    goto/32 :goto_12

    nop

    :goto_6
    iput-wide v0, p0, Landroid/animation/AnimatorSet$SeekState;->mPlayTime:J

    :goto_7
    goto/32 :goto_11

    nop

    :goto_8
    cmp-long v0, v0, v2

    goto/32 :goto_2

    nop

    :goto_9
    iget-object v0, p0, Landroid/animation/AnimatorSet$SeekState;->this$0:Landroid/animation/AnimatorSet;

    goto/32 :goto_e

    nop

    :goto_a
    iget-wide v2, p0, Landroid/animation/AnimatorSet$SeekState;->mPlayTime:J

    goto/32 :goto_f

    nop

    :goto_b
    sub-long/2addr v0, v2

    goto/32 :goto_c

    nop

    :goto_c
    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    goto/32 :goto_6

    nop

    :goto_d
    iput-wide v0, p0, Landroid/animation/AnimatorSet$SeekState;->mPlayTime:J

    goto/32 :goto_0

    nop

    :goto_e
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->getTotalDuration()J

    move-result-wide v0

    goto/32 :goto_10

    nop

    :goto_f
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto/32 :goto_d

    nop

    :goto_10
    const-wide/16 v2, -0x1

    goto/32 :goto_8

    nop

    :goto_11
    const-wide/16 v0, 0x0

    goto/32 :goto_a

    nop

    :goto_12
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->getTotalDuration()J

    move-result-wide v0

    goto/32 :goto_4

    nop
.end method

.method updateSeekDirection(Z)V
    .locals 4

    goto/32 :goto_3

    nop

    :goto_0
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->getTotalDuration()J

    move-result-wide v0

    goto/32 :goto_6

    nop

    :goto_1
    if-ne p1, v0, :cond_0

    goto/32 :goto_14

    :cond_0
    goto/32 :goto_18

    nop

    :goto_2
    return-void

    :goto_3
    if-nez p1, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_16

    nop

    :goto_4
    iget-wide v2, p0, Landroid/animation/AnimatorSet$SeekState;->mPlayTime:J

    goto/32 :goto_1c

    nop

    :goto_5
    iget-object v2, p0, Landroid/animation/AnimatorSet$SeekState;->this$0:Landroid/animation/AnimatorSet;

    goto/32 :goto_a

    nop

    :goto_6
    const-wide/16 v2, -0x1

    goto/32 :goto_17

    nop

    :goto_7
    const-wide/16 v2, 0x0

    goto/32 :goto_12

    nop

    :goto_8
    iput-wide v0, p0, Landroid/animation/AnimatorSet$SeekState;->mPlayTime:J

    goto/32 :goto_13

    nop

    :goto_9
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->getTotalDuration()J

    move-result-wide v0

    goto/32 :goto_5

    nop

    :goto_a
    invoke-static {v2}, Landroid/animation/AnimatorSet;->-$$Nest$fgetmStartDelay(Landroid/animation/AnimatorSet;)J

    move-result-wide v2

    goto/32 :goto_15

    nop

    :goto_b
    throw v0

    :goto_c
    goto/32 :goto_10

    nop

    :goto_d
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    goto/32 :goto_11

    nop

    :goto_e
    if-nez v0, :cond_2

    goto/32 :goto_1a

    :cond_2
    goto/32 :goto_19

    nop

    :goto_f
    if-gez v0, :cond_3

    goto/32 :goto_14

    :cond_3
    goto/32 :goto_1b

    nop

    :goto_10
    iget-wide v0, p0, Landroid/animation/AnimatorSet$SeekState;->mPlayTime:J

    goto/32 :goto_7

    nop

    :goto_11
    const-string v1, "Error: Cannot reverse infinite animator set"

    goto/32 :goto_1d

    nop

    :goto_12
    cmp-long v0, v0, v2

    goto/32 :goto_f

    nop

    :goto_13
    iput-boolean p1, p0, Landroid/animation/AnimatorSet$SeekState;->mSeekingInReverse:Z

    :goto_14
    goto/32 :goto_2

    nop

    :goto_15
    sub-long/2addr v0, v2

    goto/32 :goto_4

    nop

    :goto_16
    iget-object v0, p0, Landroid/animation/AnimatorSet$SeekState;->this$0:Landroid/animation/AnimatorSet;

    goto/32 :goto_0

    nop

    :goto_17
    cmp-long v0, v0, v2

    goto/32 :goto_e

    nop

    :goto_18
    iget-object v0, p0, Landroid/animation/AnimatorSet$SeekState;->this$0:Landroid/animation/AnimatorSet;

    goto/32 :goto_9

    nop

    :goto_19
    goto :goto_c

    :goto_1a
    goto/32 :goto_d

    nop

    :goto_1b
    iget-boolean v0, p0, Landroid/animation/AnimatorSet$SeekState;->mSeekingInReverse:Z

    goto/32 :goto_1

    nop

    :goto_1c
    sub-long/2addr v0, v2

    goto/32 :goto_8

    nop

    :goto_1d
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_b

    nop
.end method
