.class Landroid/animation/PropertyValuesHolder$MultiIntValuesHolder;
.super Landroid/animation/PropertyValuesHolder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/animation/PropertyValuesHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MultiIntValuesHolder"
.end annotation


# static fields
.field private static final sJNISetterPropertyMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Class;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private mJniSetter:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Landroid/animation/PropertyValuesHolder$MultiIntValuesHolder;->sJNISetterPropertyMap:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/animation/TypeConverter;Landroid/animation/TypeEvaluator;Landroid/animation/Keyframes;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/animation/PropertyValuesHolder;-><init>(Ljava/lang/String;Landroid/animation/PropertyValuesHolder-IA;)V

    invoke-virtual {p0, p2}, Landroid/animation/PropertyValuesHolder$MultiIntValuesHolder;->setConverter(Landroid/animation/TypeConverter;)V

    iput-object p4, p0, Landroid/animation/PropertyValuesHolder$MultiIntValuesHolder;->mKeyframes:Landroid/animation/Keyframes;

    invoke-virtual {p0, p3}, Landroid/animation/PropertyValuesHolder$MultiIntValuesHolder;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    return-void
.end method

.method public varargs constructor <init>(Ljava/lang/String;Landroid/animation/TypeConverter;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/animation/PropertyValuesHolder;-><init>(Ljava/lang/String;Landroid/animation/PropertyValuesHolder-IA;)V

    invoke-virtual {p0, p2}, Landroid/animation/PropertyValuesHolder$MultiIntValuesHolder;->setConverter(Landroid/animation/TypeConverter;)V

    invoke-virtual {p0, p4}, Landroid/animation/PropertyValuesHolder$MultiIntValuesHolder;->setObjectValues([Ljava/lang/Object;)V

    invoke-virtual {p0, p3}, Landroid/animation/PropertyValuesHolder$MultiIntValuesHolder;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    return-void
.end method


# virtual methods
.method setAnimatedValue(Ljava/lang/Object;)V
    .locals 9

    goto/32 :goto_1a

    nop

    :goto_0
    const/4 v2, 0x3

    goto/32 :goto_12

    nop

    :goto_1
    invoke-static/range {v2 .. v8}, Landroid/animation/PropertyValuesHolder;->-$$Nest$smnCallFourIntMethod(Ljava/lang/Object;JIIII)V

    goto/32 :goto_2

    nop

    :goto_2
    goto :goto_16

    :pswitch_0
    goto/32 :goto_c

    nop

    :goto_3
    const-wide/16 v5, 0x0

    goto/32 :goto_19

    nop

    :goto_4
    aget v5, v0, v5

    goto/32 :goto_11

    nop

    :goto_5
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :goto_6
    invoke-static {p1, v3, v4, v5, v2}, Landroid/animation/PropertyValuesHolder;->-$$Nest$smnCallTwoIntMethod(Ljava/lang/Object;JII)V

    goto/32 :goto_7

    nop

    :goto_7
    goto :goto_16

    :pswitch_1
    goto/32 :goto_17

    nop

    :goto_8
    goto :goto_16

    :pswitch_2
    goto/32 :goto_4

    nop

    :goto_9
    invoke-static {p1, v3, v4, v0}, Landroid/animation/PropertyValuesHolder;->-$$Nest$smnCallMultipleIntMethod(Ljava/lang/Object;J[I)V

    goto/32 :goto_8

    nop

    :goto_a
    const/4 v5, 0x0

    packed-switch v1, :pswitch_data_0

    :pswitch_3
    goto/32 :goto_9

    nop

    :goto_b
    move-object v2, p1

    goto/32 :goto_1

    nop

    :goto_c
    aget v5, v0, v5

    goto/32 :goto_14

    nop

    :goto_d
    check-cast v0, [I

    goto/32 :goto_13

    nop

    :goto_e
    if-nez v2, :cond_0

    goto/32 :goto_16

    :cond_0
    goto/32 :goto_18

    nop

    :goto_f
    const/4 v2, 0x2

    goto/32 :goto_10

    nop

    :goto_10
    aget v7, v0, v2

    goto/32 :goto_0

    nop

    :goto_11
    aget v6, v0, v2

    goto/32 :goto_f

    nop

    :goto_12
    aget v8, v0, v2

    goto/32 :goto_b

    nop

    :goto_13
    array-length v1, v0

    goto/32 :goto_1b

    nop

    :goto_14
    aget v2, v0, v2

    goto/32 :goto_6

    nop

    :goto_15
    invoke-static {p1, v3, v4, v2}, Landroid/animation/PropertyValuesHolder;->-$$Nest$smnCallIntMethod(Ljava/lang/Object;JI)V

    :goto_16
    goto/32 :goto_5

    nop

    :goto_17
    aget v2, v0, v5

    goto/32 :goto_15

    nop

    :goto_18
    const/4 v2, 0x1

    goto/32 :goto_a

    nop

    :goto_19
    cmp-long v2, v3, v5

    goto/32 :goto_e

    nop

    :goto_1a
    invoke-virtual {p0}, Landroid/animation/PropertyValuesHolder$MultiIntValuesHolder;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_d

    nop

    :goto_1b
    iget-wide v3, p0, Landroid/animation/PropertyValuesHolder$MultiIntValuesHolder;->mJniSetter:J

    goto/32 :goto_3

    nop
.end method

.method setupSetter(Ljava/lang/Class;)V
    .locals 9

    goto/32 :goto_7

    nop

    :goto_0
    const-wide/16 v2, 0x0

    goto/32 :goto_6

    nop

    :goto_1
    if-eqz v1, :cond_0

    goto/32 :goto_2

    :cond_0
    :try_start_0
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    move-object v1, v6

    sget-object v6, Landroid/animation/PropertyValuesHolder$MultiIntValuesHolder;->sJNISetterPropertyMap:Ljava/util/HashMap;

    invoke-virtual {v6, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_2
    iget-object v6, p0, Landroid/animation/PropertyValuesHolder$MultiIntValuesHolder;->mPropertyName:Ljava/lang/String;

    iget-wide v7, p0, Landroid/animation/PropertyValuesHolder$MultiIntValuesHolder;->mJniSetter:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_3

    nop

    :goto_3
    throw v1

    :goto_4
    goto :goto_5

    :catch_0
    move-exception v7

    :goto_5
    goto/32 :goto_1

    nop

    :goto_6
    cmp-long v0, v0, v2

    goto/32 :goto_b

    nop

    :goto_7
    iget-wide v0, p0, Landroid/animation/PropertyValuesHolder$MultiIntValuesHolder;->mJniSetter:J

    goto/32 :goto_0

    nop

    :goto_8
    goto :goto_5

    :catch_1
    move-exception v6

    :try_start_1
    iget-object v7, p0, Landroid/animation/PropertyValuesHolder$MultiIntValuesHolder;->mPropertyName:Ljava/lang/String;

    invoke-static {p1, v7, v5}, Landroid/animation/PropertyValuesHolder;->-$$Nest$smnGetMultipleIntMethod(Ljava/lang/Class;Ljava/lang/String;I)J

    move-result-wide v7

    iput-wide v7, p0, Landroid/animation/PropertyValuesHolder$MultiIntValuesHolder;->mJniSetter:J
    :try_end_1
    .catch Ljava/lang/NoSuchMethodError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/32 :goto_4

    nop

    :goto_9
    monitor-enter v0

    :try_start_2
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    iget-object v3, p0, Landroid/animation/PropertyValuesHolder$MultiIntValuesHolder;->mPropertyName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    move v2, v3

    if-eqz v2, :cond_2

    iget-object v3, p0, Landroid/animation/PropertyValuesHolder$MultiIntValuesHolder;->mPropertyName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, p0, Landroid/animation/PropertyValuesHolder$MultiIntValuesHolder;->mJniSetter:J

    :cond_2
    if-nez v2, :cond_1

    const-string/jumbo v3, "set"

    iget-object v4, p0, Landroid/animation/PropertyValuesHolder$MultiIntValuesHolder;->mPropertyName:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/animation/PropertyValuesHolder$MultiIntValuesHolder;->getMethodName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Landroid/animation/PropertyValuesHolder$MultiIntValuesHolder;->calculateValue(F)V

    invoke-virtual {p0}, Landroid/animation/PropertyValuesHolder$MultiIntValuesHolder;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [I

    array-length v5, v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-static {p1, v3, v5}, Landroid/animation/PropertyValuesHolder;->-$$Nest$smnGetMultipleIntMethod(Ljava/lang/Class;Ljava/lang/String;I)J

    move-result-wide v6

    iput-wide v6, p0, Landroid/animation/PropertyValuesHolder$MultiIntValuesHolder;->mJniSetter:J
    :try_end_3
    .catch Ljava/lang/NoSuchMethodError; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/32 :goto_8

    nop

    :goto_a
    sget-object v0, Landroid/animation/PropertyValuesHolder$MultiIntValuesHolder;->sJNISetterPropertyMap:Ljava/util/HashMap;

    goto/32 :goto_9

    nop

    :goto_b
    if-nez v0, :cond_3

    goto/32 :goto_d

    :cond_3
    goto/32 :goto_c

    nop

    :goto_c
    return-void

    :goto_d
    goto/32 :goto_a

    nop
.end method

.method setupSetterAndGetter(Ljava/lang/Object;)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {p0, v0}, Landroid/animation/PropertyValuesHolder$MultiIntValuesHolder;->setupSetter(Ljava/lang/Class;)V

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method
