.class Landroid/animation/AnimatorSet$AnimationEvent;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/animation/AnimatorSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AnimationEvent"
.end annotation


# static fields
.field static final ANIMATION_DELAY_ENDED:I = 0x1

.field static final ANIMATION_END:I = 0x2

.field static final ANIMATION_START:I


# instance fields
.field final mEvent:I

.field final mNode:Landroid/animation/AnimatorSet$Node;


# direct methods
.method constructor <init>(Landroid/animation/AnimatorSet$Node;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/animation/AnimatorSet$AnimationEvent;->mNode:Landroid/animation/AnimatorSet$Node;

    iput p2, p0, Landroid/animation/AnimatorSet$AnimationEvent;->mEvent:I

    return-void
.end method


# virtual methods
.method getTime()J
    .locals 4

    goto/32 :goto_e

    nop

    :goto_0
    add-long/2addr v2, v0

    :goto_1
    goto/32 :goto_17

    nop

    :goto_2
    if-eq v0, v1, :cond_0

    goto/32 :goto_18

    :cond_0
    goto/32 :goto_12

    nop

    :goto_3
    iget-wide v0, v0, Landroid/animation/AnimatorSet$Node;->mStartTime:J

    goto/32 :goto_a

    nop

    :goto_4
    if-eqz v0, :cond_1

    goto/32 :goto_14

    :cond_1
    goto/32 :goto_13

    nop

    :goto_5
    const-wide/16 v2, -0x1

    goto/32 :goto_1a

    nop

    :goto_6
    iget-object v2, p0, Landroid/animation/AnimatorSet$AnimationEvent;->mNode:Landroid/animation/AnimatorSet$Node;

    goto/32 :goto_c

    nop

    :goto_7
    iget-wide v0, v0, Landroid/animation/AnimatorSet$Node;->mStartTime:J

    goto/32 :goto_6

    nop

    :goto_8
    iget-wide v0, v0, Landroid/animation/AnimatorSet$Node;->mEndTime:J

    goto/32 :goto_d

    nop

    :goto_9
    if-eqz v0, :cond_2

    goto/32 :goto_b

    :cond_2
    goto/32 :goto_19

    nop

    :goto_a
    return-wide v0

    :goto_b
    goto/32 :goto_10

    nop

    :goto_c
    iget-object v2, v2, Landroid/animation/AnimatorSet$Node;->mAnimation:Landroid/animation/Animator;

    goto/32 :goto_11

    nop

    :goto_d
    return-wide v0

    :goto_e
    iget v0, p0, Landroid/animation/AnimatorSet$AnimationEvent;->mEvent:I

    goto/32 :goto_9

    nop

    :goto_f
    iget-wide v0, v0, Landroid/animation/AnimatorSet$Node;->mStartTime:J

    goto/32 :goto_5

    nop

    :goto_10
    const/4 v1, 0x1

    goto/32 :goto_2

    nop

    :goto_11
    invoke-virtual {v2}, Landroid/animation/Animator;->getStartDelay()J

    move-result-wide v2

    goto/32 :goto_0

    nop

    :goto_12
    iget-object v0, p0, Landroid/animation/AnimatorSet$AnimationEvent;->mNode:Landroid/animation/AnimatorSet$Node;

    goto/32 :goto_f

    nop

    :goto_13
    goto :goto_1

    :goto_14
    goto/32 :goto_15

    nop

    :goto_15
    iget-object v0, p0, Landroid/animation/AnimatorSet$AnimationEvent;->mNode:Landroid/animation/AnimatorSet$Node;

    goto/32 :goto_7

    nop

    :goto_16
    iget-object v0, p0, Landroid/animation/AnimatorSet$AnimationEvent;->mNode:Landroid/animation/AnimatorSet$Node;

    goto/32 :goto_8

    nop

    :goto_17
    return-wide v2

    :goto_18
    goto/32 :goto_16

    nop

    :goto_19
    iget-object v0, p0, Landroid/animation/AnimatorSet$AnimationEvent;->mNode:Landroid/animation/AnimatorSet$Node;

    goto/32 :goto_3

    nop

    :goto_1a
    cmp-long v0, v0, v2

    goto/32 :goto_4

    nop
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    iget v0, p0, Landroid/animation/AnimatorSet$AnimationEvent;->mEvent:I

    if-nez v0, :cond_0

    const-string/jumbo v0, "start"

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const-string v0, "delay ended"

    goto :goto_0

    :cond_1
    const-string v0, "end"

    :goto_0
    nop

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/animation/AnimatorSet$AnimationEvent;->mNode:Landroid/animation/AnimatorSet$Node;

    iget-object v2, v2, Landroid/animation/AnimatorSet$Node;->mAnimation:Landroid/animation/Animator;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
