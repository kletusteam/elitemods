.class public Landroid/animation/TimeAnimator;
.super Landroid/animation/ValueAnimator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/animation/TimeAnimator$TimeListener;
    }
.end annotation


# instance fields
.field private mListener:Landroid/animation/TimeAnimator$TimeListener;

.field private mPreviousTime:J


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/animation/ValueAnimator;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/animation/TimeAnimator;->mPreviousTime:J

    return-void
.end method


# virtual methods
.method animateBasedOnTime(J)Z
    .locals 8

    goto/32 :goto_6

    nop

    :goto_0
    sub-long v0, p1, v0

    goto/32 :goto_12

    nop

    :goto_1
    move-wide v6, v4

    goto/32 :goto_10

    nop

    :goto_2
    iget-object v2, p0, Landroid/animation/TimeAnimator;->mListener:Landroid/animation/TimeAnimator$TimeListener;

    goto/32 :goto_5

    nop

    :goto_3
    move-wide v4, v0

    goto/32 :goto_b

    nop

    :goto_4
    if-ltz v6, :cond_0

    goto/32 :goto_11

    :cond_0
    goto/32 :goto_1

    nop

    :goto_5
    move-object v3, p0

    goto/32 :goto_3

    nop

    :goto_6
    iget-object v0, p0, Landroid/animation/TimeAnimator;->mListener:Landroid/animation/TimeAnimator$TimeListener;

    goto/32 :goto_a

    nop

    :goto_7
    return v0

    :goto_8
    iput-wide p1, p0, Landroid/animation/TimeAnimator;->mPreviousTime:J

    goto/32 :goto_2

    nop

    :goto_9
    const/4 v0, 0x0

    goto/32 :goto_7

    nop

    :goto_a
    if-nez v0, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_15

    nop

    :goto_b
    invoke-interface/range {v2 .. v7}, Landroid/animation/TimeAnimator$TimeListener;->onTimeUpdate(Landroid/animation/TimeAnimator;JJ)V

    :goto_c
    goto/32 :goto_9

    nop

    :goto_d
    sub-long v2, p1, v2

    goto/32 :goto_e

    nop

    :goto_e
    move-wide v6, v2

    :goto_f
    goto/32 :goto_8

    nop

    :goto_10
    goto :goto_f

    :goto_11
    goto/32 :goto_d

    nop

    :goto_12
    iget-wide v2, p0, Landroid/animation/TimeAnimator;->mPreviousTime:J

    goto/32 :goto_14

    nop

    :goto_13
    cmp-long v6, v2, v4

    goto/32 :goto_4

    nop

    :goto_14
    const-wide/16 v4, 0x0

    goto/32 :goto_13

    nop

    :goto_15
    iget-wide v0, p0, Landroid/animation/TimeAnimator;->mStartTime:J

    goto/32 :goto_0

    nop
.end method

.method animateValue(F)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    return-void
.end method

.method initAnimation()V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    return-void
.end method

.method public setCurrentPlayTime(J)V
    .locals 6

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Landroid/animation/TimeAnimator;->mStartTime:J

    sub-long v4, v0, p1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Landroid/animation/TimeAnimator;->mStartTime:J

    const/4 v2, 0x1

    iput-boolean v2, p0, Landroid/animation/TimeAnimator;->mStartTimeCommitted:Z

    invoke-virtual {p0, v0, v1}, Landroid/animation/TimeAnimator;->animateBasedOnTime(J)Z

    return-void
.end method

.method public setTimeListener(Landroid/animation/TimeAnimator$TimeListener;)V
    .locals 0

    iput-object p1, p0, Landroid/animation/TimeAnimator;->mListener:Landroid/animation/TimeAnimator$TimeListener;

    return-void
.end method

.method public start()V
    .locals 2

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/animation/TimeAnimator;->mPreviousTime:J

    invoke-super {p0}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method
