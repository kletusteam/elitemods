.class public Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;
.super Landroid/Utils/BitmapHelper$GraphicalValuesData;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/Utils/BitmapHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ShapeGraphicalValuesData"
.end annotation


# instance fields
.field shape:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/Utils/BitmapHelper$GraphicalValuesData;-><init>()V

    return-void
.end method

.method public static create()Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;
    .locals 1

    new-instance v0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;

    invoke-direct {v0}, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;-><init>()V

    return-object v0
.end method

.method public static create(Landroid/graphics/Bitmap;IIILandroid/Utils/BitmapHelper$GradientDirection;IIIIII)Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;
    .locals 12

    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move/from16 v11, p10

    invoke-static/range {v0 .. v11}, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->create(Landroid/graphics/Bitmap;IIILandroid/Utils/BitmapHelper$GradientDirection;IIIIIII)Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;

    move-result-object v0

    return-object v0
.end method

.method public static create(Landroid/graphics/Bitmap;IIILandroid/Utils/BitmapHelper$GradientDirection;IIIIIII)Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;
    .locals 1

    new-instance v0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;

    invoke-direct {v0}, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;-><init>()V

    iput-object p0, v0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->source_bitmap:Landroid/graphics/Bitmap;

    iput p1, v0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->color_bg:I

    iput p2, v0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->color_bg_two:I

    iput p3, v0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->color_bg_three:I

    if-eqz p4, :cond_0

    iput-object p4, v0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->direction:Landroid/Utils/BitmapHelper$GradientDirection;

    :cond_0
    iput p5, v0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->stroke_color:I

    iput p6, v0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->stroke_color_two:I

    iput p7, v0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->stroke_width:I

    if-gtz p8, :cond_1

    const/4 p8, 0x1

    :cond_1
    iput p8, v0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->width:I

    if-gtz p9, :cond_2

    const/4 p9, 0x1

    :cond_2
    iput p9, v0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->height:I

    iput p10, v0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->corner_radius:I

    iput p11, v0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->shape:I

    return-object v0
.end method
