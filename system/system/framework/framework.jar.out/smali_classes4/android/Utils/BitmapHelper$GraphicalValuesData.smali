.class public Landroid/Utils/BitmapHelper$GraphicalValuesData;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/Utils/BitmapHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GraphicalValuesData"
.end annotation


# instance fields
.field public color_bg:I

.field public color_bg_three:I

.field public color_bg_two:I

.field public corner_radius:I

.field public direction:Landroid/Utils/BitmapHelper$GradientDirection;

.field public height:I

.field public isNeedScaledBitmap:Z

.field public source_bitmap:Landroid/graphics/Bitmap;

.field public stroke_color:I

.field public stroke_color_two:I

.field public stroke_width:I

.field public width:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->source_bitmap:Landroid/graphics/Bitmap;

    iput v1, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->color_bg:I

    iput v1, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->color_bg_two:I

    iput v1, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->color_bg_three:I

    sget-object v0, Landroid/Utils/BitmapHelper$GradientDirection;->Diagonal:Landroid/Utils/BitmapHelper$GradientDirection;

    iput-object v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->direction:Landroid/Utils/BitmapHelper$GradientDirection;

    iput v1, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->stroke_color:I

    iput v1, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->stroke_color_two:I

    iput v1, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->stroke_width:I

    iput v2, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->width:I

    iput v2, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->height:I

    iput v1, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->corner_radius:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->isNeedScaledBitmap:Z

    return-void
.end method

.method public static create()Landroid/Utils/BitmapHelper$GraphicalValuesData;
    .locals 1

    new-instance v0, Landroid/Utils/BitmapHelper$GraphicalValuesData;

    invoke-direct {v0}, Landroid/Utils/BitmapHelper$GraphicalValuesData;-><init>()V

    return-object v0
.end method

.method public static create(Landroid/graphics/Bitmap;IIILandroid/Utils/BitmapHelper$GradientDirection;IIIII)Landroid/Utils/BitmapHelper$GraphicalValuesData;
    .locals 11

    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move/from16 v5, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    invoke-static/range {v0 .. v10}, Landroid/Utils/BitmapHelper$GraphicalValuesData;->create(Landroid/graphics/Bitmap;IIILandroid/Utils/BitmapHelper$GradientDirection;IIIIII)Landroid/Utils/BitmapHelper$GraphicalValuesData;

    move-result-object v0

    return-object v0
.end method

.method public static create(Landroid/graphics/Bitmap;IIILandroid/Utils/BitmapHelper$GradientDirection;IIIIII)Landroid/Utils/BitmapHelper$GraphicalValuesData;
    .locals 1

    new-instance v0, Landroid/Utils/BitmapHelper$GraphicalValuesData;

    invoke-direct {v0}, Landroid/Utils/BitmapHelper$GraphicalValuesData;-><init>()V

    iput-object p0, v0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->source_bitmap:Landroid/graphics/Bitmap;

    iput p1, v0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->color_bg:I

    iput p2, v0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->color_bg_two:I

    iput p3, v0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->color_bg_three:I

    if-eqz p4, :cond_0

    iput-object p4, v0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->direction:Landroid/Utils/BitmapHelper$GradientDirection;

    :cond_0
    iput p5, v0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->stroke_color:I

    iput p6, v0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->stroke_color_two:I

    iput p7, v0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->stroke_width:I

    if-gtz p8, :cond_1

    const/4 p8, 0x1

    :cond_1
    iput p8, v0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->width:I

    if-gtz p9, :cond_2

    const/4 p9, 0x1

    :cond_2
    iput p9, v0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->height:I

    iput p10, v0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->corner_radius:I

    return-object v0
.end method

.method public static create(Landroid/graphics/Bitmap;IIILandroid/Utils/BitmapHelper$GradientDirection;IIIIIZ)Landroid/Utils/BitmapHelper$GraphicalValuesData;
    .locals 13

    const/4 v7, 0x0

    move-object v1, p0

    move v2, p1

    move v3, p2

    move/from16 v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    move/from16 v11, p9

    invoke-static/range {v1 .. v11}, Landroid/Utils/BitmapHelper$GraphicalValuesData;->create(Landroid/graphics/Bitmap;IIILandroid/Utils/BitmapHelper$GradientDirection;IIIIII)Landroid/Utils/BitmapHelper$GraphicalValuesData;

    move-result-object v12

    move/from16 v0, p10

    iput-boolean v0, v12, Landroid/Utils/BitmapHelper$GraphicalValuesData;->isNeedScaledBitmap:Z

    return-object v12
.end method


# virtual methods
.method public copy()Landroid/Utils/BitmapHelper$GraphicalValuesData;
    .locals 12

    iget-object v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->source_bitmap:Landroid/graphics/Bitmap;

    iget v1, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->color_bg:I

    iget v2, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->color_bg_two:I

    iget v3, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->color_bg_three:I

    iget-object v4, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->direction:Landroid/Utils/BitmapHelper$GradientDirection;

    iget v5, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->stroke_color:I

    iget v6, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->stroke_color_two:I

    iget v7, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->stroke_width:I

    iget v8, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->width:I

    iget v9, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->height:I

    iget v10, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->corner_radius:I

    invoke-static/range {v0 .. v10}, Landroid/Utils/BitmapHelper$GraphicalValuesData;->create(Landroid/graphics/Bitmap;IIILandroid/Utils/BitmapHelper$GradientDirection;IIIIII)Landroid/Utils/BitmapHelper$GraphicalValuesData;

    move-result-object v11

    iget-boolean v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->isNeedScaledBitmap:Z

    iput-boolean v0, v11, Landroid/Utils/BitmapHelper$GraphicalValuesData;->isNeedScaledBitmap:Z

    return-object v11
.end method

.method getRect()Landroid/graphics/Rect;
    .locals 4

    goto/32 :goto_3

    nop

    :goto_0
    iget v2, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->height:I

    goto/32 :goto_5

    nop

    :goto_1
    return-object v0

    :goto_2
    new-instance v0, Landroid/graphics/Rect;

    goto/32 :goto_4

    nop

    :goto_3
    const/4 v3, 0x0

    goto/32 :goto_2

    nop

    :goto_4
    iget v1, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->width:I

    goto/32 :goto_0

    nop

    :goto_5
    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    goto/32 :goto_1

    nop
.end method

.method getStrokeRect()Landroid/graphics/Rect;
    .locals 4

    goto/32 :goto_3

    nop

    :goto_0
    new-instance v1, Landroid/graphics/Rect;

    goto/32 :goto_4

    nop

    :goto_1
    invoke-direct {v1, v0, v0, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    goto/32 :goto_7

    nop

    :goto_2
    sub-int/2addr v2, v0

    goto/32 :goto_6

    nop

    :goto_3
    iget v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->stroke_width:I

    goto/32 :goto_0

    nop

    :goto_4
    iget v2, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->width:I

    goto/32 :goto_2

    nop

    :goto_5
    sub-int/2addr v3, v0

    goto/32 :goto_1

    nop

    :goto_6
    iget v3, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->height:I

    goto/32 :goto_5

    nop

    :goto_7
    return-object v1
.end method

.method isCorner()Z
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    return v0

    :goto_1
    goto/32 :goto_4

    nop

    :goto_2
    goto :goto_6

    :goto_3
    iget v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->corner_radius:I

    goto/32 :goto_7

    nop

    :goto_4
    const/4 v0, 0x0

    goto/32 :goto_2

    nop

    :goto_5
    const/4 v0, 0x1

    :goto_6
    goto/32 :goto_0

    nop

    :goto_7
    if-gtz v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_5

    nop
.end method

.method isDoubleGradient()Z
    .locals 1

    goto/32 :goto_b

    nop

    :goto_0
    const/4 v0, 0x1

    :goto_1
    goto/32 :goto_4

    nop

    :goto_2
    if-eqz v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_8

    nop

    :goto_3
    if-eqz v0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_a

    nop

    :goto_4
    return v0

    :goto_5
    goto/32 :goto_9

    nop

    :goto_6
    if-nez v0, :cond_2

    goto/32 :goto_5

    :cond_2
    goto/32 :goto_0

    nop

    :goto_7
    goto :goto_1

    :goto_8
    iget v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->color_bg_two:I

    goto/32 :goto_6

    nop

    :goto_9
    const/4 v0, 0x0

    goto/32 :goto_7

    nop

    :goto_a
    iget v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->color_bg_three:I

    goto/32 :goto_2

    nop

    :goto_b
    invoke-virtual {p0}, Landroid/Utils/BitmapHelper$GraphicalValuesData;->isStok()Z

    move-result v0

    goto/32 :goto_3

    nop
.end method

.method isFrame()Z
    .locals 1

    goto/32 :goto_7

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    goto :goto_3

    :goto_2
    const/4 v0, 0x1

    :goto_3
    goto/32 :goto_5

    nop

    :goto_4
    if-gtz v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_2

    nop

    :goto_5
    return v0

    :goto_6
    goto/32 :goto_0

    nop

    :goto_7
    iget v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->stroke_width:I

    goto/32 :goto_4

    nop
.end method

.method isStok()Z
    .locals 1

    goto/32 :goto_7

    nop

    :goto_0
    const/4 v0, 0x1

    :goto_1
    goto/32 :goto_5

    nop

    :goto_2
    goto :goto_1

    :goto_3
    const/4 v0, 0x0

    goto/32 :goto_2

    nop

    :goto_4
    if-eqz v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_0

    nop

    :goto_5
    return v0

    :goto_6
    goto/32 :goto_3

    nop

    :goto_7
    iget v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->color_bg:I

    goto/32 :goto_4

    nop
.end method

.method isTripleGradient()Z
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_8

    nop

    :goto_1
    iget v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->color_bg_two:I

    goto/32 :goto_7

    nop

    :goto_2
    return v0

    :goto_3
    goto/32 :goto_0

    nop

    :goto_4
    invoke-virtual {p0}, Landroid/Utils/BitmapHelper$GraphicalValuesData;->isStok()Z

    move-result v0

    goto/32 :goto_5

    nop

    :goto_5
    if-eqz v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_1

    nop

    :goto_6
    iget v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->color_bg_three:I

    goto/32 :goto_b

    nop

    :goto_7
    if-nez v0, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_6

    nop

    :goto_8
    goto :goto_a

    :goto_9
    const/4 v0, 0x1

    :goto_a
    goto/32 :goto_2

    nop

    :goto_b
    if-nez v0, :cond_2

    goto/32 :goto_3

    :cond_2
    goto/32 :goto_9

    nop
.end method
