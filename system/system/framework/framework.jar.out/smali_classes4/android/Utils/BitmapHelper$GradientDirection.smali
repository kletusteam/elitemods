.class public final enum Landroid/Utils/BitmapHelper$GradientDirection;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/Utils/BitmapHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "GradientDirection"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/Utils/BitmapHelper$GradientDirection;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/Utils/BitmapHelper$GradientDirection;

.field public static final enum Diagonal:Landroid/Utils/BitmapHelper$GradientDirection;

.field public static final enum Horizontal:Landroid/Utils/BitmapHelper$GradientDirection;

.field public static final enum Vertical:Landroid/Utils/BitmapHelper$GradientDirection;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Landroid/Utils/BitmapHelper$GradientDirection;

    const-string v1, "Horizontal"

    invoke-direct {v0, v1, v2}, Landroid/Utils/BitmapHelper$GradientDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/Utils/BitmapHelper$GradientDirection;->Horizontal:Landroid/Utils/BitmapHelper$GradientDirection;

    new-instance v0, Landroid/Utils/BitmapHelper$GradientDirection;

    const-string v1, "Vertical"

    invoke-direct {v0, v1, v3}, Landroid/Utils/BitmapHelper$GradientDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/Utils/BitmapHelper$GradientDirection;->Vertical:Landroid/Utils/BitmapHelper$GradientDirection;

    new-instance v0, Landroid/Utils/BitmapHelper$GradientDirection;

    const-string v1, "Diagonal"

    invoke-direct {v0, v1, v4}, Landroid/Utils/BitmapHelper$GradientDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/Utils/BitmapHelper$GradientDirection;->Diagonal:Landroid/Utils/BitmapHelper$GradientDirection;

    const/4 v0, 0x3

    new-array v0, v0, [Landroid/Utils/BitmapHelper$GradientDirection;

    sget-object v1, Landroid/Utils/BitmapHelper$GradientDirection;->Horizontal:Landroid/Utils/BitmapHelper$GradientDirection;

    aput-object v1, v0, v2

    sget-object v1, Landroid/Utils/BitmapHelper$GradientDirection;->Vertical:Landroid/Utils/BitmapHelper$GradientDirection;

    aput-object v1, v0, v3

    sget-object v1, Landroid/Utils/BitmapHelper$GradientDirection;->Diagonal:Landroid/Utils/BitmapHelper$GradientDirection;

    aput-object v1, v0, v4

    sput-object v0, Landroid/Utils/BitmapHelper$GradientDirection;->$VALUES:[Landroid/Utils/BitmapHelper$GradientDirection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/Utils/BitmapHelper$GradientDirection;
    .locals 1

    const-class v0, Landroid/Utils/BitmapHelper$GradientDirection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Landroid/Utils/BitmapHelper$GradientDirection;

    return-object v0
.end method

.method public static values()[Landroid/Utils/BitmapHelper$GradientDirection;
    .locals 1

    sget-object v0, Landroid/Utils/BitmapHelper$GradientDirection;->$VALUES:[Landroid/Utils/BitmapHelper$GradientDirection;

    invoke-virtual {v0}, [Landroid/Utils/BitmapHelper$GradientDirection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/Utils/BitmapHelper$GradientDirection;

    return-object v0
.end method
