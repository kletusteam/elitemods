.class public Landroid/Utils/BitmapHelper;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;,
        Landroid/Utils/BitmapHelper$GraphicalValuesData;,
        Landroid/Utils/BitmapHelper$GradientDirection;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static checkSize(Landroid/Utils/BitmapHelper$GraphicalValuesData;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/Utils/BitmapHelper$GraphicalValuesData;",
            ">(TT;)V"
        }
    .end annotation

    const/4 v2, 0x2

    const/4 v1, 0x1

    iget v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->width:I

    if-ge v0, v1, :cond_0

    iput v2, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->width:I

    :cond_0
    iget v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->height:I

    if-ge v0, v1, :cond_1

    iput v2, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->height:I

    :cond_1
    return-void
.end method

.method private static createBitmap(II)Landroid/graphics/Bitmap;
    .locals 1

    const/4 v0, 0x1

    if-ge p0, v0, :cond_0

    const/4 p0, 0x2

    :cond_0
    if-ge p1, v0, :cond_1

    const/4 p1, 0x2

    :cond_1
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p0, p1, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static createRectangleWithRoundedCorners(Landroid/Utils/BitmapHelper$GraphicalValuesData;)Landroid/graphics/Bitmap;
    .locals 10

    const/4 v9, 0x1

    const/4 v1, 0x0

    invoke-static {p0}, Landroid/Utils/BitmapHelper;->checkSize(Landroid/Utils/BitmapHelper$GraphicalValuesData;)V

    iget v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->width:I

    iget v2, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->height:I

    invoke-static {v0, v2}, Landroid/Utils/BitmapHelper;->createBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v7

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v5, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->source_bitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    iget v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->width:I

    iget v2, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->height:I

    invoke-static {v0, v2}, Landroid/Utils/BitmapHelper;->createBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->source_bitmap:Landroid/graphics/Bitmap;

    invoke-static {p0}, Landroid/Utils/BitmapHelper;->createShader(Landroid/Utils/BitmapHelper$GraphicalValuesData;)Landroid/graphics/Shader;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    new-instance v0, Landroid/graphics/Canvas;

    iget-object v2, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->source_bitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iget v2, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->width:I

    int-to-float v3, v2

    iget v2, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->height:I

    int-to-float v4, v2

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_0
    :goto_0
    invoke-virtual {v5}, Landroid/graphics/Paint;->reset()V

    new-instance v6, Landroid/graphics/Canvas;

    invoke-direct {v6, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-static {p0}, Landroid/Utils/BitmapHelper;->getPathRectangle(Landroid/Utils/BitmapHelper$GraphicalValuesData;)Landroid/graphics/Path;

    move-result-object v8

    invoke-virtual {p0}, Landroid/Utils/BitmapHelper$GraphicalValuesData;->isFrame()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v5, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-virtual {v6}, Landroid/graphics/Canvas;->save()I

    invoke-static {p0}, Landroid/Utils/BitmapHelper;->createStrokeShader(Landroid/Utils/BitmapHelper$GraphicalValuesData;)Landroid/graphics/Shader;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    invoke-virtual {v6, v8, v5}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    invoke-virtual {v6}, Landroid/graphics/Canvas;->restore()V

    invoke-virtual {v6}, Landroid/graphics/Canvas;->save()I

    invoke-static {p0}, Landroid/Utils/BitmapHelper;->getPathRectangleStroke(Landroid/Utils/BitmapHelper$GraphicalValuesData;)Landroid/graphics/Path;

    move-result-object v8

    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    invoke-virtual {v6, v8, v5}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    invoke-virtual {v6}, Landroid/graphics/Canvas;->restore()V

    invoke-virtual {v5}, Landroid/graphics/Paint;->reset()V

    :cond_1
    invoke-virtual {v5, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-virtual {v6}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {v6, v8}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    iget-boolean v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->isNeedScaledBitmap:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->source_bitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/Utils/BitmapHelper$GraphicalValuesData;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v6, v0, v1, v2, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :goto_1
    invoke-virtual {v6}, Landroid/graphics/Canvas;->restore()V

    return-object v7

    :cond_2
    iget-boolean v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->isNeedScaledBitmap:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->source_bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iget v2, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->height:I

    if-ne v0, v2, :cond_3

    iget-object v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->source_bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget v2, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->width:I

    if-eq v0, v2, :cond_0

    :cond_3
    iget-object v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->source_bitmap:Landroid/graphics/Bitmap;

    iget v2, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->width:I

    iget v3, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->height:I

    invoke-static {v0, v2, v3, v9}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->source_bitmap:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_4
    iget-object v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->source_bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6, v0, v1, v1, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1
.end method

.method private static createShader(Landroid/Utils/BitmapHelper$GraphicalValuesData;)Landroid/graphics/Shader;
    .locals 8

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/Utils/BitmapHelper$GraphicalValuesData;->isTripleGradient()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    new-array v5, v0, [I

    iget v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->color_bg:I

    aput v0, v5, v2

    iget v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->color_bg_two:I

    aput v0, v5, v3

    iget v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->color_bg_three:I

    aput v0, v5, v4

    :goto_0
    new-instance v0, Landroid/graphics/LinearGradient;

    iget-object v2, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->direction:Landroid/Utils/BitmapHelper$GradientDirection;

    sget-object v3, Landroid/Utils/BitmapHelper$GradientDirection;->Vertical:Landroid/Utils/BitmapHelper$GradientDirection;

    if-eq v2, v3, :cond_2

    iget v2, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->width:I

    int-to-float v3, v2

    :goto_1
    iget-object v2, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->direction:Landroid/Utils/BitmapHelper$GradientDirection;

    sget-object v4, Landroid/Utils/BitmapHelper$GradientDirection;->Horizontal:Landroid/Utils/BitmapHelper$GradientDirection;

    if-eq v2, v4, :cond_3

    iget v2, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->height:I

    int-to-float v4, v2

    :goto_2
    const/4 v6, 0x0

    sget-object v7, Landroid/graphics/Shader$TileMode;->MIRROR:Landroid/graphics/Shader$TileMode;

    move v2, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/Utils/BitmapHelper$GraphicalValuesData;->isDoubleGradient()Z

    move-result v0

    if-eqz v0, :cond_1

    new-array v5, v4, [I

    iget v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->color_bg:I

    aput v0, v5, v2

    iget v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->color_bg_two:I

    aput v0, v5, v3

    goto :goto_0

    :cond_1
    new-array v5, v4, [I

    iget v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->color_bg:I

    aput v0, v5, v2

    iget v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->color_bg:I

    aput v0, v5, v3

    goto :goto_0

    :cond_2
    move v3, v1

    goto :goto_1

    :cond_3
    move v4, v1

    goto :goto_2
.end method

.method public static createShape(Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;)Landroid/graphics/Bitmap;
    .locals 10

    const/4 v1, 0x0

    const/4 v9, 0x1

    invoke-static {p0}, Landroid/Utils/BitmapHelper;->checkSize(Landroid/Utils/BitmapHelper$GraphicalValuesData;)V

    iget v0, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->width:I

    iget v2, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->height:I

    invoke-static {v0, v2}, Landroid/Utils/BitmapHelper;->createBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v7

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v5, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->source_bitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    iget v0, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->width:I

    iget v2, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->height:I

    invoke-static {v0, v2}, Landroid/Utils/BitmapHelper;->createBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->source_bitmap:Landroid/graphics/Bitmap;

    invoke-static {p0}, Landroid/Utils/BitmapHelper;->createShader(Landroid/Utils/BitmapHelper$GraphicalValuesData;)Landroid/graphics/Shader;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    new-instance v0, Landroid/graphics/Canvas;

    iget-object v2, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->source_bitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iget v2, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->width:I

    int-to-float v3, v2

    iget v2, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->height:I

    int-to-float v4, v2

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_0
    :goto_0
    invoke-virtual {v5}, Landroid/graphics/Paint;->reset()V

    new-instance v6, Landroid/graphics/Canvas;

    invoke-direct {v6, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-static {p0}, Landroid/Utils/BitmapHelper;->getPath(Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;)Landroid/graphics/Path;

    move-result-object v8

    invoke-virtual {p0}, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->isFrame()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v5, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-virtual {v6}, Landroid/graphics/Canvas;->save()I

    invoke-static {p0}, Landroid/Utils/BitmapHelper;->createStrokeShader(Landroid/Utils/BitmapHelper$GraphicalValuesData;)Landroid/graphics/Shader;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    invoke-virtual {v6, v8, v5}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    invoke-virtual {v6}, Landroid/graphics/Canvas;->restore()V

    invoke-virtual {v6}, Landroid/graphics/Canvas;->save()I

    invoke-static {p0}, Landroid/Utils/BitmapHelper;->getPathStroke(Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;)Landroid/graphics/Path;

    move-result-object v8

    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    invoke-virtual {v6, v8, v5}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    invoke-virtual {v6}, Landroid/graphics/Canvas;->restore()V

    invoke-virtual {v5}, Landroid/graphics/Paint;->reset()V

    :cond_1
    invoke-virtual {v5, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-virtual {v6}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {v6, v8}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    iget-object v0, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->source_bitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v6, v0, v1, v2, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    invoke-virtual {v6}, Landroid/graphics/Canvas;->restore()V

    return-object v7

    :cond_2
    iget-object v0, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->source_bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iget v1, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->height:I

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->source_bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget v1, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->width:I

    if-eq v0, v1, :cond_0

    :cond_3
    iget-object v0, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->source_bitmap:Landroid/graphics/Bitmap;

    iget v1, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->width:I

    iget v2, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->height:I

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->source_bitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private static createStrokeShader(Landroid/Utils/BitmapHelper$GraphicalValuesData;)Landroid/graphics/Shader;
    .locals 8

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->stroke_color_two:I

    if-eqz v0, :cond_0

    new-array v5, v4, [I

    iget v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->stroke_color:I

    aput v0, v5, v2

    iget v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->stroke_color_two:I

    aput v0, v5, v3

    :goto_0
    new-instance v0, Landroid/graphics/LinearGradient;

    iget-object v2, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->direction:Landroid/Utils/BitmapHelper$GradientDirection;

    sget-object v3, Landroid/Utils/BitmapHelper$GradientDirection;->Vertical:Landroid/Utils/BitmapHelper$GradientDirection;

    if-eq v2, v3, :cond_1

    iget v2, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->width:I

    int-to-float v3, v2

    :goto_1
    iget-object v2, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->direction:Landroid/Utils/BitmapHelper$GradientDirection;

    sget-object v4, Landroid/Utils/BitmapHelper$GradientDirection;->Horizontal:Landroid/Utils/BitmapHelper$GradientDirection;

    if-eq v2, v4, :cond_2

    iget v2, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->height:I

    int-to-float v4, v2

    :goto_2
    const/4 v6, 0x0

    sget-object v7, Landroid/graphics/Shader$TileMode;->MIRROR:Landroid/graphics/Shader$TileMode;

    move v2, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    return-object v0

    :cond_0
    new-array v5, v4, [I

    iget v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->stroke_color:I

    aput v0, v5, v2

    iget v0, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->stroke_color:I

    aput v0, v5, v3

    goto :goto_0

    :cond_1
    move v3, v1

    goto :goto_1

    :cond_2
    move v4, v1

    goto :goto_2
.end method

.method private static getCircle(FI)Landroid/graphics/Path;
    .locals 4

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    int-to-float v1, p1

    add-float/2addr v1, p0

    int-to-float v2, p1

    add-float/2addr v2, p0

    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    return-object v0
.end method

.method public static getPath(Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;)Landroid/graphics/Path;
    .locals 10

    const/4 v8, 0x4

    const/4 v5, 0x1

    const/high16 v9, 0x40000000    # 2.0f

    const/4 v6, 0x0

    const/4 v3, 0x0

    invoke-static {p0}, Landroid/Utils/BitmapHelper;->checkSize(Landroid/Utils/BitmapHelper$GraphicalValuesData;)V

    iget v7, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->height:I

    int-to-float v7, v7

    div-float v4, v7, v9

    iget v7, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->shape:I

    if-nez v7, :cond_0

    invoke-static {v4, v6}, Landroid/Utils/BitmapHelper;->getCircle(FI)Landroid/graphics/Path;

    move-result-object v5

    :goto_0
    return-object v5

    :cond_0
    iget v7, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->shape:I

    if-ne v7, v8, :cond_1

    invoke-static {v3, v3, v8, v4}, Landroid/Utils/BitmapHelper;->getShape(FFIF)Landroid/graphics/Path;

    move-result-object v5

    goto :goto_0

    :cond_1
    iget v7, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->shape:I

    const/16 v8, 0x14

    if-ne v7, v8, :cond_2

    invoke-static {p0}, Landroid/Utils/BitmapHelper;->getPathRectangle(Landroid/Utils/BitmapHelper$GraphicalValuesData;)Landroid/graphics/Path;

    move-result-object v5

    goto :goto_0

    :cond_2
    iget v7, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->shape:I

    rem-int/lit8 v7, v7, 0x2

    if-nez v7, :cond_4

    move v1, v5

    :goto_1
    iget v7, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->shape:I

    const/16 v8, 0xa

    if-le v7, v8, :cond_5

    move v0, v5

    :goto_2
    if-eqz v1, :cond_7

    if-eqz v0, :cond_6

    move v2, v3

    :goto_3
    if-eqz v1, :cond_8

    if-eqz v0, :cond_3

    move v3, v4

    :cond_3
    :goto_4
    if-eqz v0, :cond_9

    iget v5, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->shape:I

    rem-int/lit8 v5, v5, 0xa

    :goto_5
    invoke-static {v2, v3, v5, v4}, Landroid/Utils/BitmapHelper;->getShape(FFIF)Landroid/graphics/Path;

    move-result-object v5

    goto :goto_0

    :cond_4
    move v1, v6

    goto :goto_1

    :cond_5
    move v0, v6

    goto :goto_2

    :cond_6
    move v2, v4

    goto :goto_3

    :cond_7
    move v2, v4

    goto :goto_3

    :cond_8
    if-eqz v0, :cond_3

    mul-float v3, v4, v9

    goto :goto_4

    :cond_9
    iget v5, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->shape:I

    goto :goto_5
.end method

.method public static getPathRectangle(Landroid/Utils/BitmapHelper$GraphicalValuesData;)Landroid/graphics/Path;
    .locals 6

    invoke-static {p0}, Landroid/Utils/BitmapHelper;->checkSize(Landroid/Utils/BitmapHelper$GraphicalValuesData;)V

    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    invoke-virtual {p0}, Landroid/Utils/BitmapHelper$GraphicalValuesData;->isCorner()Z

    move-result v3

    if-eqz v3, :cond_0

    iget v3, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->corner_radius:I

    int-to-float v3, v3

    const/high16 v4, 0x40800000    # 4.0f

    div-float/2addr v3, v4

    iget v4, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->stroke_width:I

    int-to-float v4, v4

    const/high16 v5, 0x41200000    # 10.0f

    div-float/2addr v4, v5

    sub-float v0, v3, v4

    iget v3, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->corner_radius:I

    int-to-float v3, v3

    iget v4, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->corner_radius:I

    int-to-float v4, v4

    div-float/2addr v4, v0

    add-float v2, v3, v4

    new-instance v3, Landroid/graphics/RectF;

    invoke-virtual {p0}, Landroid/Utils/BitmapHelper$GraphicalValuesData;->getRect()Landroid/graphics/Rect;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    sget-object v4, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v1, v3, v2, v2, v4}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Path$Direction;)V

    :goto_0
    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    return-object v1

    :cond_0
    new-instance v3, Landroid/graphics/RectF;

    invoke-virtual {p0}, Landroid/Utils/BitmapHelper$GraphicalValuesData;->getRect()Landroid/graphics/Rect;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    sget-object v4, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    goto :goto_0
.end method

.method public static getPathRectangleStroke(Landroid/Utils/BitmapHelper$GraphicalValuesData;)Landroid/graphics/Path;
    .locals 5

    invoke-static {p0}, Landroid/Utils/BitmapHelper;->checkSize(Landroid/Utils/BitmapHelper$GraphicalValuesData;)V

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    invoke-virtual {p0}, Landroid/Utils/BitmapHelper$GraphicalValuesData;->isCorner()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Landroid/graphics/RectF;

    invoke-virtual {p0}, Landroid/Utils/BitmapHelper$GraphicalValuesData;->getStrokeRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    iget v2, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->corner_radius:I

    int-to-float v2, v2

    iget v3, p0, Landroid/Utils/BitmapHelper$GraphicalValuesData;->corner_radius:I

    int-to-float v3, v3

    sget-object v4, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Path$Direction;)V

    :goto_0
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    return-object v0

    :cond_0
    new-instance v1, Landroid/graphics/RectF;

    invoke-virtual {p0}, Landroid/Utils/BitmapHelper$GraphicalValuesData;->getStrokeRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    sget-object v2, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    goto :goto_0
.end method

.method public static getPathStroke(Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;)Landroid/graphics/Path;
    .locals 11

    const/4 v10, 0x4

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/high16 v9, 0x40000000    # 2.0f

    invoke-static {p0}, Landroid/Utils/BitmapHelper;->checkSize(Landroid/Utils/BitmapHelper$GraphicalValuesData;)V

    iget v7, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->height:I

    int-to-float v7, v7

    div-float/2addr v7, v9

    iget v8, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->stroke_width:I

    int-to-float v8, v8

    sub-float v4, v7, v8

    iget v7, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->shape:I

    if-nez v7, :cond_0

    iget v5, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->stroke_width:I

    invoke-static {v4, v5}, Landroid/Utils/BitmapHelper;->getCircle(FI)Landroid/graphics/Path;

    move-result-object v5

    :goto_0
    return-object v5

    :cond_0
    iget v7, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->shape:I

    if-ne v7, v10, :cond_1

    iget v5, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->stroke_width:I

    int-to-float v5, v5

    iget v6, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->stroke_width:I

    int-to-float v6, v6

    iget v7, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->stroke_width:I

    int-to-float v7, v7

    add-float/2addr v7, v4

    invoke-static {v5, v6, v10, v7}, Landroid/Utils/BitmapHelper;->getShape(FFIF)Landroid/graphics/Path;

    move-result-object v5

    goto :goto_0

    :cond_1
    iget v7, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->shape:I

    const/16 v8, 0x14

    if-ne v7, v8, :cond_2

    invoke-static {p0}, Landroid/Utils/BitmapHelper;->getPathRectangleStroke(Landroid/Utils/BitmapHelper$GraphicalValuesData;)Landroid/graphics/Path;

    move-result-object v5

    goto :goto_0

    :cond_2
    iget v7, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->shape:I

    rem-int/lit8 v7, v7, 0x2

    if-nez v7, :cond_3

    move v1, v5

    :goto_1
    iget v7, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->shape:I

    const/16 v8, 0xa

    if-le v7, v8, :cond_4

    move v0, v5

    :goto_2
    if-eqz v1, :cond_6

    if-eqz v0, :cond_5

    iget v5, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->stroke_width:I

    int-to-float v2, v5

    :goto_3
    if-eqz v1, :cond_8

    if-eqz v0, :cond_7

    iget v5, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->stroke_width:I

    int-to-float v5, v5

    add-float v3, v4, v5

    :goto_4
    if-eqz v0, :cond_a

    iget v5, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->shape:I

    rem-int/lit8 v5, v5, 0xa

    :goto_5
    iget v6, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->stroke_width:I

    int-to-float v6, v6

    add-float/2addr v6, v4

    invoke-static {v2, v3, v5, v6}, Landroid/Utils/BitmapHelper;->getShape(FFIF)Landroid/graphics/Path;

    move-result-object v5

    goto :goto_0

    :cond_3
    move v1, v6

    goto :goto_1

    :cond_4
    move v0, v6

    goto :goto_2

    :cond_5
    iget v5, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->stroke_width:I

    int-to-float v5, v5

    add-float v2, v4, v5

    goto :goto_3

    :cond_6
    iget v5, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->stroke_width:I

    int-to-float v5, v5

    add-float v2, v4, v5

    goto :goto_3

    :cond_7
    iget v5, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->stroke_width:I

    int-to-float v3, v5

    goto :goto_4

    :cond_8
    if-eqz v0, :cond_9

    mul-float v5, v4, v9

    iget v6, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->stroke_width:I

    int-to-float v6, v6

    add-float v3, v5, v6

    goto :goto_4

    :cond_9
    iget v5, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->stroke_width:I

    int-to-float v3, v5

    goto :goto_4

    :cond_a
    iget v5, p0, Landroid/Utils/BitmapHelper$ShapeGraphicalValuesData;->shape:I

    goto :goto_5
.end method

.method private static getShape(FFIF)Landroid/graphics/Path;
    .locals 26

    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    move/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    move/from16 v0, p0

    float-to-double v0, v0

    move-wide/from16 v20, v0

    move/from16 v0, p3

    float-to-double v0, v0

    move-wide/from16 v22, v0

    sub-double v8, v20, v22

    move/from16 v0, p1

    float-to-double v0, v0

    move-wide/from16 v20, v0

    move/from16 v0, p3

    float-to-double v0, v0

    move-wide/from16 v22, v0

    sub-double v10, v20, v22

    const/4 v2, 0x1

    :goto_0
    move/from16 v0, p2

    if-ge v2, v0, :cond_0

    const-wide/high16 v20, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v22, -0x4010000000000000L    # -1.0

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->acos(D)D

    move-result-wide v22

    const-wide/high16 v24, 0x4000000000000000L    # 2.0

    mul-double v22, v22, v24

    move/from16 v0, p2

    int-to-double v0, v0

    move-wide/from16 v24, v0

    div-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->cos(D)D

    move-result-wide v22

    mul-double v4, v20, v22

    const-wide/high16 v20, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v22, -0x4010000000000000L    # -1.0

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->acos(D)D

    move-result-wide v22

    const-wide/high16 v24, 0x4000000000000000L    # 2.0

    mul-double v22, v22, v24

    move/from16 v0, p2

    int-to-double v0, v0

    move-wide/from16 v24, v0

    div-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sin(D)D

    move-result-wide v22

    mul-double v6, v20, v22

    mul-double v20, v8, v4

    mul-double v22, v10, v6

    sub-double v12, v20, v22

    mul-double v20, v8, v6

    mul-double v22, v10, v4

    add-double v14, v20, v22

    move-wide v8, v12

    move-wide v10, v14

    move/from16 v0, p3

    float-to-double v0, v0

    move-wide/from16 v20, v0

    add-double v16, v20, v8

    move/from16 v0, p3

    float-to-double v0, v0

    move-wide/from16 v20, v0

    add-double v18, v20, v10

    move-wide/from16 v0, v16

    double-to-float v0, v0

    move/from16 v20, v0

    move-wide/from16 v0, v18

    double-to-float v0, v0

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v3}, Landroid/graphics/Path;->close()V

    return-object v3
.end method
