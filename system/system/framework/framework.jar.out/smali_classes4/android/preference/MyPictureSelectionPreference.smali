.class public Landroid/preference/MyPictureSelectionPreference;
.super Landroid/preference/Preference;

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private fragment:Landroid/preference/PreferenceFragment;

.field private mApeX:I

.field private mApeY:I

.field private mContext:Landroid/content/Context;

.field private final mDensity:F

.field private mDrawableIcon:Landroid/graphics/drawable/Drawable;

.field private mHeight:I

.field private mIntent:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mOk:Z

.field private mThumb:Z

.field private mType:Ljava/lang/String;

.field private mView:Landroid/view/View;

.field private mWidth:I

.field private pictureSelectionHelper:Landroid/preference/PictureSelectionHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-string v0, "none"

    iput-object v0, p0, Landroid/preference/MyPictureSelectionPreference;->mType:Ljava/lang/String;

    const-string v0, "none"

    iput-object v0, p0, Landroid/preference/MyPictureSelectionPreference;->mIntent:Ljava/lang/String;

    iput-object p1, p0, Landroid/preference/MyPictureSelectionPreference;->mContext:Landroid/content/Context;

    invoke-direct {p0, p2}, Landroid/preference/MyPictureSelectionPreference;->initialize(Landroid/util/AttributeSet;)Z

    move-result v0

    iput-boolean v0, p0, Landroid/preference/MyPictureSelectionPreference;->mOk:Z

    invoke-virtual {p0, p0}, Landroid/preference/MyPictureSelectionPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    invoke-virtual {p0}, Landroid/preference/MyPictureSelectionPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Landroid/preference/MyPictureSelectionPreference;->mDensity:F

    return-void
.end method

.method static synthetic access$000(Landroid/preference/MyPictureSelectionPreference;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Landroid/preference/MyPictureSelectionPreference;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Landroid/preference/MyPictureSelectionPreference;)Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Landroid/preference/MyPictureSelectionPreference;->mDrawableIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public static getDrawableFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 1

    invoke-static {p0}, Landroid/preference/PictureSelectionHelper;->getDrawableFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private initialize(Landroid/util/AttributeSet;)Z
    .locals 6

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v5, 0x0

    const-string v3, "name"

    invoke-interface {p1, v5, v3}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iput-object v0, p0, Landroid/preference/MyPictureSelectionPreference;->mName:Ljava/lang/String;

    const-string/jumbo v3, "thumbnail"

    invoke-interface {p1, v5, v3, v2}, Landroid/util/AttributeSet;->getAttributeBooleanValue(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Landroid/preference/MyPictureSelectionPreference;->mThumb:Z

    const-string v3, "intent"

    invoke-interface {p1, v5, v3}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iput-object v0, p0, Landroid/preference/MyPictureSelectionPreference;->mIntent:Ljava/lang/String;

    :cond_2
    const-string/jumbo v3, "type"

    invoke-interface {p1, v5, v3}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "portrait"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "landscape"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "square"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/preference/MyPictureSelectionPreference;->mType:Ljava/lang/String;

    move v1, v2

    goto :goto_0

    :cond_4
    const-string v3, "aspectx"

    invoke-interface {p1, v5, v3, v1}, Landroid/util/AttributeSet;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Landroid/preference/MyPictureSelectionPreference;->mApeX:I

    const-string v3, "aspecty"

    invoke-interface {p1, v5, v3, v1}, Landroid/util/AttributeSet;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Landroid/preference/MyPictureSelectionPreference;->mApeY:I

    const-string/jumbo v3, "width"

    invoke-interface {p1, v5, v3, v1}, Landroid/util/AttributeSet;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Landroid/preference/MyPictureSelectionPreference;->mWidth:I

    const-string v3, "height"

    invoke-interface {p1, v5, v3, v1}, Landroid/util/AttributeSet;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Landroid/preference/MyPictureSelectionPreference;->mHeight:I

    iget v3, p0, Landroid/preference/MyPictureSelectionPreference;->mApeY:I

    if-eqz v3, :cond_5

    iget v3, p0, Landroid/preference/MyPictureSelectionPreference;->mApeX:I

    if-nez v3, :cond_6

    :cond_5
    iget v3, p0, Landroid/preference/MyPictureSelectionPreference;->mHeight:I

    if-eqz v3, :cond_0

    iget v3, p0, Landroid/preference/MyPictureSelectionPreference;->mWidth:I

    if-eqz v3, :cond_0

    :cond_6
    move v1, v2

    goto :goto_0
.end method

.method private sendIntent()V
    .locals 3

    iget-object v1, p0, Landroid/preference/MyPictureSelectionPreference;->mIntent:Ljava/lang/String;

    const-string v2, "none"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Landroid/preference/MyPictureSelectionPreference;->mIntent:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/preference/MyPictureSelectionPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method private static sendToast(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private setPreviewPicture()V
    .locals 9

    const/4 v8, 0x0

    const/high16 v7, 0x42200000    # 40.0f

    iget-object v5, p0, Landroid/preference/MyPictureSelectionPreference;->mView:Landroid/view/View;

    if-nez v5, :cond_1

    iget-boolean v5, p0, Landroid/preference/MyPictureSelectionPreference;->mThumb:Z

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Landroid/preference/MyPictureSelectionPreference;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v1, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iget-object v5, p0, Landroid/preference/MyPictureSelectionPreference;->mView:Landroid/view/View;

    const v6, 0x1020018

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget v5, p0, Landroid/preference/MyPictureSelectionPreference;->mDensity:F

    mul-float/2addr v5, v7

    float-to-int v5, v5

    iget v6, p0, Landroid/preference/MyPictureSelectionPreference;->mDensity:F

    mul-float/2addr v6, v7

    float-to-int v6, v6

    invoke-direct {v3, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v5, 0x11

    iput v5, v3, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    iget v5, p0, Landroid/preference/MyPictureSelectionPreference;->mDensity:F

    const/high16 v6, 0x40a00000    # 5.0f

    mul-float/2addr v5, v6

    float-to-int v2, v5

    int-to-float v5, v2

    const/high16 v6, 0x3fc00000    # 1.5f

    mul-float/2addr v5, v6

    float-to-int v5, v5

    invoke-virtual {v3, v2, v2, v5, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    if-eqz v4, :cond_0

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {v4, v8, v0}, Landroid/widget/LinearLayout;->removeViews(II)V

    :cond_2
    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v5, p0, Landroid/preference/MyPictureSelectionPreference;->mName:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Landroid/preference/MyPictureSelectionPreference;->mName:Ljava/lang/String;

    invoke-static {v5}, Landroid/preference/PictureSelectionHelper;->getDrawableFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, p0, Landroid/preference/MyPictureSelectionPreference;->mDrawableIcon:Landroid/graphics/drawable/Drawable;

    iget-object v5, p0, Landroid/preference/MyPictureSelectionPreference;->mDrawableIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v5, p0, Landroid/preference/MyPictureSelectionPreference;->mDrawableIcon:Landroid/graphics/drawable/Drawable;

    if-eqz v5, :cond_0

    new-instance v5, Landroid/preference/MyPictureSelectionPreference$1;

    invoke-direct {v5, p0}, Landroid/preference/MyPictureSelectionPreference$1;-><init>(Landroid/preference/MyPictureSelectionPreference;)V

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method protected onBindView(Landroid/view/View;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    iput-object p1, p0, Landroid/preference/MyPictureSelectionPreference;->mView:Landroid/view/View;

    invoke-direct {p0}, Landroid/preference/MyPictureSelectionPreference;->setPreviewPicture()V

    return-void
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 7

    iget-boolean v0, p0, Landroid/preference/MyPictureSelectionPreference;->mOk:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/preference/MyPictureSelectionPreference;->mContext:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/preference/MyPictureSelectionPreference;->mName:Ljava/lang/String;

    iget-object v1, p0, Landroid/preference/MyPictureSelectionPreference;->mType:Ljava/lang/String;

    iget v2, p0, Landroid/preference/MyPictureSelectionPreference;->mApeX:I

    iget v3, p0, Landroid/preference/MyPictureSelectionPreference;->mApeY:I

    iget v4, p0, Landroid/preference/MyPictureSelectionPreference;->mWidth:I

    iget v5, p0, Landroid/preference/MyPictureSelectionPreference;->mHeight:I

    move-object v6, p0

    invoke-static/range {v0 .. v6}, Landroid/preference/fakeFragment;->fakeFragment(Ljava/lang/String;Ljava/lang/String;IIIILandroid/preference/MyPictureSelectionPreference;)V

    iget-object v0, p0, Landroid/preference/MyPictureSelectionPreference;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x1020002

    new-instance v2, Landroid/preference/fakeFragment;

    invoke-direct {v2}, Landroid/preference/fakeFragment;-><init>()V

    iput-object v2, p0, Landroid/preference/MyPictureSelectionPreference;->fragment:Landroid/preference/PreferenceFragment;

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_0
    iget-object v0, p0, Landroid/preference/MyPictureSelectionPreference;->mContext:Landroid/content/Context;

    const-string v1, "Whoops - Context not checkcast activity!"

    invoke-static {v0, v1}, Landroid/preference/MyPictureSelectionPreference;->sendToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Landroid/preference/MyPictureSelectionPreference;->mContext:Landroid/content/Context;

    const-string v1, "Whoops - Attribute not correct!"

    invoke-static {v0, v1}, Landroid/preference/MyPictureSelectionPreference;->sendToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onResult(Z)V
    .locals 2

    iget-object v0, p0, Landroid/preference/MyPictureSelectionPreference;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Landroid/preference/MyPictureSelectionPreference;->fragment:Landroid/preference/PreferenceFragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    if-eqz p1, :cond_0

    invoke-direct {p0}, Landroid/preference/MyPictureSelectionPreference;->sendIntent()V

    :cond_0
    invoke-direct {p0}, Landroid/preference/MyPictureSelectionPreference;->setPreviewPicture()V

    return-void
.end method
