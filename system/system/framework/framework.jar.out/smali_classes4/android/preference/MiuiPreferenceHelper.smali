.class Landroid/preference/MiuiPreferenceHelper;
.super Ljava/lang/Object;


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field private mAttr:Landroid/util/AttributeSet;

.field private mContext:Landroid/content/Context;

.field mIntent:Ljava/lang/String;

.field mKey:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Landroid/preference/MiuiPreferenceHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/preference/MiuiPreferenceHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Landroid/preference/MiuiPreferenceHelper;->mIntent:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Landroid/preference/MiuiPreferenceHelper;->mKey:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Landroid/preference/MiuiPreferenceHelper;->mContext:Landroid/content/Context;

    iput-object p2, p0, Landroid/preference/MiuiPreferenceHelper;->mAttr:Landroid/util/AttributeSet;

    invoke-direct {p0}, Landroid/preference/MiuiPreferenceHelper;->init()V

    return-void
.end method

.method public static getTAG(Ljava/lang/Class;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private init()V
    .locals 1

    iget-object v0, p0, Landroid/preference/MiuiPreferenceHelper;->mAttr:Landroid/util/AttributeSet;

    if-eqz v0, :cond_0

    const-string v0, "key"

    invoke-virtual {p0, v0}, Landroid/preference/MiuiPreferenceHelper;->getAttributeAndroidValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/preference/MiuiPreferenceHelper;->mKey:Ljava/lang/String;

    const-string v0, "intent"

    invoke-virtual {p0, v0}, Landroid/preference/MiuiPreferenceHelper;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/preference/MiuiPreferenceHelper;->mIntent:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method static isValidateKey(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 6

    const/4 v4, 0x1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    :try_start_0
    invoke-static {v3, p1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return v4

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v3, p1}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;)J
    :try_end_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    :try_start_2
    invoke-static {v3, p1}, Landroid/provider/Settings$System;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;)F
    :try_end_2
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v2

    invoke-static {v3, p1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_0

    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method getAttributeAndroidValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    const-string v0, ""

    goto/32 :goto_7

    nop

    :goto_1
    iget-object v0, p0, Landroid/preference/MiuiPreferenceHelper;->mAttr:Landroid/util/AttributeSet;

    goto/32 :goto_6

    nop

    :goto_2
    invoke-interface {v0, v1, p1}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    goto/32 :goto_8

    nop

    :goto_4
    iget-object v0, p0, Landroid/preference/MiuiPreferenceHelper;->mAttr:Landroid/util/AttributeSet;

    goto/32 :goto_5

    nop

    :goto_5
    const-string v1, "http://schemas.android.com/apk/res/android"

    goto/32 :goto_2

    nop

    :goto_6
    if-nez v0, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_4

    nop

    :goto_7
    goto :goto_3

    :goto_8
    return-object v0

    :goto_9
    goto/32 :goto_0

    nop
.end method

.method getAttributeBool(Ljava/lang/String;Z)Z
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/preference/MiuiPreferenceHelper;->mAttr:Landroid/util/AttributeSet;

    goto/32 :goto_2

    nop

    :goto_1
    const/4 v1, 0x0

    goto/32 :goto_4

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_3

    nop

    :goto_3
    iget-object v0, p0, Landroid/preference/MiuiPreferenceHelper;->mAttr:Landroid/util/AttributeSet;

    goto/32 :goto_1

    nop

    :goto_4
    invoke-interface {v0, v1, p1, p2}, Landroid/util/AttributeSet;->getAttributeBooleanValue(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result p2

    :goto_5
    goto/32 :goto_6

    nop

    :goto_6
    return p2
.end method

.method getAttributeInt(Ljava/lang/String;I)I
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_3

    nop

    :goto_1
    invoke-interface {v0, v1, p1, p2}, Landroid/util/AttributeSet;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result p2

    :goto_2
    goto/32 :goto_6

    nop

    :goto_3
    iget-object v0, p0, Landroid/preference/MiuiPreferenceHelper;->mAttr:Landroid/util/AttributeSet;

    goto/32 :goto_4

    nop

    :goto_4
    const/4 v1, 0x0

    goto/32 :goto_1

    nop

    :goto_5
    iget-object v0, p0, Landroid/preference/MiuiPreferenceHelper;->mAttr:Landroid/util/AttributeSet;

    goto/32 :goto_0

    nop

    :goto_6
    return p2
.end method

.method getAttributeValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    goto/32 :goto_9

    nop

    :goto_0
    invoke-interface {v0, v1, p1}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    goto/32 :goto_5

    nop

    :goto_2
    goto :goto_1

    :goto_3
    iget-object v0, p0, Landroid/preference/MiuiPreferenceHelper;->mAttr:Landroid/util/AttributeSet;

    goto/32 :goto_7

    nop

    :goto_4
    const-string v0, ""

    goto/32 :goto_2

    nop

    :goto_5
    return-object v0

    :goto_6
    goto/32 :goto_4

    nop

    :goto_7
    const/4 v1, 0x0

    goto/32 :goto_0

    nop

    :goto_8
    if-nez v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_3

    nop

    :goto_9
    iget-object v0, p0, Landroid/preference/MiuiPreferenceHelper;->mAttr:Landroid/util/AttributeSet;

    goto/32 :goto_8

    nop
.end method

.method getBool()Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/preference/MiuiPreferenceHelper;->mKey:Ljava/lang/String;

    goto/32 :goto_2

    nop

    :goto_1
    return v0

    :goto_2
    invoke-static {v0}, Landroid/preference/SettingsEliteHelper;->getBoolofSettings(Ljava/lang/String;)Z

    move-result v0

    goto/32 :goto_1

    nop
.end method

.method getBool(I)Z
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    iget-object v0, p0, Landroid/preference/MiuiPreferenceHelper;->mKey:Ljava/lang/String;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-static {v0, p1}, Landroid/preference/SettingsEliteHelper;->getBoolofSettings(Ljava/lang/String;I)Z

    move-result v0

    goto/32 :goto_0

    nop
.end method

.method getBool(Ljava/lang/String;)Z
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    const/4 v1, 0x1

    :goto_1
    goto/32 :goto_6

    nop

    :goto_2
    const/4 v1, 0x0

    goto/32 :goto_8

    nop

    :goto_3
    invoke-static {v1}, Landroid/preference/SettingsEliteHelper;->getStringofSettings(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_4
    if-nez v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_a

    nop

    :goto_5
    iget-object v1, p0, Landroid/preference/MiuiPreferenceHelper;->mKey:Ljava/lang/String;

    goto/32 :goto_3

    nop

    :goto_6
    return v1

    :goto_7
    goto/32 :goto_2

    nop

    :goto_8
    goto :goto_1

    :goto_9
    if-nez v1, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_0

    nop

    :goto_a
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto/32 :goto_9

    nop
.end method

.method getBool(Z)Z
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    const/4 v0, 0x1

    :goto_1
    goto/32 :goto_4

    nop

    :goto_2
    goto :goto_1

    :goto_3
    iget-object v1, p0, Landroid/preference/MiuiPreferenceHelper;->mKey:Ljava/lang/String;

    goto/32 :goto_8

    nop

    :goto_4
    invoke-static {v1, v0}, Landroid/preference/SettingsEliteHelper;->getBoolofSettings(Ljava/lang/String;I)Z

    move-result v0

    goto/32 :goto_5

    nop

    :goto_5
    return v0

    :goto_6
    goto/32 :goto_7

    nop

    :goto_7
    const/4 v0, 0x0

    goto/32 :goto_2

    nop

    :goto_8
    if-nez p1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_0

    nop
.end method

.method getInt()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/preference/MiuiPreferenceHelper;->mKey:Ljava/lang/String;

    goto/32 :goto_2

    nop

    :goto_1
    return v0

    :goto_2
    invoke-static {v0}, Landroid/preference/SettingsEliteHelper;->getIntofSettings(Ljava/lang/String;)I

    move-result v0

    goto/32 :goto_1

    nop
.end method

.method getInt(I)I
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-static {v0, p1}, Landroid/preference/SettingsEliteHelper;->getIntofSettings(Ljava/lang/String;I)I

    move-result v0

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Landroid/preference/MiuiPreferenceHelper;->mKey:Ljava/lang/String;

    goto/32 :goto_0

    nop

    :goto_2
    return v0
.end method

.method getStr()Ljava/lang/String;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/preference/MiuiPreferenceHelper;->mKey:Ljava/lang/String;

    goto/32 :goto_2

    nop

    :goto_1
    return-object v0

    :goto_2
    invoke-static {v0}, Landroid/preference/SettingsEliteHelper;->getStringofSettings(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_1

    nop
.end method

.method getStr(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/preference/MiuiPreferenceHelper;->mKey:Ljava/lang/String;

    goto/32 :goto_2

    nop

    :goto_1
    return-object v0

    :goto_2
    invoke-static {v0, p1}, Landroid/preference/SettingsEliteHelper;->getStringofSettings(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_1

    nop
.end method

.method isValidateKey()Z
    .locals 6

    goto/32 :goto_5

    nop

    :goto_0
    goto :goto_b

    :catch_0
    move-exception v1

    :try_start_0
    iget-object v5, p0, Landroid/preference/MiuiPreferenceHelper;->mKey:Ljava/lang/String;

    invoke-static {v3, v5}, Landroid/provider/Settings$System;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;)F
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    goto/32 :goto_8

    nop

    :goto_1
    const/4 v4, 0x0

    goto/32 :goto_3

    nop

    :goto_2
    return v4

    :catch_1
    move-exception v0

    :try_start_1
    iget-object v5, p0, Landroid/preference/MiuiPreferenceHelper;->mKey:Ljava/lang/String;

    invoke-static {v3, v5}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;)J
    :try_end_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/32 :goto_0

    nop

    :goto_3
    goto :goto_b

    :goto_4
    invoke-static {v3, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_6

    nop

    :goto_5
    const/4 v4, 0x1

    goto/32 :goto_9

    nop

    :goto_6
    if-eqz v5, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_1

    nop

    :goto_7
    iget-object v5, p0, Landroid/preference/MiuiPreferenceHelper;->mKey:Ljava/lang/String;

    goto/32 :goto_4

    nop

    :goto_8
    goto :goto_b

    :catch_2
    move-exception v2

    goto/32 :goto_7

    nop

    :goto_9
    iget-object v5, p0, Landroid/preference/MiuiPreferenceHelper;->mContext:Landroid/content/Context;

    goto/32 :goto_a

    nop

    :goto_a
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    :try_start_2
    iget-object v5, p0, Landroid/preference/MiuiPreferenceHelper;->mKey:Ljava/lang/String;

    invoke-static {v3, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    :goto_b
    goto/32 :goto_2

    nop
.end method

.method putInt(I)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-static {v0, p1}, Landroid/preference/SettingsEliteHelper;->putIntinSettings(Ljava/lang/String;I)V

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Landroid/preference/MiuiPreferenceHelper;->mKey:Ljava/lang/String;

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method putStr(Ljava/lang/String;)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/preference/MiuiPreferenceHelper;->mKey:Ljava/lang/String;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-static {v0, p1}, Landroid/preference/SettingsEliteHelper;->putStringinSettings(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method

.method sendIntent()V
    .locals 3

    goto/32 :goto_9

    nop

    :goto_0
    return-void

    :goto_1
    invoke-static {v0, v1}, Landroid/Utils/Utils;->sendBroadcastAsUser(Landroid/content/Context;Landroid/content/Intent;)V

    :goto_2
    goto/32 :goto_0

    nop

    :goto_3
    iget-object v0, p0, Landroid/preference/MiuiPreferenceHelper;->mContext:Landroid/content/Context;

    goto/32 :goto_5

    nop

    :goto_4
    iget-object v0, p0, Landroid/preference/MiuiPreferenceHelper;->mIntent:Ljava/lang/String;

    goto/32 :goto_b

    nop

    :goto_5
    new-instance v1, Landroid/content/Intent;

    goto/32 :goto_7

    nop

    :goto_6
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto/32 :goto_1

    nop

    :goto_7
    iget-object v2, p0, Landroid/preference/MiuiPreferenceHelper;->mIntent:Ljava/lang/String;

    goto/32 :goto_6

    nop

    :goto_8
    if-nez v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_4

    nop

    :goto_9
    iget-object v0, p0, Landroid/preference/MiuiPreferenceHelper;->mIntent:Ljava/lang/String;

    goto/32 :goto_8

    nop

    :goto_a
    if-eqz v0, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_3

    nop

    :goto_b
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    goto/32 :goto_a

    nop
.end method

.method setInt(I)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/preference/MiuiPreferenceHelper;->mKey:Ljava/lang/String;

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    invoke-static {v0, p1}, Landroid/preference/SettingsEliteHelper;->putIntinSettings(Ljava/lang/String;I)V

    goto/32 :goto_1

    nop
.end method
