.class public Landroid/preference/MiuiPictureSelectionPreference;
.super Landroid/preference/Preference;

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;
.implements Ljava/io/Serializable;
.implements Landroid/preference/MiuiPictureSelection/fakeFragment$OnImageCreatedListener;


# instance fields
.field private Helper:Landroid/preference/MiuiPreferenceHelper;

.field private fragment:Landroid/app/Fragment;

.field private isNeedCopyImage:Z

.field private mAspectX:I

.field private mAspectY:I

.field private mContext:Landroid/content/Context;

.field private final mDensity:F

.field private mDrawableIcon:Landroid/graphics/drawable/Drawable;

.field private mHeight:I

.field private mName:Ljava/lang/String;

.field private mOk:Z

.field private mThumb:Z

.field private mType:Ljava/lang/String;

.field private mView:Landroid/view/View;

.field private mWidth:I

.field private pictureSelectionHelper:Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-string v0, "none"

    iput-object v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->mType:Ljava/lang/String;

    iput-object p1, p0, Landroid/preference/MiuiPictureSelectionPreference;->mContext:Landroid/content/Context;

    invoke-direct {p0, p2}, Landroid/preference/MiuiPictureSelectionPreference;->initialize(Landroid/util/AttributeSet;)Z

    move-result v0

    iput-boolean v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->mOk:Z

    invoke-virtual {p0, p0}, Landroid/preference/MiuiPictureSelectionPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    invoke-virtual {p0}, Landroid/preference/MiuiPictureSelectionPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->mDensity:F

    return-void
.end method

.method static synthetic access$000(Landroid/preference/MiuiPictureSelectionPreference;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Landroid/preference/MiuiPictureSelectionPreference;)Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->mDrawableIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private copyToTheme()V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v4, 0x3

    new-array v2, v4, [Ljava/lang/String;

    const-string v4, "mount -o rw,remount /"

    aput-object v4, v2, v8

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cp -p -f "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->PATH:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Landroid/preference/MiuiPictureSelectionPreference;->mName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".png /system/media/theme/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Landroid/preference/MiuiPictureSelectionPreference;->mName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".png"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v7

    const/4 v4, 0x2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "chmod 0666 /system/media/theme/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Landroid/preference/MiuiPictureSelectionPreference;->mName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".png"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-static {v2, v7, v7}, Landroid/Utils/Shell;->execCommand([Ljava/lang/String;ZZ)Landroid/Utils/Shell$CommandResult;

    move-result-object v1

    iget v4, v1, Landroid/Utils/Shell$CommandResult;->result:I

    if-nez v4, :cond_0

    const-string/jumbo v0, "Copied successfully"

    iget-object v4, p0, Landroid/preference/MiuiPictureSelectionPreference;->mContext:Landroid/content/Context;

    invoke-static {v4, v0, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "copyToTheme: res = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Landroid/Utils/Shell$CommandResult;->result:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " error = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v1, Landroid/Utils/Shell$CommandResult;->errorMsg:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "   succes = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v1, Landroid/Utils/Shell$CommandResult;->successMsg:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getDrawableFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 1

    invoke-static {p0}, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->getDrawableFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private initialize(Landroid/util/AttributeSet;)Z
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    new-instance v0, Landroid/preference/MiuiPreferenceHelper;

    iget-object v3, p0, Landroid/preference/MiuiPictureSelectionPreference;->mContext:Landroid/content/Context;

    invoke-direct {v0, v3, p1}, Landroid/preference/MiuiPreferenceHelper;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->Helper:Landroid/preference/MiuiPreferenceHelper;

    iget-object v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->Helper:Landroid/preference/MiuiPreferenceHelper;

    const-string v3, "name"

    invoke-virtual {v0, v3}, Landroid/preference/MiuiPreferenceHelper;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->mName:Ljava/lang/String;

    if-nez v0, :cond_0

    :goto_0
    return v1

    :cond_0
    iget-object v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->Helper:Landroid/preference/MiuiPreferenceHelper;

    const-string v3, "copytotheme"

    invoke-virtual {v0, v3, v1}, Landroid/preference/MiuiPreferenceHelper;->getAttributeBool(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->isNeedCopyImage:Z

    iget-object v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->Helper:Landroid/preference/MiuiPreferenceHelper;

    const-string/jumbo v3, "thumbnail"

    invoke-virtual {v0, v3, v2}, Landroid/preference/MiuiPreferenceHelper;->getAttributeBool(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->mThumb:Z

    iget-object v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->Helper:Landroid/preference/MiuiPreferenceHelper;

    const-string/jumbo v3, "type"

    invoke-virtual {v0, v3}, Landroid/preference/MiuiPreferenceHelper;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->mType:Ljava/lang/String;

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->mType:Ljava/lang/String;

    iget-object v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->mType:Ljava/lang/String;

    const-string v3, "portrait"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->mType:Ljava/lang/String;

    const-string v3, "landscape"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->mType:Ljava/lang/String;

    const-string v3, "square"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    iget-object v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->mType:Ljava/lang/String;

    goto :goto_1

    :cond_3
    iget-object v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->Helper:Landroid/preference/MiuiPreferenceHelper;

    const-string v3, "aspectx"

    invoke-virtual {v0, v3, v1}, Landroid/preference/MiuiPreferenceHelper;->getAttributeInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->mAspectX:I

    iget-object v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->Helper:Landroid/preference/MiuiPreferenceHelper;

    const-string v3, "aspecty"

    invoke-virtual {v0, v3, v1}, Landroid/preference/MiuiPreferenceHelper;->getAttributeInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->mAspectY:I

    iget-object v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->Helper:Landroid/preference/MiuiPreferenceHelper;

    const-string/jumbo v3, "width"

    invoke-virtual {v0, v3, v1}, Landroid/preference/MiuiPreferenceHelper;->getAttributeInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->mWidth:I

    iget-object v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->Helper:Landroid/preference/MiuiPreferenceHelper;

    const-string v3, "height"

    invoke-virtual {v0, v3, v1}, Landroid/preference/MiuiPreferenceHelper;->getAttributeInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->mHeight:I

    iget v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->mAspectY:I

    if-eqz v0, :cond_4

    iget v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->mAspectX:I

    if-nez v0, :cond_5

    :cond_4
    iget v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->mHeight:I

    if-eqz v0, :cond_6

    iget v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->mWidth:I

    if-eqz v0, :cond_6

    :cond_5
    move v0, v2

    :goto_2
    move v1, v0

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method private static sendToast(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private setPreviewPicture()V
    .locals 9

    const/4 v8, 0x0

    const/high16 v7, 0x42200000    # 40.0f

    iget-object v5, p0, Landroid/preference/MiuiPictureSelectionPreference;->mView:Landroid/view/View;

    if-nez v5, :cond_1

    iget-boolean v5, p0, Landroid/preference/MiuiPictureSelectionPreference;->mThumb:Z

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Landroid/preference/MiuiPictureSelectionPreference;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v1, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iget-object v5, p0, Landroid/preference/MiuiPictureSelectionPreference;->mView:Landroid/view/View;

    const v6, 0x1020018

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget v5, p0, Landroid/preference/MiuiPictureSelectionPreference;->mDensity:F

    mul-float/2addr v5, v7

    float-to-int v5, v5

    iget v6, p0, Landroid/preference/MiuiPictureSelectionPreference;->mDensity:F

    mul-float/2addr v6, v7

    float-to-int v6, v6

    invoke-direct {v3, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v5, 0x11

    iput v5, v3, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    iget v5, p0, Landroid/preference/MiuiPictureSelectionPreference;->mDensity:F

    const/high16 v6, 0x40a00000    # 5.0f

    mul-float/2addr v5, v6

    float-to-int v2, v5

    int-to-float v5, v2

    const/high16 v6, 0x3fc00000    # 1.5f

    mul-float/2addr v5, v6

    float-to-int v5, v5

    invoke-virtual {v3, v2, v2, v5, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    if-eqz v4, :cond_0

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {v4, v8, v0}, Landroid/widget/LinearLayout;->removeViews(II)V

    :cond_2
    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v5, p0, Landroid/preference/MiuiPictureSelectionPreference;->mName:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Landroid/preference/MiuiPictureSelectionPreference;->mName:Ljava/lang/String;

    invoke-static {v5}, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->getDrawableFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, p0, Landroid/preference/MiuiPictureSelectionPreference;->mDrawableIcon:Landroid/graphics/drawable/Drawable;

    iget-object v5, p0, Landroid/preference/MiuiPictureSelectionPreference;->mDrawableIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v5, p0, Landroid/preference/MiuiPictureSelectionPreference;->mDrawableIcon:Landroid/graphics/drawable/Drawable;

    if-eqz v5, :cond_0

    new-instance v5, Landroid/preference/MiuiPictureSelectionPreference$1;

    invoke-direct {v5, p0}, Landroid/preference/MiuiPictureSelectionPreference$1;-><init>(Landroid/preference/MiuiPictureSelectionPreference;)V

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method protected onBindView(Landroid/view/View;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    iput-object p1, p0, Landroid/preference/MiuiPictureSelectionPreference;->mView:Landroid/view/View;

    invoke-direct {p0}, Landroid/preference/MiuiPictureSelectionPreference;->setPreviewPicture()V

    return-void
.end method

.method public onImageCreated(Z)V
    .locals 2

    iget-object v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Landroid/preference/MiuiPictureSelectionPreference;->fragment:Landroid/app/Fragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    if-eqz p1, :cond_0

    iget-object v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->Helper:Landroid/preference/MiuiPreferenceHelper;

    invoke-virtual {v0}, Landroid/preference/MiuiPreferenceHelper;->sendIntent()V

    iget-boolean v0, p0, Landroid/preference/MiuiPictureSelectionPreference;->isNeedCopyImage:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Landroid/preference/MiuiPictureSelectionPreference;->copyToTheme()V

    :cond_0
    invoke-direct {p0}, Landroid/preference/MiuiPictureSelectionPreference;->setPreviewPicture()V

    return-void
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4

    iget-boolean v1, p0, Landroid/preference/MiuiPictureSelectionPreference;->mOk:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/preference/MiuiPictureSelectionPreference;->mContext:Landroid/content/Context;

    instance-of v1, v1, Landroid/app/Activity;

    if-eqz v1, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "name"

    iget-object v2, p0, Landroid/preference/MiuiPictureSelectionPreference;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "type"

    iget-object v2, p0, Landroid/preference/MiuiPictureSelectionPreference;->mType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "aspectX"

    iget v2, p0, Landroid/preference/MiuiPictureSelectionPreference;->mAspectX:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "aspectY"

    iget v2, p0, Landroid/preference/MiuiPictureSelectionPreference;->mAspectY:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "width"

    iget v2, p0, Landroid/preference/MiuiPictureSelectionPreference;->mWidth:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "height"

    iget v2, p0, Landroid/preference/MiuiPictureSelectionPreference;->mHeight:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "listener"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    new-instance v1, Landroid/preference/MiuiPictureSelection/fakeFragment;

    invoke-direct {v1}, Landroid/preference/MiuiPictureSelection/fakeFragment;-><init>()V

    iput-object v1, p0, Landroid/preference/MiuiPictureSelectionPreference;->fragment:Landroid/app/Fragment;

    iget-object v1, p0, Landroid/preference/MiuiPictureSelectionPreference;->fragment:Landroid/app/Fragment;

    invoke-virtual {v1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    iget-object v1, p0, Landroid/preference/MiuiPictureSelectionPreference;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x1020002

    iget-object v3, p0, Landroid/preference/MiuiPictureSelectionPreference;->fragment:Landroid/app/Fragment;

    invoke-virtual {v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    :goto_0
    const/4 v1, 0x1

    return v1

    :cond_0
    iget-object v1, p0, Landroid/preference/MiuiPictureSelectionPreference;->mContext:Landroid/content/Context;

    const-string v2, "Whoops - Context not checkcast activity!"

    invoke-static {v1, v2}, Landroid/preference/MiuiPictureSelectionPreference;->sendToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Landroid/preference/MiuiPictureSelectionPreference;->mContext:Landroid/content/Context;

    const-string v2, "Whoops - Attribute not correct!"

    invoke-static {v1, v2}, Landroid/preference/MiuiPictureSelectionPreference;->sendToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method
