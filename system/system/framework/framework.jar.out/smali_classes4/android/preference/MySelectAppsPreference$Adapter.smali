.class public Landroid/preference/MySelectAppsPreference$Adapter;
.super Landroid/widget/ArrayAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/preference/MySelectAppsPreference$Adapter$MyViewHolder;
    }
.end annotation


# instance fields
.field ind:I

.field final synthetic this$0:Landroid/preference/MySelectAppsPreference;


# direct methods
.method public constructor <init>(Landroid/preference/MySelectAppsPreference;ILjava/util/ArrayList;)V
    .locals 2

    invoke-virtual {p1}, Landroid/preference/MySelectAppsPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object p1, p0, Landroid/preference/MySelectAppsPreference$Adapter;->this$0:Landroid/preference/MySelectAppsPreference;

    iput p2, p0, Landroid/preference/MySelectAppsPreference$Adapter;->ind:I

    return-void
.end method


# virtual methods
.method public getItem(I)Landroid/preference/MySelectAppsPreference$AppData;
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/MySelectAppsPreference$AppData;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    if-nez p2, :cond_9

    iget-object v2, p0, Landroid/preference/MySelectAppsPreference$Adapter;->this$0:Landroid/preference/MySelectAppsPreference;

    invoke-virtual {v2}, Landroid/preference/MySelectAppsPreference;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, p1}, Landroid/preference/MySelectAppsPreference$Adapter;->getItem(I)Landroid/preference/MySelectAppsPreference$AppData;

    move-result-object v0

    iget v0, v0, Landroid/preference/MySelectAppsPreference$AppData;->checked:I

    const/4 v3, 0x1

    if-gez v0, :cond_0

    const/4 v3, 0x0

    :cond_0
    new-instance v0, Landroid/preference/MySelectAppsPreference$Adapter$MyViewHolder;

    invoke-direct {v0, v2, v1, v3}, Landroid/preference/MySelectAppsPreference$Adapter$MyViewHolder;-><init>(Landroid/content/Context;Landroid/view/View;Z)V

    move-object p2, v1

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    invoke-virtual {p0, p1}, Landroid/preference/MySelectAppsPreference$Adapter;->getItem(I)Landroid/preference/MySelectAppsPreference$AppData;

    move-result-object v1

    iget-object v2, p0, Landroid/preference/MySelectAppsPreference$Adapter;->this$0:Landroid/preference/MySelectAppsPreference;

    iget v2, v2, Landroid/preference/MySelectAppsPreference;->mExtra:I

    add-int/lit8 v2, v2, -0x2

    const/4 v5, 0x0

    if-nez v2, :cond_1

    const/4 v5, 0x1

    :cond_1
    iget v3, v1, Landroid/preference/MySelectAppsPreference$AppData;->checked:I

    const v4, 0x8

    if-ltz v3, :cond_a

    const v4, 0x0

    goto/16 :goto_3

    :cond_2
    iget v3, v1, Landroid/preference/MySelectAppsPreference$AppData;->checked:I

    iget-object v2, v0, Landroid/preference/MySelectAppsPreference$Adapter$MyViewHolder;->cbCheck:Landroid/widget/CheckBox;

    if-eqz v2, :cond_4

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1

    :cond_3
    const v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_4
    :goto_1
    iget-object v3, v0, Landroid/preference/MySelectAppsPreference$Adapter$MyViewHolder;->ivIcon:Landroid/widget/ImageView;

    iget-object v2, v1, Landroid/preference/MySelectAppsPreference$AppData;->icon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v2, v0, Landroid/preference/MySelectAppsPreference$Adapter$MyViewHolder;->tvTitle:Landroid/widget/TextView;

    const v3, 0x1

    const v0, 0x10

    if-eqz v4, :cond_5

    const v0, 0xf

    if-nez v5, :cond_5

    const v3, 0x0

    const v0, 0xe

    :cond_5
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    int-to-float v0, v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, v1, Landroid/preference/MySelectAppsPreference$AppData;->name:Ljava/lang/String;

    if-eqz v3, :cond_6

    iget-object v3, p0, Landroid/preference/MySelectAppsPreference$Adapter;->this$0:Landroid/preference/MySelectAppsPreference;

    iget v3, v3, Landroid/preference/MySelectAppsPreference;->mExtra:I

    add-int/lit8 v3, v3, -0x4

    if-nez v3, :cond_6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "<br><small>"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    iget-object v0, v1, Landroid/preference/MySelectAppsPreference$AppData;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, "</small>"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    :cond_6
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    if-eqz v4, :cond_f

    if-nez v5, :cond_f

    const/4 v2, 0x0

    if-eqz p1, :cond_7

    const/16 v2, 0x20

    :cond_7
    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    const/4 v2, 0x5

    if-eqz p1, :cond_8

    const/16 v2, 0xf

    :cond_8
    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    :goto_2
    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    const/16 v1, 0x13

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    return-object p2

    :cond_9
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/MySelectAppsPreference$Adapter$MyViewHolder;

    goto/16 :goto_0

    :cond_a
    :goto_3
    iget-object v2, v0, Landroid/preference/MySelectAppsPreference$Adapter$MyViewHolder;->cbCheck:Landroid/widget/CheckBox;

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    if-eqz v4, :cond_b

    const/4 v2, -0x1

    if-eq v3, v2, :cond_e

    const/4 v4, 0x0

    goto :goto_4

    :cond_b
    if-eqz v5, :cond_c

    const v4, 0x8

    :cond_c
    :goto_4
    iget-object v2, v0, Landroid/preference/MySelectAppsPreference$Adapter$MyViewHolder;->ivIcon:Landroid/widget/ImageView;

    const v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    const/16 v3, 0x84

    if-eqz v4, :cond_d

    const/16 v3, 0x42

    :cond_d
    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_5

    :cond_e
    iget-object v2, v0, Landroid/preference/MySelectAppsPreference$Adapter$MyViewHolder;->ivIcon:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_5
    if-nez v5, :cond_2

    if-eqz v4, :cond_2

    goto/16 :goto_1

    :cond_f
    const/4 v2, 0x0

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    goto :goto_2
.end method
