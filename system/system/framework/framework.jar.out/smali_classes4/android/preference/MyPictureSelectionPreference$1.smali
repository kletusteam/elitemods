.class Landroid/preference/MyPictureSelectionPreference$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/preference/MyPictureSelectionPreference;->setPreviewPicture()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/preference/MyPictureSelectionPreference;


# direct methods
.method constructor <init>(Landroid/preference/MyPictureSelectionPreference;)V
    .locals 0

    iput-object p1, p0, Landroid/preference/MyPictureSelectionPreference$1;->this$0:Landroid/preference/MyPictureSelectionPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    iget-object v3, p0, Landroid/preference/MyPictureSelectionPreference$1;->this$0:Landroid/preference/MyPictureSelectionPreference;

    invoke-static {v3}, Landroid/preference/MyPictureSelectionPreference;->access$000(Landroid/preference/MyPictureSelectionPreference;)Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "window"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/WindowManager;

    invoke-interface {v9}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    new-instance v11, Landroid/util/DisplayMetrics;

    invoke-direct {v11}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {v6, v11}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    iget v8, v11, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v10, v11, Landroid/util/DisplayMetrics;->widthPixels:I

    if-le v8, v10, :cond_1

    move v1, v10

    :goto_0
    mul-int/lit8 v3, v1, 0x2

    div-int/lit8 v1, v3, 0x3

    const/4 v2, -0x1

    iget-object v3, p0, Landroid/preference/MyPictureSelectionPreference$1;->this$0:Landroid/preference/MyPictureSelectionPreference;

    invoke-static {v3}, Landroid/preference/MyPictureSelectionPreference;->access$100(Landroid/preference/MyPictureSelectionPreference;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    iget-object v4, p0, Landroid/preference/MyPictureSelectionPreference$1;->this$0:Landroid/preference/MyPictureSelectionPreference;

    invoke-static {v4}, Landroid/preference/MyPictureSelectionPreference;->access$100(Landroid/preference/MyPictureSelectionPreference;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    if-ge v3, v4, :cond_0

    if-ge v8, v10, :cond_0

    mul-int/lit8 v3, v8, 0x2

    div-int/lit8 v2, v3, 0x3

    const/4 v1, -0x1

    :cond_0
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x3e8

    const/16 v4, 0x102

    const/4 v5, -0x3

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    const/high16 v3, 0x3f400000    # 0.75f

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    const/16 v3, 0x11

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    new-instance v7, Landroid/widget/ImageView;

    iget-object v3, p0, Landroid/preference/MyPictureSelectionPreference$1;->this$0:Landroid/preference/MyPictureSelectionPreference;

    invoke-static {v3}, Landroid/preference/MyPictureSelectionPreference;->access$000(Landroid/preference/MyPictureSelectionPreference;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v7, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Landroid/preference/MyPictureSelectionPreference$1;->this$0:Landroid/preference/MyPictureSelectionPreference;

    invoke-static {v3}, Landroid/preference/MyPictureSelectionPreference;->access$100(Landroid/preference/MyPictureSelectionPreference;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v7, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    new-instance v3, Landroid/preference/MyPictureSelectionPreference$1$1;

    invoke-direct {v3, p0, v9, v7}, Landroid/preference/MyPictureSelectionPreference$1$1;-><init>(Landroid/preference/MyPictureSelectionPreference$1;Landroid/view/WindowManager;Landroid/widget/ImageView;)V

    invoke-virtual {v7, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-interface {v9, v7, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_1
    move v1, v8

    goto :goto_0
.end method
