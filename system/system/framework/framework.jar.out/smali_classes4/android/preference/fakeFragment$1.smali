.class Landroid/preference/fakeFragment$1;
.super Landroid/preference/PictureSelectionHelper;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/preference/fakeFragment;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/preference/fakeFragment;


# direct methods
.method constructor <init>(Landroid/preference/fakeFragment;)V
    .locals 0

    iput-object p1, p0, Landroid/preference/fakeFragment$1;->this$0:Landroid/preference/fakeFragment;

    invoke-direct {p0}, Landroid/preference/PictureSelectionHelper;-><init>()V

    return-void
.end method


# virtual methods
.method goCropImage(Landroid/content/Intent;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Landroid/preference/fakeFragment$1;->this$0:Landroid/preference/fakeFragment;

    const/4 v1, 0x2

    invoke-virtual {v0, p1, v1}, Landroid/preference/fakeFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_0

    nop

    :goto_0
    goto :goto_2

    :catch_0
    move-exception v0

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {p0}, Landroid/preference/fakeFragment$1;->cropNotFound()V

    :goto_2
    goto/32 :goto_3

    nop

    :goto_3
    return-void
.end method

.method goSelectImage(Landroid/content/Intent;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Landroid/preference/fakeFragment$1;->this$0:Landroid/preference/fakeFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/preference/fakeFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {p0}, Landroid/preference/fakeFragment$1;->cameraNotFound()V

    :goto_1
    goto/32 :goto_3

    nop

    :goto_2
    goto :goto_1

    :catch_0
    move-exception v0

    goto/32 :goto_0

    nop

    :goto_3
    return-void
.end method

.method onResult(Z)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {v0, p1}, Landroid/preference/MyPictureSelectionPreference;->onResult(Z)V

    goto/32 :goto_0

    nop

    :goto_2
    invoke-static {}, Landroid/preference/fakeFragment;->access$000()Landroid/preference/MyPictureSelectionPreference;

    move-result-object v0

    goto/32 :goto_1

    nop
.end method
