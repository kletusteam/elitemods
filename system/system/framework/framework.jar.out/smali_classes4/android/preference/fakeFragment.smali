.class public Landroid/preference/fakeFragment;
.super Landroid/preference/PreferenceFragment;


# static fields
.field private static mApeX:I

.field private static mApeY:I

.field private static mHeight:I

.field private static mMyPictureSelectionPreference:Landroid/preference/MyPictureSelectionPreference;

.field private static mName:Ljava/lang/String;

.field private static mType:Ljava/lang/String;

.field private static mWidth:I


# instance fields
.field private pictureSelectionHelper:Landroid/preference/PictureSelectionHelper;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000()Landroid/preference/MyPictureSelectionPreference;
    .locals 1

    sget-object v0, Landroid/preference/fakeFragment;->mMyPictureSelectionPreference:Landroid/preference/MyPictureSelectionPreference;

    return-object v0
.end method

.method public static fakeFragment(Ljava/lang/String;Ljava/lang/String;IIIILandroid/preference/MyPictureSelectionPreference;)V
    .locals 0

    sput-object p0, Landroid/preference/fakeFragment;->mName:Ljava/lang/String;

    sput-object p1, Landroid/preference/fakeFragment;->mType:Ljava/lang/String;

    sput p2, Landroid/preference/fakeFragment;->mApeX:I

    sput p3, Landroid/preference/fakeFragment;->mApeY:I

    sput p4, Landroid/preference/fakeFragment;->mWidth:I

    sput p5, Landroid/preference/fakeFragment;->mHeight:I

    sput-object p6, Landroid/preference/fakeFragment;->mMyPictureSelectionPreference:Landroid/preference/MyPictureSelectionPreference;

    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    iget-object v0, p0, Landroid/preference/fakeFragment;->pictureSelectionHelper:Landroid/preference/PictureSelectionHelper;

    invoke-virtual {v0, p1, p2, p3}, Landroid/preference/PictureSelectionHelper;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Landroid/preference/fakeFragment$1;

    invoke-direct {v0, p0}, Landroid/preference/fakeFragment$1;-><init>(Landroid/preference/fakeFragment;)V

    iput-object v0, p0, Landroid/preference/fakeFragment;->pictureSelectionHelper:Landroid/preference/PictureSelectionHelper;

    const/4 v0, 0x3

    sget-object v1, Landroid/preference/fakeFragment;->mType:Ljava/lang/String;

    const-string v2, "portrait"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    sget-object v1, Landroid/preference/fakeFragment;->mType:Ljava/lang/String;

    const-string v2, "landscape"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    sget-object v1, Landroid/preference/fakeFragment;->mType:Ljava/lang/String;

    const-string v2, "square"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x2

    :cond_2
    iget-object v1, p0, Landroid/preference/fakeFragment;->pictureSelectionHelper:Landroid/preference/PictureSelectionHelper;

    invoke-virtual {p0}, Landroid/preference/fakeFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Landroid/preference/fakeFragment;->mName:Ljava/lang/String;

    sget v5, Landroid/preference/fakeFragment;->mApeX:I

    sget v6, Landroid/preference/fakeFragment;->mApeY:I

    sget v7, Landroid/preference/fakeFragment;->mWidth:I

    sget v8, Landroid/preference/fakeFragment;->mHeight:I

    move v4, v0

    invoke-virtual/range {v1 .. v8}, Landroid/preference/PictureSelectionHelper;->cropImage(Landroid/content/Context;Ljava/lang/String;IIIII)V

    return-void
.end method
