.class public Landroid/preference/MiuiPictureSelection/fakeFragment;
.super Landroid/app/Fragment;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/preference/MiuiPictureSelection/fakeFragment$OnImageCreatedListener;
    }
.end annotation


# instance fields
.field private mAspectX:I

.field private mAspectY:I

.field private mHeight:I

.field private mListener:Landroid/preference/MiuiPictureSelection/fakeFragment$OnImageCreatedListener;

.field private mName:Ljava/lang/String;

.field private mType:Ljava/lang/String;

.field private mWidth:I

.field private pictureSelectionHelper:Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/preference/MiuiPictureSelection/fakeFragment;)Landroid/preference/MiuiPictureSelection/fakeFragment$OnImageCreatedListener;
    .locals 1

    iget-object v0, p0, Landroid/preference/MiuiPictureSelection/fakeFragment;->mListener:Landroid/preference/MiuiPictureSelection/fakeFragment$OnImageCreatedListener;

    return-object v0
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    iget-object v0, p0, Landroid/preference/MiuiPictureSelection/fakeFragment;->pictureSelectionHelper:Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;

    invoke-virtual {v0, p1, p2, p3}, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9

    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Landroid/preference/MiuiPictureSelection/fakeFragment$1;

    invoke-direct {v0, p0}, Landroid/preference/MiuiPictureSelection/fakeFragment$1;-><init>(Landroid/preference/MiuiPictureSelection/fakeFragment;)V

    iput-object v0, p0, Landroid/preference/MiuiPictureSelection/fakeFragment;->pictureSelectionHelper:Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;

    const/4 v0, 0x3

    iget-object v1, p0, Landroid/preference/MiuiPictureSelection/fakeFragment;->mType:Ljava/lang/String;

    const-string v2, "portrait"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    iget-object v1, p0, Landroid/preference/MiuiPictureSelection/fakeFragment;->mType:Ljava/lang/String;

    const-string v2, "landscape"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    iget-object v1, p0, Landroid/preference/MiuiPictureSelection/fakeFragment;->mType:Ljava/lang/String;

    const-string v2, "square"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x2

    :cond_2
    iget-object v1, p0, Landroid/preference/MiuiPictureSelection/fakeFragment;->pictureSelectionHelper:Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;

    invoke-virtual {p0}, Landroid/preference/MiuiPictureSelection/fakeFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Landroid/preference/MiuiPictureSelection/fakeFragment;->mName:Ljava/lang/String;

    iget v5, p0, Landroid/preference/MiuiPictureSelection/fakeFragment;->mAspectX:I

    iget v6, p0, Landroid/preference/MiuiPictureSelection/fakeFragment;->mAspectY:I

    iget v7, p0, Landroid/preference/MiuiPictureSelection/fakeFragment;->mWidth:I

    iget v8, p0, Landroid/preference/MiuiPictureSelection/fakeFragment;->mHeight:I

    move v4, v0

    invoke-virtual/range {v1 .. v8}, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->cropImage(Landroid/content/Context;Ljava/lang/String;IIIII)V

    return-void
.end method

.method public setArguments(Landroid/os/Bundle;)V
    .locals 1

    const-string v0, "name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/preference/MiuiPictureSelection/fakeFragment;->mName:Ljava/lang/String;

    const-string/jumbo v0, "type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/preference/MiuiPictureSelection/fakeFragment;->mType:Ljava/lang/String;

    const-string v0, "aspectX"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Landroid/preference/MiuiPictureSelection/fakeFragment;->mAspectX:I

    const-string v0, "aspectY"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Landroid/preference/MiuiPictureSelection/fakeFragment;->mAspectY:I

    const-string/jumbo v0, "width"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Landroid/preference/MiuiPictureSelection/fakeFragment;->mWidth:I

    const-string v0, "height"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Landroid/preference/MiuiPictureSelection/fakeFragment;->mHeight:I

    const-string v0, "listener"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Landroid/preference/MiuiPictureSelection/fakeFragment$OnImageCreatedListener;

    iput-object v0, p0, Landroid/preference/MiuiPictureSelection/fakeFragment;->mListener:Landroid/preference/MiuiPictureSelection/fakeFragment$OnImageCreatedListener;

    return-void
.end method
