.class public abstract Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/preference/MiuiPictureSelection/PictureSelectionHelper$MyFileNameFilter;
    }
.end annotation


# static fields
.field public static final CAMERA_CAPTURE:I = 0x1

.field public static PATH:Ljava/lang/String; = null

.field public static final PIC_CROP:I = 0x2

.field public static SMALL_PATH:Ljava/lang/String;


# instance fields
.field private mApeX:I

.field private mApeY:I

.field private mContext:Landroid/content/Context;

.field private mHeight:I

.field private mName:Ljava/lang/String;

.field private mPickUri:Landroid/net/Uri;

.field private mType:I

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "/storage/emulated/0/data/myImage/"

    sput-object v0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->PATH:Ljava/lang/String;

    const-string v0, "/storage/emulated/0/data"

    sput-object v0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->SMALL_PATH:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private MyCrop()V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.camera.action.CROP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mPickUri:Landroid/net/Uri;

    const-string v2, "image/*"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    iget v1, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mApeX:I

    const-string v2, "aspectX"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget v1, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mApeY:I

    const-string v2, "aspectY"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v1, 0x1

    const-string v2, "scale"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "scaleUpIfNeeded"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v1}, Landroid/graphics/Bitmap$CompressFormat;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "outputFormat"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->PATH:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v3, "temp.png"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "output"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->goCropImage(Landroid/content/Intent;)V

    return-void
.end method

.method private MyPick()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string v2, "android.intent.action.PICK"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "return-data"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->goSelectImage(Landroid/content/Intent;)V

    return-void
.end method

.method private checkDir()Z
    .locals 5

    new-instance v0, Ljava/io/File;

    sget-object v1, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->SMALL_PATH:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    move-result v1

    if-nez v1, :cond_0

    return v2

    :cond_0
    new-instance v1, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->SMALL_PATH:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "/myImage"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    move-result v1

    if-nez v1, :cond_1

    return v2

    :cond_1
    const/4 v1, 0x1

    return v1
.end method

.method private static checkFiles(Ljava/lang/String;)Z
    .locals 9

    const/4 v0, 0x0

    new-instance v1, Ljava/io/File;

    sget-object v2, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->PATH:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper$MyFileNameFilter;

    const/4 v3, 0x0

    const-string v4, ".png"

    invoke-direct {v2, v4, v3}, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper$MyFileNameFilter;-><init>(Ljava/lang/String;Landroid/preference/MiuiPictureSelection/PictureSelectionHelper$1;)V

    invoke-virtual {v1, v2}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_2

    array-length v3, v2

    if-eqz v3, :cond_2

    new-instance v3, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->PATH:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v0, 0x1

    :cond_0
    array-length v4, v2

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v4, :cond_2

    aget-object v6, v2, v5

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    if-nez v0, :cond_1

    invoke-virtual {v6, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    const/4 v0, 0x1

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_2
    return v0
.end method

.method private static getCommonFactor(II)I
    .locals 1

    if-nez p1, :cond_0

    move v0, p0

    goto :goto_0

    :cond_0
    rem-int v0, p0, p1

    invoke-static {p1, v0}, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->getCommonFactor(II)I

    move-result v0

    :goto_0
    return v0
.end method

.method public static getDrawableFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 3

    const/4 v0, 0x0

    invoke-static {p0}, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->checkFiles(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->PATH:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".png"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/drawable/Drawable;->createFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private setMetrics(Landroid/content/Context;IIIII)Z
    .locals 7

    const-string/jumbo v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    if-eqz v0, :cond_7

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {v1, v2}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    iget v3, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v3, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mHeight:I

    iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v3, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mWidth:I

    iget v3, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mWidth:I

    iget v4, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mHeight:I

    if-le v3, v4, :cond_0

    iget v4, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mHeight:I

    iput v3, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mHeight:I

    iput v4, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mWidth:I

    :cond_0
    const/4 v3, 0x3

    const/4 v4, 0x1

    if-eq p2, v3, :cond_4

    if-eqz p2, :cond_3

    if-eq p2, v4, :cond_2

    const/4 v3, 0x2

    if-eq p2, v3, :cond_1

    goto :goto_0

    :cond_1
    iput v4, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mApeX:I

    iput v4, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mApeY:I

    iget v3, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mWidth:I

    iput v3, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mHeight:I

    goto :goto_0

    :cond_2
    iget v3, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mWidth:I

    iget v5, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mHeight:I

    invoke-static {v3, v5}, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->getCommonFactor(II)I

    move-result v3

    iget v5, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mWidth:I

    div-int v6, v5, v3

    iput v6, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mApeY:I

    iget v6, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mHeight:I

    div-int/2addr v6, v3

    iput v6, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mApeX:I

    iget v3, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mHeight:I

    iput v5, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mHeight:I

    iput v3, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mWidth:I

    goto :goto_0

    :cond_3
    iget v3, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mWidth:I

    iget v5, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mHeight:I

    invoke-static {v3, v5}, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->getCommonFactor(II)I

    move-result v3

    iget v5, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mWidth:I

    div-int/2addr v5, v3

    iput v5, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mApeX:I

    iget v5, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mHeight:I

    div-int/2addr v5, v3

    iput v5, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mApeY:I

    nop

    :goto_0
    return v4

    :cond_4
    if-eqz p3, :cond_6

    if-eqz p4, :cond_6

    iput p3, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mApeX:I

    iput p4, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mApeY:I

    if-eqz p5, :cond_5

    mul-int/lit8 v3, p5, 0x2

    iput v3, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mWidth:I

    :cond_5
    iget v3, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mWidth:I

    iget v5, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mApeY:I

    mul-int/2addr v3, v5

    iget v5, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mApeX:I

    div-int/2addr v3, v5

    iput v3, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mHeight:I

    return v4

    :cond_6
    if-eqz p5, :cond_7

    if-eqz p6, :cond_7

    mul-int/lit8 v3, p5, 0x2

    iput v3, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mWidth:I

    mul-int/lit8 v3, p6, 0x2

    iput v3, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mHeight:I

    iget v3, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mWidth:I

    iget v5, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mHeight:I

    invoke-static {v3, v5}, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->getCommonFactor(II)I

    move-result v3

    iget v5, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mWidth:I

    div-int/2addr v5, v3

    iput v5, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mApeX:I

    iget v5, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mHeight:I

    div-int/2addr v5, v3

    iput v5, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mApeY:I

    return v4

    :cond_7
    const/4 v1, 0x0

    return v1
.end method


# virtual methods
.method public cameraNotFound()V
    .locals 3

    const-string v0, "Whoops - your device doesn\'t support capturing images!"

    iget-object v1, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public cropImage(Landroid/content/Context;Ljava/lang/String;IIIII)V
    .locals 12

    move-object v7, p0

    move-object v8, p1

    iput-object v8, v7, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->checkDir()Z

    move-result v0

    const/4 v9, 0x0

    if-eqz v0, :cond_1

    move-object v10, p2

    iput-object v10, v7, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mName:Ljava/lang/String;

    move v11, p3

    iput v11, v7, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mType:I

    move-object v0, p0

    move-object v1, p1

    move v2, p3

    move/from16 v3, p4

    move/from16 v4, p5

    move/from16 v5, p6

    move/from16 v6, p7

    invoke-direct/range {v0 .. v6}, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->setMetrics(Landroid/content/Context;IIIII)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->MyPick()V

    goto :goto_0

    :cond_0
    const-string v0, "Whoops - wrong image size!"

    iget-object v1, v7, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mContext:Landroid/content/Context;

    invoke-static {v1, v0, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0, v9}, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->onResult(Z)V

    goto :goto_0

    :cond_1
    move-object v10, p2

    move v11, p3

    const-string v0, "Whoops - your device doesn\'t give access to memory!"

    iget-object v1, v7, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mContext:Landroid/content/Context;

    invoke-static {v1, v0, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0, v9}, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->onResult(Z)V

    :goto_0
    return-void
.end method

.method public cropNotFound()V
    .locals 3

    const-string v0, "Whoops - your device doesn\'t support the crop action!"

    iget-object v1, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method abstract goCropImage(Landroid/content/Intent;)V
.end method

.method abstract goSelectImage(Landroid/content/Intent;)V
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 11

    const-string/jumbo v0, "temp.png"

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, -0x1

    if-ne p2, v3, :cond_2

    const/4 v3, 0x1

    if-ne p1, v3, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mPickUri:Landroid/net/Uri;

    invoke-direct {p0}, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->MyCrop()V

    goto/16 :goto_1

    :cond_0
    const/4 v4, 0x2

    if-ne p1, v4, :cond_3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->PATH:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ".png"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mName:Ljava/lang/String;

    invoke-static {v7}, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->checkFiles(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    :cond_1
    :try_start_0
    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->PATH:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v8

    iget v9, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mWidth:I

    div-int/2addr v9, v4

    iget v10, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mHeight:I

    div-int/2addr v10, v4

    invoke-static {v8, v9, v10, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v4

    sget-object v8, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v9, 0x55

    invoke-virtual {v4, v8, v9, v7}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual {v7}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v7}, Ljava/io/OutputStream;->close()V

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    invoke-virtual {p0, v3}, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->onResult(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    const-string v4, "Whoops - your device doesn\'t save file!"

    iget-object v7, p0, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->mContext:Landroid/content/Context;

    invoke-static {v7, v4, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0, v2}, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->onResult(Z)V

    :goto_0
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->PATH:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object v0, v2

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_1

    :cond_2
    invoke-virtual {p0, v2}, Landroid/preference/MiuiPictureSelection/PictureSelectionHelper;->onResult(Z)V

    :cond_3
    :goto_1
    return-void
.end method

.method abstract onResult(Z)V
.end method
