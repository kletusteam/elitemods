.class Landroid/preference/MySelectAppsPreference$AppDataComparator;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/preference/MySelectAppsPreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AppDataComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Landroid/preference/MySelectAppsPreference$AppData;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Landroid/preference/MySelectAppsPreference$AppData;Landroid/preference/MySelectAppsPreference$AppData;)I
    .locals 2

    iget-object v1, p1, Landroid/preference/MySelectAppsPreference$AppData;->packageName:Ljava/lang/String;

    iget-object v0, p2, Landroid/preference/MySelectAppsPreference$AppData;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Landroid/preference/MySelectAppsPreference$AppData;

    check-cast p2, Landroid/preference/MySelectAppsPreference$AppData;

    invoke-virtual {p0, p1, p2}, Landroid/preference/MySelectAppsPreference$AppDataComparator;->compare(Landroid/preference/MySelectAppsPreference$AppData;Landroid/preference/MySelectAppsPreference$AppData;)I

    move-result v0

    return v0
.end method
