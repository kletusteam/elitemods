.class Landroid/preference/MySelectAppsPreference$LoadApps;
.super Landroid/os/AsyncTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/preference/MySelectAppsPreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadApps"
.end annotation


# instance fields
.field connectivityManager:Landroid/net/ConnectivityManager;

.field final synthetic this$0:Landroid/preference/MySelectAppsPreference;

.field wifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method synthetic constructor <init>(Landroid/preference/MySelectAppsPreference;)V
    .locals 0

    iput-object p1, p0, Landroid/preference/MySelectAppsPreference$LoadApps;->this$0:Landroid/preference/MySelectAppsPreference;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method static synthetic access$1(Landroid/preference/MySelectAppsPreference$LoadApps;Ljava/util/ArrayList;)V
    .locals 4

    invoke-direct {p0, p1}, Landroid/preference/MySelectAppsPreference$LoadApps;->sortWifi(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1

    iget-object v0, p0, Landroid/preference/MySelectAppsPreference$LoadApps;->this$0:Landroid/preference/MySelectAppsPreference;

    invoke-static {v0}, Landroid/preference/MySelectAppsPreference;->access$1(Landroid/preference/MySelectAppsPreference;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v0, p0, Landroid/preference/MySelectAppsPreference$LoadApps;->this$0:Landroid/preference/MySelectAppsPreference;

    invoke-static {v0}, Landroid/preference/MySelectAppsPreference;->access$2(Landroid/preference/MySelectAppsPreference;)Landroid/widget/LinearLayout;

    move-result-object v1

    const v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Landroid/preference/MySelectAppsPreference$LoadApps;->this$0:Landroid/preference/MySelectAppsPreference;

    invoke-static {v0}, Landroid/preference/MySelectAppsPreference;->access$1(Landroid/preference/MySelectAppsPreference;)Landroid/widget/ListView;

    move-result-object v2

    const/4 v3, -0x1

    new-instance v1, Landroid/preference/MySelectAppsPreference$Adapter;

    invoke-direct {v1, v0, v3, p1}, Landroid/preference/MySelectAppsPreference$Adapter;-><init>(Landroid/preference/MySelectAppsPreference;ILjava/util/ArrayList;)V

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    check-cast v1, Landroid/widget/ArrayAdapter;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    return-void

    :cond_0
    check-cast v2, Landroid/widget/ArrayAdapter;

    invoke-virtual {v2}, Landroid/widget/ArrayAdapter;->clear()V

    invoke-virtual {v2, p1}, Landroid/widget/ArrayAdapter;->addAll(Ljava/util/Collection;)V

    invoke-virtual {v2}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method private getActivitiesForPackage(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 6

    const/4 p0, 0x0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "\u0003"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "\u0001"

    invoke-virtual {p2, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v1, 0x0

    array-length v2, v3

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v4, v3, v1

    invoke-virtual {v4, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "\u0003"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-object v0
.end method

.method private getAllActivities(Landroid/content/pm/PackageManager;)Ljava/util/ArrayList;
    .locals 8

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    const/4 v2, 0x0

    invoke-direct {v4, v3, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v3, "android.intent.category.LAUNCHER"

    invoke-virtual {v4, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1, v4, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    new-instance v7, Landroid/preference/MySelectAppsPreference$AppDataActivityInfoComparator;

    invoke-direct {v7}, Landroid/preference/MySelectAppsPreference$AppDataActivityInfoComparator;-><init>()V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ResolveInfo;

    iget-object v6, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {p1, v6, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    iget-object v2, v6, Landroid/content/pm/PackageInfo;->activities:[Landroid/content/pm/ActivityInfo;

    if-eqz v2, :cond_0

    array-length v4, v2

    if-eqz v4, :cond_0

    invoke-static {v2, v7}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    new-instance v4, Landroid/preference/MySelectAppsPreference$AppData;

    invoke-direct {v4}, Landroid/preference/MySelectAppsPreference$AppData;-><init>()V

    iget-object v5, v6, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    iput-object v5, v4, Landroid/preference/MySelectAppsPreference$AppData;->packageName:Ljava/lang/String;

    iget-object v0, v6, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v0, p1}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Landroid/preference/MySelectAppsPreference$AppData;->name:Ljava/lang/String;

    invoke-virtual {v0, p1}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, v4, Landroid/preference/MySelectAppsPreference$AppData;->icon:Landroid/graphics/drawable/Drawable;

    const/4 v5, -0x3

    iput v5, v4, Landroid/preference/MySelectAppsPreference$AppData;->checked:I

    iput-object v2, v4, Landroid/preference/MySelectAppsPreference$AppData;->act:[Landroid/content/pm/ActivityInfo;

    array-length v2, v2

    new-array v2, v2, [Z

    iput-object v2, v4, Landroid/preference/MySelectAppsPreference$AppData;->actChecked:[Z

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method private getAllApps(Landroid/content/pm/PackageManager;)Ljava/util/ArrayList;
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    const/4 v2, 0x0

    invoke-direct {v4, v3, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v3, "android.intent.category.LAUNCHER"

    invoke-virtual {v4, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1, v4, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    new-instance v4, Landroid/preference/MySelectAppsPreference$AppData;

    invoke-direct {v4}, Landroid/preference/MySelectAppsPreference$AppData;-><init>()V

    iget-object v2, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iput-object v2, v4, Landroid/preference/MySelectAppsPreference$AppData;->packageName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Landroid/preference/MySelectAppsPreference$AppData;->name:Ljava/lang/String;

    invoke-virtual {v0, p1}, Landroid/content/pm/ActivityInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v4, Landroid/preference/MySelectAppsPreference$AppData;->icon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private getAllInstalledApps(Landroid/content/pm/PackageManager;)Ljava/util/ArrayList;
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/16 v0, 0x280

    invoke-virtual {p1, v0}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    new-instance v4, Landroid/preference/MySelectAppsPreference$AppData;

    invoke-direct {v4}, Landroid/preference/MySelectAppsPreference$AppData;-><init>()V

    iget-object v2, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iput-object v2, v4, Landroid/preference/MySelectAppsPreference$AppData;->packageName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Landroid/preference/MySelectAppsPreference$AppData;->name:Ljava/lang/String;

    invoke-virtual {v0, p1}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v4, Landroid/preference/MySelectAppsPreference$AppData;->icon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private getAllOpenWithApps(Landroid/content/pm/PackageManager;)Ljava/util/ArrayList;
    .locals 7

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v4, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "content://*/*"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "*/*"

    invoke-virtual {v4, v2, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "UserXP"

    const/4 v0, 0x1

    invoke-virtual {v4, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const v0, 0x20200

    invoke-virtual {p1, v4, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Landroid/preference/MySelectAppsPreference$AppData;

    invoke-direct {v4}, Landroid/preference/MySelectAppsPreference$AppData;-><init>()V

    iput-object v2, v4, Landroid/preference/MySelectAppsPreference$AppData;->packageName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Landroid/preference/MySelectAppsPreference$AppData;->name:Ljava/lang/String;

    invoke-virtual {v0, p1}, Landroid/content/pm/ActivityInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v4, Landroid/preference/MySelectAppsPreference$AppData;->icon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method private getAllShareApps(Landroid/content/pm/PackageManager;)Ljava/util/ArrayList;
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const-string v3, "android.intent.action.SEND"

    invoke-virtual {v4, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "*/*"

    invoke-virtual {v4, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "UserXP"

    const/4 v0, 0x1

    invoke-virtual {v4, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const v0, 0x20200

    invoke-virtual {p1, v4, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    new-instance v4, Landroid/preference/MySelectAppsPreference$AppData;

    invoke-direct {v4}, Landroid/preference/MySelectAppsPreference$AppData;-><init>()V

    iget-object v2, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iput-object v2, v4, Landroid/preference/MySelectAppsPreference$AppData;->packageName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Landroid/preference/MySelectAppsPreference$AppData;->name:Ljava/lang/String;

    invoke-virtual {v0, p1}, Landroid/content/pm/ActivityInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v4, Landroid/preference/MySelectAppsPreference$AppData;->icon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private getAllWifi()Ljava/util/ArrayList;
    .locals 5

    iget-object v1, p0, Landroid/preference/MySelectAppsPreference$LoadApps;->this$0:Landroid/preference/MySelectAppsPreference;

    invoke-virtual {v1}, Landroid/preference/MySelectAppsPreference;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string/jumbo v2, "wifi"

    invoke-virtual {v4, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Landroid/preference/MySelectAppsPreference$LoadApps;->wifiManager:Landroid/net/wifi/WifiManager;

    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    const-string v3, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v3, Landroid/preference/MySelectAppsPreference$LoadApps$Receiver;

    invoke-direct {v3, p0}, Landroid/preference/MySelectAppsPreference$LoadApps$Receiver;-><init>(Landroid/preference/MySelectAppsPreference$LoadApps;)V

    iput-object v3, v1, Landroid/preference/MySelectAppsPreference;->wifiReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v4, v3, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    return-object v1
.end method

.method private getPackagesWithActivities(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7

    const/4 v6, 0x0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "\u0001"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v1, 0x0

    array-length v2, v3

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v4, v3, v1

    const-string v5, "\u0003"

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-ltz v5, :cond_0

    invoke-virtual {v4, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private getSignalNullIcon()Landroid/graphics/drawable/Drawable;
    .locals 5

    const/4 v0, 0x0

    const-string/jumbo v1, "stat_sys_wifi_signal_null"

    const-string v2, "drawable"

    const-string v3, "android"

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_0

    const-string/jumbo v1, "stat_notify_wifi_in_range"

    const-string v3, "android"

    invoke-virtual {v4, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const v1, -0xde690d

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    :cond_1
    return-object v0
.end method

.method private join(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 3

    const-string v1, ""

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_1
    return-object v1
.end method

.method private removeActivityFromKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v5, "\u0003"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v5, "\u0001"

    invoke-virtual {p3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_0
    if-ge v1, v2, :cond_4

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v5, "\u0003"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    const/4 v3, 0x1

    :goto_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v3, v5, :cond_3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v5, 0x1

    if-gt v3, v5, :cond_0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gtz v3, :cond_1

    const-string v0, ""

    goto :goto_2

    :cond_0
    const-string v3, "\u0003"

    invoke-direct {p0, v3, v4}, Landroid/preference/MySelectAppsPreference$LoadApps;->join(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_1
    const-string v3, "\u0001"

    invoke-direct {p0, v3, v0}, Landroid/preference/MySelectAppsPreference$LoadApps;->join(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    return-object v0

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    return-object p3
.end method

.method private removePackageWithActivitiesFromKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    const-string v0, ""

    if-nez v1, :cond_2

    const-string v2, "\u0003"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v2, "\u0001"

    invoke-virtual {p2, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    const-string v3, "\u0001"

    invoke-direct {p0, v3, v0}, Landroid/preference/MySelectAppsPreference$LoadApps;->join(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move-object v0, p2

    :cond_2
    :goto_1
    return-object v0
.end method

.method private sortActivities(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 22

    move-object/from16 v5, p0

    iget-object v0, v5, Landroid/preference/MySelectAppsPreference$LoadApps;->this$0:Landroid/preference/MySelectAppsPreference;

    new-instance v6, Landroid/preference/MySelectAppsPreference$AppDataComparator;

    invoke-direct {v6}, Landroid/preference/MySelectAppsPreference$AppDataComparator;-><init>()V

    move-object/from16 v11, p1

    invoke-static {v11, v6}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    new-instance v9, Landroid/preference/MySelectAppsPreference$AppDataActivityInfoComparator;

    invoke-direct {v9}, Landroid/preference/MySelectAppsPreference$AppDataActivityInfoComparator;-><init>()V

    invoke-static {v0}, Landroid/preference/MySelectAppsPreference;->access$3(Landroid/preference/MySelectAppsPreference;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Landroid/preference/MySelectAppsPreference$LoadApps;->getPackagesWithActivities(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    move/from16 v19, v0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ltz v0, :cond_7

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    new-instance v5, Landroid/preference/MySelectAppsPreference$AppData;

    invoke-direct {v5}, Landroid/preference/MySelectAppsPreference$AppData;-><init>()V

    iput-object v4, v5, Landroid/preference/MySelectAppsPreference$AppData;->packageName:Ljava/lang/String;

    move-object/from16 v1, p1

    invoke-static {v1, v5, v6}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v1

    if-ltz v1, :cond_6

    move/from16 v21, v1

    const/4 v11, 0x0

    move-object/from16 v2, p1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/preference/MySelectAppsPreference$AppData;

    move-object/from16 v13, p0

    invoke-direct {v13, v4, v8}, Landroid/preference/MySelectAppsPreference$LoadApps;->getActivitiesForPackage(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v13

    iget-object v5, v2, Landroid/preference/MySelectAppsPreference$AppData;->act:[Landroid/content/pm/ActivityInfo;

    move/from16 v16, v0

    move-object/from16 v17, v3

    move-object/from16 v18, v6

    new-instance v6, Landroid/content/pm/ActivityInfo;

    invoke-direct {v6}, Landroid/content/pm/ActivityInfo;-><init>()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const/4 v15, 0x0

    :goto_1
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-ge v15, v12, :cond_2

    invoke-virtual {v13, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v6, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-static {v5, v6, v9}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v12

    if-ltz v12, :cond_1

    move/from16 v20, v12

    if-nez v11, :cond_0

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v12

    invoke-virtual {v7, v12}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    new-instance v12, Landroid/preference/MySelectAppsPreference$AppData;

    invoke-direct {v12}, Landroid/preference/MySelectAppsPreference$AppData;-><init>()V

    const/4 v1, -0x4

    iput v1, v12, Landroid/preference/MySelectAppsPreference$AppData;->checked:I

    iget-object v14, v2, Landroid/preference/MySelectAppsPreference$AppData;->packageName:Ljava/lang/String;

    iput-object v14, v12, Landroid/preference/MySelectAppsPreference$AppData;->packageName:Ljava/lang/String;

    iget-object v14, v2, Landroid/preference/MySelectAppsPreference$AppData;->name:Ljava/lang/String;

    iput-object v14, v12, Landroid/preference/MySelectAppsPreference$AppData;->name:Ljava/lang/String;

    iget-object v14, v2, Landroid/preference/MySelectAppsPreference$AppData;->icon:Landroid/graphics/drawable/Drawable;

    iput-object v14, v12, Landroid/preference/MySelectAppsPreference$AppData;->icon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v10, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v11, v11, 0x1

    :cond_0
    move/from16 v12, v20

    aget-object v1, v5, v12

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v12, v20

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    add-int/lit8 v11, v11, 0x1

    :goto_2
    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    :cond_1
    move-object/from16 v12, p0

    invoke-direct {v12, v4, v1, v8}, Landroid/preference/MySelectAppsPreference$LoadApps;->removeActivityFromKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/16 v19, 0x1

    goto :goto_2

    :cond_2
    if-eqz v11, :cond_4

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/preference/MySelectAppsPreference$AppData;

    invoke-static {v3, v9}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-virtual {v3}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v14

    array-length v3, v14

    const-class v0, [Landroid/content/pm/ActivityInfo;

    invoke-static {v14, v3, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;ILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v14

    check-cast v14, [Landroid/content/pm/ActivityInfo;

    iput-object v14, v1, Landroid/preference/MySelectAppsPreference$AppData;->act:[Landroid/content/pm/ActivityInfo;

    array-length v14, v14

    new-array v14, v14, [Z

    const/4 v3, 0x1

    invoke-static {v14, v3}, Ljava/util/Arrays;->fill([ZZ)V

    iput-object v14, v1, Landroid/preference/MySelectAppsPreference$AppData;->actChecked:[Z

    invoke-virtual {v10, v6, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_3

    move/from16 v6, v21

    move-object/from16 v1, p1

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_3

    :cond_3
    invoke-virtual {v7}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v14

    array-length v3, v14

    const-class v0, [Landroid/content/pm/ActivityInfo;

    invoke-static {v14, v3, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;ILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v14

    check-cast v14, [Landroid/content/pm/ActivityInfo;

    iput-object v14, v2, Landroid/preference/MySelectAppsPreference$AppData;->act:[Landroid/content/pm/ActivityInfo;

    array-length v14, v14

    new-array v14, v14, [Z

    iput-object v14, v2, Landroid/preference/MySelectAppsPreference$AppData;->actChecked:[Z

    move/from16 v1, v21

    move-object/from16 v14, p1

    invoke-virtual {v14, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_4
    :goto_3
    move/from16 v0, v16

    move-object/from16 v3, v17

    move-object/from16 v6, v18

    :cond_5
    :goto_4
    add-int/lit8 v0, v0, -0x1

    goto/16 :goto_0

    :cond_6
    move-object/from16 v2, p0

    invoke-direct {v2, v4, v8}, Landroid/preference/MySelectAppsPreference$LoadApps;->removePackageWithActivitiesFromKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/16 v19, 0x1

    goto :goto_4

    :cond_7
    if-eqz v19, :cond_8

    move-object/from16 v1, p0

    iget-object v0, v1, Landroid/preference/MySelectAppsPreference$LoadApps;->this$0:Landroid/preference/MySelectAppsPreference;

    invoke-virtual {v0, v8}, Landroid/preference/MySelectAppsPreference;->setString(Ljava/lang/String;)V

    :cond_8
    move-object/from16 v11, p1

    invoke-static {v11}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-static {v10}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    const/4 v8, 0x1

    :goto_5
    if-ltz v7, :cond_a

    move/from16 v9, v7

    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/preference/MySelectAppsPreference$AppData;

    iget-object v2, v2, Landroid/preference/MySelectAppsPreference$AppData;->act:[Landroid/content/pm/ActivityInfo;

    const/4 v0, 0x0

    array-length v6, v2

    :goto_6
    if-ge v0, v6, :cond_9

    aget-object v1, v2, v0

    new-instance v4, Landroid/preference/MySelectAppsPreference$AppData;

    invoke-direct {v4}, Landroid/preference/MySelectAppsPreference$AppData;-><init>()V

    iget-object v3, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iput-object v3, v4, Landroid/preference/MySelectAppsPreference$AppData;->packageName:Ljava/lang/String;

    iget-object v3, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    iput-object v3, v4, Landroid/preference/MySelectAppsPreference$AppData;->name:Ljava/lang/String;

    iput v8, v4, Landroid/preference/MySelectAppsPreference$AppData;->checked:I

    add-int/lit8 v9, v9, 0x1

    invoke-virtual {v10, v9, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_9
    add-int/lit8 v7, v7, -0x1

    goto :goto_5

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/preference/MySelectAppsPreference$LoadApps;->this$0:Landroid/preference/MySelectAppsPreference;

    iget-object v4, v0, Landroid/preference/MySelectAppsPreference;->mUncheckedTitle:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v5, :cond_b

    if-eqz v3, :cond_c

    :cond_b
    const/4 v2, -0x1

    new-instance v5, Landroid/preference/MySelectAppsPreference$AppData;

    invoke-direct {v5}, Landroid/preference/MySelectAppsPreference$AppData;-><init>()V

    iput-object v4, v5, Landroid/preference/MySelectAppsPreference$AppData;->name:Ljava/lang/String;

    iput-object v4, v5, Landroid/preference/MySelectAppsPreference$AppData;->packageName:Ljava/lang/String;

    iput v2, v5, Landroid/preference/MySelectAppsPreference$AppData;->checked:I

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v3, :cond_c

    iget-object v4, v0, Landroid/preference/MySelectAppsPreference;->mCheckedTitle:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_c

    const/4 v2, -0x1

    new-instance v5, Landroid/preference/MySelectAppsPreference$AppData;

    invoke-direct {v5}, Landroid/preference/MySelectAppsPreference$AppData;-><init>()V

    iput-object v4, v5, Landroid/preference/MySelectAppsPreference$AppData;->name:Ljava/lang/String;

    iput-object v4, v5, Landroid/preference/MySelectAppsPreference$AppData;->packageName:Ljava/lang/String;

    iput v2, v5, Landroid/preference/MySelectAppsPreference$AppData;->checked:I

    const/4 v0, 0x0

    invoke-virtual {v10, v0, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :cond_c
    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    return-object v10
.end method

.method private sortApps(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 8

    iget-object v0, p0, Landroid/preference/MySelectAppsPreference$LoadApps;->this$0:Landroid/preference/MySelectAppsPreference;

    new-instance v6, Landroid/preference/MySelectAppsPreference$AppDataComparator;

    invoke-direct {v6}, Landroid/preference/MySelectAppsPreference$AppDataComparator;-><init>()V

    invoke-static {p1, v6}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-static {v0}, Landroid/preference/MySelectAppsPreference;->access$3(Landroid/preference/MySelectAppsPreference;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "\u0001"

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    const/4 v0, 0x0

    new-instance v5, Landroid/preference/MySelectAppsPreference$AppData;

    invoke-direct {v5}, Landroid/preference/MySelectAppsPreference$AppData;-><init>()V

    const/4 v7, 0x0

    :goto_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_2

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iput-object v4, v5, Landroid/preference/MySelectAppsPreference$AppData;->packageName:Ljava/lang/String;

    invoke-static {p1, v5, v6}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v1

    if-ltz v1, :cond_1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/preference/MySelectAppsPreference$AppData;

    const/4 v4, 0x1

    iput v4, v2, Landroid/preference/MySelectAppsPreference$AppData;->checked:I

    invoke-virtual {p1, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    const/4 v7, 0x1

    goto :goto_0

    :cond_2
    if-eqz v7, :cond_5

    const/4 v0, 0x0

    const-string v5, "\u0001"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    :goto_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_4

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v0, :cond_3

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Landroid/preference/MySelectAppsPreference$LoadApps;->this$0:Landroid/preference/MySelectAppsPreference;

    invoke-virtual {v0, v3}, Landroid/preference/MySelectAppsPreference;->setString(Ljava/lang/String;)V

    :cond_5
    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    const/4 v0, 0x0

    const/4 v3, 0x0

    :goto_2
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_7

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/preference/MySelectAppsPreference$AppData;

    iget v2, v4, Landroid/preference/MySelectAppsPreference$AppData;->checked:I

    if-eqz v2, :cond_6

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    invoke-virtual {p1, v3, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    add-int/lit8 v3, v3, 0x1

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    iget-object v0, p0, Landroid/preference/MySelectAppsPreference$LoadApps;->this$0:Landroid/preference/MySelectAppsPreference;

    iget-object v4, v0, Landroid/preference/MySelectAppsPreference;->mUncheckedTitle:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_8

    if-eqz v3, :cond_9

    :cond_8
    const/4 v2, -0x1

    new-instance v5, Landroid/preference/MySelectAppsPreference$AppData;

    invoke-direct {v5}, Landroid/preference/MySelectAppsPreference$AppData;-><init>()V

    iput-object v4, v5, Landroid/preference/MySelectAppsPreference$AppData;->name:Ljava/lang/String;

    iput-object v4, v5, Landroid/preference/MySelectAppsPreference$AppData;->packageName:Ljava/lang/String;

    iput v2, v5, Landroid/preference/MySelectAppsPreference$AppData;->checked:I

    invoke-virtual {p1, v3, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    if-eqz v3, :cond_9

    iget-object v4, v0, Landroid/preference/MySelectAppsPreference;->mCheckedTitle:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_9

    const/4 v2, -0x1

    new-instance v5, Landroid/preference/MySelectAppsPreference$AppData;

    invoke-direct {v5}, Landroid/preference/MySelectAppsPreference$AppData;-><init>()V

    iput-object v4, v5, Landroid/preference/MySelectAppsPreference$AppData;->name:Ljava/lang/String;

    iput-object v4, v5, Landroid/preference/MySelectAppsPreference$AppData;->packageName:Ljava/lang/String;

    iput v2, v5, Landroid/preference/MySelectAppsPreference$AppData;->checked:I

    const/4 v0, 0x0

    invoke-virtual {p1, v0, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :cond_9
    return-object p1
.end method

.method private sortWifi(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 9

    iget-object v0, p0, Landroid/preference/MySelectAppsPreference$LoadApps;->this$0:Landroid/preference/MySelectAppsPreference;

    new-instance v6, Landroid/preference/MySelectAppsPreference$AppDataComparator;

    invoke-direct {v6}, Landroid/preference/MySelectAppsPreference$AppDataComparator;-><init>()V

    invoke-static {p1, v6}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Landroid/preference/MySelectAppsPreference;->access$3(Landroid/preference/MySelectAppsPreference;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "\u0001"

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    const/4 v0, 0x0

    new-instance v5, Landroid/preference/MySelectAppsPreference$AppData;

    invoke-direct {v5}, Landroid/preference/MySelectAppsPreference$AppData;-><init>()V

    :goto_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v8, "\u0003"

    invoke-virtual {v4, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const/4 v8, 0x0

    aget-object v8, v4, v8

    iput-object v8, v5, Landroid/preference/MySelectAppsPreference$AppData;->packageName:Ljava/lang/String;

    invoke-static {p1, v5, v6}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v1

    if-ltz v1, :cond_1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/preference/MySelectAppsPreference$AppData;

    const/4 v4, 0x1

    iput v4, v2, Landroid/preference/MySelectAppsPreference$AppData;->checked:I

    invoke-virtual {p1, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    array-length v1, v4

    const/4 v2, 0x1

    iput v2, v5, Landroid/preference/MySelectAppsPreference$AppData;->checked:I

    if-le v1, v2, :cond_2

    aget-object v8, v4, v2

    :cond_2
    iput-object v8, v5, Landroid/preference/MySelectAppsPreference$AppData;->name:Ljava/lang/String;

    invoke-direct {p0}, Landroid/preference/MySelectAppsPreference$LoadApps;->getSignalNullIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    iput-object v8, v5, Landroid/preference/MySelectAppsPreference$AppData;->icon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v5, Landroid/preference/MySelectAppsPreference$AppData;

    invoke-direct {v5}, Landroid/preference/MySelectAppsPreference$AppData;-><init>()V

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v7}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    const/4 v0, 0x0

    const/4 v3, 0x0

    :goto_2
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_5

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/preference/MySelectAppsPreference$AppData;

    iget v2, v4, Landroid/preference/MySelectAppsPreference$AppData;->checked:I

    if-eqz v2, :cond_4

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    invoke-virtual {p1, v3, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    add-int/lit8 v3, v3, 0x1

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Landroid/preference/MySelectAppsPreference$LoadApps;->this$0:Landroid/preference/MySelectAppsPreference;

    iget-object v4, v0, Landroid/preference/MySelectAppsPreference;->mUncheckedTitle:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    if-eqz v3, :cond_7

    :cond_6
    const/4 v2, -0x1

    new-instance v5, Landroid/preference/MySelectAppsPreference$AppData;

    invoke-direct {v5}, Landroid/preference/MySelectAppsPreference$AppData;-><init>()V

    iput-object v4, v5, Landroid/preference/MySelectAppsPreference$AppData;->name:Ljava/lang/String;

    iput-object v4, v5, Landroid/preference/MySelectAppsPreference$AppData;->packageName:Ljava/lang/String;

    iput v2, v5, Landroid/preference/MySelectAppsPreference$AppData;->checked:I

    invoke-virtual {p1, v3, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    if-eqz v3, :cond_7

    iget-object v4, v0, Landroid/preference/MySelectAppsPreference;->mCheckedTitle:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_7

    const/4 v2, -0x1

    new-instance v5, Landroid/preference/MySelectAppsPreference$AppData;

    invoke-direct {v5}, Landroid/preference/MySelectAppsPreference$AppData;-><init>()V

    iput-object v4, v5, Landroid/preference/MySelectAppsPreference$AppData;->name:Ljava/lang/String;

    iput-object v4, v5, Landroid/preference/MySelectAppsPreference$AppData;->packageName:Ljava/lang/String;

    iput v2, v5, Landroid/preference/MySelectAppsPreference$AppData;->checked:I

    const/4 v0, 0x0

    invoke-virtual {p1, v0, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :cond_7
    return-object p1
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Landroid/preference/MySelectAppsPreference$LoadApps;->doInBackground([Ljava/lang/Void;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/ArrayList;
    .locals 3

    iget-object v0, p0, Landroid/preference/MySelectAppsPreference$LoadApps;->this$0:Landroid/preference/MySelectAppsPreference;

    invoke-virtual {v0}, Landroid/preference/MySelectAppsPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget v0, v0, Landroid/preference/MySelectAppsPreference;->mExtra:I

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_3

    const/4 v2, 0x5

    if-eq v0, v2, :cond_4

    invoke-direct {p0, v1}, Landroid/preference/MySelectAppsPreference$LoadApps;->getAllApps(Landroid/content/pm/PackageManager;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_0

    :cond_0
    invoke-direct {p0, v1}, Landroid/preference/MySelectAppsPreference$LoadApps;->getAllActivities(Landroid/content/pm/PackageManager;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-direct {p0, v1}, Landroid/preference/MySelectAppsPreference$LoadApps;->getAllShareApps(Landroid/content/pm/PackageManager;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_0

    :cond_2
    invoke-direct {p0, v1}, Landroid/preference/MySelectAppsPreference$LoadApps;->getAllOpenWithApps(Landroid/content/pm/PackageManager;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Landroid/preference/MySelectAppsPreference$LoadApps;->getAllWifi()Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_0

    :cond_4
    invoke-direct {p0, v1}, Landroid/preference/MySelectAppsPreference$LoadApps;->getAllInstalledApps(Landroid/content/pm/PackageManager;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_0

    :goto_0
    return-object v1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Landroid/preference/MySelectAppsPreference$LoadApps;->onPostExecute(Ljava/util/ArrayList;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/ArrayList;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    iget-object v0, p0, Landroid/preference/MySelectAppsPreference$LoadApps;->this$0:Landroid/preference/MySelectAppsPreference;

    iget v3, v0, Landroid/preference/MySelectAppsPreference;->mExtra:I

    const/4 v2, 0x1

    if-eq v3, v2, :cond_1

    const/4 v2, 0x2

    if-eq v3, v2, :cond_0

    const/4 v2, 0x3

    if-eq v3, v2, :cond_2

    const/4 v2, 0x4

    if-eq v3, v2, :cond_3

    const/4 v2, 0x5

    if-eq v3, v2, :cond_4

    invoke-direct {p0, p1}, Landroid/preference/MySelectAppsPreference$LoadApps;->sortApps(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Landroid/preference/MySelectAppsPreference$LoadApps;->sortActivities(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Landroid/preference/MySelectAppsPreference$LoadApps;->sortApps(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Landroid/preference/MySelectAppsPreference$LoadApps;->sortApps(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1

    goto :goto_0

    :cond_3
    return-void

    goto :goto_0

    :cond_4
    invoke-direct {p0, p1}, Landroid/preference/MySelectAppsPreference$LoadApps;->sortApps(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1

    goto :goto_0

    :goto_0
    iget-object v0, p0, Landroid/preference/MySelectAppsPreference$LoadApps;->this$0:Landroid/preference/MySelectAppsPreference;

    invoke-static {v0}, Landroid/preference/MySelectAppsPreference;->access$2(Landroid/preference/MySelectAppsPreference;)Landroid/widget/LinearLayout;

    move-result-object v1

    const v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Landroid/preference/MySelectAppsPreference$LoadApps;->this$0:Landroid/preference/MySelectAppsPreference;

    invoke-static {v0}, Landroid/preference/MySelectAppsPreference;->access$1(Landroid/preference/MySelectAppsPreference;)Landroid/widget/ListView;

    move-result-object v2

    const/4 v3, -0x1

    new-instance v1, Landroid/preference/MySelectAppsPreference$Adapter;

    invoke-direct {v1, v0, v3, p1}, Landroid/preference/MySelectAppsPreference$Adapter;-><init>(Landroid/preference/MySelectAppsPreference;ILjava/util/ArrayList;)V

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    check-cast v1, Landroid/widget/ArrayAdapter;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    return-void
.end method
