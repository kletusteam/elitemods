.class abstract Lcom/android/framework/protobuf/Utf8$Processor;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/framework/protobuf/Utf8;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "Processor"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static partialIsValidUtf8(Ljava/nio/ByteBuffer;II)I
    .locals 6

    invoke-static {p0, p1, p2}, Lcom/android/framework/protobuf/Utf8;->access$200(Ljava/nio/ByteBuffer;II)I

    move-result v0

    add-int/2addr p1, v0

    :goto_0
    if-lt p1, p2, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, p1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result p1

    move v1, p1

    if-gez p1, :cond_f

    const/16 p1, -0x20

    const/4 v2, -0x1

    const/16 v3, -0x41

    if-ge v1, p1, :cond_4

    if-lt v0, p2, :cond_1

    return v1

    :cond_1
    const/16 p1, -0x3e

    if-lt v1, p1, :cond_3

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result p1

    if-le p1, v3, :cond_2

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    move p1, v0

    goto :goto_3

    :cond_3
    :goto_1
    return v2

    :cond_4
    const/16 v4, -0x10

    if-ge v1, v4, :cond_a

    add-int/lit8 v4, p2, -0x1

    if-lt v0, v4, :cond_5

    sub-int p1, p2, v0

    invoke-static {p0, v1, v0, p1}, Lcom/android/framework/protobuf/Utf8;->access$300(Ljava/nio/ByteBuffer;III)I

    move-result p1

    return p1

    :cond_5
    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-gt v0, v3, :cond_9

    const/16 v5, -0x60

    if-ne v1, p1, :cond_6

    if-lt v0, v5, :cond_9

    :cond_6
    const/16 p1, -0x13

    if-ne v1, p1, :cond_7

    if-ge v0, v5, :cond_9

    :cond_7
    invoke-virtual {p0, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result p1

    if-le p1, v3, :cond_8

    goto :goto_2

    :cond_8
    nop

    add-int/lit8 v4, v4, 0x1

    move p1, v4

    goto :goto_3

    :cond_9
    :goto_2
    return v2

    :cond_a
    add-int/lit8 p1, p2, -0x2

    if-lt v0, p1, :cond_b

    sub-int p1, p2, v0

    invoke-static {p0, v1, v0, p1}, Lcom/android/framework/protobuf/Utf8;->access$300(Ljava/nio/ByteBuffer;III)I

    move-result p1

    return p1

    :cond_b
    add-int/lit8 p1, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-gt v0, v3, :cond_e

    shl-int/lit8 v4, v1, 0x1c

    add-int/lit8 v5, v0, 0x70

    add-int/2addr v4, v5

    shr-int/lit8 v4, v4, 0x1e

    if-nez v4, :cond_e

    add-int/lit8 v4, p1, 0x1

    invoke-virtual {p0, p1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result p1

    if-gt p1, v3, :cond_d

    add-int/lit8 p1, v4, 0x1

    invoke-virtual {p0, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    if-le v4, v3, :cond_c

    goto :goto_4

    :cond_c
    :goto_3
    goto/16 :goto_0

    :cond_d
    move p1, v4

    :cond_e
    :goto_4
    return v2

    :cond_f
    move p1, v0

    goto/16 :goto_0
.end method


# virtual methods
.method final decodeUtf8(Ljava/nio/ByteBuffer;II)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/framework/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->isDirect()Z

    move-result v0

    goto/32 :goto_8

    nop

    :goto_1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->hasArray()Z

    move-result v0

    goto/32 :goto_6

    nop

    :goto_2
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    goto/32 :goto_5

    nop

    :goto_3
    return-object v0

    :goto_4
    invoke-virtual {p0, v1, v2, p3}, Lcom/android/framework/protobuf/Utf8$Processor;->decodeUtf8([BII)Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_9

    nop

    :goto_5
    add-int v2, v0, p2

    goto/32 :goto_4

    nop

    :goto_6
    if-nez v0, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_b

    nop

    :goto_7
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/framework/protobuf/Utf8$Processor;->decodeUtf8Direct(Ljava/nio/ByteBuffer;II)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_c

    nop

    :goto_8
    if-nez v0, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_7

    nop

    :goto_9
    return-object v1

    :goto_a
    goto/32 :goto_0

    nop

    :goto_b
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v0

    goto/32 :goto_2

    nop

    :goto_c
    return-object v0

    :goto_d
    goto/32 :goto_e

    nop

    :goto_e
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/framework/protobuf/Utf8$Processor;->decodeUtf8Default(Ljava/nio/ByteBuffer;II)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_3

    nop
.end method

.method abstract decodeUtf8([BII)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/framework/protobuf/InvalidProtocolBufferException;
        }
    .end annotation
.end method

.method final decodeUtf8Default(Ljava/nio/ByteBuffer;II)Ljava/lang/String;
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/framework/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    goto/32 :goto_2d

    nop

    :goto_0
    move-object v10, v12

    goto/32 :goto_1

    nop

    :goto_1
    invoke-static/range {v6 .. v11}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$1000(BBBB[CI)V

    goto/32 :goto_50

    nop

    :goto_2
    add-int v5, v2, v1

    goto/32 :goto_12

    nop

    :goto_3
    invoke-static {v2}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$600(B)Z

    move-result v7

    goto/32 :goto_3c

    nop

    :goto_4
    if-lt v2, v5, :cond_0

    goto/32 :goto_64

    :cond_0
    goto/32 :goto_16

    nop

    :goto_5
    invoke-static {v2, v6, v7, v12, v11}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$900(BBB[CI)V

    goto/32 :goto_4e

    nop

    :goto_6
    goto/16 :goto_64

    :goto_7
    goto/32 :goto_67

    nop

    :goto_8
    move v11, v14

    :goto_9
    goto/32 :goto_53

    nop

    :goto_a
    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v8

    goto/32 :goto_51

    nop

    :goto_b
    add-int/lit8 v6, v6, 0x1

    goto/32 :goto_45

    nop

    :goto_c
    move v2, v7

    goto/32 :goto_31

    nop

    :goto_d
    if-eqz v9, :cond_1

    goto/32 :goto_66

    :cond_1
    goto/32 :goto_65

    nop

    :goto_e
    const/4 v3, 0x0

    goto/32 :goto_38

    nop

    :goto_f
    add-int/lit8 v8, v6, 0x1

    goto/32 :goto_35

    nop

    :goto_10
    goto :goto_1f

    :goto_11
    goto/32 :goto_30

    nop

    :goto_12
    new-array v12, v1, [C

    goto/32 :goto_17

    nop

    :goto_13
    move v2, v13

    goto/32 :goto_8

    nop

    :goto_14
    throw v3

    :goto_15
    goto/32 :goto_2b

    nop

    :goto_16
    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v7

    goto/32 :goto_52

    nop

    :goto_17
    const/4 v6, 0x0

    :goto_18
    goto/32 :goto_4

    nop

    :goto_19
    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_37

    nop

    :goto_1a
    move v8, v9

    goto/32 :goto_62

    nop

    :goto_1b
    invoke-static {v2, v6, v12, v11}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$700(BB[CI)V

    goto/32 :goto_c

    nop

    :goto_1c
    add-int/lit8 v7, v6, 0x1

    goto/32 :goto_a

    nop

    :goto_1d
    add-int/lit8 v7, v6, 0x1

    goto/32 :goto_6d

    nop

    :goto_1e
    invoke-static {v2, v12, v11}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$500(B[CI)V

    :goto_1f
    goto/32 :goto_55

    nop

    :goto_20
    move v11, v9

    goto/32 :goto_6a

    nop

    :goto_21
    if-nez v7, :cond_2

    goto/32 :goto_7e

    :cond_2
    goto/32 :goto_6f

    nop

    :goto_22
    if-gez v2, :cond_3

    goto/32 :goto_42

    :cond_3
    goto/32 :goto_2e

    nop

    :goto_23
    if-lt v6, v7, :cond_4

    goto/32 :goto_6b

    :cond_4
    goto/32 :goto_4b

    nop

    :goto_24
    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v6

    goto/32 :goto_79

    nop

    :goto_25
    invoke-static {}, Lcom/android/framework/protobuf/InvalidProtocolBufferException;->invalidUtf8()Lcom/android/framework/protobuf/InvalidProtocolBufferException;

    move-result-object v3

    goto/32 :goto_33

    nop

    :goto_26
    move v11, v6

    :goto_27
    goto/32 :goto_2c

    nop

    :goto_28
    const/4 v4, 0x2

    goto/32 :goto_68

    nop

    :goto_29
    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_19

    nop

    :goto_2a
    aput-object v6, v5, v3

    goto/32 :goto_5c

    nop

    :goto_2b
    add-int/lit8 v7, v5, -0x2

    goto/32 :goto_49

    nop

    :goto_2c
    if-lt v2, v5, :cond_5

    goto/32 :goto_59

    :cond_5
    goto/32 :goto_5a

    nop

    :goto_2d
    move-object/from16 v0, p1

    goto/32 :goto_4c

    nop

    :goto_2e
    move/from16 v2, p2

    goto/32 :goto_2

    nop

    :goto_2f
    add-int/lit8 v9, v11, 0x1

    goto/32 :goto_5

    nop

    :goto_30
    move v2, v6

    goto/32 :goto_72

    nop

    :goto_31
    move v11, v8

    goto/32 :goto_3a

    nop

    :goto_32
    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v7

    goto/32 :goto_2f

    nop

    :goto_33
    throw v3

    :goto_34
    goto/32 :goto_48

    nop

    :goto_35
    invoke-static {v7, v12, v6}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$500(B[CI)V

    goto/32 :goto_3e

    nop

    :goto_36
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto/32 :goto_2a

    nop

    :goto_37
    throw v2

    :goto_38
    const/4 v4, 0x1

    goto/32 :goto_22

    nop

    :goto_39
    sub-int/2addr v3, v1

    goto/32 :goto_60

    nop

    :goto_3a
    goto/16 :goto_9

    :goto_3b
    goto/32 :goto_25

    nop

    :goto_3c
    if-nez v7, :cond_6

    goto/32 :goto_34

    :cond_6
    goto/32 :goto_47

    nop

    :goto_3d
    sub-int v3, v3, p2

    goto/32 :goto_39

    nop

    :goto_3e
    move v6, v8

    goto/32 :goto_63

    nop

    :goto_3f
    or-int v2, p2, v1

    goto/32 :goto_5f

    nop

    :goto_40
    invoke-static {v2}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$400(B)Z

    move-result v7

    goto/32 :goto_21

    nop

    :goto_41
    return-object v4

    :goto_42
    goto/32 :goto_78

    nop

    :goto_43
    invoke-static {v8}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$400(B)Z

    move-result v9

    goto/32 :goto_d

    nop

    :goto_44
    add-int/lit8 v13, v6, 0x1

    goto/32 :goto_6c

    nop

    :goto_45
    add-int/lit8 v9, v7, 0x1

    goto/32 :goto_75

    nop

    :goto_46
    add-int/lit8 v7, v5, -0x1

    goto/32 :goto_23

    nop

    :goto_47
    if-lt v6, v5, :cond_7

    goto/32 :goto_3b

    :cond_7
    goto/32 :goto_1d

    nop

    :goto_48
    invoke-static {v2}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$800(B)Z

    move-result v7

    goto/32 :goto_71

    nop

    :goto_49
    if-lt v6, v7, :cond_8

    goto/32 :goto_54

    :cond_8
    goto/32 :goto_1c

    nop

    :goto_4a
    move v7, v8

    goto/32 :goto_1a

    nop

    :goto_4b
    add-int/lit8 v7, v6, 0x1

    goto/32 :goto_24

    nop

    :goto_4c
    move/from16 v1, p3

    goto/32 :goto_3f

    nop

    :goto_4d
    aput-object v3, v5, v4

    goto/32 :goto_7a

    nop

    :goto_4e
    move v2, v8

    goto/32 :goto_20

    nop

    :goto_4f
    new-array v5, v5, [Ljava/lang/Object;

    goto/32 :goto_77

    nop

    :goto_50
    add-int/2addr v14, v4

    goto/32 :goto_13

    nop

    :goto_51
    add-int/lit8 v6, v7, 0x1

    goto/32 :goto_5d

    nop

    :goto_52
    invoke-static {v7}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$400(B)Z

    move-result v8

    goto/32 :goto_73

    nop

    :goto_53
    goto/16 :goto_27

    :goto_54
    goto/32 :goto_70

    nop

    :goto_55
    if-lt v6, v5, :cond_9

    goto/32 :goto_11

    :cond_9
    goto/32 :goto_6e

    nop

    :goto_56
    add-int/lit8 v14, v11, 0x1

    goto/32 :goto_69

    nop

    :goto_57
    new-instance v4, Ljava/lang/String;

    goto/32 :goto_5e

    nop

    :goto_58
    throw v3

    :goto_59
    goto/32 :goto_57

    nop

    :goto_5a
    add-int/lit8 v6, v2, 0x1

    goto/32 :goto_74

    nop

    :goto_5b
    invoke-static {}, Lcom/android/framework/protobuf/InvalidProtocolBufferException;->invalidUtf8()Lcom/android/framework/protobuf/InvalidProtocolBufferException;

    move-result-object v3

    goto/32 :goto_14

    nop

    :goto_5c
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/32 :goto_4d

    nop

    :goto_5d
    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v9

    goto/32 :goto_44

    nop

    :goto_5e
    invoke-direct {v4, v12, v3, v11}, Ljava/lang/String;-><init>([CII)V

    goto/32 :goto_41

    nop

    :goto_5f
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v3

    goto/32 :goto_3d

    nop

    :goto_60
    or-int/2addr v2, v3

    goto/32 :goto_e

    nop

    :goto_61
    const/4 v5, 0x3

    goto/32 :goto_4f

    nop

    :goto_62
    move v9, v10

    goto/32 :goto_0

    nop

    :goto_63
    goto/16 :goto_18

    :goto_64
    goto/32 :goto_26

    nop

    :goto_65
    goto/16 :goto_11

    :goto_66
    goto/32 :goto_b

    nop

    :goto_67
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_f

    nop

    :goto_68
    aput-object v3, v5, v4

    goto/32 :goto_7b

    nop

    :goto_69
    move v6, v2

    goto/32 :goto_4a

    nop

    :goto_6a
    goto/16 :goto_9

    :goto_6b
    goto/32 :goto_5b

    nop

    :goto_6c
    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v10

    goto/32 :goto_56

    nop

    :goto_6d
    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v6

    goto/32 :goto_76

    nop

    :goto_6e
    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v8

    goto/32 :goto_43

    nop

    :goto_6f
    add-int/lit8 v7, v11, 0x1

    goto/32 :goto_1e

    nop

    :goto_70
    invoke-static {}, Lcom/android/framework/protobuf/InvalidProtocolBufferException;->invalidUtf8()Lcom/android/framework/protobuf/InvalidProtocolBufferException;

    move-result-object v3

    goto/32 :goto_58

    nop

    :goto_71
    if-nez v7, :cond_a

    goto/32 :goto_15

    :cond_a
    goto/32 :goto_46

    nop

    :goto_72
    move v11, v7

    goto/32 :goto_7d

    nop

    :goto_73
    if-eqz v8, :cond_b

    goto/32 :goto_7

    :cond_b
    goto/32 :goto_6

    nop

    :goto_74
    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    goto/32 :goto_40

    nop

    :goto_75
    invoke-static {v8, v12, v7}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$500(B[CI)V

    goto/32 :goto_7c

    nop

    :goto_76
    add-int/lit8 v8, v11, 0x1

    goto/32 :goto_1b

    nop

    :goto_77
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v6

    goto/32 :goto_36

    nop

    :goto_78
    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    goto/32 :goto_61

    nop

    :goto_79
    add-int/lit8 v8, v7, 0x1

    goto/32 :goto_32

    nop

    :goto_7a
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/32 :goto_28

    nop

    :goto_7b
    const-string v3, "buffer limit=%d, index=%d, limit=%d"

    goto/32 :goto_29

    nop

    :goto_7c
    move v7, v9

    goto/32 :goto_10

    nop

    :goto_7d
    goto/16 :goto_9

    :goto_7e
    goto/32 :goto_3

    nop
.end method

.method abstract decodeUtf8Direct(Ljava/nio/ByteBuffer;II)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/framework/protobuf/InvalidProtocolBufferException;
        }
    .end annotation
.end method

.method abstract encodeUtf8(Ljava/lang/CharSequence;[BII)I
.end method

.method final encodeUtf8(Ljava/lang/CharSequence;Ljava/nio/ByteBuffer;)V
    .locals 4

    goto/32 :goto_c

    nop

    :goto_0
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->isDirect()Z

    move-result v0

    goto/32 :goto_e

    nop

    :goto_1
    add-int/2addr v2, v0

    goto/32 :goto_5

    nop

    :goto_2
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    goto/32 :goto_d

    nop

    :goto_3
    goto :goto_11

    :goto_4
    goto/32 :goto_10

    nop

    :goto_5
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    goto/32 :goto_12

    nop

    :goto_6
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v0

    goto/32 :goto_2

    nop

    :goto_7
    goto :goto_11

    :goto_8
    goto/32 :goto_0

    nop

    :goto_9
    if-nez v0, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_6

    nop

    :goto_a
    invoke-virtual {p0, p1, p2}, Lcom/android/framework/protobuf/Utf8$Processor;->encodeUtf8Direct(Ljava/lang/CharSequence;Ljava/nio/ByteBuffer;)V

    goto/32 :goto_3

    nop

    :goto_b
    return-void

    :goto_c
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->hasArray()Z

    move-result v0

    goto/32 :goto_9

    nop

    :goto_d
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    goto/32 :goto_1

    nop

    :goto_e
    if-nez v0, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_a

    nop

    :goto_f
    sub-int v2, v1, v0

    goto/32 :goto_13

    nop

    :goto_10
    invoke-virtual {p0, p1, p2}, Lcom/android/framework/protobuf/Utf8$Processor;->encodeUtf8Default(Ljava/lang/CharSequence;Ljava/nio/ByteBuffer;)V

    :goto_11
    goto/32 :goto_b

    nop

    :goto_12
    invoke-static {p1, v1, v2, v3}, Lcom/android/framework/protobuf/Utf8;->encode(Ljava/lang/CharSequence;[BII)I

    move-result v1

    goto/32 :goto_f

    nop

    :goto_13
    invoke-virtual {p2, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    goto/32 :goto_7

    nop
.end method

.method final encodeUtf8Default(Ljava/lang/CharSequence;Ljava/nio/ByteBuffer;)V
    .locals 9

    goto/32 :goto_8

    nop

    :goto_0
    goto/16 :goto_4c

    :catch_0
    move-exception v3

    goto/32 :goto_1c

    nop

    :goto_1
    const-string v7, " at index "

    goto/32 :goto_10

    nop

    :goto_2
    ushr-int/lit8 v6, v4, 0x6

    goto/32 :goto_1d

    nop

    :goto_3
    new-instance v6, Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_4
    if-lt v4, v5, :cond_0

    goto/32 :goto_1b

    :cond_0
    goto/32 :goto_43

    nop

    :goto_5
    int-to-byte v6, v6

    :try_start_0
    invoke-virtual {p2, v1, v6}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    and-int/lit8 v1, v4, 0x3f

    or-int/2addr v1, v3

    int-to-byte v1, v1

    invoke-virtual {p2, v5, v1}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_21

    nop

    :goto_6
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_40

    nop

    :goto_7
    add-int/lit8 v1, v7, 0x1

    goto/32 :goto_25

    nop

    :goto_8
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    goto/32 :goto_45

    nop

    :goto_9
    and-int/lit8 v8, v8, 0x3f

    goto/32 :goto_4e

    nop

    :goto_a
    int-to-byte v8, v8

    :try_start_1
    invoke-virtual {p2, v1, v8}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_2

    goto/32 :goto_7

    nop

    :goto_b
    throw v5

    :goto_c
    move v1, v7

    goto/32 :goto_30

    nop

    :goto_d
    int-to-byte v6, v6

    :try_start_2
    invoke-virtual {p2, v1, v6}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;
    :try_end_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_0

    goto/32 :goto_22

    nop

    :goto_e
    add-int/lit8 v7, v1, 0x1

    goto/32 :goto_29

    nop

    :goto_f
    int-to-byte v8, v8

    :try_start_3
    invoke-virtual {p2, v1, v8}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    and-int/lit8 v1, v5, 0x3f

    or-int/2addr v1, v3

    int-to-byte v1, v1

    invoke-virtual {p2, v7, v1}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;
    :try_end_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_2

    goto/32 :goto_1e

    nop

    :goto_10
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_2c

    nop

    :goto_11
    or-int/lit16 v6, v6, 0xe0

    goto/32 :goto_d

    nop

    :goto_12
    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    goto/32 :goto_3

    nop

    :goto_13
    if-lt v2, v0, :cond_1

    goto/32 :goto_14

    :cond_1
    :try_start_4
    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    move v5, v4

    if-ge v4, v3, :cond_2

    add-int v3, v1, v2

    int-to-byte v4, v5

    invoke-virtual {p2, v3, v4}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_34

    :catch_1
    move-exception v3

    goto/16 :goto_3f

    :cond_2
    :goto_14
    if-ne v2, v0, :cond_3

    add-int v3, v1, v2

    invoke-virtual {p2, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    return-void

    :cond_3
    add-int/2addr v1, v2

    :goto_15
    if-ge v2, v0, :cond_9

    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    if-ge v4, v3, :cond_8

    int-to-byte v5, v4

    invoke-virtual {p2, v1, v5}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;
    :try_end_4
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_4 .. :try_end_4} :catch_1

    goto/32 :goto_3b

    nop

    :goto_16
    sub-int v5, v1, v5

    goto/32 :goto_4f

    nop

    :goto_17
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_1

    nop

    :goto_18
    int-to-byte v8, v8

    :try_start_5
    invoke-virtual {p2, v7, v8}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;
    :try_end_5
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_5 .. :try_end_5} :catch_1

    goto/32 :goto_e

    nop

    :goto_19
    const v5, 0xdfff

    goto/32 :goto_38

    nop

    :goto_1a
    goto/16 :goto_3f

    :goto_1b
    goto/32 :goto_4a

    nop

    :goto_1c
    move v1, v5

    goto/32 :goto_1a

    nop

    :goto_1d
    or-int/lit16 v6, v6, 0xc0

    goto/32 :goto_5

    nop

    :goto_1e
    move v1, v7

    goto/32 :goto_44

    nop

    :goto_1f
    ushr-int/lit8 v8, v5, 0x12

    goto/32 :goto_37

    nop

    :goto_20
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_41

    nop

    :goto_21
    move v1, v5

    goto/32 :goto_0

    nop

    :goto_22
    add-int/lit8 v1, v5, 0x1

    goto/32 :goto_39

    nop

    :goto_23
    const/16 v5, 0x800

    goto/32 :goto_4

    nop

    :goto_24
    or-int/2addr v6, v3

    goto/32 :goto_4b

    nop

    :goto_25
    ushr-int/lit8 v8, v5, 0xc

    goto/32 :goto_35

    nop

    :goto_26
    goto :goto_32

    :goto_27
    goto/32 :goto_46

    nop

    :goto_28
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->position()I

    move-result v5

    goto/32 :goto_16

    nop

    :goto_29
    ushr-int/lit8 v8, v5, 0x6

    goto/32 :goto_9

    nop

    :goto_2a
    ushr-int/lit8 v6, v4, 0xc

    goto/32 :goto_11

    nop

    :goto_2b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/32 :goto_48

    nop

    :goto_2c
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_2b

    nop

    :goto_2d
    and-int/lit8 v6, v6, 0x3f

    goto/32 :goto_24

    nop

    :goto_2e
    add-int/lit8 v5, v1, 0x1

    goto/32 :goto_2a

    nop

    :goto_2f
    if-ge v4, v5, :cond_4

    goto/32 :goto_32

    :cond_4
    goto/32 :goto_19

    nop

    :goto_30
    goto :goto_3f

    :cond_5
    :goto_31
    :try_start_6
    new-instance v3, Lcom/android/framework/protobuf/Utf8$UnpairedSurrogateException;

    invoke-direct {v3, v2, v0}, Lcom/android/framework/protobuf/Utf8$UnpairedSurrogateException;-><init>(II)V

    throw v3
    :try_end_6
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_6 .. :try_end_6} :catch_1

    :goto_32
    goto/32 :goto_2e

    nop

    :goto_33
    const/4 v2, 0x0

    :goto_34
    goto/32 :goto_3d

    nop

    :goto_35
    and-int/lit8 v8, v8, 0x3f

    goto/32 :goto_4d

    nop

    :goto_36
    if-ne v5, v0, :cond_6

    goto/32 :goto_31

    :cond_6
    goto/32 :goto_47

    nop

    :goto_37
    or-int/lit16 v8, v8, 0xf0

    goto/32 :goto_a

    nop

    :goto_38
    if-lt v5, v4, :cond_7

    goto/32 :goto_27

    :cond_7
    goto/32 :goto_26

    nop

    :goto_39
    ushr-int/lit8 v6, v4, 0x6

    goto/32 :goto_2d

    nop

    :goto_3a
    add-int/2addr v4, v5

    goto/32 :goto_12

    nop

    :goto_3b
    goto/16 :goto_4c

    :cond_8
    goto/32 :goto_23

    nop

    :goto_3c
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    goto/32 :goto_28

    nop

    :goto_3d
    const/16 v3, 0x80

    goto/32 :goto_13

    nop

    :goto_3e
    return-void

    :goto_3f
    goto/32 :goto_3c

    nop

    :goto_40
    const-string v7, "Failed writing "

    goto/32 :goto_20

    nop

    :goto_41
    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v7

    goto/32 :goto_17

    nop

    :goto_42
    add-int/lit8 v7, v1, 0x1

    goto/32 :goto_1f

    nop

    :goto_43
    add-int/lit8 v5, v1, 0x1

    goto/32 :goto_2

    nop

    :goto_44
    goto :goto_4c

    :catch_2
    move-exception v3

    goto/32 :goto_c

    nop

    :goto_45
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    goto/32 :goto_33

    nop

    :goto_46
    add-int/lit8 v5, v2, 0x1

    goto/32 :goto_36

    nop

    :goto_47
    add-int/lit8 v2, v2, 0x1

    :try_start_7
    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    move v6, v5

    invoke-static {v4, v5}, Ljava/lang/Character;->isSurrogatePair(CC)Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-static {v4, v6}, Ljava/lang/Character;->toCodePoint(CC)I

    move-result v5
    :try_end_7
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_7 .. :try_end_7} :catch_1

    goto/32 :goto_42

    nop

    :goto_48
    invoke-direct {v5, v6}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_b

    nop

    :goto_49
    invoke-static {v2, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    goto/32 :goto_3a

    nop

    :goto_4a
    const v5, 0xd800

    goto/32 :goto_2f

    nop

    :goto_4b
    int-to-byte v6, v6

    :try_start_8
    invoke-virtual {p2, v5, v6}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    and-int/lit8 v5, v4, 0x3f

    or-int/2addr v5, v3

    int-to-byte v5, v5

    invoke-virtual {p2, v1, v5}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    :goto_4c
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_15

    :cond_9
    invoke-virtual {p2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_8
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_8 .. :try_end_8} :catch_1

    nop

    goto/32 :goto_3e

    nop

    :goto_4d
    or-int/2addr v8, v3

    goto/32 :goto_18

    nop

    :goto_4e
    or-int/2addr v8, v3

    goto/32 :goto_f

    nop

    :goto_4f
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_49

    nop
.end method

.method abstract encodeUtf8Direct(Ljava/lang/CharSequence;Ljava/nio/ByteBuffer;)V
.end method

.method final isValidUtf8(Ljava/nio/ByteBuffer;II)Z
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_5

    nop

    :goto_1
    if-eqz v1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_3

    nop

    :goto_2
    return v0

    :goto_3
    const/4 v0, 0x1

    :goto_4
    goto/32 :goto_2

    nop

    :goto_5
    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/android/framework/protobuf/Utf8$Processor;->partialIsValidUtf8(ILjava/nio/ByteBuffer;II)I

    move-result v1

    goto/32 :goto_1

    nop
.end method

.method final isValidUtf8([BII)Z
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_4

    nop

    :goto_1
    const/4 v0, 0x1

    :goto_2
    goto/32 :goto_3

    nop

    :goto_3
    return v0

    :goto_4
    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/android/framework/protobuf/Utf8$Processor;->partialIsValidUtf8(I[BII)I

    move-result v1

    goto/32 :goto_5

    nop

    :goto_5
    if-eqz v1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop
.end method

.method final partialIsValidUtf8(ILjava/nio/ByteBuffer;II)I
    .locals 4

    goto/32 :goto_9

    nop

    :goto_0
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v0

    goto/32 :goto_6

    nop

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_a

    nop

    :goto_2
    return v0

    :goto_3
    goto/32 :goto_f

    nop

    :goto_4
    add-int v3, v0, p4

    goto/32 :goto_5

    nop

    :goto_5
    invoke-virtual {p0, p1, v1, v2, v3}, Lcom/android/framework/protobuf/Utf8$Processor;->partialIsValidUtf8(I[BII)I

    move-result v1

    goto/32 :goto_d

    nop

    :goto_6
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    goto/32 :goto_c

    nop

    :goto_7
    if-nez v0, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_0

    nop

    :goto_8
    return v0

    :goto_9
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->hasArray()Z

    move-result v0

    goto/32 :goto_7

    nop

    :goto_a
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/framework/protobuf/Utf8$Processor;->partialIsValidUtf8Direct(ILjava/nio/ByteBuffer;II)I

    move-result v0

    goto/32 :goto_2

    nop

    :goto_b
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->isDirect()Z

    move-result v0

    goto/32 :goto_1

    nop

    :goto_c
    add-int v2, v0, p3

    goto/32 :goto_4

    nop

    :goto_d
    return v1

    :goto_e
    goto/32 :goto_b

    nop

    :goto_f
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/framework/protobuf/Utf8$Processor;->partialIsValidUtf8Default(ILjava/nio/ByteBuffer;II)I

    move-result v0

    goto/32 :goto_8

    nop
.end method

.method abstract partialIsValidUtf8(I[BII)I
.end method

.method final partialIsValidUtf8Default(ILjava/nio/ByteBuffer;II)I
    .locals 7

    goto/32 :goto_31

    nop

    :goto_0
    move p3, v1

    goto/32 :goto_42

    nop

    :goto_1
    add-int/lit8 v1, p3, 0x1

    goto/32 :goto_25

    nop

    :goto_2
    const/16 v1, -0x13

    goto/32 :goto_20

    nop

    :goto_3
    invoke-virtual {p2, p3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result p3

    goto/32 :goto_5a

    nop

    :goto_4
    int-to-byte v1, v1

    goto/32 :goto_1a

    nop

    :goto_5
    if-gt p3, v3, :cond_0

    goto/32 :goto_2e

    :cond_0
    goto/32 :goto_62

    nop

    :goto_6
    add-int/lit8 v5, p3, 0x1

    goto/32 :goto_50

    nop

    :goto_7
    if-lt v0, v1, :cond_1

    goto/32 :goto_3c

    :cond_1
    goto/32 :goto_11

    nop

    :goto_8
    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/Utf8;->access$100(III)I

    move-result p3

    goto/32 :goto_3e

    nop

    :goto_9
    return p3

    :goto_a
    goto/32 :goto_4d

    nop

    :goto_b
    if-ge v5, p4, :cond_2

    goto/32 :goto_a

    :cond_2
    goto/32 :goto_15

    nop

    :goto_c
    move p3, v5

    :goto_d
    goto/32 :goto_26

    nop

    :goto_e
    if-le v4, v3, :cond_3

    goto/32 :goto_43

    :cond_3
    goto/32 :goto_56

    nop

    :goto_f
    if-eqz v4, :cond_4

    goto/32 :goto_d

    :cond_4
    goto/32 :goto_5f

    nop

    :goto_10
    move p3, v5

    goto/32 :goto_18

    nop

    :goto_11
    const/16 v1, -0x3e

    goto/32 :goto_63

    nop

    :goto_12
    if-eq v0, v1, :cond_5

    goto/32 :goto_61

    :cond_5
    goto/32 :goto_60

    nop

    :goto_13
    if-ge v5, p4, :cond_6

    goto/32 :goto_2b

    :cond_6
    goto/32 :goto_21

    nop

    :goto_14
    shl-int/lit8 v5, v0, 0x1c

    goto/32 :goto_55

    nop

    :goto_15
    invoke-static {v0, v4}, Lcom/android/framework/protobuf/Utf8;->access$000(II)I

    move-result p3

    goto/32 :goto_9

    nop

    :goto_16
    return p1

    :goto_17
    goto/32 :goto_3d

    nop

    :goto_18
    goto/16 :goto_5c

    :goto_19
    goto/32 :goto_49

    nop

    :goto_1a
    const/4 v4, 0x0

    goto/32 :goto_1c

    nop

    :goto_1b
    move p3, v1

    goto/32 :goto_40

    nop

    :goto_1c
    if-eqz v1, :cond_7

    goto/32 :goto_19

    :cond_7
    goto/32 :goto_6

    nop

    :goto_1d
    shr-int/lit8 v4, p1, 0x8

    goto/32 :goto_3a

    nop

    :goto_1e
    return v2

    :goto_1f
    goto/32 :goto_24

    nop

    :goto_20
    if-eq v0, v1, :cond_8

    goto/32 :goto_35

    :cond_8
    goto/32 :goto_34

    nop

    :goto_21
    invoke-static {v0, v1}, Lcom/android/framework/protobuf/Utf8;->access$000(II)I

    move-result p3

    goto/32 :goto_2a

    nop

    :goto_22
    if-le v4, v3, :cond_9

    goto/32 :goto_53

    :cond_9
    goto/32 :goto_57

    nop

    :goto_23
    move p3, v5

    goto/32 :goto_64

    nop

    :goto_24
    invoke-static {p2, p3, p4}, Lcom/android/framework/protobuf/Utf8$Processor;->partialIsValidUtf8(Ljava/nio/ByteBuffer;II)I

    move-result v0

    goto/32 :goto_51

    nop

    :goto_25
    invoke-virtual {p2, p3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result p3

    goto/32 :goto_4b

    nop

    :goto_26
    if-le v1, v3, :cond_a

    goto/32 :goto_53

    :cond_a
    goto/32 :goto_14

    nop

    :goto_27
    const/16 v3, -0x41

    goto/32 :goto_7

    nop

    :goto_28
    if-eqz v5, :cond_b

    goto/32 :goto_53

    :cond_b
    goto/32 :goto_22

    nop

    :goto_29
    if-ge v5, p4, :cond_c

    goto/32 :goto_3f

    :cond_c
    goto/32 :goto_8

    nop

    :goto_2a
    return p3

    :goto_2b
    goto/32 :goto_10

    nop

    :goto_2c
    const/16 v4, -0x10

    goto/32 :goto_30

    nop

    :goto_2d
    goto :goto_33

    :goto_2e
    goto/32 :goto_39

    nop

    :goto_2f
    move p3, v5

    goto/32 :goto_52

    nop

    :goto_30
    if-lt v0, v4, :cond_d

    goto/32 :goto_48

    :cond_d
    goto/32 :goto_1d

    nop

    :goto_31
    if-nez p1, :cond_e

    goto/32 :goto_1f

    :cond_e
    goto/32 :goto_4c

    nop

    :goto_32
    goto/16 :goto_1f

    :goto_33
    goto/32 :goto_3b

    nop

    :goto_34
    if-lt v4, v5, :cond_f

    goto/32 :goto_43

    :cond_f
    :goto_35
    goto/32 :goto_1

    nop

    :goto_36
    not-int v1, v1

    goto/32 :goto_4

    nop

    :goto_37
    int-to-byte v4, v4

    goto/32 :goto_58

    nop

    :goto_38
    const/16 v1, -0x20

    goto/32 :goto_46

    nop

    :goto_39
    move p3, v1

    goto/32 :goto_32

    nop

    :goto_3a
    not-int v4, v4

    goto/32 :goto_37

    nop

    :goto_3b
    return v2

    :goto_3c
    goto/32 :goto_2c

    nop

    :goto_3d
    int-to-byte v0, p1

    goto/32 :goto_38

    nop

    :goto_3e
    return p3

    :goto_3f
    goto/32 :goto_c

    nop

    :goto_40
    goto :goto_43

    :goto_41
    goto/32 :goto_0

    nop

    :goto_42
    goto/16 :goto_1f

    :goto_43
    goto/32 :goto_47

    nop

    :goto_44
    add-int/lit8 v5, p3, 0x1

    goto/32 :goto_4f

    nop

    :goto_45
    shr-int/lit8 v1, p1, 0x8

    goto/32 :goto_36

    nop

    :goto_46
    const/4 v2, -0x1

    goto/32 :goto_27

    nop

    :goto_47
    return v2

    :goto_48
    goto/32 :goto_45

    nop

    :goto_49
    shr-int/lit8 v5, p1, 0x10

    goto/32 :goto_5b

    nop

    :goto_4a
    add-int/2addr v5, v6

    goto/32 :goto_54

    nop

    :goto_4b
    if-gt p3, v3, :cond_10

    goto/32 :goto_41

    :cond_10
    goto/32 :goto_1b

    nop

    :goto_4c
    if-ge p3, p4, :cond_11

    goto/32 :goto_17

    :cond_11
    goto/32 :goto_16

    nop

    :goto_4d
    move p3, v5

    :goto_4e
    goto/32 :goto_e

    nop

    :goto_4f
    invoke-virtual {p2, p3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    goto/32 :goto_b

    nop

    :goto_50
    invoke-virtual {p2, p3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    goto/32 :goto_13

    nop

    :goto_51
    return v0

    :goto_52
    goto/16 :goto_1f

    :goto_53
    goto/32 :goto_1e

    nop

    :goto_54
    shr-int/lit8 v5, v5, 0x1e

    goto/32 :goto_28

    nop

    :goto_55
    add-int/lit8 v6, v1, 0x70

    goto/32 :goto_4a

    nop

    :goto_56
    const/16 v5, -0x60

    goto/32 :goto_12

    nop

    :goto_57
    add-int/lit8 v5, p3, 0x1

    goto/32 :goto_3

    nop

    :goto_58
    if-eqz v4, :cond_12

    goto/32 :goto_4e

    :cond_12
    goto/32 :goto_44

    nop

    :goto_59
    invoke-virtual {p2, p3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    goto/32 :goto_29

    nop

    :goto_5a
    if-gt p3, v3, :cond_13

    goto/32 :goto_65

    :cond_13
    goto/32 :goto_23

    nop

    :goto_5b
    int-to-byte v4, v5

    :goto_5c
    goto/32 :goto_f

    nop

    :goto_5d
    invoke-virtual {p2, p3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result p3

    goto/32 :goto_5

    nop

    :goto_5e
    add-int/lit8 v1, p3, 0x1

    goto/32 :goto_5d

    nop

    :goto_5f
    add-int/lit8 v5, p3, 0x1

    goto/32 :goto_59

    nop

    :goto_60
    if-ge v4, v5, :cond_14

    goto/32 :goto_43

    :cond_14
    :goto_61
    goto/32 :goto_2

    nop

    :goto_62
    move p3, v1

    goto/32 :goto_2d

    nop

    :goto_63
    if-ge v0, v1, :cond_15

    goto/32 :goto_33

    :cond_15
    goto/32 :goto_5e

    nop

    :goto_64
    goto :goto_53

    :goto_65
    goto/32 :goto_2f

    nop
.end method

.method abstract partialIsValidUtf8Direct(ILjava/nio/ByteBuffer;II)I
.end method
