.class final Lcom/android/framework/protobuf/ListFieldSchema$ListFieldSchemaLite;
.super Lcom/android/framework/protobuf/ListFieldSchema;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/framework/protobuf/ListFieldSchema;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ListFieldSchemaLite"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/framework/protobuf/ListFieldSchema;-><init>(Lcom/android/framework/protobuf/ListFieldSchema$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/framework/protobuf/ListFieldSchema$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/framework/protobuf/ListFieldSchema$ListFieldSchemaLite;-><init>()V

    return-void
.end method

.method static getProtobufList(Ljava/lang/Object;J)Lcom/android/framework/protobuf/Internal$ProtobufList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Object;",
            "J)",
            "Lcom/android/framework/protobuf/Internal$ProtobufList<",
            "TE;>;"
        }
    .end annotation

    invoke-static {p0, p1, p2}, Lcom/android/framework/protobuf/UnsafeUtil;->getObject(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/framework/protobuf/Internal$ProtobufList;

    return-object v0
.end method


# virtual methods
.method makeImmutableListAt(Ljava/lang/Object;J)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    invoke-static {p1, p2, p3}, Lcom/android/framework/protobuf/ListFieldSchema$ListFieldSchemaLite;->getProtobufList(Ljava/lang/Object;J)Lcom/android/framework/protobuf/Internal$ProtobufList;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    invoke-interface {v0}, Lcom/android/framework/protobuf/Internal$ProtobufList;->makeImmutable()V

    goto/32 :goto_1

    nop
.end method

.method mergeListsAt(Ljava/lang/Object;Ljava/lang/Object;J)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "J)V"
        }
    .end annotation

    goto/32 :goto_14

    nop

    :goto_0
    if-gtz v2, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_11

    nop

    :goto_1
    invoke-static {p2, p3, p4}, Lcom/android/framework/protobuf/ListFieldSchema$ListFieldSchemaLite;->getProtobufList(Ljava/lang/Object;J)Lcom/android/framework/protobuf/Internal$ProtobufList;

    move-result-object v1

    goto/32 :goto_9

    nop

    :goto_2
    return-void

    :goto_3
    invoke-static {p1, p3, p4, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putObject(Ljava/lang/Object;JLjava/lang/Object;)V

    goto/32 :goto_2

    nop

    :goto_4
    add-int v4, v2, v3

    goto/32 :goto_a

    nop

    :goto_5
    invoke-interface {v0}, Lcom/android/framework/protobuf/Internal$ProtobufList;->isModifiable()Z

    move-result v4

    goto/32 :goto_e

    nop

    :goto_6
    move-object v4, v1

    :goto_7
    goto/32 :goto_3

    nop

    :goto_8
    if-gtz v2, :cond_1

    goto/32 :goto_13

    :cond_1
    goto/32 :goto_10

    nop

    :goto_9
    invoke-interface {v0}, Lcom/android/framework/protobuf/Internal$ProtobufList;->size()I

    move-result v2

    goto/32 :goto_f

    nop

    :goto_a
    invoke-interface {v0, v4}, Lcom/android/framework/protobuf/Internal$ProtobufList;->mutableCopyWithCapacity(I)Lcom/android/framework/protobuf/Internal$ProtobufList;

    move-result-object v0

    :goto_b
    goto/32 :goto_c

    nop

    :goto_c
    invoke-interface {v0, v1}, Lcom/android/framework/protobuf/Internal$ProtobufList;->addAll(Ljava/util/Collection;)Z

    :goto_d
    goto/32 :goto_8

    nop

    :goto_e
    if-eqz v4, :cond_2

    goto/32 :goto_b

    :cond_2
    goto/32 :goto_4

    nop

    :goto_f
    invoke-interface {v1}, Lcom/android/framework/protobuf/Internal$ProtobufList;->size()I

    move-result v3

    goto/32 :goto_0

    nop

    :goto_10
    move-object v4, v0

    goto/32 :goto_12

    nop

    :goto_11
    if-gtz v3, :cond_3

    goto/32 :goto_d

    :cond_3
    goto/32 :goto_5

    nop

    :goto_12
    goto :goto_7

    :goto_13
    goto/32 :goto_6

    nop

    :goto_14
    invoke-static {p1, p3, p4}, Lcom/android/framework/protobuf/ListFieldSchema$ListFieldSchemaLite;->getProtobufList(Ljava/lang/Object;J)Lcom/android/framework/protobuf/Internal$ProtobufList;

    move-result-object v0

    goto/32 :goto_1

    nop
.end method

.method mutableListAt(Ljava/lang/Object;J)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<",
            "L:Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Object;",
            "J)",
            "Ljava/util/List<",
            "T",
            "L;",
            ">;"
        }
    .end annotation

    goto/32 :goto_8

    nop

    :goto_0
    return-object v0

    :goto_1
    invoke-interface {v0, v2}, Lcom/android/framework/protobuf/Internal$ProtobufList;->mutableCopyWithCapacity(I)Lcom/android/framework/protobuf/Internal$ProtobufList;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_2
    invoke-static {p1, p2, p3, v0}, Lcom/android/framework/protobuf/UnsafeUtil;->putObject(Ljava/lang/Object;JLjava/lang/Object;)V

    :goto_3
    goto/32 :goto_0

    nop

    :goto_4
    if-eqz v1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_b

    nop

    :goto_5
    goto :goto_d

    :goto_6
    goto/32 :goto_c

    nop

    :goto_7
    invoke-interface {v0}, Lcom/android/framework/protobuf/Internal$ProtobufList;->isModifiable()Z

    move-result v1

    goto/32 :goto_4

    nop

    :goto_8
    invoke-static {p1, p2, p3}, Lcom/android/framework/protobuf/ListFieldSchema$ListFieldSchemaLite;->getProtobufList(Ljava/lang/Object;J)Lcom/android/framework/protobuf/Internal$ProtobufList;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_9
    const/16 v2, 0xa

    goto/32 :goto_5

    nop

    :goto_a
    if-eqz v1, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_9

    nop

    :goto_b
    invoke-interface {v0}, Lcom/android/framework/protobuf/Internal$ProtobufList;->size()I

    move-result v1

    nop

    goto/32 :goto_a

    nop

    :goto_c
    mul-int/lit8 v2, v1, 0x2

    :goto_d
    goto/32 :goto_1

    nop
.end method
