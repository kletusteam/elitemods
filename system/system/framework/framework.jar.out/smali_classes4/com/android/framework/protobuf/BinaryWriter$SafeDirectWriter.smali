.class final Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;
.super Lcom/android/framework/protobuf/BinaryWriter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/framework/protobuf/BinaryWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "SafeDirectWriter"
.end annotation


# instance fields
.field private buffer:Ljava/nio/ByteBuffer;

.field private limitMinusOne:I

.field private pos:I


# direct methods
.method constructor <init>(Lcom/android/framework/protobuf/BufferAllocator;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/framework/protobuf/BinaryWriter;-><init>(Lcom/android/framework/protobuf/BufferAllocator;ILcom/android/framework/protobuf/BinaryWriter$1;)V

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->nextBuffer()V

    return-void
.end method

.method private bytesWrittenToCurrentBuffer()I
    .locals 2

    iget v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->limitMinusOne:I

    iget v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    sub-int/2addr v0, v1

    return v0
.end method

.method private nextBuffer()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->newDirectBuffer()Lcom/android/framework/protobuf/AllocatedBuffer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->nextBuffer(Lcom/android/framework/protobuf/AllocatedBuffer;)V

    return-void
.end method

.method private nextBuffer(I)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->newDirectBuffer(I)Lcom/android/framework/protobuf/AllocatedBuffer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->nextBuffer(Lcom/android/framework/protobuf/AllocatedBuffer;)V

    return-void
.end method

.method private nextBuffer(Lcom/android/framework/protobuf/AllocatedBuffer;)V
    .locals 3

    invoke-virtual {p1}, Lcom/android/framework/protobuf/AllocatedBuffer;->hasNioBuffer()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/android/framework/protobuf/AllocatedBuffer;->nioBuffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->isDirect()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->finishCurrentBuffer()V

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffers:Ljava/util/ArrayDeque;

    invoke-virtual {v1, p1}, Ljava/util/ArrayDeque;->addFirst(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->limitMinusOne:I

    iput v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    return-void

    :cond_0
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Allocator returned non-direct buffer"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Allocated buffer does not have NIO buffer"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private spaceLeft()I
    .locals 1

    iget v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private writeVarint32FiveBytes(I)V
    .locals 4

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    ushr-int/lit8 v2, p1, 0x1c

    int-to-byte v2, v2

    invoke-virtual {v0, v1, v2}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    iget v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    add-int/lit8 v0, v0, -0x4

    iput v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v0, v0, 0x1

    ushr-int/lit8 v2, p1, 0x15

    and-int/lit8 v2, v2, 0x7f

    or-int/lit16 v2, v2, 0x80

    shl-int/lit8 v2, v2, 0x18

    ushr-int/lit8 v3, p1, 0xe

    and-int/lit8 v3, v3, 0x7f

    or-int/lit16 v3, v3, 0x80

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    ushr-int/lit8 v3, p1, 0x7

    and-int/lit8 v3, v3, 0x7f

    or-int/lit16 v3, v3, 0x80

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    and-int/lit8 v3, p1, 0x7f

    or-int/lit16 v3, v3, 0x80

    or-int/2addr v2, v3

    invoke-virtual {v1, v0, v2}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    return-void
.end method

.method private writeVarint32FourBytes(I)V
    .locals 5

    iget v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    add-int/lit8 v0, v0, -0x4

    iput v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v0, v0, 0x1

    const/high16 v2, 0xfe00000

    and-int/2addr v2, p1

    shl-int/lit8 v2, v2, 0x3

    const v3, 0x1fc000

    and-int/2addr v3, p1

    const/high16 v4, 0x200000

    or-int/2addr v3, v4

    shl-int/lit8 v3, v3, 0x2

    or-int/2addr v2, v3

    and-int/lit16 v3, p1, 0x3f80

    or-int/lit16 v3, v3, 0x4000

    shl-int/lit8 v3, v3, 0x1

    or-int/2addr v2, v3

    and-int/lit8 v3, p1, 0x7f

    or-int/lit16 v3, v3, 0x80

    or-int/2addr v2, v3

    invoke-virtual {v1, v0, v2}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    return-void
.end method

.method private writeVarint32OneByte(I)V
    .locals 3

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    int-to-byte v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    return-void
.end method

.method private writeVarint32ThreeBytes(I)V
    .locals 4

    iget v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    add-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    const v2, 0x1fc000

    and-int/2addr v2, p1

    shl-int/lit8 v2, v2, 0xa

    and-int/lit16 v3, p1, 0x3f80

    or-int/lit16 v3, v3, 0x4000

    shl-int/lit8 v3, v3, 0x9

    or-int/2addr v2, v3

    and-int/lit8 v3, p1, 0x7f

    or-int/lit16 v3, v3, 0x80

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    invoke-virtual {v1, v0, v2}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    return-void
.end method

.method private writeVarint32TwoBytes(I)V
    .locals 4

    iget v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    add-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v0, v0, 0x1

    and-int/lit16 v2, p1, 0x3f80

    shl-int/lit8 v2, v2, 0x1

    and-int/lit8 v3, p1, 0x7f

    or-int/lit16 v3, v3, 0x80

    or-int/2addr v2, v3

    int-to-short v2, v2

    invoke-virtual {v1, v0, v2}, Ljava/nio/ByteBuffer;->putShort(IS)Ljava/nio/ByteBuffer;

    return-void
.end method

.method private writeVarint64EightBytes(J)V
    .locals 9

    iget v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    add-int/lit8 v0, v0, -0x8

    iput v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    const/4 v2, 0x1

    add-int/2addr v0, v2

    const-wide/high16 v3, 0xfe000000000000L

    and-long/2addr v3, p1

    const/4 v5, 0x7

    shl-long/2addr v3, v5

    const-wide v5, 0x1fc0000000000L

    and-long/2addr v5, p1

    const-wide/high16 v7, 0x2000000000000L

    or-long/2addr v5, v7

    const/4 v7, 0x6

    shl-long/2addr v5, v7

    or-long/2addr v3, v5

    const-wide v5, 0x3f800000000L

    and-long/2addr v5, p1

    const-wide v7, 0x40000000000L

    or-long/2addr v5, v7

    const/4 v7, 0x5

    shl-long/2addr v5, v7

    or-long/2addr v3, v5

    const-wide v5, 0x7f0000000L

    and-long/2addr v5, p1

    const-wide v7, 0x800000000L

    or-long/2addr v5, v7

    const/4 v7, 0x4

    shl-long/2addr v5, v7

    or-long/2addr v3, v5

    const-wide/32 v5, 0xfe00000

    and-long/2addr v5, p1

    const-wide/32 v7, 0x10000000

    or-long/2addr v5, v7

    const/4 v7, 0x3

    shl-long/2addr v5, v7

    or-long/2addr v3, v5

    const-wide/32 v5, 0x1fc000

    and-long/2addr v5, p1

    const-wide/32 v7, 0x200000

    or-long/2addr v5, v7

    const/4 v7, 0x2

    shl-long/2addr v5, v7

    or-long/2addr v3, v5

    const-wide/16 v5, 0x3f80

    and-long/2addr v5, p1

    const-wide/16 v7, 0x4000

    or-long/2addr v5, v7

    shl-long/2addr v5, v2

    or-long v2, v3, v5

    const-wide/16 v4, 0x7f

    and-long/2addr v4, p1

    const-wide/16 v6, 0x80

    or-long/2addr v4, v6

    or-long/2addr v2, v4

    invoke-virtual {v1, v0, v2, v3}, Ljava/nio/ByteBuffer;->putLong(IJ)Ljava/nio/ByteBuffer;

    return-void
.end method

.method private writeVarint64EightBytesWithSign(J)V
    .locals 9

    iget v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    add-int/lit8 v0, v0, -0x8

    iput v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    const/4 v2, 0x1

    add-int/2addr v0, v2

    const-wide/high16 v3, 0xfe000000000000L

    and-long/2addr v3, p1

    const-wide/high16 v5, 0x100000000000000L

    or-long/2addr v3, v5

    const/4 v5, 0x7

    shl-long/2addr v3, v5

    const-wide v5, 0x1fc0000000000L

    and-long/2addr v5, p1

    const-wide/high16 v7, 0x2000000000000L

    or-long/2addr v5, v7

    const/4 v7, 0x6

    shl-long/2addr v5, v7

    or-long/2addr v3, v5

    const-wide v5, 0x3f800000000L

    and-long/2addr v5, p1

    const-wide v7, 0x40000000000L

    or-long/2addr v5, v7

    const/4 v7, 0x5

    shl-long/2addr v5, v7

    or-long/2addr v3, v5

    const-wide v5, 0x7f0000000L

    and-long/2addr v5, p1

    const-wide v7, 0x800000000L

    or-long/2addr v5, v7

    const/4 v7, 0x4

    shl-long/2addr v5, v7

    or-long/2addr v3, v5

    const-wide/32 v5, 0xfe00000

    and-long/2addr v5, p1

    const-wide/32 v7, 0x10000000

    or-long/2addr v5, v7

    const/4 v7, 0x3

    shl-long/2addr v5, v7

    or-long/2addr v3, v5

    const-wide/32 v5, 0x1fc000

    and-long/2addr v5, p1

    const-wide/32 v7, 0x200000

    or-long/2addr v5, v7

    const/4 v7, 0x2

    shl-long/2addr v5, v7

    or-long/2addr v3, v5

    const-wide/16 v5, 0x3f80

    and-long/2addr v5, p1

    const-wide/16 v7, 0x4000

    or-long/2addr v5, v7

    shl-long/2addr v5, v2

    or-long v2, v3, v5

    const-wide/16 v4, 0x7f

    and-long/2addr v4, p1

    const-wide/16 v6, 0x80

    or-long/2addr v4, v6

    or-long/2addr v2, v4

    invoke-virtual {v1, v0, v2, v3}, Ljava/nio/ByteBuffer;->putLong(IJ)Ljava/nio/ByteBuffer;

    return-void
.end method

.method private writeVarint64FiveBytes(J)V
    .locals 8

    iget v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    add-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v0, v0, -0x2

    const-wide v2, 0x7f0000000L

    and-long/2addr v2, p1

    const/16 v4, 0x1c

    shl-long/2addr v2, v4

    const-wide/32 v4, 0xfe00000

    and-long/2addr v4, p1

    const-wide/32 v6, 0x10000000

    or-long/2addr v4, v6

    const/16 v6, 0x1b

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    const-wide/32 v4, 0x1fc000

    and-long/2addr v4, p1

    const-wide/32 v6, 0x200000

    or-long/2addr v4, v6

    const/16 v6, 0x1a

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    const-wide/16 v4, 0x3f80

    and-long/2addr v4, p1

    const-wide/16 v6, 0x4000

    or-long/2addr v4, v6

    const/16 v6, 0x19

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    const-wide/16 v4, 0x7f

    and-long/2addr v4, p1

    const-wide/16 v6, 0x80

    or-long/2addr v4, v6

    const/16 v6, 0x18

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    invoke-virtual {v1, v0, v2, v3}, Ljava/nio/ByteBuffer;->putLong(IJ)Ljava/nio/ByteBuffer;

    return-void
.end method

.method private writeVarint64FourBytes(J)V
    .locals 1

    long-to-int v0, p1

    invoke-direct {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint32FourBytes(I)V

    return-void
.end method

.method private writeVarint64NineBytes(J)V
    .locals 4

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    const/16 v2, 0x38

    ushr-long v2, p1, v2

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-virtual {v0, v1, v2}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    const-wide v0, 0xffffffffffffffL

    and-long/2addr v0, p1

    invoke-direct {p0, v0, v1}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint64EightBytesWithSign(J)V

    return-void
.end method

.method private writeVarint64OneByte(J)V
    .locals 1

    long-to-int v0, p1

    invoke-direct {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint32OneByte(I)V

    return-void
.end method

.method private writeVarint64SevenBytes(J)V
    .locals 8

    iget v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    add-int/lit8 v0, v0, -0x7

    iput v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    const-wide v2, 0x1fc0000000000L

    and-long/2addr v2, p1

    const/16 v4, 0xe

    shl-long/2addr v2, v4

    const-wide v4, 0x3f800000000L

    and-long/2addr v4, p1

    const-wide v6, 0x40000000000L

    or-long/2addr v4, v6

    const/16 v6, 0xd

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    const-wide v4, 0x7f0000000L

    and-long/2addr v4, p1

    const-wide v6, 0x800000000L

    or-long/2addr v4, v6

    const/16 v6, 0xc

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    const-wide/32 v4, 0xfe00000

    and-long/2addr v4, p1

    const-wide/32 v6, 0x10000000

    or-long/2addr v4, v6

    const/16 v6, 0xb

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    const-wide/32 v4, 0x1fc000

    and-long/2addr v4, p1

    const-wide/32 v6, 0x200000

    or-long/2addr v4, v6

    const/16 v6, 0xa

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    const-wide/16 v4, 0x3f80

    and-long/2addr v4, p1

    const-wide/16 v6, 0x4000

    or-long/2addr v4, v6

    const/16 v6, 0x9

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    const-wide/16 v4, 0x7f

    and-long/2addr v4, p1

    const-wide/16 v6, 0x80

    or-long/2addr v4, v6

    const/16 v6, 0x8

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    invoke-virtual {v1, v0, v2, v3}, Ljava/nio/ByteBuffer;->putLong(IJ)Ljava/nio/ByteBuffer;

    return-void
.end method

.method private writeVarint64SixBytes(J)V
    .locals 8

    iget v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    add-int/lit8 v0, v0, -0x6

    iput v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v0, v0, -0x1

    const-wide v2, 0x3f800000000L

    and-long/2addr v2, p1

    const/16 v4, 0x15

    shl-long/2addr v2, v4

    const-wide v4, 0x7f0000000L

    and-long/2addr v4, p1

    const-wide v6, 0x800000000L

    or-long/2addr v4, v6

    const/16 v6, 0x14

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    const-wide/32 v4, 0xfe00000

    and-long/2addr v4, p1

    const-wide/32 v6, 0x10000000

    or-long/2addr v4, v6

    const/16 v6, 0x13

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    const-wide/32 v4, 0x1fc000

    and-long/2addr v4, p1

    const-wide/32 v6, 0x200000

    or-long/2addr v4, v6

    const/16 v6, 0x12

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    const-wide/16 v4, 0x3f80

    and-long/2addr v4, p1

    const-wide/16 v6, 0x4000

    or-long/2addr v4, v6

    const/16 v6, 0x11

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    const-wide/16 v4, 0x7f

    and-long/2addr v4, p1

    const-wide/16 v6, 0x80

    or-long/2addr v4, v6

    const/16 v6, 0x10

    shl-long/2addr v4, v6

    or-long/2addr v2, v4

    invoke-virtual {v1, v0, v2, v3}, Ljava/nio/ByteBuffer;->putLong(IJ)Ljava/nio/ByteBuffer;

    return-void
.end method

.method private writeVarint64TenBytes(J)V
    .locals 6

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    const/16 v2, 0x3f

    ushr-long v2, p1, v2

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-virtual {v0, v1, v2}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    const/16 v2, 0x38

    ushr-long v2, p1, v2

    const-wide/16 v4, 0x7f

    and-long/2addr v2, v4

    const-wide/16 v4, 0x80

    or-long/2addr v2, v4

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-virtual {v0, v1, v2}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    const-wide v0, 0xffffffffffffffL

    and-long/2addr v0, p1

    invoke-direct {p0, v0, v1}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint64EightBytesWithSign(J)V

    return-void
.end method

.method private writeVarint64ThreeBytes(J)V
    .locals 1

    long-to-int v0, p1

    invoke-direct {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint32ThreeBytes(I)V

    return-void
.end method

.method private writeVarint64TwoBytes(J)V
    .locals 1

    long-to-int v0, p1

    invoke-direct {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint32TwoBytes(I)V

    return-void
.end method


# virtual methods
.method finishCurrentBuffer()V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->bytesWrittenToCurrentBuffer()I

    move-result v1

    goto/32 :goto_f

    nop

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_10

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    goto/32 :goto_c

    nop

    :goto_3
    iput v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_6

    nop

    :goto_4
    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    goto/32 :goto_5

    nop

    :goto_5
    iget v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_d

    nop

    :goto_6
    iput v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->limitMinusOne:I

    :goto_7
    goto/32 :goto_a

    nop

    :goto_8
    iget v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->totalDoneBytes:I

    goto/32 :goto_0

    nop

    :goto_9
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    goto/32 :goto_1

    nop

    :goto_a
    return-void

    :goto_b
    const/4 v0, 0x0

    goto/32 :goto_3

    nop

    :goto_c
    if-nez v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_8

    nop

    :goto_d
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_9

    nop

    :goto_e
    iput v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->totalDoneBytes:I

    goto/32 :goto_4

    nop

    :goto_f
    add-int/2addr v0, v1

    goto/32 :goto_e

    nop

    :goto_10
    iput-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    goto/32 :goto_b

    nop
.end method

.method public getTotalBytesWritten()I
    .locals 2

    iget v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->totalDoneBytes:I

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->bytesWrittenToCurrentBuffer()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method requireSpace(I)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->spaceLeft()I

    move-result v0

    goto/32 :goto_4

    nop

    :goto_1
    invoke-direct {p0, p1}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->nextBuffer(I)V

    :goto_2
    goto/32 :goto_3

    nop

    :goto_3
    return-void

    :goto_4
    if-lt v0, p1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop
.end method

.method public write(B)V
    .locals 3

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    invoke-virtual {v0, v1, p1}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    return-void
.end method

.method public write(Ljava/nio/ByteBuffer;)V
    .locals 3

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->spaceLeft()I

    move-result v1

    if-ge v1, v0, :cond_0

    invoke-direct {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->nextBuffer(I)V

    :cond_0
    iget v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    sub-int/2addr v1, v0

    iput v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    iget-object v2, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, p1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    return-void
.end method

.method public write([BII)V
    .locals 2

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->spaceLeft()I

    move-result v0

    if-ge v0, p3, :cond_0

    invoke-direct {p0, p3}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->nextBuffer(I)V

    :cond_0
    iget v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    sub-int/2addr v0, p3

    iput v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1, p2, p3}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    return-void
.end method

.method public writeBool(IZ)V
    .locals 1

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->requireSpace(I)V

    int-to-byte v0, p2

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->write(B)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method writeBool(Z)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->write(B)V

    goto/32 :goto_2

    nop

    :goto_1
    int-to-byte v0, p1

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method public writeBytes(ILcom/android/framework/protobuf/ByteString;)V
    .locals 2

    :try_start_0
    invoke-virtual {p2, p0}, Lcom/android/framework/protobuf/ByteString;->writeToReverse(Lcom/android/framework/protobuf/ByteOutput;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    nop

    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->requireSpace(I)V

    invoke-virtual {p2}, Lcom/android/framework/protobuf/ByteString;->size()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint32(I)V

    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeTag(II)V

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public writeEndGroup(I)V
    .locals 1

    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method writeFixed32(I)V
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_4

    nop

    :goto_1
    iput v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_3

    nop

    :goto_2
    return-void

    :goto_3
    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    goto/32 :goto_0

    nop

    :goto_4
    invoke-virtual {v1, v0, p1}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    goto/32 :goto_2

    nop

    :goto_5
    iget v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_6

    nop

    :goto_6
    add-int/lit8 v0, v0, -0x4

    goto/32 :goto_1

    nop
.end method

.method public writeFixed32(II)V
    .locals 1

    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->requireSpace(I)V

    invoke-virtual {p0, p2}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeFixed32(I)V

    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method public writeFixed64(IJ)V
    .locals 1

    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->requireSpace(I)V

    invoke-virtual {p0, p2, p3}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeFixed64(J)V

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method writeFixed64(J)V
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    invoke-virtual {v1, v0, p1, p2}, Ljava/nio/ByteBuffer;->putLong(IJ)Ljava/nio/ByteBuffer;

    goto/32 :goto_6

    nop

    :goto_1
    iput v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_3

    nop

    :goto_2
    add-int/lit8 v0, v0, -0x8

    goto/32 :goto_1

    nop

    :goto_3
    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    goto/32 :goto_4

    nop

    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_0

    nop

    :goto_5
    iget v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_2

    nop

    :goto_6
    return-void
.end method

.method public writeGroup(ILjava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeTag(II)V

    invoke-static {}, Lcom/android/framework/protobuf/Protobuf;->getInstance()Lcom/android/framework/protobuf/Protobuf;

    move-result-object v0

    invoke-virtual {v0, p2, p0}, Lcom/android/framework/protobuf/Protobuf;->writeTo(Ljava/lang/Object;Lcom/android/framework/protobuf/Writer;)V

    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method public writeGroup(ILjava/lang/Object;Lcom/android/framework/protobuf/Schema;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeTag(II)V

    invoke-interface {p3, p2, p0}, Lcom/android/framework/protobuf/Schema;->writeTo(Ljava/lang/Object;Lcom/android/framework/protobuf/Writer;)V

    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method writeInt32(I)V
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    invoke-virtual {p0, p1}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint32(I)V

    goto/32 :goto_6

    nop

    :goto_1
    invoke-virtual {p0, v0, v1}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint64(J)V

    :goto_2
    goto/32 :goto_4

    nop

    :goto_3
    if-gez p1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_0

    nop

    :goto_4
    return-void

    :goto_5
    int-to-long v0, p1

    goto/32 :goto_1

    nop

    :goto_6
    goto :goto_2

    :goto_7
    goto/32 :goto_5

    nop
.end method

.method public writeInt32(II)V
    .locals 1

    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->requireSpace(I)V

    invoke-virtual {p0, p2}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeInt32(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method public writeLazy(Ljava/nio/ByteBuffer;)V
    .locals 3

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->spaceLeft()I

    move-result v1

    if-ge v1, v0, :cond_0

    iget v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->totalDoneBytes:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->totalDoneBytes:I

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffers:Ljava/util/ArrayDeque;

    invoke-static {p1}, Lcom/android/framework/protobuf/AllocatedBuffer;->wrap(Ljava/nio/ByteBuffer;)Lcom/android/framework/protobuf/AllocatedBuffer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayDeque;->addFirst(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->nextBuffer()V

    return-void

    :cond_0
    iget v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    sub-int/2addr v1, v0

    iput v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    iget-object v2, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, p1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    return-void
.end method

.method public writeLazy([BII)V
    .locals 2

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->spaceLeft()I

    move-result v0

    if-ge v0, p3, :cond_0

    iget v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->totalDoneBytes:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->totalDoneBytes:I

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffers:Ljava/util/ArrayDeque;

    invoke-static {p1, p2, p3}, Lcom/android/framework/protobuf/AllocatedBuffer;->wrap([BII)Lcom/android/framework/protobuf/AllocatedBuffer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayDeque;->addFirst(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->nextBuffer()V

    return-void

    :cond_0
    iget v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    sub-int/2addr v0, p3

    iput v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1, p2, p3}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    return-void
.end method

.method public writeMessage(ILjava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->getTotalBytesWritten()I

    move-result v0

    invoke-static {}, Lcom/android/framework/protobuf/Protobuf;->getInstance()Lcom/android/framework/protobuf/Protobuf;

    move-result-object v1

    invoke-virtual {v1, p2, p0}, Lcom/android/framework/protobuf/Protobuf;->writeTo(Ljava/lang/Object;Lcom/android/framework/protobuf/Writer;)V

    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->getTotalBytesWritten()I

    move-result v1

    sub-int/2addr v1, v0

    const/16 v2, 0xa

    invoke-virtual {p0, v2}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->requireSpace(I)V

    invoke-virtual {p0, v1}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint32(I)V

    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method public writeMessage(ILjava/lang/Object;Lcom/android/framework/protobuf/Schema;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->getTotalBytesWritten()I

    move-result v0

    invoke-interface {p3, p2, p0}, Lcom/android/framework/protobuf/Schema;->writeTo(Ljava/lang/Object;Lcom/android/framework/protobuf/Writer;)V

    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->getTotalBytesWritten()I

    move-result v1

    sub-int/2addr v1, v0

    const/16 v2, 0xa

    invoke-virtual {p0, v2}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->requireSpace(I)V

    invoke-virtual {p0, v1}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint32(I)V

    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method writeSInt32(I)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint32(I)V

    goto/32 :goto_2

    nop

    :goto_1
    invoke-static {p1}, Lcom/android/framework/protobuf/CodedOutputStream;->encodeZigZag32(I)I

    move-result v0

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method public writeSInt32(II)V
    .locals 1

    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->requireSpace(I)V

    invoke-virtual {p0, p2}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeSInt32(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method public writeSInt64(IJ)V
    .locals 1

    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->requireSpace(I)V

    invoke-virtual {p0, p2, p3}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeSInt64(J)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method writeSInt64(J)V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-static {p1, p2}, Lcom/android/framework/protobuf/CodedOutputStream;->encodeZigZag64(J)J

    move-result-wide v0

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {p0, v0, v1}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint64(J)V

    goto/32 :goto_0

    nop
.end method

.method public writeStartGroup(I)V
    .locals 1

    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method public writeString(ILjava/lang/String;)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->getTotalBytesWritten()I

    move-result v0

    invoke-virtual {p0, p2}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeString(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->getTotalBytesWritten()I

    move-result v1

    sub-int/2addr v1, v0

    const/16 v2, 0xa

    invoke-virtual {p0, v2}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->requireSpace(I)V

    invoke-virtual {p0, v1}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint32(I)V

    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method writeString(Ljava/lang/String;)V
    .locals 10

    goto/32 :goto_76

    nop

    :goto_0
    int-to-byte v9, v9

    goto/32 :goto_40

    nop

    :goto_1
    if-gez v0, :cond_0

    goto/32 :goto_44

    :cond_0
    goto/32 :goto_5a

    nop

    :goto_2
    invoke-direct {v1, v2, v0}, Lcom/android/framework/protobuf/Utf8$UnpairedSurrogateException;-><init>(II)V

    goto/32 :goto_25

    nop

    :goto_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    goto/32 :goto_12

    nop

    :goto_4
    if-lt v4, v2, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_7a

    nop

    :goto_5
    goto/16 :goto_4f

    :goto_6
    goto/32 :goto_1f

    nop

    :goto_7
    iget-object v7, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    goto/32 :goto_81

    nop

    :goto_8
    iput v9, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_2c

    nop

    :goto_9
    add-int/lit8 v9, v8, -0x1

    goto/32 :goto_6b

    nop

    :goto_a
    sub-int/2addr v2, v0

    goto/32 :goto_22

    nop

    :goto_b
    and-int/lit8 v7, v4, 0x3f

    goto/32 :goto_89

    nop

    :goto_c
    iput v7, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_70

    nop

    :goto_d
    iput v7, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_9b

    nop

    :goto_e
    if-lt v5, v4, :cond_2

    goto/32 :goto_3e

    :cond_2
    :goto_f
    goto/32 :goto_3c

    nop

    :goto_10
    const/4 v3, -0x1

    goto/32 :goto_64

    nop

    :goto_11
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    goto/32 :goto_4

    nop

    :goto_12
    const/4 v1, 0x1

    goto/32 :goto_69

    nop

    :goto_13
    iput v9, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_4b

    nop

    :goto_14
    iput v4, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    :goto_15
    goto/32 :goto_74

    nop

    :goto_16
    if-gt v5, v1, :cond_3

    goto/32 :goto_3e

    :cond_3
    goto/32 :goto_53

    nop

    :goto_17
    add-int/lit8 v7, v6, -0x1

    goto/32 :goto_c

    nop

    :goto_18
    move v5, v6

    goto/32 :goto_6a

    nop

    :goto_19
    add-int/lit8 v7, v6, -0x1

    goto/32 :goto_8c

    nop

    :goto_1a
    if-ge v4, v5, :cond_4

    goto/32 :goto_f

    :cond_4
    goto/32 :goto_21

    nop

    :goto_1b
    int-to-byte v7, v7

    goto/32 :goto_4a

    nop

    :goto_1c
    iget v6, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_38

    nop

    :goto_1d
    int-to-byte v9, v9

    goto/32 :goto_87

    nop

    :goto_1e
    invoke-virtual {v6, v5, v7}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    goto/32 :goto_34

    nop

    :goto_1f
    const/16 v5, 0x800

    goto/32 :goto_7f

    nop

    :goto_20
    add-int/lit8 v2, v0, -0x1

    goto/32 :goto_2

    nop

    :goto_21
    const v5, 0xdfff

    goto/32 :goto_e

    nop

    :goto_22
    iput v2, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    :goto_23
    goto/32 :goto_62

    nop

    :goto_24
    iget-object v7, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    goto/32 :goto_37

    nop

    :goto_25
    throw v1

    :goto_26
    goto/32 :goto_56

    nop

    :goto_27
    int-to-byte v9, v9

    goto/32 :goto_86

    nop

    :goto_28
    iget-object v7, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    goto/32 :goto_55

    nop

    :goto_29
    iget-object v5, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    goto/32 :goto_48

    nop

    :goto_2a
    iput v7, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_b

    nop

    :goto_2b
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_5d

    nop

    :goto_2c
    ushr-int/lit8 v9, v6, 0xc

    goto/32 :goto_7e

    nop

    :goto_2d
    if-nez v6, :cond_5

    goto/32 :goto_83

    :cond_5
    goto/32 :goto_2b

    nop

    :goto_2e
    invoke-virtual {v5, v6, v7}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    goto/32 :goto_3d

    nop

    :goto_2f
    or-int/lit16 v9, v9, 0xf0

    goto/32 :goto_0

    nop

    :goto_30
    add-int/2addr v4, v0

    goto/32 :goto_14

    nop

    :goto_31
    or-int/2addr v9, v2

    goto/32 :goto_1d

    nop

    :goto_32
    iput v7, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_58

    nop

    :goto_33
    iput v7, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_5c

    nop

    :goto_34
    iget-object v5, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    goto/32 :goto_1c

    nop

    :goto_35
    or-int/2addr v7, v2

    goto/32 :goto_99

    nop

    :goto_36
    iget-object v6, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    goto/32 :goto_73

    nop

    :goto_37
    iget v8, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_98

    nop

    :goto_38
    add-int/lit8 v7, v6, -0x1

    goto/32 :goto_d

    nop

    :goto_39
    iget v3, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_6f

    nop

    :goto_3a
    if-lt v3, v2, :cond_6

    goto/32 :goto_44

    :cond_6
    goto/32 :goto_54

    nop

    :goto_3b
    move v4, v3

    goto/32 :goto_3a

    nop

    :goto_3c
    iget v5, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_16

    nop

    :goto_3d
    goto :goto_4f

    :goto_3e
    goto/32 :goto_94

    nop

    :goto_3f
    const/4 v6, 0x2

    goto/32 :goto_93

    nop

    :goto_40
    invoke-virtual {v7, v8, v9}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    goto/32 :goto_82

    nop

    :goto_41
    sub-int/2addr v2, v1

    goto/32 :goto_96

    nop

    :goto_42
    iget-object v7, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    goto/32 :goto_85

    nop

    :goto_43
    goto/16 :goto_23

    :goto_44
    goto/32 :goto_10

    nop

    :goto_45
    int-to-byte v7, v7

    goto/32 :goto_1e

    nop

    :goto_46
    add-int/2addr v0, v3

    goto/32 :goto_65

    nop

    :goto_47
    invoke-virtual {v2, v3, v5}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    goto/32 :goto_57

    nop

    :goto_48
    iget v6, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_17

    nop

    :goto_49
    add-int/lit8 v7, v5, -0x1

    goto/32 :goto_2a

    nop

    :goto_4a
    invoke-virtual {v6, v5, v7}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    goto/32 :goto_7c

    nop

    :goto_4b
    ushr-int/lit8 v9, v6, 0x12

    goto/32 :goto_2f

    nop

    :goto_4c
    if-gez v5, :cond_7

    goto/32 :goto_6

    :cond_7
    goto/32 :goto_84

    nop

    :goto_4d
    int-to-byte v5, v4

    goto/32 :goto_47

    nop

    :goto_4e
    add-int/lit8 v0, v0, 0x1

    :goto_4f
    goto/32 :goto_46

    nop

    :goto_50
    if-gtz v5, :cond_8

    goto/32 :goto_79

    :cond_8
    goto/32 :goto_36

    nop

    :goto_51
    or-int/2addr v7, v2

    goto/32 :goto_45

    nop

    :goto_52
    or-int/2addr v9, v2

    goto/32 :goto_7d

    nop

    :goto_53
    iget-object v6, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    goto/32 :goto_49

    nop

    :goto_54
    iget-object v2, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    goto/32 :goto_39

    nop

    :goto_55
    iget v8, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_5b

    nop

    :goto_56
    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->requireSpace(I)V

    goto/32 :goto_4e

    nop

    :goto_57
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_43

    nop

    :goto_58
    and-int/lit8 v7, v4, 0x3f

    goto/32 :goto_51

    nop

    :goto_59
    ushr-int/lit8 v7, v4, 0x6

    goto/32 :goto_6e

    nop

    :goto_5a
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    goto/32 :goto_3b

    nop

    :goto_5b
    add-int/lit8 v9, v8, -0x1

    goto/32 :goto_13

    nop

    :goto_5c
    int-to-byte v7, v4

    goto/32 :goto_9a

    nop

    :goto_5d
    invoke-static {v5, v4}, Ljava/lang/Character;->toCodePoint(CC)I

    move-result v6

    goto/32 :goto_42

    nop

    :goto_5e
    iget v2, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_41

    nop

    :goto_5f
    or-int/2addr v9, v2

    goto/32 :goto_27

    nop

    :goto_60
    and-int/lit8 v9, v9, 0x3f

    goto/32 :goto_52

    nop

    :goto_61
    iget v5, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_50

    nop

    :goto_62
    const/16 v2, 0x80

    goto/32 :goto_1

    nop

    :goto_63
    invoke-virtual {v5, v6, v7}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    goto/32 :goto_78

    nop

    :goto_64
    if-eq v0, v3, :cond_9

    goto/32 :goto_91

    :cond_9
    goto/32 :goto_5e

    nop

    :goto_65
    goto/16 :goto_15

    :goto_66
    goto/32 :goto_6d

    nop

    :goto_67
    iput v9, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_8e

    nop

    :goto_68
    add-int/lit8 v6, v0, -0x1

    goto/32 :goto_72

    nop

    :goto_69
    sub-int/2addr v0, v1

    goto/32 :goto_9e

    nop

    :goto_6a
    invoke-static {v6, v4}, Ljava/lang/Character;->isSurrogatePair(CC)Z

    move-result v6

    goto/32 :goto_2d

    nop

    :goto_6b
    iput v9, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_92

    nop

    :goto_6c
    int-to-byte v7, v7

    goto/32 :goto_2e

    nop

    :goto_6d
    return-void

    :goto_6e
    and-int/lit8 v7, v7, 0x3f

    goto/32 :goto_35

    nop

    :goto_6f
    add-int/2addr v3, v0

    goto/32 :goto_4d

    nop

    :goto_70
    ushr-int/lit8 v7, v4, 0xc

    goto/32 :goto_77

    nop

    :goto_71
    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->requireSpace(I)V

    goto/32 :goto_3

    nop

    :goto_72
    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    goto/32 :goto_18

    nop

    :goto_73
    add-int/lit8 v7, v5, -0x1

    goto/32 :goto_32

    nop

    :goto_74
    if-gez v0, :cond_a

    goto/32 :goto_66

    :cond_a
    goto/32 :goto_11

    nop

    :goto_75
    const/4 v5, 0x0

    goto/32 :goto_8a

    nop

    :goto_76
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    goto/32 :goto_71

    nop

    :goto_77
    or-int/lit16 v7, v7, 0x1e0

    goto/32 :goto_6c

    nop

    :goto_78
    goto/16 :goto_4f

    :goto_79
    goto/32 :goto_97

    nop

    :goto_7a
    iget v5, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_4c

    nop

    :goto_7b
    add-int/lit8 v9, v8, -0x1

    goto/32 :goto_8

    nop

    :goto_7c
    iget-object v5, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    goto/32 :goto_8b

    nop

    :goto_7d
    int-to-byte v9, v9

    goto/32 :goto_88

    nop

    :goto_7e
    and-int/lit8 v9, v9, 0x3f

    goto/32 :goto_5f

    nop

    :goto_7f
    if-lt v4, v5, :cond_b

    goto/32 :goto_79

    :cond_b
    goto/32 :goto_61

    nop

    :goto_80
    new-instance v1, Lcom/android/framework/protobuf/Utf8$UnpairedSurrogateException;

    goto/32 :goto_20

    nop

    :goto_81
    iget v8, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_7b

    nop

    :goto_82
    goto/16 :goto_4f

    :goto_83
    goto/32 :goto_80

    nop

    :goto_84
    iget-object v6, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    goto/32 :goto_95

    nop

    :goto_85
    iget v8, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_9

    nop

    :goto_86
    invoke-virtual {v7, v8, v9}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    goto/32 :goto_28

    nop

    :goto_87
    invoke-virtual {v7, v8, v9}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    goto/32 :goto_24

    nop

    :goto_88
    invoke-virtual {v7, v8, v9}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    goto/32 :goto_7

    nop

    :goto_89
    or-int/2addr v7, v2

    goto/32 :goto_1b

    nop

    :goto_8a
    if-nez v0, :cond_c

    goto/32 :goto_83

    :cond_c
    goto/32 :goto_68

    nop

    :goto_8b
    iget v6, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_19

    nop

    :goto_8c
    iput v7, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_59

    nop

    :goto_8d
    or-int/lit16 v7, v7, 0x3c0

    goto/32 :goto_8f

    nop

    :goto_8e
    ushr-int/lit8 v9, v6, 0x6

    goto/32 :goto_60

    nop

    :goto_8f
    int-to-byte v7, v7

    goto/32 :goto_63

    nop

    :goto_90
    return-void

    :goto_91
    goto/32 :goto_9c

    nop

    :goto_92
    and-int/lit8 v9, v6, 0x3f

    goto/32 :goto_31

    nop

    :goto_93
    if-gt v5, v6, :cond_d

    goto/32 :goto_26

    :cond_d
    goto/32 :goto_75

    nop

    :goto_94
    iget v5, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_3f

    nop

    :goto_95
    add-int/lit8 v7, v5, -0x1

    goto/32 :goto_33

    nop

    :goto_96
    iput v2, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_90

    nop

    :goto_97
    const v5, 0xd800

    goto/32 :goto_1a

    nop

    :goto_98
    add-int/lit8 v9, v8, -0x1

    goto/32 :goto_67

    nop

    :goto_99
    int-to-byte v7, v7

    goto/32 :goto_9d

    nop

    :goto_9a
    invoke-virtual {v6, v5, v7}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    goto/32 :goto_5

    nop

    :goto_9b
    ushr-int/lit8 v7, v4, 0x6

    goto/32 :goto_8d

    nop

    :goto_9c
    iget v4, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_30

    nop

    :goto_9d
    invoke-virtual {v5, v6, v7}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    goto/32 :goto_29

    nop

    :goto_9e
    iget v2, p0, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->pos:I

    goto/32 :goto_a

    nop
.end method

.method writeTag(II)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    invoke-static {p1, p2}, Lcom/android/framework/protobuf/WireFormat;->makeTag(II)I

    move-result v0

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint32(I)V

    goto/32 :goto_1

    nop
.end method

.method public writeUInt32(II)V
    .locals 1

    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->requireSpace(I)V

    invoke-virtual {p0, p2}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint32(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method public writeUInt64(IJ)V
    .locals 1

    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->requireSpace(I)V

    invoke-virtual {p0, p2, p3}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint64(J)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method writeVarint32(I)V
    .locals 1

    goto/32 :goto_e

    nop

    :goto_0
    invoke-direct {p0, p1}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint32FourBytes(I)V

    goto/32 :goto_c

    nop

    :goto_1
    invoke-direct {p0, p1}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint32TwoBytes(I)V

    goto/32 :goto_5

    nop

    :goto_2
    and-int/lit16 v0, p1, -0x4000

    goto/32 :goto_4

    nop

    :goto_3
    invoke-direct {p0, p1}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint32OneByte(I)V

    goto/32 :goto_f

    nop

    :goto_4
    if-eqz v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_1

    nop

    :goto_5
    goto :goto_a

    :goto_6
    goto/32 :goto_17

    nop

    :goto_7
    invoke-direct {p0, p1}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint32ThreeBytes(I)V

    goto/32 :goto_15

    nop

    :goto_8
    if-eqz v0, :cond_1

    goto/32 :goto_10

    :cond_1
    goto/32 :goto_3

    nop

    :goto_9
    invoke-direct {p0, p1}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint32FiveBytes(I)V

    :goto_a
    goto/32 :goto_13

    nop

    :goto_b
    and-int/2addr v0, p1

    goto/32 :goto_12

    nop

    :goto_c
    goto :goto_a

    :goto_d
    goto/32 :goto_9

    nop

    :goto_e
    and-int/lit8 v0, p1, -0x80

    goto/32 :goto_8

    nop

    :goto_f
    goto :goto_a

    :goto_10
    goto/32 :goto_2

    nop

    :goto_11
    if-eqz v0, :cond_2

    goto/32 :goto_d

    :cond_2
    goto/32 :goto_0

    nop

    :goto_12
    if-eqz v0, :cond_3

    goto/32 :goto_16

    :cond_3
    goto/32 :goto_7

    nop

    :goto_13
    return-void

    :goto_14
    and-int/2addr v0, p1

    goto/32 :goto_11

    nop

    :goto_15
    goto :goto_a

    :goto_16
    goto/32 :goto_18

    nop

    :goto_17
    const/high16 v0, -0x200000

    goto/32 :goto_b

    nop

    :goto_18
    const/high16 v0, -0x10000000

    goto/32 :goto_14

    nop
.end method

.method writeVarint64(J)V
    .locals 1

    goto/32 :goto_a

    nop

    :goto_0
    goto :goto_2

    :pswitch_0
    goto/32 :goto_e

    nop

    :goto_1
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint64OneByte(J)V

    nop

    :goto_2
    goto/32 :goto_14

    nop

    :goto_3
    goto :goto_2

    :pswitch_1
    goto/32 :goto_4

    nop

    :goto_4
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint64NineBytes(J)V

    goto/32 :goto_11

    nop

    :goto_5
    goto :goto_2

    :pswitch_2
    goto/32 :goto_16

    nop

    :goto_6
    goto :goto_2

    :pswitch_3
    goto/32 :goto_10

    nop

    :goto_7
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint64SixBytes(J)V

    goto/32 :goto_0

    nop

    :goto_8
    goto :goto_2

    :pswitch_4
    goto/32 :goto_15

    nop

    :goto_9
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint64TwoBytes(J)V

    goto/32 :goto_b

    nop

    :goto_a
    invoke-static {p1, p2}, Lcom/android/framework/protobuf/BinaryWriter;->access$200(J)B

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/32 :goto_13

    nop

    :goto_b
    goto :goto_2

    :pswitch_5
    goto/32 :goto_1

    nop

    :goto_c
    goto :goto_2

    :pswitch_6
    goto/32 :goto_7

    nop

    :goto_d
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint64EightBytes(J)V

    goto/32 :goto_8

    nop

    :goto_e
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint64FiveBytes(J)V

    goto/32 :goto_5

    nop

    :goto_f
    goto :goto_2

    :pswitch_7
    goto/32 :goto_9

    nop

    :goto_10
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint64ThreeBytes(J)V

    goto/32 :goto_f

    nop

    :goto_11
    goto :goto_2

    :pswitch_8
    goto/32 :goto_d

    nop

    :goto_12
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint64TenBytes(J)V

    goto/32 :goto_3

    nop

    :goto_13
    goto :goto_2

    :pswitch_9
    goto/32 :goto_12

    nop

    :goto_14
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_7
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_6
        :pswitch_4
        :pswitch_8
        :pswitch_1
        :pswitch_9
    .end packed-switch

    :goto_15
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint64SevenBytes(J)V

    goto/32 :goto_c

    nop

    :goto_16
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$SafeDirectWriter;->writeVarint64FourBytes(J)V

    goto/32 :goto_6

    nop
.end method
