.class final Lcom/android/framework/protobuf/ExtensionSchemaLite;
.super Lcom/android/framework/protobuf/ExtensionSchema;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/framework/protobuf/ExtensionSchema<",
        "Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/framework/protobuf/ExtensionSchema;-><init>()V

    return-void
.end method


# virtual methods
.method extensionNumber(Ljava/util/Map$Entry;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry<",
            "**>;)I"
        }
    .end annotation

    nop

    goto/32 :goto_0

    nop

    :goto_0
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_1
    check-cast v0, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_3

    nop

    :goto_3
    return v1
.end method

.method findExtensionByNumber(Lcom/android/framework/protobuf/ExtensionRegistryLite;Lcom/android/framework/protobuf/MessageLite;I)Ljava/lang/Object;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    invoke-virtual {p1, p2, p3}, Lcom/android/framework/protobuf/ExtensionRegistryLite;->findLiteExtensionByNumber(Lcom/android/framework/protobuf/MessageLite;I)Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;

    move-result-object v0

    goto/32 :goto_0

    nop
.end method

.method getExtensions(Ljava/lang/Object;)Lcom/android/framework/protobuf/FieldSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lcom/android/framework/protobuf/FieldSet<",
            "Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;",
            ">;"
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    move-object v0, p1

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, v0, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtendableMessage;->extensions:Lcom/android/framework/protobuf/FieldSet;

    goto/32 :goto_3

    nop

    :goto_2
    check-cast v0, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtendableMessage;

    goto/32 :goto_1

    nop

    :goto_3
    return-object v0
.end method

.method getMutableExtensions(Ljava/lang/Object;)Lcom/android/framework/protobuf/FieldSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lcom/android/framework/protobuf/FieldSet<",
            "Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;",
            ">;"
        }
    .end annotation

    goto/32 :goto_3

    nop

    :goto_0
    check-cast v0, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtendableMessage;

    goto/32 :goto_2

    nop

    :goto_1
    return-object v0

    :goto_2
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtendableMessage;->ensureExtensionsAreMutable()Lcom/android/framework/protobuf/FieldSet;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_3
    move-object v0, p1

    goto/32 :goto_0

    nop
.end method

.method hasExtensions(Lcom/android/framework/protobuf/MessageLite;)Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    instance-of v0, p1, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtendableMessage;

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method makeImmutable(Ljava/lang/Object;)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {v0}, Lcom/android/framework/protobuf/FieldSet;->makeImmutable()V

    goto/32 :goto_2

    nop

    :goto_1
    invoke-virtual {p0, p1}, Lcom/android/framework/protobuf/ExtensionSchemaLite;->getExtensions(Ljava/lang/Object;)Lcom/android/framework/protobuf/FieldSet;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method parseExtension(Lcom/android/framework/protobuf/Reader;Ljava/lang/Object;Lcom/android/framework/protobuf/ExtensionRegistryLite;Lcom/android/framework/protobuf/FieldSet;Ljava/lang/Object;Lcom/android/framework/protobuf/UnknownFieldSchema;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<UT:",
            "Ljava/lang/Object;",
            "UB:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/android/framework/protobuf/Reader;",
            "Ljava/lang/Object;",
            "Lcom/android/framework/protobuf/ExtensionRegistryLite;",
            "Lcom/android/framework/protobuf/FieldSet<",
            "Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;",
            ">;TUB;",
            "Lcom/android/framework/protobuf/UnknownFieldSchema<",
            "TUT;TUB;>;)TUB;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_97

    nop

    :goto_0
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_a0

    nop

    :goto_1
    goto :goto_7

    :pswitch_0
    goto/32 :goto_63

    nop

    :goto_2
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;->getLiteType()Lcom/android/framework/protobuf/WireFormat$FieldType;

    move-result-object v4

    goto/32 :goto_26

    nop

    :goto_3
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/32 :goto_a6

    nop

    :goto_4
    goto :goto_7

    :pswitch_1
    goto/32 :goto_5

    nop

    :goto_5
    invoke-interface {p1}, Lcom/android/framework/protobuf/Reader;->readUInt32()I

    move-result v3

    goto/32 :goto_9e

    nop

    :goto_6
    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    nop

    :goto_7
    goto/32 :goto_23

    nop

    :goto_8
    goto/16 :goto_3e

    :pswitch_2
    goto/32 :goto_80

    nop

    :goto_9
    iget-object v2, v0, Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;->descriptor:Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;

    goto/32 :goto_25

    nop

    :goto_a
    goto/16 :goto_84

    :goto_b
    goto/32 :goto_7c

    nop

    :goto_c
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_59

    nop

    :goto_d
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_8e

    nop

    :goto_e
    goto/16 :goto_7

    :pswitch_3
    goto/32 :goto_a1

    nop

    :goto_f
    invoke-interface {p1, v3}, Lcom/android/framework/protobuf/Reader;->readFixed32List(Ljava/util/List;)V

    goto/32 :goto_60

    nop

    :goto_10
    invoke-interface {p1, v3}, Lcom/android/framework/protobuf/Reader;->readInt64List(Ljava/util/List;)V

    goto/32 :goto_90

    nop

    :goto_11
    iget-object v3, v0, Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;->descriptor:Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;

    goto/32 :goto_83

    nop

    :goto_12
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_f

    nop

    :goto_13
    iget-object v4, v0, Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;->descriptor:Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;

    goto/32 :goto_4d

    nop

    :goto_14
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_b6

    nop

    :goto_15
    goto :goto_7

    :pswitch_4
    goto/32 :goto_73

    nop

    :goto_16
    goto/16 :goto_3e

    :pswitch_5
    goto/32 :goto_4a

    nop

    :goto_17
    if-eqz v4, :cond_0

    goto/32 :goto_20

    :cond_0
    goto/32 :goto_43

    nop

    :goto_18
    if-nez v3, :cond_1

    goto/32 :goto_33

    :cond_1
    goto/32 :goto_32

    nop

    :goto_19
    move-object v2, v3

    goto/32 :goto_b3

    nop

    :goto_1a
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;->getNumber()I

    move-result v1

    goto/32 :goto_a9

    nop

    :goto_1b
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_8b

    nop

    :goto_1c
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    goto/32 :goto_a3

    nop

    :goto_1d
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_af

    nop

    :goto_1e
    goto/16 :goto_7

    :pswitch_6
    nop

    goto/32 :goto_9d

    nop

    :goto_1f
    return-object v5

    :goto_20
    goto/32 :goto_3c

    nop

    :goto_21
    invoke-interface {p1}, Lcom/android/framework/protobuf/Reader;->readInt32()I

    move-result v3

    goto/32 :goto_64

    nop

    :goto_22
    move-object v2, v3

    goto/32 :goto_8

    nop

    :goto_23
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;->isRepeated()Z

    move-result v3

    goto/32 :goto_5c

    nop

    :goto_24
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    goto/32 :goto_6a

    nop

    :goto_25
    invoke-virtual {v2}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->isPacked()Z

    move-result v2

    goto/32 :goto_89

    nop

    :goto_26
    invoke-virtual {v4}, Lcom/android/framework/protobuf/WireFormat$FieldType;->ordinal()I

    move-result v4

    goto/32 :goto_95

    nop

    :goto_27
    invoke-virtual {v2}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->isRepeated()Z

    move-result v2

    goto/32 :goto_4f

    nop

    :goto_28
    goto/16 :goto_3e

    :pswitch_7
    goto/32 :goto_c1

    nop

    :goto_29
    invoke-virtual {v4}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getEnumType()Lcom/android/framework/protobuf/Internal$EnumLiteMap;

    move-result-object v4

    goto/32 :goto_91

    nop

    :goto_2a
    goto/16 :goto_84

    :goto_2b
    goto/32 :goto_4b

    nop

    :goto_2c
    invoke-virtual {p4, v3, v2}, Lcom/android/framework/protobuf/FieldSet;->setField(Lcom/android/framework/protobuf/FieldSet$FieldDescriptorLite;Ljava/lang/Object;)V

    goto/32 :goto_2a

    nop

    :goto_2d
    new-instance v3, Ljava/util/ArrayList;

    goto/32 :goto_35

    nop

    :goto_2e
    move-object v2, v3

    goto/32 :goto_16

    nop

    :goto_2f
    goto/16 :goto_7

    :pswitch_8
    goto/32 :goto_45

    nop

    :goto_30
    goto :goto_3e

    :pswitch_9
    goto/32 :goto_8c

    nop

    :goto_31
    invoke-interface {p1}, Lcom/android/framework/protobuf/Reader;->readString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_e

    nop

    :goto_32
    invoke-static {v3, v2}, Lcom/android/framework/protobuf/Internal;->mergeMessage(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    :goto_33
    goto/32 :goto_11

    nop

    :goto_34
    invoke-virtual {v4}, Lcom/android/framework/protobuf/WireFormat$FieldType;->ordinal()I

    move-result v4

    goto/32 :goto_78

    nop

    :goto_35
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_70

    nop

    :goto_36
    invoke-interface {p1}, Lcom/android/framework/protobuf/Reader;->readFloat()F

    move-result v3

    goto/32 :goto_24

    nop

    :goto_37
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;->getLiteType()Lcom/android/framework/protobuf/WireFormat$FieldType;

    move-result-object v3

    goto/32 :goto_6b

    nop

    :goto_38
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_a8

    nop

    :goto_39
    goto/16 :goto_7

    :pswitch_a
    goto/32 :goto_56

    nop

    :goto_3a
    sget-object v3, Lcom/android/framework/protobuf/ExtensionSchemaLite$1;->$SwitchMap$com$google$protobuf$WireFormat$FieldType:[I

    goto/32 :goto_2

    nop

    :goto_3b
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_b9

    nop

    :goto_3c
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/32 :goto_99

    nop

    :goto_3d
    move-object v2, v3

    nop

    :goto_3e
    goto/32 :goto_71

    nop

    :goto_3f
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/32 :goto_2f

    nop

    :goto_40
    goto/16 :goto_7

    :pswitch_b
    goto/32 :goto_31

    nop

    :goto_41
    goto :goto_3e

    :pswitch_c
    goto/32 :goto_5a

    nop

    :goto_42
    invoke-interface {p1}, Lcom/android/framework/protobuf/Reader;->readUInt64()J

    move-result-wide v3

    goto/32 :goto_8f

    nop

    :goto_43
    invoke-static {v1, v3, p5, p6}, Lcom/android/framework/protobuf/SchemaUtil;->storeUnknownEnum(IILjava/lang/Object;Lcom/android/framework/protobuf/UnknownFieldSchema;)Ljava/lang/Object;

    move-result-object v5

    goto/32 :goto_1f

    nop

    :goto_44
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_be

    nop

    :goto_45
    invoke-interface {p1}, Lcom/android/framework/protobuf/Reader;->readSFixed64()J

    move-result-wide v3

    goto/32 :goto_9b

    nop

    :goto_46
    goto/16 :goto_7

    :pswitch_d
    goto/32 :goto_94

    nop

    :goto_47
    iget-object v3, v0, Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;->descriptor:Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;

    goto/32 :goto_93

    nop

    :goto_48
    goto/16 :goto_7

    :pswitch_e
    goto/32 :goto_bc

    nop

    :goto_49
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_c4

    nop

    :goto_4a
    new-instance v3, Ljava/util/ArrayList;

    goto/32 :goto_7e

    nop

    :goto_4b
    const/4 v2, 0x0

    goto/32 :goto_37

    nop

    :goto_4c
    new-instance v3, Ljava/util/ArrayList;

    goto/32 :goto_1b

    nop

    :goto_4d
    invoke-virtual {v4}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getEnumType()Lcom/android/framework/protobuf/Internal$EnumLiteMap;

    move-result-object v4

    goto/32 :goto_b2

    nop

    :goto_4e
    new-instance v3, Ljava/util/ArrayList;

    goto/32 :goto_d

    nop

    :goto_4f
    if-nez v2, :cond_2

    goto/32 :goto_2b

    :cond_2
    goto/32 :goto_9

    nop

    :goto_50
    move-object v2, v3

    goto/32 :goto_53

    nop

    :goto_51
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto/32 :goto_48

    nop

    :goto_52
    goto/16 :goto_3e

    :pswitch_f
    goto/32 :goto_7d

    nop

    :goto_53
    goto/16 :goto_3e

    :pswitch_10
    goto/32 :goto_86

    nop

    :goto_54
    check-cast v0, Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;

    goto/32 :goto_1a

    nop

    :goto_55
    new-instance v3, Ljava/util/ArrayList;

    goto/32 :goto_1d

    nop

    :goto_56
    invoke-interface {p1}, Lcom/android/framework/protobuf/Reader;->readSFixed32()I

    move-result v3

    goto/32 :goto_a2

    nop

    :goto_57
    invoke-interface {p1}, Lcom/android/framework/protobuf/Reader;->readInt32()I

    move-result v3

    goto/32 :goto_75

    nop

    :goto_58
    goto/16 :goto_3e

    :pswitch_11
    goto/32 :goto_68

    nop

    :goto_59
    throw v3

    :pswitch_12
    goto/32 :goto_96

    nop

    :goto_5a
    new-instance v3, Ljava/util/ArrayList;

    goto/32 :goto_b4

    nop

    :goto_5b
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto/32 :goto_69

    nop

    :goto_5c
    if-nez v3, :cond_3

    goto/32 :goto_b

    :cond_3
    goto/32 :goto_47

    nop

    :goto_5d
    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto/32 :goto_74

    nop

    :goto_5e
    invoke-interface {p1, v3}, Lcom/android/framework/protobuf/Reader;->readUInt64List(Ljava/util/List;)V

    goto/32 :goto_76

    nop

    :goto_5f
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_10

    nop

    :goto_60
    move-object v2, v3

    goto/32 :goto_41

    nop

    :goto_61
    const/4 v2, 0x0

    goto/32 :goto_7f

    nop

    :goto_62
    goto/16 :goto_3e

    :pswitch_13
    goto/32 :goto_55

    nop

    :goto_63
    invoke-interface {p1}, Lcom/android/framework/protobuf/Reader;->readFixed64()J

    move-result-wide v3

    goto/32 :goto_5b

    nop

    :goto_64
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/32 :goto_81

    nop

    :goto_65
    invoke-interface {p1}, Lcom/android/framework/protobuf/Reader;->readFixed32()I

    move-result v3

    goto/32 :goto_66

    nop

    :goto_66
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/32 :goto_1

    nop

    :goto_67
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_8a

    nop

    :goto_68
    new-instance v3, Ljava/util/ArrayList;

    goto/32 :goto_0

    nop

    :goto_69
    goto/16 :goto_7

    :pswitch_14
    goto/32 :goto_21

    nop

    :goto_6a
    goto/16 :goto_7

    :pswitch_15
    goto/32 :goto_ad

    nop

    :goto_6b
    sget-object v4, Lcom/android/framework/protobuf/WireFormat$FieldType;->ENUM:Lcom/android/framework/protobuf/WireFormat$FieldType;

    goto/32 :goto_6d

    nop

    :goto_6c
    goto/16 :goto_7

    :pswitch_16
    goto/32 :goto_36

    nop

    :goto_6d
    if-eq v3, v4, :cond_4

    goto/32 :goto_9a

    :cond_4
    goto/32 :goto_57

    nop

    :goto_6e
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;->getMessageDefaultInstance()Lcom/android/framework/protobuf/MessageLite;

    move-result-object v3

    goto/32 :goto_bb

    nop

    :goto_6f
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto/32 :goto_6c

    nop

    :goto_70
    invoke-interface {p1, v3}, Lcom/android/framework/protobuf/Reader;->readFloatList(Ljava/util/List;)V

    goto/32 :goto_b0

    nop

    :goto_71
    iget-object v3, v0, Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;->descriptor:Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;

    goto/32 :goto_2c

    nop

    :goto_72
    goto/16 :goto_3e

    :pswitch_17
    goto/32 :goto_4e

    nop

    :goto_73
    invoke-interface {p1}, Lcom/android/framework/protobuf/Reader;->readBool()Z

    move-result v3

    goto/32 :goto_3

    nop

    :goto_74
    new-instance v3, Ljava/lang/IllegalStateException;

    goto/32 :goto_9c

    nop

    :goto_75
    iget-object v4, v0, Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;->descriptor:Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;

    goto/32 :goto_29

    nop

    :goto_76
    move-object v2, v3

    goto/32 :goto_30

    nop

    :goto_77
    invoke-interface {p1, v3}, Lcom/android/framework/protobuf/Reader;->readSFixed32List(Ljava/util/List;)V

    goto/32 :goto_c3

    nop

    :goto_78
    aget v3, v3, v4

    packed-switch v3, :pswitch_data_2

    goto/32 :goto_82

    nop

    :goto_79
    invoke-virtual {v5}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getLiteType()Lcom/android/framework/protobuf/WireFormat$FieldType;

    move-result-object v5

    goto/32 :goto_49

    nop

    :goto_7a
    new-instance v3, Ljava/util/ArrayList;

    goto/32 :goto_14

    nop

    :goto_7b
    invoke-virtual {v4}, Lcom/android/framework/protobuf/WireFormat$FieldType;->ordinal()I

    move-result v4

    goto/32 :goto_5d

    nop

    :goto_7c
    sget-object v3, Lcom/android/framework/protobuf/ExtensionSchemaLite$1;->$SwitchMap$com$google$protobuf$WireFormat$FieldType:[I

    goto/32 :goto_b7

    nop

    :goto_7d
    new-instance v3, Ljava/util/ArrayList;

    goto/32 :goto_ab

    nop

    :goto_7e
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_77

    nop

    :goto_7f
    sget-object v3, Lcom/android/framework/protobuf/ExtensionSchemaLite$1;->$SwitchMap$com$google$protobuf$WireFormat$FieldType:[I

    goto/32 :goto_bd

    nop

    :goto_80
    new-instance v3, Ljava/util/ArrayList;

    goto/32 :goto_44

    nop

    :goto_81
    goto/16 :goto_7

    :pswitch_18
    goto/32 :goto_42

    nop

    :goto_82
    goto/16 :goto_33

    :pswitch_19
    goto/32 :goto_ba

    nop

    :goto_83
    invoke-virtual {p4, v3, v2}, Lcom/android/framework/protobuf/FieldSet;->setField(Lcom/android/framework/protobuf/FieldSet$FieldDescriptorLite;Ljava/lang/Object;)V

    :goto_84
    goto/32 :goto_8d

    nop

    :goto_85
    const-string v4, "Shouldn\'t reach here."

    goto/32 :goto_3b

    nop

    :goto_86
    new-instance v3, Ljava/util/ArrayList;

    goto/32 :goto_92

    nop

    :goto_87
    invoke-interface {p1, v3, p3}, Lcom/android/framework/protobuf/Reader;->readGroup(Ljava/lang/Class;Lcom/android/framework/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_40

    nop

    :goto_88
    invoke-interface {p1}, Lcom/android/framework/protobuf/Reader;->readInt64()J

    move-result-wide v3

    goto/32 :goto_6f

    nop

    :goto_89
    if-nez v2, :cond_5

    goto/32 :goto_2b

    :cond_5
    goto/32 :goto_61

    nop

    :goto_8a
    iget-object v5, v0, Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;->descriptor:Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;

    goto/32 :goto_79

    nop

    :goto_8b
    invoke-interface {p1, v3}, Lcom/android/framework/protobuf/Reader;->readInt32List(Ljava/util/List;)V

    goto/32 :goto_ac

    nop

    :goto_8c
    new-instance v3, Ljava/util/ArrayList;

    goto/32 :goto_5f

    nop

    :goto_8d
    return-object p5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_11
        :pswitch_1c
        :pswitch_9
        :pswitch_f
        :pswitch_1e
        :pswitch_c
        :pswitch_7
        :pswitch_10
        :pswitch_17
        :pswitch_5
        :pswitch_2
        :pswitch_1d
        :pswitch_13
        :pswitch_12
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_15
        :pswitch_16
        :pswitch_20
        :pswitch_18
        :pswitch_14
        :pswitch_0
        :pswitch_1b
        :pswitch_4
        :pswitch_1
        :pswitch_a
        :pswitch_8
        :pswitch_e
        :pswitch_1f
        :pswitch_d
        :pswitch_3
        :pswitch_b
        :pswitch_1a
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x11
        :pswitch_19
        :pswitch_19
    .end packed-switch

    :goto_8e
    invoke-interface {p1, v3}, Lcom/android/framework/protobuf/Reader;->readUInt32List(Ljava/util/List;)V

    goto/32 :goto_50

    nop

    :goto_8f
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto/32 :goto_c2

    nop

    :goto_90
    move-object v2, v3

    goto/32 :goto_a7

    nop

    :goto_91
    invoke-interface {v4, v3}, Lcom/android/framework/protobuf/Internal$EnumLiteMap;->findValueByNumber(I)Lcom/android/framework/protobuf/Internal$EnumLite;

    move-result-object v4

    goto/32 :goto_17

    nop

    :goto_92
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_ae

    nop

    :goto_93
    invoke-virtual {p4, v3, v2}, Lcom/android/framework/protobuf/FieldSet;->addRepeatedField(Lcom/android/framework/protobuf/FieldSet$FieldDescriptorLite;Ljava/lang/Object;)V

    goto/32 :goto_a

    nop

    :goto_94
    new-instance v3, Ljava/lang/IllegalStateException;

    goto/32 :goto_85

    nop

    :goto_95
    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    goto/32 :goto_1e

    nop

    :goto_96
    new-instance v3, Ljava/util/ArrayList;

    goto/32 :goto_38

    nop

    :goto_97
    move-object v0, p2

    goto/32 :goto_54

    nop

    :goto_98
    move-object v2, v3

    goto/32 :goto_28

    nop

    :goto_99
    goto/16 :goto_7

    :goto_9a
    goto/32 :goto_3a

    nop

    :goto_9b
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto/32 :goto_39

    nop

    :goto_9c
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_b1

    nop

    :goto_9d
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;->getMessageDefaultInstance()Lcom/android/framework/protobuf/MessageLite;

    move-result-object v3

    goto/32 :goto_1c

    nop

    :goto_9e
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/32 :goto_15

    nop

    :goto_9f
    invoke-interface {p1}, Lcom/android/framework/protobuf/Reader;->readSInt64()J

    move-result-wide v3

    goto/32 :goto_51

    nop

    :goto_a0
    invoke-interface {p1, v3}, Lcom/android/framework/protobuf/Reader;->readDoubleList(Ljava/util/List;)V

    goto/32 :goto_3d

    nop

    :goto_a1
    invoke-interface {p1}, Lcom/android/framework/protobuf/Reader;->readBytes()Lcom/android/framework/protobuf/ByteString;

    move-result-object v2

    goto/32 :goto_46

    nop

    :goto_a2
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/32 :goto_4

    nop

    :goto_a3
    invoke-interface {p1, v3, p3}, Lcom/android/framework/protobuf/Reader;->readMessage(Ljava/lang/Class;Lcom/android/framework/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_a4

    nop

    :goto_a4
    goto/16 :goto_7

    :pswitch_1a
    nop

    goto/32 :goto_6e

    nop

    :goto_a5
    invoke-interface {p1, v3}, Lcom/android/framework/protobuf/Reader;->readFixed64List(Ljava/util/List;)V

    goto/32 :goto_19

    nop

    :goto_a6
    goto/16 :goto_7

    :pswitch_1b
    goto/32 :goto_65

    nop

    :goto_a7
    goto/16 :goto_3e

    :pswitch_1c
    goto/32 :goto_2d

    nop

    :goto_a8
    invoke-interface {p1, v3}, Lcom/android/framework/protobuf/Reader;->readEnumList(Ljava/util/List;)V

    goto/32 :goto_13

    nop

    :goto_a9
    iget-object v2, v0, Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;->descriptor:Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;

    goto/32 :goto_27

    nop

    :goto_aa
    goto/16 :goto_3e

    :pswitch_1d
    goto/32 :goto_7a

    nop

    :goto_ab
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_5e

    nop

    :goto_ac
    move-object v2, v3

    goto/32 :goto_52

    nop

    :goto_ad
    invoke-interface {p1}, Lcom/android/framework/protobuf/Reader;->readDouble()D

    move-result-wide v3

    goto/32 :goto_6

    nop

    :goto_ae
    invoke-interface {p1, v3}, Lcom/android/framework/protobuf/Reader;->readBoolList(Ljava/util/List;)V

    goto/32 :goto_98

    nop

    :goto_af
    invoke-interface {p1, v3}, Lcom/android/framework/protobuf/Reader;->readSInt64List(Ljava/util/List;)V

    goto/32 :goto_bf

    nop

    :goto_b0
    move-object v2, v3

    goto/32 :goto_58

    nop

    :goto_b1
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_b8

    nop

    :goto_b2
    invoke-static {v1, v3, v4, p5, p6}, Lcom/android/framework/protobuf/SchemaUtil;->filterUnknownEnumList(ILjava/util/List;Lcom/android/framework/protobuf/Internal$EnumLiteMap;Ljava/lang/Object;Lcom/android/framework/protobuf/UnknownFieldSchema;)Ljava/lang/Object;

    move-result-object p5

    goto/32 :goto_c0

    nop

    :goto_b3
    goto/16 :goto_3e

    :pswitch_1e
    goto/32 :goto_4c

    nop

    :goto_b4
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_a5

    nop

    :goto_b5
    invoke-virtual {p4, v3}, Lcom/android/framework/protobuf/FieldSet;->getField(Lcom/android/framework/protobuf/FieldSet$FieldDescriptorLite;)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_18

    nop

    :goto_b6
    invoke-interface {p1, v3}, Lcom/android/framework/protobuf/Reader;->readSInt32List(Ljava/util/List;)V

    goto/32 :goto_22

    nop

    :goto_b7
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;->getLiteType()Lcom/android/framework/protobuf/WireFormat$FieldType;

    move-result-object v4

    goto/32 :goto_34

    nop

    :goto_b8
    const-string v5, "Type cannot be packed: "

    goto/32 :goto_67

    nop

    :goto_b9
    throw v3

    :pswitch_1f
    goto/32 :goto_9f

    nop

    :goto_ba
    iget-object v3, v0, Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;->descriptor:Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;

    goto/32 :goto_b5

    nop

    :goto_bb
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    goto/32 :goto_87

    nop

    :goto_bc
    invoke-interface {p1}, Lcom/android/framework/protobuf/Reader;->readSInt32()I

    move-result v3

    goto/32 :goto_3f

    nop

    :goto_bd
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;->getLiteType()Lcom/android/framework/protobuf/WireFormat$FieldType;

    move-result-object v4

    goto/32 :goto_7b

    nop

    :goto_be
    invoke-interface {p1, v3}, Lcom/android/framework/protobuf/Reader;->readSFixed64List(Ljava/util/List;)V

    goto/32 :goto_2e

    nop

    :goto_bf
    move-object v2, v3

    goto/32 :goto_aa

    nop

    :goto_c0
    move-object v2, v3

    goto/32 :goto_62

    nop

    :goto_c1
    new-instance v3, Ljava/util/ArrayList;

    goto/32 :goto_12

    nop

    :goto_c2
    goto/16 :goto_7

    :pswitch_20
    goto/32 :goto_88

    nop

    :goto_c3
    move-object v2, v3

    goto/32 :goto_72

    nop

    :goto_c4
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_c

    nop
.end method

.method parseLengthPrefixedMessageSetItem(Lcom/android/framework/protobuf/Reader;Ljava/lang/Object;Lcom/android/framework/protobuf/ExtensionRegistryLite;Lcom/android/framework/protobuf/FieldSet;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/framework/protobuf/Reader;",
            "Ljava/lang/Object;",
            "Lcom/android/framework/protobuf/ExtensionRegistryLite;",
            "Lcom/android/framework/protobuf/FieldSet<",
            "Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    invoke-interface {p1, v1, p3}, Lcom/android/framework/protobuf/Reader;->readMessage(Ljava/lang/Class;Lcom/android/framework/protobuf/ExtensionRegistryLite;)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_1

    nop

    :goto_1
    iget-object v2, v0, Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;->descriptor:Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;

    goto/32 :goto_6

    nop

    :goto_2
    move-object v0, p2

    goto/32 :goto_4

    nop

    :goto_3
    return-void

    :goto_4
    check-cast v0, Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;

    nop

    goto/32 :goto_7

    nop

    :goto_5
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_6
    invoke-virtual {p4, v2, v1}, Lcom/android/framework/protobuf/FieldSet;->setField(Lcom/android/framework/protobuf/FieldSet$FieldDescriptorLite;Ljava/lang/Object;)V

    goto/32 :goto_3

    nop

    :goto_7
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;->getMessageDefaultInstance()Lcom/android/framework/protobuf/MessageLite;

    move-result-object v1

    goto/32 :goto_5

    nop
.end method

.method parseMessageSetItem(Lcom/android/framework/protobuf/ByteString;Ljava/lang/Object;Lcom/android/framework/protobuf/ExtensionRegistryLite;Lcom/android/framework/protobuf/FieldSet;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/framework/protobuf/ByteString;",
            "Ljava/lang/Object;",
            "Lcom/android/framework/protobuf/ExtensionRegistryLite;",
            "Lcom/android/framework/protobuf/FieldSet<",
            "Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_5

    nop

    :goto_0
    throw v3

    :goto_1
    const/4 v3, 0x1

    goto/32 :goto_b

    nop

    :goto_2
    iget-object v3, v0, Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;->descriptor:Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;

    goto/32 :goto_f

    nop

    :goto_3
    invoke-virtual {v3, v1, v2, p3}, Lcom/android/framework/protobuf/Protobuf;->mergeFrom(Ljava/lang/Object;Lcom/android/framework/protobuf/Reader;Lcom/android/framework/protobuf/ExtensionRegistryLite;)V

    goto/32 :goto_2

    nop

    :goto_4
    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    goto/32 :goto_1

    nop

    :goto_5
    move-object v0, p2

    goto/32 :goto_10

    nop

    :goto_6
    invoke-virtual {p1}, Lcom/android/framework/protobuf/ByteString;->toByteArray()[B

    move-result-object v2

    goto/32 :goto_4

    nop

    :goto_7
    invoke-interface {v1}, Lcom/android/framework/protobuf/MessageLite;->newBuilderForType()Lcom/android/framework/protobuf/MessageLite$Builder;

    move-result-object v1

    goto/32 :goto_a

    nop

    :goto_8
    if-eq v3, v4, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_11

    nop

    :goto_9
    invoke-static {}, Lcom/android/framework/protobuf/Protobuf;->getInstance()Lcom/android/framework/protobuf/Protobuf;

    move-result-object v3

    goto/32 :goto_3

    nop

    :goto_a
    invoke-interface {v1}, Lcom/android/framework/protobuf/MessageLite$Builder;->buildPartial()Lcom/android/framework/protobuf/MessageLite;

    move-result-object v1

    goto/32 :goto_6

    nop

    :goto_b
    invoke-static {v2, v3}, Lcom/android/framework/protobuf/BinaryReader;->newInstance(Ljava/nio/ByteBuffer;Z)Lcom/android/framework/protobuf/BinaryReader;

    move-result-object v2

    goto/32 :goto_9

    nop

    :goto_c
    const v4, 0x7fffffff

    goto/32 :goto_8

    nop

    :goto_d
    invoke-static {}, Lcom/android/framework/protobuf/InvalidProtocolBufferException;->invalidEndTag()Lcom/android/framework/protobuf/InvalidProtocolBufferException;

    move-result-object v3

    goto/32 :goto_0

    nop

    :goto_e
    invoke-interface {v2}, Lcom/android/framework/protobuf/Reader;->getFieldNumber()I

    move-result v3

    goto/32 :goto_c

    nop

    :goto_f
    invoke-virtual {p4, v3, v1}, Lcom/android/framework/protobuf/FieldSet;->setField(Lcom/android/framework/protobuf/FieldSet$FieldDescriptorLite;Ljava/lang/Object;)V

    goto/32 :goto_e

    nop

    :goto_10
    check-cast v0, Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;

    goto/32 :goto_13

    nop

    :goto_11
    return-void

    :goto_12
    goto/32 :goto_d

    nop

    :goto_13
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$GeneratedExtension;->getMessageDefaultInstance()Lcom/android/framework/protobuf/MessageLite;

    move-result-object v1

    goto/32 :goto_7

    nop
.end method

.method serializeExtension(Lcom/android/framework/protobuf/Writer;Ljava/util/Map$Entry;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/framework/protobuf/Writer;",
            "Ljava/util/Map$Entry<",
            "**>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    nop

    goto/32 :goto_5b

    nop

    :goto_0
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_7

    nop

    :goto_1
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto/32 :goto_e

    nop

    :goto_2
    invoke-static {v1, v2, p1, v3}, Lcom/android/framework/protobuf/SchemaUtil;->writeBoolList(ILjava/util/List;Lcom/android/framework/protobuf/Writer;Z)V

    goto/32 :goto_bb

    nop

    :goto_3
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_a9

    nop

    :goto_4
    if-eqz v3, :cond_0

    goto/32 :goto_16

    :cond_0
    nop

    goto/32 :goto_17

    nop

    :goto_5
    check-cast v2, Ljava/util/List;

    goto/32 :goto_50

    nop

    :goto_6
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->isPacked()Z

    move-result v3

    goto/32 :goto_8a

    nop

    :goto_7
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_25

    nop

    :goto_8
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_2d

    nop

    :goto_9
    goto/16 :goto_d9

    :goto_a
    goto/32 :goto_8e

    nop

    :goto_b
    invoke-virtual {v2}, Lcom/android/framework/protobuf/WireFormat$FieldType;->ordinal()I

    move-result v2

    goto/32 :goto_ac

    nop

    :goto_c
    check-cast v2, Ljava/util/List;

    goto/32 :goto_db

    nop

    :goto_d
    check-cast v2, Ljava/lang/Integer;

    goto/32 :goto_ad

    nop

    :goto_e
    invoke-interface {p1, v1, v2, v3}, Lcom/android/framework/protobuf/Writer;->writeUInt64(IJ)V

    goto/32 :goto_fa

    nop

    :goto_f
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_c

    nop

    :goto_10
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->isPacked()Z

    move-result v3

    goto/32 :goto_2

    nop

    :goto_11
    invoke-interface {p1, v1, v2, v3}, Lcom/android/framework/protobuf/Writer;->writeGroup(ILjava/lang/Object;Lcom/android/framework/protobuf/Schema;)V

    goto/32 :goto_8b

    nop

    :goto_12
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_71

    nop

    :goto_13
    const/4 v2, 0x0

    packed-switch v1, :pswitch_data_0

    goto/32 :goto_8c

    nop

    :goto_14
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_a3

    nop

    :goto_15
    invoke-static {v1, v2, p1, v3}, Lcom/android/framework/protobuf/SchemaUtil;->writeDoubleList(ILjava/util/List;Lcom/android/framework/protobuf/Writer;Z)V

    nop

    :goto_16
    goto/32 :goto_9

    nop

    :goto_17
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v3

    goto/32 :goto_75

    nop

    :goto_18
    invoke-interface {p1, v1, v2}, Lcom/android/framework/protobuf/Writer;->writeBytes(ILcom/android/framework/protobuf/ByteString;)V

    goto/32 :goto_ca

    nop

    :goto_19
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto/32 :goto_9b

    nop

    :goto_1a
    check-cast v2, Ljava/lang/Long;

    goto/32 :goto_60

    nop

    :goto_1b
    invoke-static {v1, v2, p1, v3}, Lcom/android/framework/protobuf/SchemaUtil;->writeSInt64List(ILjava/util/List;Lcom/android/framework/protobuf/Writer;Z)V

    goto/32 :goto_9e

    nop

    :goto_1c
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->isPacked()Z

    move-result v3

    goto/32 :goto_c3

    nop

    :goto_1d
    check-cast v2, Ljava/util/List;

    goto/32 :goto_d3

    nop

    :goto_1e
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_6d

    nop

    :goto_1f
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_eb

    nop

    :goto_20
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_7b

    nop

    :goto_21
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto/32 :goto_2e

    nop

    :goto_22
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->isPacked()Z

    move-result v3

    goto/32 :goto_1b

    nop

    :goto_23
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto/32 :goto_df

    nop

    :goto_24
    check-cast v2, Ljava/util/List;

    goto/32 :goto_35

    nop

    :goto_25
    check-cast v2, Ljava/lang/Long;

    goto/32 :goto_19

    nop

    :goto_26
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    goto/32 :goto_69

    nop

    :goto_27
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_24

    nop

    :goto_28
    invoke-interface {p1, v1, v2, v3}, Lcom/android/framework/protobuf/Writer;->writeInt64(IJ)V

    goto/32 :goto_4f

    nop

    :goto_29
    check-cast v2, Ljava/util/List;

    goto/32 :goto_1c

    nop

    :goto_2a
    invoke-interface {p1, v1, v2}, Lcom/android/framework/protobuf/Writer;->writeFixed32(II)V

    goto/32 :goto_91

    nop

    :goto_2b
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_87

    nop

    :goto_2c
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    goto/32 :goto_b5

    nop

    :goto_2d
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_9c

    nop

    :goto_2e
    invoke-interface {p1, v1, v2}, Lcom/android/framework/protobuf/Writer;->writeInt32(II)V

    goto/32 :goto_94

    nop

    :goto_2f
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_d

    nop

    :goto_30
    check-cast v2, Ljava/util/List;

    goto/32 :goto_c1

    nop

    :goto_31
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_12

    nop

    :goto_32
    invoke-static {v1, v2, p1, v3}, Lcom/android/framework/protobuf/SchemaUtil;->writeFixed32List(ILjava/util/List;Lcom/android/framework/protobuf/Writer;Z)V

    goto/32 :goto_59

    nop

    :goto_33
    goto/16 :goto_16

    :pswitch_0
    nop

    goto/32 :goto_31

    nop

    :goto_34
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_2b

    nop

    :goto_35
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->isPacked()Z

    move-result v3

    goto/32 :goto_5d

    nop

    :goto_36
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_ba

    nop

    :goto_37
    invoke-static {v1, v2, p1, v3}, Lcom/android/framework/protobuf/SchemaUtil;->writeFloatList(ILjava/util/List;Lcom/android/framework/protobuf/Writer;Z)V

    goto/32 :goto_4a

    nop

    :goto_38
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    goto/32 :goto_dc

    nop

    :goto_39
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->isRepeated()Z

    move-result v1

    goto/32 :goto_f5

    nop

    :goto_3a
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_b3

    nop

    :goto_3b
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_f2

    nop

    :goto_3c
    invoke-interface {p1, v1, v2, v3}, Lcom/android/framework/protobuf/Writer;->writeFixed64(IJ)V

    goto/32 :goto_dd

    nop

    :goto_3d
    invoke-interface {p1, v1, v2, v3}, Lcom/android/framework/protobuf/Writer;->writeSFixed64(IJ)V

    goto/32 :goto_6a

    nop

    :goto_3e
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_80

    nop

    :goto_3f
    check-cast v2, Lcom/android/framework/protobuf/ByteString;

    goto/32 :goto_18

    nop

    :goto_40
    invoke-static {v1, v2, p1, v3}, Lcom/android/framework/protobuf/SchemaUtil;->writeUInt64List(ILjava/util/List;Lcom/android/framework/protobuf/Writer;Z)V

    goto/32 :goto_ee

    nop

    :goto_41
    invoke-interface {p1, v1, v2}, Lcom/android/framework/protobuf/Writer;->writeBool(IZ)V

    goto/32 :goto_84

    nop

    :goto_42
    goto/16 :goto_16

    :pswitch_1
    nop

    goto/32 :goto_ed

    nop

    :goto_43
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_1f

    nop

    :goto_44
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_90

    nop

    :goto_45
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    goto/32 :goto_41

    nop

    :goto_46
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_d6

    nop

    :goto_47
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_73

    nop

    :goto_48
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_65

    nop

    :goto_49
    invoke-static {v3, v4, p1, v2}, Lcom/android/framework/protobuf/SchemaUtil;->writeMessageList(ILjava/util/List;Lcom/android/framework/protobuf/Writer;Lcom/android/framework/protobuf/Schema;)V

    goto/32 :goto_74

    nop

    :goto_4a
    goto/16 :goto_16

    :pswitch_2
    nop

    goto/32 :goto_b6

    nop

    :goto_4b
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_b8

    nop

    :goto_4c
    goto/16 :goto_16

    :pswitch_3
    nop

    goto/32 :goto_86

    nop

    :goto_4d
    if-nez v1, :cond_1

    goto/32 :goto_e4

    :cond_1
    goto/32 :goto_26

    nop

    :goto_4e
    check-cast v2, Ljava/lang/String;

    goto/32 :goto_b0

    nop

    :goto_4f
    goto/16 :goto_d9

    :pswitch_4
    goto/32 :goto_68

    nop

    :goto_50
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->isPacked()Z

    move-result v3

    goto/32 :goto_b2

    nop

    :goto_51
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_4e

    nop

    :goto_52
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_b4

    nop

    :goto_53
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->isPacked()Z

    move-result v3

    goto/32 :goto_15

    nop

    :goto_54
    invoke-static {v1, v2, p1}, Lcom/android/framework/protobuf/SchemaUtil;->writeStringList(ILjava/util/List;Lcom/android/framework/protobuf/Writer;)V

    goto/32 :goto_bd

    nop

    :goto_55
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto/32 :goto_b9

    nop

    :goto_56
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_63

    nop

    :goto_57
    invoke-static {}, Lcom/android/framework/protobuf/Protobuf;->getInstance()Lcom/android/framework/protobuf/Protobuf;

    move-result-object v5

    goto/32 :goto_56

    nop

    :goto_58
    goto/16 :goto_d9

    :pswitch_5
    nop

    goto/32 :goto_96

    nop

    :goto_59
    goto/16 :goto_16

    :pswitch_6
    nop

    goto/32 :goto_43

    nop

    :goto_5a
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_a0

    nop

    :goto_5b
    invoke-interface {p2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_61

    nop

    :goto_5c
    goto/16 :goto_d9

    :pswitch_7
    goto/32 :goto_47

    nop

    :goto_5d
    invoke-static {v1, v2, p1, v3}, Lcom/android/framework/protobuf/SchemaUtil;->writeSInt32List(ILjava/util/List;Lcom/android/framework/protobuf/Writer;Z)V

    goto/32 :goto_4c

    nop

    :goto_5e
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_f

    nop

    :goto_5f
    check-cast v2, Ljava/lang/Long;

    goto/32 :goto_6f

    nop

    :goto_60
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto/32 :goto_3d

    nop

    :goto_61
    check-cast v0, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;

    goto/32 :goto_39

    nop

    :goto_62
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->isPacked()Z

    move-result v3

    goto/32 :goto_c9

    nop

    :goto_63
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    goto/32 :goto_de

    nop

    :goto_64
    check-cast v1, Ljava/util/List;

    goto/32 :goto_c0

    nop

    :goto_65
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_f3

    nop

    :goto_66
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_3a

    nop

    :goto_67
    invoke-static {}, Lcom/android/framework/protobuf/Protobuf;->getInstance()Lcom/android/framework/protobuf/Protobuf;

    move-result-object v3

    goto/32 :goto_72

    nop

    :goto_68
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_9d

    nop

    :goto_69
    if-eqz v3, :cond_2

    goto/32 :goto_e4

    :cond_2
    nop

    goto/32 :goto_bc

    nop

    :goto_6a
    goto/16 :goto_d9

    :pswitch_8
    goto/32 :goto_9a

    nop

    :goto_6b
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_30

    nop

    :goto_6c
    goto/16 :goto_d9

    :pswitch_9
    goto/32 :goto_3e

    nop

    :goto_6d
    check-cast v4, Ljava/util/List;

    goto/32 :goto_7d

    nop

    :goto_6e
    invoke-static {v1, v2, p1, v3}, Lcom/android/framework/protobuf/SchemaUtil;->writeInt32List(ILjava/util/List;Lcom/android/framework/protobuf/Writer;Z)V

    goto/32 :goto_c7

    nop

    :goto_6f
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto/32 :goto_28

    nop

    :goto_70
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_f9

    nop

    :goto_71
    check-cast v2, Ljava/util/List;

    goto/32 :goto_62

    nop

    :goto_72
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_38

    nop

    :goto_73
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_98

    nop

    :goto_74
    goto/16 :goto_16

    :pswitch_a
    goto/32 :goto_4b

    nop

    :goto_75
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_d7

    nop

    :goto_76
    check-cast v2, Ljava/lang/Integer;

    goto/32 :goto_ea

    nop

    :goto_77
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getLiteType()Lcom/android/framework/protobuf/WireFormat$FieldType;

    move-result-object v2

    goto/32 :goto_cf

    nop

    :goto_78
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_c8

    nop

    :goto_79
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    goto/32 :goto_4

    nop

    :goto_7a
    check-cast v2, Ljava/util/List;

    goto/32 :goto_22

    nop

    :goto_7b
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    goto/32 :goto_82

    nop

    :goto_7c
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_a4

    nop

    :goto_7d
    invoke-static {}, Lcom/android/framework/protobuf/Protobuf;->getInstance()Lcom/android/framework/protobuf/Protobuf;

    move-result-object v5

    goto/32 :goto_d0

    nop

    :goto_7e
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_b7

    nop

    :goto_7f
    goto/16 :goto_d9

    :pswitch_b
    goto/32 :goto_48

    nop

    :goto_80
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_99

    nop

    :goto_81
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_a2

    nop

    :goto_82
    invoke-virtual {v3, v4}, Lcom/android/framework/protobuf/Protobuf;->schemaFor(Ljava/lang/Class;)Lcom/android/framework/protobuf/Schema;

    move-result-object v3

    goto/32 :goto_11

    nop

    :goto_83
    invoke-static {v1, v2, p1}, Lcom/android/framework/protobuf/SchemaUtil;->writeBytesList(ILjava/util/List;Lcom/android/framework/protobuf/Writer;)V

    goto/32 :goto_d2

    nop

    :goto_84
    goto/16 :goto_d9

    :pswitch_c
    goto/32 :goto_46

    nop

    :goto_85
    goto/16 :goto_d9

    :pswitch_d
    goto/32 :goto_3

    nop

    :goto_86
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_81

    nop

    :goto_87
    check-cast v2, Ljava/lang/Integer;

    goto/32 :goto_55

    nop

    :goto_88
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_21
        :pswitch_19
        :pswitch_1f
        :pswitch_6
        :pswitch_16
        :pswitch_10
        :pswitch_22
        :pswitch_0
        :pswitch_3
        :pswitch_13
        :pswitch_20
        :pswitch_1d
        :pswitch_17
        :pswitch_18
        :pswitch_a
        :pswitch_f
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_15
        :pswitch_4
        :pswitch_23
        :pswitch_9
        :pswitch_1e
        :pswitch_11
        :pswitch_c
        :pswitch_7
        :pswitch_b
        :pswitch_8
        :pswitch_1c
        :pswitch_1b
        :pswitch_12
        :pswitch_1a
        :pswitch_d
        :pswitch_e
        :pswitch_5
        :pswitch_14
    .end packed-switch

    :goto_89
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_9f

    nop

    :goto_8a
    invoke-static {v1, v2, p1, v3}, Lcom/android/framework/protobuf/SchemaUtil;->writeSFixed64List(ILjava/util/List;Lcom/android/framework/protobuf/Writer;Z)V

    goto/32 :goto_33

    nop

    :goto_8b
    goto/16 :goto_d9

    :pswitch_e
    goto/32 :goto_b1

    nop

    :goto_8c
    goto/16 :goto_16

    :pswitch_f
    goto/32 :goto_ec

    nop

    :goto_8d
    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    goto/32 :goto_a1

    nop

    :goto_8e
    sget-object v1, Lcom/android/framework/protobuf/ExtensionSchemaLite$1;->$SwitchMap$com$google$protobuf$WireFormat$FieldType:[I

    goto/32 :goto_77

    nop

    :goto_8f
    goto/16 :goto_16

    :pswitch_10
    nop

    goto/32 :goto_5a

    nop

    :goto_90
    invoke-static {}, Lcom/android/framework/protobuf/Protobuf;->getInstance()Lcom/android/framework/protobuf/Protobuf;

    move-result-object v3

    goto/32 :goto_20

    nop

    :goto_91
    goto/16 :goto_d9

    :pswitch_11
    goto/32 :goto_52

    nop

    :goto_92
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_e8

    nop

    :goto_93
    invoke-interface {p1, v1, v2, v3}, Lcom/android/framework/protobuf/Writer;->writeMessage(ILjava/lang/Object;Lcom/android/framework/protobuf/Schema;)V

    goto/32 :goto_58

    nop

    :goto_94
    goto/16 :goto_d9

    :pswitch_12
    goto/32 :goto_0

    nop

    :goto_95
    invoke-virtual {v5, v2}, Lcom/android/framework/protobuf/Protobuf;->schemaFor(Ljava/lang/Class;)Lcom/android/framework/protobuf/Schema;

    move-result-object v2

    goto/32 :goto_e3

    nop

    :goto_96
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_44

    nop

    :goto_97
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_5

    nop

    :goto_98
    check-cast v2, Ljava/lang/Boolean;

    goto/32 :goto_45

    nop

    :goto_99
    check-cast v2, Ljava/lang/Long;

    goto/32 :goto_1

    nop

    :goto_9a
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_2f

    nop

    :goto_9b
    invoke-interface {p1, v1, v2, v3}, Lcom/android/framework/protobuf/Writer;->writeSInt64(IJ)V

    goto/32 :goto_cd

    nop

    :goto_9c
    check-cast v2, Ljava/lang/Double;

    goto/32 :goto_e0

    nop

    :goto_9d
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_a8

    nop

    :goto_9e
    goto/16 :goto_16

    :pswitch_13
    nop

    goto/32 :goto_c6

    nop

    :goto_9f
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_67

    nop

    :goto_a0
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_f6

    nop

    :goto_a1
    goto/16 :goto_d9

    :pswitch_14
    nop

    goto/32 :goto_89

    nop

    :goto_a2
    check-cast v2, Ljava/util/List;

    goto/32 :goto_6

    nop

    :goto_a3
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_1d

    nop

    :goto_a4
    check-cast v2, Ljava/util/List;

    goto/32 :goto_d4

    nop

    :goto_a5
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_6b

    nop

    :goto_a6
    goto/16 :goto_d9

    :pswitch_15
    goto/32 :goto_8

    nop

    :goto_a7
    sget-object v1, Lcom/android/framework/protobuf/ExtensionSchemaLite$1;->$SwitchMap$com$google$protobuf$WireFormat$FieldType:[I

    goto/32 :goto_ab

    nop

    :goto_a8
    check-cast v2, Ljava/lang/Float;

    goto/32 :goto_2c

    nop

    :goto_a9
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_3f

    nop

    :goto_aa
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_97

    nop

    :goto_ab
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getLiteType()Lcom/android/framework/protobuf/WireFormat$FieldType;

    move-result-object v2

    goto/32 :goto_b

    nop

    :goto_ac
    aget v1, v1, v2

    goto/32 :goto_13

    nop

    :goto_ad
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto/32 :goto_ae

    nop

    :goto_ae
    invoke-interface {p1, v1, v2}, Lcom/android/framework/protobuf/Writer;->writeSFixed32(II)V

    goto/32 :goto_7f

    nop

    :goto_af
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->isPacked()Z

    move-result v3

    goto/32 :goto_d5

    nop

    :goto_b0
    invoke-interface {p1, v1, v2}, Lcom/android/framework/protobuf/Writer;->writeString(ILjava/lang/String;)V

    goto/32 :goto_85

    nop

    :goto_b1
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_51

    nop

    :goto_b2
    invoke-static {v1, v2, p1, v3}, Lcom/android/framework/protobuf/SchemaUtil;->writeInt32List(ILjava/util/List;Lcom/android/framework/protobuf/Writer;Z)V

    goto/32 :goto_e6

    nop

    :goto_b3
    check-cast v2, Ljava/lang/Integer;

    goto/32 :goto_21

    nop

    :goto_b4
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_cc

    nop

    :goto_b5
    invoke-interface {p1, v1, v2}, Lcom/android/framework/protobuf/Writer;->writeFloat(IF)V

    goto/32 :goto_a6

    nop

    :goto_b6
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_70

    nop

    :goto_b7
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_5f

    nop

    :goto_b8
    check-cast v1, Ljava/util/List;

    goto/32 :goto_4d

    nop

    :goto_b9
    invoke-interface {p1, v1, v2}, Lcom/android/framework/protobuf/Writer;->writeSInt32(II)V

    goto/32 :goto_ce

    nop

    :goto_ba
    check-cast v2, Ljava/lang/Integer;

    goto/32 :goto_bf

    nop

    :goto_bb
    goto/16 :goto_16

    :pswitch_16
    nop

    goto/32 :goto_f4

    nop

    :goto_bc
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v3

    goto/32 :goto_1e

    nop

    :goto_bd
    goto/16 :goto_16

    :pswitch_17
    nop

    goto/32 :goto_c4

    nop

    :goto_be
    goto/16 :goto_16

    :pswitch_18
    nop

    goto/32 :goto_78

    nop

    :goto_bf
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto/32 :goto_f1

    nop

    :goto_c0
    if-nez v1, :cond_3

    goto/32 :goto_16

    :cond_3
    goto/32 :goto_79

    nop

    :goto_c1
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->isPacked()Z

    move-result v3

    goto/32 :goto_6e

    nop

    :goto_c2
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_da

    nop

    :goto_c3
    invoke-static {v1, v2, p1, v3}, Lcom/android/framework/protobuf/SchemaUtil;->writeUInt32List(ILjava/util/List;Lcom/android/framework/protobuf/Writer;Z)V

    goto/32 :goto_8f

    nop

    :goto_c4
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_c2

    nop

    :goto_c5
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_36

    nop

    :goto_c6
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_27

    nop

    :goto_c7
    goto/16 :goto_16

    :pswitch_19
    nop

    goto/32 :goto_14

    nop

    :goto_c8
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_e9

    nop

    :goto_c9
    invoke-static {v1, v2, p1, v3}, Lcom/android/framework/protobuf/SchemaUtil;->writeSFixed32List(ILjava/util/List;Lcom/android/framework/protobuf/Writer;Z)V

    goto/32 :goto_f7

    nop

    :goto_ca
    goto/16 :goto_d9

    :pswitch_1a
    goto/32 :goto_66

    nop

    :goto_cb
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->isPacked()Z

    move-result v3

    goto/32 :goto_37

    nop

    :goto_cc
    check-cast v2, Ljava/lang/Long;

    goto/32 :goto_ef

    nop

    :goto_cd
    goto/16 :goto_d9

    :pswitch_1b
    goto/32 :goto_34

    nop

    :goto_ce
    goto/16 :goto_d9

    :pswitch_1c
    goto/32 :goto_e2

    nop

    :goto_cf
    invoke-virtual {v2}, Lcom/android/framework/protobuf/WireFormat$FieldType;->ordinal()I

    move-result v2

    goto/32 :goto_8d

    nop

    :goto_d0
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_d1

    nop

    :goto_d1
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    goto/32 :goto_95

    nop

    :goto_d2
    goto/16 :goto_16

    :pswitch_1d
    nop

    goto/32 :goto_aa

    nop

    :goto_d3
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->isPacked()Z

    move-result v3

    goto/32 :goto_40

    nop

    :goto_d4
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->isPacked()Z

    move-result v3

    goto/32 :goto_32

    nop

    :goto_d5
    invoke-static {v1, v2, p1, v3}, Lcom/android/framework/protobuf/SchemaUtil;->writeFixed64List(ILjava/util/List;Lcom/android/framework/protobuf/Writer;Z)V

    goto/32 :goto_e1

    nop

    :goto_d6
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_76

    nop

    :goto_d7
    check-cast v4, Ljava/util/List;

    goto/32 :goto_57

    nop

    :goto_d8
    invoke-interface {p1, v1, v2, v3}, Lcom/android/framework/protobuf/Writer;->writeDouble(ID)V

    nop

    :goto_d9
    goto/32 :goto_88

    nop

    :goto_da
    check-cast v2, Ljava/util/List;

    goto/32 :goto_83

    nop

    :goto_db
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->isPacked()Z

    move-result v3

    goto/32 :goto_e7

    nop

    :goto_dc
    invoke-virtual {v3, v4}, Lcom/android/framework/protobuf/Protobuf;->schemaFor(Ljava/lang/Class;)Lcom/android/framework/protobuf/Schema;

    move-result-object v3

    goto/32 :goto_93

    nop

    :goto_dd
    goto :goto_d9

    :pswitch_1e
    goto/32 :goto_c5

    nop

    :goto_de
    invoke-virtual {v5, v2}, Lcom/android/framework/protobuf/Protobuf;->schemaFor(Ljava/lang/Class;)Lcom/android/framework/protobuf/Schema;

    move-result-object v2

    goto/32 :goto_49

    nop

    :goto_df
    invoke-interface {p1, v1, v2}, Lcom/android/framework/protobuf/Writer;->writeUInt32(II)V

    goto/32 :goto_5c

    nop

    :goto_e0
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    goto/32 :goto_d8

    nop

    :goto_e1
    goto/16 :goto_16

    :pswitch_1f
    nop

    goto/32 :goto_a5

    nop

    :goto_e2
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_f8

    nop

    :goto_e3
    invoke-static {v3, v4, p1, v2}, Lcom/android/framework/protobuf/SchemaUtil;->writeGroupList(ILjava/util/List;Lcom/android/framework/protobuf/Writer;Lcom/android/framework/protobuf/Schema;)V

    :goto_e4
    goto/32 :goto_be

    nop

    :goto_e5
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_29

    nop

    :goto_e6
    goto/16 :goto_16

    :pswitch_20
    nop

    goto/32 :goto_3b

    nop

    :goto_e7
    invoke-static {v1, v2, p1, v3}, Lcom/android/framework/protobuf/SchemaUtil;->writeInt64List(ILjava/util/List;Lcom/android/framework/protobuf/Writer;Z)V

    goto/32 :goto_42

    nop

    :goto_e8
    check-cast v2, Ljava/util/List;

    goto/32 :goto_cb

    nop

    :goto_e9
    check-cast v2, Ljava/util/List;

    goto/32 :goto_54

    nop

    :goto_ea
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto/32 :goto_2a

    nop

    :goto_eb
    check-cast v2, Ljava/util/List;

    goto/32 :goto_af

    nop

    :goto_ec
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_64

    nop

    :goto_ed
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_92

    nop

    :goto_ee
    goto/16 :goto_16

    :pswitch_21
    nop

    goto/32 :goto_5e

    nop

    :goto_ef
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    goto/32 :goto_3c

    nop

    :goto_f0
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_e5

    nop

    :goto_f1
    invoke-interface {p1, v1, v2}, Lcom/android/framework/protobuf/Writer;->writeInt32(II)V

    goto/32 :goto_6c

    nop

    :goto_f2
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_7a

    nop

    :goto_f3
    check-cast v2, Ljava/lang/Integer;

    goto/32 :goto_23

    nop

    :goto_f4
    invoke-virtual {v0}, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;->getNumber()I

    move-result v1

    goto/32 :goto_7c

    nop

    :goto_f5
    if-nez v1, :cond_4

    goto/32 :goto_a

    :cond_4
    goto/32 :goto_a7

    nop

    :goto_f6
    check-cast v2, Ljava/util/List;

    goto/32 :goto_10

    nop

    :goto_f7
    goto/16 :goto_16

    :pswitch_22
    nop

    goto/32 :goto_f0

    nop

    :goto_f8
    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_1a

    nop

    :goto_f9
    check-cast v2, Ljava/util/List;

    goto/32 :goto_53

    nop

    :goto_fa
    goto/16 :goto_d9

    :pswitch_23
    goto/32 :goto_7e

    nop
.end method

.method setExtensions(Ljava/lang/Object;Lcom/android/framework/protobuf/FieldSet;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lcom/android/framework/protobuf/FieldSet<",
            "Lcom/android/framework/protobuf/GeneratedMessageLite$ExtensionDescriptor;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    check-cast v0, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtendableMessage;

    goto/32 :goto_3

    nop

    :goto_2
    move-object v0, p1

    goto/32 :goto_1

    nop

    :goto_3
    iput-object p2, v0, Lcom/android/framework/protobuf/GeneratedMessageLite$ExtendableMessage;->extensions:Lcom/android/framework/protobuf/FieldSet;

    goto/32 :goto_0

    nop
.end method
