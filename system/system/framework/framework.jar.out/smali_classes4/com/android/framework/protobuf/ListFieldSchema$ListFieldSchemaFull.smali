.class final Lcom/android/framework/protobuf/ListFieldSchema$ListFieldSchemaFull;
.super Lcom/android/framework/protobuf/ListFieldSchema;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/framework/protobuf/ListFieldSchema;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ListFieldSchemaFull"
.end annotation


# static fields
.field private static final UNMODIFIABLE_LIST_CLASS:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/android/framework/protobuf/ListFieldSchema$ListFieldSchemaFull;->UNMODIFIABLE_LIST_CLASS:Ljava/lang/Class;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/framework/protobuf/ListFieldSchema;-><init>(Lcom/android/framework/protobuf/ListFieldSchema$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/framework/protobuf/ListFieldSchema$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/framework/protobuf/ListFieldSchema$ListFieldSchemaFull;-><init>()V

    return-void
.end method

.method static getList(Ljava/lang/Object;J)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Object;",
            "J)",
            "Ljava/util/List<",
            "TE;>;"
        }
    .end annotation

    invoke-static {p0, p1, p2}, Lcom/android/framework/protobuf/UnsafeUtil;->getObject(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private static mutableListAt(Ljava/lang/Object;JI)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<",
            "L:Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Object;",
            "JI)",
            "Ljava/util/List<",
            "T",
            "L;",
            ">;"
        }
    .end annotation

    invoke-static {p0, p1, p2}, Lcom/android/framework/protobuf/ListFieldSchema$ListFieldSchemaFull;->getList(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    instance-of v1, v0, Lcom/android/framework/protobuf/LazyStringList;

    if-eqz v1, :cond_0

    new-instance v1, Lcom/android/framework/protobuf/LazyStringArrayList;

    invoke-direct {v1, p3}, Lcom/android/framework/protobuf/LazyStringArrayList;-><init>(I)V

    move-object v0, v1

    goto :goto_0

    :cond_0
    instance-of v1, v0, Lcom/android/framework/protobuf/PrimitiveNonBoxingCollection;

    if-eqz v1, :cond_1

    instance-of v1, v0, Lcom/android/framework/protobuf/Internal$ProtobufList;

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Lcom/android/framework/protobuf/Internal$ProtobufList;

    invoke-interface {v1, p3}, Lcom/android/framework/protobuf/Internal$ProtobufList;->mutableCopyWithCapacity(I)Lcom/android/framework/protobuf/Internal$ProtobufList;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p3}, Ljava/util/ArrayList;-><init>(I)V

    move-object v0, v1

    :goto_0
    invoke-static {p0, p1, p2, v0}, Lcom/android/framework/protobuf/UnsafeUtil;->putObject(Ljava/lang/Object;JLjava/lang/Object;)V

    goto :goto_1

    :cond_2
    sget-object v1, Lcom/android/framework/protobuf/ListFieldSchema$ListFieldSchemaFull;->UNMODIFIABLE_LIST_CLASS:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v2, p3

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    move-object v0, v1

    invoke-static {p0, p1, p2, v0}, Lcom/android/framework/protobuf/UnsafeUtil;->putObject(Ljava/lang/Object;JLjava/lang/Object;)V

    goto :goto_1

    :cond_3
    instance-of v1, v0, Lcom/android/framework/protobuf/UnmodifiableLazyStringList;

    if-eqz v1, :cond_5

    new-instance v1, Lcom/android/framework/protobuf/LazyStringArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v2, p3

    invoke-direct {v1, v2}, Lcom/android/framework/protobuf/LazyStringArrayList;-><init>(I)V

    move-object v2, v0

    check-cast v2, Lcom/android/framework/protobuf/UnmodifiableLazyStringList;

    invoke-virtual {v1, v2}, Lcom/android/framework/protobuf/LazyStringArrayList;->addAll(Ljava/util/Collection;)Z

    move-object v0, v1

    invoke-static {p0, p1, p2, v0}, Lcom/android/framework/protobuf/UnsafeUtil;->putObject(Ljava/lang/Object;JLjava/lang/Object;)V

    :cond_4
    goto :goto_1

    :cond_5
    instance-of v1, v0, Lcom/android/framework/protobuf/PrimitiveNonBoxingCollection;

    if-eqz v1, :cond_4

    instance-of v1, v0, Lcom/android/framework/protobuf/Internal$ProtobufList;

    if-eqz v1, :cond_4

    move-object v1, v0

    check-cast v1, Lcom/android/framework/protobuf/Internal$ProtobufList;

    invoke-interface {v1}, Lcom/android/framework/protobuf/Internal$ProtobufList;->isModifiable()Z

    move-result v1

    if-nez v1, :cond_6

    move-object v1, v0

    check-cast v1, Lcom/android/framework/protobuf/Internal$ProtobufList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v2, p3

    invoke-interface {v1, v2}, Lcom/android/framework/protobuf/Internal$ProtobufList;->mutableCopyWithCapacity(I)Lcom/android/framework/protobuf/Internal$ProtobufList;

    move-result-object v0

    invoke-static {p0, p1, p2, v0}, Lcom/android/framework/protobuf/UnsafeUtil;->putObject(Ljava/lang/Object;JLjava/lang/Object;)V

    :cond_6
    :goto_1
    return-object v0
.end method


# virtual methods
.method makeImmutableListAt(Ljava/lang/Object;J)V
    .locals 4

    goto/32 :goto_f

    nop

    :goto_0
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    :goto_1
    goto/32 :goto_a

    nop

    :goto_2
    instance-of v2, v0, Lcom/android/framework/protobuf/Internal$ProtobufList;

    goto/32 :goto_16

    nop

    :goto_3
    instance-of v2, v0, Lcom/android/framework/protobuf/PrimitiveNonBoxingCollection;

    goto/32 :goto_14

    nop

    :goto_4
    return-void

    :goto_5
    goto/32 :goto_3

    nop

    :goto_6
    if-nez v2, :cond_0

    goto/32 :goto_13

    :cond_0
    goto/32 :goto_11

    nop

    :goto_7
    sget-object v2, Lcom/android/framework/protobuf/ListFieldSchema$ListFieldSchemaFull;->UNMODIFIABLE_LIST_CLASS:Ljava/lang/Class;

    goto/32 :goto_d

    nop

    :goto_8
    invoke-interface {v2}, Lcom/android/framework/protobuf/Internal$ProtobufList;->isModifiable()Z

    move-result v2

    goto/32 :goto_6

    nop

    :goto_9
    move-object v2, v0

    goto/32 :goto_15

    nop

    :goto_a
    invoke-static {p1, p2, p3, v1}, Lcom/android/framework/protobuf/UnsafeUtil;->putObject(Ljava/lang/Object;JLjava/lang/Object;)V

    goto/32 :goto_19

    nop

    :goto_b
    check-cast v0, Ljava/util/List;

    goto/32 :goto_1e

    nop

    :goto_c
    if-nez v2, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_4

    nop

    :goto_d
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    goto/32 :goto_1b

    nop

    :goto_e
    instance-of v2, v0, Lcom/android/framework/protobuf/LazyStringList;

    goto/32 :goto_18

    nop

    :goto_f
    invoke-static {p1, p2, p3}, Lcom/android/framework/protobuf/UnsafeUtil;->getObject(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_b

    nop

    :goto_10
    invoke-interface {v2}, Lcom/android/framework/protobuf/LazyStringList;->getUnmodifiableView()Lcom/android/framework/protobuf/LazyStringList;

    move-result-object v1

    goto/32 :goto_1c

    nop

    :goto_11
    move-object v2, v0

    goto/32 :goto_21

    nop

    :goto_12
    invoke-interface {v2}, Lcom/android/framework/protobuf/Internal$ProtobufList;->makeImmutable()V

    :goto_13
    goto/32 :goto_1f

    nop

    :goto_14
    if-nez v2, :cond_2

    goto/32 :goto_20

    :cond_2
    goto/32 :goto_2

    nop

    :goto_15
    check-cast v2, Lcom/android/framework/protobuf/LazyStringList;

    goto/32 :goto_10

    nop

    :goto_16
    if-nez v2, :cond_3

    goto/32 :goto_20

    :cond_3
    goto/32 :goto_17

    nop

    :goto_17
    move-object v2, v0

    goto/32 :goto_1a

    nop

    :goto_18
    if-nez v2, :cond_4

    goto/32 :goto_1d

    :cond_4
    goto/32 :goto_9

    nop

    :goto_19
    return-void

    :goto_1a
    check-cast v2, Lcom/android/framework/protobuf/Internal$ProtobufList;

    goto/32 :goto_8

    nop

    :goto_1b
    invoke-virtual {v2, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    goto/32 :goto_c

    nop

    :goto_1c
    goto/16 :goto_1

    :goto_1d
    goto/32 :goto_7

    nop

    :goto_1e
    const/4 v1, 0x0

    goto/32 :goto_e

    nop

    :goto_1f
    return-void

    :goto_20
    goto/32 :goto_0

    nop

    :goto_21
    check-cast v2, Lcom/android/framework/protobuf/Internal$ProtobufList;

    goto/32 :goto_12

    nop
.end method

.method mergeListsAt(Ljava/lang/Object;Ljava/lang/Object;J)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "J)V"
        }
    .end annotation

    goto/32 :goto_a

    nop

    :goto_0
    move-object v4, v1

    goto/32 :goto_c

    nop

    :goto_1
    move-object v4, v0

    :goto_2
    goto/32 :goto_b

    nop

    :goto_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    goto/32 :goto_8

    nop

    :goto_4
    return-void

    :goto_5
    invoke-static {p1, p3, p4, v1}, Lcom/android/framework/protobuf/ListFieldSchema$ListFieldSchemaFull;->mutableListAt(Ljava/lang/Object;JI)Ljava/util/List;

    move-result-object v1

    goto/32 :goto_10

    nop

    :goto_6
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :goto_7
    goto/32 :goto_f

    nop

    :goto_8
    if-gtz v2, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_9

    nop

    :goto_9
    if-gtz v3, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_6

    nop

    :goto_a
    invoke-static {p2, p3, p4}, Lcom/android/framework/protobuf/ListFieldSchema$ListFieldSchemaFull;->getList(Ljava/lang/Object;J)Ljava/util/List;

    move-result-object v0

    goto/32 :goto_e

    nop

    :goto_b
    invoke-static {p1, p3, p4, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putObject(Ljava/lang/Object;JLjava/lang/Object;)V

    goto/32 :goto_4

    nop

    :goto_c
    goto :goto_2

    :goto_d
    goto/32 :goto_1

    nop

    :goto_e
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    goto/32 :goto_5

    nop

    :goto_f
    if-gtz v2, :cond_2

    goto/32 :goto_d

    :cond_2
    goto/32 :goto_0

    nop

    :goto_10
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    goto/32 :goto_3

    nop
.end method

.method mutableListAt(Ljava/lang/Object;J)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<",
            "L:Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Object;",
            "J)",
            "Ljava/util/List<",
            "T",
            "L;",
            ">;"
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    const/16 v0, 0xa

    goto/32 :goto_1

    nop

    :goto_1
    invoke-static {p1, p2, p3, v0}, Lcom/android/framework/protobuf/ListFieldSchema$ListFieldSchemaFull;->mutableListAt(Ljava/lang/Object;JI)Ljava/util/List;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_2
    return-object v0
.end method
