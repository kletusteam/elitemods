.class final Lcom/android/framework/protobuf/Utf8$UnsafeProcessor;
.super Lcom/android/framework/protobuf/Utf8$Processor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/framework/protobuf/Utf8;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "UnsafeProcessor"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/framework/protobuf/Utf8$Processor;-><init>()V

    return-void
.end method

.method static isAvailable()Z
    .locals 1

    invoke-static {}, Lcom/android/framework/protobuf/UnsafeUtil;->hasUnsafeArrayOperations()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/framework/protobuf/UnsafeUtil;->hasUnsafeByteBufferOperations()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private static partialIsValidUtf8(JI)I
    .locals 11

    invoke-static {p0, p1, p2}, Lcom/android/framework/protobuf/Utf8$UnsafeProcessor;->unsafeEstimateConsecutiveAscii(JI)I

    move-result v0

    int-to-long v1, v0

    add-long/2addr p0, v1

    sub-int/2addr p2, v0

    :goto_0
    const/4 v1, 0x0

    :goto_1
    const-wide/16 v2, 0x1

    if-lez p2, :cond_1

    add-long v4, p0, v2

    invoke-static {p0, p1}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte(J)B

    move-result p0

    move v1, p0

    if-ltz p0, :cond_0

    add-int/lit8 p2, p2, -0x1

    move-wide p0, v4

    goto :goto_1

    :cond_0
    move-wide p0, v4

    :cond_1
    if-nez p2, :cond_2

    const/4 v2, 0x0

    return v2

    :cond_2
    add-int/lit8 p2, p2, -0x1

    const/16 v4, -0x20

    const/16 v5, -0x41

    const/4 v6, -0x1

    if-ge v1, v4, :cond_6

    if-nez p2, :cond_3

    return v1

    :cond_3
    add-int/lit8 p2, p2, -0x1

    const/16 v4, -0x3e

    if-lt v1, v4, :cond_5

    add-long/2addr v2, p0

    invoke-static {p0, p1}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte(J)B

    move-result p0

    if-le p0, v5, :cond_4

    move-wide p0, v2

    goto :goto_2

    :cond_4
    move-wide p0, v2

    goto :goto_4

    :cond_5
    :goto_2
    return v6

    :cond_6
    const/16 v7, -0x10

    if-ge v1, v7, :cond_c

    const/4 v7, 0x2

    if-ge p2, v7, :cond_7

    invoke-static {p0, p1, v1, p2}, Lcom/android/framework/protobuf/Utf8$UnsafeProcessor;->unsafeIncompleteStateFor(JII)I

    move-result v2

    return v2

    :cond_7
    add-int/lit8 p2, p2, -0x2

    add-long v7, p0, v2

    invoke-static {p0, p1}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte(J)B

    move-result p0

    if-gt p0, v5, :cond_b

    const/16 p1, -0x60

    if-ne v1, v4, :cond_8

    if-lt p0, p1, :cond_b

    :cond_8
    const/16 v4, -0x13

    if-ne v1, v4, :cond_9

    if-ge p0, p1, :cond_b

    :cond_9
    add-long/2addr v2, v7

    invoke-static {v7, v8}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte(J)B

    move-result p1

    if-le p1, v5, :cond_a

    move-wide v7, v2

    goto :goto_3

    :cond_a
    move-wide p0, v2

    goto :goto_4

    :cond_b
    :goto_3
    return v6

    :cond_c
    const/4 v4, 0x3

    if-ge p2, v4, :cond_d

    invoke-static {p0, p1, v1, p2}, Lcom/android/framework/protobuf/Utf8$UnsafeProcessor;->unsafeIncompleteStateFor(JII)I

    move-result v2

    return v2

    :cond_d
    add-int/lit8 p2, p2, -0x3

    add-long v7, p0, v2

    invoke-static {p0, p1}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte(J)B

    move-result p0

    if-gt p0, v5, :cond_10

    shl-int/lit8 p1, v1, 0x1c

    add-int/lit8 v4, p0, 0x70

    add-int/2addr p1, v4

    shr-int/lit8 p1, p1, 0x1e

    if-nez p1, :cond_10

    add-long v9, v7, v2

    invoke-static {v7, v8}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte(J)B

    move-result p1

    if-gt p1, v5, :cond_f

    add-long v7, v9, v2

    invoke-static {v9, v10}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte(J)B

    move-result p1

    if-le p1, v5, :cond_e

    goto :goto_5

    :cond_e
    move-wide p0, v7

    :goto_4
    goto/16 :goto_0

    :cond_f
    move-wide v7, v9

    :cond_10
    :goto_5
    return v6
.end method

.method private static partialIsValidUtf8([BJI)I
    .locals 11

    invoke-static {p0, p1, p2, p3}, Lcom/android/framework/protobuf/Utf8$UnsafeProcessor;->unsafeEstimateConsecutiveAscii([BJI)I

    move-result v0

    sub-int/2addr p3, v0

    int-to-long v1, v0

    add-long/2addr p1, v1

    :goto_0
    const/4 v1, 0x0

    :goto_1
    const-wide/16 v2, 0x1

    if-lez p3, :cond_1

    add-long v4, p1, v2

    invoke-static {p0, p1, p2}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte([BJ)B

    move-result p1

    move v1, p1

    if-ltz p1, :cond_0

    add-int/lit8 p3, p3, -0x1

    move-wide p1, v4

    goto :goto_1

    :cond_0
    move-wide p1, v4

    :cond_1
    if-nez p3, :cond_2

    const/4 v2, 0x0

    return v2

    :cond_2
    add-int/lit8 p3, p3, -0x1

    const/16 v4, -0x20

    const/16 v5, -0x41

    const/4 v6, -0x1

    if-ge v1, v4, :cond_6

    if-nez p3, :cond_3

    return v1

    :cond_3
    add-int/lit8 p3, p3, -0x1

    const/16 v4, -0x3e

    if-lt v1, v4, :cond_5

    add-long/2addr v2, p1

    invoke-static {p0, p1, p2}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte([BJ)B

    move-result p1

    if-le p1, v5, :cond_4

    move-wide p1, v2

    goto :goto_2

    :cond_4
    move-wide p1, v2

    goto :goto_4

    :cond_5
    :goto_2
    return v6

    :cond_6
    const/16 v7, -0x10

    if-ge v1, v7, :cond_c

    const/4 v7, 0x2

    if-ge p3, v7, :cond_7

    invoke-static {p0, v1, p1, p2, p3}, Lcom/android/framework/protobuf/Utf8$UnsafeProcessor;->unsafeIncompleteStateFor([BIJI)I

    move-result v2

    return v2

    :cond_7
    add-int/lit8 p3, p3, -0x2

    add-long v7, p1, v2

    invoke-static {p0, p1, p2}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte([BJ)B

    move-result p1

    move p2, p1

    if-gt p1, v5, :cond_b

    const/16 p1, -0x60

    if-ne v1, v4, :cond_8

    if-lt p2, p1, :cond_b

    :cond_8
    const/16 v4, -0x13

    if-ne v1, v4, :cond_9

    if-ge p2, p1, :cond_b

    :cond_9
    add-long/2addr v2, v7

    invoke-static {p0, v7, v8}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte([BJ)B

    move-result p1

    if-le p1, v5, :cond_a

    move-wide v7, v2

    goto :goto_3

    :cond_a
    move-wide p1, v2

    goto :goto_4

    :cond_b
    :goto_3
    return v6

    :cond_c
    const/4 v4, 0x3

    if-ge p3, v4, :cond_d

    invoke-static {p0, v1, p1, p2, p3}, Lcom/android/framework/protobuf/Utf8$UnsafeProcessor;->unsafeIncompleteStateFor([BIJI)I

    move-result v2

    return v2

    :cond_d
    add-int/lit8 p3, p3, -0x3

    add-long v7, p1, v2

    invoke-static {p0, p1, p2}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte([BJ)B

    move-result p1

    move p2, p1

    if-gt p1, v5, :cond_10

    shl-int/lit8 p1, v1, 0x1c

    add-int/lit8 v4, p2, 0x70

    add-int/2addr p1, v4

    shr-int/lit8 p1, p1, 0x1e

    if-nez p1, :cond_10

    add-long v9, v7, v2

    invoke-static {p0, v7, v8}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte([BJ)B

    move-result p1

    if-gt p1, v5, :cond_f

    add-long v7, v9, v2

    invoke-static {p0, v9, v10}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte([BJ)B

    move-result p1

    if-le p1, v5, :cond_e

    goto :goto_5

    :cond_e
    move-wide p1, v7

    :goto_4
    goto/16 :goto_0

    :cond_f
    move-wide v7, v9

    :cond_10
    :goto_5
    return v6
.end method

.method private static unsafeEstimateConsecutiveAscii(JI)I
    .locals 7

    move v0, p2

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    const/4 v1, 0x0

    return v1

    :cond_0
    long-to-int v1, p0

    and-int/lit8 v1, v1, 0x7

    const/16 v2, 0x8

    rsub-int/lit8 v1, v1, 0x8

    move v3, v1

    :goto_0
    if-lez v3, :cond_2

    const-wide/16 v4, 0x1

    add-long/2addr v4, p0

    invoke-static {p0, p1}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte(J)B

    move-result p0

    if-gez p0, :cond_1

    sub-int p0, v1, v3

    return p0

    :cond_1
    add-int/lit8 v3, v3, -0x1

    move-wide p0, v4

    goto :goto_0

    :cond_2
    sub-int/2addr v0, v1

    :goto_1
    if-lt v0, v2, :cond_3

    invoke-static {p0, p1}, Lcom/android/framework/protobuf/UnsafeUtil;->getLong(J)J

    move-result-wide v3

    const-wide v5, -0x7f7f7f7f7f7f7f80L    # -2.937446524422997E-306

    and-long/2addr v3, v5

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-nez v3, :cond_3

    const-wide/16 v3, 0x8

    add-long/2addr p0, v3

    add-int/lit8 v0, v0, -0x8

    goto :goto_1

    :cond_3
    sub-int v2, p2, v0

    return v2
.end method

.method private static unsafeEstimateConsecutiveAscii([BJI)I
    .locals 3

    const/16 v0, 0x10

    if-ge p3, v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_2

    const-wide/16 v1, 0x1

    add-long/2addr v1, p1

    invoke-static {p0, p1, p2}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte([BJ)B

    move-result p1

    if-gez p1, :cond_1

    return v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    move-wide p1, v1

    goto :goto_0

    :cond_2
    return p3
.end method

.method private static unsafeIncompleteStateFor(JII)I
    .locals 3

    packed-switch p3, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    nop

    invoke-static {p0, p1}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte(J)B

    move-result v0

    const-wide/16 v1, 0x1

    add-long/2addr v1, p0

    invoke-static {v1, v2}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte(J)B

    move-result v1

    invoke-static {p2, v0, v1}, Lcom/android/framework/protobuf/Utf8;->access$100(III)I

    move-result v0

    return v0

    :pswitch_1
    invoke-static {p0, p1}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte(J)B

    move-result v0

    invoke-static {p2, v0}, Lcom/android/framework/protobuf/Utf8;->access$000(II)I

    move-result v0

    return v0

    :pswitch_2
    invoke-static {p2}, Lcom/android/framework/protobuf/Utf8;->access$1200(I)I

    move-result v0

    return v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static unsafeIncompleteStateFor([BIJI)I
    .locals 3

    packed-switch p4, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    nop

    invoke-static {p0, p2, p3}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte([BJ)B

    move-result v0

    const-wide/16 v1, 0x1

    add-long/2addr v1, p2

    invoke-static {p0, v1, v2}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte([BJ)B

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/android/framework/protobuf/Utf8;->access$100(III)I

    move-result v0

    return v0

    :pswitch_1
    invoke-static {p0, p2, p3}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte([BJ)B

    move-result v0

    invoke-static {p1, v0}, Lcom/android/framework/protobuf/Utf8;->access$000(II)I

    move-result v0

    return v0

    :pswitch_2
    invoke-static {p1}, Lcom/android/framework/protobuf/Utf8;->access$1200(I)I

    move-result v0

    return v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method decodeUtf8([BII)Ljava/lang/String;
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/framework/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    goto/32 :goto_6a

    nop

    :goto_0
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/32 :goto_2a

    nop

    :goto_1
    move-object v10, v12

    goto/32 :goto_8

    nop

    :goto_2
    if-lt v2, v5, :cond_0

    goto/32 :goto_11

    :cond_0
    goto/32 :goto_40

    nop

    :goto_3
    invoke-static {v2}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$600(B)Z

    move-result v7

    goto/32 :goto_4f

    nop

    :goto_4
    invoke-static {v0, v8, v9}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte([BJ)B

    move-result v6

    goto/32 :goto_5a

    nop

    :goto_5
    int-to-long v7, v2

    goto/32 :goto_5b

    nop

    :goto_6
    invoke-static {v8}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$400(B)Z

    move-result v9

    goto/32 :goto_6f

    nop

    :goto_7
    invoke-static {v0, v9, v10}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte([BJ)B

    move-result v7

    goto/32 :goto_58

    nop

    :goto_8
    invoke-static/range {v6 .. v11}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$1000(BBBB[CI)V

    goto/32 :goto_3d

    nop

    :goto_9
    add-int/lit8 v7, v6, 0x1

    goto/32 :goto_2c

    nop

    :goto_a
    const/4 v4, 0x1

    goto/32 :goto_20

    nop

    :goto_b
    invoke-static {}, Lcom/android/framework/protobuf/InvalidProtocolBufferException;->invalidUtf8()Lcom/android/framework/protobuf/InvalidProtocolBufferException;

    move-result-object v3

    goto/32 :goto_2f

    nop

    :goto_c
    invoke-static {v2, v12, v11}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$500(B[CI)V

    :goto_d
    goto/32 :goto_1a

    nop

    :goto_e
    int-to-long v6, v6

    goto/32 :goto_80

    nop

    :goto_f
    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    goto/32 :goto_1e

    nop

    :goto_10
    throw v3

    :goto_11
    goto/32 :goto_69

    nop

    :goto_12
    move v6, v2

    goto/32 :goto_7e

    nop

    :goto_13
    int-to-long v9, v7

    goto/32 :goto_36

    nop

    :goto_14
    invoke-static {}, Lcom/android/framework/protobuf/InvalidProtocolBufferException;->invalidUtf8()Lcom/android/framework/protobuf/InvalidProtocolBufferException;

    move-result-object v3

    goto/32 :goto_10

    nop

    :goto_15
    goto/16 :goto_4b

    :goto_16
    goto/32 :goto_14

    nop

    :goto_17
    goto/16 :goto_86

    :goto_18
    goto/32 :goto_4a

    nop

    :goto_19
    invoke-static {v8, v12, v7}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$500(B[CI)V

    goto/32 :goto_73

    nop

    :goto_1a
    if-lt v6, v5, :cond_1

    goto/32 :goto_57

    :cond_1
    goto/32 :goto_4d

    nop

    :goto_1b
    invoke-static {v7, v12, v6}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$500(B[CI)V

    goto/32 :goto_47

    nop

    :goto_1c
    aput-object v3, v5, v4

    goto/32 :goto_3e

    nop

    :goto_1d
    const/4 v3, 0x0

    goto/32 :goto_a

    nop

    :goto_1e
    const/4 v5, 0x3

    goto/32 :goto_70

    nop

    :goto_1f
    int-to-long v7, v2

    goto/32 :goto_2b

    nop

    :goto_20
    if-gez v2, :cond_2

    goto/32 :goto_78

    :cond_2
    goto/32 :goto_21

    nop

    :goto_21
    move/from16 v2, p2

    goto/32 :goto_24

    nop

    :goto_22
    if-lt v6, v7, :cond_3

    goto/32 :goto_16

    :cond_3
    goto/32 :goto_81

    nop

    :goto_23
    if-eqz v8, :cond_4

    goto/32 :goto_83

    :cond_4
    goto/32 :goto_82

    nop

    :goto_24
    add-int v5, v2, v1

    goto/32 :goto_48

    nop

    :goto_25
    if-nez v7, :cond_5

    goto/32 :goto_27

    :cond_5
    goto/32 :goto_39

    nop

    :goto_26
    throw v3

    :goto_27
    goto/32 :goto_65

    nop

    :goto_28
    add-int/lit8 v9, v7, 0x1

    goto/32 :goto_19

    nop

    :goto_29
    if-nez v7, :cond_6

    goto/32 :goto_60

    :cond_6
    goto/32 :goto_6e

    nop

    :goto_2a
    const/4 v4, 0x2

    goto/32 :goto_1c

    nop

    :goto_2b
    invoke-static {v0, v7, v8}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte([BJ)B

    move-result v2

    goto/32 :goto_72

    nop

    :goto_2c
    int-to-long v8, v6

    goto/32 :goto_35

    nop

    :goto_2d
    move v11, v14

    :goto_2e
    goto/32 :goto_15

    nop

    :goto_2f
    throw v3

    :goto_30
    goto/32 :goto_53

    nop

    :goto_31
    array-length v6, v0

    goto/32 :goto_4c

    nop

    :goto_32
    int-to-long v8, v6

    goto/32 :goto_4

    nop

    :goto_33
    add-int/lit8 v14, v11, 0x1

    goto/32 :goto_12

    nop

    :goto_34
    aput-object v6, v5, v3

    goto/32 :goto_68

    nop

    :goto_35
    invoke-static {v0, v8, v9}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte([BJ)B

    move-result v6

    goto/32 :goto_74

    nop

    :goto_36
    invoke-static {v0, v9, v10}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte([BJ)B

    move-result v9

    goto/32 :goto_84

    nop

    :goto_37
    sub-int/2addr v3, v1

    goto/32 :goto_5d

    nop

    :goto_38
    invoke-static {v7}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$400(B)Z

    move-result v8

    goto/32 :goto_23

    nop

    :goto_39
    add-int/lit8 v7, v5, -0x1

    goto/32 :goto_87

    nop

    :goto_3a
    move v9, v10

    goto/32 :goto_1

    nop

    :goto_3b
    add-int/lit8 v7, v6, 0x1

    goto/32 :goto_32

    nop

    :goto_3c
    move v11, v7

    goto/32 :goto_5f

    nop

    :goto_3d
    add-int/2addr v14, v4

    goto/32 :goto_52

    nop

    :goto_3e
    const-string v3, "buffer length=%d, index=%d, size=%d"

    goto/32 :goto_6c

    nop

    :goto_3f
    invoke-static {}, Lcom/android/framework/protobuf/InvalidProtocolBufferException;->invalidUtf8()Lcom/android/framework/protobuf/InvalidProtocolBufferException;

    move-result-object v3

    goto/32 :goto_26

    nop

    :goto_40
    add-int/lit8 v6, v2, 0x1

    goto/32 :goto_1f

    nop

    :goto_41
    add-int/lit8 v8, v6, 0x1

    goto/32 :goto_1b

    nop

    :goto_42
    move v2, v6

    goto/32 :goto_3c

    nop

    :goto_43
    add-int/lit8 v6, v6, 0x1

    goto/32 :goto_28

    nop

    :goto_44
    invoke-static {v2, v6, v7, v12, v11}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$900(BBB[CI)V

    goto/32 :goto_5c

    nop

    :goto_45
    goto/16 :goto_2e

    :goto_46
    goto/32 :goto_3f

    nop

    :goto_47
    move v6, v8

    goto/32 :goto_17

    nop

    :goto_48
    new-array v12, v1, [C

    goto/32 :goto_85

    nop

    :goto_49
    add-int/lit8 v6, v7, 0x1

    goto/32 :goto_13

    nop

    :goto_4a
    move v11, v6

    :goto_4b
    goto/32 :goto_2

    nop

    :goto_4c
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto/32 :goto_34

    nop

    :goto_4d
    int-to-long v8, v6

    goto/32 :goto_67

    nop

    :goto_4e
    invoke-static {v2, v6, v12, v11}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$700(BB[CI)V

    goto/32 :goto_7d

    nop

    :goto_4f
    if-nez v7, :cond_7

    goto/32 :goto_30

    :cond_7
    goto/32 :goto_66

    nop

    :goto_50
    int-to-long v9, v7

    goto/32 :goto_7

    nop

    :goto_51
    move v8, v9

    goto/32 :goto_3a

    nop

    :goto_52
    move v2, v13

    goto/32 :goto_2d

    nop

    :goto_53
    invoke-static {v2}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$800(B)Z

    move-result v7

    goto/32 :goto_25

    nop

    :goto_54
    array-length v3, v0

    goto/32 :goto_7c

    nop

    :goto_55
    aput-object v3, v5, v4

    goto/32 :goto_0

    nop

    :goto_56
    goto/16 :goto_d

    :goto_57
    goto/32 :goto_42

    nop

    :goto_58
    add-int/lit8 v9, v11, 0x1

    goto/32 :goto_44

    nop

    :goto_59
    throw v2

    :goto_5a
    add-int/lit8 v8, v7, 0x1

    goto/32 :goto_50

    nop

    :goto_5b
    invoke-static {v0, v7, v8}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte([BJ)B

    move-result v7

    goto/32 :goto_38

    nop

    :goto_5c
    move v2, v8

    goto/32 :goto_62

    nop

    :goto_5d
    or-int/2addr v2, v3

    goto/32 :goto_1d

    nop

    :goto_5e
    if-lt v2, v5, :cond_8

    goto/32 :goto_18

    :cond_8
    goto/32 :goto_5

    nop

    :goto_5f
    goto/16 :goto_2e

    :goto_60
    goto/32 :goto_3

    nop

    :goto_61
    or-int v2, p2, v1

    goto/32 :goto_54

    nop

    :goto_62
    move v11, v9

    goto/32 :goto_45

    nop

    :goto_63
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_41

    nop

    :goto_64
    int-to-long v8, v6

    goto/32 :goto_7b

    nop

    :goto_65
    add-int/lit8 v7, v5, -0x2

    goto/32 :goto_22

    nop

    :goto_66
    if-lt v6, v5, :cond_9

    goto/32 :goto_7a

    :cond_9
    goto/32 :goto_9

    nop

    :goto_67
    invoke-static {v0, v8, v9}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte([BJ)B

    move-result v8

    goto/32 :goto_6

    nop

    :goto_68
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/32 :goto_55

    nop

    :goto_69
    new-instance v4, Ljava/lang/String;

    goto/32 :goto_6b

    nop

    :goto_6a
    move-object/from16 v0, p1

    goto/32 :goto_71

    nop

    :goto_6b
    invoke-direct {v4, v12, v3, v11}, Ljava/lang/String;-><init>([CII)V

    goto/32 :goto_77

    nop

    :goto_6c
    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_6d

    nop

    :goto_6d
    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_59

    nop

    :goto_6e
    add-int/lit8 v7, v11, 0x1

    goto/32 :goto_c

    nop

    :goto_6f
    if-eqz v9, :cond_a

    goto/32 :goto_76

    :cond_a
    goto/32 :goto_75

    nop

    :goto_70
    new-array v5, v5, [Ljava/lang/Object;

    goto/32 :goto_31

    nop

    :goto_71
    move/from16 v1, p3

    goto/32 :goto_61

    nop

    :goto_72
    invoke-static {v2}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$400(B)Z

    move-result v7

    goto/32 :goto_29

    nop

    :goto_73
    move v7, v9

    goto/32 :goto_56

    nop

    :goto_74
    add-int/lit8 v8, v11, 0x1

    goto/32 :goto_4e

    nop

    :goto_75
    goto/16 :goto_57

    :goto_76
    goto/32 :goto_43

    nop

    :goto_77
    return-object v4

    :goto_78
    goto/32 :goto_f

    nop

    :goto_79
    goto/16 :goto_2e

    :goto_7a
    goto/32 :goto_b

    nop

    :goto_7b
    invoke-static {v0, v8, v9}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte([BJ)B

    move-result v8

    goto/32 :goto_49

    nop

    :goto_7c
    sub-int v3, v3, p2

    goto/32 :goto_37

    nop

    :goto_7d
    move v2, v7

    goto/32 :goto_7f

    nop

    :goto_7e
    move v7, v8

    goto/32 :goto_51

    nop

    :goto_7f
    move v11, v8

    goto/32 :goto_79

    nop

    :goto_80
    invoke-static {v0, v6, v7}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte([BJ)B

    move-result v10

    goto/32 :goto_33

    nop

    :goto_81
    add-int/lit8 v7, v6, 0x1

    goto/32 :goto_64

    nop

    :goto_82
    goto/16 :goto_18

    :goto_83
    goto/32 :goto_63

    nop

    :goto_84
    add-int/lit8 v13, v6, 0x1

    goto/32 :goto_e

    nop

    :goto_85
    const/4 v6, 0x0

    :goto_86
    goto/32 :goto_5e

    nop

    :goto_87
    if-lt v6, v7, :cond_b

    goto/32 :goto_46

    :cond_b
    goto/32 :goto_3b

    nop
.end method

.method decodeUtf8Direct(Ljava/nio/ByteBuffer;II)Ljava/lang/String;
    .locals 21
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/framework/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    goto/32 :goto_47

    nop

    :goto_0
    add-long/2addr v9, v15

    goto/32 :goto_26

    nop

    :goto_1
    move/from16 v1, p3

    goto/32 :goto_57

    nop

    :goto_2
    int-to-long v7, v0

    goto/32 :goto_e

    nop

    :goto_3
    invoke-static {v10}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$400(B)Z

    move-result v11

    goto/32 :goto_2c

    nop

    :goto_4
    add-long v11, v9, v15

    goto/32 :goto_3f

    nop

    :goto_5
    invoke-static {v9, v10}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte(J)B

    move-result v11

    goto/32 :goto_83

    nop

    :goto_6
    invoke-static {v9, v10}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte(J)B

    move-result v10

    goto/32 :goto_40

    nop

    :goto_7
    const/4 v4, 0x1

    goto/32 :goto_56

    nop

    :goto_8
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v6

    goto/32 :goto_7e

    nop

    :goto_9
    add-int/2addr v6, v4

    goto/32 :goto_5a

    nop

    :goto_a
    move v6, v12

    goto/32 :goto_3c

    nop

    :goto_b
    if-nez v6, :cond_0

    goto/32 :goto_14

    :cond_0
    goto/32 :goto_65

    nop

    :goto_c
    sub-int/2addr v3, v0

    goto/32 :goto_59

    nop

    :goto_d
    invoke-static {}, Lcom/android/framework/protobuf/InvalidProtocolBufferException;->invalidUtf8()Lcom/android/framework/protobuf/InvalidProtocolBufferException;

    move-result-object v3

    goto/32 :goto_2a

    nop

    :goto_e
    add-long/2addr v5, v7

    goto/32 :goto_28

    nop

    :goto_f
    const/4 v3, 0x2

    goto/32 :goto_44

    nop

    :goto_10
    add-int/lit8 v11, v9, 0x1

    goto/32 :goto_7f

    nop

    :goto_11
    aput-object v3, v5, v4

    goto/32 :goto_f

    nop

    :goto_12
    if-ltz v10, :cond_1

    goto/32 :goto_1e

    :cond_1
    goto/32 :goto_86

    nop

    :goto_13
    throw v3

    :goto_14
    goto/32 :goto_6b

    nop

    :goto_15
    cmp-long v11, v9, v7

    goto/32 :goto_31

    nop

    :goto_16
    goto/16 :goto_5f

    :goto_17
    goto/32 :goto_67

    nop

    :goto_18
    cmp-long v6, v9, v11

    goto/32 :goto_73

    nop

    :goto_19
    invoke-static {v5, v2, v14}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$500(B[CI)V

    :goto_1a
    goto/32 :goto_15

    nop

    :goto_1b
    add-long v9, v5, v15

    goto/32 :goto_69

    nop

    :goto_1c
    if-nez v6, :cond_2

    goto/32 :goto_2b

    :cond_2
    goto/32 :goto_6f

    nop

    :goto_1d
    goto :goto_37

    :goto_1e
    goto/32 :goto_4f

    nop

    :goto_1f
    add-long v11, v9, v15

    goto/32 :goto_51

    nop

    :goto_20
    add-long v9, v11, v15

    goto/32 :goto_62

    nop

    :goto_21
    move-wide v5, v11

    goto/32 :goto_16

    nop

    :goto_22
    goto/16 :goto_5f

    :goto_23
    goto/32 :goto_38

    nop

    :goto_24
    goto :goto_3d

    :goto_25
    goto/32 :goto_0

    nop

    :goto_26
    add-int/lit8 v12, v6, 0x1

    goto/32 :goto_6d

    nop

    :goto_27
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v3

    goto/32 :goto_c

    nop

    :goto_28
    int-to-long v7, v1

    goto/32 :goto_41

    nop

    :goto_29
    move v14, v9

    goto/32 :goto_21

    nop

    :goto_2a
    throw v3

    :goto_2b
    goto/32 :goto_80

    nop

    :goto_2c
    if-eqz v11, :cond_3

    goto/32 :goto_3a

    :cond_3
    goto/32 :goto_39

    nop

    :goto_2d
    throw v2

    :goto_2e
    add-long/2addr v5, v15

    goto/32 :goto_10

    nop

    :goto_2f
    invoke-direct {v4, v2, v3, v14}, Ljava/lang/String;-><init>([CII)V

    goto/32 :goto_71

    nop

    :goto_30
    cmp-long v10, v5, v7

    goto/32 :goto_7c

    nop

    :goto_31
    if-ltz v11, :cond_4

    goto/32 :goto_3d

    :cond_4
    goto/32 :goto_5

    nop

    :goto_32
    aput-object v4, v5, v3

    goto/32 :goto_6a

    nop

    :goto_33
    goto/16 :goto_5f

    :goto_34
    goto/32 :goto_d

    nop

    :goto_35
    add-long v11, v9, v15

    goto/32 :goto_6

    nop

    :goto_36
    const/4 v9, 0x0

    :goto_37
    goto/32 :goto_30

    nop

    :goto_38
    invoke-static {v5}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$600(B)Z

    move-result v6

    goto/32 :goto_b

    nop

    :goto_39
    goto/16 :goto_1e

    :goto_3a
    goto/32 :goto_2e

    nop

    :goto_3b
    move v14, v6

    goto/32 :goto_5c

    nop

    :goto_3c
    goto/16 :goto_1a

    :goto_3d
    goto/32 :goto_3b

    nop

    :goto_3e
    new-array v5, v5, [Ljava/lang/Object;

    goto/32 :goto_8

    nop

    :goto_3f
    invoke-static {v9, v10}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte(J)B

    move-result v6

    goto/32 :goto_20

    nop

    :goto_40
    add-long v17, v11, v15

    goto/32 :goto_4a

    nop

    :goto_41
    add-long/2addr v7, v5

    goto/32 :goto_48

    nop

    :goto_42
    invoke-static {v5, v6, v2, v14}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$700(BB[CI)V

    goto/32 :goto_29

    nop

    :goto_43
    add-int/lit8 v6, v14, 0x1

    goto/32 :goto_66

    nop

    :goto_44
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto/32 :goto_32

    nop

    :goto_45
    goto/16 :goto_50

    :goto_46
    goto/32 :goto_78

    nop

    :goto_47
    move/from16 v0, p2

    goto/32 :goto_1

    nop

    :goto_48
    new-array v2, v1, [C

    goto/32 :goto_36

    nop

    :goto_49
    add-int/lit8 v6, v14, 0x1

    goto/32 :goto_19

    nop

    :goto_4a
    invoke-static {v11, v12}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte(J)B

    move-result v11

    goto/32 :goto_74

    nop

    :goto_4b
    invoke-static {v5, v6, v11, v2, v14}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$900(BBB[CI)V

    goto/32 :goto_53

    nop

    :goto_4c
    const/4 v5, 0x3

    goto/32 :goto_3e

    nop

    :goto_4d
    throw v3

    :goto_4e
    goto/32 :goto_63

    nop

    :goto_4f
    move v14, v9

    :goto_50
    goto/32 :goto_7b

    nop

    :goto_51
    invoke-static {v9, v10}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte(J)B

    move-result v6

    goto/32 :goto_6e

    nop

    :goto_52
    if-ltz v6, :cond_5

    goto/32 :goto_46

    :cond_5
    goto/32 :goto_35

    nop

    :goto_53
    move-wide v5, v9

    goto/32 :goto_5d

    nop

    :goto_54
    or-int/2addr v2, v3

    goto/32 :goto_64

    nop

    :goto_55
    invoke-static/range {v17 .. v18}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte(J)B

    move-result v12

    goto/32 :goto_43

    nop

    :goto_56
    if-gez v2, :cond_6

    goto/32 :goto_72

    :cond_6
    goto/32 :goto_68

    nop

    :goto_57
    or-int v2, v0, v1

    goto/32 :goto_27

    nop

    :goto_58
    invoke-static/range {v9 .. v14}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$1000(BBBB[CI)V

    goto/32 :goto_9

    nop

    :goto_59
    sub-int/2addr v3, v1

    goto/32 :goto_54

    nop

    :goto_5a
    move v14, v6

    goto/32 :goto_5e

    nop

    :goto_5b
    if-nez v6, :cond_7

    goto/32 :goto_23

    :cond_7
    goto/32 :goto_49

    nop

    :goto_5c
    move-wide v5, v9

    goto/32 :goto_22

    nop

    :goto_5d
    move v14, v12

    goto/32 :goto_33

    nop

    :goto_5e
    move-wide/from16 v5, v19

    :goto_5f
    goto/32 :goto_45

    nop

    :goto_60
    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_85

    nop

    :goto_61
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/32 :goto_11

    nop

    :goto_62
    invoke-static {v11, v12}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte(J)B

    move-result v11

    goto/32 :goto_75

    nop

    :goto_63
    new-instance v4, Ljava/lang/String;

    goto/32 :goto_2f

    nop

    :goto_64
    const/4 v3, 0x0

    goto/32 :goto_7

    nop

    :goto_65
    cmp-long v6, v9, v7

    goto/32 :goto_76

    nop

    :goto_66
    move v9, v5

    goto/32 :goto_77

    nop

    :goto_67
    invoke-static {}, Lcom/android/framework/protobuf/InvalidProtocolBufferException;->invalidUtf8()Lcom/android/framework/protobuf/InvalidProtocolBufferException;

    move-result-object v3

    goto/32 :goto_13

    nop

    :goto_68
    invoke-static/range {p1 .. p1}, Lcom/android/framework/protobuf/UnsafeUtil;->addressOffset(Ljava/nio/ByteBuffer;)J

    move-result-wide v5

    goto/32 :goto_2

    nop

    :goto_69
    invoke-static {v5, v6}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte(J)B

    move-result v5

    goto/32 :goto_6c

    nop

    :goto_6a
    const-string v3, "buffer limit=%d, index=%d, limit=%d"

    goto/32 :goto_60

    nop

    :goto_6b
    invoke-static {v5}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$800(B)Z

    move-result v6

    goto/32 :goto_1c

    nop

    :goto_6c
    invoke-static {v5}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$400(B)Z

    move-result v6

    goto/32 :goto_5b

    nop

    :goto_6d
    invoke-static {v11, v2, v6}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$500(B[CI)V

    goto/32 :goto_a

    nop

    :goto_6e
    add-int/lit8 v9, v14, 0x1

    goto/32 :goto_42

    nop

    :goto_6f
    sub-long v11, v7, v15

    goto/32 :goto_18

    nop

    :goto_70
    aput-object v6, v5, v3

    goto/32 :goto_61

    nop

    :goto_71
    return-object v4

    :goto_72
    goto/32 :goto_79

    nop

    :goto_73
    if-ltz v6, :cond_8

    goto/32 :goto_34

    :cond_8
    goto/32 :goto_4

    nop

    :goto_74
    add-long v19, v17, v15

    goto/32 :goto_55

    nop

    :goto_75
    add-int/lit8 v12, v14, 0x1

    goto/32 :goto_4b

    nop

    :goto_76
    if-ltz v6, :cond_9

    goto/32 :goto_17

    :cond_9
    goto/32 :goto_1f

    nop

    :goto_77
    move-object v13, v2

    goto/32 :goto_58

    nop

    :goto_78
    invoke-static {}, Lcom/android/framework/protobuf/InvalidProtocolBufferException;->invalidUtf8()Lcom/android/framework/protobuf/InvalidProtocolBufferException;

    move-result-object v3

    goto/32 :goto_4d

    nop

    :goto_79
    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    goto/32 :goto_4c

    nop

    :goto_7a
    move v9, v11

    goto/32 :goto_1d

    nop

    :goto_7b
    cmp-long v9, v5, v7

    goto/32 :goto_82

    nop

    :goto_7c
    const-wide/16 v15, 0x1

    goto/32 :goto_12

    nop

    :goto_7d
    sub-long v11, v7, v11

    goto/32 :goto_81

    nop

    :goto_7e
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto/32 :goto_70

    nop

    :goto_7f
    invoke-static {v10, v2, v9}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$500(B[CI)V

    goto/32 :goto_7a

    nop

    :goto_80
    const-wide/16 v11, 0x2

    goto/32 :goto_7d

    nop

    :goto_81
    cmp-long v6, v9, v11

    goto/32 :goto_52

    nop

    :goto_82
    if-ltz v9, :cond_a

    goto/32 :goto_4e

    :cond_a
    goto/32 :goto_1b

    nop

    :goto_83
    invoke-static {v11}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$400(B)Z

    move-result v12

    goto/32 :goto_84

    nop

    :goto_84
    if-eqz v12, :cond_b

    goto/32 :goto_25

    :cond_b
    goto/32 :goto_24

    nop

    :goto_85
    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_2d

    nop

    :goto_86
    invoke-static {v5, v6}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte(J)B

    move-result v10

    goto/32 :goto_3

    nop
.end method

.method encodeUtf8(Ljava/lang/CharSequence;[BII)I
    .locals 21

    goto/32 :goto_ad

    nop

    :goto_0
    const/16 v12, 0x80

    goto/32 :goto_4b

    nop

    :goto_1
    and-int/lit8 v6, v2, 0x3f

    goto/32 :goto_ac

    nop

    :goto_2
    cmp-long v12, v4, v12

    goto/32 :goto_21

    nop

    :goto_3
    or-int/lit16 v3, v12, 0x80

    goto/32 :goto_7

    nop

    :goto_4
    throw v2

    :goto_5
    goto/32 :goto_7c

    nop

    :goto_6
    invoke-static {v1, v13, v14, v3}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_1b

    nop

    :goto_7
    int-to-byte v3, v3

    goto/32 :goto_6

    nop

    :goto_8
    cmp-long v12, v4, v12

    goto/32 :goto_66

    nop

    :goto_9
    move/from16 v3, p4

    goto/32 :goto_8f

    nop

    :goto_a
    invoke-interface {v0, v11}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v15

    goto/32 :goto_35

    nop

    :goto_b
    array-length v11, v1

    goto/32 :goto_20

    nop

    :goto_c
    invoke-static {v15, v2}, Ljava/lang/Character;->isSurrogatePair(CC)Z

    move-result v2

    goto/32 :goto_92

    nop

    :goto_d
    if-lt v11, v8, :cond_0

    goto/32 :goto_af

    :cond_0
    goto/32 :goto_a

    nop

    :goto_e
    if-lez v12, :cond_1

    goto/32 :goto_61

    :cond_1
    goto/32 :goto_1f

    nop

    :goto_f
    if-ge v11, v2, :cond_2

    goto/32 :goto_77

    :cond_2
    goto/32 :goto_be

    nop

    :goto_10
    or-int/2addr v12, v13

    goto/32 :goto_11

    nop

    :goto_11
    int-to-byte v12, v12

    goto/32 :goto_3d

    nop

    :goto_12
    int-to-byte v14, v14

    goto/32 :goto_47

    nop

    :goto_13
    const/16 v12, 0x80

    goto/32 :goto_15

    nop

    :goto_14
    move/from16 v3, p4

    goto/32 :goto_70

    nop

    :goto_15
    const-wide/16 v13, 0x1

    goto/32 :goto_83

    nop

    :goto_16
    goto/16 :goto_bf

    :goto_17
    goto/32 :goto_2f

    nop

    :goto_18
    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_ae

    nop

    :goto_19
    add-long v13, v4, v2

    goto/32 :goto_54

    nop

    :goto_1a
    if-le v3, v15, :cond_3

    goto/32 :goto_5

    :cond_3
    goto/32 :goto_50

    nop

    :goto_1b
    const-wide/16 v12, 0x1

    goto/32 :goto_3c

    nop

    :goto_1c
    or-int/lit16 v12, v12, 0x1e0

    goto/32 :goto_b7

    nop

    :goto_1d
    sub-long v12, v6, v12

    goto/32 :goto_8

    nop

    :goto_1e
    add-int/lit8 v2, v11, 0x1

    goto/32 :goto_4c

    nop

    :goto_1f
    add-long v2, v4, v13

    goto/32 :goto_27

    nop

    :goto_20
    sub-int/2addr v11, v3

    goto/32 :goto_f

    nop

    :goto_21
    if-lez v12, :cond_4

    goto/32 :goto_5c

    :cond_4
    goto/32 :goto_b8

    nop

    :goto_22
    add-long v4, v6, v12

    goto/32 :goto_c7

    nop

    :goto_23
    if-lt v15, v12, :cond_5

    goto/32 :goto_17

    :cond_5
    goto/32 :goto_40

    nop

    :goto_24
    if-ne v2, v8, :cond_6

    goto/32 :goto_93

    :cond_6
    goto/32 :goto_bc

    nop

    :goto_25
    move-wide/from16 v16, v6

    goto/32 :goto_52

    nop

    :goto_26
    cmp-long v16, v4, v6

    goto/32 :goto_74

    nop

    :goto_27
    ushr-int/lit8 v12, v15, 0x6

    goto/32 :goto_2e

    nop

    :goto_28
    or-int/lit16 v13, v14, 0x80

    goto/32 :goto_c3

    nop

    :goto_29
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_42

    nop

    :goto_2a
    add-long/2addr v6, v4

    goto/32 :goto_5d

    nop

    :goto_2b
    move-wide/from16 v16, v6

    goto/32 :goto_1a

    nop

    :goto_2c
    or-int/2addr v3, v2

    goto/32 :goto_38

    nop

    :goto_2d
    invoke-static {v15, v2}, Ljava/lang/Character;->isSurrogatePair(CC)Z

    move-result v2

    goto/32 :goto_a5

    nop

    :goto_2e
    or-int/lit16 v12, v12, 0x3c0

    goto/32 :goto_b3

    nop

    :goto_2f
    move/from16 v15, v16

    :goto_30
    goto/32 :goto_6b

    nop

    :goto_31
    invoke-interface {v0, v11}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v15

    goto/32 :goto_8d

    nop

    :goto_32
    invoke-direct {v2, v11, v8}, Lcom/android/framework/protobuf/Utf8$UnpairedSurrogateException;-><init>(II)V

    goto/32 :goto_4

    nop

    :goto_33
    and-int/lit8 v14, v14, 0x3f

    goto/32 :goto_4a

    nop

    :goto_34
    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_96

    nop

    :goto_35
    if-lt v15, v12, :cond_7

    goto/32 :goto_8a

    :cond_7
    goto/32 :goto_26

    nop

    :goto_36
    const v3, 0xd800

    goto/32 :goto_90

    nop

    :goto_37
    add-int/lit8 v11, v11, 0x1

    goto/32 :goto_82

    nop

    :goto_38
    int-to-byte v2, v3

    goto/32 :goto_73

    nop

    :goto_39
    and-int/lit8 v12, v12, 0x3f

    goto/32 :goto_6a

    nop

    :goto_3a
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_6f

    nop

    :goto_3b
    or-int/lit16 v14, v14, 0xf0

    goto/32 :goto_12

    nop

    :goto_3c
    add-long v19, v4, v12

    goto/32 :goto_98

    nop

    :goto_3d
    invoke-static {v1, v2, v3, v12}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_58

    nop

    :goto_3e
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_3a

    nop

    :goto_3f
    int-to-byte v12, v15

    goto/32 :goto_81

    nop

    :goto_40
    add-long v12, v4, v13

    goto/32 :goto_80

    nop

    :goto_41
    move-wide/from16 v16, v6

    goto/32 :goto_4f

    nop

    :goto_42
    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_86

    nop

    :goto_43
    const-wide/16 v6, 0x1

    goto/32 :goto_c9

    nop

    :goto_44
    invoke-static {v15, v3}, Ljava/lang/Character;->toCodePoint(CC)I

    move-result v2

    goto/32 :goto_ab

    nop

    :goto_45
    new-instance v2, Lcom/android/framework/protobuf/Utf8$UnpairedSurrogateException;

    goto/32 :goto_32

    nop

    :goto_46
    const-wide/16 v12, 0x1

    goto/32 :goto_b4

    nop

    :goto_47
    invoke-static {v1, v4, v5, v14}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_22

    nop

    :goto_48
    const-wide/16 v12, 0x3

    goto/32 :goto_64

    nop

    :goto_49
    sub-long v16, v6, v16

    goto/32 :goto_a9

    nop

    :goto_4a
    const/16 v12, 0x80

    goto/32 :goto_28

    nop

    :goto_4b
    move-wide/from16 v16, v6

    goto/32 :goto_89

    nop

    :goto_4c
    if-ne v2, v8, :cond_8

    goto/32 :goto_53

    :cond_8
    goto/32 :goto_37

    nop

    :goto_4d
    const/16 v12, 0x80

    goto/32 :goto_60

    nop

    :goto_4e
    const-string v10, "Failed writing "

    goto/32 :goto_c0

    nop

    :goto_4f
    move-wide/from16 v4, v19

    goto/32 :goto_b9

    nop

    :goto_50
    if-le v15, v2, :cond_9

    goto/32 :goto_5

    :cond_9
    goto/32 :goto_6c

    nop

    :goto_51
    const v2, 0xdfff

    goto/32 :goto_36

    nop

    :goto_52
    goto :goto_56

    :goto_53
    goto/32 :goto_55

    nop

    :goto_54
    ushr-int/lit8 v12, v15, 0xc

    goto/32 :goto_1c

    nop

    :goto_55
    move-wide/from16 v16, v6

    :goto_56
    goto/32 :goto_5a

    nop

    :goto_57
    and-int/lit8 v6, v18, 0x3f

    goto/32 :goto_b6

    nop

    :goto_58
    move-wide/from16 v16, v6

    goto/32 :goto_4d

    nop

    :goto_59
    if-lt v15, v12, :cond_a

    goto/32 :goto_61

    :cond_a
    goto/32 :goto_7a

    nop

    :goto_5a
    new-instance v2, Lcom/android/framework/protobuf/Utf8$UnpairedSurrogateException;

    goto/32 :goto_b5

    nop

    :goto_5b
    goto :goto_5f

    :goto_5c
    goto/32 :goto_9c

    nop

    :goto_5d
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    move-result v8

    goto/32 :goto_97

    nop

    :goto_5e
    invoke-static {v1, v13, v14, v6}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    nop

    :goto_5f
    goto/32 :goto_6e

    nop

    :goto_60
    goto/16 :goto_5f

    :goto_61
    goto/32 :goto_51

    nop

    :goto_62
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_67

    nop

    :goto_63
    move/from16 v2, p3

    goto/32 :goto_9

    nop

    :goto_64
    sub-long v12, v6, v12

    goto/32 :goto_2

    nop

    :goto_65
    int-to-long v6, v3

    goto/32 :goto_2a

    nop

    :goto_66
    if-lez v12, :cond_b

    goto/32 :goto_88

    :cond_b
    goto/32 :goto_1e

    nop

    :goto_67
    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_7b

    nop

    :goto_68
    return v9

    :goto_69
    goto/32 :goto_d

    nop

    :goto_6a
    const/16 v2, 0x80

    goto/32 :goto_3

    nop

    :goto_6b
    if-eq v11, v8, :cond_c

    goto/32 :goto_69

    :cond_c
    goto/32 :goto_c4

    nop

    :goto_6c
    add-int/lit8 v2, v11, 0x1

    goto/32 :goto_24

    nop

    :goto_6d
    long-to-int v2, v4

    goto/32 :goto_76

    nop

    :goto_6e
    add-int/lit8 v11, v11, 0x1

    goto/32 :goto_bd

    nop

    :goto_6f
    add-int v6, p3, p4

    goto/32 :goto_b2

    nop

    :goto_70
    move-wide/from16 v6, v16

    goto/32 :goto_c1

    nop

    :goto_71
    const-wide/16 v6, 0x1

    goto/32 :goto_a7

    nop

    :goto_72
    new-instance v3, Ljava/lang/StringBuilder;

    goto/32 :goto_7d

    nop

    :goto_73
    invoke-static {v1, v4, v5, v2}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_41

    nop

    :goto_74
    if-ltz v16, :cond_d

    goto/32 :goto_8a

    :cond_d
    goto/32 :goto_95

    nop

    :goto_75
    move-wide/from16 v16, v6

    goto/32 :goto_8c

    nop

    :goto_76
    return v2

    :goto_77
    goto/32 :goto_75

    nop

    :goto_78
    int-to-byte v14, v15

    goto/32 :goto_a2

    nop

    :goto_79
    invoke-static {v1, v6, v7, v13}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_71

    nop

    :goto_7a
    const-wide/16 v16, 0x2

    goto/32 :goto_49

    nop

    :goto_7b
    throw v2

    :goto_7c
    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    goto/32 :goto_72

    nop

    :goto_7d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_34

    nop

    :goto_7e
    invoke-static {v1, v4, v5, v12}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_c8

    nop

    :goto_7f
    invoke-interface {v0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    goto/32 :goto_c

    nop

    :goto_80
    move/from16 v15, v16

    goto/32 :goto_78

    nop

    :goto_81
    invoke-static {v1, v4, v5, v12}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_c2

    nop

    :goto_82
    invoke-interface {v0, v11}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    goto/32 :goto_a4

    nop

    :goto_83
    if-lt v11, v8, :cond_e

    goto/32 :goto_30

    :cond_e
    goto/32 :goto_31

    nop

    :goto_84
    goto/16 :goto_69

    :goto_85
    goto/32 :goto_25

    nop

    :goto_86
    add-int/lit8 v6, v8, -0x1

    goto/32 :goto_c6

    nop

    :goto_87
    throw v2

    :goto_88
    goto/32 :goto_2b

    nop

    :goto_89
    goto/16 :goto_5f

    :goto_8a
    goto/32 :goto_b1

    nop

    :goto_8b
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_91

    nop

    :goto_8c
    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    goto/32 :goto_9a

    nop

    :goto_8d
    move/from16 v16, v15

    goto/32 :goto_23

    nop

    :goto_8e
    ushr-int/lit8 v14, v2, 0x12

    goto/32 :goto_3b

    nop

    :goto_8f
    int-to-long v4, v2

    goto/32 :goto_65

    nop

    :goto_90
    if-ge v15, v3, :cond_f

    goto/32 :goto_9e

    :cond_f
    goto/32 :goto_9d

    nop

    :goto_91
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_aa

    nop

    :goto_92
    if-eqz v2, :cond_10

    goto/32 :goto_5

    :cond_10
    :goto_93
    goto/32 :goto_45

    nop

    :goto_94
    invoke-static {v1, v4, v5, v6}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_43

    nop

    :goto_95
    add-long v16, v4, v13

    goto/32 :goto_3f

    nop

    :goto_96
    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_8b

    nop

    :goto_97
    const-string v9, " at index "

    goto/32 :goto_4e

    nop

    :goto_98
    and-int/lit8 v3, v15, 0x3f

    goto/32 :goto_2c

    nop

    :goto_99
    add-int/lit8 v11, v11, 0x1

    goto/32 :goto_9b

    nop

    :goto_9a
    new-instance v3, Ljava/lang/StringBuilder;

    goto/32 :goto_29

    nop

    :goto_9b
    move-wide v4, v12

    goto/32 :goto_16

    nop

    :goto_9c
    const-wide/16 v12, 0x4

    goto/32 :goto_1d

    nop

    :goto_9d
    if-lt v2, v15, :cond_11

    goto/32 :goto_5c

    :cond_11
    :goto_9e
    goto/32 :goto_48

    nop

    :goto_9f
    invoke-static {v1, v4, v5, v12}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_bb

    nop

    :goto_a0
    invoke-direct {v2, v3, v8}, Lcom/android/framework/protobuf/Utf8$UnpairedSurrogateException;-><init>(II)V

    goto/32 :goto_87

    nop

    :goto_a1
    int-to-byte v6, v6

    goto/32 :goto_5e

    nop

    :goto_a2
    invoke-static {v1, v4, v5, v14}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_99

    nop

    :goto_a3
    ushr-int/lit8 v12, v15, 0x6

    goto/32 :goto_39

    nop

    :goto_a4
    move v3, v2

    goto/32 :goto_2d

    nop

    :goto_a5
    if-nez v2, :cond_12

    goto/32 :goto_85

    :cond_12
    goto/32 :goto_44

    nop

    :goto_a6
    int-to-byte v6, v6

    goto/32 :goto_94

    nop

    :goto_a7
    add-long v13, v4, v6

    goto/32 :goto_b0

    nop

    :goto_a8
    const/16 v13, 0x80

    goto/32 :goto_10

    nop

    :goto_a9
    cmp-long v12, v4, v16

    goto/32 :goto_e

    nop

    :goto_aa
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_18

    nop

    :goto_ab
    move-wide/from16 v16, v6

    goto/32 :goto_46

    nop

    :goto_ac
    or-int/2addr v6, v12

    goto/32 :goto_a1

    nop

    :goto_ad
    move-object/from16 v0, p1

    goto/32 :goto_c5

    nop

    :goto_ae
    throw v2

    :goto_af
    goto/32 :goto_6d

    nop

    :goto_b0
    ushr-int/lit8 v18, v2, 0x6

    goto/32 :goto_57

    nop

    :goto_b1
    const/16 v12, 0x800

    goto/32 :goto_59

    nop

    :goto_b2
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_62

    nop

    :goto_b3
    int-to-byte v12, v12

    goto/32 :goto_7e

    nop

    :goto_b4
    add-long v6, v4, v12

    goto/32 :goto_8e

    nop

    :goto_b5
    add-int/lit8 v3, v11, -0x1

    goto/32 :goto_a0

    nop

    :goto_b6
    or-int/2addr v6, v12

    goto/32 :goto_a6

    nop

    :goto_b7
    int-to-byte v12, v12

    goto/32 :goto_9f

    nop

    :goto_b8
    const-wide/16 v2, 0x1

    goto/32 :goto_19

    nop

    :goto_b9
    const/16 v12, 0x80

    goto/32 :goto_5b

    nop

    :goto_ba
    and-int/lit8 v12, v15, 0x3f

    goto/32 :goto_a8

    nop

    :goto_bb
    add-long v4, v13, v2

    goto/32 :goto_a3

    nop

    :goto_bc
    add-int/lit8 v2, v11, 0x1

    goto/32 :goto_7f

    nop

    :goto_bd
    move/from16 v2, p3

    goto/32 :goto_14

    nop

    :goto_be
    const/4 v11, 0x0

    :goto_bf
    goto/32 :goto_13

    nop

    :goto_c0
    if-le v8, v3, :cond_13

    goto/32 :goto_77

    :cond_13
    goto/32 :goto_b

    nop

    :goto_c1
    const-wide/16 v13, 0x1

    goto/32 :goto_84

    nop

    :goto_c2
    move-wide/from16 v4, v16

    goto/32 :goto_0

    nop

    :goto_c3
    int-to-byte v13, v13

    goto/32 :goto_79

    nop

    :goto_c4
    long-to-int v9, v4

    goto/32 :goto_68

    nop

    :goto_c5
    move-object/from16 v1, p2

    goto/32 :goto_63

    nop

    :goto_c6
    invoke-interface {v0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    goto/32 :goto_3e

    nop

    :goto_c7
    ushr-int/lit8 v14, v2, 0xc

    goto/32 :goto_33

    nop

    :goto_c8
    add-long v4, v2, v13

    goto/32 :goto_ba

    nop

    :goto_c9
    add-long v4, v13, v6

    goto/32 :goto_1

    nop
.end method

.method encodeUtf8Direct(Ljava/lang/CharSequence;Ljava/nio/ByteBuffer;)V
    .locals 21

    goto/32 :goto_be

    nop

    :goto_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_af

    nop

    :goto_1
    move-object v2, v1

    goto/32 :goto_59

    nop

    :goto_2
    ushr-int/lit8 v13, v1, 0x12

    goto/32 :goto_27

    nop

    :goto_3
    add-long v16, v4, v13

    goto/32 :goto_b8

    nop

    :goto_4
    const-wide/16 v16, 0x2

    goto/32 :goto_c8

    nop

    :goto_5
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_8e

    nop

    :goto_6
    invoke-static {v4, v5, v12}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_14

    nop

    :goto_7
    move-wide/from16 v18, v13

    goto/32 :goto_e

    nop

    :goto_8
    if-lez v3, :cond_0

    goto/32 :goto_c0

    :cond_0
    goto/32 :goto_87

    nop

    :goto_9
    throw v1

    :goto_a
    goto/32 :goto_9f

    nop

    :goto_b
    invoke-direct {v1, v2, v8}, Lcom/android/framework/protobuf/Utf8$UnpairedSurrogateException;-><init>(II)V

    goto/32 :goto_44

    nop

    :goto_c
    invoke-static {v15, v1}, Ljava/lang/Character;->isSurrogatePair(CC)Z

    move-result v1

    goto/32 :goto_a7

    nop

    :goto_d
    const v1, 0xdfff

    goto/32 :goto_52

    nop

    :goto_e
    move v13, v15

    goto/32 :goto_ab

    nop

    :goto_f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_cf

    nop

    :goto_10
    invoke-interface {v0, v9}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v15

    goto/32 :goto_41

    nop

    :goto_11
    int-to-byte v5, v5

    goto/32 :goto_20

    nop

    :goto_12
    move-wide/from16 v13, v18

    goto/32 :goto_d2

    nop

    :goto_13
    move-object/from16 v1, p2

    goto/32 :goto_53

    nop

    :goto_14
    move-wide/from16 v18, v13

    goto/32 :goto_92

    nop

    :goto_15
    invoke-static {v14, v15, v2}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    nop

    :goto_16
    goto/32 :goto_9b

    nop

    :goto_17
    or-int/2addr v5, v12

    goto/32 :goto_7e

    nop

    :goto_18
    sub-long v19, v6, v19

    goto/32 :goto_3b

    nop

    :goto_19
    or-int/2addr v5, v12

    goto/32 :goto_11

    nop

    :goto_1a
    invoke-interface {v0, v9}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    goto/32 :goto_86

    nop

    :goto_1b
    move-wide/from16 v18, v13

    goto/32 :goto_72

    nop

    :goto_1c
    invoke-interface {v0, v9}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v9

    goto/32 :goto_5

    nop

    :goto_1d
    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_3a

    nop

    :goto_1e
    sub-long v10, v4, v2

    goto/32 :goto_29

    nop

    :goto_1f
    or-int/2addr v5, v12

    goto/32 :goto_67

    nop

    :goto_20
    invoke-static {v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_9e

    nop

    :goto_21
    add-int/lit8 v9, v8, -0x1

    goto/32 :goto_1c

    nop

    :goto_22
    int-to-long v9, v8

    goto/32 :goto_4b

    nop

    :goto_23
    sub-long v1, v4, v16

    goto/32 :goto_7a

    nop

    :goto_24
    int-to-byte v14, v15

    goto/32 :goto_5d

    nop

    :goto_25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_2f

    nop

    :goto_26
    or-int/lit16 v3, v3, 0x3c0

    goto/32 :goto_94

    nop

    :goto_27
    or-int/lit16 v13, v13, 0xf0

    goto/32 :goto_35

    nop

    :goto_28
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_33

    nop

    :goto_29
    long-to-int v10, v10

    goto/32 :goto_77

    nop

    :goto_2a
    if-lt v1, v15, :cond_1

    goto/32 :goto_c0

    :cond_1
    :goto_2b
    goto/32 :goto_b9

    nop

    :goto_2c
    add-long v4, v2, v13

    goto/32 :goto_82

    nop

    :goto_2d
    invoke-interface {v0, v9}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v15

    goto/32 :goto_69

    nop

    :goto_2e
    if-lt v15, v12, :cond_2

    goto/32 :goto_32

    :cond_2
    goto/32 :goto_4

    nop

    :goto_2f
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_1d

    nop

    :goto_30
    new-instance v3, Ljava/lang/StringBuilder;

    goto/32 :goto_f

    nop

    :goto_31
    goto/16 :goto_16

    :goto_32
    goto/32 :goto_5f

    nop

    :goto_33
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_68

    nop

    :goto_34
    if-le v2, v3, :cond_3

    goto/32 :goto_a

    :cond_3
    goto/32 :goto_40

    nop

    :goto_35
    int-to-byte v13, v13

    goto/32 :goto_39

    nop

    :goto_36
    const/16 v14, 0x80

    goto/32 :goto_89

    nop

    :goto_37
    if-ne v1, v8, :cond_4

    goto/32 :goto_cc

    :cond_4
    goto/32 :goto_b0

    nop

    :goto_38
    add-long v4, v14, v18

    goto/32 :goto_60

    nop

    :goto_39
    invoke-static {v4, v5, v13}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_48

    nop

    :goto_3a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_47

    nop

    :goto_3b
    cmp-long v3, v4, v19

    goto/32 :goto_8

    nop

    :goto_3c
    goto/16 :goto_80

    :goto_3d
    goto/32 :goto_7f

    nop

    :goto_3e
    return-void

    :goto_3f
    goto/32 :goto_c3

    nop

    :goto_40
    if-le v3, v1, :cond_5

    goto/32 :goto_a

    :cond_5
    goto/32 :goto_d8

    nop

    :goto_41
    if-lt v15, v12, :cond_6

    goto/32 :goto_b4

    :cond_6
    goto/32 :goto_75

    nop

    :goto_42
    add-int/lit8 v9, v9, 0x1

    goto/32 :goto_65

    nop

    :goto_43
    if-lez v3, :cond_7

    goto/32 :goto_45

    :cond_7
    goto/32 :goto_6c

    nop

    :goto_44
    throw v1

    :goto_45
    goto/32 :goto_c7

    nop

    :goto_46
    or-int/2addr v2, v3

    goto/32 :goto_4e

    nop

    :goto_47
    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_a2

    nop

    :goto_48
    const-wide/16 v13, 0x1

    goto/32 :goto_2c

    nop

    :goto_49
    const/16 v12, 0x80

    goto/32 :goto_19

    nop

    :goto_4a
    const-string v10, " at index "

    goto/32 :goto_b1

    nop

    :goto_4b
    sub-long v11, v6, v4

    goto/32 :goto_ad

    nop

    :goto_4c
    const/4 v9, 0x0

    :goto_4d
    goto/32 :goto_8b

    nop

    :goto_4e
    int-to-byte v2, v2

    goto/32 :goto_58

    nop

    :goto_4f
    new-instance v1, Lcom/android/framework/protobuf/Utf8$UnpairedSurrogateException;

    goto/32 :goto_c4

    nop

    :goto_50
    add-long v3, v1, v13

    goto/32 :goto_61

    nop

    :goto_51
    or-int/2addr v2, v3

    goto/32 :goto_a5

    nop

    :goto_52
    const v2, 0xd800

    goto/32 :goto_81

    nop

    :goto_53
    move v12, v3

    goto/32 :goto_71

    nop

    :goto_54
    const/16 v12, 0x800

    goto/32 :goto_2e

    nop

    :goto_55
    new-instance v1, Lcom/android/framework/protobuf/Utf8$UnpairedSurrogateException;

    goto/32 :goto_d7

    nop

    :goto_56
    const-wide/16 v18, 0x1

    goto/32 :goto_38

    nop

    :goto_57
    invoke-static {v15, v2}, Ljava/lang/Character;->toCodePoint(CC)I

    move-result v1

    goto/32 :goto_6e

    nop

    :goto_58
    invoke-static {v4, v5, v2}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_56

    nop

    :goto_59
    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    goto/32 :goto_30

    nop

    :goto_5a
    move/from16 v15, v16

    goto/32 :goto_24

    nop

    :goto_5b
    if-ne v1, v8, :cond_8

    goto/32 :goto_3d

    :cond_8
    goto/32 :goto_9a

    nop

    :goto_5c
    const-wide/16 v19, 0x4

    goto/32 :goto_cd

    nop

    :goto_5d
    invoke-static {v4, v5, v14}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_42

    nop

    :goto_5e
    const/16 v3, 0x80

    goto/32 :goto_bf

    nop

    :goto_5f
    move-wide/from16 v16, v2

    goto/32 :goto_d

    nop

    :goto_60
    and-int/lit8 v2, v1, 0x3f

    goto/32 :goto_51

    nop

    :goto_61
    ushr-int/lit8 v5, v15, 0x6

    goto/32 :goto_9d

    nop

    :goto_62
    add-long/2addr v6, v2

    goto/32 :goto_b2

    nop

    :goto_63
    invoke-virtual/range {p2 .. p2}, Ljava/nio/ByteBuffer;->limit()I

    move-result v9

    goto/32 :goto_78

    nop

    :goto_64
    add-long v1, v4, v13

    goto/32 :goto_c5

    nop

    :goto_65
    move-wide v4, v12

    goto/32 :goto_84

    nop

    :goto_66
    invoke-static/range {p2 .. p2}, Lcom/android/framework/protobuf/UnsafeUtil;->addressOffset(Ljava/nio/ByteBuffer;)J

    move-result-wide v2

    goto/32 :goto_d6

    nop

    :goto_67
    int-to-byte v5, v5

    goto/32 :goto_bc

    nop

    :goto_68
    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_25

    nop

    :goto_69
    move/from16 v16, v15

    goto/32 :goto_8a

    nop

    :goto_6a
    move/from16 v15, v16

    :goto_6b
    goto/32 :goto_74

    nop

    :goto_6c
    add-int/lit8 v1, v9, 0x1

    goto/32 :goto_5b

    nop

    :goto_6d
    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    goto/32 :goto_aa

    nop

    :goto_6e
    move v12, v2

    goto/32 :goto_ca

    nop

    :goto_6f
    int-to-long v4, v4

    goto/32 :goto_91

    nop

    :goto_70
    move-object/from16 v2, p2

    goto/32 :goto_8f

    nop

    :goto_71
    move-wide/from16 v2, v16

    goto/32 :goto_12

    nop

    :goto_72
    move v13, v15

    goto/32 :goto_5e

    nop

    :goto_73
    and-int/lit8 v13, v19, 0x3f

    goto/32 :goto_36

    nop

    :goto_74
    if-eq v9, v8, :cond_9

    goto/32 :goto_3f

    :cond_9
    goto/32 :goto_1e

    nop

    :goto_75
    cmp-long v16, v4, v6

    goto/32 :goto_c6

    nop

    :goto_76
    invoke-static {v4, v5, v3}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_93

    nop

    :goto_77
    invoke-virtual {v1, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    goto/32 :goto_3e

    nop

    :goto_78
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_0

    nop

    :goto_79
    int-to-byte v13, v13

    goto/32 :goto_d9

    nop

    :goto_7a
    long-to-int v1, v1

    goto/32 :goto_70

    nop

    :goto_7b
    if-lez v9, :cond_a

    goto/32 :goto_a9

    :cond_a
    goto/32 :goto_4c

    nop

    :goto_7c
    and-int/lit8 v5, v15, 0x3f

    goto/32 :goto_17

    nop

    :goto_7d
    throw v1

    :goto_7e
    int-to-byte v5, v5

    goto/32 :goto_99

    nop

    :goto_7f
    move v13, v15

    :goto_80
    goto/32 :goto_55

    nop

    :goto_81
    if-ge v15, v2, :cond_b

    goto/32 :goto_2b

    :cond_b
    goto/32 :goto_2a

    nop

    :goto_82
    ushr-int/lit8 v19, v1, 0xc

    goto/32 :goto_73

    nop

    :goto_83
    move v12, v2

    goto/32 :goto_88

    nop

    :goto_84
    goto/16 :goto_4d

    :goto_85
    goto/32 :goto_6a

    nop

    :goto_86
    move v2, v1

    goto/32 :goto_c

    nop

    :goto_87
    add-long v1, v4, v13

    goto/32 :goto_b7

    nop

    :goto_88
    move v13, v15

    goto/32 :goto_3c

    nop

    :goto_89
    or-int/2addr v13, v14

    goto/32 :goto_79

    nop

    :goto_8a
    if-lt v15, v12, :cond_c

    goto/32 :goto_85

    :cond_c
    goto/32 :goto_a0

    nop

    :goto_8b
    const/16 v12, 0x80

    goto/32 :goto_90

    nop

    :goto_8c
    move-wide/from16 v16, v2

    goto/32 :goto_97

    nop

    :goto_8d
    add-long v14, v4, v2

    goto/32 :goto_98

    nop

    :goto_8e
    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_63

    nop

    :goto_8f
    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    goto/32 :goto_a8

    nop

    :goto_90
    const-wide/16 v13, 0x1

    goto/32 :goto_95

    nop

    :goto_91
    add-long/2addr v4, v2

    goto/32 :goto_ce

    nop

    :goto_92
    move v13, v15

    goto/32 :goto_d1

    nop

    :goto_93
    add-long v3, v1, v13

    goto/32 :goto_d5

    nop

    :goto_94
    int-to-byte v3, v3

    goto/32 :goto_76

    nop

    :goto_95
    if-lt v9, v8, :cond_d

    goto/32 :goto_6b

    :cond_d
    goto/32 :goto_2d

    nop

    :goto_96
    move-wide/from16 v16, v2

    goto/32 :goto_64

    nop

    :goto_97
    const/16 v3, 0x80

    goto/32 :goto_b3

    nop

    :goto_98
    ushr-int/lit8 v19, v1, 0x6

    goto/32 :goto_da

    nop

    :goto_99
    invoke-static {v3, v4, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_c2

    nop

    :goto_9a
    add-int/lit8 v9, v9, 0x1

    goto/32 :goto_1a

    nop

    :goto_9b
    add-int/lit8 v9, v9, 0x1

    goto/32 :goto_13

    nop

    :goto_9c
    move-wide/from16 v16, v2

    goto/32 :goto_23

    nop

    :goto_9d
    and-int/lit8 v5, v5, 0x3f

    goto/32 :goto_49

    nop

    :goto_9e
    add-long v1, v3, v13

    goto/32 :goto_7c

    nop

    :goto_9f
    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    goto/32 :goto_28

    nop

    :goto_a0
    add-long v12, v4, v13

    goto/32 :goto_5a

    nop

    :goto_a1
    const-wide/16 v2, 0x1

    goto/32 :goto_8d

    nop

    :goto_a2
    throw v1

    :goto_a3
    goto/32 :goto_9c

    nop

    :goto_a4
    int-to-byte v3, v3

    goto/32 :goto_b6

    nop

    :goto_a5
    int-to-byte v2, v2

    goto/32 :goto_15

    nop

    :goto_a6
    int-to-long v6, v6

    goto/32 :goto_62

    nop

    :goto_a7
    if-nez v1, :cond_e

    goto/32 :goto_d3

    :cond_e
    goto/32 :goto_57

    nop

    :goto_a8
    return-void

    :goto_a9
    goto/32 :goto_ba

    nop

    :goto_aa
    invoke-static {v3, v1}, Ljava/lang/Character;->isSurrogatePair(CC)Z

    move-result v1

    goto/32 :goto_cb

    nop

    :goto_ab
    const/16 v3, 0x80

    goto/32 :goto_31

    nop

    :goto_ac
    move-object/from16 v1, p2

    goto/32 :goto_66

    nop

    :goto_ad
    cmp-long v9, v9, v11

    goto/32 :goto_4a

    nop

    :goto_ae
    if-lez v12, :cond_f

    goto/32 :goto_32

    :cond_f
    goto/32 :goto_96

    nop

    :goto_af
    invoke-direct {v1, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_7d

    nop

    :goto_b0
    add-int/lit8 v1, v9, 0x1

    goto/32 :goto_6d

    nop

    :goto_b1
    const-string v11, "Failed writing "

    goto/32 :goto_7b

    nop

    :goto_b2
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    move-result v8

    goto/32 :goto_22

    nop

    :goto_b3
    goto/16 :goto_16

    :goto_b4
    goto/32 :goto_54

    nop

    :goto_b5
    cmp-long v3, v4, v19

    goto/32 :goto_43

    nop

    :goto_b6
    invoke-static {v4, v5, v3}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_50

    nop

    :goto_b7
    ushr-int/lit8 v3, v15, 0xc

    goto/32 :goto_c9

    nop

    :goto_b8
    int-to-byte v12, v15

    goto/32 :goto_6

    nop

    :goto_b9
    const-wide/16 v19, 0x3

    goto/32 :goto_18

    nop

    :goto_ba
    move-wide/from16 v16, v2

    goto/32 :goto_1

    nop

    :goto_bb
    move v13, v15

    goto/32 :goto_a1

    nop

    :goto_bc
    invoke-static {v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_d4

    nop

    :goto_bd
    const/16 v3, 0x80

    goto/32 :goto_46

    nop

    :goto_be
    move-object/from16 v0, p1

    goto/32 :goto_ac

    nop

    :goto_bf
    goto/16 :goto_16

    :goto_c0
    goto/32 :goto_5c

    nop

    :goto_c1
    const/16 v12, 0x80

    goto/32 :goto_1f

    nop

    :goto_c2
    move-wide v4, v1

    goto/32 :goto_1b

    nop

    :goto_c3
    if-lt v9, v8, :cond_10

    goto/32 :goto_a3

    :cond_10
    goto/32 :goto_10

    nop

    :goto_c4
    invoke-direct {v1, v9, v8}, Lcom/android/framework/protobuf/Utf8$UnpairedSurrogateException;-><init>(II)V

    goto/32 :goto_9

    nop

    :goto_c5
    ushr-int/lit8 v3, v15, 0x6

    goto/32 :goto_26

    nop

    :goto_c6
    if-ltz v16, :cond_11

    goto/32 :goto_b4

    :cond_11
    goto/32 :goto_3

    nop

    :goto_c7
    move v13, v15

    goto/32 :goto_d0

    nop

    :goto_c8
    sub-long v16, v6, v16

    goto/32 :goto_db

    nop

    :goto_c9
    or-int/lit16 v3, v3, 0x1e0

    goto/32 :goto_a4

    nop

    :goto_ca
    add-long v2, v4, v13

    goto/32 :goto_2

    nop

    :goto_cb
    if-eqz v1, :cond_12

    goto/32 :goto_a

    :cond_12
    :goto_cc
    goto/32 :goto_4f

    nop

    :goto_cd
    sub-long v19, v6, v19

    goto/32 :goto_b5

    nop

    :goto_ce
    invoke-virtual/range {p2 .. p2}, Ljava/nio/ByteBuffer;->limit()I

    move-result v6

    goto/32 :goto_a6

    nop

    :goto_cf
    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_21

    nop

    :goto_d0
    move v3, v13

    goto/32 :goto_34

    nop

    :goto_d1
    move-wide/from16 v4, v16

    goto/32 :goto_8c

    nop

    :goto_d2
    goto/16 :goto_3f

    :goto_d3
    goto/32 :goto_83

    nop

    :goto_d4
    move-wide v4, v3

    goto/32 :goto_7

    nop

    :goto_d5
    and-int/lit8 v5, v15, 0x3f

    goto/32 :goto_c1

    nop

    :goto_d6
    invoke-virtual/range {p2 .. p2}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    goto/32 :goto_6f

    nop

    :goto_d7
    add-int/lit8 v2, v9, -0x1

    goto/32 :goto_b

    nop

    :goto_d8
    add-int/lit8 v1, v9, 0x1

    goto/32 :goto_37

    nop

    :goto_d9
    invoke-static {v2, v3, v13}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_bb

    nop

    :goto_da
    and-int/lit8 v2, v19, 0x3f

    goto/32 :goto_bd

    nop

    :goto_db
    cmp-long v12, v4, v16

    goto/32 :goto_ae

    nop
.end method

.method partialIsValidUtf8(I[BII)I
    .locals 17

    goto/32 :goto_33

    nop

    :goto_0
    const/16 v14, -0x10

    goto/32 :goto_7b

    nop

    :goto_1
    shr-int/lit8 v9, v0, 0x8

    goto/32 :goto_7a

    nop

    :goto_2
    move-wide v4, v12

    goto/32 :goto_16

    nop

    :goto_3
    return v4

    :goto_4
    goto/32 :goto_3d

    nop

    :goto_5
    move-wide v4, v12

    goto/32 :goto_59

    nop

    :goto_6
    move/from16 v2, p3

    goto/32 :goto_30

    nop

    :goto_7
    invoke-static {v1, v4, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte([BJ)B

    move-result v4

    goto/32 :goto_38

    nop

    :goto_8
    int-to-byte v14, v14

    goto/32 :goto_46

    nop

    :goto_9
    shl-int/lit8 v15, v8, 0x1c

    goto/32 :goto_45

    nop

    :goto_a
    if-le v14, v11, :cond_0

    goto/32 :goto_17

    :cond_0
    goto/32 :goto_34

    nop

    :goto_b
    if-eqz v14, :cond_1

    goto/32 :goto_14

    :cond_1
    goto/32 :goto_6c

    nop

    :goto_c
    invoke-static {v8, v9}, Lcom/android/framework/protobuf/Utf8;->access$000(II)I

    move-result v4

    goto/32 :goto_18

    nop

    :goto_d
    add-long v15, v4, v12

    goto/32 :goto_89

    nop

    :goto_e
    invoke-static {v8, v9, v14}, Lcom/android/framework/protobuf/Utf8;->access$100(III)I

    move-result v4

    goto/32 :goto_73

    nop

    :goto_f
    invoke-static {v1, v4, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte([BJ)B

    move-result v4

    goto/32 :goto_20

    nop

    :goto_10
    if-ge v14, v15, :cond_2

    goto/32 :goto_29

    :cond_2
    :goto_11
    goto/32 :goto_66

    nop

    :goto_12
    add-int v15, v15, v16

    goto/32 :goto_83

    nop

    :goto_13
    move-wide v4, v15

    :goto_14
    goto/32 :goto_42

    nop

    :goto_15
    cmp-long v4, v15, v6

    goto/32 :goto_62

    nop

    :goto_16
    goto/16 :goto_3c

    :goto_17
    goto/32 :goto_3b

    nop

    :goto_18
    return v4

    :goto_19
    goto/32 :goto_6e

    nop

    :goto_1a
    if-gez v4, :cond_3

    goto/32 :goto_19

    :cond_3
    goto/32 :goto_c

    nop

    :goto_1b
    goto/16 :goto_3c

    :goto_1c
    goto/32 :goto_21

    nop

    :goto_1d
    const/4 v6, 0x1

    goto/32 :goto_24

    nop

    :goto_1e
    const/4 v6, 0x2

    goto/32 :goto_2c

    nop

    :goto_1f
    move-wide v4, v12

    goto/32 :goto_28

    nop

    :goto_20
    if-gt v4, v11, :cond_4

    goto/32 :goto_87

    :cond_4
    goto/32 :goto_5d

    nop

    :goto_21
    return v10

    :goto_22
    goto/32 :goto_0

    nop

    :goto_23
    const/4 v10, -0x1

    goto/32 :goto_88

    nop

    :goto_24
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    goto/32 :goto_85

    nop

    :goto_25
    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    goto/32 :goto_7e

    nop

    :goto_26
    invoke-static {v6, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_79

    nop

    :goto_27
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    goto/32 :goto_72

    nop

    :goto_28
    goto :goto_3c

    :goto_29
    goto/32 :goto_67

    nop

    :goto_2a
    aput-object v7, v5, v6

    goto/32 :goto_6f

    nop

    :goto_2b
    invoke-static {v1, v4, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte([BJ)B

    move-result v14

    goto/32 :goto_15

    nop

    :goto_2c
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    goto/32 :goto_2a

    nop

    :goto_2d
    const/4 v14, 0x0

    goto/32 :goto_3f

    nop

    :goto_2e
    move-wide v4, v12

    goto/32 :goto_5f

    nop

    :goto_2f
    if-ge v8, v9, :cond_5

    goto/32 :goto_1c

    :cond_5
    goto/32 :goto_4d

    nop

    :goto_30
    move/from16 v3, p4

    goto/32 :goto_4b

    nop

    :goto_31
    or-int/2addr v4, v5

    goto/32 :goto_47

    nop

    :goto_32
    invoke-static {v1, v4, v5, v8}, Lcom/android/framework/protobuf/Utf8$UnsafeProcessor;->partialIsValidUtf8([BJI)I

    move-result v8

    goto/32 :goto_64

    nop

    :goto_33
    move/from16 v0, p1

    goto/32 :goto_4c

    nop

    :goto_34
    add-long/2addr v12, v4

    goto/32 :goto_f

    nop

    :goto_35
    new-array v5, v5, [Ljava/lang/Object;

    goto/32 :goto_78

    nop

    :goto_36
    int-to-long v6, v3

    goto/32 :goto_4e

    nop

    :goto_37
    array-length v7, v1

    goto/32 :goto_27

    nop

    :goto_38
    if-gt v4, v11, :cond_6

    goto/32 :goto_60

    :cond_6
    goto/32 :goto_2e

    nop

    :goto_39
    add-long v15, v4, v12

    goto/32 :goto_2b

    nop

    :goto_3a
    invoke-static {v8, v14}, Lcom/android/framework/protobuf/Utf8;->access$000(II)I

    move-result v4

    goto/32 :goto_3

    nop

    :goto_3b
    return v10

    :goto_3c
    goto/32 :goto_7c

    nop

    :goto_3d
    move-wide v4, v15

    :goto_3e
    goto/32 :goto_7d

    nop

    :goto_3f
    if-eqz v9, :cond_7

    goto/32 :goto_6b

    :cond_7
    goto/32 :goto_d

    nop

    :goto_40
    const-wide/16 v12, 0x1

    goto/32 :goto_55

    nop

    :goto_41
    cmp-long v4, v15, v6

    goto/32 :goto_53

    nop

    :goto_42
    if-le v9, v11, :cond_8

    goto/32 :goto_17

    :cond_8
    goto/32 :goto_9

    nop

    :goto_43
    if-lt v14, v15, :cond_9

    goto/32 :goto_29

    :cond_9
    :goto_44
    goto/32 :goto_5b

    nop

    :goto_45
    add-int/lit8 v16, v9, 0x70

    goto/32 :goto_12

    nop

    :goto_46
    if-eqz v14, :cond_a

    goto/32 :goto_3e

    :cond_a
    goto/32 :goto_39

    nop

    :goto_47
    if-gez v4, :cond_b

    goto/32 :goto_65

    :cond_b
    goto/32 :goto_81

    nop

    :goto_48
    shr-int/lit8 v14, v0, 0x8

    goto/32 :goto_84

    nop

    :goto_49
    cmp-long v4, v15, v6

    goto/32 :goto_1a

    nop

    :goto_4a
    if-eq v8, v9, :cond_c

    goto/32 :goto_11

    :cond_c
    goto/32 :goto_10

    nop

    :goto_4b
    or-int v4, v2, v3

    goto/32 :goto_56

    nop

    :goto_4c
    move-object/from16 v1, p2

    goto/32 :goto_6

    nop

    :goto_4d
    add-long/2addr v12, v4

    goto/32 :goto_7

    nop

    :goto_4e
    if-nez v0, :cond_d

    goto/32 :goto_3c

    :cond_d
    goto/32 :goto_50

    nop

    :goto_4f
    if-gez v8, :cond_e

    goto/32 :goto_52

    :cond_e
    goto/32 :goto_51

    nop

    :goto_50
    cmp-long v8, v4, v6

    goto/32 :goto_4f

    nop

    :goto_51
    return v0

    :goto_52
    goto/32 :goto_57

    nop

    :goto_53
    if-gez v4, :cond_f

    goto/32 :goto_74

    :cond_f
    goto/32 :goto_e

    nop

    :goto_54
    const/16 v9, -0x20

    goto/32 :goto_23

    nop

    :goto_55
    if-lt v8, v9, :cond_10

    goto/32 :goto_22

    :cond_10
    goto/32 :goto_58

    nop

    :goto_56
    array-length v5, v1

    goto/32 :goto_71

    nop

    :goto_57
    int-to-byte v8, v0

    goto/32 :goto_54

    nop

    :goto_58
    const/16 v9, -0x3e

    goto/32 :goto_2f

    nop

    :goto_59
    goto/16 :goto_29

    :goto_5a
    goto/32 :goto_1f

    nop

    :goto_5b
    add-long/2addr v12, v4

    goto/32 :goto_61

    nop

    :goto_5c
    move-wide v4, v12

    goto/32 :goto_1b

    nop

    :goto_5d
    move-wide v4, v12

    goto/32 :goto_86

    nop

    :goto_5e
    invoke-static {v1, v4, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte([BJ)B

    move-result v14

    goto/32 :goto_41

    nop

    :goto_5f
    goto/16 :goto_1c

    :goto_60
    goto/32 :goto_5c

    nop

    :goto_61
    invoke-static {v1, v4, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte([BJ)B

    move-result v4

    goto/32 :goto_69

    nop

    :goto_62
    if-gez v4, :cond_11

    goto/32 :goto_4

    :cond_11
    goto/32 :goto_3a

    nop

    :goto_63
    const/16 v15, -0x60

    goto/32 :goto_4a

    nop

    :goto_64
    return v8

    :goto_65
    goto/32 :goto_25

    nop

    :goto_66
    const/16 v9, -0x13

    goto/32 :goto_70

    nop

    :goto_67
    return v10

    :goto_68
    goto/32 :goto_1

    nop

    :goto_69
    if-gt v4, v11, :cond_12

    goto/32 :goto_5a

    :cond_12
    goto/32 :goto_5

    nop

    :goto_6a
    goto :goto_80

    :goto_6b
    goto/32 :goto_82

    nop

    :goto_6c
    add-long v15, v4, v12

    goto/32 :goto_5e

    nop

    :goto_6d
    long-to-int v8, v8

    goto/32 :goto_32

    nop

    :goto_6e
    move-wide v4, v15

    goto/32 :goto_6a

    nop

    :goto_6f
    const-string v6, "Array length=%d, index=%d, limit=%d"

    goto/32 :goto_26

    nop

    :goto_70
    if-eq v8, v9, :cond_13

    goto/32 :goto_44

    :cond_13
    goto/32 :goto_43

    nop

    :goto_71
    sub-int/2addr v5, v3

    goto/32 :goto_31

    nop

    :goto_72
    aput-object v7, v5, v6

    goto/32 :goto_1d

    nop

    :goto_73
    return v4

    :goto_74
    goto/32 :goto_13

    nop

    :goto_75
    int-to-byte v9, v9

    goto/32 :goto_2d

    nop

    :goto_76
    if-eqz v15, :cond_14

    goto/32 :goto_17

    :cond_14
    goto/32 :goto_a

    nop

    :goto_77
    throw v4

    :goto_78
    const/4 v6, 0x0

    goto/32 :goto_37

    nop

    :goto_79
    invoke-direct {v4, v5}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_77

    nop

    :goto_7a
    not-int v9, v9

    goto/32 :goto_75

    nop

    :goto_7b
    if-lt v8, v14, :cond_15

    goto/32 :goto_68

    :cond_15
    goto/32 :goto_48

    nop

    :goto_7c
    sub-long v8, v6, v4

    goto/32 :goto_6d

    nop

    :goto_7d
    if-le v14, v11, :cond_16

    goto/32 :goto_29

    :cond_16
    goto/32 :goto_63

    nop

    :goto_7e
    const/4 v5, 0x3

    goto/32 :goto_35

    nop

    :goto_7f
    int-to-byte v14, v15

    :goto_80
    goto/32 :goto_b

    nop

    :goto_81
    int-to-long v4, v2

    goto/32 :goto_36

    nop

    :goto_82
    shr-int/lit8 v15, v0, 0x10

    goto/32 :goto_7f

    nop

    :goto_83
    shr-int/lit8 v15, v15, 0x1e

    goto/32 :goto_76

    nop

    :goto_84
    not-int v14, v14

    goto/32 :goto_8

    nop

    :goto_85
    aput-object v7, v5, v6

    goto/32 :goto_1e

    nop

    :goto_86
    goto/16 :goto_17

    :goto_87
    goto/32 :goto_2

    nop

    :goto_88
    const/16 v11, -0x41

    goto/32 :goto_40

    nop

    :goto_89
    invoke-static {v1, v4, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte([BJ)B

    move-result v9

    goto/32 :goto_49

    nop
.end method

.method partialIsValidUtf8Direct(ILjava/nio/ByteBuffer;II)I
    .locals 15

    goto/32 :goto_3a

    nop

    :goto_0
    invoke-static {v2, v3}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte(J)B

    move-result v2

    goto/32 :goto_14

    nop

    :goto_1
    cmp-long v2, v13, v4

    goto/32 :goto_65

    nop

    :goto_2
    return v8

    :goto_3
    goto/32 :goto_8a

    nop

    :goto_4
    if-gez v6, :cond_0

    goto/32 :goto_70

    :cond_0
    goto/32 :goto_6f

    nop

    :goto_5
    const/16 v7, -0x20

    goto/32 :goto_66

    nop

    :goto_6
    goto/16 :goto_3d

    :goto_7
    goto/32 :goto_3c

    nop

    :goto_8
    add-long v13, v2, v10

    goto/32 :goto_69

    nop

    :goto_9
    goto/16 :goto_3d

    :goto_a
    goto/32 :goto_81

    nop

    :goto_b
    invoke-static {v6, v7}, Lcom/android/framework/protobuf/Utf8;->access$000(II)I

    move-result v2

    goto/32 :goto_50

    nop

    :goto_c
    not-int v12, v12

    goto/32 :goto_6a

    nop

    :goto_d
    invoke-static {v2, v3, v6}, Lcom/android/framework/protobuf/Utf8$UnsafeProcessor;->partialIsValidUtf8(JI)I

    move-result v6

    goto/32 :goto_67

    nop

    :goto_e
    const/16 v7, -0x3e

    goto/32 :goto_3e

    nop

    :goto_f
    if-eq v6, v7, :cond_1

    goto/32 :goto_53

    :cond_1
    goto/32 :goto_52

    nop

    :goto_10
    move-wide v2, v10

    goto/32 :goto_3f

    nop

    :goto_11
    int-to-long v4, v1

    goto/32 :goto_64

    nop

    :goto_12
    aput-object v5, v3, v4

    goto/32 :goto_74

    nop

    :goto_13
    throw v2

    :goto_14
    if-gt v2, v9, :cond_2

    goto/32 :goto_40

    :cond_2
    goto/32 :goto_10

    nop

    :goto_15
    move-wide v2, v13

    :goto_16
    goto/32 :goto_37

    nop

    :goto_17
    shr-int/lit8 v12, v0, 0x8

    goto/32 :goto_c

    nop

    :goto_18
    return v2

    :goto_19
    goto/32 :goto_15

    nop

    :goto_1a
    invoke-static {v2, v3}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte(J)B

    move-result v7

    goto/32 :goto_1

    nop

    :goto_1b
    aput-object v5, v3, v4

    goto/32 :goto_7b

    nop

    :goto_1c
    move-wide v2, v10

    goto/32 :goto_21

    nop

    :goto_1d
    if-eq v6, v7, :cond_3

    goto/32 :goto_80

    :cond_3
    goto/32 :goto_7f

    nop

    :goto_1e
    sub-long v6, v4, v2

    goto/32 :goto_38

    nop

    :goto_1f
    if-le v12, v9, :cond_4

    goto/32 :goto_7

    :cond_4
    goto/32 :goto_49

    nop

    :goto_20
    invoke-virtual/range {p2 .. p2}, Ljava/nio/ByteBuffer;->limit()I

    move-result v5

    goto/32 :goto_77

    nop

    :goto_21
    goto :goto_25

    :goto_22
    goto/32 :goto_8b

    nop

    :goto_23
    new-array v3, v3, [Ljava/lang/Object;

    goto/32 :goto_73

    nop

    :goto_24
    goto :goto_3d

    :goto_25
    goto/32 :goto_2

    nop

    :goto_26
    shr-int/lit8 v13, v0, 0x10

    goto/32 :goto_2d

    nop

    :goto_27
    if-gez v2, :cond_5

    goto/32 :goto_19

    :cond_5
    goto/32 :goto_62

    nop

    :goto_28
    const/4 v3, 0x3

    goto/32 :goto_23

    nop

    :goto_29
    const-string v4, "buffer limit=%d, index=%d, limit=%d"

    goto/32 :goto_45

    nop

    :goto_2a
    const/4 v12, 0x0

    goto/32 :goto_85

    nop

    :goto_2b
    if-gez v2, :cond_6

    goto/32 :goto_68

    :cond_6
    goto/32 :goto_30

    nop

    :goto_2c
    sub-int v4, p4, v1

    goto/32 :goto_5d

    nop

    :goto_2d
    int-to-byte v12, v13

    :goto_2e
    goto/32 :goto_4d

    nop

    :goto_2f
    int-to-byte v6, v0

    goto/32 :goto_5

    nop

    :goto_30
    invoke-static/range {p2 .. p2}, Lcom/android/framework/protobuf/UnsafeUtil;->addressOffset(Ljava/nio/ByteBuffer;)J

    move-result-wide v2

    goto/32 :goto_11

    nop

    :goto_31
    shl-int/lit8 v13, v6, 0x1c

    goto/32 :goto_7d

    nop

    :goto_32
    return v2

    :goto_33
    goto/32 :goto_78

    nop

    :goto_34
    or-int v2, v1, p4

    goto/32 :goto_71

    nop

    :goto_35
    goto :goto_2e

    :goto_36
    goto/32 :goto_26

    nop

    :goto_37
    if-le v12, v9, :cond_7

    goto/32 :goto_25

    :cond_7
    goto/32 :goto_59

    nop

    :goto_38
    long-to-int v6, v6

    goto/32 :goto_d

    nop

    :goto_39
    if-lt v6, v7, :cond_8

    goto/32 :goto_82

    :cond_8
    goto/32 :goto_e

    nop

    :goto_3a
    move/from16 v0, p1

    goto/32 :goto_84

    nop

    :goto_3b
    add-long/2addr v10, v2

    goto/32 :goto_5e

    nop

    :goto_3c
    return v8

    :goto_3d
    goto/32 :goto_1e

    nop

    :goto_3e
    if-ge v6, v7, :cond_9

    goto/32 :goto_a

    :cond_9
    goto/32 :goto_83

    nop

    :goto_3f
    goto/16 :goto_a

    :goto_40
    goto/32 :goto_4c

    nop

    :goto_41
    aput-object v5, v3, v4

    goto/32 :goto_29

    nop

    :goto_42
    const-wide/16 v10, 0x1

    goto/32 :goto_39

    nop

    :goto_43
    sub-int v3, v3, p4

    goto/32 :goto_4f

    nop

    :goto_44
    shr-int/lit8 v13, v13, 0x1e

    goto/32 :goto_75

    nop

    :goto_45
    invoke-static {v4, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_76

    nop

    :goto_46
    int-to-byte v7, v7

    goto/32 :goto_2a

    nop

    :goto_47
    move-wide v2, v10

    goto/32 :goto_87

    nop

    :goto_48
    add-long v13, v2, v10

    goto/32 :goto_6e

    nop

    :goto_49
    add-long/2addr v10, v2

    goto/32 :goto_6b

    nop

    :goto_4a
    if-nez v0, :cond_a

    goto/32 :goto_3d

    :cond_a
    goto/32 :goto_5a

    nop

    :goto_4b
    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    goto/32 :goto_28

    nop

    :goto_4c
    move-wide v2, v10

    goto/32 :goto_9

    nop

    :goto_4d
    if-eqz v12, :cond_b

    goto/32 :goto_79

    :cond_b
    goto/32 :goto_48

    nop

    :goto_4e
    if-eqz v12, :cond_c

    goto/32 :goto_16

    :cond_c
    goto/32 :goto_8

    nop

    :goto_4f
    or-int/2addr v2, v3

    goto/32 :goto_2b

    nop

    :goto_50
    return v2

    :goto_51
    goto/32 :goto_89

    nop

    :goto_52
    if-ge v12, v13, :cond_d

    goto/32 :goto_25

    :cond_d
    :goto_53
    goto/32 :goto_60

    nop

    :goto_54
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto/32 :goto_1b

    nop

    :goto_55
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto/32 :goto_41

    nop

    :goto_56
    cmp-long v2, v13, v4

    goto/32 :goto_27

    nop

    :goto_57
    if-gez v2, :cond_e

    goto/32 :goto_33

    :cond_e
    goto/32 :goto_5f

    nop

    :goto_58
    if-gt v2, v9, :cond_f

    goto/32 :goto_22

    :cond_f
    goto/32 :goto_1c

    nop

    :goto_59
    const/16 v13, -0x60

    goto/32 :goto_f

    nop

    :goto_5a
    cmp-long v6, v2, v4

    goto/32 :goto_4

    nop

    :goto_5b
    const/16 v12, -0x10

    goto/32 :goto_61

    nop

    :goto_5c
    if-le v7, v9, :cond_10

    goto/32 :goto_7

    :cond_10
    goto/32 :goto_31

    nop

    :goto_5d
    int-to-long v4, v4

    goto/32 :goto_6d

    nop

    :goto_5e
    invoke-static {v2, v3}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte(J)B

    move-result v2

    goto/32 :goto_58

    nop

    :goto_5f
    invoke-static {v6, v7, v12}, Lcom/android/framework/protobuf/Utf8;->access$100(III)I

    move-result v2

    goto/32 :goto_32

    nop

    :goto_60
    const/16 v7, -0x13

    goto/32 :goto_1d

    nop

    :goto_61
    if-lt v6, v12, :cond_11

    goto/32 :goto_3

    :cond_11
    goto/32 :goto_17

    nop

    :goto_62
    invoke-static {v6, v12}, Lcom/android/framework/protobuf/Utf8;->access$000(II)I

    move-result v2

    goto/32 :goto_18

    nop

    :goto_63
    const/16 v9, -0x41

    goto/32 :goto_42

    nop

    :goto_64
    add-long/2addr v2, v4

    goto/32 :goto_2c

    nop

    :goto_65
    if-gez v2, :cond_12

    goto/32 :goto_51

    :cond_12
    goto/32 :goto_b

    nop

    :goto_66
    const/4 v8, -0x1

    goto/32 :goto_63

    nop

    :goto_67
    return v6

    :goto_68
    goto/32 :goto_4b

    nop

    :goto_69
    invoke-static {v2, v3}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte(J)B

    move-result v12

    goto/32 :goto_56

    nop

    :goto_6a
    int-to-byte v12, v12

    goto/32 :goto_4e

    nop

    :goto_6b
    invoke-static {v2, v3}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte(J)B

    move-result v2

    goto/32 :goto_7e

    nop

    :goto_6c
    add-int/2addr v13, v14

    goto/32 :goto_44

    nop

    :goto_6d
    add-long/2addr v4, v2

    goto/32 :goto_4a

    nop

    :goto_6e
    invoke-static {v2, v3}, Lcom/android/framework/protobuf/UnsafeUtil;->getByte(J)B

    move-result v12

    goto/32 :goto_72

    nop

    :goto_6f
    return v0

    :goto_70
    goto/32 :goto_2f

    nop

    :goto_71
    invoke-virtual/range {p2 .. p2}, Ljava/nio/ByteBuffer;->limit()I

    move-result v3

    goto/32 :goto_43

    nop

    :goto_72
    cmp-long v2, v13, v4

    goto/32 :goto_57

    nop

    :goto_73
    const/4 v4, 0x0

    goto/32 :goto_20

    nop

    :goto_74
    const/4 v4, 0x1

    goto/32 :goto_54

    nop

    :goto_75
    if-eqz v13, :cond_13

    goto/32 :goto_7

    :cond_13
    goto/32 :goto_1f

    nop

    :goto_76
    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_13

    nop

    :goto_77
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto/32 :goto_12

    nop

    :goto_78
    move-wide v2, v13

    :goto_79
    goto/32 :goto_5c

    nop

    :goto_7a
    not-int v7, v7

    goto/32 :goto_46

    nop

    :goto_7b
    const/4 v4, 0x2

    goto/32 :goto_55

    nop

    :goto_7c
    add-long v13, v2, v10

    goto/32 :goto_1a

    nop

    :goto_7d
    add-int/lit8 v14, v7, 0x70

    goto/32 :goto_6c

    nop

    :goto_7e
    if-gt v2, v9, :cond_14

    goto/32 :goto_88

    :cond_14
    goto/32 :goto_47

    nop

    :goto_7f
    if-lt v12, v13, :cond_15

    goto/32 :goto_25

    :cond_15
    :goto_80
    goto/32 :goto_3b

    nop

    :goto_81
    return v8

    :goto_82
    goto/32 :goto_5b

    nop

    :goto_83
    add-long/2addr v10, v2

    goto/32 :goto_0

    nop

    :goto_84
    move/from16 v1, p3

    goto/32 :goto_34

    nop

    :goto_85
    if-eqz v7, :cond_16

    goto/32 :goto_36

    :cond_16
    goto/32 :goto_7c

    nop

    :goto_86
    move-wide v2, v10

    goto/32 :goto_6

    nop

    :goto_87
    goto/16 :goto_7

    :goto_88
    goto/32 :goto_86

    nop

    :goto_89
    move-wide v2, v13

    goto/32 :goto_35

    nop

    :goto_8a
    shr-int/lit8 v7, v0, 0x8

    goto/32 :goto_7a

    nop

    :goto_8b
    move-wide v2, v10

    goto/32 :goto_24

    nop
.end method
