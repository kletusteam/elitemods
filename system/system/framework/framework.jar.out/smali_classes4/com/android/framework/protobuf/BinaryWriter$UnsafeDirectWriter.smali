.class final Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;
.super Lcom/android/framework/protobuf/BinaryWriter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/framework/protobuf/BinaryWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "UnsafeDirectWriter"
.end annotation


# instance fields
.field private buffer:Ljava/nio/ByteBuffer;

.field private bufferOffset:J

.field private limitMinusOne:J

.field private pos:J


# direct methods
.method constructor <init>(Lcom/android/framework/protobuf/BufferAllocator;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/framework/protobuf/BinaryWriter;-><init>(Lcom/android/framework/protobuf/BufferAllocator;ILcom/android/framework/protobuf/BinaryWriter$1;)V

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->nextBuffer()V

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    invoke-static {}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->isSupported()Z

    move-result v0

    return v0
.end method

.method private bufferPos()I
    .locals 4

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    iget-wide v2, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->bufferOffset:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method private bytesWrittenToCurrentBuffer()I
    .locals 4

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->limitMinusOne:J

    iget-wide v2, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method private static isSupported()Z
    .locals 1

    invoke-static {}, Lcom/android/framework/protobuf/UnsafeUtil;->hasUnsafeByteBufferOperations()Z

    move-result v0

    return v0
.end method

.method private nextBuffer()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->newDirectBuffer()Lcom/android/framework/protobuf/AllocatedBuffer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->nextBuffer(Lcom/android/framework/protobuf/AllocatedBuffer;)V

    return-void
.end method

.method private nextBuffer(I)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->newDirectBuffer(I)Lcom/android/framework/protobuf/AllocatedBuffer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->nextBuffer(Lcom/android/framework/protobuf/AllocatedBuffer;)V

    return-void
.end method

.method private nextBuffer(Lcom/android/framework/protobuf/AllocatedBuffer;)V
    .locals 5

    invoke-virtual {p1}, Lcom/android/framework/protobuf/AllocatedBuffer;->hasNioBuffer()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/android/framework/protobuf/AllocatedBuffer;->nioBuffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->isDirect()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->finishCurrentBuffer()V

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->buffers:Ljava/util/ArrayDeque;

    invoke-virtual {v1, p1}, Ljava/util/ArrayDeque;->addFirst(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    invoke-static {v1}, Lcom/android/framework/protobuf/UnsafeUtil;->addressOffset(Ljava/nio/ByteBuffer;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->bufferOffset:J

    iget-object v3, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->limit()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    int-to-long v3, v3

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->limitMinusOne:J

    iput-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    return-void

    :cond_0
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Allocator returned non-direct buffer"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Allocated buffer does not have NIO buffer"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private spaceLeft()I
    .locals 1

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->bufferPos()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private writeVarint32FiveBytes(I)V
    .locals 6

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const-wide/16 v2, 0x1

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    ushr-int/lit8 v4, p1, 0x1c

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    ushr-int/lit8 v4, p1, 0x15

    and-int/lit8 v4, v4, 0x7f

    or-int/lit16 v4, v4, 0x80

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    ushr-int/lit8 v4, p1, 0xe

    and-int/lit8 v4, v4, 0x7f

    or-int/lit16 v4, v4, 0x80

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    ushr-int/lit8 v4, p1, 0x7

    and-int/lit8 v4, v4, 0x7f

    or-int/lit16 v4, v4, 0x80

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v2, v0, v2

    iput-wide v2, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    and-int/lit8 v2, p1, 0x7f

    or-int/lit16 v2, v2, 0x80

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    return-void
.end method

.method private writeVarint32FourBytes(I)V
    .locals 6

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const-wide/16 v2, 0x1

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    ushr-int/lit8 v4, p1, 0x15

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    ushr-int/lit8 v4, p1, 0xe

    and-int/lit8 v4, v4, 0x7f

    or-int/lit16 v4, v4, 0x80

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    ushr-int/lit8 v4, p1, 0x7

    and-int/lit8 v4, v4, 0x7f

    or-int/lit16 v4, v4, 0x80

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v2, v0, v2

    iput-wide v2, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    and-int/lit8 v2, p1, 0x7f

    or-int/lit16 v2, v2, 0x80

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    return-void
.end method

.method private writeVarint32OneByte(I)V
    .locals 4

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const-wide/16 v2, 0x1

    sub-long v2, v0, v2

    iput-wide v2, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    int-to-byte v2, p1

    invoke-static {v0, v1, v2}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    return-void
.end method

.method private writeVarint32ThreeBytes(I)V
    .locals 6

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const-wide/16 v2, 0x1

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    ushr-int/lit8 v4, p1, 0xe

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    ushr-int/lit8 v4, p1, 0x7

    and-int/lit8 v4, v4, 0x7f

    or-int/lit16 v4, v4, 0x80

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v2, v0, v2

    iput-wide v2, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    and-int/lit8 v2, p1, 0x7f

    or-int/lit16 v2, v2, 0x80

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    return-void
.end method

.method private writeVarint32TwoBytes(I)V
    .locals 6

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const-wide/16 v2, 0x1

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    ushr-int/lit8 v4, p1, 0x7

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v2, v0, v2

    iput-wide v2, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    and-int/lit8 v2, p1, 0x7f

    or-int/lit16 v2, v2, 0x80

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    return-void
.end method

.method private writeVarint64EightBytes(J)V
    .locals 10

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const-wide/16 v2, 0x1

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x31

    ushr-long v4, p1, v4

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x2a

    ushr-long v4, p1, v4

    const-wide/16 v6, 0x7f

    and-long/2addr v4, v6

    const-wide/16 v8, 0x80

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x23

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x1c

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x15

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0xe

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/4 v4, 0x7

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v2, v0, v2

    iput-wide v2, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    and-long v2, p1, v6

    or-long/2addr v2, v8

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    return-void
.end method

.method private writeVarint64FiveBytes(J)V
    .locals 10

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const-wide/16 v2, 0x1

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x1c

    ushr-long v4, p1, v4

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x15

    ushr-long v4, p1, v4

    const-wide/16 v6, 0x7f

    and-long/2addr v4, v6

    const-wide/16 v8, 0x80

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0xe

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/4 v4, 0x7

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v2, v0, v2

    iput-wide v2, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    and-long v2, p1, v6

    or-long/2addr v2, v8

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    return-void
.end method

.method private writeVarint64FourBytes(J)V
    .locals 10

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const-wide/16 v2, 0x1

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x15

    ushr-long v4, p1, v4

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0xe

    ushr-long v4, p1, v4

    const-wide/16 v6, 0x7f

    and-long/2addr v4, v6

    const-wide/16 v8, 0x80

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/4 v4, 0x7

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v2, v0, v2

    iput-wide v2, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    and-long v2, p1, v6

    or-long/2addr v2, v8

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    return-void
.end method

.method private writeVarint64NineBytes(J)V
    .locals 10

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const-wide/16 v2, 0x1

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x38

    ushr-long v4, p1, v4

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x31

    ushr-long v4, p1, v4

    const-wide/16 v6, 0x7f

    and-long/2addr v4, v6

    const-wide/16 v8, 0x80

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x2a

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x23

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x1c

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x15

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0xe

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/4 v4, 0x7

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v2, v0, v2

    iput-wide v2, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    and-long v2, p1, v6

    or-long/2addr v2, v8

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    return-void
.end method

.method private writeVarint64OneByte(J)V
    .locals 4

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const-wide/16 v2, 0x1

    sub-long v2, v0, v2

    iput-wide v2, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    long-to-int v2, p1

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    return-void
.end method

.method private writeVarint64SevenBytes(J)V
    .locals 10

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const-wide/16 v2, 0x1

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x2a

    ushr-long v4, p1, v4

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x23

    ushr-long v4, p1, v4

    const-wide/16 v6, 0x7f

    and-long/2addr v4, v6

    const-wide/16 v8, 0x80

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x1c

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x15

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0xe

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/4 v4, 0x7

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v2, v0, v2

    iput-wide v2, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    and-long v2, p1, v6

    or-long/2addr v2, v8

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    return-void
.end method

.method private writeVarint64SixBytes(J)V
    .locals 10

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const-wide/16 v2, 0x1

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x23

    ushr-long v4, p1, v4

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x1c

    ushr-long v4, p1, v4

    const-wide/16 v6, 0x7f

    and-long/2addr v4, v6

    const-wide/16 v8, 0x80

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x15

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0xe

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/4 v4, 0x7

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v2, v0, v2

    iput-wide v2, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    and-long v2, p1, v6

    or-long/2addr v2, v8

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    return-void
.end method

.method private writeVarint64TenBytes(J)V
    .locals 10

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const-wide/16 v2, 0x1

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x3f

    ushr-long v4, p1, v4

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x38

    ushr-long v4, p1, v4

    const-wide/16 v6, 0x7f

    and-long/2addr v4, v6

    const-wide/16 v8, 0x80

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x31

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x2a

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x23

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x1c

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0x15

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/16 v4, 0xe

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/4 v4, 0x7

    ushr-long v4, p1, v4

    and-long/2addr v4, v6

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v2, v0, v2

    iput-wide v2, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    and-long v2, p1, v6

    or-long/2addr v2, v8

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    return-void
.end method

.method private writeVarint64ThreeBytes(J)V
    .locals 10

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const-wide/16 v2, 0x1

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    long-to-int v4, p1

    ushr-int/lit8 v4, v4, 0xe

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/4 v4, 0x7

    ushr-long v4, p1, v4

    const-wide/16 v6, 0x7f

    and-long/2addr v4, v6

    const-wide/16 v8, 0x80

    or-long/2addr v4, v8

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v2, v0, v2

    iput-wide v2, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    and-long v2, p1, v6

    or-long/2addr v2, v8

    long-to-int v2, v2

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    return-void
.end method

.method private writeVarint64TwoBytes(J)V
    .locals 6

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const-wide/16 v2, 0x1

    sub-long v4, v0, v2

    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const/4 v4, 0x7

    ushr-long v4, p1, v4

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    sub-long v2, v0, v2

    iput-wide v2, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    long-to-int v2, p1

    and-int/lit8 v2, v2, 0x7f

    or-int/lit16 v2, v2, 0x80

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    return-void
.end method


# virtual methods
.method finishCurrentBuffer()V
    .locals 2

    goto/32 :goto_4

    nop

    :goto_0
    const-wide/16 v0, 0x0

    goto/32 :goto_8

    nop

    :goto_1
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    goto/32 :goto_e

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    goto/32 :goto_3

    nop

    :goto_3
    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->bufferPos()I

    move-result v1

    goto/32 :goto_7

    nop

    :goto_4
    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    goto/32 :goto_5

    nop

    :goto_5
    if-nez v0, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_f

    nop

    :goto_6
    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->bytesWrittenToCurrentBuffer()I

    move-result v1

    goto/32 :goto_a

    nop

    :goto_7
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_1

    nop

    :goto_8
    iput-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_b

    nop

    :goto_9
    iput-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    goto/32 :goto_0

    nop

    :goto_a
    add-int/2addr v0, v1

    goto/32 :goto_10

    nop

    :goto_b
    iput-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->limitMinusOne:J

    :goto_c
    goto/32 :goto_d

    nop

    :goto_d
    return-void

    :goto_e
    const/4 v0, 0x0

    goto/32 :goto_9

    nop

    :goto_f
    iget v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->totalDoneBytes:I

    goto/32 :goto_6

    nop

    :goto_10
    iput v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->totalDoneBytes:I

    goto/32 :goto_2

    nop
.end method

.method public getTotalBytesWritten()I
    .locals 2

    iget v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->totalDoneBytes:I

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->bytesWrittenToCurrentBuffer()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method requireSpace(I)V
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    invoke-direct {p0, p1}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->nextBuffer(I)V

    :goto_1
    goto/32 :goto_2

    nop

    :goto_2
    return-void

    :goto_3
    if-lt v0, p1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_0

    nop

    :goto_4
    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->spaceLeft()I

    move-result v0

    goto/32 :goto_3

    nop
.end method

.method public write(B)V
    .locals 4

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    const-wide/16 v2, 0x1

    sub-long v2, v0, v2

    iput-wide v2, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    invoke-static {v0, v1, p1}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    return-void
.end method

.method public write(Ljava/nio/ByteBuffer;)V
    .locals 5

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->spaceLeft()I

    move-result v1

    if-ge v1, v0, :cond_0

    invoke-direct {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->nextBuffer(I)V

    :cond_0
    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    int-to-long v3, v0

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->bufferPos()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, p1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    return-void
.end method

.method public write([BII)V
    .locals 4

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->spaceLeft()I

    move-result v0

    if-ge v0, p3, :cond_0

    invoke-direct {p0, p3}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->nextBuffer(I)V

    :cond_0
    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    int-to-long v2, p3

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->bufferPos()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1, p2, p3}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    return-void
.end method

.method public writeBool(IZ)V
    .locals 1

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->requireSpace(I)V

    int-to-byte v0, p2

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->write(B)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method writeBool(Z)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->write(B)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    int-to-byte v0, p1

    goto/32 :goto_0

    nop
.end method

.method public writeBytes(ILcom/android/framework/protobuf/ByteString;)V
    .locals 2

    :try_start_0
    invoke-virtual {p2, p0}, Lcom/android/framework/protobuf/ByteString;->writeToReverse(Lcom/android/framework/protobuf/ByteOutput;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    nop

    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->requireSpace(I)V

    invoke-virtual {p2}, Lcom/android/framework/protobuf/ByteString;->size()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeVarint32(I)V

    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeTag(II)V

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public writeEndGroup(I)V
    .locals 1

    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method writeFixed32(I)V
    .locals 6

    goto/32 :goto_5

    nop

    :goto_0
    and-int/lit16 v4, v4, 0xff

    goto/32 :goto_17

    nop

    :goto_1
    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_14

    nop

    :goto_2
    shr-int/lit8 v4, p1, 0x10

    goto/32 :goto_b

    nop

    :goto_3
    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_16

    nop

    :goto_4
    and-int/lit16 v2, p1, 0xff

    goto/32 :goto_1b

    nop

    :goto_5
    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_e

    nop

    :goto_6
    invoke-static {v0, v1, v2}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_f

    nop

    :goto_7
    and-int/lit16 v4, v4, 0xff

    goto/32 :goto_a

    nop

    :goto_8
    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_11

    nop

    :goto_9
    sub-long v4, v0, v2

    goto/32 :goto_12

    nop

    :goto_a
    int-to-byte v4, v4

    goto/32 :goto_8

    nop

    :goto_b
    and-int/lit16 v4, v4, 0xff

    goto/32 :goto_d

    nop

    :goto_c
    shr-int/lit8 v4, p1, 0x18

    goto/32 :goto_0

    nop

    :goto_d
    int-to-byte v4, v4

    goto/32 :goto_1

    nop

    :goto_e
    const-wide/16 v2, 0x1

    goto/32 :goto_15

    nop

    :goto_f
    return-void

    :goto_10
    sub-long v4, v0, v2

    goto/32 :goto_19

    nop

    :goto_11
    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_18

    nop

    :goto_12
    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_1c

    nop

    :goto_13
    iput-wide v2, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_4

    nop

    :goto_14
    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_9

    nop

    :goto_15
    sub-long v4, v0, v2

    goto/32 :goto_1a

    nop

    :goto_16
    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_10

    nop

    :goto_17
    int-to-byte v4, v4

    goto/32 :goto_3

    nop

    :goto_18
    sub-long v2, v0, v2

    goto/32 :goto_13

    nop

    :goto_19
    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_2

    nop

    :goto_1a
    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_c

    nop

    :goto_1b
    int-to-byte v2, v2

    goto/32 :goto_6

    nop

    :goto_1c
    shr-int/lit8 v4, p1, 0x8

    goto/32 :goto_7

    nop
.end method

.method public writeFixed32(II)V
    .locals 1

    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->requireSpace(I)V

    invoke-virtual {p0, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeFixed32(I)V

    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method public writeFixed64(IJ)V
    .locals 1

    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->requireSpace(I)V

    invoke-virtual {p0, p2, p3}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeFixed64(J)V

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method writeFixed64(J)V
    .locals 6

    goto/32 :goto_f

    nop

    :goto_0
    long-to-int v4, v4

    goto/32 :goto_14

    nop

    :goto_1
    const/16 v4, 0x20

    goto/32 :goto_21

    nop

    :goto_2
    sub-long v4, v0, v2

    goto/32 :goto_27

    nop

    :goto_3
    return-void

    :goto_4
    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_31

    nop

    :goto_5
    and-int/lit16 v4, v4, 0xff

    goto/32 :goto_2b

    nop

    :goto_6
    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_2f

    nop

    :goto_7
    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_39

    nop

    :goto_8
    int-to-byte v4, v4

    goto/32 :goto_c

    nop

    :goto_9
    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_36

    nop

    :goto_a
    and-int/lit16 v4, v4, 0xff

    goto/32 :goto_32

    nop

    :goto_b
    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_2c

    nop

    :goto_c
    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_1d

    nop

    :goto_d
    const/16 v4, 0x8

    goto/32 :goto_45

    nop

    :goto_e
    long-to-int v4, v4

    goto/32 :goto_a

    nop

    :goto_f
    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_3f

    nop

    :goto_10
    and-int/lit16 v4, v4, 0xff

    goto/32 :goto_37

    nop

    :goto_11
    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_1e

    nop

    :goto_12
    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_24

    nop

    :goto_13
    int-to-byte v2, v2

    goto/32 :goto_34

    nop

    :goto_14
    and-int/lit16 v4, v4, 0xff

    goto/32 :goto_43

    nop

    :goto_15
    sub-long v4, v0, v2

    goto/32 :goto_12

    nop

    :goto_16
    long-to-int v4, v4

    goto/32 :goto_3a

    nop

    :goto_17
    iput-wide v2, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_2e

    nop

    :goto_18
    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_3c

    nop

    :goto_19
    sub-long v4, v0, v2

    goto/32 :goto_11

    nop

    :goto_1a
    and-int/lit16 v4, v4, 0xff

    goto/32 :goto_8

    nop

    :goto_1b
    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_38

    nop

    :goto_1c
    long-to-int v4, v4

    goto/32 :goto_5

    nop

    :goto_1d
    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_2

    nop

    :goto_1e
    const/16 v4, 0x30

    goto/32 :goto_3e

    nop

    :goto_1f
    sub-long v2, v0, v2

    goto/32 :goto_17

    nop

    :goto_20
    shr-long v4, p1, v4

    goto/32 :goto_16

    nop

    :goto_21
    shr-long v4, p1, v4

    goto/32 :goto_42

    nop

    :goto_22
    shr-long v4, p1, v4

    goto/32 :goto_0

    nop

    :goto_23
    and-int/lit16 v4, v4, 0xff

    goto/32 :goto_29

    nop

    :goto_24
    const/16 v4, 0x38

    goto/32 :goto_22

    nop

    :goto_25
    shr-long v4, p1, v4

    goto/32 :goto_e

    nop

    :goto_26
    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_b

    nop

    :goto_27
    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_d

    nop

    :goto_28
    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_1

    nop

    :goto_29
    int-to-byte v4, v4

    goto/32 :goto_26

    nop

    :goto_2a
    const/16 v4, 0x28

    goto/32 :goto_25

    nop

    :goto_2b
    int-to-byte v4, v4

    goto/32 :goto_9

    nop

    :goto_2c
    sub-long v4, v0, v2

    goto/32 :goto_18

    nop

    :goto_2d
    int-to-byte v4, v4

    goto/32 :goto_46

    nop

    :goto_2e
    long-to-int v2, p1

    goto/32 :goto_3d

    nop

    :goto_2f
    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_19

    nop

    :goto_30
    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_33

    nop

    :goto_31
    const/16 v4, 0x10

    goto/32 :goto_41

    nop

    :goto_32
    int-to-byte v4, v4

    goto/32 :goto_44

    nop

    :goto_33
    sub-long v4, v0, v2

    goto/32 :goto_4

    nop

    :goto_34
    invoke-static {v0, v1, v2}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_3

    nop

    :goto_35
    long-to-int v4, v4

    goto/32 :goto_1a

    nop

    :goto_36
    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_47

    nop

    :goto_37
    int-to-byte v4, v4

    goto/32 :goto_7

    nop

    :goto_38
    sub-long v4, v0, v2

    goto/32 :goto_28

    nop

    :goto_39
    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_1f

    nop

    :goto_3a
    and-int/lit16 v4, v4, 0xff

    goto/32 :goto_2d

    nop

    :goto_3b
    long-to-int v4, v4

    goto/32 :goto_10

    nop

    :goto_3c
    const/16 v4, 0x18

    goto/32 :goto_20

    nop

    :goto_3d
    and-int/lit16 v2, v2, 0xff

    goto/32 :goto_13

    nop

    :goto_3e
    shr-long v4, p1, v4

    goto/32 :goto_1c

    nop

    :goto_3f
    const-wide/16 v2, 0x1

    goto/32 :goto_15

    nop

    :goto_40
    iput-wide v4, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_2a

    nop

    :goto_41
    shr-long v4, p1, v4

    goto/32 :goto_35

    nop

    :goto_42
    long-to-int v4, v4

    goto/32 :goto_23

    nop

    :goto_43
    int-to-byte v4, v4

    goto/32 :goto_6

    nop

    :goto_44
    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_1b

    nop

    :goto_45
    shr-long v4, p1, v4

    goto/32 :goto_3b

    nop

    :goto_46
    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_30

    nop

    :goto_47
    sub-long v4, v0, v2

    goto/32 :goto_40

    nop
.end method

.method public writeGroup(ILjava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeTag(II)V

    invoke-static {}, Lcom/android/framework/protobuf/Protobuf;->getInstance()Lcom/android/framework/protobuf/Protobuf;

    move-result-object v0

    invoke-virtual {v0, p2, p0}, Lcom/android/framework/protobuf/Protobuf;->writeTo(Ljava/lang/Object;Lcom/android/framework/protobuf/Writer;)V

    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method public writeGroup(ILjava/lang/Object;Lcom/android/framework/protobuf/Schema;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeTag(II)V

    invoke-interface {p3, p2, p0}, Lcom/android/framework/protobuf/Schema;->writeTo(Ljava/lang/Object;Lcom/android/framework/protobuf/Writer;)V

    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method writeInt32(I)V
    .locals 2

    goto/32 :goto_4

    nop

    :goto_0
    int-to-long v0, p1

    goto/32 :goto_5

    nop

    :goto_1
    invoke-virtual {p0, p1}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeVarint32(I)V

    goto/32 :goto_2

    nop

    :goto_2
    goto :goto_6

    :goto_3
    goto/32 :goto_0

    nop

    :goto_4
    if-gez p1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_1

    nop

    :goto_5
    invoke-virtual {p0, v0, v1}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeVarint64(J)V

    :goto_6
    goto/32 :goto_7

    nop

    :goto_7
    return-void
.end method

.method public writeInt32(II)V
    .locals 1

    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->requireSpace(I)V

    invoke-virtual {p0, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeInt32(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method public writeLazy(Ljava/nio/ByteBuffer;)V
    .locals 5

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->spaceLeft()I

    move-result v1

    if-ge v1, v0, :cond_0

    iget v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->totalDoneBytes:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->totalDoneBytes:I

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->buffers:Ljava/util/ArrayDeque;

    invoke-static {p1}, Lcom/android/framework/protobuf/AllocatedBuffer;->wrap(Ljava/nio/ByteBuffer;)Lcom/android/framework/protobuf/AllocatedBuffer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayDeque;->addFirst(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->nextBuffer()V

    return-void

    :cond_0
    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    int-to-long v3, v0

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->bufferPos()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, p1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    return-void
.end method

.method public writeLazy([BII)V
    .locals 4

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->spaceLeft()I

    move-result v0

    if-ge v0, p3, :cond_0

    iget v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->totalDoneBytes:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->totalDoneBytes:I

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->buffers:Ljava/util/ArrayDeque;

    invoke-static {p1, p2, p3}, Lcom/android/framework/protobuf/AllocatedBuffer;->wrap([BII)Lcom/android/framework/protobuf/AllocatedBuffer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayDeque;->addFirst(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->nextBuffer()V

    return-void

    :cond_0
    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    int-to-long v2, p3

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->bufferPos()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1, p2, p3}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    return-void
.end method

.method public writeMessage(ILjava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->getTotalBytesWritten()I

    move-result v0

    invoke-static {}, Lcom/android/framework/protobuf/Protobuf;->getInstance()Lcom/android/framework/protobuf/Protobuf;

    move-result-object v1

    invoke-virtual {v1, p2, p0}, Lcom/android/framework/protobuf/Protobuf;->writeTo(Ljava/lang/Object;Lcom/android/framework/protobuf/Writer;)V

    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->getTotalBytesWritten()I

    move-result v1

    sub-int/2addr v1, v0

    const/16 v2, 0xa

    invoke-virtual {p0, v2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->requireSpace(I)V

    invoke-virtual {p0, v1}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeVarint32(I)V

    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method public writeMessage(ILjava/lang/Object;Lcom/android/framework/protobuf/Schema;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->getTotalBytesWritten()I

    move-result v0

    invoke-interface {p3, p2, p0}, Lcom/android/framework/protobuf/Schema;->writeTo(Ljava/lang/Object;Lcom/android/framework/protobuf/Writer;)V

    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->getTotalBytesWritten()I

    move-result v1

    sub-int/2addr v1, v0

    const/16 v2, 0xa

    invoke-virtual {p0, v2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->requireSpace(I)V

    invoke-virtual {p0, v1}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeVarint32(I)V

    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method writeSInt32(I)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeVarint32(I)V

    goto/32 :goto_0

    nop

    :goto_2
    invoke-static {p1}, Lcom/android/framework/protobuf/CodedOutputStream;->encodeZigZag32(I)I

    move-result v0

    goto/32 :goto_1

    nop
.end method

.method public writeSInt32(II)V
    .locals 1

    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->requireSpace(I)V

    invoke-virtual {p0, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeSInt32(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method public writeSInt64(IJ)V
    .locals 1

    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->requireSpace(I)V

    invoke-virtual {p0, p2, p3}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeSInt64(J)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method writeSInt64(J)V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    invoke-static {p1, p2}, Lcom/android/framework/protobuf/CodedOutputStream;->encodeZigZag64(J)J

    move-result-wide v0

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    invoke-virtual {p0, v0, v1}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeVarint64(J)V

    goto/32 :goto_1

    nop
.end method

.method public writeStartGroup(I)V
    .locals 1

    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method public writeString(ILjava/lang/String;)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->getTotalBytesWritten()I

    move-result v0

    invoke-virtual {p0, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeString(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->getTotalBytesWritten()I

    move-result v1

    sub-int/2addr v1, v0

    const/16 v2, 0xa

    invoke-virtual {p0, v2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->requireSpace(I)V

    invoke-virtual {p0, v1}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeVarint32(I)V

    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method writeString(Ljava/lang/String;)V
    .locals 12

    goto/32 :goto_22

    nop

    :goto_0
    int-to-byte v8, v8

    goto/32 :goto_26

    nop

    :goto_1
    goto/16 :goto_5f

    :goto_2
    goto/32 :goto_62

    nop

    :goto_3
    sub-long v8, v6, v2

    goto/32 :goto_76

    nop

    :goto_4
    ushr-int/lit8 v8, v5, 0x6

    goto/32 :goto_80

    nop

    :goto_5
    and-int/lit8 v8, v5, 0x3f

    goto/32 :goto_4e

    nop

    :goto_6
    iput-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_9

    nop

    :goto_7
    iput-wide v8, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_65

    nop

    :goto_8
    iget-wide v6, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_83

    nop

    :goto_9
    int-to-byte v1, v5

    goto/32 :goto_2f

    nop

    :goto_a
    iput-wide v10, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_2a

    nop

    :goto_b
    if-gtz v8, :cond_0

    goto/32 :goto_3b

    :cond_0
    goto/32 :goto_77

    nop

    :goto_c
    cmp-long v8, v6, v8

    goto/32 :goto_b

    nop

    :goto_d
    if-nez v0, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_5b

    nop

    :goto_e
    or-int/lit16 v10, v10, 0xf0

    goto/32 :goto_60

    nop

    :goto_f
    iget-wide v6, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_14

    nop

    :goto_10
    iget-wide v6, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_3

    nop

    :goto_11
    or-int/2addr v10, v1

    goto/32 :goto_85

    nop

    :goto_12
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    goto/32 :goto_7b

    nop

    :goto_13
    invoke-static {v6, v5}, Ljava/lang/Character;->isSurrogatePair(CC)Z

    move-result v6

    goto/32 :goto_87

    nop

    :goto_14
    iget-wide v8, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->bufferOffset:J

    goto/32 :goto_2c

    nop

    :goto_15
    if-gtz v6, :cond_2

    goto/32 :goto_1a

    :cond_2
    goto/32 :goto_d

    nop

    :goto_16
    add-long/2addr v8, v2

    goto/32 :goto_c

    nop

    :goto_17
    if-ge v5, v6, :cond_3

    goto/32 :goto_59

    :cond_3
    goto/32 :goto_6f

    nop

    :goto_18
    or-int/2addr v10, v1

    goto/32 :goto_7d

    nop

    :goto_19
    throw v1

    :goto_1a
    goto/32 :goto_52

    nop

    :goto_1b
    iget-wide v8, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_84

    nop

    :goto_1c
    sub-long v10, v8, v2

    goto/32 :goto_55

    nop

    :goto_1d
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    goto/32 :goto_8c

    nop

    :goto_1e
    ushr-int/lit8 v10, v6, 0x12

    goto/32 :goto_e

    nop

    :goto_1f
    int-to-byte v8, v8

    goto/32 :goto_8a

    nop

    :goto_20
    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->requireSpace(I)V

    goto/32 :goto_12

    nop

    :goto_21
    iget-wide v8, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_1c

    nop

    :goto_22
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    goto/32 :goto_20

    nop

    :goto_23
    const/16 v1, 0x80

    goto/32 :goto_90

    nop

    :goto_24
    and-int/lit8 v10, v10, 0x3f

    goto/32 :goto_18

    nop

    :goto_25
    iput-wide v10, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_8b

    nop

    :goto_26
    invoke-static {v6, v7, v8}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_81

    nop

    :goto_27
    ushr-int/lit8 v8, v5, 0x6

    goto/32 :goto_35

    nop

    :goto_28
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_29

    nop

    :goto_29
    invoke-static {v7, v5}, Ljava/lang/Character;->toCodePoint(CC)I

    move-result v6

    goto/32 :goto_21

    nop

    :goto_2a
    ushr-int/lit8 v10, v6, 0xc

    goto/32 :goto_49

    nop

    :goto_2b
    invoke-static {v6, v7, v8}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_5c

    nop

    :goto_2c
    cmp-long v8, v6, v8

    goto/32 :goto_69

    nop

    :goto_2d
    iput-wide v10, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_1e

    nop

    :goto_2e
    iget-wide v8, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->bufferOffset:J

    goto/32 :goto_4f

    nop

    :goto_2f
    invoke-static {v6, v7, v1}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_61

    nop

    :goto_30
    invoke-static {v8, v9, v10}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_1

    nop

    :goto_31
    goto/16 :goto_64

    :goto_32
    goto/32 :goto_78

    nop

    :goto_33
    iget-wide v6, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_2e

    nop

    :goto_34
    add-int/lit8 v2, v0, -0x1

    goto/32 :goto_8f

    nop

    :goto_35
    or-int/lit16 v8, v8, 0x3c0

    goto/32 :goto_4a

    nop

    :goto_36
    int-to-byte v8, v8

    goto/32 :goto_67

    nop

    :goto_37
    iput-wide v8, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_82

    nop

    :goto_38
    sub-long v1, v6, v2

    goto/32 :goto_6

    nop

    :goto_39
    const/4 v4, -0x1

    goto/32 :goto_57

    nop

    :goto_3a
    goto/16 :goto_5f

    :goto_3b
    goto/32 :goto_74

    nop

    :goto_3c
    and-int/lit8 v10, v6, 0x3f

    goto/32 :goto_11

    nop

    :goto_3d
    if-lt v4, v1, :cond_4

    goto/32 :goto_71

    :cond_4
    goto/32 :goto_8e

    nop

    :goto_3e
    and-int/lit8 v8, v5, 0x3f

    goto/32 :goto_8d

    nop

    :goto_3f
    const/16 v6, 0x800

    goto/32 :goto_93

    nop

    :goto_40
    add-long/2addr v8, v10

    goto/32 :goto_72

    nop

    :goto_41
    move v7, v6

    goto/32 :goto_13

    nop

    :goto_42
    const-wide/16 v10, 0x2

    goto/32 :goto_40

    nop

    :goto_43
    int-to-byte v8, v8

    goto/32 :goto_54

    nop

    :goto_44
    iput-wide v8, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_5

    nop

    :goto_45
    or-int/2addr v8, v1

    goto/32 :goto_0

    nop

    :goto_46
    sub-long v8, v6, v2

    goto/32 :goto_7

    nop

    :goto_47
    if-gez v0, :cond_5

    goto/32 :goto_71

    :cond_5
    goto/32 :goto_1d

    nop

    :goto_48
    sub-long v10, v8, v2

    goto/32 :goto_25

    nop

    :goto_49
    and-int/lit8 v10, v10, 0x3f

    goto/32 :goto_91

    nop

    :goto_4a
    int-to-byte v8, v8

    goto/32 :goto_53

    nop

    :goto_4b
    iput-wide v8, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_3e

    nop

    :goto_4c
    iput-wide v8, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_27

    nop

    :goto_4d
    add-int/2addr v0, v4

    goto/32 :goto_31

    nop

    :goto_4e
    or-int/2addr v8, v1

    goto/32 :goto_36

    nop

    :goto_4f
    cmp-long v8, v6, v8

    goto/32 :goto_6c

    nop

    :goto_50
    sub-long v10, v8, v2

    goto/32 :goto_a

    nop

    :goto_51
    invoke-static {v8, v9, v10}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_5a

    nop

    :goto_52
    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->requireSpace(I)V

    goto/32 :goto_5e

    nop

    :goto_53
    invoke-static {v6, v7, v8}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_6a

    nop

    :goto_54
    invoke-static {v6, v7, v8}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_3a

    nop

    :goto_55
    iput-wide v10, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_3c

    nop

    :goto_56
    iget-wide v8, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->bufferOffset:J

    goto/32 :goto_42

    nop

    :goto_57
    if-eq v0, v4, :cond_6

    goto/32 :goto_64

    :cond_6
    goto/32 :goto_63

    nop

    :goto_58
    if-lt v6, v5, :cond_7

    goto/32 :goto_3b

    :cond_7
    :goto_59
    goto/32 :goto_8

    nop

    :goto_5a
    iget-wide v8, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_48

    nop

    :goto_5b
    add-int/lit8 v6, v0, -0x1

    goto/32 :goto_66

    nop

    :goto_5c
    goto/16 :goto_5f

    :goto_5d
    goto/32 :goto_3f

    nop

    :goto_5e
    add-int/lit8 v0, v0, 0x1

    :goto_5f
    goto/32 :goto_4d

    nop

    :goto_60
    int-to-byte v10, v10

    goto/32 :goto_30

    nop

    :goto_61
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_70

    nop

    :goto_62
    new-instance v1, Lcom/android/framework/protobuf/Utf8$UnpairedSurrogateException;

    goto/32 :goto_34

    nop

    :goto_63
    return-void

    :goto_64
    goto/32 :goto_88

    nop

    :goto_65
    int-to-byte v8, v5

    goto/32 :goto_2b

    nop

    :goto_66
    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    goto/32 :goto_41

    nop

    :goto_67
    invoke-static {v6, v7, v8}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_10

    nop

    :goto_68
    sub-long v8, v6, v2

    goto/32 :goto_4b

    nop

    :goto_69
    if-gez v8, :cond_8

    goto/32 :goto_5d

    :cond_8
    goto/32 :goto_46

    nop

    :goto_6a
    goto/16 :goto_5f

    :goto_6b
    goto/32 :goto_79

    nop

    :goto_6c
    if-gtz v8, :cond_9

    goto/32 :goto_6b

    :cond_9
    goto/32 :goto_68

    nop

    :goto_6d
    int-to-byte v10, v10

    goto/32 :goto_73

    nop

    :goto_6e
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    goto/32 :goto_86

    nop

    :goto_6f
    const v6, 0xdfff

    goto/32 :goto_58

    nop

    :goto_70
    goto :goto_7c

    :goto_71
    goto/32 :goto_39

    nop

    :goto_72
    cmp-long v6, v6, v8

    goto/32 :goto_15

    nop

    :goto_73
    invoke-static {v8, v9, v10}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_1b

    nop

    :goto_74
    iget-wide v6, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_56

    nop

    :goto_75
    invoke-static {v8, v9, v10}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_7a

    nop

    :goto_76
    iput-wide v8, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_4

    nop

    :goto_77
    sub-long v8, v6, v2

    goto/32 :goto_44

    nop

    :goto_78
    return-void

    :goto_79
    const v6, 0xd800

    goto/32 :goto_17

    nop

    :goto_7a
    iget-wide v8, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_50

    nop

    :goto_7b
    add-int/lit8 v0, v0, -0x1

    :goto_7c
    goto/32 :goto_23

    nop

    :goto_7d
    int-to-byte v10, v10

    goto/32 :goto_75

    nop

    :goto_7e
    iget-wide v6, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_92

    nop

    :goto_7f
    or-int/lit16 v8, v8, 0x1e0

    goto/32 :goto_43

    nop

    :goto_80
    and-int/lit8 v8, v8, 0x3f

    goto/32 :goto_45

    nop

    :goto_81
    iget-wide v6, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_89

    nop

    :goto_82
    ushr-int/lit8 v8, v5, 0xc

    goto/32 :goto_7f

    nop

    :goto_83
    iget-wide v8, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->bufferOffset:J

    goto/32 :goto_16

    nop

    :goto_84
    sub-long v10, v8, v2

    goto/32 :goto_2d

    nop

    :goto_85
    int-to-byte v10, v10

    goto/32 :goto_51

    nop

    :goto_86
    if-lt v5, v1, :cond_a

    goto/32 :goto_5d

    :cond_a
    goto/32 :goto_f

    nop

    :goto_87
    if-nez v6, :cond_b

    goto/32 :goto_2

    :cond_b
    goto/32 :goto_28

    nop

    :goto_88
    if-gez v0, :cond_c

    goto/32 :goto_32

    :cond_c
    goto/32 :goto_6e

    nop

    :goto_89
    sub-long v8, v6, v2

    goto/32 :goto_37

    nop

    :goto_8a
    invoke-static {v6, v7, v8}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte(JB)V

    goto/32 :goto_7e

    nop

    :goto_8b
    ushr-int/lit8 v10, v6, 0x6

    goto/32 :goto_24

    nop

    :goto_8c
    move v5, v4

    goto/32 :goto_3d

    nop

    :goto_8d
    or-int/2addr v8, v1

    goto/32 :goto_1f

    nop

    :goto_8e
    iget-wide v6, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->pos:J

    goto/32 :goto_38

    nop

    :goto_8f
    invoke-direct {v1, v2, v0}, Lcom/android/framework/protobuf/Utf8$UnpairedSurrogateException;-><init>(II)V

    goto/32 :goto_19

    nop

    :goto_90
    const-wide/16 v2, 0x1

    goto/32 :goto_47

    nop

    :goto_91
    or-int/2addr v10, v1

    goto/32 :goto_6d

    nop

    :goto_92
    sub-long v8, v6, v2

    goto/32 :goto_4c

    nop

    :goto_93
    if-lt v5, v6, :cond_d

    goto/32 :goto_6b

    :cond_d
    goto/32 :goto_33

    nop
.end method

.method writeTag(II)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeVarint32(I)V

    goto/32 :goto_2

    nop

    :goto_1
    invoke-static {p1, p2}, Lcom/android/framework/protobuf/WireFormat;->makeTag(II)I

    move-result v0

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method public writeUInt32(II)V
    .locals 1

    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->requireSpace(I)V

    invoke-virtual {p0, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeVarint32(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method public writeUInt64(IJ)V
    .locals 1

    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->requireSpace(I)V

    invoke-virtual {p0, p2, p3}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeVarint64(J)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeTag(II)V

    return-void
.end method

.method writeVarint32(I)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    if-eqz v0, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_15

    nop

    :goto_1
    and-int/lit8 v0, p1, -0x80

    goto/32 :goto_5

    nop

    :goto_2
    if-eqz v0, :cond_1

    goto/32 :goto_13

    :cond_1
    goto/32 :goto_d

    nop

    :goto_3
    goto :goto_f

    :goto_4
    goto/32 :goto_e

    nop

    :goto_5
    if-eqz v0, :cond_2

    goto/32 :goto_c

    :cond_2
    goto/32 :goto_16

    nop

    :goto_6
    and-int/2addr v0, p1

    goto/32 :goto_11

    nop

    :goto_7
    goto :goto_f

    :goto_8
    goto/32 :goto_10

    nop

    :goto_9
    return-void

    :goto_a
    invoke-direct {p0, p1}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeVarint32FourBytes(I)V

    goto/32 :goto_3

    nop

    :goto_b
    goto :goto_f

    :goto_c
    goto/32 :goto_17

    nop

    :goto_d
    invoke-direct {p0, p1}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeVarint32TwoBytes(I)V

    goto/32 :goto_12

    nop

    :goto_e
    invoke-direct {p0, p1}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeVarint32FiveBytes(I)V

    :goto_f
    goto/32 :goto_9

    nop

    :goto_10
    const/high16 v0, -0x10000000

    goto/32 :goto_6

    nop

    :goto_11
    if-eqz v0, :cond_3

    goto/32 :goto_4

    :cond_3
    goto/32 :goto_a

    nop

    :goto_12
    goto :goto_f

    :goto_13
    goto/32 :goto_18

    nop

    :goto_14
    and-int/2addr v0, p1

    goto/32 :goto_0

    nop

    :goto_15
    invoke-direct {p0, p1}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeVarint32ThreeBytes(I)V

    goto/32 :goto_7

    nop

    :goto_16
    invoke-direct {p0, p1}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeVarint32OneByte(I)V

    goto/32 :goto_b

    nop

    :goto_17
    and-int/lit16 v0, p1, -0x4000

    goto/32 :goto_2

    nop

    :goto_18
    const/high16 v0, -0x200000

    goto/32 :goto_14

    nop
.end method

.method writeVarint64(J)V
    .locals 1

    goto/32 :goto_14

    nop

    :goto_0
    goto :goto_4

    :pswitch_0
    goto/32 :goto_12

    nop

    :goto_1
    goto :goto_4

    :pswitch_1
    goto/32 :goto_b

    nop

    :goto_2
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeVarint64NineBytes(J)V

    goto/32 :goto_13

    nop

    :goto_3
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeVarint64OneByte(J)V

    nop

    :goto_4
    goto/32 :goto_10

    nop

    :goto_5
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeVarint64SevenBytes(J)V

    goto/32 :goto_0

    nop

    :goto_6
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeVarint64TwoBytes(J)V

    goto/32 :goto_e

    nop

    :goto_7
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeVarint64EightBytes(J)V

    goto/32 :goto_11

    nop

    :goto_8
    goto :goto_4

    :pswitch_2
    goto/32 :goto_f

    nop

    :goto_9
    goto :goto_4

    :pswitch_3
    goto/32 :goto_a

    nop

    :goto_a
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeVarint64FiveBytes(J)V

    goto/32 :goto_15

    nop

    :goto_b
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeVarint64ThreeBytes(J)V

    goto/32 :goto_d

    nop

    :goto_c
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeVarint64FourBytes(J)V

    goto/32 :goto_1

    nop

    :goto_d
    goto :goto_4

    :pswitch_4
    goto/32 :goto_6

    nop

    :goto_e
    goto :goto_4

    :pswitch_5
    goto/32 :goto_3

    nop

    :goto_f
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeVarint64TenBytes(J)V

    goto/32 :goto_16

    nop

    :goto_10
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_1
        :pswitch_8
        :pswitch_3
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_9
        :pswitch_2
    .end packed-switch

    :goto_11
    goto :goto_4

    :pswitch_6
    goto/32 :goto_5

    nop

    :goto_12
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeDirectWriter;->writeVarint64SixBytes(J)V

    goto/32 :goto_9

    nop

    :goto_13
    goto :goto_4

    :pswitch_7
    goto/32 :goto_7

    nop

    :goto_14
    invoke-static {p1, p2}, Lcom/android/framework/protobuf/BinaryWriter;->access$200(J)B

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/32 :goto_8

    nop

    :goto_15
    goto :goto_4

    :pswitch_8
    goto/32 :goto_c

    nop

    :goto_16
    goto/16 :goto_4

    :pswitch_9
    goto/32 :goto_2

    nop
.end method
