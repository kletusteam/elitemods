.class abstract Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;
.super Lcom/android/framework/protobuf/CodedOutputStream;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/framework/protobuf/CodedOutputStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "AbstractBufferedEncoder"
.end annotation


# instance fields
.field final buffer:[B

.field final limit:I

.field position:I

.field totalBytesWritten:I


# direct methods
.method constructor <init>(I)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/framework/protobuf/CodedOutputStream;-><init>(Lcom/android/framework/protobuf/CodedOutputStream$1;)V

    if-ltz p1, :cond_0

    const/16 v0, 0x14

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->buffer:[B

    array-length v0, v0

    iput v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->limit:I

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "bufferSize must be >= 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method final buffer(B)V
    .locals 3

    goto/32 :goto_1

    nop

    :goto_0
    aput-byte p1, v0, v1

    goto/32 :goto_4

    nop

    :goto_1
    iget-object v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->buffer:[B

    goto/32 :goto_3

    nop

    :goto_2
    iput v2, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_0

    nop

    :goto_3
    iget v1, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_7

    nop

    :goto_4
    iget v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->totalBytesWritten:I

    goto/32 :goto_8

    nop

    :goto_5
    iput v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->totalBytesWritten:I

    goto/32 :goto_6

    nop

    :goto_6
    return-void

    :goto_7
    add-int/lit8 v2, v1, 0x1

    goto/32 :goto_2

    nop

    :goto_8
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_5

    nop
.end method

.method final bufferFixed32NoTag(I)V
    .locals 4

    goto/32 :goto_17

    nop

    :goto_0
    int-to-byte v3, v3

    goto/32 :goto_7

    nop

    :goto_1
    and-int/lit16 v3, v3, 0xff

    goto/32 :goto_0

    nop

    :goto_2
    shr-int/lit8 v3, p1, 0x10

    goto/32 :goto_1

    nop

    :goto_3
    add-int/lit8 v2, v1, 0x1

    goto/32 :goto_d

    nop

    :goto_4
    iput v1, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_11

    nop

    :goto_5
    aput-byte v1, v0, v2

    goto/32 :goto_10

    nop

    :goto_6
    iput v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->totalBytesWritten:I

    goto/32 :goto_a

    nop

    :goto_7
    aput-byte v3, v0, v1

    goto/32 :goto_1a

    nop

    :goto_8
    int-to-byte v3, v3

    goto/32 :goto_18

    nop

    :goto_9
    shr-int/lit8 v3, p1, 0x8

    goto/32 :goto_16

    nop

    :goto_a
    return-void

    :goto_b
    iput v1, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_9

    nop

    :goto_c
    aput-byte v3, v0, v1

    goto/32 :goto_1c

    nop

    :goto_d
    iput v2, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_e

    nop

    :goto_e
    and-int/lit16 v3, p1, 0xff

    goto/32 :goto_15

    nop

    :goto_f
    add-int/lit8 v2, v1, 0x1

    goto/32 :goto_12

    nop

    :goto_10
    iget v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->totalBytesWritten:I

    goto/32 :goto_1b

    nop

    :goto_11
    shr-int/lit8 v1, p1, 0x18

    goto/32 :goto_13

    nop

    :goto_12
    iput v2, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_2

    nop

    :goto_13
    and-int/lit16 v1, v1, 0xff

    goto/32 :goto_14

    nop

    :goto_14
    int-to-byte v1, v1

    goto/32 :goto_5

    nop

    :goto_15
    int-to-byte v3, v3

    goto/32 :goto_c

    nop

    :goto_16
    and-int/lit16 v3, v3, 0xff

    goto/32 :goto_8

    nop

    :goto_17
    iget-object v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->buffer:[B

    goto/32 :goto_19

    nop

    :goto_18
    aput-byte v3, v0, v2

    goto/32 :goto_f

    nop

    :goto_19
    iget v1, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_3

    nop

    :goto_1a
    add-int/lit8 v1, v2, 0x1

    goto/32 :goto_4

    nop

    :goto_1b
    add-int/lit8 v0, v0, 0x4

    goto/32 :goto_6

    nop

    :goto_1c
    add-int/lit8 v1, v2, 0x1

    goto/32 :goto_b

    nop
.end method

.method final bufferFixed64NoTag(J)V
    .locals 8

    goto/32 :goto_44

    nop

    :goto_0
    const-wide/16 v3, 0xff

    goto/32 :goto_2a

    nop

    :goto_1
    iput v2, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_18

    nop

    :goto_2
    iget v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->totalBytesWritten:I

    goto/32 :goto_28

    nop

    :goto_3
    and-int/lit16 v3, v3, 0xff

    goto/32 :goto_26

    nop

    :goto_4
    int-to-byte v3, v3

    goto/32 :goto_9

    nop

    :goto_5
    and-int/lit16 v1, v1, 0xff

    goto/32 :goto_21

    nop

    :goto_6
    int-to-byte v5, v5

    goto/32 :goto_16

    nop

    :goto_7
    shr-long v6, p1, v5

    goto/32 :goto_3d

    nop

    :goto_8
    iput v1, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_1b

    nop

    :goto_9
    aput-byte v3, v0, v1

    goto/32 :goto_40

    nop

    :goto_a
    iput v1, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_20

    nop

    :goto_b
    add-int/lit8 v2, v1, 0x1

    goto/32 :goto_38

    nop

    :goto_c
    add-int/lit8 v1, v2, 0x1

    goto/32 :goto_37

    nop

    :goto_d
    shr-long v3, p1, v3

    goto/32 :goto_29

    nop

    :goto_e
    aput-byte v6, v0, v1

    goto/32 :goto_11

    nop

    :goto_f
    const/16 v1, 0x38

    goto/32 :goto_36

    nop

    :goto_10
    aput-byte v3, v0, v2

    goto/32 :goto_42

    nop

    :goto_11
    add-int/lit8 v1, v2, 0x1

    goto/32 :goto_a

    nop

    :goto_12
    long-to-int v6, v6

    goto/32 :goto_3a

    nop

    :goto_13
    iget v1, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_25

    nop

    :goto_14
    shr-long v6, p1, v6

    goto/32 :goto_41

    nop

    :goto_15
    int-to-byte v3, v3

    goto/32 :goto_10

    nop

    :goto_16
    aput-byte v5, v0, v1

    goto/32 :goto_22

    nop

    :goto_17
    shr-long v3, p1, v3

    goto/32 :goto_32

    nop

    :goto_18
    const/16 v3, 0x30

    goto/32 :goto_35

    nop

    :goto_19
    return-void

    :goto_1a
    int-to-byte v3, v3

    goto/32 :goto_1e

    nop

    :goto_1b
    const/16 v5, 0x8

    goto/32 :goto_7

    nop

    :goto_1c
    iput v2, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_0

    nop

    :goto_1d
    long-to-int v3, v3

    goto/32 :goto_34

    nop

    :goto_1e
    aput-byte v3, v0, v2

    goto/32 :goto_23

    nop

    :goto_1f
    const/16 v3, 0x28

    goto/32 :goto_17

    nop

    :goto_20
    const/16 v6, 0x18

    goto/32 :goto_39

    nop

    :goto_21
    int-to-byte v1, v1

    goto/32 :goto_3c

    nop

    :goto_22
    add-int/lit8 v1, v2, 0x1

    goto/32 :goto_8

    nop

    :goto_23
    add-int/lit8 v2, v1, 0x1

    goto/32 :goto_1

    nop

    :goto_24
    int-to-byte v6, v6

    goto/32 :goto_e

    nop

    :goto_25
    add-int/lit8 v2, v1, 0x1

    goto/32 :goto_1c

    nop

    :goto_26
    int-to-byte v3, v3

    goto/32 :goto_2e

    nop

    :goto_27
    and-int/lit16 v3, v3, 0xff

    goto/32 :goto_1a

    nop

    :goto_28
    add-int/2addr v0, v5

    goto/32 :goto_3f

    nop

    :goto_29
    long-to-int v3, v3

    goto/32 :goto_3

    nop

    :goto_2a
    and-long v5, p1, v3

    goto/32 :goto_2b

    nop

    :goto_2b
    long-to-int v5, v5

    goto/32 :goto_6

    nop

    :goto_2c
    and-long/2addr v3, v6

    goto/32 :goto_3e

    nop

    :goto_2d
    long-to-int v1, v3

    goto/32 :goto_5

    nop

    :goto_2e
    aput-byte v3, v0, v1

    goto/32 :goto_c

    nop

    :goto_2f
    long-to-int v6, v6

    goto/32 :goto_24

    nop

    :goto_30
    const/16 v3, 0x20

    goto/32 :goto_d

    nop

    :goto_31
    aput-byte v6, v0, v2

    goto/32 :goto_b

    nop

    :goto_32
    long-to-int v3, v3

    goto/32 :goto_27

    nop

    :goto_33
    const/16 v6, 0x10

    goto/32 :goto_14

    nop

    :goto_34
    and-int/lit16 v3, v3, 0xff

    goto/32 :goto_4

    nop

    :goto_35
    shr-long v3, p1, v3

    goto/32 :goto_1d

    nop

    :goto_36
    shr-long v3, p1, v1

    goto/32 :goto_2d

    nop

    :goto_37
    iput v1, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_1f

    nop

    :goto_38
    iput v2, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_33

    nop

    :goto_39
    shr-long v6, p1, v6

    goto/32 :goto_2c

    nop

    :goto_3a
    int-to-byte v6, v6

    goto/32 :goto_31

    nop

    :goto_3b
    iput v1, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_f

    nop

    :goto_3c
    aput-byte v1, v0, v2

    goto/32 :goto_2

    nop

    :goto_3d
    and-long/2addr v6, v3

    goto/32 :goto_12

    nop

    :goto_3e
    long-to-int v3, v3

    goto/32 :goto_15

    nop

    :goto_3f
    iput v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->totalBytesWritten:I

    goto/32 :goto_19

    nop

    :goto_40
    add-int/lit8 v1, v2, 0x1

    goto/32 :goto_3b

    nop

    :goto_41
    and-long/2addr v6, v3

    goto/32 :goto_2f

    nop

    :goto_42
    add-int/lit8 v2, v1, 0x1

    goto/32 :goto_43

    nop

    :goto_43
    iput v2, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_30

    nop

    :goto_44
    iget-object v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->buffer:[B

    goto/32 :goto_13

    nop
.end method

.method final bufferInt32NoTag(I)V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {p0, v0, v1}, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->bufferUInt64NoTag(J)V

    :goto_1
    goto/32 :goto_4

    nop

    :goto_2
    if-gez p1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_5

    nop

    :goto_3
    int-to-long v0, p1

    goto/32 :goto_0

    nop

    :goto_4
    return-void

    :goto_5
    invoke-virtual {p0, p1}, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->bufferUInt32NoTag(I)V

    goto/32 :goto_6

    nop

    :goto_6
    goto :goto_1

    :goto_7
    goto/32 :goto_3

    nop
.end method

.method final bufferTag(II)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->bufferUInt32NoTag(I)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    invoke-static {p1, p2}, Lcom/android/framework/protobuf/WireFormat;->makeTag(II)I

    move-result v0

    goto/32 :goto_0

    nop
.end method

.method final bufferUInt32NoTag(I)V
    .locals 6

    goto/32 :goto_35

    nop

    :goto_0
    iget v3, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_10

    nop

    :goto_1
    and-int/lit8 v5, p1, 0x7f

    goto/32 :goto_6

    nop

    :goto_2
    and-int/lit8 v2, p1, -0x80

    goto/32 :goto_13

    nop

    :goto_3
    iput v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->totalBytesWritten:I

    goto/32 :goto_4

    nop

    :goto_4
    return-void

    :goto_5
    goto/32 :goto_28

    nop

    :goto_6
    or-int/lit16 v5, v5, 0x80

    goto/32 :goto_21

    nop

    :goto_7
    iput v2, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_8

    nop

    :goto_8
    int-to-byte v2, p1

    goto/32 :goto_27

    nop

    :goto_9
    aput-byte v2, v0, v1

    goto/32 :goto_3a

    nop

    :goto_a
    iput v4, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_c

    nop

    :goto_b
    iget v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_30

    nop

    :goto_c
    int-to-long v3, v3

    goto/32 :goto_1

    nop

    :goto_d
    ushr-int/lit8 p1, p1, 0x7

    goto/32 :goto_18

    nop

    :goto_e
    iget v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->totalBytesWritten:I

    goto/32 :goto_1f

    nop

    :goto_f
    iput v4, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_2b

    nop

    :goto_10
    add-int/lit8 v4, v3, 0x1

    goto/32 :goto_a

    nop

    :goto_11
    and-int/lit8 v2, p1, 0x7f

    goto/32 :goto_3c

    nop

    :goto_12
    invoke-static {v2, v3, v4, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    nop

    goto/32 :goto_37

    nop

    :goto_13
    if-eqz v2, :cond_0

    goto/32 :goto_33

    :cond_0
    goto/32 :goto_19

    nop

    :goto_14
    iget-object v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->buffer:[B

    goto/32 :goto_1e

    nop

    :goto_15
    iget v1, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_24

    nop

    :goto_16
    if-nez v0, :cond_1

    goto/32 :goto_2e

    :cond_1
    goto/32 :goto_b

    nop

    :goto_17
    add-int/lit8 v4, v3, 0x1

    goto/32 :goto_f

    nop

    :goto_18
    goto :goto_2e

    :goto_19
    iget-object v2, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->buffer:[B

    goto/32 :goto_36

    nop

    :goto_1a
    iput v3, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->totalBytesWritten:I

    nop

    goto/32 :goto_32

    nop

    :goto_1b
    int-to-byte v5, p1

    goto/32 :goto_12

    nop

    :goto_1c
    iput v2, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_11

    nop

    :goto_1d
    add-int/lit8 v2, v1, 0x1

    goto/32 :goto_7

    nop

    :goto_1e
    iget v1, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_1d

    nop

    :goto_1f
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_3

    nop

    :goto_20
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_2c

    nop

    :goto_21
    int-to-byte v5, v5

    goto/32 :goto_23

    nop

    :goto_22
    iget-object v2, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->buffer:[B

    goto/32 :goto_0

    nop

    :goto_23
    invoke-static {v2, v3, v4, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_3b

    nop

    :goto_24
    add-int/lit8 v2, v1, 0x1

    goto/32 :goto_1c

    nop

    :goto_25
    and-int/lit8 v0, p1, -0x80

    goto/32 :goto_38

    nop

    :goto_26
    int-to-long v2, v2

    goto/32 :goto_2a

    nop

    :goto_27
    aput-byte v2, v0, v1

    goto/32 :goto_e

    nop

    :goto_28
    iget-object v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->buffer:[B

    goto/32 :goto_15

    nop

    :goto_29
    long-to-int v2, v2

    goto/32 :goto_39

    nop

    :goto_2a
    sub-long/2addr v2, v0

    goto/32 :goto_29

    nop

    :goto_2b
    int-to-long v3, v3

    goto/32 :goto_1b

    nop

    :goto_2c
    iput v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->totalBytesWritten:I

    goto/32 :goto_d

    nop

    :goto_2d
    goto :goto_31

    :goto_2e
    goto/32 :goto_25

    nop

    :goto_2f
    add-int/2addr v3, v2

    goto/32 :goto_1a

    nop

    :goto_30
    int-to-long v0, v0

    :goto_31
    goto/32 :goto_2

    nop

    :goto_32
    return-void

    :goto_33
    goto/32 :goto_22

    nop

    :goto_34
    int-to-byte v2, v2

    goto/32 :goto_9

    nop

    :goto_35
    invoke-static {}, Lcom/android/framework/protobuf/CodedOutputStream;->access$100()Z

    move-result v0

    goto/32 :goto_16

    nop

    :goto_36
    iget v3, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_17

    nop

    :goto_37
    iget v2, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_26

    nop

    :goto_38
    if-eqz v0, :cond_2

    goto/32 :goto_5

    :cond_2
    goto/32 :goto_14

    nop

    :goto_39
    iget v3, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->totalBytesWritten:I

    goto/32 :goto_2f

    nop

    :goto_3a
    iget v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->totalBytesWritten:I

    goto/32 :goto_20

    nop

    :goto_3b
    ushr-int/lit8 p1, p1, 0x7

    goto/32 :goto_2d

    nop

    :goto_3c
    or-int/lit16 v2, v2, 0x80

    goto/32 :goto_34

    nop
.end method

.method final bufferUInt64NoTag(J)V
    .locals 11

    goto/32 :goto_3a

    nop

    :goto_0
    ushr-long/2addr p1, v1

    goto/32 :goto_36

    nop

    :goto_1
    iput v2, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_1b

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_22

    nop

    :goto_3
    return-void

    :goto_4
    goto/32 :goto_26

    nop

    :goto_5
    int-to-long v6, v0

    :goto_6
    goto/32 :goto_3c

    nop

    :goto_7
    iget v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->totalBytesWritten:I

    goto/32 :goto_28

    nop

    :goto_8
    cmp-long v0, v8, v2

    goto/32 :goto_14

    nop

    :goto_9
    add-int/2addr v1, v0

    goto/32 :goto_3d

    nop

    :goto_a
    iget v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_24

    nop

    :goto_b
    and-int/lit8 v10, v10, 0x7f

    goto/32 :goto_33

    nop

    :goto_c
    iget v8, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_32

    nop

    :goto_d
    invoke-static {v0, v1, v2, v3}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    nop

    goto/32 :goto_a

    nop

    :goto_e
    iget-object v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->buffer:[B

    goto/32 :goto_c

    nop

    :goto_f
    add-int/lit8 v7, v6, 0x1

    goto/32 :goto_27

    nop

    :goto_10
    add-int/lit8 v2, v1, 0x1

    goto/32 :goto_38

    nop

    :goto_11
    goto :goto_6

    :goto_12
    goto/32 :goto_1c

    nop

    :goto_13
    int-to-byte v10, v10

    goto/32 :goto_39

    nop

    :goto_14
    if-eqz v0, :cond_1

    goto/32 :goto_2f

    :cond_1
    goto/32 :goto_1e

    nop

    :goto_15
    if-eqz v0, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_42

    nop

    :goto_16
    or-int/lit16 v7, v7, 0x80

    goto/32 :goto_18

    nop

    :goto_17
    cmp-long v0, v6, v2

    goto/32 :goto_15

    nop

    :goto_18
    int-to-byte v7, v7

    goto/32 :goto_35

    nop

    :goto_19
    sub-long/2addr v0, v6

    goto/32 :goto_3f

    nop

    :goto_1a
    and-int/lit8 v7, v7, 0x7f

    goto/32 :goto_16

    nop

    :goto_1b
    int-to-long v1, v1

    goto/32 :goto_44

    nop

    :goto_1c
    and-long v6, p1, v4

    goto/32 :goto_17

    nop

    :goto_1d
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_25

    nop

    :goto_1e
    iget-object v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->buffer:[B

    goto/32 :goto_37

    nop

    :goto_1f
    iget v6, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_f

    nop

    :goto_20
    const/4 v1, 0x7

    goto/32 :goto_43

    nop

    :goto_21
    int-to-long v8, v8

    goto/32 :goto_30

    nop

    :goto_22
    iget v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_5

    nop

    :goto_23
    long-to-int v7, p1

    goto/32 :goto_1a

    nop

    :goto_24
    int-to-long v0, v0

    goto/32 :goto_19

    nop

    :goto_25
    iput v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->totalBytesWritten:I

    goto/32 :goto_3

    nop

    :goto_26
    iget-object v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->buffer:[B

    goto/32 :goto_1f

    nop

    :goto_27
    iput v7, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_23

    nop

    :goto_28
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_29

    nop

    :goto_29
    iput v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->totalBytesWritten:I

    goto/32 :goto_0

    nop

    :goto_2a
    const-wide/16 v4, -0x80

    goto/32 :goto_2

    nop

    :goto_2b
    long-to-int v2, p1

    goto/32 :goto_2d

    nop

    :goto_2c
    iget v1, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_10

    nop

    :goto_2d
    int-to-byte v2, v2

    goto/32 :goto_45

    nop

    :goto_2e
    return-void

    :goto_2f
    goto/32 :goto_e

    nop

    :goto_30
    long-to-int v10, p1

    goto/32 :goto_b

    nop

    :goto_31
    iget v1, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->totalBytesWritten:I

    goto/32 :goto_9

    nop

    :goto_32
    add-int/lit8 v9, v8, 0x1

    goto/32 :goto_3b

    nop

    :goto_33
    or-int/lit16 v10, v10, 0x80

    goto/32 :goto_13

    nop

    :goto_34
    add-int/lit8 v2, v1, 0x1

    goto/32 :goto_1

    nop

    :goto_35
    aput-byte v7, v0, v6

    goto/32 :goto_7

    nop

    :goto_36
    goto/16 :goto_12

    :goto_37
    iget v1, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_34

    nop

    :goto_38
    iput v2, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_2b

    nop

    :goto_39
    invoke-static {v0, v8, v9, v10}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_41

    nop

    :goto_3a
    invoke-static {}, Lcom/android/framework/protobuf/CodedOutputStream;->access$100()Z

    move-result v0

    goto/32 :goto_20

    nop

    :goto_3b
    iput v9, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->position:I

    goto/32 :goto_21

    nop

    :goto_3c
    and-long v8, p1, v4

    goto/32 :goto_8

    nop

    :goto_3d
    iput v1, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->totalBytesWritten:I

    nop

    goto/32 :goto_2e

    nop

    :goto_3e
    iget v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->totalBytesWritten:I

    goto/32 :goto_1d

    nop

    :goto_3f
    long-to-int v0, v0

    goto/32 :goto_31

    nop

    :goto_40
    int-to-byte v3, v3

    goto/32 :goto_d

    nop

    :goto_41
    ushr-long/2addr p1, v1

    goto/32 :goto_11

    nop

    :goto_42
    iget-object v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->buffer:[B

    goto/32 :goto_2c

    nop

    :goto_43
    const-wide/16 v2, 0x0

    goto/32 :goto_2a

    nop

    :goto_44
    long-to-int v3, p1

    goto/32 :goto_40

    nop

    :goto_45
    aput-byte v2, v0, v1

    goto/32 :goto_3e

    nop
.end method

.method public final getTotalBytesWritten()I
    .locals 1

    iget v0, p0, Lcom/android/framework/protobuf/CodedOutputStream$AbstractBufferedEncoder;->totalBytesWritten:I

    return v0
.end method

.method public final spaceLeft()I
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "spaceLeft() can only be called on CodedOutputStreams that are writing to a flat array or ByteBuffer."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
