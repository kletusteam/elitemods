.class Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder$SkippedDataSink;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder$RefillCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SkippedDataSink"
.end annotation


# instance fields
.field private byteArrayStream:Ljava/io/ByteArrayOutputStream;

.field private lastPos:I

.field final synthetic this$0:Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder;


# direct methods
.method private constructor <init>(Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder;)V
    .locals 0

    iput-object p1, p0, Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder$SkippedDataSink;->this$0:Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder;->access$500(Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder;)I

    move-result p1

    iput p1, p0, Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder$SkippedDataSink;->lastPos:I

    return-void
.end method


# virtual methods
.method getSkippedData()Ljava/nio/ByteBuffer;
    .locals 4

    goto/32 :goto_2

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v3, p0, Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder$SkippedDataSink;->this$0:Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder;

    goto/32 :goto_d

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder$SkippedDataSink;->byteArrayStream:Ljava/io/ByteArrayOutputStream;

    goto/32 :goto_c

    nop

    :goto_3
    invoke-static {v0, v1, v2}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v0

    goto/32 :goto_11

    nop

    :goto_4
    invoke-static {v1}, Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder;->access$600(Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder;)[B

    move-result-object v1

    goto/32 :goto_13

    nop

    :goto_5
    sub-int/2addr v2, v3

    goto/32 :goto_3

    nop

    :goto_6
    iget-object v0, p0, Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder$SkippedDataSink;->byteArrayStream:Ljava/io/ByteArrayOutputStream;

    goto/32 :goto_7

    nop

    :goto_7
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    goto/32 :goto_f

    nop

    :goto_8
    iget v3, p0, Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder$SkippedDataSink;->lastPos:I

    goto/32 :goto_5

    nop

    :goto_9
    iget-object v1, p0, Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder$SkippedDataSink;->this$0:Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder;

    goto/32 :goto_4

    nop

    :goto_a
    invoke-static {v0}, Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder;->access$600(Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder;)[B

    move-result-object v0

    goto/32 :goto_e

    nop

    :goto_b
    iget-object v2, p0, Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder$SkippedDataSink;->this$0:Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder;

    goto/32 :goto_15

    nop

    :goto_c
    if-eqz v0, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_10

    nop

    :goto_d
    invoke-static {v3}, Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder;->access$500(Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder;)I

    move-result v3

    goto/32 :goto_14

    nop

    :goto_e
    iget v1, p0, Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder$SkippedDataSink;->lastPos:I

    goto/32 :goto_b

    nop

    :goto_f
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_10
    iget-object v0, p0, Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder$SkippedDataSink;->this$0:Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder;

    goto/32 :goto_a

    nop

    :goto_11
    return-object v0

    :goto_12
    goto/32 :goto_9

    nop

    :goto_13
    iget v2, p0, Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder$SkippedDataSink;->lastPos:I

    goto/32 :goto_1

    nop

    :goto_14
    invoke-virtual {v0, v1, v2, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto/32 :goto_6

    nop

    :goto_15
    invoke-static {v2}, Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder;->access$500(Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder;)I

    move-result v2

    goto/32 :goto_8

    nop
.end method

.method public onRefill()V
    .locals 5

    iget-object v0, p0, Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder$SkippedDataSink;->byteArrayStream:Ljava/io/ByteArrayOutputStream;

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder$SkippedDataSink;->byteArrayStream:Ljava/io/ByteArrayOutputStream;

    :cond_0
    iget-object v0, p0, Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder$SkippedDataSink;->byteArrayStream:Ljava/io/ByteArrayOutputStream;

    iget-object v1, p0, Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder$SkippedDataSink;->this$0:Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder;

    invoke-static {v1}, Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder;->access$600(Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder;)[B

    move-result-object v1

    iget v2, p0, Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder$SkippedDataSink;->lastPos:I

    iget-object v3, p0, Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder$SkippedDataSink;->this$0:Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder;

    invoke-static {v3}, Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder;->access$500(Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder;)I

    move-result v3

    iget v4, p0, Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder$SkippedDataSink;->lastPos:I

    sub-int/2addr v3, v4

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/framework/protobuf/CodedInputStream$StreamDecoder$SkippedDataSink;->lastPos:I

    return-void
.end method
