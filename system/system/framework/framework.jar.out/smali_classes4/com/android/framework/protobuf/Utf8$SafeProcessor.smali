.class final Lcom/android/framework/protobuf/Utf8$SafeProcessor;
.super Lcom/android/framework/protobuf/Utf8$Processor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/framework/protobuf/Utf8;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "SafeProcessor"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/framework/protobuf/Utf8$Processor;-><init>()V

    return-void
.end method

.method private static partialIsValidUtf8([BII)I
    .locals 1

    :goto_0
    if-ge p1, p2, :cond_0

    aget-byte v0, p0, p1

    if-ltz v0, :cond_0

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    if-lt p1, p2, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    invoke-static {p0, p1, p2}, Lcom/android/framework/protobuf/Utf8$SafeProcessor;->partialIsValidUtf8NonAscii([BII)I

    move-result v0

    :goto_1
    return v0
.end method

.method private static partialIsValidUtf8NonAscii([BII)I
    .locals 6

    :goto_0
    if-lt p1, p2, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    add-int/lit8 v0, p1, 0x1

    aget-byte p1, p0, p1

    move v1, p1

    if-gez p1, :cond_d

    const/16 p1, -0x20

    const/4 v2, -0x1

    const/16 v3, -0x41

    if-ge v1, p1, :cond_3

    if-lt v0, p2, :cond_1

    return v1

    :cond_1
    const/16 p1, -0x3e

    if-lt v1, p1, :cond_2

    add-int/lit8 p1, v0, 0x1

    aget-byte v0, p0, v0

    if-le v0, v3, :cond_b

    move v0, p1

    :cond_2
    return v2

    :cond_3
    const/16 v4, -0x10

    if-ge v1, v4, :cond_8

    add-int/lit8 v4, p2, -0x1

    if-lt v0, v4, :cond_4

    invoke-static {p0, v0, p2}, Lcom/android/framework/protobuf/Utf8;->access$1100([BII)I

    move-result p1

    return p1

    :cond_4
    add-int/lit8 v4, v0, 0x1

    aget-byte v0, p0, v0

    move v5, v0

    if-gt v0, v3, :cond_7

    const/16 v0, -0x60

    if-ne v1, p1, :cond_5

    if-lt v5, v0, :cond_7

    :cond_5
    const/16 p1, -0x13

    if-ne v1, p1, :cond_6

    if-ge v5, v0, :cond_7

    :cond_6
    add-int/lit8 p1, v4, 0x1

    aget-byte v0, p0, v4

    if-le v0, v3, :cond_b

    move v4, p1

    :cond_7
    return v2

    :cond_8
    add-int/lit8 p1, p2, -0x2

    if-lt v0, p1, :cond_9

    invoke-static {p0, v0, p2}, Lcom/android/framework/protobuf/Utf8;->access$1100([BII)I

    move-result p1

    return p1

    :cond_9
    add-int/lit8 p1, v0, 0x1

    aget-byte v0, p0, v0

    move v4, v0

    if-gt v0, v3, :cond_a

    shl-int/lit8 v0, v1, 0x1c

    add-int/lit8 v5, v4, 0x70

    add-int/2addr v0, v5

    shr-int/lit8 v0, v0, 0x1e

    if-nez v0, :cond_a

    add-int/lit8 v0, p1, 0x1

    aget-byte p1, p0, p1

    if-gt p1, v3, :cond_c

    add-int/lit8 p1, v0, 0x1

    aget-byte v0, p0, v0

    if-le v0, v3, :cond_b

    :cond_a
    goto :goto_1

    :cond_b
    goto :goto_0

    :cond_c
    move p1, v0

    :goto_1
    return v2

    :cond_d
    move p1, v0

    goto :goto_0
.end method


# virtual methods
.method decodeUtf8([BII)Ljava/lang/String;
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/framework/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    goto/32 :goto_2c

    nop

    :goto_0
    move v9, v10

    goto/32 :goto_5

    nop

    :goto_1
    const/4 v4, 0x2

    goto/32 :goto_16

    nop

    :goto_2
    move v2, v6

    goto/32 :goto_4b

    nop

    :goto_3
    move v2, v8

    goto/32 :goto_38

    nop

    :goto_4
    move v2, v13

    goto/32 :goto_6b

    nop

    :goto_5
    move-object v10, v12

    goto/32 :goto_42

    nop

    :goto_6
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/32 :goto_53

    nop

    :goto_7
    add-int/lit8 v7, v5, -0x2

    goto/32 :goto_2b

    nop

    :goto_8
    if-lt v6, v7, :cond_0

    goto/32 :goto_6f

    :cond_0
    goto/32 :goto_54

    nop

    :goto_9
    throw v3

    :goto_a
    goto/32 :goto_7

    nop

    :goto_b
    invoke-static {v8, v12, v7}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$500(B[CI)V

    goto/32 :goto_67

    nop

    :goto_c
    invoke-static {v2, v6, v7, v12, v11}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$900(BBB[CI)V

    goto/32 :goto_3

    nop

    :goto_d
    add-int/lit8 v14, v11, 0x1

    goto/32 :goto_63

    nop

    :goto_e
    aput-object v6, v5, v3

    goto/32 :goto_6

    nop

    :goto_f
    sub-int v3, v3, p2

    goto/32 :goto_5d

    nop

    :goto_10
    move v8, v9

    goto/32 :goto_0

    nop

    :goto_11
    or-int/2addr v2, v3

    goto/32 :goto_3e

    nop

    :goto_12
    move v11, v8

    goto/32 :goto_47

    nop

    :goto_13
    add-int/lit8 v6, v6, 0x1

    goto/32 :goto_7d

    nop

    :goto_14
    add-int/lit8 v8, v7, 0x1

    goto/32 :goto_66

    nop

    :goto_15
    if-lt v6, v5, :cond_1

    goto/32 :goto_48

    :cond_1
    goto/32 :goto_4f

    nop

    :goto_16
    aput-object v3, v5, v4

    goto/32 :goto_22

    nop

    :goto_17
    add-int/lit8 v7, v6, 0x1

    goto/32 :goto_2e

    nop

    :goto_18
    throw v3

    :goto_19
    goto/32 :goto_2f

    nop

    :goto_1a
    invoke-static {v2}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$600(B)Z

    move-result v7

    goto/32 :goto_31

    nop

    :goto_1b
    if-nez v7, :cond_2

    goto/32 :goto_a

    :cond_2
    goto/32 :goto_20

    nop

    :goto_1c
    invoke-static {v2, v12, v11}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$500(B[CI)V

    :goto_1d
    goto/32 :goto_3f

    nop

    :goto_1e
    invoke-static {v2, v6, v12, v11}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$700(BB[CI)V

    goto/32 :goto_52

    nop

    :goto_1f
    add-int/lit8 v8, v6, 0x1

    goto/32 :goto_49

    nop

    :goto_20
    add-int/lit8 v7, v5, -0x1

    goto/32 :goto_8

    nop

    :goto_21
    invoke-static {v2}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$400(B)Z

    move-result v7

    goto/32 :goto_30

    nop

    :goto_22
    const-string v3, "buffer length=%d, index=%d, size=%d"

    goto/32 :goto_7a

    nop

    :goto_23
    aget-byte v2, v0, v2

    goto/32 :goto_21

    nop

    :goto_24
    invoke-static {v8}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$400(B)Z

    move-result v9

    goto/32 :goto_40

    nop

    :goto_25
    invoke-static {}, Lcom/android/framework/protobuf/InvalidProtocolBufferException;->invalidUtf8()Lcom/android/framework/protobuf/InvalidProtocolBufferException;

    move-result-object v3

    goto/32 :goto_69

    nop

    :goto_26
    array-length v3, v0

    goto/32 :goto_f

    nop

    :goto_27
    add-int/2addr v14, v4

    goto/32 :goto_4

    nop

    :goto_28
    invoke-direct {v4, v12, v3, v11}, Ljava/lang/String;-><init>([CII)V

    goto/32 :goto_39

    nop

    :goto_29
    add-int/lit8 v7, v11, 0x1

    goto/32 :goto_1c

    nop

    :goto_2a
    aget-byte v6, v0, v6

    goto/32 :goto_64

    nop

    :goto_2b
    if-lt v6, v7, :cond_3

    goto/32 :goto_3d

    :cond_3
    goto/32 :goto_17

    nop

    :goto_2c
    move-object/from16 v0, p1

    goto/32 :goto_5b

    nop

    :goto_2d
    array-length v6, v0

    goto/32 :goto_70

    nop

    :goto_2e
    aget-byte v8, v0, v6

    goto/32 :goto_7c

    nop

    :goto_2f
    invoke-static {v2}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$800(B)Z

    move-result v7

    goto/32 :goto_1b

    nop

    :goto_30
    if-nez v7, :cond_4

    goto/32 :goto_46

    :cond_4
    goto/32 :goto_29

    nop

    :goto_31
    if-nez v7, :cond_5

    goto/32 :goto_19

    :cond_5
    goto/32 :goto_15

    nop

    :goto_32
    const/4 v6, 0x0

    :goto_33
    goto/32 :goto_4a

    nop

    :goto_34
    const/4 v4, 0x1

    goto/32 :goto_5f

    nop

    :goto_35
    aget-byte v8, v0, v6

    goto/32 :goto_24

    nop

    :goto_36
    new-array v12, v1, [C

    goto/32 :goto_32

    nop

    :goto_37
    invoke-static {}, Lcom/android/framework/protobuf/InvalidProtocolBufferException;->invalidUtf8()Lcom/android/framework/protobuf/InvalidProtocolBufferException;

    move-result-object v3

    goto/32 :goto_9

    nop

    :goto_38
    move v11, v9

    goto/32 :goto_6e

    nop

    :goto_39
    return-object v4

    :goto_3a
    goto/32 :goto_6d

    nop

    :goto_3b
    if-lt v2, v5, :cond_6

    goto/32 :goto_6a

    :cond_6
    goto/32 :goto_65

    nop

    :goto_3c
    goto/16 :goto_58

    :goto_3d
    goto/32 :goto_25

    nop

    :goto_3e
    const/4 v3, 0x0

    goto/32 :goto_34

    nop

    :goto_3f
    if-lt v6, v5, :cond_7

    goto/32 :goto_72

    :cond_7
    goto/32 :goto_35

    nop

    :goto_40
    if-eqz v9, :cond_8

    goto/32 :goto_56

    :cond_8
    goto/32 :goto_55

    nop

    :goto_41
    aget-byte v7, v0, v2

    goto/32 :goto_62

    nop

    :goto_42
    invoke-static/range {v6 .. v11}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$1000(BBBB[CI)V

    goto/32 :goto_27

    nop

    :goto_43
    goto/16 :goto_75

    :goto_44
    goto/32 :goto_7e

    nop

    :goto_45
    goto/16 :goto_6c

    :goto_46
    goto/32 :goto_1a

    nop

    :goto_47
    goto/16 :goto_6c

    :goto_48
    goto/32 :goto_7b

    nop

    :goto_49
    invoke-static {v7, v12, v6}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$500(B[CI)V

    goto/32 :goto_5a

    nop

    :goto_4a
    if-lt v2, v5, :cond_9

    goto/32 :goto_75

    :cond_9
    goto/32 :goto_41

    nop

    :goto_4b
    move v11, v7

    goto/32 :goto_45

    nop

    :goto_4c
    if-eqz v8, :cond_a

    goto/32 :goto_44

    :cond_a
    goto/32 :goto_43

    nop

    :goto_4d
    new-array v5, v5, [Ljava/lang/Object;

    goto/32 :goto_2d

    nop

    :goto_4e
    or-int v2, p2, v1

    goto/32 :goto_26

    nop

    :goto_4f
    add-int/lit8 v7, v6, 0x1

    goto/32 :goto_2a

    nop

    :goto_50
    aget-byte v9, v0, v7

    goto/32 :goto_77

    nop

    :goto_51
    aget-byte v10, v0, v6

    goto/32 :goto_d

    nop

    :goto_52
    move v2, v7

    goto/32 :goto_12

    nop

    :goto_53
    aput-object v3, v5, v4

    goto/32 :goto_68

    nop

    :goto_54
    add-int/lit8 v7, v6, 0x1

    goto/32 :goto_78

    nop

    :goto_55
    goto/16 :goto_72

    :goto_56
    goto/32 :goto_13

    nop

    :goto_57
    move v11, v6

    :goto_58
    goto/32 :goto_3b

    nop

    :goto_59
    const/4 v5, 0x3

    goto/32 :goto_4d

    nop

    :goto_5a
    move v6, v8

    goto/32 :goto_74

    nop

    :goto_5b
    move/from16 v1, p3

    goto/32 :goto_4e

    nop

    :goto_5c
    throw v2

    :goto_5d
    sub-int/2addr v3, v1

    goto/32 :goto_11

    nop

    :goto_5e
    add-int/lit8 v9, v11, 0x1

    goto/32 :goto_c

    nop

    :goto_5f
    if-gez v2, :cond_b

    goto/32 :goto_3a

    :cond_b
    goto/32 :goto_73

    nop

    :goto_60
    move v7, v8

    goto/32 :goto_10

    nop

    :goto_61
    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_5c

    nop

    :goto_62
    invoke-static {v7}, Lcom/android/framework/protobuf/Utf8$DecodeUtil;->access$400(B)Z

    move-result v8

    goto/32 :goto_4c

    nop

    :goto_63
    move v6, v2

    goto/32 :goto_60

    nop

    :goto_64
    add-int/lit8 v8, v11, 0x1

    goto/32 :goto_1e

    nop

    :goto_65
    add-int/lit8 v6, v2, 0x1

    goto/32 :goto_23

    nop

    :goto_66
    aget-byte v7, v0, v7

    goto/32 :goto_5e

    nop

    :goto_67
    move v7, v9

    goto/32 :goto_71

    nop

    :goto_68
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/32 :goto_1

    nop

    :goto_69
    throw v3

    :goto_6a
    goto/32 :goto_79

    nop

    :goto_6b
    move v11, v14

    :goto_6c
    goto/32 :goto_3c

    nop

    :goto_6d
    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    goto/32 :goto_59

    nop

    :goto_6e
    goto :goto_6c

    :goto_6f
    goto/32 :goto_37

    nop

    :goto_70
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto/32 :goto_e

    nop

    :goto_71
    goto/16 :goto_1d

    :goto_72
    goto/32 :goto_2

    nop

    :goto_73
    move/from16 v2, p2

    goto/32 :goto_76

    nop

    :goto_74
    goto/16 :goto_33

    :goto_75
    goto/32 :goto_57

    nop

    :goto_76
    add-int v5, v2, v1

    goto/32 :goto_36

    nop

    :goto_77
    add-int/lit8 v13, v6, 0x1

    goto/32 :goto_51

    nop

    :goto_78
    aget-byte v6, v0, v6

    goto/32 :goto_14

    nop

    :goto_79
    new-instance v4, Ljava/lang/String;

    goto/32 :goto_28

    nop

    :goto_7a
    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_61

    nop

    :goto_7b
    invoke-static {}, Lcom/android/framework/protobuf/InvalidProtocolBufferException;->invalidUtf8()Lcom/android/framework/protobuf/InvalidProtocolBufferException;

    move-result-object v3

    goto/32 :goto_18

    nop

    :goto_7c
    add-int/lit8 v6, v7, 0x1

    goto/32 :goto_50

    nop

    :goto_7d
    add-int/lit8 v9, v7, 0x1

    goto/32 :goto_b

    nop

    :goto_7e
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_1f

    nop
.end method

.method decodeUtf8Direct(Ljava/nio/ByteBuffer;II)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/framework/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/framework/protobuf/Utf8$SafeProcessor;->decodeUtf8Default(Ljava/nio/ByteBuffer;II)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method encodeUtf8(Ljava/lang/CharSequence;[BII)I
    .locals 10

    goto/32 :goto_37

    nop

    :goto_0
    if-ne v6, v7, :cond_0

    goto/32 :goto_1c

    :cond_0
    goto/32 :goto_4f

    nop

    :goto_1
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_2e

    nop

    :goto_2
    if-lt v1, v3, :cond_1

    goto/32 :goto_7f

    :cond_1
    goto/32 :goto_5c

    nop

    :goto_3
    if-nez v6, :cond_2

    goto/32 :goto_1c

    :cond_2
    goto/32 :goto_44

    nop

    :goto_4
    or-int/2addr v7, v4

    goto/32 :goto_2d

    nop

    :goto_5
    if-lt v2, v0, :cond_3

    goto/32 :goto_46

    :cond_3
    goto/32 :goto_41

    nop

    :goto_6
    ushr-int/lit8 v9, v6, 0xc

    goto/32 :goto_32

    nop

    :goto_7
    add-int/2addr v1, v2

    :goto_8
    goto/32 :goto_71

    nop

    :goto_9
    int-to-byte v7, v7

    goto/32 :goto_39

    nop

    :goto_a
    ushr-int/lit8 v9, v6, 0x12

    goto/32 :goto_69

    nop

    :goto_b
    add-int/lit8 v6, v3, -0x2

    goto/32 :goto_78

    nop

    :goto_c
    const/16 v4, 0x80

    goto/32 :goto_5

    nop

    :goto_d
    move v7, v6

    goto/32 :goto_18

    nop

    :goto_e
    add-int/lit8 v8, v3, -0x3

    goto/32 :goto_1a

    nop

    :goto_f
    if-lt v6, v5, :cond_4

    goto/32 :goto_3c

    :cond_4
    :goto_10
    goto/32 :goto_e

    nop

    :goto_11
    return v4

    :goto_12
    goto/32 :goto_7

    nop

    :goto_13
    if-eqz v4, :cond_5

    goto/32 :goto_91

    :cond_5
    :goto_14
    goto/32 :goto_5b

    nop

    :goto_15
    move v1, p3

    goto/32 :goto_4b

    nop

    :goto_16
    if-le v1, v8, :cond_6

    goto/32 :goto_8b

    :cond_6
    goto/32 :goto_5a

    nop

    :goto_17
    aput-byte v7, p2, v1

    goto/32 :goto_86

    nop

    :goto_18
    invoke-static {v5, v6}, Ljava/lang/Character;->isSurrogatePair(CC)Z

    move-result v6

    goto/32 :goto_3

    nop

    :goto_19
    const/16 v6, 0x800

    goto/32 :goto_65

    nop

    :goto_1a
    if-le v1, v8, :cond_7

    goto/32 :goto_3c

    :cond_7
    goto/32 :goto_26

    nop

    :goto_1b
    goto/16 :goto_8

    :goto_1c
    goto/32 :goto_6c

    nop

    :goto_1d
    ushr-int/lit8 v7, v5, 0x6

    goto/32 :goto_5d

    nop

    :goto_1e
    or-int/2addr v7, v4

    goto/32 :goto_9

    nop

    :goto_1f
    invoke-direct {v4, v6}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_61

    nop

    :goto_20
    add-int/lit8 v6, v1, 0x1

    goto/32 :goto_1d

    nop

    :goto_21
    if-le v5, v6, :cond_8

    goto/32 :goto_91

    :cond_8
    goto/32 :goto_89

    nop

    :goto_22
    add-int/lit8 v1, v6, 0x1

    goto/32 :goto_7c

    nop

    :goto_23
    if-lt v5, v3, :cond_9

    goto/32 :goto_46

    :cond_9
    goto/32 :goto_28

    nop

    :goto_24
    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    goto/32 :goto_81

    nop

    :goto_25
    invoke-direct {v4, v6, v0}, Lcom/android/framework/protobuf/Utf8$UnpairedSurrogateException;-><init>(II)V

    goto/32 :goto_8a

    nop

    :goto_26
    add-int/lit8 v6, v1, 0x1

    goto/32 :goto_76

    nop

    :goto_27
    invoke-direct {v4, v2, v0}, Lcom/android/framework/protobuf/Utf8$UnpairedSurrogateException;-><init>(II)V

    goto/32 :goto_90

    nop

    :goto_28
    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    goto/32 :goto_43

    nop

    :goto_29
    and-int/lit8 v9, v6, 0x3f

    goto/32 :goto_38

    nop

    :goto_2a
    int-to-byte v7, v7

    goto/32 :goto_3f

    nop

    :goto_2b
    add-int/lit8 v1, v8, 0x1

    goto/32 :goto_6

    nop

    :goto_2c
    or-int/2addr v9, v4

    goto/32 :goto_30

    nop

    :goto_2d
    int-to-byte v7, v7

    goto/32 :goto_40

    nop

    :goto_2e
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_66

    nop

    :goto_2f
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_45

    nop

    :goto_30
    int-to-byte v9, v9

    goto/32 :goto_6a

    nop

    :goto_31
    move v1, v6

    goto/32 :goto_7e

    nop

    :goto_32
    and-int/lit8 v9, v9, 0x3f

    goto/32 :goto_2c

    nop

    :goto_33
    add-int/lit8 v1, v8, 0x1

    goto/32 :goto_29

    nop

    :goto_34
    add-int v3, p3, p4

    :goto_35
    goto/32 :goto_c

    nop

    :goto_36
    ushr-int/lit8 v7, v5, 0x6

    goto/32 :goto_49

    nop

    :goto_37
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    goto/32 :goto_15

    nop

    :goto_38
    or-int/2addr v9, v4

    goto/32 :goto_6f

    nop

    :goto_39
    aput-byte v7, p2, v6

    goto/32 :goto_75

    nop

    :goto_3a
    and-int/lit8 v9, v9, 0x3f

    goto/32 :goto_42

    nop

    :goto_3b
    goto/16 :goto_6e

    :goto_3c
    goto/32 :goto_59

    nop

    :goto_3d
    if-ge v5, v7, :cond_a

    goto/32 :goto_10

    :cond_a
    goto/32 :goto_f

    nop

    :goto_3e
    int-to-byte v7, v5

    goto/32 :goto_82

    nop

    :goto_3f
    aput-byte v7, p2, v1

    goto/32 :goto_22

    nop

    :goto_40
    aput-byte v7, p2, v6

    goto/32 :goto_79

    nop

    :goto_41
    add-int v5, v2, v1

    goto/32 :goto_23

    nop

    :goto_42
    or-int/2addr v9, v4

    goto/32 :goto_7d

    nop

    :goto_43
    move v6, v5

    goto/32 :goto_8e

    nop

    :goto_44
    invoke-static {v5, v7}, Ljava/lang/Character;->toCodePoint(CC)I

    move-result v6

    goto/32 :goto_7b

    nop

    :goto_45
    goto :goto_35

    :goto_46
    goto/32 :goto_5e

    nop

    :goto_47
    int-to-byte v7, v7

    goto/32 :goto_8d

    nop

    :goto_48
    aput-byte v9, p2, v1

    goto/32 :goto_33

    nop

    :goto_49
    and-int/lit8 v7, v7, 0x3f

    goto/32 :goto_1e

    nop

    :goto_4a
    add-int/lit8 v8, v1, 0x1

    goto/32 :goto_63

    nop

    :goto_4b
    const/4 v2, 0x0

    goto/32 :goto_34

    nop

    :goto_4c
    aput-byte v9, p2, v1

    goto/32 :goto_2b

    nop

    :goto_4d
    add-int/lit8 v4, v2, 0x1

    goto/32 :goto_83

    nop

    :goto_4e
    or-int/lit16 v7, v7, 0x1e0

    goto/32 :goto_47

    nop

    :goto_4f
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_50

    nop

    :goto_50
    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    goto/32 :goto_d

    nop

    :goto_51
    add-int v4, v1, v2

    goto/32 :goto_6b

    nop

    :goto_52
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v6

    goto/32 :goto_84

    nop

    :goto_53
    const v6, 0xdfff

    goto/32 :goto_88

    nop

    :goto_54
    add-int v4, v1, v0

    goto/32 :goto_11

    nop

    :goto_55
    int-to-byte v9, v9

    goto/32 :goto_4c

    nop

    :goto_56
    const-string v7, "Failed writing "

    goto/32 :goto_85

    nop

    :goto_57
    add-int/lit8 v1, v6, 0x1

    goto/32 :goto_36

    nop

    :goto_58
    new-instance v6, Ljava/lang/StringBuilder;

    goto/32 :goto_70

    nop

    :goto_59
    add-int/lit8 v8, v3, -0x4

    goto/32 :goto_16

    nop

    :goto_5a
    add-int/lit8 v6, v2, 0x1

    goto/32 :goto_87

    nop

    :goto_5b
    new-instance v4, Lcom/android/framework/protobuf/Utf8$UnpairedSurrogateException;

    goto/32 :goto_27

    nop

    :goto_5c
    add-int/lit8 v6, v1, 0x1

    goto/32 :goto_3e

    nop

    :goto_5d
    or-int/lit16 v7, v7, 0x3c0

    goto/32 :goto_2a

    nop

    :goto_5e
    if-eq v2, v0, :cond_b

    goto/32 :goto_12

    :cond_b
    goto/32 :goto_54

    nop

    :goto_5f
    aput-byte v5, p2, v4

    goto/32 :goto_2f

    nop

    :goto_60
    invoke-static {v5, v4}, Ljava/lang/Character;->isSurrogatePair(CC)Z

    move-result v4

    goto/32 :goto_13

    nop

    :goto_61
    throw v4

    :goto_62
    goto/32 :goto_67

    nop

    :goto_63
    ushr-int/lit8 v9, v6, 0x6

    goto/32 :goto_3a

    nop

    :goto_64
    const-string v7, " at index "

    goto/32 :goto_1

    nop

    :goto_65
    if-lt v5, v6, :cond_c

    goto/32 :goto_7a

    :cond_c
    goto/32 :goto_b

    nop

    :goto_66
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/32 :goto_1f

    nop

    :goto_67
    return v1

    :goto_68
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_64

    nop

    :goto_69
    or-int/lit16 v9, v9, 0xf0

    goto/32 :goto_55

    nop

    :goto_6a
    aput-byte v9, p2, v8

    goto/32 :goto_4a

    nop

    :goto_6b
    int-to-byte v5, v6

    goto/32 :goto_5f

    nop

    :goto_6c
    new-instance v4, Lcom/android/framework/protobuf/Utf8$UnpairedSurrogateException;

    goto/32 :goto_77

    nop

    :goto_6d
    aput-byte v9, p2, v8

    nop

    :goto_6e
    goto/32 :goto_73

    nop

    :goto_6f
    int-to-byte v9, v9

    goto/32 :goto_6d

    nop

    :goto_70
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_56

    nop

    :goto_71
    if-lt v2, v0, :cond_d

    goto/32 :goto_62

    :cond_d
    goto/32 :goto_24

    nop

    :goto_72
    and-int/lit8 v7, v5, 0x3f

    goto/32 :goto_80

    nop

    :goto_73
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_1b

    nop

    :goto_74
    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    goto/32 :goto_58

    nop

    :goto_75
    add-int/lit8 v6, v1, 0x1

    goto/32 :goto_72

    nop

    :goto_76
    ushr-int/lit8 v7, v5, 0xc

    goto/32 :goto_4e

    nop

    :goto_77
    add-int/lit8 v6, v2, -0x1

    goto/32 :goto_25

    nop

    :goto_78
    if-le v1, v6, :cond_e

    goto/32 :goto_7a

    :cond_e
    goto/32 :goto_20

    nop

    :goto_79
    goto :goto_6e

    :goto_7a
    goto/32 :goto_53

    nop

    :goto_7b
    add-int/lit8 v8, v1, 0x1

    goto/32 :goto_a

    nop

    :goto_7c
    and-int/lit8 v7, v5, 0x3f

    goto/32 :goto_4

    nop

    :goto_7d
    int-to-byte v9, v9

    goto/32 :goto_48

    nop

    :goto_7e
    goto/16 :goto_6e

    :goto_7f
    goto/32 :goto_19

    nop

    :goto_80
    or-int/2addr v7, v4

    goto/32 :goto_8c

    nop

    :goto_81
    if-lt v5, v4, :cond_f

    goto/32 :goto_7f

    :cond_f
    goto/32 :goto_2

    nop

    :goto_82
    aput-byte v7, p2, v1

    goto/32 :goto_31

    nop

    :goto_83
    invoke-interface {p1, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    goto/32 :goto_60

    nop

    :goto_84
    if-ne v4, v6, :cond_10

    goto/32 :goto_14

    :cond_10
    goto/32 :goto_4d

    nop

    :goto_85
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_68

    nop

    :goto_86
    move v1, v6

    goto/32 :goto_3b

    nop

    :goto_87
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v7

    goto/32 :goto_0

    nop

    :goto_88
    const v7, 0xd800

    goto/32 :goto_3d

    nop

    :goto_89
    add-int/lit8 v4, v2, 0x1

    goto/32 :goto_52

    nop

    :goto_8a
    throw v4

    :goto_8b
    goto/32 :goto_8f

    nop

    :goto_8c
    int-to-byte v7, v7

    goto/32 :goto_17

    nop

    :goto_8d
    aput-byte v7, p2, v1

    goto/32 :goto_57

    nop

    :goto_8e
    if-lt v5, v4, :cond_11

    goto/32 :goto_46

    :cond_11
    goto/32 :goto_51

    nop

    :goto_8f
    if-le v7, v5, :cond_12

    goto/32 :goto_91

    :cond_12
    goto/32 :goto_21

    nop

    :goto_90
    throw v4

    :goto_91
    goto/32 :goto_74

    nop
.end method

.method encodeUtf8Direct(Ljava/lang/CharSequence;Ljava/nio/ByteBuffer;)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0, p1, p2}, Lcom/android/framework/protobuf/Utf8$SafeProcessor;->encodeUtf8Default(Ljava/lang/CharSequence;Ljava/nio/ByteBuffer;)V

    goto/32 :goto_0

    nop
.end method

.method partialIsValidUtf8(I[BII)I
    .locals 7

    goto/32 :goto_13

    nop

    :goto_0
    move p3, v5

    :goto_1
    goto/32 :goto_62

    nop

    :goto_2
    const/16 v3, -0x41

    goto/32 :goto_58

    nop

    :goto_3
    return p3

    :goto_4
    goto/32 :goto_9

    nop

    :goto_5
    move p3, v1

    goto/32 :goto_56

    nop

    :goto_6
    if-le v4, v3, :cond_0

    goto/32 :goto_1a

    :cond_0
    goto/32 :goto_32

    nop

    :goto_7
    not-int v1, v1

    goto/32 :goto_5c

    nop

    :goto_8
    move p3, v5

    goto/32 :goto_f

    nop

    :goto_9
    move p3, v5

    :goto_a
    goto/32 :goto_17

    nop

    :goto_b
    goto :goto_25

    :goto_c
    goto/32 :goto_4a

    nop

    :goto_d
    goto :goto_1a

    :goto_e
    goto/32 :goto_5f

    nop

    :goto_f
    goto/16 :goto_3f

    :goto_10
    goto/32 :goto_1e

    nop

    :goto_11
    goto :goto_c

    :goto_12
    goto/32 :goto_14

    nop

    :goto_13
    if-nez p1, :cond_1

    goto/32 :goto_25

    :cond_1
    goto/32 :goto_33

    nop

    :goto_14
    move p3, v1

    goto/32 :goto_b

    nop

    :goto_15
    return p1

    :goto_16
    goto/32 :goto_38

    nop

    :goto_17
    if-le v4, v3, :cond_2

    goto/32 :goto_c

    :cond_2
    goto/32 :goto_49

    nop

    :goto_18
    return v0

    :goto_19
    goto :goto_25

    :goto_1a
    goto/32 :goto_24

    nop

    :goto_1b
    if-gt p3, v3, :cond_3

    goto/32 :goto_e

    :cond_3
    goto/32 :goto_2e

    nop

    :goto_1c
    if-eqz v1, :cond_4

    goto/32 :goto_10

    :cond_4
    goto/32 :goto_5d

    nop

    :goto_1d
    aget-byte p3, p2, p3

    goto/32 :goto_30

    nop

    :goto_1e
    shr-int/lit8 v5, p1, 0x10

    goto/32 :goto_3e

    nop

    :goto_1f
    if-eqz v4, :cond_5

    goto/32 :goto_a

    :cond_5
    goto/32 :goto_21

    nop

    :goto_20
    int-to-byte v4, v4

    goto/32 :goto_1f

    nop

    :goto_21
    add-int/lit8 v5, p3, 0x1

    goto/32 :goto_50

    nop

    :goto_22
    const/4 v2, -0x1

    goto/32 :goto_2

    nop

    :goto_23
    aget-byte p3, p2, p3

    goto/32 :goto_1b

    nop

    :goto_24
    return v2

    :goto_25
    goto/32 :goto_35

    nop

    :goto_26
    invoke-static {v0, v1}, Lcom/android/framework/protobuf/Utf8;->access$000(II)I

    move-result p3

    goto/32 :goto_40

    nop

    :goto_27
    add-int/lit8 v1, p3, 0x1

    goto/32 :goto_52

    nop

    :goto_28
    shr-int/lit8 v4, p1, 0x8

    goto/32 :goto_46

    nop

    :goto_29
    aget-byte v4, p2, p3

    goto/32 :goto_55

    nop

    :goto_2a
    const/16 v4, -0x10

    goto/32 :goto_53

    nop

    :goto_2b
    aget-byte v1, p2, p3

    goto/32 :goto_36

    nop

    :goto_2c
    invoke-static {v0, v4}, Lcom/android/framework/protobuf/Utf8;->access$000(II)I

    move-result p3

    goto/32 :goto_3

    nop

    :goto_2d
    if-eqz v4, :cond_6

    goto/32 :goto_1

    :cond_6
    goto/32 :goto_3d

    nop

    :goto_2e
    move p3, v5

    goto/32 :goto_d

    nop

    :goto_2f
    shr-int/lit8 v5, v5, 0x1e

    goto/32 :goto_42

    nop

    :goto_30
    if-gt p3, v3, :cond_7

    goto/32 :goto_12

    :cond_7
    goto/32 :goto_47

    nop

    :goto_31
    const/16 v1, -0x3e

    goto/32 :goto_51

    nop

    :goto_32
    add-int/lit8 v5, p3, 0x1

    goto/32 :goto_23

    nop

    :goto_33
    if-ge p3, p4, :cond_8

    goto/32 :goto_16

    :cond_8
    goto/32 :goto_15

    nop

    :goto_34
    add-int/2addr v5, v6

    goto/32 :goto_2f

    nop

    :goto_35
    invoke-static {p2, p3, p4}, Lcom/android/framework/protobuf/Utf8$SafeProcessor;->partialIsValidUtf8([BII)I

    move-result v0

    goto/32 :goto_18

    nop

    :goto_36
    if-ge v5, p4, :cond_9

    goto/32 :goto_41

    :cond_9
    goto/32 :goto_26

    nop

    :goto_37
    if-ge v5, p4, :cond_a

    goto/32 :goto_4

    :cond_a
    goto/32 :goto_2c

    nop

    :goto_38
    int-to-byte v0, p1

    goto/32 :goto_63

    nop

    :goto_39
    if-eq v0, v1, :cond_b

    goto/32 :goto_4f

    :cond_b
    goto/32 :goto_4e

    nop

    :goto_3a
    if-gt p3, v3, :cond_c

    goto/32 :goto_3c

    :cond_c
    goto/32 :goto_5b

    nop

    :goto_3b
    goto/16 :goto_57

    :goto_3c
    goto/32 :goto_5

    nop

    :goto_3d
    add-int/lit8 v5, p3, 0x1

    goto/32 :goto_29

    nop

    :goto_3e
    int-to-byte v4, v5

    :goto_3f
    goto/32 :goto_2d

    nop

    :goto_40
    return p3

    :goto_41
    goto/32 :goto_8

    nop

    :goto_42
    if-eqz v5, :cond_d

    goto/32 :goto_1a

    :cond_d
    goto/32 :goto_6

    nop

    :goto_43
    add-int/lit8 v6, v1, 0x70

    goto/32 :goto_34

    nop

    :goto_44
    return v2

    :goto_45
    goto/32 :goto_2a

    nop

    :goto_46
    not-int v4, v4

    goto/32 :goto_20

    nop

    :goto_47
    move p3, v1

    goto/32 :goto_11

    nop

    :goto_48
    const/16 v1, -0x13

    goto/32 :goto_5e

    nop

    :goto_49
    const/16 v5, -0x60

    goto/32 :goto_39

    nop

    :goto_4a
    return v2

    :goto_4b
    goto/32 :goto_60

    nop

    :goto_4c
    add-int/lit8 v1, p3, 0x1

    goto/32 :goto_1d

    nop

    :goto_4d
    invoke-static {v0, v1, v4}, Lcom/android/framework/protobuf/Utf8;->access$100(III)I

    move-result p3

    goto/32 :goto_64

    nop

    :goto_4e
    if-ge v4, v5, :cond_e

    goto/32 :goto_c

    :cond_e
    :goto_4f
    goto/32 :goto_48

    nop

    :goto_50
    aget-byte v4, p2, p3

    goto/32 :goto_37

    nop

    :goto_51
    if-ge v0, v1, :cond_f

    goto/32 :goto_57

    :cond_f
    goto/32 :goto_27

    nop

    :goto_52
    aget-byte p3, p2, p3

    goto/32 :goto_3a

    nop

    :goto_53
    if-lt v0, v4, :cond_10

    goto/32 :goto_4b

    :cond_10
    goto/32 :goto_28

    nop

    :goto_54
    const/4 v4, 0x0

    goto/32 :goto_1c

    nop

    :goto_55
    if-ge v5, p4, :cond_11

    goto/32 :goto_65

    :cond_11
    goto/32 :goto_4d

    nop

    :goto_56
    goto/16 :goto_25

    :goto_57
    goto/32 :goto_44

    nop

    :goto_58
    if-lt v0, v1, :cond_12

    goto/32 :goto_45

    :cond_12
    goto/32 :goto_31

    nop

    :goto_59
    if-lt v4, v5, :cond_13

    goto/32 :goto_c

    :cond_13
    :goto_5a
    goto/32 :goto_4c

    nop

    :goto_5b
    move p3, v1

    goto/32 :goto_3b

    nop

    :goto_5c
    int-to-byte v1, v1

    goto/32 :goto_54

    nop

    :goto_5d
    add-int/lit8 v5, p3, 0x1

    goto/32 :goto_2b

    nop

    :goto_5e
    if-eq v0, v1, :cond_14

    goto/32 :goto_5a

    :cond_14
    goto/32 :goto_59

    nop

    :goto_5f
    move p3, v5

    goto/32 :goto_19

    nop

    :goto_60
    shr-int/lit8 v1, p1, 0x8

    goto/32 :goto_7

    nop

    :goto_61
    shl-int/lit8 v5, v0, 0x1c

    goto/32 :goto_43

    nop

    :goto_62
    if-le v1, v3, :cond_15

    goto/32 :goto_1a

    :cond_15
    goto/32 :goto_61

    nop

    :goto_63
    const/16 v1, -0x20

    goto/32 :goto_22

    nop

    :goto_64
    return p3

    :goto_65
    goto/32 :goto_0

    nop
.end method

.method partialIsValidUtf8Direct(ILjava/nio/ByteBuffer;II)I
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/framework/protobuf/Utf8$SafeProcessor;->partialIsValidUtf8Default(ILjava/nio/ByteBuffer;II)I

    move-result v0

    goto/32 :goto_0

    nop
.end method
