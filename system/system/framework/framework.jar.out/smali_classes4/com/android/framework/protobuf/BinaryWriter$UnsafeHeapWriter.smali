.class final Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;
.super Lcom/android/framework/protobuf/BinaryWriter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/framework/protobuf/BinaryWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "UnsafeHeapWriter"
.end annotation


# instance fields
.field private allocatedBuffer:Lcom/android/framework/protobuf/AllocatedBuffer;

.field private buffer:[B

.field private limit:J

.field private limitMinusOne:J

.field private offset:J

.field private offsetMinusOne:J

.field private pos:J


# direct methods
.method constructor <init>(Lcom/android/framework/protobuf/BufferAllocator;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/framework/protobuf/BinaryWriter;-><init>(Lcom/android/framework/protobuf/BufferAllocator;ILcom/android/framework/protobuf/BinaryWriter$1;)V

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->nextBuffer()V

    return-void
.end method

.method private arrayPos()I
    .locals 2

    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    long-to-int v0, v0

    return v0
.end method

.method static isSupported()Z
    .locals 1

    invoke-static {}, Lcom/android/framework/protobuf/UnsafeUtil;->hasUnsafeArrayOperations()Z

    move-result v0

    return v0
.end method

.method private nextBuffer()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->newHeapBuffer()Lcom/android/framework/protobuf/AllocatedBuffer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->nextBuffer(Lcom/android/framework/protobuf/AllocatedBuffer;)V

    return-void
.end method

.method private nextBuffer(I)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->newHeapBuffer(I)Lcom/android/framework/protobuf/AllocatedBuffer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->nextBuffer(Lcom/android/framework/protobuf/AllocatedBuffer;)V

    return-void
.end method

.method private nextBuffer(Lcom/android/framework/protobuf/AllocatedBuffer;)V
    .locals 5

    invoke-virtual {p1}, Lcom/android/framework/protobuf/AllocatedBuffer;->hasArray()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->finishCurrentBuffer()V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffers:Ljava/util/ArrayDeque;

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->addFirst(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->allocatedBuffer:Lcom/android/framework/protobuf/AllocatedBuffer;

    invoke-virtual {p1}, Lcom/android/framework/protobuf/AllocatedBuffer;->array()[B

    move-result-object v0

    iput-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    invoke-virtual {p1}, Lcom/android/framework/protobuf/AllocatedBuffer;->arrayOffset()I

    move-result v0

    invoke-virtual {p1}, Lcom/android/framework/protobuf/AllocatedBuffer;->limit()I

    move-result v1

    add-int/2addr v1, v0

    int-to-long v1, v1

    iput-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->limit:J

    invoke-virtual {p1}, Lcom/android/framework/protobuf/AllocatedBuffer;->position()I

    move-result v1

    add-int/2addr v1, v0

    int-to-long v1, v1

    iput-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->offset:J

    const-wide/16 v3, 0x1

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->offsetMinusOne:J

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->limit:J

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->limitMinusOne:J

    iput-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    return-void

    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Allocator returned non-heap buffer"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private writeVarint32FiveBytes(I)V
    .locals 7

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const-wide/16 v3, 0x1

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    ushr-int/lit8 v5, p1, 0x1c

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    ushr-int/lit8 v5, p1, 0x15

    and-int/lit8 v5, v5, 0x7f

    or-int/lit16 v5, v5, 0x80

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    ushr-int/lit8 v5, p1, 0xe

    and-int/lit8 v5, v5, 0x7f

    or-int/lit16 v5, v5, 0x80

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    ushr-int/lit8 v5, p1, 0x7

    and-int/lit8 v5, v5, 0x7f

    or-int/lit16 v5, v5, 0x80

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v3, v1, v3

    iput-wide v3, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    and-int/lit8 v3, p1, 0x7f

    or-int/lit16 v3, v3, 0x80

    int-to-byte v3, v3

    invoke-static {v0, v1, v2, v3}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    return-void
.end method

.method private writeVarint32FourBytes(I)V
    .locals 7

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const-wide/16 v3, 0x1

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    ushr-int/lit8 v5, p1, 0x15

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    ushr-int/lit8 v5, p1, 0xe

    and-int/lit8 v5, v5, 0x7f

    or-int/lit16 v5, v5, 0x80

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    ushr-int/lit8 v5, p1, 0x7

    and-int/lit8 v5, v5, 0x7f

    or-int/lit16 v5, v5, 0x80

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v3, v1, v3

    iput-wide v3, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    and-int/lit8 v3, p1, 0x7f

    or-int/lit16 v3, v3, 0x80

    int-to-byte v3, v3

    invoke-static {v0, v1, v2, v3}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    return-void
.end method

.method private writeVarint32OneByte(I)V
    .locals 5

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const-wide/16 v3, 0x1

    sub-long v3, v1, v3

    iput-wide v3, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    int-to-byte v3, p1

    invoke-static {v0, v1, v2, v3}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    return-void
.end method

.method private writeVarint32ThreeBytes(I)V
    .locals 7

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const-wide/16 v3, 0x1

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    ushr-int/lit8 v5, p1, 0xe

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    ushr-int/lit8 v5, p1, 0x7

    and-int/lit8 v5, v5, 0x7f

    or-int/lit16 v5, v5, 0x80

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v3, v1, v3

    iput-wide v3, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    and-int/lit8 v3, p1, 0x7f

    or-int/lit16 v3, v3, 0x80

    int-to-byte v3, v3

    invoke-static {v0, v1, v2, v3}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    return-void
.end method

.method private writeVarint32TwoBytes(I)V
    .locals 7

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const-wide/16 v3, 0x1

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    ushr-int/lit8 v5, p1, 0x7

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v3, v1, v3

    iput-wide v3, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    and-int/lit8 v3, p1, 0x7f

    or-int/lit16 v3, v3, 0x80

    int-to-byte v3, v3

    invoke-static {v0, v1, v2, v3}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    return-void
.end method

.method private writeVarint64EightBytes(J)V
    .locals 11

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const-wide/16 v3, 0x1

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x31

    ushr-long v5, p1, v5

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x2a

    ushr-long v5, p1, v5

    const-wide/16 v7, 0x7f

    and-long/2addr v5, v7

    const-wide/16 v9, 0x80

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x23

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x1c

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x15

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0xe

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/4 v5, 0x7

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v3, v1, v3

    iput-wide v3, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    and-long v3, p1, v7

    or-long/2addr v3, v9

    long-to-int v3, v3

    int-to-byte v3, v3

    invoke-static {v0, v1, v2, v3}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    return-void
.end method

.method private writeVarint64FiveBytes(J)V
    .locals 11

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const-wide/16 v3, 0x1

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x1c

    ushr-long v5, p1, v5

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x15

    ushr-long v5, p1, v5

    const-wide/16 v7, 0x7f

    and-long/2addr v5, v7

    const-wide/16 v9, 0x80

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0xe

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/4 v5, 0x7

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v3, v1, v3

    iput-wide v3, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    and-long v3, p1, v7

    or-long/2addr v3, v9

    long-to-int v3, v3

    int-to-byte v3, v3

    invoke-static {v0, v1, v2, v3}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    return-void
.end method

.method private writeVarint64FourBytes(J)V
    .locals 11

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const-wide/16 v3, 0x1

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x15

    ushr-long v5, p1, v5

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0xe

    ushr-long v5, p1, v5

    const-wide/16 v7, 0x7f

    and-long/2addr v5, v7

    const-wide/16 v9, 0x80

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/4 v5, 0x7

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v3, v1, v3

    iput-wide v3, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    and-long v3, p1, v7

    or-long/2addr v3, v9

    long-to-int v3, v3

    int-to-byte v3, v3

    invoke-static {v0, v1, v2, v3}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    return-void
.end method

.method private writeVarint64NineBytes(J)V
    .locals 11

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const-wide/16 v3, 0x1

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x38

    ushr-long v5, p1, v5

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x31

    ushr-long v5, p1, v5

    const-wide/16 v7, 0x7f

    and-long/2addr v5, v7

    const-wide/16 v9, 0x80

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x2a

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x23

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x1c

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x15

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0xe

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/4 v5, 0x7

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v3, v1, v3

    iput-wide v3, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    and-long v3, p1, v7

    or-long/2addr v3, v9

    long-to-int v3, v3

    int-to-byte v3, v3

    invoke-static {v0, v1, v2, v3}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    return-void
.end method

.method private writeVarint64OneByte(J)V
    .locals 5

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const-wide/16 v3, 0x1

    sub-long v3, v1, v3

    iput-wide v3, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    long-to-int v3, p1

    int-to-byte v3, v3

    invoke-static {v0, v1, v2, v3}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    return-void
.end method

.method private writeVarint64SevenBytes(J)V
    .locals 11

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const-wide/16 v3, 0x1

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x2a

    ushr-long v5, p1, v5

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x23

    ushr-long v5, p1, v5

    const-wide/16 v7, 0x7f

    and-long/2addr v5, v7

    const-wide/16 v9, 0x80

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x1c

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x15

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0xe

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/4 v5, 0x7

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v3, v1, v3

    iput-wide v3, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    and-long v3, p1, v7

    or-long/2addr v3, v9

    long-to-int v3, v3

    int-to-byte v3, v3

    invoke-static {v0, v1, v2, v3}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    return-void
.end method

.method private writeVarint64SixBytes(J)V
    .locals 11

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const-wide/16 v3, 0x1

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x23

    ushr-long v5, p1, v5

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x1c

    ushr-long v5, p1, v5

    const-wide/16 v7, 0x7f

    and-long/2addr v5, v7

    const-wide/16 v9, 0x80

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x15

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0xe

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/4 v5, 0x7

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v3, v1, v3

    iput-wide v3, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    and-long v3, p1, v7

    or-long/2addr v3, v9

    long-to-int v3, v3

    int-to-byte v3, v3

    invoke-static {v0, v1, v2, v3}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    return-void
.end method

.method private writeVarint64TenBytes(J)V
    .locals 11

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const-wide/16 v3, 0x1

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x3f

    ushr-long v5, p1, v5

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x38

    ushr-long v5, p1, v5

    const-wide/16 v7, 0x7f

    and-long/2addr v5, v7

    const-wide/16 v9, 0x80

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x31

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x2a

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x23

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x1c

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0x15

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/16 v5, 0xe

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/4 v5, 0x7

    ushr-long v5, p1, v5

    and-long/2addr v5, v7

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v3, v1, v3

    iput-wide v3, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    and-long v3, p1, v7

    or-long/2addr v3, v9

    long-to-int v3, v3

    int-to-byte v3, v3

    invoke-static {v0, v1, v2, v3}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    return-void
.end method

.method private writeVarint64ThreeBytes(J)V
    .locals 11

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const-wide/16 v3, 0x1

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    long-to-int v5, p1

    ushr-int/lit8 v5, v5, 0xe

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/4 v5, 0x7

    ushr-long v5, p1, v5

    const-wide/16 v7, 0x7f

    and-long/2addr v5, v7

    const-wide/16 v9, 0x80

    or-long/2addr v5, v9

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v3, v1, v3

    iput-wide v3, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    and-long v3, p1, v7

    or-long/2addr v3, v9

    long-to-int v3, v3

    int-to-byte v3, v3

    invoke-static {v0, v1, v2, v3}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    return-void
.end method

.method private writeVarint64TwoBytes(J)V
    .locals 7

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const-wide/16 v3, 0x1

    sub-long v5, v1, v3

    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const/4 v5, 0x7

    ushr-long v5, p1, v5

    long-to-int v5, v5

    int-to-byte v5, v5

    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    sub-long v3, v1, v3

    iput-wide v3, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    long-to-int v3, p1

    and-int/lit8 v3, v3, 0x7f

    or-int/lit16 v3, v3, 0x80

    int-to-byte v3, v3

    invoke-static {v0, v1, v2, v3}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    return-void
.end method


# virtual methods
.method bytesWrittenToCurrentBuffer()I
    .locals 4

    goto/32 :goto_1

    nop

    :goto_0
    iget-wide v2, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_4

    nop

    :goto_1
    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->limitMinusOne:J

    goto/32 :goto_0

    nop

    :goto_2
    long-to-int v0, v0

    goto/32 :goto_3

    nop

    :goto_3
    return v0

    :goto_4
    sub-long/2addr v0, v2

    goto/32 :goto_2

    nop
.end method

.method finishCurrentBuffer()V
    .locals 3

    goto/32 :goto_a

    nop

    :goto_0
    return-void

    :goto_1
    iput-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->allocatedBuffer:Lcom/android/framework/protobuf/AllocatedBuffer;

    goto/32 :goto_11

    nop

    :goto_2
    const/4 v0, 0x0

    goto/32 :goto_1

    nop

    :goto_3
    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->arrayPos()I

    move-result v1

    goto/32 :goto_c

    nop

    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_12

    nop

    :goto_5
    iput-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->limitMinusOne:J

    :goto_6
    goto/32 :goto_0

    nop

    :goto_7
    sub-int/2addr v1, v2

    goto/32 :goto_4

    nop

    :goto_8
    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->bytesWrittenToCurrentBuffer()I

    move-result v1

    goto/32 :goto_13

    nop

    :goto_9
    invoke-virtual {v2}, Lcom/android/framework/protobuf/AllocatedBuffer;->arrayOffset()I

    move-result v2

    goto/32 :goto_7

    nop

    :goto_a
    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->allocatedBuffer:Lcom/android/framework/protobuf/AllocatedBuffer;

    goto/32 :goto_10

    nop

    :goto_b
    iget v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->totalDoneBytes:I

    goto/32 :goto_8

    nop

    :goto_c
    iget-object v2, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->allocatedBuffer:Lcom/android/framework/protobuf/AllocatedBuffer;

    goto/32 :goto_9

    nop

    :goto_d
    iput v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->totalDoneBytes:I

    goto/32 :goto_e

    nop

    :goto_e
    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->allocatedBuffer:Lcom/android/framework/protobuf/AllocatedBuffer;

    goto/32 :goto_3

    nop

    :goto_f
    iput-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_5

    nop

    :goto_10
    if-nez v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_b

    nop

    :goto_11
    const-wide/16 v0, 0x0

    goto/32 :goto_f

    nop

    :goto_12
    invoke-virtual {v0, v1}, Lcom/android/framework/protobuf/AllocatedBuffer;->position(I)Lcom/android/framework/protobuf/AllocatedBuffer;

    goto/32 :goto_2

    nop

    :goto_13
    add-int/2addr v0, v1

    goto/32 :goto_d

    nop
.end method

.method public getTotalBytesWritten()I
    .locals 2

    iget v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->totalDoneBytes:I

    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->bytesWrittenToCurrentBuffer()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method requireSpace(I)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->spaceLeft()I

    move-result v0

    goto/32 :goto_1

    nop

    :goto_1
    if-lt v0, p1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_3

    nop

    :goto_2
    return-void

    :goto_3
    invoke-direct {p0, p1}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->nextBuffer(I)V

    :goto_4
    goto/32 :goto_2

    nop
.end method

.method spaceLeft()I
    .locals 4

    goto/32 :goto_4

    nop

    :goto_0
    long-to-int v0, v0

    goto/32 :goto_1

    nop

    :goto_1
    return v0

    :goto_2
    iget-wide v2, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->offsetMinusOne:J

    goto/32 :goto_3

    nop

    :goto_3
    sub-long/2addr v0, v2

    goto/32 :goto_0

    nop

    :goto_4
    iget-wide v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_2

    nop
.end method

.method public write(B)V
    .locals 5

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    const-wide/16 v3, 0x1

    sub-long v3, v1, v3

    iput-wide v3, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    invoke-static {v0, v1, v2, p1}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    return-void
.end method

.method public write(Ljava/nio/ByteBuffer;)V
    .locals 5

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->requireSpace(I)V

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    int-to-long v3, v0

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->arrayPos()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p1, v1, v2, v0}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    return-void
.end method

.method public write([BII)V
    .locals 5

    const/4 v0, 0x1

    if-ltz p2, :cond_0

    add-int v1, p2, p3

    array-length v2, p1

    if-gt v1, v2, :cond_0

    invoke-virtual {p0, p3}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->requireSpace(I)V

    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    int-to-long v3, p3

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->arrayPos()I

    move-result v2

    add-int/2addr v2, v0

    invoke-static {p1, p2, v1, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void

    :cond_0
    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    array-length v4, p1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const-string/jumbo v0, "value.length=%d, offset=%d, length=%d"

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public writeBool(IZ)V
    .locals 1

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->requireSpace(I)V

    int-to-byte v0, p2

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->write(B)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeTag(II)V

    return-void
.end method

.method writeBool(Z)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    int-to-byte v0, p1

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->write(B)V

    goto/32 :goto_0

    nop
.end method

.method public writeBytes(ILcom/android/framework/protobuf/ByteString;)V
    .locals 2

    :try_start_0
    invoke-virtual {p2, p0}, Lcom/android/framework/protobuf/ByteString;->writeToReverse(Lcom/android/framework/protobuf/ByteOutput;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    nop

    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->requireSpace(I)V

    invoke-virtual {p2}, Lcom/android/framework/protobuf/ByteString;->size()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeVarint32(I)V

    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeTag(II)V

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public writeEndGroup(I)V
    .locals 1

    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeTag(II)V

    return-void
.end method

.method writeFixed32(I)V
    .locals 7

    goto/32 :goto_10

    nop

    :goto_0
    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_12

    nop

    :goto_1
    shr-int/lit8 v5, p1, 0x8

    goto/32 :goto_1b

    nop

    :goto_2
    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_3

    nop

    :goto_3
    sub-long v5, v1, v3

    goto/32 :goto_15

    nop

    :goto_4
    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    goto/32 :goto_2

    nop

    :goto_5
    int-to-byte v5, v5

    goto/32 :goto_1e

    nop

    :goto_6
    return-void

    :goto_7
    shr-int/lit8 v5, p1, 0x10

    goto/32 :goto_a

    nop

    :goto_8
    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_1

    nop

    :goto_9
    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_1a

    nop

    :goto_a
    and-int/lit16 v5, v5, 0xff

    goto/32 :goto_b

    nop

    :goto_b
    int-to-byte v5, v5

    goto/32 :goto_9

    nop

    :goto_c
    sub-long v5, v1, v3

    goto/32 :goto_1c

    nop

    :goto_d
    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    goto/32 :goto_1d

    nop

    :goto_e
    int-to-byte v3, v3

    goto/32 :goto_11

    nop

    :goto_f
    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_4

    nop

    :goto_10
    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    goto/32 :goto_13

    nop

    :goto_11
    invoke-static {v0, v1, v2, v3}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_6

    nop

    :goto_12
    sub-long v5, v1, v3

    goto/32 :goto_8

    nop

    :goto_13
    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_14

    nop

    :goto_14
    const-wide/16 v3, 0x1

    goto/32 :goto_c

    nop

    :goto_15
    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_7

    nop

    :goto_16
    shr-int/lit8 v5, p1, 0x18

    goto/32 :goto_18

    nop

    :goto_17
    sub-long v3, v1, v3

    goto/32 :goto_19

    nop

    :goto_18
    and-int/lit16 v5, v5, 0xff

    goto/32 :goto_1f

    nop

    :goto_19
    iput-wide v3, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_20

    nop

    :goto_1a
    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    goto/32 :goto_0

    nop

    :goto_1b
    and-int/lit16 v5, v5, 0xff

    goto/32 :goto_5

    nop

    :goto_1c
    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_16

    nop

    :goto_1d
    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_17

    nop

    :goto_1e
    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_d

    nop

    :goto_1f
    int-to-byte v5, v5

    goto/32 :goto_f

    nop

    :goto_20
    and-int/lit16 v3, p1, 0xff

    goto/32 :goto_e

    nop
.end method

.method public writeFixed32(II)V
    .locals 1

    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->requireSpace(I)V

    invoke-virtual {p0, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeFixed32(I)V

    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeTag(II)V

    return-void
.end method

.method public writeFixed64(IJ)V
    .locals 1

    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->requireSpace(I)V

    invoke-virtual {p0, p2, p3}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeFixed64(J)V

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeTag(II)V

    return-void
.end method

.method writeFixed64(J)V
    .locals 7

    goto/32 :goto_1e

    nop

    :goto_0
    const-wide/16 v3, 0x1

    goto/32 :goto_34

    nop

    :goto_1
    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_47

    nop

    :goto_2
    int-to-byte v5, v5

    goto/32 :goto_40

    nop

    :goto_3
    shr-long v5, p1, v5

    goto/32 :goto_30

    nop

    :goto_4
    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_4d

    nop

    :goto_5
    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_19

    nop

    :goto_6
    long-to-int v5, v5

    goto/32 :goto_1a

    nop

    :goto_7
    and-int/lit16 v5, v5, 0xff

    goto/32 :goto_36

    nop

    :goto_8
    and-int/lit16 v5, v5, 0xff

    goto/32 :goto_1f

    nop

    :goto_9
    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_f

    nop

    :goto_a
    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_35

    nop

    :goto_b
    sub-long v5, v1, v3

    goto/32 :goto_1c

    nop

    :goto_c
    sub-long v3, v1, v3

    goto/32 :goto_e

    nop

    :goto_d
    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_21

    nop

    :goto_e
    iput-wide v3, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_37

    nop

    :goto_f
    const/16 v5, 0x38

    goto/32 :goto_3f

    nop

    :goto_10
    return-void

    :goto_11
    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    goto/32 :goto_2c

    nop

    :goto_12
    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    goto/32 :goto_d

    nop

    :goto_13
    and-int/lit16 v3, v3, 0xff

    goto/32 :goto_4a

    nop

    :goto_14
    int-to-byte v5, v5

    goto/32 :goto_33

    nop

    :goto_15
    and-int/lit16 v5, v5, 0xff

    goto/32 :goto_48

    nop

    :goto_16
    const/16 v5, 0x18

    goto/32 :goto_4f

    nop

    :goto_17
    and-int/lit16 v5, v5, 0xff

    goto/32 :goto_2

    nop

    :goto_18
    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_11

    nop

    :goto_19
    sub-long v5, v1, v3

    goto/32 :goto_28

    nop

    :goto_1a
    and-int/lit16 v5, v5, 0xff

    goto/32 :goto_14

    nop

    :goto_1b
    int-to-byte v5, v5

    goto/32 :goto_18

    nop

    :goto_1c
    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_42

    nop

    :goto_1d
    const/16 v5, 0x8

    goto/32 :goto_20

    nop

    :goto_1e
    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    goto/32 :goto_3d

    nop

    :goto_1f
    int-to-byte v5, v5

    goto/32 :goto_1

    nop

    :goto_20
    shr-long v5, p1, v5

    goto/32 :goto_3c

    nop

    :goto_21
    sub-long v5, v1, v3

    goto/32 :goto_2b

    nop

    :goto_22
    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_24

    nop

    :goto_23
    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_c

    nop

    :goto_24
    sub-long v5, v1, v3

    goto/32 :goto_45

    nop

    :goto_25
    shr-long v5, p1, v5

    goto/32 :goto_31

    nop

    :goto_26
    sub-long v5, v1, v3

    goto/32 :goto_a

    nop

    :goto_27
    shr-long v5, p1, v5

    goto/32 :goto_49

    nop

    :goto_28
    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_29

    nop

    :goto_29
    const/16 v5, 0x10

    goto/32 :goto_41

    nop

    :goto_2a
    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_2f

    nop

    :goto_2b
    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_16

    nop

    :goto_2c
    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_26

    nop

    :goto_2d
    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_2e

    nop

    :goto_2e
    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    goto/32 :goto_4

    nop

    :goto_2f
    const/16 v5, 0x30

    goto/32 :goto_3

    nop

    :goto_30
    long-to-int v5, v5

    goto/32 :goto_8

    nop

    :goto_31
    long-to-int v5, v5

    goto/32 :goto_17

    nop

    :goto_32
    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    goto/32 :goto_5

    nop

    :goto_33
    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_32

    nop

    :goto_34
    sub-long v5, v1, v3

    goto/32 :goto_9

    nop

    :goto_35
    const/16 v5, 0x20

    goto/32 :goto_25

    nop

    :goto_36
    int-to-byte v5, v5

    goto/32 :goto_46

    nop

    :goto_37
    long-to-int v3, p1

    goto/32 :goto_13

    nop

    :goto_38
    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_3a

    nop

    :goto_39
    long-to-int v5, v5

    goto/32 :goto_7

    nop

    :goto_3a
    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    goto/32 :goto_23

    nop

    :goto_3b
    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    goto/32 :goto_22

    nop

    :goto_3c
    long-to-int v5, v5

    goto/32 :goto_15

    nop

    :goto_3d
    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_0

    nop

    :goto_3e
    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_b

    nop

    :goto_3f
    shr-long v5, p1, v5

    goto/32 :goto_44

    nop

    :goto_40
    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_12

    nop

    :goto_41
    shr-long v5, p1, v5

    goto/32 :goto_39

    nop

    :goto_42
    const/16 v5, 0x28

    goto/32 :goto_27

    nop

    :goto_43
    int-to-byte v5, v5

    goto/32 :goto_2d

    nop

    :goto_44
    long-to-int v5, v5

    goto/32 :goto_4e

    nop

    :goto_45
    iput-wide v5, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_1d

    nop

    :goto_46
    invoke-static {v0, v1, v2, v5}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_3b

    nop

    :goto_47
    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    goto/32 :goto_3e

    nop

    :goto_48
    int-to-byte v5, v5

    goto/32 :goto_38

    nop

    :goto_49
    long-to-int v5, v5

    goto/32 :goto_4c

    nop

    :goto_4a
    int-to-byte v3, v3

    goto/32 :goto_4b

    nop

    :goto_4b
    invoke-static {v0, v1, v2, v3}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_10

    nop

    :goto_4c
    and-int/lit16 v5, v5, 0xff

    goto/32 :goto_1b

    nop

    :goto_4d
    sub-long v5, v1, v3

    goto/32 :goto_2a

    nop

    :goto_4e
    and-int/lit16 v5, v5, 0xff

    goto/32 :goto_43

    nop

    :goto_4f
    shr-long v5, p1, v5

    goto/32 :goto_6

    nop
.end method

.method public writeGroup(ILjava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeTag(II)V

    invoke-static {}, Lcom/android/framework/protobuf/Protobuf;->getInstance()Lcom/android/framework/protobuf/Protobuf;

    move-result-object v0

    invoke-virtual {v0, p2, p0}, Lcom/android/framework/protobuf/Protobuf;->writeTo(Ljava/lang/Object;Lcom/android/framework/protobuf/Writer;)V

    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeTag(II)V

    return-void
.end method

.method public writeGroup(ILjava/lang/Object;Lcom/android/framework/protobuf/Schema;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeTag(II)V

    invoke-interface {p3, p2, p0}, Lcom/android/framework/protobuf/Schema;->writeTo(Ljava/lang/Object;Lcom/android/framework/protobuf/Writer;)V

    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeTag(II)V

    return-void
.end method

.method writeInt32(I)V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    goto :goto_7

    :goto_1
    goto/32 :goto_5

    nop

    :goto_2
    if-gez p1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {p0, p1}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeVarint32(I)V

    goto/32 :goto_0

    nop

    :goto_4
    return-void

    :goto_5
    int-to-long v0, p1

    goto/32 :goto_6

    nop

    :goto_6
    invoke-virtual {p0, v0, v1}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeVarint64(J)V

    :goto_7
    goto/32 :goto_4

    nop
.end method

.method public writeInt32(II)V
    .locals 1

    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->requireSpace(I)V

    invoke-virtual {p0, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeInt32(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeTag(II)V

    return-void
.end method

.method public writeLazy(Ljava/nio/ByteBuffer;)V
    .locals 5

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->spaceLeft()I

    move-result v1

    if-ge v1, v0, :cond_0

    iget v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->totalDoneBytes:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->totalDoneBytes:I

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffers:Ljava/util/ArrayDeque;

    invoke-static {p1}, Lcom/android/framework/protobuf/AllocatedBuffer;->wrap(Ljava/nio/ByteBuffer;)Lcom/android/framework/protobuf/AllocatedBuffer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayDeque;->addFirst(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->nextBuffer()V

    :cond_0
    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    int-to-long v3, v0

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->arrayPos()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p1, v1, v2, v0}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    return-void
.end method

.method public writeLazy([BII)V
    .locals 5

    const/4 v0, 0x1

    if-ltz p2, :cond_1

    add-int v1, p2, p3

    array-length v2, p1

    if-gt v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->spaceLeft()I

    move-result v1

    if-ge v1, p3, :cond_0

    iget v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->totalDoneBytes:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->totalDoneBytes:I

    iget-object v0, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffers:Ljava/util/ArrayDeque;

    invoke-static {p1, p2, p3}, Lcom/android/framework/protobuf/AllocatedBuffer;->wrap([BII)Lcom/android/framework/protobuf/AllocatedBuffer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayDeque;->addFirst(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->nextBuffer()V

    return-void

    :cond_0
    iget-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    int-to-long v3, p3

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    invoke-direct {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->arrayPos()I

    move-result v2

    add-int/2addr v2, v0

    invoke-static {p1, p2, v1, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void

    :cond_1
    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    array-length v4, p1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const-string/jumbo v0, "value.length=%d, offset=%d, length=%d"

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public writeMessage(ILjava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->getTotalBytesWritten()I

    move-result v0

    invoke-static {}, Lcom/android/framework/protobuf/Protobuf;->getInstance()Lcom/android/framework/protobuf/Protobuf;

    move-result-object v1

    invoke-virtual {v1, p2, p0}, Lcom/android/framework/protobuf/Protobuf;->writeTo(Ljava/lang/Object;Lcom/android/framework/protobuf/Writer;)V

    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->getTotalBytesWritten()I

    move-result v1

    sub-int/2addr v1, v0

    const/16 v2, 0xa

    invoke-virtual {p0, v2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->requireSpace(I)V

    invoke-virtual {p0, v1}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeVarint32(I)V

    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeTag(II)V

    return-void
.end method

.method public writeMessage(ILjava/lang/Object;Lcom/android/framework/protobuf/Schema;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->getTotalBytesWritten()I

    move-result v0

    invoke-interface {p3, p2, p0}, Lcom/android/framework/protobuf/Schema;->writeTo(Ljava/lang/Object;Lcom/android/framework/protobuf/Writer;)V

    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->getTotalBytesWritten()I

    move-result v1

    sub-int/2addr v1, v0

    const/16 v2, 0xa

    invoke-virtual {p0, v2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->requireSpace(I)V

    invoke-virtual {p0, v1}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeVarint32(I)V

    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeTag(II)V

    return-void
.end method

.method writeSInt32(I)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-static {p1}, Lcom/android/framework/protobuf/CodedOutputStream;->encodeZigZag32(I)I

    move-result v0

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeVarint32(I)V

    goto/32 :goto_0

    nop
.end method

.method public writeSInt32(II)V
    .locals 1

    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->requireSpace(I)V

    invoke-virtual {p0, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeSInt32(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeTag(II)V

    return-void
.end method

.method public writeSInt64(IJ)V
    .locals 1

    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->requireSpace(I)V

    invoke-virtual {p0, p2, p3}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeSInt64(J)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeTag(II)V

    return-void
.end method

.method writeSInt64(J)V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0, v0, v1}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeVarint64(J)V

    goto/32 :goto_0

    nop

    :goto_2
    invoke-static {p1, p2}, Lcom/android/framework/protobuf/CodedOutputStream;->encodeZigZag64(J)J

    move-result-wide v0

    goto/32 :goto_1

    nop
.end method

.method public writeStartGroup(I)V
    .locals 1

    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeTag(II)V

    return-void
.end method

.method public writeString(ILjava/lang/String;)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->getTotalBytesWritten()I

    move-result v0

    invoke-virtual {p0, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeString(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->getTotalBytesWritten()I

    move-result v1

    sub-int/2addr v1, v0

    const/16 v2, 0xa

    invoke-virtual {p0, v2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->requireSpace(I)V

    invoke-virtual {p0, v1}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeVarint32(I)V

    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeTag(II)V

    return-void
.end method

.method writeString(Ljava/lang/String;)V
    .locals 13

    goto/32 :goto_79

    nop

    :goto_0
    add-int/lit8 v0, v0, -0x1

    :goto_1
    goto/32 :goto_5e

    nop

    :goto_2
    iget-wide v7, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_26

    nop

    :goto_3
    int-to-byte v9, v9

    goto/32 :goto_76

    nop

    :goto_4
    sub-long v9, v7, v2

    goto/32 :goto_40

    nop

    :goto_5
    move v5, v4

    goto/32 :goto_41

    nop

    :goto_6
    if-ge v5, v6, :cond_0

    goto/32 :goto_54

    :cond_0
    goto/32 :goto_34

    nop

    :goto_7
    iget-object v8, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    goto/32 :goto_4f

    nop

    :goto_8
    cmp-long v6, v6, v8

    goto/32 :goto_63

    nop

    :goto_9
    iget-wide v8, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->offsetMinusOne:J

    goto/32 :goto_84

    nop

    :goto_a
    invoke-static {v8, v6, v7, v9}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_64

    nop

    :goto_b
    or-int/2addr v11, v1

    goto/32 :goto_7c

    nop

    :goto_c
    iput-wide v9, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_71

    nop

    :goto_d
    iput-wide v2, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_3c

    nop

    :goto_e
    sub-long v11, v9, v2

    goto/32 :goto_45

    nop

    :goto_f
    const-wide/16 v2, 0x1

    goto/32 :goto_25

    nop

    :goto_10
    or-int/lit16 v9, v9, 0x3c0

    goto/32 :goto_42

    nop

    :goto_11
    if-gtz v8, :cond_1

    goto/32 :goto_97

    :cond_1
    goto/32 :goto_48

    nop

    :goto_12
    if-eq v0, v4, :cond_2

    goto/32 :goto_24

    :cond_2
    goto/32 :goto_23

    nop

    :goto_13
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_3a

    nop

    :goto_14
    sub-long v11, v9, v2

    goto/32 :goto_6d

    nop

    :goto_15
    ushr-int/lit8 v11, v6, 0x12

    goto/32 :goto_4c

    nop

    :goto_16
    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    goto/32 :goto_35

    nop

    :goto_17
    iget-object v8, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    goto/32 :goto_9c

    nop

    :goto_18
    int-to-byte v9, v9

    goto/32 :goto_66

    nop

    :goto_19
    invoke-static {v8, v9, v10, v11}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_17

    nop

    :goto_1a
    iget-wide v7, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_4

    nop

    :goto_1b
    add-int/lit8 v0, v0, 0x1

    :goto_1c
    goto/32 :goto_73

    nop

    :goto_1d
    iput-wide v9, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_69

    nop

    :goto_1e
    cmp-long v8, v6, v8

    goto/32 :goto_81

    nop

    :goto_1f
    sub-long v11, v9, v2

    goto/32 :goto_95

    nop

    :goto_20
    goto/16 :goto_24

    :goto_21
    goto/32 :goto_6c

    nop

    :goto_22
    const v6, 0xd800

    goto/32 :goto_6

    nop

    :goto_23
    return-void

    :goto_24
    goto/32 :goto_5d

    nop

    :goto_25
    if-gez v0, :cond_3

    goto/32 :goto_3b

    :cond_3
    goto/32 :goto_29

    nop

    :goto_26
    sub-long v9, v7, v2

    goto/32 :goto_2b

    nop

    :goto_27
    or-int/2addr v9, v1

    goto/32 :goto_18

    nop

    :goto_28
    int-to-byte v11, v11

    goto/32 :goto_4d

    nop

    :goto_29
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    goto/32 :goto_5

    nop

    :goto_2a
    iget-object v6, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    goto/32 :goto_1a

    nop

    :goto_2b
    iput-wide v9, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_31

    nop

    :goto_2c
    add-long/2addr v8, v2

    goto/32 :goto_6a

    nop

    :goto_2d
    iget-wide v9, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_4b

    nop

    :goto_2e
    iget-object v6, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    goto/32 :goto_2

    nop

    :goto_2f
    iput-wide v9, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_51

    nop

    :goto_30
    iget-wide v6, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_99

    nop

    :goto_31
    ushr-int/lit8 v9, v5, 0x6

    goto/32 :goto_86

    nop

    :goto_32
    and-int/lit8 v11, v11, 0x3f

    goto/32 :goto_b

    nop

    :goto_33
    ushr-int/lit8 v11, v6, 0xc

    goto/32 :goto_32

    nop

    :goto_34
    const v6, 0xdfff

    goto/32 :goto_53

    nop

    :goto_35
    move v7, v6

    goto/32 :goto_8a

    nop

    :goto_36
    add-int/lit8 v6, v0, -0x1

    goto/32 :goto_16

    nop

    :goto_37
    ushr-int/lit8 v9, v5, 0xc

    goto/32 :goto_6e

    nop

    :goto_38
    invoke-static {v6, v7, v8, v9}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_2a

    nop

    :goto_39
    int-to-byte v9, v9

    goto/32 :goto_a

    nop

    :goto_3a
    goto/16 :goto_1

    :goto_3b
    goto/32 :goto_67

    nop

    :goto_3c
    int-to-byte v2, v5

    goto/32 :goto_9a

    nop

    :goto_3d
    const/16 v6, 0x800

    goto/32 :goto_6f

    nop

    :goto_3e
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    goto/32 :goto_0

    nop

    :goto_3f
    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->requireSpace(I)V

    goto/32 :goto_3e

    nop

    :goto_40
    iput-wide v9, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_37

    nop

    :goto_41
    if-lt v4, v1, :cond_4

    goto/32 :goto_3b

    :cond_4
    goto/32 :goto_61

    nop

    :goto_42
    int-to-byte v9, v9

    goto/32 :goto_43

    nop

    :goto_43
    invoke-static {v6, v7, v8, v9}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_5a

    nop

    :goto_44
    sub-long v9, v7, v2

    goto/32 :goto_1d

    nop

    :goto_45
    iput-wide v11, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_8d

    nop

    :goto_46
    const-wide/16 v10, 0x2

    goto/32 :goto_78

    nop

    :goto_47
    int-to-byte v9, v5

    goto/32 :goto_87

    nop

    :goto_48
    iget-object v8, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    goto/32 :goto_68

    nop

    :goto_49
    iget-object v8, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    goto/32 :goto_98

    nop

    :goto_4a
    iget-wide v6, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_8e

    nop

    :goto_4b
    sub-long v11, v9, v2

    goto/32 :goto_91

    nop

    :goto_4c
    or-int/lit16 v11, v11, 0xf0

    goto/32 :goto_77

    nop

    :goto_4d
    invoke-static {v8, v9, v10, v11}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_83

    nop

    :goto_4e
    iget-wide v6, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_9d

    nop

    :goto_4f
    iget-wide v9, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_e

    nop

    :goto_50
    iget-wide v6, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_8c

    nop

    :goto_51
    and-int/lit8 v9, v5, 0x3f

    goto/32 :goto_27

    nop

    :goto_52
    if-nez v6, :cond_5

    goto/32 :goto_7f

    :cond_5
    goto/32 :goto_8b

    nop

    :goto_53
    if-lt v6, v5, :cond_6

    goto/32 :goto_75

    :cond_6
    :goto_54
    goto/32 :goto_30

    nop

    :goto_55
    iget-wide v6, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_9

    nop

    :goto_56
    invoke-static {v7, v5}, Ljava/lang/Character;->toCodePoint(CC)I

    move-result v6

    goto/32 :goto_7

    nop

    :goto_57
    int-to-byte v9, v9

    goto/32 :goto_38

    nop

    :goto_58
    throw v1

    :goto_59
    goto/32 :goto_80

    nop

    :goto_5a
    goto/16 :goto_1c

    :goto_5b
    goto/32 :goto_22

    nop

    :goto_5c
    or-int/2addr v9, v1

    goto/32 :goto_39

    nop

    :goto_5d
    if-gez v0, :cond_7

    goto/32 :goto_21

    :cond_7
    goto/32 :goto_82

    nop

    :goto_5e
    const/16 v1, 0x80

    goto/32 :goto_f

    nop

    :goto_5f
    if-gtz v8, :cond_8

    goto/32 :goto_75

    :cond_8
    goto/32 :goto_49

    nop

    :goto_60
    sub-long v9, v6, v2

    goto/32 :goto_c

    nop

    :goto_61
    iget-object v1, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    goto/32 :goto_50

    nop

    :goto_62
    or-int/2addr v9, v1

    goto/32 :goto_57

    nop

    :goto_63
    if-gtz v6, :cond_9

    goto/32 :goto_59

    :cond_9
    goto/32 :goto_7a

    nop

    :goto_64
    iget-object v6, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    goto/32 :goto_70

    nop

    :goto_65
    invoke-static {v8, v9, v10, v11}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_7e

    nop

    :goto_66
    invoke-static {v8, v6, v7, v9}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_2e

    nop

    :goto_67
    const/4 v4, -0x1

    goto/32 :goto_12

    nop

    :goto_68
    sub-long v9, v6, v2

    goto/32 :goto_88

    nop

    :goto_69
    ushr-int/lit8 v9, v5, 0x6

    goto/32 :goto_10

    nop

    :goto_6a
    cmp-long v8, v6, v8

    goto/32 :goto_5f

    nop

    :goto_6b
    ushr-int/lit8 v11, v6, 0x6

    goto/32 :goto_85

    nop

    :goto_6c
    return-void

    :goto_6d
    iput-wide v11, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_15

    nop

    :goto_6e
    or-int/lit16 v9, v9, 0x1e0

    goto/32 :goto_3

    nop

    :goto_6f
    if-lt v5, v6, :cond_a

    goto/32 :goto_5b

    :cond_a
    goto/32 :goto_4e

    nop

    :goto_70
    iget-wide v7, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_44

    nop

    :goto_71
    and-int/lit8 v9, v5, 0x3f

    goto/32 :goto_5c

    nop

    :goto_72
    or-int/2addr v11, v1

    goto/32 :goto_9e

    nop

    :goto_73
    add-int/2addr v0, v4

    goto/32 :goto_20

    nop

    :goto_74
    goto/16 :goto_1c

    :goto_75
    goto/32 :goto_4a

    nop

    :goto_76
    invoke-static {v6, v7, v8, v9}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_74

    nop

    :goto_77
    int-to-byte v11, v11

    goto/32 :goto_65

    nop

    :goto_78
    add-long/2addr v8, v10

    goto/32 :goto_8

    nop

    :goto_79
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    goto/32 :goto_3f

    nop

    :goto_7a
    if-nez v0, :cond_b

    goto/32 :goto_7f

    :cond_b
    goto/32 :goto_36

    nop

    :goto_7b
    if-lt v5, v1, :cond_c

    goto/32 :goto_97

    :cond_c
    goto/32 :goto_55

    nop

    :goto_7c
    int-to-byte v11, v11

    goto/32 :goto_93

    nop

    :goto_7d
    or-int/2addr v11, v1

    goto/32 :goto_28

    nop

    :goto_7e
    goto/16 :goto_1c

    :goto_7f
    goto/32 :goto_94

    nop

    :goto_80
    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->requireSpace(I)V

    goto/32 :goto_1b

    nop

    :goto_81
    if-gtz v8, :cond_d

    goto/32 :goto_5b

    :cond_d
    goto/32 :goto_8f

    nop

    :goto_82
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    goto/32 :goto_7b

    nop

    :goto_83
    iget-object v8, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    goto/32 :goto_2d

    nop

    :goto_84
    cmp-long v8, v6, v8

    goto/32 :goto_11

    nop

    :goto_85
    and-int/lit8 v11, v11, 0x3f

    goto/32 :goto_72

    nop

    :goto_86
    and-int/lit8 v9, v9, 0x3f

    goto/32 :goto_62

    nop

    :goto_87
    invoke-static {v8, v6, v7, v9}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_96

    nop

    :goto_88
    iput-wide v9, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_47

    nop

    :goto_89
    invoke-direct {v1, v2, v0}, Lcom/android/framework/protobuf/Utf8$UnpairedSurrogateException;-><init>(II)V

    goto/32 :goto_58

    nop

    :goto_8a
    invoke-static {v6, v5}, Ljava/lang/Character;->isSurrogatePair(CC)Z

    move-result v6

    goto/32 :goto_52

    nop

    :goto_8b
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_56

    nop

    :goto_8c
    sub-long v2, v6, v2

    goto/32 :goto_d

    nop

    :goto_8d
    and-int/lit8 v11, v6, 0x3f

    goto/32 :goto_7d

    nop

    :goto_8e
    iget-wide v8, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->offset:J

    goto/32 :goto_46

    nop

    :goto_8f
    iget-object v8, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    goto/32 :goto_60

    nop

    :goto_90
    iget-object v8, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->buffer:[B

    goto/32 :goto_92

    nop

    :goto_91
    iput-wide v11, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_6b

    nop

    :goto_92
    iget-wide v9, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_14

    nop

    :goto_93
    invoke-static {v8, v9, v10, v11}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_90

    nop

    :goto_94
    new-instance v1, Lcom/android/framework/protobuf/Utf8$UnpairedSurrogateException;

    goto/32 :goto_9b

    nop

    :goto_95
    iput-wide v11, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_33

    nop

    :goto_96
    goto/16 :goto_1c

    :goto_97
    goto/32 :goto_3d

    nop

    :goto_98
    sub-long v9, v6, v2

    goto/32 :goto_2f

    nop

    :goto_99
    iget-wide v8, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->offset:J

    goto/32 :goto_2c

    nop

    :goto_9a
    invoke-static {v1, v6, v7, v2}, Lcom/android/framework/protobuf/UnsafeUtil;->putByte([BJB)V

    goto/32 :goto_13

    nop

    :goto_9b
    add-int/lit8 v2, v0, -0x1

    goto/32 :goto_89

    nop

    :goto_9c
    iget-wide v9, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->pos:J

    goto/32 :goto_1f

    nop

    :goto_9d
    iget-wide v8, p0, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->offset:J

    goto/32 :goto_1e

    nop

    :goto_9e
    int-to-byte v11, v11

    goto/32 :goto_19

    nop
.end method

.method writeTag(II)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeVarint32(I)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    invoke-static {p1, p2}, Lcom/android/framework/protobuf/WireFormat;->makeTag(II)I

    move-result v0

    goto/32 :goto_0

    nop
.end method

.method public writeUInt32(II)V
    .locals 1

    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->requireSpace(I)V

    invoke-virtual {p0, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeVarint32(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeTag(II)V

    return-void
.end method

.method public writeUInt64(IJ)V
    .locals 1

    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->requireSpace(I)V

    invoke-virtual {p0, p2, p3}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeVarint64(J)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeTag(II)V

    return-void
.end method

.method writeVarint32(I)V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    if-eqz v0, :cond_0

    goto/32 :goto_11

    :cond_0
    goto/32 :goto_7

    nop

    :goto_1
    invoke-direct {p0, p1}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeVarint32TwoBytes(I)V

    goto/32 :goto_4

    nop

    :goto_2
    invoke-direct {p0, p1}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeVarint32OneByte(I)V

    goto/32 :goto_16

    nop

    :goto_3
    and-int/lit8 v0, p1, -0x80

    goto/32 :goto_8

    nop

    :goto_4
    goto :goto_14

    :goto_5
    goto/32 :goto_18

    nop

    :goto_6
    return-void

    :goto_7
    invoke-direct {p0, p1}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeVarint32FourBytes(I)V

    goto/32 :goto_10

    nop

    :goto_8
    if-eqz v0, :cond_1

    goto/32 :goto_17

    :cond_1
    goto/32 :goto_2

    nop

    :goto_9
    invoke-direct {p0, p1}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeVarint32ThreeBytes(I)V

    goto/32 :goto_d

    nop

    :goto_a
    if-eqz v0, :cond_2

    goto/32 :goto_e

    :cond_2
    goto/32 :goto_9

    nop

    :goto_b
    const/high16 v0, -0x10000000

    goto/32 :goto_c

    nop

    :goto_c
    and-int/2addr v0, p1

    goto/32 :goto_0

    nop

    :goto_d
    goto :goto_14

    :goto_e
    goto/32 :goto_b

    nop

    :goto_f
    and-int/2addr v0, p1

    goto/32 :goto_a

    nop

    :goto_10
    goto :goto_14

    :goto_11
    goto/32 :goto_13

    nop

    :goto_12
    if-eqz v0, :cond_3

    goto/32 :goto_5

    :cond_3
    goto/32 :goto_1

    nop

    :goto_13
    invoke-direct {p0, p1}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeVarint32FiveBytes(I)V

    :goto_14
    goto/32 :goto_6

    nop

    :goto_15
    and-int/lit16 v0, p1, -0x4000

    goto/32 :goto_12

    nop

    :goto_16
    goto :goto_14

    :goto_17
    goto/32 :goto_15

    nop

    :goto_18
    const/high16 v0, -0x200000

    goto/32 :goto_f

    nop
.end method

.method writeVarint64(J)V
    .locals 1

    goto/32 :goto_d

    nop

    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeVarint64FiveBytes(J)V

    goto/32 :goto_a

    nop

    :goto_1
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeVarint64TenBytes(J)V

    goto/32 :goto_13

    nop

    :goto_2
    goto :goto_11

    :pswitch_0
    goto/32 :goto_12

    nop

    :goto_3
    goto :goto_11

    :pswitch_1
    goto/32 :goto_0

    nop

    :goto_4
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeVarint64TwoBytes(J)V

    goto/32 :goto_5

    nop

    :goto_5
    goto :goto_11

    :pswitch_2
    goto/32 :goto_10

    nop

    :goto_6
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeVarint64NineBytes(J)V

    goto/32 :goto_9

    nop

    :goto_7
    goto :goto_11

    :pswitch_3
    goto/32 :goto_4

    nop

    :goto_8
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeVarint64EightBytes(J)V

    goto/32 :goto_2

    nop

    :goto_9
    goto :goto_11

    :pswitch_4
    goto/32 :goto_8

    nop

    :goto_a
    goto :goto_11

    :pswitch_5
    goto/32 :goto_e

    nop

    :goto_b
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeVarint64SixBytes(J)V

    goto/32 :goto_3

    nop

    :goto_c
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_8
        :pswitch_5
        :pswitch_1
        :pswitch_7
        :pswitch_0
        :pswitch_4
        :pswitch_6
        :pswitch_9
    .end packed-switch

    :goto_d
    invoke-static {p1, p2}, Lcom/android/framework/protobuf/BinaryWriter;->access$200(J)B

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/32 :goto_16

    nop

    :goto_e
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeVarint64FourBytes(J)V

    goto/32 :goto_15

    nop

    :goto_f
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeVarint64ThreeBytes(J)V

    goto/32 :goto_7

    nop

    :goto_10
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeVarint64OneByte(J)V

    nop

    :goto_11
    goto/32 :goto_c

    nop

    :goto_12
    invoke-direct {p0, p1, p2}, Lcom/android/framework/protobuf/BinaryWriter$UnsafeHeapWriter;->writeVarint64SevenBytes(J)V

    goto/32 :goto_14

    nop

    :goto_13
    goto :goto_11

    :pswitch_6
    goto/32 :goto_6

    nop

    :goto_14
    goto :goto_11

    :pswitch_7
    goto/32 :goto_b

    nop

    :goto_15
    goto :goto_11

    :pswitch_8
    goto/32 :goto_f

    nop

    :goto_16
    goto :goto_11

    :pswitch_9
    goto/32 :goto_1

    nop
.end method
