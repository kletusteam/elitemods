.class Lcom/android/framework/protobuf/UnknownFieldSetLiteSchema;
.super Lcom/android/framework/protobuf/UnknownFieldSchema;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/framework/protobuf/UnknownFieldSchema<",
        "Lcom/android/framework/protobuf/UnknownFieldSetLite;",
        "Lcom/android/framework/protobuf/UnknownFieldSetLite;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/framework/protobuf/UnknownFieldSchema;-><init>()V

    return-void
.end method


# virtual methods
.method addFixed32(Lcom/android/framework/protobuf/UnknownFieldSetLite;II)V
    .locals 2

    goto/32 :goto_4

    nop

    :goto_0
    invoke-static {p2, v0}, Lcom/android/framework/protobuf/WireFormat;->makeTag(II)I

    move-result v0

    goto/32 :goto_3

    nop

    :goto_1
    return-void

    :goto_2
    invoke-virtual {p1, v0, v1}, Lcom/android/framework/protobuf/UnknownFieldSetLite;->storeField(ILjava/lang/Object;)V

    goto/32 :goto_1

    nop

    :goto_3
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_2

    nop

    :goto_4
    const/4 v0, 0x5

    goto/32 :goto_0

    nop
.end method

.method bridge synthetic addFixed32(Ljava/lang/Object;II)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/framework/protobuf/UnknownFieldSetLiteSchema;->addFixed32(Lcom/android/framework/protobuf/UnknownFieldSetLite;II)V

    goto/32 :goto_2

    nop

    :goto_1
    check-cast p1, Lcom/android/framework/protobuf/UnknownFieldSetLite;

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method addFixed64(Lcom/android/framework/protobuf/UnknownFieldSetLite;IJ)V
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {p1, v0, v1}, Lcom/android/framework/protobuf/UnknownFieldSetLite;->storeField(ILjava/lang/Object;)V

    goto/32 :goto_2

    nop

    :goto_2
    return-void

    :goto_3
    const/4 v0, 0x1

    goto/32 :goto_4

    nop

    :goto_4
    invoke-static {p2, v0}, Lcom/android/framework/protobuf/WireFormat;->makeTag(II)I

    move-result v0

    goto/32 :goto_0

    nop
.end method

.method bridge synthetic addFixed64(Ljava/lang/Object;IJ)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/framework/protobuf/UnknownFieldSetLiteSchema;->addFixed64(Lcom/android/framework/protobuf/UnknownFieldSetLite;IJ)V

    goto/32 :goto_2

    nop

    :goto_1
    check-cast p1, Lcom/android/framework/protobuf/UnknownFieldSetLite;

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method addGroup(Lcom/android/framework/protobuf/UnknownFieldSetLite;ILcom/android/framework/protobuf/UnknownFieldSetLite;)V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    invoke-static {p2, v0}, Lcom/android/framework/protobuf/WireFormat;->makeTag(II)I

    move-result v0

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {p1, v0, p3}, Lcom/android/framework/protobuf/UnknownFieldSetLite;->storeField(ILjava/lang/Object;)V

    goto/32 :goto_2

    nop

    :goto_2
    return-void

    :goto_3
    const/4 v0, 0x3

    goto/32 :goto_0

    nop
.end method

.method bridge synthetic addGroup(Ljava/lang/Object;ILjava/lang/Object;)V
    .locals 0

    goto/32 :goto_3

    nop

    :goto_0
    check-cast p3, Lcom/android/framework/protobuf/UnknownFieldSetLite;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/framework/protobuf/UnknownFieldSetLiteSchema;->addGroup(Lcom/android/framework/protobuf/UnknownFieldSetLite;ILcom/android/framework/protobuf/UnknownFieldSetLite;)V

    goto/32 :goto_2

    nop

    :goto_2
    return-void

    :goto_3
    check-cast p1, Lcom/android/framework/protobuf/UnknownFieldSetLite;

    goto/32 :goto_0

    nop
.end method

.method addLengthDelimited(Lcom/android/framework/protobuf/UnknownFieldSetLite;ILcom/android/framework/protobuf/ByteString;)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {p1, v0, p3}, Lcom/android/framework/protobuf/UnknownFieldSetLite;->storeField(ILjava/lang/Object;)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    const/4 v0, 0x2

    goto/32 :goto_3

    nop

    :goto_3
    invoke-static {p2, v0}, Lcom/android/framework/protobuf/WireFormat;->makeTag(II)I

    move-result v0

    goto/32 :goto_0

    nop
.end method

.method bridge synthetic addLengthDelimited(Ljava/lang/Object;ILcom/android/framework/protobuf/ByteString;)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/framework/protobuf/UnknownFieldSetLiteSchema;->addLengthDelimited(Lcom/android/framework/protobuf/UnknownFieldSetLite;ILcom/android/framework/protobuf/ByteString;)V

    goto/32 :goto_2

    nop

    :goto_1
    check-cast p1, Lcom/android/framework/protobuf/UnknownFieldSetLite;

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method addVarint(Lcom/android/framework/protobuf/UnknownFieldSetLite;IJ)V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {p1, v0, v1}, Lcom/android/framework/protobuf/UnknownFieldSetLite;->storeField(ILjava/lang/Object;)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    const/4 v0, 0x0

    goto/32 :goto_4

    nop

    :goto_3
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_4
    invoke-static {p2, v0}, Lcom/android/framework/protobuf/WireFormat;->makeTag(II)I

    move-result v0

    goto/32 :goto_3

    nop
.end method

.method bridge synthetic addVarint(Ljava/lang/Object;IJ)V
    .locals 0

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/android/framework/protobuf/UnknownFieldSetLiteSchema;->addVarint(Lcom/android/framework/protobuf/UnknownFieldSetLite;IJ)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    check-cast p1, Lcom/android/framework/protobuf/UnknownFieldSetLite;

    goto/32 :goto_0

    nop
.end method

.method getBuilderFromMessage(Ljava/lang/Object;)Lcom/android/framework/protobuf/UnknownFieldSetLite;
    .locals 2

    goto/32 :goto_4

    nop

    :goto_0
    return-object v0

    :goto_1
    invoke-virtual {p0, p1, v0}, Lcom/android/framework/protobuf/UnknownFieldSetLiteSchema;->setToMessage(Ljava/lang/Object;Lcom/android/framework/protobuf/UnknownFieldSetLite;)V

    :goto_2
    goto/32 :goto_0

    nop

    :goto_3
    invoke-static {}, Lcom/android/framework/protobuf/UnknownFieldSetLite;->getDefaultInstance()Lcom/android/framework/protobuf/UnknownFieldSetLite;

    move-result-object v1

    goto/32 :goto_5

    nop

    :goto_4
    invoke-virtual {p0, p1}, Lcom/android/framework/protobuf/UnknownFieldSetLiteSchema;->getFromMessage(Ljava/lang/Object;)Lcom/android/framework/protobuf/UnknownFieldSetLite;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_5
    if-eq v0, v1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_6

    nop

    :goto_6
    invoke-static {}, Lcom/android/framework/protobuf/UnknownFieldSetLite;->newInstance()Lcom/android/framework/protobuf/UnknownFieldSetLite;

    move-result-object v0

    goto/32 :goto_1

    nop
.end method

.method bridge synthetic getBuilderFromMessage(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0, p1}, Lcom/android/framework/protobuf/UnknownFieldSetLiteSchema;->getBuilderFromMessage(Ljava/lang/Object;)Lcom/android/framework/protobuf/UnknownFieldSetLite;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_1
    return-object p1
.end method

.method getFromMessage(Ljava/lang/Object;)Lcom/android/framework/protobuf/UnknownFieldSetLite;
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    iget-object v0, v0, Lcom/android/framework/protobuf/GeneratedMessageLite;->unknownFields:Lcom/android/framework/protobuf/UnknownFieldSetLite;

    goto/32 :goto_2

    nop

    :goto_1
    check-cast v0, Lcom/android/framework/protobuf/GeneratedMessageLite;

    goto/32 :goto_0

    nop

    :goto_2
    return-object v0

    :goto_3
    move-object v0, p1

    goto/32 :goto_1

    nop
.end method

.method bridge synthetic getFromMessage(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-object p1

    :goto_1
    invoke-virtual {p0, p1}, Lcom/android/framework/protobuf/UnknownFieldSetLiteSchema;->getFromMessage(Ljava/lang/Object;)Lcom/android/framework/protobuf/UnknownFieldSetLite;

    move-result-object p1

    goto/32 :goto_0

    nop
.end method

.method getSerializedSize(Lcom/android/framework/protobuf/UnknownFieldSetLite;)I
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    invoke-virtual {p1}, Lcom/android/framework/protobuf/UnknownFieldSetLite;->getSerializedSize()I

    move-result v0

    goto/32 :goto_0

    nop
.end method

.method bridge synthetic getSerializedSize(Ljava/lang/Object;)I
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    check-cast p1, Lcom/android/framework/protobuf/UnknownFieldSetLite;

    goto/32 :goto_2

    nop

    :goto_1
    return p1

    :goto_2
    invoke-virtual {p0, p1}, Lcom/android/framework/protobuf/UnknownFieldSetLiteSchema;->getSerializedSize(Lcom/android/framework/protobuf/UnknownFieldSetLite;)I

    move-result p1

    goto/32 :goto_1

    nop
.end method

.method getSerializedSizeAsMessageSet(Lcom/android/framework/protobuf/UnknownFieldSetLite;)I
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    invoke-virtual {p1}, Lcom/android/framework/protobuf/UnknownFieldSetLite;->getSerializedSizeAsMessageSet()I

    move-result v0

    goto/32 :goto_0

    nop
.end method

.method bridge synthetic getSerializedSizeAsMessageSet(Ljava/lang/Object;)I
    .locals 0

    goto/32 :goto_2

    nop

    :goto_0
    return p1

    :goto_1
    invoke-virtual {p0, p1}, Lcom/android/framework/protobuf/UnknownFieldSetLiteSchema;->getSerializedSizeAsMessageSet(Lcom/android/framework/protobuf/UnknownFieldSetLite;)I

    move-result p1

    goto/32 :goto_0

    nop

    :goto_2
    check-cast p1, Lcom/android/framework/protobuf/UnknownFieldSetLite;

    goto/32 :goto_1

    nop
.end method

.method makeImmutable(Ljava/lang/Object;)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0, p1}, Lcom/android/framework/protobuf/UnknownFieldSetLiteSchema;->getFromMessage(Ljava/lang/Object;)Lcom/android/framework/protobuf/UnknownFieldSetLite;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    invoke-virtual {v0}, Lcom/android/framework/protobuf/UnknownFieldSetLite;->makeImmutable()V

    goto/32 :goto_1

    nop
.end method

.method merge(Lcom/android/framework/protobuf/UnknownFieldSetLite;Lcom/android/framework/protobuf/UnknownFieldSetLite;)Lcom/android/framework/protobuf/UnknownFieldSetLite;
    .locals 1

    goto/32 :goto_5

    nop

    :goto_0
    move-object v0, p1

    goto/32 :goto_3

    nop

    :goto_1
    invoke-static {p1, p2}, Lcom/android/framework/protobuf/UnknownFieldSetLite;->mutableCopyOf(Lcom/android/framework/protobuf/UnknownFieldSetLite;Lcom/android/framework/protobuf/UnknownFieldSetLite;)Lcom/android/framework/protobuf/UnknownFieldSetLite;

    move-result-object v0

    :goto_2
    goto/32 :goto_7

    nop

    :goto_3
    goto :goto_2

    :goto_4
    goto/32 :goto_1

    nop

    :goto_5
    invoke-static {}, Lcom/android/framework/protobuf/UnknownFieldSetLite;->getDefaultInstance()Lcom/android/framework/protobuf/UnknownFieldSetLite;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_6
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_0

    nop

    :goto_7
    return-object v0

    :goto_8
    invoke-virtual {p2, v0}, Lcom/android/framework/protobuf/UnknownFieldSetLite;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_6

    nop
.end method

.method bridge synthetic merge(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/android/framework/protobuf/UnknownFieldSetLiteSchema;->merge(Lcom/android/framework/protobuf/UnknownFieldSetLite;Lcom/android/framework/protobuf/UnknownFieldSetLite;)Lcom/android/framework/protobuf/UnknownFieldSetLite;

    move-result-object p1

    goto/32 :goto_3

    nop

    :goto_1
    check-cast p2, Lcom/android/framework/protobuf/UnknownFieldSetLite;

    goto/32 :goto_0

    nop

    :goto_2
    check-cast p1, Lcom/android/framework/protobuf/UnknownFieldSetLite;

    goto/32 :goto_1

    nop

    :goto_3
    return-object p1
.end method

.method newBuilder()Lcom/android/framework/protobuf/UnknownFieldSetLite;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    invoke-static {}, Lcom/android/framework/protobuf/UnknownFieldSetLite;->newInstance()Lcom/android/framework/protobuf/UnknownFieldSetLite;

    move-result-object v0

    goto/32 :goto_0

    nop
.end method

.method bridge synthetic newBuilder()Ljava/lang/Object;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    invoke-virtual {p0}, Lcom/android/framework/protobuf/UnknownFieldSetLiteSchema;->newBuilder()Lcom/android/framework/protobuf/UnknownFieldSetLite;

    move-result-object v0

    goto/32 :goto_0

    nop
.end method

.method setBuilderToMessage(Ljava/lang/Object;Lcom/android/framework/protobuf/UnknownFieldSetLite;)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/android/framework/protobuf/UnknownFieldSetLiteSchema;->setToMessage(Ljava/lang/Object;Lcom/android/framework/protobuf/UnknownFieldSetLite;)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method bridge synthetic setBuilderToMessage(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    check-cast p2, Lcom/android/framework/protobuf/UnknownFieldSetLite;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {p0, p1, p2}, Lcom/android/framework/protobuf/UnknownFieldSetLiteSchema;->setBuilderToMessage(Ljava/lang/Object;Lcom/android/framework/protobuf/UnknownFieldSetLite;)V

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method

.method setToMessage(Ljava/lang/Object;Lcom/android/framework/protobuf/UnknownFieldSetLite;)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    move-object v0, p1

    goto/32 :goto_1

    nop

    :goto_1
    check-cast v0, Lcom/android/framework/protobuf/GeneratedMessageLite;

    goto/32 :goto_2

    nop

    :goto_2
    iput-object p2, v0, Lcom/android/framework/protobuf/GeneratedMessageLite;->unknownFields:Lcom/android/framework/protobuf/UnknownFieldSetLite;

    goto/32 :goto_3

    nop

    :goto_3
    return-void
.end method

.method bridge synthetic setToMessage(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    check-cast p2, Lcom/android/framework/protobuf/UnknownFieldSetLite;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {p0, p1, p2}, Lcom/android/framework/protobuf/UnknownFieldSetLiteSchema;->setToMessage(Ljava/lang/Object;Lcom/android/framework/protobuf/UnknownFieldSetLite;)V

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method

.method shouldDiscardUnknownFields(Lcom/android/framework/protobuf/Reader;)Z
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_0

    nop
.end method

.method toImmutable(Lcom/android/framework/protobuf/UnknownFieldSetLite;)Lcom/android/framework/protobuf/UnknownFieldSetLite;
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-object p1

    :goto_1
    invoke-virtual {p1}, Lcom/android/framework/protobuf/UnknownFieldSetLite;->makeImmutable()V

    goto/32 :goto_0

    nop
.end method

.method bridge synthetic toImmutable(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {p0, p1}, Lcom/android/framework/protobuf/UnknownFieldSetLiteSchema;->toImmutable(Lcom/android/framework/protobuf/UnknownFieldSetLite;)Lcom/android/framework/protobuf/UnknownFieldSetLite;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_1
    return-object p1

    :goto_2
    check-cast p1, Lcom/android/framework/protobuf/UnknownFieldSetLite;

    goto/32 :goto_0

    nop
.end method

.method writeAsMessageSetTo(Lcom/android/framework/protobuf/UnknownFieldSetLite;Lcom/android/framework/protobuf/Writer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p1, p2}, Lcom/android/framework/protobuf/UnknownFieldSetLite;->writeAsMessageSetTo(Lcom/android/framework/protobuf/Writer;)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method bridge synthetic writeAsMessageSetTo(Ljava/lang/Object;Lcom/android/framework/protobuf/Writer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    check-cast p1, Lcom/android/framework/protobuf/UnknownFieldSetLite;

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    invoke-virtual {p0, p1, p2}, Lcom/android/framework/protobuf/UnknownFieldSetLiteSchema;->writeAsMessageSetTo(Lcom/android/framework/protobuf/UnknownFieldSetLite;Lcom/android/framework/protobuf/Writer;)V

    goto/32 :goto_1

    nop
.end method

.method writeTo(Lcom/android/framework/protobuf/UnknownFieldSetLite;Lcom/android/framework/protobuf/Writer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p1, p2}, Lcom/android/framework/protobuf/UnknownFieldSetLite;->writeTo(Lcom/android/framework/protobuf/Writer;)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method bridge synthetic writeTo(Ljava/lang/Object;Lcom/android/framework/protobuf/Writer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    check-cast p1, Lcom/android/framework/protobuf/UnknownFieldSetLite;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {p0, p1, p2}, Lcom/android/framework/protobuf/UnknownFieldSetLiteSchema;->writeTo(Lcom/android/framework/protobuf/UnknownFieldSetLite;Lcom/android/framework/protobuf/Writer;)V

    goto/32 :goto_0

    nop
.end method
