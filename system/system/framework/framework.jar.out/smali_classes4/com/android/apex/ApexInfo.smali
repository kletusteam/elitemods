.class public Lcom/android/apex/ApexInfo;
.super Ljava/lang/Object;


# instance fields
.field private isActive:Ljava/lang/Boolean;

.field private isFactory:Ljava/lang/Boolean;

.field private lastUpdateMillis:Ljava/lang/Long;

.field private moduleName:Ljava/lang/String;

.field private modulePath:Ljava/lang/String;

.field private preinstalledModulePath:Ljava/lang/String;

.field private provideSharedApexLibs:Ljava/lang/Boolean;

.field private versionCode:Ljava/lang/Long;

.field private versionName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static read(Lorg/xmlpull/v1/XmlPullParser;)Lcom/android/apex/ApexInfo;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;,
            Ljavax/xml/datatype/DatatypeConfigurationException;
        }
    .end annotation

    new-instance v0, Lcom/android/apex/ApexInfo;

    invoke-direct {v0}, Lcom/android/apex/ApexInfo;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    const-string/jumbo v3, "moduleName"

    invoke-interface {p0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object v3, v1

    invoke-virtual {v0, v3}, Lcom/android/apex/ApexInfo;->setModuleName(Ljava/lang/String;)V

    :cond_0
    const-string/jumbo v3, "modulePath"

    invoke-interface {p0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    move-object v3, v1

    invoke-virtual {v0, v3}, Lcom/android/apex/ApexInfo;->setModulePath(Ljava/lang/String;)V

    :cond_1
    const-string/jumbo v3, "preinstalledModulePath"

    invoke-interface {p0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    move-object v3, v1

    invoke-virtual {v0, v3}, Lcom/android/apex/ApexInfo;->setPreinstalledModulePath(Ljava/lang/String;)V

    :cond_2
    const-string/jumbo v3, "versionCode"

    invoke-interface {p0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Lcom/android/apex/ApexInfo;->setVersionCode(J)V

    :cond_3
    const-string/jumbo v3, "versionName"

    invoke-interface {p0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    move-object v3, v1

    invoke-virtual {v0, v3}, Lcom/android/apex/ApexInfo;->setVersionName(Ljava/lang/String;)V

    :cond_4
    const-string v3, "isFactory"

    invoke-interface {p0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/android/apex/ApexInfo;->setIsFactory(Z)V

    :cond_5
    const-string v3, "isActive"

    invoke-interface {p0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/android/apex/ApexInfo;->setIsActive(Z)V

    :cond_6
    const-string v3, "lastUpdateMillis"

    invoke-interface {p0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Lcom/android/apex/ApexInfo;->setLastUpdateMillis(J)V

    :cond_7
    const-string/jumbo v3, "provideSharedApexLibs"

    invoke-interface {p0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/android/apex/ApexInfo;->setProvideSharedApexLibs(Z)V

    :cond_8
    invoke-static {p0}, Lcom/android/apex/XmlParser;->skip(Lorg/xmlpull/v1/XmlPullParser;)V

    return-object v0
.end method


# virtual methods
.method public getIsActive()Z
    .locals 1

    iget-object v0, p0, Lcom/android/apex/ApexInfo;->isActive:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public getIsFactory()Z
    .locals 1

    iget-object v0, p0, Lcom/android/apex/ApexInfo;->isFactory:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public getLastUpdateMillis()J
    .locals 2

    iget-object v0, p0, Lcom/android/apex/ApexInfo;->lastUpdateMillis:Ljava/lang/Long;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    return-wide v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getModuleName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/apex/ApexInfo;->moduleName:Ljava/lang/String;

    return-object v0
.end method

.method public getModulePath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/apex/ApexInfo;->modulePath:Ljava/lang/String;

    return-object v0
.end method

.method public getPreinstalledModulePath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/apex/ApexInfo;->preinstalledModulePath:Ljava/lang/String;

    return-object v0
.end method

.method public getProvideSharedApexLibs()Z
    .locals 1

    iget-object v0, p0, Lcom/android/apex/ApexInfo;->provideSharedApexLibs:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public getVersionCode()J
    .locals 2

    iget-object v0, p0, Lcom/android/apex/ApexInfo;->versionCode:Ljava/lang/Long;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    return-wide v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getVersionName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/apex/ApexInfo;->versionName:Ljava/lang/String;

    return-object v0
.end method

.method hasIsActive()Z
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return v0

    :goto_1
    const/4 v0, 0x1

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/apex/ApexInfo;->isActive:Ljava/lang/Boolean;

    goto/32 :goto_6

    nop

    :goto_3
    const/4 v0, 0x0

    goto/32 :goto_4

    nop

    :goto_4
    return v0

    :goto_5
    goto/32 :goto_1

    nop

    :goto_6
    if-eqz v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_3

    nop
.end method

.method hasIsFactory()Z
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    if-eqz v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_5

    nop

    :goto_1
    return v0

    :goto_2
    return v0

    :goto_3
    goto/32 :goto_6

    nop

    :goto_4
    iget-object v0, p0, Lcom/android/apex/ApexInfo;->isFactory:Ljava/lang/Boolean;

    goto/32 :goto_0

    nop

    :goto_5
    const/4 v0, 0x0

    goto/32 :goto_2

    nop

    :goto_6
    const/4 v0, 0x1

    goto/32 :goto_1

    nop
.end method

.method hasLastUpdateMillis()Z
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    iget-object v0, p0, Lcom/android/apex/ApexInfo;->lastUpdateMillis:Ljava/lang/Long;

    goto/32 :goto_4

    nop

    :goto_2
    return v0

    :goto_3
    goto/32 :goto_6

    nop

    :goto_4
    if-eqz v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_5

    nop

    :goto_5
    const/4 v0, 0x0

    goto/32 :goto_2

    nop

    :goto_6
    const/4 v0, 0x1

    goto/32 :goto_0

    nop
.end method

.method hasModuleName()Z
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    return v0

    :goto_1
    const/4 v0, 0x1

    goto/32 :goto_0

    nop

    :goto_2
    if-eqz v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_3

    nop

    :goto_3
    const/4 v0, 0x0

    goto/32 :goto_5

    nop

    :goto_4
    iget-object v0, p0, Lcom/android/apex/ApexInfo;->moduleName:Ljava/lang/String;

    goto/32 :goto_2

    nop

    :goto_5
    return v0

    :goto_6
    goto/32 :goto_1

    nop
.end method

.method hasModulePath()Z
    .locals 1

    goto/32 :goto_6

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_2

    nop

    :goto_1
    if-eqz v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_0

    nop

    :goto_2
    return v0

    :goto_3
    goto/32 :goto_4

    nop

    :goto_4
    const/4 v0, 0x1

    goto/32 :goto_5

    nop

    :goto_5
    return v0

    :goto_6
    iget-object v0, p0, Lcom/android/apex/ApexInfo;->modulePath:Ljava/lang/String;

    goto/32 :goto_1

    nop
.end method

.method hasPreinstalledModulePath()Z
    .locals 1

    goto/32 :goto_6

    nop

    :goto_0
    return v0

    :goto_1
    const/4 v0, 0x1

    goto/32 :goto_0

    nop

    :goto_2
    return v0

    :goto_3
    goto/32 :goto_1

    nop

    :goto_4
    if-eqz v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_5

    nop

    :goto_5
    const/4 v0, 0x0

    goto/32 :goto_2

    nop

    :goto_6
    iget-object v0, p0, Lcom/android/apex/ApexInfo;->preinstalledModulePath:Ljava/lang/String;

    goto/32 :goto_4

    nop
.end method

.method hasProvideSharedApexLibs()Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/apex/ApexInfo;->provideSharedApexLibs:Ljava/lang/Boolean;

    goto/32 :goto_1

    nop

    :goto_1
    if-eqz v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_2

    nop

    :goto_2
    const/4 v0, 0x0

    goto/32 :goto_4

    nop

    :goto_3
    const/4 v0, 0x1

    goto/32 :goto_6

    nop

    :goto_4
    return v0

    :goto_5
    goto/32 :goto_3

    nop

    :goto_6
    return v0
.end method

.method hasVersionCode()Z
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    const/4 v0, 0x1

    goto/32 :goto_6

    nop

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_4

    nop

    :goto_2
    if-eqz v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_1

    nop

    :goto_3
    iget-object v0, p0, Lcom/android/apex/ApexInfo;->versionCode:Ljava/lang/Long;

    goto/32 :goto_2

    nop

    :goto_4
    return v0

    :goto_5
    goto/32 :goto_0

    nop

    :goto_6
    return v0
.end method

.method hasVersionName()Z
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return v0

    :goto_1
    goto/32 :goto_5

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/apex/ApexInfo;->versionName:Ljava/lang/String;

    goto/32 :goto_4

    nop

    :goto_3
    return v0

    :goto_4
    if-eqz v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_6

    nop

    :goto_5
    const/4 v0, 0x1

    goto/32 :goto_3

    nop

    :goto_6
    const/4 v0, 0x0

    goto/32 :goto_0

    nop
.end method

.method public setIsActive(Z)V
    .locals 1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/android/apex/ApexInfo;->isActive:Ljava/lang/Boolean;

    return-void
.end method

.method public setIsFactory(Z)V
    .locals 1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/android/apex/ApexInfo;->isFactory:Ljava/lang/Boolean;

    return-void
.end method

.method public setLastUpdateMillis(J)V
    .locals 1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/android/apex/ApexInfo;->lastUpdateMillis:Ljava/lang/Long;

    return-void
.end method

.method public setModuleName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/apex/ApexInfo;->moduleName:Ljava/lang/String;

    return-void
.end method

.method public setModulePath(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/apex/ApexInfo;->modulePath:Ljava/lang/String;

    return-void
.end method

.method public setPreinstalledModulePath(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/apex/ApexInfo;->preinstalledModulePath:Ljava/lang/String;

    return-void
.end method

.method public setProvideSharedApexLibs(Z)V
    .locals 1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/android/apex/ApexInfo;->provideSharedApexLibs:Ljava/lang/Boolean;

    return-void
.end method

.method public setVersionCode(J)V
    .locals 1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/android/apex/ApexInfo;->versionCode:Ljava/lang/Long;

    return-void
.end method

.method public setVersionName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/apex/ApexInfo;->versionName:Ljava/lang/String;

    return-void
.end method

.method write(Lcom/android/apex/XmlWriter;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_45

    nop

    :goto_0
    invoke-virtual {p1, v0}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_18

    nop

    :goto_1
    invoke-virtual {p0}, Lcom/android/apex/ApexInfo;->hasProvideSharedApexLibs()Z

    move-result v0

    goto/32 :goto_9

    nop

    :goto_2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_44

    nop

    :goto_3
    invoke-virtual {p1, v0}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_34

    nop

    :goto_4
    invoke-virtual {p1, v0}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_1b

    nop

    :goto_5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_25

    nop

    :goto_6
    invoke-virtual {p0}, Lcom/android/apex/ApexInfo;->hasModulePath()Z

    move-result v0

    goto/32 :goto_30

    nop

    :goto_7
    if-nez v0, :cond_0

    goto/32 :goto_35

    :cond_0
    goto/32 :goto_10

    nop

    :goto_8
    invoke-virtual {p1, v0}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_33

    nop

    :goto_9
    if-nez v0, :cond_1

    goto/32 :goto_f

    :cond_1
    goto/32 :goto_31

    nop

    :goto_a
    const-string v0, ">\n"

    goto/32 :goto_15

    nop

    :goto_b
    invoke-virtual {p0}, Lcom/android/apex/ApexInfo;->getModulePath()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_11

    nop

    :goto_c
    invoke-virtual {p1, v0}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_52

    nop

    :goto_d
    const-string v1, "<"

    goto/32 :goto_5c

    nop

    :goto_e
    invoke-virtual {p1, v1}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    :goto_f
    goto/32 :goto_a

    nop

    :goto_10
    const-string v0, " moduleName=\""

    goto/32 :goto_c

    nop

    :goto_11
    invoke-virtual {p1, v0}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_3c

    nop

    :goto_12
    const-string v0, " isActive=\""

    goto/32 :goto_46

    nop

    :goto_13
    invoke-virtual {p0}, Lcom/android/apex/ApexInfo;->hasVersionCode()Z

    move-result v0

    goto/32 :goto_55

    nop

    :goto_14
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_15
    invoke-virtual {p1, v0}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_37

    nop

    :goto_16
    invoke-virtual {p0}, Lcom/android/apex/ApexInfo;->getLastUpdateMillis()J

    move-result-wide v2

    goto/32 :goto_22

    nop

    :goto_17
    invoke-virtual {p1, v0}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_e

    nop

    :goto_18
    invoke-virtual {p0}, Lcom/android/apex/ApexInfo;->getIsFactory()Z

    move-result v0

    goto/32 :goto_2f

    nop

    :goto_19
    invoke-virtual {p1, v1}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    :goto_1a
    goto/32 :goto_21

    nop

    :goto_1b
    invoke-virtual {p0}, Lcom/android/apex/ApexInfo;->getVersionName()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_42

    nop

    :goto_1c
    if-nez v0, :cond_2

    goto/32 :goto_1a

    :cond_2
    goto/32 :goto_5a

    nop

    :goto_1d
    if-nez v0, :cond_3

    goto/32 :goto_4b

    :cond_3
    goto/32 :goto_2c

    nop

    :goto_1e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_d

    nop

    :goto_1f
    invoke-virtual {p1, v0}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_2a

    nop

    :goto_20
    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_17

    nop

    :goto_21
    invoke-virtual {p0}, Lcom/android/apex/ApexInfo;->hasIsActive()Z

    move-result v0

    goto/32 :goto_28

    nop

    :goto_22
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_1f

    nop

    :goto_23
    invoke-virtual {p1, v1}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    :goto_24
    goto/32 :goto_2d

    nop

    :goto_25
    invoke-virtual {p1, v0}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_2e

    nop

    :goto_26
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_14

    nop

    :goto_27
    invoke-virtual {p0}, Lcom/android/apex/ApexInfo;->hasIsFactory()Z

    move-result v0

    goto/32 :goto_1c

    nop

    :goto_28
    if-nez v0, :cond_4

    goto/32 :goto_41

    :cond_4
    goto/32 :goto_12

    nop

    :goto_29
    invoke-virtual {p1, v0}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_19

    nop

    :goto_2a
    invoke-virtual {p1, v1}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    :goto_2b
    goto/32 :goto_1

    nop

    :goto_2c
    const-string v0, " preinstalledModulePath=\""

    goto/32 :goto_54

    nop

    :goto_2d
    invoke-virtual {p0}, Lcom/android/apex/ApexInfo;->hasVersionName()Z

    move-result v0

    goto/32 :goto_58

    nop

    :goto_2e
    return-void

    :goto_2f
    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_29

    nop

    :goto_30
    if-nez v0, :cond_5

    goto/32 :goto_3d

    :cond_5
    goto/32 :goto_5b

    nop

    :goto_31
    const-string v0, " provideSharedApexLibs=\""

    goto/32 :goto_50

    nop

    :goto_32
    invoke-virtual {p0}, Lcom/android/apex/ApexInfo;->getIsActive()Z

    move-result v0

    goto/32 :goto_38

    nop

    :goto_33
    invoke-virtual {p0}, Lcom/android/apex/ApexInfo;->hasModuleName()Z

    move-result v0

    goto/32 :goto_3f

    nop

    :goto_34
    invoke-virtual {p1, v1}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    :goto_35
    goto/32 :goto_6

    nop

    :goto_36
    const-string v0, " versionName=\""

    goto/32 :goto_4

    nop

    :goto_37
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_2

    nop

    :goto_38
    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_53

    nop

    :goto_39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_26

    nop

    :goto_3a
    invoke-virtual {p1, v1}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    :goto_3b
    goto/32 :goto_27

    nop

    :goto_3c
    invoke-virtual {p1, v1}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    :goto_3d
    goto/32 :goto_5d

    nop

    :goto_3e
    invoke-virtual {p1, v0}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_4a

    nop

    :goto_3f
    const-string v1, "\""

    goto/32 :goto_7

    nop

    :goto_40
    invoke-virtual {p1, v1}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    :goto_41
    goto/32 :goto_59

    nop

    :goto_42
    invoke-virtual {p1, v0}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_3a

    nop

    :goto_43
    invoke-virtual {p1, v0}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_4c

    nop

    :goto_44
    const-string v2, "</"

    goto/32 :goto_39

    nop

    :goto_45
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_1e

    nop

    :goto_46
    invoke-virtual {p1, v0}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_32

    nop

    :goto_47
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_48
    invoke-virtual {p0}, Lcom/android/apex/ApexInfo;->getPreinstalledModulePath()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_3e

    nop

    :goto_49
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_47

    nop

    :goto_4a
    invoke-virtual {p1, v1}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    :goto_4b
    goto/32 :goto_13

    nop

    :goto_4c
    invoke-virtual {p0}, Lcom/android/apex/ApexInfo;->getVersionCode()J

    move-result-wide v2

    goto/32 :goto_4e

    nop

    :goto_4d
    if-nez v0, :cond_6

    goto/32 :goto_2b

    :cond_6
    goto/32 :goto_5f

    nop

    :goto_4e
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_51

    nop

    :goto_4f
    invoke-virtual {p1, v0}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_b

    nop

    :goto_50
    invoke-virtual {p1, v0}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_5e

    nop

    :goto_51
    invoke-virtual {p1, v0}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_23

    nop

    :goto_52
    invoke-virtual {p0}, Lcom/android/apex/ApexInfo;->getModuleName()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_53
    invoke-virtual {p1, v0}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_40

    nop

    :goto_54
    invoke-virtual {p1, v0}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_48

    nop

    :goto_55
    if-nez v0, :cond_7

    goto/32 :goto_24

    :cond_7
    goto/32 :goto_56

    nop

    :goto_56
    const-string v0, " versionCode=\""

    goto/32 :goto_43

    nop

    :goto_57
    invoke-virtual {p1, v0}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_16

    nop

    :goto_58
    if-nez v0, :cond_8

    goto/32 :goto_3b

    :cond_8
    goto/32 :goto_36

    nop

    :goto_59
    invoke-virtual {p0}, Lcom/android/apex/ApexInfo;->hasLastUpdateMillis()Z

    move-result v0

    goto/32 :goto_4d

    nop

    :goto_5a
    const-string v0, " isFactory=\""

    goto/32 :goto_0

    nop

    :goto_5b
    const-string v0, " modulePath=\""

    goto/32 :goto_4f

    nop

    :goto_5c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_49

    nop

    :goto_5d
    invoke-virtual {p0}, Lcom/android/apex/ApexInfo;->hasPreinstalledModulePath()Z

    move-result v0

    goto/32 :goto_1d

    nop

    :goto_5e
    invoke-virtual {p0}, Lcom/android/apex/ApexInfo;->getProvideSharedApexLibs()Z

    move-result v0

    goto/32 :goto_20

    nop

    :goto_5f
    const-string v0, " lastUpdateMillis=\""

    goto/32 :goto_57

    nop
.end method
