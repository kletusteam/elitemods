.class public Lcom/android/apex/XmlWriter;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private indent:I

.field private out:Ljava/io/PrintWriter;

.field private outBuffer:Ljava/lang/StringBuilder;

.field private startLine:Z


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Ljava/io/PrintWriter;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/apex/XmlWriter;->out:Ljava/io/PrintWriter;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/android/apex/XmlWriter;->outBuffer:Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/apex/XmlWriter;->indent:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/apex/XmlWriter;->startLine:Z

    return-void
.end method

.method private printIndent()V
    .locals 3

    nop

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/android/apex/XmlWriter;->indent:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/apex/XmlWriter;->outBuffer:Ljava/lang/StringBuilder;

    const-string v2, "    "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/apex/XmlWriter;->startLine:Z

    return-void
.end method

.method public static write(Lcom/android/apex/XmlWriter;Lcom/android/apex/ApexInfo;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"

    invoke-virtual {p0, v0}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    const-string v0, "apex-info"

    invoke-virtual {p1, p0, v0}, Lcom/android/apex/ApexInfo;->write(Lcom/android/apex/XmlWriter;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/apex/XmlWriter;->printXml()V

    return-void
.end method

.method public static write(Lcom/android/apex/XmlWriter;Lcom/android/apex/ApexInfoList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"

    invoke-virtual {p0, v0}, Lcom/android/apex/XmlWriter;->print(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    const-string v0, "apex-info-list"

    invoke-virtual {p1, p0, v0}, Lcom/android/apex/ApexInfoList;->write(Lcom/android/apex/XmlWriter;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/apex/XmlWriter;->printXml()V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/android/apex/XmlWriter;->out:Ljava/io/PrintWriter;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/PrintWriter;->close()V

    :cond_0
    return-void
.end method

.method decreaseIndent()V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_1

    nop

    :goto_1
    iput v0, p0, Lcom/android/apex/XmlWriter;->indent:I

    goto/32 :goto_2

    nop

    :goto_2
    return-void

    :goto_3
    iget v0, p0, Lcom/android/apex/XmlWriter;->indent:I

    goto/32 :goto_0

    nop
.end method

.method increaseIndent()V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p0, Lcom/android/apex/XmlWriter;->indent:I

    goto/32 :goto_3

    nop

    :goto_1
    return-void

    :goto_2
    iput v0, p0, Lcom/android/apex/XmlWriter;->indent:I

    goto/32 :goto_1

    nop

    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_2

    nop
.end method

.method print(Ljava/lang/String;)V
    .locals 5

    goto/32 :goto_e

    nop

    :goto_0
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    goto/32 :goto_18

    nop

    :goto_1
    const/4 v2, 0x0

    :goto_2
    goto/32 :goto_13

    nop

    :goto_3
    add-int/lit8 v3, v2, 0x1

    goto/32 :goto_1c

    nop

    :goto_4
    goto :goto_2

    :goto_5
    goto/32 :goto_f

    nop

    :goto_6
    iget-boolean v3, p0, Lcom/android/apex/XmlWriter;->startLine:Z

    goto/32 :goto_a

    nop

    :goto_7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_3

    nop

    :goto_8
    iget-object v3, p0, Lcom/android/apex/XmlWriter;->outBuffer:Ljava/lang/StringBuilder;

    goto/32 :goto_b

    nop

    :goto_9
    const/4 v3, 0x1

    goto/32 :goto_11

    nop

    :goto_a
    if-nez v3, :cond_0

    goto/32 :goto_15

    :cond_0
    goto/32 :goto_17

    nop

    :goto_b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_9

    nop

    :goto_c
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_1

    nop

    :goto_d
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_4

    nop

    :goto_e
    const-string v0, "\n"

    goto/32 :goto_1b

    nop

    :goto_f
    return-void

    :goto_10
    aget-object v4, v1, v2

    goto/32 :goto_7

    nop

    :goto_11
    iput-boolean v3, p0, Lcom/android/apex/XmlWriter;->startLine:Z

    :goto_12
    goto/32 :goto_d

    nop

    :goto_13
    array-length v3, v1

    goto/32 :goto_1a

    nop

    :goto_14
    invoke-direct {p0}, Lcom/android/apex/XmlWriter;->printIndent()V

    :goto_15
    goto/32 :goto_16

    nop

    :goto_16
    iget-object v3, p0, Lcom/android/apex/XmlWriter;->outBuffer:Ljava/lang/StringBuilder;

    goto/32 :goto_10

    nop

    :goto_17
    aget-object v3, v1, v2

    goto/32 :goto_0

    nop

    :goto_18
    if-eqz v3, :cond_1

    goto/32 :goto_15

    :cond_1
    goto/32 :goto_14

    nop

    :goto_19
    if-lt v3, v4, :cond_2

    goto/32 :goto_12

    :cond_2
    goto/32 :goto_8

    nop

    :goto_1a
    if-lt v2, v3, :cond_3

    goto/32 :goto_5

    :cond_3
    goto/32 :goto_6

    nop

    :goto_1b
    const/4 v1, -0x1

    goto/32 :goto_c

    nop

    :goto_1c
    array-length v4, v1

    goto/32 :goto_19

    nop
.end method

.method printXml()V
    .locals 2

    goto/32 :goto_4

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v1, p0, Lcom/android/apex/XmlWriter;->outBuffer:Ljava/lang/StringBuilder;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_0

    nop

    :goto_4
    iget-object v0, p0, Lcom/android/apex/XmlWriter;->out:Ljava/io/PrintWriter;

    goto/32 :goto_1

    nop
.end method
