.class public final Lcom/android/server/vibrator/VibratorManagerServiceDumpProto;
.super Ljava/lang/Object;


# static fields
.field public static final ALARM_DEFAULT_INTENSITY:J = 0x10500000013L

.field public static final ALARM_INTENSITY:J = 0x10500000012L

.field public static final CURRENT_EXTERNAL_VIBRATION:J = 0x10b00000004L

.field public static final CURRENT_VIBRATION:J = 0x10b00000002L

.field public static final HAPTIC_FEEDBACK_DEFAULT_INTENSITY:J = 0x10500000008L

.field public static final HAPTIC_FEEDBACK_INTENSITY:J = 0x10500000007L

.field public static final HARDWARE_FEEDBACK_DEFAULT_INTENSITY:J = 0x10500000017L

.field public static final HARDWARE_FEEDBACK_INTENSITY:J = 0x10500000016L

.field public static final IS_VIBRATING:J = 0x10800000003L

.field public static final LOW_POWER_MODE:J = 0x10800000006L

.field public static final MEDIA_DEFAULT_INTENSITY:J = 0x10500000015L

.field public static final MEDIA_INTENSITY:J = 0x10500000014L

.field public static final NOTIFICATION_DEFAULT_INTENSITY:J = 0x1050000000aL

.field public static final NOTIFICATION_INTENSITY:J = 0x10500000009L

.field public static final PREVIOUS_ALARM_VIBRATIONS:J = 0x20b0000000fL

.field public static final PREVIOUS_EXTERNAL_VIBRATIONS:J = 0x20b00000011L

.field public static final PREVIOUS_NOTIFICATION_VIBRATIONS:J = 0x20b0000000eL

.field public static final PREVIOUS_RING_VIBRATIONS:J = 0x20b0000000dL

.field public static final PREVIOUS_VIBRATIONS:J = 0x20b00000010L

.field public static final RING_DEFAULT_INTENSITY:J = 0x1050000000cL

.field public static final RING_INTENSITY:J = 0x1050000000bL

.field public static final VIBRATE_ON:J = 0x10800000018L

.field public static final VIBRATOR_IDS:J = 0x20500000001L

.field public static final VIBRATOR_UNDER_EXTERNAL_CONTROL:J = 0x10800000005L


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
