.class public final Lcom/android/server/power/LowPowerStandbyControllerDumpProto;
.super Ljava/lang/Object;


# static fields
.field public static final ALLOWLIST:J = 0x2050000000aL

.field public static final IDLE_SINCE_NON_INTERACTIVE:J = 0x10800000008L

.field public static final IS_ACTIVE:J = 0x10800000001L

.field public static final IS_DEVICE_IDLE:J = 0x10800000009L

.field public static final IS_ENABLED:J = 0x10800000002L

.field public static final IS_ENABLED_BY_DEFAULT_CONFIG:J = 0x10800000004L

.field public static final IS_INTERACTIVE:J = 0x10800000005L

.field public static final IS_SUPPORTED_CONFIG:J = 0x10800000003L

.field public static final LAST_INTERACTIVE_TIME:J = 0x10300000006L

.field public static final STANDBY_TIMEOUT_CONFIG:J = 0x10500000007L


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
