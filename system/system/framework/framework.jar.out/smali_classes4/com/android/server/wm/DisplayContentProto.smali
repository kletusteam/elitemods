.class public final Lcom/android/server/wm/DisplayContentProto;
.super Ljava/lang/Object;


# static fields
.field public static final ABOVE_APP_WINDOWS:J = 0x20b00000006L

.field public static final APP_TRANSITION:J = 0x10b00000010L

.field public static final BELOW_APP_WINDOWS:J = 0x20b00000007L

.field public static final CAN_SHOW_IME:J = 0x10800000020L

.field public static final CHANGING_APPS:J = 0x20b00000013L

.field public static final CLOSING_APPS:J = 0x20b00000012L

.field public static final CURRENT_FOCUS:J = 0x10b0000001eL

.field public static final DISPLAY_FRAMES:J = 0x10b0000000dL

.field public static final DISPLAY_INFO:J = 0x10b0000000aL

.field public static final DISPLAY_READY:J = 0x1080000001aL

.field public static final DISPLAY_ROTATION:J = 0x10b00000021L

.field public static final DOCKED_TASK_DIVIDER_CONTROLLER:J = 0x10b00000004L

.field public static final DPI:J = 0x10500000009L

.field public static final FOCUSED_APP:J = 0x1090000000fL

.field public static final FOCUSED_ROOT_TASK_ID:J = 0x10500000017L

.field public static final ID:J = 0x10500000002L

.field public static final IME_INSETS_SOURCE_PROVIDER:J = 0x10b0000001fL

.field public static final IME_POLICY:J = 0x10500000022L

.field public static final IME_WINDOWS:J = 0x20b00000008L

.field public static final INPUT_METHOD_CONTROL_TARGET:J = 0x10b0000001dL

.field public static final INPUT_METHOD_INPUT_TARGET:J = 0x10b0000001cL

.field public static final INPUT_METHOD_TARGET:J = 0x10b0000001bL

.field public static final INSETS_SOURCE_PROVIDERS:J = 0x20b00000023L

.field public static final IS_SLEEPING:J = 0x10800000024L

.field public static final KEEP_CLEAR_AREAS:J = 0x20b00000026L

.field public static final MIN_SIZE_OF_RESIZEABLE_TASK_DP:J = 0x10500000027L

.field public static final OPENING_APPS:J = 0x20b00000011L

.field public static final OVERLAY_WINDOWS:J = 0x20b00000014L

.field public static final PINNED_TASK_CONTROLLER:J = 0x10b00000005L

.field public static final RESUMED_ACTIVITY:J = 0x10b00000018L

.field public static final ROOT_DISPLAY_AREA:J = 0x10b00000015L

.field public static final ROTATION:J = 0x1050000000bL

.field public static final SCREEN_ROTATION_ANIMATION:J = 0x10b0000000cL

.field public static final SINGLE_TASK_INSTANCE:J = 0x10800000016L

.field public static final SLEEP_TOKENS:J = 0x20900000025L

.field public static final SURFACE_SIZE:J = 0x1050000000eL

.field public static final TASKS:J = 0x20b00000019L

.field public static final WINDOW_CONTAINER:J = 0x10b00000001L


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
