.class public final Lcom/android/server/wm/WindowManagerServiceDumpProto;
.super Ljava/lang/Object;


# static fields
.field public static final DISPLAY_FROZEN:J = 0x10800000006L

.field public static final FOCUSED_APP:J = 0x10900000004L

.field public static final FOCUSED_DISPLAY_ID:J = 0x10500000009L

.field public static final FOCUSED_WINDOW:J = 0x10b00000003L

.field public static final HARD_KEYBOARD_AVAILABLE:J = 0x1080000000aL

.field public static final INPUT_METHOD_WINDOW:J = 0x10b00000005L

.field public static final LAST_ORIENTATION:J = 0x10500000008L

.field public static final POLICY:J = 0x10b00000001L

.field public static final ROOT_WINDOW_CONTAINER:J = 0x10b00000002L

.field public static final ROTATION:J = 0x10500000007L

.field public static final WINDOW_FRAMES_VALID:J = 0x1080000000bL


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
