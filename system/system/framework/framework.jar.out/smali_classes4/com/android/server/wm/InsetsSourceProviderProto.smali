.class public final Lcom/android/server/wm/InsetsSourceProviderProto;
.super Ljava/lang/Object;


# static fields
.field public static final CAPTURED_LEASH:J = 0x10b00000008L

.field public static final CLIENT_VISIBLE:J = 0x1080000000bL

.field public static final CONTROL:J = 0x10b00000004L

.field public static final CONTROLLABLE:J = 0x1080000000fL

.field public static final CONTROL_TARGET:J = 0x10b00000005L

.field public static final FAKE_CONTROL:J = 0x10b00000003L

.field public static final FAKE_CONTROL_TARGET:J = 0x10b00000007L

.field public static final FINISH_SEAMLESS_ROTATE_FRAME_NUMBER:J = 0x1030000000eL

.field public static final FRAME:J = 0x10b00000002L

.field public static final IME_OVERRIDDEN_FRAME:J = 0x10b00000009L

.field public static final IS_LEASH_READY_FOR_DISPATCHING:J = 0x1080000000aL

.field public static final PENDING_CONTROL_TARGET:J = 0x10b00000006L

.field public static final SEAMLESS_ROTATING:J = 0x1080000000dL

.field public static final SERVER_VISIBLE:J = 0x1080000000cL

.field public static final SOURCE:J = 0x10b00000001L


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
