.class public final Lcom/android/server/people/ConversationInfoProto;
.super Ljava/lang/Object;


# static fields
.field public static final CONTACT_PHONE_NUMBER:J = 0x10900000007L

.field public static final CONTACT_URI:J = 0x10900000003L

.field public static final CONVERSATION_FLAGS:J = 0x10500000006L

.field public static final LAST_EVENT_TIMESTAMP:J = 0x10300000009L

.field public static final LOCUS_ID_PROTO:J = 0x10b00000002L

.field public static final NOTIFICATION_CHANNEL_ID:J = 0x10900000004L

.field public static final PARENT_NOTIFICATION_CHANNEL_ID:J = 0x10900000008L

.field public static final SHORTCUT_FLAGS:J = 0x10500000005L

.field public static final SHORTCUT_ID:J = 0x10900000001L


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
