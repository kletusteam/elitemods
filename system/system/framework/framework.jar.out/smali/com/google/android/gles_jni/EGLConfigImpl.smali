.class public Lcom/google/android/gles_jni/EGLConfigImpl;
.super Ljavax/microedition/khronos/egl/EGLConfig;


# instance fields
.field private mEGLConfig:J


# direct methods
.method constructor <init>(J)V
    .locals 0

    invoke-direct {p0}, Ljavax/microedition/khronos/egl/EGLConfig;-><init>()V

    iput-wide p1, p0, Lcom/google/android/gles_jni/EGLConfigImpl;->mEGLConfig:J

    return-void
.end method


# virtual methods
.method get()J
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    return-wide v0

    :goto_1
    iget-wide v0, p0, Lcom/google/android/gles_jni/EGLConfigImpl;->mEGLConfig:J

    goto/32 :goto_0

    nop
.end method
