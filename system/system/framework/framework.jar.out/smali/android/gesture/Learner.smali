.class abstract Landroid/gesture/Learner;
.super Ljava/lang/Object;


# instance fields
.field private final mInstances:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/gesture/Instance;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/gesture/Learner;->mInstances:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method addInstance(Landroid/gesture/Instance;)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/gesture/Learner;->mInstances:Ljava/util/ArrayList;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method

.method abstract classify(II[F)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II[F)",
            "Ljava/util/ArrayList<",
            "Landroid/gesture/Prediction;",
            ">;"
        }
    .end annotation
.end method

.method getInstances()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Landroid/gesture/Instance;",
            ">;"
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Landroid/gesture/Learner;->mInstances:Ljava/util/ArrayList;

    goto/32 :goto_0

    nop
.end method

.method removeInstance(J)V
    .locals 6

    goto/32 :goto_5

    nop

    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_c

    nop

    :goto_1
    check-cast v3, Landroid/gesture/Instance;

    goto/32 :goto_8

    nop

    :goto_2
    goto :goto_d

    :goto_3
    goto/32 :goto_7

    nop

    :goto_4
    if-lt v2, v1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_f

    nop

    :goto_5
    iget-object v0, p0, Landroid/gesture/Learner;->mInstances:Ljava/util/ArrayList;

    goto/32 :goto_0

    nop

    :goto_6
    if-eqz v4, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_e

    nop

    :goto_7
    return-void

    :goto_8
    iget-wide v4, v3, Landroid/gesture/Instance;->id:J

    goto/32 :goto_10

    nop

    :goto_9
    return-void

    :goto_a
    goto/32 :goto_b

    nop

    :goto_b
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_2

    nop

    :goto_c
    const/4 v2, 0x0

    :goto_d
    goto/32 :goto_4

    nop

    :goto_e
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/32 :goto_9

    nop

    :goto_f
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_1

    nop

    :goto_10
    cmp-long v4, p1, v4

    goto/32 :goto_6

    nop
.end method

.method removeInstances(Ljava/lang/String;)V
    .locals 6

    goto/32 :goto_17

    nop

    :goto_0
    iget-object v5, v4, Landroid/gesture/Instance;->label:Ljava/lang/String;

    goto/32 :goto_6

    nop

    :goto_1
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_e

    nop

    :goto_2
    goto :goto_16

    :goto_3
    goto/32 :goto_19

    nop

    :goto_4
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    goto/32 :goto_15

    nop

    :goto_5
    check-cast v4, Landroid/gesture/Instance;

    goto/32 :goto_9

    nop

    :goto_6
    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    goto/32 :goto_13

    nop

    :goto_7
    iget-object v5, v4, Landroid/gesture/Instance;->label:Ljava/lang/String;

    goto/32 :goto_8

    nop

    :goto_8
    if-nez v5, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_0

    nop

    :goto_9
    iget-object v5, v4, Landroid/gesture/Instance;->label:Ljava/lang/String;

    goto/32 :goto_d

    nop

    :goto_a
    if-nez p1, :cond_1

    goto/32 :goto_14

    :cond_1
    :goto_b
    goto/32 :goto_7

    nop

    :goto_c
    add-int/lit8 v3, v3, 0x1

    goto/32 :goto_2

    nop

    :goto_d
    if-eqz v5, :cond_2

    goto/32 :goto_b

    :cond_2
    goto/32 :goto_a

    nop

    :goto_e
    iget-object v1, p0, Landroid/gesture/Learner;->mInstances:Ljava/util/ArrayList;

    goto/32 :goto_4

    nop

    :goto_f
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_5

    nop

    :goto_10
    return-void

    :goto_11
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_12
    goto/32 :goto_c

    nop

    :goto_13
    if-nez v5, :cond_3

    goto/32 :goto_12

    :cond_3
    :goto_14
    goto/32 :goto_11

    nop

    :goto_15
    const/4 v3, 0x0

    :goto_16
    goto/32 :goto_18

    nop

    :goto_17
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_1

    nop

    :goto_18
    if-lt v3, v2, :cond_4

    goto/32 :goto_3

    :cond_4
    goto/32 :goto_f

    nop

    :goto_19
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    goto/32 :goto_10

    nop
.end method
