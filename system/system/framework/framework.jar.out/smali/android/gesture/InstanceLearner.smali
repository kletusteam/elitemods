.class Landroid/gesture/InstanceLearner;
.super Landroid/gesture/Learner;


# static fields
.field private static final sComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Landroid/gesture/Prediction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/gesture/InstanceLearner$1;

    invoke-direct {v0}, Landroid/gesture/InstanceLearner$1;-><init>()V

    sput-object v0, Landroid/gesture/InstanceLearner;->sComparator:Ljava/util/Comparator;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/gesture/Learner;-><init>()V

    return-void
.end method


# virtual methods
.method classify(II[F)Ljava/util/ArrayList;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II[F)",
            "Ljava/util/ArrayList<",
            "Landroid/gesture/Prediction;",
            ">;"
        }
    .end annotation

    goto/32 :goto_18

    nop

    :goto_0
    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_5

    nop

    :goto_1
    invoke-virtual {v4}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v5

    goto/32 :goto_15

    nop

    :goto_2
    goto/16 :goto_2b

    :goto_3
    goto/32 :goto_47

    nop

    :goto_4
    array-length v8, v0

    goto/32 :goto_27

    nop

    :goto_5
    goto :goto_16

    :goto_6
    goto/32 :goto_12

    nop

    :goto_7
    check-cast v6, Landroid/gesture/Instance;

    goto/32 :goto_4c

    nop

    :goto_8
    array-length v7, v7

    goto/32 :goto_4

    nop

    :goto_9
    const/4 v7, 0x2

    goto/32 :goto_26

    nop

    :goto_a
    div-double/2addr v12, v10

    :goto_b
    goto/32 :goto_30

    nop

    :goto_c
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    goto/32 :goto_1f

    nop

    :goto_d
    invoke-static {v7, v0, v9}, Landroid/gesture/GestureUtils;->minimumCosineDistance([F[FI)F

    move-result v7

    goto/32 :goto_2d

    nop

    :goto_e
    invoke-virtual/range {p0 .. p0}, Landroid/gesture/InstanceLearner;->getInstances()Ljava/util/ArrayList;

    move-result-object v2

    goto/32 :goto_f

    nop

    :goto_f
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    goto/32 :goto_2f

    nop

    :goto_10
    if-nez v7, :cond_0

    goto/32 :goto_23

    :cond_0
    goto/32 :goto_29

    nop

    :goto_11
    new-instance v1, Ljava/util/ArrayList;

    goto/32 :goto_3f

    nop

    :goto_12
    sget-object v5, Landroid/gesture/InstanceLearner;->sComparator:Ljava/util/Comparator;

    goto/32 :goto_1a

    nop

    :goto_13
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    goto/32 :goto_7

    nop

    :goto_14
    move/from16 v9, p2

    goto/32 :goto_d

    nop

    :goto_15
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_16
    goto/32 :goto_19

    nop

    :goto_17
    cmpl-double v7, v10, v12

    goto/32 :goto_33

    nop

    :goto_18
    move-object/from16 v0, p3

    goto/32 :goto_11

    nop

    :goto_19
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    goto/32 :goto_21

    nop

    :goto_1a
    invoke-static {v1, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto/32 :goto_28

    nop

    :goto_1b
    goto :goto_b

    :goto_1c
    goto/32 :goto_31

    nop

    :goto_1d
    invoke-direct {v7, v6, v10, v11}, Landroid/gesture/Prediction;-><init>(Ljava/lang/String;D)V

    goto/32 :goto_0

    nop

    :goto_1e
    iget-object v14, v6, Landroid/gesture/Instance;->label:Ljava/lang/String;

    goto/32 :goto_37

    nop

    :goto_1f
    check-cast v6, Ljava/lang/String;

    goto/32 :goto_2e

    nop

    :goto_20
    check-cast v7, Ljava/lang/Double;

    goto/32 :goto_48

    nop

    :goto_21
    if-nez v6, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_c

    nop

    :goto_22
    if-gtz v14, :cond_2

    goto/32 :goto_46

    :cond_2
    :goto_23
    goto/32 :goto_1e

    nop

    :goto_24
    new-instance v7, Landroid/gesture/Prediction;

    goto/32 :goto_1d

    nop

    :goto_25
    move/from16 v9, p2

    goto/32 :goto_38

    nop

    :goto_26
    move/from16 v8, p1

    goto/32 :goto_42

    nop

    :goto_27
    if-ne v7, v8, :cond_3

    goto/32 :goto_39

    :cond_3
    goto/32 :goto_49

    nop

    :goto_28
    return-object v1

    :goto_29
    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v14

    goto/32 :goto_3a

    nop

    :goto_2a
    const/4 v5, 0x0

    :goto_2b
    goto/32 :goto_36

    nop

    :goto_2c
    iget-object v7, v6, Landroid/gesture/Instance;->vector:[F

    goto/32 :goto_14

    nop

    :goto_2d
    float-to-double v10, v7

    goto/32 :goto_43

    nop

    :goto_2e
    invoke-virtual {v4, v6}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    goto/32 :goto_20

    nop

    :goto_2f
    new-instance v4, Ljava/util/TreeMap;

    goto/32 :goto_32

    nop

    :goto_30
    iget-object v7, v6, Landroid/gesture/Instance;->label:Ljava/lang/String;

    goto/32 :goto_3c

    nop

    :goto_31
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    goto/32 :goto_a

    nop

    :goto_32
    invoke-direct {v4}, Ljava/util/TreeMap;-><init>()V

    goto/32 :goto_2a

    nop

    :goto_33
    if-eqz v7, :cond_4

    goto/32 :goto_1c

    :cond_4
    goto/32 :goto_3b

    nop

    :goto_34
    float-to-double v10, v7

    :goto_35
    goto/32 :goto_4d

    nop

    :goto_36
    if-lt v5, v3, :cond_5

    goto/32 :goto_3

    :cond_5
    goto/32 :goto_13

    nop

    :goto_37
    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v15

    goto/32 :goto_45

    nop

    :goto_38
    goto :goto_46

    :goto_39
    goto/32 :goto_9

    nop

    :goto_3a
    cmpl-double v14, v12, v14

    goto/32 :goto_22

    nop

    :goto_3b
    const-wide v12, 0x7fefffffffffffffL    # Double.MAX_VALUE

    goto/32 :goto_1b

    nop

    :goto_3c
    invoke-virtual {v4, v7}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    goto/32 :goto_3e

    nop

    :goto_3d
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_2

    nop

    :goto_3e
    check-cast v7, Ljava/lang/Double;

    goto/32 :goto_10

    nop

    :goto_3f
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_e

    nop

    :goto_40
    move/from16 v9, p2

    goto/32 :goto_4a

    nop

    :goto_41
    invoke-static {v7, v0}, Landroid/gesture/GestureUtils;->squaredEuclideanDistance([F[F)F

    move-result v7

    goto/32 :goto_34

    nop

    :goto_42
    if-eq v8, v7, :cond_6

    goto/32 :goto_44

    :cond_6
    goto/32 :goto_2c

    nop

    :goto_43
    goto :goto_35

    :goto_44
    goto/32 :goto_40

    nop

    :goto_45
    invoke-virtual {v4, v14, v15}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_46
    goto/32 :goto_3d

    nop

    :goto_47
    move/from16 v8, p1

    goto/32 :goto_4b

    nop

    :goto_48
    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    goto/32 :goto_24

    nop

    :goto_49
    move/from16 v8, p1

    goto/32 :goto_25

    nop

    :goto_4a
    iget-object v7, v6, Landroid/gesture/Instance;->vector:[F

    goto/32 :goto_41

    nop

    :goto_4b
    move/from16 v9, p2

    goto/32 :goto_1

    nop

    :goto_4c
    iget-object v7, v6, Landroid/gesture/Instance;->vector:[F

    goto/32 :goto_8

    nop

    :goto_4d
    const-wide/16 v12, 0x0

    goto/32 :goto_17

    nop
.end method
