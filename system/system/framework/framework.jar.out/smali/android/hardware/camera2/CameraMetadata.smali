.class public abstract Landroid/hardware/camera2/CameraMetadata;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TKey:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final AUTOMOTIVE_LENS_FACING_EXTERIOR_FRONT:I = 0x1

.field public static final AUTOMOTIVE_LENS_FACING_EXTERIOR_LEFT:I = 0x3

.field public static final AUTOMOTIVE_LENS_FACING_EXTERIOR_OTHER:I = 0x0

.field public static final AUTOMOTIVE_LENS_FACING_EXTERIOR_REAR:I = 0x2

.field public static final AUTOMOTIVE_LENS_FACING_EXTERIOR_RIGHT:I = 0x4

.field public static final AUTOMOTIVE_LENS_FACING_INTERIOR_OTHER:I = 0x5

.field public static final AUTOMOTIVE_LENS_FACING_INTERIOR_SEAT_ROW_1_CENTER:I = 0x7

.field public static final AUTOMOTIVE_LENS_FACING_INTERIOR_SEAT_ROW_1_LEFT:I = 0x6

.field public static final AUTOMOTIVE_LENS_FACING_INTERIOR_SEAT_ROW_1_RIGHT:I = 0x8

.field public static final AUTOMOTIVE_LENS_FACING_INTERIOR_SEAT_ROW_2_CENTER:I = 0xa

.field public static final AUTOMOTIVE_LENS_FACING_INTERIOR_SEAT_ROW_2_LEFT:I = 0x9

.field public static final AUTOMOTIVE_LENS_FACING_INTERIOR_SEAT_ROW_2_RIGHT:I = 0xb

.field public static final AUTOMOTIVE_LENS_FACING_INTERIOR_SEAT_ROW_3_CENTER:I = 0xd

.field public static final AUTOMOTIVE_LENS_FACING_INTERIOR_SEAT_ROW_3_LEFT:I = 0xc

.field public static final AUTOMOTIVE_LENS_FACING_INTERIOR_SEAT_ROW_3_RIGHT:I = 0xe

.field public static final AUTOMOTIVE_LOCATION_EXTERIOR_FRONT:I = 0x2

.field public static final AUTOMOTIVE_LOCATION_EXTERIOR_LEFT:I = 0x4

.field public static final AUTOMOTIVE_LOCATION_EXTERIOR_OTHER:I = 0x1

.field public static final AUTOMOTIVE_LOCATION_EXTERIOR_REAR:I = 0x3

.field public static final AUTOMOTIVE_LOCATION_EXTERIOR_RIGHT:I = 0x5

.field public static final AUTOMOTIVE_LOCATION_EXTRA_FRONT:I = 0x7

.field public static final AUTOMOTIVE_LOCATION_EXTRA_LEFT:I = 0x9

.field public static final AUTOMOTIVE_LOCATION_EXTRA_OTHER:I = 0x6

.field public static final AUTOMOTIVE_LOCATION_EXTRA_REAR:I = 0x8

.field public static final AUTOMOTIVE_LOCATION_EXTRA_RIGHT:I = 0xa

.field public static final AUTOMOTIVE_LOCATION_INTERIOR:I = 0x0

.field public static final COLOR_CORRECTION_ABERRATION_MODE_FAST:I = 0x1

.field public static final COLOR_CORRECTION_ABERRATION_MODE_HIGH_QUALITY:I = 0x2

.field public static final COLOR_CORRECTION_ABERRATION_MODE_OFF:I = 0x0

.field public static final COLOR_CORRECTION_MODE_FAST:I = 0x1

.field public static final COLOR_CORRECTION_MODE_HIGH_QUALITY:I = 0x2

.field public static final COLOR_CORRECTION_MODE_TRANSFORM_MATRIX:I = 0x0

.field public static final CONTROL_AE_ANTIBANDING_MODE_50HZ:I = 0x1

.field public static final CONTROL_AE_ANTIBANDING_MODE_60HZ:I = 0x2

.field public static final CONTROL_AE_ANTIBANDING_MODE_AUTO:I = 0x3

.field public static final CONTROL_AE_ANTIBANDING_MODE_OFF:I = 0x0

.field public static final CONTROL_AE_MODE_OFF:I = 0x0

.field public static final CONTROL_AE_MODE_ON:I = 0x1

.field public static final CONTROL_AE_MODE_ON_ALWAYS_FLASH:I = 0x3

.field public static final CONTROL_AE_MODE_ON_AUTO_FLASH:I = 0x2

.field public static final CONTROL_AE_MODE_ON_AUTO_FLASH_REDEYE:I = 0x4

.field public static final CONTROL_AE_MODE_ON_EXTERNAL_FLASH:I = 0x5

.field public static final CONTROL_AE_PRECAPTURE_TRIGGER_CANCEL:I = 0x2

.field public static final CONTROL_AE_PRECAPTURE_TRIGGER_IDLE:I = 0x0

.field public static final CONTROL_AE_PRECAPTURE_TRIGGER_START:I = 0x1

.field public static final CONTROL_AE_STATE_CONVERGED:I = 0x2

.field public static final CONTROL_AE_STATE_FLASH_REQUIRED:I = 0x4

.field public static final CONTROL_AE_STATE_INACTIVE:I = 0x0

.field public static final CONTROL_AE_STATE_LOCKED:I = 0x3

.field public static final CONTROL_AE_STATE_PRECAPTURE:I = 0x5

.field public static final CONTROL_AE_STATE_SEARCHING:I = 0x1

.field public static final CONTROL_AF_MODE_AUTO:I = 0x1

.field public static final CONTROL_AF_MODE_CONTINUOUS_PICTURE:I = 0x4

.field public static final CONTROL_AF_MODE_CONTINUOUS_VIDEO:I = 0x3

.field public static final CONTROL_AF_MODE_EDOF:I = 0x5

.field public static final CONTROL_AF_MODE_MACRO:I = 0x2

.field public static final CONTROL_AF_MODE_OFF:I = 0x0

.field public static final CONTROL_AF_SCENE_CHANGE_DETECTED:I = 0x1

.field public static final CONTROL_AF_SCENE_CHANGE_NOT_DETECTED:I = 0x0

.field public static final CONTROL_AF_STATE_ACTIVE_SCAN:I = 0x3

.field public static final CONTROL_AF_STATE_FOCUSED_LOCKED:I = 0x4

.field public static final CONTROL_AF_STATE_INACTIVE:I = 0x0

.field public static final CONTROL_AF_STATE_NOT_FOCUSED_LOCKED:I = 0x5

.field public static final CONTROL_AF_STATE_PASSIVE_FOCUSED:I = 0x2

.field public static final CONTROL_AF_STATE_PASSIVE_SCAN:I = 0x1

.field public static final CONTROL_AF_STATE_PASSIVE_UNFOCUSED:I = 0x6

.field public static final CONTROL_AF_TRIGGER_CANCEL:I = 0x2

.field public static final CONTROL_AF_TRIGGER_IDLE:I = 0x0

.field public static final CONTROL_AF_TRIGGER_START:I = 0x1

.field public static final CONTROL_AWB_MODE_AUTO:I = 0x1

.field public static final CONTROL_AWB_MODE_CLOUDY_DAYLIGHT:I = 0x6

.field public static final CONTROL_AWB_MODE_DAYLIGHT:I = 0x5

.field public static final CONTROL_AWB_MODE_FLUORESCENT:I = 0x3

.field public static final CONTROL_AWB_MODE_INCANDESCENT:I = 0x2

.field public static final CONTROL_AWB_MODE_OFF:I = 0x0

.field public static final CONTROL_AWB_MODE_SHADE:I = 0x8

.field public static final CONTROL_AWB_MODE_TWILIGHT:I = 0x7

.field public static final CONTROL_AWB_MODE_WARM_FLUORESCENT:I = 0x4

.field public static final CONTROL_AWB_STATE_CONVERGED:I = 0x2

.field public static final CONTROL_AWB_STATE_INACTIVE:I = 0x0

.field public static final CONTROL_AWB_STATE_LOCKED:I = 0x3

.field public static final CONTROL_AWB_STATE_SEARCHING:I = 0x1

.field public static final CONTROL_CAPTURE_INTENT_CUSTOM:I = 0x0

.field public static final CONTROL_CAPTURE_INTENT_MANUAL:I = 0x6

.field public static final CONTROL_CAPTURE_INTENT_MOTION_TRACKING:I = 0x7

.field public static final CONTROL_CAPTURE_INTENT_PREVIEW:I = 0x1

.field public static final CONTROL_CAPTURE_INTENT_STILL_CAPTURE:I = 0x2

.field public static final CONTROL_CAPTURE_INTENT_VIDEO_RECORD:I = 0x3

.field public static final CONTROL_CAPTURE_INTENT_VIDEO_SNAPSHOT:I = 0x4

.field public static final CONTROL_CAPTURE_INTENT_ZERO_SHUTTER_LAG:I = 0x5

.field public static final CONTROL_EFFECT_MODE_AQUA:I = 0x8

.field public static final CONTROL_EFFECT_MODE_BLACKBOARD:I = 0x7

.field public static final CONTROL_EFFECT_MODE_MONO:I = 0x1

.field public static final CONTROL_EFFECT_MODE_NEGATIVE:I = 0x2

.field public static final CONTROL_EFFECT_MODE_OFF:I = 0x0

.field public static final CONTROL_EFFECT_MODE_POSTERIZE:I = 0x5

.field public static final CONTROL_EFFECT_MODE_SEPIA:I = 0x4

.field public static final CONTROL_EFFECT_MODE_SOLARIZE:I = 0x3

.field public static final CONTROL_EFFECT_MODE_WHITEBOARD:I = 0x6

.field public static final CONTROL_EXTENDED_SCENE_MODE_BOKEH_CONTINUOUS:I = 0x2

.field public static final CONTROL_EXTENDED_SCENE_MODE_BOKEH_STILL_CAPTURE:I = 0x1

.field public static final CONTROL_EXTENDED_SCENE_MODE_DISABLED:I = 0x0

.field public static final CONTROL_EXTENDED_SCENE_MODE_VENDOR_START:I = 0x40

.field public static final CONTROL_MODE_AUTO:I = 0x1

.field public static final CONTROL_MODE_OFF:I = 0x0

.field public static final CONTROL_MODE_OFF_KEEP_STATE:I = 0x3

.field public static final CONTROL_MODE_USE_EXTENDED_SCENE_MODE:I = 0x4

.field public static final CONTROL_MODE_USE_SCENE_MODE:I = 0x2

.field public static final CONTROL_SCENE_MODE_ACTION:I = 0x2

.field public static final CONTROL_SCENE_MODE_BARCODE:I = 0x10

.field public static final CONTROL_SCENE_MODE_BEACH:I = 0x8

.field public static final CONTROL_SCENE_MODE_CANDLELIGHT:I = 0xf

.field public static final CONTROL_SCENE_MODE_DEVICE_CUSTOM_END:I = 0x7f

.field public static final CONTROL_SCENE_MODE_DEVICE_CUSTOM_START:I = 0x64

.field public static final CONTROL_SCENE_MODE_DISABLED:I = 0x0

.field public static final CONTROL_SCENE_MODE_FACE_PRIORITY:I = 0x1

.field public static final CONTROL_SCENE_MODE_FACE_PRIORITY_LOW_LIGHT:I = 0x13

.field public static final CONTROL_SCENE_MODE_FIREWORKS:I = 0xc

.field public static final CONTROL_SCENE_MODE_HDR:I = 0x12

.field public static final CONTROL_SCENE_MODE_HIGH_SPEED_VIDEO:I = 0x11
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CONTROL_SCENE_MODE_LANDSCAPE:I = 0x4

.field public static final CONTROL_SCENE_MODE_NIGHT:I = 0x5

.field public static final CONTROL_SCENE_MODE_NIGHT_PORTRAIT:I = 0x6

.field public static final CONTROL_SCENE_MODE_PARTY:I = 0xe

.field public static final CONTROL_SCENE_MODE_PORTRAIT:I = 0x3

.field public static final CONTROL_SCENE_MODE_SNOW:I = 0x9

.field public static final CONTROL_SCENE_MODE_SPORTS:I = 0xd

.field public static final CONTROL_SCENE_MODE_STEADYPHOTO:I = 0xb

.field public static final CONTROL_SCENE_MODE_SUNSET:I = 0xa

.field public static final CONTROL_SCENE_MODE_THEATRE:I = 0x7

.field public static final CONTROL_VIDEO_STABILIZATION_MODE_OFF:I = 0x0

.field public static final CONTROL_VIDEO_STABILIZATION_MODE_ON:I = 0x1

.field public static final CONTROL_VIDEO_STABILIZATION_MODE_PREVIEW_STABILIZATION:I = 0x2

.field private static final DEBUG:Z

.field public static final DISTORTION_CORRECTION_MODE_FAST:I = 0x1

.field public static final DISTORTION_CORRECTION_MODE_HIGH_QUALITY:I = 0x2

.field public static final DISTORTION_CORRECTION_MODE_OFF:I = 0x0

.field public static final EDGE_MODE_FAST:I = 0x1

.field public static final EDGE_MODE_HIGH_QUALITY:I = 0x2

.field public static final EDGE_MODE_OFF:I = 0x0

.field public static final EDGE_MODE_ZERO_SHUTTER_LAG:I = 0x3

.field public static final FLASH_MODE_OFF:I = 0x0

.field public static final FLASH_MODE_SINGLE:I = 0x1

.field public static final FLASH_MODE_TORCH:I = 0x2

.field public static final FLASH_STATE_CHARGING:I = 0x1

.field public static final FLASH_STATE_FIRED:I = 0x3

.field public static final FLASH_STATE_PARTIAL:I = 0x4

.field public static final FLASH_STATE_READY:I = 0x2

.field public static final FLASH_STATE_UNAVAILABLE:I = 0x0

.field public static final HOT_PIXEL_MODE_FAST:I = 0x1

.field public static final HOT_PIXEL_MODE_HIGH_QUALITY:I = 0x2

.field public static final HOT_PIXEL_MODE_OFF:I = 0x0

.field public static final INFO_SUPPORTED_HARDWARE_LEVEL_3:I = 0x3

.field public static final INFO_SUPPORTED_HARDWARE_LEVEL_EXTERNAL:I = 0x4

.field public static final INFO_SUPPORTED_HARDWARE_LEVEL_FULL:I = 0x1

.field public static final INFO_SUPPORTED_HARDWARE_LEVEL_LEGACY:I = 0x2

.field public static final INFO_SUPPORTED_HARDWARE_LEVEL_LIMITED:I = 0x0

.field public static final LED_AVAILABLE_LEDS_TRANSMIT:I = 0x0

.field public static final LENS_FACING_BACK:I = 0x1

.field public static final LENS_FACING_EXTERNAL:I = 0x2

.field public static final LENS_FACING_FRONT:I = 0x0

.field public static final LENS_INFO_FOCUS_DISTANCE_CALIBRATION_APPROXIMATE:I = 0x1

.field public static final LENS_INFO_FOCUS_DISTANCE_CALIBRATION_CALIBRATED:I = 0x2

.field public static final LENS_INFO_FOCUS_DISTANCE_CALIBRATION_UNCALIBRATED:I = 0x0

.field public static final LENS_OPTICAL_STABILIZATION_MODE_OFF:I = 0x0

.field public static final LENS_OPTICAL_STABILIZATION_MODE_ON:I = 0x1

.field public static final LENS_POSE_REFERENCE_AUTOMOTIVE:I = 0x3

.field public static final LENS_POSE_REFERENCE_GYROSCOPE:I = 0x1

.field public static final LENS_POSE_REFERENCE_PRIMARY_CAMERA:I = 0x0

.field public static final LENS_POSE_REFERENCE_UNDEFINED:I = 0x2

.field public static final LENS_STATE_MOVING:I = 0x1

.field public static final LENS_STATE_STATIONARY:I = 0x0

.field public static final LOGICAL_MULTI_CAMERA_SENSOR_SYNC_TYPE_APPROXIMATE:I = 0x0

.field public static final LOGICAL_MULTI_CAMERA_SENSOR_SYNC_TYPE_CALIBRATED:I = 0x1

.field public static final NOISE_REDUCTION_MODE_FAST:I = 0x1

.field public static final NOISE_REDUCTION_MODE_HIGH_QUALITY:I = 0x2

.field public static final NOISE_REDUCTION_MODE_MINIMAL:I = 0x3

.field public static final NOISE_REDUCTION_MODE_OFF:I = 0x0

.field public static final NOISE_REDUCTION_MODE_ZERO_SHUTTER_LAG:I = 0x4

.field public static final REQUEST_AVAILABLE_CAPABILITIES_BACKWARD_COMPATIBLE:I = 0x0

.field public static final REQUEST_AVAILABLE_CAPABILITIES_BURST_CAPTURE:I = 0x6

.field public static final REQUEST_AVAILABLE_CAPABILITIES_CONSTRAINED_HIGH_SPEED_VIDEO:I = 0x9

.field public static final REQUEST_AVAILABLE_CAPABILITIES_DEPTH_OUTPUT:I = 0x8

.field public static final REQUEST_AVAILABLE_CAPABILITIES_DYNAMIC_RANGE_TEN_BIT:I = 0x12

.field public static final REQUEST_AVAILABLE_CAPABILITIES_LOGICAL_MULTI_CAMERA:I = 0xb

.field public static final REQUEST_AVAILABLE_CAPABILITIES_MANUAL_POST_PROCESSING:I = 0x2

.field public static final REQUEST_AVAILABLE_CAPABILITIES_MANUAL_SENSOR:I = 0x1

.field public static final REQUEST_AVAILABLE_CAPABILITIES_MONOCHROME:I = 0xc

.field public static final REQUEST_AVAILABLE_CAPABILITIES_MOTION_TRACKING:I = 0xa

.field public static final REQUEST_AVAILABLE_CAPABILITIES_OFFLINE_PROCESSING:I = 0xf

.field public static final REQUEST_AVAILABLE_CAPABILITIES_PRIVATE_REPROCESSING:I = 0x4

.field public static final REQUEST_AVAILABLE_CAPABILITIES_RAW:I = 0x3

.field public static final REQUEST_AVAILABLE_CAPABILITIES_READ_SENSOR_SETTINGS:I = 0x5

.field public static final REQUEST_AVAILABLE_CAPABILITIES_REMOSAIC_REPROCESSING:I = 0x11

.field public static final REQUEST_AVAILABLE_CAPABILITIES_SECURE_IMAGE_DATA:I = 0xd

.field public static final REQUEST_AVAILABLE_CAPABILITIES_STREAM_USE_CASE:I = 0x13

.field public static final REQUEST_AVAILABLE_CAPABILITIES_SYSTEM_CAMERA:I = 0xe

.field public static final REQUEST_AVAILABLE_CAPABILITIES_ULTRA_HIGH_RESOLUTION_SENSOR:I = 0x10

.field public static final REQUEST_AVAILABLE_CAPABILITIES_YUV_REPROCESSING:I = 0x7

.field public static final REQUEST_AVAILABLE_DYNAMIC_RANGE_PROFILES_MAP_DOLBY_VISION_10B_HDR_OEM:I = 0x40

.field public static final REQUEST_AVAILABLE_DYNAMIC_RANGE_PROFILES_MAP_DOLBY_VISION_10B_HDR_OEM_PO:I = 0x80

.field public static final REQUEST_AVAILABLE_DYNAMIC_RANGE_PROFILES_MAP_DOLBY_VISION_10B_HDR_REF:I = 0x10

.field public static final REQUEST_AVAILABLE_DYNAMIC_RANGE_PROFILES_MAP_DOLBY_VISION_10B_HDR_REF_PO:I = 0x20

.field public static final REQUEST_AVAILABLE_DYNAMIC_RANGE_PROFILES_MAP_DOLBY_VISION_8B_HDR_OEM:I = 0x400

.field public static final REQUEST_AVAILABLE_DYNAMIC_RANGE_PROFILES_MAP_DOLBY_VISION_8B_HDR_OEM_PO:I = 0x800

.field public static final REQUEST_AVAILABLE_DYNAMIC_RANGE_PROFILES_MAP_DOLBY_VISION_8B_HDR_REF:I = 0x100

.field public static final REQUEST_AVAILABLE_DYNAMIC_RANGE_PROFILES_MAP_DOLBY_VISION_8B_HDR_REF_PO:I = 0x200

.field public static final REQUEST_AVAILABLE_DYNAMIC_RANGE_PROFILES_MAP_HDR10:I = 0x4

.field public static final REQUEST_AVAILABLE_DYNAMIC_RANGE_PROFILES_MAP_HDR10_PLUS:I = 0x8

.field public static final REQUEST_AVAILABLE_DYNAMIC_RANGE_PROFILES_MAP_HLG10:I = 0x2

.field public static final REQUEST_AVAILABLE_DYNAMIC_RANGE_PROFILES_MAP_MAX:I = 0x1000

.field public static final REQUEST_AVAILABLE_DYNAMIC_RANGE_PROFILES_MAP_STANDARD:I = 0x1

.field public static final SCALER_AVAILABLE_STREAM_USE_CASES_DEFAULT:I = 0x0

.field public static final SCALER_AVAILABLE_STREAM_USE_CASES_PREVIEW:I = 0x1

.field public static final SCALER_AVAILABLE_STREAM_USE_CASES_PREVIEW_VIDEO_STILL:I = 0x4

.field public static final SCALER_AVAILABLE_STREAM_USE_CASES_STILL_CAPTURE:I = 0x2

.field public static final SCALER_AVAILABLE_STREAM_USE_CASES_VENDOR_START:I = 0x10000

.field public static final SCALER_AVAILABLE_STREAM_USE_CASES_VIDEO_CALL:I = 0x5

.field public static final SCALER_AVAILABLE_STREAM_USE_CASES_VIDEO_RECORD:I = 0x3

.field public static final SCALER_CROPPING_TYPE_CENTER_ONLY:I = 0x0

.field public static final SCALER_CROPPING_TYPE_FREEFORM:I = 0x1

.field public static final SCALER_ROTATE_AND_CROP_180:I = 0x2

.field public static final SCALER_ROTATE_AND_CROP_270:I = 0x3

.field public static final SCALER_ROTATE_AND_CROP_90:I = 0x1

.field public static final SCALER_ROTATE_AND_CROP_AUTO:I = 0x4

.field public static final SCALER_ROTATE_AND_CROP_NONE:I = 0x0

.field public static final SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_BGGR:I = 0x3

.field public static final SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_GBRG:I = 0x2

.field public static final SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_GRBG:I = 0x1

.field public static final SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_MONO:I = 0x5

.field public static final SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_NIR:I = 0x6

.field public static final SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_RGB:I = 0x4

.field public static final SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_RGGB:I = 0x0

.field public static final SENSOR_INFO_TIMESTAMP_SOURCE_REALTIME:I = 0x1

.field public static final SENSOR_INFO_TIMESTAMP_SOURCE_UNKNOWN:I = 0x0

.field public static final SENSOR_PIXEL_MODE_DEFAULT:I = 0x0

.field public static final SENSOR_PIXEL_MODE_MAXIMUM_RESOLUTION:I = 0x1

.field public static final SENSOR_REFERENCE_ILLUMINANT1_CLOUDY_WEATHER:I = 0xa

.field public static final SENSOR_REFERENCE_ILLUMINANT1_COOL_WHITE_FLUORESCENT:I = 0xe

.field public static final SENSOR_REFERENCE_ILLUMINANT1_D50:I = 0x17

.field public static final SENSOR_REFERENCE_ILLUMINANT1_D55:I = 0x14

.field public static final SENSOR_REFERENCE_ILLUMINANT1_D65:I = 0x15

.field public static final SENSOR_REFERENCE_ILLUMINANT1_D75:I = 0x16

.field public static final SENSOR_REFERENCE_ILLUMINANT1_DAYLIGHT:I = 0x1

.field public static final SENSOR_REFERENCE_ILLUMINANT1_DAYLIGHT_FLUORESCENT:I = 0xc

.field public static final SENSOR_REFERENCE_ILLUMINANT1_DAY_WHITE_FLUORESCENT:I = 0xd

.field public static final SENSOR_REFERENCE_ILLUMINANT1_FINE_WEATHER:I = 0x9

.field public static final SENSOR_REFERENCE_ILLUMINANT1_FLASH:I = 0x4

.field public static final SENSOR_REFERENCE_ILLUMINANT1_FLUORESCENT:I = 0x2

.field public static final SENSOR_REFERENCE_ILLUMINANT1_ISO_STUDIO_TUNGSTEN:I = 0x18

.field public static final SENSOR_REFERENCE_ILLUMINANT1_SHADE:I = 0xb

.field public static final SENSOR_REFERENCE_ILLUMINANT1_STANDARD_A:I = 0x11

.field public static final SENSOR_REFERENCE_ILLUMINANT1_STANDARD_B:I = 0x12

.field public static final SENSOR_REFERENCE_ILLUMINANT1_STANDARD_C:I = 0x13

.field public static final SENSOR_REFERENCE_ILLUMINANT1_TUNGSTEN:I = 0x3

.field public static final SENSOR_REFERENCE_ILLUMINANT1_WHITE_FLUORESCENT:I = 0xf

.field public static final SENSOR_TEST_PATTERN_MODE_BLACK:I = 0x5

.field public static final SENSOR_TEST_PATTERN_MODE_COLOR_BARS:I = 0x2

.field public static final SENSOR_TEST_PATTERN_MODE_COLOR_BARS_FADE_TO_GRAY:I = 0x3

.field public static final SENSOR_TEST_PATTERN_MODE_CUSTOM1:I = 0x100

.field public static final SENSOR_TEST_PATTERN_MODE_OFF:I = 0x0

.field public static final SENSOR_TEST_PATTERN_MODE_PN9:I = 0x4

.field public static final SENSOR_TEST_PATTERN_MODE_SOLID_COLOR:I = 0x1

.field public static final SHADING_MODE_FAST:I = 0x1

.field public static final SHADING_MODE_HIGH_QUALITY:I = 0x2

.field public static final SHADING_MODE_OFF:I = 0x0

.field public static final STATISTICS_FACE_DETECT_MODE_FULL:I = 0x2

.field public static final STATISTICS_FACE_DETECT_MODE_OFF:I = 0x0

.field public static final STATISTICS_FACE_DETECT_MODE_SIMPLE:I = 0x1

.field public static final STATISTICS_LENS_SHADING_MAP_MODE_OFF:I = 0x0

.field public static final STATISTICS_LENS_SHADING_MAP_MODE_ON:I = 0x1

.field public static final STATISTICS_OIS_DATA_MODE_OFF:I = 0x0

.field public static final STATISTICS_OIS_DATA_MODE_ON:I = 0x1

.field public static final STATISTICS_SCENE_FLICKER_50HZ:I = 0x1

.field public static final STATISTICS_SCENE_FLICKER_60HZ:I = 0x2

.field public static final STATISTICS_SCENE_FLICKER_NONE:I = 0x0

.field public static final SYNC_FRAME_NUMBER_CONVERGING:I = -0x1

.field public static final SYNC_FRAME_NUMBER_UNKNOWN:I = -0x2

.field public static final SYNC_MAX_LATENCY_PER_FRAME_CONTROL:I = 0x0

.field public static final SYNC_MAX_LATENCY_UNKNOWN:I = -0x1

.field private static final TAG:Ljava/lang/String; = "CameraMetadataAb"

.field public static final TONEMAP_MODE_CONTRAST_CURVE:I = 0x0

.field public static final TONEMAP_MODE_FAST:I = 0x1

.field public static final TONEMAP_MODE_GAMMA_VALUE:I = 0x3

.field public static final TONEMAP_MODE_HIGH_QUALITY:I = 0x2

.field public static final TONEMAP_MODE_PRESET_CURVE:I = 0x4

.field public static final TONEMAP_PRESET_CURVE_REC709:I = 0x1

.field public static final TONEMAP_PRESET_CURVE_SRGB:I


# instance fields
.field private mNativeInstance:Landroid/hardware/camera2/impl/CameraMetadataNative;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "debug.camera.enable_log_framework"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "debug.camera.enable_log_metadata"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v1, 0x1

    :cond_1
    sput-boolean v1, Landroid/hardware/camera2/CameraMetadata;->DEBUG:Z

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/hardware/camera2/CameraMetadata;->mNativeInstance:Landroid/hardware/camera2/impl/CameraMetadataNative;

    return-void
.end method

.method private static shouldKeyBeAdded(Ljava/lang/Object;Ljava/lang/reflect/Field;[IZ)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TKey:",
            "Ljava/lang/Object;",
            ">(TTKey;",
            "Ljava/lang/reflect/Field;",
            "[IZ)Z"
        }
    .end annotation

    if-eqz p0, :cond_7

    instance-of v0, p0, Landroid/hardware/camera2/CameraCharacteristics$Key;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {v0}, Landroid/hardware/camera2/CameraCharacteristics$Key;->getNativeKey()Landroid/hardware/camera2/impl/CameraMetadataNative$Key;

    move-result-object v0

    goto :goto_0

    :cond_0
    instance-of v0, p0, Landroid/hardware/camera2/CaptureResult$Key;

    if-eqz v0, :cond_1

    move-object v0, p0

    check-cast v0, Landroid/hardware/camera2/CaptureResult$Key;

    invoke-virtual {v0}, Landroid/hardware/camera2/CaptureResult$Key;->getNativeKey()Landroid/hardware/camera2/impl/CameraMetadataNative$Key;

    move-result-object v0

    goto :goto_0

    :cond_1
    instance-of v0, p0, Landroid/hardware/camera2/CaptureRequest$Key;

    if-eqz v0, :cond_6

    move-object v0, p0

    check-cast v0, Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-virtual {v0}, Landroid/hardware/camera2/CaptureRequest$Key;->getNativeKey()Landroid/hardware/camera2/impl/CameraMetadataNative$Key;

    move-result-object v0

    :goto_0
    const-class v1, Landroid/hardware/camera2/impl/PublicKey;

    invoke-virtual {p1, v1}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v1

    const/4 v2, 0x0

    if-nez v1, :cond_2

    return v2

    :cond_2
    const/4 v1, 0x1

    if-nez p2, :cond_3

    return v1

    :cond_3
    const-class v3, Landroid/hardware/camera2/impl/SyntheticKey;

    invoke-virtual {p1, v3}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v3

    if-eqz v3, :cond_4

    return p3

    :cond_4
    invoke-virtual {v0}, Landroid/hardware/camera2/impl/CameraMetadataNative$Key;->getTag()I

    move-result v3

    invoke-static {p2, v3}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v4

    if-ltz v4, :cond_5

    move v2, v1

    :cond_5
    return v2

    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "key type must be that of a metadata key"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "key must not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method protected abstract getKeyClass()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "TTKey;>;"
        }
    .end annotation
.end method

.method getKeys(Ljava/lang/Class;Ljava/lang/Class;Landroid/hardware/camera2/CameraMetadata;[IZ)Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TKey:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/Class<",
            "TTKey;>;",
            "Landroid/hardware/camera2/CameraMetadata<",
            "TTKey;>;[IZ)",
            "Ljava/util/ArrayList<",
            "TTKey;>;"
        }
    .end annotation

    goto/32 :goto_54

    nop

    :goto_0
    goto/16 :goto_17

    :goto_1
    goto/32 :goto_2b

    nop

    :goto_2
    instance-of v5, v4, Landroid/hardware/camera2/CaptureResult$Key;

    goto/32 :goto_7f

    nop

    :goto_3
    if-nez v5, :cond_0

    goto/32 :goto_5f

    :cond_0
    goto/32 :goto_7c

    nop

    :goto_4
    if-nez v0, :cond_1

    goto/32 :goto_6d

    :cond_1
    goto/32 :goto_6c

    nop

    :goto_5
    invoke-direct {v3, v4, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/32 :goto_78

    nop

    :goto_6
    invoke-virtual {v5}, Landroid/hardware/camera2/CaptureRequest$Key;->getName()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_e

    nop

    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    goto/32 :goto_55

    nop

    :goto_8
    if-nez v1, :cond_2

    goto/32 :goto_7e

    :cond_2
    goto/32 :goto_16

    nop

    :goto_9
    instance-of v5, v4, Landroid/hardware/camera2/CaptureRequest$Key;

    goto/32 :goto_3

    nop

    :goto_a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    goto/32 :goto_73

    nop

    :goto_b
    const-string v4, "Can\'t get IllegalArgumentException"

    goto/32 :goto_24

    nop

    :goto_c
    sget-boolean v7, Landroid/hardware/camera2/CameraMetadata;->DEBUG:Z

    goto/32 :goto_6a

    nop

    :goto_d
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_4

    nop

    :goto_e
    move-object v6, v4

    goto/32 :goto_3a

    nop

    :goto_f
    check-cast v6, Landroid/hardware/camera2/CaptureResult$Key;

    goto/32 :goto_22

    nop

    :goto_10
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto/32 :goto_50

    nop

    :goto_11
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto/32 :goto_6b

    nop

    :goto_12
    if-nez v7, :cond_3

    goto/32 :goto_2e

    :cond_3
    goto/32 :goto_19

    nop

    :goto_13
    if-nez v8, :cond_4

    goto/32 :goto_61

    :cond_4
    :goto_14
    goto/32 :goto_60

    nop

    :goto_15
    invoke-virtual {p3, v6}, Landroid/hardware/camera2/CameraMetadata;->getProtected(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    goto/32 :goto_65

    nop

    :goto_16
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_17
    goto/32 :goto_7

    nop

    :goto_18
    invoke-virtual {v5}, Landroid/hardware/camera2/CameraCharacteristics$Key;->getName()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_51

    nop

    :goto_19
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_3f

    nop

    :goto_1a
    new-instance v3, Ljava/lang/AssertionError;

    goto/32 :goto_63

    nop

    :goto_1b
    invoke-virtual {p1}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v2

    goto/32 :goto_44

    nop

    :goto_1c
    if-nez p4, :cond_5

    goto/32 :goto_1e

    :cond_5
    goto/32 :goto_1d

    nop

    :goto_1d
    invoke-static {p4}, Ljava/util/Arrays;->sort([I)V

    :goto_1e
    goto/32 :goto_77

    nop

    :goto_1f
    if-nez v6, :cond_6

    goto/32 :goto_79

    :cond_6
    goto/32 :goto_5c

    nop

    :goto_20
    goto/16 :goto_71

    :goto_21
    goto/32 :goto_32

    nop

    :goto_22
    invoke-virtual {v6}, Landroid/hardware/camera2/CaptureResult$Key;->getVendorId()J

    move-result-wide v6

    goto/32 :goto_29

    nop

    :goto_23
    new-instance v7, Ljava/lang/StringBuilder;

    goto/32 :goto_4f

    nop

    :goto_24
    invoke-direct {v3, v4, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/32 :goto_4b

    nop

    :goto_25
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    goto/32 :goto_11

    nop

    :goto_26
    const-string v2, "getKeysStatic for "

    goto/32 :goto_57

    nop

    :goto_27
    invoke-virtual {p3, v4}, Landroid/hardware/camera2/CameraMetadata;->getProtected(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    goto/32 :goto_13

    nop

    :goto_28
    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v6

    goto/32 :goto_6f

    nop

    :goto_29
    goto :goto_38

    :goto_2a
    goto/32 :goto_68

    nop

    :goto_2b
    if-nez p3, :cond_7

    goto/32 :goto_14

    :cond_7
    goto/32 :goto_27

    nop

    :goto_2c
    if-eqz v1, :cond_8

    goto/32 :goto_7b

    :cond_8
    goto/32 :goto_7a

    nop

    :goto_2d
    goto/16 :goto_79

    :goto_2e
    goto/32 :goto_c

    nop

    :goto_2f
    if-nez v5, :cond_9

    goto/32 :goto_17

    :cond_9
    goto/32 :goto_56

    nop

    :goto_30
    new-instance v7, Ljava/lang/StringBuilder;

    goto/32 :goto_42

    nop

    :goto_31
    invoke-virtual {v1, p2}, Landroid/hardware/camera2/impl/CameraMetadataNative;->getAllVendorKeys(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_32
    iget-object v1, p0, Landroid/hardware/camera2/CameraMetadata;->mNativeInstance:Landroid/hardware/camera2/impl/CameraMetadataNative;

    goto/32 :goto_2c

    nop

    :goto_33
    check-cast v6, Landroid/hardware/camera2/CameraCharacteristics$Key;

    goto/32 :goto_37

    nop

    :goto_34
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_4a

    nop

    :goto_35
    invoke-static {v5, v6, v7}, Landroid/hardware/camera2/impl/CameraMetadataNative;->getTag(Ljava/lang/String;J)I

    move-result v8

    goto/32 :goto_3c

    nop

    :goto_36
    aget-object v5, v2, v4

    goto/32 :goto_28

    nop

    :goto_37
    invoke-virtual {v6}, Landroid/hardware/camera2/CameraCharacteristics$Key;->getVendorId()J

    move-result-wide v6

    :goto_38
    goto/32 :goto_4d

    nop

    :goto_39
    if-nez v6, :cond_a

    goto/32 :goto_79

    :cond_a
    :try_start_0
    invoke-virtual {v5, p3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    nop

    goto/32 :goto_72

    nop

    :goto_3a
    check-cast v6, Landroid/hardware/camera2/CaptureRequest$Key;

    goto/32 :goto_5d

    nop

    :goto_3b
    add-int/lit8 v4, v4, 0x1

    goto/32 :goto_20

    nop

    :goto_3c
    invoke-static {p4, v8}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v8

    goto/32 :goto_53

    nop

    :goto_3d
    const-string v8, "getKeysStatic - key was added - "

    goto/32 :goto_a

    nop

    :goto_3e
    invoke-virtual {v5}, Landroid/hardware/camera2/CaptureResult$Key;->getName()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_47

    nop

    :goto_3f
    sget-boolean v7, Landroid/hardware/camera2/CameraMetadata;->DEBUG:Z

    goto/32 :goto_5a

    nop

    :goto_40
    new-instance v3, Ljava/lang/AssertionError;

    goto/32 :goto_b

    nop

    :goto_41
    if-nez v0, :cond_b

    goto/32 :goto_75

    :cond_b
    goto/32 :goto_48

    nop

    :goto_42
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_3d

    nop

    :goto_43
    if-lt v4, v3, :cond_c

    goto/32 :goto_21

    :cond_c
    goto/32 :goto_36

    nop

    :goto_44
    array-length v3, v2

    goto/32 :goto_70

    nop

    :goto_45
    const-string v1, "CameraMetadataAb"

    goto/32 :goto_41

    nop

    :goto_46
    move-object v5, v4

    goto/32 :goto_4e

    nop

    :goto_47
    move-object v6, v4

    goto/32 :goto_f

    nop

    :goto_48
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_62

    nop

    :goto_49
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_9

    nop

    :goto_4a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_74

    nop

    :goto_4b
    throw v3

    :catch_0
    move-exception v1

    goto/32 :goto_1a

    nop

    :goto_4c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    goto/32 :goto_25

    nop

    :goto_4d
    if-nez p4, :cond_d

    goto/32 :goto_1

    :cond_d
    goto/32 :goto_35

    nop

    :goto_4e
    check-cast v5, Landroid/hardware/camera2/CaptureResult$Key;

    goto/32 :goto_3e

    nop

    :goto_4f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_76

    nop

    :goto_50
    invoke-static {v1, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_2d

    nop

    :goto_51
    move-object v6, v4

    goto/32 :goto_33

    nop

    :goto_52
    goto/16 :goto_79

    :catch_1
    move-exception v1

    goto/32 :goto_40

    nop

    :goto_53
    if-ltz v8, :cond_e

    goto/32 :goto_1

    :cond_e
    goto/32 :goto_0

    nop

    :goto_54
    sget-boolean v0, Landroid/hardware/camera2/CameraMetadata;->DEBUG:Z

    goto/32 :goto_45

    nop

    :goto_55
    if-nez v4, :cond_f

    goto/32 :goto_7e

    :cond_f
    goto/32 :goto_49

    nop

    :goto_56
    move-object v5, v4

    goto/32 :goto_64

    nop

    :goto_57
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_34

    nop

    :goto_58
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_1b

    nop

    :goto_59
    and-int/lit8 v6, v6, 0x1

    goto/32 :goto_39

    nop

    :goto_5a
    if-nez v7, :cond_10

    goto/32 :goto_79

    :cond_10
    goto/32 :goto_30

    nop

    :goto_5b
    return-object v0

    :goto_5c
    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v6

    goto/32 :goto_59

    nop

    :goto_5d
    invoke-virtual {v6}, Landroid/hardware/camera2/CaptureRequest$Key;->getVendorId()J

    move-result-wide v6

    goto/32 :goto_5e

    nop

    :goto_5e
    goto/16 :goto_38

    :goto_5f
    goto/32 :goto_2

    nop

    :goto_60
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_61
    goto/32 :goto_7d

    nop

    :goto_62
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_26

    nop

    :goto_63
    const-string v4, "Can\'t get IllegalAccessException"

    goto/32 :goto_5

    nop

    :goto_64
    check-cast v5, Landroid/hardware/camera2/CameraCharacteristics$Key;

    goto/32 :goto_18

    nop

    :goto_65
    if-nez v7, :cond_11

    goto/32 :goto_79

    :cond_11
    :goto_66
    goto/32 :goto_6e

    nop

    :goto_67
    const-class v0, Landroid/hardware/camera2/TotalCaptureResult;

    goto/32 :goto_d

    nop

    :goto_68
    instance-of v5, v4, Landroid/hardware/camera2/CameraCharacteristics$Key;

    goto/32 :goto_2f

    nop

    :goto_69
    check-cast v5, Landroid/hardware/camera2/CaptureRequest$Key;

    goto/32 :goto_6

    nop

    :goto_6a
    if-nez v7, :cond_12

    goto/32 :goto_79

    :cond_12
    goto/32 :goto_23

    nop

    :goto_6b
    invoke-static {v1, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_52

    nop

    :goto_6c
    const-class p1, Landroid/hardware/camera2/CaptureResult;

    :goto_6d
    goto/32 :goto_1c

    nop

    :goto_6e
    invoke-static {v6, v5, p4, p5}, Landroid/hardware/camera2/CameraMetadata;->shouldKeyBeAdded(Ljava/lang/Object;Ljava/lang/reflect/Field;[IZ)Z

    move-result v7

    goto/32 :goto_12

    nop

    :goto_6f
    invoke-virtual {v6, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v6

    goto/32 :goto_1f

    nop

    :goto_70
    const/4 v4, 0x0

    :goto_71
    goto/32 :goto_43

    nop

    :goto_72
    if-nez p3, :cond_13

    goto/32 :goto_66

    :cond_13
    goto/32 :goto_15

    nop

    :goto_73
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    goto/32 :goto_10

    nop

    :goto_74
    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_75
    goto/32 :goto_67

    nop

    :goto_76
    const-string v8, "getKeysStatic - key was filtered - "

    goto/32 :goto_4c

    nop

    :goto_77
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_58

    nop

    :goto_78
    throw v3

    :goto_79
    goto/32 :goto_3b

    nop

    :goto_7a
    return-object v0

    :goto_7b
    goto/32 :goto_31

    nop

    :goto_7c
    move-object v5, v4

    goto/32 :goto_69

    nop

    :goto_7d
    goto/16 :goto_17

    :goto_7e
    goto/32 :goto_5b

    nop

    :goto_7f
    if-nez v5, :cond_14

    goto/32 :goto_2a

    :cond_14
    goto/32 :goto_46

    nop
.end method

.method public getKeys()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TTKey;>;"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    nop

    invoke-virtual {p0}, Landroid/hardware/camera2/CameraMetadata;->getKeyClass()Ljava/lang/Class;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, v6

    move-object v3, p0

    invoke-virtual/range {v0 .. v5}, Landroid/hardware/camera2/CameraMetadata;->getKeys(Ljava/lang/Class;Ljava/lang/Class;Landroid/hardware/camera2/CameraMetadata;[IZ)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getNativeMetadata()Landroid/hardware/camera2/impl/CameraMetadataNative;
    .locals 1

    iget-object v0, p0, Landroid/hardware/camera2/CameraMetadata;->mNativeInstance:Landroid/hardware/camera2/impl/CameraMetadataNative;

    return-object v0
.end method

.method public getNativeMetadataPtr()J
    .locals 2

    iget-object v0, p0, Landroid/hardware/camera2/CameraMetadata;->mNativeInstance:Landroid/hardware/camera2/impl/CameraMetadataNative;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    return-wide v0

    :cond_0
    invoke-virtual {v0}, Landroid/hardware/camera2/impl/CameraMetadataNative;->getMetadataPtr()J

    move-result-wide v0

    return-wide v0
.end method

.method protected abstract getProtected(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TTKey;)TT;"
        }
    .end annotation
.end method

.method protected setNativeInstance(Landroid/hardware/camera2/impl/CameraMetadataNative;)V
    .locals 0

    iput-object p1, p0, Landroid/hardware/camera2/CameraMetadata;->mNativeInstance:Landroid/hardware/camera2/impl/CameraMetadataNative;

    return-void
.end method
