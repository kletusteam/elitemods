.class public Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;
.super Landroid/hardware/camera2/CameraInjectionSession;

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/hardware/camera2/impl/CameraInjectionSessionImpl$CameraInjectionCallback;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "CameraInjectionSessionImpl"


# instance fields
.field private final mCallback:Landroid/hardware/camera2/impl/CameraInjectionSessionImpl$CameraInjectionCallback;

.field private final mExecutor:Ljava/util/concurrent/Executor;

.field private mInjectionSession:Landroid/hardware/camera2/ICameraInjectionSession;

.field private final mInjectionStatusCallback:Landroid/hardware/camera2/CameraInjectionSession$InjectionStatusCallback;

.field private final mInterfaceLock:Ljava/lang/Object;


# direct methods
.method public static synthetic $r8$lambda$rS7LgMn0Lln0LD7SkXkB2FaVZZI(Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;I)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->notifyError(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmInjectionStatusCallback(Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;)Landroid/hardware/camera2/CameraInjectionSession$InjectionStatusCallback;
    .locals 0

    iget-object p0, p0, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->mInjectionStatusCallback:Landroid/hardware/camera2/CameraInjectionSession$InjectionStatusCallback;

    return-object p0
.end method

.method public constructor <init>(Landroid/hardware/camera2/CameraInjectionSession$InjectionStatusCallback;Ljava/util/concurrent/Executor;)V
    .locals 1

    invoke-direct {p0}, Landroid/hardware/camera2/CameraInjectionSession;-><init>()V

    new-instance v0, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl$CameraInjectionCallback;

    invoke-direct {v0, p0}, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl$CameraInjectionCallback;-><init>(Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;)V

    iput-object v0, p0, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->mCallback:Landroid/hardware/camera2/impl/CameraInjectionSessionImpl$CameraInjectionCallback;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->mInterfaceLock:Ljava/lang/Object;

    iput-object p1, p0, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->mInjectionStatusCallback:Landroid/hardware/camera2/CameraInjectionSession$InjectionStatusCallback;

    iput-object p2, p0, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->mExecutor:Ljava/util/concurrent/Executor;

    return-void
.end method

.method private notifyError(I)V
    .locals 1

    iget-object v0, p0, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->mInjectionSession:Landroid/hardware/camera2/ICameraInjectionSession;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->mInjectionStatusCallback:Landroid/hardware/camera2/CameraInjectionSession$InjectionStatusCallback;

    invoke-virtual {v0, p1}, Landroid/hardware/camera2/CameraInjectionSession$InjectionStatusCallback;->onInjectionError(I)V

    :cond_0
    return-void
.end method

.method private scheduleNotifyError(I)V
    .locals 5

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    :try_start_0
    iget-object v2, p0, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v3, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl$$ExternalSyntheticLambda0;

    invoke-direct {v3}, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl$$ExternalSyntheticLambda0;-><init>()V

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3, p0, v4}, Lcom/android/internal/util/function/pooled/PooledLambda;->obtainRunnable(Ljava/util/function/BiConsumer;Ljava/lang/Object;Ljava/lang/Object;)Lcom/android/internal/util/function/pooled/PooledRunnable;

    move-result-object v3

    invoke-interface {v3}, Lcom/android/internal/util/function/pooled/PooledRunnable;->recycleOnUse()Lcom/android/internal/util/function/pooled/PooledRunnable;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    nop

    return-void

    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v2
.end method


# virtual methods
.method public binderDied()V
    .locals 5

    iget-object v0, p0, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->mInterfaceLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    const-string v1, "CameraInjectionSessionImpl"

    const-string v2, "CameraInjectionSessionImpl died unexpectedly"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->mInjectionSession:Landroid/hardware/camera2/ICameraInjectionSession;

    if-nez v1, :cond_0

    monitor-exit v0

    return-void

    :cond_0
    new-instance v1, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl$1;

    invoke-direct {v1, p0}, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl$1;-><init>(Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;)V

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v4, p0, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->mExecutor:Ljava/util/concurrent/Executor;

    invoke-interface {v4, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    nop

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v4

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    nop

    throw v4

    :catchall_1
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1
.end method

.method public close()V
    .locals 3

    iget-object v0, p0, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->mInterfaceLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->mInjectionSession:Landroid/hardware/camera2/ICameraInjectionSession;

    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/hardware/camera2/ICameraInjectionSession;->stopInjection()V

    iget-object v1, p0, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->mInjectionSession:Landroid/hardware/camera2/ICameraInjectionSession;

    invoke-interface {v1}, Landroid/hardware/camera2/ICameraInjectionSession;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, p0, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    const/4 v1, 0x0

    iput-object v1, p0, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->mInjectionSession:Landroid/hardware/camera2/ICameraInjectionSession;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    goto :goto_0

    :catchall_0
    move-exception v1

    goto :goto_1

    :catch_0
    move-exception v1

    :goto_0
    :try_start_1
    monitor-exit v0

    return-void

    :goto_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0}, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    nop

    return-void

    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public getCallback()Landroid/hardware/camera2/impl/CameraInjectionSessionImpl$CameraInjectionCallback;
    .locals 1

    iget-object v0, p0, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->mCallback:Landroid/hardware/camera2/impl/CameraInjectionSessionImpl$CameraInjectionCallback;

    return-object v0
.end method

.method public onInjectionError(I)V
    .locals 6

    const-string v0, "CameraInjectionSessionImpl"

    const-string v1, "Injection session error received, code %d"

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->mInterfaceLock:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->mInjectionSession:Landroid/hardware/camera2/ICameraInjectionSession;

    if-nez v1, :cond_0

    monitor-exit v0

    return-void

    :cond_0
    packed-switch p1, :pswitch_data_0

    const-string v1, "CameraInjectionSessionImpl"

    goto :goto_0

    :pswitch_0
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->scheduleNotifyError(I)V

    goto :goto_1

    :pswitch_1
    invoke-direct {p0, v2}, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->scheduleNotifyError(I)V

    goto :goto_1

    :pswitch_2
    invoke-direct {p0, v5}, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->scheduleNotifyError(I)V

    goto :goto_1

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown error from injection session: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v2}, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->scheduleNotifyError(I)V

    :goto_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setRemoteInjectionSession(Landroid/hardware/camera2/ICameraInjectionSession;)V
    .locals 7

    iget-object v0, p0, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->mInterfaceLock:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x0

    if-nez p1, :cond_0

    :try_start_0
    const-string v2, "CameraInjectionSessionImpl"

    const-string v3, "The camera injection session has encountered a serious error"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v1}, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->scheduleNotifyError(I)V

    monitor-exit v0

    return-void

    :cond_0
    iput-object p1, p0, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->mInjectionSession:Landroid/hardware/camera2/ICameraInjectionSession;

    invoke-interface {p1}, Landroid/hardware/camera2/ICameraInjectionSession;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v3, "CameraInjectionSessionImpl"

    const-string v4, "The camera injection session has encountered a serious error"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v1}, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->scheduleNotifyError(I)V

    monitor-exit v0

    return-void

    :cond_1
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-interface {v2, p0, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    iget-object v5, p0, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v6, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl$2;

    invoke-direct {v6, p0}, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl$2;-><init>(Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;)V

    invoke-interface {v5, v6}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    :catchall_0
    move-exception v1

    goto :goto_1

    :catch_0
    move-exception v5

    :try_start_3
    invoke-direct {p0, v1}, Landroid/hardware/camera2/impl/CameraInjectionSessionImpl;->scheduleNotifyError(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    :goto_0
    nop

    monitor-exit v0

    return-void

    :goto_1
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    nop

    throw v1

    :catchall_1
    move-exception v1

    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v1
.end method
