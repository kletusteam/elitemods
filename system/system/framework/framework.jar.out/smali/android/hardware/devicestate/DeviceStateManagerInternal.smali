.class public abstract Landroid/hardware/devicestate/DeviceStateManagerInternal;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bootAnimationCompleted()V
.end method

.method public abstract getSupportedStateIdentifiers()[I
.end method

.method public abstract isReversedState()Z
.end method

.method public abstract setInteractive(Z)V
.end method
