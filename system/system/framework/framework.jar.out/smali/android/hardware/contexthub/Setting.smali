.class public interface abstract annotation Landroid/hardware/contexthub/Setting;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final AIRPLANE_MODE:B = 0x4t

.field public static final BT_MAIN:B = 0x6t

.field public static final BT_SCANNING:B = 0x7t

.field public static final LOCATION:B = 0x1t

.field public static final MICROPHONE:B = 0x5t

.field public static final WIFI_MAIN:B = 0x2t

.field public static final WIFI_SCANNING:B = 0x3t
