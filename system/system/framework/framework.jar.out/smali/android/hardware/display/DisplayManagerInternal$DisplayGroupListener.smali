.class public interface abstract Landroid/hardware/display/DisplayManagerInternal$DisplayGroupListener;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/display/DisplayManagerInternal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DisplayGroupListener"
.end annotation


# virtual methods
.method public abstract onDisplayGroupAdded(I)V
.end method

.method public abstract onDisplayGroupChanged(I)V
.end method

.method public abstract onDisplayGroupRemoved(I)V
.end method
