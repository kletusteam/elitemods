.class public Landroid/accessibilityservice/InputMethod;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/accessibilityservice/InputMethod$SessionImpl;,
        Landroid/accessibilityservice/InputMethod$AccessibilityInputConnection;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "A11yInputMethod"


# instance fields
.field private mInputEditorInfo:Landroid/view/inputmethod/EditorInfo;

.field private mInputStarted:Z

.field private final mService:Landroid/accessibilityservice/AccessibilityService;

.field private mStartedInputConnection:Lcom/android/internal/inputmethod/RemoteAccessibilityInputConnection;


# direct methods
.method static bridge synthetic -$$Nest$fgetmService(Landroid/accessibilityservice/InputMethod;)Landroid/accessibilityservice/AccessibilityService;
    .locals 0

    iget-object p0, p0, Landroid/accessibilityservice/InputMethod;->mService:Landroid/accessibilityservice/AccessibilityService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmStartedInputConnection(Landroid/accessibilityservice/InputMethod;)Lcom/android/internal/inputmethod/RemoteAccessibilityInputConnection;
    .locals 0

    iget-object p0, p0, Landroid/accessibilityservice/InputMethod;->mStartedInputConnection:Lcom/android/internal/inputmethod/RemoteAccessibilityInputConnection;

    return-object p0
.end method

.method public constructor <init>(Landroid/accessibilityservice/AccessibilityService;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/accessibilityservice/InputMethod;->mService:Landroid/accessibilityservice/AccessibilityService;

    return-void
.end method


# virtual methods
.method final createImeSession(Lcom/android/internal/inputmethod/IAccessibilityInputMethodSessionCallback;)V
    .locals 4

    goto/32 :goto_7

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v1, p0, Landroid/accessibilityservice/InputMethod;->mService:Landroid/accessibilityservice/AccessibilityService;

    goto/32 :goto_9

    nop

    :goto_2
    new-instance v2, Landroid/accessibilityservice/InputMethod$SessionImpl;

    goto/32 :goto_6

    nop

    :goto_3
    invoke-direct {v0, v1, v2}, Landroid/accessibilityservice/AccessibilityInputMethodSessionWrapper;-><init>(Landroid/os/Looper;Landroid/accessibilityservice/AccessibilityInputMethodSession;)V

    :try_start_0
    iget-object v1, p0, Landroid/accessibilityservice/InputMethod;->mService:Landroid/accessibilityservice/AccessibilityService;

    invoke-virtual {v1}, Landroid/accessibilityservice/AccessibilityService;->getConnectionId()I

    move-result v1

    invoke-interface {p1, v0, v1}, Lcom/android/internal/inputmethod/IAccessibilityInputMethodSessionCallback;->sessionCreated(Lcom/android/internal/inputmethod/IAccessibilityInputMethodSession;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_4

    nop

    :goto_4
    goto :goto_5

    :catch_0
    move-exception v1

    :goto_5
    goto/32 :goto_0

    nop

    :goto_6
    const/4 v3, 0x0

    goto/32 :goto_8

    nop

    :goto_7
    new-instance v0, Landroid/accessibilityservice/AccessibilityInputMethodSessionWrapper;

    goto/32 :goto_1

    nop

    :goto_8
    invoke-direct {v2, p0, v3}, Landroid/accessibilityservice/InputMethod$SessionImpl;-><init>(Landroid/accessibilityservice/InputMethod;Landroid/accessibilityservice/InputMethod$SessionImpl-IA;)V

    goto/32 :goto_3

    nop

    :goto_9
    invoke-virtual {v1}, Landroid/accessibilityservice/AccessibilityService;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    goto/32 :goto_2

    nop
.end method

.method final doFinishInput()V
    .locals 2

    goto/32 :goto_b

    nop

    :goto_0
    iput-boolean v0, p0, Landroid/accessibilityservice/InputMethod;->mInputStarted:Z

    goto/32 :goto_2

    nop

    :goto_1
    if-nez v1, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_a

    nop

    :goto_2
    const/4 v0, 0x0

    goto/32 :goto_c

    nop

    :goto_3
    const-string v1, "CALL: doFinishInput"

    goto/32 :goto_d

    nop

    :goto_4
    invoke-virtual {p0}, Landroid/accessibilityservice/InputMethod;->onFinishInput()V

    :goto_5
    goto/32 :goto_8

    nop

    :goto_6
    iget-boolean v1, p0, Landroid/accessibilityservice/InputMethod;->mInputStarted:Z

    goto/32 :goto_1

    nop

    :goto_7
    iput-object v0, p0, Landroid/accessibilityservice/InputMethod;->mInputEditorInfo:Landroid/view/inputmethod/EditorInfo;

    goto/32 :goto_e

    nop

    :goto_8
    const/4 v0, 0x0

    goto/32 :goto_0

    nop

    :goto_9
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_4

    nop

    :goto_a
    const-string v1, "CALL: onFinishInput"

    goto/32 :goto_9

    nop

    :goto_b
    const-string v0, "A11yInputMethod"

    goto/32 :goto_3

    nop

    :goto_c
    iput-object v0, p0, Landroid/accessibilityservice/InputMethod;->mStartedInputConnection:Lcom/android/internal/inputmethod/RemoteAccessibilityInputConnection;

    goto/32 :goto_7

    nop

    :goto_d
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_6

    nop

    :goto_e
    return-void
.end method

.method final doStartInput(Lcom/android/internal/inputmethod/RemoteAccessibilityInputConnection;Landroid/view/inputmethod/EditorInfo;Z)V
    .locals 2

    goto/32 :goto_f

    nop

    :goto_0
    const/4 v0, 0x1

    goto/32 :goto_9

    nop

    :goto_1
    return-void

    :goto_2
    goto/32 :goto_0

    nop

    :goto_3
    iput-object p2, p0, Landroid/accessibilityservice/InputMethod;->mInputEditorInfo:Landroid/view/inputmethod/EditorInfo;

    goto/32 :goto_c

    nop

    :goto_4
    invoke-virtual {p0, p2, p3}, Landroid/accessibilityservice/InputMethod;->onStartInput(Landroid/view/inputmethod/EditorInfo;Z)V

    goto/32 :goto_b

    nop

    :goto_5
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_4

    nop

    :goto_6
    if-eqz p1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop

    :goto_7
    if-eqz p3, :cond_1

    goto/32 :goto_2

    :cond_1
    :goto_8
    goto/32 :goto_10

    nop

    :goto_9
    iput-boolean v0, p0, Landroid/accessibilityservice/InputMethod;->mInputStarted:Z

    goto/32 :goto_d

    nop

    :goto_a
    if-nez v0, :cond_2

    goto/32 :goto_2

    :cond_2
    goto/32 :goto_11

    nop

    :goto_b
    return-void

    :goto_c
    const-string v0, "A11yInputMethod"

    goto/32 :goto_e

    nop

    :goto_d
    iput-object p1, p0, Landroid/accessibilityservice/InputMethod;->mStartedInputConnection:Lcom/android/internal/inputmethod/RemoteAccessibilityInputConnection;

    goto/32 :goto_3

    nop

    :goto_e
    const-string v1, "CALL: onStartInput"

    goto/32 :goto_5

    nop

    :goto_f
    if-nez p1, :cond_3

    goto/32 :goto_8

    :cond_3
    goto/32 :goto_7

    nop

    :goto_10
    iget-boolean v0, p0, Landroid/accessibilityservice/InputMethod;->mInputStarted:Z

    goto/32 :goto_a

    nop

    :goto_11
    invoke-virtual {p0}, Landroid/accessibilityservice/InputMethod;->doFinishInput()V

    goto/32 :goto_6

    nop
.end method

.method public final getCurrentInputConnection()Landroid/accessibilityservice/InputMethod$AccessibilityInputConnection;
    .locals 2

    iget-object v0, p0, Landroid/accessibilityservice/InputMethod;->mStartedInputConnection:Lcom/android/internal/inputmethod/RemoteAccessibilityInputConnection;

    if-eqz v0, :cond_0

    new-instance v1, Landroid/accessibilityservice/InputMethod$AccessibilityInputConnection;

    invoke-direct {v1, p0, v0}, Landroid/accessibilityservice/InputMethod$AccessibilityInputConnection;-><init>(Landroid/accessibilityservice/InputMethod;Lcom/android/internal/inputmethod/RemoteAccessibilityInputConnection;)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getCurrentInputEditorInfo()Landroid/view/inputmethod/EditorInfo;
    .locals 1

    iget-object v0, p0, Landroid/accessibilityservice/InputMethod;->mInputEditorInfo:Landroid/view/inputmethod/EditorInfo;

    return-object v0
.end method

.method public final getCurrentInputStarted()Z
    .locals 1

    iget-boolean v0, p0, Landroid/accessibilityservice/InputMethod;->mInputStarted:Z

    return v0
.end method

.method public onFinishInput()V
    .locals 0

    return-void
.end method

.method public onStartInput(Landroid/view/inputmethod/EditorInfo;Z)V
    .locals 0

    return-void
.end method

.method public onUpdateSelection(IIIIII)V
    .locals 0

    return-void
.end method

.method final restartInput(Lcom/android/internal/inputmethod/RemoteAccessibilityInputConnection;Landroid/view/inputmethod/EditorInfo;)V
    .locals 3

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {p0, p1, p2, v2}, Landroid/accessibilityservice/InputMethod;->doStartInput(Lcom/android/internal/inputmethod/RemoteAccessibilityInputConnection;Landroid/view/inputmethod/EditorInfo;Z)V

    goto/32 :goto_1

    nop

    :goto_1
    invoke-static {v0, v1}, Landroid/os/Trace;->traceEnd(J)V

    goto/32 :goto_8

    nop

    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_4

    nop

    :goto_3
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_e

    nop

    :goto_5
    const/4 v2, 0x1

    goto/32 :goto_0

    nop

    :goto_6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_7
    const-string v1, "A11yInputMethod"

    goto/32 :goto_b

    nop

    :goto_8
    return-void

    :goto_9
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_a
    invoke-static {v0, v1, v2}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    goto/32 :goto_5

    nop

    :goto_b
    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_d

    nop

    :goto_c
    const-string v2, "AccessibilityService.restartInput"

    goto/32 :goto_a

    nop

    :goto_d
    const-wide/16 v0, 0x20

    goto/32 :goto_c

    nop

    :goto_e
    const-string/jumbo v1, "restartInput(): editor="

    goto/32 :goto_6

    nop
.end method

.method final startInput(Lcom/android/internal/inputmethod/RemoteAccessibilityInputConnection;Landroid/view/inputmethod/EditorInfo;)V
    .locals 3

    goto/32 :goto_e

    nop

    :goto_0
    invoke-static {v0, v1, v2}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    goto/32 :goto_4

    nop

    :goto_1
    const-string/jumbo v1, "startInput(): editor="

    goto/32 :goto_8

    nop

    :goto_2
    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_7

    nop

    :goto_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_d

    nop

    :goto_4
    const/4 v2, 0x0

    goto/32 :goto_c

    nop

    :goto_5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1

    nop

    :goto_6
    const-string v2, "AccessibilityService.startInput"

    goto/32 :goto_0

    nop

    :goto_7
    const-wide/16 v0, 0x20

    goto/32 :goto_6

    nop

    :goto_8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_a

    nop

    :goto_9
    invoke-static {v0, v1}, Landroid/os/Trace;->traceEnd(J)V

    goto/32 :goto_b

    nop

    :goto_a
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_b
    return-void

    :goto_c
    invoke-virtual {p0, p1, p2, v2}, Landroid/accessibilityservice/InputMethod;->doStartInput(Lcom/android/internal/inputmethod/RemoteAccessibilityInputConnection;Landroid/view/inputmethod/EditorInfo;Z)V

    goto/32 :goto_9

    nop

    :goto_d
    const-string v1, "A11yInputMethod"

    goto/32 :goto_2

    nop

    :goto_e
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_5

    nop
.end method
