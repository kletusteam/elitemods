.class interface abstract Landroid/accessibilityservice/AccessibilityInputMethodSession;
.super Ljava/lang/Object;


# virtual methods
.method public abstract finishInput()V
.end method

.method public abstract invalidateInput(Landroid/view/inputmethod/EditorInfo;Lcom/android/internal/inputmethod/IRemoteAccessibilityInputConnection;I)V
.end method

.method public abstract setEnabled(Z)V
.end method

.method public abstract updateSelection(IIIIII)V
.end method
