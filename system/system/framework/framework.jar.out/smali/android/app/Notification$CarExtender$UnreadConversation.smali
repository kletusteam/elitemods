.class public Landroid/app/Notification$CarExtender$UnreadConversation;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/Notification$CarExtender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UnreadConversation"
.end annotation


# static fields
.field private static final KEY_AUTHOR:Ljava/lang/String; = "author"

.field private static final KEY_MESSAGES:Ljava/lang/String; = "messages"

.field static final KEY_ON_READ:Ljava/lang/String; = "on_read"

.field static final KEY_ON_REPLY:Ljava/lang/String; = "on_reply"

.field private static final KEY_PARTICIPANTS:Ljava/lang/String; = "participants"

.field static final KEY_REMOTE_INPUT:Ljava/lang/String; = "remote_input"

.field private static final KEY_TEXT:Ljava/lang/String; = "text"

.field private static final KEY_TIMESTAMP:Ljava/lang/String; = "timestamp"


# instance fields
.field private final mLatestTimestamp:J

.field private final mMessages:[Ljava/lang/String;

.field private final mParticipants:[Ljava/lang/String;

.field private final mReadPendingIntent:Landroid/app/PendingIntent;

.field private final mRemoteInput:Landroid/app/RemoteInput;

.field private final mReplyPendingIntent:Landroid/app/PendingIntent;


# direct methods
.method constructor <init>([Ljava/lang/String;Landroid/app/RemoteInput;Landroid/app/PendingIntent;Landroid/app/PendingIntent;[Ljava/lang/String;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/app/Notification$CarExtender$UnreadConversation;->mMessages:[Ljava/lang/String;

    iput-object p2, p0, Landroid/app/Notification$CarExtender$UnreadConversation;->mRemoteInput:Landroid/app/RemoteInput;

    iput-object p4, p0, Landroid/app/Notification$CarExtender$UnreadConversation;->mReadPendingIntent:Landroid/app/PendingIntent;

    iput-object p3, p0, Landroid/app/Notification$CarExtender$UnreadConversation;->mReplyPendingIntent:Landroid/app/PendingIntent;

    iput-object p5, p0, Landroid/app/Notification$CarExtender$UnreadConversation;->mParticipants:[Ljava/lang/String;

    iput-wide p6, p0, Landroid/app/Notification$CarExtender$UnreadConversation;->mLatestTimestamp:J

    return-void
.end method

.method static getUnreadConversationFromBundle(Landroid/os/Bundle;)Landroid/app/Notification$CarExtender$UnreadConversation;
    .locals 15

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    const-string/jumbo v1, "messages"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_5

    array-length v3, v1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x1

    const/4 v5, 0x0

    :goto_0
    array-length v6, v3

    if-ge v5, v6, :cond_3

    aget-object v6, v1, v5

    instance-of v6, v6, Landroid/os/Bundle;

    if-nez v6, :cond_1

    const/4 v4, 0x0

    goto :goto_1

    :cond_1
    aget-object v6, v1, v5

    check-cast v6, Landroid/os/Bundle;

    const-string/jumbo v7, "text"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    aget-object v6, v3, v5

    if-nez v6, :cond_2

    const/4 v4, 0x0

    goto :goto_1

    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_3
    :goto_1
    if-eqz v4, :cond_4

    move-object v2, v3

    goto :goto_2

    :cond_4
    return-object v0

    :cond_5
    :goto_2
    const-class v3, Landroid/app/PendingIntent;

    const-string/jumbo v4, "on_read"

    invoke-virtual {p0, v4, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/PendingIntent;

    const-class v4, Landroid/app/PendingIntent;

    const-string/jumbo v5, "on_reply"

    invoke-virtual {p0, v5, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/PendingIntent;

    const-class v5, Landroid/app/RemoteInput;

    const-string/jumbo v6, "remote_input"

    invoke-virtual {p0, v6, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    move-object v13, v5

    check-cast v13, Landroid/app/RemoteInput;

    const-string/jumbo v5, "participants"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_7

    array-length v5, v14

    const/4 v6, 0x1

    if-eq v5, v6, :cond_6

    goto :goto_3

    :cond_6
    new-instance v0, Landroid/app/Notification$CarExtender$UnreadConversation;

    const-string/jumbo v5, "timestamp"

    invoke-virtual {p0, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v11

    move-object v5, v0

    move-object v6, v2

    move-object v7, v13

    move-object v8, v4

    move-object v9, v3

    move-object v10, v14

    invoke-direct/range {v5 .. v12}, Landroid/app/Notification$CarExtender$UnreadConversation;-><init>([Ljava/lang/String;Landroid/app/RemoteInput;Landroid/app/PendingIntent;Landroid/app/PendingIntent;[Ljava/lang/String;J)V

    return-object v0

    :cond_7
    :goto_3
    return-object v0
.end method


# virtual methods
.method getBundleForUnreadConversation()Landroid/os/Bundle;
    .locals 7

    goto/32 :goto_2c

    nop

    :goto_0
    iget-object v5, p0, Landroid/app/Notification$CarExtender$UnreadConversation;->mMessages:[Ljava/lang/String;

    goto/32 :goto_1a

    nop

    :goto_1
    const-string/jumbo v4, "on_reply"

    goto/32 :goto_b

    nop

    :goto_2
    if-lt v3, v4, :cond_0

    goto/32 :goto_19

    :cond_0
    goto/32 :goto_29

    nop

    :goto_3
    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    goto/32 :goto_c

    nop

    :goto_4
    const-string/jumbo v6, "text"

    goto/32 :goto_30

    nop

    :goto_5
    const-string v5, "author"

    goto/32 :goto_24

    nop

    :goto_6
    array-length v2, v2

    goto/32 :goto_25

    nop

    :goto_7
    const-string/jumbo v3, "messages"

    goto/32 :goto_3

    nop

    :goto_8
    iget-object v3, p0, Landroid/app/Notification$CarExtender$UnreadConversation;->mReplyPendingIntent:Landroid/app/PendingIntent;

    goto/32 :goto_1

    nop

    :goto_9
    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto/32 :goto_17

    nop

    :goto_a
    if-nez v2, :cond_1

    goto/32 :goto_14

    :cond_1
    goto/32 :goto_1d

    nop

    :goto_b
    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto/32 :goto_f

    nop

    :goto_c
    iget-object v3, p0, Landroid/app/Notification$CarExtender$UnreadConversation;->mRemoteInput:Landroid/app/RemoteInput;

    goto/32 :goto_11

    nop

    :goto_d
    const-string/jumbo v5, "timestamp"

    goto/32 :goto_e

    nop

    :goto_e
    invoke-virtual {v0, v5, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto/32 :goto_26

    nop

    :goto_f
    iget-object v3, p0, Landroid/app/Notification$CarExtender$UnreadConversation;->mReadPendingIntent:Landroid/app/PendingIntent;

    goto/32 :goto_12

    nop

    :goto_10
    const/4 v1, 0x0

    goto/32 :goto_1e

    nop

    :goto_11
    if-nez v3, :cond_2

    goto/32 :goto_23

    :cond_2
    goto/32 :goto_20

    nop

    :goto_12
    const-string/jumbo v4, "on_read"

    goto/32 :goto_9

    nop

    :goto_13
    aget-object v1, v2, v3

    :goto_14
    goto/32 :goto_2e

    nop

    :goto_15
    const-string/jumbo v4, "participants"

    goto/32 :goto_21

    nop

    :goto_16
    if-gt v3, v4, :cond_3

    goto/32 :goto_14

    :cond_3
    goto/32 :goto_2f

    nop

    :goto_17
    iget-object v3, p0, Landroid/app/Notification$CarExtender$UnreadConversation;->mParticipants:[Ljava/lang/String;

    goto/32 :goto_15

    nop

    :goto_18
    goto :goto_2b

    :goto_19
    goto/32 :goto_7

    nop

    :goto_1a
    aget-object v5, v5, v3

    goto/32 :goto_4

    nop

    :goto_1b
    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    goto/32 :goto_0

    nop

    :goto_1c
    add-int/lit8 v3, v3, 0x1

    goto/32 :goto_18

    nop

    :goto_1d
    array-length v3, v2

    goto/32 :goto_31

    nop

    :goto_1e
    iget-object v2, p0, Landroid/app/Notification$CarExtender$UnreadConversation;->mParticipants:[Ljava/lang/String;

    goto/32 :goto_a

    nop

    :goto_1f
    array-length v4, v2

    goto/32 :goto_2

    nop

    :goto_20
    const-string/jumbo v4, "remote_input"

    goto/32 :goto_22

    nop

    :goto_21
    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    goto/32 :goto_2d

    nop

    :goto_22
    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :goto_23
    goto/32 :goto_8

    nop

    :goto_24
    invoke-virtual {v4, v5, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_27

    nop

    :goto_25
    new-array v2, v2, [Landroid/os/Parcelable;

    goto/32 :goto_2a

    nop

    :goto_26
    return-object v0

    :goto_27
    aput-object v4, v2, v3

    goto/32 :goto_1c

    nop

    :goto_28
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto/32 :goto_10

    nop

    :goto_29
    new-instance v4, Landroid/os/Bundle;

    goto/32 :goto_1b

    nop

    :goto_2a
    const/4 v3, 0x0

    :goto_2b
    goto/32 :goto_1f

    nop

    :goto_2c
    new-instance v0, Landroid/os/Bundle;

    goto/32 :goto_28

    nop

    :goto_2d
    iget-wide v3, p0, Landroid/app/Notification$CarExtender$UnreadConversation;->mLatestTimestamp:J

    goto/32 :goto_d

    nop

    :goto_2e
    iget-object v2, p0, Landroid/app/Notification$CarExtender$UnreadConversation;->mMessages:[Ljava/lang/String;

    goto/32 :goto_6

    nop

    :goto_2f
    const/4 v3, 0x0

    goto/32 :goto_13

    nop

    :goto_30
    invoke-virtual {v4, v6, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_5

    nop

    :goto_31
    const/4 v4, 0x1

    goto/32 :goto_16

    nop
.end method

.method public getLatestTimestamp()J
    .locals 2

    iget-wide v0, p0, Landroid/app/Notification$CarExtender$UnreadConversation;->mLatestTimestamp:J

    return-wide v0
.end method

.method public getMessages()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/app/Notification$CarExtender$UnreadConversation;->mMessages:[Ljava/lang/String;

    return-object v0
.end method

.method public getParticipant()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Landroid/app/Notification$CarExtender$UnreadConversation;->mParticipants:[Ljava/lang/String;

    array-length v1, v0

    if-lez v1, :cond_0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getParticipants()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/app/Notification$CarExtender$UnreadConversation;->mParticipants:[Ljava/lang/String;

    return-object v0
.end method

.method public getReadPendingIntent()Landroid/app/PendingIntent;
    .locals 1

    iget-object v0, p0, Landroid/app/Notification$CarExtender$UnreadConversation;->mReadPendingIntent:Landroid/app/PendingIntent;

    return-object v0
.end method

.method public getRemoteInput()Landroid/app/RemoteInput;
    .locals 1

    iget-object v0, p0, Landroid/app/Notification$CarExtender$UnreadConversation;->mRemoteInput:Landroid/app/RemoteInput;

    return-object v0
.end method

.method public getReplyPendingIntent()Landroid/app/PendingIntent;
    .locals 1

    iget-object v0, p0, Landroid/app/Notification$CarExtender$UnreadConversation;->mReplyPendingIntent:Landroid/app/PendingIntent;

    return-object v0
.end method
