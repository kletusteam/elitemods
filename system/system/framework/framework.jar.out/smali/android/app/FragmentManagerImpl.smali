.class final Landroid/app/FragmentManagerImpl;
.super Landroid/app/FragmentManager;

# interfaces
.implements Landroid/view/LayoutInflater$Factory2;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/FragmentManagerImpl$StartEnterTransitionListener;,
        Landroid/app/FragmentManagerImpl$PopBackStackState;,
        Landroid/app/FragmentManagerImpl$OpGenerator;,
        Landroid/app/FragmentManagerImpl$AnimateOnHWLayerIfNeededListener;
    }
.end annotation


# static fields
.field static DEBUG:Z = false

.field static final TAG:Ljava/lang/String; = "FragmentManager"

.field static final TARGET_REQUEST_CODE_STATE_TAG:Ljava/lang/String; = "android:target_req_state"

.field static final TARGET_STATE_TAG:Ljava/lang/String; = "android:target_state"

.field static final USER_VISIBLE_HINT_TAG:Ljava/lang/String; = "android:user_visible_hint"

.field static final VIEW_STATE_TAG:Ljava/lang/String; = "android:view_state"


# instance fields
.field mActive:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Landroid/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field final mAdded:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field mAllowOldReentrantBehavior:Z

.field mAvailBackStackIndices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mBackStack:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/app/BackStackRecord;",
            ">;"
        }
    .end annotation
.end field

.field mBackStackChangeListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/app/FragmentManager$OnBackStackChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field mBackStackIndices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/app/BackStackRecord;",
            ">;"
        }
    .end annotation
.end field

.field mContainer:Landroid/app/FragmentContainer;

.field mCreatedMenus:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field mCurState:I

.field mDestroyed:Z

.field mExecCommit:Ljava/lang/Runnable;

.field mExecutingActions:Z

.field mHavePendingDeferredStart:Z

.field mHost:Landroid/app/FragmentHostCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/app/FragmentHostCallback<",
            "*>;"
        }
    .end annotation
.end field

.field final mLifecycleCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Landroid/util/Pair<",
            "Landroid/app/FragmentManager$FragmentLifecycleCallbacks;",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field mNeedMenuInvalidate:Z

.field mNextFragmentIndex:I

.field mNoTransactionsBecause:Ljava/lang/String;

.field mParent:Landroid/app/Fragment;

.field mPendingActions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/app/FragmentManagerImpl$OpGenerator;",
            ">;"
        }
    .end annotation
.end field

.field mPostponedTransactions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/app/FragmentManagerImpl$StartEnterTransitionListener;",
            ">;"
        }
    .end annotation
.end field

.field mPrimaryNav:Landroid/app/Fragment;

.field mSavedNonConfig:Landroid/app/FragmentManagerNonConfig;

.field mStateArray:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field mStateBundle:Landroid/os/Bundle;

.field mStateSaved:Z

.field mTmpAddedFragments:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field mTmpIsPop:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field mTmpRecords:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/app/BackStackRecord;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$mcompleteExecute(Landroid/app/FragmentManagerImpl;Landroid/app/BackStackRecord;ZZZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/app/FragmentManagerImpl;->completeExecute(Landroid/app/BackStackRecord;ZZZ)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mscheduleCommit(Landroid/app/FragmentManagerImpl;)V
    .locals 0

    invoke-direct {p0}, Landroid/app/FragmentManagerImpl;->scheduleCommit()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    return-void
.end method

.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/FragmentManager;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Landroid/app/FragmentManagerImpl;->mNextFragmentIndex:I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v1, p0, Landroid/app/FragmentManagerImpl;->mLifecycleCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    iput v0, p0, Landroid/app/FragmentManagerImpl;->mCurState:I

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mStateBundle:Landroid/os/Bundle;

    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mStateArray:Landroid/util/SparseArray;

    new-instance v0, Landroid/app/FragmentManagerImpl$1;

    invoke-direct {v0, p0}, Landroid/app/FragmentManagerImpl$1;-><init>(Landroid/app/FragmentManagerImpl;)V

    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mExecCommit:Ljava/lang/Runnable;

    return-void
.end method

.method private addAddedFragments(Landroid/util/ArraySet;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/ArraySet<",
            "Landroid/app/Fragment;",
            ">;)V"
        }
    .end annotation

    iget v0, p0, Landroid/app/FragmentManagerImpl;->mCurState:I

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x4

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x0

    move v8, v2

    :goto_0
    if-ge v8, v1, :cond_2

    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Landroid/app/Fragment;

    iget v2, v9, Landroid/app/Fragment;->mState:I

    if-ge v2, v0, :cond_1

    invoke-virtual {v9}, Landroid/app/Fragment;->getNextAnim()I

    move-result v5

    invoke-virtual {v9}, Landroid/app/Fragment;->getNextTransition()I

    move-result v6

    const/4 v7, 0x0

    move-object v2, p0

    move-object v3, v9

    move v4, v0

    invoke-virtual/range {v2 .. v7}, Landroid/app/FragmentManagerImpl;->moveToState(Landroid/app/Fragment;IIIZ)V

    iget-object v2, v9, Landroid/app/Fragment;->mView:Landroid/view/View;

    if-eqz v2, :cond_1

    iget-boolean v2, v9, Landroid/app/Fragment;->mHidden:Z

    if-nez v2, :cond_1

    iget-boolean v2, v9, Landroid/app/Fragment;->mIsNewlyAdded:Z

    if-eqz v2, :cond_1

    invoke-virtual {p1, v9}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private burpActive()V
    .locals 3

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->delete(I)V

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private checkStateLoss()V
    .locals 3

    iget-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mStateSaved:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mNoTransactionsBecause:Ljava/lang/String;

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can not perform this action inside of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mNoTransactionsBecause:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can not perform this action after onSaveInstanceState"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private cleanupExec()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mExecutingActions:Z

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mTmpIsPop:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mTmpRecords:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method private completeExecute(Landroid/app/BackStackRecord;ZZZ)V
    .locals 9

    if-eqz p2, :cond_0

    invoke-virtual {p1, p4}, Landroid/app/BackStackRecord;->executePopOps(Z)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/app/BackStackRecord;->executeOps()V

    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    move-object v8, v2

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz p3, :cond_1

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x1

    move-object v2, p0

    move-object v3, v0

    move-object v4, v8

    invoke-static/range {v2 .. v7}, Landroid/app/FragmentTransition;->startTransitions(Landroid/app/FragmentManagerImpl;Ljava/util/ArrayList;Ljava/util/ArrayList;IIZ)V

    :cond_1
    if-eqz p4, :cond_2

    iget v2, p0, Landroid/app/FragmentManagerImpl;->mCurState:I

    invoke-virtual {p0, v2, v1}, Landroid/app/FragmentManagerImpl;->moveToState(IZ)V

    :cond_2
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v1, :cond_4

    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Fragment;

    if-eqz v3, :cond_3

    iget-object v4, v3, Landroid/app/Fragment;->mView:Landroid/view/View;

    if-eqz v4, :cond_3

    iget-boolean v4, v3, Landroid/app/Fragment;->mIsNewlyAdded:Z

    if-eqz v4, :cond_3

    iget v4, v3, Landroid/app/Fragment;->mContainerId:I

    invoke-virtual {p1, v4}, Landroid/app/BackStackRecord;->interactsWith(I)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x0

    iput-boolean v4, v3, Landroid/app/Fragment;->mIsNewlyAdded:Z

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    return-void
.end method

.method private dispatchMoveToState(I)V
    .locals 2

    iget-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mAllowOldReentrantBehavior:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, v1}, Landroid/app/FragmentManagerImpl;->moveToState(IZ)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mExecutingActions:Z

    invoke-virtual {p0, p1, v1}, Landroid/app/FragmentManagerImpl;->moveToState(IZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-boolean v1, p0, Landroid/app/FragmentManagerImpl;->mExecutingActions:Z

    nop

    :goto_0
    invoke-virtual {p0}, Landroid/app/FragmentManagerImpl;->execPendingActions()Z

    return-void

    :catchall_0
    move-exception v0

    iput-boolean v1, p0, Landroid/app/FragmentManagerImpl;->mExecutingActions:Z

    throw v0
.end method

.method private endAnimatingAwayFragments()V
    .locals 4

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    :goto_0
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_2

    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Fragment;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/app/Fragment;->getAnimatingAway()Landroid/animation/Animator;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Landroid/app/Fragment;->getAnimatingAway()Landroid/animation/Animator;

    move-result-object v3

    invoke-virtual {v3}, Landroid/animation/Animator;->end()V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    return-void
.end method

.method private ensureExecReady(Z)V
    .locals 2

    iget-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mExecutingActions:Z

    if-nez v0, :cond_3

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    invoke-virtual {v1}, Landroid/app/FragmentHostCallback;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_2

    if-nez p1, :cond_0

    invoke-direct {p0}, Landroid/app/FragmentManagerImpl;->checkStateLoss()V

    :cond_0
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mTmpRecords:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mTmpRecords:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mTmpIsPop:Ljava/util/ArrayList;

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mExecutingActions:Z

    const/4 v0, 0x0

    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, v1, v1}, Landroid/app/FragmentManagerImpl;->executePostponedTransaction(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mExecutingActions:Z

    nop

    return-void

    :catchall_0
    move-exception v1

    iput-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mExecutingActions:Z

    throw v1

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must be called from main thread of fragment host"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "FragmentManager is already executing transactions"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static executeOps(Ljava/util/ArrayList;Ljava/util/ArrayList;II)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/app/BackStackRecord;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Boolean;",
            ">;II)V"
        }
    .end annotation

    move v0, p2

    :goto_0
    if-ge v0, p3, :cond_2

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/BackStackRecord;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    const/4 v4, -0x1

    invoke-virtual {v1, v4}, Landroid/app/BackStackRecord;->bumpBackStackNesting(I)V

    add-int/lit8 v4, p3, -0x1

    if-ne v0, v4, :cond_0

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v1, v3}, Landroid/app/BackStackRecord;->executePopOps(Z)V

    goto :goto_2

    :cond_1
    invoke-virtual {v1, v3}, Landroid/app/BackStackRecord;->bumpBackStackNesting(I)V

    invoke-virtual {v1}, Landroid/app/BackStackRecord;->executeOps()V

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private executeOpsTogether(Ljava/util/ArrayList;Ljava/util/ArrayList;II)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/app/BackStackRecord;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Boolean;",
            ">;II)V"
        }
    .end annotation

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    move/from16 v9, p3

    move/from16 v10, p4

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/BackStackRecord;

    iget-boolean v11, v0, Landroid/app/BackStackRecord;->mReorderingAllowed:Z

    const/4 v0, 0x0

    iget-object v1, v6, Landroid/app/FragmentManagerImpl;->mTmpAddedFragments:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v6, Landroid/app/FragmentManagerImpl;->mTmpAddedFragments:Ljava/util/ArrayList;

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    :goto_0
    iget-object v1, v6, Landroid/app/FragmentManagerImpl;->mTmpAddedFragments:Ljava/util/ArrayList;

    iget-object v2, v6, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual/range {p0 .. p0}, Landroid/app/FragmentManagerImpl;->getPrimaryNavigationFragment()Landroid/app/Fragment;

    move-result-object v1

    move/from16 v2, p3

    move v12, v0

    move-object v13, v1

    :goto_1
    const/4 v14, 0x1

    if-ge v2, v10, :cond_4

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/BackStackRecord;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v3, v6, Landroid/app/FragmentManagerImpl;->mTmpAddedFragments:Ljava/util/ArrayList;

    invoke-virtual {v0, v3, v13}, Landroid/app/BackStackRecord;->expandOps(Ljava/util/ArrayList;Landroid/app/Fragment;)Landroid/app/Fragment;

    move-result-object v3

    move-object v13, v3

    goto :goto_2

    :cond_1
    iget-object v3, v6, Landroid/app/FragmentManagerImpl;->mTmpAddedFragments:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Landroid/app/BackStackRecord;->trackAddedFragmentsInPop(Ljava/util/ArrayList;)V

    :goto_2
    if-nez v12, :cond_3

    iget-boolean v3, v0, Landroid/app/BackStackRecord;->mAddToBackStack:Z

    if-eqz v3, :cond_2

    goto :goto_3

    :cond_2
    const/4 v14, 0x0

    :cond_3
    :goto_3
    move v12, v14

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    iget-object v0, v6, Landroid/app/FragmentManagerImpl;->mTmpAddedFragments:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    if-nez v11, :cond_5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    invoke-static/range {v0 .. v5}, Landroid/app/FragmentTransition;->startTransitions(Landroid/app/FragmentManagerImpl;Ljava/util/ArrayList;Ljava/util/ArrayList;IIZ)V

    :cond_5
    invoke-static/range {p1 .. p4}, Landroid/app/FragmentManagerImpl;->executeOps(Ljava/util/ArrayList;Ljava/util/ArrayList;II)V

    move/from16 v15, p4

    if-eqz v11, :cond_6

    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    move-object v5, v0

    invoke-direct {v6, v5}, Landroid/app/FragmentManagerImpl;->addAddedFragments(Landroid/util/ArraySet;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move-object/from16 v16, v5

    invoke-direct/range {v0 .. v5}, Landroid/app/FragmentManagerImpl;->postponePostponableTransactions(Ljava/util/ArrayList;Ljava/util/ArrayList;IILandroid/util/ArraySet;)I

    move-result v15

    move-object/from16 v0, v16

    invoke-direct {v6, v0}, Landroid/app/FragmentManagerImpl;->makeRemovedFragmentsInvisible(Landroid/util/ArraySet;)V

    :cond_6
    if-eq v15, v9, :cond_7

    if-eqz v11, :cond_7

    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    move v4, v15

    invoke-static/range {v0 .. v5}, Landroid/app/FragmentTransition;->startTransitions(Landroid/app/FragmentManagerImpl;Ljava/util/ArrayList;Ljava/util/ArrayList;IIZ)V

    iget v0, v6, Landroid/app/FragmentManagerImpl;->mCurState:I

    invoke-virtual {v6, v0, v14}, Landroid/app/FragmentManagerImpl;->moveToState(IZ)V

    :cond_7
    move/from16 v0, p3

    :goto_4
    if-ge v0, v10, :cond_9

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/BackStackRecord;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_8

    iget v3, v1, Landroid/app/BackStackRecord;->mIndex:I

    if-ltz v3, :cond_8

    iget v3, v1, Landroid/app/BackStackRecord;->mIndex:I

    invoke-virtual {v6, v3}, Landroid/app/FragmentManagerImpl;->freeBackStackIndex(I)V

    const/4 v3, -0x1

    iput v3, v1, Landroid/app/BackStackRecord;->mIndex:I

    :cond_8
    invoke-virtual {v1}, Landroid/app/BackStackRecord;->runOnCommitRunnables()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_9
    if-eqz v12, :cond_a

    invoke-virtual/range {p0 .. p0}, Landroid/app/FragmentManagerImpl;->reportBackStackChanged()V

    :cond_a
    return-void
.end method

.method private executePostponedTransaction(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/app/BackStackRecord;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mPostponedTransactions:Ljava/util/ArrayList;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_5

    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mPostponedTransactions:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/FragmentManagerImpl$StartEnterTransitionListener;

    const/4 v4, -0x1

    if-eqz p1, :cond_1

    invoke-static {v3}, Landroid/app/FragmentManagerImpl$StartEnterTransitionListener;->-$$Nest$fgetmIsBack(Landroid/app/FragmentManagerImpl$StartEnterTransitionListener;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-static {v3}, Landroid/app/FragmentManagerImpl$StartEnterTransitionListener;->-$$Nest$fgetmRecord(Landroid/app/FragmentManagerImpl$StartEnterTransitionListener;)Landroid/app/BackStackRecord;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v5

    if-eq v5, v4, :cond_1

    invoke-virtual {p2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v3}, Landroid/app/FragmentManagerImpl$StartEnterTransitionListener;->cancelTransaction()V

    goto :goto_2

    :cond_1
    invoke-virtual {v3}, Landroid/app/FragmentManagerImpl$StartEnterTransitionListener;->isReady()Z

    move-result v5

    if-nez v5, :cond_2

    if-eqz p1, :cond_4

    invoke-static {v3}, Landroid/app/FragmentManagerImpl$StartEnterTransitionListener;->-$$Nest$fgetmRecord(Landroid/app/FragmentManagerImpl$StartEnterTransitionListener;)Landroid/app/BackStackRecord;

    move-result-object v5

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, p1, v1, v6}, Landroid/app/BackStackRecord;->interactsWith(Ljava/util/ArrayList;II)Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_2
    iget-object v5, p0, Landroid/app/FragmentManagerImpl;->mPostponedTransactions:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    add-int/lit8 v2, v2, -0x1

    add-int/lit8 v0, v0, -0x1

    if-eqz p1, :cond_3

    invoke-static {v3}, Landroid/app/FragmentManagerImpl$StartEnterTransitionListener;->-$$Nest$fgetmIsBack(Landroid/app/FragmentManagerImpl$StartEnterTransitionListener;)Z

    move-result v5

    if-nez v5, :cond_3

    invoke-static {v3}, Landroid/app/FragmentManagerImpl$StartEnterTransitionListener;->-$$Nest$fgetmRecord(Landroid/app/FragmentManagerImpl$StartEnterTransitionListener;)Landroid/app/BackStackRecord;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v5

    move v6, v5

    if-eq v5, v4, :cond_3

    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3}, Landroid/app/FragmentManagerImpl$StartEnterTransitionListener;->cancelTransaction()V

    goto :goto_2

    :cond_3
    invoke-virtual {v3}, Landroid/app/FragmentManagerImpl$StartEnterTransitionListener;->completeTransaction()V

    :cond_4
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    return-void
.end method

.method private findFragmentUnder(Landroid/app/Fragment;)Landroid/app/Fragment;
    .locals 7

    iget-object v0, p1, Landroid/app/Fragment;->mContainer:Landroid/view/ViewGroup;

    iget-object v1, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    const/4 v2, 0x0

    if-eqz v0, :cond_3

    if-nez v1, :cond_0

    goto :goto_1

    :cond_0
    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    add-int/lit8 v4, v3, -0x1

    :goto_0
    if-ltz v4, :cond_2

    iget-object v5, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/Fragment;

    iget-object v6, v5, Landroid/app/Fragment;->mContainer:Landroid/view/ViewGroup;

    if-ne v6, v0, :cond_1

    iget-object v6, v5, Landroid/app/Fragment;->mView:Landroid/view/View;

    if-eqz v6, :cond_1

    return-object v5

    :cond_1
    add-int/lit8 v4, v4, -0x1

    goto :goto_0

    :cond_2
    return-object v2

    :cond_3
    :goto_1
    return-object v2
.end method

.method private forcePostponedTransactions()V
    .locals 2

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mPostponedTransactions:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    :goto_0
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mPostponedTransactions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mPostponedTransactions:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/FragmentManagerImpl$StartEnterTransitionListener;

    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl$StartEnterTransitionListener;->completeTransaction()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private generateOpsForPendingActions(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/app/BackStackRecord;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Boolean;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mPendingActions:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    :cond_0
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mPendingActions:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mPendingActions:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/FragmentManagerImpl$OpGenerator;

    invoke-interface {v3, p1, p2}, Landroid/app/FragmentManagerImpl$OpGenerator;->generateOps(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v3

    or-int/2addr v0, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mPendingActions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    invoke-virtual {v2}, Landroid/app/FragmentHostCallback;->getHandler()Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mExecCommit:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    monitor-exit p0

    return v0

    :cond_2
    :goto_1
    const/4 v1, 0x0

    monitor-exit p0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private makeRemovedFragmentsInvisible(Landroid/util/ArraySet;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/ArraySet<",
            "Landroid/app/Fragment;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/util/ArraySet;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    invoke-virtual {p1, v1}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Fragment;

    iget-boolean v3, v2, Landroid/app/Fragment;->mAdded:Z

    if-nez v3, :cond_0

    invoke-virtual {v2}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setTransitionAlpha(F)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method static modifiesAlpha(Landroid/animation/Animator;)Z
    .locals 7

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    :cond_0
    instance-of v1, p0, Landroid/animation/ValueAnimator;

    const/4 v2, 0x1

    if-eqz v1, :cond_3

    move-object v1, p0

    check-cast v1, Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->getValues()[Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    const/4 v4, 0x0

    :goto_0
    array-length v5, v3

    if-ge v4, v5, :cond_2

    aget-object v5, v3, v4

    invoke-virtual {v5}, Landroid/animation/PropertyValuesHolder;->getPropertyName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "alpha"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    return v2

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    goto :goto_2

    :cond_3
    instance-of v1, p0, Landroid/animation/AnimatorSet;

    if-eqz v1, :cond_5

    move-object v1, p0

    check-cast v1, Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->getChildAnimations()Ljava/util/ArrayList;

    move-result-object v1

    const/4 v3, 0x0

    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_6

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/animation/Animator;

    invoke-static {v4}, Landroid/app/FragmentManagerImpl;->modifiesAlpha(Landroid/animation/Animator;)Z

    move-result v4

    if-eqz v4, :cond_4

    return v2

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_5
    :goto_2
    nop

    :cond_6
    return v0
.end method

.method private popBackStackImmediate(Ljava/lang/String;II)Z
    .locals 9

    invoke-virtual {p0}, Landroid/app/FragmentManagerImpl;->execPendingActions()Z

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/app/FragmentManagerImpl;->ensureExecReady(Z)V

    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mPrimaryNav:Landroid/app/Fragment;

    if-eqz v1, :cond_0

    if-gez p2, :cond_0

    if-nez p1, :cond_0

    iget-object v1, v1, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    move-result v2

    if-eqz v2, :cond_0

    return v0

    :cond_0
    iget-object v4, p0, Landroid/app/FragmentManagerImpl;->mTmpRecords:Ljava/util/ArrayList;

    iget-object v5, p0, Landroid/app/FragmentManagerImpl;->mTmpIsPop:Ljava/util/ArrayList;

    move-object v3, p0

    move-object v6, p1

    move v7, p2

    move v8, p3

    invoke-virtual/range {v3 .. v8}, Landroid/app/FragmentManagerImpl;->popBackStackState(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;II)Z

    move-result v1

    if-eqz v1, :cond_1

    iput-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mExecutingActions:Z

    :try_start_0
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mTmpRecords:Ljava/util/ArrayList;

    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mTmpIsPop:Ljava/util/ArrayList;

    invoke-direct {p0, v0, v2}, Landroid/app/FragmentManagerImpl;->removeRedundantOperationsAndExecute(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Landroid/app/FragmentManagerImpl;->cleanupExec()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Landroid/app/FragmentManagerImpl;->cleanupExec()V

    throw v0

    :cond_1
    :goto_0
    invoke-virtual {p0}, Landroid/app/FragmentManagerImpl;->doPendingDeferredStart()V

    invoke-direct {p0}, Landroid/app/FragmentManagerImpl;->burpActive()V

    return v1
.end method

.method private postponePostponableTransactions(Ljava/util/ArrayList;Ljava/util/ArrayList;IILandroid/util/ArraySet;)I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/app/BackStackRecord;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Boolean;",
            ">;II",
            "Landroid/util/ArraySet<",
            "Landroid/app/Fragment;",
            ">;)I"
        }
    .end annotation

    move v0, p4

    add-int/lit8 v1, p4, -0x1

    :goto_0
    if-lt v1, p3, :cond_5

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/BackStackRecord;

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v2}, Landroid/app/BackStackRecord;->isPostponed()Z

    move-result v4

    const/4 v5, 0x0

    if-eqz v4, :cond_0

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {v2, p1, v4, p4}, Landroid/app/BackStackRecord;->interactsWith(Ljava/util/ArrayList;II)Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v4, 0x1

    goto :goto_1

    :cond_0
    move v4, v5

    :goto_1
    if-eqz v4, :cond_4

    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mPostponedTransactions:Ljava/util/ArrayList;

    if-nez v6, :cond_1

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Landroid/app/FragmentManagerImpl;->mPostponedTransactions:Ljava/util/ArrayList;

    :cond_1
    new-instance v6, Landroid/app/FragmentManagerImpl$StartEnterTransitionListener;

    invoke-direct {v6, v2, v3}, Landroid/app/FragmentManagerImpl$StartEnterTransitionListener;-><init>(Landroid/app/BackStackRecord;Z)V

    iget-object v7, p0, Landroid/app/FragmentManagerImpl;->mPostponedTransactions:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2, v6}, Landroid/app/BackStackRecord;->setOnStartPostponedListener(Landroid/app/Fragment$OnStartEnterTransitionListener;)V

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Landroid/app/BackStackRecord;->executeOps()V

    goto :goto_2

    :cond_2
    invoke-virtual {v2, v5}, Landroid/app/BackStackRecord;->executePopOps(Z)V

    :goto_2
    add-int/lit8 v0, v0, -0x1

    if-eq v1, v0, :cond_3

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    invoke-virtual {p1, v0, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :cond_3
    invoke-direct {p0, p5}, Landroid/app/FragmentManagerImpl;->addAddedFragments(Landroid/util/ArraySet;)V

    :cond_4
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_5
    return v0
.end method

.method private removeRedundantOperationsAndExecute(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/app/BackStackRecord;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_7

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_2

    :cond_0
    if-eqz p2, :cond_6

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_6

    invoke-direct {p0, p1, p2}, Landroid/app/FragmentManagerImpl;->executePostponedTransaction(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_4

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/BackStackRecord;

    iget-boolean v3, v3, Landroid/app/BackStackRecord;->mReorderingAllowed:Z

    if-nez v3, :cond_3

    if-eq v1, v2, :cond_1

    invoke-direct {p0, p1, p2, v1, v2}, Landroid/app/FragmentManagerImpl;->executeOpsTogether(Ljava/util/ArrayList;Ljava/util/ArrayList;II)V

    :cond_1
    add-int/lit8 v4, v2, 0x1

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_2

    :goto_1
    if-ge v4, v0, :cond_2

    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/BackStackRecord;

    iget-boolean v5, v5, Landroid/app/BackStackRecord;->mReorderingAllowed:Z

    if-nez v5, :cond_2

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_2
    invoke-direct {p0, p1, p2, v2, v4}, Landroid/app/FragmentManagerImpl;->executeOpsTogether(Ljava/util/ArrayList;Ljava/util/ArrayList;II)V

    move v1, v4

    add-int/lit8 v2, v4, -0x1

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    if-eq v1, v0, :cond_5

    invoke-direct {p0, p1, p2, v1, v0}, Landroid/app/FragmentManagerImpl;->executeOpsTogether(Ljava/util/ArrayList;Ljava/util/ArrayList;II)V

    :cond_5
    return-void

    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Internal error with the back stack records"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    :goto_2
    return-void
.end method

.method public static reverseTransit(I)I
    .locals 1

    const/4 v0, 0x0

    sparse-switch p0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const/16 v0, 0x1001

    goto :goto_0

    :sswitch_1
    const/16 v0, 0x1003

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x2002

    nop

    :goto_0
    return v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1001 -> :sswitch_2
        0x1003 -> :sswitch_1
        0x2002 -> :sswitch_0
    .end sparse-switch
.end method

.method private scheduleCommit()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mPostponedTransactions:Ljava/util/ArrayList;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mPendingActions:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v2, :cond_1

    move v1, v2

    :cond_1
    if-nez v0, :cond_2

    if-eqz v1, :cond_3

    :cond_2
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    invoke-virtual {v2}, Landroid/app/FragmentHostCallback;->getHandler()Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mExecCommit:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    invoke-virtual {v2}, Landroid/app/FragmentHostCallback;->getHandler()Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mExecCommit:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private setHWLayerAnimListenerIfAlpha(Landroid/view/View;Landroid/animation/Animator;)V
    .locals 1

    if-eqz p1, :cond_2

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p1, p2}, Landroid/app/FragmentManagerImpl;->shouldRunOnHWLayer(Landroid/view/View;Landroid/animation/Animator;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/app/FragmentManagerImpl$AnimateOnHWLayerIfNeededListener;

    invoke-direct {v0, p1}, Landroid/app/FragmentManagerImpl$AnimateOnHWLayerIfNeededListener;-><init>(Landroid/view/View;)V

    invoke-virtual {p2, v0}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    :cond_1
    return-void

    :cond_2
    :goto_0
    return-void
.end method

.method private static setRetaining(Landroid/app/FragmentManagerNonConfig;)V
    .locals 4

    if-nez p0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/app/FragmentManagerNonConfig;->getFragments()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Fragment;

    const/4 v3, 0x1

    iput-boolean v3, v2, Landroid/app/Fragment;->mRetaining:Z

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/app/FragmentManagerNonConfig;->getChildNonConfigs()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/FragmentManagerNonConfig;

    invoke-static {v3}, Landroid/app/FragmentManagerImpl;->setRetaining(Landroid/app/FragmentManagerNonConfig;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method static shouldRunOnHWLayer(Landroid/view/View;Landroid/animation/Animator;)Z
    .locals 2

    const/4 v0, 0x0

    if-eqz p0, :cond_2

    if-nez p1, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getLayerType()I

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/view/View;->hasOverlappingRendering()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p1}, Landroid/app/FragmentManagerImpl;->modifiesAlpha(Landroid/animation/Animator;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    nop

    :goto_0
    return v0

    :cond_2
    :goto_1
    return v0
.end method

.method private throwException(Ljava/lang/RuntimeException;)V
    .locals 8

    invoke-virtual {p1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FragmentManager"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/util/LogWriter;

    const/4 v2, 0x6

    invoke-direct {v0, v2, v1}, Landroid/util/LogWriter;-><init>(ILjava/lang/String;)V

    new-instance v2, Lcom/android/internal/util/FastPrintWriter;

    const/4 v3, 0x0

    const/16 v4, 0x400

    invoke-direct {v2, v0, v3, v4}, Lcom/android/internal/util/FastPrintWriter;-><init>(Ljava/io/Writer;ZI)V

    iget-object v4, p0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    const-string v5, "Failed dumping state"

    const/4 v6, 0x0

    const-string v7, "  "

    if-eqz v4, :cond_0

    const-string v4, "Activity state:"

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v4, p0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v4, v7, v6, v2, v3}, Landroid/app/FragmentHostCallback;->onDump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    invoke-virtual {v2}, Ljava/io/PrintWriter;->flush()V

    invoke-static {v1, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    goto :goto_1

    :cond_0
    const-string v4, "Fragment manager state:"

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_1
    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {p0, v7, v6, v2, v3}, Landroid/app/FragmentManagerImpl;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v3

    invoke-virtual {v2}, Ljava/io/PrintWriter;->flush()V

    invoke-static {v1, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_1
    invoke-virtual {v2}, Ljava/io/PrintWriter;->flush()V

    throw p1
.end method

.method public static transitToStyleIndex(IZ)I
    .locals 2

    const/4 v0, -0x1

    sparse-switch p0, :sswitch_data_0

    goto :goto_3

    :sswitch_0
    if-eqz p1, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :cond_0
    const/4 v1, 0x3

    :goto_0
    move v0, v1

    goto :goto_3

    :sswitch_1
    if-eqz p1, :cond_1

    const/4 v1, 0x4

    goto :goto_1

    :cond_1
    const/4 v1, 0x5

    :goto_1
    move v0, v1

    goto :goto_3

    :sswitch_2
    if-eqz p1, :cond_2

    const/4 v1, 0x0

    goto :goto_2

    :cond_2
    const/4 v1, 0x1

    :goto_2
    move v0, v1

    nop

    :goto_3
    return v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1001 -> :sswitch_2
        0x1003 -> :sswitch_1
        0x2002 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method addBackStackState(Landroid/app/BackStackRecord;)V
    .locals 1

    goto/32 :goto_8

    nop

    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_3

    nop

    :goto_1
    if-eqz v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_0

    nop

    :goto_2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_4

    nop

    :goto_3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_5

    nop

    :goto_4
    return-void

    :goto_5
    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    :goto_6
    goto/32 :goto_7

    nop

    :goto_7
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    goto/32 :goto_2

    nop

    :goto_8
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    goto/32 :goto_1

    nop
.end method

.method public addFragment(Landroid/app/Fragment;Z)V
    .locals 3

    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "add: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0, p1}, Landroid/app/FragmentManagerImpl;->makeActive(Landroid/app/Fragment;)V

    iget-boolean v0, p1, Landroid/app/Fragment;->mDetached:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    iput-boolean v0, p1, Landroid/app/Fragment;->mAdded:Z

    const/4 v1, 0x0

    iput-boolean v1, p1, Landroid/app/Fragment;->mRemoving:Z

    iget-object v2, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    if-nez v2, :cond_1

    iput-boolean v1, p1, Landroid/app/Fragment;->mHiddenChanged:Z

    :cond_1
    iget-boolean v1, p1, Landroid/app/Fragment;->mHasMenu:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p1, Landroid/app/Fragment;->mMenuVisible:Z

    if-eqz v1, :cond_2

    iput-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mNeedMenuInvalidate:Z

    :cond_2
    if-eqz p2, :cond_4

    invoke-virtual {p0, p1}, Landroid/app/FragmentManagerImpl;->moveToState(Landroid/app/Fragment;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Fragment already added: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    :goto_0
    return-void
.end method

.method public addOnBackStackChangedListener(Landroid/app/FragmentManager$OnBackStackChangedListener;)V
    .locals 1

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStackChangeListeners:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStackChangeListeners:Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStackChangeListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public allocBackStackIndex(Landroid/app/BackStackRecord;)I
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAvailBackStackIndices:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAvailBackStackIndices:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget-boolean v1, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    if-eqz v1, :cond_1

    const-string v1, "FragmentManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adding back stack index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    monitor-exit p0

    return v0

    :cond_2
    :goto_0
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    if-nez v0, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    :cond_3
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sget-boolean v1, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    if-eqz v1, :cond_4

    const-string v1, "FragmentManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Setting back stack index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public attachController(Landroid/app/FragmentHostCallback;Landroid/app/FragmentContainer;Landroid/app/Fragment;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/FragmentHostCallback<",
            "*>;",
            "Landroid/app/FragmentContainer;",
            "Landroid/app/Fragment;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    if-nez v0, :cond_1

    iput-object p1, p0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    iput-object p2, p0, Landroid/app/FragmentManagerImpl;->mContainer:Landroid/app/FragmentContainer;

    iput-object p3, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    invoke-virtual {p0}, Landroid/app/FragmentManagerImpl;->getTargetSdk()I

    move-result v0

    const/16 v1, 0x19

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mAllowOldReentrantBehavior:Z

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already attached"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public attachFragment(Landroid/app/Fragment;)V
    .locals 3

    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "attach: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p1, Landroid/app/Fragment;->mDetached:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    iput-boolean v0, p1, Landroid/app/Fragment;->mDetached:Z

    iget-boolean v0, p1, Landroid/app/Fragment;->mAdded:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    if-eqz v0, :cond_1

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "add from attach: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    iput-boolean v0, p1, Landroid/app/Fragment;->mAdded:Z

    iget-boolean v1, p1, Landroid/app/Fragment;->mHasMenu:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p1, Landroid/app/Fragment;->mMenuVisible:Z

    if-eqz v1, :cond_3

    iput-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mNeedMenuInvalidate:Z

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Fragment already added: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    :goto_0
    return-void
.end method

.method public beginTransaction()Landroid/app/FragmentTransaction;
    .locals 1

    new-instance v0, Landroid/app/BackStackRecord;

    invoke-direct {v0, p0}, Landroid/app/BackStackRecord;-><init>(Landroid/app/FragmentManagerImpl;)V

    return-object v0
.end method

.method completeShowHideFragment(Landroid/app/Fragment;)V
    .locals 6

    goto/32 :goto_2e

    nop

    :goto_0
    invoke-virtual {p1, v2}, Landroid/app/Fragment;->setHideReplaced(Z)V

    goto/32 :goto_11

    nop

    :goto_1
    if-nez v3, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_0

    nop

    :goto_2
    iput-boolean v2, p1, Landroid/app/Fragment;->mHiddenChanged:Z

    goto/32 :goto_37

    nop

    :goto_3
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto/32 :goto_38

    nop

    :goto_4
    iget-boolean v0, p1, Landroid/app/Fragment;->mHasMenu:Z

    goto/32 :goto_1c

    nop

    :goto_5
    invoke-virtual {p1}, Landroid/app/Fragment;->getNextTransitionStyle()I

    move-result v4

    goto/32 :goto_b

    nop

    :goto_6
    iget-object v3, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_33

    nop

    :goto_7
    if-nez v3, :cond_1

    goto/32 :goto_3b

    :cond_1
    goto/32 :goto_3a

    nop

    :goto_8
    iget-boolean v3, p1, Landroid/app/Fragment;->mHidden:Z

    goto/32 :goto_27

    nop

    :goto_9
    iget-boolean v0, p1, Landroid/app/Fragment;->mMenuVisible:Z

    goto/32 :goto_10

    nop

    :goto_a
    const/4 v1, 0x1

    goto/32 :goto_16

    nop

    :goto_b
    invoke-virtual {p0, p1, v0, v3, v4}, Landroid/app/FragmentManagerImpl;->loadAnimator(Landroid/app/Fragment;IZI)Landroid/animation/Animator;

    move-result-object v0

    goto/32 :goto_d

    nop

    :goto_c
    invoke-virtual {v0, v3}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    goto/32 :goto_8

    nop

    :goto_d
    if-nez v0, :cond_2

    goto/32 :goto_39

    :cond_2
    goto/32 :goto_15

    nop

    :goto_e
    invoke-virtual {p1, v2}, Landroid/app/Fragment;->setHideReplaced(Z)V

    :goto_f
    goto/32 :goto_1f

    nop

    :goto_10
    if-nez v0, :cond_3

    goto/32 :goto_2c

    :cond_3
    goto/32 :goto_2b

    nop

    :goto_11
    goto :goto_14

    :goto_12
    goto/32 :goto_18

    nop

    :goto_13
    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_14
    goto/32 :goto_6

    nop

    :goto_15
    iget-object v3, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_c

    nop

    :goto_16
    const/4 v2, 0x0

    goto/32 :goto_34

    nop

    :goto_17
    if-nez v4, :cond_4

    goto/32 :goto_f

    :cond_4
    goto/32 :goto_e

    nop

    :goto_18
    iget-object v3, p1, Landroid/app/Fragment;->mContainer:Landroid/view/ViewGroup;

    goto/32 :goto_3c

    nop

    :goto_19
    if-eqz v3, :cond_5

    goto/32 :goto_29

    :cond_5
    goto/32 :goto_32

    nop

    :goto_1a
    goto :goto_14

    :goto_1b
    goto/32 :goto_3f

    nop

    :goto_1c
    if-nez v0, :cond_6

    goto/32 :goto_2c

    :cond_6
    goto/32 :goto_9

    nop

    :goto_1d
    move v3, v2

    :goto_1e
    nop

    goto/32 :goto_3d

    nop

    :goto_1f
    iget-boolean v0, p1, Landroid/app/Fragment;->mAdded:Z

    goto/32 :goto_23

    nop

    :goto_20
    invoke-virtual {p1}, Landroid/app/Fragment;->isHideReplaced()Z

    move-result v4

    goto/32 :goto_17

    nop

    :goto_21
    iget-boolean v3, p1, Landroid/app/Fragment;->mHidden:Z

    goto/32 :goto_2f

    nop

    :goto_22
    return-void

    :goto_23
    if-nez v0, :cond_7

    goto/32 :goto_2c

    :cond_7
    goto/32 :goto_4

    nop

    :goto_24
    invoke-virtual {v0, v5}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    goto/32 :goto_1a

    nop

    :goto_25
    iget-boolean v3, p1, Landroid/app/Fragment;->mHidden:Z

    goto/32 :goto_3e

    nop

    :goto_26
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    goto/32 :goto_20

    nop

    :goto_27
    if-nez v3, :cond_8

    goto/32 :goto_1b

    :cond_8
    goto/32 :goto_2d

    nop

    :goto_28
    goto :goto_1e

    :goto_29
    goto/32 :goto_1d

    nop

    :goto_2a
    invoke-virtual {p1}, Landroid/app/Fragment;->isHideReplaced()Z

    move-result v3

    goto/32 :goto_19

    nop

    :goto_2b
    iput-boolean v1, p0, Landroid/app/FragmentManagerImpl;->mNeedMenuInvalidate:Z

    :goto_2c
    goto/32 :goto_2

    nop

    :goto_2d
    invoke-virtual {p1}, Landroid/app/Fragment;->isHideReplaced()Z

    move-result v3

    goto/32 :goto_1

    nop

    :goto_2e
    iget-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_a

    nop

    :goto_2f
    if-nez v3, :cond_9

    goto/32 :goto_29

    :cond_9
    goto/32 :goto_2a

    nop

    :goto_30
    invoke-direct {v5, p0, v3, v4}, Landroid/app/FragmentManagerImpl$3;-><init>(Landroid/app/FragmentManagerImpl;Landroid/view/ViewGroup;Landroid/view/View;)V

    goto/32 :goto_24

    nop

    :goto_31
    invoke-virtual {p1}, Landroid/app/Fragment;->getNextTransition()I

    move-result v0

    goto/32 :goto_25

    nop

    :goto_32
    const/16 v3, 0x8

    goto/32 :goto_28

    nop

    :goto_33
    invoke-direct {p0, v3, v0}, Landroid/app/FragmentManagerImpl;->setHWLayerAnimListenerIfAlpha(Landroid/view/View;Landroid/animation/Animator;)V

    goto/32 :goto_3

    nop

    :goto_34
    if-nez v0, :cond_a

    goto/32 :goto_f

    :cond_a
    goto/32 :goto_31

    nop

    :goto_35
    invoke-virtual {p1, v0}, Landroid/app/Fragment;->onHiddenChanged(Z)V

    goto/32 :goto_22

    nop

    :goto_36
    new-instance v5, Landroid/app/FragmentManagerImpl$3;

    goto/32 :goto_30

    nop

    :goto_37
    iget-boolean v0, p1, Landroid/app/Fragment;->mHidden:Z

    goto/32 :goto_35

    nop

    :goto_38
    goto/16 :goto_f

    :goto_39
    goto/32 :goto_21

    nop

    :goto_3a
    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->startViewTransition(Landroid/view/View;)V

    :goto_3b
    goto/32 :goto_36

    nop

    :goto_3c
    iget-object v4, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_7

    nop

    :goto_3d
    iget-object v4, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_26

    nop

    :goto_3e
    xor-int/2addr v3, v1

    goto/32 :goto_5

    nop

    :goto_3f
    iget-object v3, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_13

    nop
.end method

.method public detachFragment(Landroid/app/Fragment;)V
    .locals 4

    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "detach: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p1, Landroid/app/Fragment;->mDetached:Z

    if-nez v0, :cond_3

    const/4 v0, 0x1

    iput-boolean v0, p1, Landroid/app/Fragment;->mDetached:Z

    iget-boolean v1, p1, Landroid/app/Fragment;->mAdded:Z

    if-eqz v1, :cond_3

    sget-boolean v1, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    if-eqz v1, :cond_1

    const-string v1, "FragmentManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "remove from detach: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-boolean v1, p1, Landroid/app/Fragment;->mHasMenu:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p1, Landroid/app/Fragment;->mMenuVisible:Z

    if-eqz v1, :cond_2

    iput-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mNeedMenuInvalidate:Z

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p1, Landroid/app/Fragment;->mAdded:Z

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    :goto_0
    return-void
.end method

.method public dispatchActivityCreated()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mStateSaved:Z

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Landroid/app/FragmentManagerImpl;->dispatchMoveToState(I)V

    return-void
.end method

.method public dispatchConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Fragment;

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Landroid/app/Fragment;->performConfigurationChanged(Landroid/content/res/Configuration;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public dispatchContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    iget v0, p0, Landroid/app/FragmentManagerImpl;->mCurState:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ge v0, v2, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Fragment;

    if-eqz v3, :cond_1

    invoke-virtual {v3, p1}, Landroid/app/Fragment;->performContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v4

    if-eqz v4, :cond_1

    return v2

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return v1
.end method

.method public dispatchCreate()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mStateSaved:Z

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/app/FragmentManagerImpl;->dispatchMoveToState(I)V

    return-void
.end method

.method public dispatchCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .locals 5

    iget v0, p0, Landroid/app/FragmentManagerImpl;->mCurState:I

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Fragment;

    if-eqz v3, :cond_2

    invoke-virtual {v3, p1, p2}, Landroid/app/Fragment;->performCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v0, 0x1

    if-nez v1, :cond_1

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object v1, v4

    :cond_1
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mCreatedMenus:Ljava/util/ArrayList;

    if-eqz v2, :cond_6

    const/4 v2, 0x0

    :goto_1
    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mCreatedMenus:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_6

    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mCreatedMenus:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Fragment;

    if-eqz v1, :cond_4

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    :cond_4
    invoke-virtual {v3}, Landroid/app/Fragment;->onDestroyOptionsMenu()V

    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_6
    iput-object v1, p0, Landroid/app/FragmentManagerImpl;->mCreatedMenus:Ljava/util/ArrayList;

    return v0
.end method

.method public dispatchDestroy()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mDestroyed:Z

    invoke-virtual {p0}, Landroid/app/FragmentManagerImpl;->execPendingActions()Z

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/app/FragmentManagerImpl;->dispatchMoveToState(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mContainer:Landroid/app/FragmentContainer;

    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    return-void
.end method

.method public dispatchDestroyView()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/app/FragmentManagerImpl;->dispatchMoveToState(I)V

    return-void
.end method

.method public dispatchLowMemory()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Fragment;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/Fragment;->performLowMemory()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public dispatchMultiWindowModeChanged(Z)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Fragment;

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Landroid/app/Fragment;->performMultiWindowModeChanged(Z)V

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public dispatchMultiWindowModeChanged(ZLandroid/content/res/Configuration;)V
    .locals 2

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Fragment;

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1, p2}, Landroid/app/Fragment;->performMultiWindowModeChanged(ZLandroid/content/res/Configuration;)V

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method dispatchOnFragmentActivityCreated(Landroid/app/Fragment;Landroid/os/Bundle;Z)V
    .locals 3

    goto/32 :goto_1d

    nop

    :goto_0
    instance-of v1, v0, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_1a

    nop

    :goto_1
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    goto/32 :goto_1b

    nop

    :goto_3
    return-void

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_f

    nop

    :goto_5
    move-object v1, v0

    goto/32 :goto_12

    nop

    :goto_6
    invoke-virtual {v1, p1, p2, v2}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentActivityCreated(Landroid/app/Fragment;Landroid/os/Bundle;Z)V

    :goto_7
    goto/32 :goto_13

    nop

    :goto_8
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    goto/32 :goto_11

    nop

    :goto_9
    invoke-virtual {v0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_a
    invoke-virtual {v2, p0, p1, p2}, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;->onFragmentActivityCreated(Landroid/app/FragmentManager;Landroid/app/Fragment;Landroid/os/Bundle;)V

    :goto_b
    goto/32 :goto_d

    nop

    :goto_c
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    goto/32 :goto_17

    nop

    :goto_d
    goto :goto_2

    :goto_e
    goto/32 :goto_3

    nop

    :goto_f
    check-cast v1, Landroid/util/Pair;

    goto/32 :goto_14

    nop

    :goto_10
    if-nez v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_9

    nop

    :goto_11
    check-cast v2, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;

    goto/32 :goto_a

    nop

    :goto_12
    check-cast v1, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_15

    nop

    :goto_13
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mLifecycleCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    goto/32 :goto_1

    nop

    :goto_14
    if-nez p3, :cond_1

    goto/32 :goto_18

    :cond_1
    goto/32 :goto_1c

    nop

    :goto_15
    const/4 v2, 0x1

    goto/32 :goto_6

    nop

    :goto_16
    if-nez v1, :cond_2

    goto/32 :goto_e

    :cond_2
    goto/32 :goto_4

    nop

    :goto_17
    if-nez v2, :cond_3

    goto/32 :goto_b

    :cond_3
    :goto_18
    goto/32 :goto_8

    nop

    :goto_19
    check-cast v2, Ljava/lang/Boolean;

    goto/32 :goto_c

    nop

    :goto_1a
    if-nez v1, :cond_4

    goto/32 :goto_7

    :cond_4
    goto/32 :goto_5

    nop

    :goto_1b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_16

    nop

    :goto_1c
    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    goto/32 :goto_19

    nop

    :goto_1d
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    goto/32 :goto_10

    nop
.end method

.method dispatchOnFragmentAttached(Landroid/app/Fragment;Landroid/content/Context;Z)V
    .locals 3

    goto/32 :goto_1c

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_10

    nop

    :goto_1
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mLifecycleCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    goto/32 :goto_3

    nop

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_14

    nop

    :goto_3
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    goto/32 :goto_5

    nop

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_1b

    nop

    :goto_6
    invoke-virtual {v2, p0, p1, p2}, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;->onFragmentAttached(Landroid/app/FragmentManager;Landroid/app/Fragment;Landroid/content/Context;)V

    :goto_7
    goto/32 :goto_e

    nop

    :goto_8
    return-void

    :goto_9
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    goto/32 :goto_11

    nop

    :goto_a
    if-nez v1, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_13

    nop

    :goto_b
    invoke-virtual {v1, p1, p2, v2}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentAttached(Landroid/app/Fragment;Landroid/content/Context;Z)V

    :goto_c
    goto/32 :goto_1

    nop

    :goto_d
    check-cast v2, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;

    goto/32 :goto_6

    nop

    :goto_e
    goto :goto_4

    :goto_f
    goto/32 :goto_8

    nop

    :goto_10
    invoke-virtual {v0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    goto/32 :goto_17

    nop

    :goto_11
    if-nez v2, :cond_2

    goto/32 :goto_7

    :cond_2
    :goto_12
    goto/32 :goto_1d

    nop

    :goto_13
    move-object v1, v0

    goto/32 :goto_15

    nop

    :goto_14
    check-cast v1, Landroid/util/Pair;

    goto/32 :goto_18

    nop

    :goto_15
    check-cast v1, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_19

    nop

    :goto_16
    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    goto/32 :goto_1a

    nop

    :goto_17
    instance-of v1, v0, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_a

    nop

    :goto_18
    if-nez p3, :cond_3

    goto/32 :goto_12

    :cond_3
    goto/32 :goto_16

    nop

    :goto_19
    const/4 v2, 0x1

    goto/32 :goto_b

    nop

    :goto_1a
    check-cast v2, Ljava/lang/Boolean;

    goto/32 :goto_9

    nop

    :goto_1b
    if-nez v1, :cond_4

    goto/32 :goto_f

    :cond_4
    goto/32 :goto_2

    nop

    :goto_1c
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    goto/32 :goto_0

    nop

    :goto_1d
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    goto/32 :goto_d

    nop
.end method

.method dispatchOnFragmentCreated(Landroid/app/Fragment;Landroid/os/Bundle;Z)V
    .locals 3

    goto/32 :goto_1d

    nop

    :goto_0
    check-cast v2, Ljava/lang/Boolean;

    goto/32 :goto_10

    nop

    :goto_1
    invoke-virtual {v0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    goto/32 :goto_d

    nop

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_5

    nop

    :goto_3
    if-nez v2, :cond_0

    goto/32 :goto_12

    :cond_0
    :goto_4
    goto/32 :goto_14

    nop

    :goto_5
    check-cast v1, Landroid/util/Pair;

    goto/32 :goto_1c

    nop

    :goto_6
    check-cast v2, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;

    goto/32 :goto_11

    nop

    :goto_7
    return-void

    :goto_8
    if-nez v1, :cond_1

    goto/32 :goto_1a

    :cond_1
    goto/32 :goto_2

    nop

    :goto_9
    invoke-virtual {v1, p1, p2, v2}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentCreated(Landroid/app/Fragment;Landroid/os/Bundle;Z)V

    :goto_a
    goto/32 :goto_f

    nop

    :goto_b
    if-nez v0, :cond_2

    goto/32 :goto_a

    :cond_2
    goto/32 :goto_1

    nop

    :goto_c
    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    goto/32 :goto_0

    nop

    :goto_d
    instance-of v1, v0, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_18

    nop

    :goto_e
    const/4 v2, 0x1

    goto/32 :goto_9

    nop

    :goto_f
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mLifecycleCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    goto/32 :goto_15

    nop

    :goto_10
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    goto/32 :goto_3

    nop

    :goto_11
    invoke-virtual {v2, p0, p1, p2}, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;->onFragmentCreated(Landroid/app/FragmentManager;Landroid/app/Fragment;Landroid/os/Bundle;)V

    :goto_12
    goto/32 :goto_19

    nop

    :goto_13
    move-object v1, v0

    goto/32 :goto_1b

    nop

    :goto_14
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    goto/32 :goto_6

    nop

    :goto_15
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_16
    goto/32 :goto_17

    nop

    :goto_17
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_8

    nop

    :goto_18
    if-nez v1, :cond_3

    goto/32 :goto_a

    :cond_3
    goto/32 :goto_13

    nop

    :goto_19
    goto :goto_16

    :goto_1a
    goto/32 :goto_7

    nop

    :goto_1b
    check-cast v1, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_e

    nop

    :goto_1c
    if-nez p3, :cond_4

    goto/32 :goto_4

    :cond_4
    goto/32 :goto_c

    nop

    :goto_1d
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    goto/32 :goto_b

    nop
.end method

.method dispatchOnFragmentDestroyed(Landroid/app/Fragment;Z)V
    .locals 3

    goto/32 :goto_17

    nop

    :goto_0
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    goto/32 :goto_10

    nop

    :goto_2
    move-object v1, v0

    goto/32 :goto_9

    nop

    :goto_3
    goto :goto_1

    :goto_4
    goto/32 :goto_1d

    nop

    :goto_5
    instance-of v1, v0, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_e

    nop

    :goto_6
    invoke-virtual {v2, p0, p1}, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;->onFragmentDestroyed(Landroid/app/FragmentManager;Landroid/app/Fragment;)V

    :goto_7
    goto/32 :goto_3

    nop

    :goto_8
    check-cast v1, Landroid/util/Pair;

    goto/32 :goto_1a

    nop

    :goto_9
    check-cast v1, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_16

    nop

    :goto_a
    if-nez v2, :cond_0

    goto/32 :goto_7

    :cond_0
    :goto_b
    goto/32 :goto_c

    nop

    :goto_c
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    goto/32 :goto_13

    nop

    :goto_d
    invoke-virtual {v0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_e
    if-nez v1, :cond_1

    goto/32 :goto_12

    :cond_1
    goto/32 :goto_2

    nop

    :goto_f
    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    goto/32 :goto_1b

    nop

    :goto_10
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_14

    nop

    :goto_11
    invoke-virtual {v1, p1, v2}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentDestroyed(Landroid/app/Fragment;Z)V

    :goto_12
    goto/32 :goto_18

    nop

    :goto_13
    check-cast v2, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;

    goto/32 :goto_6

    nop

    :goto_14
    if-nez v1, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_1c

    nop

    :goto_15
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    goto/32 :goto_a

    nop

    :goto_16
    const/4 v2, 0x1

    goto/32 :goto_11

    nop

    :goto_17
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    goto/32 :goto_19

    nop

    :goto_18
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mLifecycleCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    goto/32 :goto_0

    nop

    :goto_19
    if-nez v0, :cond_3

    goto/32 :goto_12

    :cond_3
    goto/32 :goto_d

    nop

    :goto_1a
    if-nez p2, :cond_4

    goto/32 :goto_b

    :cond_4
    goto/32 :goto_f

    nop

    :goto_1b
    check-cast v2, Ljava/lang/Boolean;

    goto/32 :goto_15

    nop

    :goto_1c
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_1d
    return-void
.end method

.method dispatchOnFragmentDetached(Landroid/app/Fragment;Z)V
    .locals 3

    goto/32 :goto_1d

    nop

    :goto_0
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    goto/32 :goto_19

    nop

    :goto_1
    if-nez v1, :cond_0

    goto/32 :goto_13

    :cond_0
    goto/32 :goto_11

    nop

    :goto_2
    const/4 v2, 0x1

    goto/32 :goto_12

    nop

    :goto_3
    if-nez v1, :cond_1

    goto/32 :goto_f

    :cond_1
    goto/32 :goto_9

    nop

    :goto_4
    invoke-virtual {v2, p0, p1}, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;->onFragmentDetached(Landroid/app/FragmentManager;Landroid/app/Fragment;)V

    :goto_5
    goto/32 :goto_e

    nop

    :goto_6
    check-cast v2, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;

    goto/32 :goto_4

    nop

    :goto_7
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_3

    nop

    :goto_8
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mLifecycleCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    goto/32 :goto_17

    nop

    :goto_9
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_d

    nop

    :goto_a
    invoke-virtual {v0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    goto/32 :goto_b

    nop

    :goto_b
    instance-of v1, v0, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_1

    nop

    :goto_c
    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    goto/32 :goto_14

    nop

    :goto_d
    check-cast v1, Landroid/util/Pair;

    goto/32 :goto_1b

    nop

    :goto_e
    goto :goto_18

    :goto_f
    goto/32 :goto_16

    nop

    :goto_10
    check-cast v1, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_2

    nop

    :goto_11
    move-object v1, v0

    goto/32 :goto_10

    nop

    :goto_12
    invoke-virtual {v1, p1, v2}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentDetached(Landroid/app/Fragment;Z)V

    :goto_13
    goto/32 :goto_8

    nop

    :goto_14
    check-cast v2, Ljava/lang/Boolean;

    goto/32 :goto_0

    nop

    :goto_15
    if-nez v0, :cond_2

    goto/32 :goto_13

    :cond_2
    goto/32 :goto_a

    nop

    :goto_16
    return-void

    :goto_17
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_18
    goto/32 :goto_7

    nop

    :goto_19
    if-nez v2, :cond_3

    goto/32 :goto_5

    :cond_3
    :goto_1a
    goto/32 :goto_1c

    nop

    :goto_1b
    if-nez p2, :cond_4

    goto/32 :goto_1a

    :cond_4
    goto/32 :goto_c

    nop

    :goto_1c
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    goto/32 :goto_6

    nop

    :goto_1d
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    goto/32 :goto_15

    nop
.end method

.method dispatchOnFragmentPaused(Landroid/app/Fragment;Z)V
    .locals 3

    goto/32 :goto_1d

    nop

    :goto_0
    check-cast v2, Ljava/lang/Boolean;

    goto/32 :goto_c

    nop

    :goto_1
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    goto/32 :goto_b

    nop

    :goto_3
    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    goto/32 :goto_0

    nop

    :goto_4
    instance-of v1, v0, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_10

    nop

    :goto_5
    invoke-virtual {v0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_6
    check-cast v1, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_16

    nop

    :goto_7
    if-nez v0, :cond_0

    goto/32 :goto_19

    :cond_0
    goto/32 :goto_5

    nop

    :goto_8
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    goto/32 :goto_9

    nop

    :goto_9
    check-cast v2, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;

    goto/32 :goto_d

    nop

    :goto_a
    return-void

    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_15

    nop

    :goto_c
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    goto/32 :goto_13

    nop

    :goto_d
    invoke-virtual {v2, p0, p1}, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;->onFragmentPaused(Landroid/app/FragmentManager;Landroid/app/Fragment;)V

    :goto_e
    goto/32 :goto_1b

    nop

    :goto_f
    check-cast v1, Landroid/util/Pair;

    goto/32 :goto_1a

    nop

    :goto_10
    if-nez v1, :cond_1

    goto/32 :goto_19

    :cond_1
    goto/32 :goto_12

    nop

    :goto_11
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mLifecycleCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    goto/32 :goto_1

    nop

    :goto_12
    move-object v1, v0

    goto/32 :goto_6

    nop

    :goto_13
    if-nez v2, :cond_2

    goto/32 :goto_e

    :cond_2
    :goto_14
    goto/32 :goto_8

    nop

    :goto_15
    if-nez v1, :cond_3

    goto/32 :goto_1c

    :cond_3
    goto/32 :goto_17

    nop

    :goto_16
    const/4 v2, 0x1

    goto/32 :goto_18

    nop

    :goto_17
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_f

    nop

    :goto_18
    invoke-virtual {v1, p1, v2}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentPaused(Landroid/app/Fragment;Z)V

    :goto_19
    goto/32 :goto_11

    nop

    :goto_1a
    if-nez p2, :cond_4

    goto/32 :goto_14

    :cond_4
    goto/32 :goto_3

    nop

    :goto_1b
    goto/16 :goto_2

    :goto_1c
    goto/32 :goto_a

    nop

    :goto_1d
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    goto/32 :goto_7

    nop
.end method

.method dispatchOnFragmentPreAttached(Landroid/app/Fragment;Landroid/content/Context;Z)V
    .locals 3

    goto/32 :goto_1a

    nop

    :goto_0
    return-void

    :goto_1
    move-object v1, v0

    goto/32 :goto_1b

    nop

    :goto_2
    instance-of v1, v0, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_1c

    nop

    :goto_3
    invoke-virtual {v1, p1, p2, v2}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentPreAttached(Landroid/app/Fragment;Landroid/content/Context;Z)V

    :goto_4
    goto/32 :goto_17

    nop

    :goto_5
    if-nez v1, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_12

    nop

    :goto_6
    const/4 v2, 0x1

    goto/32 :goto_3

    nop

    :goto_7
    if-nez p3, :cond_1

    goto/32 :goto_19

    :cond_1
    goto/32 :goto_13

    nop

    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_5

    nop

    :goto_9
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    goto/32 :goto_1d

    nop

    :goto_a
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_b
    goto/32 :goto_8

    nop

    :goto_c
    goto :goto_b

    :goto_d
    goto/32 :goto_0

    nop

    :goto_e
    if-nez v0, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_14

    nop

    :goto_f
    check-cast v2, Ljava/lang/Boolean;

    goto/32 :goto_11

    nop

    :goto_10
    check-cast v1, Landroid/util/Pair;

    goto/32 :goto_7

    nop

    :goto_11
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    goto/32 :goto_18

    nop

    :goto_12
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_10

    nop

    :goto_13
    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    goto/32 :goto_f

    nop

    :goto_14
    invoke-virtual {v0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_15
    invoke-virtual {v2, p0, p1, p2}, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;->onFragmentPreAttached(Landroid/app/FragmentManager;Landroid/app/Fragment;Landroid/content/Context;)V

    :goto_16
    goto/32 :goto_c

    nop

    :goto_17
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mLifecycleCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    goto/32 :goto_a

    nop

    :goto_18
    if-nez v2, :cond_3

    goto/32 :goto_16

    :cond_3
    :goto_19
    goto/32 :goto_9

    nop

    :goto_1a
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    goto/32 :goto_e

    nop

    :goto_1b
    check-cast v1, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_6

    nop

    :goto_1c
    if-nez v1, :cond_4

    goto/32 :goto_4

    :cond_4
    goto/32 :goto_1

    nop

    :goto_1d
    check-cast v2, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;

    goto/32 :goto_15

    nop
.end method

.method dispatchOnFragmentPreCreated(Landroid/app/Fragment;Landroid/os/Bundle;Z)V
    .locals 3

    goto/32 :goto_2

    nop

    :goto_0
    check-cast v1, Landroid/util/Pair;

    goto/32 :goto_9

    nop

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_19

    :cond_0
    goto/32 :goto_8

    nop

    :goto_2
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    goto/32 :goto_1

    nop

    :goto_3
    check-cast v2, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;

    goto/32 :goto_14

    nop

    :goto_4
    instance-of v1, v0, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_6

    nop

    :goto_5
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    goto/32 :goto_3

    nop

    :goto_6
    if-nez v1, :cond_1

    goto/32 :goto_19

    :cond_1
    goto/32 :goto_1d

    nop

    :goto_7
    check-cast v2, Ljava/lang/Boolean;

    goto/32 :goto_b

    nop

    :goto_8
    invoke-virtual {v0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_9
    if-nez p3, :cond_2

    goto/32 :goto_17

    :cond_2
    goto/32 :goto_a

    nop

    :goto_a
    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    goto/32 :goto_7

    nop

    :goto_b
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    goto/32 :goto_16

    nop

    :goto_c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_1a

    nop

    :goto_d
    const/4 v2, 0x1

    goto/32 :goto_18

    nop

    :goto_e
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mLifecycleCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    goto/32 :goto_11

    nop

    :goto_f
    check-cast v1, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_d

    nop

    :goto_10
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_11
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_12
    goto/32 :goto_c

    nop

    :goto_13
    return-void

    :goto_14
    invoke-virtual {v2, p0, p1, p2}, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;->onFragmentPreCreated(Landroid/app/FragmentManager;Landroid/app/Fragment;Landroid/os/Bundle;)V

    :goto_15
    goto/32 :goto_1b

    nop

    :goto_16
    if-nez v2, :cond_3

    goto/32 :goto_15

    :cond_3
    :goto_17
    goto/32 :goto_5

    nop

    :goto_18
    invoke-virtual {v1, p1, p2, v2}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentPreCreated(Landroid/app/Fragment;Landroid/os/Bundle;Z)V

    :goto_19
    goto/32 :goto_e

    nop

    :goto_1a
    if-nez v1, :cond_4

    goto/32 :goto_1c

    :cond_4
    goto/32 :goto_10

    nop

    :goto_1b
    goto :goto_12

    :goto_1c
    goto/32 :goto_13

    nop

    :goto_1d
    move-object v1, v0

    goto/32 :goto_f

    nop
.end method

.method dispatchOnFragmentResumed(Landroid/app/Fragment;Z)V
    .locals 3

    goto/32 :goto_10

    nop

    :goto_0
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mLifecycleCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    goto/32 :goto_1b

    nop

    :goto_1
    const/4 v2, 0x1

    goto/32 :goto_8

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_19

    nop

    :goto_3
    if-nez v2, :cond_1

    goto/32 :goto_e

    :cond_1
    :goto_4
    goto/32 :goto_c

    nop

    :goto_5
    if-nez v1, :cond_2

    goto/32 :goto_9

    :cond_2
    goto/32 :goto_6

    nop

    :goto_6
    move-object v1, v0

    goto/32 :goto_7

    nop

    :goto_7
    check-cast v1, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_1

    nop

    :goto_8
    invoke-virtual {v1, p1, v2}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentResumed(Landroid/app/Fragment;Z)V

    :goto_9
    goto/32 :goto_0

    nop

    :goto_a
    if-nez p2, :cond_3

    goto/32 :goto_4

    :cond_3
    goto/32 :goto_1d

    nop

    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_11

    nop

    :goto_c
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    goto/32 :goto_f

    nop

    :goto_d
    invoke-virtual {v2, p0, p1}, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;->onFragmentResumed(Landroid/app/FragmentManager;Landroid/app/Fragment;)V

    :goto_e
    goto/32 :goto_15

    nop

    :goto_f
    check-cast v2, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;

    goto/32 :goto_d

    nop

    :goto_10
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    goto/32 :goto_2

    nop

    :goto_11
    check-cast v1, Landroid/util/Pair;

    goto/32 :goto_a

    nop

    :goto_12
    if-nez v1, :cond_4

    goto/32 :goto_16

    :cond_4
    goto/32 :goto_b

    nop

    :goto_13
    return-void

    :goto_14
    check-cast v2, Ljava/lang/Boolean;

    goto/32 :goto_18

    nop

    :goto_15
    goto :goto_1c

    :goto_16
    goto/32 :goto_13

    nop

    :goto_17
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_12

    nop

    :goto_18
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    goto/32 :goto_3

    nop

    :goto_19
    invoke-virtual {v0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    goto/32 :goto_1a

    nop

    :goto_1a
    instance-of v1, v0, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_5

    nop

    :goto_1b
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1c
    goto/32 :goto_17

    nop

    :goto_1d
    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    goto/32 :goto_14

    nop
.end method

.method dispatchOnFragmentSaveInstanceState(Landroid/app/Fragment;Landroid/os/Bundle;Z)V
    .locals 3

    goto/32 :goto_15

    nop

    :goto_0
    goto/16 :goto_18

    :goto_1
    goto/32 :goto_14

    nop

    :goto_2
    const/4 v2, 0x1

    goto/32 :goto_5

    nop

    :goto_3
    if-nez v2, :cond_0

    goto/32 :goto_11

    :cond_0
    :goto_4
    goto/32 :goto_d

    nop

    :goto_5
    invoke-virtual {v1, p1, p2, v2}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentSaveInstanceState(Landroid/app/Fragment;Landroid/os/Bundle;Z)V

    :goto_6
    goto/32 :goto_7

    nop

    :goto_7
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mLifecycleCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    goto/32 :goto_17

    nop

    :goto_8
    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    goto/32 :goto_1a

    nop

    :goto_9
    check-cast v2, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;

    goto/32 :goto_10

    nop

    :goto_a
    if-nez v0, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_1d

    nop

    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_19

    nop

    :goto_c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_12

    nop

    :goto_d
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    goto/32 :goto_9

    nop

    :goto_e
    if-nez p3, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_8

    nop

    :goto_f
    instance-of v1, v0, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_1c

    nop

    :goto_10
    invoke-virtual {v2, p0, p1, p2}, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;->onFragmentSaveInstanceState(Landroid/app/FragmentManager;Landroid/app/Fragment;Landroid/os/Bundle;)V

    :goto_11
    goto/32 :goto_0

    nop

    :goto_12
    if-nez v1, :cond_3

    goto/32 :goto_1

    :cond_3
    goto/32 :goto_b

    nop

    :goto_13
    move-object v1, v0

    goto/32 :goto_16

    nop

    :goto_14
    return-void

    :goto_15
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    goto/32 :goto_a

    nop

    :goto_16
    check-cast v1, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_2

    nop

    :goto_17
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_18
    goto/32 :goto_c

    nop

    :goto_19
    check-cast v1, Landroid/util/Pair;

    goto/32 :goto_e

    nop

    :goto_1a
    check-cast v2, Ljava/lang/Boolean;

    goto/32 :goto_1b

    nop

    :goto_1b
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    goto/32 :goto_3

    nop

    :goto_1c
    if-nez v1, :cond_4

    goto/32 :goto_6

    :cond_4
    goto/32 :goto_13

    nop

    :goto_1d
    invoke-virtual {v0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    goto/32 :goto_f

    nop
.end method

.method dispatchOnFragmentStarted(Landroid/app/Fragment;Z)V
    .locals 3

    goto/32 :goto_6

    nop

    :goto_0
    check-cast v2, Ljava/lang/Boolean;

    goto/32 :goto_14

    nop

    :goto_1
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    goto/32 :goto_12

    nop

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_15

    nop

    :goto_4
    move-object v1, v0

    goto/32 :goto_e

    nop

    :goto_5
    return-void

    :goto_6
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    goto/32 :goto_18

    nop

    :goto_7
    const/4 v2, 0x1

    goto/32 :goto_f

    nop

    :goto_8
    check-cast v2, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;

    goto/32 :goto_9

    nop

    :goto_9
    invoke-virtual {v2, p0, p1}, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;->onFragmentStarted(Landroid/app/FragmentManager;Landroid/app/Fragment;)V

    :goto_a
    goto/32 :goto_1b

    nop

    :goto_b
    if-nez p2, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_1d

    nop

    :goto_c
    if-nez v2, :cond_1

    goto/32 :goto_a

    :cond_1
    :goto_d
    goto/32 :goto_1a

    nop

    :goto_e
    check-cast v1, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_7

    nop

    :goto_f
    invoke-virtual {v1, p1, v2}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentStarted(Landroid/app/Fragment;Z)V

    :goto_10
    goto/32 :goto_16

    nop

    :goto_11
    instance-of v1, v0, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_13

    nop

    :goto_12
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_19

    nop

    :goto_13
    if-nez v1, :cond_2

    goto/32 :goto_10

    :cond_2
    goto/32 :goto_4

    nop

    :goto_14
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    goto/32 :goto_c

    nop

    :goto_15
    check-cast v1, Landroid/util/Pair;

    goto/32 :goto_b

    nop

    :goto_16
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mLifecycleCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    goto/32 :goto_1

    nop

    :goto_17
    invoke-virtual {v0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    goto/32 :goto_11

    nop

    :goto_18
    if-nez v0, :cond_3

    goto/32 :goto_10

    :cond_3
    goto/32 :goto_17

    nop

    :goto_19
    if-nez v1, :cond_4

    goto/32 :goto_1c

    :cond_4
    goto/32 :goto_3

    nop

    :goto_1a
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    goto/32 :goto_8

    nop

    :goto_1b
    goto/16 :goto_2

    :goto_1c
    goto/32 :goto_5

    nop

    :goto_1d
    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    goto/32 :goto_0

    nop
.end method

.method dispatchOnFragmentStopped(Landroid/app/Fragment;Z)V
    .locals 3

    goto/32 :goto_1c

    nop

    :goto_0
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    goto/32 :goto_1

    nop

    :goto_1
    check-cast v2, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;

    goto/32 :goto_12

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_16

    :cond_0
    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {v0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_4
    if-nez v1, :cond_1

    goto/32 :goto_16

    :cond_1
    goto/32 :goto_1d

    nop

    :goto_5
    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    goto/32 :goto_6

    nop

    :goto_6
    check-cast v2, Ljava/lang/Boolean;

    goto/32 :goto_7

    nop

    :goto_7
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    goto/32 :goto_b

    nop

    :goto_8
    check-cast v1, Landroid/util/Pair;

    goto/32 :goto_19

    nop

    :goto_9
    instance-of v1, v0, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_4

    nop

    :goto_a
    check-cast v1, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_11

    nop

    :goto_b
    if-nez v2, :cond_2

    goto/32 :goto_13

    :cond_2
    :goto_c
    goto/32 :goto_0

    nop

    :goto_d
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_e
    if-nez v1, :cond_3

    goto/32 :goto_10

    :cond_3
    goto/32 :goto_d

    nop

    :goto_f
    goto :goto_18

    :goto_10
    goto/32 :goto_14

    nop

    :goto_11
    const/4 v2, 0x1

    goto/32 :goto_15

    nop

    :goto_12
    invoke-virtual {v2, p0, p1}, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;->onFragmentStopped(Landroid/app/FragmentManager;Landroid/app/Fragment;)V

    :goto_13
    goto/32 :goto_f

    nop

    :goto_14
    return-void

    :goto_15
    invoke-virtual {v1, p1, v2}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentStopped(Landroid/app/Fragment;Z)V

    :goto_16
    goto/32 :goto_1a

    nop

    :goto_17
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_18
    goto/32 :goto_1b

    nop

    :goto_19
    if-nez p2, :cond_4

    goto/32 :goto_c

    :cond_4
    goto/32 :goto_5

    nop

    :goto_1a
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mLifecycleCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    goto/32 :goto_17

    nop

    :goto_1b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_e

    nop

    :goto_1c
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    goto/32 :goto_2

    nop

    :goto_1d
    move-object v1, v0

    goto/32 :goto_a

    nop
.end method

.method dispatchOnFragmentViewCreated(Landroid/app/Fragment;Landroid/view/View;Landroid/os/Bundle;Z)V
    .locals 3

    goto/32 :goto_16

    nop

    :goto_0
    check-cast v1, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_8

    nop

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_10

    nop

    :goto_2
    check-cast v2, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;

    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {v2, p0, p1, p2, p3}, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;->onFragmentViewCreated(Landroid/app/FragmentManager;Landroid/app/Fragment;Landroid/view/View;Landroid/os/Bundle;)V

    :goto_4
    goto/32 :goto_5

    nop

    :goto_5
    goto/16 :goto_1c

    :goto_6
    goto/32 :goto_f

    nop

    :goto_7
    instance-of v1, v0, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_a

    nop

    :goto_8
    const/4 v2, 0x1

    goto/32 :goto_c

    nop

    :goto_9
    if-nez v0, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_13

    nop

    :goto_a
    if-nez v1, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_17

    nop

    :goto_b
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    goto/32 :goto_2

    nop

    :goto_c
    invoke-virtual {v1, p1, p2, p3, v2}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentViewCreated(Landroid/app/Fragment;Landroid/view/View;Landroid/os/Bundle;Z)V

    :goto_d
    goto/32 :goto_18

    nop

    :goto_e
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    goto/32 :goto_11

    nop

    :goto_f
    return-void

    :goto_10
    check-cast v1, Landroid/util/Pair;

    goto/32 :goto_15

    nop

    :goto_11
    if-nez v2, :cond_2

    goto/32 :goto_4

    :cond_2
    :goto_12
    goto/32 :goto_b

    nop

    :goto_13
    invoke-virtual {v0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_14
    check-cast v2, Ljava/lang/Boolean;

    goto/32 :goto_e

    nop

    :goto_15
    if-nez p4, :cond_3

    goto/32 :goto_12

    :cond_3
    goto/32 :goto_19

    nop

    :goto_16
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    goto/32 :goto_9

    nop

    :goto_17
    move-object v1, v0

    goto/32 :goto_0

    nop

    :goto_18
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mLifecycleCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    goto/32 :goto_1b

    nop

    :goto_19
    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    goto/32 :goto_14

    nop

    :goto_1a
    if-nez v1, :cond_4

    goto/32 :goto_6

    :cond_4
    goto/32 :goto_1

    nop

    :goto_1b
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1c
    goto/32 :goto_1d

    nop

    :goto_1d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_1a

    nop
.end method

.method dispatchOnFragmentViewDestroyed(Landroid/app/Fragment;Z)V
    .locals 3

    goto/32 :goto_b

    nop

    :goto_0
    check-cast v1, Landroid/util/Pair;

    goto/32 :goto_6

    nop

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_2
    if-nez v1, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_7

    nop

    :goto_3
    if-nez v0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_1a

    nop

    :goto_4
    invoke-virtual {v1, p1, v2}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentViewDestroyed(Landroid/app/Fragment;Z)V

    :goto_5
    goto/32 :goto_15

    nop

    :goto_6
    if-nez p2, :cond_2

    goto/32 :goto_12

    :cond_2
    goto/32 :goto_13

    nop

    :goto_7
    move-object v1, v0

    goto/32 :goto_14

    nop

    :goto_8
    check-cast v2, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;

    goto/32 :goto_17

    nop

    :goto_9
    return-void

    :goto_a
    if-nez v1, :cond_3

    goto/32 :goto_f

    :cond_3
    goto/32 :goto_1

    nop

    :goto_b
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    goto/32 :goto_3

    nop

    :goto_c
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_d
    goto/32 :goto_19

    nop

    :goto_e
    goto :goto_d

    :goto_f
    goto/32 :goto_9

    nop

    :goto_10
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    goto/32 :goto_8

    nop

    :goto_11
    if-nez v2, :cond_4

    goto/32 :goto_18

    :cond_4
    :goto_12
    goto/32 :goto_10

    nop

    :goto_13
    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    goto/32 :goto_1c

    nop

    :goto_14
    check-cast v1, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_1d

    nop

    :goto_15
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mLifecycleCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    goto/32 :goto_c

    nop

    :goto_16
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    goto/32 :goto_11

    nop

    :goto_17
    invoke-virtual {v2, p0, p1}, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;->onFragmentViewDestroyed(Landroid/app/FragmentManager;Landroid/app/Fragment;)V

    :goto_18
    goto/32 :goto_e

    nop

    :goto_19
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_a

    nop

    :goto_1a
    invoke-virtual {v0}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    goto/32 :goto_1b

    nop

    :goto_1b
    instance-of v1, v0, Landroid/app/FragmentManagerImpl;

    goto/32 :goto_2

    nop

    :goto_1c
    check-cast v2, Ljava/lang/Boolean;

    goto/32 :goto_16

    nop

    :goto_1d
    const/4 v2, 0x1

    goto/32 :goto_4

    nop
.end method

.method public dispatchOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    iget v0, p0, Landroid/app/FragmentManagerImpl;->mCurState:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ge v0, v2, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Fragment;

    if-eqz v3, :cond_1

    invoke-virtual {v3, p1}, Landroid/app/Fragment;->performOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v4

    if-eqz v4, :cond_1

    return v2

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return v1
.end method

.method public dispatchOptionsMenuClosed(Landroid/view/Menu;)V
    .locals 2

    iget v0, p0, Landroid/app/FragmentManagerImpl;->mCurState:I

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Fragment;

    if-eqz v1, :cond_1

    invoke-virtual {v1, p1}, Landroid/app/Fragment;->performOptionsMenuClosed(Landroid/view/Menu;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public dispatchPause()V
    .locals 1

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Landroid/app/FragmentManagerImpl;->dispatchMoveToState(I)V

    return-void
.end method

.method public dispatchPictureInPictureModeChanged(Z)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Fragment;

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Landroid/app/Fragment;->performPictureInPictureModeChanged(Z)V

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public dispatchPictureInPictureModeChanged(ZLandroid/content/res/Configuration;)V
    .locals 2

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Fragment;

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1, p2}, Landroid/app/Fragment;->performPictureInPictureModeChanged(ZLandroid/content/res/Configuration;)V

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public dispatchPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    iget v0, p0, Landroid/app/FragmentManagerImpl;->mCurState:I

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Fragment;

    if-eqz v2, :cond_1

    invoke-virtual {v2, p1}, Landroid/app/Fragment;->performPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v0, 0x1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return v0
.end method

.method public dispatchResume()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mStateSaved:Z

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Landroid/app/FragmentManagerImpl;->dispatchMoveToState(I)V

    return-void
.end method

.method public dispatchStart()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mStateSaved:Z

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Landroid/app/FragmentManagerImpl;->dispatchMoveToState(I)V

    return-void
.end method

.method public dispatchStop()V
    .locals 1

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Landroid/app/FragmentManagerImpl;->dispatchMoveToState(I)V

    return-void
.end method

.method public dispatchTrimMemory(I)V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Fragment;

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Landroid/app/Fragment;->performTrimMemory(I)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method doPendingDeferredStart()V
    .locals 4

    goto/32 :goto_13

    nop

    :goto_0
    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_5

    nop

    :goto_1
    const/4 v1, 0x0

    :goto_2
    goto/32 :goto_11

    nop

    :goto_3
    invoke-virtual {p0}, Landroid/app/FragmentManagerImpl;->startPendingDeferredFragments()V

    :goto_4
    goto/32 :goto_14

    nop

    :goto_5
    check-cast v2, Landroid/app/Fragment;

    goto/32 :goto_e

    nop

    :goto_6
    const/4 v0, 0x0

    goto/32 :goto_1

    nop

    :goto_7
    iget-object v3, v2, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    goto/32 :goto_18

    nop

    :goto_8
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    goto/32 :goto_0

    nop

    :goto_9
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    goto/32 :goto_b

    nop

    :goto_a
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_6

    nop

    :goto_b
    if-lt v1, v2, :cond_1

    goto/32 :goto_10

    :cond_1
    goto/32 :goto_8

    nop

    :goto_c
    if-eqz v0, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_19

    nop

    :goto_d
    iget-object v3, v2, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    goto/32 :goto_1a

    nop

    :goto_e
    if-nez v2, :cond_3

    goto/32 :goto_16

    :cond_3
    goto/32 :goto_7

    nop

    :goto_f
    goto :goto_2

    :goto_10
    goto/32 :goto_c

    nop

    :goto_11
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    goto/32 :goto_9

    nop

    :goto_12
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_f

    nop

    :goto_13
    iget-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mHavePendingDeferredStart:Z

    goto/32 :goto_a

    nop

    :goto_14
    return-void

    :goto_15
    or-int/2addr v0, v3

    :goto_16
    goto/32 :goto_12

    nop

    :goto_17
    iput-boolean v1, p0, Landroid/app/FragmentManagerImpl;->mHavePendingDeferredStart:Z

    goto/32 :goto_3

    nop

    :goto_18
    if-nez v3, :cond_4

    goto/32 :goto_16

    :cond_4
    goto/32 :goto_d

    nop

    :goto_19
    const/4 v1, 0x0

    goto/32 :goto_17

    nop

    :goto_1a
    invoke-virtual {v3}, Landroid/app/LoaderManagerImpl;->hasRunningLoaders()Z

    move-result v3

    goto/32 :goto_15

    nop
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "    "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "Active Fragments in "

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, ":"

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Fragment;

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v4, "  #"

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    const-string v4, ": "

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    if-eqz v3, :cond_0

    invoke-virtual {v3, v0, p2, p3, p4}, Landroid/app/Fragment;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "Added Fragments:"

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v1, :cond_2

    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Fragment;

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v4, "  #"

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    const-string v4, ": "

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/app/Fragment;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mCreatedMenus:Ljava/util/ArrayList;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "Fragments Created Menus:"

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v1, :cond_3

    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mCreatedMenus:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Fragment;

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v4, "  #"

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    const-string v4, ": "

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/app/Fragment;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_4

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "Back Stack:"

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v1, :cond_4

    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/BackStackRecord;

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v4, "  #"

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    const-string v4, ": "

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/app/BackStackRecord;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-virtual {v3, v0, p2, p3, p4}, Landroid/app/BackStackRecord;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_4
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v2

    if-lez v1, :cond_5

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "Back Stack Indices:"

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const/4 v2, 0x0

    :goto_4
    if-ge v2, v1, :cond_5

    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/BackStackRecord;

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v4, "  #"

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    const-string v4, ": "

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_5
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAvailBackStackIndices:Ljava/util/ArrayList;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_6

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "mAvailBackStackIndices: "

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAvailBackStackIndices:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_6
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mPendingActions:Ljava/util/ArrayList;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_7

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "Pending Actions:"

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const/4 v2, 0x0

    :goto_5
    if-ge v2, v1, :cond_7

    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mPendingActions:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/FragmentManagerImpl$OpGenerator;

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v4, "  #"

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    const-string v4, ": "

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_7
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "FragmentManager misc state:"

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "  mHost="

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "  mContainer="

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mContainer:Landroid/app/FragmentContainer;

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    if-eqz v2, :cond_8

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "  mParent="

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_8
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "  mCurState="

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v2, p0, Landroid/app/FragmentManagerImpl;->mCurState:I

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    const-string v2, " mStateSaved="

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v2, p0, Landroid/app/FragmentManagerImpl;->mStateSaved:Z

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Z)V

    const-string v2, " mDestroyed="

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v2, p0, Landroid/app/FragmentManagerImpl;->mDestroyed:Z

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Z)V

    iget-boolean v2, p0, Landroid/app/FragmentManagerImpl;->mNeedMenuInvalidate:Z

    if-eqz v2, :cond_9

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "  mNeedMenuInvalidate="

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v2, p0, Landroid/app/FragmentManagerImpl;->mNeedMenuInvalidate:Z

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Z)V

    :cond_9
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mNoTransactionsBecause:Ljava/lang/String;

    if-eqz v2, :cond_a

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v2, "  mNoTransactionsBecause="

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mNoTransactionsBecause:Ljava/lang/String;

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_a
    return-void

    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public enqueueAction(Landroid/app/FragmentManagerImpl$OpGenerator;Z)V
    .locals 2

    if-nez p2, :cond_0

    invoke-direct {p0}, Landroid/app/FragmentManagerImpl;->checkStateLoss()V

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mDestroyed:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mPendingActions:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mPendingActions:Ljava/util/ArrayList;

    :cond_2
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mPendingActions:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Landroid/app/FragmentManagerImpl;->scheduleCommit()V

    monitor-exit p0

    return-void

    :cond_3
    :goto_0
    if-eqz p2, :cond_4

    monitor-exit p0

    return-void

    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Activity has been destroyed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method ensureInflatedFragmentView(Landroid/app/Fragment;)V
    .locals 3

    goto/32 :goto_1

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_13

    :cond_0
    goto/32 :goto_9

    nop

    :goto_1
    iget-boolean v0, p1, Landroid/app/Fragment;->mFromLayout:Z

    goto/32 :goto_1c

    nop

    :goto_2
    iget-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_8

    nop

    :goto_3
    invoke-virtual {p1, v0, v1, v2}, Landroid/app/Fragment;->performCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    goto/32 :goto_18

    nop

    :goto_4
    iget-boolean v0, p1, Landroid/app/Fragment;->mHidden:Z

    goto/32 :goto_0

    nop

    :goto_5
    iget-object v2, p1, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_3

    nop

    :goto_6
    iget-boolean v0, p1, Landroid/app/Fragment;->mPerformedCreateView:Z

    goto/32 :goto_e

    nop

    :goto_7
    return-void

    :goto_8
    if-nez v0, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_15

    nop

    :goto_9
    iget-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_b

    nop

    :goto_a
    iget-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_f

    nop

    :goto_b
    const/16 v2, 0x8

    goto/32 :goto_12

    nop

    :goto_c
    invoke-virtual {p0, p1, v0, v2, v1}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentViewCreated(Landroid/app/Fragment;Landroid/view/View;Landroid/os/Bundle;Z)V

    :goto_d
    goto/32 :goto_7

    nop

    :goto_e
    if-eqz v0, :cond_2

    goto/32 :goto_d

    :cond_2
    goto/32 :goto_1b

    nop

    :goto_f
    iget-object v2, p1, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_c

    nop

    :goto_10
    iget-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_16

    nop

    :goto_11
    const/4 v1, 0x0

    goto/32 :goto_5

    nop

    :goto_12
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_13
    goto/32 :goto_10

    nop

    :goto_14
    invoke-virtual {v0, v1}, Landroid/view/View;->setSaveFromParentEnabled(Z)V

    goto/32 :goto_4

    nop

    :goto_15
    iget-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_17

    nop

    :goto_16
    iget-object v2, p1, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_19

    nop

    :goto_17
    const/4 v1, 0x0

    goto/32 :goto_14

    nop

    :goto_18
    iput-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_2

    nop

    :goto_19
    invoke-virtual {p1, v0, v2}, Landroid/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    goto/32 :goto_a

    nop

    :goto_1a
    invoke-virtual {p1, v0}, Landroid/app/Fragment;->performGetLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    goto/32 :goto_11

    nop

    :goto_1b
    iget-object v0, p1, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_1a

    nop

    :goto_1c
    if-nez v0, :cond_3

    goto/32 :goto_d

    :cond_3
    goto/32 :goto_6

    nop
.end method

.method public execPendingActions()Z
    .locals 4

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/app/FragmentManagerImpl;->ensureExecReady(Z)V

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mTmpRecords:Ljava/util/ArrayList;

    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mTmpIsPop:Ljava/util/ArrayList;

    invoke-direct {p0, v2, v3}, Landroid/app/FragmentManagerImpl;->generateOpsForPendingActions(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v2

    if-eqz v2, :cond_0

    iput-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mExecutingActions:Z

    :try_start_0
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mTmpRecords:Ljava/util/ArrayList;

    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mTmpIsPop:Ljava/util/ArrayList;

    invoke-direct {p0, v2, v3}, Landroid/app/FragmentManagerImpl;->removeRedundantOperationsAndExecute(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Landroid/app/FragmentManagerImpl;->cleanupExec()V

    nop

    const/4 v1, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Landroid/app/FragmentManagerImpl;->cleanupExec()V

    throw v0

    :cond_0
    invoke-virtual {p0}, Landroid/app/FragmentManagerImpl;->doPendingDeferredStart()V

    invoke-direct {p0}, Landroid/app/FragmentManagerImpl;->burpActive()V

    return v1
.end method

.method public execSingleAction(Landroid/app/FragmentManagerImpl$OpGenerator;Z)V
    .locals 2

    if-eqz p2, :cond_1

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mDestroyed:Z

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-direct {p0, p2}, Landroid/app/FragmentManagerImpl;->ensureExecReady(Z)V

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mTmpRecords:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mTmpIsPop:Ljava/util/ArrayList;

    invoke-interface {p1, v0, v1}, Landroid/app/FragmentManagerImpl$OpGenerator;->generateOps(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mExecutingActions:Z

    :try_start_0
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mTmpRecords:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mTmpIsPop:Ljava/util/ArrayList;

    invoke-direct {p0, v0, v1}, Landroid/app/FragmentManagerImpl;->removeRedundantOperationsAndExecute(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Landroid/app/FragmentManagerImpl;->cleanupExec()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Landroid/app/FragmentManagerImpl;->cleanupExec()V

    throw v0

    :cond_2
    :goto_0
    invoke-virtual {p0}, Landroid/app/FragmentManagerImpl;->doPendingDeferredStart()V

    invoke-direct {p0}, Landroid/app/FragmentManagerImpl;->burpActive()V

    return-void
.end method

.method public executePendingTransactions()Z
    .locals 1

    invoke-virtual {p0}, Landroid/app/FragmentManagerImpl;->execPendingActions()Z

    move-result v0

    invoke-direct {p0}, Landroid/app/FragmentManagerImpl;->forcePostponedTransactions()V

    return v0
.end method

.method public findFragmentById(I)Landroid/app/Fragment;
    .locals 3

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Fragment;

    if-eqz v1, :cond_0

    iget v2, v1, Landroid/app/Fragment;->mFragmentId:I

    if-ne v2, p1, :cond_0

    return-object v1

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_3

    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Fragment;

    if-eqz v1, :cond_2

    iget v2, v1, Landroid/app/Fragment;->mFragmentId:I

    if-ne v2, p1, :cond_2

    return-object v1

    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    return-object v0
.end method

.method public findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;
    .locals 3

    if-eqz p1, :cond_1

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Fragment;

    if-eqz v1, :cond_0

    iget-object v2, v1, Landroid/app/Fragment;->mTag:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    if-eqz v0, :cond_3

    if-eqz p1, :cond_3

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_3

    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Fragment;

    if-eqz v1, :cond_2

    iget-object v2, v1, Landroid/app/Fragment;->mTag:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    return-object v1

    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    return-object v0
.end method

.method public findFragmentByWho(Ljava/lang/String;)Landroid/app/Fragment;
    .locals 3

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Fragment;

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Landroid/app/Fragment;->findFragmentByWho(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    move-object v1, v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public freeBackStackIndex(I)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAvailBackStackIndices:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mAvailBackStackIndices:Ljava/util/ArrayList;

    :cond_0
    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    if-eqz v0, :cond_1

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Freeing back stack index "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAvailBackStackIndices:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getBackStackEntryAt(I)Landroid/app/FragmentManager$BackStackEntry;
    .locals 1

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/FragmentManager$BackStackEntry;

    return-object v0
.end method

.method public getBackStackEntryCount()I
    .locals 1

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/app/Fragment;
    .locals 5

    const/4 v0, -0x1

    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Fragment;

    if-nez v0, :cond_1

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Fragment no longer exists for key "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": index "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v2}, Landroid/app/FragmentManagerImpl;->throwException(Ljava/lang/RuntimeException;)V

    :cond_1
    return-object v0
.end method

.method public getFragments()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/app/Fragment;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method getLayoutInflaterFactory()Landroid/view/LayoutInflater$Factory2;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    return-object p0
.end method

.method public getPrimaryNavigationFragment()Landroid/app/Fragment;
    .locals 1

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mPrimaryNav:Landroid/app/Fragment;

    return-object v0
.end method

.method getTargetSdk()I
    .locals 3

    goto/32 :goto_3

    nop

    :goto_0
    if-nez v1, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_8

    nop

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_5

    nop

    :goto_2
    if-nez v0, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_6

    nop

    :goto_3
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    goto/32 :goto_2

    nop

    :goto_4
    if-nez v0, :cond_2

    goto/32 :goto_a

    :cond_2
    goto/32 :goto_7

    nop

    :goto_5
    return v0

    :goto_6
    invoke-virtual {v0}, Landroid/app/FragmentHostCallback;->getContext()Landroid/content/Context;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_7
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_8
    iget v2, v1, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    goto/32 :goto_9

    nop

    :goto_9
    return v2

    :goto_a
    goto/32 :goto_1

    nop
.end method

.method public hideFragment(Landroid/app/Fragment;)V
    .locals 2

    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "hide: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FragmentManager"

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p1, Landroid/app/Fragment;->mHidden:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p1, Landroid/app/Fragment;->mHidden:Z

    iget-boolean v1, p1, Landroid/app/Fragment;->mHiddenChanged:Z

    xor-int/2addr v0, v1

    iput-boolean v0, p1, Landroid/app/Fragment;->mHiddenChanged:Z

    :cond_1
    return-void
.end method

.method public invalidateOptionsMenu()V
    .locals 3

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    if-eqz v0, :cond_0

    iget v1, p0, Landroid/app/FragmentManagerImpl;->mCurState:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/app/FragmentHostCallback;->onInvalidateOptionsMenu()V

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mNeedMenuInvalidate:Z

    :goto_0
    return-void
.end method

.method public isDestroyed()Z
    .locals 1

    iget-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mDestroyed:Z

    return v0
.end method

.method isStateAtLeast(I)Z
    .locals 1

    goto/32 :goto_7

    nop

    :goto_0
    if-ge v0, p1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_6

    nop

    :goto_1
    goto :goto_5

    :goto_2
    goto/32 :goto_4

    nop

    :goto_3
    return v0

    :goto_4
    const/4 v0, 0x0

    :goto_5
    goto/32 :goto_3

    nop

    :goto_6
    const/4 v0, 0x1

    goto/32 :goto_1

    nop

    :goto_7
    iget v0, p0, Landroid/app/FragmentManagerImpl;->mCurState:I

    goto/32 :goto_0

    nop
.end method

.method public isStateSaved()Z
    .locals 1

    iget-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mStateSaved:Z

    return v0
.end method

.method loadAnimator(Landroid/app/Fragment;IZI)Landroid/animation/Animator;
    .locals 5

    goto/32 :goto_d

    nop

    :goto_0
    invoke-static {p2, p3}, Landroid/app/FragmentManagerImpl;->transitToStyleIndex(IZ)I

    move-result v2

    goto/32 :goto_1

    nop

    :goto_1
    if-ltz v2, :cond_0

    goto/32 :goto_1c

    :cond_0
    goto/32 :goto_1b

    nop

    :goto_2
    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    goto/32 :goto_15

    nop

    :goto_3
    const/4 v4, 0x0

    goto/32 :goto_5

    nop

    :goto_4
    invoke-virtual {v3, p4, v4}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v3

    goto/32 :goto_3

    nop

    :goto_5
    invoke-virtual {v3, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    goto/32 :goto_24

    nop

    :goto_6
    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    goto/32 :goto_2a

    nop

    :goto_7
    if-eqz p4, :cond_1

    goto/32 :goto_16

    :cond_1
    goto/32 :goto_14

    nop

    :goto_8
    invoke-virtual {p1, p2, p3, v0}, Landroid/app/Fragment;->onCreateAnimator(IZI)Landroid/animation/Animator;

    move-result-object v0

    goto/32 :goto_1f

    nop

    :goto_9
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    goto/32 :goto_29

    nop

    :goto_a
    invoke-virtual {p1}, Landroid/app/Fragment;->getNextAnim()I

    move-result v2

    goto/32 :goto_1e

    nop

    :goto_b
    return-object v0

    :goto_c
    goto/32 :goto_1a

    nop

    :goto_d
    invoke-virtual {p1}, Landroid/app/Fragment;->getNextAnim()I

    move-result v0

    goto/32 :goto_8

    nop

    :goto_e
    invoke-virtual {v1}, Landroid/app/FragmentHostCallback;->getContext()Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_20

    nop

    :goto_f
    sget-object v4, Lcom/android/internal/R$styleable;->FragmentAnimation:[I

    goto/32 :goto_4

    nop

    :goto_10
    if-eqz p4, :cond_2

    goto/32 :goto_22

    :cond_2
    goto/32 :goto_21

    nop

    :goto_11
    return-object v1

    :goto_12
    goto/32 :goto_26

    nop

    :goto_13
    if-nez v1, :cond_3

    goto/32 :goto_28

    :cond_3
    goto/32 :goto_27

    nop

    :goto_14
    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    goto/32 :goto_23

    nop

    :goto_15
    invoke-virtual {v3}, Landroid/app/FragmentHostCallback;->onGetWindowAnimations()I

    move-result p4

    :goto_16
    goto/32 :goto_10

    nop

    :goto_17
    const/4 v1, 0x0

    goto/32 :goto_2c

    nop

    :goto_18
    return-object v1

    :goto_19
    goto/32 :goto_0

    nop

    :goto_1a
    invoke-virtual {p1}, Landroid/app/Fragment;->getNextAnim()I

    move-result v1

    goto/32 :goto_1d

    nop

    :goto_1b
    return-object v1

    :goto_1c
    goto/32 :goto_7

    nop

    :goto_1d
    if-nez v1, :cond_4

    goto/32 :goto_28

    :cond_4
    goto/32 :goto_9

    nop

    :goto_1e
    invoke-static {v1, v2}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v1

    goto/32 :goto_13

    nop

    :goto_1f
    if-nez v0, :cond_5

    goto/32 :goto_c

    :cond_5
    goto/32 :goto_b

    nop

    :goto_20
    invoke-static {v1, v4}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v1

    goto/32 :goto_2b

    nop

    :goto_21
    return-object v1

    :goto_22
    goto/32 :goto_6

    nop

    :goto_23
    invoke-virtual {v3}, Landroid/app/FragmentHostCallback;->onHasWindowAnimations()Z

    move-result v3

    goto/32 :goto_25

    nop

    :goto_24
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    goto/32 :goto_2d

    nop

    :goto_25
    if-nez v3, :cond_6

    goto/32 :goto_16

    :cond_6
    goto/32 :goto_2

    nop

    :goto_26
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    goto/32 :goto_e

    nop

    :goto_27
    return-object v1

    :goto_28
    goto/32 :goto_17

    nop

    :goto_29
    invoke-virtual {v1}, Landroid/app/FragmentHostCallback;->getContext()Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_a

    nop

    :goto_2a
    invoke-virtual {v3}, Landroid/app/FragmentHostCallback;->getContext()Landroid/content/Context;

    move-result-object v3

    goto/32 :goto_f

    nop

    :goto_2b
    return-object v1

    :goto_2c
    if-eqz p2, :cond_7

    goto/32 :goto_19

    :cond_7
    goto/32 :goto_18

    nop

    :goto_2d
    if-eqz v4, :cond_8

    goto/32 :goto_12

    :cond_8
    goto/32 :goto_11

    nop
.end method

.method makeActive(Landroid/app/Fragment;)V
    .locals 2

    goto/32 :goto_c

    nop

    :goto_0
    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    goto/32 :goto_5

    nop

    :goto_1
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    goto/32 :goto_1c

    nop

    :goto_2
    const-string v1, "FragmentManager"

    goto/32 :goto_18

    nop

    :goto_3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_6

    nop

    :goto_4
    iget v1, p1, Landroid/app/Fragment;->mIndex:I

    goto/32 :goto_f

    nop

    :goto_5
    if-nez v0, :cond_0

    goto/32 :goto_19

    :cond_0
    goto/32 :goto_b

    nop

    :goto_6
    const-string v1, "Allocated fragment index "

    goto/32 :goto_11

    nop

    :goto_7
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    goto/32 :goto_8

    nop

    :goto_8
    invoke-virtual {p1, v0, v1}, Landroid/app/Fragment;->setIndex(ILandroid/app/Fragment;)V

    goto/32 :goto_1a

    nop

    :goto_9
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_15

    nop

    :goto_a
    iget v0, p0, Landroid/app/FragmentManagerImpl;->mNextFragmentIndex:I

    goto/32 :goto_17

    nop

    :goto_b
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_3

    nop

    :goto_c
    iget v0, p1, Landroid/app/Fragment;->mIndex:I

    goto/32 :goto_10

    nop

    :goto_d
    if-eqz v0, :cond_1

    goto/32 :goto_1d

    :cond_1
    goto/32 :goto_1b

    nop

    :goto_e
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    goto/32 :goto_4

    nop

    :goto_f
    invoke-virtual {v0, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto/32 :goto_0

    nop

    :goto_10
    if-gez v0, :cond_2

    goto/32 :goto_13

    :cond_2
    goto/32 :goto_12

    nop

    :goto_11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_12
    return-void

    :goto_13
    goto/32 :goto_a

    nop

    :goto_14
    iput v1, p0, Landroid/app/FragmentManagerImpl;->mNextFragmentIndex:I

    goto/32 :goto_7

    nop

    :goto_15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_16
    return-void

    :goto_17
    add-int/lit8 v1, v0, 0x1

    goto/32 :goto_14

    nop

    :goto_18
    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_19
    goto/32 :goto_16

    nop

    :goto_1a
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    goto/32 :goto_d

    nop

    :goto_1b
    new-instance v0, Landroid/util/SparseArray;

    goto/32 :goto_1

    nop

    :goto_1c
    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    :goto_1d
    goto/32 :goto_e

    nop
.end method

.method makeInactive(Landroid/app/Fragment;)V
    .locals 3

    goto/32 :goto_a

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_f

    :cond_0
    goto/32 :goto_10

    nop

    :goto_1
    return-void

    :goto_2
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    goto/32 :goto_14

    nop

    :goto_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_12

    nop

    :goto_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_b

    nop

    :goto_5
    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    goto/32 :goto_0

    nop

    :goto_6
    const-string v1, "Freeing fragment index "

    goto/32 :goto_3

    nop

    :goto_7
    const/4 v2, 0x0

    goto/32 :goto_13

    nop

    :goto_8
    if-ltz v0, :cond_1

    goto/32 :goto_16

    :cond_1
    goto/32 :goto_15

    nop

    :goto_9
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_6

    nop

    :goto_a
    iget v0, p1, Landroid/app/Fragment;->mIndex:I

    goto/32 :goto_8

    nop

    :goto_b
    const-string v1, "FragmentManager"

    goto/32 :goto_e

    nop

    :goto_c
    invoke-virtual {v0, v1}, Landroid/app/FragmentHostCallback;->inactivateFragment(Ljava/lang/String;)V

    goto/32 :goto_11

    nop

    :goto_d
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    goto/32 :goto_17

    nop

    :goto_e
    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_f
    goto/32 :goto_2

    nop

    :goto_10
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_9

    nop

    :goto_11
    invoke-virtual {p1}, Landroid/app/Fragment;->initState()V

    goto/32 :goto_1

    nop

    :goto_12
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_13
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto/32 :goto_d

    nop

    :goto_14
    iget v1, p1, Landroid/app/Fragment;->mIndex:I

    goto/32 :goto_7

    nop

    :goto_15
    return-void

    :goto_16
    goto/32 :goto_5

    nop

    :goto_17
    iget-object v1, p1, Landroid/app/Fragment;->mWho:Ljava/lang/String;

    goto/32 :goto_c

    nop
.end method

.method moveFragmentToExpectedState(Landroid/app/Fragment;)V
    .locals 10

    goto/32 :goto_3

    nop

    :goto_0
    iget v0, p0, Landroid/app/FragmentManagerImpl;->mCurState:I

    goto/32 :goto_3a

    nop

    :goto_1
    iget-object v4, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_2e

    nop

    :goto_2
    invoke-virtual {p1}, Landroid/app/Fragment;->getNextTransition()I

    move-result v3

    goto/32 :goto_2a

    nop

    :goto_3
    if-eqz p1, :cond_0

    goto/32 :goto_24

    :cond_0
    goto/32 :goto_23

    nop

    :goto_4
    iget-object v4, v1, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_16

    nop

    :goto_5
    invoke-direct {p0, v3, v2}, Landroid/app/FragmentManagerImpl;->setHWLayerAnimListenerIfAlpha(Landroid/view/View;Landroid/animation/Animator;)V

    goto/32 :goto_20

    nop

    :goto_6
    iput-boolean v3, p1, Landroid/app/Fragment;->mIsNewlyAdded:Z

    goto/32 :goto_2

    nop

    :goto_7
    if-nez v1, :cond_1

    goto/32 :goto_1d

    :cond_1
    goto/32 :goto_2f

    nop

    :goto_8
    return-void

    :goto_9
    if-nez v1, :cond_2

    goto/32 :goto_12

    :cond_2
    goto/32 :goto_4

    nop

    :goto_a
    iget-object v1, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_1a

    nop

    :goto_b
    iget-object v3, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_1b

    nop

    :goto_c
    const/4 v9, 0x0

    goto/32 :goto_31

    nop

    :goto_d
    if-nez v4, :cond_3

    goto/32 :goto_21

    :cond_3
    goto/32 :goto_39

    nop

    :goto_e
    if-nez v1, :cond_4

    goto/32 :goto_38

    :cond_4
    goto/32 :goto_14

    nop

    :goto_f
    if-nez v2, :cond_5

    goto/32 :goto_21

    :cond_5
    goto/32 :goto_b

    nop

    :goto_10
    invoke-virtual/range {v4 .. v9}, Landroid/app/FragmentManagerImpl;->moveToState(Landroid/app/Fragment;IIIZ)V

    goto/32 :goto_a

    nop

    :goto_11
    invoke-virtual {v5, v8, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    :goto_12
    goto/32 :goto_27

    nop

    :goto_13
    invoke-virtual {v5, v7}, Landroid/view/ViewGroup;->removeViewAt(I)V

    goto/32 :goto_15

    nop

    :goto_14
    invoke-virtual {p1}, Landroid/app/Fragment;->isInBackStack()Z

    move-result v1

    goto/32 :goto_7

    nop

    :goto_15
    iget-object v8, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_11

    nop

    :goto_16
    iget-object v5, p1, Landroid/app/Fragment;->mContainer:Landroid/view/ViewGroup;

    goto/32 :goto_18

    nop

    :goto_17
    iget-object v3, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_5

    nop

    :goto_18
    invoke-virtual {v5, v4}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v6

    goto/32 :goto_26

    nop

    :goto_19
    iget-boolean v1, p1, Landroid/app/Fragment;->mHiddenChanged:Z

    goto/32 :goto_34

    nop

    :goto_1a
    if-nez v1, :cond_6

    goto/32 :goto_21

    :cond_6
    goto/32 :goto_22

    nop

    :goto_1b
    invoke-virtual {v2, v3}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    goto/32 :goto_17

    nop

    :goto_1c
    goto/16 :goto_38

    :goto_1d
    goto/32 :goto_37

    nop

    :goto_1e
    invoke-virtual {p0, p1}, Landroid/app/FragmentManagerImpl;->completeShowHideFragment(Landroid/app/Fragment;)V

    :goto_1f
    goto/32 :goto_8

    nop

    :goto_20
    invoke-virtual {v2}, Landroid/animation/Animator;->start()V

    :goto_21
    goto/32 :goto_19

    nop

    :goto_22
    invoke-direct {p0, p1}, Landroid/app/FragmentManagerImpl;->findFragmentUnder(Landroid/app/Fragment;)Landroid/app/Fragment;

    move-result-object v1

    goto/32 :goto_9

    nop

    :goto_23
    return-void

    :goto_24
    goto/32 :goto_0

    nop

    :goto_25
    const/4 v2, 0x1

    goto/32 :goto_28

    nop

    :goto_26
    iget-object v7, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_32

    nop

    :goto_27
    iget-boolean v4, p1, Landroid/app/Fragment;->mIsNewlyAdded:Z

    goto/32 :goto_d

    nop

    :goto_28
    const/4 v3, 0x0

    goto/32 :goto_e

    nop

    :goto_29
    invoke-virtual {v4, v5}, Landroid/view/View;->setTransitionAlpha(F)V

    goto/32 :goto_6

    nop

    :goto_2a
    invoke-virtual {p1}, Landroid/app/Fragment;->getNextTransitionStyle()I

    move-result v4

    goto/32 :goto_35

    nop

    :goto_2b
    if-lt v7, v6, :cond_7

    goto/32 :goto_12

    :cond_7
    goto/32 :goto_13

    nop

    :goto_2c
    move v6, v0

    goto/32 :goto_10

    nop

    :goto_2d
    invoke-virtual {p1}, Landroid/app/Fragment;->getNextTransition()I

    move-result v7

    goto/32 :goto_33

    nop

    :goto_2e
    const/high16 v5, 0x3f800000    # 1.0f

    goto/32 :goto_29

    nop

    :goto_2f
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto/32 :goto_1c

    nop

    :goto_30
    if-nez v4, :cond_8

    goto/32 :goto_21

    :cond_8
    goto/32 :goto_1

    nop

    :goto_31
    move-object v4, p0

    goto/32 :goto_36

    nop

    :goto_32
    invoke-virtual {v5, v7}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v7

    goto/32 :goto_2b

    nop

    :goto_33
    invoke-virtual {p1}, Landroid/app/Fragment;->getNextTransitionStyle()I

    move-result v8

    goto/32 :goto_c

    nop

    :goto_34
    if-nez v1, :cond_9

    goto/32 :goto_1f

    :cond_9
    goto/32 :goto_1e

    nop

    :goto_35
    invoke-virtual {p0, p1, v3, v2, v4}, Landroid/app/FragmentManagerImpl;->loadAnimator(Landroid/app/Fragment;IZI)Landroid/animation/Animator;

    move-result-object v2

    goto/32 :goto_f

    nop

    :goto_36
    move-object v5, p1

    goto/32 :goto_2c

    nop

    :goto_37
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_38
    goto/32 :goto_2d

    nop

    :goto_39
    iget-object v4, p1, Landroid/app/Fragment;->mContainer:Landroid/view/ViewGroup;

    goto/32 :goto_30

    nop

    :goto_3a
    iget-boolean v1, p1, Landroid/app/Fragment;->mRemoving:Z

    goto/32 :goto_25

    nop
.end method

.method moveToState(IZ)V
    .locals 6

    goto/32 :goto_15

    nop

    :goto_0
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_4b

    nop

    :goto_1
    invoke-virtual {p0, v4}, Landroid/app/FragmentManagerImpl;->moveFragmentToExpectedState(Landroid/app/Fragment;)V

    goto/32 :goto_4d

    nop

    :goto_2
    iget-boolean v3, p0, Landroid/app/FragmentManagerImpl;->mNeedMenuInvalidate:Z

    goto/32 :goto_49

    nop

    :goto_3
    iget v0, p0, Landroid/app/FragmentManagerImpl;->mCurState:I

    goto/32 :goto_48

    nop

    :goto_4
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_2e

    nop

    :goto_5
    if-eqz v0, :cond_0

    goto/32 :goto_2f

    :cond_0
    goto/32 :goto_2d

    nop

    :goto_6
    invoke-virtual {p0}, Landroid/app/FragmentManagerImpl;->startPendingDeferredFragments()V

    :goto_7
    goto/32 :goto_2

    nop

    :goto_8
    invoke-virtual {v4}, Landroid/app/LoaderManagerImpl;->hasRunningLoaders()Z

    move-result v4

    goto/32 :goto_d

    nop

    :goto_9
    iget-object v4, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    goto/32 :goto_4a

    nop

    :goto_a
    if-nez v5, :cond_1

    goto/32 :goto_2b

    :cond_1
    :goto_b
    goto/32 :goto_22

    nop

    :goto_c
    const-string v1, "No activity"

    goto/32 :goto_4

    nop

    :goto_d
    or-int/2addr v0, v4

    :goto_e
    goto/32 :goto_0

    nop

    :goto_f
    if-lt v2, v1, :cond_2

    goto/32 :goto_4c

    :cond_2
    goto/32 :goto_36

    nop

    :goto_10
    goto/16 :goto_2f

    :goto_11
    goto/32 :goto_38

    nop

    :goto_12
    if-eqz v0, :cond_3

    goto/32 :goto_7

    :cond_3
    goto/32 :goto_6

    nop

    :goto_13
    iput p1, p0, Landroid/app/FragmentManagerImpl;->mCurState:I

    goto/32 :goto_27

    nop

    :goto_14
    invoke-virtual {v5}, Landroid/app/LoaderManagerImpl;->hasRunningLoaders()Z

    move-result v5

    goto/32 :goto_2a

    nop

    :goto_15
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    goto/32 :goto_5

    nop

    :goto_16
    if-nez v3, :cond_4

    goto/32 :goto_20

    :cond_4
    goto/32 :goto_1e

    nop

    :goto_17
    const/4 v5, 0x5

    goto/32 :goto_24

    nop

    :goto_18
    if-nez v4, :cond_5

    goto/32 :goto_2b

    :cond_5
    goto/32 :goto_3d

    nop

    :goto_19
    goto/16 :goto_45

    :goto_1a
    goto/32 :goto_12

    nop

    :goto_1b
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    goto/32 :goto_44

    nop

    :goto_1c
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    goto/32 :goto_1d

    nop

    :goto_1d
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_28

    nop

    :goto_1e
    iget v4, p0, Landroid/app/FragmentManagerImpl;->mCurState:I

    goto/32 :goto_17

    nop

    :goto_1f
    iput-boolean v3, p0, Landroid/app/FragmentManagerImpl;->mNeedMenuInvalidate:Z

    :goto_20
    goto/32 :goto_3f

    nop

    :goto_21
    if-eqz v5, :cond_6

    goto/32 :goto_2b

    :cond_6
    goto/32 :goto_1

    nop

    :goto_22
    iget-boolean v5, v4, Landroid/app/Fragment;->mIsNewlyAdded:Z

    goto/32 :goto_21

    nop

    :goto_23
    if-eqz p2, :cond_7

    goto/32 :goto_41

    :cond_7
    goto/32 :goto_3

    nop

    :goto_24
    if-eq v4, v5, :cond_8

    goto/32 :goto_20

    :cond_8
    goto/32 :goto_3e

    nop

    :goto_25
    const/4 v3, 0x0

    goto/32 :goto_1f

    nop

    :goto_26
    iget-object v4, v3, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    goto/32 :goto_42

    nop

    :goto_27
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    goto/32 :goto_46

    nop

    :goto_28
    const/4 v2, 0x0

    :goto_29
    goto/32 :goto_f

    nop

    :goto_2a
    or-int/2addr v0, v5

    :goto_2b
    goto/32 :goto_35

    nop

    :goto_2c
    const/4 v0, 0x0

    goto/32 :goto_1c

    nop

    :goto_2d
    if-eqz p1, :cond_9

    goto/32 :goto_11

    :cond_9
    goto/32 :goto_10

    nop

    :goto_2e
    throw v0

    :goto_2f
    goto/32 :goto_23

    nop

    :goto_30
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    goto/32 :goto_1b

    nop

    :goto_31
    check-cast v3, Landroid/app/Fragment;

    goto/32 :goto_34

    nop

    :goto_32
    iget-object v4, v3, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    goto/32 :goto_8

    nop

    :goto_33
    iget-object v5, v4, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    goto/32 :goto_14

    nop

    :goto_34
    invoke-virtual {p0, v3}, Landroid/app/FragmentManagerImpl;->moveFragmentToExpectedState(Landroid/app/Fragment;)V

    goto/32 :goto_26

    nop

    :goto_35
    add-int/lit8 v3, v3, 0x1

    goto/32 :goto_19

    nop

    :goto_36
    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    goto/32 :goto_3a

    nop

    :goto_37
    check-cast v4, Landroid/app/Fragment;

    goto/32 :goto_18

    nop

    :goto_38
    new-instance v0, Ljava/lang/IllegalStateException;

    goto/32 :goto_c

    nop

    :goto_39
    iget-boolean v5, v4, Landroid/app/Fragment;->mDetached:Z

    goto/32 :goto_a

    nop

    :goto_3a
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_31

    nop

    :goto_3b
    if-eqz v5, :cond_a

    goto/32 :goto_b

    :cond_a
    goto/32 :goto_39

    nop

    :goto_3c
    if-lt v3, v2, :cond_b

    goto/32 :goto_1a

    :cond_b
    goto/32 :goto_9

    nop

    :goto_3d
    iget-boolean v5, v4, Landroid/app/Fragment;->mRemoving:Z

    goto/32 :goto_3b

    nop

    :goto_3e
    invoke-virtual {v3}, Landroid/app/FragmentHostCallback;->onInvalidateOptionsMenu()V

    goto/32 :goto_25

    nop

    :goto_3f
    return-void

    :goto_40
    return-void

    :goto_41
    goto/32 :goto_13

    nop

    :goto_42
    if-nez v4, :cond_c

    goto/32 :goto_e

    :cond_c
    goto/32 :goto_32

    nop

    :goto_43
    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    goto/32 :goto_16

    nop

    :goto_44
    const/4 v3, 0x0

    :goto_45
    goto/32 :goto_3c

    nop

    :goto_46
    if-nez v0, :cond_d

    goto/32 :goto_20

    :cond_d
    goto/32 :goto_2c

    nop

    :goto_47
    if-nez v5, :cond_e

    goto/32 :goto_2b

    :cond_e
    goto/32 :goto_33

    nop

    :goto_48
    if-eq v0, p1, :cond_f

    goto/32 :goto_41

    :cond_f
    goto/32 :goto_40

    nop

    :goto_49
    if-nez v3, :cond_10

    goto/32 :goto_20

    :cond_10
    goto/32 :goto_43

    nop

    :goto_4a
    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_37

    nop

    :goto_4b
    goto/16 :goto_29

    :goto_4c
    goto/32 :goto_30

    nop

    :goto_4d
    iget-object v5, v4, Landroid/app/Fragment;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    goto/32 :goto_47

    nop
.end method

.method moveToState(Landroid/app/Fragment;)V
    .locals 6

    goto/32 :goto_6

    nop

    :goto_0
    invoke-virtual/range {v0 .. v5}, Landroid/app/FragmentManagerImpl;->moveToState(Landroid/app/Fragment;IIIZ)V

    goto/32 :goto_5

    nop

    :goto_1
    const/4 v5, 0x0

    goto/32 :goto_3

    nop

    :goto_2
    const/4 v4, 0x0

    goto/32 :goto_1

    nop

    :goto_3
    move-object v0, p0

    goto/32 :goto_4

    nop

    :goto_4
    move-object v1, p1

    goto/32 :goto_0

    nop

    :goto_5
    return-void

    :goto_6
    iget v2, p0, Landroid/app/FragmentManagerImpl;->mCurState:I

    goto/32 :goto_7

    nop

    :goto_7
    const/4 v3, 0x0

    goto/32 :goto_2

    nop
.end method

.method moveToState(Landroid/app/Fragment;IIIZ)V
    .locals 19

    goto/32 :goto_6c

    nop

    :goto_0
    iput-object v14, v8, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_12

    nop

    :goto_1
    const-string v4, " for a container view with no id"

    goto/32 :goto_152

    nop

    :goto_2
    iget-object v1, v7, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    goto/32 :goto_17b

    nop

    :goto_3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_4d

    nop

    :goto_4
    if-nez v1, :cond_0

    goto/32 :goto_18e

    :cond_0
    goto/32 :goto_4c

    nop

    :goto_5
    iget-object v3, v8, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_1eb

    nop

    :goto_6
    invoke-static {v13, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_7
    goto/32 :goto_72

    nop

    :goto_8
    cmpl-float v2, v2, v3

    goto/32 :goto_fb

    nop

    :goto_9
    const/4 v0, 0x3

    :goto_a
    goto/32 :goto_9b

    nop

    :goto_b
    const-string/jumbo v2, "moveto ACTIVITY_CREATED: "

    goto/32 :goto_19c

    nop

    :goto_c
    goto/16 :goto_85

    :goto_d
    goto/32 :goto_231

    nop

    :goto_e
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_60

    nop

    :goto_f
    if-eqz v2, :cond_1

    goto/32 :goto_1aa

    :cond_1
    goto/32 :goto_49

    nop

    :goto_10
    const-string/jumbo v2, "movefrom STARTED: "

    goto/32 :goto_1d2

    nop

    :goto_11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_5e

    nop

    :goto_12
    iput-object v14, v8, Landroid/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    :goto_13
    goto/32 :goto_195

    nop

    :goto_14
    sget-boolean v1, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    goto/32 :goto_fa

    nop

    :goto_15
    iget v2, v8, Landroid/app/Fragment;->mContainerId:I

    goto/32 :goto_126

    nop

    :goto_16
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1e9

    nop

    :goto_17
    if-ge v1, v2, :cond_2

    goto/32 :goto_173

    :cond_2
    goto/32 :goto_171

    nop

    :goto_18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_107

    nop

    :goto_19
    move-object/from16 v1, p0

    goto/32 :goto_67

    nop

    :goto_1a
    iget-object v2, v8, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_136

    nop

    :goto_1b
    if-gt v0, v11, :cond_3

    goto/32 :goto_c6

    :cond_3
    goto/32 :goto_c5

    nop

    :goto_1c
    if-gt v0, v1, :cond_4

    goto/32 :goto_8a

    :cond_4
    goto/32 :goto_207

    nop

    :goto_1d
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_42

    nop

    :goto_1e
    move-object v10, v6

    goto/32 :goto_11b

    nop

    :goto_1f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_b6

    nop

    :goto_20
    if-eqz v0, :cond_5

    goto/32 :goto_13b

    :cond_5
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v3, v8, Landroid/app/Fragment;->mContainerId:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_234

    nop

    :goto_21
    const-string v5, ") for fragment "

    goto/32 :goto_a8

    nop

    :goto_22
    iget-object v1, v7, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    goto/32 :goto_123

    nop

    :goto_23
    invoke-virtual/range {p0 .. p1}, Landroid/app/FragmentManagerImpl;->makeInactive(Landroid/app/Fragment;)V

    goto/32 :goto_1a0

    nop

    :goto_24
    invoke-virtual {v8, v12}, Landroid/app/Fragment;->setAnimatingAway(Landroid/animation/Animator;)V

    goto/32 :goto_1db

    nop

    :goto_25
    iget v1, v8, Landroid/app/Fragment;->mState:I

    packed-switch v1, :pswitch_data_1

    goto/32 :goto_183

    nop

    :goto_26
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_b3

    nop

    :goto_27
    const-string v4, "Cannot create fragment "

    goto/32 :goto_1af

    nop

    :goto_28
    goto/16 :goto_157

    :goto_29
    goto/32 :goto_76

    nop

    :goto_2a
    move-object/from16 v2, p1

    goto/32 :goto_161

    nop

    :goto_2b
    new-instance v1, Landroid/util/SuperNotCalledException;

    goto/32 :goto_3b

    nop

    :goto_2c
    if-eqz v1, :cond_6

    goto/32 :goto_125

    :cond_6
    goto/32 :goto_150

    nop

    :goto_2d
    invoke-virtual {v12}, Landroid/animation/Animator;->start()V

    :goto_2e
    goto/32 :goto_12c

    nop

    :goto_2f
    iput-object v1, v8, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    goto/32 :goto_17a

    nop

    :goto_30
    if-eqz v0, :cond_7

    goto/32 :goto_11e

    :cond_7
    goto/32 :goto_d0

    nop

    :goto_31
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_bd

    nop

    :goto_32
    if-nez v1, :cond_8

    goto/32 :goto_233

    :cond_8
    goto/32 :goto_1de

    nop

    :goto_33
    const/4 v11, 0x3

    goto/32 :goto_101

    nop

    :goto_34
    iget-object v0, v8, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_20a

    nop

    :goto_35
    const/4 v3, 0x0

    goto/32 :goto_8

    nop

    :goto_36
    if-eqz v1, :cond_9

    goto/32 :goto_1a1

    :cond_9
    goto/32 :goto_23

    nop

    :goto_37
    iget-object v1, v8, Landroid/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    goto/32 :goto_21e

    nop

    :goto_38
    invoke-virtual {v1}, Landroid/app/FragmentHostCallback;->getContext()Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_a4

    nop

    :goto_39
    if-nez v1, :cond_a

    goto/32 :goto_162

    :cond_a
    goto/32 :goto_155

    nop

    :goto_3a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_1fa

    nop

    :goto_3b
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_194

    nop

    :goto_3c
    invoke-virtual {v12, v9}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    goto/32 :goto_225

    nop

    :goto_3d
    iget-object v0, v8, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_1f2

    nop

    :goto_3e
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_63

    nop

    :goto_3f
    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_ea

    nop

    :goto_40
    iget-object v2, v7, Landroid/app/FragmentManagerImpl;->mContainer:Landroid/app/FragmentContainer;

    goto/32 :goto_228

    nop

    :goto_41
    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    goto/32 :goto_10f

    nop

    :goto_42
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_bf

    nop

    :goto_43
    if-lt v0, v1, :cond_b

    goto/32 :goto_ce

    :cond_b
    goto/32 :goto_205

    nop

    :goto_44
    iget v1, v8, Landroid/app/Fragment;->mState:I

    goto/32 :goto_1da

    nop

    :goto_45
    invoke-virtual {v1}, Landroid/app/FragmentHostCallback;->getContext()Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_96

    nop

    :goto_46
    move/from16 v11, p4

    :goto_47
    goto/32 :goto_1bc

    nop

    :goto_48
    move-object/from16 v8, p1

    goto/32 :goto_be

    nop

    :goto_49
    iget-object v2, v8, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_14d

    nop

    :goto_4a
    goto/16 :goto_1ef

    :goto_4b
    goto/32 :goto_1e7

    nop

    :goto_4c
    iget-object v1, v8, Landroid/app/Fragment;->mParentFragment:Landroid/app/Fragment;

    goto/32 :goto_2c

    nop

    :goto_4d
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_18

    nop

    :goto_4e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_f4

    nop

    :goto_4f
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_54

    nop

    :goto_50
    if-eqz v2, :cond_c

    goto/32 :goto_13b

    :cond_c
    goto/32 :goto_e5

    nop

    :goto_51
    invoke-virtual {v7, v8, v15}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentStopped(Landroid/app/Fragment;Z)V

    :goto_52
    :pswitch_0
    goto/32 :goto_e4

    nop

    :goto_53
    iput-object v14, v8, Landroid/app/Fragment;->mFragmentManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_128

    nop

    :goto_54
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_12b

    nop

    :goto_55
    invoke-static {v13, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_56
    goto/32 :goto_113

    nop

    :goto_57
    iput-object v0, v8, Landroid/app/Fragment;->mContainer:Landroid/view/ViewGroup;

    goto/32 :goto_221

    nop

    :goto_58
    goto/16 :goto_ce

    :goto_59
    goto/32 :goto_44

    nop

    :goto_5a
    if-nez v1, :cond_d

    goto/32 :goto_e7

    :cond_d
    goto/32 :goto_37

    nop

    :goto_5b
    if-nez v0, :cond_e

    goto/32 :goto_1c3

    :cond_e
    goto/32 :goto_1c2

    nop

    :goto_5c
    if-nez v12, :cond_f

    goto/32 :goto_2e

    :cond_f
    goto/32 :goto_224

    nop

    :goto_5d
    invoke-virtual {v2, v3}, Landroid/app/FragmentContainer;->onFindViewById(I)Landroid/view/View;

    move-result-object v2

    goto/32 :goto_d7

    nop

    :goto_5e
    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_1ad

    nop

    :goto_5f
    invoke-virtual {v7, v8, v1, v15}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentAttached(Landroid/app/Fragment;Landroid/content/Context;Z)V

    goto/32 :goto_138

    nop

    :goto_60
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_b

    nop

    :goto_61
    iget-object v1, v8, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_73

    nop

    :goto_62
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_1b2

    nop

    :goto_63
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_f7

    nop

    :goto_64
    move v0, v1

    :pswitch_1
    goto/32 :goto_e1

    nop

    :goto_65
    if-nez v1, :cond_10

    goto/32 :goto_8a

    :cond_10
    goto/32 :goto_14f

    nop

    :goto_66
    if-gt v1, v9, :cond_11

    goto/32 :goto_1ba

    :cond_11
    goto/32 :goto_41

    nop

    :goto_67
    move-object v10, v6

    goto/32 :goto_8b

    nop

    :goto_68
    const/4 v5, 0x0

    goto/32 :goto_114

    nop

    :goto_69
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_1e5

    nop

    :goto_6a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_ad

    nop

    :goto_6b
    move/from16 v11, p4

    goto/32 :goto_139

    nop

    :goto_6c
    move-object/from16 v7, p0

    goto/32 :goto_48

    nop

    :goto_6d
    iget-object v1, v8, Landroid/app/Fragment;->mContainer:Landroid/view/ViewGroup;

    goto/32 :goto_141

    nop

    :goto_6e
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_16

    nop

    :goto_6f
    goto/16 :goto_239

    :goto_70
    goto/32 :goto_238

    nop

    :goto_71
    if-nez v1, :cond_12

    goto/32 :goto_204

    :cond_12
    goto/32 :goto_197

    nop

    :goto_72
    invoke-virtual/range {p1 .. p1}, Landroid/app/Fragment;->performStart()V

    goto/32 :goto_23a

    nop

    :goto_73
    if-nez v1, :cond_13

    goto/32 :goto_c6

    :cond_13
    goto/32 :goto_83

    nop

    :goto_74
    invoke-virtual {v8, v2, v3}, Landroid/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    goto/32 :goto_115

    nop

    :goto_75
    iget v1, v8, Landroid/app/Fragment;->mState:I

    goto/32 :goto_198

    nop

    :goto_76
    move/from16 v10, p3

    goto/32 :goto_156

    nop

    :goto_77
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_18b

    nop

    :goto_78
    goto/16 :goto_ce

    :goto_79
    goto/32 :goto_151

    nop

    :goto_7a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_1f3

    nop

    :goto_7b
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    :goto_7c
    goto/32 :goto_1cb

    nop

    :goto_7d
    iput-object v1, v8, Landroid/app/Fragment;->mFragmentManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_1f6

    nop

    :goto_7e
    iget-boolean v1, v8, Landroid/app/Fragment;->mRetaining:Z

    goto/32 :goto_82

    nop

    :goto_7f
    move-object v10, v6

    :goto_80
    goto/32 :goto_19d

    nop

    :goto_81
    iget-object v1, v7, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    goto/32 :goto_131

    nop

    :goto_82
    if-eqz v1, :cond_14

    goto/32 :goto_bb

    :cond_14
    goto/32 :goto_1b1

    nop

    :goto_83
    iget-object v1, v8, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_109

    nop

    :goto_84
    move/from16 v11, p4

    :goto_85
    goto/32 :goto_f0

    nop

    :goto_86
    iget v2, v8, Landroid/app/Fragment;->mContainerId:I

    goto/32 :goto_1d8

    nop

    :goto_87
    iget-object v2, v8, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    goto/32 :goto_102

    nop

    :goto_88
    move/from16 v11, p4

    goto/32 :goto_28

    nop

    :goto_89
    iget v0, v8, Landroid/app/Fragment;->mState:I

    :goto_8a
    goto/32 :goto_c7

    nop

    :goto_8b
    move/from16 v6, v16

    goto/32 :goto_140

    nop

    :goto_8c
    iget-object v1, v8, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_1d5

    nop

    :goto_8d
    move/from16 v10, p3

    goto/32 :goto_165

    nop

    :goto_8e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_21a

    nop

    :goto_8f
    iget-boolean v1, v8, Landroid/app/Fragment;->mUserVisibleHint:Z

    goto/32 :goto_da

    nop

    :goto_90
    const/high16 v2, 0x3f800000    # 1.0f

    goto/32 :goto_ca

    nop

    :goto_91
    invoke-virtual {v1}, Landroid/view/View;->clearAnimation()V

    goto/32 :goto_6d

    nop

    :goto_92
    const-string/jumbo v2, "movefrom CREATED: "

    goto/32 :goto_d9

    nop

    :goto_93
    move-object/from16 v2, p0

    goto/32 :goto_98

    nop

    :goto_94
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_230

    nop

    :goto_95
    iget-object v1, v8, Landroid/app/Fragment;->mParentFragment:Landroid/app/Fragment;

    goto/32 :goto_13c

    nop

    :goto_96
    invoke-virtual {v7, v8, v1, v15}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentPreAttached(Landroid/app/Fragment;Landroid/content/Context;Z)V

    goto/32 :goto_ae

    nop

    :goto_97
    if-nez v1, :cond_15

    goto/32 :goto_220

    :cond_15
    goto/32 :goto_12e

    nop

    :goto_98
    move-object v9, v3

    goto/32 :goto_b5

    nop

    :goto_99
    iget-object v2, v8, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_193

    nop

    :goto_9a
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_229

    nop

    :goto_9b
    iget v1, v8, Landroid/app/Fragment;->mState:I

    goto/32 :goto_dd

    nop

    :goto_9c
    const-string v6, "Fragment "

    goto/32 :goto_15b

    nop

    :goto_9d
    iget-object v1, v7, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    goto/32 :goto_18c

    nop

    :goto_9e
    invoke-virtual {v8, v1}, Landroid/app/Fragment;->performCreate(Landroid/os/Bundle;)V

    goto/32 :goto_212

    nop

    :goto_9f
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_d3

    nop

    :goto_a0
    sget-boolean v1, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    goto/32 :goto_fd

    nop

    :goto_a1
    const/4 v1, 0x4

    goto/32 :goto_22b

    nop

    :goto_a2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_146

    nop

    :goto_a3
    invoke-virtual {v7, v8, v0, v15}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentActivityCreated(Landroid/app/Fragment;Landroid/os/Bundle;Z)V

    goto/32 :goto_34

    nop

    :goto_a4
    invoke-virtual {v8, v1}, Landroid/app/Fragment;->onAttach(Landroid/content/Context;)V

    goto/32 :goto_1cd

    nop

    :goto_a5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_17e

    nop

    :goto_a6
    const-string v2, "android:target_state"

    goto/32 :goto_103

    nop

    :goto_a7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_112

    nop

    :goto_a8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_167

    nop

    :goto_a9
    const/4 v10, 0x4

    goto/32 :goto_33

    nop

    :goto_aa
    invoke-virtual {v7, v8, v15}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentDetached(Landroid/app/Fragment;Z)V

    goto/32 :goto_1b6

    nop

    :goto_ab
    invoke-virtual {v7, v8, v15}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentPaused(Landroid/app/Fragment;Z)V

    :goto_ac
    :pswitch_2
    goto/32 :goto_c1

    nop

    :goto_ad
    invoke-direct {v1, v2}, Landroid/util/SuperNotCalledException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_105

    nop

    :goto_ae
    iput-boolean v15, v8, Landroid/app/Fragment;->mCalled:Z

    goto/32 :goto_b4

    nop

    :goto_af
    const-string/jumbo v3, "unknown"

    goto/32 :goto_16c

    nop

    :goto_b0
    if-gt v0, v9, :cond_16

    goto/32 :goto_179

    :cond_16
    goto/32 :goto_178

    nop

    :goto_b1
    invoke-virtual {v1, v2, v15}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    goto/32 :goto_1c0

    nop

    :goto_b2
    invoke-virtual {v7, v8, v1, v15}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentCreated(Landroid/app/Fragment;Landroid/os/Bundle;Z)V

    goto/32 :goto_4a

    nop

    :goto_b3
    const-string/jumbo v2, "movefrom ACTIVITY_CREATED: "

    goto/32 :goto_3

    nop

    :goto_b4
    iget-object v1, v7, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    goto/32 :goto_38

    nop

    :goto_b5
    move-object v3, v5

    goto/32 :goto_ff

    nop

    :goto_b6
    const-string v5, "No view found for id 0x"

    goto/32 :goto_31

    nop

    :goto_b7
    iget v1, v8, Landroid/app/Fragment;->mState:I

    goto/32 :goto_21d

    nop

    :goto_b8
    iput v15, v8, Landroid/app/Fragment;->mState:I

    :goto_b9
    goto/32 :goto_22c

    nop

    :goto_ba
    goto :goto_b9

    :goto_bb
    goto/32 :goto_b8

    nop

    :goto_bc
    iget-object v2, v8, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_176

    nop

    :goto_bd
    iget v5, v8, Landroid/app/Fragment;->mContainerId:I

    goto/32 :goto_1e0

    nop

    :goto_be
    iget-boolean v0, v8, Landroid/app/Fragment;->mAdded:Z

    goto/32 :goto_e3

    nop

    :goto_bf
    const-string v3, " declared target fragment "

    goto/32 :goto_168

    nop

    :goto_c0
    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v1

    goto/32 :goto_1ac

    nop

    :goto_c1
    const/4 v1, 0x4

    goto/32 :goto_19a

    nop

    :goto_c2
    if-eqz v1, :cond_17

    goto/32 :goto_4b

    :cond_17
    goto/32 :goto_104

    nop

    :goto_c3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_1f4

    nop

    :goto_c4
    if-lt v0, v1, :cond_18

    goto/32 :goto_ac

    :cond_18
    goto/32 :goto_1fe

    nop

    :goto_c5
    const/4 v0, 0x3

    :goto_c6
    goto/32 :goto_14c

    nop

    :goto_c7
    iget-boolean v1, v8, Landroid/app/Fragment;->mDeferStart:Z

    goto/32 :goto_a9

    nop

    :goto_c8
    const/16 v3, 0x8

    goto/32 :goto_7b

    nop

    :goto_c9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_12a

    nop

    :goto_ca
    invoke-virtual {v1, v2}, Landroid/view/View;->setTransitionAlpha(F)V

    goto/32 :goto_5c

    nop

    :goto_cb
    const-string v5, " ("

    goto/32 :goto_4e

    nop

    :goto_cc
    iget-object v1, v8, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_237

    nop

    :goto_cd
    move/from16 v11, p4

    :goto_ce
    goto/32 :goto_75

    nop

    :goto_cf
    const/4 v4, 0x0

    goto/32 :goto_1df

    nop

    :goto_d0
    const/4 v0, 0x0

    goto/32 :goto_86

    nop

    :goto_d1
    iput v0, v8, Landroid/app/Fragment;->mState:I

    :goto_d2
    goto/32 :goto_135

    nop

    :goto_d3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1dc

    nop

    :goto_d4
    goto/16 :goto_13

    :pswitch_3
    goto/32 :goto_1f1

    nop

    :goto_d5
    const/4 v14, 0x0

    goto/32 :goto_f2

    nop

    :goto_d6
    iget-boolean v1, v8, Landroid/app/Fragment;->mRemoving:Z

    goto/32 :goto_65

    nop

    :goto_d7
    check-cast v2, Landroid/view/ViewGroup;

    goto/32 :goto_50

    nop

    :goto_d8
    sget-boolean v1, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    goto/32 :goto_181

    nop

    :goto_d9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_ee

    nop

    :goto_da
    if-eqz v1, :cond_19

    goto/32 :goto_c6

    :cond_19
    goto/32 :goto_1b7

    nop

    :goto_db
    iget-object v1, v8, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_9e

    nop

    :goto_dc
    invoke-virtual {v1}, Landroid/app/FragmentHostCallback;->getContext()Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_5f

    nop

    :goto_dd
    const/4 v12, 0x2

    goto/32 :goto_22d

    nop

    :goto_de
    if-eqz v1, :cond_1a

    goto/32 :goto_204

    :cond_1a
    goto/32 :goto_199

    nop

    :goto_df
    goto/16 :goto_179

    :goto_e0
    goto/32 :goto_1ab

    nop

    :goto_e1
    if-gt v0, v12, :cond_1b

    goto/32 :goto_189

    :cond_1b
    goto/32 :goto_188

    nop

    :goto_e2
    new-instance v2, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_19b

    nop

    :goto_e3
    const/4 v9, 0x1

    goto/32 :goto_1a8

    nop

    :goto_e4
    if-lt v0, v12, :cond_1c

    goto/32 :goto_175

    :cond_1c
    goto/32 :goto_196

    nop

    :goto_e5
    iget-boolean v0, v8, Landroid/app/Fragment;->mRestored:Z

    goto/32 :goto_20

    nop

    :goto_e6
    invoke-virtual/range {p0 .. p1}, Landroid/app/FragmentManagerImpl;->saveFragmentViewState(Landroid/app/Fragment;)V

    :goto_e7
    goto/32 :goto_1c9

    nop

    :goto_e8
    const/4 v0, 0x1

    goto/32 :goto_78

    nop

    :goto_e9
    iget-boolean v1, v8, Landroid/app/Fragment;->mFromLayout:Z

    goto/32 :goto_133

    nop

    :goto_ea
    iget-object v2, v8, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    goto/32 :goto_117

    nop

    :goto_eb
    const-string v3, " that does not belong to this FragmentManager!"

    goto/32 :goto_215

    nop

    :goto_ec
    invoke-virtual {v8, v0}, Landroid/app/Fragment;->restoreViewState(Landroid/os/Bundle;)V

    :goto_ed
    goto/32 :goto_1b9

    nop

    :goto_ee
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_163

    nop

    :goto_ef
    if-le v1, v0, :cond_1d

    goto/32 :goto_59

    :cond_1d
    goto/32 :goto_e9

    nop

    :goto_f0
    iput-object v14, v8, Landroid/app/Fragment;->mContainer:Landroid/view/ViewGroup;

    goto/32 :goto_1e4

    nop

    :goto_f1
    iget-boolean v0, v8, Landroid/app/Fragment;->mDetached:Z

    goto/32 :goto_5b

    nop

    :goto_f2
    const/4 v15, 0x0

    goto/32 :goto_ef

    nop

    :goto_f3
    iget-object v1, v7, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    goto/32 :goto_17c

    nop

    :goto_f4
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_21

    nop

    :goto_f5
    move/from16 v11, p4

    goto/32 :goto_58

    nop

    :goto_f6
    invoke-virtual {v7, v8, v1, v15}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentPreCreated(Landroid/app/Fragment;Landroid/os/Bundle;Z)V

    goto/32 :goto_db

    nop

    :goto_f7
    invoke-static {v13, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_f8
    goto/32 :goto_1a3

    nop

    :goto_f9
    iput-object v14, v8, Landroid/app/Fragment;->mParentFragment:Landroid/app/Fragment;

    goto/32 :goto_53

    nop

    :goto_fa
    if-nez v1, :cond_1e

    goto/32 :goto_1fb

    :cond_1e
    goto/32 :goto_6e

    nop

    :goto_fb
    if-gtz v2, :cond_1f

    goto/32 :goto_1f8

    :cond_1f
    goto/32 :goto_154

    nop

    :goto_fc
    iget-object v1, v8, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_186

    nop

    :goto_fd
    if-nez v1, :cond_20

    goto/32 :goto_7

    :cond_20
    goto/32 :goto_9f

    nop

    :goto_fe
    iget v2, v2, Landroid/app/Fragment;->mIndex:I

    goto/32 :goto_3f

    nop

    :goto_ff
    move-object/from16 v17, v4

    goto/32 :goto_240

    nop

    :goto_100
    move v1, v0

    goto/32 :goto_18d

    nop

    :goto_101
    if-nez v1, :cond_21

    goto/32 :goto_a

    :cond_21
    goto/32 :goto_b7

    nop

    :goto_102
    const/4 v3, 0x1

    goto/32 :goto_cf

    nop

    :goto_103
    invoke-virtual {v7, v1, v2}, Landroid/app/FragmentManagerImpl;->getFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    goto/32 :goto_2f

    nop

    :goto_104
    iget-object v1, v8, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_f6

    nop

    :goto_105
    throw v1

    :goto_106
    :pswitch_4
    goto/32 :goto_184

    nop

    :goto_107
    invoke-static {v13, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_108
    goto/32 :goto_fc

    nop

    :goto_109
    const-string v2, "android:view_state"

    goto/32 :goto_c0

    nop

    :goto_10a
    iget-object v2, v8, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_182

    nop

    :goto_10b
    iput-boolean v15, v8, Landroid/app/Fragment;->mInLayout:Z

    goto/32 :goto_174

    nop

    :goto_10c
    move-object/from16 v5, p1

    goto/32 :goto_147

    nop

    :goto_10d
    if-nez v1, :cond_22

    goto/32 :goto_108

    :cond_22
    goto/32 :goto_158

    nop

    :goto_10e
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_20d

    nop

    :goto_10f
    if-nez v0, :cond_23

    goto/32 :goto_56

    :cond_23
    goto/32 :goto_e

    nop

    :goto_110
    iget-object v1, v8, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_119

    nop

    :goto_111
    if-nez v1, :cond_24

    goto/32 :goto_79

    :cond_24
    goto/32 :goto_1b4

    nop

    :goto_112
    const-string/jumbo v2, "moveToState: Fragment state for "

    goto/32 :goto_a5

    nop

    :goto_113
    iget-boolean v0, v8, Landroid/app/Fragment;->mFromLayout:Z

    goto/32 :goto_30

    nop

    :goto_114
    const/4 v6, 0x1

    goto/32 :goto_1d4

    nop

    :goto_115
    iget-object v2, v8, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_209

    nop

    :goto_116
    if-eqz v2, :cond_25

    goto/32 :goto_70

    :cond_25
    goto/32 :goto_1dd

    nop

    :goto_117
    if-eq v1, v2, :cond_26

    goto/32 :goto_11c

    :cond_26
    goto/32 :goto_23e

    nop

    :goto_118
    new-instance v3, Landroid/app/FragmentManagerImpl$2;

    goto/32 :goto_1bb

    nop

    :goto_119
    if-nez v1, :cond_27

    goto/32 :goto_d

    :cond_27
    goto/32 :goto_1c6

    nop

    :goto_11a
    if-nez v2, :cond_28

    goto/32 :goto_70

    :cond_28
    goto/32 :goto_6f

    nop

    :goto_11b
    goto/16 :goto_80

    :goto_11c
    goto/32 :goto_1e6

    nop

    :goto_11d
    iput-boolean v9, v8, Landroid/app/Fragment;->mIsNewlyAdded:Z

    :goto_11e
    goto/32 :goto_3d

    nop

    :goto_11f
    if-eqz v1, :cond_29

    goto/32 :goto_121

    :cond_29
    goto/32 :goto_120

    nop

    :goto_120
    return-void

    :goto_121
    goto/32 :goto_13e

    nop

    :goto_122
    move/from16 v10, p3

    goto/32 :goto_46

    nop

    :goto_123
    if-nez v1, :cond_2a

    goto/32 :goto_149

    :cond_2a
    goto/32 :goto_160

    nop

    :goto_124
    goto/16 :goto_13d

    :goto_125
    goto/32 :goto_95

    nop

    :goto_126
    const/4 v3, -0x1

    goto/32 :goto_159

    nop

    :goto_127
    iget v1, v8, Landroid/app/Fragment;->mState:I

    packed-switch v1, :pswitch_data_0

    goto/32 :goto_d4

    nop

    :goto_128
    goto/16 :goto_ce

    :goto_129
    goto/32 :goto_22e

    nop

    :goto_12a
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_62

    nop

    :goto_12b
    const-string/jumbo v2, "movefrom RESUMED: "

    goto/32 :goto_c9

    nop

    :goto_12c
    iget-object v1, v8, Landroid/app/Fragment;->mContainer:Landroid/view/ViewGroup;

    goto/32 :goto_216

    nop

    :goto_12d
    if-gt v0, v11, :cond_2b

    goto/32 :goto_23b

    :cond_2b
    goto/32 :goto_a0

    nop

    :goto_12e
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_1f9

    nop

    :goto_12f
    goto/16 :goto_ce

    :pswitch_5
    goto/32 :goto_1cc

    nop

    :goto_130
    move/from16 v0, p2

    goto/32 :goto_df

    nop

    :goto_131
    invoke-virtual {v1}, Landroid/app/FragmentHostCallback;->getFragmentManagerImpl()Landroid/app/FragmentManagerImpl;

    move-result-object v1

    :goto_132
    goto/32 :goto_7d

    nop

    :goto_133
    if-nez v1, :cond_2c

    goto/32 :goto_121

    :cond_2c
    goto/32 :goto_210

    nop

    :goto_134
    iput-boolean v1, v8, Landroid/app/Fragment;->mUserVisibleHint:Z

    goto/32 :goto_8f

    nop

    :goto_135
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_6
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_5
    .end packed-switch

    :goto_136
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :goto_137
    goto/32 :goto_18f

    nop

    :goto_138
    iget-boolean v1, v8, Landroid/app/Fragment;->mIsCreated:Z

    goto/32 :goto_c2

    nop

    :goto_139
    invoke-virtual {v7, v8, v10, v15, v11}, Landroid/app/FragmentManagerImpl;->loadAnimator(Landroid/app/Fragment;IZI)Landroid/animation/Animator;

    move-result-object v1

    goto/32 :goto_1d1

    nop

    :goto_13a
    invoke-direct {v7, v3}, Landroid/app/FragmentManagerImpl;->throwException(Ljava/lang/RuntimeException;)V

    :goto_13b
    goto/32 :goto_213

    nop

    :goto_13c
    invoke-virtual {v1, v8}, Landroid/app/Fragment;->onAttachFragment(Landroid/app/Fragment;)V

    :goto_13d
    goto/32 :goto_15d

    nop

    :goto_13e
    invoke-virtual/range {p1 .. p1}, Landroid/app/Fragment;->getAnimatingAway()Landroid/animation/Animator;

    move-result-object v1

    goto/32 :goto_39

    nop

    :goto_13f
    invoke-virtual {v8, v2}, Landroid/app/Fragment;->performGetLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v2

    goto/32 :goto_5

    nop

    :goto_140
    invoke-virtual/range {v1 .. v6}, Landroid/app/FragmentManagerImpl;->moveToState(Landroid/app/Fragment;IIIZ)V

    goto/32 :goto_1ff

    nop

    :goto_141
    iget-object v2, v8, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_172

    nop

    :goto_142
    iput-object v14, v8, Landroid/app/Fragment;->mHost:Landroid/app/FragmentHostCallback;

    goto/32 :goto_f9

    nop

    :goto_143
    iget-boolean v1, v8, Landroid/app/Fragment;->mRetaining:Z

    goto/32 :goto_36

    nop

    :goto_144
    if-nez v1, :cond_2d

    goto/32 :goto_f8

    :cond_2d
    goto/32 :goto_1f0

    nop

    :goto_145
    invoke-virtual/range {p1 .. p1}, Landroid/app/Fragment;->getStateAfterAnimating()I

    move-result v3

    goto/32 :goto_1e1

    nop

    :goto_146
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_13a

    nop

    :goto_147
    invoke-direct/range {v1 .. v6}, Landroid/app/FragmentManagerImpl$2;-><init>(Landroid/app/FragmentManagerImpl;Landroid/view/ViewGroup;Landroid/view/View;Landroid/app/Fragment;Landroid/app/Fragment;)V

    goto/32 :goto_3c

    nop

    :goto_148
    goto/16 :goto_132

    :goto_149
    goto/32 :goto_81

    nop

    :goto_14a
    iget-object v0, v8, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_ec

    nop

    :goto_14b
    iget-boolean v2, v7, Landroid/app/FragmentManagerImpl;->mDestroyed:Z

    goto/32 :goto_22f

    nop

    :goto_14c
    iget-object v1, v7, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    goto/32 :goto_170

    nop

    :goto_14d
    invoke-virtual {v2}, Landroid/view/View;->getTransitionAlpha()F

    move-result v2

    goto/32 :goto_35

    nop

    :goto_14e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_10

    nop

    :goto_14f
    iget v1, v8, Landroid/app/Fragment;->mState:I

    goto/32 :goto_1c

    nop

    :goto_150
    iget-object v1, v7, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    goto/32 :goto_16a

    nop

    :goto_151
    sget-boolean v1, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    goto/32 :goto_97

    nop

    :goto_152
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_11

    nop

    :goto_153
    if-gtz v2, :cond_2e

    goto/32 :goto_29

    :cond_2e
    goto/32 :goto_14b

    nop

    :goto_154
    move/from16 v10, p3

    goto/32 :goto_6b

    nop

    :goto_155
    invoke-virtual {v8, v14}, Landroid/app/Fragment;->setAnimatingAway(Landroid/animation/Animator;)V

    goto/32 :goto_145

    nop

    :goto_156
    move/from16 v11, p4

    :goto_157
    goto/32 :goto_201

    nop

    :goto_158
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_26

    nop

    :goto_159
    if-eq v2, v3, :cond_2f

    goto/32 :goto_1ae

    :cond_2f
    goto/32 :goto_e2

    nop

    :goto_15a
    invoke-virtual {v7, v8, v2, v3, v15}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentViewCreated(Landroid/app/Fragment;Landroid/view/View;Landroid/os/Bundle;Z)V

    goto/32 :goto_bc

    nop

    :goto_15b
    if-nez v1, :cond_30

    goto/32 :goto_1d7

    :cond_30
    goto/32 :goto_9d

    nop

    :goto_15c
    invoke-virtual {v7, v8, v15}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentResumed(Landroid/app/Fragment;Z)V

    goto/32 :goto_0

    nop

    :goto_15d
    iget-object v1, v7, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    goto/32 :goto_dc

    nop

    :goto_15e
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_1f

    nop

    :goto_15f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_55

    nop

    :goto_160
    iget-object v1, v1, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_148

    nop

    :goto_161
    invoke-virtual/range {v1 .. v6}, Landroid/app/FragmentManagerImpl;->moveToState(Landroid/app/Fragment;IIIZ)V

    :goto_162
    goto/32 :goto_127

    nop

    :goto_163
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_21f

    nop

    :goto_164
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_1cf

    nop

    :goto_165
    move/from16 v11, p4

    goto/32 :goto_1a9

    nop

    :goto_166
    if-nez v1, :cond_31

    goto/32 :goto_1b3

    :cond_31
    goto/32 :goto_4f

    nop

    :goto_167
    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_a2

    nop

    :goto_168
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_20c

    nop

    :goto_169
    const/16 v2, 0x1a

    goto/32 :goto_17

    nop

    :goto_16a
    invoke-virtual {v1, v8}, Landroid/app/FragmentHostCallback;->onAttachFragment(Landroid/app/Fragment;)V

    goto/32 :goto_124

    nop

    :goto_16b
    invoke-virtual/range {p0 .. p1}, Landroid/app/FragmentManagerImpl;->ensureInflatedFragmentView(Landroid/app/Fragment;)V

    goto/32 :goto_66

    nop

    :goto_16c
    move-object v0, v3

    :goto_16d
    goto/32 :goto_1fc

    nop

    :goto_16e
    if-nez v2, :cond_32

    goto/32 :goto_11e

    :cond_32
    goto/32 :goto_10a

    nop

    :goto_16f
    invoke-virtual/range {p1 .. p1}, Landroid/app/Fragment;->getAnimatingAway()Landroid/animation/Animator;

    move-result-object v1

    goto/32 :goto_111

    nop

    :goto_170
    iput-object v1, v8, Landroid/app/Fragment;->mHost:Landroid/app/FragmentHostCallback;

    goto/32 :goto_2

    nop

    :goto_171
    iget-object v1, v8, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_91

    nop

    :goto_172
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->endViewTransition(Landroid/view/View;)V

    :goto_173
    goto/32 :goto_192

    nop

    :goto_174
    goto/16 :goto_47

    :goto_175
    goto/32 :goto_1b8

    nop

    :goto_176
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    goto/32 :goto_116

    nop

    :goto_177
    move/from16 v10, p3

    goto/32 :goto_88

    nop

    :goto_178
    const/4 v0, 0x1

    :goto_179
    goto/32 :goto_d6

    nop

    :goto_17a
    iget-object v1, v8, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    goto/32 :goto_217

    nop

    :goto_17b
    iput-object v1, v8, Landroid/app/Fragment;->mParentFragment:Landroid/app/Fragment;

    goto/32 :goto_22

    nop

    :goto_17c
    invoke-virtual {v1, v8}, Landroid/app/FragmentHostCallback;->onShouldSaveFragmentState(Landroid/app/Fragment;)Z

    move-result v1

    goto/32 :goto_5a

    nop

    :goto_17d
    invoke-virtual {v12, v1}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    goto/32 :goto_cc

    nop

    :goto_17e
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_1d3

    nop

    :goto_17f
    iget-object v1, v8, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_90

    nop

    :goto_180
    invoke-virtual {v7, v8, v15}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentViewDestroyed(Landroid/app/Fragment;Z)V

    goto/32 :goto_110

    nop

    :goto_181
    if-nez v1, :cond_33

    goto/32 :goto_1f5

    :cond_33
    goto/32 :goto_1a4

    nop

    :goto_182
    invoke-virtual {v2, v15}, Landroid/view/View;->setSaveFromParentEnabled(Z)V

    goto/32 :goto_226

    nop

    :goto_183
    move/from16 v10, p3

    goto/32 :goto_1be

    nop

    :goto_184
    move v1, v0

    :goto_185
    goto/32 :goto_16b

    nop

    :goto_186
    if-nez v1, :cond_34

    goto/32 :goto_e7

    :cond_34
    goto/32 :goto_f3

    nop

    :goto_187
    if-nez v1, :cond_35

    goto/32 :goto_d

    :cond_35
    goto/32 :goto_211

    nop

    :goto_188
    iput v11, v8, Landroid/app/Fragment;->mState:I

    :goto_189
    :pswitch_6
    goto/32 :goto_12d

    nop

    :goto_18a
    if-nez v2, :cond_36

    goto/32 :goto_7c

    :cond_36
    goto/32 :goto_1ca

    nop

    :goto_18b
    iget v2, v8, Landroid/app/Fragment;->mState:I

    goto/32 :goto_164

    nop

    :goto_18c
    iget-object v2, v8, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    goto/32 :goto_fe

    nop

    :goto_18d
    goto :goto_185

    :goto_18e
    goto/32 :goto_2b

    nop

    :goto_18f
    iget-boolean v2, v8, Landroid/app/Fragment;->mHidden:Z

    goto/32 :goto_18a

    nop

    :goto_190
    iget-object v4, v8, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_219

    nop

    :goto_191
    iput-object v2, v8, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_206

    nop

    :goto_192
    const/4 v1, 0x0

    goto/32 :goto_21b

    nop

    :goto_193
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    goto/32 :goto_f

    nop

    :goto_194
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_9a

    nop

    :goto_195
    move/from16 v10, p3

    goto/32 :goto_f5

    nop

    :goto_196
    sget-boolean v1, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    goto/32 :goto_10d

    nop

    :goto_197
    const/4 v0, 0x1

    goto/32 :goto_203

    nop

    :goto_198
    if-ne v1, v0, :cond_37

    goto/32 :goto_d2

    :cond_37
    goto/32 :goto_1c8

    nop

    :goto_199
    invoke-virtual/range {p1 .. p1}, Landroid/app/Fragment;->isInBackStack()Z

    move-result v1

    goto/32 :goto_71

    nop

    :goto_19a
    if-lt v0, v1, :cond_38

    goto/32 :goto_52

    :cond_38
    goto/32 :goto_236

    nop

    :goto_19b
    new-instance v3, Ljava/lang/StringBuilder;

    goto/32 :goto_1c4

    nop

    :goto_19c
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_1b5

    nop

    :goto_19d
    iget-object v1, v7, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    goto/32 :goto_45

    nop

    :goto_19e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_21c

    nop

    :goto_19f
    iget-object v0, v8, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_a3

    nop

    :goto_1a0
    goto/16 :goto_ce

    :goto_1a1
    goto/32 :goto_142

    nop

    :goto_1a2
    invoke-static {v13, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_d1

    nop

    :goto_1a3
    invoke-virtual/range {p1 .. p1}, Landroid/app/Fragment;->performStop()V

    goto/32 :goto_51

    nop

    :goto_1a4
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_227

    nop

    :goto_1a5
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_cb

    nop

    :goto_1a6
    const-string/jumbo v2, "moveto RESUMED: "

    goto/32 :goto_19e

    nop

    :goto_1a7
    iget-object v1, v8, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_a6

    nop

    :goto_1a8
    if-nez v0, :cond_39

    goto/32 :goto_e0

    :cond_39
    goto/32 :goto_f1

    nop

    :goto_1a9
    goto/16 :goto_157

    :goto_1aa
    goto/32 :goto_177

    nop

    :goto_1ab
    move/from16 v0, p2

    goto/32 :goto_b0

    nop

    :goto_1ac
    iput-object v1, v8, Landroid/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    goto/32 :goto_1a7

    nop

    :goto_1ad
    invoke-direct {v7, v2}, Landroid/app/FragmentManagerImpl;->throwException(Ljava/lang/RuntimeException;)V

    :goto_1ae
    goto/32 :goto_40

    nop

    :goto_1af
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_1e2

    nop

    :goto_1b0
    iget-object v3, v8, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_74

    nop

    :goto_1b1
    invoke-virtual/range {p1 .. p1}, Landroid/app/Fragment;->performDestroy()V

    goto/32 :goto_23d

    nop

    :goto_1b2
    invoke-static {v13, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1b3
    goto/32 :goto_22a

    nop

    :goto_1b4
    invoke-virtual {v8, v0}, Landroid/app/Fragment;->setStateAfterAnimating(I)V

    goto/32 :goto_e8

    nop

    :goto_1b5
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_15f

    nop

    :goto_1b6
    if-eqz p5, :cond_3a

    goto/32 :goto_ce

    :cond_3a
    goto/32 :goto_143

    nop

    :goto_1b7
    iput-boolean v9, v8, Landroid/app/Fragment;->mDeferStart:Z

    goto/32 :goto_1b

    nop

    :goto_1b8
    move/from16 v10, p3

    goto/32 :goto_1fd

    nop

    :goto_1b9
    iput-object v14, v8, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    :goto_1ba
    goto/32 :goto_64

    nop

    :goto_1bb
    move-object v1, v3

    goto/32 :goto_93

    nop

    :goto_1bc
    const/4 v1, 0x1

    goto/32 :goto_43

    nop

    :goto_1bd
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto/32 :goto_c

    nop

    :goto_1be
    move/from16 v11, p4

    goto/32 :goto_12f

    nop

    :goto_1bf
    iget-object v1, v8, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_1c7

    nop

    :goto_1c0
    iput v1, v8, Landroid/app/Fragment;->mTargetRequestCode:I

    :goto_1c1
    goto/32 :goto_8c

    nop

    :goto_1c2
    goto/16 :goto_e0

    :goto_1c3
    goto/32 :goto_130

    nop

    :goto_1c4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_27

    nop

    :goto_1c5
    iget v1, v1, Landroid/app/Fragment;->mState:I

    goto/32 :goto_20e

    nop

    :goto_1c6
    iget-object v1, v8, Landroid/app/Fragment;->mContainer:Landroid/view/ViewGroup;

    goto/32 :goto_187

    nop

    :goto_1c7
    const-string v2, "android:target_req_state"

    goto/32 :goto_b1

    nop

    :goto_1c8
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_a7

    nop

    :goto_1c9
    invoke-virtual/range {p1 .. p1}, Landroid/app/Fragment;->performDestroyView()V

    goto/32 :goto_180

    nop

    :goto_1ca
    iget-object v2, v8, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_c8

    nop

    :goto_1cb
    iget-object v2, v8, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_1b0

    nop

    :goto_1cc
    const/4 v1, 0x5

    goto/32 :goto_c4

    nop

    :goto_1cd
    iget-boolean v1, v8, Landroid/app/Fragment;->mCalled:Z

    goto/32 :goto_4

    nop

    :goto_1ce
    invoke-virtual {v8, v1, v9}, Landroid/app/Fragment;->restoreChildFragmentState(Landroid/os/Bundle;Z)V

    goto/32 :goto_1ee

    nop

    :goto_1cf
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_1a2

    nop

    :goto_1d0
    invoke-virtual {v8, v14}, Landroid/app/Fragment;->setAnimatingAway(Landroid/animation/Animator;)V

    goto/32 :goto_232

    nop

    :goto_1d1
    move-object v12, v1

    goto/32 :goto_1f7

    nop

    :goto_1d2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_3e

    nop

    :goto_1d3
    const-string v2, " not updated inline; expected state "

    goto/32 :goto_94

    nop

    :goto_1d4
    move-object/from16 v1, p0

    goto/32 :goto_2a

    nop

    :goto_1d5
    const-string v2, "android:user_visible_hint"

    goto/32 :goto_223

    nop

    :goto_1d6
    throw v1

    :goto_1d7
    goto/32 :goto_7f

    nop

    :goto_1d8
    if-nez v2, :cond_3b

    goto/32 :goto_214

    :cond_3b
    goto/32 :goto_15

    nop

    :goto_1d9
    invoke-virtual {v5, v4}, Landroid/view/ViewGroup;->startViewTransition(Landroid/view/View;)V

    goto/32 :goto_24

    nop

    :goto_1da
    if-gt v1, v0, :cond_3c

    goto/32 :goto_129

    :cond_3c
    goto/32 :goto_25

    nop

    :goto_1db
    invoke-virtual {v8, v0}, Landroid/app/Fragment;->setStateAfterAnimating(I)V

    goto/32 :goto_118

    nop

    :goto_1dc
    const-string/jumbo v2, "moveto STARTED: "

    goto/32 :goto_7a

    nop

    :goto_1dd
    iget-object v2, v8, Landroid/app/Fragment;->mContainer:Landroid/view/ViewGroup;

    goto/32 :goto_11a

    nop

    :goto_1de
    invoke-virtual/range {p1 .. p1}, Landroid/app/Fragment;->getAnimatingAway()Landroid/animation/Animator;

    move-result-object v1

    goto/32 :goto_1d0

    nop

    :goto_1df
    const/4 v5, 0x0

    goto/32 :goto_1ea

    nop

    :goto_1e0
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_1a5

    nop

    :goto_1e1
    const/4 v4, 0x0

    goto/32 :goto_68

    nop

    :goto_1e2
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_1

    nop

    :goto_1e3
    const-string v3, " did not call through to super.onAttach()"

    goto/32 :goto_23f

    nop

    :goto_1e4
    iput-object v14, v8, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_10b

    nop

    :goto_1e5
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_1d6

    nop

    :goto_1e6
    move-object v10, v6

    goto/32 :goto_208

    nop

    :goto_1e7
    iget-object v1, v8, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_1ce

    nop

    :goto_1e8
    invoke-virtual/range {p1 .. p1}, Landroid/app/Fragment;->performResume()V

    goto/32 :goto_15c

    nop

    :goto_1e9
    const-string/jumbo v2, "moveto CREATED: "

    goto/32 :goto_8e

    nop

    :goto_1ea
    const/16 v16, 0x1

    goto/32 :goto_19

    nop

    :goto_1eb
    invoke-virtual {v8, v2, v0, v3}, Landroid/app/Fragment;->performCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    goto/32 :goto_191

    nop

    :goto_1ec
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_6

    nop

    :goto_1ed
    const-string v2, " found "

    goto/32 :goto_77

    nop

    :goto_1ee
    iput v9, v8, Landroid/app/Fragment;->mState:I

    :goto_1ef
    goto/32 :goto_23c

    nop

    :goto_1f0
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_14e

    nop

    :goto_1f1
    if-gtz v0, :cond_3d

    goto/32 :goto_106

    :cond_3d
    goto/32 :goto_14

    nop

    :goto_1f2
    invoke-virtual {v8, v0}, Landroid/app/Fragment;->performActivityCreated(Landroid/os/Bundle;)V

    goto/32 :goto_19f

    nop

    :goto_1f3
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_1ec

    nop

    :goto_1f4
    invoke-static {v13, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1f5
    goto/32 :goto_1e8

    nop

    :goto_1f6
    iget-object v1, v8, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    goto/32 :goto_9c

    nop

    :goto_1f7
    goto :goto_202

    :goto_1f8
    goto/32 :goto_8d

    nop

    :goto_1f9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_92

    nop

    :goto_1fa
    invoke-static {v13, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1fb
    goto/32 :goto_61

    nop

    :goto_1fc
    new-instance v3, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_15e

    nop

    :goto_1fd
    move/from16 v11, p4

    goto/32 :goto_218

    nop

    :goto_1fe
    sget-boolean v1, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    goto/32 :goto_166

    nop

    :goto_1ff
    goto/16 :goto_80

    :goto_200
    goto/32 :goto_1e

    nop

    :goto_201
    move-object v12, v1

    :goto_202
    goto/32 :goto_17f

    nop

    :goto_203
    goto/16 :goto_8a

    :goto_204
    goto/32 :goto_89

    nop

    :goto_205
    iget-boolean v1, v7, Landroid/app/FragmentManagerImpl;->mDestroyed:Z

    goto/32 :goto_20f

    nop

    :goto_206
    iget-object v2, v8, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_16e

    nop

    :goto_207
    iget v1, v8, Landroid/app/Fragment;->mState:I

    goto/32 :goto_de

    nop

    :goto_208
    new-instance v1, Ljava/lang/IllegalStateException;

    goto/32 :goto_10e

    nop

    :goto_209
    iget-object v3, v8, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_15a

    nop

    :goto_20a
    if-nez v0, :cond_3e

    goto/32 :goto_ed

    :cond_3e
    goto/32 :goto_14a

    nop

    :goto_20b
    if-gt v0, v11, :cond_3f

    goto/32 :goto_a

    :cond_3f
    goto/32 :goto_9

    nop

    :goto_20c
    iget-object v3, v8, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    goto/32 :goto_235

    nop

    :goto_20d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1d

    nop

    :goto_20e
    if-lt v1, v9, :cond_40

    goto/32 :goto_200

    :cond_40
    goto/32 :goto_87

    nop

    :goto_20f
    if-nez v1, :cond_41

    goto/32 :goto_233

    :cond_41
    goto/32 :goto_222

    nop

    :goto_210
    iget-boolean v1, v8, Landroid/app/Fragment;->mInLayout:Z

    goto/32 :goto_11f

    nop

    :goto_211
    invoke-virtual/range {p0 .. p0}, Landroid/app/FragmentManagerImpl;->getTargetSdk()I

    move-result v1

    goto/32 :goto_169

    nop

    :goto_212
    iget-object v1, v8, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_b2

    nop

    :goto_213
    move-object v0, v2

    :goto_214
    goto/32 :goto_57

    nop

    :goto_215
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_69

    nop

    :goto_216
    iget-object v2, v8, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_1bd

    nop

    :goto_217
    if-nez v1, :cond_42

    goto/32 :goto_1c1

    :cond_42
    goto/32 :goto_1bf

    nop

    :goto_218
    goto/16 :goto_47

    :pswitch_7
    goto/32 :goto_122

    nop

    :goto_219
    move-object/from16 v6, p1

    goto/32 :goto_1d9

    nop

    :goto_21a
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_3a

    nop

    :goto_21b
    iget v2, v7, Landroid/app/FragmentManagerImpl;->mCurState:I

    goto/32 :goto_153

    nop

    :goto_21c
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_c3

    nop

    :goto_21d
    if-lt v1, v10, :cond_43

    goto/32 :goto_a

    :cond_43
    goto/32 :goto_20b

    nop

    :goto_21e
    if-eqz v1, :cond_44

    goto/32 :goto_e7

    :cond_44
    goto/32 :goto_e6

    nop

    :goto_21f
    invoke-static {v13, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_220
    goto/32 :goto_7e

    nop

    :goto_221
    iget-object v2, v8, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_13f

    nop

    :goto_222
    invoke-virtual/range {p1 .. p1}, Landroid/app/Fragment;->getAnimatingAway()Landroid/animation/Animator;

    move-result-object v1

    goto/32 :goto_32

    nop

    :goto_223
    invoke-virtual {v1, v2, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    goto/32 :goto_134

    nop

    :goto_224
    iget-object v5, v8, Landroid/app/Fragment;->mContainer:Landroid/view/ViewGroup;

    goto/32 :goto_190

    nop

    :goto_225
    iget-object v1, v8, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_17d

    nop

    :goto_226
    if-nez v0, :cond_45

    goto/32 :goto_137

    :cond_45
    goto/32 :goto_1a

    nop

    :goto_227
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1a6

    nop

    :goto_228
    iget v3, v8, Landroid/app/Fragment;->mContainerId:I

    goto/32 :goto_5d

    nop

    :goto_229
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_1e3

    nop

    :goto_22a
    invoke-virtual/range {p1 .. p1}, Landroid/app/Fragment;->performPause()V

    goto/32 :goto_ab

    nop

    :goto_22b
    if-gt v0, v1, :cond_46

    goto/32 :goto_13

    :cond_46
    goto/32 :goto_d8

    nop

    :goto_22c
    invoke-virtual/range {p1 .. p1}, Landroid/app/Fragment;->performDetach()V

    goto/32 :goto_aa

    nop

    :goto_22d
    const-string v13, "FragmentManager"

    goto/32 :goto_d5

    nop

    :goto_22e
    move/from16 v10, p3

    goto/32 :goto_cd

    nop

    :goto_22f
    if-eqz v2, :cond_47

    goto/32 :goto_29

    :cond_47
    goto/32 :goto_99

    nop

    :goto_230
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_1ed

    nop

    :goto_231
    move/from16 v10, p3

    goto/32 :goto_84

    nop

    :goto_232
    invoke-virtual {v1}, Landroid/animation/Animator;->cancel()V

    :goto_233
    goto/32 :goto_16f

    nop

    :goto_234
    goto/16 :goto_16d

    :catch_0
    move-exception v0

    goto/32 :goto_af

    nop

    :goto_235
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_eb

    nop

    :goto_236
    sget-boolean v1, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    goto/32 :goto_144

    nop

    :goto_237
    invoke-direct {v7, v1, v12}, Landroid/app/FragmentManagerImpl;->setHWLayerAnimListenerIfAlpha(Landroid/view/View;Landroid/animation/Animator;)V

    goto/32 :goto_2d

    nop

    :goto_238
    move v9, v15

    :goto_239
    goto/32 :goto_11d

    nop

    :goto_23a
    invoke-virtual {v7, v8, v15}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentStarted(Landroid/app/Fragment;Z)V

    :goto_23b
    :pswitch_8
    goto/32 :goto_a1

    nop

    :goto_23c
    iput-boolean v15, v8, Landroid/app/Fragment;->mRetaining:Z

    goto/32 :goto_100

    nop

    :goto_23d
    invoke-virtual {v7, v8, v15}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentDestroyed(Landroid/app/Fragment;Z)V

    goto/32 :goto_ba

    nop

    :goto_23e
    iget-object v1, v8, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    goto/32 :goto_1c5

    nop

    :goto_23f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_6a

    nop

    :goto_240
    move-object/from16 v18, v5

    goto/32 :goto_10c

    nop
.end method

.method public noteStateNotSaved()V
    .locals 3

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mSavedNonConfig:Landroid/app/FragmentManagerNonConfig;

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mStateSaved:Z

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Fragment;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/app/Fragment;->noteStateNotSaved()V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 16

    move-object/from16 v6, p0

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    const-string v0, "fragment"

    move-object/from16 v9, p2

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    const-string v0, "class"

    invoke-interface {v8, v1, v0}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/android/internal/R$styleable;->Fragment:[I

    invoke-virtual {v7, v8, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v10

    const/4 v2, 0x0

    if-nez v0, :cond_1

    invoke-virtual {v10, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v11, v0

    goto :goto_0

    :cond_1
    move-object v11, v0

    :goto_0
    const/4 v0, 0x1

    const/4 v3, -0x1

    invoke-virtual {v10, v0, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v12

    const/4 v4, 0x2

    invoke-virtual {v10, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10}, Landroid/content/res/TypedArray;->recycle()V

    if-eqz p1, :cond_2

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v2

    :cond_2
    move v14, v2

    if-ne v14, v3, :cond_4

    if-ne v12, v3, :cond_4

    if-eqz v13, :cond_3

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface/range {p4 .. p4}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Must specify unique android:id, android:tag, or have a parent with an id for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    :goto_1
    if-eq v12, v3, :cond_5

    invoke-virtual {v6, v12}, Landroid/app/FragmentManagerImpl;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v2

    goto :goto_2

    :cond_5
    move-object v2, v1

    :goto_2
    if-nez v2, :cond_6

    if-eqz v13, :cond_6

    invoke-virtual {v6, v13}, Landroid/app/FragmentManagerImpl;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    :cond_6
    if-nez v2, :cond_7

    if-eq v14, v3, :cond_7

    invoke-virtual {v6, v14}, Landroid/app/FragmentManagerImpl;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v2

    :cond_7
    sget-boolean v3, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    if-eqz v3, :cond_8

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onCreateView: id=0x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v12}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " fname="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " existing="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "FragmentManager"

    invoke-static {v4, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    if-nez v2, :cond_a

    iget-object v3, v6, Landroid/app/FragmentManagerImpl;->mContainer:Landroid/app/FragmentContainer;

    invoke-virtual {v3, v7, v11, v1}, Landroid/app/FragmentContainer;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/Fragment;

    move-result-object v2

    iput-boolean v0, v2, Landroid/app/Fragment;->mFromLayout:Z

    if-eqz v12, :cond_9

    move v1, v12

    goto :goto_3

    :cond_9
    move v1, v14

    :goto_3
    iput v1, v2, Landroid/app/Fragment;->mFragmentId:I

    iput v14, v2, Landroid/app/Fragment;->mContainerId:I

    iput-object v13, v2, Landroid/app/Fragment;->mTag:Ljava/lang/String;

    iput-boolean v0, v2, Landroid/app/Fragment;->mInLayout:Z

    iput-object v6, v2, Landroid/app/Fragment;->mFragmentManager:Landroid/app/FragmentManagerImpl;

    iget-object v1, v6, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    iput-object v1, v2, Landroid/app/Fragment;->mHost:Landroid/app/FragmentHostCallback;

    iget-object v1, v6, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    invoke-virtual {v1}, Landroid/app/FragmentHostCallback;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, v2, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    invoke-virtual {v2, v1, v8, v3}, Landroid/app/Fragment;->onInflate(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    invoke-virtual {v6, v2, v0}, Landroid/app/FragmentManagerImpl;->addFragment(Landroid/app/Fragment;Z)V

    move-object v15, v2

    goto :goto_4

    :cond_a
    iget-boolean v1, v2, Landroid/app/Fragment;->mInLayout:Z

    if-nez v1, :cond_10

    iput-boolean v0, v2, Landroid/app/Fragment;->mInLayout:Z

    iget-object v1, v6, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    iput-object v1, v2, Landroid/app/Fragment;->mHost:Landroid/app/FragmentHostCallback;

    iget-boolean v1, v2, Landroid/app/Fragment;->mRetaining:Z

    if-nez v1, :cond_b

    iget-object v1, v6, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    invoke-virtual {v1}, Landroid/app/FragmentHostCallback;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, v2, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    invoke-virtual {v2, v1, v8, v3}, Landroid/app/Fragment;->onInflate(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    :cond_b
    move-object v15, v2

    :goto_4
    iget v1, v6, Landroid/app/FragmentManagerImpl;->mCurState:I

    if-ge v1, v0, :cond_c

    iget-boolean v0, v15, Landroid/app/Fragment;->mFromLayout:Z

    if-eqz v0, :cond_c

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object v1, v15

    invoke-virtual/range {v0 .. v5}, Landroid/app/FragmentManagerImpl;->moveToState(Landroid/app/Fragment;IIIZ)V

    goto :goto_5

    :cond_c
    invoke-virtual {v6, v15}, Landroid/app/FragmentManagerImpl;->moveToState(Landroid/app/Fragment;)V

    :goto_5
    iget-object v0, v15, Landroid/app/Fragment;->mView:Landroid/view/View;

    if-eqz v0, :cond_f

    if-eqz v12, :cond_d

    iget-object v0, v15, Landroid/app/Fragment;->mView:Landroid/view/View;

    invoke-virtual {v0, v12}, Landroid/view/View;->setId(I)V

    :cond_d
    iget-object v0, v15, Landroid/app/Fragment;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_e

    iget-object v0, v15, Landroid/app/Fragment;->mView:Landroid/view/View;

    invoke-virtual {v0, v13}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_e
    iget-object v0, v15, Landroid/app/Fragment;->mView:Landroid/view/View;

    return-object v0

    :cond_f
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not create a view."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_10
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface/range {p4 .. p4}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ": Duplicate id 0x"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v12}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", tag "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", or parent id 0x"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v14}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " with another fragment for "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public performPendingDeferredStart(Landroid/app/Fragment;)V
    .locals 7

    iget-boolean v0, p1, Landroid/app/Fragment;->mDeferStart:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mExecutingActions:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mHavePendingDeferredStart:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p1, Landroid/app/Fragment;->mDeferStart:Z

    iget v3, p0, Landroid/app/FragmentManagerImpl;->mCurState:I

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, Landroid/app/FragmentManagerImpl;->moveToState(Landroid/app/Fragment;IIIZ)V

    :cond_1
    return-void
.end method

.method public popBackStack()V
    .locals 4

    new-instance v0, Landroid/app/FragmentManagerImpl$PopBackStackState;

    const/4 v1, 0x0

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-direct {v0, p0, v1, v2, v3}, Landroid/app/FragmentManagerImpl$PopBackStackState;-><init>(Landroid/app/FragmentManagerImpl;Ljava/lang/String;II)V

    invoke-virtual {p0, v0, v3}, Landroid/app/FragmentManagerImpl;->enqueueAction(Landroid/app/FragmentManagerImpl$OpGenerator;Z)V

    return-void
.end method

.method public popBackStack(II)V
    .locals 3

    if-ltz p1, :cond_0

    new-instance v0, Landroid/app/FragmentManagerImpl$PopBackStackState;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1, p2}, Landroid/app/FragmentManagerImpl$PopBackStackState;-><init>(Landroid/app/FragmentManagerImpl;Ljava/lang/String;II)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/app/FragmentManagerImpl;->enqueueAction(Landroid/app/FragmentManagerImpl$OpGenerator;Z)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public popBackStack(Ljava/lang/String;I)V
    .locals 2

    new-instance v0, Landroid/app/FragmentManagerImpl$PopBackStackState;

    const/4 v1, -0x1

    invoke-direct {v0, p0, p1, v1, p2}, Landroid/app/FragmentManagerImpl$PopBackStackState;-><init>(Landroid/app/FragmentManagerImpl;Ljava/lang/String;II)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/app/FragmentManagerImpl;->enqueueAction(Landroid/app/FragmentManagerImpl$OpGenerator;Z)V

    return-void
.end method

.method public popBackStackImmediate()Z
    .locals 3

    invoke-direct {p0}, Landroid/app/FragmentManagerImpl;->checkStateLoss()V

    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Landroid/app/FragmentManagerImpl;->popBackStackImmediate(Ljava/lang/String;II)Z

    move-result v0

    return v0
.end method

.method public popBackStackImmediate(II)Z
    .locals 3

    invoke-direct {p0}, Landroid/app/FragmentManagerImpl;->checkStateLoss()V

    if-ltz p1, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Landroid/app/FragmentManagerImpl;->popBackStackImmediate(Ljava/lang/String;II)Z

    move-result v0

    return v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public popBackStackImmediate(Ljava/lang/String;I)Z
    .locals 1

    invoke-direct {p0}, Landroid/app/FragmentManagerImpl;->checkStateLoss()V

    const/4 v0, -0x1

    invoke-direct {p0, p1, v0, p2}, Landroid/app/FragmentManagerImpl;->popBackStackImmediate(Ljava/lang/String;II)Z

    move-result v0

    return v0
.end method

.method popBackStackState(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;II)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/app/BackStackRecord;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/lang/String;",
            "II)Z"
        }
    .end annotation

    goto/32 :goto_30

    nop

    :goto_0
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    goto/32 :goto_21

    nop

    :goto_1
    if-gez p4, :cond_0

    goto/32 :goto_59

    :cond_0
    goto/32 :goto_36

    nop

    :goto_2
    invoke-virtual {v0}, Landroid/app/BackStackRecord;->getName()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_51

    nop

    :goto_3
    and-int/lit8 v0, p5, 0x1

    goto/32 :goto_53

    nop

    :goto_4
    goto :goto_e

    :goto_5
    goto/32 :goto_24

    nop

    :goto_6
    goto :goto_1f

    :goto_7
    goto/32 :goto_5b

    nop

    :goto_8
    const/4 v3, -0x1

    goto/32 :goto_55

    nop

    :goto_9
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_15

    nop

    :goto_a
    if-gez p4, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_41

    nop

    :goto_b
    if-ltz v0, :cond_2

    goto/32 :goto_23

    :cond_2
    goto/32 :goto_22

    nop

    :goto_c
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_3b

    nop

    :goto_d
    add-int/lit8 v3, v3, -0x1

    :goto_e
    goto/32 :goto_2c

    nop

    :goto_f
    goto :goto_1c

    :goto_10
    goto/32 :goto_54

    nop

    :goto_11
    if-eq p4, v4, :cond_3

    goto/32 :goto_59

    :cond_3
    goto/32 :goto_58

    nop

    :goto_12
    check-cast v0, Landroid/app/BackStackRecord;

    goto/32 :goto_3e

    nop

    :goto_13
    if-eqz p3, :cond_4

    goto/32 :goto_16

    :cond_4
    goto/32 :goto_27

    nop

    :goto_14
    const/4 v1, 0x0

    goto/32 :goto_35

    nop

    :goto_15
    goto/16 :goto_7

    :goto_16
    goto/32 :goto_8

    nop

    :goto_17
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    goto/32 :goto_48

    nop

    :goto_18
    return v1

    :goto_19
    goto/32 :goto_4d

    nop

    :goto_1a
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto/32 :goto_2b

    nop

    :goto_1b
    move v3, v0

    :goto_1c
    goto/32 :goto_52

    nop

    :goto_1d
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    goto/32 :goto_2a

    nop

    :goto_1e
    sub-int/2addr v0, v2

    :goto_1f
    goto/32 :goto_42

    nop

    :goto_20
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_6

    nop

    :goto_21
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_12

    nop

    :goto_22
    return v1

    :goto_23
    goto/32 :goto_17

    nop

    :goto_24
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    goto/32 :goto_49

    nop

    :goto_25
    if-gez p4, :cond_5

    goto/32 :goto_5

    :cond_5
    :goto_26
    goto/32 :goto_40

    nop

    :goto_27
    if-ltz p4, :cond_6

    goto/32 :goto_16

    :cond_6
    goto/32 :goto_4c

    nop

    :goto_28
    invoke-virtual {p3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    goto/32 :goto_4f

    nop

    :goto_29
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_1e

    nop

    :goto_2a
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_56

    nop

    :goto_2b
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_20

    nop

    :goto_2c
    if-gez v3, :cond_7

    goto/32 :goto_5

    :cond_7
    goto/32 :goto_0

    nop

    :goto_2d
    const/4 v2, 0x1

    goto/32 :goto_13

    nop

    :goto_2e
    sub-int/2addr v0, v2

    goto/32 :goto_47

    nop

    :goto_2f
    if-eqz v3, :cond_8

    goto/32 :goto_16

    :cond_8
    goto/32 :goto_57

    nop

    :goto_30
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    goto/32 :goto_14

    nop

    :goto_31
    return v1

    :goto_32
    goto/32 :goto_3

    nop

    :goto_33
    goto/16 :goto_10

    :goto_34
    goto/32 :goto_1

    nop

    :goto_35
    if-eqz v0, :cond_9

    goto/32 :goto_4b

    :cond_9
    goto/32 :goto_4a

    nop

    :goto_36
    iget v4, v0, Landroid/app/BackStackRecord;->mIndex:I

    goto/32 :goto_11

    nop

    :goto_37
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_1a

    nop

    :goto_38
    add-int/lit8 v3, v3, -0x1

    goto/32 :goto_4

    nop

    :goto_39
    if-nez p3, :cond_a

    goto/32 :goto_34

    :cond_a
    goto/32 :goto_2

    nop

    :goto_3a
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_3d

    nop

    :goto_3b
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto/32 :goto_9

    nop

    :goto_3c
    invoke-virtual {v0}, Landroid/app/BackStackRecord;->getName()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_28

    nop

    :goto_3d
    check-cast v0, Landroid/app/BackStackRecord;

    goto/32 :goto_39

    nop

    :goto_3e
    if-nez p3, :cond_b

    goto/32 :goto_50

    :cond_b
    goto/32 :goto_3c

    nop

    :goto_3f
    if-nez v4, :cond_c

    goto/32 :goto_34

    :cond_c
    goto/32 :goto_33

    nop

    :goto_40
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_4e

    nop

    :goto_41
    iget v4, v0, Landroid/app/BackStackRecord;->mIndex:I

    goto/32 :goto_43

    nop

    :goto_42
    if-gt v0, v3, :cond_d

    goto/32 :goto_7

    :cond_d
    goto/32 :goto_1d

    nop

    :goto_43
    if-eq p4, v4, :cond_e

    goto/32 :goto_5

    :cond_e
    :goto_44
    goto/32 :goto_38

    nop

    :goto_45
    check-cast v1, Landroid/app/BackStackRecord;

    goto/32 :goto_c

    nop

    :goto_46
    sub-int/2addr v0, v2

    goto/32 :goto_b

    nop

    :goto_47
    if-eq v3, v0, :cond_f

    goto/32 :goto_19

    :cond_f
    goto/32 :goto_18

    nop

    :goto_48
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_45

    nop

    :goto_49
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_2e

    nop

    :goto_4a
    return v1

    :goto_4b
    goto/32 :goto_2d

    nop

    :goto_4c
    and-int/lit8 v3, p5, 0x1

    goto/32 :goto_2f

    nop

    :goto_4d
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    goto/32 :goto_29

    nop

    :goto_4e
    sub-int/2addr v0, v2

    goto/32 :goto_1b

    nop

    :goto_4f
    if-eqz v4, :cond_10

    goto/32 :goto_44

    :cond_10
    :goto_50
    goto/32 :goto_a

    nop

    :goto_51
    invoke-virtual {p3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    goto/32 :goto_3f

    nop

    :goto_52
    if-gez v3, :cond_11

    goto/32 :goto_10

    :cond_11
    goto/32 :goto_5c

    nop

    :goto_53
    if-nez v0, :cond_12

    goto/32 :goto_5

    :cond_12
    goto/32 :goto_d

    nop

    :goto_54
    if-ltz v3, :cond_13

    goto/32 :goto_32

    :cond_13
    goto/32 :goto_31

    nop

    :goto_55
    if-eqz p3, :cond_14

    goto/32 :goto_26

    :cond_14
    goto/32 :goto_25

    nop

    :goto_56
    check-cast v1, Landroid/app/BackStackRecord;

    goto/32 :goto_37

    nop

    :goto_57
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_46

    nop

    :goto_58
    goto/16 :goto_10

    :goto_59
    nop

    goto/32 :goto_5a

    nop

    :goto_5a
    add-int/lit8 v3, v3, -0x1

    goto/32 :goto_f

    nop

    :goto_5b
    return v2

    :goto_5c
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    goto/32 :goto_3a

    nop
.end method

.method public putFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/app/Fragment;)V
    .locals 3

    iget v0, p3, Landroid/app/Fragment;->mIndex:I

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not currently in the FragmentManager"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Landroid/app/FragmentManagerImpl;->throwException(Ljava/lang/RuntimeException;)V

    :cond_0
    iget v0, p3, Landroid/app/Fragment;->mIndex:I

    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public registerFragmentLifecycleCallbacks(Landroid/app/FragmentManager$FragmentLifecycleCallbacks;Z)V
    .locals 3

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mLifecycleCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Landroid/util/Pair;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public removeFragment(Landroid/app/Fragment;)V
    .locals 4

    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "remove: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " nesting="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/app/Fragment;->mBackStackNesting:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p1}, Landroid/app/Fragment;->isInBackStack()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    iget-boolean v2, p1, Landroid/app/Fragment;->mDetached:Z

    if-eqz v2, :cond_1

    if-eqz v0, :cond_3

    :cond_1
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-boolean v2, p1, Landroid/app/Fragment;->mHasMenu:Z

    if-eqz v2, :cond_2

    iget-boolean v2, p1, Landroid/app/Fragment;->mMenuVisible:Z

    if-eqz v2, :cond_2

    iput-boolean v1, p0, Landroid/app/FragmentManagerImpl;->mNeedMenuInvalidate:Z

    :cond_2
    const/4 v2, 0x0

    iput-boolean v2, p1, Landroid/app/Fragment;->mAdded:Z

    iput-boolean v1, p1, Landroid/app/Fragment;->mRemoving:Z

    :cond_3
    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public removeOnBackStackChangedListener(Landroid/app/FragmentManager$OnBackStackChangedListener;)V
    .locals 1

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStackChangeListeners:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method reportBackStackChanged()V
    .locals 2

    goto/32 :goto_8

    nop

    :goto_0
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mBackStackChangeListeners:Ljava/util/ArrayList;

    goto/32 :goto_7

    nop

    :goto_1
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mBackStackChangeListeners:Ljava/util/ArrayList;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_6

    nop

    :goto_3
    goto :goto_d

    :goto_4
    goto/32 :goto_5

    nop

    :goto_5
    return-void

    :goto_6
    if-lt v0, v1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_0

    nop

    :goto_7
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_a

    nop

    :goto_8
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStackChangeListeners:Ljava/util/ArrayList;

    goto/32 :goto_b

    nop

    :goto_9
    invoke-interface {v1}, Landroid/app/FragmentManager$OnBackStackChangedListener;->onBackStackChanged()V

    goto/32 :goto_e

    nop

    :goto_a
    check-cast v1, Landroid/app/FragmentManager$OnBackStackChangedListener;

    goto/32 :goto_9

    nop

    :goto_b
    if-nez v0, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_c

    nop

    :goto_c
    const/4 v0, 0x0

    :goto_d
    goto/32 :goto_1

    nop

    :goto_e
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_3

    nop
.end method

.method restoreAllState(Landroid/os/Parcelable;Landroid/app/FragmentManagerNonConfig;)V
    .locals 12

    goto/32 :goto_ed

    nop

    :goto_0
    if-gez v8, :cond_0

    goto/32 :goto_a6

    :cond_0
    goto/32 :goto_116

    nop

    :goto_1
    goto/16 :goto_53

    :goto_2
    goto/32 :goto_52

    nop

    :goto_3
    if-gez v2, :cond_1

    goto/32 :goto_11a

    :cond_1
    goto/32 :goto_59

    nop

    :goto_4
    iget-object v8, v7, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    goto/32 :goto_df

    nop

    :goto_5
    const-string v10, " target no longer exists: "

    goto/32 :goto_84

    nop

    :goto_6
    aget v8, v8, v4

    goto/32 :goto_97

    nop

    :goto_7
    add-int/lit8 v6, v6, 0x1

    goto/32 :goto_80

    nop

    :goto_8
    if-nez v4, :cond_2

    goto/32 :goto_23

    :cond_2
    goto/32 :goto_3a

    nop

    :goto_9
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto/32 :goto_5e

    nop

    :goto_a
    invoke-virtual {v11}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v11

    goto/32 :goto_4e

    nop

    :goto_b
    iget-object v11, p0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    goto/32 :goto_f7

    nop

    :goto_c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_a4

    nop

    :goto_d
    move-object v0, p1

    goto/32 :goto_c8

    nop

    :goto_e
    array-length v4, v4

    goto/32 :goto_124

    nop

    :goto_f
    if-nez v5, :cond_3

    goto/32 :goto_7c

    :cond_3
    goto/32 :goto_a7

    nop

    :goto_10
    iput-object v10, v7, Landroid/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    goto/32 :goto_123

    nop

    :goto_11
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_12
    goto/32 :goto_10e

    nop

    :goto_13
    iput-object v2, v5, Landroid/app/FragmentState;->mInstance:Landroid/app/Fragment;

    :goto_14
    goto/32 :goto_15

    nop

    :goto_15
    add-int/lit8 v4, v4, 0x1

    goto/32 :goto_e5

    nop

    :goto_16
    invoke-virtual {v8, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    goto/32 :goto_20

    nop

    :goto_17
    if-lt v6, v5, :cond_4

    goto/32 :goto_f1

    :cond_4
    goto/32 :goto_1f

    nop

    :goto_18
    invoke-direct {v5, v6, v7}, Landroid/util/LogWriter;-><init>(ILjava/lang/String;)V

    goto/32 :goto_2d

    nop

    :goto_19
    check-cast v7, Landroid/app/Fragment;

    goto/32 :goto_e1

    nop

    :goto_1a
    const/4 v4, 0x0

    :goto_1b
    goto/32 :goto_d6

    nop

    :goto_1c
    iget-object v4, v0, Landroid/app/FragmentManagerState;->mAdded:[I

    goto/32 :goto_73

    nop

    :goto_1d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    goto/32 :goto_99

    nop

    :goto_1e
    if-eqz v5, :cond_5

    goto/32 :goto_c6

    :cond_5
    goto/32 :goto_c0

    nop

    :goto_1f
    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    goto/32 :goto_19

    nop

    :goto_20
    check-cast v8, Landroid/app/Fragment;

    goto/32 :goto_25

    nop

    :goto_21
    iget-object v8, v0, Landroid/app/FragmentManagerState;->mAdded:[I

    goto/32 :goto_6

    nop

    :goto_22
    goto/16 :goto_5b

    :goto_23
    goto/32 :goto_5a

    nop

    :goto_24
    sget-boolean v8, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    goto/32 :goto_62

    nop

    :goto_25
    iput-object v8, v7, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    goto/32 :goto_4

    nop

    :goto_26
    const-string v11, "android:view_state"

    goto/32 :goto_d2

    nop

    :goto_27
    invoke-virtual {v8, v9, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto/32 :goto_13

    nop

    :goto_28
    if-nez v4, :cond_6

    goto/32 :goto_2

    :cond_6
    goto/32 :goto_41

    nop

    :goto_29
    iget-object v8, p0, Landroid/app/FragmentManagerImpl;->mContainer:Landroid/app/FragmentContainer;

    goto/32 :goto_32

    nop

    :goto_2a
    iput-object v4, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    goto/32 :goto_1a

    nop

    :goto_2b
    const/4 v3, 0x0

    goto/32 :goto_ae

    nop

    :goto_2c
    iget-object v5, v0, Landroid/app/FragmentManagerState;->mAdded:[I

    goto/32 :goto_f9

    nop

    :goto_2d
    new-instance v6, Lcom/android/internal/util/FastPrintWriter;

    goto/32 :goto_54

    nop

    :goto_2e
    iget-object v9, v0, Landroid/app/FragmentManagerState;->mActive:[Landroid/app/FragmentState;

    goto/32 :goto_105

    nop

    :goto_2f
    goto/16 :goto_48

    :goto_30
    goto/32 :goto_22

    nop

    :goto_31
    check-cast v2, Landroid/app/Fragment;

    goto/32 :goto_119

    nop

    :goto_32
    iget-object v9, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    goto/32 :goto_7d

    nop

    :goto_33
    aget-object v9, v9, v8

    goto/32 :goto_82

    nop

    :goto_34
    check-cast v5, Landroid/app/Fragment;

    goto/32 :goto_1e

    nop

    :goto_35
    new-instance v7, Ljava/lang/StringBuilder;

    goto/32 :goto_aa

    nop

    :goto_36
    const/4 v4, 0x0

    :goto_37
    goto/32 :goto_2c

    nop

    :goto_38
    new-instance v6, Ljava/lang/StringBuilder;

    goto/32 :goto_c

    nop

    :goto_39
    sget-boolean v5, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    goto/32 :goto_f

    nop

    :goto_3a
    new-instance v2, Ljava/util/ArrayList;

    goto/32 :goto_87

    nop

    :goto_3b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_7f

    nop

    :goto_3c
    iget-object v9, v0, Landroid/app/FragmentManagerState;->mActive:[Landroid/app/FragmentState;

    goto/32 :goto_33

    nop

    :goto_3d
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    goto/32 :goto_1c

    nop

    :goto_3e
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    goto/32 :goto_ec

    nop

    :goto_3f
    if-ne v9, v10, :cond_7

    goto/32 :goto_b0

    :cond_7
    goto/32 :goto_e3

    nop

    :goto_40
    if-lt v4, v7, :cond_8

    goto/32 :goto_107

    :cond_8
    goto/32 :goto_e4

    nop

    :goto_41
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    goto/32 :goto_1

    nop

    :goto_42
    iget-object v4, v0, Landroid/app/FragmentManagerState;->mBackStack:[Landroid/app/BackStackState;

    goto/32 :goto_e

    nop

    :goto_43
    iget-object v1, v0, Landroid/app/FragmentManagerState;->mActive:[Landroid/app/FragmentState;

    goto/32 :goto_11c

    nop

    :goto_44
    iget-object v10, v9, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_26

    nop

    :goto_45
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_a3

    nop

    :goto_46
    iget-object v8, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    goto/32 :goto_e2

    nop

    :goto_47
    const/4 v2, 0x0

    :goto_48
    goto/32 :goto_42

    nop

    :goto_49
    if-eq v8, v9, :cond_9

    goto/32 :goto_d8

    :cond_9
    goto/32 :goto_66

    nop

    :goto_4a
    iput-object v2, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    goto/32 :goto_47

    nop

    :goto_4b
    new-instance v9, Ljava/lang/StringBuilder;

    goto/32 :goto_45

    nop

    :goto_4c
    iget-object v10, v9, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_b

    nop

    :goto_4d
    if-eqz v6, :cond_a

    goto/32 :goto_11e

    :cond_a
    goto/32 :goto_b9

    nop

    :goto_4e
    invoke-virtual {v10, v11}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    goto/32 :goto_44

    nop

    :goto_4f
    invoke-virtual {p2}, Landroid/app/FragmentManagerNonConfig;->getChildNonConfigs()Ljava/util/List;

    move-result-object v1

    goto/32 :goto_28

    nop

    :goto_50
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_103

    nop

    :goto_51
    move-object v6, v7

    goto/32 :goto_106

    nop

    :goto_52
    move v5, v3

    :goto_53
    goto/32 :goto_d0

    nop

    :goto_54
    const/16 v7, 0x400

    goto/32 :goto_b1

    nop

    :goto_55
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto/32 :goto_11

    nop

    :goto_56
    check-cast v7, Landroid/app/Fragment;

    goto/32 :goto_58

    nop

    :goto_57
    if-nez v10, :cond_b

    goto/32 :goto_95

    :cond_b
    goto/32 :goto_4c

    nop

    :goto_58
    iget v8, v7, Landroid/app/Fragment;->mTargetIndex:I

    goto/32 :goto_0

    nop

    :goto_59
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    goto/32 :goto_8c

    nop

    :goto_5a
    iput-object v2, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    :goto_5b
    goto/32 :goto_ca

    nop

    :goto_5c
    return-void

    :goto_5d
    goto/32 :goto_d

    nop

    :goto_5e
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_5f
    goto/32 :goto_46

    nop

    :goto_60
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    goto/32 :goto_9

    nop

    :goto_61
    invoke-virtual {p2}, Landroid/app/FragmentManagerNonConfig;->getFragments()Ljava/util/List;

    move-result-object v4

    goto/32 :goto_4f

    nop

    :goto_62
    if-nez v8, :cond_c

    goto/32 :goto_5f

    :cond_c
    goto/32 :goto_de

    nop

    :goto_63
    move v5, v3

    :goto_64
    goto/32 :goto_126

    nop

    :goto_65
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_a5

    nop

    :goto_66
    new-instance v9, Ljava/lang/IllegalStateException;

    goto/32 :goto_e0

    nop

    :goto_67
    iget-object v4, v0, Landroid/app/FragmentManagerState;->mBackStack:[Landroid/app/BackStackState;

    goto/32 :goto_8

    nop

    :goto_68
    array-length v9, v9

    goto/32 :goto_49

    nop

    :goto_69
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_7a

    nop

    :goto_6a
    iput-object v2, v7, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    goto/32 :goto_6d

    nop

    :goto_6b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/32 :goto_e8

    nop

    :goto_6c
    array-length v5, v5

    goto/32 :goto_121

    nop

    :goto_6d
    iget-object v10, v9, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_57

    nop

    :goto_6e
    if-nez v8, :cond_d

    goto/32 :goto_12

    :cond_d
    goto/32 :goto_f3

    nop

    :goto_6f
    const-string v7, "FragmentManager"

    goto/32 :goto_18

    nop

    :goto_70
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    goto/32 :goto_a9

    nop

    :goto_71
    iget-object v7, p0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    goto/32 :goto_29

    nop

    :goto_72
    sget-boolean v6, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    goto/32 :goto_bd

    nop

    :goto_73
    if-nez v4, :cond_e

    goto/32 :goto_d5

    :cond_e
    goto/32 :goto_36

    nop

    :goto_74
    const-string v7, " (index "

    goto/32 :goto_3b

    nop

    :goto_75
    iput-object v7, v9, Landroid/app/FragmentState;->mInstance:Landroid/app/Fragment;

    goto/32 :goto_cd

    nop

    :goto_76
    invoke-virtual {v4, v7, v6, v3}, Landroid/app/BackStackRecord;->dump(Ljava/lang/String;Ljava/io/PrintWriter;Z)V

    goto/32 :goto_7b

    nop

    :goto_77
    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    goto/32 :goto_4a

    nop

    :goto_78
    goto/16 :goto_64

    :goto_79
    goto/32 :goto_63

    nop

    :goto_7a
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_74

    nop

    :goto_7b
    invoke-virtual {v6}, Ljava/io/PrintWriter;->flush()V

    :goto_7c
    goto/32 :goto_108

    nop

    :goto_7d
    invoke-virtual {v5, v7, v8, v9, v6}, Landroid/app/FragmentState;->instantiate(Landroid/app/FragmentHostCallback;Landroid/app/FragmentContainer;Landroid/app/Fragment;Landroid/app/FragmentManagerNonConfig;)Landroid/app/Fragment;

    move-result-object v7

    goto/32 :goto_24

    nop

    :goto_7e
    new-instance v9, Ljava/lang/StringBuilder;

    goto/32 :goto_b4

    nop

    :goto_7f
    iget v7, v4, Landroid/app/BackStackRecord;->mIndex:I

    goto/32 :goto_10b

    nop

    :goto_80
    goto/16 :goto_127

    :goto_81
    goto/32 :goto_ce

    nop

    :goto_82
    iget v9, v9, Landroid/app/FragmentState;->mIndex:I

    goto/32 :goto_db

    nop

    :goto_83
    new-instance v4, Landroid/util/SparseArray;

    goto/32 :goto_eb

    nop

    :goto_84
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    goto/32 :goto_b2

    nop

    :goto_85
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto/32 :goto_88

    nop

    :goto_86
    iget v5, v4, Landroid/app/BackStackRecord;->mIndex:I

    goto/32 :goto_8d

    nop

    :goto_87
    iget-object v4, v0, Landroid/app/FragmentManagerState;->mBackStack:[Landroid/app/BackStackState;

    goto/32 :goto_b3

    nop

    :goto_88
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_89
    goto/32 :goto_3e

    nop

    :goto_8a
    array-length v9, v9

    goto/32 :goto_115

    nop

    :goto_8b
    const-string v7, "  "

    goto/32 :goto_76

    nop

    :goto_8c
    iget v3, v0, Landroid/app/FragmentManagerState;->mPrimaryNavActiveIndex:I

    goto/32 :goto_e9

    nop

    :goto_8d
    if-gez v5, :cond_f

    goto/32 :goto_f5

    :cond_f
    goto/32 :goto_be

    nop

    :goto_8e
    iget-object v9, v0, Landroid/app/FragmentManagerState;->mActive:[Landroid/app/FragmentState;

    goto/32 :goto_68

    nop

    :goto_8f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    goto/32 :goto_ab

    nop

    :goto_90
    return-void

    :goto_91
    goto/32 :goto_ba

    nop

    :goto_92
    iget v2, v0, Landroid/app/FragmentManagerState;->mNextFragmentIndex:I

    goto/32 :goto_100

    nop

    :goto_93
    const-string v7, "): "

    goto/32 :goto_128

    nop

    :goto_94
    iput-object v10, v7, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    :goto_95
    goto/32 :goto_125

    nop

    :goto_96
    iput-boolean v3, v7, Landroid/app/Fragment;->mAdded:Z

    goto/32 :goto_6a

    nop

    :goto_97
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    goto/32 :goto_ff

    nop

    :goto_98
    iget-object v9, v0, Landroid/app/FragmentManagerState;->mActive:[Landroid/app/FragmentState;

    goto/32 :goto_8a

    nop

    :goto_99
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/32 :goto_cf

    nop

    :goto_9a
    const-string/jumbo v10, "restoreAllState: re-attaching retained "

    goto/32 :goto_ea

    nop

    :goto_9b
    const-string/jumbo v8, "restoreAllState: added #"

    goto/32 :goto_a8

    nop

    :goto_9c
    if-nez v5, :cond_10

    goto/32 :goto_14

    :cond_10
    goto/32 :goto_ee

    nop

    :goto_9d
    invoke-virtual {v5, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    goto/32 :goto_34

    nop

    :goto_9e
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    goto/32 :goto_78

    nop

    :goto_9f
    iget-object v4, v0, Landroid/app/FragmentManagerState;->mBackStack:[Landroid/app/BackStackState;

    goto/32 :goto_ad

    nop

    :goto_a0
    if-lt v6, v5, :cond_11

    goto/32 :goto_81

    :cond_11
    goto/32 :goto_c1

    nop

    :goto_a1
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_9b

    nop

    :goto_a2
    if-nez v4, :cond_12

    goto/32 :goto_79

    :cond_12
    goto/32 :goto_9e

    nop

    :goto_a3
    const-string v10, "Re-attaching retained fragment "

    goto/32 :goto_70

    nop

    :goto_a4
    const-string/jumbo v7, "restoreAllState: back stack #"

    goto/32 :goto_69

    nop

    :goto_a5
    iput-object v2, v7, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    :goto_a6
    goto/32 :goto_7

    nop

    :goto_a7
    const-string v5, "FragmentManager"

    goto/32 :goto_38

    nop

    :goto_a8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    goto/32 :goto_109

    nop

    :goto_a9
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    goto/32 :goto_5

    nop

    :goto_aa
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_d9

    nop

    :goto_ab
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    goto/32 :goto_c4

    nop

    :goto_ac
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    goto/32 :goto_55

    nop

    :goto_ad
    aget-object v4, v4, v2

    goto/32 :goto_110

    nop

    :goto_ae
    if-nez p2, :cond_13

    goto/32 :goto_f1

    :cond_13
    goto/32 :goto_61

    nop

    :goto_af
    goto/16 :goto_10f

    :goto_b0
    goto/32 :goto_8e

    nop

    :goto_b1
    invoke-direct {v6, v5, v3, v7}, Lcom/android/internal/util/FastPrintWriter;-><init>(Ljava/io/Writer;ZI)V

    goto/32 :goto_8b

    nop

    :goto_b2
    iget v10, v7, Landroid/app/Fragment;->mTargetIndex:I

    goto/32 :goto_10a

    nop

    :goto_b3
    array-length v4, v4

    goto/32 :goto_77

    nop

    :goto_b4
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_9a

    nop

    :goto_b5
    const-string/jumbo v10, "restoreAllState: active #"

    goto/32 :goto_8f

    nop

    :goto_b6
    iget-object v5, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    goto/32 :goto_fe

    nop

    :goto_b7
    const-string v8, "FragmentManager"

    goto/32 :goto_4b

    nop

    :goto_b8
    const/4 v6, 0x1

    goto/32 :goto_117

    nop

    :goto_b9
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    goto/32 :goto_dd

    nop

    :goto_ba
    const/4 v1, 0x0

    goto/32 :goto_bf

    nop

    :goto_bb
    array-length v5, v5

    goto/32 :goto_bc

    nop

    :goto_bc
    invoke-direct {v4, v5}, Landroid/util/SparseArray;-><init>(I)V

    goto/32 :goto_2a

    nop

    :goto_bd
    if-nez v6, :cond_14

    goto/32 :goto_89

    :cond_14
    goto/32 :goto_c3

    nop

    :goto_be
    iget v5, v4, Landroid/app/BackStackRecord;->mIndex:I

    goto/32 :goto_f4

    nop

    :goto_bf
    const/4 v2, 0x0

    goto/32 :goto_2b

    nop

    :goto_c0
    new-instance v6, Ljava/lang/IllegalStateException;

    goto/32 :goto_35

    nop

    :goto_c1
    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    goto/32 :goto_56

    nop

    :goto_c2
    aget-object v5, v5, v4

    goto/32 :goto_9c

    nop

    :goto_c3
    const-string v6, "FragmentManager"

    goto/32 :goto_122

    nop

    :goto_c4
    const-string v10, ": "

    goto/32 :goto_101

    nop

    :goto_c5
    invoke-direct {p0, v6}, Landroid/app/FragmentManagerImpl;->throwException(Ljava/lang/RuntimeException;)V

    :goto_c6
    goto/32 :goto_b8

    nop

    :goto_c7
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto/32 :goto_65

    nop

    :goto_c8
    check-cast v0, Landroid/app/FragmentManagerState;

    goto/32 :goto_43

    nop

    :goto_c9
    if-lt v4, v5, :cond_15

    goto/32 :goto_d5

    :cond_15
    goto/32 :goto_b6

    nop

    :goto_ca
    iget v2, v0, Landroid/app/FragmentManagerState;->mPrimaryNavActiveIndex:I

    goto/32 :goto_3

    nop

    :goto_cb
    if-nez p2, :cond_16

    goto/32 :goto_81

    :cond_16
    goto/32 :goto_129

    nop

    :goto_cc
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    goto/32 :goto_104

    nop

    :goto_cd
    iput-object v2, v7, Landroid/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    goto/32 :goto_10d

    nop

    :goto_ce
    iget-object v4, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    goto/32 :goto_3d

    nop

    :goto_cf
    invoke-direct {v9, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_d7

    nop

    :goto_d0
    const/4 v6, 0x0

    :goto_d1
    goto/32 :goto_17

    nop

    :goto_d2
    invoke-virtual {v10, v11}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v10

    goto/32 :goto_10

    nop

    :goto_d3
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    goto/32 :goto_21

    nop

    :goto_d4
    throw v2

    :goto_d5
    goto/32 :goto_67

    nop

    :goto_d6
    iget-object v5, v0, Landroid/app/FragmentManagerState;->mActive:[Landroid/app/FragmentState;

    goto/32 :goto_6c

    nop

    :goto_d7
    invoke-direct {p0, v9}, Landroid/app/FragmentManagerImpl;->throwException(Ljava/lang/RuntimeException;)V

    :goto_d8
    goto/32 :goto_2e

    nop

    :goto_d9
    const-string v8, "No instantiated fragment for index #"

    goto/32 :goto_d3

    nop

    :goto_da
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    goto/32 :goto_40

    nop

    :goto_db
    iget v10, v7, Landroid/app/Fragment;->mIndex:I

    goto/32 :goto_3f

    nop

    :goto_dc
    return-void

    :goto_dd
    monitor-enter v6

    :try_start_0
    iget-object v7, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v6

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_37

    :catchall_0
    move-exception v2

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_11d

    nop

    :goto_de
    const-string v8, "FragmentManager"

    goto/32 :goto_112

    nop

    :goto_df
    if-eqz v8, :cond_17

    goto/32 :goto_a6

    :cond_17
    goto/32 :goto_b7

    nop

    :goto_e0
    new-instance v10, Ljava/lang/StringBuilder;

    goto/32 :goto_50

    nop

    :goto_e1
    sget-boolean v8, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    goto/32 :goto_6e

    nop

    :goto_e2
    iget v9, v7, Landroid/app/Fragment;->mIndex:I

    goto/32 :goto_27

    nop

    :goto_e3
    add-int/lit8 v8, v8, 0x1

    goto/32 :goto_af

    nop

    :goto_e4
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    goto/32 :goto_51

    nop

    :goto_e5
    goto/16 :goto_1b

    :goto_e6
    goto/32 :goto_cb

    nop

    :goto_e7
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_86

    nop

    :goto_e8
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_10c

    nop

    :goto_e9
    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_31

    nop

    :goto_ea
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    goto/32 :goto_ac

    nop

    :goto_eb
    iget-object v5, v0, Landroid/app/FragmentManagerState;->mActive:[Landroid/app/FragmentState;

    goto/32 :goto_bb

    nop

    :goto_ec
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    goto/32 :goto_4d

    nop

    :goto_ed
    if-eqz p1, :cond_18

    goto/32 :goto_5d

    :cond_18
    goto/32 :goto_5c

    nop

    :goto_ee
    const/4 v6, 0x0

    goto/32 :goto_102

    nop

    :goto_ef
    iput-boolean v3, v7, Landroid/app/Fragment;->mInLayout:Z

    goto/32 :goto_96

    nop

    :goto_f0
    goto/16 :goto_d1

    :goto_f1
    goto/32 :goto_83

    nop

    :goto_f2
    iget v11, v7, Landroid/app/Fragment;->mIndex:I

    goto/32 :goto_1d

    nop

    :goto_f3
    const-string v8, "FragmentManager"

    goto/32 :goto_7e

    nop

    :goto_f4
    invoke-virtual {p0, v5, v4}, Landroid/app/FragmentManagerImpl;->setBackStackIndex(ILandroid/app/BackStackRecord;)V

    :goto_f5
    goto/32 :goto_f8

    nop

    :goto_f6
    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_c5

    nop

    :goto_f7
    invoke-virtual {v11}, Landroid/app/FragmentHostCallback;->getContext()Landroid/content/Context;

    move-result-object v11

    goto/32 :goto_a

    nop

    :goto_f8
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_2f

    nop

    :goto_f9
    array-length v5, v5

    goto/32 :goto_c9

    nop

    :goto_fa
    const-string v8, ": "

    goto/32 :goto_cc

    nop

    :goto_fb
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    goto/32 :goto_f2

    nop

    :goto_fc
    iget-object v5, v0, Landroid/app/FragmentManagerState;->mActive:[Landroid/app/FragmentState;

    goto/32 :goto_c2

    nop

    :goto_fd
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_d4

    nop

    :goto_fe
    iget-object v6, v0, Landroid/app/FragmentManagerState;->mAdded:[I

    goto/32 :goto_114

    nop

    :goto_ff
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto/32 :goto_f6

    nop

    :goto_100
    iput v2, p0, Landroid/app/FragmentManagerImpl;->mNextFragmentIndex:I

    goto/32 :goto_dc

    nop

    :goto_101
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    goto/32 :goto_60

    nop

    :goto_102
    if-nez v1, :cond_19

    goto/32 :goto_107

    :cond_19
    goto/32 :goto_da

    nop

    :goto_103
    const-string v11, "Could not find active fragment with index "

    goto/32 :goto_fb

    nop

    :goto_104
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    goto/32 :goto_85

    nop

    :goto_105
    aget-object v9, v9, v8

    goto/32 :goto_75

    nop

    :goto_106
    check-cast v6, Landroid/app/FragmentManagerNonConfig;

    :goto_107
    goto/32 :goto_71

    nop

    :goto_108
    iget-object v5, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    goto/32 :goto_e7

    nop

    :goto_109
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    goto/32 :goto_fa

    nop

    :goto_10a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    goto/32 :goto_c7

    nop

    :goto_10b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_93

    nop

    :goto_10c
    new-instance v5, Landroid/util/LogWriter;

    goto/32 :goto_111

    nop

    :goto_10d
    iput v3, v7, Landroid/app/Fragment;->mBackStackNesting:I

    goto/32 :goto_ef

    nop

    :goto_10e
    const/4 v8, 0x0

    :goto_10f
    goto/32 :goto_98

    nop

    :goto_110
    invoke-virtual {v4, p0}, Landroid/app/BackStackState;->instantiate(Landroid/app/FragmentManagerImpl;)Landroid/app/BackStackRecord;

    move-result-object v4

    goto/32 :goto_39

    nop

    :goto_111
    const/4 v6, 0x2

    goto/32 :goto_6f

    nop

    :goto_112
    new-instance v9, Ljava/lang/StringBuilder;

    goto/32 :goto_113

    nop

    :goto_113
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_b5

    nop

    :goto_114
    aget v6, v6, v4

    goto/32 :goto_9d

    nop

    :goto_115
    if-lt v8, v9, :cond_1a

    goto/32 :goto_b0

    :cond_1a
    goto/32 :goto_3c

    nop

    :goto_116
    iget-object v8, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    goto/32 :goto_118

    nop

    :goto_117
    iput-boolean v6, v5, Landroid/app/Fragment;->mAdded:Z

    goto/32 :goto_72

    nop

    :goto_118
    iget v9, v7, Landroid/app/Fragment;->mTargetIndex:I

    goto/32 :goto_16

    nop

    :goto_119
    iput-object v2, p0, Landroid/app/FragmentManagerImpl;->mPrimaryNav:Landroid/app/Fragment;

    :goto_11a
    goto/32 :goto_92

    nop

    :goto_11b
    const-string v3, "Already added!"

    goto/32 :goto_fd

    nop

    :goto_11c
    if-eqz v1, :cond_1b

    goto/32 :goto_91

    :cond_1b
    goto/32 :goto_90

    nop

    :goto_11d
    throw v2

    :goto_11e
    goto/32 :goto_120

    nop

    :goto_11f
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_6b

    nop

    :goto_120
    new-instance v2, Ljava/lang/IllegalStateException;

    goto/32 :goto_11b

    nop

    :goto_121
    if-lt v4, v5, :cond_1c

    goto/32 :goto_e6

    :cond_1c
    goto/32 :goto_fc

    nop

    :goto_122
    new-instance v7, Ljava/lang/StringBuilder;

    goto/32 :goto_a1

    nop

    :goto_123
    iget-object v10, v9, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_94

    nop

    :goto_124
    if-lt v2, v4, :cond_1d

    goto/32 :goto_30

    :cond_1d
    goto/32 :goto_9f

    nop

    :goto_125
    add-int/lit8 v6, v6, 0x1

    goto/32 :goto_f0

    nop

    :goto_126
    const/4 v6, 0x0

    :goto_127
    goto/32 :goto_a0

    nop

    :goto_128
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_11f

    nop

    :goto_129
    invoke-virtual {p2}, Landroid/app/FragmentManagerNonConfig;->getFragments()Ljava/util/List;

    move-result-object v4

    goto/32 :goto_a2

    nop
.end method

.method retainNonConfig()Landroid/app/FragmentManagerNonConfig;
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mSavedNonConfig:Landroid/app/FragmentManagerNonConfig;

    goto/32 :goto_0

    nop

    :goto_2
    invoke-static {v0}, Landroid/app/FragmentManagerImpl;->setRetaining(Landroid/app/FragmentManagerNonConfig;)V

    goto/32 :goto_1

    nop

    :goto_3
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mSavedNonConfig:Landroid/app/FragmentManagerNonConfig;

    goto/32 :goto_2

    nop
.end method

.method saveAllState()Landroid/os/Parcelable;
    .locals 13

    goto/32 :goto_a0

    nop

    :goto_0
    iget-object v6, v5, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_2a

    nop

    :goto_1
    invoke-direct {v5, v9}, Landroid/app/FragmentState;-><init>(Landroid/app/Fragment;)V

    goto/32 :goto_a6

    nop

    :goto_2
    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_92

    nop

    :goto_3
    if-ltz v6, :cond_0

    goto/32 :goto_5c

    :cond_0
    goto/32 :goto_c5

    nop

    :goto_4
    goto/16 :goto_c4

    :goto_5
    goto/32 :goto_3b

    nop

    :goto_6
    iget-object v9, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    goto/32 :goto_7c

    nop

    :goto_7
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_4f

    nop

    :goto_8
    sget-boolean v4, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    goto/32 :goto_52

    nop

    :goto_9
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    goto/32 :goto_98

    nop

    :goto_a
    new-instance v6, Ljava/lang/StringBuilder;

    goto/32 :goto_47

    nop

    :goto_b
    const/4 v3, 0x0

    goto/32 :goto_c9

    nop

    :goto_c
    iget-object v9, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    goto/32 :goto_72

    nop

    :goto_d
    const-string/jumbo v9, "saveAllState: adding back stack #"

    goto/32 :goto_29

    nop

    :goto_e
    iget v6, v9, Landroid/app/Fragment;->mState:I

    goto/32 :goto_94

    nop

    :goto_f
    invoke-virtual {v12, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    goto/32 :goto_30

    nop

    :goto_10
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1a

    nop

    :goto_11
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_31

    nop

    :goto_12
    iget v6, v6, Landroid/app/Fragment;->mIndex:I

    goto/32 :goto_3

    nop

    :goto_13
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_c1

    nop

    :goto_14
    iget-object v5, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    goto/32 :goto_7f

    nop

    :goto_15
    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    goto/32 :goto_76

    nop

    :goto_16
    if-eqz v3, :cond_1

    goto/32 :goto_cf

    :cond_1
    goto/32 :goto_8

    nop

    :goto_17
    new-array v2, v1, [Landroid/app/FragmentState;

    goto/32 :goto_b

    nop

    :goto_18
    iget-object v6, v5, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_73

    nop

    :goto_19
    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    goto/32 :goto_a4

    nop

    :goto_1a
    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    goto/32 :goto_40

    nop

    :goto_1b
    if-lt v4, v1, :cond_2

    goto/32 :goto_60

    :cond_2
    goto/32 :goto_6

    nop

    :goto_1c
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    goto/32 :goto_c6

    nop

    :goto_1d
    aput-object v6, v4, v5

    goto/32 :goto_b2

    nop

    :goto_1e
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_83

    nop

    :goto_1f
    if-eqz v6, :cond_3

    goto/32 :goto_5

    :cond_3
    goto/32 :goto_9f

    nop

    :goto_20
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    goto/32 :goto_17

    nop

    :goto_21
    check-cast v9, Landroid/app/BackStackRecord;

    goto/32 :goto_59

    nop

    :goto_22
    invoke-static {v8, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_23
    goto/32 :goto_4d

    nop

    :goto_24
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    goto/32 :goto_20

    nop

    :goto_25
    new-instance v10, Ljava/lang/StringBuilder;

    goto/32 :goto_6f

    nop

    :goto_26
    new-instance v10, Ljava/lang/StringBuilder;

    goto/32 :goto_84

    nop

    :goto_27
    new-instance v6, Landroid/app/BackStackState;

    goto/32 :goto_c

    nop

    :goto_28
    check-cast v9, Landroid/app/Fragment;

    goto/32 :goto_85

    nop

    :goto_29
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_bb

    nop

    :goto_2a
    iget v10, v9, Landroid/app/Fragment;->mTargetRequestCode:I

    goto/32 :goto_61

    nop

    :goto_2b
    if-nez v1, :cond_4

    goto/32 :goto_97

    :cond_4
    goto/32 :goto_bd

    nop

    :goto_2c
    new-instance v5, Landroid/app/FragmentManagerState;

    goto/32 :goto_3d

    nop

    :goto_2d
    add-int/lit8 v4, v4, 0x1

    goto/32 :goto_5f

    nop

    :goto_2e
    if-nez v6, :cond_5

    goto/32 :goto_58

    :cond_5
    goto/32 :goto_ad

    nop

    :goto_2f
    invoke-virtual {p0, v6, v11, v10}, Landroid/app/FragmentManagerImpl;->putFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/app/Fragment;)V

    goto/32 :goto_64

    nop

    :goto_30
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    goto/32 :goto_15

    nop

    :goto_31
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/32 :goto_a9

    nop

    :goto_32
    new-instance v5, Landroid/app/FragmentState;

    goto/32 :goto_1

    nop

    :goto_33
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    goto/32 :goto_3a

    nop

    :goto_34
    const-string v10, "Saved state of "

    goto/32 :goto_86

    nop

    :goto_35
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    goto/32 :goto_91

    nop

    :goto_36
    invoke-direct {v10, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_99

    nop

    :goto_37
    iget v6, p0, Landroid/app/FragmentManagerImpl;->mNextFragmentIndex:I

    goto/32 :goto_77

    nop

    :goto_38
    iput-object v2, v5, Landroid/app/FragmentManagerState;->mActive:[Landroid/app/FragmentState;

    goto/32 :goto_81

    nop

    :goto_39
    if-eqz v6, :cond_6

    goto/32 :goto_a5

    :cond_6
    goto/32 :goto_cb

    nop

    :goto_3a
    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    goto/32 :goto_1c

    nop

    :goto_3b
    iget-object v6, v9, Landroid/app/Fragment;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_c3

    nop

    :goto_3c
    const/4 v0, 0x0

    goto/32 :goto_95

    nop

    :goto_3d
    invoke-direct {v5}, Landroid/app/FragmentManagerState;-><init>()V

    goto/32 :goto_38

    nop

    :goto_3e
    invoke-virtual {v11, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    goto/32 :goto_7a

    nop

    :goto_3f
    const-string v8, "FragmentManager"

    goto/32 :goto_1b

    nop

    :goto_40
    iget-object v12, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    goto/32 :goto_f

    nop

    :goto_41
    if-nez v6, :cond_7

    goto/32 :goto_aa

    :cond_7
    goto/32 :goto_a

    nop

    :goto_42
    iget v6, v9, Landroid/app/Fragment;->mIndex:I

    goto/32 :goto_cd

    nop

    :goto_43
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    goto/32 :goto_2b

    nop

    :goto_44
    iget v6, v6, Landroid/app/Fragment;->mIndex:I

    goto/32 :goto_9c

    nop

    :goto_45
    const-string/jumbo v4, "saveAllState: no fragments!"

    goto/32 :goto_55

    nop

    :goto_46
    const-string v11, " has target not in fragment manager: "

    goto/32 :goto_9

    nop

    :goto_47
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_34

    nop

    :goto_48
    invoke-direct {v6, v10}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_5b

    nop

    :goto_49
    iget-object v9, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    goto/32 :goto_1e

    nop

    :goto_4a
    iput-object v4, v5, Landroid/app/FragmentManagerState;->mBackStack:[Landroid/app/BackStackState;

    goto/32 :goto_37

    nop

    :goto_4b
    goto/16 :goto_97

    :goto_4c
    goto/32 :goto_24

    nop

    :goto_4d
    add-int/lit8 v9, v9, 0x1

    goto/32 :goto_8e

    nop

    :goto_4e
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/32 :goto_22

    nop

    :goto_4f
    goto/16 :goto_6b

    :goto_50
    goto/32 :goto_2c

    nop

    :goto_51
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    goto/32 :goto_b6

    nop

    :goto_52
    if-nez v4, :cond_8

    goto/32 :goto_56

    :cond_8
    goto/32 :goto_45

    nop

    :goto_53
    const/4 v9, 0x0

    :goto_54
    goto/32 :goto_68

    nop

    :goto_55
    invoke-static {v8, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_56
    goto/32 :goto_ce

    nop

    :goto_57
    invoke-static {v8, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_58
    goto/32 :goto_7

    nop

    :goto_59
    invoke-direct {v6, p0, v9}, Landroid/app/BackStackState;-><init>(Landroid/app/FragmentManagerImpl;Landroid/app/BackStackRecord;)V

    goto/32 :goto_1d

    nop

    :goto_5a
    if-lez v1, :cond_9

    goto/32 :goto_4c

    :cond_9
    goto/32 :goto_4b

    nop

    :goto_5b
    invoke-direct {p0, v6}, Landroid/app/FragmentManagerImpl;->throwException(Ljava/lang/RuntimeException;)V

    :goto_5c
    goto/32 :goto_ab

    nop

    :goto_5d
    invoke-virtual {p0}, Landroid/app/FragmentManagerImpl;->execPendingActions()Z

    goto/32 :goto_ba

    nop

    :goto_5e
    if-lt v5, v1, :cond_a

    goto/32 :goto_50

    :cond_a
    goto/32 :goto_27

    nop

    :goto_5f
    goto/16 :goto_ca

    :goto_60
    goto/32 :goto_16

    nop

    :goto_61
    const-string v11, "android:target_req_state"

    goto/32 :goto_d0

    nop

    :goto_62
    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    goto/32 :goto_13

    nop

    :goto_63
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/32 :goto_42

    nop

    :goto_64
    iget v6, v9, Landroid/app/Fragment;->mTargetRequestCode:I

    goto/32 :goto_b0

    nop

    :goto_65
    new-array v0, v1, [I

    goto/32 :goto_53

    nop

    :goto_66
    new-instance v10, Ljava/lang/IllegalStateException;

    goto/32 :goto_af

    nop

    :goto_67
    iget-object v10, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    goto/32 :goto_35

    nop

    :goto_68
    if-lt v9, v1, :cond_b

    goto/32 :goto_8f

    :cond_b
    goto/32 :goto_67

    nop

    :goto_69
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_2

    nop

    :goto_6a
    const/4 v5, 0x0

    :goto_6b
    goto/32 :goto_5e

    nop

    :goto_6c
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_c8

    nop

    :goto_6d
    const/4 v0, 0x0

    goto/32 :goto_7b

    nop

    :goto_6e
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    goto/32 :goto_cc

    nop

    :goto_6f
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_a7

    nop

    :goto_70
    if-ltz v10, :cond_c

    goto/32 :goto_9a

    :cond_c
    goto/32 :goto_a3

    nop

    :goto_71
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_36

    nop

    :goto_72
    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    goto/32 :goto_21

    nop

    :goto_73
    iget-object v10, v9, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    goto/32 :goto_b8

    nop

    :goto_74
    invoke-direct {p0, v10}, Landroid/app/FragmentManagerImpl;->throwException(Ljava/lang/RuntimeException;)V

    :goto_75
    goto/32 :goto_9e

    nop

    :goto_76
    aget v12, v0, v9

    goto/32 :goto_82

    nop

    :goto_77
    iput v6, v5, Landroid/app/FragmentManagerState;->mNextFragmentIndex:I

    goto/32 :goto_ac

    nop

    :goto_78
    invoke-direct {v10, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_74

    nop

    :goto_79
    const-string v11, "Failure saving state: "

    goto/32 :goto_6e

    nop

    :goto_7a
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    goto/32 :goto_4e

    nop

    :goto_7b
    const/4 v4, 0x0

    goto/32 :goto_49

    nop

    :goto_7c
    invoke-virtual {v9, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v9

    goto/32 :goto_28

    nop

    :goto_7d
    invoke-virtual {p0}, Landroid/app/FragmentManagerImpl;->saveNonConfig()V

    goto/32 :goto_96

    nop

    :goto_7e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_ae

    nop

    :goto_7f
    if-nez v5, :cond_d

    goto/32 :goto_50

    :cond_d
    goto/32 :goto_6c

    nop

    :goto_80
    iget-object v6, v5, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_1f

    nop

    :goto_81
    iput-object v0, v5, Landroid/app/FragmentManagerState;->mAdded:[I

    goto/32 :goto_4a

    nop

    :goto_82
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    goto/32 :goto_b9

    nop

    :goto_83
    if-gtz v1, :cond_e

    goto/32 :goto_8f

    :cond_e
    goto/32 :goto_65

    nop

    :goto_84
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_79

    nop

    :goto_85
    if-nez v9, :cond_f

    goto/32 :goto_aa

    :cond_f
    goto/32 :goto_bf

    nop

    :goto_86
    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_8d

    nop

    :goto_87
    if-nez v6, :cond_10

    goto/32 :goto_c4

    :cond_10
    goto/32 :goto_b5

    nop

    :goto_88
    iget v10, v10, Landroid/app/Fragment;->mIndex:I

    goto/32 :goto_b4

    nop

    :goto_89
    const-string v6, "Failure saving state: active "

    goto/32 :goto_a8

    nop

    :goto_8a
    new-instance v11, Ljava/lang/StringBuilder;

    goto/32 :goto_69

    nop

    :goto_8b
    sget-boolean v6, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    goto/32 :goto_41

    nop

    :goto_8c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_c7

    nop

    :goto_8d
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_7e

    nop

    :goto_8e
    goto/16 :goto_54

    :goto_8f
    goto/32 :goto_14

    nop

    :goto_90
    aget v10, v0, v9

    goto/32 :goto_b1

    nop

    :goto_91
    check-cast v10, Landroid/app/Fragment;

    goto/32 :goto_88

    nop

    :goto_92
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_63

    nop

    :goto_93
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_d

    nop

    :goto_94
    if-gtz v6, :cond_11

    goto/32 :goto_5

    :cond_11
    goto/32 :goto_80

    nop

    :goto_95
    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mSavedNonConfig:Landroid/app/FragmentManagerNonConfig;

    goto/32 :goto_43

    nop

    :goto_96
    return-object v5

    :goto_97
    goto/32 :goto_a2

    nop

    :goto_98
    iget-object v11, v9, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    goto/32 :goto_51

    nop

    :goto_99
    invoke-direct {p0, v10}, Landroid/app/FragmentManagerImpl;->throwException(Ljava/lang/RuntimeException;)V

    :goto_9a
    goto/32 :goto_be

    nop

    :goto_9b
    if-nez v6, :cond_12

    goto/32 :goto_9d

    :cond_12
    goto/32 :goto_44

    nop

    :goto_9c
    iput v6, v5, Landroid/app/FragmentManagerState;->mPrimaryNavActiveIndex:I

    :goto_9d
    goto/32 :goto_7d

    nop

    :goto_9e
    sget-boolean v10, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    goto/32 :goto_d1

    nop

    :goto_9f
    invoke-virtual {p0, v9}, Landroid/app/FragmentManagerImpl;->saveFragmentBasicState(Landroid/app/Fragment;)Landroid/os/Bundle;

    move-result-object v6

    goto/32 :goto_c0

    nop

    :goto_a0
    invoke-direct {p0}, Landroid/app/FragmentManagerImpl;->forcePostponedTransactions()V

    goto/32 :goto_bc

    nop

    :goto_a1
    iput-boolean v0, p0, Landroid/app/FragmentManagerImpl;->mStateSaved:Z

    goto/32 :goto_3c

    nop

    :goto_a2
    return-object v0

    :goto_a3
    new-instance v10, Ljava/lang/IllegalStateException;

    goto/32 :goto_8a

    nop

    :goto_a4
    iput-object v6, v5, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    :goto_a5
    goto/32 :goto_18

    nop

    :goto_a6
    aput-object v5, v2, v4

    goto/32 :goto_e

    nop

    :goto_a7
    const-string/jumbo v11, "saveAllState: adding fragment #"

    goto/32 :goto_33

    nop

    :goto_a8
    const-string v7, ": "

    goto/32 :goto_3f

    nop

    :goto_a9
    invoke-static {v8, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_aa
    goto/32 :goto_2d

    nop

    :goto_ab
    iget-object v6, v5, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_39

    nop

    :goto_ac
    iget-object v6, p0, Landroid/app/FragmentManagerImpl;->mPrimaryNav:Landroid/app/Fragment;

    goto/32 :goto_9b

    nop

    :goto_ad
    new-instance v6, Ljava/lang/StringBuilder;

    goto/32 :goto_93

    nop

    :goto_ae
    iget-object v7, v5, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_11

    nop

    :goto_af
    new-instance v11, Ljava/lang/StringBuilder;

    goto/32 :goto_10

    nop

    :goto_b0
    if-nez v6, :cond_13

    goto/32 :goto_c4

    :cond_13
    goto/32 :goto_0

    nop

    :goto_b1
    if-ltz v10, :cond_14

    goto/32 :goto_75

    :cond_14
    goto/32 :goto_66

    nop

    :goto_b2
    sget-boolean v6, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    goto/32 :goto_2e

    nop

    :goto_b3
    new-array v4, v1, [Landroid/app/BackStackState;

    goto/32 :goto_6a

    nop

    :goto_b4
    aput v10, v0, v9

    goto/32 :goto_90

    nop

    :goto_b5
    iget-object v6, v9, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    goto/32 :goto_12

    nop

    :goto_b6
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/32 :goto_48

    nop

    :goto_b7
    iget-object v6, v9, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    goto/32 :goto_87

    nop

    :goto_b8
    const-string v11, "android:target_state"

    goto/32 :goto_2f

    nop

    :goto_b9
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto/32 :goto_78

    nop

    :goto_ba
    const/4 v0, 0x1

    goto/32 :goto_a1

    nop

    :goto_bb
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    goto/32 :goto_8c

    nop

    :goto_bc
    invoke-direct {p0}, Landroid/app/FragmentManagerImpl;->endAnimatingAwayFragments()V

    goto/32 :goto_5d

    nop

    :goto_bd
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    goto/32 :goto_5a

    nop

    :goto_be
    const/4 v3, 0x1

    goto/32 :goto_32

    nop

    :goto_bf
    iget v10, v9, Landroid/app/Fragment;->mIndex:I

    goto/32 :goto_70

    nop

    :goto_c0
    iput-object v6, v5, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    goto/32 :goto_b7

    nop

    :goto_c1
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/32 :goto_57

    nop

    :goto_c2
    const-string v5, " has cleared index: "

    goto/32 :goto_89

    nop

    :goto_c3
    iput-object v6, v5, Landroid/app/FragmentState;->mSavedFragmentState:Landroid/os/Bundle;

    :goto_c4
    goto/32 :goto_8b

    nop

    :goto_c5
    new-instance v6, Ljava/lang/IllegalStateException;

    goto/32 :goto_26

    nop

    :goto_c6
    iget-object v11, p0, Landroid/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;

    goto/32 :goto_3e

    nop

    :goto_c7
    iget-object v9, p0, Landroid/app/FragmentManagerImpl;->mBackStack:Ljava/util/ArrayList;

    goto/32 :goto_62

    nop

    :goto_c8
    if-gtz v1, :cond_15

    goto/32 :goto_50

    :cond_15
    goto/32 :goto_b3

    nop

    :goto_c9
    const/4 v4, 0x0

    :goto_ca
    goto/32 :goto_c2

    nop

    :goto_cb
    new-instance v6, Landroid/os/Bundle;

    goto/32 :goto_19

    nop

    :goto_cc
    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    goto/32 :goto_46

    nop

    :goto_cd
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/32 :goto_71

    nop

    :goto_ce
    return-object v0

    :goto_cf
    goto/32 :goto_6d

    nop

    :goto_d0
    invoke-virtual {v6, v11, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/32 :goto_4

    nop

    :goto_d1
    if-nez v10, :cond_16

    goto/32 :goto_23

    :cond_16
    goto/32 :goto_25

    nop
.end method

.method saveFragmentBasicState(Landroid/app/Fragment;)Landroid/os/Bundle;
    .locals 3

    goto/32 :goto_2a

    nop

    :goto_0
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    :goto_1
    goto/32 :goto_25

    nop

    :goto_2
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    goto/32 :goto_1a

    nop

    :goto_3
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mStateBundle:Landroid/os/Bundle;

    goto/32 :goto_b

    nop

    :goto_4
    iget-object v1, p1, Landroid/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    goto/32 :goto_f

    nop

    :goto_5
    invoke-virtual {p0, p1}, Landroid/app/FragmentManagerImpl;->saveFragmentViewState(Landroid/app/Fragment;)V

    :goto_6
    goto/32 :goto_4

    nop

    :goto_7
    return-object v0

    :goto_8
    new-instance v1, Landroid/os/Bundle;

    goto/32 :goto_2

    nop

    :goto_9
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    goto/32 :goto_2b

    nop

    :goto_a
    iget-object v1, p1, Landroid/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    goto/32 :goto_23

    nop

    :goto_b
    if-eqz v1, :cond_0

    goto/32 :goto_2c

    :cond_0
    goto/32 :goto_26

    nop

    :goto_c
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    goto/32 :goto_27

    nop

    :goto_d
    iput-object v1, p0, Landroid/app/FragmentManagerImpl;->mStateBundle:Landroid/os/Bundle;

    :goto_e
    goto/32 :goto_17

    nop

    :goto_f
    if-nez v1, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_2d

    nop

    :goto_10
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mStateBundle:Landroid/os/Bundle;

    goto/32 :goto_11

    nop

    :goto_11
    invoke-virtual {p1, v1}, Landroid/app/Fragment;->performSaveInstanceState(Landroid/os/Bundle;)V

    goto/32 :goto_18

    nop

    :goto_12
    if-eqz v0, :cond_2

    goto/32 :goto_28

    :cond_2
    goto/32 :goto_13

    nop

    :goto_13
    new-instance v1, Landroid/os/Bundle;

    goto/32 :goto_c

    nop

    :goto_14
    if-eqz v1, :cond_3

    goto/32 :goto_e

    :cond_3
    goto/32 :goto_22

    nop

    :goto_15
    invoke-virtual {p0, p1, v1, v2}, Landroid/app/FragmentManagerImpl;->dispatchOnFragmentSaveInstanceState(Landroid/app/Fragment;Landroid/os/Bundle;Z)V

    goto/32 :goto_16

    nop

    :goto_16
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mStateBundle:Landroid/os/Bundle;

    goto/32 :goto_24

    nop

    :goto_17
    iget-object v1, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_19

    nop

    :goto_18
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mStateBundle:Landroid/os/Bundle;

    goto/32 :goto_1e

    nop

    :goto_19
    if-nez v1, :cond_4

    goto/32 :goto_6

    :cond_4
    goto/32 :goto_5

    nop

    :goto_1a
    move-object v0, v1

    :goto_1b
    goto/32 :goto_a

    nop

    :goto_1c
    if-eqz v1, :cond_5

    goto/32 :goto_20

    :cond_5
    goto/32 :goto_12

    nop

    :goto_1d
    iget-boolean v1, p1, Landroid/app/Fragment;->mUserVisibleHint:Z

    goto/32 :goto_21

    nop

    :goto_1e
    const/4 v2, 0x0

    goto/32 :goto_15

    nop

    :goto_1f
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :goto_20
    goto/32 :goto_7

    nop

    :goto_21
    const-string v2, "android:user_visible_hint"

    goto/32 :goto_1f

    nop

    :goto_22
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mStateBundle:Landroid/os/Bundle;

    goto/32 :goto_29

    nop

    :goto_23
    const-string v2, "android:view_state"

    goto/32 :goto_0

    nop

    :goto_24
    invoke-virtual {v1}, Landroid/os/Bundle;->isEmpty()Z

    move-result v1

    goto/32 :goto_14

    nop

    :goto_25
    iget-boolean v1, p1, Landroid/app/Fragment;->mUserVisibleHint:Z

    goto/32 :goto_1c

    nop

    :goto_26
    new-instance v1, Landroid/os/Bundle;

    goto/32 :goto_9

    nop

    :goto_27
    move-object v0, v1

    :goto_28
    goto/32 :goto_1d

    nop

    :goto_29
    const/4 v1, 0x0

    goto/32 :goto_d

    nop

    :goto_2a
    const/4 v0, 0x0

    goto/32 :goto_3

    nop

    :goto_2b
    iput-object v1, p0, Landroid/app/FragmentManagerImpl;->mStateBundle:Landroid/os/Bundle;

    :goto_2c
    goto/32 :goto_10

    nop

    :goto_2d
    if-eqz v0, :cond_6

    goto/32 :goto_1b

    :cond_6
    goto/32 :goto_8

    nop
.end method

.method public saveFragmentInstanceState(Landroid/app/Fragment;)Landroid/app/Fragment$SavedState;
    .locals 3

    iget v0, p1, Landroid/app/Fragment;->mIndex:I

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not currently in the FragmentManager"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Landroid/app/FragmentManagerImpl;->throwException(Ljava/lang/RuntimeException;)V

    :cond_0
    iget v0, p1, Landroid/app/Fragment;->mState:I

    const/4 v1, 0x0

    if-lez v0, :cond_2

    invoke-virtual {p0, p1}, Landroid/app/FragmentManagerImpl;->saveFragmentBasicState(Landroid/app/Fragment;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Landroid/app/Fragment$SavedState;

    invoke-direct {v1, v0}, Landroid/app/Fragment$SavedState;-><init>(Landroid/os/Bundle;)V

    :cond_1
    return-object v1

    :cond_2
    return-object v1
.end method

.method saveFragmentViewState(Landroid/app/Fragment;)V
    .locals 2

    goto/32 :goto_14

    nop

    :goto_0
    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    :goto_1
    goto/32 :goto_10

    nop

    :goto_2
    if-eqz v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_13

    nop

    :goto_3
    goto :goto_1

    :goto_4
    goto/32 :goto_0

    nop

    :goto_5
    iput-object v0, p1, Landroid/app/Fragment;->mSavedViewState:Landroid/util/SparseArray;

    goto/32 :goto_18

    nop

    :goto_6
    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mStateArray:Landroid/util/SparseArray;

    :goto_7
    goto/32 :goto_8

    nop

    :goto_8
    return-void

    :goto_9
    return-void

    :goto_a
    goto/32 :goto_11

    nop

    :goto_b
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    goto/32 :goto_15

    nop

    :goto_c
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    goto/32 :goto_16

    nop

    :goto_d
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mStateArray:Landroid/util/SparseArray;

    goto/32 :goto_c

    nop

    :goto_e
    if-eqz v0, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_9

    nop

    :goto_f
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mStateArray:Landroid/util/SparseArray;

    goto/32 :goto_17

    nop

    :goto_10
    iget-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_f

    nop

    :goto_11
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mStateArray:Landroid/util/SparseArray;

    goto/32 :goto_2

    nop

    :goto_12
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mStateArray:Landroid/util/SparseArray;

    goto/32 :goto_5

    nop

    :goto_13
    new-instance v0, Landroid/util/SparseArray;

    goto/32 :goto_b

    nop

    :goto_14
    iget-object v0, p1, Landroid/app/Fragment;->mView:Landroid/view/View;

    goto/32 :goto_e

    nop

    :goto_15
    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mStateArray:Landroid/util/SparseArray;

    goto/32 :goto_3

    nop

    :goto_16
    if-gtz v0, :cond_2

    goto/32 :goto_7

    :cond_2
    goto/32 :goto_12

    nop

    :goto_17
    invoke-virtual {v0, v1}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    goto/32 :goto_d

    nop

    :goto_18
    const/4 v0, 0x0

    goto/32 :goto_6

    nop
.end method

.method saveNonConfig()V
    .locals 8

    goto/32 :goto_21

    nop

    :goto_0
    invoke-virtual {v4, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_31

    nop

    :goto_1
    iget-boolean v5, v4, Landroid/app/Fragment;->mRetainInstance:Z

    goto/32 :goto_2e

    nop

    :goto_2
    if-lt v2, v4, :cond_0

    goto/32 :goto_19

    :cond_0
    goto/32 :goto_7

    nop

    :goto_3
    const/4 v2, 0x0

    :goto_4
    goto/32 :goto_20

    nop

    :goto_5
    goto/16 :goto_4e

    :goto_6
    goto/32 :goto_4d

    nop

    :goto_7
    iget-object v4, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    goto/32 :goto_0

    nop

    :goto_8
    invoke-static {v6, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_9
    goto/32 :goto_10

    nop

    :goto_a
    goto :goto_14

    :goto_b
    goto/32 :goto_28

    nop

    :goto_c
    iget-object v5, v4, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_4c

    nop

    :goto_d
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_41

    nop

    :goto_e
    invoke-virtual {v7}, Landroid/util/SparseArray;->size()I

    move-result v7

    goto/32 :goto_15

    nop

    :goto_f
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    goto/32 :goto_3e

    nop

    :goto_10
    iget-object v5, v4, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_1e

    nop

    :goto_11
    goto/16 :goto_44

    :goto_12
    goto/32 :goto_43

    nop

    :goto_13
    const/4 v6, 0x0

    :goto_14
    goto/32 :goto_4b

    nop

    :goto_15
    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(I)V

    goto/32 :goto_1d

    nop

    :goto_16
    if-nez v5, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_49

    nop

    :goto_17
    return-void

    :goto_18
    goto :goto_4

    :goto_19
    goto/32 :goto_4a

    nop

    :goto_1a
    const-string v6, "FragmentManager"

    goto/32 :goto_8

    nop

    :goto_1b
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_47

    nop

    :goto_1c
    sget-boolean v5, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    goto/32 :goto_4f

    nop

    :goto_1d
    move-object v1, v6

    goto/32 :goto_13

    nop

    :goto_1e
    if-nez v5, :cond_2

    goto/32 :goto_6

    :cond_2
    goto/32 :goto_32

    nop

    :goto_1f
    invoke-virtual {v5}, Landroid/app/FragmentManagerImpl;->saveNonConfig()V

    goto/32 :goto_c

    nop

    :goto_20
    iget-object v4, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    goto/32 :goto_40

    nop

    :goto_21
    const/4 v0, 0x0

    goto/32 :goto_42

    nop

    :goto_22
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_1a

    nop

    :goto_23
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/32 :goto_22

    nop

    :goto_24
    invoke-direct {v2, v0, v1}, Landroid/app/FragmentManagerNonConfig;-><init>(Ljava/util/List;Ljava/util/List;)V

    goto/32 :goto_35

    nop

    :goto_25
    goto :goto_36

    :goto_26
    goto/32 :goto_2c

    nop

    :goto_27
    if-nez v4, :cond_3

    goto/32 :goto_3c

    :cond_3
    goto/32 :goto_1

    nop

    :goto_28
    if-nez v1, :cond_4

    goto/32 :goto_3c

    :cond_4
    goto/32 :goto_3b

    nop

    :goto_29
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_45

    nop

    :goto_2a
    if-eqz v1, :cond_5

    goto/32 :goto_b

    :cond_5
    goto/32 :goto_16

    nop

    :goto_2b
    if-nez v2, :cond_6

    goto/32 :goto_19

    :cond_6
    goto/32 :goto_3

    nop

    :goto_2c
    new-instance v2, Landroid/app/FragmentManagerNonConfig;

    goto/32 :goto_24

    nop

    :goto_2d
    const-string/jumbo v6, "retainNonConfig: keeping retained "

    goto/32 :goto_3d

    nop

    :goto_2e
    if-nez v5, :cond_7

    goto/32 :goto_9

    :cond_7
    goto/32 :goto_39

    nop

    :goto_2f
    iget v5, v5, Landroid/app/Fragment;->mIndex:I

    goto/32 :goto_11

    nop

    :goto_30
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_2d

    nop

    :goto_31
    check-cast v4, Landroid/app/Fragment;

    goto/32 :goto_27

    nop

    :goto_32
    iget-object v5, v4, Landroid/app/Fragment;->mChildFragmentManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_1f

    nop

    :goto_33
    iput-object v3, p0, Landroid/app/FragmentManagerImpl;->mSavedNonConfig:Landroid/app/FragmentManagerNonConfig;

    goto/32 :goto_25

    nop

    :goto_34
    iget-object v5, v4, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    goto/32 :goto_2f

    nop

    :goto_35
    iput-object v2, p0, Landroid/app/FragmentManagerImpl;->mSavedNonConfig:Landroid/app/FragmentManagerNonConfig;

    :goto_36
    goto/32 :goto_17

    nop

    :goto_37
    iput v5, v4, Landroid/app/Fragment;->mTargetIndex:I

    goto/32 :goto_1c

    nop

    :goto_38
    new-instance v5, Ljava/lang/StringBuilder;

    goto/32 :goto_30

    nop

    :goto_39
    if-eqz v0, :cond_8

    goto/32 :goto_46

    :cond_8
    goto/32 :goto_51

    nop

    :goto_3a
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_18

    nop

    :goto_3b
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_3c
    goto/32 :goto_3a

    nop

    :goto_3d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/32 :goto_23

    nop

    :goto_3e
    const/4 v3, 0x0

    goto/32 :goto_2b

    nop

    :goto_3f
    iget-object v7, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    goto/32 :goto_e

    nop

    :goto_40
    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v4

    goto/32 :goto_2

    nop

    :goto_41
    iget-object v5, v4, Landroid/app/Fragment;->mTarget:Landroid/app/Fragment;

    goto/32 :goto_50

    nop

    :goto_42
    const/4 v1, 0x0

    goto/32 :goto_f

    nop

    :goto_43
    const/4 v5, -0x1

    :goto_44
    goto/32 :goto_37

    nop

    :goto_45
    move-object v0, v5

    :goto_46
    goto/32 :goto_d

    nop

    :goto_47
    add-int/lit8 v6, v6, 0x1

    goto/32 :goto_a

    nop

    :goto_48
    if-eqz v1, :cond_9

    goto/32 :goto_26

    :cond_9
    goto/32 :goto_33

    nop

    :goto_49
    new-instance v6, Ljava/util/ArrayList;

    goto/32 :goto_3f

    nop

    :goto_4a
    if-eqz v0, :cond_a

    goto/32 :goto_26

    :cond_a
    goto/32 :goto_48

    nop

    :goto_4b
    if-lt v6, v2, :cond_b

    goto/32 :goto_b

    :cond_b
    goto/32 :goto_1b

    nop

    :goto_4c
    iget-object v5, v5, Landroid/app/FragmentManagerImpl;->mSavedNonConfig:Landroid/app/FragmentManagerNonConfig;

    goto/32 :goto_5

    nop

    :goto_4d
    iget-object v5, v4, Landroid/app/Fragment;->mChildNonConfig:Landroid/app/FragmentManagerNonConfig;

    :goto_4e
    goto/32 :goto_2a

    nop

    :goto_4f
    if-nez v5, :cond_c

    goto/32 :goto_9

    :cond_c
    goto/32 :goto_38

    nop

    :goto_50
    if-nez v5, :cond_d

    goto/32 :goto_12

    :cond_d
    goto/32 :goto_34

    nop

    :goto_51
    new-instance v5, Ljava/util/ArrayList;

    goto/32 :goto_29

    nop
.end method

.method public setBackStackIndex(ILandroid/app/BackStackRecord;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_2

    sget-boolean v1, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    if-eqz v1, :cond_1

    const-string v1, "FragmentManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Setting back stack index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    invoke-virtual {v1, p1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    :goto_0
    if-ge v0, p1, :cond_5

    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAvailBackStackIndices:Ljava/util/ArrayList;

    if-nez v1, :cond_3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Landroid/app/FragmentManagerImpl;->mAvailBackStackIndices:Ljava/util/ArrayList;

    :cond_3
    sget-boolean v1, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    if-eqz v1, :cond_4

    const-string v1, "FragmentManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adding available back stack index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mAvailBackStackIndices:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    sget-boolean v1, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    if-eqz v1, :cond_6

    const-string v1, "FragmentManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adding back stack index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mBackStackIndices:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setPrimaryNavigationFragment(Landroid/app/Fragment;)V
    .locals 3

    if-eqz p1, :cond_1

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    iget v1, p1, Landroid/app/Fragment;->mIndex:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    iget-object v0, p1, Landroid/app/Fragment;->mHost:Landroid/app/FragmentHostCallback;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    if-ne v0, p0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not an active fragment of FragmentManager "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    iput-object p1, p0, Landroid/app/FragmentManagerImpl;->mPrimaryNav:Landroid/app/Fragment;

    return-void
.end method

.method public showFragment(Landroid/app/Fragment;)V
    .locals 2

    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "show: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FragmentManager"

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p1, Landroid/app/Fragment;->mHidden:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p1, Landroid/app/Fragment;->mHidden:Z

    iget-boolean v0, p1, Landroid/app/Fragment;->mHiddenChanged:Z

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p1, Landroid/app/Fragment;->mHiddenChanged:Z

    :cond_1
    return-void
.end method

.method startPendingDeferredFragments()V
    .locals 2

    goto/32 :goto_10

    nop

    :goto_0
    return-void

    :goto_1
    check-cast v1, Landroid/app/Fragment;

    goto/32 :goto_2

    nop

    :goto_2
    if-nez v1, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_8

    nop

    :goto_3
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    goto/32 :goto_11

    nop

    :goto_4
    goto :goto_e

    :goto_5
    goto/32 :goto_0

    nop

    :goto_6
    return-void

    :goto_7
    goto/32 :goto_d

    nop

    :goto_8
    invoke-virtual {p0, v1}, Landroid/app/FragmentManagerImpl;->performPendingDeferredStart(Landroid/app/Fragment;)V

    :goto_9
    goto/32 :goto_12

    nop

    :goto_a
    if-lt v0, v1, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_3

    nop

    :goto_b
    if-eqz v0, :cond_2

    goto/32 :goto_7

    :cond_2
    goto/32 :goto_6

    nop

    :goto_c
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    goto/32 :goto_a

    nop

    :goto_d
    const/4 v0, 0x0

    :goto_e
    goto/32 :goto_f

    nop

    :goto_f
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    goto/32 :goto_c

    nop

    :goto_10
    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mActive:Landroid/util/SparseArray;

    goto/32 :goto_b

    nop

    :goto_11
    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_1

    nop

    :goto_12
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_4

    nop
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "FragmentManager{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mParent:Landroid/app/Fragment;

    if-eqz v1, :cond_0

    invoke-static {v1, v0}, Landroid/util/DebugUtils;->buildShortClassTag(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    invoke-static {v1, v0}, Landroid/util/DebugUtils;->buildShortClassTag(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    :goto_0
    const-string/jumbo v1, "}}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public unregisterFragmentLifecycleCallbacks(Landroid/app/FragmentManager$FragmentLifecycleCallbacks;)V
    .locals 4

    iget-object v0, p0, Landroid/app/FragmentManagerImpl;->mLifecycleCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Landroid/app/FragmentManagerImpl;->mLifecycleCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v2

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mLifecycleCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    iget-object v3, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-ne v3, p1, :cond_0

    iget-object v3, p0, Landroid/app/FragmentManagerImpl;->mLifecycleCallbacks:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
