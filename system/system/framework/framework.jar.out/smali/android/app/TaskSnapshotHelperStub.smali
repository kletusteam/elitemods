.class public interface abstract Landroid/app/TaskSnapshotHelperStub;
.super Ljava/lang/Object;


# direct methods
.method public static get()Landroid/app/TaskSnapshotHelperStub;
    .locals 1

    const-class v0, Landroid/app/TaskSnapshotHelperStub;

    invoke-static {v0}, Lcom/miui/base/MiuiStubUtil;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/TaskSnapshotHelperStub;

    return-object v0
.end method


# virtual methods
.method public checkExpired(Ljava/lang/String;JJ)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public delayTime(Ljava/lang/String;)J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public destroyQS(I)V
    .locals 0

    return-void
.end method

.method public ensureEnable(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public ensureEnable(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public ensureSysLite(Landroid/content/Context;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isMiuiHome(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public setBackgroundWithQS(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public setBackgroundWithQS(Landroid/content/Context;ILandroid/content/pm/ActivityInfo;)Landroid/graphics/drawable/Drawable;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public shouldDelay(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method
