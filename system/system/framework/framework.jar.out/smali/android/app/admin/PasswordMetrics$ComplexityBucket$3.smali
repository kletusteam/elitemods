.class final enum Landroid/app/admin/PasswordMetrics$ComplexityBucket$3;
.super Landroid/app/admin/PasswordMetrics$ComplexityBucket;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/admin/PasswordMetrics$ComplexityBucket;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4010
    name = null
.end annotation


# direct methods
.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/app/admin/PasswordMetrics$ComplexityBucket;-><init>(Ljava/lang/String;IILandroid/app/admin/PasswordMetrics$ComplexityBucket-IA;)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IILandroid/app/admin/PasswordMetrics$ComplexityBucket$3-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/app/admin/PasswordMetrics$ComplexityBucket$3;-><init>(Ljava/lang/String;II)V

    return-void
.end method


# virtual methods
.method allowsCredType(I)Z
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    goto :goto_5

    :goto_1
    goto/32 :goto_4

    nop

    :goto_2
    const/4 v0, 0x1

    goto/32 :goto_0

    nop

    :goto_3
    const/4 v0, -0x1

    goto/32 :goto_7

    nop

    :goto_4
    const/4 v0, 0x0

    :goto_5
    goto/32 :goto_6

    nop

    :goto_6
    return v0

    :goto_7
    if-ne p1, v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_2

    nop
.end method

.method canHaveSequence()Z
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    const/4 v0, 0x1

    goto/32 :goto_0

    nop
.end method

.method getMinimumLength(Z)I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method
