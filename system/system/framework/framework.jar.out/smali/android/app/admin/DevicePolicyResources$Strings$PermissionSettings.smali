.class public final Landroid/app/admin/DevicePolicyResources$Strings$PermissionSettings;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/admin/DevicePolicyResources$Strings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PermissionSettings"
.end annotation


# static fields
.field public static final BACKGROUND_ACCESS_DISABLED_BY_ADMIN_MESSAGE:Ljava/lang/String; = "PermissionSettings.BACKGROUND_ACCESS_DISABLED_BY_ADMIN_MESSAGE"

.field public static final BACKGROUND_ACCESS_ENABLED_BY_ADMIN_MESSAGE:Ljava/lang/String; = "PermissionSettings.BACKGROUND_ACCESS_ENABLED_BY_ADMIN_MESSAGE"

.field public static final FOREGROUND_ACCESS_ENABLED_BY_ADMIN_MESSAGE:Ljava/lang/String; = "PermissionSettings.FOREGROUND_ACCESS_ENABLED_BY_ADMIN_MESSAGE"

.field public static final LOCATION_AUTO_GRANTED_MESSAGE:Ljava/lang/String; = "PermissionSettings.LOCATION_AUTO_GRANTED_MESSAGE"

.field private static final PREFIX:Ljava/lang/String; = "PermissionSettings."


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
