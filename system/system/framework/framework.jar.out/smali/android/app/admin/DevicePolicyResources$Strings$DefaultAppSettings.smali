.class public final Landroid/app/admin/DevicePolicyResources$Strings$DefaultAppSettings;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/admin/DevicePolicyResources$Strings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultAppSettings"
.end annotation


# static fields
.field public static final HOME_MISSING_WORK_PROFILE_SUPPORT_MESSAGE:Ljava/lang/String; = "DefaultAppSettings.HOME_MISSING_WORK_PROFILE_SUPPORT_MESSAGE"

.field private static final PREFIX:Ljava/lang/String; = "DefaultAppSettings."

.field public static final WORK_PROFILE_DEFAULT_APPS_TITLE:Ljava/lang/String; = "DefaultAppSettings.WORK_PROFILE_DEFAULT_APPS_TITLE"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
