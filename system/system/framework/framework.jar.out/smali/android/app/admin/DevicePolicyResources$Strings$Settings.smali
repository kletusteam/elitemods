.class public final Landroid/app/admin/DevicePolicyResources$Strings$Settings;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/admin/DevicePolicyResources$Strings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Settings"
.end annotation


# static fields
.field public static final ACCESSIBILITY_CATEGORY_PERSONAL:Ljava/lang/String; = "Settings.ACCESSIBILITY_CATEGORY_PERSONAL"

.field public static final ACCESSIBILITY_CATEGORY_WORK:Ljava/lang/String; = "Settings.ACCESSIBILITY_CATEGORY_WORK"

.field public static final ACCESSIBILITY_PERSONAL_ACCOUNT_TITLE:Ljava/lang/String; = "Settings.ACCESSIBILITY_PERSONAL_ACCOUNT_TITLE"

.field public static final ACCESSIBILITY_WORK_ACCOUNT_TITLE:Ljava/lang/String; = "Settings.ACCESSIBILITY_WORK_ACCOUNT_TITLE"

.field public static final ACCOUNTS_SEARCH_KEYWORDS:Ljava/lang/String; = "Settings.ACCOUNTS_SEARCH_KEYWORDS"

.field public static final ACTIVATE_DEVICE_ADMIN_APP:Ljava/lang/String; = "Settings.ACTIVATE_DEVICE_ADMIN_APP"

.field public static final ACTIVATE_DEVICE_ADMIN_APP_TITLE:Ljava/lang/String; = "Settings.ACTIVATE_DEVICE_ADMIN_APP_TITLE"

.field public static final ACTIVATE_THIS_DEVICE_ADMIN_APP:Ljava/lang/String; = "Settings.ACTIVATE_THIS_DEVICE_ADMIN_APP"

.field public static final ACTIVE_DEVICE_ADMIN_WARNING:Ljava/lang/String; = "Settings.ACTIVE_DEVICE_ADMIN_WARNING"

.field public static final ADMIN_ACTIONS_APPS_COUNT:Ljava/lang/String; = "Settings.ADMIN_ACTIONS_APPS_COUNT"

.field public static final ADMIN_ACTIONS_APPS_COUNT_MINIMUM:Ljava/lang/String; = "Settings.ADMIN_ACTIONS_APPS_COUNT_MINIMUM"

.field public static final ADMIN_ACTION_ACCESS_CAMERA:Ljava/lang/String; = "Settings.ADMIN_ACTION_ACCESS_CAMERA"

.field public static final ADMIN_ACTION_ACCESS_LOCATION:Ljava/lang/String; = "Settings.ADMIN_ACTION_ACCESS_LOCATION"

.field public static final ADMIN_ACTION_ACCESS_MICROPHONE:Ljava/lang/String; = "Settings.ADMIN_ACTION_ACCESS_MICROPHONE"

.field public static final ADMIN_ACTION_APPS_COUNT_ESTIMATED:Ljava/lang/String; = "Settings.ADMIN_ACTION_APPS_COUNT_ESTIMATED"

.field public static final ADMIN_ACTION_APPS_INSTALLED:Ljava/lang/String; = "Settings.ADMIN_ACTION_APPS_INSTALLED"

.field public static final ADMIN_ACTION_NONE:Ljava/lang/String; = "Settings.ADMIN_ACTION_NONE"

.field public static final ADMIN_ACTION_SET_CURRENT_INPUT_METHOD:Ljava/lang/String; = "Settings.ADMIN_ACTION_SET_CURRENT_INPUT_METHOD"

.field public static final ADMIN_ACTION_SET_DEFAULT_APPS:Ljava/lang/String; = "Settings.ADMIN_ACTION_SET_DEFAULT_APPS"

.field public static final ADMIN_ACTION_SET_HTTP_PROXY:Ljava/lang/String; = "Settings.ADMIN_ACTION_SET_HTTP_PROXY"

.field public static final ADMIN_ACTION_SET_INPUT_METHOD_NAME:Ljava/lang/String; = "Settings.ADMIN_ACTION_SET_INPUT_METHOD_NAME"

.field public static final ADMIN_CAN_LOCK_DEVICE:Ljava/lang/String; = "Settings.ADMIN_CAN_LOCK_DEVICE"

.field public static final ADMIN_CAN_SEE_APPS_WARNING:Ljava/lang/String; = "Settings.ADMIN_CAN_SEE_APPS_WARNING"

.field public static final ADMIN_CAN_SEE_BUG_REPORT_WARNING:Ljava/lang/String; = "Settings.ADMIN_CAN_SEE_BUG_REPORT_WARNING"

.field public static final ADMIN_CAN_SEE_NETWORK_LOGS_WARNING:Ljava/lang/String; = "Settings.ADMIN_CAN_SEE_NETWORK_LOGS_WARNING"

.field public static final ADMIN_CAN_SEE_SECURITY_LOGS_WARNING:Ljava/lang/String; = "Settings.ADMIN_CAN_SEE_SECURITY_LOGS_WARNING"

.field public static final ADMIN_CAN_SEE_USAGE_WARNING:Ljava/lang/String; = "Settings.ADMIN_CAN_SEE_USAGE_WARNING"

.field public static final ADMIN_CAN_SEE_WORK_DATA_WARNING:Ljava/lang/String; = "Settings.ADMIN_CAN_SEE_WORK_DATA_WARNING"

.field public static final ADMIN_CAN_WIPE_DEVICE:Ljava/lang/String; = "Settings.ADMIN_CAN_WIPE_DEVICE"

.field public static final ADMIN_CONFIGURED_FAILED_PASSWORD_WIPE_DEVICE:Ljava/lang/String; = "Settings.ADMIN_CONFIGURED_FAILED_PASSWORD_WIPE_DEVICE"

.field public static final ADMIN_CONFIGURED_FAILED_PASSWORD_WIPE_WORK_PROFILE:Ljava/lang/String; = "Settings.ADMIN_CONFIGURED_FAILED_PASSWORD_WIPE_WORK_PROFILE"

.field public static final ALWAYS_ON_VPN_DEVICE:Ljava/lang/String; = "Settings.ALWAYS_ON_VPN_DEVICE"

.field public static final ALWAYS_ON_VPN_PERSONAL_PROFILE:Ljava/lang/String; = "Settings.ALWAYS_ON_VPN_PERSONAL_PROFILE"

.field public static final ALWAYS_ON_VPN_WORK_PROFILE:Ljava/lang/String; = "Settings.ALWAYS_ON_VPN_WORK_PROFILE"

.field public static final APP_CAN_ACCESS_PERSONAL_DATA:Ljava/lang/String; = "Settings.APP_CAN_ACCESS_PERSONAL_DATA"

.field public static final APP_CAN_ACCESS_PERSONAL_PERMISSIONS:Ljava/lang/String; = "Settings.APP_CAN_ACCESS_PERSONAL_PERMISSIONS"

.field public static final AUTO_SYNC_PERSONAL_DATA:Ljava/lang/String; = "Settings.AUTO_SYNC_PERSONAL_DATA"

.field public static final AUTO_SYNC_WORK_DATA:Ljava/lang/String; = "Settings.AUTO_SYNC_WORK_DATA"

.field public static final CA_CERTS_DEVICE:Ljava/lang/String; = "Settings.CA_CERTS_DEVICE"

.field public static final CA_CERTS_PERSONAL_PROFILE:Ljava/lang/String; = "Settings.CA_CERTS_PERSONAL_PROFILE"

.field public static final CA_CERTS_WORK_PROFILE:Ljava/lang/String; = "Settings.CA_CERTS_WORK_PROFILE"

.field public static final CHANGES_BY_ORGANIZATION_TITLE:Ljava/lang/String; = "Settings.CHANGES_BY_ORGANIZATION_TITLE"

.field public static final CHANGES_MADE_BY_YOUR_ORGANIZATION_ADMIN_TITLE:Ljava/lang/String; = "Settings.CHANGES_MADE_BY_YOUR_ORGANIZATION_ADMIN_TITLE"

.field public static final CONFIRM_WORK_PROFILE_PASSWORD_HEADER:Ljava/lang/String; = "Settings.CONFIRM_WORK_PROFILE_PASSWORD_HEADER"

.field public static final CONFIRM_WORK_PROFILE_PATTERN_HEADER:Ljava/lang/String; = "Settings.CONFIRM_WORK_PROFILE_PATTERN_HEADER"

.field public static final CONFIRM_WORK_PROFILE_PIN_HEADER:Ljava/lang/String; = "Settings.CONFIRM_WORK_PROFILE_PIN_HEADER"

.field public static final CONNECTED_APPS_SEARCH_KEYWORDS:Ljava/lang/String; = "Settings.CONNECTED_APPS_SEARCH_KEYWORDS"

.field public static final CONNECTED_APPS_SHARE_PERMISSIONS_AND_DATA:Ljava/lang/String; = "Settings.CONNECTED_APPS_SHARE_PERMISSIONS_AND_DATA"

.field public static final CONNECTED_WORK_AND_PERSONAL_APPS_TITLE:Ljava/lang/String; = "Settings.CONNECTED_WORK_AND_PERSONAL_APPS_TITLE"

.field public static final CONNECT_APPS_DIALOG_SUMMARY:Ljava/lang/String; = "Settings.CONNECT_APPS_DIALOG_SUMMARY"

.field public static final CONNECT_APPS_DIALOG_TITLE:Ljava/lang/String; = "Settings.CONNECT_APPS_DIALOG_TITLE"

.field public static final CONTACT_YOUR_IT_ADMIN:Ljava/lang/String; = "Settings.CONTACT_YOUR_IT_ADMIN"

.field public static final CONTROLLED_BY_ADMIN_SUMMARY:Ljava/lang/String; = "Settings.CONTROLLED_BY_ADMIN_SUMMARY"

.field public static final CROSS_PROFILE_CALENDAR_SUMMARY:Ljava/lang/String; = "Settings.CROSS_PROFILE_CALENDAR_SUMMARY"

.field public static final CROSS_PROFILE_CALENDAR_TITLE:Ljava/lang/String; = "Settings.CROSS_PROFILE_CALENDAR_TITLE"

.field public static final DEVICE_ADMIN_POLICIES_WARNING:Ljava/lang/String; = "Settings.DEVICE_ADMIN_POLICIES_WARNING"

.field public static final DEVICE_ADMIN_SETTINGS_TITLE:Ljava/lang/String; = "Settings.DEVICE_ADMIN_SETTINGS_TITLE"

.field public static final DEVICE_MANAGED_WITHOUT_NAME:Ljava/lang/String; = "Settings.DEVICE_MANAGED_WITHOUT_NAME"

.field public static final DEVICE_MANAGED_WITH_NAME:Ljava/lang/String; = "Settings.DEVICE_MANAGED_WITH_NAME"

.field public static final DEVICE_OWNER_INSTALLED_CERTIFICATE_AUTHORITY_WARNING:Ljava/lang/String; = "Settings.DEVICE_OWNER_INSTALLED_CERTIFICATE_AUTHORITY_WARNING"

.field public static final DISABLED_BY_ADMIN_SWITCH_SUMMARY:Ljava/lang/String; = "Settings.DISABLED_BY_ADMIN_SWITCH_SUMMARY"

.field public static final DISABLED_BY_IT_ADMIN_TITLE:Ljava/lang/String; = "Settings.DISABLED_BY_IT_ADMIN_TITLE"

.field public static final ENABLED_BY_ADMIN_SWITCH_SUMMARY:Ljava/lang/String; = "Settings.ENABLED_BY_ADMIN_SWITCH_SUMMARY"

.field public static final ENABLE_WORK_PROFILE_SYNC_WITH_PERSONAL_SOUNDS_DIALOG_MESSAGE:Ljava/lang/String; = "Settings.ENABLE_WORK_PROFILE_SYNC_WITH_PERSONAL_SOUNDS_DIALOG_MESSAGE"

.field public static final ENABLE_WORK_PROFILE_SYNC_WITH_PERSONAL_SOUNDS_DIALOG_TITLE:Ljava/lang/String; = "Settings.ENABLE_WORK_PROFILE_SYNC_WITH_PERSONAL_SOUNDS_DIALOG_TITLE"

.field public static final ENTERPRISE_PRIVACY_FOOTER:Ljava/lang/String; = "Settings.ENTERPRISE_PRIVACY_FOOTER"

.field public static final ENTERPRISE_PRIVACY_HEADER:Ljava/lang/String; = "Settings.ENTERPRISE_PRIVACY_HEADER"

.field public static final ERROR_MOVE_DEVICE_ADMIN:Ljava/lang/String; = "Settings.ERROR_MOVE_DEVICE_ADMIN"

.field public static final FACE_SETTINGS_FOR_WORK_TITLE:Ljava/lang/String; = "Settings.FACE_SETTINGS_FOR_WORK_TITLE"

.field public static final FACE_UNLOCK_DISABLED:Ljava/lang/String; = "Settings.FACE_UNLOCK_DISABLED"

.field public static final FINGERPRINT_FOR_WORK:Ljava/lang/String; = "Settings.FINGERPRINT_FOR_WORK"

.field public static final FINGERPRINT_UNLOCK_DISABLED:Ljava/lang/String; = "Settings.FINGERPRINT_UNLOCK_DISABLED"

.field public static final FINGERPRINT_UNLOCK_DISABLED_EXPLANATION:Ljava/lang/String; = "Settings.FINGERPRINT_UNLOCK_DISABLED_EXPLANATION"

.field public static final FORGOT_PASSWORD_TEXT:Ljava/lang/String; = "Settings.FORGOT_PASSWORD_TEXT"

.field public static final FORGOT_PASSWORD_TITLE:Ljava/lang/String; = "Settings.FORGOT_PASSWORD_TITLE"

.field public static final HOW_TO_DISCONNECT_APPS:Ljava/lang/String; = "Settings.HOW_TO_DISCONNECT_APPS"

.field public static final INFORMATION_SEEN_BY_ORGANIZATION_TITLE:Ljava/lang/String; = "Settings.INFORMATION_SEEN_BY_ORGANIZATION_TITLE"

.field public static final INFORMATION_YOUR_ORGANIZATION_CAN_SEE_TITLE:Ljava/lang/String; = "Settings.INFORMATION_YOUR_ORGANIZATION_CAN_SEE_TITLE"

.field public static final INSTALL_IN_PERSONAL_PROFILE_TO_CONNECT_PROMPT:Ljava/lang/String; = "Settings.INSTALL_IN_PERSONAL_PROFILE_TO_CONNECT_PROMPT"

.field public static final INSTALL_IN_WORK_PROFILE_TO_CONNECT_PROMPT:Ljava/lang/String; = "Settings.INSTALL_IN_WORK_PROFILE_TO_CONNECT_PROMPT"

.field public static final IT_ADMIN_POLICY_DISABLING_INFO_URL:Ljava/lang/String; = "Settings.IT_ADMIN_POLICY_DISABLING_INFO_URL"

.field public static final LOCK_SCREEN_HIDE_WORK_NOTIFICATION_CONTENT:Ljava/lang/String; = "Settings.LOCK_SCREEN_HIDE_WORK_NOTIFICATION_CONTENT"

.field public static final LOCK_SCREEN_SHOW_WORK_NOTIFICATION_CONTENT:Ljava/lang/String; = "Settings.LOCK_SCREEN_SHOW_WORK_NOTIFICATION_CONTENT"

.field public static final LOCK_SETTINGS_NEW_PROFILE_LOCK_TITLE:Ljava/lang/String; = "Settings.LOCK_SETTINGS_NEW_PROFILE_LOCK_TITLE"

.field public static final LOCK_SETTINGS_UPDATE_PROFILE_LOCK_TITLE:Ljava/lang/String; = "Settings.LOCK_SETTINGS_UPDATE_PROFILE_LOCK_TITLE"

.field public static final MANAGED_BY:Ljava/lang/String; = "Settings.MANAGED_BY"

.field public static final MANAGED_DEVICE_INFO:Ljava/lang/String; = "Settings.MANAGED_DEVICE_INFO"

.field public static final MANAGED_DEVICE_INFO_SUMMARY:Ljava/lang/String; = "Settings.MANAGED_DEVICE_INFO_SUMMARY"

.field public static final MANAGED_DEVICE_INFO_SUMMARY_WITH_NAME:Ljava/lang/String; = "Settings.MANAGED_DEVICE_INFO_SUMMARY_WITH_NAME"

.field public static final MANAGED_PROFILE_SETTINGS_TITLE:Ljava/lang/String; = "Settings.MANAGED_PROFILE_SETTINGS_TITLE"

.field public static final MANAGE_DEVICE_ADMIN_APPS:Ljava/lang/String; = "Settings.MANAGE_DEVICE_ADMIN_APPS"

.field public static final MORE_SECURITY_SETTINGS_WORK_PROFILE_SUMMARY:Ljava/lang/String; = "Settings.MORE_SECURITY_SETTINGS_WORK_PROFILE_SUMMARY"

.field public static final NEW_DEVICE_ADMIN_WARNING:Ljava/lang/String; = "Settings.NEW_DEVICE_ADMIN_WARNING"

.field public static final NEW_DEVICE_ADMIN_WARNING_SIMPLIFIED:Ljava/lang/String; = "Settings.NEW_DEVICE_ADMIN_WARNING_SIMPLIFIED"

.field public static final NO_DEVICE_ADMINS:Ljava/lang/String; = "Settings.NO_DEVICE_ADMINS"

.field public static final NUMBER_OF_DEVICE_ADMINS:Ljava/lang/String; = "Settings.NUMBER_OF_DEVICE_ADMINS"

.field public static final NUMBER_OF_DEVICE_ADMINS_NONE:Ljava/lang/String; = "Settings.NUMBER_OF_DEVICE_ADMINS_NONE"

.field public static final ONLY_CONNECT_TRUSTED_APPS:Ljava/lang/String; = "Settings.ONLY_CONNECT_TRUSTED_APPS"

.field public static final OTHER_OPTIONS_DISABLED_BY_ADMIN:Ljava/lang/String; = "Settings.OTHER_OPTIONS_DISABLED_BY_ADMIN"

.field public static final PASSWORD_RECENTLY_USED:Ljava/lang/String; = "Settings.PASSWORD_RECENTLY_USED"

.field public static final PERSONAL_CATEGORY_HEADER:Ljava/lang/String; = "Settings.PERSONAL_CATEGORY_HEADER"

.field public static final PERSONAL_DICTIONARY_FOR_WORK:Ljava/lang/String; = "Settings.PERSONAL_DICTIONARY_FOR_WORK"

.field public static final PERSONAL_PROFILE_APP_SUBTEXT:Ljava/lang/String; = "Settings.PERSONAL_PROFILE_APP_SUBTEXT"

.field public static final PIN_RECENTLY_USED:Ljava/lang/String; = "Settings.PIN_RECENTLY_USED"

.field private static final PREFIX:Ljava/lang/String; = "Settings."

.field public static final REENTER_WORK_PROFILE_PASSWORD_HEADER:Ljava/lang/String; = "Settings.REENTER_WORK_PROFILE_PASSWORD_HEADER"

.field public static final REENTER_WORK_PROFILE_PIN_HEADER:Ljava/lang/String; = "Settings.REENTER_WORK_PROFILE_PIN_HEADER"

.field public static final REMOVE_ACCOUNT_FAILED_ADMIN_RESTRICTION:Ljava/lang/String; = "Settings.REMOVE_ACCOUNT_FAILED_ADMIN_RESTRICTION"

.field public static final REMOVE_AND_UNINSTALL_DEVICE_ADMIN:Ljava/lang/String; = "Settings.REMOVE_AND_UNINSTALL_DEVICE_ADMIN"

.field public static final REMOVE_DEVICE_ADMIN:Ljava/lang/String; = "Settings.REMOVE_DEVICE_ADMIN"

.field public static final REMOVE_WORK_PROFILE:Ljava/lang/String; = "Settings.REMOVE_WORK_PROFILE"

.field public static final SET_PROFILE_OWNER_DIALOG_TITLE:Ljava/lang/String; = "Settings.SET_PROFILE_OWNER_DIALOG_TITLE"

.field public static final SET_PROFILE_OWNER_POSTSETUP_WARNING:Ljava/lang/String; = "Settings.SET_PROFILE_OWNER_POSTSETUP_WARNING"

.field public static final SET_WORK_PROFILE_PASSWORD_HEADER:Ljava/lang/String; = "Settings.SET_WORK_PROFILE_PASSWORD_HEADER"

.field public static final SET_WORK_PROFILE_PATTERN_HEADER:Ljava/lang/String; = "Settings.SET_WORK_PROFILE_PATTERN_HEADER"

.field public static final SET_WORK_PROFILE_PIN_HEADER:Ljava/lang/String; = "Settings.SET_WORK_PROFILE_PIN_HEADER"

.field public static final SHARE_REMOTE_BUGREPORT_DIALOG_TITLE:Ljava/lang/String; = "Settings.SHARE_REMOTE_BUGREPORT_DIALOG_TITLE"

.field public static final SHARE_REMOTE_BUGREPORT_FINISHED_REQUEST_CONSENT:Ljava/lang/String; = "Settings.SHARE_REMOTE_BUGREPORT_FINISHED_REQUEST_CONSENT"

.field public static final SHARE_REMOTE_BUGREPORT_NOT_FINISHED_REQUEST_CONSENT:Ljava/lang/String; = "Settings.SHARE_REMOTE_BUGREPORT_NOT_FINISHED_REQUEST_CONSENT"

.field public static final SHARING_REMOTE_BUGREPORT_MESSAGE:Ljava/lang/String; = "Settings.SHARING_REMOTE_BUGREPORT_MESSAGE"

.field public static final SPELL_CHECKER_FOR_WORK:Ljava/lang/String; = "Settings.SPELL_CHECKER_FOR_WORK"

.field public static final UNINSTALL_DEVICE_ADMIN:Ljava/lang/String; = "Settings.UNINSTALL_DEVICE_ADMIN"

.field public static final USER_ADMIN_POLICIES_WARNING:Ljava/lang/String; = "Settings.USER_ADMIN_POLICIES_WARNING"

.field public static final WORK_APPS_CANNOT_ACCESS_NOTIFICATION_SETTINGS:Ljava/lang/String; = "Settings.WORK_APPS_CANNOT_ACCESS_NOTIFICATION_SETTINGS"

.field public static final WORK_CATEGORY_HEADER:Ljava/lang/String; = "Settings.WORK_CATEGORY_HEADER"

.field public static final WORK_PROFILE_ADMIN_POLICIES_WARNING:Ljava/lang/String; = "Settings.WORK_PROFILE_ADMIN_POLICIES_WARNING"

.field public static final WORK_PROFILE_ALARM_RINGTONE_TITLE:Ljava/lang/String; = "Settings.WORK_PROFILE_ALARM_RINGTONE_TITLE"

.field public static final WORK_PROFILE_APP_SUBTEXT:Ljava/lang/String; = "Settings.WORK_PROFILE_APP_SUBTEXT"

.field public static final WORK_PROFILE_CONFIRM_PASSWORD:Ljava/lang/String; = "Settings.WORK_PROFILE_CONFIRM_PASSWORD"

.field public static final WORK_PROFILE_CONFIRM_PATTERN:Ljava/lang/String; = "Settings.WORK_PROFILE_CONFIRM_PATTERN"

.field public static final WORK_PROFILE_CONFIRM_PIN:Ljava/lang/String; = "Settings.WORK_PROFILE_CONFIRM_PIN"

.field public static final WORK_PROFILE_CONFIRM_REMOVE_MESSAGE:Ljava/lang/String; = "Settings.WORK_PROFILE_CONFIRM_REMOVE_MESSAGE"

.field public static final WORK_PROFILE_CONFIRM_REMOVE_TITLE:Ljava/lang/String; = "Settings.WORK_PROFILE_CONFIRM_REMOVE_TITLE"

.field public static final WORK_PROFILE_CONTACT_SEARCH_SUMMARY:Ljava/lang/String; = "Settings.WORK_PROFILE_CONTACT_SEARCH_SUMMARY"

.field public static final WORK_PROFILE_CONTACT_SEARCH_TITLE:Ljava/lang/String; = "Settings.WORK_PROFILE_CONTACT_SEARCH_TITLE"

.field public static final WORK_PROFILE_DISABLE_USAGE_ACCESS_WARNING:Ljava/lang/String; = "Settings.WORK_PROFILE_DISABLE_USAGE_ACCESS_WARNING"

.field public static final WORK_PROFILE_FINGERPRINT_LAST_DELETE_MESSAGE:Ljava/lang/String; = "Settings.WORK_PROFILE_FINGERPRINT_LAST_DELETE_MESSAGE"

.field public static final WORK_PROFILE_INSTALLED_CERTIFICATE_AUTHORITY_WARNING:Ljava/lang/String; = "Settings.WORK_PROFILE_INSTALLED_CERTIFICATE_AUTHORITY_WARNING"

.field public static final WORK_PROFILE_IT_ADMIN_CANT_RESET_SCREEN_LOCK:Ljava/lang/String; = "Settings.WORK_PROFILE_IT_ADMIN_CANT_RESET_SCREEN_LOCK"

.field public static final WORK_PROFILE_IT_ADMIN_CANT_RESET_SCREEN_LOCK_ACTION:Ljava/lang/String; = "Settings.WORK_PROFILE_IT_ADMIN_CANT_RESET_SCREEN_LOCK_ACTION"

.field public static final WORK_PROFILE_KEYBOARDS_AND_TOOLS:Ljava/lang/String; = "Settings.WORK_PROFILE_KEYBOARDS_AND_TOOLS"

.field public static final WORK_PROFILE_LAST_PASSWORD_ATTEMPT_BEFORE_WIPE:Ljava/lang/String; = "Settings.WORK_PROFILE_LAST_PASSWORD_ATTEMPT_BEFORE_WIPE"

.field public static final WORK_PROFILE_LAST_PATTERN_ATTEMPT_BEFORE_WIPE:Ljava/lang/String; = "Settings.WORK_PROFILE_LAST_PATTERN_ATTEMPT_BEFORE_WIPE"

.field public static final WORK_PROFILE_LAST_PIN_ATTEMPT_BEFORE_WIPE:Ljava/lang/String; = "Settings.WORK_PROFILE_LAST_PIN_ATTEMPT_BEFORE_WIPE"

.field public static final WORK_PROFILE_LOCATION_SWITCH_TITLE:Ljava/lang/String; = "Settings.WORK_PROFILE_LOCATION_SWITCH_TITLE"

.field public static final WORK_PROFILE_LOCKED_NOTIFICATION_TITLE:Ljava/lang/String; = "Settings.WORK_PROFILE_LOCKED_NOTIFICATION_TITLE"

.field public static final WORK_PROFILE_LOCK_ATTEMPTS_FAILED:Ljava/lang/String; = "Settings.WORK_PROFILE_LOCK_ATTEMPTS_FAILED"

.field public static final WORK_PROFILE_LOCK_SCREEN_REDACT_NOTIFICATION_SUMMARY:Ljava/lang/String; = "Settings.WORK_PROFILE_LOCK_SCREEN_REDACT_NOTIFICATION_SUMMARY"

.field public static final WORK_PROFILE_LOCK_SCREEN_REDACT_NOTIFICATION_TITLE:Ljava/lang/String; = "Settings.WORK_PROFILE_LOCK_SCREEN_REDACT_NOTIFICATION_TITLE"

.field public static final WORK_PROFILE_MANAGED_BY:Ljava/lang/String; = "Settings.WORK_PROFILE_MANAGED_BY"

.field public static final WORK_PROFILE_NOTIFICATIONS_SECTION_HEADER:Ljava/lang/String; = "Settings.WORK_PROFILE_NOTIFICATIONS_SECTION_HEADER"

.field public static final WORK_PROFILE_NOTIFICATION_LISTENER_BLOCKED:Ljava/lang/String; = "Settings.WORK_PROFILE_NOTIFICATION_LISTENER_BLOCKED"

.field public static final WORK_PROFILE_NOTIFICATION_RINGTONE_TITLE:Ljava/lang/String; = "Settings.WORK_PROFILE_NOTIFICATION_RINGTONE_TITLE"

.field public static final WORK_PROFILE_NOT_AVAILABLE:Ljava/lang/String; = "Settings.WORK_PROFILE_NOT_AVAILABLE"

.field public static final WORK_PROFILE_OFF_CONDITION_TITLE:Ljava/lang/String; = "Settings.WORK_PROFILE_OFF_CONDITION_TITLE"

.field public static final WORK_PROFILE_PASSWORD_REQUIRED:Ljava/lang/String; = "Settings.WORK_PROFILE_PASSWORD_REQUIRED"

.field public static final WORK_PROFILE_PATTERN_REQUIRED:Ljava/lang/String; = "Settings.WORK_PROFILE_PATTERN_REQUIRED"

.field public static final WORK_PROFILE_PIN_REQUIRED:Ljava/lang/String; = "Settings.WORK_PROFILE_PIN_REQUIRED"

.field public static final WORK_PROFILE_PRIVACY_POLICY_INFO:Ljava/lang/String; = "Settings.WORK_PROFILE_PRIVACY_POLICY_INFO"

.field public static final WORK_PROFILE_PRIVACY_POLICY_INFO_SUMMARY:Ljava/lang/String; = "Settings.WORK_PROFILE_PRIVACY_POLICY_INFO_SUMMARY"

.field public static final WORK_PROFILE_RINGTONE_TITLE:Ljava/lang/String; = "Settings.WORK_PROFILE_RINGTONE_TITLE"

.field public static final WORK_PROFILE_SCREEN_LOCK_SETUP_MESSAGE:Ljava/lang/String; = "Settings.WORK_PROFILE_SCREEN_LOCK_SETUP_MESSAGE"

.field public static final WORK_PROFILE_SECURITY_TITLE:Ljava/lang/String; = "Settings.WORK_PROFILE_SECURITY_TITLE"

.field public static final WORK_PROFILE_SETTING:Ljava/lang/String; = "Settings.WORK_PROFILE_SETTING"

.field public static final WORK_PROFILE_SETTING_OFF_SUMMARY:Ljava/lang/String; = "Settings.WORK_PROFILE_SETTING_OFF_SUMMARY"

.field public static final WORK_PROFILE_SETTING_ON_SUMMARY:Ljava/lang/String; = "Settings.WORK_PROFILE_SETTING_ON_SUMMARY"

.field public static final WORK_PROFILE_SET_UNLOCK_LAUNCH_PICKER_TITLE:Ljava/lang/String; = "Settings.WORK_PROFILE_SET_UNLOCK_LAUNCH_PICKER_TITLE"

.field public static final WORK_PROFILE_SOUND_SETTINGS_SECTION_HEADER:Ljava/lang/String; = "Settings.WORK_PROFILE_SOUND_SETTINGS_SECTION_HEADER"

.field public static final WORK_PROFILE_SYNC_WITH_PERSONAL_SOUNDS_ACTIVE_SUMMARY:Ljava/lang/String; = "Settings.WORK_PROFILE_SYNC_WITH_PERSONAL_SOUNDS_ACTIVE_SUMMARY"

.field public static final WORK_PROFILE_UNIFICATION_SEARCH_KEYWORDS:Ljava/lang/String; = "Settings.WORK_PROFILE_UNIFICATION_SEARCH_KEYWORDS"

.field public static final WORK_PROFILE_UNIFY_LOCKS_DETAIL:Ljava/lang/String; = "Settings.WORK_PROFILE_UNIFY_LOCKS_DETAIL"

.field public static final WORK_PROFILE_UNIFY_LOCKS_NONCOMPLIANT:Ljava/lang/String; = "Settings.WORK_PROFILE_UNIFY_LOCKS_NONCOMPLIANT"

.field public static final WORK_PROFILE_UNIFY_LOCKS_SUMMARY:Ljava/lang/String; = "Settings.WORK_PROFILE_UNIFY_LOCKS_SUMMARY"

.field public static final WORK_PROFILE_UNIFY_LOCKS_TITLE:Ljava/lang/String; = "Settings.WORK_PROFILE_UNIFY_LOCKS_TITLE"

.field public static final WORK_PROFILE_USER_LABEL:Ljava/lang/String; = "Settings.WORK_PROFILE_USER_LABEL"

.field public static final WORK_PROFILE_USE_PERSONAL_SOUNDS_SUMMARY:Ljava/lang/String; = "Settings.WORK_PROFILE_USE_PERSONAL_SOUNDS_SUMMARY"

.field public static final WORK_PROFILE_USE_PERSONAL_SOUNDS_TITLE:Ljava/lang/String; = "Settings.WORK_PROFILE_USE_PERSONAL_SOUNDS_TITLE"

.field public static final YOUR_ACCESS_TO_THIS_DEVICE_TITLE:Ljava/lang/String; = "Settings.YOUR_ACCESS_TO_THIS_DEVICE_TITLE"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
