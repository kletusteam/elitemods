.class public final Landroid/app/admin/DevicePolicyResources$Drawables$Source;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/admin/DevicePolicyResources$Drawables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Source"
.end annotation


# static fields
.field public static final HOME_WIDGET:Ljava/lang/String; = "HOME_WIDGET"

.field public static final LAUNCHER_OFF_BUTTON:Ljava/lang/String; = "LAUNCHER_OFF_BUTTON"

.field public static final NOTIFICATION:Ljava/lang/String; = "NOTIFICATION"

.field public static final PROFILE_SWITCH_ANIMATION:Ljava/lang/String; = "PROFILE_SWITCH_ANIMATION"

.field public static final QUICK_SETTINGS:Ljava/lang/String; = "QUICK_SETTINGS"

.field public static final STATUS_BAR:Ljava/lang/String; = "STATUS_BAR"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
