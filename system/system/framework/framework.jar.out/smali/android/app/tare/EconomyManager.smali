.class public Landroid/app/tare/EconomyManager;
.super Ljava/lang/Object;


# static fields
.field public static final CAKE_IN_ARC:J = 0x3b9aca00L

.field public static final DEFAULT_AM_ACTION_ALARM_ALARMCLOCK_BASE_PRICE_CAKES:J

.field public static final DEFAULT_AM_ACTION_ALARM_ALARMCLOCK_CTP_CAKES:J

.field public static final DEFAULT_AM_ACTION_ALARM_ALLOW_WHILE_IDLE_EXACT_NONWAKEUP_BASE_PRICE_CAKES:J

.field public static final DEFAULT_AM_ACTION_ALARM_ALLOW_WHILE_IDLE_EXACT_NONWAKEUP_CTP_CAKES:J

.field public static final DEFAULT_AM_ACTION_ALARM_ALLOW_WHILE_IDLE_EXACT_WAKEUP_BASE_PRICE_CAKES:J

.field public static final DEFAULT_AM_ACTION_ALARM_ALLOW_WHILE_IDLE_EXACT_WAKEUP_CTP_CAKES:J

.field public static final DEFAULT_AM_ACTION_ALARM_ALLOW_WHILE_IDLE_INEXACT_NONWAKEUP_BASE_PRICE_CAKES:J

.field public static final DEFAULT_AM_ACTION_ALARM_ALLOW_WHILE_IDLE_INEXACT_NONWAKEUP_CTP_CAKES:J

.field public static final DEFAULT_AM_ACTION_ALARM_ALLOW_WHILE_IDLE_INEXACT_WAKEUP_BASE_PRICE_CAKES:J

.field public static final DEFAULT_AM_ACTION_ALARM_ALLOW_WHILE_IDLE_INEXACT_WAKEUP_CTP_CAKES:J

.field public static final DEFAULT_AM_ACTION_ALARM_EXACT_NONWAKEUP_BASE_PRICE_CAKES:J

.field public static final DEFAULT_AM_ACTION_ALARM_EXACT_NONWAKEUP_CTP_CAKES:J

.field public static final DEFAULT_AM_ACTION_ALARM_EXACT_WAKEUP_BASE_PRICE_CAKES:J

.field public static final DEFAULT_AM_ACTION_ALARM_EXACT_WAKEUP_CTP_CAKES:J

.field public static final DEFAULT_AM_ACTION_ALARM_INEXACT_NONWAKEUP_BASE_PRICE_CAKES:J

.field public static final DEFAULT_AM_ACTION_ALARM_INEXACT_NONWAKEUP_CTP_CAKES:J

.field public static final DEFAULT_AM_ACTION_ALARM_INEXACT_WAKEUP_BASE_PRICE_CAKES:J

.field public static final DEFAULT_AM_ACTION_ALARM_INEXACT_WAKEUP_CTP_CAKES:J

.field public static final DEFAULT_AM_HARD_CONSUMPTION_LIMIT_CAKES:J

.field public static final DEFAULT_AM_INITIAL_CONSUMPTION_LIMIT_CAKES:J

.field public static final DEFAULT_AM_MAX_SATIATED_BALANCE_CAKES:J

.field public static final DEFAULT_AM_MIN_SATIATED_BALANCE_EXEMPTED_CAKES:J

.field public static final DEFAULT_AM_MIN_SATIATED_BALANCE_HEADLESS_SYSTEM_APP_CAKES:J

.field public static final DEFAULT_AM_MIN_SATIATED_BALANCE_OTHER_APP_CAKES:J

.field public static final DEFAULT_AM_REWARD_NOTIFICATION_INTERACTION_INSTANT_CAKES:J

.field public static final DEFAULT_AM_REWARD_NOTIFICATION_INTERACTION_MAX_CAKES:J

.field public static final DEFAULT_AM_REWARD_NOTIFICATION_INTERACTION_ONGOING_CAKES:J

.field public static final DEFAULT_AM_REWARD_NOTIFICATION_SEEN_INSTANT_CAKES:J

.field public static final DEFAULT_AM_REWARD_NOTIFICATION_SEEN_MAX_CAKES:J

.field public static final DEFAULT_AM_REWARD_NOTIFICATION_SEEN_ONGOING_CAKES:J

.field public static final DEFAULT_AM_REWARD_NOTIFICATION_SEEN_WITHIN_15_INSTANT_CAKES:J

.field public static final DEFAULT_AM_REWARD_NOTIFICATION_SEEN_WITHIN_15_MAX_CAKES:J

.field public static final DEFAULT_AM_REWARD_NOTIFICATION_SEEN_WITHIN_15_ONGOING_CAKES:J

.field public static final DEFAULT_AM_REWARD_OTHER_USER_INTERACTION_INSTANT_CAKES:J

.field public static final DEFAULT_AM_REWARD_OTHER_USER_INTERACTION_MAX_CAKES:J

.field public static final DEFAULT_AM_REWARD_OTHER_USER_INTERACTION_ONGOING_CAKES:J

.field public static final DEFAULT_AM_REWARD_TOP_ACTIVITY_INSTANT_CAKES:J

.field public static final DEFAULT_AM_REWARD_TOP_ACTIVITY_MAX_CAKES:J

.field public static final DEFAULT_AM_REWARD_TOP_ACTIVITY_ONGOING_CAKES:J = 0x989680L

.field public static final DEFAULT_AM_REWARD_WIDGET_INTERACTION_INSTANT_CAKES:J

.field public static final DEFAULT_AM_REWARD_WIDGET_INTERACTION_MAX_CAKES:J

.field public static final DEFAULT_AM_REWARD_WIDGET_INTERACTION_ONGOING_CAKES:J

.field public static final DEFAULT_JS_ACTION_JOB_DEFAULT_RUNNING_BASE_PRICE_CAKES:J

.field public static final DEFAULT_JS_ACTION_JOB_DEFAULT_RUNNING_CTP_CAKES:J

.field public static final DEFAULT_JS_ACTION_JOB_DEFAULT_START_BASE_PRICE_CAKES:J

.field public static final DEFAULT_JS_ACTION_JOB_DEFAULT_START_CTP_CAKES:J

.field public static final DEFAULT_JS_ACTION_JOB_HIGH_RUNNING_BASE_PRICE_CAKES:J

.field public static final DEFAULT_JS_ACTION_JOB_HIGH_RUNNING_CTP_CAKES:J

.field public static final DEFAULT_JS_ACTION_JOB_HIGH_START_BASE_PRICE_CAKES:J

.field public static final DEFAULT_JS_ACTION_JOB_HIGH_START_CTP_CAKES:J

.field public static final DEFAULT_JS_ACTION_JOB_LOW_RUNNING_BASE_PRICE_CAKES:J

.field public static final DEFAULT_JS_ACTION_JOB_LOW_RUNNING_CTP_CAKES:J

.field public static final DEFAULT_JS_ACTION_JOB_LOW_START_BASE_PRICE_CAKES:J

.field public static final DEFAULT_JS_ACTION_JOB_LOW_START_CTP_CAKES:J

.field public static final DEFAULT_JS_ACTION_JOB_MAX_RUNNING_BASE_PRICE_CAKES:J

.field public static final DEFAULT_JS_ACTION_JOB_MAX_RUNNING_CTP_CAKES:J

.field public static final DEFAULT_JS_ACTION_JOB_MAX_START_BASE_PRICE_CAKES:J

.field public static final DEFAULT_JS_ACTION_JOB_MAX_START_CTP_CAKES:J

.field public static final DEFAULT_JS_ACTION_JOB_MIN_RUNNING_BASE_PRICE_CAKES:J

.field public static final DEFAULT_JS_ACTION_JOB_MIN_RUNNING_CTP_CAKES:J

.field public static final DEFAULT_JS_ACTION_JOB_MIN_START_BASE_PRICE_CAKES:J

.field public static final DEFAULT_JS_ACTION_JOB_MIN_START_CTP_CAKES:J

.field public static final DEFAULT_JS_ACTION_JOB_TIMEOUT_PENALTY_BASE_PRICE_CAKES:J

.field public static final DEFAULT_JS_ACTION_JOB_TIMEOUT_PENALTY_CTP_CAKES:J

.field public static final DEFAULT_JS_HARD_CONSUMPTION_LIMIT_CAKES:J

.field public static final DEFAULT_JS_INITIAL_CONSUMPTION_LIMIT_CAKES:J

.field public static final DEFAULT_JS_MAX_SATIATED_BALANCE_CAKES:J

.field public static final DEFAULT_JS_MIN_SATIATED_BALANCE_EXEMPTED_CAKES:J

.field public static final DEFAULT_JS_MIN_SATIATED_BALANCE_HEADLESS_SYSTEM_APP_CAKES:J

.field public static final DEFAULT_JS_MIN_SATIATED_BALANCE_OTHER_APP_CAKES:J

.field public static final DEFAULT_JS_REWARD_NOTIFICATION_INTERACTION_INSTANT_CAKES:J

.field public static final DEFAULT_JS_REWARD_NOTIFICATION_INTERACTION_MAX_CAKES:J

.field public static final DEFAULT_JS_REWARD_NOTIFICATION_INTERACTION_ONGOING_CAKES:J

.field public static final DEFAULT_JS_REWARD_NOTIFICATION_SEEN_INSTANT_CAKES:J

.field public static final DEFAULT_JS_REWARD_NOTIFICATION_SEEN_MAX_CAKES:J

.field public static final DEFAULT_JS_REWARD_NOTIFICATION_SEEN_ONGOING_CAKES:J

.field public static final DEFAULT_JS_REWARD_OTHER_USER_INTERACTION_INSTANT_CAKES:J

.field public static final DEFAULT_JS_REWARD_OTHER_USER_INTERACTION_MAX_CAKES:J

.field public static final DEFAULT_JS_REWARD_OTHER_USER_INTERACTION_ONGOING_CAKES:J

.field public static final DEFAULT_JS_REWARD_TOP_ACTIVITY_INSTANT_CAKES:J

.field public static final DEFAULT_JS_REWARD_TOP_ACTIVITY_MAX_CAKES:J

.field public static final DEFAULT_JS_REWARD_TOP_ACTIVITY_ONGOING_CAKES:J = 0x1dcd6500L

.field public static final DEFAULT_JS_REWARD_WIDGET_INTERACTION_INSTANT_CAKES:J

.field public static final DEFAULT_JS_REWARD_WIDGET_INTERACTION_MAX_CAKES:J

.field public static final DEFAULT_JS_REWARD_WIDGET_INTERACTION_ONGOING_CAKES:J

.field public static final KEY_AM_ACTION_ALARM_ALARMCLOCK_BASE_PRICE:Ljava/lang/String; = "am_action_alarm_alarmclock_base_price"

.field public static final KEY_AM_ACTION_ALARM_ALARMCLOCK_CTP:Ljava/lang/String; = "am_action_alarm_alarmclock_ctp"

.field public static final KEY_AM_ACTION_ALARM_ALLOW_WHILE_IDLE_EXACT_NONWAKEUP_BASE_PRICE:Ljava/lang/String; = "am_action_alarm_allow_while_idle_exact_nonwakeup_base_price"

.field public static final KEY_AM_ACTION_ALARM_ALLOW_WHILE_IDLE_EXACT_NONWAKEUP_CTP:Ljava/lang/String; = "am_action_alarm_allow_while_idle_exact_nonwakeup_ctp"

.field public static final KEY_AM_ACTION_ALARM_ALLOW_WHILE_IDLE_EXACT_WAKEUP_BASE_PRICE:Ljava/lang/String; = "am_action_alarm_allow_while_idle_exact_wakeup_base_price"

.field public static final KEY_AM_ACTION_ALARM_ALLOW_WHILE_IDLE_EXACT_WAKEUP_CTP:Ljava/lang/String; = "am_action_alarm_allow_while_idle_exact_wakeup_ctp"

.field public static final KEY_AM_ACTION_ALARM_ALLOW_WHILE_IDLE_INEXACT_NONWAKEUP_BASE_PRICE:Ljava/lang/String; = "am_action_alarm_allow_while_idle_inexact_nonwakeup_base_price"

.field public static final KEY_AM_ACTION_ALARM_ALLOW_WHILE_IDLE_INEXACT_NONWAKEUP_CTP:Ljava/lang/String; = "am_action_alarm_allow_while_idle_inexact_nonwakeup_ctp"

.field public static final KEY_AM_ACTION_ALARM_ALLOW_WHILE_IDLE_INEXACT_WAKEUP_BASE_PRICE:Ljava/lang/String; = "am_action_alarm_allow_while_idle_inexact_wakeup_base_price"

.field public static final KEY_AM_ACTION_ALARM_ALLOW_WHILE_IDLE_INEXACT_WAKEUP_CTP:Ljava/lang/String; = "am_action_alarm_allow_while_idle_inexact_wakeup_ctp"

.field public static final KEY_AM_ACTION_ALARM_EXACT_NONWAKEUP_BASE_PRICE:Ljava/lang/String; = "am_action_alarm_exact_nonwakeup_base_price"

.field public static final KEY_AM_ACTION_ALARM_EXACT_NONWAKEUP_CTP:Ljava/lang/String; = "am_action_alarm_exact_nonwakeup_ctp"

.field public static final KEY_AM_ACTION_ALARM_EXACT_WAKEUP_BASE_PRICE:Ljava/lang/String; = "am_action_alarm_exact_wakeup_base_price"

.field public static final KEY_AM_ACTION_ALARM_EXACT_WAKEUP_CTP:Ljava/lang/String; = "am_action_alarm_exact_wakeup_ctp"

.field public static final KEY_AM_ACTION_ALARM_INEXACT_NONWAKEUP_BASE_PRICE:Ljava/lang/String; = "am_action_alarm_inexact_nonwakeup_base_price"

.field public static final KEY_AM_ACTION_ALARM_INEXACT_NONWAKEUP_CTP:Ljava/lang/String; = "am_action_alarm_inexact_nonwakeup_ctp"

.field public static final KEY_AM_ACTION_ALARM_INEXACT_WAKEUP_BASE_PRICE:Ljava/lang/String; = "am_action_alarm_inexact_wakeup_base_price"

.field public static final KEY_AM_ACTION_ALARM_INEXACT_WAKEUP_CTP:Ljava/lang/String; = "am_action_alarm_inexact_wakeup_ctp"

.field public static final KEY_AM_HARD_CONSUMPTION_LIMIT:Ljava/lang/String; = "am_hard_consumption_limit"

.field public static final KEY_AM_INITIAL_CONSUMPTION_LIMIT:Ljava/lang/String; = "am_initial_consumption_limit"

.field public static final KEY_AM_MAX_SATIATED_BALANCE:Ljava/lang/String; = "am_max_satiated_balance"

.field public static final KEY_AM_MIN_SATIATED_BALANCE_EXEMPTED:Ljava/lang/String; = "am_min_satiated_balance_exempted"

.field public static final KEY_AM_MIN_SATIATED_BALANCE_HEADLESS_SYSTEM_APP:Ljava/lang/String; = "am_min_satiated_balance_headless_system_app"

.field public static final KEY_AM_MIN_SATIATED_BALANCE_OTHER_APP:Ljava/lang/String; = "am_min_satiated_balance_other_app"

.field public static final KEY_AM_REWARD_NOTIFICATION_INTERACTION_INSTANT:Ljava/lang/String; = "am_reward_notification_interaction_instant"

.field public static final KEY_AM_REWARD_NOTIFICATION_INTERACTION_MAX:Ljava/lang/String; = "am_reward_notification_interaction_max"

.field public static final KEY_AM_REWARD_NOTIFICATION_INTERACTION_ONGOING:Ljava/lang/String; = "am_reward_notification_interaction_ongoing"

.field public static final KEY_AM_REWARD_NOTIFICATION_SEEN_INSTANT:Ljava/lang/String; = "am_reward_notification_seen_instant"

.field public static final KEY_AM_REWARD_NOTIFICATION_SEEN_MAX:Ljava/lang/String; = "am_reward_notification_seen_max"

.field public static final KEY_AM_REWARD_NOTIFICATION_SEEN_ONGOING:Ljava/lang/String; = "am_reward_notification_seen_ongoing"

.field public static final KEY_AM_REWARD_NOTIFICATION_SEEN_WITHIN_15_INSTANT:Ljava/lang/String; = "am_reward_notification_seen_within_15_instant"

.field public static final KEY_AM_REWARD_NOTIFICATION_SEEN_WITHIN_15_MAX:Ljava/lang/String; = "am_reward_notification_seen_within_15_max"

.field public static final KEY_AM_REWARD_NOTIFICATION_SEEN_WITHIN_15_ONGOING:Ljava/lang/String; = "am_reward_notification_seen_within_15_ongoing"

.field public static final KEY_AM_REWARD_OTHER_USER_INTERACTION_INSTANT:Ljava/lang/String; = "am_reward_other_user_interaction_instant"

.field public static final KEY_AM_REWARD_OTHER_USER_INTERACTION_MAX:Ljava/lang/String; = "am_reward_other_user_interaction_max"

.field public static final KEY_AM_REWARD_OTHER_USER_INTERACTION_ONGOING:Ljava/lang/String; = "am_reward_other_user_interaction_ongoing"

.field public static final KEY_AM_REWARD_TOP_ACTIVITY_INSTANT:Ljava/lang/String; = "am_reward_top_activity_instant"

.field public static final KEY_AM_REWARD_TOP_ACTIVITY_MAX:Ljava/lang/String; = "am_reward_top_activity_max"

.field public static final KEY_AM_REWARD_TOP_ACTIVITY_ONGOING:Ljava/lang/String; = "am_reward_top_activity_ongoing"

.field public static final KEY_AM_REWARD_WIDGET_INTERACTION_INSTANT:Ljava/lang/String; = "am_reward_widget_interaction_instant"

.field public static final KEY_AM_REWARD_WIDGET_INTERACTION_MAX:Ljava/lang/String; = "am_reward_widget_interaction_max"

.field public static final KEY_AM_REWARD_WIDGET_INTERACTION_ONGOING:Ljava/lang/String; = "am_reward_widget_interaction_ongoing"

.field public static final KEY_JS_ACTION_JOB_DEFAULT_RUNNING_BASE_PRICE:Ljava/lang/String; = "js_action_job_default_running_base_price"

.field public static final KEY_JS_ACTION_JOB_DEFAULT_RUNNING_CTP:Ljava/lang/String; = "js_action_job_default_running_ctp"

.field public static final KEY_JS_ACTION_JOB_DEFAULT_START_BASE_PRICE:Ljava/lang/String; = "js_action_job_default_start_base_price"

.field public static final KEY_JS_ACTION_JOB_DEFAULT_START_CTP:Ljava/lang/String; = "js_action_job_default_start_ctp"

.field public static final KEY_JS_ACTION_JOB_HIGH_RUNNING_BASE_PRICE:Ljava/lang/String; = "js_action_job_high_running_base_price"

.field public static final KEY_JS_ACTION_JOB_HIGH_RUNNING_CTP:Ljava/lang/String; = "js_action_job_high_running_ctp"

.field public static final KEY_JS_ACTION_JOB_HIGH_START_BASE_PRICE:Ljava/lang/String; = "js_action_job_high_start_base_price"

.field public static final KEY_JS_ACTION_JOB_HIGH_START_CTP:Ljava/lang/String; = "js_action_job_high_start_ctp"

.field public static final KEY_JS_ACTION_JOB_LOW_RUNNING_BASE_PRICE:Ljava/lang/String; = "js_action_job_low_running_base_price"

.field public static final KEY_JS_ACTION_JOB_LOW_RUNNING_CTP:Ljava/lang/String; = "js_action_job_low_running_ctp"

.field public static final KEY_JS_ACTION_JOB_LOW_START_BASE_PRICE:Ljava/lang/String; = "js_action_job_low_start_base_price"

.field public static final KEY_JS_ACTION_JOB_LOW_START_CTP:Ljava/lang/String; = "js_action_job_low_start_ctp"

.field public static final KEY_JS_ACTION_JOB_MAX_RUNNING_BASE_PRICE:Ljava/lang/String; = "js_action_job_max_running_base_price"

.field public static final KEY_JS_ACTION_JOB_MAX_RUNNING_CTP:Ljava/lang/String; = "js_action_job_max_running_ctp"

.field public static final KEY_JS_ACTION_JOB_MAX_START_BASE_PRICE:Ljava/lang/String; = "js_action_job_max_start_base_price"

.field public static final KEY_JS_ACTION_JOB_MAX_START_CTP:Ljava/lang/String; = "js_action_job_max_start_ctp"

.field public static final KEY_JS_ACTION_JOB_MIN_RUNNING_BASE_PRICE:Ljava/lang/String; = "js_action_job_min_running_base_price"

.field public static final KEY_JS_ACTION_JOB_MIN_RUNNING_CTP:Ljava/lang/String; = "js_action_job_min_running_ctp"

.field public static final KEY_JS_ACTION_JOB_MIN_START_BASE_PRICE:Ljava/lang/String; = "js_action_job_min_start_base_price"

.field public static final KEY_JS_ACTION_JOB_MIN_START_CTP:Ljava/lang/String; = "js_action_job_min_start_ctp"

.field public static final KEY_JS_ACTION_JOB_TIMEOUT_PENALTY_BASE_PRICE:Ljava/lang/String; = "js_action_job_timeout_penalty_base_price"

.field public static final KEY_JS_ACTION_JOB_TIMEOUT_PENALTY_CTP:Ljava/lang/String; = "js_action_job_timeout_penalty_ctp"

.field public static final KEY_JS_HARD_CONSUMPTION_LIMIT:Ljava/lang/String; = "js_hard_consumption_limit"

.field public static final KEY_JS_INITIAL_CONSUMPTION_LIMIT:Ljava/lang/String; = "js_initial_consumption_limit"

.field public static final KEY_JS_MAX_SATIATED_BALANCE:Ljava/lang/String; = "js_max_satiated_balance"

.field public static final KEY_JS_MIN_SATIATED_BALANCE_EXEMPTED:Ljava/lang/String; = "js_min_satiated_balance_exempted"

.field public static final KEY_JS_MIN_SATIATED_BALANCE_HEADLESS_SYSTEM_APP:Ljava/lang/String; = "js_min_satiated_balance_headless_system_app"

.field public static final KEY_JS_MIN_SATIATED_BALANCE_OTHER_APP:Ljava/lang/String; = "js_min_satiated_balance_other_app"

.field public static final KEY_JS_REWARD_NOTIFICATION_INTERACTION_INSTANT:Ljava/lang/String; = "js_reward_notification_interaction_instant"

.field public static final KEY_JS_REWARD_NOTIFICATION_INTERACTION_MAX:Ljava/lang/String; = "js_reward_notification_interaction_max"

.field public static final KEY_JS_REWARD_NOTIFICATION_INTERACTION_ONGOING:Ljava/lang/String; = "js_reward_notification_interaction_ongoing"

.field public static final KEY_JS_REWARD_NOTIFICATION_SEEN_INSTANT:Ljava/lang/String; = "js_reward_notification_seen_instant"

.field public static final KEY_JS_REWARD_NOTIFICATION_SEEN_MAX:Ljava/lang/String; = "js_reward_notification_seen_max"

.field public static final KEY_JS_REWARD_NOTIFICATION_SEEN_ONGOING:Ljava/lang/String; = "js_reward_notification_seen_ongoing"

.field public static final KEY_JS_REWARD_OTHER_USER_INTERACTION_INSTANT:Ljava/lang/String; = "js_reward_other_user_interaction_instant"

.field public static final KEY_JS_REWARD_OTHER_USER_INTERACTION_MAX:Ljava/lang/String; = "js_reward_other_user_interaction_max"

.field public static final KEY_JS_REWARD_OTHER_USER_INTERACTION_ONGOING:Ljava/lang/String; = "js_reward_other_user_interaction_ongoing"

.field public static final KEY_JS_REWARD_TOP_ACTIVITY_INSTANT:Ljava/lang/String; = "js_reward_top_activity_instant"

.field public static final KEY_JS_REWARD_TOP_ACTIVITY_MAX:Ljava/lang/String; = "js_reward_top_activity_max"

.field public static final KEY_JS_REWARD_TOP_ACTIVITY_ONGOING:Ljava/lang/String; = "js_reward_top_activity_ongoing"

.field public static final KEY_JS_REWARD_WIDGET_INTERACTION_INSTANT:Ljava/lang/String; = "js_reward_widget_interaction_instant"

.field public static final KEY_JS_REWARD_WIDGET_INTERACTION_MAX:Ljava/lang/String; = "js_reward_widget_interaction_max"

.field public static final KEY_JS_REWARD_WIDGET_INTERACTION_ONGOING:Ljava/lang/String; = "js_reward_widget_interaction_ongoing"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TARE-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Landroid/app/tare/EconomyManager;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/app/tare/EconomyManager;->TAG:Ljava/lang/String;

    const/16 v0, 0x1f4

    invoke-static {v0}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v1

    sput-wide v1, Landroid/app/tare/EconomyManager;->DEFAULT_AM_MIN_SATIATED_BALANCE_EXEMPTED_CAKES:J

    nop

    const/16 v1, 0x100

    invoke-static {v1}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v1

    sput-wide v1, Landroid/app/tare/EconomyManager;->DEFAULT_AM_MIN_SATIATED_BALANCE_HEADLESS_SYSTEM_APP_CAKES:J

    const/16 v1, 0xa0

    invoke-static {v1}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v1

    sput-wide v1, Landroid/app/tare/EconomyManager;->DEFAULT_AM_MIN_SATIATED_BALANCE_OTHER_APP_CAKES:J

    const/16 v1, 0x3c0

    invoke-static {v1}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v1

    sput-wide v1, Landroid/app/tare/EconomyManager;->DEFAULT_AM_MAX_SATIATED_BALANCE_CAKES:J

    const/16 v1, 0xb40

    invoke-static {v1}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v1

    sput-wide v1, Landroid/app/tare/EconomyManager;->DEFAULT_AM_INITIAL_CONSUMPTION_LIMIT_CAKES:J

    const/16 v1, 0x3a98

    invoke-static {v1}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v2

    sput-wide v2, Landroid/app/tare/EconomyManager;->DEFAULT_AM_HARD_CONSUMPTION_LIMIT_CAKES:J

    const/4 v2, 0x0

    invoke-static {v2}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v3

    sput-wide v3, Landroid/app/tare/EconomyManager;->DEFAULT_AM_REWARD_TOP_ACTIVITY_INSTANT_CAKES:J

    invoke-static {v0}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v3

    sput-wide v3, Landroid/app/tare/EconomyManager;->DEFAULT_AM_REWARD_TOP_ACTIVITY_MAX_CAKES:J

    const/4 v3, 0x3

    invoke-static {v3}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v4

    sput-wide v4, Landroid/app/tare/EconomyManager;->DEFAULT_AM_REWARD_NOTIFICATION_SEEN_INSTANT_CAKES:J

    invoke-static {v2}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v4

    sput-wide v4, Landroid/app/tare/EconomyManager;->DEFAULT_AM_REWARD_NOTIFICATION_SEEN_ONGOING_CAKES:J

    const/16 v4, 0x3c

    invoke-static {v4}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v5

    sput-wide v5, Landroid/app/tare/EconomyManager;->DEFAULT_AM_REWARD_NOTIFICATION_SEEN_MAX_CAKES:J

    nop

    const/4 v5, 0x5

    invoke-static {v5}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v6

    sput-wide v6, Landroid/app/tare/EconomyManager;->DEFAULT_AM_REWARD_NOTIFICATION_SEEN_WITHIN_15_INSTANT_CAKES:J

    nop

    invoke-static {v2}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v6

    sput-wide v6, Landroid/app/tare/EconomyManager;->DEFAULT_AM_REWARD_NOTIFICATION_SEEN_WITHIN_15_ONGOING_CAKES:J

    nop

    invoke-static {v0}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v6

    sput-wide v6, Landroid/app/tare/EconomyManager;->DEFAULT_AM_REWARD_NOTIFICATION_SEEN_WITHIN_15_MAX_CAKES:J

    nop

    invoke-static {v5}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v6

    sput-wide v6, Landroid/app/tare/EconomyManager;->DEFAULT_AM_REWARD_NOTIFICATION_INTERACTION_INSTANT_CAKES:J

    nop

    invoke-static {v2}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v6

    sput-wide v6, Landroid/app/tare/EconomyManager;->DEFAULT_AM_REWARD_NOTIFICATION_INTERACTION_ONGOING_CAKES:J

    invoke-static {v0}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v6

    sput-wide v6, Landroid/app/tare/EconomyManager;->DEFAULT_AM_REWARD_NOTIFICATION_INTERACTION_MAX_CAKES:J

    const/16 v6, 0xa

    invoke-static {v6}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v7

    sput-wide v7, Landroid/app/tare/EconomyManager;->DEFAULT_AM_REWARD_WIDGET_INTERACTION_INSTANT_CAKES:J

    invoke-static {v2}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v7

    sput-wide v7, Landroid/app/tare/EconomyManager;->DEFAULT_AM_REWARD_WIDGET_INTERACTION_ONGOING_CAKES:J

    invoke-static {v0}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v7

    sput-wide v7, Landroid/app/tare/EconomyManager;->DEFAULT_AM_REWARD_WIDGET_INTERACTION_MAX_CAKES:J

    invoke-static {v6}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v7

    sput-wide v7, Landroid/app/tare/EconomyManager;->DEFAULT_AM_REWARD_OTHER_USER_INTERACTION_INSTANT_CAKES:J

    invoke-static {v2}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v7

    sput-wide v7, Landroid/app/tare/EconomyManager;->DEFAULT_AM_REWARD_OTHER_USER_INTERACTION_ONGOING_CAKES:J

    invoke-static {v0}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v7

    sput-wide v7, Landroid/app/tare/EconomyManager;->DEFAULT_AM_REWARD_OTHER_USER_INTERACTION_MAX_CAKES:J

    nop

    invoke-static {v3}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v7

    sput-wide v7, Landroid/app/tare/EconomyManager;->DEFAULT_AM_ACTION_ALARM_ALLOW_WHILE_IDLE_EXACT_WAKEUP_CTP_CAKES:J

    nop

    invoke-static {v3}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v7

    sput-wide v7, Landroid/app/tare/EconomyManager;->DEFAULT_AM_ACTION_ALARM_ALLOW_WHILE_IDLE_INEXACT_WAKEUP_CTP_CAKES:J

    invoke-static {v3}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v7

    sput-wide v7, Landroid/app/tare/EconomyManager;->DEFAULT_AM_ACTION_ALARM_EXACT_WAKEUP_CTP_CAKES:J

    invoke-static {v3}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v7

    sput-wide v7, Landroid/app/tare/EconomyManager;->DEFAULT_AM_ACTION_ALARM_INEXACT_WAKEUP_CTP_CAKES:J

    nop

    const/4 v0, 0x1

    invoke-static {v0}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v7

    sput-wide v7, Landroid/app/tare/EconomyManager;->DEFAULT_AM_ACTION_ALARM_ALLOW_WHILE_IDLE_EXACT_NONWAKEUP_CTP_CAKES:J

    invoke-static {v0}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v7

    sput-wide v7, Landroid/app/tare/EconomyManager;->DEFAULT_AM_ACTION_ALARM_EXACT_NONWAKEUP_CTP_CAKES:J

    nop

    invoke-static {v0}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v7

    sput-wide v7, Landroid/app/tare/EconomyManager;->DEFAULT_AM_ACTION_ALARM_ALLOW_WHILE_IDLE_INEXACT_NONWAKEUP_CTP_CAKES:J

    invoke-static {v0}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v7

    sput-wide v7, Landroid/app/tare/EconomyManager;->DEFAULT_AM_ACTION_ALARM_INEXACT_NONWAKEUP_CTP_CAKES:J

    invoke-static {v5}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v7

    sput-wide v7, Landroid/app/tare/EconomyManager;->DEFAULT_AM_ACTION_ALARM_ALARMCLOCK_CTP_CAKES:J

    invoke-static {v5}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v7

    sput-wide v7, Landroid/app/tare/EconomyManager;->DEFAULT_AM_ACTION_ALARM_ALLOW_WHILE_IDLE_EXACT_WAKEUP_BASE_PRICE_CAKES:J

    const/4 v7, 0x4

    invoke-static {v7}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v8

    sput-wide v8, Landroid/app/tare/EconomyManager;->DEFAULT_AM_ACTION_ALARM_ALLOW_WHILE_IDLE_INEXACT_WAKEUP_BASE_PRICE_CAKES:J

    invoke-static {v7}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v8

    sput-wide v8, Landroid/app/tare/EconomyManager;->DEFAULT_AM_ACTION_ALARM_EXACT_WAKEUP_BASE_PRICE_CAKES:J

    invoke-static {v3}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v8

    sput-wide v8, Landroid/app/tare/EconomyManager;->DEFAULT_AM_ACTION_ALARM_INEXACT_WAKEUP_BASE_PRICE_CAKES:J

    nop

    invoke-static {v3}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v8

    sput-wide v8, Landroid/app/tare/EconomyManager;->DEFAULT_AM_ACTION_ALARM_ALLOW_WHILE_IDLE_EXACT_NONWAKEUP_BASE_PRICE_CAKES:J

    nop

    const/4 v8, 0x2

    invoke-static {v8}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v9

    sput-wide v9, Landroid/app/tare/EconomyManager;->DEFAULT_AM_ACTION_ALARM_EXACT_NONWAKEUP_BASE_PRICE_CAKES:J

    nop

    invoke-static {v8}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v9

    sput-wide v9, Landroid/app/tare/EconomyManager;->DEFAULT_AM_ACTION_ALARM_ALLOW_WHILE_IDLE_INEXACT_NONWAKEUP_BASE_PRICE_CAKES:J

    nop

    invoke-static {v0}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v9

    sput-wide v9, Landroid/app/tare/EconomyManager;->DEFAULT_AM_ACTION_ALARM_INEXACT_NONWAKEUP_BASE_PRICE_CAKES:J

    invoke-static {v6}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v9

    sput-wide v9, Landroid/app/tare/EconomyManager;->DEFAULT_AM_ACTION_ALARM_ALARMCLOCK_BASE_PRICE_CAKES:J

    invoke-static {v1}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v9

    sput-wide v9, Landroid/app/tare/EconomyManager;->DEFAULT_JS_MIN_SATIATED_BALANCE_EXEMPTED_CAKES:J

    nop

    const/16 v9, 0x1d4c

    invoke-static {v9}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v9

    sput-wide v9, Landroid/app/tare/EconomyManager;->DEFAULT_JS_MIN_SATIATED_BALANCE_HEADLESS_SYSTEM_APP_CAKES:J

    const/16 v9, 0x7d0

    invoke-static {v9}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v9

    sput-wide v9, Landroid/app/tare/EconomyManager;->DEFAULT_JS_MIN_SATIATED_BALANCE_OTHER_APP_CAKES:J

    const v9, 0xea60

    invoke-static {v9}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v9

    sput-wide v9, Landroid/app/tare/EconomyManager;->DEFAULT_JS_MAX_SATIATED_BALANCE_CAKES:J

    const/16 v9, 0x7148

    invoke-static {v9}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v9

    sput-wide v9, Landroid/app/tare/EconomyManager;->DEFAULT_JS_INITIAL_CONSUMPTION_LIMIT_CAKES:J

    const v9, 0x3d090

    invoke-static {v9}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v9

    sput-wide v9, Landroid/app/tare/EconomyManager;->DEFAULT_JS_HARD_CONSUMPTION_LIMIT_CAKES:J

    invoke-static {v2}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v9

    sput-wide v9, Landroid/app/tare/EconomyManager;->DEFAULT_JS_REWARD_TOP_ACTIVITY_INSTANT_CAKES:J

    invoke-static {v1}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v9

    sput-wide v9, Landroid/app/tare/EconomyManager;->DEFAULT_JS_REWARD_TOP_ACTIVITY_MAX_CAKES:J

    invoke-static {v0}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v9

    sput-wide v9, Landroid/app/tare/EconomyManager;->DEFAULT_JS_REWARD_NOTIFICATION_SEEN_INSTANT_CAKES:J

    invoke-static {v2}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v9

    sput-wide v9, Landroid/app/tare/EconomyManager;->DEFAULT_JS_REWARD_NOTIFICATION_SEEN_ONGOING_CAKES:J

    invoke-static {v6}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v9

    sput-wide v9, Landroid/app/tare/EconomyManager;->DEFAULT_JS_REWARD_NOTIFICATION_SEEN_MAX_CAKES:J

    nop

    invoke-static {v5}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v9

    sput-wide v9, Landroid/app/tare/EconomyManager;->DEFAULT_JS_REWARD_NOTIFICATION_INTERACTION_INSTANT_CAKES:J

    nop

    invoke-static {v2}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v9

    sput-wide v9, Landroid/app/tare/EconomyManager;->DEFAULT_JS_REWARD_NOTIFICATION_INTERACTION_ONGOING_CAKES:J

    const/16 v1, 0x1388

    invoke-static {v1}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v9

    sput-wide v9, Landroid/app/tare/EconomyManager;->DEFAULT_JS_REWARD_NOTIFICATION_INTERACTION_MAX_CAKES:J

    invoke-static {v6}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v9

    sput-wide v9, Landroid/app/tare/EconomyManager;->DEFAULT_JS_REWARD_WIDGET_INTERACTION_INSTANT_CAKES:J

    invoke-static {v2}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v9

    sput-wide v9, Landroid/app/tare/EconomyManager;->DEFAULT_JS_REWARD_WIDGET_INTERACTION_ONGOING_CAKES:J

    invoke-static {v1}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v9

    sput-wide v9, Landroid/app/tare/EconomyManager;->DEFAULT_JS_REWARD_WIDGET_INTERACTION_MAX_CAKES:J

    invoke-static {v6}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v9

    sput-wide v9, Landroid/app/tare/EconomyManager;->DEFAULT_JS_REWARD_OTHER_USER_INTERACTION_INSTANT_CAKES:J

    invoke-static {v2}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v9

    sput-wide v9, Landroid/app/tare/EconomyManager;->DEFAULT_JS_REWARD_OTHER_USER_INTERACTION_ONGOING_CAKES:J

    invoke-static {v1}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v1

    sput-wide v1, Landroid/app/tare/EconomyManager;->DEFAULT_JS_REWARD_OTHER_USER_INTERACTION_MAX_CAKES:J

    invoke-static {v3}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v1

    sput-wide v1, Landroid/app/tare/EconomyManager;->DEFAULT_JS_ACTION_JOB_MAX_START_CTP_CAKES:J

    invoke-static {v8}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v1

    sput-wide v1, Landroid/app/tare/EconomyManager;->DEFAULT_JS_ACTION_JOB_MAX_RUNNING_CTP_CAKES:J

    invoke-static {v3}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v1

    sput-wide v1, Landroid/app/tare/EconomyManager;->DEFAULT_JS_ACTION_JOB_HIGH_START_CTP_CAKES:J

    invoke-static {v8}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v1

    sput-wide v1, Landroid/app/tare/EconomyManager;->DEFAULT_JS_ACTION_JOB_HIGH_RUNNING_CTP_CAKES:J

    invoke-static {v3}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v1

    sput-wide v1, Landroid/app/tare/EconomyManager;->DEFAULT_JS_ACTION_JOB_DEFAULT_START_CTP_CAKES:J

    invoke-static {v8}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v1

    sput-wide v1, Landroid/app/tare/EconomyManager;->DEFAULT_JS_ACTION_JOB_DEFAULT_RUNNING_CTP_CAKES:J

    invoke-static {v3}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v1

    sput-wide v1, Landroid/app/tare/EconomyManager;->DEFAULT_JS_ACTION_JOB_LOW_START_CTP_CAKES:J

    invoke-static {v8}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v1

    sput-wide v1, Landroid/app/tare/EconomyManager;->DEFAULT_JS_ACTION_JOB_LOW_RUNNING_CTP_CAKES:J

    invoke-static {v3}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v1

    sput-wide v1, Landroid/app/tare/EconomyManager;->DEFAULT_JS_ACTION_JOB_MIN_START_CTP_CAKES:J

    invoke-static {v8}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v1

    sput-wide v1, Landroid/app/tare/EconomyManager;->DEFAULT_JS_ACTION_JOB_MIN_RUNNING_CTP_CAKES:J

    const/16 v1, 0x1e

    invoke-static {v1}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v1

    sput-wide v1, Landroid/app/tare/EconomyManager;->DEFAULT_JS_ACTION_JOB_TIMEOUT_PENALTY_CTP_CAKES:J

    invoke-static {v6}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v1

    sput-wide v1, Landroid/app/tare/EconomyManager;->DEFAULT_JS_ACTION_JOB_MAX_START_BASE_PRICE_CAKES:J

    invoke-static {v5}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v1

    sput-wide v1, Landroid/app/tare/EconomyManager;->DEFAULT_JS_ACTION_JOB_MAX_RUNNING_BASE_PRICE_CAKES:J

    const/16 v1, 0x8

    invoke-static {v1}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v1

    sput-wide v1, Landroid/app/tare/EconomyManager;->DEFAULT_JS_ACTION_JOB_HIGH_START_BASE_PRICE_CAKES:J

    invoke-static {v7}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v1

    sput-wide v1, Landroid/app/tare/EconomyManager;->DEFAULT_JS_ACTION_JOB_HIGH_RUNNING_BASE_PRICE_CAKES:J

    const/4 v1, 0x6

    invoke-static {v1}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v1

    sput-wide v1, Landroid/app/tare/EconomyManager;->DEFAULT_JS_ACTION_JOB_DEFAULT_START_BASE_PRICE_CAKES:J

    invoke-static {v3}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v1

    sput-wide v1, Landroid/app/tare/EconomyManager;->DEFAULT_JS_ACTION_JOB_DEFAULT_RUNNING_BASE_PRICE_CAKES:J

    invoke-static {v7}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v1

    sput-wide v1, Landroid/app/tare/EconomyManager;->DEFAULT_JS_ACTION_JOB_LOW_START_BASE_PRICE_CAKES:J

    invoke-static {v8}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v1

    sput-wide v1, Landroid/app/tare/EconomyManager;->DEFAULT_JS_ACTION_JOB_LOW_RUNNING_BASE_PRICE_CAKES:J

    invoke-static {v8}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v1

    sput-wide v1, Landroid/app/tare/EconomyManager;->DEFAULT_JS_ACTION_JOB_MIN_START_BASE_PRICE_CAKES:J

    invoke-static {v0}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v0

    sput-wide v0, Landroid/app/tare/EconomyManager;->DEFAULT_JS_ACTION_JOB_MIN_RUNNING_BASE_PRICE_CAKES:J

    invoke-static {v4}, Landroid/app/tare/EconomyManager;->arcToCake(I)J

    move-result-wide v0

    sput-wide v0, Landroid/app/tare/EconomyManager;->DEFAULT_JS_ACTION_JOB_TIMEOUT_PENALTY_BASE_PRICE_CAKES:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static arcToCake(I)J
    .locals 4

    int-to-long v0, p0

    const-wide/32 v2, 0x3b9aca00

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public static parseCreditValue(Ljava/lang/String;J)J
    .locals 7

    if-eqz p0, :cond_8

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_2

    :cond_0
    const-string v0, "c"

    invoke-virtual {v1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-wide/16 v3, 0x1

    goto :goto_0

    :cond_1
    const-string v0, "ck"

    invoke-virtual {v1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-wide/16 v3, 0x1

    goto :goto_0

    :cond_2
    const-string v0, "A"

    invoke-virtual {v1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-wide/32 v3, 0x3b9aca00

    goto :goto_0

    :cond_3
    const-string v0, "ARC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x3

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-wide/32 v3, 0x3b9aca00

    :goto_0
    const-string v1, "k"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-wide/16 v1, 0x3e8

    mul-long/2addr v3, v1

    goto :goto_1

    :cond_4
    const-string v1, "M"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-wide/32 v1, 0xf4240

    mul-long/2addr v3, v1

    goto :goto_1

    :cond_5
    const-string v1, "G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-wide/32 v1, 0x3b9aca00

    mul-long/2addr v3, v1

    :cond_6
    :goto_1
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    mul-long/2addr v1, v3

    return-wide v1

    :catch_0
    move-exception v1

    sget-object v2, Landroid/app/tare/EconomyManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Malformed config string: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    return-wide p1

    :cond_7
    sget-object v0, Landroid/app/tare/EconomyManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t determine units of credit value: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-wide p1

    :cond_8
    :goto_2
    return-wide p1
.end method
