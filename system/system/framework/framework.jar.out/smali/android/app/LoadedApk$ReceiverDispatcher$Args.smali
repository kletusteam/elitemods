.class final Landroid/app/LoadedApk$ReceiverDispatcher$Args;
.super Landroid/content/BroadcastReceiver$PendingResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/LoadedApk$ReceiverDispatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "Args"
.end annotation


# instance fields
.field private mCurIntent:Landroid/content/Intent;

.field private mDispatched:Z

.field private final mOrdered:Z

.field private mRunCalled:Z

.field final synthetic this$0:Landroid/app/LoadedApk$ReceiverDispatcher;


# direct methods
.method public constructor <init>(Landroid/app/LoadedApk$ReceiverDispatcher;Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;ZZI)V
    .locals 12

    move-object v10, p0

    move-object v11, p1

    iput-object v11, v10, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->this$0:Landroid/app/LoadedApk$ReceiverDispatcher;

    nop

    iget-boolean v0, v11, Landroid/app/LoadedApk$ReceiverDispatcher;->mRegistered:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    :goto_0
    move v4, v0

    iget-object v0, v11, Landroid/app/LoadedApk$ReceiverDispatcher;->mIIntentReceiver:Landroid/content/IIntentReceiver$Stub;

    invoke-virtual {v0}, Landroid/content/IIntentReceiver$Stub;->asBinder()Landroid/os/IBinder;

    move-result-object v7

    invoke-virtual {p2}, Landroid/content/Intent;->getFlags()I

    move-result v9

    move-object v0, p0

    move v1, p3

    move-object/from16 v2, p4

    move-object/from16 v3, p5

    move/from16 v5, p6

    move/from16 v6, p7

    move/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Landroid/content/BroadcastReceiver$PendingResult;-><init>(ILjava/lang/String;Landroid/os/Bundle;IZZLandroid/os/IBinder;II)V

    move-object v0, p2

    iput-object v0, v10, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->mCurIntent:Landroid/content/Intent;

    move/from16 v1, p6

    iput-boolean v1, v10, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->mOrdered:Z

    return-void
.end method


# virtual methods
.method public final getRunnable()Ljava/lang/Runnable;
    .locals 1

    new-instance v0, Landroid/app/LoadedApk$ReceiverDispatcher$Args$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Landroid/app/LoadedApk$ReceiverDispatcher$Args$$ExternalSyntheticLambda0;-><init>(Landroid/app/LoadedApk$ReceiverDispatcher$Args;)V

    return-object v0
.end method

.method synthetic lambda$getRunnable$0$android-app-LoadedApk$ReceiverDispatcher$Args()V
    .locals 10

    goto/32 :goto_57

    nop

    :goto_0
    new-instance v3, Ljava/lang/RuntimeException;

    goto/32 :goto_10

    nop

    :goto_1
    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    goto/32 :goto_95

    nop

    :goto_2
    iget-boolean v6, p0, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->mDispatched:Z

    goto/32 :goto_8c

    nop

    :goto_3
    iget-object v5, p0, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->mCurIntent:Landroid/content/Intent;

    goto/32 :goto_15

    nop

    :goto_4
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_3d

    nop

    :goto_5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/32 :goto_37

    nop

    :goto_6
    if-nez v5, :cond_0

    goto/32 :goto_97

    :cond_0
    goto/32 :goto_88

    nop

    :goto_7
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v2

    goto/32 :goto_80

    nop

    :goto_8
    if-nez v4, :cond_1

    goto/32 :goto_4a

    :cond_1
    goto/32 :goto_1c

    nop

    :goto_9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/32 :goto_2

    nop

    :goto_a
    invoke-virtual {v0}, Landroid/content/BroadcastReceiver;->getPendingResult()Landroid/content/BroadcastReceiver$PendingResult;

    move-result-object v3

    goto/32 :goto_8b

    nop

    :goto_b
    iget-object v6, v6, Landroid/app/LoadedApk$ReceiverDispatcher;->mReceiver:Landroid/content/BroadcastReceiver;

    goto/32 :goto_69

    nop

    :goto_c
    iget-object v8, v8, Landroid/app/LoadedApk$ReceiverDispatcher;->mReceiver:Landroid/content/BroadcastReceiver;

    goto/32 :goto_31

    nop

    :goto_d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_27

    nop

    :goto_e
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/32 :goto_3e

    nop

    :goto_f
    if-nez v1, :cond_2

    goto/32 :goto_82

    :cond_2
    goto/32 :goto_44

    nop

    :goto_10
    new-instance v5, Ljava/lang/StringBuilder;

    goto/32 :goto_77

    nop

    :goto_11
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_6c

    nop

    :goto_12
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_68

    nop

    :goto_13
    const-string/jumbo v5, "seq"

    goto/32 :goto_1f

    nop

    :goto_14
    sget-boolean v8, Landroid/app/ActivityThread;->DEBUG_BROADCAST:Z

    goto/32 :goto_87

    nop

    :goto_15
    invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_12

    nop

    :goto_16
    const/4 v4, -0x1

    goto/32 :goto_13

    nop

    :goto_17
    iget-boolean v1, p0, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->mOrdered:Z

    goto/32 :goto_42

    nop

    :goto_18
    invoke-virtual {p0}, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->finish()V

    :goto_19
    goto/32 :goto_99

    nop

    :goto_1a
    const-string v6, "Null intent being dispatched, mDispatched="

    goto/32 :goto_9

    nop

    :goto_1b
    const-string v9, "Finishing failed broadcast to "

    goto/32 :goto_78

    nop

    :goto_1c
    iget-object v5, p0, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->this$0:Landroid/app/LoadedApk$ReceiverDispatcher;

    goto/32 :goto_66

    nop

    :goto_1d
    goto/16 :goto_72

    :goto_1e
    goto/32 :goto_71

    nop

    :goto_1f
    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    goto/32 :goto_4f

    nop

    :goto_20
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1b

    nop

    :goto_21
    iget-object v9, p0, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->this$0:Landroid/app/LoadedApk$ReceiverDispatcher;

    goto/32 :goto_59

    nop

    :goto_22
    const/4 v5, 0x0

    goto/32 :goto_73

    nop

    :goto_23
    return-void

    :goto_24
    if-nez v3, :cond_3

    goto/32 :goto_30

    :cond_3
    :goto_25
    goto/32 :goto_a

    nop

    :goto_26
    if-nez v2, :cond_4

    goto/32 :goto_52

    :cond_4
    goto/32 :goto_53

    nop

    :goto_27
    iget-object v5, p0, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->this$0:Landroid/app/LoadedApk$ReceiverDispatcher;

    goto/32 :goto_6a

    nop

    :goto_28
    iget-object v8, p0, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->this$0:Landroid/app/LoadedApk$ReceiverDispatcher;

    goto/32 :goto_c

    nop

    :goto_29
    if-nez v5, :cond_5

    goto/32 :goto_82

    :cond_5
    goto/32 :goto_f

    nop

    :goto_2a
    iget-object v5, p0, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->this$0:Landroid/app/LoadedApk$ReceiverDispatcher;

    goto/32 :goto_7d

    nop

    :goto_2b
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_60

    nop

    :goto_2c
    const-string v6, "Error receiving broadcast "

    goto/32 :goto_83

    nop

    :goto_2d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_2b

    nop

    :goto_2e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_8a

    nop

    :goto_2f
    return-void

    :goto_30
    goto/32 :goto_85

    nop

    :goto_31
    invoke-virtual {v3, v8, v7}, Landroid/app/Instrumentation;->onException(Ljava/lang/Object;Ljava/lang/Throwable;)Z

    move-result v3

    goto/32 :goto_24

    nop

    :goto_32
    const-string v8, "broadcastReceiveReg: "

    goto/32 :goto_6e

    nop

    :goto_33
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_96

    nop

    :goto_34
    iget-boolean v6, p0, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->mRunCalled:Z

    goto/32 :goto_4c

    nop

    :goto_35
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/32 :goto_11

    nop

    :goto_36
    if-eqz v4, :cond_6

    goto/32 :goto_46

    :cond_6
    goto/32 :goto_8e

    nop

    :goto_37
    iget-object v6, p0, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->this$0:Landroid/app/LoadedApk$ReceiverDispatcher;

    goto/32 :goto_7b

    nop

    :goto_38
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_32

    nop

    :goto_39
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto/32 :goto_7e

    nop

    :goto_3a
    const-string v6, ", run() has already been called"

    goto/32 :goto_1d

    nop

    :goto_3b
    goto/16 :goto_4a

    :goto_3c
    goto/32 :goto_65

    nop

    :goto_3d
    const-string v5, " to "

    goto/32 :goto_d

    nop

    :goto_3e
    const-string v6, " in "

    goto/32 :goto_5

    nop

    :goto_3f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_3

    nop

    :goto_40
    iget-object v0, v0, Landroid/app/LoadedApk$ReceiverDispatcher;->mReceiver:Landroid/content/BroadcastReceiver;

    goto/32 :goto_17

    nop

    :goto_41
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_4

    nop

    :goto_42
    sget-boolean v2, Landroid/app/ActivityThread;->DEBUG_BROADCAST:Z

    goto/32 :goto_93

    nop

    :goto_43
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_6f

    nop

    :goto_44
    sget-boolean v5, Landroid/app/ActivityThread;->DEBUG_BROADCAST:Z

    goto/32 :goto_6

    nop

    :goto_45
    invoke-static {v6, v5}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :goto_46
    goto/32 :goto_22

    nop

    :goto_47
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_2e

    nop

    :goto_48
    iget-object v3, v3, Landroid/app/LoadedApk$ReceiverDispatcher;->mInstrumentation:Landroid/app/Instrumentation;

    goto/32 :goto_28

    nop

    :goto_49
    throw v3

    :goto_4a
    goto/32 :goto_2a

    nop

    :goto_4b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_5b

    nop

    :goto_4c
    if-nez v6, :cond_7

    goto/32 :goto_1e

    :cond_7
    goto/32 :goto_3a

    nop

    :goto_4d
    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_47

    nop

    :goto_4e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/32 :goto_91

    nop

    :goto_4f
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_67

    nop

    :goto_50
    if-nez v8, :cond_8

    goto/32 :goto_5f

    :cond_8
    goto/32 :goto_58

    nop

    :goto_51
    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_52
    goto/32 :goto_7

    nop

    :goto_53
    iget-object v2, p0, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->mCurIntent:Landroid/content/Intent;

    goto/32 :goto_16

    nop

    :goto_54
    iget-object v6, p0, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->this$0:Landroid/app/LoadedApk$ReceiverDispatcher;

    goto/32 :goto_b

    nop

    :goto_55
    if-nez v3, :cond_9

    goto/32 :goto_30

    :cond_9
    goto/32 :goto_5a

    nop

    :goto_56
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto/32 :goto_74

    nop

    :goto_57
    iget-object v0, p0, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->this$0:Landroid/app/LoadedApk$ReceiverDispatcher;

    goto/32 :goto_40

    nop

    :goto_58
    if-nez v1, :cond_a

    goto/32 :goto_5f

    :cond_a
    goto/32 :goto_14

    nop

    :goto_59
    iget-object v9, v9, Landroid/app/LoadedApk$ReceiverDispatcher;->mReceiver:Landroid/content/BroadcastReceiver;

    goto/32 :goto_98

    nop

    :goto_5a
    iget-object v3, p0, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->this$0:Landroid/app/LoadedApk$ReceiverDispatcher;

    goto/32 :goto_48

    nop

    :goto_5b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_4d

    nop

    :goto_5c
    const-string v6, "Finishing null broadcast to "

    goto/32 :goto_62

    nop

    :goto_5d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_5c

    nop

    :goto_5e
    invoke-virtual {p0, v2}, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->sendFinished(Landroid/app/IActivityManager;)V

    :goto_5f
    goto/32 :goto_79

    nop

    :goto_60
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_51

    nop

    :goto_61
    iget-object v3, v3, Landroid/app/LoadedApk$ReceiverDispatcher;->mInstrumentation:Landroid/app/Instrumentation;

    goto/32 :goto_55

    nop

    :goto_62
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/32 :goto_54

    nop

    :goto_63
    iget-boolean v8, v8, Landroid/app/LoadedApk$ReceiverDispatcher;->mRegistered:Z

    goto/32 :goto_50

    nop

    :goto_64
    const-string v5, "Dispatching broadcast "

    goto/32 :goto_3f

    nop

    :goto_65
    const-wide/16 v5, 0x40

    goto/32 :goto_8d

    nop

    :goto_66
    iget-boolean v5, v5, Landroid/app/LoadedApk$ReceiverDispatcher;->mForgotten:Z

    goto/32 :goto_9a

    nop

    :goto_67
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_64

    nop

    :goto_68
    const-string v5, " seq="

    goto/32 :goto_41

    nop

    :goto_69
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/32 :goto_33

    nop

    :goto_6a
    iget-object v5, v5, Landroid/app/LoadedApk$ReceiverDispatcher;->mReceiver:Landroid/content/BroadcastReceiver;

    goto/32 :goto_4b

    nop

    :goto_6b
    const-string v5, " mOrderedHint="

    goto/32 :goto_2d

    nop

    :goto_6c
    const-string v6, "LoadedApk"

    goto/32 :goto_45

    nop

    :goto_6d
    new-instance v7, Ljava/lang/StringBuilder;

    goto/32 :goto_38

    nop

    :goto_6e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    goto/32 :goto_1

    nop

    :goto_6f
    iget-object v5, p0, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->this$0:Landroid/app/LoadedApk$ReceiverDispatcher;

    goto/32 :goto_94

    nop

    :goto_70
    goto/16 :goto_25

    :catch_0
    move-exception v7

    goto/32 :goto_90

    nop

    :goto_71
    const-string v6, ""

    :goto_72
    goto/32 :goto_35

    nop

    :goto_73
    iput-object v5, p0, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->mCurIntent:Landroid/content/Intent;

    goto/32 :goto_86

    nop

    :goto_74
    invoke-static {v5, v6, v7}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    :goto_75
    :try_start_0
    iget-object v7, p0, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->this$0:Landroid/app/LoadedApk$ReceiverDispatcher;

    iget-object v7, v7, Landroid/app/LoadedApk$ReceiverDispatcher;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/content/Intent;->setExtrasClassLoader(Ljava/lang/ClassLoader;)V

    invoke-static {v4}, Landroid/app/ActivityThread;->isProtectedBroadcast(Landroid/content/Intent;)Z

    move-result v8

    iget-object v9, p0, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->this$0:Landroid/app/LoadedApk$ReceiverDispatcher;

    iget-object v9, v9, Landroid/app/LoadedApk$ReceiverDispatcher;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getAttributionSource()Landroid/content/AttributionSource;

    move-result-object v9

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->prepareToEnterProcess(ZLandroid/content/AttributionSource;)V

    invoke-virtual {p0, v7}, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->setExtrasClassLoader(Ljava/lang/ClassLoader;)V

    invoke-virtual {v0, p0}, Landroid/content/BroadcastReceiver;->setPendingResult(Landroid/content/BroadcastReceiver$PendingResult;)V

    invoke-virtual {v0}, Landroid/content/BroadcastReceiver;->getMonitor()Landroid/os/perfdebug/BroadcastMonitor;

    move-result-object v8

    invoke-virtual {v8}, Landroid/os/perfdebug/BroadcastMonitor;->monitorReceiveBegin()V

    iget-object v8, p0, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->this$0:Landroid/app/LoadedApk$ReceiverDispatcher;

    iget-object v8, v8, Landroid/app/LoadedApk$ReceiverDispatcher;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v8, v4}, Landroid/content/BroadcastReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    invoke-virtual {v0}, Landroid/content/BroadcastReceiver;->getMonitor()Landroid/os/perfdebug/BroadcastMonitor;

    move-result-object v8

    invoke-virtual {v8, v1, v4, v0}, Landroid/os/perfdebug/BroadcastMonitor;->monitorReceiveEnd(ZLandroid/content/Intent;Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_70

    nop

    :goto_76
    invoke-direct {v3, v5, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/32 :goto_49

    nop

    :goto_77
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_2c

    nop

    :goto_78
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    goto/32 :goto_21

    nop

    :goto_79
    iget-object v3, p0, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->this$0:Landroid/app/LoadedApk$ReceiverDispatcher;

    goto/32 :goto_61

    nop

    :goto_7a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_6b

    nop

    :goto_7b
    iget-object v6, v6, Landroid/app/LoadedApk$ReceiverDispatcher;->mReceiver:Landroid/content/BroadcastReceiver;

    goto/32 :goto_4e

    nop

    :goto_7c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1a

    nop

    :goto_7d
    iget-boolean v5, v5, Landroid/app/LoadedApk$ReceiverDispatcher;->mRegistered:Z

    goto/32 :goto_29

    nop

    :goto_7e
    invoke-static {v3, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_7f
    goto/32 :goto_5e

    nop

    :goto_80
    iget-object v4, p0, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->mCurIntent:Landroid/content/Intent;

    goto/32 :goto_36

    nop

    :goto_81
    invoke-virtual {p0, v2}, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->sendFinished(Landroid/app/IActivityManager;)V

    :goto_82
    goto/32 :goto_23

    nop

    :goto_83
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/32 :goto_e

    nop

    :goto_84
    if-nez v0, :cond_b

    goto/32 :goto_4a

    :cond_b
    goto/32 :goto_8

    nop

    :goto_85
    invoke-static {v5, v6}, Landroid/os/Trace;->traceEnd(J)V

    goto/32 :goto_0

    nop

    :goto_86
    const/4 v5, 0x1

    goto/32 :goto_8f

    nop

    :goto_87
    if-nez v8, :cond_c

    goto/32 :goto_7f

    :cond_c
    goto/32 :goto_9b

    nop

    :goto_88
    new-instance v5, Ljava/lang/StringBuilder;

    goto/32 :goto_5d

    nop

    :goto_89
    iput-boolean v5, p0, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->mRunCalled:Z

    goto/32 :goto_84

    nop

    :goto_8a
    const-string v5, "  mRegistered="

    goto/32 :goto_43

    nop

    :goto_8b
    if-nez v3, :cond_d

    goto/32 :goto_19

    :cond_d
    goto/32 :goto_18

    nop

    :goto_8c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/32 :goto_34

    nop

    :goto_8d
    invoke-static {v5, v6}, Landroid/os/Trace;->isTagEnabled(J)Z

    move-result v7

    goto/32 :goto_92

    nop

    :goto_8e
    new-instance v5, Ljava/lang/StringBuilder;

    goto/32 :goto_7c

    nop

    :goto_8f
    iput-boolean v5, p0, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->mDispatched:Z

    goto/32 :goto_89

    nop

    :goto_90
    iget-object v8, p0, Landroid/app/LoadedApk$ReceiverDispatcher$Args;->this$0:Landroid/app/LoadedApk$ReceiverDispatcher;

    goto/32 :goto_63

    nop

    :goto_91
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_76

    nop

    :goto_92
    if-nez v7, :cond_e

    goto/32 :goto_75

    :cond_e
    goto/32 :goto_6d

    nop

    :goto_93
    const-string v3, "ActivityThread"

    goto/32 :goto_26

    nop

    :goto_94
    iget-boolean v5, v5, Landroid/app/LoadedApk$ReceiverDispatcher;->mRegistered:Z

    goto/32 :goto_7a

    nop

    :goto_95
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    goto/32 :goto_56

    nop

    :goto_96
    invoke-static {v3, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_97
    goto/32 :goto_81

    nop

    :goto_98
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    goto/32 :goto_39

    nop

    :goto_99
    invoke-static {v5, v6}, Landroid/os/Trace;->traceEnd(J)V

    goto/32 :goto_2f

    nop

    :goto_9a
    if-nez v5, :cond_f

    goto/32 :goto_3c

    :cond_f
    goto/32 :goto_3b

    nop

    :goto_9b
    new-instance v8, Ljava/lang/StringBuilder;

    goto/32 :goto_20

    nop
.end method
