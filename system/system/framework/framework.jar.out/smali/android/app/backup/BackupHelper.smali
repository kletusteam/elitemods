.class public interface abstract Landroid/app/backup/BackupHelper;
.super Ljava/lang/Object;


# virtual methods
.method public abstract performBackup(Landroid/os/ParcelFileDescriptor;Landroid/app/backup/BackupDataOutput;Landroid/os/ParcelFileDescriptor;)V
.end method

.method public abstract restoreEntity(Landroid/app/backup/BackupDataInputStream;)V
.end method

.method public abstract writeNewStateDescription(Landroid/os/ParcelFileDescriptor;)V
.end method
