.class public abstract Landroid/app/FragmentHostCallback;
.super Landroid/app/FragmentContainer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/app/FragmentContainer;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private mAllLoaderManagers:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Landroid/app/LoaderManager;",
            ">;"
        }
    .end annotation
.end field

.field private mCheckedForLoaderManager:Z

.field final mContext:Landroid/content/Context;

.field final mFragmentManager:Landroid/app/FragmentManagerImpl;

.field private final mHandler:Landroid/os/Handler;

.field private mLoaderManager:Landroid/app/LoaderManagerImpl;

.field private mLoadersStarted:Z

.field private mRetainLoaders:Z

.field final mWindowAnimations:I


# direct methods
.method constructor <init>(Landroid/app/Activity;)V
    .locals 2

    iget-object v0, p1, Landroid/app/Activity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p1, v0, v1}, Landroid/app/FragmentHostCallback;-><init>(Landroid/app/Activity;Landroid/content/Context;Landroid/os/Handler;I)V

    return-void
.end method

.method constructor <init>(Landroid/app/Activity;Landroid/content/Context;Landroid/os/Handler;I)V
    .locals 1

    invoke-direct {p0}, Landroid/app/FragmentContainer;-><init>()V

    new-instance v0, Landroid/app/FragmentManagerImpl;

    invoke-direct {v0}, Landroid/app/FragmentManagerImpl;-><init>()V

    iput-object v0, p0, Landroid/app/FragmentHostCallback;->mFragmentManager:Landroid/app/FragmentManagerImpl;

    iput-object p1, p0, Landroid/app/FragmentHostCallback;->mActivity:Landroid/app/Activity;

    iput-object p2, p0, Landroid/app/FragmentHostCallback;->mContext:Landroid/content/Context;

    iput-object p3, p0, Landroid/app/FragmentHostCallback;->mHandler:Landroid/os/Handler;

    iput p4, p0, Landroid/app/FragmentHostCallback;->mWindowAnimations:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;I)V
    .locals 2

    instance-of v0, p1, Landroid/app/Activity;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Landroid/app/Activity;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {p1, p2}, Landroid/app/FragmentHostCallback;->chooseHandler(Landroid/content/Context;Landroid/os/Handler;)Landroid/os/Handler;

    move-result-object v1

    invoke-direct {p0, v0, p1, v1, p3}, Landroid/app/FragmentHostCallback;-><init>(Landroid/app/Activity;Landroid/content/Context;Landroid/os/Handler;I)V

    return-void
.end method

.method private static chooseHandler(Landroid/content/Context;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 2

    if-nez p1, :cond_0

    instance-of v0, p0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Landroid/app/Activity;

    iget-object v1, v0, Landroid/app/Activity;->mHandler:Landroid/os/Handler;

    return-object v1

    :cond_0
    return-object p1
.end method


# virtual methods
.method doLoaderDestroy()V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/app/FragmentHostCallback;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    if-eqz v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_3

    nop

    :goto_3
    return-void

    :goto_4
    goto/32 :goto_5

    nop

    :goto_5
    invoke-virtual {v0}, Landroid/app/LoaderManagerImpl;->doDestroy()V

    goto/32 :goto_1

    nop
.end method

.method doLoaderRetain()V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    if-eqz v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    goto/32 :goto_4

    nop

    :goto_3
    iget-object v0, p0, Landroid/app/FragmentHostCallback;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    goto/32 :goto_0

    nop

    :goto_4
    invoke-virtual {v0}, Landroid/app/LoaderManagerImpl;->doRetain()V

    goto/32 :goto_5

    nop

    :goto_5
    return-void
.end method

.method doLoaderStart()V
    .locals 3

    goto/32 :goto_11

    nop

    :goto_0
    iput-boolean v0, p0, Landroid/app/FragmentHostCallback;->mLoadersStarted:Z

    goto/32 :goto_4

    nop

    :goto_1
    iput-object v1, p0, Landroid/app/FragmentHostCallback;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    :goto_2
    goto/32 :goto_13

    nop

    :goto_3
    if-eqz v1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_d

    nop

    :goto_4
    iget-object v1, p0, Landroid/app/FragmentHostCallback;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    goto/32 :goto_f

    nop

    :goto_5
    if-nez v0, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_b

    nop

    :goto_6
    invoke-virtual {v1}, Landroid/app/LoaderManagerImpl;->doStart()V

    goto/32 :goto_7

    nop

    :goto_7
    goto :goto_2

    :goto_8
    goto/32 :goto_12

    nop

    :goto_9
    const/4 v0, 0x1

    goto/32 :goto_0

    nop

    :goto_a
    invoke-virtual {p0, v2, v0, v1}, Landroid/app/FragmentHostCallback;->getLoaderManager(Ljava/lang/String;ZZ)Landroid/app/LoaderManagerImpl;

    move-result-object v1

    goto/32 :goto_1

    nop

    :goto_b
    return-void

    :goto_c
    goto/32 :goto_9

    nop

    :goto_d
    const/4 v1, 0x0

    goto/32 :goto_e

    nop

    :goto_e
    const-string v2, "(root)"

    goto/32 :goto_a

    nop

    :goto_f
    if-nez v1, :cond_2

    goto/32 :goto_8

    :cond_2
    goto/32 :goto_6

    nop

    :goto_10
    return-void

    :goto_11
    iget-boolean v0, p0, Landroid/app/FragmentHostCallback;->mLoadersStarted:Z

    goto/32 :goto_5

    nop

    :goto_12
    iget-boolean v1, p0, Landroid/app/FragmentHostCallback;->mCheckedForLoaderManager:Z

    goto/32 :goto_3

    nop

    :goto_13
    iput-boolean v0, p0, Landroid/app/FragmentHostCallback;->mCheckedForLoaderManager:Z

    goto/32 :goto_10

    nop
.end method

.method doLoaderStop(Z)V
    .locals 2

    goto/32 :goto_a

    nop

    :goto_0
    const/4 v1, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    iput-boolean v1, p0, Landroid/app/FragmentHostCallback;->mLoadersStarted:Z

    goto/32 :goto_7

    nop

    :goto_2
    if-eqz v0, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_b

    nop

    :goto_3
    invoke-virtual {v0}, Landroid/app/LoaderManagerImpl;->doRetain()V

    goto/32 :goto_e

    nop

    :goto_4
    return-void

    :goto_5
    goto/32 :goto_0

    nop

    :goto_6
    return-void

    :goto_7
    if-nez p1, :cond_1

    goto/32 :goto_f

    :cond_1
    goto/32 :goto_3

    nop

    :goto_8
    invoke-virtual {v0}, Landroid/app/LoaderManagerImpl;->doStop()V

    :goto_9
    goto/32 :goto_6

    nop

    :goto_a
    iput-boolean p1, p0, Landroid/app/FragmentHostCallback;->mRetainLoaders:Z

    goto/32 :goto_10

    nop

    :goto_b
    return-void

    :goto_c
    goto/32 :goto_d

    nop

    :goto_d
    iget-boolean v1, p0, Landroid/app/FragmentHostCallback;->mLoadersStarted:Z

    goto/32 :goto_11

    nop

    :goto_e
    goto :goto_9

    :goto_f
    goto/32 :goto_8

    nop

    :goto_10
    iget-object v0, p0, Landroid/app/FragmentHostCallback;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    goto/32 :goto_2

    nop

    :goto_11
    if-eqz v1, :cond_2

    goto/32 :goto_5

    :cond_2
    goto/32 :goto_4

    nop
.end method

.method dumpLoaders(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3

    goto/32 :goto_19

    nop

    :goto_0
    iget-object v0, p0, Landroid/app/FragmentHostCallback;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    goto/32 :goto_c

    nop

    :goto_1
    const-string v2, "  "

    goto/32 :goto_10

    nop

    :goto_2
    iget-object v0, p0, Landroid/app/FragmentHostCallback;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    goto/32 :goto_5

    nop

    :goto_3
    const-string/jumbo v0, "mLoadersStarted="

    goto/32 :goto_14

    nop

    :goto_4
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_7

    nop

    :goto_5
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_f

    nop

    :goto_6
    const-string v0, ":"

    goto/32 :goto_11

    nop

    :goto_7
    const-string v0, "Loader Manager "

    goto/32 :goto_a

    nop

    :goto_8
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_13

    nop

    :goto_9
    iget-object v0, p0, Landroid/app/FragmentHostCallback;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    goto/32 :goto_17

    nop

    :goto_a
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_0

    nop

    :goto_b
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    goto/32 :goto_9

    nop

    :goto_c
    invoke-static {v0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    goto/32 :goto_8

    nop

    :goto_d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_15

    nop

    :goto_e
    return-void

    :goto_f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_18

    nop

    :goto_10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_d

    nop

    :goto_11
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/32 :goto_2

    nop

    :goto_12
    iget-boolean v0, p0, Landroid/app/FragmentHostCallback;->mLoadersStarted:Z

    goto/32 :goto_b

    nop

    :goto_13
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_6

    nop

    :goto_14
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_12

    nop

    :goto_15
    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/app/LoaderManagerImpl;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    :goto_16
    goto/32 :goto_e

    nop

    :goto_17
    if-nez v0, :cond_0

    goto/32 :goto_16

    :cond_0
    goto/32 :goto_4

    nop

    :goto_18
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/32 :goto_1

    nop

    :goto_19
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_3

    nop
.end method

.method getActivity()Landroid/app/Activity;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Landroid/app/FragmentHostCallback;->mActivity:Landroid/app/Activity;

    goto/32 :goto_0

    nop
.end method

.method getContext()Landroid/content/Context;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Landroid/app/FragmentHostCallback;->mContext:Landroid/content/Context;

    goto/32 :goto_0

    nop
.end method

.method getFragmentManagerImpl()Landroid/app/FragmentManagerImpl;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/app/FragmentHostCallback;->mFragmentManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method getHandler()Landroid/os/Handler;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Landroid/app/FragmentHostCallback;->mHandler:Landroid/os/Handler;

    goto/32 :goto_0

    nop
.end method

.method getLoaderManager(Ljava/lang/String;ZZ)Landroid/app/LoaderManagerImpl;
    .locals 2

    goto/32 :goto_14

    nop

    :goto_0
    check-cast v0, Landroid/app/LoaderManagerImpl;

    goto/32 :goto_f

    nop

    :goto_1
    new-instance v1, Landroid/app/LoaderManagerImpl;

    goto/32 :goto_4

    nop

    :goto_2
    invoke-virtual {v0}, Landroid/app/LoaderManagerImpl;->doStart()V

    :goto_3
    goto/32 :goto_13

    nop

    :goto_4
    invoke-direct {v1, p1, p0, p2}, Landroid/app/LoaderManagerImpl;-><init>(Ljava/lang/String;Landroid/app/FragmentHostCallback;Z)V

    goto/32 :goto_c

    nop

    :goto_5
    if-nez v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_e

    nop

    :goto_6
    goto :goto_3

    :goto_7
    goto/32 :goto_15

    nop

    :goto_8
    iget-object v0, p0, Landroid/app/FragmentHostCallback;->mAllLoaderManagers:Landroid/util/ArrayMap;

    goto/32 :goto_b

    nop

    :goto_9
    new-instance v0, Landroid/util/ArrayMap;

    goto/32 :goto_18

    nop

    :goto_a
    if-eqz v1, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_2

    nop

    :goto_b
    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_c
    move-object v0, v1

    goto/32 :goto_d

    nop

    :goto_d
    iget-object v1, p0, Landroid/app/FragmentHostCallback;->mAllLoaderManagers:Landroid/util/ArrayMap;

    goto/32 :goto_12

    nop

    :goto_e
    iget-boolean v1, v0, Landroid/app/LoaderManagerImpl;->mStarted:Z

    goto/32 :goto_a

    nop

    :goto_f
    if-eqz v0, :cond_2

    goto/32 :goto_7

    :cond_2
    goto/32 :goto_17

    nop

    :goto_10
    iput-object v0, p0, Landroid/app/FragmentHostCallback;->mAllLoaderManagers:Landroid/util/ArrayMap;

    :goto_11
    goto/32 :goto_8

    nop

    :goto_12
    invoke-virtual {v1, p1, v0}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_6

    nop

    :goto_13
    return-object v0

    :goto_14
    iget-object v0, p0, Landroid/app/FragmentHostCallback;->mAllLoaderManagers:Landroid/util/ArrayMap;

    goto/32 :goto_16

    nop

    :goto_15
    if-nez p2, :cond_3

    goto/32 :goto_3

    :cond_3
    goto/32 :goto_5

    nop

    :goto_16
    if-eqz v0, :cond_4

    goto/32 :goto_11

    :cond_4
    goto/32 :goto_9

    nop

    :goto_17
    if-nez p3, :cond_5

    goto/32 :goto_7

    :cond_5
    goto/32 :goto_1

    nop

    :goto_18
    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    goto/32 :goto_10

    nop
.end method

.method getLoaderManagerImpl()Landroid/app/LoaderManagerImpl;
    .locals 3

    goto/32 :goto_8

    nop

    :goto_0
    const-string v2, "(root)"

    goto/32 :goto_5

    nop

    :goto_1
    return-object v0

    :goto_2
    iget-boolean v1, p0, Landroid/app/FragmentHostCallback;->mLoadersStarted:Z

    goto/32 :goto_0

    nop

    :goto_3
    return-object v0

    :goto_4
    goto/32 :goto_7

    nop

    :goto_5
    invoke-virtual {p0, v2, v1, v0}, Landroid/app/FragmentHostCallback;->getLoaderManager(Ljava/lang/String;ZZ)Landroid/app/LoaderManagerImpl;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_6
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_3

    nop

    :goto_7
    const/4 v0, 0x1

    goto/32 :goto_a

    nop

    :goto_8
    iget-object v0, p0, Landroid/app/FragmentHostCallback;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    goto/32 :goto_6

    nop

    :goto_9
    iput-object v0, p0, Landroid/app/FragmentHostCallback;->mLoaderManager:Landroid/app/LoaderManagerImpl;

    goto/32 :goto_1

    nop

    :goto_a
    iput-boolean v0, p0, Landroid/app/FragmentHostCallback;->mCheckedForLoaderManager:Z

    goto/32 :goto_2

    nop
.end method

.method getRetainLoaders()Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-boolean v0, p0, Landroid/app/FragmentHostCallback;->mRetainLoaders:Z

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method inactivateFragment(Ljava/lang/String;)V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/app/FragmentHostCallback;->mAllLoaderManagers:Landroid/util/ArrayMap;

    goto/32 :goto_5

    nop

    :goto_1
    invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_2
    goto/32 :goto_3

    nop

    :goto_3
    return-void

    :goto_4
    if-eqz v1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_a

    nop

    :goto_5
    if-nez v0, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_7

    nop

    :goto_6
    iget-object v1, p0, Landroid/app/FragmentHostCallback;->mAllLoaderManagers:Landroid/util/ArrayMap;

    goto/32 :goto_1

    nop

    :goto_7
    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_8
    check-cast v0, Landroid/app/LoaderManagerImpl;

    goto/32 :goto_b

    nop

    :goto_9
    iget-boolean v1, v0, Landroid/app/LoaderManagerImpl;->mRetaining:Z

    goto/32 :goto_4

    nop

    :goto_a
    invoke-virtual {v0}, Landroid/app/LoaderManagerImpl;->doDestroy()V

    goto/32 :goto_6

    nop

    :goto_b
    if-nez v0, :cond_2

    goto/32 :goto_2

    :cond_2
    goto/32 :goto_9

    nop
.end method

.method public onAttachFragment(Landroid/app/Fragment;)V
    .locals 0

    return-void
.end method

.method public onDump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onFindViewById(I)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)TT;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract onGetHost()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation
.end method

.method public onGetLayoutInflater()Landroid/view/LayoutInflater;
    .locals 2

    iget-object v0, p0, Landroid/app/FragmentHostCallback;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    return-object v0
.end method

.method public onGetWindowAnimations()I
    .locals 1

    iget v0, p0, Landroid/app/FragmentHostCallback;->mWindowAnimations:I

    return v0
.end method

.method public onHasView()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onHasWindowAnimations()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onInvalidateOptionsMenu()V
    .locals 0

    return-void
.end method

.method public onRequestPermissionsFromFragment(Landroid/app/Fragment;[Ljava/lang/String;I)V
    .locals 0

    return-void
.end method

.method public onShouldSaveFragmentState(Landroid/app/Fragment;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onStartActivityAsUserFromFragment(Landroid/app/Fragment;Landroid/content/Intent;ILandroid/os/Bundle;Landroid/os/UserHandle;)V
    .locals 2

    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Landroid/app/FragmentHostCallback;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p2, p5}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Starting activity with a requestCode requires a FragmentActivity host"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onStartActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 2

    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Landroid/app/FragmentHostCallback;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Starting activity with a requestCode requires a FragmentActivity host"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onStartIntentSenderFromFragment(Landroid/app/Fragment;Landroid/content/IntentSender;ILandroid/content/Intent;IIILandroid/os/Bundle;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/IntentSender$SendIntentException;
        }
    .end annotation

    const/4 v0, -0x1

    move v1, p3

    if-ne v1, v0, :cond_0

    move-object v0, p0

    iget-object v2, v0, Landroid/app/FragmentHostCallback;->mContext:Landroid/content/Context;

    move-object v3, p2

    move-object v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-virtual/range {v2 .. v8}, Landroid/content/Context;->startIntentSender(Landroid/content/IntentSender;Landroid/content/Intent;IIILandroid/os/Bundle;)V

    return-void

    :cond_0
    move-object v0, p0

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Starting intent sender with a requestCode requires a FragmentActivity host"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public onUseFragmentManagerInflaterFactory()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method reportLoaderStart()V
    .locals 4

    goto/32 :goto_17

    nop

    :goto_0
    invoke-virtual {v3, v2}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_14

    nop

    :goto_1
    goto :goto_d

    :goto_2
    goto/32 :goto_10

    nop

    :goto_3
    if-lt v2, v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_e

    nop

    :goto_4
    invoke-virtual {v3}, Landroid/app/LoaderManagerImpl;->finishRetain()V

    goto/32 :goto_6

    nop

    :goto_5
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_1

    nop

    :goto_6
    invoke-virtual {v3}, Landroid/app/LoaderManagerImpl;->doReportStart()V

    goto/32 :goto_5

    nop

    :goto_7
    iget-object v3, p0, Landroid/app/FragmentHostCallback;->mAllLoaderManagers:Landroid/util/ArrayMap;

    goto/32 :goto_0

    nop

    :goto_8
    add-int/lit8 v2, v2, -0x1

    goto/32 :goto_15

    nop

    :goto_9
    add-int/lit8 v2, v0, -0x1

    :goto_a
    goto/32 :goto_11

    nop

    :goto_b
    aput-object v3, v1, v2

    goto/32 :goto_8

    nop

    :goto_c
    const/4 v2, 0x0

    :goto_d
    goto/32 :goto_3

    nop

    :goto_e
    aget-object v3, v1, v2

    goto/32 :goto_4

    nop

    :goto_f
    new-array v1, v0, [Landroid/app/LoaderManagerImpl;

    goto/32 :goto_9

    nop

    :goto_10
    return-void

    :goto_11
    if-gez v2, :cond_1

    goto/32 :goto_16

    :cond_1
    goto/32 :goto_7

    nop

    :goto_12
    invoke-virtual {v0}, Landroid/util/ArrayMap;->size()I

    move-result v0

    goto/32 :goto_f

    nop

    :goto_13
    if-nez v0, :cond_2

    goto/32 :goto_2

    :cond_2
    goto/32 :goto_12

    nop

    :goto_14
    check-cast v3, Landroid/app/LoaderManagerImpl;

    goto/32 :goto_b

    nop

    :goto_15
    goto :goto_a

    :goto_16
    goto/32 :goto_c

    nop

    :goto_17
    iget-object v0, p0, Landroid/app/FragmentHostCallback;->mAllLoaderManagers:Landroid/util/ArrayMap;

    goto/32 :goto_13

    nop
.end method

.method restoreLoaderNonConfig(Landroid/util/ArrayMap;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Landroid/app/LoaderManager;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_4

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p1}, Landroid/util/ArrayMap;->size()I

    move-result v1

    :goto_2
    goto/32 :goto_9

    nop

    :goto_3
    const/4 v0, 0x0

    goto/32 :goto_1

    nop

    :goto_4
    if-nez p1, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_3

    nop

    :goto_5
    invoke-virtual {p1, v0}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_b

    nop

    :goto_6
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_7

    nop

    :goto_7
    goto :goto_2

    :goto_8
    goto/32 :goto_c

    nop

    :goto_9
    if-lt v0, v1, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_5

    nop

    :goto_a
    invoke-virtual {v2, p0}, Landroid/app/LoaderManagerImpl;->updateHostController(Landroid/app/FragmentHostCallback;)V

    goto/32 :goto_6

    nop

    :goto_b
    check-cast v2, Landroid/app/LoaderManagerImpl;

    goto/32 :goto_a

    nop

    :goto_c
    iput-object p1, p0, Landroid/app/FragmentHostCallback;->mAllLoaderManagers:Landroid/util/ArrayMap;

    goto/32 :goto_0

    nop
.end method

.method retainLoaderNonConfig()Landroid/util/ArrayMap;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Landroid/app/LoaderManager;",
            ">;"
        }
    .end annotation

    goto/32 :goto_4

    nop

    :goto_0
    iget-object v4, p0, Landroid/app/FragmentHostCallback;->mAllLoaderManagers:Landroid/util/ArrayMap;

    goto/32 :goto_f

    nop

    :goto_1
    aput-object v4, v2, v3

    goto/32 :goto_1a

    nop

    :goto_2
    if-nez v3, :cond_0

    goto/32 :goto_2f

    :cond_0
    goto/32 :goto_7

    nop

    :goto_3
    iget-boolean v6, v5, Landroid/app/LoaderManagerImpl;->mRetaining:Z

    goto/32 :goto_29

    nop

    :goto_4
    const/4 v0, 0x0

    goto/32 :goto_a

    nop

    :goto_5
    iget-object v7, v5, Landroid/app/LoaderManagerImpl;->mWho:Ljava/lang/String;

    goto/32 :goto_20

    nop

    :goto_6
    aget-object v5, v2, v4

    goto/32 :goto_1c

    nop

    :goto_7
    iget-boolean v6, v5, Landroid/app/LoaderManagerImpl;->mStarted:Z

    goto/32 :goto_28

    nop

    :goto_8
    return-object v1

    :goto_9
    goto/32 :goto_15

    nop

    :goto_a
    iget-object v1, p0, Landroid/app/FragmentHostCallback;->mAllLoaderManagers:Landroid/util/ArrayMap;

    goto/32 :goto_22

    nop

    :goto_b
    if-lt v4, v1, :cond_1

    goto/32 :goto_14

    :cond_1
    goto/32 :goto_6

    nop

    :goto_c
    goto :goto_12

    :goto_d
    goto/32 :goto_16

    nop

    :goto_e
    if-gez v3, :cond_2

    goto/32 :goto_d

    :cond_2
    goto/32 :goto_0

    nop

    :goto_f
    invoke-virtual {v4, v3}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_18

    nop

    :goto_10
    iget-object v6, p0, Landroid/app/FragmentHostCallback;->mAllLoaderManagers:Landroid/util/ArrayMap;

    goto/32 :goto_5

    nop

    :goto_11
    add-int/lit8 v3, v1, -0x1

    :goto_12
    goto/32 :goto_e

    nop

    :goto_13
    goto :goto_26

    :goto_14
    goto/32 :goto_1f

    nop

    :goto_15
    const/4 v1, 0x0

    goto/32 :goto_1b

    nop

    :goto_16
    invoke-virtual {p0}, Landroid/app/FragmentHostCallback;->getRetainLoaders()Z

    move-result v3

    goto/32 :goto_25

    nop

    :goto_17
    add-int/lit8 v4, v4, 0x1

    goto/32 :goto_13

    nop

    :goto_18
    check-cast v4, Landroid/app/LoaderManagerImpl;

    goto/32 :goto_1

    nop

    :goto_19
    invoke-virtual {v1}, Landroid/util/ArrayMap;->size()I

    move-result v1

    goto/32 :goto_2d

    nop

    :goto_1a
    add-int/lit8 v3, v3, -0x1

    goto/32 :goto_c

    nop

    :goto_1b
    return-object v1

    :goto_1c
    iget-boolean v6, v5, Landroid/app/LoaderManagerImpl;->mRetaining:Z

    goto/32 :goto_1e

    nop

    :goto_1d
    const/4 v0, 0x1

    goto/32 :goto_23

    nop

    :goto_1e
    if-eqz v6, :cond_3

    goto/32 :goto_2f

    :cond_3
    goto/32 :goto_2

    nop

    :goto_1f
    if-nez v0, :cond_4

    goto/32 :goto_9

    :cond_4
    goto/32 :goto_2a

    nop

    :goto_20
    invoke-virtual {v6, v7}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_21
    goto/32 :goto_17

    nop

    :goto_22
    if-nez v1, :cond_5

    goto/32 :goto_14

    :cond_5
    goto/32 :goto_19

    nop

    :goto_23
    goto :goto_21

    :goto_24
    goto/32 :goto_27

    nop

    :goto_25
    const/4 v4, 0x0

    :goto_26
    goto/32 :goto_b

    nop

    :goto_27
    invoke-virtual {v5}, Landroid/app/LoaderManagerImpl;->doDestroy()V

    goto/32 :goto_10

    nop

    :goto_28
    if-eqz v6, :cond_6

    goto/32 :goto_2c

    :cond_6
    goto/32 :goto_2b

    nop

    :goto_29
    if-nez v6, :cond_7

    goto/32 :goto_24

    :cond_7
    goto/32 :goto_1d

    nop

    :goto_2a
    iget-object v1, p0, Landroid/app/FragmentHostCallback;->mAllLoaderManagers:Landroid/util/ArrayMap;

    goto/32 :goto_8

    nop

    :goto_2b
    invoke-virtual {v5}, Landroid/app/LoaderManagerImpl;->doStart()V

    :goto_2c
    goto/32 :goto_2e

    nop

    :goto_2d
    new-array v2, v1, [Landroid/app/LoaderManagerImpl;

    goto/32 :goto_11

    nop

    :goto_2e
    invoke-virtual {v5}, Landroid/app/LoaderManagerImpl;->doRetain()V

    :goto_2f
    goto/32 :goto_3

    nop
.end method
