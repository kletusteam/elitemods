.class public interface abstract Landroid/app/KeyguardManager$WeakEscrowTokenRemovedListener;
.super Ljava/lang/Object;


# annotations
.annotation runtime Landroid/annotation/SystemApi;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/KeyguardManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "WeakEscrowTokenRemovedListener"
.end annotation


# virtual methods
.method public abstract onWeakEscrowTokenRemoved(JLandroid/os/UserHandle;)V
.end method
