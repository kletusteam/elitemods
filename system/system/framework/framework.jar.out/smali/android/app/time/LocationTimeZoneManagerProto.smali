.class public final Landroid/app/time/LocationTimeZoneManagerProto;
.super Ljava/lang/Object;


# static fields
.field public static final CONTROLLER_STATE_CERTAIN:I = 0x5

.field public static final CONTROLLER_STATE_DESTROYED:I = 0x7

.field public static final CONTROLLER_STATE_FAILED:I = 0x6

.field public static final CONTROLLER_STATE_INITIALIZING:I = 0x3

.field public static final CONTROLLER_STATE_PROVIDERS_INITIALIZING:I = 0x1

.field public static final CONTROLLER_STATE_STOPPED:I = 0x2

.field public static final CONTROLLER_STATE_UNCERTAIN:I = 0x4

.field public static final CONTROLLER_STATE_UNKNOWN:I = 0x0

.field public static final TIME_ZONE_PROVIDER_STATE_CERTAIN:I = 0x2

.field public static final TIME_ZONE_PROVIDER_STATE_DESTROYED:I = 0x6

.field public static final TIME_ZONE_PROVIDER_STATE_DISABLED:I = 0x4

.field public static final TIME_ZONE_PROVIDER_STATE_INITIALIZING:I = 0x1

.field public static final TIME_ZONE_PROVIDER_STATE_PERM_FAILED:I = 0x5

.field public static final TIME_ZONE_PROVIDER_STATE_UNCERTAIN:I = 0x3

.field public static final TIME_ZONE_PROVIDER_STATE_UNKNOWN:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
