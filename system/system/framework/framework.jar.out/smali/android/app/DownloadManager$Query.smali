.class public Landroid/app/DownloadManager$Query;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/DownloadManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Query"
.end annotation


# static fields
.field public static final ORDER_ASCENDING:I = 0x1

.field public static final ORDER_DESCENDING:I = 0x2


# instance fields
.field private mAppendedClause:Ljava/lang/String;

.field private mColumnAppData:Ljava/lang/String;

.field private mColumnNotificationPackage:Ljava/lang/String;

.field private mFilterString:Ljava/lang/String;

.field private mIds:[J

.field private mOnlyIncludeVisibleInDownloadsUi:Z

.field private mOrderByColumn:Ljava/lang/String;

.field private mOrderDirection:I

.field private mStatusFlags:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/app/DownloadManager$Query;->mIds:[J

    iput-object v0, p0, Landroid/app/DownloadManager$Query;->mStatusFlags:Ljava/lang/Integer;

    iput-object v0, p0, Landroid/app/DownloadManager$Query;->mFilterString:Ljava/lang/String;

    const-string v0, "lastmod"

    iput-object v0, p0, Landroid/app/DownloadManager$Query;->mOrderByColumn:Ljava/lang/String;

    const/4 v0, 0x2

    iput v0, p0, Landroid/app/DownloadManager$Query;->mOrderDirection:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/app/DownloadManager$Query;->mOnlyIncludeVisibleInDownloadsUi:Z

    return-void
.end method

.method private joinStrings(Ljava/lang/String;Ljava/lang/Iterable;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x1

    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    if-nez v1, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private statusClause(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "status"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method addExtraSelectionParts(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_e

    nop

    :goto_0
    const/4 v3, 0x2

    goto/32 :goto_1f

    nop

    :goto_1
    aput-object v3, v0, v2

    goto/32 :goto_17

    nop

    :goto_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    goto/32 :goto_1d

    nop

    :goto_3
    if-eqz v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_10

    nop

    :goto_4
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_5
    goto/32 :goto_18

    nop

    :goto_6
    iget-object v5, p0, Landroid/app/DownloadManager$Query;->mColumnAppData:Ljava/lang/String;

    goto/32 :goto_16

    nop

    :goto_7
    aput-object v2, v0, v1

    goto/32 :goto_8

    nop

    :goto_8
    invoke-static {v4, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_9
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_a
    goto/32 :goto_15

    nop

    :goto_b
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_c
    goto/32 :goto_19

    nop

    :goto_d
    new-array v0, v3, [Ljava/lang/Object;

    goto/32 :goto_1e

    nop

    :goto_e
    iget-object v0, p0, Landroid/app/DownloadManager$Query;->mColumnAppData:Ljava/lang/String;

    goto/32 :goto_11

    nop

    :goto_f
    new-array v0, v3, [Ljava/lang/Object;

    goto/32 :goto_14

    nop

    :goto_10
    iget-object v0, p0, Landroid/app/DownloadManager$Query;->mAppendedClause:Ljava/lang/String;

    goto/32 :goto_4

    nop

    :goto_11
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    goto/32 :goto_20

    nop

    :goto_12
    invoke-static {v4, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_b

    nop

    :goto_13
    if-eqz v0, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_d

    nop

    :goto_14
    const-string/jumbo v3, "notificationpackage"

    goto/32 :goto_1

    nop

    :goto_15
    iget-object v0, p0, Landroid/app/DownloadManager$Query;->mAppendedClause:Ljava/lang/String;

    goto/32 :goto_1b

    nop

    :goto_16
    aput-object v5, v0, v1

    goto/32 :goto_12

    nop

    :goto_17
    iget-object v2, p0, Landroid/app/DownloadManager$Query;->mColumnNotificationPackage:Ljava/lang/String;

    goto/32 :goto_7

    nop

    :goto_18
    return-void

    :goto_19
    iget-object v0, p0, Landroid/app/DownloadManager$Query;->mColumnNotificationPackage:Ljava/lang/String;

    goto/32 :goto_2

    nop

    :goto_1a
    const/4 v2, 0x0

    goto/32 :goto_0

    nop

    :goto_1b
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    goto/32 :goto_3

    nop

    :goto_1c
    aput-object v5, v0, v2

    goto/32 :goto_6

    nop

    :goto_1d
    if-eqz v0, :cond_2

    goto/32 :goto_a

    :cond_2
    goto/32 :goto_f

    nop

    :goto_1e
    const-string v5, "entity"

    goto/32 :goto_1c

    nop

    :goto_1f
    const-string v4, "%s=\'%s\'"

    goto/32 :goto_13

    nop

    :goto_20
    const/4 v1, 0x1

    goto/32 :goto_1a

    nop
.end method

.method public orderBy(Ljava/lang/String;I)Landroid/app/DownloadManager$Query;
    .locals 3

    const/4 v0, 0x1

    if-eq p2, v0, :cond_1

    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid direction: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    invoke-static {}, Landroid/app/DownloadManager;->isInternationalVersion()Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "_id"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iput-object v0, p0, Landroid/app/DownloadManager$Query;->mOrderByColumn:Ljava/lang/String;

    iput p2, p0, Landroid/app/DownloadManager$Query;->mOrderDirection:I

    return-object p0

    :cond_2
    const-string v0, "last_modified_timestamp"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "lastmod"

    iput-object v0, p0, Landroid/app/DownloadManager$Query;->mOrderByColumn:Ljava/lang/String;

    goto :goto_1

    :cond_3
    const-string/jumbo v0, "total_size"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string/jumbo v0, "total_bytes"

    iput-object v0, p0, Landroid/app/DownloadManager$Query;->mOrderByColumn:Ljava/lang/String;

    :goto_1
    iput p2, p0, Landroid/app/DownloadManager$Query;->mOrderDirection:I

    return-object p0

    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot order by "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method runQuery(Landroid/content/ContentResolver;[Ljava/lang/String;Landroid/net/Uri;)Landroid/database/Cursor;
    .locals 12

    goto/32 :goto_74

    nop

    :goto_0
    iget-object v4, p0, Landroid/app/DownloadManager$Query;->mStatusFlags:Ljava/lang/Integer;

    goto/32 :goto_4a

    nop

    :goto_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_6f

    nop

    :goto_2
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto/32 :goto_8e

    nop

    :goto_3
    const-string v2, "deleted != \'1\'"

    goto/32 :goto_6b

    nop

    :goto_4
    const/16 v4, 0xc0

    goto/32 :goto_31

    nop

    :goto_5
    const/4 v3, 0x0

    goto/32 :goto_37

    nop

    :goto_6
    move-object v2, p2

    goto/32 :goto_7b

    nop

    :goto_7
    const-string v3, " AND "

    goto/32 :goto_1a

    nop

    :goto_8
    array-length v3, v2

    :goto_9
    goto/32 :goto_64

    nop

    :goto_a
    if-nez v2, :cond_0

    goto/32 :goto_3d

    :cond_0
    goto/32 :goto_3f

    nop

    :goto_b
    iget v2, p0, Landroid/app/DownloadManager$Query;->mOrderDirection:I

    goto/32 :goto_c

    nop

    :goto_c
    if-eq v2, v0, :cond_1

    goto/32 :goto_62

    :cond_1
    goto/32 :goto_50

    nop

    :goto_d
    iget-object v5, p0, Landroid/app/DownloadManager$Query;->mFilterString:Ljava/lang/String;

    goto/32 :goto_99

    nop

    :goto_e
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_f
    goto/32 :goto_3e

    nop

    :goto_10
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_11

    nop

    :goto_11
    const-string v5, ")"

    goto/32 :goto_98

    nop

    :goto_12
    if-nez v4, :cond_2

    goto/32 :goto_60

    :cond_2
    goto/32 :goto_4

    nop

    :goto_13
    if-eqz v4, :cond_3

    goto/32 :goto_15

    :cond_3
    goto/32 :goto_43

    nop

    :goto_14
    goto/16 :goto_72

    :goto_15
    goto/32 :goto_71

    nop

    :goto_16
    move-object v10, v0

    goto/32 :goto_9a

    nop

    :goto_17
    const-string v4, "%"

    goto/32 :goto_8d

    nop

    :goto_18
    invoke-direct {p0, v3, v6}, Landroid/app/DownloadManager$Query;->joinStrings(Ljava/lang/String;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v9

    goto/32 :goto_b

    nop

    :goto_19
    array-length v2, v8

    goto/32 :goto_5e

    nop

    :goto_1a
    if-nez v2, :cond_4

    goto/32 :goto_42

    :cond_4
    goto/32 :goto_57

    nop

    :goto_1b
    invoke-direct {p0, v5, v4}, Landroid/app/DownloadManager$Query;->statusClause(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_88

    nop

    :goto_1c
    iget-object v2, p0, Landroid/app/DownloadManager$Query;->mOrderByColumn:Ljava/lang/String;

    goto/32 :goto_2e

    nop

    :goto_1d
    const/16 v5, 0x258

    goto/32 :goto_8c

    nop

    :goto_1e
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_1d

    nop

    :goto_1f
    move-object v4, v8

    goto/32 :goto_23

    nop

    :goto_20
    iget-object v2, p0, Landroid/app/DownloadManager$Query;->mStatusFlags:Ljava/lang/Integer;

    goto/32 :goto_7

    nop

    :goto_21
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto/32 :goto_30

    nop

    :goto_22
    invoke-direct {p0, v9, v5}, Landroid/app/DownloadManager$Query;->statusClause(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_58

    nop

    :goto_23
    move-object v5, v11

    goto/32 :goto_2

    nop

    :goto_24
    invoke-direct {p0, v5, v4}, Landroid/app/DownloadManager$Query;->statusClause(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_77

    nop

    :goto_25
    invoke-direct {p0, v5, v4}, Landroid/app/DownloadManager$Query;->statusClause(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_65

    nop

    :goto_26
    invoke-direct {p0, v5, v4}, Landroid/app/DownloadManager$Query;->statusClause(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_68

    nop

    :goto_27
    if-nez v4, :cond_5

    goto/32 :goto_f

    :cond_5
    goto/32 :goto_4c

    nop

    :goto_28
    iget-boolean v2, p0, Landroid/app/DownloadManager$Query;->mOnlyIncludeVisibleInDownloadsUi:Z

    goto/32 :goto_5c

    nop

    :goto_29
    const/16 v4, 0xc3

    goto/32 :goto_26

    nop

    :goto_2a
    if-nez v4, :cond_6

    goto/32 :goto_66

    :cond_6
    goto/32 :goto_5d

    nop

    :goto_2b
    invoke-direct {p0, v9, v5}, Landroid/app/DownloadManager$Query;->statusClause(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_10

    nop

    :goto_2c
    if-nez v4, :cond_7

    goto/32 :goto_53

    :cond_7
    goto/32 :goto_7a

    nop

    :goto_2d
    const-string v5, "("

    goto/32 :goto_97

    nop

    :goto_2e
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_3a

    nop

    :goto_2f
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_6a

    nop

    :goto_30
    and-int/lit8 v4, v4, 0x10

    goto/32 :goto_27

    nop

    :goto_31
    invoke-direct {p0, v5, v4}, Landroid/app/DownloadManager$Query;->statusClause(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_5f

    nop

    :goto_32
    const/16 v4, 0xc6

    goto/32 :goto_25

    nop

    :goto_33
    invoke-virtual {p0, v6}, Landroid/app/DownloadManager$Query;->addExtraSelectionParts(Ljava/util/List;)V

    goto/32 :goto_3

    nop

    :goto_34
    new-instance v3, Ljava/lang/StringBuilder;

    goto/32 :goto_95

    nop

    :goto_35
    invoke-static {}, Landroid/app/DownloadManager;->isInternationalVersion()Z

    move-result v4

    goto/32 :goto_73

    nop

    :goto_36
    if-nez v4, :cond_8

    goto/32 :goto_56

    :cond_8
    goto/32 :goto_94

    nop

    :goto_37
    goto/16 :goto_9

    :goto_38
    goto/32 :goto_8

    nop

    :goto_39
    const/16 v5, 0x190

    goto/32 :goto_82

    nop

    :goto_3a
    const-string v2, " "

    goto/32 :goto_2f

    nop

    :goto_3b
    invoke-direct {p0, v4, v2}, Landroid/app/DownloadManager$Query;->joinStrings(Ljava/lang/String;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_41

    nop

    :goto_3c
    invoke-static {v2, v8}, Landroid/app/DownloadManager;->getWhereArgsForIds([J[Ljava/lang/String;)[Ljava/lang/String;

    :goto_3d
    goto/32 :goto_79

    nop

    :goto_3e
    const-string v4, " OR "

    goto/32 :goto_3b

    nop

    :goto_3f
    invoke-static {v2}, Landroid/app/DownloadManager;->getWhereClauseForIds([J)Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_5a

    nop

    :goto_40
    and-int/lit8 v4, v4, 0x4

    goto/32 :goto_2a

    nop

    :goto_41
    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_42
    goto/32 :goto_28

    nop

    :goto_43
    move v4, v3

    goto/32 :goto_14

    nop

    :goto_44
    const/16 v4, 0xc2

    goto/32 :goto_8a

    nop

    :goto_45
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto/32 :goto_4d

    nop

    :goto_46
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_0

    nop

    :goto_47
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_92

    nop

    :goto_48
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto/32 :goto_40

    nop

    :goto_49
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_e

    nop

    :goto_4a
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto/32 :goto_89

    nop

    :goto_4b
    new-array v8, v7, [Ljava/lang/String;

    goto/32 :goto_63

    nop

    :goto_4c
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_6e

    nop

    :goto_4d
    move-object v0, p1

    goto/32 :goto_6

    nop

    :goto_4e
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto/32 :goto_8f

    nop

    :goto_4f
    const/4 v0, 0x0

    goto/32 :goto_67

    nop

    :goto_50
    const-string v0, "ASC"

    goto/32 :goto_61

    nop

    :goto_51
    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_19

    nop

    :goto_52
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_53
    goto/32 :goto_54

    nop

    :goto_54
    iget-object v4, p0, Landroid/app/DownloadManager$Query;->mStatusFlags:Ljava/lang/Integer;

    goto/32 :goto_21

    nop

    :goto_55
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_56
    goto/32 :goto_78

    nop

    :goto_57
    new-instance v2, Ljava/util/ArrayList;

    goto/32 :goto_46

    nop

    :goto_58
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_1e

    nop

    :goto_59
    move v7, v4

    goto/32 :goto_4b

    nop

    :goto_5a
    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_83

    nop

    :goto_5b
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_29

    nop

    :goto_5c
    if-nez v2, :cond_9

    goto/32 :goto_6d

    :cond_9
    goto/32 :goto_8b

    nop

    :goto_5d
    const/16 v4, 0xc1

    goto/32 :goto_1b

    nop

    :goto_5e
    sub-int/2addr v2, v0

    goto/32 :goto_34

    nop

    :goto_5f
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_60
    goto/32 :goto_80

    nop

    :goto_61
    goto/16 :goto_85

    :goto_62
    goto/32 :goto_84

    nop

    :goto_63
    const/4 v0, 0x1

    goto/32 :goto_7c

    nop

    :goto_64
    iget-object v4, p0, Landroid/app/DownloadManager$Query;->mFilterString:Ljava/lang/String;

    goto/32 :goto_13

    nop

    :goto_65
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_66
    goto/32 :goto_81

    nop

    :goto_67
    iget-object v2, p0, Landroid/app/DownloadManager$Query;->mIds:[J

    goto/32 :goto_87

    nop

    :goto_68
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_75

    nop

    :goto_69
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_47

    nop

    :goto_6a
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_45

    nop

    :goto_6b
    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_18

    nop

    :goto_6c
    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_6d
    goto/32 :goto_33

    nop

    :goto_6e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_2d

    nop

    :goto_6f
    aput-object v3, v8, v2

    :goto_70
    goto/32 :goto_20

    nop

    :goto_71
    add-int/lit8 v4, v3, 0x1

    :goto_72
    goto/32 :goto_59

    nop

    :goto_73
    if-eqz v4, :cond_a

    goto/32 :goto_66

    :cond_a
    goto/32 :goto_32

    nop

    :goto_74
    move-object v1, p3

    goto/32 :goto_69

    nop

    :goto_75
    const/16 v4, 0xc4

    goto/32 :goto_24

    nop

    :goto_76
    const-string/jumbo v2, "title LIKE ?"

    goto/32 :goto_51

    nop

    :goto_77
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_35

    nop

    :goto_78
    iget-object v4, p0, Landroid/app/DownloadManager$Query;->mStatusFlags:Ljava/lang/Integer;

    goto/32 :goto_4e

    nop

    :goto_79
    iget-object v2, p0, Landroid/app/DownloadManager$Query;->mFilterString:Ljava/lang/String;

    goto/32 :goto_96

    nop

    :goto_7a
    const/16 v4, 0xc8

    goto/32 :goto_91

    nop

    :goto_7b
    move-object v3, v9

    goto/32 :goto_1f

    nop

    :goto_7c
    if-gtz v7, :cond_b

    goto/32 :goto_70

    :cond_b
    goto/32 :goto_a

    nop

    :goto_7d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_1

    nop

    :goto_7e
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto/32 :goto_90

    nop

    :goto_7f
    invoke-direct {p0, v5, v4}, Landroid/app/DownloadManager$Query;->statusClause(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_55

    nop

    :goto_80
    iget-object v4, p0, Landroid/app/DownloadManager$Query;->mStatusFlags:Ljava/lang/Integer;

    goto/32 :goto_48

    nop

    :goto_81
    iget-object v4, p0, Landroid/app/DownloadManager$Query;->mStatusFlags:Ljava/lang/Integer;

    goto/32 :goto_7e

    nop

    :goto_82
    const-string v9, ">="

    goto/32 :goto_22

    nop

    :goto_83
    iget-object v2, p0, Landroid/app/DownloadManager$Query;->mIds:[J

    goto/32 :goto_3c

    nop

    :goto_84
    const-string v0, "DESC"

    :goto_85
    goto/32 :goto_16

    nop

    :goto_86
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1c

    nop

    :goto_87
    if-eqz v2, :cond_c

    goto/32 :goto_38

    :cond_c
    goto/32 :goto_5

    nop

    :goto_88
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_44

    nop

    :goto_89
    and-int/2addr v4, v0

    goto/32 :goto_93

    nop

    :goto_8a
    invoke-direct {p0, v5, v4}, Landroid/app/DownloadManager$Query;->statusClause(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_5b

    nop

    :goto_8b
    const-string v2, "is_visible_in_downloads_ui != \'0\'"

    goto/32 :goto_6c

    nop

    :goto_8c
    const-string v9, "<"

    goto/32 :goto_2b

    nop

    :goto_8d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_d

    nop

    :goto_8e
    return-object v0

    :goto_8f
    and-int/lit8 v4, v4, 0x2

    goto/32 :goto_12

    nop

    :goto_90
    and-int/lit8 v4, v4, 0x8

    goto/32 :goto_2c

    nop

    :goto_91
    invoke-direct {p0, v5, v4}, Landroid/app/DownloadManager$Query;->statusClause(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_52

    nop

    :goto_92
    move-object v6, v0

    goto/32 :goto_4f

    nop

    :goto_93
    const-string v5, "="

    goto/32 :goto_36

    nop

    :goto_94
    const/16 v4, 0xbe

    goto/32 :goto_7f

    nop

    :goto_95
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_17

    nop

    :goto_96
    if-nez v2, :cond_d

    goto/32 :goto_70

    :cond_d
    goto/32 :goto_76

    nop

    :goto_97
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_39

    nop

    :goto_98
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_49

    nop

    :goto_99
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_7d

    nop

    :goto_9a
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_86

    nop
.end method

.method public setFilterByAppData(Ljava/lang/String;)Landroid/app/DownloadManager$Query;
    .locals 0

    iput-object p1, p0, Landroid/app/DownloadManager$Query;->mColumnAppData:Ljava/lang/String;

    return-object p0
.end method

.method public setFilterByAppendedClause(Ljava/lang/String;)Landroid/app/DownloadManager$Query;
    .locals 0

    iput-object p1, p0, Landroid/app/DownloadManager$Query;->mAppendedClause:Ljava/lang/String;

    return-object p0
.end method

.method public varargs setFilterById([J)Landroid/app/DownloadManager$Query;
    .locals 0

    iput-object p1, p0, Landroid/app/DownloadManager$Query;->mIds:[J

    return-object p0
.end method

.method public setFilterByNotificationPackage(Ljava/lang/String;)Landroid/app/DownloadManager$Query;
    .locals 0

    iput-object p1, p0, Landroid/app/DownloadManager$Query;->mColumnNotificationPackage:Ljava/lang/String;

    return-object p0
.end method

.method public setFilterByStatus(I)Landroid/app/DownloadManager$Query;
    .locals 1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Landroid/app/DownloadManager$Query;->mStatusFlags:Ljava/lang/Integer;

    return-object p0
.end method

.method public setFilterByString(Ljava/lang/String;)Landroid/app/DownloadManager$Query;
    .locals 0

    iput-object p1, p0, Landroid/app/DownloadManager$Query;->mFilterString:Ljava/lang/String;

    return-object p0
.end method

.method public setOnlyIncludeVisibleInDownloadsUi(Z)Landroid/app/DownloadManager$Query;
    .locals 0

    iput-boolean p1, p0, Landroid/app/DownloadManager$Query;->mOnlyIncludeVisibleInDownloadsUi:Z

    return-object p0
.end method
