.class public Landroid/app/FragmentBreadCrumbs;
.super Landroid/view/ViewGroup;

# interfaces
.implements Landroid/app/FragmentManager$OnBackStackChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/FragmentBreadCrumbs$OnBreadCrumbClickListener;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final DEFAULT_GRAVITY:I = 0x800013


# instance fields
.field mActivity:Landroid/app/Activity;

.field mContainer:Landroid/widget/LinearLayout;

.field private mGravity:I

.field mInflater:Landroid/view/LayoutInflater;

.field private mLayoutResId:I

.field mMaxVisible:I

.field private mOnBreadCrumbClickListener:Landroid/app/FragmentBreadCrumbs$OnBreadCrumbClickListener;

.field private mOnClickListener:Landroid/view/View$OnClickListener;

.field private mParentClickListener:Landroid/view/View$OnClickListener;

.field mParentEntry:Landroid/app/BackStackRecord;

.field private mTextColor:I

.field mTopEntry:Landroid/app/BackStackRecord;


# direct methods
.method static bridge synthetic -$$Nest$fgetmOnBreadCrumbClickListener(Landroid/app/FragmentBreadCrumbs;)Landroid/app/FragmentBreadCrumbs$OnBreadCrumbClickListener;
    .locals 0

    iget-object p0, p0, Landroid/app/FragmentBreadCrumbs;->mOnBreadCrumbClickListener:Landroid/app/FragmentBreadCrumbs$OnBreadCrumbClickListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmParentClickListener(Landroid/app/FragmentBreadCrumbs;)Landroid/view/View$OnClickListener;
    .locals 0

    iget-object p0, p0, Landroid/app/FragmentBreadCrumbs;->mParentClickListener:Landroid/view/View$OnClickListener;

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/app/FragmentBreadCrumbs;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const v0, 0x1120053

    invoke-direct {p0, p1, p2, v0}, Landroid/app/FragmentBreadCrumbs;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/app/FragmentBreadCrumbs;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 4

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const/4 v0, -0x1

    iput v0, p0, Landroid/app/FragmentBreadCrumbs;->mMaxVisible:I

    new-instance v0, Landroid/app/FragmentBreadCrumbs$1;

    invoke-direct {v0, p0}, Landroid/app/FragmentBreadCrumbs$1;-><init>(Landroid/app/FragmentBreadCrumbs;)V

    iput-object v0, p0, Landroid/app/FragmentBreadCrumbs;->mOnClickListener:Landroid/view/View$OnClickListener;

    sget-object v0, Lcom/android/internal/R$styleable;->FragmentBreadCrumbs:[I

    invoke-virtual {p1, p2, v0, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x0

    const v2, 0x800013

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Landroid/app/FragmentBreadCrumbs;->mGravity:I

    const/4 v2, 0x2

    const v3, 0x1090082

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Landroid/app/FragmentBreadCrumbs;->mLayoutResId:I

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Landroid/app/FragmentBreadCrumbs;->mTextColor:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private createBackStackEntry(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/BackStackRecord;
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    new-instance v0, Landroid/app/BackStackRecord;

    iget-object v1, p0, Landroid/app/FragmentBreadCrumbs;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    check-cast v1, Landroid/app/FragmentManagerImpl;

    invoke-direct {v0, v1}, Landroid/app/BackStackRecord;-><init>(Landroid/app/FragmentManagerImpl;)V

    invoke-virtual {v0, p1}, Landroid/app/BackStackRecord;->setBreadCrumbTitle(Ljava/lang/CharSequence;)Landroid/app/FragmentTransaction;

    invoke-virtual {v0, p2}, Landroid/app/BackStackRecord;->setBreadCrumbShortTitle(Ljava/lang/CharSequence;)Landroid/app/FragmentTransaction;

    return-object v0
.end method

.method private getPreEntry(I)Landroid/app/FragmentManager$BackStackEntry;
    .locals 1

    iget-object v0, p0, Landroid/app/FragmentBreadCrumbs;->mParentEntry:Landroid/app/BackStackRecord;

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Landroid/app/FragmentBreadCrumbs;->mTopEntry:Landroid/app/BackStackRecord;

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Landroid/app/FragmentBreadCrumbs;->mTopEntry:Landroid/app/BackStackRecord;

    return-object v0
.end method

.method private getPreEntryCount()I
    .locals 4

    iget-object v0, p0, Landroid/app/FragmentBreadCrumbs;->mTopEntry:Landroid/app/BackStackRecord;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    iget-object v3, p0, Landroid/app/FragmentBreadCrumbs;->mParentEntry:Landroid/app/BackStackRecord;

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    move v1, v2

    :goto_1
    add-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public onBackStackChanged()V
    .locals 0

    invoke-virtual {p0}, Landroid/app/FragmentBreadCrumbs;->updateCrumbs()V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 10

    invoke-virtual {p0}, Landroid/app/FragmentBreadCrumbs;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/app/FragmentBreadCrumbs;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget v2, p0, Landroid/app/FragmentBreadCrumbs;->mPaddingTop:I

    iget v3, p0, Landroid/app/FragmentBreadCrumbs;->mPaddingTop:I

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v3, v4

    iget v4, p0, Landroid/app/FragmentBreadCrumbs;->mPaddingBottom:I

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Landroid/app/FragmentBreadCrumbs;->getLayoutDirection()I

    move-result v4

    iget v5, p0, Landroid/app/FragmentBreadCrumbs;->mGravity:I

    const v6, 0x800007

    and-int/2addr v5, v6

    invoke-static {v5, v4}, Landroid/view/Gravity;->getAbsoluteGravity(II)I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    iget v7, p0, Landroid/app/FragmentBreadCrumbs;->mPaddingLeft:I

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v7

    goto :goto_0

    :sswitch_0
    iget v6, p0, Landroid/app/FragmentBreadCrumbs;->mRight:I

    iget v7, p0, Landroid/app/FragmentBreadCrumbs;->mLeft:I

    sub-int/2addr v6, v7

    iget v7, p0, Landroid/app/FragmentBreadCrumbs;->mPaddingRight:I

    sub-int/2addr v6, v7

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    sub-int v7, v6, v7

    goto :goto_0

    :sswitch_1
    iget v6, p0, Landroid/app/FragmentBreadCrumbs;->mPaddingLeft:I

    iget v7, p0, Landroid/app/FragmentBreadCrumbs;->mRight:I

    iget v8, p0, Landroid/app/FragmentBreadCrumbs;->mLeft:I

    sub-int/2addr v7, v8

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    sub-int/2addr v7, v8

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v7, v6

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v7

    nop

    :goto_0
    iget v8, p0, Landroid/app/FragmentBreadCrumbs;->mPaddingLeft:I

    if-ge v7, v8, :cond_1

    iget v7, p0, Landroid/app/FragmentBreadCrumbs;->mPaddingLeft:I

    :cond_1
    iget v8, p0, Landroid/app/FragmentBreadCrumbs;->mRight:I

    iget v9, p0, Landroid/app/FragmentBreadCrumbs;->mLeft:I

    sub-int/2addr v8, v9

    iget v9, p0, Landroid/app/FragmentBreadCrumbs;->mPaddingRight:I

    sub-int/2addr v8, v9

    if-le v6, v8, :cond_2

    iget v8, p0, Landroid/app/FragmentBreadCrumbs;->mRight:I

    iget v9, p0, Landroid/app/FragmentBreadCrumbs;->mLeft:I

    sub-int/2addr v8, v9

    iget v9, p0, Landroid/app/FragmentBreadCrumbs;->mPaddingRight:I

    sub-int v6, v8, v9

    :cond_2
    invoke-virtual {v1, v7, v2, v6, v3}, Landroid/view/View;->layout(IIII)V

    return-void

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x5 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onMeasure(II)V
    .locals 8

    invoke-virtual {p0}, Landroid/app/FragmentBreadCrumbs;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v0, :cond_1

    invoke-virtual {p0, v4}, Landroid/app/FragmentBreadCrumbs;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v6

    const/16 v7, 0x8

    if-eq v6, v7, :cond_0

    invoke-virtual {p0, v5, p1, p2}, Landroid/app/FragmentBreadCrumbs;->measureChild(Landroid/view/View;II)V

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    invoke-static {v2, v6}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    invoke-static {v1, v6}, Ljava/lang/Math;->max(II)I

    move-result v1

    nop

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredState()I

    move-result v6

    invoke-static {v3, v6}, Landroid/app/FragmentBreadCrumbs;->combineMeasuredStates(II)I

    move-result v3

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    iget v4, p0, Landroid/app/FragmentBreadCrumbs;->mPaddingLeft:I

    iget v5, p0, Landroid/app/FragmentBreadCrumbs;->mPaddingRight:I

    add-int/2addr v4, v5

    add-int/2addr v2, v4

    iget v4, p0, Landroid/app/FragmentBreadCrumbs;->mPaddingTop:I

    iget v5, p0, Landroid/app/FragmentBreadCrumbs;->mPaddingBottom:I

    add-int/2addr v4, v5

    add-int/2addr v1, v4

    invoke-virtual {p0}, Landroid/app/FragmentBreadCrumbs;->getSuggestedMinimumHeight()I

    move-result v4

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {p0}, Landroid/app/FragmentBreadCrumbs;->getSuggestedMinimumWidth()I

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v2, p1, v3}, Landroid/app/FragmentBreadCrumbs;->resolveSizeAndState(III)I

    move-result v4

    shl-int/lit8 v5, v3, 0x10

    invoke-static {v1, p2, v5}, Landroid/app/FragmentBreadCrumbs;->resolveSizeAndState(III)I

    move-result v5

    invoke-virtual {p0, v4, v5}, Landroid/app/FragmentBreadCrumbs;->setMeasuredDimension(II)V

    return-void
.end method

.method public setActivity(Landroid/app/Activity;)V
    .locals 3

    iput-object p1, p0, Landroid/app/FragmentBreadCrumbs;->mActivity:Landroid/app/Activity;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Landroid/app/FragmentBreadCrumbs;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x1090084

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Landroid/app/FragmentBreadCrumbs;->mContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Landroid/app/FragmentBreadCrumbs;->addView(Landroid/view/View;)V

    invoke-virtual {p1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/FragmentManager;->addOnBackStackChangedListener(Landroid/app/FragmentManager$OnBackStackChangedListener;)V

    invoke-virtual {p0}, Landroid/app/FragmentBreadCrumbs;->updateCrumbs()V

    new-instance v0, Landroid/animation/LayoutTransition;

    invoke-direct {v0}, Landroid/animation/LayoutTransition;-><init>()V

    invoke-virtual {p0, v0}, Landroid/app/FragmentBreadCrumbs;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    return-void
.end method

.method public setMaxVisible(I)V
    .locals 2

    const/4 v0, 0x1

    if-lt p1, v0, :cond_0

    iput p1, p0, Landroid/app/FragmentBreadCrumbs;->mMaxVisible:I

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "visibleCrumbs must be greater than zero"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setOnBreadCrumbClickListener(Landroid/app/FragmentBreadCrumbs$OnBreadCrumbClickListener;)V
    .locals 0

    iput-object p1, p0, Landroid/app/FragmentBreadCrumbs;->mOnBreadCrumbClickListener:Landroid/app/FragmentBreadCrumbs$OnBreadCrumbClickListener;

    return-void
.end method

.method public setParentTitle(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/app/FragmentBreadCrumbs;->createBackStackEntry(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/BackStackRecord;

    move-result-object v0

    iput-object v0, p0, Landroid/app/FragmentBreadCrumbs;->mParentEntry:Landroid/app/BackStackRecord;

    iput-object p3, p0, Landroid/app/FragmentBreadCrumbs;->mParentClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p0}, Landroid/app/FragmentBreadCrumbs;->updateCrumbs()V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/app/FragmentBreadCrumbs;->createBackStackEntry(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/BackStackRecord;

    move-result-object v0

    iput-object v0, p0, Landroid/app/FragmentBreadCrumbs;->mTopEntry:Landroid/app/BackStackRecord;

    invoke-virtual {p0}, Landroid/app/FragmentBreadCrumbs;->updateCrumbs()V

    return-void
.end method

.method updateCrumbs()V
    .locals 14

    goto/32 :goto_6a

    nop

    :goto_0
    if-nez v5, :cond_0

    goto/32 :goto_11

    :cond_0
    goto/32 :goto_a

    nop

    :goto_1
    add-int v5, v1, v2

    goto/32 :goto_41

    nop

    :goto_2
    sub-int v12, v3, v12

    goto/32 :goto_38

    nop

    :goto_3
    add-int/lit8 v12, v12, 0x1

    goto/32 :goto_5b

    nop

    :goto_4
    sub-int v11, v3, v11

    goto/32 :goto_5d

    nop

    :goto_5
    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_6
    goto/32 :goto_40

    nop

    :goto_7
    add-int/lit8 v10, v3, -0x1

    goto/32 :goto_48

    nop

    :goto_8
    if-gt v3, v4, :cond_1

    goto/32 :goto_4b

    :cond_1
    goto/32 :goto_66

    nop

    :goto_9
    invoke-direct {p0, v4}, Landroid/app/FragmentBreadCrumbs;->getPreEntry(I)Landroid/app/FragmentManager$BackStackEntry;

    move-result-object v5

    goto/32 :goto_6c

    nop

    :goto_a
    move v12, v9

    goto/32 :goto_10

    nop

    :goto_b
    invoke-virtual {v10}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v11

    goto/32 :goto_2a

    nop

    :goto_c
    invoke-virtual {v10, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    goto/32 :goto_f

    nop

    :goto_d
    goto/16 :goto_2d

    :goto_e
    goto/32 :goto_24

    nop

    :goto_f
    iget v12, p0, Landroid/app/FragmentBreadCrumbs;->mMaxVisible:I

    goto/32 :goto_2

    nop

    :goto_10
    goto/16 :goto_53

    :goto_11
    goto/32 :goto_52

    nop

    :goto_12
    if-lt v4, v2, :cond_2

    goto/32 :goto_6d

    :cond_2
    goto/32 :goto_9

    nop

    :goto_13
    if-eqz v4, :cond_3

    goto/32 :goto_47

    :cond_3
    goto/32 :goto_62

    nop

    :goto_14
    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_2e

    nop

    :goto_15
    if-lt v4, v3, :cond_4

    goto/32 :goto_69

    :cond_4
    goto/32 :goto_64

    nop

    :goto_16
    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    goto/32 :goto_c

    nop

    :goto_17
    add-int v4, v1, v2

    goto/32 :goto_51

    nop

    :goto_18
    const/4 v4, 0x0

    :goto_19
    goto/32 :goto_1

    nop

    :goto_1a
    goto :goto_19

    :goto_1b
    goto/32 :goto_17

    nop

    :goto_1c
    invoke-direct {p0}, Landroid/app/FragmentBreadCrumbs;->getPreEntryCount()I

    move-result v2

    goto/32 :goto_2f

    nop

    :goto_1d
    invoke-virtual {v9, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    goto/32 :goto_5f

    nop

    :goto_1e
    invoke-virtual {v11, v12}, Landroid/view/View;->setEnabled(Z)V

    goto/32 :goto_49

    nop

    :goto_1f
    invoke-virtual {v10, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    goto/32 :goto_54

    nop

    :goto_20
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    goto/32 :goto_5e

    nop

    :goto_21
    move v11, v8

    goto/32 :goto_3c

    nop

    :goto_22
    iget-object v6, p0, Landroid/app/FragmentBreadCrumbs;->mOnClickListener:Landroid/view/View$OnClickListener;

    goto/32 :goto_5

    nop

    :goto_23
    iget-object v10, p0, Landroid/app/FragmentBreadCrumbs;->mContainer:Landroid/widget/LinearLayout;

    goto/32 :goto_1f

    nop

    :goto_24
    return-void

    :goto_25
    invoke-virtual {v13, v4}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    goto/32 :goto_3

    nop

    :goto_26
    invoke-interface {v5}, Landroid/app/FragmentManager$BackStackEntry;->getBreadCrumbTitle()Ljava/lang/CharSequence;

    move-result-object v10

    goto/32 :goto_14

    nop

    :goto_27
    if-lt v5, v12, :cond_5

    goto/32 :goto_4e

    :cond_5
    goto/32 :goto_6b

    nop

    :goto_28
    move v11, v9

    :goto_29
    goto/32 :goto_16

    nop

    :goto_2a
    if-ne v11, v5, :cond_6

    goto/32 :goto_69

    :cond_6
    goto/32 :goto_60

    nop

    :goto_2b
    if-lt v4, v5, :cond_7

    goto/32 :goto_1b

    :cond_7
    goto/32 :goto_12

    nop

    :goto_2c
    const/4 v5, 0x0

    :goto_2d
    goto/32 :goto_65

    nop

    :goto_2e
    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    goto/32 :goto_45

    nop

    :goto_2f
    iget-object v3, p0, Landroid/app/FragmentBreadCrumbs;->mContainer:Landroid/widget/LinearLayout;

    goto/32 :goto_39

    nop

    :goto_30
    invoke-virtual {v10, v11, p0, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    goto/32 :goto_1d

    nop

    :goto_31
    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    :goto_32
    goto/32 :goto_8

    nop

    :goto_33
    iget v11, p0, Landroid/app/FragmentBreadCrumbs;->mLayoutResId:I

    goto/32 :goto_30

    nop

    :goto_34
    sub-int v5, v4, v2

    goto/32 :goto_55

    nop

    :goto_35
    const v7, 0x1020016

    goto/32 :goto_50

    nop

    :goto_36
    add-int/lit8 v12, v3, -0x1

    goto/32 :goto_27

    nop

    :goto_37
    invoke-virtual {v10, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    goto/32 :goto_b

    nop

    :goto_38
    if-gt v5, v12, :cond_8

    goto/32 :goto_11

    :cond_8
    goto/32 :goto_0

    nop

    :goto_39
    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    goto/32 :goto_18

    nop

    :goto_3a
    if-gtz v11, :cond_9

    goto/32 :goto_3f

    :cond_9
    goto/32 :goto_4

    nop

    :goto_3b
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_d

    nop

    :goto_3c
    goto :goto_29

    :goto_3d
    goto/32 :goto_28

    nop

    :goto_3e
    invoke-virtual {v11, v12}, Landroid/view/View;->setVisibility(I)V

    :goto_3f
    goto/32 :goto_3b

    nop

    :goto_40
    add-int/lit8 v4, v4, 0x1

    goto/32 :goto_1a

    nop

    :goto_41
    const v6, 0x102037f

    goto/32 :goto_35

    nop

    :goto_42
    iget-object v10, p0, Landroid/app/FragmentBreadCrumbs;->mInflater:Landroid/view/LayoutInflater;

    goto/32 :goto_33

    nop

    :goto_43
    if-lt v12, v3, :cond_a

    goto/32 :goto_5c

    :cond_a
    goto/32 :goto_4c

    nop

    :goto_44
    iget-object v6, p0, Landroid/app/FragmentBreadCrumbs;->mContainer:Landroid/widget/LinearLayout;

    goto/32 :goto_57

    nop

    :goto_45
    iget v10, p0, Landroid/app/FragmentBreadCrumbs;->mTextColor:I

    goto/32 :goto_67

    nop

    :goto_46
    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    :goto_47
    goto/32 :goto_44

    nop

    :goto_48
    invoke-virtual {v5, v10}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    goto/32 :goto_4f

    nop

    :goto_49
    iget v11, p0, Landroid/app/FragmentBreadCrumbs;->mMaxVisible:I

    goto/32 :goto_3a

    nop

    :goto_4a
    goto/16 :goto_32

    :goto_4b
    goto/32 :goto_2c

    nop

    :goto_4c
    iget-object v13, p0, Landroid/app/FragmentBreadCrumbs;->mContainer:Landroid/widget/LinearLayout;

    goto/32 :goto_25

    nop

    :goto_4d
    goto :goto_5a

    :goto_4e
    goto/32 :goto_59

    nop

    :goto_4f
    add-int/lit8 v3, v3, -0x1

    goto/32 :goto_4a

    nop

    :goto_50
    const/16 v8, 0x8

    goto/32 :goto_63

    nop

    :goto_51
    iget-object v5, p0, Landroid/app/FragmentBreadCrumbs;->mContainer:Landroid/widget/LinearLayout;

    goto/32 :goto_31

    nop

    :goto_52
    move v12, v8

    :goto_53
    goto/32 :goto_3e

    nop

    :goto_54
    invoke-virtual {v10, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    goto/32 :goto_36

    nop

    :goto_55
    invoke-virtual {v0, v5}, Landroid/app/FragmentManager;->getBackStackEntryAt(I)Landroid/app/FragmentManager$BackStackEntry;

    move-result-object v5

    :goto_56
    nop

    goto/32 :goto_15

    nop

    :goto_57
    invoke-virtual {v6, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/32 :goto_22

    nop

    :goto_58
    if-ge v4, v3, :cond_b

    goto/32 :goto_6

    :cond_b
    goto/32 :goto_42

    nop

    :goto_59
    move v12, v9

    :goto_5a
    goto/32 :goto_1e

    nop

    :goto_5b
    goto :goto_61

    :goto_5c
    goto/32 :goto_68

    nop

    :goto_5d
    if-lt v5, v11, :cond_c

    goto/32 :goto_3d

    :cond_c
    goto/32 :goto_21

    nop

    :goto_5e
    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v1

    goto/32 :goto_1c

    nop

    :goto_5f
    check-cast v7, Landroid/widget/TextView;

    goto/32 :goto_26

    nop

    :goto_60
    move v12, v4

    :goto_61
    goto/32 :goto_43

    nop

    :goto_62
    invoke-virtual {v9, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    goto/32 :goto_46

    nop

    :goto_63
    const/4 v9, 0x0

    goto/32 :goto_2b

    nop

    :goto_64
    iget-object v10, p0, Landroid/app/FragmentBreadCrumbs;->mContainer:Landroid/widget/LinearLayout;

    goto/32 :goto_37

    nop

    :goto_65
    if-lt v5, v3, :cond_d

    goto/32 :goto_e

    :cond_d
    goto/32 :goto_23

    nop

    :goto_66
    iget-object v5, p0, Landroid/app/FragmentBreadCrumbs;->mContainer:Landroid/widget/LinearLayout;

    goto/32 :goto_7

    nop

    :goto_67
    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setTextColor(I)V

    goto/32 :goto_13

    nop

    :goto_68
    move v3, v4

    :goto_69
    goto/32 :goto_58

    nop

    :goto_6a
    iget-object v0, p0, Landroid/app/FragmentBreadCrumbs;->mActivity:Landroid/app/Activity;

    goto/32 :goto_20

    nop

    :goto_6b
    const/4 v12, 0x1

    goto/32 :goto_4d

    nop

    :goto_6c
    goto :goto_56

    :goto_6d
    goto/32 :goto_34

    nop
.end method
