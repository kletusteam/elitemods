.class final Landroid/app/BackStackRecord;
.super Landroid/app/FragmentTransaction;

# interfaces
.implements Landroid/app/FragmentManager$BackStackEntry;
.implements Landroid/app/FragmentManagerImpl$OpGenerator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/BackStackRecord$Op;
    }
.end annotation


# static fields
.field static final OP_ADD:I = 0x1

.field static final OP_ATTACH:I = 0x7

.field static final OP_DETACH:I = 0x6

.field static final OP_HIDE:I = 0x4

.field static final OP_NULL:I = 0x0

.field static final OP_REMOVE:I = 0x3

.field static final OP_REPLACE:I = 0x2

.field static final OP_SET_PRIMARY_NAV:I = 0x8

.field static final OP_SHOW:I = 0x5

.field static final OP_UNSET_PRIMARY_NAV:I = 0x9

.field static final TAG:Ljava/lang/String; = "FragmentManager"


# instance fields
.field mAddToBackStack:Z

.field mAllowAddToBackStack:Z

.field mBreadCrumbShortTitleRes:I

.field mBreadCrumbShortTitleText:Ljava/lang/CharSequence;

.field mBreadCrumbTitleRes:I

.field mBreadCrumbTitleText:Ljava/lang/CharSequence;

.field mCommitRunnables:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field mCommitted:Z

.field mEnterAnim:I

.field mExitAnim:I

.field mIndex:I

.field final mManager:Landroid/app/FragmentManagerImpl;

.field mName:Ljava/lang/String;

.field mOps:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/app/BackStackRecord$Op;",
            ">;"
        }
    .end annotation
.end field

.field mPopEnterAnim:I

.field mPopExitAnim:I

.field mReorderingAllowed:Z

.field mSharedElementSourceNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mSharedElementTargetNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mTransition:I

.field mTransitionStyle:I


# direct methods
.method public constructor <init>(Landroid/app/FragmentManagerImpl;)V
    .locals 3

    invoke-direct {p0}, Landroid/app/FragmentTransaction;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/BackStackRecord;->mAllowAddToBackStack:Z

    const/4 v1, -0x1

    iput v1, p0, Landroid/app/BackStackRecord;->mIndex:I

    iput-object p1, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    invoke-virtual {p1}, Landroid/app/FragmentManagerImpl;->getTargetSdk()I

    move-result v1

    const/16 v2, 0x19

    if-le v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Landroid/app/BackStackRecord;->mReorderingAllowed:Z

    return-void
.end method

.method private doAddOp(ILandroid/app/Fragment;Ljava/lang/String;I)V
    .locals 5

    iget-object v0, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    invoke-virtual {v0}, Landroid/app/FragmentManagerImpl;->getTargetSdk()I

    move-result v0

    const/16 v1, 0x19

    if-le v0, v1, :cond_1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getModifiers()I

    move-result v1

    invoke-virtual {v0}, Ljava/lang/Class;->isAnonymousClass()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, Ljava/lang/reflect/Modifier;->isPublic(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/Class;->isMemberClass()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v1}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Fragment "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " must be a public static class to be  properly recreated from instance state."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    :goto_0
    iget-object v0, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    iput-object v0, p2, Landroid/app/Fragment;->mFragmentManager:Landroid/app/FragmentManagerImpl;

    const-string v0, " now "

    const-string v1, ": was "

    if-eqz p3, :cond_4

    iget-object v2, p2, Landroid/app/Fragment;->mTag:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p2, Landroid/app/Fragment;->mTag:Ljava/lang/String;

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Can\'t change tag of fragment "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p2, Landroid/app/Fragment;->mTag:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_3
    :goto_1
    iput-object p3, p2, Landroid/app/Fragment;->mTag:Ljava/lang/String;

    :cond_4
    if-eqz p1, :cond_8

    const/4 v2, -0x1

    if-eq p1, v2, :cond_7

    iget v2, p2, Landroid/app/Fragment;->mFragmentId:I

    if-eqz v2, :cond_6

    iget v2, p2, Landroid/app/Fragment;->mFragmentId:I

    if-ne v2, p1, :cond_5

    goto :goto_2

    :cond_5
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Can\'t change container ID of fragment "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p2, Landroid/app/Fragment;->mFragmentId:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_6
    :goto_2
    iput p1, p2, Landroid/app/Fragment;->mFragmentId:I

    iput p1, p2, Landroid/app/Fragment;->mContainerId:I

    goto :goto_3

    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t add fragment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with tag "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to container view with no id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    :goto_3
    new-instance v0, Landroid/app/BackStackRecord$Op;

    invoke-direct {v0, p4, p2}, Landroid/app/BackStackRecord$Op;-><init>(ILandroid/app/Fragment;)V

    invoke-virtual {p0, v0}, Landroid/app/BackStackRecord;->addOp(Landroid/app/BackStackRecord$Op;)V

    return-void
.end method

.method private static isFragmentPostponed(Landroid/app/BackStackRecord$Op;)Z
    .locals 2

    iget-object v0, p0, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    if-eqz v0, :cond_0

    iget-boolean v1, v0, Landroid/app/Fragment;->mAdded:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Landroid/app/Fragment;->mView:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-boolean v1, v0, Landroid/app/Fragment;->mDetached:Z

    if-nez v1, :cond_0

    iget-boolean v1, v0, Landroid/app/Fragment;->mHidden:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/app/Fragment;->isPostponed()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method


# virtual methods
.method public add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Landroid/app/BackStackRecord;->doAddOp(ILandroid/app/Fragment;Ljava/lang/String;I)V

    return-object p0
.end method

.method public add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/app/BackStackRecord;->doAddOp(ILandroid/app/Fragment;Ljava/lang/String;I)V

    return-object p0
.end method

.method public add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, p1, p2, v1}, Landroid/app/BackStackRecord;->doAddOp(ILandroid/app/Fragment;Ljava/lang/String;I)V

    return-object p0
.end method

.method addOp(Landroid/app/BackStackRecord$Op;)V
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    iget v0, p0, Landroid/app/BackStackRecord;->mPopExitAnim:I

    goto/32 :goto_7

    nop

    :goto_1
    iget v0, p0, Landroid/app/BackStackRecord;->mExitAnim:I

    goto/32 :goto_3

    nop

    :goto_2
    iput v0, p1, Landroid/app/BackStackRecord$Op;->enterAnim:I

    goto/32 :goto_1

    nop

    :goto_3
    iput v0, p1, Landroid/app/BackStackRecord$Op;->exitAnim:I

    goto/32 :goto_6

    nop

    :goto_4
    iget-object v0, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_5

    nop

    :goto_5
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_a

    nop

    :goto_6
    iget v0, p0, Landroid/app/BackStackRecord;->mPopEnterAnim:I

    goto/32 :goto_9

    nop

    :goto_7
    iput v0, p1, Landroid/app/BackStackRecord$Op;->popExitAnim:I

    goto/32 :goto_8

    nop

    :goto_8
    return-void

    :goto_9
    iput v0, p1, Landroid/app/BackStackRecord$Op;->popEnterAnim:I

    goto/32 :goto_0

    nop

    :goto_a
    iget v0, p0, Landroid/app/BackStackRecord;->mEnterAnim:I

    goto/32 :goto_2

    nop
.end method

.method public addSharedElement(Landroid/view/View;Ljava/lang/String;)Landroid/app/FragmentTransaction;
    .locals 4

    invoke-virtual {p1}, Landroid/view/View;->getTransitionName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v1, p0, Landroid/app/BackStackRecord;->mSharedElementSourceNames:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Landroid/app/BackStackRecord;->mSharedElementSourceNames:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Landroid/app/BackStackRecord;->mSharedElementTargetNames:Ljava/util/ArrayList;

    goto :goto_0

    :cond_0
    iget-object v1, p0, Landroid/app/BackStackRecord;->mSharedElementTargetNames:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Landroid/app/BackStackRecord;->mSharedElementSourceNames:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :goto_0
    iget-object v1, p0, Landroid/app/BackStackRecord;->mSharedElementSourceNames:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Landroid/app/BackStackRecord;->mSharedElementTargetNames:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object p0

    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "A shared element with the source name \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " has already been added to the transaction."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "A shared element with the target name \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' has already been added to the transaction."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Unique transitionNames are required for all sharedElements"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;
    .locals 2

    iget-boolean v0, p0, Landroid/app/BackStackRecord;->mAllowAddToBackStack:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/BackStackRecord;->mAddToBackStack:Z

    iput-object p1, p0, Landroid/app/BackStackRecord;->mName:Ljava/lang/String;

    return-object p0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This FragmentTransaction is not allowed to be added to the back stack."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public attach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    .locals 2

    new-instance v0, Landroid/app/BackStackRecord$Op;

    const/4 v1, 0x7

    invoke-direct {v0, v1, p1}, Landroid/app/BackStackRecord$Op;-><init>(ILandroid/app/Fragment;)V

    invoke-virtual {p0, v0}, Landroid/app/BackStackRecord;->addOp(Landroid/app/BackStackRecord$Op;)V

    return-object p0
.end method

.method bumpBackStackNesting(I)V
    .locals 6

    goto/32 :goto_1d

    nop

    :goto_0
    iget v5, v5, Landroid/app/Fragment;->mBackStackNesting:I

    goto/32 :goto_a

    nop

    :goto_1
    sget-boolean v4, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    goto/32 :goto_14

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_17

    :cond_0
    goto/32 :goto_1a

    nop

    :goto_3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_20

    nop

    :goto_4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_f

    nop

    :goto_5
    const/4 v2, 0x0

    :goto_6
    goto/32 :goto_e

    nop

    :goto_7
    return-void

    :goto_8
    goto/32 :goto_26

    nop

    :goto_9
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_4

    nop

    :goto_a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_3

    nop

    :goto_b
    iget-object v0, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_23

    nop

    :goto_c
    return-void

    :goto_d
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_15

    nop

    :goto_e
    if-lt v2, v0, :cond_1

    goto/32 :goto_2c

    :cond_1
    goto/32 :goto_13

    nop

    :goto_f
    const-string v5, "Bump nesting of "

    goto/32 :goto_18

    nop

    :goto_10
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_11

    nop

    :goto_11
    iget-object v5, v3, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    goto/32 :goto_0

    nop

    :goto_12
    if-nez v4, :cond_2

    goto/32 :goto_21

    :cond_2
    goto/32 :goto_32

    nop

    :goto_13
    iget-object v3, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_1e

    nop

    :goto_14
    if-nez v4, :cond_3

    goto/32 :goto_21

    :cond_3
    goto/32 :goto_9

    nop

    :goto_15
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_2a

    nop

    :goto_16
    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_17
    goto/32 :goto_b

    nop

    :goto_18
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_33

    nop

    :goto_19
    iput v5, v4, Landroid/app/Fragment;->mBackStackNesting:I

    goto/32 :goto_1

    nop

    :goto_1a
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_25

    nop

    :goto_1b
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_2b

    nop

    :goto_1c
    iget-object v4, v3, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    goto/32 :goto_12

    nop

    :goto_1d
    iget-boolean v0, p0, Landroid/app/BackStackRecord;->mAddToBackStack:Z

    goto/32 :goto_30

    nop

    :goto_1e
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_27

    nop

    :goto_1f
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_2f

    nop

    :goto_20
    invoke-static {v1, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_21
    goto/32 :goto_1b

    nop

    :goto_22
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_2d

    nop

    :goto_23
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_5

    nop

    :goto_24
    const-string v2, " by "

    goto/32 :goto_d

    nop

    :goto_25
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_2e

    nop

    :goto_26
    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    goto/32 :goto_29

    nop

    :goto_27
    check-cast v3, Landroid/app/BackStackRecord$Op;

    goto/32 :goto_1c

    nop

    :goto_28
    iget v5, v4, Landroid/app/Fragment;->mBackStackNesting:I

    goto/32 :goto_31

    nop

    :goto_29
    const-string v1, "FragmentManager"

    goto/32 :goto_2

    nop

    :goto_2a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_16

    nop

    :goto_2b
    goto/16 :goto_6

    :goto_2c
    goto/32 :goto_c

    nop

    :goto_2d
    const-string v5, " to "

    goto/32 :goto_10

    nop

    :goto_2e
    const-string v2, "Bump nesting in "

    goto/32 :goto_1f

    nop

    :goto_2f
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_24

    nop

    :goto_30
    if-eqz v0, :cond_4

    goto/32 :goto_8

    :cond_4
    goto/32 :goto_7

    nop

    :goto_31
    add-int/2addr v5, p1

    goto/32 :goto_19

    nop

    :goto_32
    iget-object v4, v3, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    goto/32 :goto_28

    nop

    :goto_33
    iget-object v5, v3, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    goto/32 :goto_22

    nop
.end method

.method public commit()I
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/BackStackRecord;->commitInternal(Z)I

    move-result v0

    return v0
.end method

.method public commitAllowingStateLoss()I
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/app/BackStackRecord;->commitInternal(Z)I

    move-result v0

    return v0
.end method

.method commitInternal(Z)I
    .locals 4

    goto/32 :goto_1b

    nop

    :goto_0
    const/16 v3, 0x400

    goto/32 :goto_1f

    nop

    :goto_1
    const/4 v2, 0x2

    goto/32 :goto_d

    nop

    :goto_2
    goto :goto_b

    :goto_3
    goto/32 :goto_26

    nop

    :goto_4
    invoke-virtual {v0, p0}, Landroid/app/FragmentManagerImpl;->allocBackStackIndex(Landroid/app/BackStackRecord;)I

    move-result v0

    goto/32 :goto_1a

    nop

    :goto_5
    new-instance v0, Ljava/lang/IllegalStateException;

    goto/32 :goto_15

    nop

    :goto_6
    iput-boolean v0, p0, Landroid/app/BackStackRecord;->mCommitted:Z

    goto/32 :goto_28

    nop

    :goto_7
    const-string v1, "Commit: "

    goto/32 :goto_1c

    nop

    :goto_8
    return v0

    :goto_9
    goto/32 :goto_5

    nop

    :goto_a
    iput v0, p0, Landroid/app/BackStackRecord;->mIndex:I

    :goto_b
    goto/32 :goto_25

    nop

    :goto_c
    if-eqz v0, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_2b

    nop

    :goto_d
    invoke-direct {v0, v2, v1}, Landroid/util/LogWriter;-><init>(ILjava/lang/String;)V

    goto/32 :goto_10

    nop

    :goto_e
    iget v0, p0, Landroid/app/BackStackRecord;->mIndex:I

    goto/32 :goto_8

    nop

    :goto_f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_7

    nop

    :goto_10
    new-instance v1, Lcom/android/internal/util/FastPrintWriter;

    goto/32 :goto_11

    nop

    :goto_11
    const/4 v2, 0x0

    goto/32 :goto_0

    nop

    :goto_12
    const-string v1, "FragmentManager"

    goto/32 :goto_22

    nop

    :goto_13
    if-nez v0, :cond_1

    goto/32 :goto_21

    :cond_1
    goto/32 :goto_17

    nop

    :goto_14
    throw v0

    :goto_15
    const-string v1, "commit already called"

    goto/32 :goto_18

    nop

    :goto_16
    const/4 v3, 0x0

    goto/32 :goto_1e

    nop

    :goto_17
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_f

    nop

    :goto_18
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_14

    nop

    :goto_19
    const/4 v0, 0x1

    goto/32 :goto_6

    nop

    :goto_1a
    iput v0, p0, Landroid/app/BackStackRecord;->mIndex:I

    goto/32 :goto_2

    nop

    :goto_1b
    iget-boolean v0, p0, Landroid/app/BackStackRecord;->mCommitted:Z

    goto/32 :goto_c

    nop

    :goto_1c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_1d

    nop

    :goto_1d
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_2c

    nop

    :goto_1e
    invoke-virtual {p0, v2, v3, v1, v3}, Landroid/app/BackStackRecord;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    goto/32 :goto_20

    nop

    :goto_1f
    invoke-direct {v1, v0, v2, v3}, Lcom/android/internal/util/FastPrintWriter;-><init>(Ljava/io/Writer;ZI)V

    goto/32 :goto_27

    nop

    :goto_20
    invoke-virtual {v1}, Ljava/io/PrintWriter;->flush()V

    :goto_21
    goto/32 :goto_19

    nop

    :goto_22
    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_23

    nop

    :goto_23
    new-instance v0, Landroid/util/LogWriter;

    goto/32 :goto_1

    nop

    :goto_24
    if-nez v0, :cond_2

    goto/32 :goto_3

    :cond_2
    goto/32 :goto_29

    nop

    :goto_25
    iget-object v0, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_2a

    nop

    :goto_26
    const/4 v0, -0x1

    goto/32 :goto_a

    nop

    :goto_27
    const-string v2, "  "

    goto/32 :goto_16

    nop

    :goto_28
    iget-boolean v0, p0, Landroid/app/BackStackRecord;->mAddToBackStack:Z

    goto/32 :goto_24

    nop

    :goto_29
    iget-object v0, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_4

    nop

    :goto_2a
    invoke-virtual {v0, p0, p1}, Landroid/app/FragmentManagerImpl;->enqueueAction(Landroid/app/FragmentManagerImpl$OpGenerator;Z)V

    goto/32 :goto_e

    nop

    :goto_2b
    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    goto/32 :goto_13

    nop

    :goto_2c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_12

    nop
.end method

.method public commitNow()V
    .locals 2

    invoke-virtual {p0}, Landroid/app/BackStackRecord;->disallowAddToBackStack()Landroid/app/FragmentTransaction;

    iget-object v0, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Landroid/app/FragmentManagerImpl;->execSingleAction(Landroid/app/FragmentManagerImpl$OpGenerator;Z)V

    return-void
.end method

.method public commitNowAllowingStateLoss()V
    .locals 2

    invoke-virtual {p0}, Landroid/app/BackStackRecord;->disallowAddToBackStack()Landroid/app/FragmentTransaction;

    iget-object v0, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Landroid/app/FragmentManagerImpl;->execSingleAction(Landroid/app/FragmentManagerImpl$OpGenerator;Z)V

    return-void
.end method

.method public detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    .locals 2

    new-instance v0, Landroid/app/BackStackRecord$Op;

    const/4 v1, 0x6

    invoke-direct {v0, v1, p1}, Landroid/app/BackStackRecord$Op;-><init>(ILandroid/app/Fragment;)V

    invoke-virtual {p0, v0}, Landroid/app/BackStackRecord;->addOp(Landroid/app/BackStackRecord$Op;)V

    return-object p0
.end method

.method public disallowAddToBackStack()Landroid/app/FragmentTransaction;
    .locals 2

    iget-boolean v0, p0, Landroid/app/BackStackRecord;->mAddToBackStack:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/app/BackStackRecord;->mAllowAddToBackStack:Z

    return-object p0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This transaction is already being added to the back stack"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, p3, v0}, Landroid/app/BackStackRecord;->dump(Ljava/lang/String;Ljava/io/PrintWriter;Z)V

    return-void
.end method

.method dump(Ljava/lang/String;Ljava/io/PrintWriter;Z)V
    .locals 6

    goto/32 :goto_29

    nop

    :goto_0
    iget v5, v3, Landroid/app/BackStackRecord$Op;->enterAnim:I

    goto/32 :goto_99

    nop

    :goto_1
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_53

    nop

    :goto_2
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_75

    nop

    :goto_3
    iget v5, v3, Landroid/app/BackStackRecord$Op;->popEnterAnim:I

    goto/32 :goto_2b

    nop

    :goto_4
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_7d

    nop

    :goto_5
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_40

    nop

    :goto_6
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_5d

    nop

    :goto_7
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_35

    nop

    :goto_8
    iget v5, v3, Landroid/app/BackStackRecord$Op;->cmd:I

    goto/32 :goto_24

    nop

    :goto_9
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_46

    nop

    :goto_a
    if-nez p3, :cond_0

    goto/32 :goto_a4

    :cond_0
    goto/32 :goto_7b

    nop

    :goto_b
    iget-object v0, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_28

    nop

    :goto_c
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_a2

    nop

    :goto_d
    const-string v5, ": "

    goto/32 :goto_9

    nop

    :goto_e
    iget-object v5, v3, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    goto/32 :goto_1a

    nop

    :goto_f
    iget-object v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleText:Ljava/lang/CharSequence;

    goto/32 :goto_79

    nop

    :goto_10
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_9b

    nop

    :goto_11
    const-string v5, " "

    goto/32 :goto_66

    nop

    :goto_12
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_ac

    nop

    :goto_13
    goto/16 :goto_6b

    :pswitch_0
    goto/32 :goto_2d

    nop

    :goto_14
    const-string v4, "DETACH"

    goto/32 :goto_86

    nop

    :goto_15
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_31

    nop

    :goto_16
    if-eqz v0, :cond_1

    goto/32 :goto_7a

    :cond_1
    goto/32 :goto_f

    nop

    :goto_17
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_a3

    nop

    :goto_18
    const-string v0, " mBreadCrumbShortTitleText="

    goto/32 :goto_a0

    nop

    :goto_19
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_97

    nop

    :goto_1a
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    goto/32 :goto_a

    nop

    :goto_1b
    iget-boolean v0, p0, Landroid/app/BackStackRecord;->mCommitted:Z

    goto/32 :goto_58

    nop

    :goto_1c
    const-string v0, " mTransitionStyle=#"

    goto/32 :goto_23

    nop

    :goto_1d
    if-nez v0, :cond_2

    goto/32 :goto_b9

    :cond_2
    :goto_1e
    goto/32 :goto_b6

    nop

    :goto_1f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_37

    nop

    :goto_20
    goto/16 :goto_6b

    :pswitch_1
    goto/32 :goto_2a

    nop

    :goto_21
    if-nez v5, :cond_3

    goto/32 :goto_bb

    :cond_3
    :goto_22
    goto/32 :goto_95

    nop

    :goto_23
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_48

    nop

    :goto_24
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_3f

    nop

    :goto_25
    goto/16 :goto_41

    :goto_26
    goto/32 :goto_a1

    nop

    :goto_27
    const-string v5, "enterAnim=#"

    goto/32 :goto_61

    nop

    :goto_28
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    goto/32 :goto_70

    nop

    :goto_29
    if-nez p3, :cond_4

    goto/32 :goto_63

    :cond_4
    goto/32 :goto_69

    nop

    :goto_2a
    const-string v4, "ADD"

    goto/32 :goto_9e

    nop

    :goto_2b
    if-eqz v5, :cond_5

    goto/32 :goto_b0

    :cond_5
    goto/32 :goto_9d

    nop

    :goto_2c
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_1b

    nop

    :goto_2d
    const-string v4, "ATTACH"

    goto/32 :goto_2f

    nop

    :goto_2e
    iget v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleRes:I

    goto/32 :goto_16

    nop

    :goto_2f
    goto/16 :goto_6b

    :pswitch_2
    goto/32 :goto_14

    nop

    :goto_30
    goto/16 :goto_6b

    :pswitch_3
    goto/32 :goto_84

    nop

    :goto_31
    const-string v5, " exitAnim=#"

    goto/32 :goto_c

    nop

    :goto_32
    const-string v0, " mCommitted="

    goto/32 :goto_2c

    nop

    :goto_33
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_a8

    nop

    :goto_34
    iget v0, p0, Landroid/app/BackStackRecord;->mExitAnim:I

    goto/32 :goto_5e

    nop

    :goto_35
    iget-object v1, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_5

    nop

    :goto_36
    const-string/jumbo v0, "mBreadCrumbShortTitleRes=#"

    goto/32 :goto_10

    nop

    :goto_37
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_7e

    nop

    :goto_38
    if-nez v0, :cond_6

    goto/32 :goto_8d

    :cond_6
    goto/32 :goto_bf

    nop

    :goto_39
    iget v0, p0, Landroid/app/BackStackRecord;->mTransition:I

    goto/32 :goto_a6

    nop

    :goto_3a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_39

    nop

    :goto_3b
    goto/16 :goto_6b

    :pswitch_4
    goto/32 :goto_ad

    nop

    :goto_3c
    const-string v4, "REMOVE"

    goto/32 :goto_3b

    nop

    :goto_3d
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_1f

    nop

    :goto_3e
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_c0

    nop

    :goto_3f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_65

    nop

    :goto_40
    const/4 v2, 0x0

    :goto_41
    goto/32 :goto_49

    nop

    :goto_42
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_b8

    nop

    :goto_43
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_64

    nop

    :goto_44
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/32 :goto_3d

    nop

    :goto_45
    iget-object v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbTitleText:Ljava/lang/CharSequence;

    goto/32 :goto_b4

    nop

    :goto_46
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_11

    nop

    :goto_47
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_1c

    nop

    :goto_48
    iget v0, p0, Landroid/app/BackStackRecord;->mTransitionStyle:I

    goto/32 :goto_5f

    nop

    :goto_49
    if-lt v2, v1, :cond_7

    goto/32 :goto_26

    :cond_7
    goto/32 :goto_80

    nop

    :goto_4a
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_ba

    nop

    :goto_4b
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_a9

    nop

    :goto_4c
    const-string/jumbo v0, "mEnterAnim=#"

    goto/32 :goto_87

    nop

    :goto_4d
    const-string v5, " popExitAnim=#"

    goto/32 :goto_19

    nop

    :goto_4e
    const-string v4, "HIDE"

    goto/32 :goto_6c

    nop

    :goto_4f
    const-string v4, "SHOW"

    goto/32 :goto_93

    nop

    :goto_50
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_b2

    nop

    :goto_51
    if-eqz v0, :cond_8

    goto/32 :goto_5b

    :cond_8
    goto/32 :goto_52

    nop

    :goto_52
    iget-object v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbTitleText:Ljava/lang/CharSequence;

    goto/32 :goto_5a

    nop

    :goto_53
    const-string v5, "cmd="

    goto/32 :goto_92

    nop

    :goto_54
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_83

    nop

    :goto_55
    if-eqz v0, :cond_9

    goto/32 :goto_1e

    :cond_9
    goto/32 :goto_b7

    nop

    :goto_56
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_45

    nop

    :goto_57
    if-eqz v5, :cond_a

    goto/32 :goto_22

    :cond_a
    goto/32 :goto_ab

    nop

    :goto_58
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    goto/32 :goto_72

    nop

    :goto_59
    iget v0, p0, Landroid/app/BackStackRecord;->mExitAnim:I

    goto/32 :goto_73

    nop

    :goto_5a
    if-nez v0, :cond_b

    goto/32 :goto_b5

    :cond_b
    :goto_5b
    goto/32 :goto_33

    nop

    :goto_5c
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_78

    nop

    :goto_5d
    iget v5, v3, Landroid/app/BackStackRecord$Op;->popEnterAnim:I

    goto/32 :goto_9c

    nop

    :goto_5e
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_6d

    nop

    :goto_5f
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_8c

    nop

    :goto_60
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_88

    nop

    :goto_61
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_0

    nop

    :goto_62
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :goto_63
    goto/32 :goto_b

    nop

    :goto_64
    check-cast v3, Landroid/app/BackStackRecord$Op;

    goto/32 :goto_be

    nop

    :goto_65
    goto :goto_6b

    :pswitch_5
    goto/32 :goto_68

    nop

    :goto_66
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_e

    nop

    :goto_67
    iget v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbTitleRes:I

    goto/32 :goto_51

    nop

    :goto_68
    const-string v4, "UNSET_PRIMARY_NAV"

    goto/32 :goto_30

    nop

    :goto_69
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_b3

    nop

    :goto_6a
    const-string v4, "NULL"

    nop

    :goto_6b
    goto/32 :goto_50

    nop

    :goto_6c
    goto :goto_6b

    :pswitch_6
    goto/32 :goto_3c

    nop

    :goto_6d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :goto_6e
    goto/32 :goto_7f

    nop

    :goto_6f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_ae

    nop

    :goto_70
    if-eqz v0, :cond_c

    goto/32 :goto_26

    :cond_c
    goto/32 :goto_2

    nop

    :goto_71
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_a7

    nop

    :goto_72
    iget v0, p0, Landroid/app/BackStackRecord;->mTransition:I

    goto/32 :goto_38

    nop

    :goto_73
    if-nez v0, :cond_d

    goto/32 :goto_6e

    :cond_d
    :goto_74
    goto/32 :goto_8e

    nop

    :goto_75
    const-string v0, "Operations:"

    goto/32 :goto_44

    nop

    :goto_76
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_90

    nop

    :goto_77
    iget v0, p0, Landroid/app/BackStackRecord;->mPopExitAnim:I

    goto/32 :goto_42

    nop

    :goto_78
    iget v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbTitleRes:I

    goto/32 :goto_82

    nop

    :goto_79
    if-nez v0, :cond_e

    goto/32 :goto_63

    :cond_e
    :goto_7a
    goto/32 :goto_b1

    nop

    :goto_7b
    iget v5, v3, Landroid/app/BackStackRecord$Op;->enterAnim:I

    goto/32 :goto_57

    nop

    :goto_7c
    const-string/jumbo v0, "mTransition=#"

    goto/32 :goto_3a

    nop

    :goto_7d
    iget v0, p0, Landroid/app/BackStackRecord;->mIndex:I

    goto/32 :goto_89

    nop

    :goto_7e
    const-string v1, "    "

    goto/32 :goto_bc

    nop

    :goto_7f
    iget v0, p0, Landroid/app/BackStackRecord;->mPopEnterAnim:I

    goto/32 :goto_55

    nop

    :goto_80
    iget-object v3, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_43

    nop

    :goto_81
    const-string/jumbo v0, "mPopEnterAnim=#"

    goto/32 :goto_60

    nop

    :goto_82
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_71

    nop

    :goto_83
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_18

    nop

    :goto_84
    const-string v4, "SET_PRIMARY_NAV"

    goto/32 :goto_13

    nop

    :goto_85
    iget v0, p0, Landroid/app/BackStackRecord;->mEnterAnim:I

    goto/32 :goto_98

    nop

    :goto_86
    goto/16 :goto_6b

    :pswitch_7
    goto/32 :goto_4f

    nop

    :goto_87
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_9f

    nop

    :goto_88
    iget v0, p0, Landroid/app/BackStackRecord;->mPopEnterAnim:I

    goto/32 :goto_aa

    nop

    :goto_89
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    goto/32 :goto_32

    nop

    :goto_8a
    iget-object v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleText:Ljava/lang/CharSequence;

    goto/32 :goto_62

    nop

    :goto_8b
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_1

    nop

    :goto_8c
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :goto_8d
    goto/32 :goto_85

    nop

    :goto_8e
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_4c

    nop

    :goto_8f
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_4d

    nop

    :goto_90
    const-string v0, " mPopExitAnim=#"

    goto/32 :goto_96

    nop

    :goto_91
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_9a

    nop

    :goto_92
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_8

    nop

    :goto_93
    goto/16 :goto_6b

    :pswitch_8
    goto/32 :goto_4e

    nop

    :goto_94
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_34

    nop

    :goto_95
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_27

    nop

    :goto_96
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_77

    nop

    :goto_97
    iget v5, v3, Landroid/app/BackStackRecord$Op;->popExitAnim:I

    goto/32 :goto_17

    nop

    :goto_98
    if-eqz v0, :cond_f

    goto/32 :goto_74

    :cond_f
    goto/32 :goto_59

    nop

    :goto_99
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_15

    nop

    :goto_9a
    const-string v0, " mIndex="

    goto/32 :goto_4

    nop

    :goto_9b
    iget v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleRes:I

    goto/32 :goto_54

    nop

    :goto_9c
    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_8f

    nop

    :goto_9d
    iget v5, v3, Landroid/app/BackStackRecord$Op;->popExitAnim:I

    goto/32 :goto_af

    nop

    :goto_9e
    goto/16 :goto_6b

    :pswitch_9
    goto/32 :goto_6a

    nop

    :goto_9f
    iget v0, p0, Landroid/app/BackStackRecord;->mEnterAnim:I

    goto/32 :goto_bd

    nop

    :goto_a0
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_8a

    nop

    :goto_a1
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_9
        :pswitch_1
        :pswitch_4
        :pswitch_6
        :pswitch_8
        :pswitch_7
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_5
    .end packed-switch

    :goto_a2
    iget v5, v3, Landroid/app/BackStackRecord$Op;->exitAnim:I

    goto/32 :goto_4a

    nop

    :goto_a3
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :goto_a4
    goto/32 :goto_a5

    nop

    :goto_a5
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_25

    nop

    :goto_a6
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_47

    nop

    :goto_a7
    const-string v0, " mBreadCrumbTitleText="

    goto/32 :goto_56

    nop

    :goto_a8
    const-string/jumbo v0, "mBreadCrumbTitleRes=#"

    goto/32 :goto_5c

    nop

    :goto_a9
    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(I)V

    goto/32 :goto_d

    nop

    :goto_aa
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_76

    nop

    :goto_ab
    iget v5, v3, Landroid/app/BackStackRecord$Op;->exitAnim:I

    goto/32 :goto_21

    nop

    :goto_ac
    const-string/jumbo v5, "popEnterAnim=#"

    goto/32 :goto_6

    nop

    :goto_ad
    const-string v4, "REPLACE"

    goto/32 :goto_20

    nop

    :goto_ae
    iget-object v0, p0, Landroid/app/BackStackRecord;->mName:Ljava/lang/String;

    goto/32 :goto_91

    nop

    :goto_af
    if-nez v5, :cond_10

    goto/32 :goto_a4

    :cond_10
    :goto_b0
    goto/32 :goto_12

    nop

    :goto_b1
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_36

    nop

    :goto_b2
    const-string v5, "  Op #"

    goto/32 :goto_4b

    nop

    :goto_b3
    const-string/jumbo v0, "mName="

    goto/32 :goto_6f

    nop

    :goto_b4
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :goto_b5
    goto/32 :goto_2e

    nop

    :goto_b6
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_81

    nop

    :goto_b7
    iget v0, p0, Landroid/app/BackStackRecord;->mPopExitAnim:I

    goto/32 :goto_1d

    nop

    :goto_b8
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :goto_b9
    goto/32 :goto_67

    nop

    :goto_ba
    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :goto_bb
    goto/32 :goto_3

    nop

    :goto_bc
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_bd
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_3e

    nop

    :goto_be
    iget v4, v3, Landroid/app/BackStackRecord$Op;->cmd:I

    packed-switch v4, :pswitch_data_0

    goto/32 :goto_8b

    nop

    :goto_bf
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_7c

    nop

    :goto_c0
    const-string v0, " mExitAnim=#"

    goto/32 :goto_94

    nop
.end method

.method executeOps()V
    .locals 7

    goto/32 :goto_2e

    nop

    :goto_0
    const/4 v6, 0x0

    goto/32 :goto_6

    nop

    :goto_1
    iget v5, v3, Landroid/app/BackStackRecord$Op;->cmd:I

    goto/32 :goto_12

    nop

    :goto_2
    goto/16 :goto_1b

    :pswitch_0
    goto/32 :goto_a

    nop

    :goto_3
    if-eqz v1, :cond_0

    goto/32 :goto_14

    :cond_0
    goto/32 :goto_b

    nop

    :goto_4
    goto/16 :goto_1b

    :pswitch_1
    goto/32 :goto_2c

    nop

    :goto_5
    iget v5, v3, Landroid/app/BackStackRecord$Op;->exitAnim:I

    goto/32 :goto_8

    nop

    :goto_6
    invoke-virtual {v5, v6}, Landroid/app/FragmentManagerImpl;->setPrimaryNavigationFragment(Landroid/app/Fragment;)V

    goto/32 :goto_19

    nop

    :goto_7
    iget-object v4, v3, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    goto/32 :goto_38

    nop

    :goto_8
    invoke-virtual {v4, v5}, Landroid/app/Fragment;->setNextAnim(I)V

    goto/32 :goto_20

    nop

    :goto_9
    new-instance v5, Ljava/lang/StringBuilder;

    goto/32 :goto_1f

    nop

    :goto_a
    iget v5, v3, Landroid/app/BackStackRecord$Op;->enterAnim:I

    goto/32 :goto_36

    nop

    :goto_b
    iget-object v1, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_33

    nop

    :goto_c
    iget-object v5, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_4a

    nop

    :goto_d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/32 :goto_43

    nop

    :goto_e
    invoke-virtual {v5, v4}, Landroid/app/FragmentManagerImpl;->setPrimaryNavigationFragment(Landroid/app/Fragment;)V

    goto/32 :goto_2

    nop

    :goto_f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_47

    nop

    :goto_10
    invoke-virtual {v2, v4}, Landroid/app/FragmentManagerImpl;->moveFragmentToExpectedState(Landroid/app/Fragment;)V

    :goto_11
    goto/32 :goto_26

    nop

    :goto_12
    if-ne v5, v2, :cond_1

    goto/32 :goto_11

    :cond_1
    goto/32 :goto_37

    nop

    :goto_13
    invoke-virtual {v1, v3, v2}, Landroid/app/FragmentManagerImpl;->moveToState(IZ)V

    :goto_14
    goto/32 :goto_4d

    nop

    :goto_15
    invoke-virtual {v4, v5, v6}, Landroid/app/Fragment;->setNextTransition(II)V

    :goto_16
    goto/32 :goto_4c

    nop

    :goto_17
    iget-object v5, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_0

    nop

    :goto_18
    const/4 v2, 0x1

    goto/32 :goto_44

    nop

    :goto_19
    goto :goto_1b

    :pswitch_2
    goto/32 :goto_46

    nop

    :goto_1a
    invoke-virtual {v5, v4, v6}, Landroid/app/FragmentManagerImpl;->addFragment(Landroid/app/Fragment;Z)V

    nop

    :goto_1b
    goto/32 :goto_3f

    nop

    :goto_1c
    invoke-virtual {v4, v5}, Landroid/app/Fragment;->setNextAnim(I)V

    goto/32 :goto_40

    nop

    :goto_1d
    throw v2

    :pswitch_3
    goto/32 :goto_17

    nop

    :goto_1e
    iget-object v5, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_2a

    nop

    :goto_1f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_34

    nop

    :goto_20
    iget-object v5, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_29

    nop

    :goto_21
    goto :goto_1b

    :pswitch_4
    goto/32 :goto_25

    nop

    :goto_22
    iget-boolean v1, p0, Landroid/app/BackStackRecord;->mReorderingAllowed:Z

    goto/32 :goto_3

    nop

    :goto_23
    goto/16 :goto_32

    :goto_24
    goto/32 :goto_22

    nop

    :goto_25
    iget v5, v3, Landroid/app/BackStackRecord$Op;->exitAnim:I

    goto/32 :goto_4e

    nop

    :goto_26
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_23

    nop

    :goto_27
    invoke-virtual {v4, v5}, Landroid/app/Fragment;->setNextAnim(I)V

    goto/32 :goto_1e

    nop

    :goto_28
    invoke-virtual {v5, v4}, Landroid/app/FragmentManagerImpl;->showFragment(Landroid/app/Fragment;)V

    goto/32 :goto_4f

    nop

    :goto_29
    invoke-virtual {v5, v4}, Landroid/app/FragmentManagerImpl;->hideFragment(Landroid/app/Fragment;)V

    goto/32 :goto_21

    nop

    :goto_2a
    const/4 v6, 0x0

    goto/32 :goto_1a

    nop

    :goto_2b
    iget v5, v3, Landroid/app/BackStackRecord$Op;->exitAnim:I

    goto/32 :goto_1c

    nop

    :goto_2c
    iget v5, v3, Landroid/app/BackStackRecord$Op;->enterAnim:I

    goto/32 :goto_27

    nop

    :goto_2d
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_3e

    nop

    :goto_2e
    iget-object v0, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_41

    nop

    :goto_2f
    invoke-virtual {v5, v4}, Landroid/app/FragmentManagerImpl;->removeFragment(Landroid/app/Fragment;)V

    goto/32 :goto_4

    nop

    :goto_30
    iget-object v3, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_2d

    nop

    :goto_31
    const/4 v1, 0x0

    :goto_32
    goto/32 :goto_18

    nop

    :goto_33
    iget v3, v1, Landroid/app/FragmentManagerImpl;->mCurState:I

    goto/32 :goto_13

    nop

    :goto_34
    const-string v6, "Unknown cmd: "

    goto/32 :goto_d

    nop

    :goto_35
    if-eqz v5, :cond_2

    goto/32 :goto_11

    :cond_2
    goto/32 :goto_1

    nop

    :goto_36
    invoke-virtual {v4, v5}, Landroid/app/Fragment;->setNextAnim(I)V

    goto/32 :goto_c

    nop

    :goto_37
    if-nez v4, :cond_3

    goto/32 :goto_11

    :cond_3
    goto/32 :goto_3a

    nop

    :goto_38
    if-nez v4, :cond_4

    goto/32 :goto_16

    :cond_4
    goto/32 :goto_39

    nop

    :goto_39
    iget v5, p0, Landroid/app/BackStackRecord;->mTransition:I

    goto/32 :goto_49

    nop

    :goto_3a
    iget-object v2, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_10

    nop

    :goto_3b
    iget v5, v3, Landroid/app/BackStackRecord$Op;->enterAnim:I

    goto/32 :goto_4b

    nop

    :goto_3c
    goto/16 :goto_1b

    :pswitch_5
    goto/32 :goto_3b

    nop

    :goto_3d
    new-instance v2, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_9

    nop

    :goto_3e
    check-cast v3, Landroid/app/BackStackRecord$Op;

    goto/32 :goto_7

    nop

    :goto_3f
    iget-boolean v5, p0, Landroid/app/BackStackRecord;->mReorderingAllowed:Z

    goto/32 :goto_35

    nop

    :goto_40
    iget-object v5, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_50

    nop

    :goto_41
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_31

    nop

    :goto_42
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/32 :goto_f

    nop

    :goto_43
    iget v6, v3, Landroid/app/BackStackRecord$Op;->cmd:I

    goto/32 :goto_42

    nop

    :goto_44
    if-lt v1, v0, :cond_5

    goto/32 :goto_24

    :cond_5
    goto/32 :goto_30

    nop

    :goto_45
    iget-object v5, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_2f

    nop

    :goto_46
    iget-object v5, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_e

    nop

    :goto_47
    invoke-direct {v2, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_1d

    nop

    :goto_48
    goto/16 :goto_1b

    :pswitch_6
    goto/32 :goto_2b

    nop

    :goto_49
    iget v6, p0, Landroid/app/BackStackRecord;->mTransitionStyle:I

    goto/32 :goto_15

    nop

    :goto_4a
    invoke-virtual {v5, v4}, Landroid/app/FragmentManagerImpl;->attachFragment(Landroid/app/Fragment;)V

    goto/32 :goto_48

    nop

    :goto_4b
    invoke-virtual {v4, v5}, Landroid/app/Fragment;->setNextAnim(I)V

    goto/32 :goto_51

    nop

    :goto_4c
    iget v5, v3, Landroid/app/BackStackRecord$Op;->cmd:I

    packed-switch v5, :pswitch_data_0

    :pswitch_7
    goto/32 :goto_3d

    nop

    :goto_4d
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_7
        :pswitch_4
        :pswitch_8
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :goto_4e
    invoke-virtual {v4, v5}, Landroid/app/Fragment;->setNextAnim(I)V

    goto/32 :goto_45

    nop

    :goto_4f
    goto/16 :goto_1b

    :pswitch_8
    goto/32 :goto_5

    nop

    :goto_50
    invoke-virtual {v5, v4}, Landroid/app/FragmentManagerImpl;->detachFragment(Landroid/app/Fragment;)V

    goto/32 :goto_3c

    nop

    :goto_51
    iget-object v5, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_28

    nop
.end method

.method executePopOps(Z)V
    .locals 6

    goto/32 :goto_4

    nop

    :goto_0
    iget v4, v2, Landroid/app/BackStackRecord$Op;->popExitAnim:I

    goto/32 :goto_3e

    nop

    :goto_1
    invoke-virtual {v4, v3}, Landroid/app/FragmentManagerImpl;->setPrimaryNavigationFragment(Landroid/app/Fragment;)V

    goto/32 :goto_3c

    nop

    :goto_2
    if-eqz v0, :cond_0

    goto/32 :goto_4a

    :cond_0
    goto/32 :goto_48

    nop

    :goto_3
    iget v4, p0, Landroid/app/BackStackRecord;->mTransition:I

    goto/32 :goto_30

    nop

    :goto_4
    iget-object v0, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_3f

    nop

    :goto_5
    if-gez v0, :cond_1

    goto/32 :goto_51

    :cond_1
    goto/32 :goto_44

    nop

    :goto_6
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_4f

    nop

    :goto_7
    const-string v5, "Unknown cmd: "

    goto/32 :goto_47

    nop

    :goto_8
    invoke-virtual {v3, v4}, Landroid/app/Fragment;->setNextAnim(I)V

    goto/32 :goto_e

    nop

    :goto_9
    iget v4, v2, Landroid/app/BackStackRecord$Op;->popExitAnim:I

    goto/32 :goto_42

    nop

    :goto_a
    iget-object v0, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_1e

    nop

    :goto_b
    if-nez v3, :cond_2

    goto/32 :goto_d

    :cond_2
    goto/32 :goto_3

    nop

    :goto_c
    invoke-virtual {v3, v4, v5}, Landroid/app/Fragment;->setNextTransition(II)V

    :goto_d
    goto/32 :goto_17

    nop

    :goto_e
    iget-object v4, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_4e

    nop

    :goto_f
    iget-boolean v0, p0, Landroid/app/BackStackRecord;->mReorderingAllowed:Z

    goto/32 :goto_2

    nop

    :goto_10
    iget-object v4, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_12

    nop

    :goto_11
    iget-object v4, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_4b

    nop

    :goto_12
    invoke-virtual {v4, v3}, Landroid/app/FragmentManagerImpl;->removeFragment(Landroid/app/Fragment;)V

    nop

    :goto_13
    goto/32 :goto_39

    nop

    :goto_14
    iget-object v4, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_1d

    nop

    :goto_15
    iget v5, v2, Landroid/app/BackStackRecord$Op;->cmd:I

    goto/32 :goto_25

    nop

    :goto_16
    const/4 v1, 0x1

    goto/32 :goto_2e

    nop

    :goto_17
    iget v4, v2, Landroid/app/BackStackRecord$Op;->cmd:I

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    goto/32 :goto_43

    nop

    :goto_18
    invoke-virtual {v4, v3}, Landroid/app/FragmentManagerImpl;->showFragment(Landroid/app/Fragment;)V

    goto/32 :goto_33

    nop

    :goto_19
    iget v5, p0, Landroid/app/BackStackRecord;->mTransitionStyle:I

    goto/32 :goto_c

    nop

    :goto_1a
    if-ne v4, v5, :cond_3

    goto/32 :goto_29

    :cond_3
    goto/32 :goto_3a

    nop

    :goto_1b
    invoke-virtual {v3, v4}, Landroid/app/Fragment;->setNextAnim(I)V

    goto/32 :goto_2b

    nop

    :goto_1c
    const/4 v5, 0x3

    goto/32 :goto_1a

    nop

    :goto_1d
    invoke-virtual {v4, v3}, Landroid/app/FragmentManagerImpl;->detachFragment(Landroid/app/Fragment;)V

    goto/32 :goto_22

    nop

    :goto_1e
    iget v2, v0, Landroid/app/FragmentManagerImpl;->mCurState:I

    goto/32 :goto_49

    nop

    :goto_1f
    goto :goto_13

    :pswitch_1
    goto/32 :goto_45

    nop

    :goto_20
    const/4 v5, 0x0

    goto/32 :goto_37

    nop

    :goto_21
    iget-object v4, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_20

    nop

    :goto_22
    goto :goto_13

    :pswitch_2
    goto/32 :goto_54

    nop

    :goto_23
    invoke-direct {v1, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_40

    nop

    :goto_24
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_23

    nop

    :goto_25
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_24

    nop

    :goto_26
    goto :goto_13

    :pswitch_3
    goto/32 :goto_9

    nop

    :goto_27
    iget-object v4, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_35

    nop

    :goto_28
    invoke-virtual {v4, v3}, Landroid/app/FragmentManagerImpl;->moveFragmentToExpectedState(Landroid/app/Fragment;)V

    :goto_29
    goto/32 :goto_2c

    nop

    :goto_2a
    iget-object v4, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_1

    nop

    :goto_2b
    iget-object v4, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_18

    nop

    :goto_2c
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_50

    nop

    :goto_2d
    goto/16 :goto_13

    :pswitch_4
    goto/32 :goto_0

    nop

    :goto_2e
    sub-int/2addr v0, v1

    :goto_2f
    goto/32 :goto_5

    nop

    :goto_30
    invoke-static {v4}, Landroid/app/FragmentManagerImpl;->reverseTransit(I)I

    move-result v4

    goto/32 :goto_19

    nop

    :goto_31
    iget v4, v2, Landroid/app/BackStackRecord$Op;->popEnterAnim:I

    goto/32 :goto_52

    nop

    :goto_32
    check-cast v2, Landroid/app/BackStackRecord$Op;

    goto/32 :goto_3d

    nop

    :goto_33
    goto/16 :goto_13

    :pswitch_5
    goto/32 :goto_31

    nop

    :goto_34
    goto/16 :goto_13

    :pswitch_6
    goto/32 :goto_4c

    nop

    :goto_35
    const/4 v5, 0x0

    goto/32 :goto_36

    nop

    :goto_36
    invoke-virtual {v4, v5}, Landroid/app/FragmentManagerImpl;->setPrimaryNavigationFragment(Landroid/app/Fragment;)V

    goto/32 :goto_34

    nop

    :goto_37
    invoke-virtual {v4, v3, v5}, Landroid/app/FragmentManagerImpl;->addFragment(Landroid/app/Fragment;Z)V

    goto/32 :goto_2d

    nop

    :goto_38
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :goto_39
    iget-boolean v4, p0, Landroid/app/BackStackRecord;->mReorderingAllowed:Z

    goto/32 :goto_53

    nop

    :goto_3a
    if-nez v3, :cond_4

    goto/32 :goto_29

    :cond_4
    goto/32 :goto_4d

    nop

    :goto_3b
    invoke-virtual {v3, v4}, Landroid/app/Fragment;->setNextAnim(I)V

    goto/32 :goto_14

    nop

    :goto_3c
    goto/16 :goto_13

    :pswitch_7
    goto/32 :goto_27

    nop

    :goto_3d
    iget-object v3, v2, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    goto/32 :goto_b

    nop

    :goto_3e
    invoke-virtual {v3, v4}, Landroid/app/Fragment;->setNextAnim(I)V

    goto/32 :goto_10

    nop

    :goto_3f
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_16

    nop

    :goto_40
    throw v1

    :pswitch_8
    goto/32 :goto_2a

    nop

    :goto_41
    iget v4, v2, Landroid/app/BackStackRecord$Op;->cmd:I

    goto/32 :goto_1c

    nop

    :goto_42
    invoke-virtual {v3, v4}, Landroid/app/Fragment;->setNextAnim(I)V

    goto/32 :goto_11

    nop

    :goto_43
    new-instance v1, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_6

    nop

    :goto_44
    iget-object v2, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_46

    nop

    :goto_45
    iget v4, v2, Landroid/app/BackStackRecord$Op;->popEnterAnim:I

    goto/32 :goto_1b

    nop

    :goto_46
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_32

    nop

    :goto_47
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_15

    nop

    :goto_48
    if-nez p1, :cond_5

    goto/32 :goto_4a

    :cond_5
    goto/32 :goto_a

    nop

    :goto_49
    invoke-virtual {v0, v2, v1}, Landroid/app/FragmentManagerImpl;->moveToState(IZ)V

    :goto_4a
    goto/32 :goto_38

    nop

    :goto_4b
    invoke-virtual {v4, v3}, Landroid/app/FragmentManagerImpl;->hideFragment(Landroid/app/Fragment;)V

    goto/32 :goto_1f

    nop

    :goto_4c
    iget v4, v2, Landroid/app/BackStackRecord$Op;->popExitAnim:I

    goto/32 :goto_3b

    nop

    :goto_4d
    iget-object v4, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    goto/32 :goto_28

    nop

    :goto_4e
    invoke-virtual {v4, v3}, Landroid/app/FragmentManagerImpl;->attachFragment(Landroid/app/Fragment;)V

    goto/32 :goto_26

    nop

    :goto_4f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_7

    nop

    :goto_50
    goto/16 :goto_2f

    :goto_51
    goto/32 :goto_f

    nop

    :goto_52
    invoke-virtual {v3, v4}, Landroid/app/Fragment;->setNextAnim(I)V

    goto/32 :goto_21

    nop

    :goto_53
    if-eqz v4, :cond_6

    goto/32 :goto_29

    :cond_6
    goto/32 :goto_41

    nop

    :goto_54
    iget v4, v2, Landroid/app/BackStackRecord$Op;->popEnterAnim:I

    goto/32 :goto_8

    nop
.end method

.method expandOps(Ljava/util/ArrayList;Landroid/app/Fragment;)Landroid/app/Fragment;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/app/Fragment;",
            ">;",
            "Landroid/app/Fragment;",
            ")",
            "Landroid/app/Fragment;"
        }
    .end annotation

    goto/32 :goto_17

    nop

    :goto_0
    const/4 v10, 0x3

    goto/32 :goto_d

    nop

    :goto_1
    add-int/2addr v0, v4

    :goto_2
    goto/32 :goto_1c

    nop

    :goto_3
    invoke-virtual {p1, v8}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/32 :goto_1

    nop

    :goto_4
    iget-object v6, v1, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    goto/32 :goto_1b

    nop

    :goto_5
    iget-object v9, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_52

    nop

    :goto_6
    iput v4, v1, Landroid/app/BackStackRecord$Op;->cmd:I

    goto/32 :goto_2e

    nop

    :goto_7
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_1d

    nop

    :goto_8
    return-object p2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_4
        :pswitch_1
        :pswitch_3
        :pswitch_0
    .end packed-switch

    :goto_9
    iget v2, v1, Landroid/app/BackStackRecord$Op;->cmd:I

    goto/32 :goto_15

    nop

    :goto_a
    invoke-virtual {v9, v0, v10}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto/32 :goto_4b

    nop

    :goto_b
    iget v10, v1, Landroid/app/BackStackRecord$Op;->enterAnim:I

    goto/32 :goto_45

    nop

    :goto_c
    invoke-direct {v10, v3, v8}, Landroid/app/BackStackRecord$Op;-><init>(ILandroid/app/Fragment;)V

    goto/32 :goto_a

    nop

    :goto_d
    invoke-direct {v9, v10, v8}, Landroid/app/BackStackRecord$Op;-><init>(ILandroid/app/Fragment;)V

    goto/32 :goto_b

    nop

    :goto_e
    iget v9, v8, Landroid/app/Fragment;->mContainerId:I

    goto/32 :goto_30

    nop

    :goto_f
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    nop

    :goto_10
    goto/32 :goto_57

    nop

    :goto_11
    iput v10, v9, Landroid/app/BackStackRecord$Op;->popEnterAnim:I

    goto/32 :goto_33

    nop

    :goto_12
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto/32 :goto_7

    nop

    :goto_13
    goto/16 :goto_18

    :goto_14
    goto/32 :goto_8

    nop

    :goto_15
    const/16 v3, 0x9

    goto/32 :goto_4f

    nop

    :goto_16
    new-instance v5, Landroid/app/BackStackRecord$Op;

    goto/32 :goto_4

    nop

    :goto_17
    const/4 v0, 0x0

    :goto_18
    goto/32 :goto_1f

    nop

    :goto_19
    invoke-virtual {v10, v0, v9}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto/32 :goto_3

    nop

    :goto_1a
    invoke-virtual {v2, v0, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto/32 :goto_31

    nop

    :goto_1b
    invoke-direct {v5, v3, v6}, Landroid/app/BackStackRecord$Op;-><init>(ILandroid/app/Fragment;)V

    goto/32 :goto_28

    nop

    :goto_1c
    add-int/lit8 v7, v7, -0x1

    goto/32 :goto_4c

    nop

    :goto_1d
    goto :goto_2f

    :goto_1e
    goto/32 :goto_6

    nop

    :goto_1f
    iget-object v1, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_49

    nop

    :goto_20
    goto/16 :goto_10

    :pswitch_0
    goto/32 :goto_2a

    nop

    :goto_21
    iget-object v3, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_12

    nop

    :goto_22
    const/4 v6, 0x0

    goto/32 :goto_44

    nop

    :goto_23
    iget-object v2, v1, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    goto/32 :goto_3b

    nop

    :goto_24
    iget-object v2, v1, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    goto/32 :goto_f

    nop

    :goto_25
    sub-int/2addr v7, v4

    :goto_26
    goto/32 :goto_3c

    nop

    :goto_27
    new-instance v9, Landroid/app/BackStackRecord$Op;

    goto/32 :goto_0

    nop

    :goto_28
    invoke-virtual {v2, v0, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto/32 :goto_50

    nop

    :goto_29
    if-nez v6, :cond_0

    goto/32 :goto_1e

    :cond_0
    goto/32 :goto_21

    nop

    :goto_2a
    iget-object v2, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_51

    nop

    :goto_2b
    check-cast v1, Landroid/app/BackStackRecord$Op;

    goto/32 :goto_9

    nop

    :goto_2c
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_2b

    nop

    :goto_2d
    goto/16 :goto_10

    :pswitch_1
    goto/32 :goto_4a

    nop

    :goto_2e
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2f
    goto/32 :goto_42

    nop

    :goto_30
    if-eq v9, v5, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_39

    nop

    :goto_31
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_54

    nop

    :goto_32
    iget-object v2, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_16

    nop

    :goto_33
    iget v10, v1, Landroid/app/BackStackRecord$Op;->exitAnim:I

    goto/32 :goto_55

    nop

    :goto_34
    check-cast v8, Landroid/app/Fragment;

    goto/32 :goto_e

    nop

    :goto_35
    const/4 p2, 0x0

    goto/32 :goto_3e

    nop

    :goto_36
    if-lt v0, v1, :cond_2

    goto/32 :goto_14

    :cond_2
    goto/32 :goto_47

    nop

    :goto_37
    const/4 p2, 0x0

    :goto_38
    goto/32 :goto_27

    nop

    :goto_39
    if-eq v8, v2, :cond_3

    goto/32 :goto_40

    :cond_3
    goto/32 :goto_4e

    nop

    :goto_3a
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/32 :goto_23

    nop

    :goto_3b
    if-eq v2, p2, :cond_4

    goto/32 :goto_10

    :cond_4
    goto/32 :goto_32

    nop

    :goto_3c
    if-gez v7, :cond_5

    goto/32 :goto_4d

    :cond_5
    goto/32 :goto_43

    nop

    :goto_3d
    iget v10, v1, Landroid/app/BackStackRecord$Op;->popEnterAnim:I

    goto/32 :goto_11

    nop

    :goto_3e
    goto/16 :goto_10

    :pswitch_2
    goto/32 :goto_41

    nop

    :goto_3f
    goto/16 :goto_2

    :goto_40
    goto/32 :goto_58

    nop

    :goto_41
    iget-object v2, v1, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    goto/32 :goto_56

    nop

    :goto_42
    goto/16 :goto_10

    :pswitch_3
    goto/32 :goto_24

    nop

    :goto_43
    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    goto/32 :goto_34

    nop

    :goto_44
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    goto/32 :goto_25

    nop

    :goto_45
    iput v10, v9, Landroid/app/BackStackRecord$Op;->enterAnim:I

    goto/32 :goto_3d

    nop

    :goto_46
    invoke-direct {v5, v3, p2}, Landroid/app/BackStackRecord$Op;-><init>(ILandroid/app/Fragment;)V

    goto/32 :goto_1a

    nop

    :goto_47
    iget-object v1, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_2c

    nop

    :goto_48
    iput v10, v9, Landroid/app/BackStackRecord$Op;->popExitAnim:I

    goto/32 :goto_53

    nop

    :goto_49
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_36

    nop

    :goto_4a
    iget-object v2, v1, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    goto/32 :goto_3a

    nop

    :goto_4b
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_37

    nop

    :goto_4c
    goto/16 :goto_26

    :goto_4d
    goto/32 :goto_29

    nop

    :goto_4e
    const/4 v6, 0x1

    goto/32 :goto_3f

    nop

    :goto_4f
    const/4 v4, 0x1

    packed-switch v2, :pswitch_data_0

    :pswitch_4
    goto/32 :goto_20

    nop

    :goto_50
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_35

    nop

    :goto_51
    new-instance v5, Landroid/app/BackStackRecord$Op;

    goto/32 :goto_46

    nop

    :goto_52
    new-instance v10, Landroid/app/BackStackRecord$Op;

    goto/32 :goto_c

    nop

    :goto_53
    iget-object v10, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_19

    nop

    :goto_54
    iget-object p2, v1, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    goto/32 :goto_2d

    nop

    :goto_55
    iput v10, v9, Landroid/app/BackStackRecord$Op;->exitAnim:I

    goto/32 :goto_59

    nop

    :goto_56
    iget v5, v2, Landroid/app/Fragment;->mContainerId:I

    goto/32 :goto_22

    nop

    :goto_57
    add-int/2addr v0, v4

    goto/32 :goto_13

    nop

    :goto_58
    if-eq v8, p2, :cond_6

    goto/32 :goto_38

    :cond_6
    goto/32 :goto_5

    nop

    :goto_59
    iget v10, v1, Landroid/app/BackStackRecord$Op;->popExitAnim:I

    goto/32 :goto_48

    nop
.end method

.method public generateOps(Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/app/BackStackRecord;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Boolean;",
            ">;)Z"
        }
    .end annotation

    sget-boolean v0, Landroid/app/FragmentManagerImpl;->DEBUG:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Run: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FragmentManager"

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-boolean v0, p0, Landroid/app/BackStackRecord;->mAddToBackStack:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    invoke-virtual {v0, p0}, Landroid/app/FragmentManagerImpl;->addBackStackState(Landroid/app/BackStackRecord;)V

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public getBreadCrumbShortTitle()Ljava/lang/CharSequence;
    .locals 2

    iget v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleRes:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    iget-object v0, v0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    iget-object v0, v0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    invoke-virtual {v0}, Landroid/app/FragmentHostCallback;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleRes:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getBreadCrumbShortTitleRes()I
    .locals 1

    iget v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleRes:I

    return v0
.end method

.method public getBreadCrumbTitle()Ljava/lang/CharSequence;
    .locals 2

    iget v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbTitleRes:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    iget-object v0, v0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/app/BackStackRecord;->mManager:Landroid/app/FragmentManagerImpl;

    iget-object v0, v0, Landroid/app/FragmentManagerImpl;->mHost:Landroid/app/FragmentHostCallback;

    invoke-virtual {v0}, Landroid/app/FragmentHostCallback;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Landroid/app/BackStackRecord;->mBreadCrumbTitleRes:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbTitleText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getBreadCrumbTitleRes()I
    .locals 1

    iget v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbTitleRes:I

    return v0
.end method

.method public getId()I
    .locals 1

    iget v0, p0, Landroid/app/BackStackRecord;->mIndex:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/app/BackStackRecord;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getTransition()I
    .locals 1

    iget v0, p0, Landroid/app/BackStackRecord;->mTransition:I

    return v0
.end method

.method public getTransitionStyle()I
    .locals 1

    iget v0, p0, Landroid/app/BackStackRecord;->mTransitionStyle:I

    return v0
.end method

.method public hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    .locals 2

    new-instance v0, Landroid/app/BackStackRecord$Op;

    const/4 v1, 0x4

    invoke-direct {v0, v1, p1}, Landroid/app/BackStackRecord$Op;-><init>(ILandroid/app/Fragment;)V

    invoke-virtual {p0, v0}, Landroid/app/BackStackRecord;->addOp(Landroid/app/BackStackRecord$Op;)V

    return-object p0
.end method

.method interactsWith(I)Z
    .locals 5

    goto/32 :goto_1

    nop

    :goto_0
    if-lt v1, v0, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_7

    nop

    :goto_1
    iget-object v0, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_14

    nop

    :goto_2
    if-nez v4, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_e

    nop

    :goto_3
    const/4 v4, 0x1

    goto/32 :goto_15

    nop

    :goto_4
    iget v2, v2, Landroid/app/Fragment;->mContainerId:I

    :goto_5
    goto/32 :goto_8

    nop

    :goto_6
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_f

    nop

    :goto_7
    iget-object v3, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_6

    nop

    :goto_8
    if-nez v2, :cond_2

    goto/32 :goto_16

    :cond_2
    goto/32 :goto_10

    nop

    :goto_9
    const/4 v2, 0x0

    goto/32 :goto_0

    nop

    :goto_a
    const/4 v1, 0x0

    :goto_b
    goto/32 :goto_9

    nop

    :goto_c
    goto :goto_b

    :goto_d
    goto/32 :goto_12

    nop

    :goto_e
    iget-object v2, v3, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    goto/32 :goto_4

    nop

    :goto_f
    check-cast v3, Landroid/app/BackStackRecord$Op;

    goto/32 :goto_13

    nop

    :goto_10
    if-eq v2, p1, :cond_3

    goto/32 :goto_16

    :cond_3
    goto/32 :goto_3

    nop

    :goto_11
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_c

    nop

    :goto_12
    return v2

    :goto_13
    iget-object v4, v3, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    goto/32 :goto_2

    nop

    :goto_14
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_a

    nop

    :goto_15
    return v4

    :goto_16
    goto/32 :goto_11

    nop
.end method

.method interactsWith(Ljava/util/ArrayList;II)Z
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/app/BackStackRecord;",
            ">;II)Z"
        }
    .end annotation

    goto/32 :goto_a

    nop

    :goto_0
    iget v11, v11, Landroid/app/Fragment;->mContainerId:I

    goto/32 :goto_1f

    nop

    :goto_1
    if-nez v5, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_2a

    nop

    :goto_2
    if-lt v6, p3, :cond_1

    goto/32 :goto_9

    :cond_1
    goto/32 :goto_36

    nop

    :goto_3
    move v2, v5

    goto/32 :goto_2b

    nop

    :goto_4
    return v0

    :goto_5
    goto/32 :goto_2d

    nop

    :goto_6
    if-lt v3, v1, :cond_2

    goto/32 :goto_11

    :cond_2
    goto/32 :goto_22

    nop

    :goto_7
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_28

    nop

    :goto_8
    goto/16 :goto_2c

    :goto_9
    goto/32 :goto_23

    nop

    :goto_a
    const/4 v0, 0x0

    goto/32 :goto_f

    nop

    :goto_b
    const/4 v3, 0x0

    :goto_c
    goto/32 :goto_6

    nop

    :goto_d
    iget-object v10, v7, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_15

    nop

    :goto_e
    check-cast v4, Landroid/app/BackStackRecord$Op;

    goto/32 :goto_12

    nop

    :goto_f
    if-eq p3, p2, :cond_3

    goto/32 :goto_5

    :cond_3
    goto/32 :goto_4

    nop

    :goto_10
    goto :goto_c

    :goto_11
    goto/32 :goto_21

    nop

    :goto_12
    iget-object v5, v4, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    goto/32 :goto_14

    nop

    :goto_13
    iget-object v8, v7, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_38

    nop

    :goto_14
    if-nez v5, :cond_4

    goto/32 :goto_27

    :cond_4
    goto/32 :goto_2e

    nop

    :goto_15
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    goto/32 :goto_29

    nop

    :goto_16
    const/4 v0, 0x1

    goto/32 :goto_17

    nop

    :goto_17
    return v0

    :goto_18
    goto/32 :goto_1c

    nop

    :goto_19
    if-nez v11, :cond_5

    goto/32 :goto_20

    :cond_5
    goto/32 :goto_37

    nop

    :goto_1a
    goto :goto_31

    :goto_1b
    goto/32 :goto_2f

    nop

    :goto_1c
    add-int/lit8 v9, v9, 0x1

    goto/32 :goto_1a

    nop

    :goto_1d
    move v11, v0

    :goto_1e
    goto/32 :goto_25

    nop

    :goto_1f
    goto :goto_1e

    :goto_20
    goto/32 :goto_1d

    nop

    :goto_21
    return v0

    :goto_22
    iget-object v4, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_32

    nop

    :goto_23
    add-int/lit8 v3, v3, 0x1

    goto/32 :goto_10

    nop

    :goto_24
    iget v5, v5, Landroid/app/Fragment;->mContainerId:I

    goto/32 :goto_26

    nop

    :goto_25
    if-eq v11, v5, :cond_6

    goto/32 :goto_18

    :cond_6
    goto/32 :goto_16

    nop

    :goto_26
    goto :goto_34

    :goto_27
    goto/32 :goto_33

    nop

    :goto_28
    const/4 v2, -0x1

    goto/32 :goto_b

    nop

    :goto_29
    check-cast v10, Landroid/app/BackStackRecord$Op;

    goto/32 :goto_39

    nop

    :goto_2a
    if-ne v5, v2, :cond_7

    goto/32 :goto_9

    :cond_7
    goto/32 :goto_3

    nop

    :goto_2b
    move v6, p2

    :goto_2c
    goto/32 :goto_2

    nop

    :goto_2d
    iget-object v1, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_7

    nop

    :goto_2e
    iget-object v5, v4, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    goto/32 :goto_24

    nop

    :goto_2f
    add-int/lit8 v6, v6, 0x1

    goto/32 :goto_8

    nop

    :goto_30
    const/4 v9, 0x0

    :goto_31
    goto/32 :goto_35

    nop

    :goto_32
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_e

    nop

    :goto_33
    move v5, v0

    :goto_34
    goto/32 :goto_1

    nop

    :goto_35
    if-lt v9, v8, :cond_8

    goto/32 :goto_1b

    :cond_8
    goto/32 :goto_d

    nop

    :goto_36
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    goto/32 :goto_3a

    nop

    :goto_37
    iget-object v11, v10, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    goto/32 :goto_0

    nop

    :goto_38
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    goto/32 :goto_30

    nop

    :goto_39
    iget-object v11, v10, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    goto/32 :goto_19

    nop

    :goto_3a
    check-cast v7, Landroid/app/BackStackRecord;

    goto/32 :goto_13

    nop
.end method

.method public isAddToBackStackAllowed()Z
    .locals 1

    iget-boolean v0, p0, Landroid/app/BackStackRecord;->mAllowAddToBackStack:Z

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    iget-object v0, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    return v0
.end method

.method isPostponed()Z
    .locals 3

    goto/32 :goto_b

    nop

    :goto_0
    iget-object v1, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_e

    nop

    :goto_1
    return v2

    :goto_2
    goto/32 :goto_11

    nop

    :goto_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_f

    nop

    :goto_4
    invoke-static {v1}, Landroid/app/BackStackRecord;->isFragmentPostponed(Landroid/app/BackStackRecord$Op;)Z

    move-result v2

    goto/32 :goto_10

    nop

    :goto_5
    const/4 v0, 0x0

    goto/32 :goto_8

    nop

    :goto_6
    goto :goto_c

    :goto_7
    goto/32 :goto_5

    nop

    :goto_8
    return v0

    :goto_9
    check-cast v1, Landroid/app/BackStackRecord$Op;

    goto/32 :goto_4

    nop

    :goto_a
    const/4 v2, 0x1

    goto/32 :goto_1

    nop

    :goto_b
    const/4 v0, 0x0

    :goto_c
    goto/32 :goto_d

    nop

    :goto_d
    iget-object v1, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_3

    nop

    :goto_e
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_9

    nop

    :goto_f
    if-lt v0, v1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_0

    nop

    :goto_10
    if-nez v2, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_a

    nop

    :goto_11
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_6

    nop
.end method

.method public remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    .locals 2

    new-instance v0, Landroid/app/BackStackRecord$Op;

    const/4 v1, 0x3

    invoke-direct {v0, v1, p1}, Landroid/app/BackStackRecord$Op;-><init>(ILandroid/app/Fragment;)V

    invoke-virtual {p0, v0}, Landroid/app/BackStackRecord;->addOp(Landroid/app/BackStackRecord$Op;)V

    return-object p0
.end method

.method public replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Landroid/app/BackStackRecord;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    return-object v0
.end method

.method public replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;
    .locals 2

    if-eqz p1, :cond_0

    const/4 v0, 0x2

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/app/BackStackRecord;->doAddOp(ILandroid/app/Fragment;Ljava/lang/String;I)V

    return-object p0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must use non-zero containerViewId"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public runOnCommit(Ljava/lang/Runnable;)Landroid/app/FragmentTransaction;
    .locals 2

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Landroid/app/BackStackRecord;->disallowAddToBackStack()Landroid/app/FragmentTransaction;

    iget-object v0, p0, Landroid/app/BackStackRecord;->mCommitRunnables:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/app/BackStackRecord;->mCommitRunnables:Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Landroid/app/BackStackRecord;->mCommitRunnables:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object p0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "runnable cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public runOnCommitRunnables()V
    .locals 3

    iget-object v0, p0, Landroid/app/BackStackRecord;->mCommitRunnables:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Landroid/app/BackStackRecord;->mCommitRunnables:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/app/BackStackRecord;->mCommitRunnables:Ljava/util/ArrayList;

    :cond_1
    return-void
.end method

.method public setBreadCrumbShortTitle(I)Landroid/app/FragmentTransaction;
    .locals 1

    iput p1, p0, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleRes:I

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleText:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setBreadCrumbShortTitle(Ljava/lang/CharSequence;)Landroid/app/FragmentTransaction;
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleRes:I

    iput-object p1, p0, Landroid/app/BackStackRecord;->mBreadCrumbShortTitleText:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setBreadCrumbTitle(I)Landroid/app/FragmentTransaction;
    .locals 1

    iput p1, p0, Landroid/app/BackStackRecord;->mBreadCrumbTitleRes:I

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbTitleText:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setBreadCrumbTitle(Ljava/lang/CharSequence;)Landroid/app/FragmentTransaction;
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Landroid/app/BackStackRecord;->mBreadCrumbTitleRes:I

    iput-object p1, p0, Landroid/app/BackStackRecord;->mBreadCrumbTitleText:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setCustomAnimations(II)Landroid/app/FragmentTransaction;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, v0}, Landroid/app/BackStackRecord;->setCustomAnimations(IIII)Landroid/app/FragmentTransaction;

    move-result-object v0

    return-object v0
.end method

.method public setCustomAnimations(IIII)Landroid/app/FragmentTransaction;
    .locals 0

    iput p1, p0, Landroid/app/BackStackRecord;->mEnterAnim:I

    iput p2, p0, Landroid/app/BackStackRecord;->mExitAnim:I

    iput p3, p0, Landroid/app/BackStackRecord;->mPopEnterAnim:I

    iput p4, p0, Landroid/app/BackStackRecord;->mPopExitAnim:I

    return-object p0
.end method

.method setOnStartPostponedListener(Landroid/app/Fragment$OnStartEnterTransitionListener;)V
    .locals 3

    goto/32 :goto_f

    nop

    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_e

    nop

    :goto_1
    if-nez v2, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_2

    nop

    :goto_2
    iget-object v2, v1, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    goto/32 :goto_b

    nop

    :goto_3
    iget-object v1, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_0

    nop

    :goto_4
    invoke-static {v1}, Landroid/app/BackStackRecord;->isFragmentPostponed(Landroid/app/BackStackRecord$Op;)Z

    move-result v2

    goto/32 :goto_1

    nop

    :goto_5
    check-cast v1, Landroid/app/BackStackRecord$Op;

    goto/32 :goto_4

    nop

    :goto_6
    return-void

    :goto_7
    goto :goto_10

    :goto_8
    goto/32 :goto_6

    nop

    :goto_9
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_5

    nop

    :goto_a
    iget-object v1, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_9

    nop

    :goto_b
    invoke-virtual {v2, p1}, Landroid/app/Fragment;->setOnStartEnterTransitionListener(Landroid/app/Fragment$OnStartEnterTransitionListener;)V

    :goto_c
    goto/32 :goto_d

    nop

    :goto_d
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_7

    nop

    :goto_e
    if-lt v0, v1, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_a

    nop

    :goto_f
    const/4 v0, 0x0

    :goto_10
    goto/32 :goto_3

    nop
.end method

.method public setPrimaryNavigationFragment(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    .locals 2

    new-instance v0, Landroid/app/BackStackRecord$Op;

    const/16 v1, 0x8

    invoke-direct {v0, v1, p1}, Landroid/app/BackStackRecord$Op;-><init>(ILandroid/app/Fragment;)V

    invoke-virtual {p0, v0}, Landroid/app/BackStackRecord;->addOp(Landroid/app/BackStackRecord$Op;)V

    return-object p0
.end method

.method public setReorderingAllowed(Z)Landroid/app/FragmentTransaction;
    .locals 0

    iput-boolean p1, p0, Landroid/app/BackStackRecord;->mReorderingAllowed:Z

    return-object p0
.end method

.method public setTransition(I)Landroid/app/FragmentTransaction;
    .locals 0

    iput p1, p0, Landroid/app/BackStackRecord;->mTransition:I

    return-object p0
.end method

.method public setTransitionStyle(I)Landroid/app/FragmentTransaction;
    .locals 0

    iput p1, p0, Landroid/app/BackStackRecord;->mTransitionStyle:I

    return-object p0
.end method

.method public show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    .locals 2

    new-instance v0, Landroid/app/BackStackRecord$Op;

    const/4 v1, 0x5

    invoke-direct {v0, v1, p1}, Landroid/app/BackStackRecord$Op;-><init>(ILandroid/app/Fragment;)V

    invoke-virtual {p0, v0}, Landroid/app/BackStackRecord;->addOp(Landroid/app/BackStackRecord$Op;)V

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "BackStackEntry{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Landroid/app/BackStackRecord;->mIndex:I

    if-ltz v1, :cond_0

    const-string v1, " #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Landroid/app/BackStackRecord;->mIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v1, p0, Landroid/app/BackStackRecord;->mName:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Landroid/app/BackStackRecord;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method trackAddedFragmentsInPop(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/app/Fragment;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_5

    nop

    :goto_0
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_7

    nop

    :goto_1
    if-lt v0, v1, :cond_0

    goto/32 :goto_10

    :cond_0
    goto/32 :goto_12

    nop

    :goto_2
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    nop

    :goto_3
    goto/32 :goto_8

    nop

    :goto_4
    goto :goto_3

    :pswitch_0
    goto/32 :goto_9

    nop

    :goto_5
    const/4 v0, 0x0

    :goto_6
    goto/32 :goto_b

    nop

    :goto_7
    check-cast v1, Landroid/app/BackStackRecord$Op;

    goto/32 :goto_11

    nop

    :goto_8
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_f

    nop

    :goto_9
    iget-object v2, v1, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    goto/32 :goto_d

    nop

    :goto_a
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :goto_b
    iget-object v1, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_c

    nop

    :goto_c
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_1

    nop

    :goto_d
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_13

    nop

    :goto_e
    iget-object v2, v1, Landroid/app/BackStackRecord$Op;->fragment:Landroid/app/Fragment;

    goto/32 :goto_2

    nop

    :goto_f
    goto :goto_6

    :goto_10
    goto/32 :goto_a

    nop

    :goto_11
    iget v2, v1, Landroid/app/BackStackRecord$Op;->cmd:I

    packed-switch v2, :pswitch_data_0

    :pswitch_1
    goto/32 :goto_4

    nop

    :goto_12
    iget-object v1, p0, Landroid/app/BackStackRecord;->mOps:Ljava/util/ArrayList;

    goto/32 :goto_0

    nop

    :goto_13
    goto :goto_3

    :pswitch_2
    goto/32 :goto_e

    nop
.end method
