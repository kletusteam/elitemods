.class public Landroid/app/VoiceInteractor$PickOptionRequest;
.super Landroid/app/VoiceInteractor$Request;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/VoiceInteractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PickOptionRequest"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/VoiceInteractor$PickOptionRequest$Option;
    }
.end annotation


# instance fields
.field final mExtras:Landroid/os/Bundle;

.field final mOptions:[Landroid/app/VoiceInteractor$PickOptionRequest$Option;

.field final mPrompt:Landroid/app/VoiceInteractor$Prompt;


# direct methods
.method public constructor <init>(Landroid/app/VoiceInteractor$Prompt;[Landroid/app/VoiceInteractor$PickOptionRequest$Option;Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0}, Landroid/app/VoiceInteractor$Request;-><init>()V

    iput-object p1, p0, Landroid/app/VoiceInteractor$PickOptionRequest;->mPrompt:Landroid/app/VoiceInteractor$Prompt;

    iput-object p2, p0, Landroid/app/VoiceInteractor$PickOptionRequest;->mOptions:[Landroid/app/VoiceInteractor$PickOptionRequest$Option;

    iput-object p3, p0, Landroid/app/VoiceInteractor$PickOptionRequest;->mExtras:Landroid/os/Bundle;

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;[Landroid/app/VoiceInteractor$PickOptionRequest$Option;Landroid/os/Bundle;)V
    .locals 1

    invoke-direct {p0}, Landroid/app/VoiceInteractor$Request;-><init>()V

    if-eqz p1, :cond_0

    new-instance v0, Landroid/app/VoiceInteractor$Prompt;

    invoke-direct {v0, p1}, Landroid/app/VoiceInteractor$Prompt;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Landroid/app/VoiceInteractor$PickOptionRequest;->mPrompt:Landroid/app/VoiceInteractor$Prompt;

    iput-object p2, p0, Landroid/app/VoiceInteractor$PickOptionRequest;->mOptions:[Landroid/app/VoiceInteractor$PickOptionRequest$Option;

    iput-object p3, p0, Landroid/app/VoiceInteractor$PickOptionRequest;->mExtras:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 4

    goto/32 :goto_b

    nop

    :goto_0
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_31

    nop

    :goto_1
    const-string v2, "    Synonyms:"

    goto/32 :goto_f

    nop

    :goto_2
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_2b

    nop

    :goto_3
    const-string v2, "    mLabel="

    goto/32 :goto_4c

    nop

    :goto_4
    const-string v0, "Options:"

    goto/32 :goto_4d

    nop

    :goto_5
    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_2e

    nop

    :goto_6
    goto/16 :goto_3d

    :goto_7
    goto/32 :goto_1b

    nop

    :goto_8
    iget-object v3, v1, Landroid/app/VoiceInteractor$PickOptionRequest$Option;->mSynonyms:Ljava/util/ArrayList;

    goto/32 :goto_37

    nop

    :goto_9
    const-string v3, "      #"

    goto/32 :goto_0

    nop

    :goto_a
    iget-object v2, v1, Landroid/app/VoiceInteractor$PickOptionRequest$Option;->mLabel:Ljava/lang/CharSequence;

    goto/32 :goto_3b

    nop

    :goto_b
    invoke-super {p0, p1, p2, p3, p4}, Landroid/app/VoiceInteractor$Request;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    goto/32 :goto_32

    nop

    :goto_c
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_11

    nop

    :goto_d
    if-nez v0, :cond_0

    goto/32 :goto_42

    :cond_0
    goto/32 :goto_3a

    nop

    :goto_e
    if-nez v0, :cond_1

    goto/32 :goto_1d

    :cond_1
    goto/32 :goto_1f

    nop

    :goto_f
    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/32 :goto_3c

    nop

    :goto_10
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    goto/32 :goto_38

    nop

    :goto_11
    iget-object v0, p0, Landroid/app/VoiceInteractor$PickOptionRequest;->mExtras:Landroid/os/Bundle;

    goto/32 :goto_41

    nop

    :goto_12
    iget-object v1, p0, Landroid/app/VoiceInteractor$PickOptionRequest;->mOptions:[Landroid/app/VoiceInteractor$PickOptionRequest$Option;

    goto/32 :goto_2c

    nop

    :goto_13
    const-string/jumbo v0, "mPrompt="

    goto/32 :goto_14

    nop

    :goto_14
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_49

    nop

    :goto_15
    if-lt v0, v2, :cond_2

    goto/32 :goto_1d

    :cond_2
    goto/32 :goto_20

    nop

    :goto_16
    iget-object v0, p0, Landroid/app/VoiceInteractor$PickOptionRequest;->mExtras:Landroid/os/Bundle;

    goto/32 :goto_d

    nop

    :goto_17
    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(I)V

    goto/32 :goto_29

    nop

    :goto_18
    iget-object v2, v1, Landroid/app/VoiceInteractor$PickOptionRequest$Option;->mExtras:Landroid/os/Bundle;

    goto/32 :goto_23

    nop

    :goto_19
    iget-object v2, v1, Landroid/app/VoiceInteractor$PickOptionRequest$Option;->mSynonyms:Ljava/util/ArrayList;

    goto/32 :goto_1e

    nop

    :goto_1a
    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_43

    nop

    :goto_1b
    iget-object v2, v1, Landroid/app/VoiceInteractor$PickOptionRequest$Option;->mExtras:Landroid/os/Bundle;

    goto/32 :goto_47

    nop

    :goto_1c
    goto/16 :goto_3f

    :goto_1d
    goto/32 :goto_16

    nop

    :goto_1e
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    goto/32 :goto_22

    nop

    :goto_1f
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_4

    nop

    :goto_20
    aget-object v1, v1, v0

    goto/32 :goto_2

    nop

    :goto_21
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_25

    nop

    :goto_22
    if-gtz v2, :cond_3

    goto/32 :goto_7

    :cond_3
    goto/32 :goto_27

    nop

    :goto_23
    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :goto_24
    goto/32 :goto_30

    nop

    :goto_25
    const-string v2, "    mIndex="

    goto/32 :goto_1a

    nop

    :goto_26
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_3

    nop

    :goto_27
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_1

    nop

    :goto_28
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_2f

    nop

    :goto_29
    iget-object v2, v1, Landroid/app/VoiceInteractor$PickOptionRequest$Option;->mSynonyms:Ljava/util/ArrayList;

    goto/32 :goto_2d

    nop

    :goto_2a
    const-string v2, ":"

    goto/32 :goto_40

    nop

    :goto_2b
    const-string v2, "  #"

    goto/32 :goto_5

    nop

    :goto_2c
    array-length v2, v1

    goto/32 :goto_15

    nop

    :goto_2d
    if-nez v2, :cond_4

    goto/32 :goto_7

    :cond_4
    goto/32 :goto_19

    nop

    :goto_2e
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    goto/32 :goto_2a

    nop

    :goto_2f
    iget-object v3, v1, Landroid/app/VoiceInteractor$PickOptionRequest$Option;->mSynonyms:Ljava/util/ArrayList;

    goto/32 :goto_44

    nop

    :goto_30
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_1c

    nop

    :goto_31
    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    goto/32 :goto_48

    nop

    :goto_32
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_13

    nop

    :goto_33
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_34

    nop

    :goto_34
    const-string v2, "    mExtras="

    goto/32 :goto_35

    nop

    :goto_35
    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_18

    nop

    :goto_36
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_9

    nop

    :goto_37
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    goto/32 :goto_4a

    nop

    :goto_38
    iget-object v0, p0, Landroid/app/VoiceInteractor$PickOptionRequest;->mOptions:[Landroid/app/VoiceInteractor$PickOptionRequest$Option;

    goto/32 :goto_e

    nop

    :goto_39
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_6

    nop

    :goto_3a
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_45

    nop

    :goto_3b
    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    goto/32 :goto_21

    nop

    :goto_3c
    const/4 v2, 0x0

    :goto_3d
    goto/32 :goto_8

    nop

    :goto_3e
    const/4 v0, 0x0

    :goto_3f
    goto/32 :goto_12

    nop

    :goto_40
    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/32 :goto_26

    nop

    :goto_41
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :goto_42
    goto/32 :goto_4b

    nop

    :goto_43
    iget v2, v1, Landroid/app/VoiceInteractor$PickOptionRequest$Option;->mIndex:I

    goto/32 :goto_17

    nop

    :goto_44
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_46

    nop

    :goto_45
    const-string/jumbo v0, "mExtras="

    goto/32 :goto_c

    nop

    :goto_46
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    goto/32 :goto_39

    nop

    :goto_47
    if-nez v2, :cond_5

    goto/32 :goto_24

    :cond_5
    goto/32 :goto_33

    nop

    :goto_48
    const-string v3, ": "

    goto/32 :goto_28

    nop

    :goto_49
    iget-object v0, p0, Landroid/app/VoiceInteractor$PickOptionRequest;->mPrompt:Landroid/app/VoiceInteractor$Prompt;

    goto/32 :goto_10

    nop

    :goto_4a
    if-lt v2, v3, :cond_6

    goto/32 :goto_7

    :cond_6
    goto/32 :goto_36

    nop

    :goto_4b
    return-void

    :goto_4c
    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_a

    nop

    :goto_4d
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/32 :goto_3e

    nop
.end method

.method getRequestTypeName()Ljava/lang/String;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    const-string v0, "PickOption"

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method public onPickOptionResult(Z[Landroid/app/VoiceInteractor$PickOptionRequest$Option;Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method submit(Lcom/android/internal/app/IVoiceInteractor;Ljava/lang/String;Lcom/android/internal/app/IVoiceInteractorCallback;)Lcom/android/internal/app/IVoiceInteractorRequest;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    goto/32 :goto_3

    nop

    :goto_0
    move-object v1, p2

    goto/32 :goto_6

    nop

    :goto_1
    move-object v0, p1

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v5, p0, Landroid/app/VoiceInteractor$PickOptionRequest;->mExtras:Landroid/os/Bundle;

    goto/32 :goto_1

    nop

    :goto_3
    iget-object v3, p0, Landroid/app/VoiceInteractor$PickOptionRequest;->mPrompt:Landroid/app/VoiceInteractor$Prompt;

    goto/32 :goto_7

    nop

    :goto_4
    return-object v0

    :goto_5
    invoke-interface/range {v0 .. v5}, Lcom/android/internal/app/IVoiceInteractor;->startPickOption(Ljava/lang/String;Lcom/android/internal/app/IVoiceInteractorCallback;Landroid/app/VoiceInteractor$Prompt;[Landroid/app/VoiceInteractor$PickOptionRequest$Option;Landroid/os/Bundle;)Lcom/android/internal/app/IVoiceInteractorRequest;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_6
    move-object v2, p3

    goto/32 :goto_5

    nop

    :goto_7
    iget-object v4, p0, Landroid/app/VoiceInteractor$PickOptionRequest;->mOptions:[Landroid/app/VoiceInteractor$PickOptionRequest$Option;

    goto/32 :goto_2

    nop
.end method
