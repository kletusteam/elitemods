.class public interface abstract Landroid/app/ExitTransitionCoordinator$ExitTransitionCallbacks;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/ExitTransitionCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ExitTransitionCallbacks"
.end annotation


# virtual methods
.method public hideSharedElements()V
    .locals 0

    return-void
.end method

.method public abstract isReturnTransitionAllowed()Z
.end method

.method public abstract onFinish()V
.end method
