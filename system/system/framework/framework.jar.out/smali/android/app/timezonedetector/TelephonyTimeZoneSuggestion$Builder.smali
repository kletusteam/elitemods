.class public final Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion$Builder;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private mDebugInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mMatchType:I

.field private mQuality:I

.field private final mSlotIndex:I

.field private mZoneId:Ljava/lang/String;


# direct methods
.method static bridge synthetic -$$Nest$fgetmDebugInfo(Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion$Builder;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion$Builder;->mDebugInfo:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMatchType(Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion$Builder;)I
    .locals 0

    iget p0, p0, Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion$Builder;->mMatchType:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmQuality(Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion$Builder;)I
    .locals 0

    iget p0, p0, Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion$Builder;->mQuality:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSlotIndex(Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion$Builder;)I
    .locals 0

    iget p0, p0, Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion$Builder;->mSlotIndex:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmZoneId(Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion$Builder;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion$Builder;->mZoneId:Ljava/lang/String;

    return-object p0
.end method

.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion$Builder;->mSlotIndex:I

    return-void
.end method


# virtual methods
.method public addDebugInfo(Ljava/lang/String;)Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion$Builder;
    .locals 1

    iget-object v0, p0, Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion$Builder;->mDebugInfo:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion$Builder;->mDebugInfo:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion$Builder;->mDebugInfo:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public build()Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion;
    .locals 2

    invoke-virtual {p0}, Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion$Builder;->validate()V

    new-instance v0, Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion;-><init>(Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion$Builder;Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion-IA;)V

    return-object v0
.end method

.method public setMatchType(I)Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion$Builder;
    .locals 0

    iput p1, p0, Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion$Builder;->mMatchType:I

    return-object p0
.end method

.method public setQuality(I)Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion$Builder;
    .locals 0

    iput p1, p0, Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion$Builder;->mQuality:I

    return-object p0
.end method

.method public setZoneId(Ljava/lang/String;)Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion$Builder;
    .locals 0

    iput-object p1, p0, Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion$Builder;->mZoneId:Ljava/lang/String;

    return-object p0
.end method

.method validate()V
    .locals 8

    goto/32 :goto_1f

    nop

    :goto_0
    if-eqz v1, :cond_0

    goto/32 :goto_19

    :cond_0
    goto/32 :goto_18

    nop

    :goto_1
    goto :goto_11

    :goto_2
    goto/32 :goto_32

    nop

    :goto_3
    if-eq v1, v4, :cond_1

    goto/32 :goto_23

    :cond_1
    :goto_4
    goto/32 :goto_22

    nop

    :goto_5
    if-ne v1, v4, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_1c

    nop

    :goto_6
    if-ne v1, v4, :cond_3

    goto/32 :goto_4

    :cond_3
    goto/32 :goto_2e

    nop

    :goto_7
    move v7, v6

    :goto_8
    goto/32 :goto_35

    nop

    :goto_9
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_2c

    nop

    :goto_a
    const/4 v2, 0x0

    goto/32 :goto_2f

    nop

    :goto_b
    if-nez v7, :cond_4

    goto/32 :goto_27

    :cond_4
    goto/32 :goto_24

    nop

    :goto_c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_29

    nop

    :goto_d
    if-eqz v2, :cond_5

    goto/32 :goto_2d

    :cond_5
    goto/32 :goto_1e

    nop

    :goto_e
    new-instance v4, Ljava/lang/RuntimeException;

    goto/32 :goto_16

    nop

    :goto_f
    if-ne v0, v6, :cond_6

    goto/32 :goto_11

    :cond_6
    goto/32 :goto_17

    nop

    :goto_10
    goto :goto_8

    :goto_11
    goto/32 :goto_7

    nop

    :goto_12
    if-eq v0, v4, :cond_7

    goto/32 :goto_2

    :cond_7
    goto/32 :goto_1

    nop

    :goto_13
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_30

    nop

    :goto_14
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_13

    nop

    :goto_15
    const-string v5, "Invalid quality or match type for null zone ID. quality="

    goto/32 :goto_c

    nop

    :goto_16
    new-instance v5, Ljava/lang/StringBuilder;

    goto/32 :goto_1a

    nop

    :goto_17
    if-ne v0, v5, :cond_8

    goto/32 :goto_11

    :cond_8
    goto/32 :goto_12

    nop

    :goto_18
    goto :goto_25

    :goto_19
    goto/32 :goto_2a

    nop

    :goto_1a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1b

    nop

    :goto_1b
    const-string v6, "Invalid quality or match type with zone ID. quality="

    goto/32 :goto_39

    nop

    :goto_1c
    const/4 v4, 0x5

    goto/32 :goto_3

    nop

    :goto_1d
    iget v1, p0, Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion$Builder;->mMatchType:I

    goto/32 :goto_31

    nop

    :goto_1e
    if-eqz v0, :cond_9

    goto/32 :goto_19

    :cond_9
    goto/32 :goto_0

    nop

    :goto_1f
    iget v0, p0, Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion$Builder;->mQuality:I

    goto/32 :goto_1d

    nop

    :goto_20
    invoke-direct {v4, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_33

    nop

    :goto_21
    const/4 v5, 0x2

    goto/32 :goto_2b

    nop

    :goto_22
    move v2, v6

    :goto_23
    goto/32 :goto_b

    nop

    :goto_24
    if-nez v2, :cond_a

    goto/32 :goto_27

    :cond_a
    :goto_25
    goto/32 :goto_26

    nop

    :goto_26
    return-void

    :goto_27
    goto/32 :goto_e

    nop

    :goto_28
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_3a

    nop

    :goto_29
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_14

    nop

    :goto_2a
    new-instance v2, Ljava/lang/RuntimeException;

    goto/32 :goto_28

    nop

    :goto_2b
    const/4 v6, 0x1

    goto/32 :goto_f

    nop

    :goto_2c
    throw v2

    :goto_2d
    goto/32 :goto_a

    nop

    :goto_2e
    const/4 v4, 0x4

    goto/32 :goto_5

    nop

    :goto_2f
    const/4 v4, 0x3

    goto/32 :goto_21

    nop

    :goto_30
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_9

    nop

    :goto_31
    iget-object v2, p0, Landroid/app/timezonedetector/TelephonyTimeZoneSuggestion$Builder;->mZoneId:Ljava/lang/String;

    goto/32 :goto_38

    nop

    :goto_32
    move v7, v2

    goto/32 :goto_10

    nop

    :goto_33
    throw v4

    :goto_34
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_20

    nop

    :goto_35
    if-ne v1, v5, :cond_b

    goto/32 :goto_4

    :cond_b
    goto/32 :goto_6

    nop

    :goto_36
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_34

    nop

    :goto_37
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/32 :goto_3b

    nop

    :goto_38
    const-string v3, ", matchType="

    goto/32 :goto_d

    nop

    :goto_39
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/32 :goto_37

    nop

    :goto_3a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_15

    nop

    :goto_3b
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_36

    nop
.end method
