.class public interface abstract annotation Landroid/app/ProcessStateEnum;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# static fields
.field public static final BACKUP:I = 0x9

.field public static final BOUND_FOREGROUND_SERVICE:I = 0x5

.field public static final BOUND_TOP:I = 0x3

.field public static final CACHED_ACTIVITY:I = 0x10

.field public static final CACHED_ACTIVITY_CLIENT:I = 0x11

.field public static final CACHED_EMPTY:I = 0x13

.field public static final CACHED_RECENT:I = 0x12

.field public static final FOREGROUND_SERVICE:I = 0x4

.field public static final HEAVY_WEIGHT:I = 0xd

.field public static final HOME:I = 0xe

.field public static final IMPORTANT_BACKGROUND:I = 0x7

.field public static final IMPORTANT_FOREGROUND:I = 0x6

.field public static final LAST_ACTIVITY:I = 0xf

.field public static final NONEXISTENT:I = 0x14

.field public static final PERSISTENT:I = 0x0

.field public static final PERSISTENT_UI:I = 0x1

.field public static final RECEIVER:I = 0xb

.field public static final SERVICE:I = 0xa

.field public static final TOP:I = 0x2

.field public static final TOP_SLEEPING:I = 0xc

.field public static final TRANSIENT_BACKGROUND:I = 0x8

.field public static final UNKNOWN:I = -0x1
