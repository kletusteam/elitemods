.class public interface abstract Landroid/app/ActivityManagerInternal$AppBackgroundRestrictionListener;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/ActivityManagerInternal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AppBackgroundRestrictionListener"
.end annotation


# virtual methods
.method public onAutoRestrictedBucketFeatureFlagChanged(Z)V
    .locals 0

    return-void
.end method

.method public onRestrictionLevelChanged(ILjava/lang/String;I)V
    .locals 0

    return-void
.end method
