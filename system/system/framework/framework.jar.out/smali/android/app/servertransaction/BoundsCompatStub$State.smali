.class public interface abstract annotation Landroid/app/servertransaction/BoundsCompatStub$State;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/servertransaction/BoundsCompatStub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2609
    name = "State"
.end annotation

.annotation runtime Ljava/lang/annotation/Retention;
    value = .enum Ljava/lang/annotation/RetentionPolicy;->SOURCE:Ljava/lang/annotation/RetentionPolicy;
.end annotation


# static fields
.field public static final ALL_DISABLED:I = 0x0

.field public static final DISPLAY_COMPAT_MODE_ENABLED:I = 0x4

.field public static final FIXED_ASPECT_RATIO_MODE_ENABLED:I = 0x8

.field public static final MAX_ASPECT_RATIO_APPLIED:I = 0x1

.field public static final MIN_ASPECT_RATIO_APPLIED:I = 0x2
