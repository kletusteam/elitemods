.class Landroid/app/ConfigurationController;
.super Ljava/lang/Object;


# static fields
.field private static final TAG:Ljava/lang/String; = "ConfigurationController"


# instance fields
.field private final mActivityThread:Landroid/app/ActivityThreadInternal;

.field private mCompatConfiguration:Landroid/content/res/Configuration;

.field private mConfiguration:Landroid/content/res/Configuration;

.field private mPendingConfiguration:Landroid/content/res/Configuration;

.field private final mResourcesManager:Landroid/app/ResourcesManager;


# direct methods
.method constructor <init>(Landroid/app/ActivityThreadInternal;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Landroid/app/ResourcesManager;->getInstance()Landroid/app/ResourcesManager;

    move-result-object v0

    iput-object v0, p0, Landroid/app/ConfigurationController;->mResourcesManager:Landroid/app/ResourcesManager;

    iput-object p1, p0, Landroid/app/ConfigurationController;->mActivityThread:Landroid/app/ActivityThreadInternal;

    return-void
.end method

.method static createNewConfigAndUpdateIfNotNull(Landroid/content/res/Configuration;Landroid/content/res/Configuration;)Landroid/content/res/Configuration;
    .locals 1

    if-nez p1, :cond_0

    return-object p0

    :cond_0
    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0, p0}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    invoke-virtual {v0, p1}, Landroid/content/res/Configuration;->updateFrom(Landroid/content/res/Configuration;)I

    return-object v0
.end method


# virtual methods
.method final applyCompatConfiguration()Landroid/content/res/Configuration;
    .locals 4

    goto/32 :goto_7

    nop

    :goto_0
    invoke-virtual {v2, v1, v3}, Landroid/app/ResourcesManager;->applyCompatConfiguration(ILandroid/content/res/Configuration;)Z

    move-result v2

    goto/32 :goto_1

    nop

    :goto_1
    if-nez v2, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_3

    nop

    :goto_2
    iget-object v2, p0, Landroid/app/ConfigurationController;->mResourcesManager:Landroid/app/ResourcesManager;

    goto/32 :goto_6

    nop

    :goto_3
    iget-object v0, p0, Landroid/app/ConfigurationController;->mCompatConfiguration:Landroid/content/res/Configuration;

    :goto_4
    goto/32 :goto_d

    nop

    :goto_5
    if-eqz v2, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_8

    nop

    :goto_6
    iget-object v3, p0, Landroid/app/ConfigurationController;->mCompatConfiguration:Landroid/content/res/Configuration;

    goto/32 :goto_0

    nop

    :goto_7
    iget-object v0, p0, Landroid/app/ConfigurationController;->mConfiguration:Landroid/content/res/Configuration;

    goto/32 :goto_10

    nop

    :goto_8
    new-instance v2, Landroid/content/res/Configuration;

    goto/32 :goto_9

    nop

    :goto_9
    invoke-direct {v2}, Landroid/content/res/Configuration;-><init>()V

    goto/32 :goto_a

    nop

    :goto_a
    iput-object v2, p0, Landroid/app/ConfigurationController;->mCompatConfiguration:Landroid/content/res/Configuration;

    :goto_b
    goto/32 :goto_f

    nop

    :goto_c
    iget-object v2, p0, Landroid/app/ConfigurationController;->mCompatConfiguration:Landroid/content/res/Configuration;

    goto/32 :goto_5

    nop

    :goto_d
    return-object v0

    :goto_e
    iget-object v3, p0, Landroid/app/ConfigurationController;->mConfiguration:Landroid/content/res/Configuration;

    goto/32 :goto_11

    nop

    :goto_f
    iget-object v2, p0, Landroid/app/ConfigurationController;->mCompatConfiguration:Landroid/content/res/Configuration;

    goto/32 :goto_e

    nop

    :goto_10
    iget v1, v0, Landroid/content/res/Configuration;->densityDpi:I

    goto/32 :goto_c

    nop

    :goto_11
    invoke-virtual {v2, v3}, Landroid/content/res/Configuration;->setTo(Landroid/content/res/Configuration;)V

    goto/32 :goto_2

    nop
.end method

.method getCompatConfiguration()Landroid/content/res/Configuration;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Landroid/app/ConfigurationController;->mCompatConfiguration:Landroid/content/res/Configuration;

    goto/32 :goto_0

    nop
.end method

.method getConfiguration()Landroid/content/res/Configuration;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/app/ConfigurationController;->mConfiguration:Landroid/content/res/Configuration;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method getCurDefaultDisplayDpi()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/app/ConfigurationController;->mConfiguration:Landroid/content/res/Configuration;

    goto/32 :goto_1

    nop

    :goto_1
    iget v0, v0, Landroid/content/res/Configuration;->densityDpi:I

    goto/32 :goto_2

    nop

    :goto_2
    return v0
.end method

.method getPendingConfiguration(Z)Landroid/content/res/Configuration;
    .locals 3

    goto/32 :goto_2

    nop

    :goto_0
    throw v2

    :goto_1
    iget-object v1, p0, Landroid/app/ConfigurationController;->mResourcesManager:Landroid/app/ResourcesManager;

    goto/32 :goto_3

    nop

    :goto_2
    const/4 v0, 0x0

    goto/32 :goto_1

    nop

    :goto_3
    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Landroid/app/ConfigurationController;->mPendingConfiguration:Landroid/content/res/Configuration;

    if-eqz v2, :cond_0

    move-object v0, v2

    if-eqz p1, :cond_0

    const/4 v2, 0x0

    iput-object v2, p0, Landroid/app/ConfigurationController;->mPendingConfiguration:Landroid/content/res/Configuration;

    :cond_0
    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_0

    nop
.end method

.method handleConfigurationChanged(Landroid/content/res/CompatibilityInfo;)V
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    invoke-virtual {p0, v0, p1}, Landroid/app/ConfigurationController;->handleConfigurationChanged(Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)V

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    invoke-static {}, Landroid/view/WindowManagerGlobal;->getInstance()Landroid/view/WindowManagerGlobal;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_3
    iget-object v1, p0, Landroid/app/ConfigurationController;->mConfiguration:Landroid/content/res/Configuration;

    goto/32 :goto_4

    nop

    :goto_4
    invoke-virtual {v0, v1}, Landroid/view/WindowManagerGlobal;->reportNewConfiguration(Landroid/content/res/Configuration;)V

    goto/32 :goto_1

    nop

    :goto_5
    iget-object v0, p0, Landroid/app/ConfigurationController;->mConfiguration:Landroid/content/res/Configuration;

    goto/32 :goto_0

    nop
.end method

.method handleConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    goto/32 :goto_4

    nop

    :goto_0
    invoke-interface {v0}, Landroid/app/ActivityThreadInternal;->isCachedProcessState()Z

    move-result v0

    goto/32 :goto_8

    nop

    :goto_1
    invoke-virtual {p0, p1, v2}, Landroid/app/ConfigurationController;->handleConfigurationChanged(Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)V

    goto/32 :goto_b

    nop

    :goto_2
    invoke-virtual {p0, p1}, Landroid/app/ConfigurationController;->updatePendingConfiguration(Landroid/content/res/Configuration;)Landroid/content/res/Configuration;

    goto/32 :goto_9

    nop

    :goto_3
    invoke-static {v0, v1, v2}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    goto/32 :goto_7

    nop

    :goto_4
    iget-object v0, p0, Landroid/app/ConfigurationController;->mActivityThread:Landroid/app/ActivityThreadInternal;

    goto/32 :goto_0

    nop

    :goto_5
    const-string v2, "configChanged"

    goto/32 :goto_3

    nop

    :goto_6
    return-void

    :goto_7
    const/4 v2, 0x0

    goto/32 :goto_1

    nop

    :goto_8
    if-nez v0, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_2

    nop

    :goto_9
    return-void

    :goto_a
    goto/32 :goto_c

    nop

    :goto_b
    invoke-static {v0, v1}, Landroid/os/Trace;->traceEnd(J)V

    goto/32 :goto_6

    nop

    :goto_c
    const-wide/16 v0, 0x40

    goto/32 :goto_5

    nop
.end method

.method handleConfigurationChanged(Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)V
    .locals 10

    goto/32 :goto_4

    nop

    :goto_0
    const/4 v2, 0x0

    goto/32 :goto_d

    nop

    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto/32 :goto_16

    nop

    :goto_2
    invoke-interface {v0}, Landroid/app/ActivityThreadInternal;->getSystemContext()Landroid/app/ContextImpl;

    move-result-object v0

    goto/32 :goto_1a

    nop

    :goto_3
    if-lt v6, v5, :cond_0

    goto/32 :goto_17

    :cond_0
    goto/32 :goto_e

    nop

    :goto_4
    iget-object v0, p0, Landroid/app/ConfigurationController;->mActivityThread:Landroid/app/ActivityThreadInternal;

    goto/32 :goto_2

    nop

    :goto_5
    invoke-static {v8}, Landroid/window/ConfigurationHelper;->freeTextLayoutCachesIfNeeded(I)V

    goto/32 :goto_1b

    nop

    :goto_6
    check-cast v7, Landroid/content/ComponentCallbacks2;

    goto/32 :goto_b

    nop

    :goto_7
    invoke-interface {v4, v5}, Landroid/app/ActivityThreadInternal;->collectComponentCallbacks(Z)Ljava/util/ArrayList;

    move-result-object v4

    goto/32 :goto_5

    nop

    :goto_8
    const/4 v6, 0x0

    :goto_9
    goto/32 :goto_3

    nop

    :goto_a
    throw v2

    :goto_b
    if-eqz v2, :cond_1

    goto/32 :goto_1f

    :cond_1
    goto/32 :goto_1e

    nop

    :goto_c
    iget-object v1, p0, Landroid/app/ConfigurationController;->mActivityThread:Landroid/app/ActivityThreadInternal;

    goto/32 :goto_20

    nop

    :goto_d
    if-nez v1, :cond_2

    goto/32 :goto_11

    :cond_2
    goto/32 :goto_1c

    nop

    :goto_e
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    goto/32 :goto_6

    nop

    :goto_f
    iget-object v4, p0, Landroid/app/ConfigurationController;->mActivityThread:Landroid/app/ActivityThreadInternal;

    goto/32 :goto_7

    nop

    :goto_10
    goto :goto_13

    :goto_11
    goto/32 :goto_12

    nop

    :goto_12
    move-object v3, v2

    :goto_13
    goto/32 :goto_14

    nop

    :goto_14
    iget-object v4, p0, Landroid/app/ConfigurationController;->mResourcesManager:Landroid/app/ResourcesManager;

    goto/32 :goto_18

    nop

    :goto_15
    return-void

    :catchall_0
    move-exception v2

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_a

    nop

    :goto_16
    goto :goto_9

    :goto_17
    goto/32 :goto_15

    nop

    :goto_18
    monitor-enter v4

    :try_start_1
    iget-object v5, p0, Landroid/app/ConfigurationController;->mPendingConfiguration:Landroid/content/res/Configuration;

    if-eqz v5, :cond_4

    invoke-virtual {v5, p1}, Landroid/content/res/Configuration;->isOtherSeqNewer(Landroid/content/res/Configuration;)Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, p0, Landroid/app/ConfigurationController;->mPendingConfiguration:Landroid/content/res/Configuration;

    move-object p1, v5

    iget v5, p1, Landroid/content/res/Configuration;->densityDpi:I

    invoke-virtual {p0, v5}, Landroid/app/ConfigurationController;->updateDefaultDensity(I)V

    :cond_3
    iput-object v2, p0, Landroid/app/ConfigurationController;->mPendingConfiguration:Landroid/content/res/Configuration;

    :cond_4
    if-nez p1, :cond_5

    monitor-exit v4

    return-void

    :cond_5
    iget-object v2, p0, Landroid/app/ConfigurationController;->mConfiguration:Landroid/content/res/Configuration;

    const/4 v5, 0x0

    if-eqz v2, :cond_6

    invoke-virtual {v2, p1}, Landroid/content/res/Configuration;->diffPublicOnly(Landroid/content/res/Configuration;)I

    move-result v2

    if-nez v2, :cond_6

    const/4 v2, 0x1

    goto :goto_19

    :cond_6
    move v2, v5

    :goto_19
    sget-boolean v6, Landroid/app/ActivityThread;->DEBUG_CONFIGURATION:Z

    if-eqz v6, :cond_7

    const-string v6, "ConfigurationController"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Handle configuration changed: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    invoke-static {}, Landroid/view/ForceDarkHelperStub;->getInstance()Landroid/view/ForceDarkHelperStub;

    move-result-object v6

    invoke-interface {v6, p1}, Landroid/view/ForceDarkHelperStub;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v6, p0, Landroid/app/ConfigurationController;->mActivityThread:Landroid/app/ActivityThreadInternal;

    invoke-interface {v6}, Landroid/app/ActivityThreadInternal;->getApplication()Landroid/app/Application;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    iget-object v8, p0, Landroid/app/ConfigurationController;->mResourcesManager:Landroid/app/ResourcesManager;

    invoke-virtual {v8, p1, p2}, Landroid/app/ResourcesManager;->applyConfigurationToResources(Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)Z

    invoke-virtual {v6}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {p0, v8}, Landroid/app/ConfigurationController;->updateLocaleListFromAppContext(Landroid/content/Context;)V

    iget-object v8, p0, Landroid/app/ConfigurationController;->mConfiguration:Landroid/content/res/Configuration;

    if-nez v8, :cond_8

    new-instance v8, Landroid/content/res/Configuration;

    invoke-direct {v8}, Landroid/content/res/Configuration;-><init>()V

    iput-object v8, p0, Landroid/app/ConfigurationController;->mConfiguration:Landroid/content/res/Configuration;

    :cond_8
    iget-object v8, p0, Landroid/app/ConfigurationController;->mConfiguration:Landroid/content/res/Configuration;

    invoke-virtual {v8, p1}, Landroid/content/res/Configuration;->isOtherSeqNewer(Landroid/content/res/Configuration;)Z

    move-result v8

    if-nez v8, :cond_9

    if-nez p2, :cond_9

    monitor-exit v4

    return-void

    :cond_9
    iget-object v8, p0, Landroid/app/ConfigurationController;->mConfiguration:Landroid/content/res/Configuration;

    invoke-virtual {v8, p1}, Landroid/content/res/Configuration;->updateFrom(Landroid/content/res/Configuration;)I

    move-result v8

    invoke-virtual {p0}, Landroid/app/ConfigurationController;->applyCompatConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    move-object p1, v9

    invoke-static {p1}, Landroid/graphics/HardwareRenderer;->sendDeviceConfigurationForDebugging(Landroid/content/res/Configuration;)V

    invoke-virtual {v0}, Landroid/content/res/Resources$Theme;->getChangingConfigurations()I

    move-result v9

    and-int/2addr v9, v8

    if-eqz v9, :cond_a

    invoke-virtual {v0}, Landroid/content/res/Resources$Theme;->rebase()V

    :cond_a
    if-eqz v3, :cond_b

    invoke-virtual {v3}, Landroid/content/res/Resources$Theme;->getChangingConfigurations()I

    move-result v9

    and-int/2addr v9, v8

    if-eqz v9, :cond_b

    invoke-virtual {v3}, Landroid/content/res/Resources$Theme;->rebase()V

    :cond_b
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/32 :goto_f

    nop

    :goto_1a
    invoke-virtual {v0}, Landroid/app/ContextImpl;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    goto/32 :goto_c

    nop

    :goto_1b
    if-nez v4, :cond_c

    goto/32 :goto_17

    :cond_c
    goto/32 :goto_1d

    nop

    :goto_1c
    invoke-virtual {v1}, Landroid/app/ContextImpl;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    goto/32 :goto_10

    nop

    :goto_1d
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    goto/32 :goto_8

    nop

    :goto_1e
    invoke-virtual {p0, v7, p1}, Landroid/app/ConfigurationController;->performConfigurationChanged(Landroid/content/ComponentCallbacks2;Landroid/content/res/Configuration;)V

    :goto_1f
    goto/32 :goto_1

    nop

    :goto_20
    invoke-interface {v1}, Landroid/app/ActivityThreadInternal;->getSystemUiContextNoCreate()Landroid/app/ContextImpl;

    move-result-object v1

    goto/32 :goto_0

    nop
.end method

.method performConfigurationChanged(Landroid/content/ComponentCallbacks2;Landroid/content/res/Configuration;)V
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    invoke-virtual {v1}, Landroid/view/ContextThemeWrapper;->getOverrideConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    :goto_1
    goto/32 :goto_6

    nop

    :goto_2
    if-nez v1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_3

    nop

    :goto_3
    move-object v1, p1

    goto/32 :goto_9

    nop

    :goto_4
    invoke-interface {p1, v1}, Landroid/content/ComponentCallbacks2;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    goto/32 :goto_8

    nop

    :goto_5
    const/4 v0, 0x0

    goto/32 :goto_7

    nop

    :goto_6
    invoke-static {p2, v0}, Landroid/app/ConfigurationController;->createNewConfigAndUpdateIfNotNull(Landroid/content/res/Configuration;Landroid/content/res/Configuration;)Landroid/content/res/Configuration;

    move-result-object v1

    goto/32 :goto_4

    nop

    :goto_7
    instance-of v1, p1, Landroid/view/ContextThemeWrapper;

    goto/32 :goto_2

    nop

    :goto_8
    return-void

    :goto_9
    check-cast v1, Landroid/view/ContextThemeWrapper;

    goto/32 :goto_0

    nop
.end method

.method setCompatConfiguration(Landroid/content/res/Configuration;)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-direct {v0, p1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    goto/32 :goto_3

    nop

    :goto_1
    return-void

    :goto_2
    new-instance v0, Landroid/content/res/Configuration;

    goto/32 :goto_0

    nop

    :goto_3
    iput-object v0, p0, Landroid/app/ConfigurationController;->mCompatConfiguration:Landroid/content/res/Configuration;

    goto/32 :goto_1

    nop
.end method

.method setConfiguration(Landroid/content/res/Configuration;)V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    iput-object v0, p0, Landroid/app/ConfigurationController;->mConfiguration:Landroid/content/res/Configuration;

    goto/32 :goto_2

    nop

    :goto_1
    invoke-direct {v0, p1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    goto/32 :goto_0

    nop

    :goto_2
    return-void

    :goto_3
    new-instance v0, Landroid/content/res/Configuration;

    goto/32 :goto_1

    nop
.end method

.method updateDefaultDensity(I)V
    .locals 1

    goto/32 :goto_9

    nop

    :goto_0
    invoke-interface {v0}, Landroid/app/ActivityThreadInternal;->isInDensityCompatMode()Z

    move-result v0

    goto/32 :goto_2

    nop

    :goto_1
    sput p1, Landroid/util/DisplayMetrics;->DENSITY_DEVICE:I

    goto/32 :goto_7

    nop

    :goto_2
    if-eqz v0, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_4

    nop

    :goto_3
    sget v0, Landroid/util/DisplayMetrics;->DENSITY_DEVICE:I

    goto/32 :goto_6

    nop

    :goto_4
    if-nez p1, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_3

    nop

    :goto_5
    return-void

    :goto_6
    if-ne p1, v0, :cond_2

    goto/32 :goto_8

    :cond_2
    goto/32 :goto_1

    nop

    :goto_7
    invoke-static {p1}, Landroid/graphics/Bitmap;->setDefaultDensity(I)V

    :goto_8
    goto/32 :goto_5

    nop

    :goto_9
    iget-object v0, p0, Landroid/app/ConfigurationController;->mActivityThread:Landroid/app/ActivityThreadInternal;

    goto/32 :goto_0

    nop
.end method

.method updateLocaleListFromAppContext(Landroid/content/Context;)V
    .locals 5

    goto/32 :goto_16

    nop

    :goto_0
    invoke-virtual {v1, v3}, Landroid/os/LocaleList;->get(I)Ljava/util/Locale;

    move-result-object v4

    goto/32 :goto_c

    nop

    :goto_1
    if-lt v3, v2, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_0

    nop

    :goto_2
    const/4 v3, 0x0

    :goto_3
    goto/32 :goto_1

    nop

    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto/32 :goto_d

    nop

    :goto_5
    invoke-static {v3}, Landroid/os/LocaleList;->setDefault(Landroid/os/LocaleList;)V

    goto/32 :goto_14

    nop

    :goto_6
    return-void

    :goto_7
    goto/32 :goto_4

    nop

    :goto_8
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_9
    invoke-virtual {v0}, Landroid/content/res/Configuration;->getLocales()Landroid/os/LocaleList;

    move-result-object v0

    goto/32 :goto_13

    nop

    :goto_a
    invoke-virtual {v0, v1}, Landroid/os/LocaleList;->get(I)Ljava/util/Locale;

    move-result-object v0

    goto/32 :goto_15

    nop

    :goto_b
    if-nez v4, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_11

    nop

    :goto_c
    invoke-virtual {v0, v4}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v4

    goto/32 :goto_b

    nop

    :goto_d
    goto :goto_3

    :goto_e
    goto/32 :goto_10

    nop

    :goto_f
    invoke-direct {v3, v0, v1}, Landroid/os/LocaleList;-><init>(Ljava/util/Locale;Landroid/os/LocaleList;)V

    goto/32 :goto_5

    nop

    :goto_10
    new-instance v3, Landroid/os/LocaleList;

    goto/32 :goto_f

    nop

    :goto_11
    invoke-static {v1, v3}, Landroid/os/LocaleList;->setDefault(Landroid/os/LocaleList;I)V

    goto/32 :goto_6

    nop

    :goto_12
    invoke-virtual {v1}, Landroid/app/ResourcesManager;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    goto/32 :goto_17

    nop

    :goto_13
    const/4 v1, 0x0

    goto/32 :goto_a

    nop

    :goto_14
    return-void

    :goto_15
    iget-object v1, p0, Landroid/app/ConfigurationController;->mResourcesManager:Landroid/app/ResourcesManager;

    goto/32 :goto_12

    nop

    :goto_16
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_17
    invoke-virtual {v1}, Landroid/content/res/Configuration;->getLocales()Landroid/os/LocaleList;

    move-result-object v1

    goto/32 :goto_18

    nop

    :goto_18
    invoke-virtual {v1}, Landroid/os/LocaleList;->size()I

    move-result v2

    goto/32 :goto_2

    nop
.end method

.method updatePendingConfiguration(Landroid/content/res/Configuration;)Landroid/content/res/Configuration;
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    throw v1

    :goto_1
    iget-object v0, p0, Landroid/app/ConfigurationController;->mResourcesManager:Landroid/app/ResourcesManager;

    goto/32 :goto_2

    nop

    :goto_2
    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Landroid/app/ConfigurationController;->mPendingConfiguration:Landroid/content/res/Configuration;

    if-eqz v1, :cond_1

    invoke-virtual {v1, p1}, Landroid/content/res/Configuration;->isOtherSeqNewer(Landroid/content/res/Configuration;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_3

    :cond_0
    monitor-exit v0

    const/4 v0, 0x0

    return-object v0

    :cond_1
    :goto_3
    iput-object p1, p0, Landroid/app/ConfigurationController;->mPendingConfiguration:Landroid/content/res/Configuration;

    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_0

    nop
.end method
