.class Landroid/app/Notification$StandardTemplateParams;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/Notification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "StandardTemplateParams"
.end annotation


# static fields
.field public static final DECORATION_MINIMAL:I = 0x1

.field public static final DECORATION_PARTIAL:I = 0x2

.field public static VIEW_TYPE_BIG:I

.field public static VIEW_TYPE_GROUP_HEADER:I

.field public static VIEW_TYPE_HEADS_UP:I

.field public static VIEW_TYPE_MINIMIZED:I

.field public static VIEW_TYPE_NORMAL:I

.field public static VIEW_TYPE_PUBLIC:I

.field public static VIEW_TYPE_UNSPECIFIED:I


# instance fields
.field allowColorization:Z

.field headerTextSecondary:Ljava/lang/CharSequence;

.field mAllowTextWithProgress:Z

.field mCallStyleActions:Z

.field mHeaderless:Z

.field mHideActions:Z

.field mHideAppName:Z

.field mHideLeftIcon:Z

.field mHideProgress:Z

.field mHideRightIcon:Z

.field mHideSnoozeButton:Z

.field mHideSubText:Z

.field mHideTime:Z

.field mHideTitle:Z

.field mHighlightExpander:Z

.field mPromotedPicture:Landroid/graphics/drawable/Icon;

.field mTextViewId:I

.field mTitleViewId:I

.field mViewType:I

.field maxRemoteInputHistory:I

.field summaryText:Ljava/lang/CharSequence;

.field text:Ljava/lang/CharSequence;

.field title:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Landroid/app/Notification$StandardTemplateParams;->VIEW_TYPE_UNSPECIFIED:I

    const/4 v0, 0x1

    sput v0, Landroid/app/Notification$StandardTemplateParams;->VIEW_TYPE_NORMAL:I

    const/4 v0, 0x2

    sput v0, Landroid/app/Notification$StandardTemplateParams;->VIEW_TYPE_BIG:I

    const/4 v0, 0x3

    sput v0, Landroid/app/Notification$StandardTemplateParams;->VIEW_TYPE_HEADS_UP:I

    const/4 v0, 0x4

    sput v0, Landroid/app/Notification$StandardTemplateParams;->VIEW_TYPE_MINIMIZED:I

    const/4 v0, 0x5

    sput v0, Landroid/app/Notification$StandardTemplateParams;->VIEW_TYPE_PUBLIC:I

    const/4 v0, 0x6

    sput v0, Landroid/app/Notification$StandardTemplateParams;->VIEW_TYPE_GROUP_HEADER:I

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Landroid/app/Notification$StandardTemplateParams;->VIEW_TYPE_UNSPECIFIED:I

    iput v0, p0, Landroid/app/Notification$StandardTemplateParams;->mViewType:I

    const/4 v0, 0x3

    iput v0, p0, Landroid/app/Notification$StandardTemplateParams;->maxRemoteInputHistory:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/Notification$StandardTemplateParams;->allowColorization:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/app/Notification$StandardTemplateParams;->mHighlightExpander:Z

    return-void
.end method

.method synthetic constructor <init>(Landroid/app/Notification$StandardTemplateParams-IA;)V
    .locals 0

    invoke-direct {p0}, Landroid/app/Notification$StandardTemplateParams;-><init>()V

    return-void
.end method


# virtual methods
.method final allowTextWithProgress(Z)Landroid/app/Notification$StandardTemplateParams;
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-object p0

    :goto_1
    iput-boolean p1, p0, Landroid/app/Notification$StandardTemplateParams;->mAllowTextWithProgress:Z

    goto/32 :goto_0

    nop
.end method

.method final callStyleActions(Z)Landroid/app/Notification$StandardTemplateParams;
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-object p0

    :goto_1
    iput-boolean p1, p0, Landroid/app/Notification$StandardTemplateParams;->mCallStyleActions:Z

    goto/32 :goto_0

    nop
.end method

.method public decorationType(I)Landroid/app/Notification$StandardTemplateParams;
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/app/Notification$StandardTemplateParams;->hideTitle(Z)Landroid/app/Notification$StandardTemplateParams;

    const/4 v1, 0x0

    if-gt p1, v0, :cond_0

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    invoke-virtual {p0, v1}, Landroid/app/Notification$StandardTemplateParams;->hideLeftIcon(Z)Landroid/app/Notification$StandardTemplateParams;

    invoke-virtual {p0, v0}, Landroid/app/Notification$StandardTemplateParams;->hideRightIcon(Z)Landroid/app/Notification$StandardTemplateParams;

    invoke-virtual {p0, v0}, Landroid/app/Notification$StandardTemplateParams;->hideProgress(Z)Landroid/app/Notification$StandardTemplateParams;

    invoke-virtual {p0, v0}, Landroid/app/Notification$StandardTemplateParams;->hideActions(Z)Landroid/app/Notification$StandardTemplateParams;

    return-object p0
.end method

.method final disallowColorization()Landroid/app/Notification$StandardTemplateParams;
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return-object p0

    :goto_1
    iput-boolean v0, p0, Landroid/app/Notification$StandardTemplateParams;->allowColorization:Z

    goto/32 :goto_0

    nop

    :goto_2
    const/4 v0, 0x0

    goto/32 :goto_1

    nop
.end method

.method final fillTextsFrom(Landroid/app/Notification$Builder;)Landroid/app/Notification$StandardTemplateParams;
    .locals 2

    goto/32 :goto_b

    nop

    :goto_0
    const-string v1, "android.title"

    goto/32 :goto_5

    nop

    :goto_1
    iget-object v0, v0, Landroid/app/Notification;->extras:Landroid/os/Bundle;

    goto/32 :goto_0

    nop

    :goto_2
    iput-object v1, p0, Landroid/app/Notification$StandardTemplateParams;->summaryText:Ljava/lang/CharSequence;

    goto/32 :goto_d

    nop

    :goto_3
    const-string v1, "android.subText"

    goto/32 :goto_a

    nop

    :goto_4
    iput-object v1, p0, Landroid/app/Notification$StandardTemplateParams;->title:Ljava/lang/CharSequence;

    goto/32 :goto_8

    nop

    :goto_5
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    goto/32 :goto_7

    nop

    :goto_6
    invoke-static {p1, v1}, Landroid/app/Notification$Builder;->-$$Nest$mprocessLegacyText(Landroid/app/Notification$Builder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    goto/32 :goto_9

    nop

    :goto_7
    invoke-static {p1, v1}, Landroid/app/Notification$Builder;->-$$Nest$mprocessLegacyText(Landroid/app/Notification$Builder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    goto/32 :goto_4

    nop

    :goto_8
    const-string v1, "android.text"

    goto/32 :goto_c

    nop

    :goto_9
    iput-object v1, p0, Landroid/app/Notification$StandardTemplateParams;->text:Ljava/lang/CharSequence;

    goto/32 :goto_3

    nop

    :goto_a
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    goto/32 :goto_2

    nop

    :goto_b
    invoke-static {p1}, Landroid/app/Notification$Builder;->-$$Nest$fgetmN(Landroid/app/Notification$Builder;)Landroid/app/Notification;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_c
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    goto/32 :goto_6

    nop

    :goto_d
    return-object p0
.end method

.method final hasTitle()Z
    .locals 1

    goto/32 :goto_a

    nop

    :goto_0
    if-eqz v0, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_7

    nop

    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    goto/32 :goto_6

    nop

    :goto_2
    iget-boolean v0, p0, Landroid/app/Notification$StandardTemplateParams;->mHideTitle:Z

    goto/32 :goto_0

    nop

    :goto_3
    return v0

    :goto_4
    const/4 v0, 0x0

    :goto_5
    goto/32 :goto_3

    nop

    :goto_6
    if-eqz v0, :cond_1

    goto/32 :goto_9

    :cond_1
    goto/32 :goto_2

    nop

    :goto_7
    const/4 v0, 0x1

    goto/32 :goto_8

    nop

    :goto_8
    goto :goto_5

    :goto_9
    goto/32 :goto_4

    nop

    :goto_a
    iget-object v0, p0, Landroid/app/Notification$StandardTemplateParams;->title:Ljava/lang/CharSequence;

    goto/32 :goto_1

    nop
.end method

.method final headerTextSecondary(Ljava/lang/CharSequence;)Landroid/app/Notification$StandardTemplateParams;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iput-object p1, p0, Landroid/app/Notification$StandardTemplateParams;->headerTextSecondary:Ljava/lang/CharSequence;

    goto/32 :goto_1

    nop

    :goto_1
    return-object p0
.end method

.method public headerless(Z)Landroid/app/Notification$StandardTemplateParams;
    .locals 0

    iput-boolean p1, p0, Landroid/app/Notification$StandardTemplateParams;->mHeaderless:Z

    return-object p0
.end method

.method final hideActions(Z)Landroid/app/Notification$StandardTemplateParams;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iput-boolean p1, p0, Landroid/app/Notification$StandardTemplateParams;->mHideActions:Z

    goto/32 :goto_1

    nop

    :goto_1
    return-object p0
.end method

.method public hideAppName(Z)Landroid/app/Notification$StandardTemplateParams;
    .locals 0

    iput-boolean p1, p0, Landroid/app/Notification$StandardTemplateParams;->mHideAppName:Z

    return-object p0
.end method

.method final hideLeftIcon(Z)Landroid/app/Notification$StandardTemplateParams;
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-object p0

    :goto_1
    iput-boolean p1, p0, Landroid/app/Notification$StandardTemplateParams;->mHideLeftIcon:Z

    goto/32 :goto_0

    nop
.end method

.method final hideProgress(Z)Landroid/app/Notification$StandardTemplateParams;
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-object p0

    :goto_1
    iput-boolean p1, p0, Landroid/app/Notification$StandardTemplateParams;->mHideProgress:Z

    goto/32 :goto_0

    nop
.end method

.method final hideRightIcon(Z)Landroid/app/Notification$StandardTemplateParams;
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-object p0

    :goto_1
    iput-boolean p1, p0, Landroid/app/Notification$StandardTemplateParams;->mHideRightIcon:Z

    goto/32 :goto_0

    nop
.end method

.method final hideSnoozeButton(Z)Landroid/app/Notification$StandardTemplateParams;
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-object p0

    :goto_1
    iput-boolean p1, p0, Landroid/app/Notification$StandardTemplateParams;->mHideSnoozeButton:Z

    goto/32 :goto_0

    nop
.end method

.method public hideSubText(Z)Landroid/app/Notification$StandardTemplateParams;
    .locals 0

    iput-boolean p1, p0, Landroid/app/Notification$StandardTemplateParams;->mHideSubText:Z

    return-object p0
.end method

.method public hideTime(Z)Landroid/app/Notification$StandardTemplateParams;
    .locals 0

    iput-boolean p1, p0, Landroid/app/Notification$StandardTemplateParams;->mHideTime:Z

    return-object p0
.end method

.method final hideTitle(Z)Landroid/app/Notification$StandardTemplateParams;
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-object p0

    :goto_1
    iput-boolean p1, p0, Landroid/app/Notification$StandardTemplateParams;->mHideTitle:Z

    goto/32 :goto_0

    nop
.end method

.method final highlightExpander(Z)Landroid/app/Notification$StandardTemplateParams;
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-object p0

    :goto_1
    iput-boolean p1, p0, Landroid/app/Notification$StandardTemplateParams;->mHighlightExpander:Z

    goto/32 :goto_0

    nop
.end method

.method final promotedPicture(Landroid/graphics/drawable/Icon;)Landroid/app/Notification$StandardTemplateParams;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iput-object p1, p0, Landroid/app/Notification$StandardTemplateParams;->mPromotedPicture:Landroid/graphics/drawable/Icon;

    goto/32 :goto_1

    nop

    :goto_1
    return-object p0
.end method

.method final reset()Landroid/app/Notification$StandardTemplateParams;
    .locals 3

    goto/32 :goto_1c

    nop

    :goto_0
    iput-object v1, p0, Landroid/app/Notification$StandardTemplateParams;->headerTextSecondary:Ljava/lang/CharSequence;

    goto/32 :goto_13

    nop

    :goto_1
    iput-boolean v0, p0, Landroid/app/Notification$StandardTemplateParams;->mHideRightIcon:Z

    goto/32 :goto_15

    nop

    :goto_2
    iput-boolean v0, p0, Landroid/app/Notification$StandardTemplateParams;->mHideProgress:Z

    goto/32 :goto_4

    nop

    :goto_3
    iput-boolean v1, p0, Landroid/app/Notification$StandardTemplateParams;->allowColorization:Z

    goto/32 :goto_18

    nop

    :goto_4
    iput-boolean v0, p0, Landroid/app/Notification$StandardTemplateParams;->mHideSnoozeButton:Z

    goto/32 :goto_12

    nop

    :goto_5
    return-object p0

    :goto_6
    iput-boolean v0, p0, Landroid/app/Notification$StandardTemplateParams;->mHideTime:Z

    goto/32 :goto_1a

    nop

    :goto_7
    iput-boolean v0, p0, Landroid/app/Notification$StandardTemplateParams;->mHeaderless:Z

    goto/32 :goto_a

    nop

    :goto_8
    iput-boolean v0, p0, Landroid/app/Notification$StandardTemplateParams;->mHideTitle:Z

    goto/32 :goto_1b

    nop

    :goto_9
    const/4 v0, 0x0

    goto/32 :goto_7

    nop

    :goto_a
    iput-boolean v0, p0, Landroid/app/Notification$StandardTemplateParams;->mHideAppName:Z

    goto/32 :goto_8

    nop

    :goto_b
    iput-object v1, p0, Landroid/app/Notification$StandardTemplateParams;->title:Ljava/lang/CharSequence;

    goto/32 :goto_10

    nop

    :goto_c
    iput v2, p0, Landroid/app/Notification$StandardTemplateParams;->mTitleViewId:I

    goto/32 :goto_1e

    nop

    :goto_d
    iput-object v1, p0, Landroid/app/Notification$StandardTemplateParams;->summaryText:Ljava/lang/CharSequence;

    goto/32 :goto_0

    nop

    :goto_e
    iput-boolean v0, p0, Landroid/app/Notification$StandardTemplateParams;->mCallStyleActions:Z

    goto/32 :goto_16

    nop

    :goto_f
    iput v0, p0, Landroid/app/Notification$StandardTemplateParams;->mViewType:I

    goto/32 :goto_9

    nop

    :goto_10
    iput-object v1, p0, Landroid/app/Notification$StandardTemplateParams;->text:Ljava/lang/CharSequence;

    goto/32 :goto_d

    nop

    :goto_11
    iput-object v1, p0, Landroid/app/Notification$StandardTemplateParams;->mPromotedPicture:Landroid/graphics/drawable/Icon;

    goto/32 :goto_e

    nop

    :goto_12
    iput-boolean v0, p0, Landroid/app/Notification$StandardTemplateParams;->mHideLeftIcon:Z

    goto/32 :goto_1

    nop

    :goto_13
    const/4 v1, 0x3

    goto/32 :goto_19

    nop

    :goto_14
    iput v2, p0, Landroid/app/Notification$StandardTemplateParams;->mTextViewId:I

    goto/32 :goto_b

    nop

    :goto_15
    const/4 v1, 0x0

    goto/32 :goto_11

    nop

    :goto_16
    iput-boolean v0, p0, Landroid/app/Notification$StandardTemplateParams;->mAllowTextWithProgress:Z

    goto/32 :goto_17

    nop

    :goto_17
    const v2, 0x1020016

    goto/32 :goto_c

    nop

    :goto_18
    iput-boolean v0, p0, Landroid/app/Notification$StandardTemplateParams;->mHighlightExpander:Z

    goto/32 :goto_5

    nop

    :goto_19
    iput v1, p0, Landroid/app/Notification$StandardTemplateParams;->maxRemoteInputHistory:I

    goto/32 :goto_1d

    nop

    :goto_1a
    iput-boolean v0, p0, Landroid/app/Notification$StandardTemplateParams;->mHideActions:Z

    goto/32 :goto_2

    nop

    :goto_1b
    iput-boolean v0, p0, Landroid/app/Notification$StandardTemplateParams;->mHideSubText:Z

    goto/32 :goto_6

    nop

    :goto_1c
    sget v0, Landroid/app/Notification$StandardTemplateParams;->VIEW_TYPE_UNSPECIFIED:I

    goto/32 :goto_f

    nop

    :goto_1d
    const/4 v1, 0x1

    goto/32 :goto_3

    nop

    :goto_1e
    const v2, 0x102051b

    goto/32 :goto_14

    nop
.end method

.method public setMaxRemoteInputHistory(I)Landroid/app/Notification$StandardTemplateParams;
    .locals 0

    iput p1, p0, Landroid/app/Notification$StandardTemplateParams;->maxRemoteInputHistory:I

    return-object p0
.end method

.method final summaryText(Ljava/lang/CharSequence;)Landroid/app/Notification$StandardTemplateParams;
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-object p0

    :goto_1
    iput-object p1, p0, Landroid/app/Notification$StandardTemplateParams;->summaryText:Ljava/lang/CharSequence;

    goto/32 :goto_0

    nop
.end method

.method final text(Ljava/lang/CharSequence;)Landroid/app/Notification$StandardTemplateParams;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iput-object p1, p0, Landroid/app/Notification$StandardTemplateParams;->text:Ljava/lang/CharSequence;

    goto/32 :goto_1

    nop

    :goto_1
    return-object p0
.end method

.method public textViewId(I)Landroid/app/Notification$StandardTemplateParams;
    .locals 0

    iput p1, p0, Landroid/app/Notification$StandardTemplateParams;->mTextViewId:I

    return-object p0
.end method

.method final title(Ljava/lang/CharSequence;)Landroid/app/Notification$StandardTemplateParams;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iput-object p1, p0, Landroid/app/Notification$StandardTemplateParams;->title:Ljava/lang/CharSequence;

    goto/32 :goto_1

    nop

    :goto_1
    return-object p0
.end method

.method public titleViewId(I)Landroid/app/Notification$StandardTemplateParams;
    .locals 0

    iput p1, p0, Landroid/app/Notification$StandardTemplateParams;->mTitleViewId:I

    return-object p0
.end method

.method final viewType(I)Landroid/app/Notification$StandardTemplateParams;
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-object p0

    :goto_1
    iput p1, p0, Landroid/app/Notification$StandardTemplateParams;->mViewType:I

    goto/32 :goto_0

    nop
.end method
