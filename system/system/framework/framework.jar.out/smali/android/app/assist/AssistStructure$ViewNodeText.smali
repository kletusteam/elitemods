.class final Landroid/app/assist/AssistStructure$ViewNodeText;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/assist/AssistStructure;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ViewNodeText"
.end annotation


# instance fields
.field mHint:Ljava/lang/String;

.field mLineBaselines:[I

.field mLineCharOffsets:[I

.field mText:Ljava/lang/CharSequence;

.field mTextBackgroundColor:I

.field mTextColor:I

.field mTextSelectionEnd:I

.field mTextSelectionStart:I

.field mTextSize:F

.field mTextStyle:I


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mTextColor:I

    iput v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mTextBackgroundColor:I

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mTextColor:I

    iput v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mTextBackgroundColor:I

    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mText:Ljava/lang/CharSequence;

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mTextSize:F

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mTextStyle:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mTextColor:I

    if-nez p2, :cond_0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mTextBackgroundColor:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mTextSelectionStart:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mTextSelectionEnd:I

    invoke-virtual {p1}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v0

    iput-object v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mLineCharOffsets:[I

    invoke-virtual {p1}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v0

    iput-object v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mLineBaselines:[I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mHint:Ljava/lang/String;

    :cond_0
    return-void
.end method


# virtual methods
.method isSimple()Z
    .locals 2

    goto/32 :goto_8

    nop

    :goto_0
    if-eqz v0, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_4

    nop

    :goto_1
    if-eqz v0, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_a

    nop

    :goto_2
    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mTextSelectionStart:I

    goto/32 :goto_b

    nop

    :goto_3
    if-eq v0, v1, :cond_2

    goto/32 :goto_e

    :cond_2
    goto/32 :goto_2

    nop

    :goto_4
    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mLineCharOffsets:[I

    goto/32 :goto_10

    nop

    :goto_5
    const/4 v1, 0x0

    :goto_6
    goto/32 :goto_9

    nop

    :goto_7
    const/4 v1, 0x1

    goto/32 :goto_3

    nop

    :goto_8
    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mTextBackgroundColor:I

    goto/32 :goto_7

    nop

    :goto_9
    return v1

    :goto_a
    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mHint:Ljava/lang/String;

    goto/32 :goto_f

    nop

    :goto_b
    if-eqz v0, :cond_3

    goto/32 :goto_e

    :cond_3
    goto/32 :goto_11

    nop

    :goto_c
    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mLineBaselines:[I

    goto/32 :goto_1

    nop

    :goto_d
    goto :goto_6

    :goto_e
    goto/32 :goto_5

    nop

    :goto_f
    if-eqz v0, :cond_4

    goto/32 :goto_e

    :cond_4
    goto/32 :goto_d

    nop

    :goto_10
    if-eqz v0, :cond_5

    goto/32 :goto_e

    :cond_5
    goto/32 :goto_c

    nop

    :goto_11
    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mTextSelectionEnd:I

    goto/32 :goto_0

    nop
.end method

.method writeToParcel(Landroid/os/Parcel;ZZ)V
    .locals 2

    goto/32 :goto_4

    nop

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mText:Ljava/lang/CharSequence;

    goto/32 :goto_17

    nop

    :goto_2
    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mTextStyle:I

    goto/32 :goto_1c

    nop

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    goto/32 :goto_b

    nop

    :goto_4
    if-nez p3, :cond_0

    goto/32 :goto_18

    :cond_0
    goto/32 :goto_1

    nop

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_e

    nop

    :goto_6
    return-void

    :goto_7
    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mHint:Ljava/lang/String;

    goto/32 :goto_f

    nop

    :goto_8
    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mTextSize:F

    goto/32 :goto_0

    nop

    :goto_9
    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mTextBackgroundColor:I

    goto/32 :goto_14

    nop

    :goto_a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    goto/32 :goto_7

    nop

    :goto_b
    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mLineBaselines:[I

    goto/32 :goto_a

    nop

    :goto_c
    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mTextSelectionEnd:I

    goto/32 :goto_5

    nop

    :goto_d
    const/4 v1, 0x0

    goto/32 :goto_1b

    nop

    :goto_e
    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mLineCharOffsets:[I

    goto/32 :goto_3

    nop

    :goto_f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    :goto_10
    goto/32 :goto_6

    nop

    :goto_11
    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mTextSelectionStart:I

    goto/32 :goto_16

    nop

    :goto_12
    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNodeText;->mTextColor:I

    goto/32 :goto_13

    nop

    :goto_13
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_15

    nop

    :goto_14
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_11

    nop

    :goto_15
    if-eqz p2, :cond_1

    goto/32 :goto_10

    :cond_1
    goto/32 :goto_9

    nop

    :goto_16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_c

    nop

    :goto_17
    goto :goto_1a

    :goto_18
    goto/32 :goto_19

    nop

    :goto_19
    const-string v0, ""

    :goto_1a
    goto/32 :goto_d

    nop

    :goto_1b
    invoke-static {v0, p1, v1}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    goto/32 :goto_8

    nop

    :goto_1c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_12

    nop
.end method
