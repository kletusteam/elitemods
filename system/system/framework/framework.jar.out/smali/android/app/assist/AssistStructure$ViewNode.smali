.class public Landroid/app/assist/AssistStructure$ViewNode;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/assist/AssistStructure;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ViewNode"
.end annotation


# static fields
.field static final AUTOFILL_FLAGS_HAS_AUTOFILL_HINTS:I = 0x10

.field static final AUTOFILL_FLAGS_HAS_AUTOFILL_OPTIONS:I = 0x20

.field static final AUTOFILL_FLAGS_HAS_AUTOFILL_SESSION_ID:I = 0x800

.field static final AUTOFILL_FLAGS_HAS_AUTOFILL_TYPE:I = 0x8

.field static final AUTOFILL_FLAGS_HAS_AUTOFILL_VALUE:I = 0x4

.field static final AUTOFILL_FLAGS_HAS_AUTOFILL_VIEW_ID:I = 0x1

.field static final AUTOFILL_FLAGS_HAS_AUTOFILL_VIRTUAL_VIEW_ID:I = 0x2

.field static final AUTOFILL_FLAGS_HAS_HINT_ID_ENTRY:I = 0x1000

.field static final AUTOFILL_FLAGS_HAS_HTML_INFO:I = 0x40

.field static final AUTOFILL_FLAGS_HAS_MAX_TEXT_EMS:I = 0x200

.field static final AUTOFILL_FLAGS_HAS_MAX_TEXT_LENGTH:I = 0x400

.field static final AUTOFILL_FLAGS_HAS_MIN_TEXT_EMS:I = 0x100

.field static final AUTOFILL_FLAGS_HAS_TEXT_ID_ENTRY:I = 0x80

.field static final FLAGS_ACCESSIBILITY_FOCUSED:I = 0x1000

.field static final FLAGS_ACTIVATED:I = 0x2000

.field static final FLAGS_ALL_CONTROL:I = -0x10000

.field static final FLAGS_ASSIST_BLOCKED:I = 0x80

.field static final FLAGS_CHECKABLE:I = 0x100

.field static final FLAGS_CHECKED:I = 0x200

.field static final FLAGS_CLICKABLE:I = 0x400

.field static final FLAGS_CONTEXT_CLICKABLE:I = 0x4000

.field static final FLAGS_DISABLED:I = 0x1

.field static final FLAGS_FOCUSABLE:I = 0x10

.field static final FLAGS_FOCUSED:I = 0x20

.field static final FLAGS_HAS_ALPHA:I = 0x20000000

.field static final FLAGS_HAS_CHILDREN:I = 0x100000

.field static final FLAGS_HAS_COMPLEX_TEXT:I = 0x800000

.field static final FLAGS_HAS_CONTENT_DESCRIPTION:I = 0x2000000

.field static final FLAGS_HAS_ELEVATION:I = 0x10000000

.field static final FLAGS_HAS_EXTRAS:I = 0x400000

.field static final FLAGS_HAS_ID:I = 0x200000

.field static final FLAGS_HAS_INPUT_TYPE:I = 0x40000

.field static final FLAGS_HAS_LARGE_COORDS:I = 0x4000000

.field static final FLAGS_HAS_LOCALE_LIST:I = 0x10000

.field static final FLAGS_HAS_MATRIX:I = 0x40000000

.field static final FLAGS_HAS_MIME_TYPES:I = -0x80000000

.field static final FLAGS_HAS_SCROLL:I = 0x8000000

.field static final FLAGS_HAS_TEXT:I = 0x1000000

.field static final FLAGS_HAS_URL_DOMAIN:I = 0x80000

.field static final FLAGS_HAS_URL_SCHEME:I = 0x20000

.field static final FLAGS_LONG_CLICKABLE:I = 0x800

.field static final FLAGS_OPAQUE:I = 0x8000

.field static final FLAGS_SELECTED:I = 0x40

.field static final FLAGS_VISIBILITY_MASK:I = 0xc

.field public static final TEXT_COLOR_UNDEFINED:I = 0x1

.field public static final TEXT_STYLE_BOLD:I = 0x1

.field public static final TEXT_STYLE_ITALIC:I = 0x2

.field public static final TEXT_STYLE_STRIKE_THRU:I = 0x8

.field public static final TEXT_STYLE_UNDERLINE:I = 0x4


# instance fields
.field mAlpha:F

.field mAutofillFlags:I

.field mAutofillHints:[Ljava/lang/String;

.field mAutofillId:Landroid/view/autofill/AutofillId;

.field mAutofillOptions:[Ljava/lang/CharSequence;

.field mAutofillOverlay:Landroid/app/assist/AssistStructure$AutofillOverlay;

.field mAutofillType:I

.field mAutofillValue:Landroid/view/autofill/AutofillValue;

.field mChildren:[Landroid/app/assist/AssistStructure$ViewNode;

.field mClassName:Ljava/lang/String;

.field mContentDescription:Ljava/lang/CharSequence;

.field mElevation:F

.field mExtras:Landroid/os/Bundle;

.field mFlags:I

.field mHeight:I

.field mHintIdEntry:Ljava/lang/String;

.field mHtmlInfo:Landroid/view/ViewStructure$HtmlInfo;

.field mId:I

.field mIdEntry:Ljava/lang/String;

.field mIdPackage:Ljava/lang/String;

.field mIdType:Ljava/lang/String;

.field mImportantForAutofill:I

.field mInputType:I

.field mLocaleList:Landroid/os/LocaleList;

.field mMatrix:Landroid/graphics/Matrix;

.field mMaxEms:I

.field mMaxLength:I

.field mMinEms:I

.field mReceiveContentMimeTypes:[Ljava/lang/String;

.field mSanitized:Z

.field mScrollX:I

.field mScrollY:I

.field mText:Landroid/app/assist/AssistStructure$ViewNodeText;

.field mTextIdEntry:Ljava/lang/String;

.field mWebDomain:Ljava/lang/String;

.field mWebScheme:Ljava/lang/String;

.field mWidth:I

.field mX:I

.field mY:I


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mId:I

    const/4 v1, 0x0

    iput v1, p0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillType:I

    iput v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mMinEms:I

    iput v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mMaxEms:I

    iput v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mMaxLength:I

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mAlpha:F

    return-void
.end method

.method constructor <init>(Landroid/app/assist/AssistStructure$ParcelTransferReader;I)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mId:I

    const/4 v1, 0x0

    iput v1, p0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillType:I

    iput v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mMinEms:I

    iput v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mMaxEms:I

    iput v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mMaxLength:I

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mAlpha:F

    const v0, 0x22222222

    invoke-virtual {p1, v0, p2}, Landroid/app/assist/AssistStructure$ParcelTransferReader;->readParcel(II)Landroid/os/Parcel;

    move-result-object v0

    iget v1, p1, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mNumReadViews:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p1, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mNumReadViews:I

    iget-object v1, p1, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mStringReader:Landroid/os/PooledStringReader;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PooledStringReader;

    iget-object v2, p1, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mTmpMatrix:[F

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [F

    invoke-virtual {p0, v0, v1, v2}, Landroid/app/assist/AssistStructure$ViewNode;->initializeFromParcelWithoutChildren(Landroid/os/Parcel;Landroid/os/PooledStringReader;[F)V

    iget v1, p0, Landroid/app/assist/AssistStructure$ViewNode;->mFlags:I

    const/high16 v2, 0x100000

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/os/Parcel;->readInt()I

    move-result v1

    new-array v2, v1, [Landroid/app/assist/AssistStructure$ViewNode;

    iput-object v2, p0, Landroid/app/assist/AssistStructure$ViewNode;->mChildren:[Landroid/app/assist/AssistStructure$ViewNode;

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    iget-object v3, p0, Landroid/app/assist/AssistStructure$ViewNode;->mChildren:[Landroid/app/assist/AssistStructure$ViewNode;

    new-instance v4, Landroid/app/assist/AssistStructure$ViewNode;

    add-int/lit8 v5, p2, 0x1

    invoke-direct {v4, p1, v5}, Landroid/app/assist/AssistStructure$ViewNode;-><init>(Landroid/app/assist/AssistStructure$ParcelTransferReader;I)V

    aput-object v4, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mId:I

    const/4 v1, 0x0

    iput v1, p0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillType:I

    iput v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mMinEms:I

    iput v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mMaxEms:I

    iput v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mMaxLength:I

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mAlpha:F

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v0}, Landroid/app/assist/AssistStructure$ViewNode;->initializeFromParcelWithoutChildren(Landroid/os/Parcel;Landroid/os/PooledStringReader;[F)V

    return-void
.end method

.method private static readString(Landroid/os/Parcel;Landroid/os/PooledStringReader;)Ljava/lang/String;
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/os/PooledStringReader;->readString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static writeString(Landroid/os/Parcel;Landroid/os/PooledStringWriter;Ljava/lang/String;)V
    .locals 0

    if-eqz p1, :cond_0

    invoke-virtual {p1, p2}, Landroid/os/PooledStringWriter;->writeString(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public getAlpha()F
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mAlpha:F

    return v0
.end method

.method public getAutofillHints()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillHints:[Ljava/lang/String;

    return-object v0
.end method

.method public getAutofillId()Landroid/view/autofill/AutofillId;
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillId:Landroid/view/autofill/AutofillId;

    return-object v0
.end method

.method public getAutofillOptions()[Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillOptions:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getAutofillType()I
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillType:I

    return v0
.end method

.method public getAutofillValue()Landroid/view/autofill/AutofillValue;
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillValue:Landroid/view/autofill/AutofillValue;

    return-object v0
.end method

.method public getChildAt(I)Landroid/app/assist/AssistStructure$ViewNode;
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mChildren:[Landroid/app/assist/AssistStructure$ViewNode;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getChildCount()I
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mChildren:[Landroid/app/assist/AssistStructure$ViewNode;

    if-eqz v0, :cond_0

    array-length v0, v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getClassName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mClassName:Ljava/lang/String;

    return-object v0
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mContentDescription:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getElevation()F
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mElevation:F

    return v0
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mExtras:Landroid/os/Bundle;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mHeight:I

    return v0
.end method

.method public getHint()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mText:Landroid/app/assist/AssistStructure$ViewNodeText;

    if-eqz v0, :cond_0

    iget-object v0, v0, Landroid/app/assist/AssistStructure$ViewNodeText;->mHint:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getHintIdEntry()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mHintIdEntry:Ljava/lang/String;

    return-object v0
.end method

.method public getHtmlInfo()Landroid/view/ViewStructure$HtmlInfo;
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mHtmlInfo:Landroid/view/ViewStructure$HtmlInfo;

    return-object v0
.end method

.method public getId()I
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mId:I

    return v0
.end method

.method public getIdEntry()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mIdEntry:Ljava/lang/String;

    return-object v0
.end method

.method public getIdPackage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mIdPackage:Ljava/lang/String;

    return-object v0
.end method

.method public getIdType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mIdType:Ljava/lang/String;

    return-object v0
.end method

.method public getImportantForAutofill()I
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mImportantForAutofill:I

    return v0
.end method

.method public getInputType()I
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mInputType:I

    return v0
.end method

.method public getLeft()I
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mX:I

    return v0
.end method

.method public getLocaleList()Landroid/os/LocaleList;
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mLocaleList:Landroid/os/LocaleList;

    return-object v0
.end method

.method public getMaxTextEms()I
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mMaxEms:I

    return v0
.end method

.method public getMaxTextLength()I
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mMaxLength:I

    return v0
.end method

.method public getMinTextEms()I
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mMinEms:I

    return v0
.end method

.method public getReceiveContentMimeTypes()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mReceiveContentMimeTypes:[Ljava/lang/String;

    return-object v0
.end method

.method public getScrollX()I
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mScrollX:I

    return v0
.end method

.method public getScrollY()I
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mScrollY:I

    return v0
.end method

.method public getText()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mText:Landroid/app/assist/AssistStructure$ViewNodeText;

    if-eqz v0, :cond_0

    iget-object v0, v0, Landroid/app/assist/AssistStructure$ViewNodeText;->mText:Ljava/lang/CharSequence;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getTextBackgroundColor()I
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mText:Landroid/app/assist/AssistStructure$ViewNodeText;

    if-eqz v0, :cond_0

    iget v0, v0, Landroid/app/assist/AssistStructure$ViewNodeText;->mTextBackgroundColor:I

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0
.end method

.method public getTextColor()I
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mText:Landroid/app/assist/AssistStructure$ViewNodeText;

    if-eqz v0, :cond_0

    iget v0, v0, Landroid/app/assist/AssistStructure$ViewNodeText;->mTextColor:I

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0
.end method

.method public getTextIdEntry()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mTextIdEntry:Ljava/lang/String;

    return-object v0
.end method

.method public getTextLineBaselines()[I
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mText:Landroid/app/assist/AssistStructure$ViewNodeText;

    if-eqz v0, :cond_0

    iget-object v0, v0, Landroid/app/assist/AssistStructure$ViewNodeText;->mLineBaselines:[I

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getTextLineCharOffsets()[I
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mText:Landroid/app/assist/AssistStructure$ViewNodeText;

    if-eqz v0, :cond_0

    iget-object v0, v0, Landroid/app/assist/AssistStructure$ViewNodeText;->mLineCharOffsets:[I

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getTextSelectionEnd()I
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mText:Landroid/app/assist/AssistStructure$ViewNodeText;

    if-eqz v0, :cond_0

    iget v0, v0, Landroid/app/assist/AssistStructure$ViewNodeText;->mTextSelectionEnd:I

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0
.end method

.method public getTextSelectionStart()I
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mText:Landroid/app/assist/AssistStructure$ViewNodeText;

    if-eqz v0, :cond_0

    iget v0, v0, Landroid/app/assist/AssistStructure$ViewNodeText;->mTextSelectionStart:I

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0
.end method

.method public getTextSize()F
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mText:Landroid/app/assist/AssistStructure$ViewNodeText;

    if-eqz v0, :cond_0

    iget v0, v0, Landroid/app/assist/AssistStructure$ViewNodeText;->mTextSize:F

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getTextStyle()I
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mText:Landroid/app/assist/AssistStructure$ViewNodeText;

    if-eqz v0, :cond_0

    iget v0, v0, Landroid/app/assist/AssistStructure$ViewNodeText;->mTextStyle:I

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getTop()I
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mY:I

    return v0
.end method

.method public getTransformation()Landroid/graphics/Matrix;
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public getVisibility()I
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mFlags:I

    and-int/lit8 v0, v0, 0xc

    return v0
.end method

.method public getWebDomain()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mWebDomain:Ljava/lang/String;

    return-object v0
.end method

.method public getWebScheme()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mWebScheme:Ljava/lang/String;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mWidth:I

    return v0
.end method

.method initializeFromParcelWithoutChildren(Landroid/os/Parcel;Landroid/os/PooledStringReader;[F)V
    .locals 8

    goto/32 :goto_df

    nop

    :goto_0
    const-class v5, Landroid/view/autofill/AutofillValue;

    goto/32 :goto_75

    nop

    :goto_1
    iput-object v6, p0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillId:Landroid/view/autofill/AutofillId;

    goto/32 :goto_a9

    nop

    :goto_2
    const/high16 v2, 0x10000

    goto/32 :goto_6d

    nop

    :goto_3
    iput v6, p0, Landroid/app/assist/AssistStructure$ViewNode;->mX:I

    goto/32 :goto_99

    nop

    :goto_4
    iput-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mClassName:Ljava/lang/String;

    goto/32 :goto_3a

    nop

    :goto_5
    if-nez v2, :cond_0

    goto/32 :goto_92

    :cond_0
    goto/32 :goto_b5

    nop

    :goto_6
    and-int/lit16 v5, v1, 0x200

    goto/32 :goto_9e

    nop

    :goto_7
    and-int/2addr v5, v0

    goto/32 :goto_64

    nop

    :goto_8
    new-instance v5, Landroid/app/assist/AssistStructure$ViewNodeText;

    goto/32 :goto_a7

    nop

    :goto_9
    and-int/lit16 v6, v6, 0x7fff

    goto/32 :goto_26

    nop

    :goto_a
    const/high16 v2, 0x20000

    goto/32 :goto_cb

    nop

    :goto_b
    iput v5, p0, Landroid/app/assist/AssistStructure$ViewNode;->mX:I

    goto/32 :goto_42

    nop

    :goto_c
    new-array p3, v5, [F

    :goto_d
    goto/32 :goto_7e

    nop

    :goto_e
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    goto/32 :goto_2b

    nop

    :goto_f
    if-nez v2, :cond_1

    goto/32 :goto_92

    :cond_1
    goto/32 :goto_8d

    nop

    :goto_10
    const/high16 v5, 0x1000000

    goto/32 :goto_7

    nop

    :goto_11
    iput v5, p0, Landroid/app/assist/AssistStructure$ViewNode;->mElevation:F

    :goto_12
    goto/32 :goto_86

    nop

    :goto_13
    invoke-direct {v5, p1, v2}, Landroid/app/assist/AssistStructure$ViewNodeText;-><init>(Landroid/os/Parcel;Z)V

    goto/32 :goto_49

    nop

    :goto_14
    if-eqz p3, :cond_2

    goto/32 :goto_d

    :cond_2
    goto/32 :goto_9c

    nop

    :goto_15
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    goto/32 :goto_b

    nop

    :goto_16
    iput-object v2, p0, Landroid/app/assist/AssistStructure$ViewNode;->mLocaleList:Landroid/os/LocaleList;

    :goto_17
    goto/32 :goto_81

    nop

    :goto_18
    iget v1, p0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillFlags:I

    goto/32 :goto_8c

    nop

    :goto_19
    iput-object v5, p0, Landroid/app/assist/AssistStructure$ViewNode;->mHintIdEntry:Ljava/lang/String;

    :goto_1a
    goto/32 :goto_9b

    nop

    :goto_1b
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v7

    goto/32 :goto_c2

    nop

    :goto_1c
    if-nez v2, :cond_3

    goto/32 :goto_d1

    :cond_3
    goto/32 :goto_74

    nop

    :goto_1d
    if-nez v2, :cond_4

    goto/32 :goto_32

    :cond_4
    goto/32 :goto_ce

    nop

    :goto_1e
    const-class v2, Landroid/os/LocaleList;

    goto/32 :goto_96

    nop

    :goto_1f
    const/high16 v5, 0x8000000

    goto/32 :goto_8b

    nop

    :goto_20
    if-nez v2, :cond_5

    goto/32 :goto_b9

    :cond_5
    goto/32 :goto_4d

    nop

    :goto_21
    iput v5, p0, Landroid/app/assist/AssistStructure$ViewNode;->mHeight:I

    goto/32 :goto_be

    nop

    :goto_22
    iput-object v5, p0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillValue:Landroid/view/autofill/AutofillValue;

    :goto_23
    goto/32 :goto_59

    nop

    :goto_24
    and-int/2addr v5, v0

    goto/32 :goto_37

    nop

    :goto_25
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v7

    goto/32 :goto_48

    nop

    :goto_26
    iput v6, p0, Landroid/app/assist/AssistStructure$ViewNode;->mY:I

    goto/32 :goto_e0

    nop

    :goto_27
    invoke-static {p1, p2}, Landroid/app/assist/AssistStructure$ViewNode;->readString(Landroid/os/Parcel;Landroid/os/PooledStringReader;)Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_d6

    nop

    :goto_28
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    goto/32 :goto_8e

    nop

    :goto_29
    move v2, v4

    :goto_2a
    goto/32 :goto_13

    nop

    :goto_2b
    iput v5, p0, Landroid/app/assist/AssistStructure$ViewNode;->mMaxEms:I

    :goto_2c
    goto/32 :goto_a4

    nop

    :goto_2d
    iput v5, p0, Landroid/app/assist/AssistStructure$ViewNode;->mScrollX:I

    goto/32 :goto_54

    nop

    :goto_2e
    if-nez v6, :cond_6

    goto/32 :goto_c3

    :cond_6
    goto/32 :goto_40

    nop

    :goto_2f
    iput v6, p0, Landroid/app/assist/AssistStructure$ViewNode;->mHeight:I

    :goto_30
    goto/32 :goto_1f

    nop

    :goto_31
    iput-object v2, p0, Landroid/app/assist/AssistStructure$ViewNode;->mReceiveContentMimeTypes:[Ljava/lang/String;

    :goto_32
    goto/32 :goto_5a

    nop

    :goto_33
    const/4 v2, 0x0

    goto/32 :goto_3b

    nop

    :goto_34
    check-cast v2, Landroid/os/LocaleList;

    goto/32 :goto_16

    nop

    :goto_35
    if-nez v5, :cond_7

    goto/32 :goto_23

    :cond_7
    goto/32 :goto_0

    nop

    :goto_36
    const/high16 v5, 0x10000000

    goto/32 :goto_24

    nop

    :goto_37
    if-nez v5, :cond_8

    goto/32 :goto_12

    :cond_8
    goto/32 :goto_79

    nop

    :goto_38
    invoke-virtual {v5, p3}, Landroid/graphics/Matrix;->setValues([F)V

    :goto_39
    goto/32 :goto_36

    nop

    :goto_3a
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    goto/32 :goto_cf

    nop

    :goto_3b
    const/4 v3, 0x0

    goto/32 :goto_d3

    nop

    :goto_3c
    invoke-static {p1, p2}, Landroid/app/assist/AssistStructure$ViewNode;->readString(Landroid/os/Parcel;Landroid/os/PooledStringReader;)Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_bd

    nop

    :goto_3d
    and-int/2addr v2, v0

    goto/32 :goto_1d

    nop

    :goto_3e
    iput v1, p0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillFlags:I

    goto/32 :goto_18

    nop

    :goto_3f
    and-int/2addr v5, v0

    goto/32 :goto_8a

    nop

    :goto_40
    iget-object v6, p0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillId:Landroid/view/autofill/AutofillId;

    goto/32 :goto_1b

    nop

    :goto_41
    const/high16 v5, 0x2000000

    goto/32 :goto_3f

    nop

    :goto_42
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    goto/32 :goto_55

    nop

    :goto_43
    if-nez v5, :cond_9

    goto/32 :goto_bf

    :cond_9
    goto/32 :goto_15

    nop

    :goto_44
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    goto/32 :goto_3e

    nop

    :goto_45
    if-ne v2, v3, :cond_a

    goto/32 :goto_92

    :cond_a
    goto/32 :goto_3c

    nop

    :goto_46
    iput v5, p0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillType:I

    :goto_47
    goto/32 :goto_ab

    nop

    :goto_48
    invoke-direct {v6, v5, v7}, Landroid/view/autofill/AutofillId;-><init>(II)V

    goto/32 :goto_1

    nop

    :goto_49
    iput-object v5, p0, Landroid/app/assist/AssistStructure$ViewNode;->mText:Landroid/app/assist/AssistStructure$ViewNodeText;

    :goto_4a
    goto/32 :goto_5f

    nop

    :goto_4b
    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    goto/32 :goto_87

    nop

    :goto_4c
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    goto/32 :goto_b3

    nop

    :goto_4d
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    goto/32 :goto_b8

    nop

    :goto_4e
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v5

    goto/32 :goto_93

    nop

    :goto_4f
    iput-object v6, p0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillId:Landroid/view/autofill/AutofillId;

    :goto_50
    goto/32 :goto_58

    nop

    :goto_51
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    goto/32 :goto_a3

    nop

    :goto_52
    iput-object v5, p0, Landroid/app/assist/AssistStructure$ViewNode;->mHtmlInfo:Landroid/view/ViewStructure$HtmlInfo;

    :goto_53
    goto/32 :goto_90

    nop

    :goto_54
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    goto/32 :goto_dc

    nop

    :goto_55
    iput v5, p0, Landroid/app/assist/AssistStructure$ViewNode;->mY:I

    goto/32 :goto_51

    nop

    :goto_56
    iget-object v5, p0, Landroid/app/assist/AssistStructure$ViewNode;->mMatrix:Landroid/graphics/Matrix;

    goto/32 :goto_38

    nop

    :goto_57
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v2

    goto/32 :goto_6f

    nop

    :goto_58
    and-int/lit16 v6, v1, 0x800

    goto/32 :goto_2e

    nop

    :goto_59
    and-int/lit8 v5, v1, 0x20

    goto/32 :goto_de

    nop

    :goto_5a
    const/high16 v2, 0x400000

    goto/32 :goto_a5

    nop

    :goto_5b
    if-nez v1, :cond_b

    goto/32 :goto_1a

    :cond_b
    goto/32 :goto_28

    nop

    :goto_5c
    invoke-direct {v6, v5}, Landroid/view/autofill/AutofillId;-><init>(I)V

    goto/32 :goto_4f

    nop

    :goto_5d
    if-nez v5, :cond_c

    goto/32 :goto_1a

    :cond_c
    goto/32 :goto_7d

    nop

    :goto_5e
    invoke-virtual {p1}, Landroid/os/Parcel;->readStringArray()[Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_e5

    nop

    :goto_5f
    const/high16 v2, 0x40000

    goto/32 :goto_69

    nop

    :goto_60
    if-eqz v6, :cond_d

    goto/32 :goto_2a

    :cond_d
    goto/32 :goto_29

    nop

    :goto_61
    iput v2, p0, Landroid/app/assist/AssistStructure$ViewNode;->mId:I

    goto/32 :goto_72

    nop

    :goto_62
    return-void

    :goto_63
    and-int/lit16 v6, v5, 0x7fff

    goto/32 :goto_a8

    nop

    :goto_64
    if-nez v5, :cond_e

    goto/32 :goto_4a

    :cond_e
    goto/32 :goto_8

    nop

    :goto_65
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_9f

    nop

    :goto_66
    if-nez v5, :cond_f

    goto/32 :goto_dd

    :cond_f
    goto/32 :goto_e2

    nop

    :goto_67
    and-int/lit16 v6, v6, 0x7fff

    goto/32 :goto_2f

    nop

    :goto_68
    and-int/lit8 v5, v1, 0x4

    goto/32 :goto_35

    nop

    :goto_69
    and-int/2addr v2, v0

    goto/32 :goto_20

    nop

    :goto_6a
    check-cast v5, Landroid/view/ViewStructure$HtmlInfo;

    goto/32 :goto_52

    nop

    :goto_6b
    if-nez v5, :cond_10

    goto/32 :goto_af

    :cond_10
    goto/32 :goto_d8

    nop

    :goto_6c
    shr-int/lit8 v6, v5, 0x10

    goto/32 :goto_67

    nop

    :goto_6d
    and-int/2addr v2, v0

    goto/32 :goto_c6

    nop

    :goto_6e
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    goto/32 :goto_21

    nop

    :goto_6f
    iput-object v2, p0, Landroid/app/assist/AssistStructure$ViewNode;->mExtras:Landroid/os/Bundle;

    :goto_70
    goto/32 :goto_62

    nop

    :goto_71
    const/high16 v5, 0x40000000    # 2.0f

    goto/32 :goto_83

    nop

    :goto_72
    const/4 v3, -0x1

    goto/32 :goto_45

    nop

    :goto_73
    and-int/2addr v5, v0

    goto/32 :goto_43

    nop

    :goto_74
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_d0

    nop

    :goto_75
    invoke-virtual {p1, v3, v5}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    goto/32 :goto_e1

    nop

    :goto_76
    goto/16 :goto_bb

    :goto_77
    goto/32 :goto_ba

    nop

    :goto_78
    and-int/lit8 v5, v1, 0x40

    goto/32 :goto_a6

    nop

    :goto_79
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v5

    goto/32 :goto_11

    nop

    :goto_7a
    if-nez v6, :cond_11

    goto/32 :goto_aa

    :cond_11
    goto/32 :goto_b2

    nop

    :goto_7b
    if-nez v5, :cond_12

    goto/32 :goto_94

    :cond_12
    goto/32 :goto_4e

    nop

    :goto_7c
    and-int/lit16 v6, v5, 0x7fff

    goto/32 :goto_3

    nop

    :goto_7d
    invoke-static {p1, p2}, Landroid/app/assist/AssistStructure$ViewNode;->readString(Landroid/os/Parcel;Landroid/os/PooledStringReader;)Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_19

    nop

    :goto_7e
    invoke-virtual {p1, p3}, Landroid/os/Parcel;->readFloatArray([F)V

    goto/32 :goto_56

    nop

    :goto_7f
    and-int/lit8 v5, v1, 0x8

    goto/32 :goto_c7

    nop

    :goto_80
    if-nez v5, :cond_13

    goto/32 :goto_c3

    :cond_13
    goto/32 :goto_4c

    nop

    :goto_81
    const/high16 v2, -0x80000000

    goto/32 :goto_3d

    nop

    :goto_82
    check-cast v5, Ljava/lang/CharSequence;

    goto/32 :goto_a1

    nop

    :goto_83
    and-int/2addr v5, v0

    goto/32 :goto_e4

    nop

    :goto_84
    and-int/2addr v2, v0

    goto/32 :goto_5

    nop

    :goto_85
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    goto/32 :goto_46

    nop

    :goto_86
    const/high16 v5, 0x20000000

    goto/32 :goto_8f

    nop

    :goto_87
    iput-object v5, p0, Landroid/app/assist/AssistStructure$ViewNode;->mMatrix:Landroid/graphics/Matrix;

    goto/32 :goto_14

    nop

    :goto_88
    move v5, v4

    goto/32 :goto_76

    nop

    :goto_89
    and-int/2addr v6, v0

    goto/32 :goto_60

    nop

    :goto_8a
    if-nez v5, :cond_14

    goto/32 :goto_a2

    :cond_14
    goto/32 :goto_ac

    nop

    :goto_8b
    and-int/2addr v5, v0

    goto/32 :goto_66

    nop

    :goto_8c
    const/high16 v2, 0x200000

    goto/32 :goto_84

    nop

    :goto_8d
    invoke-static {p1, p2}, Landroid/app/assist/AssistStructure$ViewNode;->readString(Landroid/os/Parcel;Landroid/os/PooledStringReader;)Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_b7

    nop

    :goto_8e
    if-eq v5, v4, :cond_15

    goto/32 :goto_77

    :cond_15
    goto/32 :goto_88

    nop

    :goto_8f
    and-int/2addr v5, v0

    goto/32 :goto_7b

    nop

    :goto_90
    and-int/lit16 v5, v1, 0x100

    goto/32 :goto_6b

    nop

    :goto_91
    iput-object v2, p0, Landroid/app/assist/AssistStructure$ViewNode;->mIdPackage:Ljava/lang/String;

    :goto_92
    goto/32 :goto_33

    nop

    :goto_93
    iput v5, p0, Landroid/app/assist/AssistStructure$ViewNode;->mAlpha:F

    :goto_94
    goto/32 :goto_41

    nop

    :goto_95
    invoke-virtual {p1}, Landroid/os/Parcel;->readCharSequenceArray()[Ljava/lang/CharSequence;

    move-result-object v5

    goto/32 :goto_c4

    nop

    :goto_96
    invoke-virtual {p1, v3, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_34

    nop

    :goto_97
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    goto/32 :goto_7c

    nop

    :goto_98
    if-nez v2, :cond_16

    goto/32 :goto_a0

    :cond_16
    goto/32 :goto_65

    nop

    :goto_99
    shr-int/lit8 v6, v5, 0x10

    goto/32 :goto_9

    nop

    :goto_9a
    and-int/lit16 v5, v1, 0x80

    goto/32 :goto_9d

    nop

    :goto_9b
    const/high16 v5, 0x4000000

    goto/32 :goto_73

    nop

    :goto_9c
    const/16 v5, 0x9

    goto/32 :goto_c

    nop

    :goto_9d
    if-nez v5, :cond_17

    goto/32 :goto_d7

    :cond_17
    goto/32 :goto_27

    nop

    :goto_9e
    if-nez v5, :cond_18

    goto/32 :goto_2c

    :cond_18
    goto/32 :goto_e

    nop

    :goto_9f
    iput-object v2, p0, Landroid/app/assist/AssistStructure$ViewNode;->mWebDomain:Ljava/lang/String;

    :goto_a0
    goto/32 :goto_2

    nop

    :goto_a1
    iput-object v5, p0, Landroid/app/assist/AssistStructure$ViewNode;->mContentDescription:Ljava/lang/CharSequence;

    :goto_a2
    goto/32 :goto_10

    nop

    :goto_a3
    iput v5, p0, Landroid/app/assist/AssistStructure$ViewNode;->mWidth:I

    goto/32 :goto_6e

    nop

    :goto_a4
    and-int/lit16 v5, v1, 0x400

    goto/32 :goto_e3

    nop

    :goto_a5
    and-int/2addr v2, v0

    goto/32 :goto_c0

    nop

    :goto_a6
    if-nez v5, :cond_19

    goto/32 :goto_53

    :cond_19
    goto/32 :goto_c8

    nop

    :goto_a7
    const/high16 v6, 0x800000

    goto/32 :goto_89

    nop

    :goto_a8
    iput v6, p0, Landroid/app/assist/AssistStructure$ViewNode;->mWidth:I

    goto/32 :goto_6c

    nop

    :goto_a9
    goto/16 :goto_50

    :goto_aa
    goto/32 :goto_ad

    nop

    :goto_ab
    and-int/lit8 v5, v1, 0x10

    goto/32 :goto_d2

    nop

    :goto_ac
    sget-object v5, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    goto/32 :goto_b4

    nop

    :goto_ad
    new-instance v6, Landroid/view/autofill/AutofillId;

    goto/32 :goto_5c

    nop

    :goto_ae
    iput v5, p0, Landroid/app/assist/AssistStructure$ViewNode;->mMinEms:I

    :goto_af
    goto/32 :goto_6

    nop

    :goto_b0
    new-instance v5, Landroid/graphics/Matrix;

    goto/32 :goto_4b

    nop

    :goto_b1
    iput-boolean v5, p0, Landroid/app/assist/AssistStructure$ViewNode;->mSanitized:Z

    goto/32 :goto_db

    nop

    :goto_b2
    new-instance v6, Landroid/view/autofill/AutofillId;

    goto/32 :goto_25

    nop

    :goto_b3
    and-int/lit8 v6, v1, 0x2

    goto/32 :goto_7a

    nop

    :goto_b4
    invoke-interface {v5, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    goto/32 :goto_82

    nop

    :goto_b5
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    goto/32 :goto_61

    nop

    :goto_b6
    and-int/lit16 v5, v1, 0x1000

    goto/32 :goto_5d

    nop

    :goto_b7
    iput-object v2, p0, Landroid/app/assist/AssistStructure$ViewNode;->mIdType:Ljava/lang/String;

    goto/32 :goto_d9

    nop

    :goto_b8
    iput v2, p0, Landroid/app/assist/AssistStructure$ViewNode;->mInputType:I

    :goto_b9
    goto/32 :goto_a

    nop

    :goto_ba
    move v5, v2

    :goto_bb
    goto/32 :goto_b1

    nop

    :goto_bc
    and-int/2addr v2, v0

    goto/32 :goto_98

    nop

    :goto_bd
    iput-object v2, p0, Landroid/app/assist/AssistStructure$ViewNode;->mIdEntry:Ljava/lang/String;

    goto/32 :goto_f

    nop

    :goto_be
    goto/16 :goto_30

    :goto_bf
    goto/32 :goto_97

    nop

    :goto_c0
    if-nez v2, :cond_1a

    goto/32 :goto_70

    :cond_1a
    goto/32 :goto_57

    nop

    :goto_c1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    goto/32 :goto_cc

    nop

    :goto_c2
    invoke-virtual {v6, v7}, Landroid/view/autofill/AutofillId;->setSessionId(I)V

    :goto_c3
    goto/32 :goto_7f

    nop

    :goto_c4
    iput-object v5, p0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillOptions:[Ljava/lang/CharSequence;

    :goto_c5
    goto/32 :goto_78

    nop

    :goto_c6
    if-nez v2, :cond_1b

    goto/32 :goto_17

    :cond_1b
    goto/32 :goto_1e

    nop

    :goto_c7
    if-nez v5, :cond_1c

    goto/32 :goto_47

    :cond_1c
    goto/32 :goto_85

    nop

    :goto_c8
    const-class v5, Landroid/view/ViewStructure$HtmlInfo;

    goto/32 :goto_ca

    nop

    :goto_c9
    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mFlags:I

    goto/32 :goto_44

    nop

    :goto_ca
    invoke-virtual {p1, v3, v5}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    goto/32 :goto_6a

    nop

    :goto_cb
    and-int/2addr v2, v0

    goto/32 :goto_1c

    nop

    :goto_cc
    iput v5, p0, Landroid/app/assist/AssistStructure$ViewNode;->mMaxLength:I

    :goto_cd
    goto/32 :goto_9a

    nop

    :goto_ce
    invoke-virtual {p1}, Landroid/os/Parcel;->readStringArray()[Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_31

    nop

    :goto_cf
    iput v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mFlags:I

    goto/32 :goto_c9

    nop

    :goto_d0
    iput-object v2, p0, Landroid/app/assist/AssistStructure$ViewNode;->mWebScheme:Ljava/lang/String;

    :goto_d1
    goto/32 :goto_d4

    nop

    :goto_d2
    if-nez v5, :cond_1d

    goto/32 :goto_e6

    :cond_1d
    goto/32 :goto_5e

    nop

    :goto_d3
    const/4 v4, 0x1

    goto/32 :goto_5b

    nop

    :goto_d4
    const/high16 v2, 0x80000

    goto/32 :goto_bc

    nop

    :goto_d5
    and-int/lit8 v5, v1, 0x1

    goto/32 :goto_80

    nop

    :goto_d6
    iput-object v5, p0, Landroid/app/assist/AssistStructure$ViewNode;->mTextIdEntry:Ljava/lang/String;

    :goto_d7
    goto/32 :goto_b6

    nop

    :goto_d8
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    goto/32 :goto_ae

    nop

    :goto_d9
    invoke-static {p1, p2}, Landroid/app/assist/AssistStructure$ViewNode;->readString(Landroid/os/Parcel;Landroid/os/PooledStringReader;)Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_91

    nop

    :goto_da
    iput v5, p0, Landroid/app/assist/AssistStructure$ViewNode;->mImportantForAutofill:I

    goto/32 :goto_d5

    nop

    :goto_db
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    goto/32 :goto_da

    nop

    :goto_dc
    iput v5, p0, Landroid/app/assist/AssistStructure$ViewNode;->mScrollY:I

    :goto_dd
    goto/32 :goto_71

    nop

    :goto_de
    if-nez v5, :cond_1e

    goto/32 :goto_c5

    :cond_1e
    goto/32 :goto_95

    nop

    :goto_df
    invoke-static {p1, p2}, Landroid/app/assist/AssistStructure$ViewNode;->readString(Landroid/os/Parcel;Landroid/os/PooledStringReader;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_e0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    goto/32 :goto_63

    nop

    :goto_e1
    check-cast v5, Landroid/view/autofill/AutofillValue;

    goto/32 :goto_22

    nop

    :goto_e2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    goto/32 :goto_2d

    nop

    :goto_e3
    if-nez v5, :cond_1f

    goto/32 :goto_cd

    :cond_1f
    goto/32 :goto_c1

    nop

    :goto_e4
    if-nez v5, :cond_20

    goto/32 :goto_39

    :cond_20
    goto/32 :goto_b0

    nop

    :goto_e5
    iput-object v5, p0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillHints:[Ljava/lang/String;

    :goto_e6
    goto/32 :goto_68

    nop
.end method

.method public isAccessibilityFocused()Z
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mFlags:I

    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isActivated()Z
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mFlags:I

    and-int/lit16 v0, v0, 0x2000

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isAssistBlocked()Z
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mFlags:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isCheckable()Z
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mFlags:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isChecked()Z
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mFlags:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isClickable()Z
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mFlags:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isContextClickable()Z
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mFlags:I

    and-int/lit16 v0, v0, 0x4000

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isEnabled()Z
    .locals 2

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mFlags:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public isFocusable()Z
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mFlags:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isFocused()Z
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mFlags:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isLongClickable()Z
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mFlags:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isOpaque()Z
    .locals 2

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mFlags:I

    const v1, 0x8000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSanitized()Z
    .locals 1

    iget-boolean v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mSanitized:Z

    return v0
.end method

.method public isSelected()Z
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mFlags:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setAutofillOverlay(Landroid/app/assist/AssistStructure$AutofillOverlay;)V
    .locals 0

    iput-object p1, p0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillOverlay:Landroid/app/assist/AssistStructure$AutofillOverlay;

    return-void
.end method

.method public setWebDomain(Ljava/lang/String;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v1, "AssistStructure"

    const-string v2, "Failed to parse web domain"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/app/assist/AssistStructure$ViewNode;->mWebScheme:Ljava/lang/String;

    if-nez v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :cond_2
    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/app/assist/AssistStructure$ViewNode;->mWebDomain:Ljava/lang/String;

    return-void
.end method

.method public updateAutofillValue(Landroid/view/autofill/AutofillValue;)V
    .locals 2

    iput-object p1, p0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillValue:Landroid/view/autofill/AutofillValue;

    invoke-virtual {p1}, Landroid/view/autofill/AutofillValue;->isText()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mText:Landroid/app/assist/AssistStructure$ViewNodeText;

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/assist/AssistStructure$ViewNodeText;

    invoke-direct {v0}, Landroid/app/assist/AssistStructure$ViewNodeText;-><init>()V

    iput-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mText:Landroid/app/assist/AssistStructure$ViewNodeText;

    :cond_0
    iget-object v0, p0, Landroid/app/assist/AssistStructure$ViewNode;->mText:Landroid/app/assist/AssistStructure$ViewNodeText;

    invoke-virtual {p1}, Landroid/view/autofill/AutofillValue;->getTextValue()Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Landroid/app/assist/AssistStructure$ViewNodeText;->mText:Ljava/lang/CharSequence;

    :cond_1
    return-void
.end method

.method writeSelfToParcel(Landroid/os/Parcel;Landroid/os/PooledStringWriter;Z[FZ)I
    .locals 25

    goto/32 :goto_bc

    nop

    :goto_0
    if-eqz v9, :cond_0

    goto/32 :goto_34

    :cond_0
    goto/32 :goto_d8

    nop

    :goto_1
    move-object/from16 v1, p1

    goto/32 :goto_79

    nop

    :goto_2
    invoke-virtual {v1, v7}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    :goto_3
    goto/32 :goto_f8

    nop

    :goto_4
    iget-object v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mText:Landroid/app/assist/AssistStructure$ViewNodeText;

    goto/32 :goto_67

    nop

    :goto_5
    invoke-static {v1, v2, v6}, Landroid/app/assist/AssistStructure$ViewNode;->writeString(Landroid/os/Parcel;Landroid/os/PooledStringWriter;Ljava/lang/String;)V

    goto/32 :goto_15d

    nop

    :goto_6
    const/4 v7, 0x0

    :goto_7
    goto/32 :goto_10a

    nop

    :goto_8
    new-array v7, v7, [F

    goto/32 :goto_137

    nop

    :goto_9
    iget-object v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mExtras:Landroid/os/Bundle;

    goto/32 :goto_190

    nop

    :goto_a
    or-int v4, v4, v21

    :goto_b
    goto/32 :goto_9b

    nop

    :goto_c
    or-int/lit8 v5, v5, 0x4

    :goto_d
    goto/32 :goto_9a

    nop

    :goto_e
    if-nez v6, :cond_1

    goto/32 :goto_98

    :cond_1
    goto/32 :goto_97

    nop

    :goto_f
    or-int/lit16 v5, v5, 0x80

    :goto_10
    goto/32 :goto_162

    nop

    :goto_11
    iget-object v8, v0, Landroid/app/assist/AssistStructure$ViewNode;->mWebDomain:Ljava/lang/String;

    goto/32 :goto_110

    nop

    :goto_12
    if-ne v6, v8, :cond_2

    goto/32 :goto_155

    :cond_2
    goto/32 :goto_154

    nop

    :goto_13
    and-int/lit16 v7, v5, 0x80

    goto/32 :goto_57

    nop

    :goto_14
    or-int/lit8 v6, v6, 0x20

    goto/32 :goto_62

    nop

    :goto_15
    if-nez p5, :cond_3

    goto/32 :goto_e4

    :cond_3
    goto/32 :goto_15f

    nop

    :goto_16
    if-nez v8, :cond_4

    goto/32 :goto_d5

    :cond_4
    goto/32 :goto_f0

    nop

    :goto_17
    or-int v4, v4, v19

    :goto_18
    goto/32 :goto_100

    nop

    :goto_19
    if-nez v6, :cond_5

    goto/32 :goto_39

    :cond_5
    goto/32 :goto_38

    nop

    :goto_1a
    move v6, v11

    :goto_1b
    goto/32 :goto_194

    nop

    :goto_1c
    const/high16 v21, 0x80000

    goto/32 :goto_37

    nop

    :goto_1d
    const/16 v7, 0x9

    goto/32 :goto_8

    nop

    :goto_1e
    if-nez v8, :cond_6

    goto/32 :goto_59

    :cond_6
    goto/32 :goto_c5

    nop

    :goto_1f
    if-nez v7, :cond_7

    goto/32 :goto_14d

    :cond_7
    goto/32 :goto_12c

    nop

    :goto_20
    invoke-virtual {v1, v7}, Landroid/os/Parcel;->writeInt(I)V

    :goto_21
    goto/32 :goto_c0

    nop

    :goto_22
    iget-object v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mHintIdEntry:Ljava/lang/String;

    goto/32 :goto_188

    nop

    :goto_23
    or-int v4, v4, v17

    :goto_24
    goto/32 :goto_45

    nop

    :goto_25
    goto/16 :goto_7

    :goto_26
    goto/32 :goto_169

    nop

    :goto_27
    const/high16 v18, 0x1000000

    goto/32 :goto_182

    nop

    :goto_28
    if-eqz v6, :cond_8

    goto/32 :goto_a6

    :cond_8
    goto/32 :goto_4f

    nop

    :goto_29
    iget-object v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mIdEntry:Ljava/lang/String;

    goto/32 :goto_dc

    nop

    :goto_2a
    if-nez v8, :cond_9

    goto/32 :goto_168

    :cond_9
    goto/32 :goto_17b

    nop

    :goto_2b
    const v5, 0xffff

    goto/32 :goto_139

    nop

    :goto_2c
    goto/16 :goto_f2

    :goto_2d
    goto/32 :goto_e0

    nop

    :goto_2e
    and-int v8, v4, v15

    goto/32 :goto_16

    nop

    :goto_2f
    invoke-static {v8, v1, v11}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    :goto_30
    goto/32 :goto_107

    nop

    :goto_31
    invoke-virtual {v1, v7}, Landroid/os/Parcel;->writeInt(I)V

    :goto_32
    goto/32 :goto_b7

    nop

    :goto_33
    goto :goto_4a

    :goto_34
    goto/32 :goto_49

    nop

    :goto_35
    iget-object v8, v0, Landroid/app/assist/AssistStructure$ViewNode;->mContentDescription:Ljava/lang/CharSequence;

    goto/32 :goto_2f

    nop

    :goto_36
    if-eqz p4, :cond_a

    goto/32 :goto_138

    :cond_a
    goto/32 :goto_1d

    nop

    :goto_37
    if-nez v6, :cond_b

    goto/32 :goto_b

    :cond_b
    goto/32 :goto_a

    nop

    :goto_38
    or-int/lit16 v5, v5, 0x800

    :goto_39
    goto/32 :goto_198

    nop

    :goto_3a
    const/high16 v12, 0x8000000

    goto/32 :goto_199

    nop

    :goto_3b
    iget-object v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillId:Landroid/view/autofill/AutofillId;

    goto/32 :goto_61

    nop

    :goto_3c
    if-nez v5, :cond_c

    goto/32 :goto_152

    :cond_c
    goto/32 :goto_102

    nop

    :goto_3d
    iget-boolean v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mSanitized:Z

    goto/32 :goto_12d

    nop

    :goto_3e
    iget v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mScrollY:I

    goto/32 :goto_fe

    nop

    :goto_3f
    if-nez v7, :cond_d

    goto/32 :goto_b6

    :cond_d
    goto/32 :goto_f6

    nop

    :goto_40
    iget-object v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mIdEntry:Ljava/lang/String;

    goto/32 :goto_179

    nop

    :goto_41
    or-int v4, v4, v16

    :goto_42
    goto/32 :goto_4

    nop

    :goto_43
    const/4 v7, 0x1

    :goto_44
    goto/32 :goto_12e

    nop

    :goto_45
    iget v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mInputType:I

    goto/32 :goto_b2

    nop

    :goto_46
    invoke-virtual {v1, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_ba

    nop

    :goto_47
    and-int/lit16 v7, v5, 0x200

    goto/32 :goto_3f

    nop

    :goto_48
    shl-int/lit8 v7, v7, 0x10

    goto/32 :goto_14b

    nop

    :goto_49
    move v10, v11

    :goto_4a
    goto/32 :goto_54

    nop

    :goto_4b
    iget v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mMaxLength:I

    goto/32 :goto_b8

    nop

    :goto_4c
    and-int/lit16 v7, v5, 0x400

    goto/32 :goto_82

    nop

    :goto_4d
    iget-object v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillId:Landroid/view/autofill/AutofillId;

    goto/32 :goto_cd

    nop

    :goto_4e
    if-eqz v7, :cond_e

    goto/32 :goto_f2

    :cond_e
    goto/32 :goto_74

    nop

    :goto_4f
    iget v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mY:I

    goto/32 :goto_d9

    nop

    :goto_50
    iget-object v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillHints:[Ljava/lang/String;

    goto/32 :goto_2

    nop

    :goto_51
    or-int v4, v4, v18

    goto/32 :goto_13c

    nop

    :goto_52
    const/4 v14, 0x0

    goto/32 :goto_aa

    nop

    :goto_53
    invoke-virtual {v1, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_161

    nop

    :goto_54
    invoke-virtual {v8, v1, v10, v3}, Landroid/app/assist/AssistStructure$ViewNodeText;->writeToParcel(Landroid/os/Parcel;ZZ)V

    :goto_55
    goto/32 :goto_b3

    nop

    :goto_56
    if-nez v8, :cond_f

    goto/32 :goto_15b

    :cond_f
    goto/32 :goto_66

    nop

    :goto_57
    if-nez v7, :cond_10

    goto/32 :goto_113

    :cond_10
    goto/32 :goto_e7

    nop

    :goto_58
    invoke-virtual {v1, v8}, Landroid/os/Parcel;->writeInt(I)V

    :goto_59
    goto/32 :goto_ad

    nop

    :goto_5a
    iget v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mAlpha:F

    goto/32 :goto_175

    nop

    :goto_5b
    if-nez v7, :cond_11

    goto/32 :goto_fd

    :cond_11
    goto/32 :goto_146

    nop

    :goto_5c
    invoke-virtual {v8, v7}, Landroid/graphics/Matrix;->getValues([F)V

    goto/32 :goto_fa

    nop

    :goto_5d
    const/high16 v15, 0x20000000

    goto/32 :goto_5f

    nop

    :goto_5e
    iget v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mWidth:I

    goto/32 :goto_d7

    nop

    :goto_5f
    if-nez v6, :cond_12

    goto/32 :goto_de

    :cond_12
    goto/32 :goto_dd

    nop

    :goto_60
    if-nez v8, :cond_13

    goto/32 :goto_55

    :cond_13
    goto/32 :goto_114

    nop

    :goto_61
    invoke-virtual {v6}, Landroid/view/autofill/AutofillId;->hasSession()Z

    move-result v6

    goto/32 :goto_19

    nop

    :goto_62
    goto/16 :goto_8b

    :goto_63
    goto/32 :goto_8a

    nop

    :goto_64
    and-int/2addr v7, v4

    goto/32 :goto_a0

    nop

    :goto_65
    const/4 v5, 0x0

    goto/32 :goto_11c

    nop

    :goto_66
    iget-object v8, v0, Landroid/app/assist/AssistStructure$ViewNode;->mWebScheme:Ljava/lang/String;

    goto/32 :goto_15a

    nop

    :goto_67
    const/high16 v17, 0x800000

    goto/32 :goto_27

    nop

    :goto_68
    check-cast v7, Landroid/os/Parcelable;

    goto/32 :goto_84

    nop

    :goto_69
    iget v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mId:I

    goto/32 :goto_160

    nop

    :goto_6a
    invoke-virtual {v1, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_3e

    nop

    :goto_6b
    if-nez v6, :cond_14

    goto/32 :goto_172

    :cond_14
    goto/32 :goto_17c

    nop

    :goto_6c
    and-int/lit16 v12, v12, -0x8000

    goto/32 :goto_109

    nop

    :goto_6d
    if-nez v7, :cond_15

    goto/32 :goto_32

    :cond_15
    goto/32 :goto_f7

    nop

    :goto_6e
    instance-of v6, v6, Landroid/os/Parcelable;

    goto/32 :goto_8d

    nop

    :goto_6f
    shl-int/lit8 v7, v7, 0x10

    goto/32 :goto_108

    nop

    :goto_70
    const/high16 v23, -0x80000000

    goto/32 :goto_135

    nop

    :goto_71
    if-nez v6, :cond_16

    goto/32 :goto_39

    :cond_16
    goto/32 :goto_ec

    nop

    :goto_72
    if-nez v7, :cond_17

    goto/32 :goto_90

    :cond_17
    goto/32 :goto_4d

    nop

    :goto_73
    if-nez v5, :cond_18

    goto/32 :goto_189

    :cond_18
    goto/32 :goto_3d

    nop

    :goto_74
    if-eqz p3, :cond_19

    goto/32 :goto_2d

    :cond_19
    goto/32 :goto_2c

    nop

    :goto_75
    if-nez v6, :cond_1a

    goto/32 :goto_e4

    :cond_1a
    goto/32 :goto_15

    nop

    :goto_76
    const/high16 v9, 0x4000000

    goto/32 :goto_157

    nop

    :goto_77
    or-int/lit8 v5, v5, 0x2

    :goto_78
    goto/32 :goto_3b

    nop

    :goto_79
    move-object/from16 v2, p2

    goto/32 :goto_153

    nop

    :goto_7a
    if-nez v6, :cond_1b

    goto/32 :goto_42

    :cond_1b
    goto/32 :goto_41

    nop

    :goto_7b
    iget-object v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillOverlay:Landroid/app/assist/AssistStructure$AutofillOverlay;

    goto/32 :goto_18d

    nop

    :goto_7c
    and-int/lit8 v7, v5, 0x10

    goto/32 :goto_e8

    nop

    :goto_7d
    invoke-virtual {v1, v7}, Landroid/os/Parcel;->writeInt(I)V

    :goto_7e
    goto/32 :goto_13

    nop

    :goto_7f
    or-int/2addr v4, v12

    :goto_80
    goto/32 :goto_cf

    nop

    :goto_81
    and-int v9, v4, v17

    goto/32 :goto_0

    nop

    :goto_82
    if-nez v7, :cond_1c

    goto/32 :goto_7e

    :cond_1c
    goto/32 :goto_ce

    nop

    :goto_83
    const/high16 v14, 0x10000000

    goto/32 :goto_156

    nop

    :goto_84
    invoke-virtual {v1, v7, v11}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    :goto_85
    goto/32 :goto_174

    nop

    :goto_86
    or-int/lit16 v5, v5, 0x1000

    :goto_87
    goto/32 :goto_183

    nop

    :goto_88
    iget-object v10, v0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillOverlay:Landroid/app/assist/AssistStructure$AutofillOverlay;

    goto/32 :goto_9d

    nop

    :goto_89
    if-nez v6, :cond_1d

    goto/32 :goto_cb

    :cond_1d
    goto/32 :goto_ca

    nop

    :goto_8a
    and-int/lit8 v6, v6, -0x21

    :goto_8b
    goto/32 :goto_46

    nop

    :goto_8c
    if-nez v8, :cond_1e

    goto/32 :goto_a2

    :cond_1e
    goto/32 :goto_145

    nop

    :goto_8d
    if-nez v6, :cond_1f

    goto/32 :goto_11b

    :cond_1f
    goto/32 :goto_11a

    nop

    :goto_8e
    if-nez v7, :cond_20

    goto/32 :goto_189

    :cond_20
    goto/32 :goto_22

    nop

    :goto_8f
    invoke-virtual {v1, v7}, Landroid/os/Parcel;->writeInt(I)V

    :goto_90
    goto/32 :goto_16d

    nop

    :goto_91
    const/high16 v22, 0x10000

    goto/32 :goto_e

    nop

    :goto_92
    iget-object v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mReceiveContentMimeTypes:[Ljava/lang/String;

    goto/32 :goto_70

    nop

    :goto_93
    or-int/2addr v7, v8

    goto/32 :goto_132

    nop

    :goto_94
    iget v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mMinEms:I

    goto/32 :goto_c7

    nop

    :goto_95
    iget-object v7, v7, Landroid/app/assist/AssistStructure$AutofillOverlay;->value:Landroid/view/autofill/AutofillValue;

    goto/32 :goto_164

    nop

    :goto_96
    if-nez v8, :cond_21

    goto/32 :goto_111

    :cond_21
    goto/32 :goto_11

    nop

    :goto_97
    or-int v4, v4, v22

    :goto_98
    goto/32 :goto_92

    nop

    :goto_99
    iget v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mImportantForAutofill:I

    goto/32 :goto_53

    nop

    :goto_9a
    iget v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillType:I

    goto/32 :goto_89

    nop

    :goto_9b
    iget-object v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mLocaleList:Landroid/os/LocaleList;

    goto/32 :goto_91

    nop

    :goto_9c
    iget-object v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mIdType:Ljava/lang/String;

    goto/32 :goto_c4

    nop

    :goto_9d
    if-nez v10, :cond_22

    goto/32 :goto_8b

    :cond_22
    goto/32 :goto_f3

    nop

    :goto_9e
    move-object/from16 v7, p4

    :goto_9f
    goto/32 :goto_158

    nop

    :goto_a0
    if-nez v7, :cond_23

    goto/32 :goto_db

    :cond_23
    goto/32 :goto_69

    nop

    :goto_a1
    invoke-virtual {v1, v8}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    :goto_a2
    goto/32 :goto_c6

    nop

    :goto_a3
    iget-object v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillHints:[Ljava/lang/String;

    goto/32 :goto_106

    nop

    :goto_a4
    const/high16 v20, 0x20000

    goto/32 :goto_197

    nop

    :goto_a5
    if-nez v6, :cond_24

    goto/32 :goto_bf

    :cond_24
    :goto_a6
    goto/32 :goto_be

    nop

    :goto_a7
    if-nez v7, :cond_25

    goto/32 :goto_ff

    :cond_25
    goto/32 :goto_177

    nop

    :goto_a8
    or-int/lit16 v5, v5, 0x100

    :goto_a9
    goto/32 :goto_15c

    nop

    :goto_aa
    cmpl-float v6, v6, v14

    goto/32 :goto_83

    nop

    :goto_ab
    or-int/lit16 v5, v5, 0x200

    :goto_ac
    goto/32 :goto_4b

    nop

    :goto_ad
    and-int v8, v4, v20

    goto/32 :goto_56

    nop

    :goto_ae
    if-eqz v10, :cond_26

    goto/32 :goto_17f

    :cond_26
    goto/32 :goto_17e

    nop

    :goto_af
    if-nez v6, :cond_27

    goto/32 :goto_78

    :cond_27
    goto/32 :goto_77

    nop

    :goto_b0
    invoke-virtual {v1, v8, v11}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    :goto_b1
    goto/32 :goto_18b

    nop

    :goto_b2
    const/high16 v19, 0x40000

    goto/32 :goto_ed

    nop

    :goto_b3
    and-int v8, v4, v19

    goto/32 :goto_1e

    nop

    :goto_b4
    if-nez v6, :cond_28

    goto/32 :goto_16b

    :cond_28
    goto/32 :goto_16a

    nop

    :goto_b5
    invoke-virtual {v1, v7}, Landroid/os/Parcel;->writeInt(I)V

    :goto_b6
    goto/32 :goto_4c

    nop

    :goto_b7
    and-int/lit8 v7, v5, 0x8

    goto/32 :goto_5b

    nop

    :goto_b8
    if-gt v6, v8, :cond_29

    goto/32 :goto_11e

    :cond_29
    goto/32 :goto_11d

    nop

    :goto_b9
    return v4

    :goto_ba
    invoke-virtual {v1, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_64

    nop

    :goto_bb
    and-int v8, v4, v21

    goto/32 :goto_96

    nop

    :goto_bc
    move-object/from16 v0, p0

    goto/32 :goto_1

    nop

    :goto_bd
    iget v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mX:I

    goto/32 :goto_d3

    nop

    :goto_be
    or-int/2addr v4, v9

    :goto_bf
    goto/32 :goto_d0

    nop

    :goto_c0
    and-int v7, v4, v12

    goto/32 :goto_a7

    nop

    :goto_c1
    iget-object v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mWebDomain:Ljava/lang/String;

    goto/32 :goto_1c

    nop

    :goto_c2
    iget v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mY:I

    goto/32 :goto_6f

    nop

    :goto_c3
    and-int v7, v4, v9

    goto/32 :goto_1f

    nop

    :goto_c4
    invoke-static {v1, v2, v7}, Landroid/app/assist/AssistStructure$ViewNode;->writeString(Landroid/os/Parcel;Landroid/os/PooledStringWriter;Ljava/lang/String;)V

    goto/32 :goto_13a

    nop

    :goto_c5
    iget v8, v0, Landroid/app/assist/AssistStructure$ViewNode;->mInputType:I

    goto/32 :goto_58

    nop

    :goto_c6
    and-int v8, v4, v24

    goto/32 :goto_176

    nop

    :goto_c7
    if-gt v6, v8, :cond_2a

    goto/32 :goto_a9

    :cond_2a
    goto/32 :goto_a8

    nop

    :goto_c8
    and-int/lit8 v7, v5, 0x2

    goto/32 :goto_72

    nop

    :goto_c9
    iget-object v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillOptions:[Ljava/lang/CharSequence;

    goto/32 :goto_163

    nop

    :goto_ca
    or-int/lit8 v5, v5, 0x8

    :goto_cb
    goto/32 :goto_a3

    nop

    :goto_cc
    if-ne v7, v8, :cond_2b

    goto/32 :goto_db

    :cond_2b
    goto/32 :goto_40

    nop

    :goto_cd
    invoke-virtual {v7}, Landroid/view/autofill/AutofillId;->getVirtualChildIntId()I

    move-result v7

    goto/32 :goto_8f

    nop

    :goto_ce
    iget v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mMaxLength:I

    goto/32 :goto_7d

    nop

    :goto_cf
    iget-object v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mMatrix:Landroid/graphics/Matrix;

    goto/32 :goto_17d

    nop

    :goto_d0
    iget v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mScrollX:I

    goto/32 :goto_3a

    nop

    :goto_d1
    and-int/lit8 v7, v5, 0x40

    goto/32 :goto_150

    nop

    :goto_d2
    if-nez v3, :cond_2c

    goto/32 :goto_26

    :cond_2c
    goto/32 :goto_15e

    nop

    :goto_d3
    and-int/lit16 v6, v6, -0x8000

    goto/32 :goto_76

    nop

    :goto_d4
    invoke-virtual {v1, v8}, Landroid/os/Parcel;->writeFloat(F)V

    :goto_d5
    goto/32 :goto_f4

    nop

    :goto_d6
    invoke-virtual {v6}, Landroid/view/autofill/AutofillId;->isVirtualInt()Z

    move-result v6

    goto/32 :goto_af

    nop

    :goto_d7
    and-int/lit16 v6, v6, -0x8000

    goto/32 :goto_6b

    nop

    :goto_d8
    const/4 v10, 0x1

    goto/32 :goto_33

    nop

    :goto_d9
    and-int/lit16 v6, v6, -0x8000

    goto/32 :goto_159

    nop

    :goto_da
    invoke-static {v1, v2, v7}, Landroid/app/assist/AssistStructure$ViewNode;->writeString(Landroid/os/Parcel;Landroid/os/PooledStringWriter;Ljava/lang/String;)V

    :goto_db
    goto/32 :goto_73

    nop

    :goto_dc
    if-nez v7, :cond_2d

    goto/32 :goto_db

    :cond_2d
    goto/32 :goto_9c

    nop

    :goto_dd
    or-int/2addr v4, v15

    :goto_de
    goto/32 :goto_125

    nop

    :goto_df
    invoke-virtual {v1, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_c8

    nop

    :goto_e0
    move v7, v11

    goto/32 :goto_f1

    nop

    :goto_e1
    invoke-virtual {v7}, Landroid/view/autofill/AutofillId;->getViewId()I

    move-result v7

    goto/32 :goto_df

    nop

    :goto_e2
    if-nez v10, :cond_2e

    goto/32 :goto_63

    :cond_2e
    goto/32 :goto_14

    nop

    :goto_e3
    or-int/2addr v4, v6

    :goto_e4
    goto/32 :goto_129

    nop

    :goto_e5
    iget-object v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mHtmlInfo:Landroid/view/ViewStructure$HtmlInfo;

    goto/32 :goto_68

    nop

    :goto_e6
    iget-object v8, v0, Landroid/app/assist/AssistStructure$ViewNode;->mLocaleList:Landroid/os/LocaleList;

    goto/32 :goto_b0

    nop

    :goto_e7
    iget-object v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mTextIdEntry:Ljava/lang/String;

    goto/32 :goto_112

    nop

    :goto_e8
    if-nez v7, :cond_2f

    goto/32 :goto_3

    :cond_2f
    goto/32 :goto_50

    nop

    :goto_e9
    or-int v4, v4, v20

    :goto_ea
    goto/32 :goto_c1

    nop

    :goto_eb
    if-nez v7, :cond_30

    goto/32 :goto_119

    :cond_30
    goto/32 :goto_10c

    nop

    :goto_ec
    or-int/lit8 v5, v5, 0x1

    goto/32 :goto_d6

    nop

    :goto_ed
    if-nez v6, :cond_31

    goto/32 :goto_18

    :cond_31
    goto/32 :goto_17

    nop

    :goto_ee
    or-int v4, v4, v23

    :goto_ef
    goto/32 :goto_9

    nop

    :goto_f0
    iget v8, v0, Landroid/app/assist/AssistStructure$ViewNode;->mAlpha:F

    goto/32 :goto_d4

    nop

    :goto_f1
    goto/16 :goto_44

    :goto_f2
    goto/32 :goto_43

    nop

    :goto_f3
    iget-boolean v10, v10, Landroid/app/assist/AssistStructure$AutofillOverlay;->focused:Z

    goto/32 :goto_e2

    nop

    :goto_f4
    and-int v8, v4, v16

    goto/32 :goto_196

    nop

    :goto_f5
    iget v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mScrollY:I

    goto/32 :goto_10d

    nop

    :goto_f6
    iget v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mMaxEms:I

    goto/32 :goto_b5

    nop

    :goto_f7
    iget-object v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillId:Landroid/view/autofill/AutofillId;

    goto/32 :goto_e1

    nop

    :goto_f8
    and-int/lit8 v7, v5, 0x4

    goto/32 :goto_12b

    nop

    :goto_f9
    iget-object v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mTextIdEntry:Ljava/lang/String;

    goto/32 :goto_16e

    nop

    :goto_fa
    invoke-virtual {v1, v7}, Landroid/os/Parcel;->writeFloatArray([F)V

    goto/32 :goto_147

    nop

    :goto_fb
    if-nez v6, :cond_32

    goto/32 :goto_d

    :cond_32
    goto/32 :goto_c

    nop

    :goto_fc
    invoke-virtual {v1, v7}, Landroid/os/Parcel;->writeInt(I)V

    :goto_fd
    goto/32 :goto_7c

    nop

    :goto_fe
    invoke-virtual {v1, v7}, Landroid/os/Parcel;->writeInt(I)V

    :goto_ff
    goto/32 :goto_165

    nop

    :goto_100
    iget-object v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mWebScheme:Ljava/lang/String;

    goto/32 :goto_a4

    nop

    :goto_101
    iget-object v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mChildren:[Landroid/app/assist/AssistStructure$ViewNode;

    goto/32 :goto_75

    nop

    :goto_102
    iget-boolean v10, v0, Landroid/app/assist/AssistStructure$ViewNode;->mSanitized:Z

    goto/32 :goto_ae

    nop

    :goto_103
    and-int v8, v4, v14

    goto/32 :goto_2a

    nop

    :goto_104
    iget-object v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mHtmlInfo:Landroid/view/ViewStructure$HtmlInfo;

    goto/32 :goto_6e

    nop

    :goto_105
    const/high16 v16, 0x2000000

    goto/32 :goto_7a

    nop

    :goto_106
    if-nez v6, :cond_33

    goto/32 :goto_131

    :cond_33
    goto/32 :goto_130

    nop

    :goto_107
    and-int v8, v4, v18

    goto/32 :goto_60

    nop

    :goto_108
    iget v8, v0, Landroid/app/assist/AssistStructure$ViewNode;->mX:I

    goto/32 :goto_93

    nop

    :goto_109
    if-nez v12, :cond_34

    goto/32 :goto_141

    :cond_34
    goto/32 :goto_13b

    nop

    :goto_10a
    invoke-virtual {v1, v7, v11}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    :goto_10b
    goto/32 :goto_136

    nop

    :goto_10c
    iget-object v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillOptions:[Ljava/lang/CharSequence;

    goto/32 :goto_118

    nop

    :goto_10d
    if-nez v6, :cond_35

    goto/32 :goto_80

    :cond_35
    :goto_10e
    goto/32 :goto_7f

    nop

    :goto_10f
    const/high16 v7, 0x200000

    goto/32 :goto_166

    nop

    :goto_110
    invoke-virtual {v1, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    :goto_111
    goto/32 :goto_186

    nop

    :goto_112
    invoke-static {v1, v2, v7}, Landroid/app/assist/AssistStructure$ViewNode;->writeString(Landroid/os/Parcel;Landroid/os/PooledStringWriter;Ljava/lang/String;)V

    :goto_113
    goto/32 :goto_128

    nop

    :goto_114
    iget-object v8, v0, Landroid/app/assist/AssistStructure$ViewNode;->mText:Landroid/app/assist/AssistStructure$ViewNodeText;

    goto/32 :goto_81

    nop

    :goto_115
    or-int/2addr v6, v12

    goto/32 :goto_a5

    nop

    :goto_116
    or-int/2addr v4, v14

    :goto_117
    goto/32 :goto_5a

    nop

    :goto_118
    invoke-virtual {v1, v7}, Landroid/os/Parcel;->writeCharSequenceArray([Ljava/lang/CharSequence;)V

    :goto_119
    goto/32 :goto_d1

    nop

    :goto_11a
    or-int/lit8 v5, v5, 0x40

    :goto_11b
    goto/32 :goto_94

    nop

    :goto_11c
    iget v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mId:I

    goto/32 :goto_10f

    nop

    :goto_11d
    or-int/lit16 v5, v5, 0x400

    :goto_11e
    goto/32 :goto_f9

    nop

    :goto_11f
    or-int/2addr v7, v8

    goto/32 :goto_20

    nop

    :goto_120
    or-int v4, v4, v24

    :goto_121
    goto/32 :goto_101

    nop

    :goto_122
    and-int/lit8 v7, v5, 0x1

    goto/32 :goto_6d

    nop

    :goto_123
    iget-object v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillId:Landroid/view/autofill/AutofillId;

    goto/32 :goto_185

    nop

    :goto_124
    if-nez v6, :cond_36

    goto/32 :goto_121

    :cond_36
    goto/32 :goto_120

    nop

    :goto_125
    iget-object v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mContentDescription:Ljava/lang/CharSequence;

    goto/32 :goto_105

    nop

    :goto_126
    invoke-virtual {v1, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_12f

    nop

    :goto_127
    cmpl-float v6, v6, v15

    goto/32 :goto_5d

    nop

    :goto_128
    and-int/lit16 v7, v5, 0x1000

    goto/32 :goto_8e

    nop

    :goto_129
    iget-object v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillId:Landroid/view/autofill/AutofillId;

    goto/32 :goto_71

    nop

    :goto_12a
    iget v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mElevation:F

    goto/32 :goto_52

    nop

    :goto_12b
    if-nez v7, :cond_37

    goto/32 :goto_10b

    :cond_37
    goto/32 :goto_d2

    nop

    :goto_12c
    iget v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mX:I

    goto/32 :goto_126

    nop

    :goto_12d
    invoke-virtual {v1, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_99

    nop

    :goto_12e
    move v3, v7

    goto/32 :goto_122

    nop

    :goto_12f
    iget v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mY:I

    goto/32 :goto_16c

    nop

    :goto_130
    or-int/lit8 v5, v5, 0x10

    :goto_131
    goto/32 :goto_c9

    nop

    :goto_132
    invoke-virtual {v1, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_144

    nop

    :goto_133
    if-nez v8, :cond_38

    goto/32 :goto_b1

    :cond_38
    goto/32 :goto_e6

    nop

    :goto_134
    if-nez v6, :cond_39

    goto/32 :goto_87

    :cond_39
    goto/32 :goto_86

    nop

    :goto_135
    if-nez v6, :cond_3a

    goto/32 :goto_ef

    :cond_3a
    goto/32 :goto_ee

    nop

    :goto_136
    and-int/lit8 v7, v5, 0x20

    goto/32 :goto_eb

    nop

    :goto_137
    goto/16 :goto_9f

    :goto_138
    goto/32 :goto_9e

    nop

    :goto_139
    and-int/2addr v4, v5

    goto/32 :goto_65

    nop

    :goto_13a
    iget-object v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mIdPackage:Ljava/lang/String;

    goto/32 :goto_da

    nop

    :goto_13b
    const/4 v12, 0x1

    goto/32 :goto_140

    nop

    :goto_13c
    invoke-virtual {v6}, Landroid/app/assist/AssistStructure$ViewNodeText;->isSimple()Z

    move-result v6

    goto/32 :goto_184

    nop

    :goto_13d
    iget v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mWidth:I

    goto/32 :goto_18a

    nop

    :goto_13e
    move-object/from16 v7, p4

    :goto_13f
    goto/32 :goto_103

    nop

    :goto_140
    goto :goto_14f

    :goto_141
    goto/32 :goto_14e

    nop

    :goto_142
    invoke-virtual {v1, v8}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    :goto_143
    goto/32 :goto_b9

    nop

    :goto_144
    iget v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mHeight:I

    goto/32 :goto_48

    nop

    :goto_145
    iget-object v8, v0, Landroid/app/assist/AssistStructure$ViewNode;->mReceiveContentMimeTypes:[Ljava/lang/String;

    goto/32 :goto_a1

    nop

    :goto_146
    iget v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillType:I

    goto/32 :goto_fc

    nop

    :goto_147
    goto :goto_13f

    :goto_148
    goto/32 :goto_13e

    nop

    :goto_149
    iget v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mId:I

    goto/32 :goto_cc

    nop

    :goto_14a
    if-nez v7, :cond_3b

    goto/32 :goto_193

    :cond_3b
    goto/32 :goto_178

    nop

    :goto_14b
    iget v8, v0, Landroid/app/assist/AssistStructure$ViewNode;->mWidth:I

    goto/32 :goto_11f

    nop

    :goto_14c
    goto/16 :goto_21

    :goto_14d
    goto/32 :goto_c2

    nop

    :goto_14e
    move v12, v11

    :goto_14f
    goto/32 :goto_115

    nop

    :goto_150
    if-nez v7, :cond_3c

    goto/32 :goto_85

    :cond_3c
    goto/32 :goto_e5

    nop

    :goto_151
    and-int/lit16 v6, v4, -0x201

    :goto_152
    goto/32 :goto_88

    nop

    :goto_153
    const/4 v3, 0x1

    goto/32 :goto_18e

    nop

    :goto_154
    or-int/2addr v4, v7

    :goto_155
    goto/32 :goto_bd

    nop

    :goto_156
    if-nez v6, :cond_3d

    goto/32 :goto_117

    :cond_3d
    goto/32 :goto_116

    nop

    :goto_157
    const/4 v11, 0x0

    goto/32 :goto_28

    nop

    :goto_158
    iget-object v8, v0, Landroid/app/assist/AssistStructure$ViewNode;->mMatrix:Landroid/graphics/Matrix;

    goto/32 :goto_5c

    nop

    :goto_159
    if-eqz v6, :cond_3e

    goto/32 :goto_a6

    :cond_3e
    goto/32 :goto_5e

    nop

    :goto_15a
    invoke-virtual {v1, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    :goto_15b
    goto/32 :goto_bb

    nop

    :goto_15c
    iget v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mMaxEms:I

    goto/32 :goto_17a

    nop

    :goto_15d
    move v6, v4

    goto/32 :goto_3c

    nop

    :goto_15e
    iget-object v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillValue:Landroid/view/autofill/AutofillValue;

    goto/32 :goto_25

    nop

    :goto_15f
    const/high16 v6, 0x100000

    goto/32 :goto_e3

    nop

    :goto_160
    invoke-virtual {v1, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_149

    nop

    :goto_161
    iget-boolean v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mSanitized:Z

    goto/32 :goto_4e

    nop

    :goto_162
    iget-object v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mHintIdEntry:Ljava/lang/String;

    goto/32 :goto_134

    nop

    :goto_163
    if-nez v6, :cond_3f

    goto/32 :goto_170

    :cond_3f
    goto/32 :goto_16f

    nop

    :goto_164
    if-nez v7, :cond_40

    goto/32 :goto_181

    :cond_40
    goto/32 :goto_7b

    nop

    :goto_165
    and-int v7, v4, v13

    goto/32 :goto_173

    nop

    :goto_166
    const/4 v8, -0x1

    goto/32 :goto_12

    nop

    :goto_167
    invoke-virtual {v1, v8}, Landroid/os/Parcel;->writeFloat(F)V

    :goto_168
    goto/32 :goto_2e

    nop

    :goto_169
    iget-object v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillOverlay:Landroid/app/assist/AssistStructure$AutofillOverlay;

    goto/32 :goto_191

    nop

    :goto_16a
    or-int/2addr v4, v13

    :goto_16b
    goto/32 :goto_12a

    nop

    :goto_16c
    invoke-virtual {v1, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_13d

    nop

    :goto_16d
    and-int/lit16 v7, v5, 0x800

    goto/32 :goto_187

    nop

    :goto_16e
    if-nez v6, :cond_41

    goto/32 :goto_10

    :cond_41
    goto/32 :goto_f

    nop

    :goto_16f
    or-int/lit8 v5, v5, 0x20

    :goto_170
    goto/32 :goto_104

    nop

    :goto_171
    goto/16 :goto_1b

    :goto_172
    goto/32 :goto_1a

    nop

    :goto_173
    if-nez v7, :cond_42

    goto/32 :goto_148

    :cond_42
    goto/32 :goto_36

    nop

    :goto_174
    and-int/lit16 v7, v5, 0x100

    goto/32 :goto_14a

    nop

    :goto_175
    const/high16 v15, 0x3f800000    # 1.0f

    goto/32 :goto_127

    nop

    :goto_176
    if-nez v8, :cond_43

    goto/32 :goto_143

    :cond_43
    goto/32 :goto_18f

    nop

    :goto_177
    iget v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mScrollX:I

    goto/32 :goto_6a

    nop

    :goto_178
    iget v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mMinEms:I

    goto/32 :goto_192

    nop

    :goto_179
    invoke-static {v1, v2, v7}, Landroid/app/assist/AssistStructure$ViewNode;->writeString(Landroid/os/Parcel;Landroid/os/PooledStringWriter;Ljava/lang/String;)V

    goto/32 :goto_29

    nop

    :goto_17a
    if-gt v6, v8, :cond_44

    goto/32 :goto_ac

    :cond_44
    goto/32 :goto_ab

    nop

    :goto_17b
    iget v8, v0, Landroid/app/assist/AssistStructure$ViewNode;->mElevation:F

    goto/32 :goto_167

    nop

    :goto_17c
    const/4 v6, 0x1

    goto/32 :goto_171

    nop

    :goto_17d
    const/high16 v13, 0x40000000    # 2.0f

    goto/32 :goto_b4

    nop

    :goto_17e
    if-eqz p3, :cond_45

    goto/32 :goto_152

    :cond_45
    :goto_17f
    goto/32 :goto_151

    nop

    :goto_180
    goto/16 :goto_7

    :goto_181
    goto/32 :goto_6

    nop

    :goto_182
    if-nez v6, :cond_46

    goto/32 :goto_24

    :cond_46
    goto/32 :goto_51

    nop

    :goto_183
    iget-object v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mClassName:Ljava/lang/String;

    goto/32 :goto_5

    nop

    :goto_184
    if-eqz v6, :cond_47

    goto/32 :goto_24

    :cond_47
    goto/32 :goto_23

    nop

    :goto_185
    invoke-virtual {v7}, Landroid/view/autofill/AutofillId;->getSessionId()I

    move-result v7

    goto/32 :goto_31

    nop

    :goto_186
    and-int v8, v4, v22

    goto/32 :goto_133

    nop

    :goto_187
    if-nez v7, :cond_48

    goto/32 :goto_32

    :cond_48
    goto/32 :goto_123

    nop

    :goto_188
    invoke-static {v1, v2, v7}, Landroid/app/assist/AssistStructure$ViewNode;->writeString(Landroid/os/Parcel;Landroid/os/PooledStringWriter;Ljava/lang/String;)V

    :goto_189
    goto/32 :goto_c3

    nop

    :goto_18a
    invoke-virtual {v1, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_195

    nop

    :goto_18b
    and-int v8, v4, v23

    goto/32 :goto_8c

    nop

    :goto_18c
    invoke-virtual {v1, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_14c

    nop

    :goto_18d
    iget-object v7, v7, Landroid/app/assist/AssistStructure$AutofillOverlay;->value:Landroid/view/autofill/AutofillValue;

    goto/32 :goto_180

    nop

    :goto_18e
    iget v4, v0, Landroid/app/assist/AssistStructure$ViewNode;->mFlags:I

    goto/32 :goto_2b

    nop

    :goto_18f
    iget-object v8, v0, Landroid/app/assist/AssistStructure$ViewNode;->mExtras:Landroid/os/Bundle;

    goto/32 :goto_142

    nop

    :goto_190
    const/high16 v24, 0x400000

    goto/32 :goto_124

    nop

    :goto_191
    if-nez v7, :cond_49

    goto/32 :goto_181

    :cond_49
    goto/32 :goto_95

    nop

    :goto_192
    invoke-virtual {v1, v7}, Landroid/os/Parcel;->writeInt(I)V

    :goto_193
    goto/32 :goto_47

    nop

    :goto_194
    iget v12, v0, Landroid/app/assist/AssistStructure$ViewNode;->mHeight:I

    goto/32 :goto_6c

    nop

    :goto_195
    iget v7, v0, Landroid/app/assist/AssistStructure$ViewNode;->mHeight:I

    goto/32 :goto_18c

    nop

    :goto_196
    if-nez v8, :cond_4a

    goto/32 :goto_30

    :cond_4a
    goto/32 :goto_35

    nop

    :goto_197
    if-nez v6, :cond_4b

    goto/32 :goto_ea

    :cond_4b
    goto/32 :goto_e9

    nop

    :goto_198
    iget-object v6, v0, Landroid/app/assist/AssistStructure$ViewNode;->mAutofillValue:Landroid/view/autofill/AutofillValue;

    goto/32 :goto_fb

    nop

    :goto_199
    if-eqz v6, :cond_4c

    goto/32 :goto_10e

    :cond_4c
    goto/32 :goto_f5

    nop
.end method
