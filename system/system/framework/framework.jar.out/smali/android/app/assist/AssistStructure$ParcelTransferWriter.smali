.class final Landroid/app/assist/AssistStructure$ParcelTransferWriter;
.super Landroid/os/Binder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/assist/AssistStructure;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ParcelTransferWriter"
.end annotation


# instance fields
.field mCurViewStackEntry:Landroid/app/assist/AssistStructure$ViewStackEntry;

.field mCurViewStackPos:I

.field mCurWindow:I

.field mNumWindows:I

.field mNumWrittenViews:I

.field mNumWrittenWindows:I

.field final mSanitizeOnWrite:Z

.field final mTmpMatrix:[F

.field final mViewStack:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/app/assist/AssistStructure$ViewStackEntry;",
            ">;"
        }
    .end annotation
.end field

.field final mWriteStructure:Z


# direct methods
.method constructor <init>(Landroid/app/assist/AssistStructure;Landroid/os/Parcel;)V
    .locals 3

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mViewStack:Ljava/util/ArrayList;

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mTmpMatrix:[F

    invoke-static {p1}, Landroid/app/assist/AssistStructure;->-$$Nest$fgetmSanitizeOnWrite(Landroid/app/assist/AssistStructure;)Z

    move-result v0

    iput-boolean v0, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mSanitizeOnWrite:Z

    invoke-virtual {p1}, Landroid/app/assist/AssistStructure;->waitForReady()Z

    move-result v0

    iput-boolean v0, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mWriteStructure:Z

    invoke-static {p1}, Landroid/app/assist/AssistStructure;->-$$Nest$fgetmFlags(Landroid/app/assist/AssistStructure;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-static {p1}, Landroid/app/assist/AssistStructure;->-$$Nest$fgetmAutofillFlags(Landroid/app/assist/AssistStructure;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-static {p1}, Landroid/app/assist/AssistStructure;->-$$Nest$fgetmAcquisitionStartTime(Landroid/app/assist/AssistStructure;)J

    move-result-wide v1

    invoke-virtual {p2, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    invoke-static {p1}, Landroid/app/assist/AssistStructure;->-$$Nest$fgetmAcquisitionEndTime(Landroid/app/assist/AssistStructure;)J

    move-result-wide v1

    invoke-virtual {p2, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    invoke-static {p1}, Landroid/app/assist/AssistStructure;->-$$Nest$fgetmWindowNodes(Landroid/app/assist/AssistStructure;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iput v1, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mNumWindows:I

    if-eqz v0, :cond_0

    if-lez v1, :cond_0

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->writeInt(I)V

    :goto_0
    return-void
.end method


# virtual methods
.method pushViewStackEntry(Landroid/app/assist/AssistStructure$ViewNode;I)V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mViewStack:Ljava/util/ArrayList;

    goto/32 :goto_7

    nop

    :goto_1
    iput v1, v0, Landroid/app/assist/AssistStructure$ViewStackEntry;->numChildren:I

    goto/32 :goto_12

    nop

    :goto_2
    iget-object v0, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mViewStack:Ljava/util/ArrayList;

    goto/32 :goto_13

    nop

    :goto_3
    check-cast v0, Landroid/app/assist/AssistStructure$ViewStackEntry;

    :goto_4
    goto/32 :goto_a

    nop

    :goto_5
    iget-object v1, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mViewStack:Ljava/util/ArrayList;

    goto/32 :goto_10

    nop

    :goto_6
    new-instance v0, Landroid/app/assist/AssistStructure$ViewStackEntry;

    goto/32 :goto_e

    nop

    :goto_7
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_b

    nop

    :goto_8
    iput-object v0, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mCurViewStackEntry:Landroid/app/assist/AssistStructure$ViewStackEntry;

    goto/32 :goto_9

    nop

    :goto_9
    return-void

    :goto_a
    iput-object p1, v0, Landroid/app/assist/AssistStructure$ViewStackEntry;->node:Landroid/app/assist/AssistStructure$ViewNode;

    goto/32 :goto_f

    nop

    :goto_b
    if-ge p2, v0, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_6

    nop

    :goto_c
    goto :goto_4

    :goto_d
    goto/32 :goto_2

    nop

    :goto_e
    invoke-direct {v0}, Landroid/app/assist/AssistStructure$ViewStackEntry;-><init>()V

    goto/32 :goto_5

    nop

    :goto_f
    invoke-virtual {p1}, Landroid/app/assist/AssistStructure$ViewNode;->getChildCount()I

    move-result v1

    goto/32 :goto_1

    nop

    :goto_10
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_c

    nop

    :goto_11
    iput v1, v0, Landroid/app/assist/AssistStructure$ViewStackEntry;->curChild:I

    goto/32 :goto_8

    nop

    :goto_12
    const/4 v1, 0x0

    goto/32 :goto_11

    nop

    :goto_13
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_3

    nop
.end method

.method writeNextEntryToParcel(Landroid/app/assist/AssistStructure;Landroid/os/Parcel;Landroid/os/PooledStringWriter;)Z
    .locals 5

    goto/32 :goto_25

    nop

    :goto_0
    return v3

    :goto_1
    const/4 v3, 0x0

    goto/32 :goto_7

    nop

    :goto_2
    iput v3, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mCurViewStackPos:I

    goto/32 :goto_18

    nop

    :goto_3
    goto :goto_12

    :goto_4
    goto/32 :goto_2d

    nop

    :goto_5
    add-int/2addr v4, v1

    goto/32 :goto_a

    nop

    :goto_6
    check-cast v2, Landroid/app/assist/AssistStructure$WindowNode;

    goto/32 :goto_3c

    nop

    :goto_7
    if-lt v0, v2, :cond_0

    goto/32 :goto_36

    :cond_0
    goto/32 :goto_2f

    nop

    :goto_8
    iput-object v2, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mCurViewStackEntry:Landroid/app/assist/AssistStructure$ViewStackEntry;

    goto/32 :goto_21

    nop

    :goto_9
    iget v4, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mNumWrittenWindows:I

    goto/32 :goto_5

    nop

    :goto_a
    iput v4, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mNumWrittenWindows:I

    goto/32 :goto_29

    nop

    :goto_b
    check-cast v2, Landroid/app/assist/AssistStructure$ViewStackEntry;

    goto/32 :goto_8

    nop

    :goto_c
    return v1

    :goto_d
    goto/32 :goto_3b

    nop

    :goto_e
    invoke-virtual {p0, v0, p2, p3, v1}, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->writeView(Landroid/app/assist/AssistStructure$ViewNode;Landroid/os/Parcel;Landroid/os/PooledStringWriter;I)V

    goto/32 :goto_26

    nop

    :goto_f
    if-lt v0, v2, :cond_1

    goto/32 :goto_27

    :cond_1
    goto/32 :goto_28

    nop

    :goto_10
    iput v3, v2, Landroid/app/assist/AssistStructure$ViewStackEntry;->curChild:I

    goto/32 :goto_e

    nop

    :goto_11
    if-lt v0, v2, :cond_2

    goto/32 :goto_27

    :cond_2
    :goto_12
    goto/32 :goto_c

    nop

    :goto_13
    add-int/2addr v3, v1

    goto/32 :goto_10

    nop

    :goto_14
    iput-object v2, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mCurViewStackEntry:Landroid/app/assist/AssistStructure$ViewStackEntry;

    goto/32 :goto_3

    nop

    :goto_15
    const/4 v1, 0x1

    goto/32 :goto_37

    nop

    :goto_16
    iget v0, v0, Landroid/app/assist/AssistStructure$ViewStackEntry;->curChild:I

    goto/32 :goto_33

    nop

    :goto_17
    iget v2, v2, Landroid/app/assist/AssistStructure$ViewStackEntry;->numChildren:I

    goto/32 :goto_11

    nop

    :goto_18
    invoke-virtual {p0, v4, p2, p3, v3}, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->writeView(Landroid/app/assist/AssistStructure$ViewNode;Landroid/os/Parcel;Landroid/os/PooledStringWriter;I)V

    goto/32 :goto_35

    nop

    :goto_19
    aget-object v0, v0, v2

    goto/32 :goto_2c

    nop

    :goto_1a
    iget-object v4, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mTmpMatrix:[F

    goto/32 :goto_2a

    nop

    :goto_1b
    iget v2, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mNumWindows:I

    goto/32 :goto_1

    nop

    :goto_1c
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_6

    nop

    :goto_1d
    iget-object v0, v0, Landroid/app/assist/AssistStructure$ViewNode;->mChildren:[Landroid/app/assist/AssistStructure$ViewNode;

    goto/32 :goto_20

    nop

    :goto_1e
    iget v0, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mCurViewStackPos:I

    goto/32 :goto_23

    nop

    :goto_1f
    iget v2, v2, Landroid/app/assist/AssistStructure$ViewStackEntry;->curChild:I

    goto/32 :goto_19

    nop

    :goto_20
    iget-object v2, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mCurViewStackEntry:Landroid/app/assist/AssistStructure$ViewStackEntry;

    goto/32 :goto_1f

    nop

    :goto_21
    iget v0, v2, Landroid/app/assist/AssistStructure$ViewStackEntry;->curChild:I

    goto/32 :goto_38

    nop

    :goto_22
    iput v4, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mCurWindow:I

    goto/32 :goto_3a

    nop

    :goto_23
    sub-int/2addr v0, v1

    goto/32 :goto_2b

    nop

    :goto_24
    iget-object v0, v0, Landroid/app/assist/AssistStructure$ViewStackEntry;->node:Landroid/app/assist/AssistStructure$ViewNode;

    goto/32 :goto_1d

    nop

    :goto_25
    iget-object v0, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mCurViewStackEntry:Landroid/app/assist/AssistStructure$ViewStackEntry;

    goto/32 :goto_15

    nop

    :goto_26
    return v1

    :goto_27
    goto/32 :goto_1e

    nop

    :goto_28
    iget-object v0, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mCurViewStackEntry:Landroid/app/assist/AssistStructure$ViewStackEntry;

    goto/32 :goto_24

    nop

    :goto_29
    iget-object v4, v2, Landroid/app/assist/AssistStructure$WindowNode;->mRoot:Landroid/app/assist/AssistStructure$ViewNode;

    goto/32 :goto_2

    nop

    :goto_2a
    invoke-virtual {v2, p2, p3, v4}, Landroid/app/assist/AssistStructure$WindowNode;->writeSelfToParcel(Landroid/os/Parcel;Landroid/os/PooledStringWriter;[F)V

    goto/32 :goto_9

    nop

    :goto_2b
    iput v0, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mCurViewStackPos:I

    goto/32 :goto_2e

    nop

    :goto_2c
    iget-object v2, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mCurViewStackEntry:Landroid/app/assist/AssistStructure$ViewStackEntry;

    goto/32 :goto_32

    nop

    :goto_2d
    iget-object v2, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mViewStack:Ljava/util/ArrayList;

    goto/32 :goto_31

    nop

    :goto_2e
    if-ltz v0, :cond_3

    goto/32 :goto_4

    :cond_3
    goto/32 :goto_30

    nop

    :goto_2f
    invoke-static {p1}, Landroid/app/assist/AssistStructure;->-$$Nest$fgetmWindowNodes(Landroid/app/assist/AssistStructure;)Ljava/util/ArrayList;

    move-result-object v2

    goto/32 :goto_1c

    nop

    :goto_30
    const/4 v2, 0x0

    goto/32 :goto_14

    nop

    :goto_31
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_b

    nop

    :goto_32
    iget v3, v2, Landroid/app/assist/AssistStructure$ViewStackEntry;->curChild:I

    goto/32 :goto_13

    nop

    :goto_33
    iget-object v2, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mCurViewStackEntry:Landroid/app/assist/AssistStructure$ViewStackEntry;

    goto/32 :goto_3d

    nop

    :goto_34
    add-int/2addr v4, v1

    goto/32 :goto_22

    nop

    :goto_35
    return v1

    :goto_36
    goto/32 :goto_0

    nop

    :goto_37
    if-nez v0, :cond_4

    goto/32 :goto_d

    :cond_4
    goto/32 :goto_16

    nop

    :goto_38
    iget-object v2, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mCurViewStackEntry:Landroid/app/assist/AssistStructure$ViewStackEntry;

    goto/32 :goto_17

    nop

    :goto_39
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_1a

    nop

    :goto_3a
    const v4, 0x11111111

    goto/32 :goto_39

    nop

    :goto_3b
    iget v0, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mCurWindow:I

    goto/32 :goto_1b

    nop

    :goto_3c
    iget v4, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mCurWindow:I

    goto/32 :goto_34

    nop

    :goto_3d
    iget v2, v2, Landroid/app/assist/AssistStructure$ViewStackEntry;->numChildren:I

    goto/32 :goto_f

    nop
.end method

.method writeToParcel(Landroid/app/assist/AssistStructure;Landroid/os/Parcel;)V
    .locals 4

    goto/32 :goto_1a

    nop

    :goto_0
    iget v3, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mNumWrittenViews:I

    goto/32 :goto_19

    nop

    :goto_1
    const-string v3, " bytes, containing "

    goto/32 :goto_21

    nop

    :goto_2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_e

    nop

    :goto_3
    if-nez v1, :cond_0

    goto/32 :goto_20

    :cond_0
    goto/32 :goto_22

    nop

    :goto_4
    invoke-virtual {p0, p1, p2}, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->writeToParcelInner(Landroid/app/assist/AssistStructure;Landroid/os/Parcel;)Z

    move-result v1

    goto/32 :goto_18

    nop

    :goto_5
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_1e

    nop

    :goto_6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_c

    nop

    :goto_7
    iput v1, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mNumWrittenViews:I

    goto/32 :goto_4

    nop

    :goto_8
    const-string v3, "final"

    :goto_9
    goto/32 :goto_17

    nop

    :goto_a
    invoke-static {v3, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_14

    nop

    :goto_b
    sub-int/2addr v3, v0

    goto/32 :goto_10

    nop

    :goto_c
    const-string v3, "Flattened "

    goto/32 :goto_15

    nop

    :goto_d
    const-string v3, "AssistStructure"

    goto/32 :goto_a

    nop

    :goto_e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_d

    nop

    :goto_f
    const-string v3, " windows, "

    goto/32 :goto_13

    nop

    :goto_10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_1

    nop

    :goto_11
    iget v3, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mNumWrittenWindows:I

    goto/32 :goto_1d

    nop

    :goto_12
    const-string v3, " assist data: "

    goto/32 :goto_5

    nop

    :goto_13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_0

    nop

    :goto_14
    return-void

    :goto_15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_3

    nop

    :goto_16
    const/4 v1, 0x0

    goto/32 :goto_1c

    nop

    :goto_17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_12

    nop

    :goto_18
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_1b

    nop

    :goto_1a
    invoke-virtual {p2}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    goto/32 :goto_16

    nop

    :goto_1b
    const-string v3, " views"

    goto/32 :goto_2

    nop

    :goto_1c
    iput v1, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mNumWrittenWindows:I

    goto/32 :goto_7

    nop

    :goto_1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_f

    nop

    :goto_1e
    invoke-virtual {p2}, Landroid/os/Parcel;->dataPosition()I

    move-result v3

    goto/32 :goto_b

    nop

    :goto_1f
    goto/16 :goto_9

    :goto_20
    goto/32 :goto_8

    nop

    :goto_21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_11

    nop

    :goto_22
    const-string/jumbo v3, "partial"

    goto/32 :goto_1f

    nop
.end method

.method writeToParcelInner(Landroid/app/assist/AssistStructure;Landroid/os/Parcel;)Z
    .locals 4

    goto/32 :goto_5

    nop

    :goto_0
    invoke-virtual {p2, p0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    goto/32 :goto_2

    nop

    :goto_1
    if-eqz v0, :cond_0

    goto/32 :goto_f

    :cond_0
    goto/32 :goto_e

    nop

    :goto_2
    invoke-virtual {v0}, Landroid/os/PooledStringWriter;->finish()V

    goto/32 :goto_9

    nop

    :goto_3
    iget-object v2, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mViewStack:Ljava/util/ArrayList;

    goto/32 :goto_10

    nop

    :goto_4
    invoke-virtual {v0}, Landroid/os/PooledStringWriter;->finish()V

    goto/32 :goto_3

    nop

    :goto_5
    iget v0, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mNumWindows:I

    goto/32 :goto_8

    nop

    :goto_6
    invoke-virtual {p0, p1, p2, v0}, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->writeNextEntryToParcel(Landroid/app/assist/AssistStructure;Landroid/os/Parcel;Landroid/os/PooledStringWriter;)Z

    move-result v2

    goto/32 :goto_d

    nop

    :goto_7
    const/high16 v3, 0x10000

    goto/32 :goto_c

    nop

    :goto_8
    const/4 v1, 0x0

    goto/32 :goto_1

    nop

    :goto_9
    const/4 v1, 0x1

    goto/32 :goto_13

    nop

    :goto_a
    invoke-virtual {p2}, Landroid/os/Parcel;->dataSize()I

    move-result v2

    goto/32 :goto_7

    nop

    :goto_b
    new-instance v0, Landroid/os/PooledStringWriter;

    goto/32 :goto_15

    nop

    :goto_c
    if-gt v2, v3, :cond_1

    goto/32 :goto_16

    :cond_1
    goto/32 :goto_11

    nop

    :goto_d
    if-nez v2, :cond_2

    goto/32 :goto_14

    :cond_2
    goto/32 :goto_a

    nop

    :goto_e
    return v1

    :goto_f
    goto/32 :goto_b

    nop

    :goto_10
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    goto/32 :goto_12

    nop

    :goto_11
    invoke-virtual {p2, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_0

    nop

    :goto_12
    return v1

    :goto_13
    return v1

    :goto_14
    goto/32 :goto_4

    nop

    :goto_15
    invoke-direct {v0, p2}, Landroid/os/PooledStringWriter;-><init>(Landroid/os/Parcel;)V

    :goto_16
    goto/32 :goto_6

    nop
.end method

.method writeView(Landroid/app/assist/AssistStructure$ViewNode;Landroid/os/Parcel;Landroid/os/PooledStringWriter;I)V
    .locals 7

    goto/32 :goto_3

    nop

    :goto_0
    const/4 v6, 0x1

    goto/32 :goto_12

    nop

    :goto_1
    iput v1, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mCurViewStackPos:I

    goto/32 :goto_d

    nop

    :goto_2
    invoke-virtual/range {v1 .. v6}, Landroid/app/assist/AssistStructure$ViewNode;->writeSelfToParcel(Landroid/os/Parcel;Landroid/os/PooledStringWriter;Z[FZ)I

    move-result v0

    goto/32 :goto_6

    nop

    :goto_3
    const v0, 0x22222222

    goto/32 :goto_c

    nop

    :goto_4
    iget-object v1, p1, Landroid/app/assist/AssistStructure$ViewNode;->mChildren:[Landroid/app/assist/AssistStructure$ViewNode;

    goto/32 :goto_17

    nop

    :goto_5
    and-int/2addr v1, v0

    goto/32 :goto_7

    nop

    :goto_6
    iget v1, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mNumWrittenViews:I

    goto/32 :goto_10

    nop

    :goto_7
    if-nez v1, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_4

    nop

    :goto_8
    move-object v2, p2

    goto/32 :goto_f

    nop

    :goto_9
    const/high16 v1, 0x100000

    goto/32 :goto_5

    nop

    :goto_a
    return-void

    :goto_b
    iget-object v5, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mTmpMatrix:[F

    goto/32 :goto_0

    nop

    :goto_c
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_16

    nop

    :goto_d
    invoke-virtual {p0, p1, v1}, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->pushViewStackEntry(Landroid/app/assist/AssistStructure$ViewNode;I)V

    :goto_e
    goto/32 :goto_a

    nop

    :goto_f
    move-object v3, p3

    goto/32 :goto_2

    nop

    :goto_10
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_13

    nop

    :goto_11
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_1

    nop

    :goto_12
    move-object v1, p1

    goto/32 :goto_8

    nop

    :goto_13
    iput v1, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mNumWrittenViews:I

    goto/32 :goto_9

    nop

    :goto_14
    invoke-virtual {p2, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_15

    nop

    :goto_15
    iget v1, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mCurViewStackPos:I

    goto/32 :goto_11

    nop

    :goto_16
    iget-boolean v4, p0, Landroid/app/assist/AssistStructure$ParcelTransferWriter;->mSanitizeOnWrite:Z

    goto/32 :goto_b

    nop

    :goto_17
    array-length v1, v1

    goto/32 :goto_14

    nop
.end method
