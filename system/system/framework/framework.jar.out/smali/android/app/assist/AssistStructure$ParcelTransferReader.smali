.class final Landroid/app/assist/AssistStructure$ParcelTransferReader;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/assist/AssistStructure;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "ParcelTransferReader"
.end annotation


# instance fields
.field private final mChannel:Landroid/os/IBinder;

.field private mCurParcel:Landroid/os/Parcel;

.field mNumReadViews:I

.field mNumReadWindows:I

.field mStringReader:Landroid/os/PooledStringReader;

.field final mTmpMatrix:[F

.field private mTransferToken:Landroid/os/IBinder;

.field final synthetic this$0:Landroid/app/assist/AssistStructure;


# direct methods
.method constructor <init>(Landroid/app/assist/AssistStructure;Landroid/os/IBinder;)V
    .locals 1

    iput-object p1, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->this$0:Landroid/app/assist/AssistStructure;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mTmpMatrix:[F

    iput-object p2, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mChannel:Landroid/os/IBinder;

    return-void
.end method

.method private fetchData()V
    .locals 5

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    :try_start_0
    const-string v1, "android.app.AssistStructure"

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mTransferToken:Landroid/os/IBinder;

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    iget-object v1, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mCurParcel:Landroid/os/Parcel;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    :cond_0
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    iput-object v1, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mCurParcel:Landroid/os/Parcel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v2, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mChannel:Landroid/os/IBinder;

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    nop

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    nop

    iput v4, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mNumReadViews:I

    iput v4, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mNumReadWindows:I

    return-void

    :catch_0
    move-exception v1

    :try_start_2
    const-string v2, "AssistStructure"

    const-string v3, "Failure reading AssistStructure data"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failure reading AssistStructure data: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v1
.end method


# virtual methods
.method go()V
    .locals 4

    goto/32 :goto_22

    nop

    :goto_0
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_f

    nop

    :goto_1
    if-lt v1, v0, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_11

    nop

    :goto_2
    if-gtz v0, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_9

    nop

    :goto_3
    return-void

    :goto_4
    invoke-static {v0, v1, v2}, Landroid/app/assist/AssistStructure;->-$$Nest$fputmAcquisitionEndTime(Landroid/app/assist/AssistStructure;J)V

    goto/32 :goto_e

    nop

    :goto_5
    invoke-static {v0, v1}, Landroid/app/assist/AssistStructure;->-$$Nest$fputmFlags(Landroid/app/assist/AssistStructure;I)V

    goto/32 :goto_13

    nop

    :goto_6
    invoke-virtual {v0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    goto/32 :goto_2

    nop

    :goto_7
    iget-object v1, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mCurParcel:Landroid/os/Parcel;

    goto/32 :goto_20

    nop

    :goto_8
    new-instance v3, Landroid/app/assist/AssistStructure$WindowNode;

    goto/32 :goto_21

    nop

    :goto_9
    new-instance v1, Landroid/os/PooledStringReader;

    goto/32 :goto_12

    nop

    :goto_a
    invoke-direct {v1, v2}, Landroid/os/PooledStringReader;-><init>(Landroid/os/Parcel;)V

    goto/32 :goto_1e

    nop

    :goto_b
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    goto/32 :goto_18

    nop

    :goto_c
    goto :goto_1a

    :goto_d
    goto/32 :goto_14

    nop

    :goto_e
    iget-object v0, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mCurParcel:Landroid/os/Parcel;

    goto/32 :goto_6

    nop

    :goto_f
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_c

    nop

    :goto_10
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    goto/32 :goto_25

    nop

    :goto_11
    iget-object v2, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->this$0:Landroid/app/assist/AssistStructure;

    goto/32 :goto_23

    nop

    :goto_12
    iget-object v2, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mCurParcel:Landroid/os/Parcel;

    goto/32 :goto_a

    nop

    :goto_13
    iget-object v0, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->this$0:Landroid/app/assist/AssistStructure;

    goto/32 :goto_1d

    nop

    :goto_14
    iget-object v1, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mCurParcel:Landroid/os/Parcel;

    goto/32 :goto_b

    nop

    :goto_15
    iget-object v0, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->this$0:Landroid/app/assist/AssistStructure;

    goto/32 :goto_7

    nop

    :goto_16
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    goto/32 :goto_1c

    nop

    :goto_17
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    goto/32 :goto_5

    nop

    :goto_18
    const/4 v1, 0x0

    goto/32 :goto_1b

    nop

    :goto_19
    const/4 v1, 0x0

    :goto_1a
    goto/32 :goto_1

    nop

    :goto_1b
    iput-object v1, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mCurParcel:Landroid/os/Parcel;

    goto/32 :goto_3

    nop

    :goto_1c
    invoke-static {v0, v1}, Landroid/app/assist/AssistStructure;->-$$Nest$fputmAutofillFlags(Landroid/app/assist/AssistStructure;I)V

    goto/32 :goto_26

    nop

    :goto_1d
    iget-object v1, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mCurParcel:Landroid/os/Parcel;

    goto/32 :goto_16

    nop

    :goto_1e
    iput-object v1, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mStringReader:Landroid/os/PooledStringReader;

    goto/32 :goto_19

    nop

    :goto_1f
    iget-object v1, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mCurParcel:Landroid/os/Parcel;

    goto/32 :goto_10

    nop

    :goto_20
    invoke-virtual {v1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    goto/32 :goto_4

    nop

    :goto_21
    invoke-direct {v3, p0}, Landroid/app/assist/AssistStructure$WindowNode;-><init>(Landroid/app/assist/AssistStructure$ParcelTransferReader;)V

    goto/32 :goto_0

    nop

    :goto_22
    invoke-direct {p0}, Landroid/app/assist/AssistStructure$ParcelTransferReader;->fetchData()V

    goto/32 :goto_27

    nop

    :goto_23
    invoke-static {v2}, Landroid/app/assist/AssistStructure;->-$$Nest$fgetmWindowNodes(Landroid/app/assist/AssistStructure;)Ljava/util/ArrayList;

    move-result-object v2

    goto/32 :goto_8

    nop

    :goto_24
    iget-object v1, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mCurParcel:Landroid/os/Parcel;

    goto/32 :goto_17

    nop

    :goto_25
    invoke-static {v0, v1, v2}, Landroid/app/assist/AssistStructure;->-$$Nest$fputmAcquisitionStartTime(Landroid/app/assist/AssistStructure;J)V

    goto/32 :goto_15

    nop

    :goto_26
    iget-object v0, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->this$0:Landroid/app/assist/AssistStructure;

    goto/32 :goto_1f

    nop

    :goto_27
    iget-object v0, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->this$0:Landroid/app/assist/AssistStructure;

    goto/32 :goto_24

    nop
.end method

.method readParcel(II)Landroid/os/Parcel;
    .locals 4

    goto/32 :goto_1f

    nop

    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    goto/32 :goto_13

    nop

    :goto_1
    invoke-direct {v1, v2}, Landroid/os/PooledStringReader;-><init>(Landroid/os/Parcel;)V

    goto/32 :goto_1e

    nop

    :goto_2
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_21

    nop

    :goto_3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_14

    nop

    :goto_4
    new-instance v1, Landroid/os/BadParcelableException;

    goto/32 :goto_d

    nop

    :goto_5
    iget-object v1, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mCurParcel:Landroid/os/Parcel;

    goto/32 :goto_17

    nop

    :goto_6
    invoke-direct {p0}, Landroid/app/assist/AssistStructure$ParcelTransferReader;->fetchData()V

    goto/32 :goto_26

    nop

    :goto_7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_f

    nop

    :goto_8
    iget-object v1, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mCurParcel:Landroid/os/Parcel;

    goto/32 :goto_0

    nop

    :goto_9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_a

    nop

    :goto_a
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_7

    nop

    :goto_b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_1b

    nop

    :goto_c
    const-string v3, ", expected token "

    goto/32 :goto_9

    nop

    :goto_d
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_3

    nop

    :goto_e
    invoke-virtual {v0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    goto/32 :goto_20

    nop

    :goto_f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_27

    nop

    :goto_10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_c

    nop

    :goto_11
    return-object v1

    :goto_12
    goto/32 :goto_15

    nop

    :goto_13
    iget-object v1, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mCurParcel:Landroid/os/Parcel;

    goto/32 :goto_11

    nop

    :goto_14
    const-string v3, "Got token "

    goto/32 :goto_b

    nop

    :goto_15
    new-instance v1, Ljava/lang/IllegalStateException;

    goto/32 :goto_1c

    nop

    :goto_16
    iget-object v2, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mCurParcel:Landroid/os/Parcel;

    goto/32 :goto_1

    nop

    :goto_17
    invoke-virtual {v1}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    goto/32 :goto_22

    nop

    :goto_18
    return-object v1

    :goto_19
    goto/32 :goto_4

    nop

    :goto_1a
    if-nez v1, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_6

    nop

    :goto_1b
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_10

    nop

    :goto_1c
    const-string v2, "Reached end of partial data without transfer token"

    goto/32 :goto_2

    nop

    :goto_1d
    if-eq v0, p1, :cond_1

    goto/32 :goto_19

    :cond_1
    goto/32 :goto_23

    nop

    :goto_1e
    iput-object v1, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mStringReader:Landroid/os/PooledStringReader;

    goto/32 :goto_8

    nop

    :goto_1f
    iget-object v0, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mCurParcel:Landroid/os/Parcel;

    goto/32 :goto_e

    nop

    :goto_20
    if-nez v0, :cond_2

    goto/32 :goto_25

    :cond_2
    goto/32 :goto_1d

    nop

    :goto_21
    throw v1

    :goto_22
    iput-object v1, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mTransferToken:Landroid/os/IBinder;

    goto/32 :goto_1a

    nop

    :goto_23
    iget-object v1, p0, Landroid/app/assist/AssistStructure$ParcelTransferReader;->mCurParcel:Landroid/os/Parcel;

    goto/32 :goto_18

    nop

    :goto_24
    throw v1

    :goto_25
    goto/32 :goto_5

    nop

    :goto_26
    new-instance v1, Landroid/os/PooledStringReader;

    goto/32 :goto_16

    nop

    :goto_27
    invoke-direct {v1, v2}, Landroid/os/BadParcelableException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_24

    nop
.end method
