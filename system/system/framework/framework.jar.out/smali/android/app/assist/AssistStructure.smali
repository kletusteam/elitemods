.class public Landroid/app/assist/AssistStructure;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/assist/AssistStructure$HtmlInfoNodeBuilder;,
        Landroid/app/assist/AssistStructure$HtmlInfoNode;,
        Landroid/app/assist/AssistStructure$ViewNodeBuilder;,
        Landroid/app/assist/AssistStructure$AutofillOverlay;,
        Landroid/app/assist/AssistStructure$ViewNodeParcelable;,
        Landroid/app/assist/AssistStructure$ViewNode;,
        Landroid/app/assist/AssistStructure$WindowNode;,
        Landroid/app/assist/AssistStructure$ViewNodeText;,
        Landroid/app/assist/AssistStructure$ParcelTransferReader;,
        Landroid/app/assist/AssistStructure$ParcelTransferWriter;,
        Landroid/app/assist/AssistStructure$ViewStackEntry;,
        Landroid/app/assist/AssistStructure$SendChannel;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Landroid/app/assist/AssistStructure;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEBUG_PARCEL:Z = false

.field private static final DEBUG_PARCEL_CHILDREN:Z = false

.field private static final DEBUG_PARCEL_TREE:Z = false

.field private static final DESCRIPTOR:Ljava/lang/String; = "android.app.AssistStructure"

.field private static final TAG:Ljava/lang/String; = "AssistStructure"

.field private static final TRANSACTION_XFER:I = 0x2

.field private static final VALIDATE_VIEW_TOKEN:I = 0x22222222

.field private static final VALIDATE_WINDOW_TOKEN:I = 0x11111111


# instance fields
.field private mAcquisitionEndTime:J

.field private mAcquisitionStartTime:J

.field private mActivityComponent:Landroid/content/ComponentName;

.field private mAutofillFlags:I

.field private mFlags:I

.field private mHaveData:Z

.field private mIsHomeActivity:Z

.field private final mPendingAsyncChildren:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/app/assist/AssistStructure$ViewNodeBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private mReceiveChannel:Landroid/os/IBinder;

.field private mSanitizeOnWrite:Z

.field private mSendChannel:Landroid/app/assist/AssistStructure$SendChannel;

.field private mTaskId:I

.field private mTmpRect:Landroid/graphics/Rect;

.field private final mWindowNodes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/app/assist/AssistStructure$WindowNode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetmAcquisitionEndTime(Landroid/app/assist/AssistStructure;)J
    .locals 2

    iget-wide v0, p0, Landroid/app/assist/AssistStructure;->mAcquisitionEndTime:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmAcquisitionStartTime(Landroid/app/assist/AssistStructure;)J
    .locals 2

    iget-wide v0, p0, Landroid/app/assist/AssistStructure;->mAcquisitionStartTime:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmAutofillFlags(Landroid/app/assist/AssistStructure;)I
    .locals 0

    iget p0, p0, Landroid/app/assist/AssistStructure;->mAutofillFlags:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmFlags(Landroid/app/assist/AssistStructure;)I
    .locals 0

    iget p0, p0, Landroid/app/assist/AssistStructure;->mFlags:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmPendingAsyncChildren(Landroid/app/assist/AssistStructure;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Landroid/app/assist/AssistStructure;->mPendingAsyncChildren:Ljava/util/ArrayList;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSanitizeOnWrite(Landroid/app/assist/AssistStructure;)Z
    .locals 0

    iget-boolean p0, p0, Landroid/app/assist/AssistStructure;->mSanitizeOnWrite:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmTmpRect(Landroid/app/assist/AssistStructure;)Landroid/graphics/Rect;
    .locals 0

    iget-object p0, p0, Landroid/app/assist/AssistStructure;->mTmpRect:Landroid/graphics/Rect;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmWindowNodes(Landroid/app/assist/AssistStructure;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Landroid/app/assist/AssistStructure;->mWindowNodes:Ljava/util/ArrayList;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmAcquisitionEndTime(Landroid/app/assist/AssistStructure;J)V
    .locals 0

    iput-wide p1, p0, Landroid/app/assist/AssistStructure;->mAcquisitionEndTime:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmAcquisitionStartTime(Landroid/app/assist/AssistStructure;J)V
    .locals 0

    iput-wide p1, p0, Landroid/app/assist/AssistStructure;->mAcquisitionStartTime:J

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmAutofillFlags(Landroid/app/assist/AssistStructure;I)V
    .locals 0

    iput p1, p0, Landroid/app/assist/AssistStructure;->mAutofillFlags:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmFlags(Landroid/app/assist/AssistStructure;I)V
    .locals 0

    iput p1, p0, Landroid/app/assist/AssistStructure;->mFlags:I

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/app/assist/AssistStructure$1;

    invoke-direct {v0}, Landroid/app/assist/AssistStructure$1;-><init>()V

    sput-object v0, Landroid/app/assist/AssistStructure;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/app/assist/AssistStructure;->mWindowNodes:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/app/assist/AssistStructure;->mPendingAsyncChildren:Ljava/util/ArrayList;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/app/assist/AssistStructure;->mTmpRect:Landroid/graphics/Rect;

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/app/assist/AssistStructure;->mSanitizeOnWrite:Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid/app/assist/AssistStructure;->mHaveData:Z

    iput v0, p0, Landroid/app/assist/AssistStructure;->mFlags:I

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;ZI)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/app/assist/AssistStructure;->mWindowNodes:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/app/assist/AssistStructure;->mPendingAsyncChildren:Ljava/util/ArrayList;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/app/assist/AssistStructure;->mTmpRect:Landroid/graphics/Rect;

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/app/assist/AssistStructure;->mSanitizeOnWrite:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/assist/AssistStructure;->mHaveData:Z

    iput p3, p0, Landroid/app/assist/AssistStructure;->mFlags:I

    invoke-static {}, Landroid/view/WindowManagerGlobal;->getInstance()Landroid/view/WindowManagerGlobal;

    move-result-object v0

    invoke-virtual {p1}, Landroid/app/Activity;->getActivityToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/WindowManagerGlobal;->getRootViews(Landroid/os/IBinder;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/ViewRootImpl;

    invoke-virtual {v2}, Landroid/view/ViewRootImpl;->getView()Landroid/view/View;

    move-result-object v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Skipping window with dettached view: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Landroid/view/ViewRootImpl;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "AssistStructure"

    invoke-static {v4, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_0
    iget-object v3, p0, Landroid/app/assist/AssistStructure;->mWindowNodes:Ljava/util/ArrayList;

    new-instance v4, Landroid/app/assist/AssistStructure$WindowNode;

    invoke-direct {v4, p0, v2, p2, p3}, Landroid/app/assist/AssistStructure$WindowNode;-><init>(Landroid/app/assist/AssistStructure;Landroid/view/ViewRootImpl;ZI)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/app/assist/AssistStructure;->mWindowNodes:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/app/assist/AssistStructure;->mPendingAsyncChildren:Ljava/util/ArrayList;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/app/assist/AssistStructure;->mTmpRect:Landroid/graphics/Rect;

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/app/assist/AssistStructure;->mSanitizeOnWrite:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Landroid/app/assist/AssistStructure;->mTaskId:I

    invoke-static {p1}, Landroid/content/ComponentName;->readFromParcel(Landroid/os/Parcel;)Landroid/content/ComponentName;

    move-result-object v1

    iput-object v1, p0, Landroid/app/assist/AssistStructure;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    move v0, v2

    :cond_0
    iput-boolean v0, p0, Landroid/app/assist/AssistStructure;->mIsHomeActivity:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    iput-object v0, p0, Landroid/app/assist/AssistStructure;->mReceiveChannel:Landroid/os/IBinder;

    return-void
.end method


# virtual methods
.method public clearSendChannel()V
    .locals 2

    iget-object v0, p0, Landroid/app/assist/AssistStructure;->mSendChannel:Landroid/app/assist/AssistStructure$SendChannel;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    iput-object v1, v0, Landroid/app/assist/AssistStructure$SendChannel;->mAssistStructure:Landroid/app/assist/AssistStructure;

    :cond_0
    return-void
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method dump(Ljava/lang/String;Landroid/app/assist/AssistStructure$ViewNode;Z)V
    .locals 22

    goto/32 :goto_3

    nop

    :goto_0
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    goto/32 :goto_173

    nop

    :goto_1
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_13f

    nop

    :goto_2
    new-instance v13, Ljava/lang/StringBuilder;

    goto/32 :goto_111

    nop

    :goto_3
    move-object/from16 v0, p1

    goto/32 :goto_17b

    nop

    :goto_4
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    goto/32 :goto_c8

    nop

    :goto_5
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getLeft()I

    move-result v3

    goto/32 :goto_a

    nop

    :goto_6
    new-instance v15, Ljava/lang/StringBuilder;

    goto/32 :goto_128

    nop

    :goto_7
    if-nez v2, :cond_0

    goto/32 :goto_e9

    :cond_0
    goto/32 :goto_11

    nop

    :goto_8
    new-instance v13, Ljava/lang/StringBuilder;

    goto/32 :goto_24

    nop

    :goto_9
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_60

    nop

    :goto_a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_a7

    nop

    :goto_b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_5

    nop

    :goto_c
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_159

    nop

    :goto_d
    move-object/from16 v17, v3

    :goto_e
    goto/32 :goto_84

    nop

    :goto_f
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    goto/32 :goto_27

    nop

    :goto_10
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_4d

    nop

    :goto_11
    new-instance v15, Ljava/lang/StringBuilder;

    goto/32 :goto_3c

    nop

    :goto_12
    const-string v8, "  Transformation: "

    goto/32 :goto_167

    nop

    :goto_13
    new-instance v6, Ljava/lang/StringBuilder;

    goto/32 :goto_138

    nop

    :goto_14
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    goto/32 :goto_99

    nop

    :goto_15
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    goto/32 :goto_76

    nop

    :goto_16
    invoke-static {v5, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_17
    goto/32 :goto_174

    nop

    :goto_18
    if-nez v1, :cond_1

    goto/32 :goto_fa

    :cond_1
    goto/32 :goto_f9

    nop

    :goto_19
    const-string v14, "  Web domain: "

    goto/32 :goto_148

    nop

    :goto_1a
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_ce

    nop

    :goto_1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_9

    nop

    :goto_1c
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_d7

    nop

    :goto_1d
    move/from16 v16, v2

    :goto_1e
    goto/32 :goto_74

    nop

    :goto_1f
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    goto/32 :goto_f

    nop

    :goto_20
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    goto/32 :goto_142

    nop

    :goto_21
    const-string v10, "  Alpha: "

    goto/32 :goto_169

    nop

    :goto_22
    if-nez v10, :cond_2

    goto/32 :goto_4a

    :cond_2
    goto/32 :goto_11a

    nop

    :goto_23
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    goto/32 :goto_fe

    nop

    :goto_24
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_178

    nop

    :goto_25
    new-instance v3, Ljava/lang/StringBuilder;

    goto/32 :goto_11e

    nop

    :goto_26
    const-string v14, " , style: #"

    goto/32 :goto_20

    nop

    :goto_27
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getTextIdEntry()Ljava/lang/String;

    move-result-object v14

    goto/32 :goto_2e

    nop

    :goto_28
    move-object/from16 v2, p0

    goto/32 :goto_15e

    nop

    :goto_29
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_126

    nop

    :goto_2a
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getTextColor()I

    move-result v14

    goto/32 :goto_e3

    nop

    :goto_2b
    new-instance v8, Ljava/lang/StringBuilder;

    goto/32 :goto_71

    nop

    :goto_2c
    invoke-static {v5, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2d
    goto/32 :goto_70

    nop

    :goto_2e
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    goto/32 :goto_150

    nop

    :goto_2f
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    goto/32 :goto_19

    nop

    :goto_30
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    goto/32 :goto_112

    nop

    :goto_31
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_b8

    nop

    :goto_32
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getHintIdEntry()Ljava/lang/String;

    move-result-object v11

    goto/32 :goto_8f

    nop

    :goto_33
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    goto/32 :goto_90

    nop

    :goto_34
    goto/16 :goto_1e

    :goto_35
    goto/32 :goto_1d

    nop

    :goto_36
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_cd

    nop

    :goto_37
    invoke-static {v5, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_2

    nop

    :goto_38
    const-string v14, "  Text color fg: #"

    goto/32 :goto_122

    nop

    :goto_39
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    goto/32 :goto_125

    nop

    :goto_3a
    move-object/from16 v0, p1

    goto/32 :goto_ff

    nop

    :goto_3b
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    goto/32 :goto_136

    nop

    :goto_3c
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_75

    nop

    :goto_3d
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_cb

    nop

    :goto_3e
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    goto/32 :goto_133

    nop

    :goto_3f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_10d

    nop

    :goto_40
    const-string v4, " "

    goto/32 :goto_160

    nop

    :goto_41
    invoke-static {v5, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_25

    nop

    :goto_42
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_17f

    nop

    :goto_43
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_40

    nop

    :goto_44
    invoke-virtual {v11, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    goto/32 :goto_a2

    nop

    :goto_45
    const-string v2, " NO autofill ID"

    goto/32 :goto_10c

    nop

    :goto_46
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_17d

    nop

    :goto_47
    move-object/from16 v19, v2

    goto/32 :goto_146

    nop

    :goto_48
    const-string v3, "  Children:"

    goto/32 :goto_f4

    nop

    :goto_49
    invoke-static {v5, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_4a
    goto/32 :goto_103

    nop

    :goto_4b
    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_be

    nop

    :goto_4c
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    goto/32 :goto_fb

    nop

    :goto_4d
    const-string v5, "] "

    goto/32 :goto_59

    nop

    :goto_4e
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    goto/32 :goto_9a

    nop

    :goto_4f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_14f

    nop

    :goto_50
    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_14a

    nop

    :goto_51
    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    goto/32 :goto_eb

    nop

    :goto_52
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_91

    nop

    :goto_53
    new-instance v15, Ljava/lang/StringBuilder;

    goto/32 :goto_12b

    nop

    :goto_54
    goto/16 :goto_e0

    :goto_55
    goto/32 :goto_df

    nop

    :goto_56
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    goto/32 :goto_11f

    nop

    :goto_57
    move/from16 v16, v2

    goto/32 :goto_58

    nop

    :goto_58
    const-string v2, "  LocaleList: "

    goto/32 :goto_50

    nop

    :goto_59
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_104

    nop

    :goto_5a
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    goto/32 :goto_9d

    nop

    :goto_5b
    invoke-static {v5, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_8

    nop

    :goto_5c
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    goto/32 :goto_e1

    nop

    :goto_5d
    invoke-static {v15}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    goto/32 :goto_e5

    nop

    :goto_5e
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    goto/32 :goto_3f

    nop

    :goto_5f
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getId()I

    move-result v2

    goto/32 :goto_17e

    nop

    :goto_60
    invoke-static {v5, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_162

    nop

    :goto_61
    const-string v15, "  HtmlInfo: tag="

    goto/32 :goto_23

    nop

    :goto_62
    invoke-virtual {v15, v5}, Landroid/app/assist/AssistStructure$ViewNode;->getChildAt(I)Landroid/app/assist/AssistStructure$ViewNode;

    move-result-object v0

    goto/32 :goto_7a

    nop

    :goto_63
    move/from16 v21, v2

    goto/32 :goto_28

    nop

    :goto_64
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_da

    nop

    :goto_65
    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v12

    :goto_66
    nop

    goto/32 :goto_7d

    nop

    :goto_67
    invoke-static {v5, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_68
    goto/32 :goto_120

    nop

    :goto_69
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_f7

    nop

    :goto_6a
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    goto/32 :goto_14b

    nop

    :goto_6b
    goto :goto_66

    :goto_6c
    goto/32 :goto_65

    nop

    :goto_6d
    new-instance v15, Ljava/lang/StringBuilder;

    goto/32 :goto_46

    nop

    :goto_6e
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getTextSelectionStart()I

    move-result v14

    goto/32 :goto_72

    nop

    :goto_6f
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_bd

    nop

    :goto_70
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v8

    goto/32 :goto_7e

    nop

    :goto_71
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_79

    nop

    :goto_72
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    goto/32 :goto_b7

    nop

    :goto_73
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_c4

    nop

    :goto_74
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getReceiveContentMimeTypes()[Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_7

    nop

    :goto_75
    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    goto/32 :goto_95

    nop

    :goto_76
    if-nez v2, :cond_3

    goto/32 :goto_c1

    :cond_3
    goto/32 :goto_143

    nop

    :goto_77
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->isAssistBlocked()Z

    move-result v11

    goto/32 :goto_ac

    nop

    :goto_78
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getTop()I

    move-result v4

    goto/32 :goto_43

    nop

    :goto_79
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    goto/32 :goto_21

    nop

    :goto_7a
    move/from16 v21, v2

    goto/32 :goto_d8

    nop

    :goto_7b
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    goto/32 :goto_11d

    nop

    :goto_7c
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_163

    nop

    :goto_7d
    new-instance v13, Ljava/lang/StringBuilder;

    goto/32 :goto_a5

    nop

    :goto_7e
    if-nez v8, :cond_4

    goto/32 :goto_108

    :cond_4
    goto/32 :goto_109

    nop

    :goto_7f
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    goto/32 :goto_c6

    nop

    :goto_80
    invoke-static {v15}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    goto/32 :goto_1c

    nop

    :goto_81
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_3a

    nop

    :goto_82
    const-string v11, "  Content description: "

    goto/32 :goto_fc

    nop

    :goto_83
    if-nez v3, :cond_5

    goto/32 :goto_17

    :cond_5
    goto/32 :goto_b3

    nop

    :goto_84
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getHint()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_de

    nop

    :goto_85
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getAutofillValue()Landroid/view/autofill/AutofillValue;

    move-result-object v15

    goto/32 :goto_155

    nop

    :goto_86
    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    goto/32 :goto_57

    nop

    :goto_87
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_179

    nop

    :goto_88
    if-gtz v2, :cond_6

    goto/32 :goto_15f

    :cond_6
    goto/32 :goto_13c

    nop

    :goto_89
    new-instance v11, Ljava/lang/StringBuilder;

    goto/32 :goto_e2

    nop

    :goto_8a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto/32 :goto_2c

    nop

    :goto_8b
    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_1b

    nop

    :goto_8c
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    goto/32 :goto_5b

    nop

    :goto_8d
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_15d

    nop

    :goto_8e
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    goto/32 :goto_12

    nop

    :goto_8f
    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_15a

    nop

    :goto_90
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getInputType()I

    move-result v14

    goto/32 :goto_cf

    nop

    :goto_91
    const-string v15, ", important="

    goto/32 :goto_d5

    nop

    :goto_92
    if-nez v14, :cond_7

    goto/32 :goto_35

    :cond_7
    goto/32 :goto_6

    nop

    :goto_93
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_f8

    nop

    :goto_94
    const-string v15, ", value="

    goto/32 :goto_121

    nop

    :goto_95
    move-object/from16 v17, v3

    goto/32 :goto_c9

    nop

    :goto_96
    const-string v15, ", attr="

    goto/32 :goto_4c

    nop

    :goto_97
    new-instance v9, Ljava/lang/StringBuilder;

    goto/32 :goto_1a

    nop

    :goto_98
    move/from16 v21, v2

    goto/32 :goto_15b

    nop

    :goto_99
    invoke-interface {v10}, Ljava/lang/CharSequence;->length()I

    move-result v13

    goto/32 :goto_ee

    nop

    :goto_9a
    invoke-static {v5, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_9b
    goto/32 :goto_119

    nop

    :goto_9c
    move-object/from16 v15, p2

    goto/32 :goto_62

    nop

    :goto_9d
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getTextBackgroundColor()I

    move-result v14

    goto/32 :goto_115

    nop

    :goto_9e
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getScrollX()I

    move-result v4

    goto/32 :goto_171

    nop

    :goto_9f
    invoke-static {v5, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_a0
    goto/32 :goto_10a

    nop

    :goto_a1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_41

    nop

    :goto_a2
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto/32 :goto_175

    nop

    :goto_a3
    const-string v8, "  Scroll: "

    goto/32 :goto_144

    nop

    :goto_a4
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_fd

    nop

    :goto_a5
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_152

    nop

    :goto_a6
    const-string v14, "  Input type: "

    goto/32 :goto_33

    nop

    :goto_a7
    const-string v3, ","

    goto/32 :goto_106

    nop

    :goto_a8
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getAutofillType()I

    move-result v15

    goto/32 :goto_93

    nop

    :goto_a9
    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_135

    nop

    :goto_aa
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    goto/32 :goto_bb

    nop

    :goto_ab
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    goto/32 :goto_37

    nop

    :goto_ac
    if-nez v11, :cond_8

    goto/32 :goto_176

    :cond_8
    goto/32 :goto_89

    nop

    :goto_ad
    goto/16 :goto_16e

    :goto_ae
    goto/32 :goto_116

    nop

    :goto_af
    goto/16 :goto_68

    :goto_b0
    goto/32 :goto_47

    nop

    :goto_b1
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_c

    nop

    :goto_b2
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_aa

    nop

    :goto_b3
    new-instance v7, Ljava/lang/StringBuilder;

    goto/32 :goto_b4

    nop

    :goto_b4
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_8e

    nop

    :goto_b5
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_12d

    nop

    :goto_b6
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getIdPackage()Ljava/lang/String;

    move-result-object v9

    goto/32 :goto_181

    nop

    :goto_b7
    const-string v14, "-"

    goto/32 :goto_6a

    nop

    :goto_b8
    invoke-static {v5, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_af

    nop

    :goto_b9
    return-void

    :goto_ba
    const-string v13, " chars]"

    goto/32 :goto_bf

    nop

    :goto_bb
    const-string v15, "  Extras: "

    goto/32 :goto_100

    nop

    :goto_bc
    cmpl-float v8, v9, v8

    goto/32 :goto_13d

    nop

    :goto_bd
    const-string v4, ":"

    goto/32 :goto_36

    nop

    :goto_be
    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    goto/32 :goto_158

    nop

    :goto_bf
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    goto/32 :goto_182

    nop

    :goto_c0
    invoke-static {v5, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_c1
    goto/32 :goto_77

    nop

    :goto_c2
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_29

    nop

    :goto_c3
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_f5

    nop

    :goto_c4
    invoke-static {v5, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_c5
    goto/32 :goto_9e

    nop

    :goto_c6
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto/32 :goto_11b

    nop

    :goto_c7
    const-string v15, "  BLOCKED"

    goto/32 :goto_44

    nop

    :goto_c8
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getTextSize()F

    move-result v14

    goto/32 :goto_156

    nop

    :goto_c9
    const-string v3, "  MIME types: "

    goto/32 :goto_4b

    nop

    :goto_ca
    const-string v7, "  ID: #"

    goto/32 :goto_183

    nop

    :goto_cb
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    goto/32 :goto_a6

    nop

    :goto_cc
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_ca

    nop

    :goto_cd
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_157

    nop

    :goto_ce
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    goto/32 :goto_184

    nop

    :goto_cf
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    goto/32 :goto_ab

    nop

    :goto_d0
    move-object/from16 v2, p0

    :goto_d1
    goto/32 :goto_b9

    nop

    :goto_d2
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    goto/32 :goto_c7

    nop

    :goto_d3
    const-string v11, "  Resource id: "

    goto/32 :goto_22

    nop

    :goto_d4
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getIdType()Ljava/lang/String;

    move-result-object v8

    goto/32 :goto_b6

    nop

    :goto_d5
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_dc

    nop

    :goto_d6
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    goto/32 :goto_38

    nop

    :goto_d7
    const-string v15, ", hints="

    goto/32 :goto_8d

    nop

    :goto_d8
    move-object/from16 v2, p0

    goto/32 :goto_10b

    nop

    :goto_d9
    if-eqz v12, :cond_9

    goto/32 :goto_6c

    :cond_9
    goto/32 :goto_18

    nop

    :goto_da
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_c3

    nop

    :goto_db
    new-instance v7, Ljava/lang/StringBuilder;

    goto/32 :goto_137

    nop

    :goto_dc
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getImportantForAutofill()I

    move-result v15

    goto/32 :goto_42

    nop

    :goto_dd
    cmpl-float v9, v7, v8

    goto/32 :goto_e6

    nop

    :goto_de
    if-nez v3, :cond_a

    goto/32 :goto_55

    :cond_a
    goto/32 :goto_53

    nop

    :goto_df
    move-object/from16 v18, v2

    :goto_e0
    goto/32 :goto_15

    nop

    :goto_e1
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto/32 :goto_16

    nop

    :goto_e2
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_d2

    nop

    :goto_e3
    invoke-static {v14}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v14

    goto/32 :goto_7b

    nop

    :goto_e4
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    goto/32 :goto_4e

    nop

    :goto_e5
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_94

    nop

    :goto_e6
    if-nez v9, :cond_b

    goto/32 :goto_11c

    :cond_b
    goto/32 :goto_97

    nop

    :goto_e7
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getAlpha()F

    move-result v9

    goto/32 :goto_bc

    nop

    :goto_e8
    goto/16 :goto_e

    :goto_e9
    goto/32 :goto_d

    nop

    :goto_ea
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_16a

    nop

    :goto_eb
    move-object/from16 v18, v2

    goto/32 :goto_15c

    nop

    :goto_ec
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    goto/32 :goto_a3

    nop

    :goto_ed
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    goto/32 :goto_6e

    nop

    :goto_ee
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    goto/32 :goto_ba

    nop

    :goto_ef
    move-object/from16 v20, v3

    goto/32 :goto_48

    nop

    :goto_f0
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getAutofillId()Landroid/view/autofill/AutofillId;

    move-result-object v11

    goto/32 :goto_12f

    nop

    :goto_f1
    if-eqz v4, :cond_c

    goto/32 :goto_154

    :cond_c
    goto/32 :goto_153

    nop

    :goto_f2
    const-string v15, ", sanitized="

    goto/32 :goto_129

    nop

    :goto_f3
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    goto/32 :goto_3e

    nop

    :goto_f4
    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_a1

    nop

    :goto_f5
    const-string v3, "View ["

    goto/32 :goto_b

    nop

    :goto_f6
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_12e

    nop

    :goto_f7
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    goto/32 :goto_61

    nop

    :goto_f8
    const-string v15, ", options="

    goto/32 :goto_139

    nop

    :goto_f9
    goto/16 :goto_6c

    :goto_fa
    goto/32 :goto_141

    nop

    :goto_fb
    invoke-virtual {v13}, Landroid/view/ViewStructure$HtmlInfo;->getAttributes()Ljava/util/List;

    move-result-object v15

    goto/32 :goto_f3

    nop

    :goto_fc
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    goto/32 :goto_0

    nop

    :goto_fd
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_16d

    nop

    :goto_fe
    invoke-virtual {v13}, Landroid/view/ViewStructure$HtmlInfo;->getTag()Ljava/lang/String;

    move-result-object v15

    goto/32 :goto_14c

    nop

    :goto_ff
    move/from16 v2, v21

    goto/32 :goto_ad

    nop

    :goto_100
    invoke-virtual {v11, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    goto/32 :goto_10e

    nop

    :goto_101
    if-lt v5, v2, :cond_d

    goto/32 :goto_ae

    :cond_d
    goto/32 :goto_9c

    nop

    :goto_102
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->isSanitized()Z

    move-result v15

    goto/32 :goto_52

    nop

    :goto_103
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getWebDomain()Ljava/lang/String;

    move-result-object v12

    goto/32 :goto_130

    nop

    :goto_104
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getClassName()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_c2

    nop

    :goto_105
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_4f

    nop

    :goto_106
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_78

    nop

    :goto_107
    invoke-static {v5, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_108
    goto/32 :goto_149

    nop

    :goto_109
    new-instance v10, Ljava/lang/StringBuilder;

    goto/32 :goto_1

    nop

    :goto_10a
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getTransformation()Landroid/graphics/Matrix;

    move-result-object v3

    goto/32 :goto_83

    nop

    :goto_10b
    invoke-virtual {v2, v3, v0, v1}, Landroid/app/assist/AssistStructure;->dump(Ljava/lang/String;Landroid/app/assist/AssistStructure$ViewNode;Z)V

    goto/32 :goto_81

    nop

    :goto_10c
    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_31

    nop

    :goto_10d
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getIdEntry()Ljava/lang/String;

    move-result-object v7

    goto/32 :goto_16b

    nop

    :goto_10e
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    goto/32 :goto_166

    nop

    :goto_10f
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getHeight()I

    move-result v5

    goto/32 :goto_10

    nop

    :goto_110
    move-object/from16 v15, p2

    goto/32 :goto_98

    nop

    :goto_111
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1f

    nop

    :goto_112
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    goto/32 :goto_161

    nop

    :goto_113
    new-instance v13, Ljava/lang/StringBuilder;

    goto/32 :goto_3d

    nop

    :goto_114
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    goto/32 :goto_7f

    nop

    :goto_115
    invoke-static {v14}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v14

    goto/32 :goto_39

    nop

    :goto_116
    move-object/from16 v15, p2

    goto/32 :goto_63

    nop

    :goto_117
    new-instance v13, Ljava/lang/StringBuilder;

    goto/32 :goto_124

    nop

    :goto_118
    invoke-static {v5, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_34

    nop

    :goto_119
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getHtmlInfo()Landroid/view/ViewStructure$HtmlInfo;

    move-result-object v13

    goto/32 :goto_127

    nop

    :goto_11a
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->isSanitized()Z

    move-result v12

    goto/32 :goto_d9

    nop

    :goto_11b
    invoke-static {v5, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_11c
    goto/32 :goto_e7

    nop

    :goto_11d
    const-string v14, ", bg: #"

    goto/32 :goto_5a

    nop

    :goto_11e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_f6

    nop

    :goto_11f
    const-string v14, "): "

    goto/32 :goto_3b

    nop

    :goto_120
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getChildCount()I

    move-result v2

    goto/32 :goto_88

    nop

    :goto_121
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_85

    nop

    :goto_122
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    goto/32 :goto_2a

    nop

    :goto_123
    new-instance v13, Ljava/lang/StringBuilder;

    goto/32 :goto_16f

    nop

    :goto_124
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_d6

    nop

    :goto_125
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    goto/32 :goto_131

    nop

    :goto_126
    const-string v5, "AssistStructure"

    goto/32 :goto_13e

    nop

    :goto_127
    if-nez v13, :cond_e

    goto/32 :goto_134

    :cond_e
    goto/32 :goto_147

    nop

    :goto_128
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_86

    nop

    :goto_129
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_102

    nop

    :goto_12a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_118

    nop

    :goto_12b
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_51

    nop

    :goto_12c
    const-string v14, "  Text size: "

    goto/32 :goto_4

    nop

    :goto_12d
    const-string v13, "REDACTED["

    goto/32 :goto_14

    nop

    :goto_12e
    const-string v5, "    "

    goto/32 :goto_a4

    nop

    :goto_12f
    if-eqz v11, :cond_f

    goto/32 :goto_b0

    :cond_f
    goto/32 :goto_6d

    nop

    :goto_130
    if-nez v12, :cond_10

    goto/32 :goto_9b

    :cond_10
    goto/32 :goto_123

    nop

    :goto_131
    invoke-static {v5, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_113

    nop

    :goto_132
    invoke-static {v5, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_e8

    nop

    :goto_133
    invoke-static {v5, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_134
    goto/32 :goto_172

    nop

    :goto_135
    const-string v15, ", type="

    goto/32 :goto_17a

    nop

    :goto_136
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    goto/32 :goto_8c

    nop

    :goto_137
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_ec

    nop

    :goto_138
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_cc

    nop

    :goto_139
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_168

    nop

    :goto_13a
    const-string v14, "  Text (sel "

    goto/32 :goto_ed

    nop

    :goto_13b
    invoke-static {v5, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_54

    nop

    :goto_13c
    new-instance v15, Ljava/lang/StringBuilder;

    goto/32 :goto_ea

    nop

    :goto_13d
    if-nez v8, :cond_11

    goto/32 :goto_2d

    :cond_11
    goto/32 :goto_2b

    nop

    :goto_13e
    invoke-static {v5, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_5f

    nop

    :goto_13f
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    goto/32 :goto_82

    nop

    :goto_140
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_10f

    nop

    :goto_141
    new-instance v12, Ljava/lang/StringBuilder;

    goto/32 :goto_b5

    nop

    :goto_142
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getTextStyle()I

    move-result v14

    goto/32 :goto_30

    nop

    :goto_143
    new-instance v11, Ljava/lang/StringBuilder;

    goto/32 :goto_b2

    nop

    :goto_144
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    goto/32 :goto_17c

    nop

    :goto_145
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    goto/32 :goto_8a

    nop

    :goto_146
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_180

    nop

    :goto_147
    new-instance v14, Ljava/lang/StringBuilder;

    goto/32 :goto_69

    nop

    :goto_148
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    goto/32 :goto_e4

    nop

    :goto_149
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getText()Ljava/lang/CharSequence;

    move-result-object v10

    goto/32 :goto_d3

    nop

    :goto_14a
    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_12a

    nop

    :goto_14b
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getTextSelectionEnd()I

    move-result v14

    goto/32 :goto_56

    nop

    :goto_14c
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    goto/32 :goto_96

    nop

    :goto_14d
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_a9

    nop

    :goto_14e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_132

    nop

    :goto_14f
    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_32

    nop

    :goto_150
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    goto/32 :goto_49

    nop

    :goto_151
    const-string v15, "  Autofill info: id= "

    goto/32 :goto_14d

    nop

    :goto_152
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    goto/32 :goto_13a

    nop

    :goto_153
    if-nez v6, :cond_12

    goto/32 :goto_a0

    :cond_12
    :goto_154
    goto/32 :goto_db

    nop

    :goto_155
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_f2

    nop

    :goto_156
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v13

    goto/32 :goto_26

    nop

    :goto_157
    const-string v4, "/"

    goto/32 :goto_7c

    nop

    :goto_158
    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/32 :goto_14e

    nop

    :goto_159
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_9f

    nop

    :goto_15a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_13b

    nop

    :goto_15b
    move-object/from16 v20, v3

    goto/32 :goto_d0

    nop

    :goto_15c
    const-string v2, "  Hint: "

    goto/32 :goto_8b

    nop

    :goto_15d
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getAutofillHints()[Ljava/lang/String;

    move-result-object v15

    goto/32 :goto_5d

    nop

    :goto_15e
    goto/16 :goto_d1

    :goto_15f
    goto/32 :goto_110

    nop

    :goto_160
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_165

    nop

    :goto_161
    invoke-static {v5, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_117

    nop

    :goto_162
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_105

    nop

    :goto_163
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_164
    goto/32 :goto_73

    nop

    :goto_165
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getWidth()I

    move-result v5

    goto/32 :goto_87

    nop

    :goto_166
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto/32 :goto_c0

    nop

    :goto_167
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    goto/32 :goto_5c

    nop

    :goto_168
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getAutofillOptions()[Ljava/lang/CharSequence;

    move-result-object v15

    goto/32 :goto_80

    nop

    :goto_169
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    goto/32 :goto_145

    nop

    :goto_16a
    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    goto/32 :goto_ef

    nop

    :goto_16b
    if-nez v7, :cond_13

    goto/32 :goto_164

    :cond_13
    goto/32 :goto_d4

    nop

    :goto_16c
    const/4 v8, 0x0

    goto/32 :goto_dd

    nop

    :goto_16d
    const/4 v5, 0x0

    :goto_16e
    goto/32 :goto_101

    nop

    :goto_16f
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_2f

    nop

    :goto_170
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_151

    nop

    :goto_171
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getScrollY()I

    move-result v6

    goto/32 :goto_f1

    nop

    :goto_172
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getLocaleList()Landroid/os/LocaleList;

    move-result-object v14

    goto/32 :goto_92

    nop

    :goto_173
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/32 :goto_107

    nop

    :goto_174
    invoke-virtual/range {p2 .. p2}, Landroid/app/assist/AssistStructure$ViewNode;->getElevation()F

    move-result v7

    goto/32 :goto_16c

    nop

    :goto_175
    invoke-static {v5, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_176
    goto/32 :goto_f0

    nop

    :goto_177
    move-object/from16 v19, v2

    goto/32 :goto_45

    nop

    :goto_178
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    goto/32 :goto_12c

    nop

    :goto_179
    const-string/jumbo v5, "x"

    goto/32 :goto_140

    nop

    :goto_17a
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/32 :goto_a8

    nop

    :goto_17b
    move/from16 v1, p3

    goto/32 :goto_64

    nop

    :goto_17c
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    goto/32 :goto_b1

    nop

    :goto_17d
    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    goto/32 :goto_177

    nop

    :goto_17e
    if-nez v2, :cond_14

    goto/32 :goto_c5

    :cond_14
    goto/32 :goto_13

    nop

    :goto_17f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_67

    nop

    :goto_180
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_170

    nop

    :goto_181
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_6f

    nop

    :goto_182
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    goto/32 :goto_6b

    nop

    :goto_183
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_5e

    nop

    :goto_184
    const-string v10, "  Elevation: "

    goto/32 :goto_114

    nop
.end method

.method public dump(Z)V
    .locals 6

    iget-object v0, p0, Landroid/app/assist/AssistStructure;->mActivityComponent:Landroid/content/ComponentName;

    const-string v1, "AssistStructure"

    if-nez v0, :cond_0

    const-string v0, "dump(): calling ensureData() first"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/assist/AssistStructure;->ensureData()V

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Task id: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Landroid/app/assist/AssistStructure;->mTaskId:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Activity: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Landroid/app/assist/AssistStructure;->mActivityComponent:Landroid/content/ComponentName;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sanitize on write: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Landroid/app/assist/AssistStructure;->mSanitizeOnWrite:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Flags: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Landroid/app/assist/AssistStructure;->mFlags:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/assist/AssistStructure;->getWindowNodeCount()I

    move-result v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_2

    invoke-virtual {p0, v2}, Landroid/app/assist/AssistStructure;->getWindowNodeAt(I)Landroid/app/assist/AssistStructure$WindowNode;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Window #"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Landroid/app/assist/AssistStructure$WindowNode;->getLeft()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Landroid/app/assist/AssistStructure$WindowNode;->getTop()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Landroid/app/assist/AssistStructure$WindowNode;->getWidth()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Landroid/app/assist/AssistStructure$WindowNode;->getHeight()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Landroid/app/assist/AssistStructure$WindowNode;->getTitle()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v3}, Landroid/app/assist/AssistStructure$WindowNode;->getRootViewNode()Landroid/app/assist/AssistStructure$ViewNode;

    move-result-object v4

    const-string v5, "  "

    invoke-virtual {p0, v5, v4, p1}, Landroid/app/assist/AssistStructure;->dump(Ljava/lang/String;Landroid/app/assist/AssistStructure$ViewNode;Z)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    return-void
.end method

.method public ensureData()V
    .locals 2

    iget-boolean v0, p0, Landroid/app/assist/AssistStructure;->mHaveData:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/assist/AssistStructure;->mHaveData:Z

    new-instance v0, Landroid/app/assist/AssistStructure$ParcelTransferReader;

    iget-object v1, p0, Landroid/app/assist/AssistStructure;->mReceiveChannel:Landroid/os/IBinder;

    invoke-direct {v0, p0, v1}, Landroid/app/assist/AssistStructure$ParcelTransferReader;-><init>(Landroid/app/assist/AssistStructure;Landroid/os/IBinder;)V

    invoke-virtual {v0}, Landroid/app/assist/AssistStructure$ParcelTransferReader;->go()V

    return-void
.end method

.method public ensureDataForAutofill()V
    .locals 2

    iget-boolean v0, p0, Landroid/app/assist/AssistStructure;->mHaveData:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/assist/AssistStructure;->mHaveData:Z

    iget-object v0, p0, Landroid/app/assist/AssistStructure;->mReceiveChannel:Landroid/os/IBinder;

    invoke-static {v0}, Landroid/os/Binder;->allowBlocking(Landroid/os/IBinder;)Landroid/os/IBinder;

    :try_start_0
    new-instance v0, Landroid/app/assist/AssistStructure$ParcelTransferReader;

    iget-object v1, p0, Landroid/app/assist/AssistStructure;->mReceiveChannel:Landroid/os/IBinder;

    invoke-direct {v0, p0, v1}, Landroid/app/assist/AssistStructure$ParcelTransferReader;-><init>(Landroid/app/assist/AssistStructure;Landroid/os/IBinder;)V

    invoke-virtual {v0}, Landroid/app/assist/AssistStructure$ParcelTransferReader;->go()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Landroid/app/assist/AssistStructure;->mReceiveChannel:Landroid/os/IBinder;

    invoke-static {v0}, Landroid/os/Binder;->defaultBlocking(Landroid/os/IBinder;)Landroid/os/IBinder;

    nop

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Landroid/app/assist/AssistStructure;->mReceiveChannel:Landroid/os/IBinder;

    invoke-static {v1}, Landroid/os/Binder;->defaultBlocking(Landroid/os/IBinder;)Landroid/os/IBinder;

    throw v0
.end method

.method public getAcquisitionEndTime()J
    .locals 2

    invoke-virtual {p0}, Landroid/app/assist/AssistStructure;->ensureData()V

    iget-wide v0, p0, Landroid/app/assist/AssistStructure;->mAcquisitionEndTime:J

    return-wide v0
.end method

.method public getAcquisitionStartTime()J
    .locals 2

    invoke-virtual {p0}, Landroid/app/assist/AssistStructure;->ensureData()V

    iget-wide v0, p0, Landroid/app/assist/AssistStructure;->mAcquisitionStartTime:J

    return-wide v0
.end method

.method public getActivityComponent()Landroid/content/ComponentName;
    .locals 1

    iget-object v0, p0, Landroid/app/assist/AssistStructure;->mActivityComponent:Landroid/content/ComponentName;

    return-object v0
.end method

.method public getFlags()I
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure;->mFlags:I

    return v0
.end method

.method public getTaskId()I
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure;->mTaskId:I

    return v0
.end method

.method public getWindowNodeAt(I)Landroid/app/assist/AssistStructure$WindowNode;
    .locals 1

    invoke-virtual {p0}, Landroid/app/assist/AssistStructure;->ensureData()V

    iget-object v0, p0, Landroid/app/assist/AssistStructure;->mWindowNodes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/assist/AssistStructure$WindowNode;

    return-object v0
.end method

.method public getWindowNodeCount()I
    .locals 1

    invoke-virtual {p0}, Landroid/app/assist/AssistStructure;->ensureData()V

    iget-object v0, p0, Landroid/app/assist/AssistStructure;->mWindowNodes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public isHomeActivity()Z
    .locals 1

    iget-boolean v0, p0, Landroid/app/assist/AssistStructure;->mIsHomeActivity:Z

    return v0
.end method

.method public sanitizeForParceling(Z)V
    .locals 0

    iput-boolean p1, p0, Landroid/app/assist/AssistStructure;->mSanitizeOnWrite:Z

    return-void
.end method

.method public setAcquisitionEndTime(J)V
    .locals 0

    iput-wide p1, p0, Landroid/app/assist/AssistStructure;->mAcquisitionEndTime:J

    return-void
.end method

.method public setAcquisitionStartTime(J)V
    .locals 0

    iput-wide p1, p0, Landroid/app/assist/AssistStructure;->mAcquisitionStartTime:J

    return-void
.end method

.method public setActivityComponent(Landroid/content/ComponentName;)V
    .locals 0

    iput-object p1, p0, Landroid/app/assist/AssistStructure;->mActivityComponent:Landroid/content/ComponentName;

    return-void
.end method

.method public setHomeActivity(Z)V
    .locals 0

    iput-boolean p1, p0, Landroid/app/assist/AssistStructure;->mIsHomeActivity:Z

    return-void
.end method

.method public setTaskId(I)V
    .locals 0

    iput p1, p0, Landroid/app/assist/AssistStructure;->mTaskId:I

    return-void
.end method

.method waitForReady()Z
    .locals 7

    goto/32 :goto_2

    nop

    :goto_0
    goto :goto_a

    :cond_0
    :goto_1
    :try_start_0
    iget-object v3, p0, Landroid/app/assist/AssistStructure;->mPendingAsyncChildren:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_1

    const-string v3, "AssistStructure"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Skipping assist structure, waiting too long for async children (have "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Landroid/app/assist/AssistStructure;->mPendingAsyncChildren:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " remaining"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    :cond_1
    monitor-exit p0

    xor-int/lit8 v1, v0, 0x1

    return v1

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_7

    nop

    :goto_2
    const/4 v0, 0x0

    goto/32 :goto_5

    nop

    :goto_3
    move-wide v5, v3

    goto/32 :goto_8

    nop

    :goto_4
    if-ltz v3, :cond_2

    goto/32 :goto_1

    :cond_2
    goto/32 :goto_9

    nop

    :goto_5
    monitor-enter p0

    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    const-wide/16 v3, 0x1388

    add-long/2addr v1, v3

    :goto_6
    iget-object v3, p0, Landroid/app/assist/AssistStructure;->mPendingAsyncChildren:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/32 :goto_3

    nop

    :goto_7
    throw v1

    :goto_8
    cmp-long v3, v3, v1

    goto/32 :goto_4

    nop

    :goto_9
    sub-long v3, v1, v5

    :try_start_2
    invoke-virtual {p0, v3, v4}, Ljava/lang/Object;->wait(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_a
    goto/32 :goto_b

    nop

    :goto_b
    goto :goto_6

    :catch_0
    move-exception v3

    goto/32 :goto_0

    nop
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget v0, p0, Landroid/app/assist/AssistStructure;->mTaskId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Landroid/app/assist/AssistStructure;->mActivityComponent:Landroid/content/ComponentName;

    invoke-static {v0, p1}, Landroid/content/ComponentName;->writeToParcel(Landroid/content/ComponentName;Landroid/os/Parcel;)V

    iget-boolean v0, p0, Landroid/app/assist/AssistStructure;->mIsHomeActivity:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Landroid/app/assist/AssistStructure;->mHaveData:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/app/assist/AssistStructure;->mSendChannel:Landroid/app/assist/AssistStructure$SendChannel;

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/assist/AssistStructure$SendChannel;

    invoke-direct {v0, p0}, Landroid/app/assist/AssistStructure$SendChannel;-><init>(Landroid/app/assist/AssistStructure;)V

    iput-object v0, p0, Landroid/app/assist/AssistStructure;->mSendChannel:Landroid/app/assist/AssistStructure$SendChannel;

    :cond_0
    iget-object v0, p0, Landroid/app/assist/AssistStructure;->mSendChannel:Landroid/app/assist/AssistStructure$SendChannel;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Landroid/app/assist/AssistStructure;->mReceiveChannel:Landroid/os/IBinder;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    :goto_0
    return-void
.end method
