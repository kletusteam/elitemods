.class public Landroid/app/TaskInfo;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/TaskInfo$CameraCompatControlState;
    }
.end annotation


# static fields
.field public static final CAMERA_COMPAT_CONTROL_DISMISSED:I = 0x3

.field public static final CAMERA_COMPAT_CONTROL_HIDDEN:I = 0x0

.field public static final CAMERA_COMPAT_CONTROL_TREATMENT_APPLIED:I = 0x2

.field public static final CAMERA_COMPAT_CONTROL_TREATMENT_SUGGESTED:I = 0x1

.field private static final TAG:Ljava/lang/String; = "TaskInfo"


# instance fields
.field public baseActivity:Landroid/content/ComponentName;

.field public baseIntent:Landroid/content/Intent;

.field public cameraCompatControlState:I

.field public final configuration:Landroid/content/res/Configuration;

.field public defaultMinSize:I

.field public displayAreaFeatureId:I

.field public displayCutoutInsets:Landroid/graphics/Rect;

.field public displayId:I

.field public isFocused:Z

.field public isResizeable:Z

.field public isRunning:Z

.field public isSleeping:Z

.field public isVisible:Z

.field public lastActiveTime:J

.field public launchCookies:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/os/IBinder;",
            ">;"
        }
    .end annotation
.end field

.field public launchIntoPipHostTaskId:I

.field public mIsCastMode:Z

.field public mTopActivityLocusId:Landroid/content/LocusId;

.field public minHeight:I

.field public minWidth:I

.field public numActivities:I

.field public origActivity:Landroid/content/ComponentName;

.field public parentTaskId:I

.field public pictureInPictureParams:Landroid/app/PictureInPictureParams;

.field public positionInParent:Landroid/graphics/Point;

.field public realActivity:Landroid/content/ComponentName;

.field public resizeMode:I

.field public shouldDockBigOverlays:Z

.field public supportsMultiWindow:Z

.field public supportsSplitScreenMultiWindow:Z

.field public taskDescription:Landroid/app/ActivityManager$TaskDescription;

.field public taskId:I

.field public token:Landroid/window/WindowContainerToken;

.field public topActivity:Landroid/content/ComponentName;

.field public topActivityEligibleForLetterboxEducation:Z

.field public topActivityInMiuiSizeCompat:Z

.field public topActivityInSizeCompat:Z

.field public topActivityInfo:Landroid/content/pm/ActivityInfo;

.field public topActivityType:I

.field public userId:I


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Landroid/app/TaskInfo;->displayAreaFeatureId:I

    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    iput-object v0, p0, Landroid/app/TaskInfo;->configuration:Landroid/content/res/Configuration;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/app/TaskInfo;->launchCookies:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput v0, p0, Landroid/app/TaskInfo;->cameraCompatControlState:I

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Landroid/app/TaskInfo;->displayAreaFeatureId:I

    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    iput-object v0, p0, Landroid/app/TaskInfo;->configuration:Landroid/content/res/Configuration;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/app/TaskInfo;->launchCookies:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput v0, p0, Landroid/app/TaskInfo;->cameraCompatControlState:I

    invoke-virtual {p0, p1}, Landroid/app/TaskInfo;->readFromParcel(Landroid/os/Parcel;)V

    return-void
.end method

.method public static cameraCompatControlStateToString(I)Ljava/lang/String;
    .locals 3

    packed-switch p0, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected camera compat control state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :pswitch_0
    const-string v0, "dismissed"

    return-object v0

    :pswitch_1
    const-string/jumbo v0, "treatment-applied"

    return-object v0

    :pswitch_2
    const-string/jumbo v0, "treatment-suggested"

    return-object v0

    :pswitch_3
    const-string v0, "hidden"

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public addLaunchCookie(Landroid/os/IBinder;)V
    .locals 1

    if-eqz p1, :cond_1

    iget-object v0, p0, Landroid/app/TaskInfo;->launchCookies:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Landroid/app/TaskInfo;->launchCookies:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method public containsLaunchCookie(Landroid/os/IBinder;)Z
    .locals 1

    iget-object v0, p0, Landroid/app/TaskInfo;->launchCookies:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public equalsForCompatUi(Landroid/app/TaskInfo;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    iget v1, p0, Landroid/app/TaskInfo;->displayId:I

    iget v2, p1, Landroid/app/TaskInfo;->displayId:I

    if-ne v1, v2, :cond_4

    iget v1, p0, Landroid/app/TaskInfo;->taskId:I

    iget v2, p1, Landroid/app/TaskInfo;->taskId:I

    if-ne v1, v2, :cond_4

    iget-boolean v1, p0, Landroid/app/TaskInfo;->topActivityInSizeCompat:Z

    iget-boolean v2, p1, Landroid/app/TaskInfo;->topActivityInSizeCompat:Z

    if-ne v1, v2, :cond_4

    iget-boolean v1, p0, Landroid/app/TaskInfo;->topActivityEligibleForLetterboxEducation:Z

    iget-boolean v2, p1, Landroid/app/TaskInfo;->topActivityEligibleForLetterboxEducation:Z

    if-ne v1, v2, :cond_4

    iget v1, p0, Landroid/app/TaskInfo;->cameraCompatControlState:I

    iget v2, p1, Landroid/app/TaskInfo;->cameraCompatControlState:I

    if-ne v1, v2, :cond_4

    invoke-virtual {p0}, Landroid/app/TaskInfo;->hasCompatUI()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/app/TaskInfo;->configuration:Landroid/content/res/Configuration;

    iget-object v1, v1, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v1}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    iget-object v2, p1, Landroid/app/TaskInfo;->configuration:Landroid/content/res/Configuration;

    iget-object v2, v2, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v2}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_1
    invoke-virtual {p0}, Landroid/app/TaskInfo;->hasCompatUI()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/app/TaskInfo;->configuration:Landroid/content/res/Configuration;

    invoke-virtual {v1}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v1

    iget-object v2, p1, Landroid/app/TaskInfo;->configuration:Landroid/content/res/Configuration;

    invoke-virtual {v2}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v2

    if-ne v1, v2, :cond_4

    :cond_2
    invoke-virtual {p0}, Landroid/app/TaskInfo;->hasCompatUI()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Landroid/app/TaskInfo;->isVisible:Z

    iget-boolean v2, p1, Landroid/app/TaskInfo;->isVisible:Z

    if-ne v1, v2, :cond_4

    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    nop

    :goto_0
    return v0
.end method

.method public equalsForMiuiSizeCompat(Landroid/app/TaskInfo;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    iget v1, p0, Landroid/app/TaskInfo;->displayId:I

    iget v2, p1, Landroid/app/TaskInfo;->displayId:I

    if-ne v1, v2, :cond_4

    iget v1, p0, Landroid/app/TaskInfo;->taskId:I

    iget v2, p1, Landroid/app/TaskInfo;->taskId:I

    if-ne v1, v2, :cond_4

    iget-boolean v1, p0, Landroid/app/TaskInfo;->topActivityInMiuiSizeCompat:Z

    iget-boolean v2, p1, Landroid/app/TaskInfo;->topActivityInMiuiSizeCompat:Z

    if-ne v1, v2, :cond_4

    iget-boolean v1, p0, Landroid/app/TaskInfo;->topActivityEligibleForLetterboxEducation:Z

    iget-boolean v2, p1, Landroid/app/TaskInfo;->topActivityEligibleForLetterboxEducation:Z

    if-ne v1, v2, :cond_4

    iget v1, p0, Landroid/app/TaskInfo;->cameraCompatControlState:I

    iget v2, p1, Landroid/app/TaskInfo;->cameraCompatControlState:I

    if-ne v1, v2, :cond_4

    invoke-virtual {p0}, Landroid/app/TaskInfo;->hasMiuiCompatUi()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/app/TaskInfo;->configuration:Landroid/content/res/Configuration;

    iget-object v1, v1, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v1}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    iget-object v2, p1, Landroid/app/TaskInfo;->configuration:Landroid/content/res/Configuration;

    iget-object v2, v2, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v2}, Landroid/app/WindowConfiguration;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_1
    invoke-virtual {p0}, Landroid/app/TaskInfo;->hasMiuiCompatUi()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/app/TaskInfo;->configuration:Landroid/content/res/Configuration;

    invoke-virtual {v1}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v1

    iget-object v2, p1, Landroid/app/TaskInfo;->configuration:Landroid/content/res/Configuration;

    invoke-virtual {v2}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v2

    if-ne v1, v2, :cond_4

    :cond_2
    invoke-virtual {p0}, Landroid/app/TaskInfo;->hasMiuiCompatUi()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Landroid/app/TaskInfo;->isVisible:Z

    iget-boolean v2, p1, Landroid/app/TaskInfo;->isVisible:Z

    if-ne v1, v2, :cond_4

    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    nop

    :goto_0
    return v0
.end method

.method public equalsForTaskOrganizer(Landroid/app/TaskInfo;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    iget v1, p0, Landroid/app/TaskInfo;->topActivityType:I

    iget v2, p1, Landroid/app/TaskInfo;->topActivityType:I

    if-ne v1, v2, :cond_1

    iget-boolean v1, p0, Landroid/app/TaskInfo;->isResizeable:Z

    iget-boolean v2, p1, Landroid/app/TaskInfo;->isResizeable:Z

    if-ne v1, v2, :cond_1

    iget-boolean v1, p0, Landroid/app/TaskInfo;->supportsMultiWindow:Z

    iget-boolean v2, p1, Landroid/app/TaskInfo;->supportsMultiWindow:Z

    if-ne v1, v2, :cond_1

    iget v1, p0, Landroid/app/TaskInfo;->displayAreaFeatureId:I

    iget v2, p1, Landroid/app/TaskInfo;->displayAreaFeatureId:I

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Landroid/app/TaskInfo;->positionInParent:Landroid/graphics/Point;

    iget-object v2, p1, Landroid/app/TaskInfo;->positionInParent:Landroid/graphics/Point;

    invoke-static {v1, v2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/app/TaskInfo;->pictureInPictureParams:Landroid/app/PictureInPictureParams;

    iget-object v2, p1, Landroid/app/TaskInfo;->pictureInPictureParams:Landroid/app/PictureInPictureParams;

    invoke-static {v1, v2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Landroid/app/TaskInfo;->shouldDockBigOverlays:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-boolean v2, p1, Landroid/app/TaskInfo;->shouldDockBigOverlays:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/app/TaskInfo;->displayCutoutInsets:Landroid/graphics/Rect;

    iget-object v2, p1, Landroid/app/TaskInfo;->displayCutoutInsets:Landroid/graphics/Rect;

    invoke-static {v1, v2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroid/app/TaskInfo;->getWindowingMode()I

    move-result v1

    invoke-virtual {p1}, Landroid/app/TaskInfo;->getWindowingMode()I

    move-result v2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Landroid/app/TaskInfo;->taskDescription:Landroid/app/ActivityManager$TaskDescription;

    iget-object v2, p1, Landroid/app/TaskInfo;->taskDescription:Landroid/app/ActivityManager$TaskDescription;

    invoke-static {v1, v2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Landroid/app/TaskInfo;->isFocused:Z

    iget-boolean v2, p1, Landroid/app/TaskInfo;->isFocused:Z

    if-ne v1, v2, :cond_1

    iget-boolean v1, p0, Landroid/app/TaskInfo;->isVisible:Z

    iget-boolean v2, p1, Landroid/app/TaskInfo;->isVisible:Z

    if-ne v1, v2, :cond_1

    iget-boolean v1, p0, Landroid/app/TaskInfo;->isSleeping:Z

    iget-boolean v2, p1, Landroid/app/TaskInfo;->isSleeping:Z

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Landroid/app/TaskInfo;->mTopActivityLocusId:Landroid/content/LocusId;

    iget-object v2, p1, Landroid/app/TaskInfo;->mTopActivityLocusId:Landroid/content/LocusId;

    invoke-static {v1, v2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p0, Landroid/app/TaskInfo;->parentTaskId:I

    iget v2, p1, Landroid/app/TaskInfo;->parentTaskId:I

    if-ne v1, v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    nop

    :goto_0
    return v0
.end method

.method public getActivityType()I
    .locals 1

    iget-object v0, p0, Landroid/app/TaskInfo;->configuration:Landroid/content/res/Configuration;

    iget-object v0, v0, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v0}, Landroid/app/WindowConfiguration;->getActivityType()I

    move-result v0

    return v0
.end method

.method public getConfiguration()Landroid/content/res/Configuration;
    .locals 1

    iget-object v0, p0, Landroid/app/TaskInfo;->configuration:Landroid/content/res/Configuration;

    return-object v0
.end method

.method public getParentTaskId()I
    .locals 1

    iget v0, p0, Landroid/app/TaskInfo;->parentTaskId:I

    return v0
.end method

.method public getPictureInPictureParams()Landroid/app/PictureInPictureParams;
    .locals 1

    iget-object v0, p0, Landroid/app/TaskInfo;->pictureInPictureParams:Landroid/app/PictureInPictureParams;

    return-object v0
.end method

.method public getToken()Landroid/window/WindowContainerToken;
    .locals 1

    iget-object v0, p0, Landroid/app/TaskInfo;->token:Landroid/window/WindowContainerToken;

    return-object v0
.end method

.method public getWindowingMode()I
    .locals 1

    iget-object v0, p0, Landroid/app/TaskInfo;->configuration:Landroid/content/res/Configuration;

    iget-object v0, v0, Landroid/content/res/Configuration;->windowConfiguration:Landroid/app/WindowConfiguration;

    invoke-virtual {v0}, Landroid/app/WindowConfiguration;->getWindowingMode()I

    move-result v0

    return v0
.end method

.method public hasCameraCompatControl()Z
    .locals 2

    iget v0, p0, Landroid/app/TaskInfo;->cameraCompatControlState:I

    if-eqz v0, :cond_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasCompatUI()Z
    .locals 1

    invoke-virtual {p0}, Landroid/app/TaskInfo;->hasCameraCompatControl()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Landroid/app/TaskInfo;->topActivityInSizeCompat:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Landroid/app/TaskInfo;->topActivityEligibleForLetterboxEducation:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public hasMiuiCompatUi()Z
    .locals 1

    invoke-virtual {p0}, Landroid/app/TaskInfo;->hasCameraCompatControl()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Landroid/app/TaskInfo;->topActivityInMiuiSizeCompat:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Landroid/app/TaskInfo;->topActivityEligibleForLetterboxEducation:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public hasParentTask()Z
    .locals 2

    iget v0, p0, Landroid/app/TaskInfo;->parentTaskId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isVisible()Z
    .locals 1

    iget-boolean v0, p0, Landroid/app/TaskInfo;->isVisible:Z

    return v0
.end method

.method readFromParcel(Landroid/os/Parcel;)V
    .locals 2

    goto/32 :goto_2e

    nop

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readTypedObject(Landroid/os/Parcelable$Creator;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_a

    nop

    :goto_1
    check-cast v0, Landroid/window/WindowContainerToken;

    goto/32 :goto_11

    nop

    :goto_2
    check-cast v0, Landroid/app/ActivityManager$TaskDescription;

    goto/32 :goto_3a

    nop

    :goto_3
    iput-object v0, p0, Landroid/app/TaskInfo;->topActivityInfo:Landroid/content/pm/ActivityInfo;

    goto/32 :goto_28

    nop

    :goto_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readBoolean()Z

    move-result v0

    goto/32 :goto_44

    nop

    :goto_5
    sget-object v0, Landroid/app/PictureInPictureParams;->CREATOR:Landroid/os/Parcelable$Creator;

    goto/32 :goto_1b

    nop

    :goto_6
    sget-object v0, Landroid/window/WindowContainerToken;->CREATOR:Landroid/os/Parcelable$Creator;

    goto/32 :goto_55

    nop

    :goto_7
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    goto/32 :goto_53

    nop

    :goto_8
    iput-boolean v0, p0, Landroid/app/TaskInfo;->topActivityInSizeCompat:Z

    goto/32 :goto_f

    nop

    :goto_9
    iput v0, p0, Landroid/app/TaskInfo;->userId:I

    goto/32 :goto_2f

    nop

    :goto_a
    check-cast v0, Landroid/graphics/Rect;

    goto/32 :goto_46

    nop

    :goto_b
    invoke-virtual {p1}, Landroid/os/Parcel;->readBoolean()Z

    move-result v0

    goto/32 :goto_4f

    nop

    :goto_c
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    goto/32 :goto_3e

    nop

    :goto_d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readTypedObject(Landroid/os/Parcelable$Creator;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_2a

    nop

    :goto_e
    iput v0, p0, Landroid/app/TaskInfo;->cameraCompatControlState:I

    goto/32 :goto_1a

    nop

    :goto_f
    invoke-virtual {p1}, Landroid/os/Parcel;->readBoolean()Z

    move-result v0

    goto/32 :goto_56

    nop

    :goto_10
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    goto/32 :goto_41

    nop

    :goto_11
    iput-object v0, p0, Landroid/app/TaskInfo;->token:Landroid/window/WindowContainerToken;

    goto/32 :goto_51

    nop

    :goto_12
    sget-object v0, Landroid/content/pm/ActivityInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    goto/32 :goto_4b

    nop

    :goto_13
    iput-boolean v0, p0, Landroid/app/TaskInfo;->isFocused:Z

    goto/32 :goto_57

    nop

    :goto_14
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBinderList(Ljava/util/List;)V

    goto/32 :goto_2c

    nop

    :goto_15
    check-cast v0, Landroid/content/Intent;

    goto/32 :goto_23

    nop

    :goto_16
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    goto/32 :goto_e

    nop

    :goto_17
    iput-object v0, p0, Landroid/app/TaskInfo;->baseActivity:Landroid/content/ComponentName;

    goto/32 :goto_1d

    nop

    :goto_18
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readTypedObject(Landroid/os/Parcelable$Creator;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_29

    nop

    :goto_19
    invoke-virtual {p1}, Landroid/os/Parcel;->readBoolean()Z

    move-result v0

    goto/32 :goto_40

    nop

    :goto_1a
    invoke-virtual {p1}, Landroid/os/Parcel;->readBoolean()Z

    move-result v0

    goto/32 :goto_54

    nop

    :goto_1b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readTypedObject(Landroid/os/Parcelable$Creator;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_47

    nop

    :goto_1c
    iput-boolean v0, p0, Landroid/app/TaskInfo;->isVisible:Z

    goto/32 :goto_4a

    nop

    :goto_1d
    invoke-static {p1}, Landroid/content/ComponentName;->readFromParcel(Landroid/os/Parcel;)Landroid/content/ComponentName;

    move-result-object v0

    goto/32 :goto_5d

    nop

    :goto_1e
    iput-boolean v0, p0, Landroid/app/TaskInfo;->supportsMultiWindow:Z

    goto/32 :goto_2d

    nop

    :goto_1f
    invoke-static {p1}, Landroid/content/ComponentName;->readFromParcel(Landroid/os/Parcel;)Landroid/content/ComponentName;

    move-result-object v0

    goto/32 :goto_4c

    nop

    :goto_20
    iput v0, p0, Landroid/app/TaskInfo;->minHeight:I

    goto/32 :goto_3c

    nop

    :goto_21
    check-cast v0, Landroid/content/pm/ActivityInfo;

    goto/32 :goto_3

    nop

    :goto_22
    iput-object v0, p0, Landroid/app/TaskInfo;->mTopActivityLocusId:Landroid/content/LocusId;

    goto/32 :goto_10

    nop

    :goto_23
    iput-object v0, p0, Landroid/app/TaskInfo;->baseIntent:Landroid/content/Intent;

    goto/32 :goto_35

    nop

    :goto_24
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    goto/32 :goto_5f

    nop

    :goto_25
    invoke-static {p1}, Landroid/content/ComponentName;->readFromParcel(Landroid/os/Parcel;)Landroid/content/ComponentName;

    move-result-object v0

    goto/32 :goto_34

    nop

    :goto_26
    iput v0, p0, Landroid/app/TaskInfo;->minWidth:I

    goto/32 :goto_33

    nop

    :goto_27
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readTypedObject(Landroid/os/Parcelable$Creator;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_28
    invoke-virtual {p1}, Landroid/os/Parcel;->readBoolean()Z

    move-result v0

    goto/32 :goto_32

    nop

    :goto_29
    check-cast v0, Landroid/graphics/Point;

    goto/32 :goto_3d

    nop

    :goto_2a
    check-cast v0, Landroid/content/LocusId;

    goto/32 :goto_22

    nop

    :goto_2b
    invoke-virtual {p1}, Landroid/os/Parcel;->readBoolean()Z

    move-result v0

    goto/32 :goto_8

    nop

    :goto_2c
    sget-object v0, Landroid/graphics/Point;->CREATOR:Landroid/os/Parcelable$Creator;

    goto/32 :goto_18

    nop

    :goto_2d
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    goto/32 :goto_48

    nop

    :goto_2e
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    goto/32 :goto_9

    nop

    :goto_2f
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    goto/32 :goto_5e

    nop

    :goto_30
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    goto/32 :goto_26

    nop

    :goto_31
    sget-object v0, Landroid/content/LocusId;->CREATOR:Landroid/os/Parcelable$Creator;

    goto/32 :goto_d

    nop

    :goto_32
    iput-boolean v0, p0, Landroid/app/TaskInfo;->isResizeable:Z

    goto/32 :goto_30

    nop

    :goto_33
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    goto/32 :goto_20

    nop

    :goto_34
    iput-object v0, p0, Landroid/app/TaskInfo;->realActivity:Landroid/content/ComponentName;

    goto/32 :goto_7

    nop

    :goto_35
    invoke-static {p1}, Landroid/content/ComponentName;->readFromParcel(Landroid/os/Parcel;)Landroid/content/ComponentName;

    move-result-object v0

    goto/32 :goto_17

    nop

    :goto_36
    invoke-virtual {p1}, Landroid/os/Parcel;->readBoolean()Z

    move-result v0

    goto/32 :goto_13

    nop

    :goto_37
    iput v0, p0, Landroid/app/TaskInfo;->topActivityType:I

    goto/32 :goto_5

    nop

    :goto_38
    invoke-virtual {v0, p1}, Landroid/content/res/Configuration;->readFromParcel(Landroid/os/Parcel;)V

    goto/32 :goto_6

    nop

    :goto_39
    iget-object v0, p0, Landroid/app/TaskInfo;->launchCookies:Ljava/util/ArrayList;

    goto/32 :goto_14

    nop

    :goto_3a
    iput-object v0, p0, Landroid/app/TaskInfo;->taskDescription:Landroid/app/ActivityManager$TaskDescription;

    goto/32 :goto_59

    nop

    :goto_3b
    iput-object v0, p0, Landroid/app/TaskInfo;->pictureInPictureParams:Landroid/app/PictureInPictureParams;

    goto/32 :goto_19

    nop

    :goto_3c
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    goto/32 :goto_5c

    nop

    :goto_3d
    iput-object v0, p0, Landroid/app/TaskInfo;->positionInParent:Landroid/graphics/Point;

    goto/32 :goto_c

    nop

    :goto_3e
    iput v0, p0, Landroid/app/TaskInfo;->parentTaskId:I

    goto/32 :goto_36

    nop

    :goto_3f
    sget-object v0, Landroid/graphics/Rect;->CREATOR:Landroid/os/Parcelable$Creator;

    goto/32 :goto_0

    nop

    :goto_40
    iput-boolean v0, p0, Landroid/app/TaskInfo;->shouldDockBigOverlays:Z

    goto/32 :goto_60

    nop

    :goto_41
    iput v0, p0, Landroid/app/TaskInfo;->displayAreaFeatureId:I

    goto/32 :goto_16

    nop

    :goto_42
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    goto/32 :goto_45

    nop

    :goto_43
    iget-object v0, p0, Landroid/app/TaskInfo;->configuration:Landroid/content/res/Configuration;

    goto/32 :goto_38

    nop

    :goto_44
    iput-boolean v0, p0, Landroid/app/TaskInfo;->topActivityEligibleForLetterboxEducation:Z

    goto/32 :goto_31

    nop

    :goto_45
    iput-wide v0, p0, Landroid/app/TaskInfo;->lastActiveTime:J

    goto/32 :goto_5a

    nop

    :goto_46
    iput-object v0, p0, Landroid/app/TaskInfo;->displayCutoutInsets:Landroid/graphics/Rect;

    goto/32 :goto_12

    nop

    :goto_47
    check-cast v0, Landroid/app/PictureInPictureParams;

    goto/32 :goto_3b

    nop

    :goto_48
    iput v0, p0, Landroid/app/TaskInfo;->resizeMode:I

    goto/32 :goto_43

    nop

    :goto_49
    iput-boolean v0, p0, Landroid/app/TaskInfo;->isSleeping:Z

    goto/32 :goto_2b

    nop

    :goto_4a
    invoke-virtual {p1}, Landroid/os/Parcel;->readBoolean()Z

    move-result v0

    goto/32 :goto_49

    nop

    :goto_4b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readTypedObject(Landroid/os/Parcelable$Creator;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_21

    nop

    :goto_4c
    iput-object v0, p0, Landroid/app/TaskInfo;->origActivity:Landroid/content/ComponentName;

    goto/32 :goto_25

    nop

    :goto_4d
    return-void

    :goto_4e
    iput v0, p0, Landroid/app/TaskInfo;->launchIntoPipHostTaskId:I

    goto/32 :goto_3f

    nop

    :goto_4f
    iput-boolean v0, p0, Landroid/app/TaskInfo;->isRunning:Z

    goto/32 :goto_52

    nop

    :goto_50
    iput-boolean v0, p0, Landroid/app/TaskInfo;->supportsSplitScreenMultiWindow:Z

    goto/32 :goto_5b

    nop

    :goto_51
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    goto/32 :goto_37

    nop

    :goto_52
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    goto/32 :goto_58

    nop

    :goto_53
    iput v0, p0, Landroid/app/TaskInfo;->numActivities:I

    goto/32 :goto_42

    nop

    :goto_54
    iput-boolean v0, p0, Landroid/app/TaskInfo;->mIsCastMode:Z

    goto/32 :goto_4d

    nop

    :goto_55
    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_56
    iput-boolean v0, p0, Landroid/app/TaskInfo;->topActivityInMiuiSizeCompat:Z

    goto/32 :goto_4

    nop

    :goto_57
    invoke-virtual {p1}, Landroid/os/Parcel;->readBoolean()Z

    move-result v0

    goto/32 :goto_1c

    nop

    :goto_58
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readTypedObject(Landroid/os/Parcelable$Creator;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_15

    nop

    :goto_59
    invoke-virtual {p1}, Landroid/os/Parcel;->readBoolean()Z

    move-result v0

    goto/32 :goto_50

    nop

    :goto_5a
    sget-object v0, Landroid/app/ActivityManager$TaskDescription;->CREATOR:Landroid/os/Parcelable$Creator;

    goto/32 :goto_27

    nop

    :goto_5b
    invoke-virtual {p1}, Landroid/os/Parcel;->readBoolean()Z

    move-result v0

    goto/32 :goto_1e

    nop

    :goto_5c
    iput v0, p0, Landroid/app/TaskInfo;->defaultMinSize:I

    goto/32 :goto_39

    nop

    :goto_5d
    iput-object v0, p0, Landroid/app/TaskInfo;->topActivity:Landroid/content/ComponentName;

    goto/32 :goto_1f

    nop

    :goto_5e
    iput v0, p0, Landroid/app/TaskInfo;->taskId:I

    goto/32 :goto_24

    nop

    :goto_5f
    iput v0, p0, Landroid/app/TaskInfo;->displayId:I

    goto/32 :goto_b

    nop

    :goto_60
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    goto/32 :goto_4e

    nop
.end method

.method public shouldDockBigOverlays()Z
    .locals 1

    iget-boolean v0, p0, Landroid/app/TaskInfo;->shouldDockBigOverlays:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TaskInfo{userId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/app/TaskInfo;->userId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " taskId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/app/TaskInfo;->taskId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " displayId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/app/TaskInfo;->displayId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isRunning="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/app/TaskInfo;->isRunning:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " baseIntent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/app/TaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " baseActivity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/app/TaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " topActivity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/app/TaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " origActivity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/app/TaskInfo;->origActivity:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " realActivity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/app/TaskInfo;->realActivity:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " numActivities="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/app/TaskInfo;->numActivities:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " lastActiveTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Landroid/app/TaskInfo;->lastActiveTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " supportsSplitScreenMultiWindow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/app/TaskInfo;->supportsSplitScreenMultiWindow:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " supportsMultiWindow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/app/TaskInfo;->supportsMultiWindow:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " resizeMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/app/TaskInfo;->resizeMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isResizeable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/app/TaskInfo;->isResizeable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " minWidth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/app/TaskInfo;->minWidth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " minHeight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/app/TaskInfo;->minHeight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " defaultMinSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/app/TaskInfo;->defaultMinSize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/app/TaskInfo;->token:Landroid/window/WindowContainerToken;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " topActivityType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/app/TaskInfo;->topActivityType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " pictureInPictureParams="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/app/TaskInfo;->pictureInPictureParams:Landroid/app/PictureInPictureParams;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " shouldDockBigOverlays="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/app/TaskInfo;->shouldDockBigOverlays:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " launchIntoPipHostTaskId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/app/TaskInfo;->launchIntoPipHostTaskId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " displayCutoutSafeInsets="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/app/TaskInfo;->displayCutoutInsets:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " topActivityInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/app/TaskInfo;->topActivityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " launchCookies="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/app/TaskInfo;->launchCookies:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " positionInParent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/app/TaskInfo;->positionInParent:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " parentTaskId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/app/TaskInfo;->parentTaskId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isFocused="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/app/TaskInfo;->isFocused:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isVisible="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/app/TaskInfo;->isVisible:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isSleeping="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/app/TaskInfo;->isSleeping:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " topActivityInSizeCompat="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/app/TaskInfo;->topActivityInSizeCompat:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " topActivityInMiuiSizeCompat="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/app/TaskInfo;->topActivityInMiuiSizeCompat:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " topActivityEligibleForLetterboxEducation= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/app/TaskInfo;->topActivityEligibleForLetterboxEducation:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " locusId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/app/TaskInfo;->mTopActivityLocusId:Landroid/content/LocusId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " displayAreaFeatureId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/app/TaskInfo;->displayAreaFeatureId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " cameraCompatControlState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/app/TaskInfo;->cameraCompatControlState:I

    invoke-static {v1}, Landroid/app/TaskInfo;->cameraCompatControlStateToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mIsCastMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/app/TaskInfo;->mIsCastMode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    goto/32 :goto_8

    nop

    :goto_0
    iget-object v0, p0, Landroid/app/TaskInfo;->mTopActivityLocusId:Landroid/content/LocusId;

    goto/32 :goto_41

    nop

    :goto_1
    iget-boolean v0, p0, Landroid/app/TaskInfo;->topActivityEligibleForLetterboxEducation:Z

    goto/32 :goto_13

    nop

    :goto_2
    invoke-virtual {v0, p1, p2}, Landroid/content/res/Configuration;->writeToParcel(Landroid/os/Parcel;I)V

    goto/32 :goto_3

    nop

    :goto_3
    iget-object v0, p0, Landroid/app/TaskInfo;->token:Landroid/window/WindowContainerToken;

    goto/32 :goto_9

    nop

    :goto_4
    iget v0, p0, Landroid/app/TaskInfo;->defaultMinSize:I

    goto/32 :goto_26

    nop

    :goto_5
    iget-object v0, p0, Landroid/app/TaskInfo;->topActivity:Landroid/content/ComponentName;

    goto/32 :goto_22

    nop

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    goto/32 :goto_37

    nop

    :goto_7
    iget v0, p0, Landroid/app/TaskInfo;->numActivities:I

    goto/32 :goto_43

    nop

    :goto_8
    iget v0, p0, Landroid/app/TaskInfo;->userId:I

    goto/32 :goto_3e

    nop

    :goto_9
    invoke-virtual {v0, p1, p2}, Landroid/window/WindowContainerToken;->writeToParcel(Landroid/os/Parcel;I)V

    goto/32 :goto_1a

    nop

    :goto_a
    const/4 v1, 0x0

    goto/32 :goto_31

    nop

    :goto_b
    iget-object v0, p0, Landroid/app/TaskInfo;->origActivity:Landroid/content/ComponentName;

    goto/32 :goto_e

    nop

    :goto_c
    iget-object v0, p0, Landroid/app/TaskInfo;->configuration:Landroid/content/res/Configuration;

    goto/32 :goto_2

    nop

    :goto_d
    iget v0, p0, Landroid/app/TaskInfo;->displayAreaFeatureId:I

    goto/32 :goto_3d

    nop

    :goto_e
    invoke-static {v0, p1}, Landroid/content/ComponentName;->writeToParcel(Landroid/content/ComponentName;Landroid/os/Parcel;)V

    goto/32 :goto_15

    nop

    :goto_f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    goto/32 :goto_17

    nop

    :goto_10
    iget v0, p0, Landroid/app/TaskInfo;->cameraCompatControlState:I

    goto/32 :goto_4c

    nop

    :goto_11
    iget-boolean v0, p0, Landroid/app/TaskInfo;->mIsCastMode:Z

    goto/32 :goto_45

    nop

    :goto_12
    iget-object v0, p0, Landroid/app/TaskInfo;->taskDescription:Landroid/app/ActivityManager$TaskDescription;

    goto/32 :goto_34

    nop

    :goto_13
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    goto/32 :goto_0

    nop

    :goto_14
    iget-boolean v0, p0, Landroid/app/TaskInfo;->isVisible:Z

    goto/32 :goto_6

    nop

    :goto_15
    iget-object v0, p0, Landroid/app/TaskInfo;->realActivity:Landroid/content/ComponentName;

    goto/32 :goto_2a

    nop

    :goto_16
    iget-object v0, p0, Landroid/app/TaskInfo;->positionInParent:Landroid/graphics/Point;

    goto/32 :goto_40

    nop

    :goto_17
    iget v0, p0, Landroid/app/TaskInfo;->launchIntoPipHostTaskId:I

    goto/32 :goto_2f

    nop

    :goto_18
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    goto/32 :goto_2c

    nop

    :goto_19
    iget v0, p0, Landroid/app/TaskInfo;->displayId:I

    goto/32 :goto_2b

    nop

    :goto_1a
    iget v0, p0, Landroid/app/TaskInfo;->topActivityType:I

    goto/32 :goto_35

    nop

    :goto_1b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    goto/32 :goto_1

    nop

    :goto_1c
    iget-boolean v0, p0, Landroid/app/TaskInfo;->shouldDockBigOverlays:Z

    goto/32 :goto_f

    nop

    :goto_1d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    goto/32 :goto_38

    nop

    :goto_1e
    iget v0, p0, Landroid/app/TaskInfo;->parentTaskId:I

    goto/32 :goto_36

    nop

    :goto_1f
    invoke-static {v0, p1}, Landroid/content/ComponentName;->writeToParcel(Landroid/content/ComponentName;Landroid/os/Parcel;)V

    goto/32 :goto_5

    nop

    :goto_20
    iget-object v0, p0, Landroid/app/TaskInfo;->baseIntent:Landroid/content/Intent;

    goto/32 :goto_a

    nop

    :goto_21
    iget-object v0, p0, Landroid/app/TaskInfo;->baseActivity:Landroid/content/ComponentName;

    goto/32 :goto_1f

    nop

    :goto_22
    invoke-static {v0, p1}, Landroid/content/ComponentName;->writeToParcel(Landroid/content/ComponentName;Landroid/os/Parcel;)V

    goto/32 :goto_b

    nop

    :goto_23
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    goto/32 :goto_2e

    nop

    :goto_24
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_28

    nop

    :goto_25
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    goto/32 :goto_4f

    nop

    :goto_26
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_42

    nop

    :goto_27
    iget-object v0, p0, Landroid/app/TaskInfo;->topActivityInfo:Landroid/content/pm/ActivityInfo;

    goto/32 :goto_3a

    nop

    :goto_28
    iget v0, p0, Landroid/app/TaskInfo;->minHeight:I

    goto/32 :goto_44

    nop

    :goto_29
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeTypedObject(Landroid/os/Parcelable;I)V

    goto/32 :goto_27

    nop

    :goto_2a
    invoke-static {v0, p1}, Landroid/content/ComponentName;->writeToParcel(Landroid/content/ComponentName;Landroid/os/Parcel;)V

    goto/32 :goto_7

    nop

    :goto_2b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_32

    nop

    :goto_2c
    iget v0, p0, Landroid/app/TaskInfo;->resizeMode:I

    goto/32 :goto_3b

    nop

    :goto_2d
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    goto/32 :goto_12

    nop

    :goto_2e
    iget-boolean v0, p0, Landroid/app/TaskInfo;->topActivityInMiuiSizeCompat:Z

    goto/32 :goto_1b

    nop

    :goto_2f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_30

    nop

    :goto_30
    iget-object v0, p0, Landroid/app/TaskInfo;->displayCutoutInsets:Landroid/graphics/Rect;

    goto/32 :goto_29

    nop

    :goto_31
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeTypedObject(Landroid/os/Parcelable;I)V

    goto/32 :goto_21

    nop

    :goto_32
    iget-boolean v0, p0, Landroid/app/TaskInfo;->isRunning:Z

    goto/32 :goto_47

    nop

    :goto_33
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    goto/32 :goto_14

    nop

    :goto_34
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeTypedObject(Landroid/os/Parcelable;I)V

    goto/32 :goto_4d

    nop

    :goto_35
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_49

    nop

    :goto_36
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_3c

    nop

    :goto_37
    iget-boolean v0, p0, Landroid/app/TaskInfo;->isSleeping:Z

    goto/32 :goto_1d

    nop

    :goto_38
    iget-boolean v0, p0, Landroid/app/TaskInfo;->topActivityInSizeCompat:Z

    goto/32 :goto_23

    nop

    :goto_39
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBinderList(Ljava/util/List;)V

    goto/32 :goto_16

    nop

    :goto_3a
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeTypedObject(Landroid/os/Parcelable;I)V

    goto/32 :goto_51

    nop

    :goto_3b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_c

    nop

    :goto_3c
    iget-boolean v0, p0, Landroid/app/TaskInfo;->isFocused:Z

    goto/32 :goto_33

    nop

    :goto_3d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_10

    nop

    :goto_3e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_4e

    nop

    :goto_3f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_19

    nop

    :goto_40
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeTypedObject(Landroid/os/Parcelable;I)V

    goto/32 :goto_1e

    nop

    :goto_41
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeTypedObject(Landroid/os/Parcelable;I)V

    goto/32 :goto_d

    nop

    :goto_42
    iget-object v0, p0, Landroid/app/TaskInfo;->launchCookies:Ljava/util/ArrayList;

    goto/32 :goto_39

    nop

    :goto_43
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_48

    nop

    :goto_44
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_4

    nop

    :goto_45
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    goto/32 :goto_4b

    nop

    :goto_46
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeTypedObject(Landroid/os/Parcelable;I)V

    goto/32 :goto_1c

    nop

    :goto_47
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    goto/32 :goto_20

    nop

    :goto_48
    iget-wide v0, p0, Landroid/app/TaskInfo;->lastActiveTime:J

    goto/32 :goto_2d

    nop

    :goto_49
    iget-object v0, p0, Landroid/app/TaskInfo;->pictureInPictureParams:Landroid/app/PictureInPictureParams;

    goto/32 :goto_46

    nop

    :goto_4a
    iget-boolean v0, p0, Landroid/app/TaskInfo;->supportsMultiWindow:Z

    goto/32 :goto_18

    nop

    :goto_4b
    return-void

    :goto_4c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/32 :goto_11

    nop

    :goto_4d
    iget-boolean v0, p0, Landroid/app/TaskInfo;->supportsSplitScreenMultiWindow:Z

    goto/32 :goto_50

    nop

    :goto_4e
    iget v0, p0, Landroid/app/TaskInfo;->taskId:I

    goto/32 :goto_3f

    nop

    :goto_4f
    iget v0, p0, Landroid/app/TaskInfo;->minWidth:I

    goto/32 :goto_24

    nop

    :goto_50
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBoolean(Z)V

    goto/32 :goto_4a

    nop

    :goto_51
    iget-boolean v0, p0, Landroid/app/TaskInfo;->isResizeable:Z

    goto/32 :goto_25

    nop
.end method
