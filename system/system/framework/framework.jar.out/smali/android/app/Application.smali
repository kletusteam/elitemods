.class public Landroid/app/Application;
.super Landroid/content/ContextWrapper;

# interfaces
.implements Landroid/content/ComponentCallbacks2;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/Application$OnProvideAssistDataListener;,
        Landroid/app/Application$ActivityLifecycleCallbacks;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Application"


# instance fields
.field private mActivityLifecycleCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/app/Application$ActivityLifecycleCallbacks;",
            ">;"
        }
    .end annotation
.end field

.field private mAssistCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/app/Application$OnProvideAssistDataListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mCallbacksController:Landroid/content/ComponentCallbacksController;

.field public mLoadedApk:Landroid/app/LoadedApk;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Landroid/app/Application;->mActivityLifecycleCallbacks:Ljava/util/ArrayList;

    iput-object v0, p0, Landroid/app/Application;->mAssistCallbacks:Ljava/util/ArrayList;

    new-instance v0, Landroid/content/ComponentCallbacksController;

    invoke-direct {v0}, Landroid/content/ComponentCallbacksController;-><init>()V

    iput-object v0, p0, Landroid/app/Application;->mCallbacksController:Landroid/content/ComponentCallbacksController;

    return-void
.end method

.method private collectActivityLifecycleCallbacks()[Ljava/lang/Object;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/app/Application;->mActivityLifecycleCallbacks:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Landroid/app/Application;->mActivityLifecycleCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    iget-object v2, p0, Landroid/app/Application;->mActivityLifecycleCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    :cond_0
    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private getLoadedApkInfo()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Landroid/app/Application;->mLoadedApk:Landroid/app/LoadedApk;

    if-nez v0, :cond_0

    const-string/jumbo v0, "null"

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Landroid/app/Application;->mLoadedApk:Landroid/app/LoadedApk;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/pkg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/app/Application;->mLoadedApk:Landroid/app/LoadedApk;

    iget-object v1, v1, Landroid/app/LoadedApk;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getProcessName()Ljava/lang/String;
    .locals 1

    invoke-static {}, Landroid/app/ActivityThread;->currentProcessName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method final attach(Landroid/content/Context;)V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    iput-object v0, p0, Landroid/app/Application;->mLoadedApk:Landroid/app/LoadedApk;

    goto/32 :goto_2

    nop

    :goto_1
    invoke-static {p1}, Landroid/app/ContextImpl;->getImpl(Landroid/content/Context;)Landroid/app/ContextImpl;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_2
    return-void

    :goto_3
    invoke-virtual {p0, p1}, Landroid/app/Application;->attachBaseContext(Landroid/content/Context;)V

    goto/32 :goto_1

    nop

    :goto_4
    iget-object v0, v0, Landroid/app/ContextImpl;->mPackageInfo:Landroid/app/LoadedApk;

    goto/32 :goto_0

    nop
.end method

.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/content/ContextWrapper;->attachBaseContext(Landroid/content/Context;)V

    invoke-static {}, Landroid/app/ApplicationStub;->get()Landroid/app/ApplicationStub;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/app/ApplicationStub;->attachBaseContext(Landroid/app/Application;Landroid/content/Context;)V

    return-void
.end method

.method dispatchActivityConfigurationChanged(Landroid/app/Activity;)V
    .locals 3

    goto/32 :goto_c

    nop

    :goto_0
    aget-object v2, v0, v1

    goto/32 :goto_4

    nop

    :goto_1
    array-length v2, v0

    goto/32 :goto_b

    nop

    :goto_2
    goto :goto_7

    :goto_3
    goto/32 :goto_5

    nop

    :goto_4
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    goto/32 :goto_a

    nop

    :goto_5
    return-void

    :goto_6
    const/4 v1, 0x0

    :goto_7
    goto/32 :goto_1

    nop

    :goto_8
    if-nez v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_6

    nop

    :goto_9
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_2

    nop

    :goto_a
    invoke-interface {v2, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityConfigurationChanged(Landroid/app/Activity;)V

    goto/32 :goto_9

    nop

    :goto_b
    if-lt v1, v2, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_0

    nop

    :goto_c
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_8

    nop
.end method

.method dispatchActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 3

    goto/32 :goto_0

    nop

    :goto_0
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_1
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    goto/32 :goto_a

    nop

    :goto_2
    const/4 v1, 0x0

    :goto_3
    goto/32 :goto_7

    nop

    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_b

    nop

    :goto_5
    aget-object v2, v0, v1

    goto/32 :goto_1

    nop

    :goto_6
    return-void

    :goto_7
    array-length v2, v0

    goto/32 :goto_9

    nop

    :goto_8
    if-nez v0, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_2

    nop

    :goto_9
    if-lt v1, v2, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_5

    nop

    :goto_a
    invoke-interface {v2, p1, p2}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V

    goto/32 :goto_4

    nop

    :goto_b
    goto :goto_3

    :goto_c
    goto/32 :goto_6

    nop
.end method

.method dispatchActivityDestroyed(Landroid/app/Activity;)V
    .locals 3

    goto/32 :goto_5

    nop

    :goto_0
    const/4 v1, 0x0

    :goto_1
    goto/32 :goto_a

    nop

    :goto_2
    return-void

    :goto_3
    if-lt v1, v2, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_c

    nop

    :goto_4
    if-nez v0, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_0

    nop

    :goto_5
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_6
    goto :goto_1

    :goto_7
    goto/32 :goto_2

    nop

    :goto_8
    invoke-interface {v2, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityDestroyed(Landroid/app/Activity;)V

    goto/32 :goto_9

    nop

    :goto_9
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_6

    nop

    :goto_a
    array-length v2, v0

    goto/32 :goto_3

    nop

    :goto_b
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    goto/32 :goto_8

    nop

    :goto_c
    aget-object v2, v0, v1

    goto/32 :goto_b

    nop
.end method

.method dispatchActivityPaused(Landroid/app/Activity;)V
    .locals 3

    goto/32 :goto_7

    nop

    :goto_0
    array-length v2, v0

    goto/32 :goto_5

    nop

    :goto_1
    aget-object v2, v0, v1

    goto/32 :goto_6

    nop

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_3

    nop

    :goto_3
    goto :goto_c

    :goto_4
    goto/32 :goto_8

    nop

    :goto_5
    if-lt v1, v2, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_1

    nop

    :goto_6
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    goto/32 :goto_9

    nop

    :goto_7
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_a

    nop

    :goto_8
    return-void

    :goto_9
    invoke-interface {v2, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityPaused(Landroid/app/Activity;)V

    goto/32 :goto_2

    nop

    :goto_a
    if-nez v0, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_b

    nop

    :goto_b
    const/4 v1, 0x0

    :goto_c
    goto/32 :goto_0

    nop
.end method

.method dispatchActivityPostCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 3

    goto/32 :goto_b

    nop

    :goto_0
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_7

    nop

    :goto_1
    if-lt v1, v2, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_a

    nop

    :goto_2
    const/4 v1, 0x0

    :goto_3
    goto/32 :goto_c

    nop

    :goto_4
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    goto/32 :goto_6

    nop

    :goto_5
    return-void

    :goto_6
    invoke-interface {v2, p1, p2}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityPostCreated(Landroid/app/Activity;Landroid/os/Bundle;)V

    goto/32 :goto_0

    nop

    :goto_7
    goto :goto_3

    :goto_8
    goto/32 :goto_5

    nop

    :goto_9
    if-nez v0, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_2

    nop

    :goto_a
    aget-object v2, v0, v1

    goto/32 :goto_4

    nop

    :goto_b
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_c
    array-length v2, v0

    goto/32 :goto_1

    nop
.end method

.method dispatchActivityPostDestroyed(Landroid/app/Activity;)V
    .locals 3

    goto/32 :goto_2

    nop

    :goto_0
    goto :goto_4

    :goto_1
    goto/32 :goto_7

    nop

    :goto_2
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_a

    nop

    :goto_3
    const/4 v1, 0x0

    :goto_4
    goto/32 :goto_8

    nop

    :goto_5
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    goto/32 :goto_6

    nop

    :goto_6
    invoke-interface {v2, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityPostDestroyed(Landroid/app/Activity;)V

    goto/32 :goto_b

    nop

    :goto_7
    return-void

    :goto_8
    array-length v2, v0

    goto/32 :goto_9

    nop

    :goto_9
    if-lt v1, v2, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_c

    nop

    :goto_a
    if-nez v0, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_3

    nop

    :goto_b
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_0

    nop

    :goto_c
    aget-object v2, v0, v1

    goto/32 :goto_5

    nop
.end method

.method dispatchActivityPostPaused(Landroid/app/Activity;)V
    .locals 3

    goto/32 :goto_6

    nop

    :goto_0
    return-void

    :goto_1
    invoke-interface {v2, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityPostPaused(Landroid/app/Activity;)V

    goto/32 :goto_4

    nop

    :goto_2
    goto :goto_a

    :goto_3
    goto/32 :goto_0

    nop

    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_2

    nop

    :goto_5
    if-lt v1, v2, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_c

    nop

    :goto_6
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_7
    if-nez v0, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_9

    nop

    :goto_8
    array-length v2, v0

    goto/32 :goto_5

    nop

    :goto_9
    const/4 v1, 0x0

    :goto_a
    goto/32 :goto_8

    nop

    :goto_b
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    goto/32 :goto_1

    nop

    :goto_c
    aget-object v2, v0, v1

    goto/32 :goto_b

    nop
.end method

.method dispatchActivityPostResumed(Landroid/app/Activity;)V
    .locals 3

    goto/32 :goto_0

    nop

    :goto_0
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_1
    if-lt v1, v2, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_8

    nop

    :goto_2
    array-length v2, v0

    goto/32 :goto_1

    nop

    :goto_3
    return-void

    :goto_4
    if-nez v0, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_5

    nop

    :goto_5
    const/4 v1, 0x0

    :goto_6
    goto/32 :goto_2

    nop

    :goto_7
    invoke-interface {v2, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityPostResumed(Landroid/app/Activity;)V

    goto/32 :goto_b

    nop

    :goto_8
    aget-object v2, v0, v1

    goto/32 :goto_c

    nop

    :goto_9
    goto :goto_6

    :goto_a
    goto/32 :goto_3

    nop

    :goto_b
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_9

    nop

    :goto_c
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    goto/32 :goto_7

    nop
.end method

.method dispatchActivityPostSaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 3

    goto/32 :goto_2

    nop

    :goto_0
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    goto/32 :goto_6

    nop

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_8

    nop

    :goto_2
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_3
    array-length v2, v0

    goto/32 :goto_c

    nop

    :goto_4
    goto :goto_9

    :goto_5
    goto/32 :goto_7

    nop

    :goto_6
    invoke-interface {v2, p1, p2}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityPostSaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V

    goto/32 :goto_a

    nop

    :goto_7
    return-void

    :goto_8
    const/4 v1, 0x0

    :goto_9
    goto/32 :goto_3

    nop

    :goto_a
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_4

    nop

    :goto_b
    aget-object v2, v0, v1

    goto/32 :goto_0

    nop

    :goto_c
    if-lt v1, v2, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_b

    nop
.end method

.method dispatchActivityPostStarted(Landroid/app/Activity;)V
    .locals 3

    goto/32 :goto_8

    nop

    :goto_0
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_9

    nop

    :goto_1
    if-lt v1, v2, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_2

    nop

    :goto_2
    aget-object v2, v0, v1

    goto/32 :goto_5

    nop

    :goto_3
    const/4 v1, 0x0

    :goto_4
    goto/32 :goto_c

    nop

    :goto_5
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    goto/32 :goto_7

    nop

    :goto_6
    if-nez v0, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_3

    nop

    :goto_7
    invoke-interface {v2, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityPostStarted(Landroid/app/Activity;)V

    goto/32 :goto_0

    nop

    :goto_8
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_9
    goto :goto_4

    :goto_a
    goto/32 :goto_b

    nop

    :goto_b
    return-void

    :goto_c
    array-length v2, v0

    goto/32 :goto_1

    nop
.end method

.method dispatchActivityPostStopped(Landroid/app/Activity;)V
    .locals 3

    goto/32 :goto_2

    nop

    :goto_0
    if-lt v1, v2, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_6

    nop

    :goto_1
    invoke-interface {v2, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityPostStopped(Landroid/app/Activity;)V

    goto/32 :goto_7

    nop

    :goto_2
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_3
    goto :goto_a

    :goto_4
    goto/32 :goto_b

    nop

    :goto_5
    if-nez v0, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_9

    nop

    :goto_6
    aget-object v2, v0, v1

    goto/32 :goto_c

    nop

    :goto_7
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_3

    nop

    :goto_8
    array-length v2, v0

    goto/32 :goto_0

    nop

    :goto_9
    const/4 v1, 0x0

    :goto_a
    goto/32 :goto_8

    nop

    :goto_b
    return-void

    :goto_c
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    goto/32 :goto_1

    nop
.end method

.method dispatchActivityPreCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 3

    goto/32 :goto_4

    nop

    :goto_0
    invoke-interface {v2, p1, p2}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityPreCreated(Landroid/app/Activity;Landroid/os/Bundle;)V

    goto/32 :goto_9

    nop

    :goto_1
    aget-object v2, v0, v1

    goto/32 :goto_3

    nop

    :goto_2
    array-length v2, v0

    goto/32 :goto_7

    nop

    :goto_3
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    goto/32 :goto_0

    nop

    :goto_4
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_c

    nop

    :goto_5
    goto :goto_b

    :goto_6
    goto/32 :goto_8

    nop

    :goto_7
    if-lt v1, v2, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_1

    nop

    :goto_8
    return-void

    :goto_9
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_5

    nop

    :goto_a
    const/4 v1, 0x0

    :goto_b
    goto/32 :goto_2

    nop

    :goto_c
    if-nez v0, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_a

    nop
.end method

.method dispatchActivityPreDestroyed(Landroid/app/Activity;)V
    .locals 3

    goto/32 :goto_6

    nop

    :goto_0
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_9

    nop

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_b

    nop

    :goto_2
    invoke-interface {v2, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityPreDestroyed(Landroid/app/Activity;)V

    goto/32 :goto_0

    nop

    :goto_3
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    goto/32 :goto_2

    nop

    :goto_4
    if-lt v1, v2, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_8

    nop

    :goto_5
    array-length v2, v0

    goto/32 :goto_4

    nop

    :goto_6
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_7
    return-void

    :goto_8
    aget-object v2, v0, v1

    goto/32 :goto_3

    nop

    :goto_9
    goto :goto_c

    :goto_a
    goto/32 :goto_7

    nop

    :goto_b
    const/4 v1, 0x0

    :goto_c
    goto/32 :goto_5

    nop
.end method

.method dispatchActivityPrePaused(Landroid/app/Activity;)V
    .locals 3

    goto/32 :goto_1

    nop

    :goto_0
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_8

    nop

    :goto_1
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_a

    nop

    :goto_2
    aget-object v2, v0, v1

    goto/32 :goto_4

    nop

    :goto_3
    invoke-interface {v2, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityPrePaused(Landroid/app/Activity;)V

    goto/32 :goto_0

    nop

    :goto_4
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    goto/32 :goto_3

    nop

    :goto_5
    const/4 v1, 0x0

    :goto_6
    goto/32 :goto_7

    nop

    :goto_7
    array-length v2, v0

    goto/32 :goto_b

    nop

    :goto_8
    goto :goto_6

    :goto_9
    goto/32 :goto_c

    nop

    :goto_a
    if-nez v0, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_5

    nop

    :goto_b
    if-lt v1, v2, :cond_1

    goto/32 :goto_9

    :cond_1
    goto/32 :goto_2

    nop

    :goto_c
    return-void
.end method

.method dispatchActivityPreResumed(Landroid/app/Activity;)V
    .locals 3

    goto/32 :goto_b

    nop

    :goto_0
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    goto/32 :goto_c

    nop

    :goto_1
    if-lt v1, v2, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_7

    nop

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_8

    nop

    :goto_3
    array-length v2, v0

    goto/32 :goto_1

    nop

    :goto_4
    if-nez v0, :cond_1

    goto/32 :goto_9

    :cond_1
    goto/32 :goto_5

    nop

    :goto_5
    const/4 v1, 0x0

    :goto_6
    goto/32 :goto_3

    nop

    :goto_7
    aget-object v2, v0, v1

    goto/32 :goto_0

    nop

    :goto_8
    goto :goto_6

    :goto_9
    goto/32 :goto_a

    nop

    :goto_a
    return-void

    :goto_b
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_c
    invoke-interface {v2, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityPreResumed(Landroid/app/Activity;)V

    goto/32 :goto_2

    nop
.end method

.method dispatchActivityPreSaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 3

    goto/32 :goto_0

    nop

    :goto_0
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_b

    nop

    :goto_1
    return-void

    :goto_2
    invoke-interface {v2, p1, p2}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityPreSaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V

    goto/32 :goto_3

    nop

    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_5

    nop

    :goto_4
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    goto/32 :goto_2

    nop

    :goto_5
    goto :goto_8

    :goto_6
    goto/32 :goto_1

    nop

    :goto_7
    const/4 v1, 0x0

    :goto_8
    goto/32 :goto_9

    nop

    :goto_9
    array-length v2, v0

    goto/32 :goto_a

    nop

    :goto_a
    if-lt v1, v2, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_c

    nop

    :goto_b
    if-nez v0, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_7

    nop

    :goto_c
    aget-object v2, v0, v1

    goto/32 :goto_4

    nop
.end method

.method dispatchActivityPreStarted(Landroid/app/Activity;)V
    .locals 3

    goto/32 :goto_0

    nop

    :goto_0
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_a

    nop

    :goto_3
    array-length v2, v0

    goto/32 :goto_5

    nop

    :goto_4
    invoke-interface {v2, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityPreStarted(Landroid/app/Activity;)V

    goto/32 :goto_7

    nop

    :goto_5
    if-lt v1, v2, :cond_1

    goto/32 :goto_9

    :cond_1
    goto/32 :goto_6

    nop

    :goto_6
    aget-object v2, v0, v1

    goto/32 :goto_c

    nop

    :goto_7
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_8

    nop

    :goto_8
    goto :goto_b

    :goto_9
    goto/32 :goto_1

    nop

    :goto_a
    const/4 v1, 0x0

    :goto_b
    goto/32 :goto_3

    nop

    :goto_c
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    goto/32 :goto_4

    nop
.end method

.method dispatchActivityPreStopped(Landroid/app/Activity;)V
    .locals 3

    goto/32 :goto_c

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_2

    nop

    :goto_1
    array-length v2, v0

    goto/32 :goto_9

    nop

    :goto_2
    const/4 v1, 0x0

    :goto_3
    goto/32 :goto_1

    nop

    :goto_4
    aget-object v2, v0, v1

    goto/32 :goto_8

    nop

    :goto_5
    goto :goto_3

    :goto_6
    goto/32 :goto_b

    nop

    :goto_7
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_5

    nop

    :goto_8
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    goto/32 :goto_a

    nop

    :goto_9
    if-lt v1, v2, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_4

    nop

    :goto_a
    invoke-interface {v2, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityPreStopped(Landroid/app/Activity;)V

    goto/32 :goto_7

    nop

    :goto_b
    return-void

    :goto_c
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_0

    nop
.end method

.method dispatchActivityResumed(Landroid/app/Activity;)V
    .locals 3

    goto/32 :goto_7

    nop

    :goto_0
    return-void

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_2

    nop

    :goto_2
    goto :goto_6

    :goto_3
    goto/32 :goto_0

    nop

    :goto_4
    invoke-interface {v2, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityResumed(Landroid/app/Activity;)V

    goto/32 :goto_1

    nop

    :goto_5
    const/4 v1, 0x0

    :goto_6
    goto/32 :goto_a

    nop

    :goto_7
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_b

    nop

    :goto_8
    if-lt v1, v2, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_9

    nop

    :goto_9
    aget-object v2, v0, v1

    goto/32 :goto_c

    nop

    :goto_a
    array-length v2, v0

    goto/32 :goto_8

    nop

    :goto_b
    if-nez v0, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_5

    nop

    :goto_c
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    goto/32 :goto_4

    nop
.end method

.method dispatchActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 3

    goto/32 :goto_9

    nop

    :goto_0
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    goto/32 :goto_8

    nop

    :goto_1
    goto :goto_7

    :goto_2
    goto/32 :goto_5

    nop

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_6

    nop

    :goto_4
    array-length v2, v0

    goto/32 :goto_a

    nop

    :goto_5
    return-void

    :goto_6
    const/4 v1, 0x0

    :goto_7
    goto/32 :goto_4

    nop

    :goto_8
    invoke-interface {v2, p1, p2}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V

    goto/32 :goto_c

    nop

    :goto_9
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_a
    if-lt v1, v2, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_b

    nop

    :goto_b
    aget-object v2, v0, v1

    goto/32 :goto_0

    nop

    :goto_c
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_1

    nop
.end method

.method dispatchActivityStarted(Landroid/app/Activity;)V
    .locals 3

    goto/32 :goto_c

    nop

    :goto_0
    return-void

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_5

    nop

    :goto_2
    array-length v2, v0

    goto/32 :goto_a

    nop

    :goto_3
    invoke-interface {v2, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityStarted(Landroid/app/Activity;)V

    goto/32 :goto_b

    nop

    :goto_4
    aget-object v2, v0, v1

    goto/32 :goto_9

    nop

    :goto_5
    const/4 v1, 0x0

    :goto_6
    goto/32 :goto_2

    nop

    :goto_7
    goto :goto_6

    :goto_8
    goto/32 :goto_0

    nop

    :goto_9
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    goto/32 :goto_3

    nop

    :goto_a
    if-lt v1, v2, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_4

    nop

    :goto_b
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_7

    nop

    :goto_c
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_1

    nop
.end method

.method dispatchActivityStopped(Landroid/app/Activity;)V
    .locals 3

    goto/32 :goto_3

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_6

    nop

    :goto_1
    aget-object v2, v0, v1

    goto/32 :goto_c

    nop

    :goto_2
    if-lt v1, v2, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_1

    nop

    :goto_3
    invoke-direct {p0}, Landroid/app/Application;->collectActivityLifecycleCallbacks()[Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_4
    array-length v2, v0

    goto/32 :goto_2

    nop

    :goto_5
    return-void

    :goto_6
    const/4 v1, 0x0

    :goto_7
    goto/32 :goto_4

    nop

    :goto_8
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_a

    nop

    :goto_9
    invoke-interface {v2, p1}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityStopped(Landroid/app/Activity;)V

    goto/32 :goto_8

    nop

    :goto_a
    goto :goto_7

    :goto_b
    goto/32 :goto_5

    nop

    :goto_c
    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    goto/32 :goto_9

    nop
.end method

.method dispatchOnProvideAssistData(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 3

    goto/32 :goto_c

    nop

    :goto_0
    const/4 v1, 0x0

    :goto_1
    goto/32 :goto_2

    nop

    :goto_2
    array-length v2, v0

    goto/32 :goto_d

    nop

    :goto_3
    invoke-interface {v2, p1, p2}, Landroid/app/Application$OnProvideAssistDataListener;->onProvideAssistData(Landroid/app/Activity;Landroid/os/Bundle;)V

    goto/32 :goto_8

    nop

    :goto_4
    goto :goto_1

    :goto_5
    goto/32 :goto_b

    nop

    :goto_6
    throw v0

    :goto_7
    check-cast v2, Landroid/app/Application$OnProvideAssistDataListener;

    goto/32 :goto_3

    nop

    :goto_8
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_4

    nop

    :goto_9
    aget-object v2, v0, v1

    goto/32 :goto_7

    nop

    :goto_a
    if-nez v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_0

    nop

    :goto_b
    return-void

    :catchall_0
    move-exception v0

    :try_start_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_6

    nop

    :goto_c
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Landroid/app/Application;->mAssistCallbacks:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    monitor-exit p0

    return-void

    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/32 :goto_a

    nop

    :goto_d
    if-lt v1, v2, :cond_2

    goto/32 :goto_5

    :cond_2
    goto/32 :goto_9

    nop
.end method

.method public getAutofillClient()Landroid/view/autofill/AutofillManager$AutofillClient;
    .locals 9

    invoke-super {p0}, Landroid/content/ContextWrapper;->getAutofillClient()Landroid/view/autofill/AutofillManager$AutofillClient;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    sget-boolean v1, Landroid/view/autofill/Helper;->sVerbose:Z

    const-string v2, "Application"

    if-eqz v1, :cond_1

    const-string v1, "getAutofillClient(): null on super, trying to find activity thread"

    invoke-static {v2, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    move-result-object v1

    const/4 v3, 0x0

    if-nez v1, :cond_2

    return-object v3

    :cond_2
    iget-object v4, v1, Landroid/app/ActivityThread;->mActivities:Landroid/util/ArrayMap;

    invoke-virtual {v4}, Landroid/util/ArrayMap;->size()I

    move-result v4

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v4, :cond_7

    iget-object v6, v1, Landroid/app/ActivityThread;->mActivities:Landroid/util/ArrayMap;

    invoke-virtual {v6, v5}, Landroid/util/ArrayMap;->valueAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/ActivityThread$ActivityClientRecord;

    if-nez v6, :cond_3

    goto :goto_1

    :cond_3
    iget-object v7, v6, Landroid/app/ActivityThread$ActivityClientRecord;->activity:Landroid/app/Activity;

    if-nez v7, :cond_4

    goto :goto_1

    :cond_4
    invoke-virtual {v7}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->hasFocus()Z

    move-result v8

    if-eqz v8, :cond_6

    sget-boolean v3, Landroid/view/autofill/Helper;->sVerbose:Z

    if-eqz v3, :cond_5

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getAutofillClient(): found activity for "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, ": "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    invoke-virtual {v7}, Landroid/app/Activity;->getAutofillClient()Landroid/view/autofill/AutofillManager$AutofillClient;

    move-result-object v2

    return-object v2

    :cond_6
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_7
    sget-boolean v5, Landroid/view/autofill/Helper;->sVerbose:Z

    if-eqz v5, :cond_8

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getAutofillClient(): none of the "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " activities on "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " have focus"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    return-object v3
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    iget-object v0, p0, Landroid/app/Application;->mCallbacksController:Landroid/content/ComponentCallbacksController;

    invoke-virtual {v0, p1}, Landroid/content/ComponentCallbacksController;->dispatchConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onCreate()V
    .locals 0

    return-void
.end method

.method public onLowMemory()V
    .locals 1

    iget-object v0, p0, Landroid/app/Application;->mCallbacksController:Landroid/content/ComponentCallbacksController;

    invoke-virtual {v0}, Landroid/content/ComponentCallbacksController;->dispatchLowMemory()V

    return-void
.end method

.method public onTerminate()V
    .locals 0

    return-void
.end method

.method public onTrimMemory(I)V
    .locals 1

    iget-object v0, p0, Landroid/app/Application;->mCallbacksController:Landroid/content/ComponentCallbacksController;

    invoke-virtual {v0, p1}, Landroid/content/ComponentCallbacksController;->dispatchTrimMemory(I)V

    return-void
.end method

.method public registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V
    .locals 2

    iget-object v0, p0, Landroid/app/Application;->mActivityLifecycleCallbacks:Ljava/util/ArrayList;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Landroid/app/Application;->mActivityLifecycleCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V
    .locals 1

    iget-object v0, p0, Landroid/app/Application;->mCallbacksController:Landroid/content/ComponentCallbacksController;

    invoke-virtual {v0, p1}, Landroid/content/ComponentCallbacksController;->registerCallbacks(Landroid/content/ComponentCallbacks;)V

    return-void
.end method

.method public registerMiuiActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V
    .locals 2

    iget-object v0, p0, Landroid/app/Application;->mActivityLifecycleCallbacks:Ljava/util/ArrayList;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Landroid/app/Application;->mActivityLifecycleCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public registerOnProvideAssistDataListener(Landroid/app/Application$OnProvideAssistDataListener;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Landroid/app/Application;->mAssistCallbacks:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/app/Application;->mAssistCallbacks:Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Landroid/app/Application;->mAssistCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V
    .locals 2

    iget-object v0, p0, Landroid/app/Application;->mActivityLifecycleCallbacks:Ljava/util/ArrayList;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Landroid/app/Application;->mActivityLifecycleCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public unregisterComponentCallbacks(Landroid/content/ComponentCallbacks;)V
    .locals 1

    iget-object v0, p0, Landroid/app/Application;->mCallbacksController:Landroid/content/ComponentCallbacksController;

    invoke-virtual {v0, p1}, Landroid/content/ComponentCallbacksController;->unregisterCallbacks(Landroid/content/ComponentCallbacks;)V

    return-void
.end method

.method public unregisterOnProvideAssistDataListener(Landroid/app/Application$OnProvideAssistDataListener;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Landroid/app/Application;->mAssistCallbacks:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
