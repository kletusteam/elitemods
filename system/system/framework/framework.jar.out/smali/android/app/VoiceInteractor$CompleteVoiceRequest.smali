.class public Landroid/app/VoiceInteractor$CompleteVoiceRequest;
.super Landroid/app/VoiceInteractor$Request;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/VoiceInteractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CompleteVoiceRequest"
.end annotation


# instance fields
.field final mExtras:Landroid/os/Bundle;

.field final mPrompt:Landroid/app/VoiceInteractor$Prompt;


# direct methods
.method public constructor <init>(Landroid/app/VoiceInteractor$Prompt;Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0}, Landroid/app/VoiceInteractor$Request;-><init>()V

    iput-object p1, p0, Landroid/app/VoiceInteractor$CompleteVoiceRequest;->mPrompt:Landroid/app/VoiceInteractor$Prompt;

    iput-object p2, p0, Landroid/app/VoiceInteractor$CompleteVoiceRequest;->mExtras:Landroid/os/Bundle;

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Landroid/os/Bundle;)V
    .locals 1

    invoke-direct {p0}, Landroid/app/VoiceInteractor$Request;-><init>()V

    if-eqz p1, :cond_0

    new-instance v0, Landroid/app/VoiceInteractor$Prompt;

    invoke-direct {v0, p1}, Landroid/app/VoiceInteractor$Prompt;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Landroid/app/VoiceInteractor$CompleteVoiceRequest;->mPrompt:Landroid/app/VoiceInteractor$Prompt;

    iput-object p2, p0, Landroid/app/VoiceInteractor$CompleteVoiceRequest;->mExtras:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    goto/32 :goto_9

    nop

    :goto_0
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    goto/32 :goto_4

    nop

    :goto_1
    iget-object v0, p0, Landroid/app/VoiceInteractor$CompleteVoiceRequest;->mExtras:Landroid/os/Bundle;

    goto/32 :goto_b

    nop

    :goto_2
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_3

    nop

    :goto_3
    iget-object v0, p0, Landroid/app/VoiceInteractor$CompleteVoiceRequest;->mPrompt:Landroid/app/VoiceInteractor$Prompt;

    goto/32 :goto_0

    nop

    :goto_4
    iget-object v0, p0, Landroid/app/VoiceInteractor$CompleteVoiceRequest;->mExtras:Landroid/os/Bundle;

    goto/32 :goto_7

    nop

    :goto_5
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_e

    nop

    :goto_6
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_d

    nop

    :goto_7
    if-nez v0, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_5

    nop

    :goto_8
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto/32 :goto_1

    nop

    :goto_9
    invoke-super {p0, p1, p2, p3, p4}, Landroid/app/VoiceInteractor$Request;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    goto/32 :goto_6

    nop

    :goto_a
    return-void

    :goto_b
    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :goto_c
    goto/32 :goto_a

    nop

    :goto_d
    const-string/jumbo v0, "mPrompt="

    goto/32 :goto_2

    nop

    :goto_e
    const-string/jumbo v0, "mExtras="

    goto/32 :goto_8

    nop
.end method

.method getRequestTypeName()Ljava/lang/String;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    const-string v0, "CompleteVoice"

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method public onCompleteResult(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method submit(Lcom/android/internal/app/IVoiceInteractor;Ljava/lang/String;Lcom/android/internal/app/IVoiceInteractorCallback;)Lcom/android/internal/app/IVoiceInteractorRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    iget-object v1, p0, Landroid/app/VoiceInteractor$CompleteVoiceRequest;->mExtras:Landroid/os/Bundle;

    goto/32 :goto_3

    nop

    :goto_1
    return-object v0

    :goto_2
    iget-object v0, p0, Landroid/app/VoiceInteractor$CompleteVoiceRequest;->mPrompt:Landroid/app/VoiceInteractor$Prompt;

    goto/32 :goto_0

    nop

    :goto_3
    invoke-interface {p1, p2, p3, v0, v1}, Lcom/android/internal/app/IVoiceInteractor;->startCompleteVoice(Ljava/lang/String;Lcom/android/internal/app/IVoiceInteractorCallback;Landroid/app/VoiceInteractor$Prompt;Landroid/os/Bundle;)Lcom/android/internal/app/IVoiceInteractorRequest;

    move-result-object v0

    goto/32 :goto_1

    nop
.end method
