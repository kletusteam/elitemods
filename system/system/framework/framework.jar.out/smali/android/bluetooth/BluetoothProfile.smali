.class public interface abstract Landroid/bluetooth/BluetoothProfile;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/bluetooth/BluetoothProfile$ServiceListener;,
        Landroid/bluetooth/BluetoothProfile$ConnectionPolicy;,
        Landroid/bluetooth/BluetoothProfile$BtProfileState;
    }
.end annotation


# static fields
.field public static final A2DP:I = 0x2

.field public static final A2DP_SINK:I = 0xb
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation
.end field

.field public static final AVRCP:I = 0xd

.field public static final AVRCP_CONTROLLER:I = 0xc
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation
.end field

.field public static final BATTERY:I = 0x1e

.field public static final BC_PROFILE:I = 0x23

.field public static final BROADCAST:I = 0x21

.field public static final CC_SERVER:I = 0x25

.field public static final CONNECTION_POLICY_ALLOWED:I = 0x64
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation
.end field

.field public static final CONNECTION_POLICY_FORBIDDEN:I = 0x0
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation
.end field

.field public static final CONNECTION_POLICY_UNKNOWN:I = -0x1
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation
.end field

.field public static final COORDINATED_AUDIO_UNICAST:I = 0x26

.field public static final CSIP_SET_COORDINATOR:I = 0x19

.field public static final DUN:I = 0x1f

.field public static final EXTRA_PREVIOUS_STATE:Ljava/lang/String; = "android.bluetooth.profile.extra.PREVIOUS_STATE"

.field public static final EXTRA_STATE:Ljava/lang/String; = "android.bluetooth.profile.extra.STATE"

.field public static final GATT:I = 0x7

.field public static final GATT_SERVER:I = 0x8

.field public static final GROUP_CLIENT:I = 0x20

.field public static final HAP_CLIENT:I = 0x1c

.field public static final HEADSET:I = 0x1

.field public static final HEADSET_CLIENT:I = 0x10
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation
.end field

.field public static final HEALTH:I = 0x3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final HEARING_AID:I = 0x15

.field public static final HID_DEVICE:I = 0x13

.field public static final HID_HOST:I = 0x4
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation
.end field

.field public static final LE_AUDIO:I = 0x16

.field public static final LE_AUDIO_BROADCAST:I = 0x1a
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation
.end field

.field public static final LE_AUDIO_BROADCAST_ASSISTANT:I = 0x1d
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation
.end field

.field public static final LE_CALL_CONTROL:I = 0x1b

.field public static final MAP:I = 0x9
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation
.end field

.field public static final MAP_CLIENT:I = 0x12
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation
.end field

.field public static final MAX_PROFILE_ID:I = 0x26

.field public static final MCP_SERVER:I = 0x18

.field public static final OPP:I = 0x14
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation
.end field

.field public static final PAN:I = 0x5
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation
.end field

.field public static final PBAP:I = 0x6
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation
.end field

.field public static final PBAP_CLIENT:I = 0x11
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation
.end field

.field public static final PC_PROFILE:I = 0x24

.field public static final PRIORITY_AUTO_CONNECT:I = 0x3e8

.field public static final PRIORITY_OFF:I = 0x0
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PRIORITY_ON:I = 0x64
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PRIORITY_UNDEFINED:I = -0x1

.field public static final SAP:I = 0xa

.field public static final STATE_CONNECTED:I = 0x2

.field public static final STATE_CONNECTING:I = 0x1

.field public static final STATE_DISCONNECTED:I = 0x0

.field public static final STATE_DISCONNECTING:I = 0x3

.field public static final VCP:I = 0x22

.field public static final VOLUME_CONTROL:I = 0x17
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation
.end field


# direct methods
.method public static getConnectionStateName(I)Ljava/lang/String;
    .locals 1
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    packed-switch p0, :pswitch_data_0

    const-string v0, "STATE_UNKNOWN"

    return-object v0

    :pswitch_0
    const-string v0, "STATE_DISCONNECTING"

    return-object v0

    :pswitch_1
    const-string v0, "STATE_CONNECTED"

    return-object v0

    :pswitch_2
    const-string v0, "STATE_CONNECTING"

    return-object v0

    :pswitch_3
    const-string v0, "STATE_DISCONNECTED"

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getProfileName(I)Ljava/lang/String;
    .locals 1
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const-string v0, "UNKNOWN_PROFILE"

    return-object v0

    :pswitch_1
    const-string v0, "CC_SERVER"

    return-object v0

    :pswitch_2
    const-string v0, "PC_PROFILE"

    return-object v0

    :pswitch_3
    const-string v0, "BC_PROFILE"

    return-object v0

    :pswitch_4
    const-string v0, "VCP"

    return-object v0

    :pswitch_5
    const-string v0, "BROADCAST"

    return-object v0

    :pswitch_6
    const-string v0, "GROUP_CLIENT"

    return-object v0

    :pswitch_7
    const-string v0, "DUN"

    return-object v0

    :pswitch_8
    const-string v0, "BATTERY"

    return-object v0

    :pswitch_9
    const-string v0, "LE_AUDIO_BROADCAST_ASSISTANT"

    return-object v0

    :pswitch_a
    const-string v0, "HAP_CLIENT"

    return-object v0

    :pswitch_b
    const-string v0, "LE_CALL_CONTROL"

    return-object v0

    :pswitch_c
    const-string v0, "LE_AUDIO_BROADCAST"

    return-object v0

    :pswitch_d
    const-string v0, "CSIP_SET_COORDINATOR"

    return-object v0

    :pswitch_e
    const-string v0, "MCP_SERVER"

    return-object v0

    :pswitch_f
    const-string v0, "VOLUME_CONTROL"

    return-object v0

    :pswitch_10
    const-string v0, "LE_AUDIO"

    return-object v0

    :pswitch_11
    const-string v0, "HEARING_AID"

    return-object v0

    :pswitch_12
    const-string v0, "OPP"

    return-object v0

    :pswitch_13
    const-string v0, "HID_DEVICE"

    return-object v0

    :pswitch_14
    const-string v0, "MAP_CLIENT"

    return-object v0

    :pswitch_15
    const-string v0, "PBAP_CLIENT"

    return-object v0

    :pswitch_16
    const-string v0, "HEADSET_CLIENT"

    return-object v0

    :pswitch_17
    const-string v0, "AVRCP"

    return-object v0

    :pswitch_18
    const-string v0, "AVRCP_CONTROLLER"

    return-object v0

    :pswitch_19
    const-string v0, "A2DP_SINK"

    return-object v0

    :pswitch_1a
    const-string v0, "SAP"

    return-object v0

    :pswitch_1b
    const-string v0, "MAP"

    return-object v0

    :pswitch_1c
    const-string v0, "GATT_SERVER"

    return-object v0

    :pswitch_1d
    const-string v0, "GATT"

    return-object v0

    :pswitch_1e
    const-string v0, "PBAP"

    return-object v0

    :pswitch_1f
    const-string v0, "PAN"

    return-object v0

    :pswitch_20
    const-string v0, "HID_HOST"

    return-object v0

    :pswitch_21
    const-string v0, "A2DP"

    return-object v0

    :pswitch_22
    const-string v0, "HEADSET"

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_22
        :pswitch_21
        :pswitch_0
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_0
        :pswitch_0
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public abstract getConnectedDevices()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getConnectionState(Landroid/bluetooth/BluetoothDevice;)I
.end method

.method public abstract getDevicesMatchingConnectionStates([I)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation
.end method
