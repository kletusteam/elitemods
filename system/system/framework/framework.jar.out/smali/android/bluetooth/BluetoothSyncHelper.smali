.class public final Landroid/bluetooth/BluetoothSyncHelper;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/bluetooth/BluetoothProfile;


# static fields
.field public static final ACTION_CONNECTION_STATE_CHANGED:Ljava/lang/String; = "android.bluetooth.bc.profile.action.CONNECTION_STATE_CHANGED"

.field private static final DBG:Z = true

.field private static final TAG:Ljava/lang/String; = "BluetoothSyncHelper"


# instance fields
.field private mAppCallbackWrappers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Landroid/bluetooth/BleBroadcastAudioScanAssistCallback;",
            "Landroid/bluetooth/IBleBroadcastAudioScanAssistCallback;",
            ">;"
        }
    .end annotation
.end field

.field private final mAttributionSource:Landroid/content/AttributionSource;

.field private mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mContext:Landroid/content/Context;

.field private final mProfileConnector:Landroid/bluetooth/BluetoothProfileConnector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/bluetooth/BluetoothProfileConnector<",
            "Landroid/bluetooth/IBluetoothSyncHelper;",
            ">;"
        }
    .end annotation
.end field

.field private sBleAssistManagers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Landroid/bluetooth/BluetoothDevice;",
            "Landroid/bluetooth/BleBroadcastAudioScanAssistManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$smlog(Ljava/lang/String;)V
    .locals 0

    invoke-static {p0}, Landroid/bluetooth/BluetoothSyncHelper;->log(Ljava/lang/String;)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;)V
    .locals 7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/bluetooth/BluetoothSyncHelper;->sBleAssistManagers:Ljava/util/Map;

    iput-object v0, p0, Landroid/bluetooth/BluetoothSyncHelper;->mContext:Landroid/content/Context;

    new-instance v0, Landroid/bluetooth/BluetoothSyncHelper$1;

    const-class v1, Landroid/bluetooth/IBluetoothSyncHelper;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    const/16 v4, 0x23

    const-string v5, "BluetoothSyncHelper"

    move-object v1, v0

    move-object v2, p0

    move-object v3, p0

    invoke-direct/range {v1 .. v6}, Landroid/bluetooth/BluetoothSyncHelper$1;-><init>(Landroid/bluetooth/BluetoothSyncHelper;Landroid/bluetooth/BluetoothProfile;ILjava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Landroid/bluetooth/BluetoothSyncHelper;->mProfileConnector:Landroid/bluetooth/BluetoothProfileConnector;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    iput-object v1, p0, Landroid/bluetooth/BluetoothSyncHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, p1, p2}, Landroid/bluetooth/BluetoothProfileConnector;->connect(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;)V

    const-class v0, Landroid/bluetooth/BluetoothManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothManager;

    new-instance v1, Ljava/util/IdentityHashMap;

    invoke-direct {v1}, Ljava/util/IdentityHashMap;-><init>()V

    iput-object v1, p0, Landroid/bluetooth/BluetoothSyncHelper;->mAppCallbackWrappers:Ljava/util/Map;

    new-instance v1, Ljava/util/IdentityHashMap;

    invoke-direct {v1}, Ljava/util/IdentityHashMap;-><init>()V

    iput-object v1, p0, Landroid/bluetooth/BluetoothSyncHelper;->sBleAssistManagers:Ljava/util/Map;

    iput-object p1, p0, Landroid/bluetooth/BluetoothSyncHelper;->mContext:Landroid/content/Context;

    iget-object v1, p0, Landroid/bluetooth/BluetoothSyncHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getAttributionSource()Landroid/content/AttributionSource;

    move-result-object v1

    iput-object v1, p0, Landroid/bluetooth/BluetoothSyncHelper;->mAttributionSource:Landroid/content/AttributionSource;

    return-void
.end method

.method private isEnabled()Z
    .locals 2

    iget-object v0, p0, Landroid/bluetooth/BluetoothSyncHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method static isSupported()Z
    .locals 3

    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BluetoothSyncHelper: isSupported returns "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/bluetooth/BluetoothSyncHelper;->log(Ljava/lang/String;)V

    return v0
.end method

.method private isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/bluetooth/BluetoothAdapter;->checkBluetoothAddress(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_1
    return v0
.end method

.method private static log(Ljava/lang/String;)V
    .locals 1

    const-string v0, "BluetoothSyncHelper"

    invoke-static {v0, p0}, Landroid/bluetooth/BleBroadcastSourceInfo;->BASS_Debug(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private wrap(Landroid/bluetooth/BleBroadcastAudioScanAssistCallback;Landroid/os/Handler;)Landroid/bluetooth/IBleBroadcastAudioScanAssistCallback;
    .locals 1

    new-instance v0, Landroid/bluetooth/BluetoothSyncHelper$2;

    invoke-direct {v0, p0, p2, p1}, Landroid/bluetooth/BluetoothSyncHelper$2;-><init>(Landroid/bluetooth/BluetoothSyncHelper;Landroid/os/Handler;Landroid/bluetooth/BleBroadcastAudioScanAssistCallback;)V

    return-object v0
.end method


# virtual methods
.method addBroadcastSource(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BleBroadcastSourceInfo;ZLandroid/content/AttributionSource;)Z
    .locals 6

    goto/32 :goto_0

    nop

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_15

    nop

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_b

    nop

    :goto_3
    const-string v1, "isGroupOp: "

    goto/32 :goto_1

    nop

    :goto_4
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_6

    nop

    :goto_5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_12

    nop

    :goto_6
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_13

    nop

    :goto_7
    const-string v5, "Stack:"

    goto/32 :goto_16

    nop

    :goto_8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_4

    nop

    :goto_9
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_e

    nop

    :goto_b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_7

    nop

    :goto_c
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothSyncHelper;->getService()Landroid/bluetooth/IBluetoothSyncHelper;

    move-result-object v1

    goto/32 :goto_10

    nop

    :goto_d
    invoke-static {v5}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_8

    nop

    :goto_e
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_f
    const/4 v0, 0x0

    goto/32 :goto_c

    nop

    :goto_10
    const-string v2, "BluetoothSyncHelper"

    goto/32 :goto_19

    nop

    :goto_11
    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    goto/32 :goto_d

    nop

    :goto_12
    invoke-static {v0}, Landroid/bluetooth/BluetoothSyncHelper;->log(Ljava/lang/String;)V

    goto/32 :goto_f

    nop

    :goto_13
    const/4 v0, 0x0

    goto/32 :goto_1e

    nop

    :goto_14
    const-string v1, "addBroadcastSource  for :"

    goto/32 :goto_17

    nop

    :goto_15
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_14

    nop

    :goto_16
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_23

    nop

    :goto_17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_22

    nop

    :goto_18
    const-string v1, "SourceInfo: "

    goto/32 :goto_a

    nop

    :goto_19
    if-nez v1, :cond_0

    goto/32 :goto_1a

    :cond_0
    :try_start_0
    invoke-direct {p0}, Landroid/bluetooth/BluetoothSyncHelper;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothSyncHelper;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1, p1, p2, p3, p4}, Landroid/bluetooth/IBluetoothSyncHelper;->addBroadcastSource(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BleBroadcastSourceInfo;ZLandroid/content/AttributionSource;)Z

    move-result v2

    return v2

    :catch_0
    move-exception v3

    goto :goto_1c

    :cond_1
    :goto_1a
    if-nez v1, :cond_2

    const-string v3, "Proxy not attached to service"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_21

    nop

    :goto_1b
    goto :goto_1f

    :goto_1c
    goto/32 :goto_2

    nop

    :goto_1d
    return v0

    :goto_1e
    goto :goto_20

    :cond_2
    :goto_1f
    nop

    :goto_20
    goto/32 :goto_1d

    nop

    :goto_21
    const/4 v0, 0x0

    goto/32 :goto_1b

    nop

    :goto_22
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_18

    nop

    :goto_23
    new-instance v5, Ljava/lang/Throwable;

    goto/32 :goto_11

    nop
.end method

.method close()V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    goto/32 :goto_3

    nop

    :goto_1
    iget-object v0, p0, Landroid/bluetooth/BluetoothSyncHelper;->mProfileConnector:Landroid/bluetooth/BluetoothProfileConnector;

    goto/32 :goto_4

    nop

    :goto_2
    iget-object v0, p0, Landroid/bluetooth/BluetoothSyncHelper;->mAppCallbackWrappers:Ljava/util/Map;

    goto/32 :goto_0

    nop

    :goto_3
    return-void

    :goto_4
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothProfileConnector;->disconnect()V

    goto/32 :goto_2

    nop
.end method

.method public connect(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "connect("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/bluetooth/BluetoothSyncHelper;->log(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/bluetooth/BluetoothSyncHelper;->getService()Landroid/bluetooth/IBluetoothSyncHelper;

    move-result-object v0

    const-string v1, "BluetoothSyncHelper"

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-direct {p0}, Landroid/bluetooth/BluetoothSyncHelper;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothSyncHelper;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Landroid/bluetooth/BluetoothSyncHelper;->mAttributionSource:Landroid/content/AttributionSource;

    invoke-interface {v0, p1, v3}, Landroid/bluetooth/IBluetoothSyncHelper;->connect(Landroid/bluetooth/BluetoothDevice;Landroid/content/AttributionSource;)Z

    move-result v1

    return v1

    :catch_0
    move-exception v3

    goto :goto_0

    :cond_0
    if-nez v0, :cond_1

    const-string v3, "Proxy not attached to service"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Stack:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/Throwable;

    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v5}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_1
    :goto_1
    return v2
.end method

.method public disconnect(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "disconnect("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/bluetooth/BluetoothSyncHelper;->log(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/bluetooth/BluetoothSyncHelper;->getService()Landroid/bluetooth/IBluetoothSyncHelper;

    move-result-object v0

    const-string v1, "BluetoothSyncHelper"

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-direct {p0}, Landroid/bluetooth/BluetoothSyncHelper;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothSyncHelper;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Landroid/bluetooth/BluetoothSyncHelper;->mAttributionSource:Landroid/content/AttributionSource;

    invoke-interface {v0, p1, v3}, Landroid/bluetooth/IBluetoothSyncHelper;->disconnect(Landroid/bluetooth/BluetoothDevice;Landroid/content/AttributionSource;)Z

    move-result v1

    return v1

    :catch_0
    move-exception v3

    goto :goto_0

    :cond_0
    if-nez v0, :cond_1

    const-string v3, "Proxy not attached to service"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Stack:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/Throwable;

    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v5}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_1
    :goto_1
    return v2
.end method

.method getAllBroadcastSourceInformation(Landroid/bluetooth/BluetoothDevice;Landroid/content/AttributionSource;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/bluetooth/BluetoothDevice;",
            "Landroid/content/AttributionSource;",
            ")",
            "Ljava/util/List<",
            "Landroid/bluetooth/BleBroadcastSourceInfo;",
            ">;"
        }
    .end annotation

    goto/32 :goto_c

    nop

    :goto_0
    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    goto/32 :goto_7

    nop

    :goto_1
    const-string v5, "Stack:"

    goto/32 :goto_9

    nop

    :goto_2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_6

    nop

    :goto_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_f

    nop

    :goto_4
    goto :goto_b

    :goto_5
    goto/32 :goto_11

    nop

    :goto_6
    const-string v1, "GetAllBroadcastReceiverStates for :"

    goto/32 :goto_19

    nop

    :goto_7
    invoke-static {v5}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_8

    nop

    :goto_8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_15

    nop

    :goto_9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_17

    nop

    :goto_a
    return-object v2

    :cond_0
    :goto_b
    goto/32 :goto_16

    nop

    :goto_c
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_2

    nop

    :goto_d
    if-nez v0, :cond_1

    goto/32 :goto_e

    :cond_1
    :try_start_0
    invoke-direct {p0}, Landroid/bluetooth/BluetoothSyncHelper;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothSyncHelper;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0, p1, p2}, Landroid/bluetooth/IBluetoothSyncHelper;->getAllBroadcastSourceInformation(Landroid/bluetooth/BluetoothDevice;Landroid/content/AttributionSource;)Ljava/util/List;

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v3

    goto :goto_5

    :cond_2
    :goto_e
    if-nez v0, :cond_0

    const-string v3, "Proxy not attached to service"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_4

    nop

    :goto_f
    invoke-static {v0}, Landroid/bluetooth/BluetoothSyncHelper;->log(Ljava/lang/String;)V

    goto/32 :goto_1a

    nop

    :goto_10
    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_a

    nop

    :goto_11
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_12

    nop

    :goto_12
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1

    nop

    :goto_13
    const/4 v2, 0x0

    goto/32 :goto_d

    nop

    :goto_14
    const-string v1, "BluetoothSyncHelper"

    goto/32 :goto_13

    nop

    :goto_15
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_10

    nop

    :goto_16
    return-object v2

    :goto_17
    new-instance v5, Ljava/lang/Throwable;

    goto/32 :goto_0

    nop

    :goto_18
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_18

    nop

    :goto_1a
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothSyncHelper;->getService()Landroid/bluetooth/IBluetoothSyncHelper;

    move-result-object v0

    goto/32 :goto_14

    nop
.end method

.method public getBleBroadcastAudioScanAssistManager(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BleBroadcastAudioScanAssistCallback;)Landroid/bluetooth/BleBroadcastAudioScanAssistManager;
    .locals 2

    invoke-static {}, Landroid/bluetooth/BluetoothSyncHelper;->isSupported()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "BluetoothSyncHelper"

    const-string v1, "Broadcast scan assistance not supported"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Landroid/bluetooth/BluetoothSyncHelper;->sBleAssistManagers:Ljava/util/Map;

    if-eqz v1, :cond_1

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/bluetooth/BleBroadcastAudioScanAssistManager;

    :cond_1
    if-nez v0, :cond_2

    new-instance v1, Landroid/bluetooth/BleBroadcastAudioScanAssistManager;

    invoke-direct {v1, p0, p1, p2}, Landroid/bluetooth/BleBroadcastAudioScanAssistManager;-><init>(Landroid/bluetooth/BluetoothSyncHelper;Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BleBroadcastAudioScanAssistCallback;)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    const-string v1, "calling registerAppCb only"

    invoke-static {v1}, Landroid/bluetooth/BluetoothSyncHelper;->log(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {p0, p1, p2}, Landroid/bluetooth/BluetoothSyncHelper;->registerAppCallback(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BleBroadcastAudioScanAssistCallback;)V

    return-object v0
.end method

.method public getConnectedDevices()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    const-string v0, "getConnectedDevices()"

    invoke-static {v0}, Landroid/bluetooth/BluetoothSyncHelper;->log(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/bluetooth/BluetoothSyncHelper;->getService()Landroid/bluetooth/IBluetoothSyncHelper;

    move-result-object v0

    const-string v1, "BluetoothSyncHelper"

    if-eqz v0, :cond_0

    :try_start_0
    invoke-direct {p0}, Landroid/bluetooth/BluetoothSyncHelper;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/bluetooth/BluetoothSyncHelper;->mAttributionSource:Landroid/content/AttributionSource;

    invoke-interface {v0, v2}, Landroid/bluetooth/IBluetoothSyncHelper;->getConnectedDevices(Landroid/content/AttributionSource;)Ljava/util/List;

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v2

    goto :goto_0

    :cond_0
    if-nez v0, :cond_1

    const-string v2, "Proxy not attached to service"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Stack:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v4}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    return-object v1
.end method

.method public getConnectionPolicy(Landroid/bluetooth/BluetoothDevice;)I
    .locals 6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getConnectionPolicy("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/bluetooth/BluetoothSyncHelper;->log(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/bluetooth/BluetoothSyncHelper;->getService()Landroid/bluetooth/IBluetoothSyncHelper;

    move-result-object v0

    const-string v1, "BluetoothSyncHelper"

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-direct {p0}, Landroid/bluetooth/BluetoothSyncHelper;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothSyncHelper;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Landroid/bluetooth/BluetoothSyncHelper;->mAttributionSource:Landroid/content/AttributionSource;

    invoke-interface {v0, p1, v3}, Landroid/bluetooth/IBluetoothSyncHelper;->getConnectionPolicy(Landroid/bluetooth/BluetoothDevice;Landroid/content/AttributionSource;)I

    move-result v1

    return v1

    :catch_0
    move-exception v3

    goto :goto_0

    :cond_0
    if-nez v0, :cond_1

    const-string v3, "Proxy not attached to service"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Stack:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/Throwable;

    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v5}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_1
    :goto_1
    return v2
.end method

.method public getConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    .locals 6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getState("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/bluetooth/BluetoothSyncHelper;->log(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/bluetooth/BluetoothSyncHelper;->getService()Landroid/bluetooth/IBluetoothSyncHelper;

    move-result-object v0

    const-string v1, "BluetoothSyncHelper"

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-direct {p0}, Landroid/bluetooth/BluetoothSyncHelper;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothSyncHelper;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Landroid/bluetooth/BluetoothSyncHelper;->mAttributionSource:Landroid/content/AttributionSource;

    invoke-interface {v0, p1, v3}, Landroid/bluetooth/IBluetoothSyncHelper;->getConnectionState(Landroid/bluetooth/BluetoothDevice;Landroid/content/AttributionSource;)I

    move-result v1

    return v1

    :catch_0
    move-exception v3

    goto :goto_0

    :cond_0
    if-nez v0, :cond_1

    const-string v3, "Proxy not attached to service"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Stack:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/Throwable;

    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v5}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_1
    :goto_1
    return v2
.end method

.method public getDevicesMatchingConnectionStates([I)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    const-string v0, "getDevicesMatchingStates()"

    invoke-static {v0}, Landroid/bluetooth/BluetoothSyncHelper;->log(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/bluetooth/BluetoothSyncHelper;->getService()Landroid/bluetooth/IBluetoothSyncHelper;

    move-result-object v0

    const-string v1, "BluetoothSyncHelper"

    if-eqz v0, :cond_0

    :try_start_0
    invoke-direct {p0}, Landroid/bluetooth/BluetoothSyncHelper;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/bluetooth/BluetoothSyncHelper;->mAttributionSource:Landroid/content/AttributionSource;

    invoke-interface {v0, p1, v2}, Landroid/bluetooth/IBluetoothSyncHelper;->getDevicesMatchingConnectionStates([ILandroid/content/AttributionSource;)Ljava/util/List;

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v2

    goto :goto_0

    :cond_0
    if-nez v0, :cond_1

    const-string v2, "Proxy not attached to service"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Stack:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v4}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    return-object v1
.end method

.method getService()Landroid/bluetooth/IBluetoothSyncHelper;
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    return-object v0

    :goto_1
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothProfileConnector;->getService()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_2
    check-cast v0, Landroid/bluetooth/IBluetoothSyncHelper;

    goto/32 :goto_0

    nop

    :goto_3
    iget-object v0, p0, Landroid/bluetooth/BluetoothSyncHelper;->mProfileConnector:Landroid/bluetooth/BluetoothProfileConnector;

    goto/32 :goto_1

    nop
.end method

.method registerAppCallback(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BleBroadcastAudioScanAssistCallback;)V
    .locals 7

    goto/32 :goto_2

    nop

    :goto_0
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_8

    nop

    :goto_1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_0

    nop

    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_e

    nop

    :goto_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_d

    nop

    :goto_4
    new-instance v0, Landroid/os/Handler;

    goto/32 :goto_13

    nop

    :goto_5
    const-string v6, "Stack:"

    goto/32 :goto_f

    nop

    :goto_6
    const-string v3, "BluetoothSyncHelper"

    goto/32 :goto_1e

    nop

    :goto_7
    invoke-direct {p0, p2, v0}, Landroid/bluetooth/BluetoothSyncHelper;->wrap(Landroid/bluetooth/BleBroadcastAudioScanAssistCallback;Landroid/os/Handler;)Landroid/bluetooth/IBleBroadcastAudioScanAssistCallback;

    move-result-object v1

    goto/32 :goto_17

    nop

    :goto_8
    return-void

    :cond_0
    nop

    goto/32 :goto_10

    nop

    :goto_9
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_14

    nop

    :goto_a
    invoke-direct {v6}, Ljava/lang/Throwable;-><init>()V

    goto/32 :goto_12

    nop

    :goto_b
    const-string v1, "appCB: "

    goto/32 :goto_1b

    nop

    :goto_c
    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    goto/32 :goto_7

    nop

    :goto_d
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_b

    nop

    :goto_e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1a

    nop

    :goto_f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/32 :goto_18

    nop

    :goto_10
    return-void

    :goto_11
    new-instance v5, Ljava/lang/StringBuilder;

    goto/32 :goto_1c

    nop

    :goto_12
    invoke-static {v6}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    goto/32 :goto_19

    nop

    :goto_13
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    goto/32 :goto_c

    nop

    :goto_14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_1d

    nop

    :goto_15
    return-void

    :goto_16
    goto/32 :goto_11

    nop

    :goto_17
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothSyncHelper;->getService()Landroid/bluetooth/IBluetoothSyncHelper;

    move-result-object v2

    goto/32 :goto_6

    nop

    :goto_18
    new-instance v6, Ljava/lang/Throwable;

    goto/32 :goto_a

    nop

    :goto_19
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/32 :goto_1

    nop

    :goto_1a
    const-string/jumbo v1, "registerAppCallback device :"

    goto/32 :goto_3

    nop

    :goto_1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_1c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_5

    nop

    :goto_1d
    invoke-static {v0}, Landroid/bluetooth/BluetoothSyncHelper;->log(Ljava/lang/String;)V

    goto/32 :goto_4

    nop

    :goto_1e
    if-nez v2, :cond_1

    goto/32 :goto_1f

    :cond_1
    :try_start_0
    invoke-direct {p0}, Landroid/bluetooth/BluetoothSyncHelper;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothSyncHelper;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Landroid/bluetooth/BluetoothSyncHelper;->mAttributionSource:Landroid/content/AttributionSource;

    invoke-interface {v2, p1, v1, v4}, Landroid/bluetooth/IBluetoothSyncHelper;->registerAppCallback(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/IBleBroadcastAudioScanAssistCallback;Landroid/content/AttributionSource;)V

    iget-object v4, p0, Landroid/bluetooth/BluetoothSyncHelper;->mAppCallbackWrappers:Ljava/util/Map;

    if-eqz v4, :cond_2

    invoke-interface {v4, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1f

    :catch_0
    move-exception v4

    goto :goto_16

    :cond_2
    :goto_1f
    if-nez v2, :cond_0

    const-string v4, "Proxy not attached to service"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_15

    nop
.end method

.method removeBroadcastSource(Landroid/bluetooth/BluetoothDevice;BZLandroid/content/AttributionSource;)Z
    .locals 6

    goto/32 :goto_1b

    nop

    :goto_0
    invoke-static {v0}, Landroid/bluetooth/BluetoothSyncHelper;->log(Ljava/lang/String;)V

    goto/32 :goto_d

    nop

    :goto_1
    const-string v1, "SourceId: "

    goto/32 :goto_f

    nop

    :goto_2
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_8

    nop

    :goto_3
    const-string v2, "BluetoothSyncHelper"

    goto/32 :goto_21

    nop

    :goto_4
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_1a

    nop

    :goto_5
    goto :goto_7

    :cond_0
    :goto_6
    nop

    :goto_7
    goto/32 :goto_9

    nop

    :goto_8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_14

    nop

    :goto_9
    return v1

    :goto_a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_11

    nop

    :goto_b
    const-string v1, "isGroupOp: "

    goto/32 :goto_1c

    nop

    :goto_c
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_19

    nop

    :goto_d
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothSyncHelper;->getService()Landroid/bluetooth/IBluetoothSyncHelper;

    move-result-object v0

    goto/32 :goto_18

    nop

    :goto_e
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_b

    nop

    :goto_f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_e

    nop

    :goto_10
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_13

    nop

    :goto_11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_12
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_15

    nop

    :goto_13
    const-string/jumbo v1, "removeBroadcastSource for :"

    goto/32 :goto_a

    nop

    :goto_14
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_1e

    nop

    :goto_15
    new-instance v5, Ljava/lang/Throwable;

    goto/32 :goto_20

    nop

    :goto_16
    goto :goto_6

    :goto_17
    goto/32 :goto_4

    nop

    :goto_18
    const/4 v1, 0x0

    goto/32 :goto_3

    nop

    :goto_19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_1a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1f

    nop

    :goto_1b
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_10

    nop

    :goto_1c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_c

    nop

    :goto_1d
    const/4 v1, 0x0

    goto/32 :goto_16

    nop

    :goto_1e
    const/4 v1, 0x0

    goto/32 :goto_5

    nop

    :goto_1f
    const-string v5, "Stack:"

    goto/32 :goto_12

    nop

    :goto_20
    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    goto/32 :goto_23

    nop

    :goto_21
    if-nez v0, :cond_1

    goto/32 :goto_22

    :cond_1
    :try_start_0
    invoke-direct {p0}, Landroid/bluetooth/BluetoothSyncHelper;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothSyncHelper;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/bluetooth/IBluetoothSyncHelper;->removeBroadcastSource(Landroid/bluetooth/BluetoothDevice;BZLandroid/content/AttributionSource;)Z

    move-result v2

    return v2

    :catch_0
    move-exception v3

    goto :goto_17

    :cond_2
    :goto_22
    if-nez v0, :cond_0

    const-string v3, "Proxy not attached to service"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_1d

    nop

    :goto_23
    invoke-static {v5}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_2

    nop
.end method

.method searchforLeAudioBroadcasters(Landroid/bluetooth/BluetoothDevice;Landroid/content/AttributionSource;)Z
    .locals 6

    goto/32 :goto_14

    nop

    :goto_0
    return v2

    :goto_1
    const-string v1, "BluetoothSyncHelper"

    goto/32 :goto_16

    nop

    :goto_2
    invoke-static {v5}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_7

    nop

    :goto_3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_18

    nop

    :goto_4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1a

    nop

    :goto_5
    goto :goto_12

    :goto_6
    goto/32 :goto_13

    nop

    :goto_7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_3

    nop

    :goto_8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_e

    nop

    :goto_9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_1b

    nop

    :goto_a
    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    goto/32 :goto_2

    nop

    :goto_b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_1c

    nop

    :goto_c
    if-nez v0, :cond_0

    goto/32 :goto_d

    :cond_0
    :try_start_0
    invoke-direct {p0}, Landroid/bluetooth/BluetoothSyncHelper;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothSyncHelper;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0, p1, p2}, Landroid/bluetooth/IBluetoothSyncHelper;->searchforLeAudioBroadcasters(Landroid/bluetooth/BluetoothDevice;Landroid/content/AttributionSource;)Z

    move-result v1

    return v1

    :catch_0
    move-exception v3

    goto :goto_6

    :cond_1
    :goto_d
    if-nez v0, :cond_2

    const-string v3, "Proxy not attached to service"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_5

    nop

    :goto_e
    const-string/jumbo v1, "searchforLeAudioBroadcasters("

    goto/32 :goto_19

    nop

    :goto_f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_b

    nop

    :goto_10
    const-string v1, ")"

    goto/32 :goto_f

    nop

    :goto_11
    return v2

    :cond_2
    :goto_12
    goto/32 :goto_0

    nop

    :goto_13
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_4

    nop

    :goto_14
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_8

    nop

    :goto_15
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothSyncHelper;->getService()Landroid/bluetooth/IBluetoothSyncHelper;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_16
    const/4 v2, 0x0

    goto/32 :goto_c

    nop

    :goto_17
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_10

    nop

    :goto_18
    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_11

    nop

    :goto_19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_17

    nop

    :goto_1a
    const-string v5, "Stack:"

    goto/32 :goto_9

    nop

    :goto_1b
    new-instance v5, Ljava/lang/Throwable;

    goto/32 :goto_a

    nop

    :goto_1c
    invoke-static {v0}, Landroid/bluetooth/BluetoothSyncHelper;->log(Ljava/lang/String;)V

    goto/32 :goto_15

    nop
.end method

.method selectBroadcastSource(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/le/ScanResult;ZLandroid/content/AttributionSource;)Z
    .locals 6

    goto/32 :goto_14

    nop

    :goto_0
    invoke-static {v5}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_15

    nop

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_2

    :cond_0
    :try_start_0
    invoke-direct {p0}, Landroid/bluetooth/BluetoothSyncHelper;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothSyncHelper;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/bluetooth/IBluetoothSyncHelper;->selectBroadcastSource(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/le/ScanResult;ZLandroid/content/AttributionSource;)Z

    move-result v1

    return v1

    :catch_0
    move-exception v3

    goto :goto_7

    :cond_1
    :goto_2
    if-nez v0, :cond_2

    const-string v3, "Proxy not attached to service"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_6

    nop

    :goto_3
    new-instance v5, Ljava/lang/Throwable;

    goto/32 :goto_1f

    nop

    :goto_4
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_3

    nop

    :goto_5
    const-string v1, ": groupop"

    goto/32 :goto_8

    nop

    :goto_6
    goto/16 :goto_1b

    :goto_7
    goto/32 :goto_c

    nop

    :goto_8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_13

    nop

    :goto_9
    const-string/jumbo v1, "selectBroadcastSource("

    goto/32 :goto_1d

    nop

    :goto_a
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_d

    nop

    :goto_c
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_19

    nop

    :goto_d
    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_1a

    nop

    :goto_e
    return v2

    :goto_f
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothSyncHelper;->getService()Landroid/bluetooth/IBluetoothSyncHelper;

    move-result-object v0

    goto/32 :goto_17

    nop

    :goto_10
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_9

    nop

    :goto_11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_18

    nop

    :goto_12
    const-string v1, ")"

    goto/32 :goto_11

    nop

    :goto_13
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_12

    nop

    :goto_14
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_10

    nop

    :goto_15
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_b

    nop

    :goto_16
    invoke-static {v0}, Landroid/bluetooth/BluetoothSyncHelper;->log(Ljava/lang/String;)V

    goto/32 :goto_f

    nop

    :goto_17
    const-string v1, "BluetoothSyncHelper"

    goto/32 :goto_1e

    nop

    :goto_18
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_16

    nop

    :goto_19
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1c

    nop

    :goto_1a
    return v2

    :cond_2
    :goto_1b
    goto/32 :goto_e

    nop

    :goto_1c
    const-string v5, "Stack:"

    goto/32 :goto_4

    nop

    :goto_1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_a

    nop

    :goto_1e
    const/4 v2, 0x0

    goto/32 :goto_1

    nop

    :goto_1f
    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    goto/32 :goto_0

    nop
.end method

.method setBroadcastCode(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BleBroadcastSourceInfo;ZLandroid/content/AttributionSource;)Z
    .locals 6

    goto/32 :goto_c

    nop

    :goto_0
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_17

    nop

    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_2
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_3
    invoke-static {v0}, Landroid/bluetooth/BluetoothSyncHelper;->log(Ljava/lang/String;)V

    goto/32 :goto_27

    nop

    :goto_4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_1c

    nop

    :goto_5
    new-instance v5, Ljava/lang/Throwable;

    goto/32 :goto_12

    nop

    :goto_6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_7
    const-string v5, "Stack:"

    goto/32 :goto_18

    nop

    :goto_8
    invoke-static {v0}, Landroid/bluetooth/BluetoothSyncHelper;->log(Ljava/lang/String;)V

    goto/32 :goto_11

    nop

    :goto_9
    if-nez v1, :cond_0

    goto/32 :goto_a

    :cond_0
    :try_start_0
    invoke-direct {p0}, Landroid/bluetooth/BluetoothSyncHelper;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothSyncHelper;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1, p1, p2, p3, p4}, Landroid/bluetooth/IBluetoothSyncHelper;->setBroadcastCode(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BleBroadcastSourceInfo;ZLandroid/content/AttributionSource;)Z

    move-result v2

    return v2

    :catch_0
    move-exception v3

    goto :goto_1b

    :cond_1
    :goto_a
    if-nez v1, :cond_2

    const-string v3, "Proxy not attached to service"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_20

    nop

    :goto_b
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothSyncHelper;->getService()Landroid/bluetooth/IBluetoothSyncHelper;

    move-result-object v1

    goto/32 :goto_1d

    nop

    :goto_c
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_e

    nop

    :goto_d
    invoke-static {v5}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_15

    nop

    :goto_e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_24

    nop

    :goto_f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_10

    nop

    :goto_10
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_1f

    nop

    :goto_11
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_0

    nop

    :goto_12
    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    goto/32 :goto_d

    nop

    :goto_13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_14

    nop

    :goto_14
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_25

    nop

    :goto_15
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_f

    nop

    :goto_16
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_19

    nop

    :goto_17
    const-string v1, "SourceInfo: "

    goto/32 :goto_13

    nop

    :goto_18
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_5

    nop

    :goto_19
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_7

    nop

    :goto_1a
    goto :goto_22

    :goto_1b
    goto/32 :goto_16

    nop

    :goto_1c
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_1e

    nop

    :goto_1d
    const-string v2, "BluetoothSyncHelper"

    goto/32 :goto_9

    nop

    :goto_1e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_1f
    const/4 v0, 0x0

    goto/32 :goto_21

    nop

    :goto_20
    const/4 v0, 0x0

    goto/32 :goto_1a

    nop

    :goto_21
    goto :goto_23

    :cond_2
    :goto_22
    nop

    :goto_23
    goto/32 :goto_26

    nop

    :goto_24
    const-string/jumbo v1, "setBroadcastCode for :"

    goto/32 :goto_4

    nop

    :goto_25
    const-string v1, "isGroupOp: "

    goto/32 :goto_6

    nop

    :goto_26
    return v0

    :goto_27
    const/4 v0, 0x0

    goto/32 :goto_b

    nop
.end method

.method public setConnectionPolicy(Landroid/bluetooth/BluetoothDevice;I)Z
    .locals 6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setConnectionPolicy("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/bluetooth/BluetoothSyncHelper;->log(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/bluetooth/BluetoothSyncHelper;->getService()Landroid/bluetooth/IBluetoothSyncHelper;

    move-result-object v0

    const-string v1, "BluetoothSyncHelper"

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    :try_start_0
    invoke-direct {p0}, Landroid/bluetooth/BluetoothSyncHelper;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothSyncHelper;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v3

    if-eqz v3, :cond_1

    if-eqz p2, :cond_0

    const/16 v3, 0x64

    if-eq p2, v3, :cond_0

    return v2

    :cond_0
    iget-object v3, p0, Landroid/bluetooth/BluetoothSyncHelper;->mAttributionSource:Landroid/content/AttributionSource;

    invoke-interface {v0, p1, p2, v3}, Landroid/bluetooth/IBluetoothSyncHelper;->setConnectionPolicy(Landroid/bluetooth/BluetoothDevice;ILandroid/content/AttributionSource;)Z

    move-result v1

    return v1

    :catch_0
    move-exception v3

    goto :goto_0

    :cond_1
    if-nez v0, :cond_2

    const-string v3, "Proxy not attached to service"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Stack:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/Throwable;

    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v5}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_2
    :goto_1
    return v2
.end method

.method startScanOffload(Landroid/bluetooth/BluetoothDevice;Z)Z
    .locals 6

    goto/32 :goto_14

    nop

    :goto_0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_b

    nop

    :goto_1
    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_15

    nop

    :goto_2
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_19

    nop

    :goto_3
    goto :goto_16

    :goto_4
    goto/32 :goto_13

    nop

    :goto_5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_c

    nop

    :goto_6
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_10

    nop

    :goto_7
    const-string v5, "Stack:"

    goto/32 :goto_0

    nop

    :goto_8
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_7

    nop

    :goto_9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_a
    const/4 v2, 0x0

    goto/32 :goto_1c

    nop

    :goto_b
    new-instance v5, Ljava/lang/Throwable;

    goto/32 :goto_f

    nop

    :goto_c
    invoke-static {v0}, Landroid/bluetooth/BluetoothSyncHelper;->log(Ljava/lang/String;)V

    goto/32 :goto_1a

    nop

    :goto_d
    const-string v1, ")"

    goto/32 :goto_18

    nop

    :goto_e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_17

    nop

    :goto_f
    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    goto/32 :goto_12

    nop

    :goto_10
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_1

    nop

    :goto_11
    return v2

    :goto_12
    invoke-static {v5}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_6

    nop

    :goto_13
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_8

    nop

    :goto_14
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_1f

    nop

    :goto_15
    return v2

    :cond_0
    :goto_16
    goto/32 :goto_11

    nop

    :goto_17
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_d

    nop

    :goto_18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_19
    const-string v1, ", isGroupOp: "

    goto/32 :goto_e

    nop

    :goto_1a
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothSyncHelper;->getService()Landroid/bluetooth/IBluetoothSyncHelper;

    move-result-object v0

    goto/32 :goto_1b

    nop

    :goto_1b
    const-string v1, "BluetoothSyncHelper"

    goto/32 :goto_a

    nop

    :goto_1c
    if-nez v0, :cond_1

    goto/32 :goto_1d

    :cond_1
    :try_start_0
    invoke-direct {p0}, Landroid/bluetooth/BluetoothSyncHelper;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothSyncHelper;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Landroid/bluetooth/BluetoothSyncHelper;->mAttributionSource:Landroid/content/AttributionSource;

    invoke-interface {v0, p1, p2, v3}, Landroid/bluetooth/IBluetoothSyncHelper;->startScanOffload(Landroid/bluetooth/BluetoothDevice;ZLandroid/content/AttributionSource;)Z

    move-result v1

    return v1

    :catch_0
    move-exception v3

    goto/16 :goto_4

    :cond_2
    :goto_1d
    if-nez v0, :cond_0

    const-string v3, "Proxy not attached to service"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_3

    nop

    :goto_1e
    const-string/jumbo v1, "startScanOffload("

    goto/32 :goto_9

    nop

    :goto_1f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1e

    nop
.end method

.method stopScanOffload(Landroid/bluetooth/BluetoothDevice;Z)Z
    .locals 6

    goto/32 :goto_18

    nop

    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_d

    nop

    :goto_1
    return v2

    :cond_0
    :goto_2
    goto/32 :goto_9

    nop

    :goto_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_19

    nop

    :goto_4
    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_1

    nop

    :goto_5
    const/4 v2, 0x0

    goto/32 :goto_13

    nop

    :goto_6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_7
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_e

    nop

    :goto_8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_15

    nop

    :goto_9
    return v2

    :goto_a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_b

    nop

    :goto_b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_4

    nop

    :goto_c
    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    goto/32 :goto_11

    nop

    :goto_d
    invoke-static {v0}, Landroid/bluetooth/BluetoothSyncHelper;->log(Ljava/lang/String;)V

    goto/32 :goto_1c

    nop

    :goto_e
    const-string v5, "Stack:"

    goto/32 :goto_8

    nop

    :goto_f
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_17

    nop

    :goto_10
    const-string v1, "BluetoothSyncHelper"

    goto/32 :goto_5

    nop

    :goto_11
    invoke-static {v5}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_a

    nop

    :goto_12
    const-string/jumbo v1, "stopScanOffload("

    goto/32 :goto_1a

    nop

    :goto_13
    if-nez v0, :cond_1

    goto/32 :goto_14

    :cond_1
    :try_start_0
    invoke-direct {p0}, Landroid/bluetooth/BluetoothSyncHelper;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothSyncHelper;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Landroid/bluetooth/BluetoothSyncHelper;->mAttributionSource:Landroid/content/AttributionSource;

    invoke-interface {v0, p1, p2, v3}, Landroid/bluetooth/IBluetoothSyncHelper;->stopScanOffload(Landroid/bluetooth/BluetoothDevice;ZLandroid/content/AttributionSource;)Z

    move-result v1

    return v1

    :catch_0
    move-exception v3

    goto :goto_1e

    :cond_2
    :goto_14
    if-nez v0, :cond_0

    const-string v3, "Proxy not attached to service"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_1d

    nop

    :goto_15
    new-instance v5, Ljava/lang/Throwable;

    goto/32 :goto_c

    nop

    :goto_16
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_7

    nop

    :goto_17
    const-string v1, ", isGroupOp: "

    goto/32 :goto_3

    nop

    :goto_18
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_1b

    nop

    :goto_19
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_1f

    nop

    :goto_1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_f

    nop

    :goto_1b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_12

    nop

    :goto_1c
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothSyncHelper;->getService()Landroid/bluetooth/IBluetoothSyncHelper;

    move-result-object v0

    goto/32 :goto_10

    nop

    :goto_1d
    goto/16 :goto_2

    :goto_1e
    goto/32 :goto_16

    nop

    :goto_1f
    const-string v1, ")"

    goto/32 :goto_6

    nop
.end method

.method stopSearchforLeAudioBroadcasters(Landroid/bluetooth/BluetoothDevice;Landroid/content/AttributionSource;)Z
    .locals 6

    goto/32 :goto_d

    nop

    :goto_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_17

    nop

    :goto_1
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_1b

    nop

    :goto_2
    const-string v1, "BluetoothSyncHelper"

    goto/32 :goto_18

    nop

    :goto_3
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothSyncHelper;->getService()Landroid/bluetooth/IBluetoothSyncHelper;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_9

    nop

    :goto_5
    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    goto/32 :goto_10

    nop

    :goto_6
    const-string v1, ")"

    goto/32 :goto_12

    nop

    :goto_7
    goto :goto_c

    :goto_8
    goto/32 :goto_11

    nop

    :goto_9
    const-string v5, "Stack:"

    goto/32 :goto_1

    nop

    :goto_a
    invoke-static {v0}, Landroid/bluetooth/BluetoothSyncHelper;->log(Ljava/lang/String;)V

    goto/32 :goto_3

    nop

    :goto_b
    return v2

    :cond_0
    :goto_c
    goto/32 :goto_14

    nop

    :goto_d
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_15

    nop

    :goto_e
    if-nez v0, :cond_1

    goto/32 :goto_f

    :cond_1
    :try_start_0
    invoke-direct {p0}, Landroid/bluetooth/BluetoothSyncHelper;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothSyncHelper;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0, p1, p2}, Landroid/bluetooth/IBluetoothSyncHelper;->stopSearchforLeAudioBroadcasters(Landroid/bluetooth/BluetoothDevice;Landroid/content/AttributionSource;)Z

    move-result v1

    return v1

    :catch_0
    move-exception v3

    goto :goto_8

    :cond_2
    :goto_f
    if-nez v0, :cond_0

    const-string v3, "Proxy not attached to service"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_7

    nop

    :goto_10
    invoke-static {v5}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_19

    nop

    :goto_11
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_4

    nop

    :goto_12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_1a

    nop

    :goto_13
    const-string/jumbo v1, "stopSearchforLeAudioBroadcasters("

    goto/32 :goto_16

    nop

    :goto_14
    return v2

    :goto_15
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_13

    nop

    :goto_16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_1c

    nop

    :goto_17
    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_b

    nop

    :goto_18
    const/4 v2, 0x0

    goto/32 :goto_e

    nop

    :goto_19
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_0

    nop

    :goto_1a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_a

    nop

    :goto_1b
    new-instance v5, Ljava/lang/Throwable;

    goto/32 :goto_5

    nop

    :goto_1c
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_6

    nop
.end method

.method unregisterAppCallback(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BleBroadcastAudioScanAssistCallback;)V
    .locals 6

    goto/32 :goto_a

    nop

    :goto_0
    return-void

    :cond_0
    :goto_1
    goto/32 :goto_12

    nop

    :goto_2
    const-string v1, "appCB:"

    goto/32 :goto_b

    nop

    :goto_3
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_1c

    nop

    :goto_4
    const-string/jumbo v1, "unregisterAppCallback: device"

    goto/32 :goto_8

    nop

    :goto_5
    if-nez v0, :cond_1

    goto/32 :goto_6

    :cond_1
    :try_start_0
    invoke-direct {p0}, Landroid/bluetooth/BluetoothSyncHelper;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothSyncHelper;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Landroid/bluetooth/BluetoothSyncHelper;->mAttributionSource:Landroid/content/AttributionSource;

    invoke-interface {v0, p1, v1, v3}, Landroid/bluetooth/IBluetoothSyncHelper;->unregisterAppCallback(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/IBleBroadcastAudioScanAssistCallback;Landroid/content/AttributionSource;)V

    iget-object v3, p0, Landroid/bluetooth/BluetoothSyncHelper;->mAppCallbackWrappers:Ljava/util/Map;

    invoke-interface {v3, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :catch_0
    move-exception v3

    goto :goto_18

    :cond_2
    :goto_6
    if-nez v0, :cond_0

    const-string v3, "Proxy not attached to service"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_17

    nop

    :goto_7
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_0

    nop

    :goto_8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_16

    nop

    :goto_9
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_4

    nop

    :goto_a
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_9

    nop

    :goto_b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_13

    nop

    :goto_c
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_1b

    nop

    :goto_d
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_15

    nop

    :goto_e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_f

    nop

    :goto_f
    invoke-static {v0}, Landroid/bluetooth/BluetoothSyncHelper;->log(Ljava/lang/String;)V

    goto/32 :goto_1a

    nop

    :goto_10
    const-string v5, "Stack:"

    goto/32 :goto_1d

    nop

    :goto_11
    new-instance v5, Ljava/lang/Throwable;

    goto/32 :goto_1f

    nop

    :goto_12
    return-void

    :goto_13
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_e

    nop

    :goto_14
    invoke-static {v5}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_3

    nop

    :goto_15
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_10

    nop

    :goto_16
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_17
    goto/16 :goto_1

    :goto_18
    goto/32 :goto_d

    nop

    :goto_19
    const-string v2, "BluetoothSyncHelper"

    goto/32 :goto_5

    nop

    :goto_1a
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothSyncHelper;->getService()Landroid/bluetooth/IBluetoothSyncHelper;

    move-result-object v0

    goto/32 :goto_1e

    nop

    :goto_1b
    check-cast v1, Landroid/bluetooth/IBleBroadcastAudioScanAssistCallback;

    goto/32 :goto_19

    nop

    :goto_1c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_7

    nop

    :goto_1d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_11

    nop

    :goto_1e
    iget-object v1, p0, Landroid/bluetooth/BluetoothSyncHelper;->mAppCallbackWrappers:Ljava/util/Map;

    goto/32 :goto_c

    nop

    :goto_1f
    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    goto/32 :goto_14

    nop
.end method

.method updateBroadcastSource(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BleBroadcastSourceInfo;ZLandroid/content/AttributionSource;)Z
    .locals 6

    goto/32 :goto_12

    nop

    :goto_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_19

    nop

    :goto_1
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_b

    nop

    :goto_2
    const-string/jumbo v1, "updateBroadcastSource for :"

    goto/32 :goto_13

    nop

    :goto_3
    new-instance v5, Ljava/lang/Throwable;

    goto/32 :goto_7

    nop

    :goto_4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_5
    return v0

    :goto_6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_2

    nop

    :goto_7
    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    goto/32 :goto_1d

    nop

    :goto_8
    const-string v1, "SourceInfo: "

    goto/32 :goto_4

    nop

    :goto_9
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_a
    invoke-static {v0}, Landroid/bluetooth/BluetoothSyncHelper;->log(Ljava/lang/String;)V

    goto/32 :goto_1e

    nop

    :goto_b
    const-string v1, "isGroupOp: "

    goto/32 :goto_11

    nop

    :goto_c
    goto :goto_e

    :cond_0
    :goto_d
    nop

    :goto_e
    goto/32 :goto_5

    nop

    :goto_f
    if-nez v1, :cond_1

    goto/32 :goto_10

    :cond_1
    :try_start_0
    invoke-direct {p0}, Landroid/bluetooth/BluetoothSyncHelper;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-direct {p0, p1}, Landroid/bluetooth/BluetoothSyncHelper;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1, p1, p2, p3, p4}, Landroid/bluetooth/IBluetoothSyncHelper;->updateBroadcastSource(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BleBroadcastSourceInfo;ZLandroid/content/AttributionSource;)Z

    move-result v2

    return v2

    :catch_0
    move-exception v3

    goto :goto_20

    :cond_2
    :goto_10
    if-nez v1, :cond_0

    const-string v3, "Proxy not attached to service"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_23

    nop

    :goto_11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_17

    nop

    :goto_12
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_14
    const/4 v0, 0x0

    goto/32 :goto_c

    nop

    :goto_15
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_18

    nop

    :goto_16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_a

    nop

    :goto_17
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    goto/32 :goto_16

    nop

    :goto_18
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1b

    nop

    :goto_19
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_14

    nop

    :goto_1a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_0

    nop

    :goto_1b
    const-string v5, "Stack:"

    goto/32 :goto_22

    nop

    :goto_1c
    const-string v2, "BluetoothSyncHelper"

    goto/32 :goto_f

    nop

    :goto_1d
    invoke-static {v5}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_1a

    nop

    :goto_1e
    const/4 v0, 0x0

    goto/32 :goto_21

    nop

    :goto_1f
    goto/16 :goto_d

    :goto_20
    goto/32 :goto_15

    nop

    :goto_21
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothSyncHelper;->getService()Landroid/bluetooth/IBluetoothSyncHelper;

    move-result-object v1

    goto/32 :goto_1c

    nop

    :goto_22
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/32 :goto_3

    nop

    :goto_23
    const/4 v0, 0x0

    goto/32 :goto_1f

    nop
.end method
