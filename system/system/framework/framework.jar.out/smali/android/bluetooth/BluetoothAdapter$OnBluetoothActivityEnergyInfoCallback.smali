.class public interface abstract Landroid/bluetooth/BluetoothAdapter$OnBluetoothActivityEnergyInfoCallback;
.super Ljava/lang/Object;


# annotations
.annotation runtime Landroid/annotation/SystemApi;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/bluetooth/BluetoothAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnBluetoothActivityEnergyInfoCallback"
.end annotation


# virtual methods
.method public abstract onBluetoothActivityEnergyInfoAvailable(Landroid/bluetooth/BluetoothActivityEnergyInfo;)V
.end method

.method public abstract onBluetoothActivityEnergyInfoError(I)V
.end method
