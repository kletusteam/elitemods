.class public interface abstract Landroid/bluetooth/IBluetoothCsipSetCoordinatorLockCallback;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/bluetooth/IBluetoothCsipSetCoordinatorLockCallback$Stub;,
        Landroid/bluetooth/IBluetoothCsipSetCoordinatorLockCallback$Default;
    }
.end annotation


# static fields
.field public static final DESCRIPTOR:Ljava/lang/String; = "android.bluetooth.IBluetoothCsipSetCoordinatorLockCallback"


# virtual methods
.method public abstract onGroupLockSet(IIZ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
