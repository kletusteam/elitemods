.class public final enum Landroid/audio/policy/configuration/V7_0/AudioChannelMask;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Landroid/audio/policy/configuration/V7_0/AudioChannelMask;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_INDEX_MASK_1:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_INDEX_MASK_10:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_INDEX_MASK_11:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_INDEX_MASK_12:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_INDEX_MASK_13:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_INDEX_MASK_14:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_INDEX_MASK_15:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_INDEX_MASK_16:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_INDEX_MASK_17:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_INDEX_MASK_18:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_INDEX_MASK_19:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_INDEX_MASK_2:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_INDEX_MASK_20:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_INDEX_MASK_21:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_INDEX_MASK_22:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_INDEX_MASK_23:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_INDEX_MASK_24:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_INDEX_MASK_3:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_INDEX_MASK_4:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_INDEX_MASK_5:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_INDEX_MASK_6:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_INDEX_MASK_7:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_INDEX_MASK_8:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_INDEX_MASK_9:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_IN_2POINT0POINT2:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_IN_2POINT1POINT2:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_IN_3POINT0POINT2:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_IN_3POINT1POINT2:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_IN_5POINT1:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_IN_6:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_IN_FRONT_BACK:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_IN_MONO:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_IN_STEREO:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_IN_VOICE_CALL_MONO:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_IN_VOICE_DNLINK_MONO:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_IN_VOICE_UPLINK_MONO:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_NONE:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_13POINT_360RA:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_22POINT2:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_2POINT0POINT2:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_2POINT1:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_2POINT1POINT2:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_3POINT0POINT2:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_3POINT1:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_3POINT1POINT2:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_5POINT1:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_5POINT1POINT2:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_5POINT1POINT4:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_5POINT1_BACK:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_5POINT1_SIDE:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_6POINT1:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_7POINT1:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_7POINT1POINT2:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_7POINT1POINT4:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_HAPTIC_AB:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_MONO:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_MONO_HAPTIC_A:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_MONO_HAPTIC_AB:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_PENTA:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_QUAD:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_QUAD_BACK:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_QUAD_SIDE:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_STEREO:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_STEREO_HAPTIC_A:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_STEREO_HAPTIC_AB:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_SURROUND:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_TRI:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

.field public static final enum AUDIO_CHANNEL_OUT_TRI_BACK:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;


# instance fields
.field private final rawName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 71

    new-instance v0, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v1, "AUDIO_CHANNEL_NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v1}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_NONE:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v1, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v3, "AUDIO_CHANNEL_OUT_MONO"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4, v3}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_MONO:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v3, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v5, "AUDIO_CHANNEL_OUT_STEREO"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6, v5}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_STEREO:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v5, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v7, "AUDIO_CHANNEL_OUT_2POINT1"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8, v7}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_2POINT1:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v7, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v9, "AUDIO_CHANNEL_OUT_TRI"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10, v9}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_TRI:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v9, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v11, "AUDIO_CHANNEL_OUT_TRI_BACK"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12, v11}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v9, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_TRI_BACK:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v11, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v13, "AUDIO_CHANNEL_OUT_3POINT1"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14, v13}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v11, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_3POINT1:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v13, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v15, "AUDIO_CHANNEL_OUT_2POINT0POINT2"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14, v15}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v13, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_2POINT0POINT2:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v15, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v14, "AUDIO_CHANNEL_OUT_2POINT1POINT2"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12, v14}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v15, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_2POINT1POINT2:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v14, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v12, "AUDIO_CHANNEL_OUT_3POINT0POINT2"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10, v12}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v14, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_3POINT0POINT2:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v12, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v10, "AUDIO_CHANNEL_OUT_3POINT1POINT2"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8, v10}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v12, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_3POINT1POINT2:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v10, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v8, "AUDIO_CHANNEL_OUT_QUAD"

    const/16 v6, 0xb

    const-string v4, "AUDIO_CHANNEL_OUT_QUAD"

    invoke-direct {v10, v8, v6, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v10, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_QUAD:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_OUT_QUAD_BACK"

    const/16 v8, 0xc

    const-string v2, "AUDIO_CHANNEL_OUT_QUAD_BACK"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_QUAD_BACK:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_OUT_QUAD_SIDE"

    const/16 v8, 0xd

    move-object/from16 v16, v4

    const-string v4, "AUDIO_CHANNEL_OUT_QUAD_SIDE"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_QUAD_SIDE:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_OUT_SURROUND"

    const/16 v8, 0xe

    move-object/from16 v17, v2

    const-string v2, "AUDIO_CHANNEL_OUT_SURROUND"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_SURROUND:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_OUT_PENTA"

    const/16 v8, 0xf

    move-object/from16 v18, v4

    const-string v4, "AUDIO_CHANNEL_OUT_PENTA"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_PENTA:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_OUT_5POINT1"

    const/16 v8, 0x10

    move-object/from16 v19, v2

    const-string v2, "AUDIO_CHANNEL_OUT_5POINT1"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_5POINT1:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_OUT_5POINT1_BACK"

    const/16 v8, 0x11

    move-object/from16 v20, v4

    const-string v4, "AUDIO_CHANNEL_OUT_5POINT1_BACK"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_5POINT1_BACK:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_OUT_5POINT1_SIDE"

    const/16 v8, 0x12

    move-object/from16 v21, v2

    const-string v2, "AUDIO_CHANNEL_OUT_5POINT1_SIDE"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_5POINT1_SIDE:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_OUT_5POINT1POINT2"

    const/16 v8, 0x13

    move-object/from16 v22, v4

    const-string v4, "AUDIO_CHANNEL_OUT_5POINT1POINT2"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_5POINT1POINT2:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_OUT_5POINT1POINT4"

    const/16 v8, 0x14

    move-object/from16 v23, v2

    const-string v2, "AUDIO_CHANNEL_OUT_5POINT1POINT4"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_5POINT1POINT4:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_OUT_6POINT1"

    const/16 v8, 0x15

    move-object/from16 v24, v4

    const-string v4, "AUDIO_CHANNEL_OUT_6POINT1"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_6POINT1:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_OUT_7POINT1"

    const/16 v8, 0x16

    move-object/from16 v25, v2

    const-string v2, "AUDIO_CHANNEL_OUT_7POINT1"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_7POINT1:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_OUT_7POINT1POINT2"

    const/16 v8, 0x17

    move-object/from16 v26, v4

    const-string v4, "AUDIO_CHANNEL_OUT_7POINT1POINT2"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_7POINT1POINT2:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_OUT_7POINT1POINT4"

    const/16 v8, 0x18

    move-object/from16 v27, v2

    const-string v2, "AUDIO_CHANNEL_OUT_7POINT1POINT4"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_7POINT1POINT4:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_OUT_13POINT_360RA"

    const/16 v8, 0x19

    move-object/from16 v28, v4

    const-string v4, "AUDIO_CHANNEL_OUT_13POINT_360RA"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_13POINT_360RA:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_OUT_22POINT2"

    const/16 v8, 0x1a

    move-object/from16 v29, v2

    const-string v2, "AUDIO_CHANNEL_OUT_22POINT2"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_22POINT2:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_OUT_MONO_HAPTIC_A"

    const/16 v8, 0x1b

    move-object/from16 v30, v4

    const-string v4, "AUDIO_CHANNEL_OUT_MONO_HAPTIC_A"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_MONO_HAPTIC_A:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_OUT_STEREO_HAPTIC_A"

    const/16 v8, 0x1c

    move-object/from16 v31, v2

    const-string v2, "AUDIO_CHANNEL_OUT_STEREO_HAPTIC_A"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_STEREO_HAPTIC_A:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_OUT_HAPTIC_AB"

    const/16 v8, 0x1d

    move-object/from16 v32, v4

    const-string v4, "AUDIO_CHANNEL_OUT_HAPTIC_AB"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_HAPTIC_AB:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_OUT_MONO_HAPTIC_AB"

    const/16 v8, 0x1e

    move-object/from16 v33, v2

    const-string v2, "AUDIO_CHANNEL_OUT_MONO_HAPTIC_AB"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_MONO_HAPTIC_AB:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_OUT_STEREO_HAPTIC_AB"

    const/16 v8, 0x1f

    move-object/from16 v34, v4

    const-string v4, "AUDIO_CHANNEL_OUT_STEREO_HAPTIC_AB"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_OUT_STEREO_HAPTIC_AB:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_IN_MONO"

    const/16 v8, 0x20

    move-object/from16 v35, v2

    const-string v2, "AUDIO_CHANNEL_IN_MONO"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_IN_MONO:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_IN_STEREO"

    const/16 v8, 0x21

    move-object/from16 v36, v4

    const-string v4, "AUDIO_CHANNEL_IN_STEREO"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_IN_STEREO:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_IN_FRONT_BACK"

    const/16 v8, 0x22

    move-object/from16 v37, v2

    const-string v2, "AUDIO_CHANNEL_IN_FRONT_BACK"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_IN_FRONT_BACK:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_IN_6"

    const/16 v8, 0x23

    move-object/from16 v38, v4

    const-string v4, "AUDIO_CHANNEL_IN_6"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_IN_6:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_IN_2POINT0POINT2"

    const/16 v8, 0x24

    move-object/from16 v39, v2

    const-string v2, "AUDIO_CHANNEL_IN_2POINT0POINT2"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_IN_2POINT0POINT2:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_IN_2POINT1POINT2"

    const/16 v8, 0x25

    move-object/from16 v40, v4

    const-string v4, "AUDIO_CHANNEL_IN_2POINT1POINT2"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_IN_2POINT1POINT2:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_IN_3POINT0POINT2"

    const/16 v8, 0x26

    move-object/from16 v41, v2

    const-string v2, "AUDIO_CHANNEL_IN_3POINT0POINT2"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_IN_3POINT0POINT2:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_IN_3POINT1POINT2"

    const/16 v8, 0x27

    move-object/from16 v42, v4

    const-string v4, "AUDIO_CHANNEL_IN_3POINT1POINT2"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_IN_3POINT1POINT2:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_IN_5POINT1"

    const/16 v8, 0x28

    move-object/from16 v43, v2

    const-string v2, "AUDIO_CHANNEL_IN_5POINT1"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_IN_5POINT1:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_IN_VOICE_UPLINK_MONO"

    const/16 v8, 0x29

    move-object/from16 v44, v4

    const-string v4, "AUDIO_CHANNEL_IN_VOICE_UPLINK_MONO"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_IN_VOICE_UPLINK_MONO:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_IN_VOICE_DNLINK_MONO"

    const/16 v8, 0x2a

    move-object/from16 v45, v2

    const-string v2, "AUDIO_CHANNEL_IN_VOICE_DNLINK_MONO"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_IN_VOICE_DNLINK_MONO:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_IN_VOICE_CALL_MONO"

    const/16 v8, 0x2b

    move-object/from16 v46, v4

    const-string v4, "AUDIO_CHANNEL_IN_VOICE_CALL_MONO"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_IN_VOICE_CALL_MONO:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_INDEX_MASK_1"

    const/16 v8, 0x2c

    move-object/from16 v47, v2

    const-string v2, "AUDIO_CHANNEL_INDEX_MASK_1"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_INDEX_MASK_1:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_INDEX_MASK_2"

    const/16 v8, 0x2d

    move-object/from16 v48, v4

    const-string v4, "AUDIO_CHANNEL_INDEX_MASK_2"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_INDEX_MASK_2:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_INDEX_MASK_3"

    const/16 v8, 0x2e

    move-object/from16 v49, v2

    const-string v2, "AUDIO_CHANNEL_INDEX_MASK_3"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_INDEX_MASK_3:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_INDEX_MASK_4"

    const/16 v8, 0x2f

    move-object/from16 v50, v4

    const-string v4, "AUDIO_CHANNEL_INDEX_MASK_4"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_INDEX_MASK_4:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_INDEX_MASK_5"

    const/16 v8, 0x30

    move-object/from16 v51, v2

    const-string v2, "AUDIO_CHANNEL_INDEX_MASK_5"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_INDEX_MASK_5:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_INDEX_MASK_6"

    const/16 v8, 0x31

    move-object/from16 v52, v4

    const-string v4, "AUDIO_CHANNEL_INDEX_MASK_6"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_INDEX_MASK_6:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_INDEX_MASK_7"

    const/16 v8, 0x32

    move-object/from16 v53, v2

    const-string v2, "AUDIO_CHANNEL_INDEX_MASK_7"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_INDEX_MASK_7:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_INDEX_MASK_8"

    const/16 v8, 0x33

    move-object/from16 v54, v4

    const-string v4, "AUDIO_CHANNEL_INDEX_MASK_8"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_INDEX_MASK_8:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_INDEX_MASK_9"

    const/16 v8, 0x34

    move-object/from16 v55, v2

    const-string v2, "AUDIO_CHANNEL_INDEX_MASK_9"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_INDEX_MASK_9:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_INDEX_MASK_10"

    const/16 v8, 0x35

    move-object/from16 v56, v4

    const-string v4, "AUDIO_CHANNEL_INDEX_MASK_10"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_INDEX_MASK_10:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_INDEX_MASK_11"

    const/16 v8, 0x36

    move-object/from16 v57, v2

    const-string v2, "AUDIO_CHANNEL_INDEX_MASK_11"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_INDEX_MASK_11:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_INDEX_MASK_12"

    const/16 v8, 0x37

    move-object/from16 v58, v4

    const-string v4, "AUDIO_CHANNEL_INDEX_MASK_12"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_INDEX_MASK_12:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_INDEX_MASK_13"

    const/16 v8, 0x38

    move-object/from16 v59, v2

    const-string v2, "AUDIO_CHANNEL_INDEX_MASK_13"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_INDEX_MASK_13:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_INDEX_MASK_14"

    const/16 v8, 0x39

    move-object/from16 v60, v4

    const-string v4, "AUDIO_CHANNEL_INDEX_MASK_14"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_INDEX_MASK_14:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_INDEX_MASK_15"

    const/16 v8, 0x3a

    move-object/from16 v61, v2

    const-string v2, "AUDIO_CHANNEL_INDEX_MASK_15"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_INDEX_MASK_15:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_INDEX_MASK_16"

    const/16 v8, 0x3b

    move-object/from16 v62, v4

    const-string v4, "AUDIO_CHANNEL_INDEX_MASK_16"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_INDEX_MASK_16:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_INDEX_MASK_17"

    const/16 v8, 0x3c

    move-object/from16 v63, v2

    const-string v2, "AUDIO_CHANNEL_INDEX_MASK_17"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_INDEX_MASK_17:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_INDEX_MASK_18"

    const/16 v8, 0x3d

    move-object/from16 v64, v4

    const-string v4, "AUDIO_CHANNEL_INDEX_MASK_18"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_INDEX_MASK_18:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_INDEX_MASK_19"

    const/16 v8, 0x3e

    move-object/from16 v65, v2

    const-string v2, "AUDIO_CHANNEL_INDEX_MASK_19"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_INDEX_MASK_19:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_INDEX_MASK_20"

    const/16 v8, 0x3f

    move-object/from16 v66, v4

    const-string v4, "AUDIO_CHANNEL_INDEX_MASK_20"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_INDEX_MASK_20:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_INDEX_MASK_21"

    const/16 v8, 0x40

    move-object/from16 v67, v2

    const-string v2, "AUDIO_CHANNEL_INDEX_MASK_21"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_INDEX_MASK_21:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_INDEX_MASK_22"

    const/16 v8, 0x41

    move-object/from16 v68, v4

    const-string v4, "AUDIO_CHANNEL_INDEX_MASK_22"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_INDEX_MASK_22:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_INDEX_MASK_23"

    const/16 v8, 0x42

    move-object/from16 v69, v2

    const-string v2, "AUDIO_CHANNEL_INDEX_MASK_23"

    invoke-direct {v4, v6, v8, v2}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_INDEX_MASK_23:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    new-instance v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const-string v6, "AUDIO_CHANNEL_INDEX_MASK_24"

    const/16 v8, 0x43

    move-object/from16 v70, v4

    const-string v4, "AUDIO_CHANNEL_INDEX_MASK_24"

    invoke-direct {v2, v6, v8, v4}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->AUDIO_CHANNEL_INDEX_MASK_24:Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const/16 v4, 0x44

    new-array v4, v4, [Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    const/4 v6, 0x0

    aput-object v0, v4, v6

    const/4 v0, 0x1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    aput-object v3, v4, v0

    const/4 v0, 0x3

    aput-object v5, v4, v0

    const/4 v0, 0x4

    aput-object v7, v4, v0

    const/4 v0, 0x5

    aput-object v9, v4, v0

    const/4 v0, 0x6

    aput-object v11, v4, v0

    const/4 v0, 0x7

    aput-object v13, v4, v0

    const/16 v0, 0x8

    aput-object v15, v4, v0

    const/16 v0, 0x9

    aput-object v14, v4, v0

    const/16 v0, 0xa

    aput-object v12, v4, v0

    const/16 v0, 0xb

    aput-object v10, v4, v0

    const/16 v0, 0xc

    aput-object v16, v4, v0

    const/16 v0, 0xd

    aput-object v17, v4, v0

    const/16 v0, 0xe

    aput-object v18, v4, v0

    const/16 v0, 0xf

    aput-object v19, v4, v0

    const/16 v0, 0x10

    aput-object v20, v4, v0

    const/16 v0, 0x11

    aput-object v21, v4, v0

    const/16 v0, 0x12

    aput-object v22, v4, v0

    const/16 v0, 0x13

    aput-object v23, v4, v0

    const/16 v0, 0x14

    aput-object v24, v4, v0

    const/16 v0, 0x15

    aput-object v25, v4, v0

    const/16 v0, 0x16

    aput-object v26, v4, v0

    const/16 v0, 0x17

    aput-object v27, v4, v0

    const/16 v0, 0x18

    aput-object v28, v4, v0

    const/16 v0, 0x19

    aput-object v29, v4, v0

    const/16 v0, 0x1a

    aput-object v30, v4, v0

    const/16 v0, 0x1b

    aput-object v31, v4, v0

    const/16 v0, 0x1c

    aput-object v32, v4, v0

    const/16 v0, 0x1d

    aput-object v33, v4, v0

    const/16 v0, 0x1e

    aput-object v34, v4, v0

    const/16 v0, 0x1f

    aput-object v35, v4, v0

    const/16 v0, 0x20

    aput-object v36, v4, v0

    const/16 v0, 0x21

    aput-object v37, v4, v0

    const/16 v0, 0x22

    aput-object v38, v4, v0

    const/16 v0, 0x23

    aput-object v39, v4, v0

    const/16 v0, 0x24

    aput-object v40, v4, v0

    const/16 v0, 0x25

    aput-object v41, v4, v0

    const/16 v0, 0x26

    aput-object v42, v4, v0

    const/16 v0, 0x27

    aput-object v43, v4, v0

    const/16 v0, 0x28

    aput-object v44, v4, v0

    const/16 v0, 0x29

    aput-object v45, v4, v0

    const/16 v0, 0x2a

    aput-object v46, v4, v0

    const/16 v0, 0x2b

    aput-object v47, v4, v0

    const/16 v0, 0x2c

    aput-object v48, v4, v0

    const/16 v0, 0x2d

    aput-object v49, v4, v0

    const/16 v0, 0x2e

    aput-object v50, v4, v0

    const/16 v0, 0x2f

    aput-object v51, v4, v0

    const/16 v0, 0x30

    aput-object v52, v4, v0

    const/16 v0, 0x31

    aput-object v53, v4, v0

    const/16 v0, 0x32

    aput-object v54, v4, v0

    const/16 v0, 0x33

    aput-object v55, v4, v0

    const/16 v0, 0x34

    aput-object v56, v4, v0

    const/16 v0, 0x35

    aput-object v57, v4, v0

    const/16 v0, 0x36

    aput-object v58, v4, v0

    const/16 v0, 0x37

    aput-object v59, v4, v0

    const/16 v0, 0x38

    aput-object v60, v4, v0

    const/16 v0, 0x39

    aput-object v61, v4, v0

    const/16 v0, 0x3a

    aput-object v62, v4, v0

    const/16 v0, 0x3b

    aput-object v63, v4, v0

    const/16 v0, 0x3c

    aput-object v64, v4, v0

    const/16 v0, 0x3d

    aput-object v65, v4, v0

    const/16 v0, 0x3e

    aput-object v66, v4, v0

    const/16 v0, 0x3f

    aput-object v67, v4, v0

    const/16 v0, 0x40

    aput-object v68, v4, v0

    const/16 v0, 0x41

    aput-object v69, v4, v0

    const/16 v0, 0x42

    aput-object v70, v4, v0

    const/16 v0, 0x43

    aput-object v2, v4, v0

    sput-object v4, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->$VALUES:[Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->rawName:Ljava/lang/String;

    return-void
.end method

.method static fromString(Ljava/lang/String;)Landroid/audio/policy/configuration/V7_0/AudioChannelMask;
    .locals 5

    invoke-static {}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->values()[Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    invoke-virtual {v3}, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->getRawName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/audio/policy/configuration/V7_0/AudioChannelMask;
    .locals 1

    const-class v0, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    return-object v0
.end method

.method public static values()[Landroid/audio/policy/configuration/V7_0/AudioChannelMask;
    .locals 1

    sget-object v0, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->$VALUES:[Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    invoke-virtual {v0}, [Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/audio/policy/configuration/V7_0/AudioChannelMask;

    return-object v0
.end method


# virtual methods
.method public getRawName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/audio/policy/configuration/V7_0/AudioChannelMask;->rawName:Ljava/lang/String;

    return-object v0
.end method
