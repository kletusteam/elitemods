.class public final Landroid/Manifest$permission_group;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/Manifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "permission_group"
.end annotation


# static fields
.field public static final ACTIVITY_RECOGNITION:Ljava/lang/String; = "android.permission-group.ACTIVITY_RECOGNITION"

.field public static final CALENDAR:Ljava/lang/String; = "android.permission-group.CALENDAR"

.field public static final CALL_LOG:Ljava/lang/String; = "android.permission-group.CALL_LOG"

.field public static final CAMERA:Ljava/lang/String; = "android.permission-group.CAMERA"

.field public static final CONTACTS:Ljava/lang/String; = "android.permission-group.CONTACTS"

.field public static final LOCATION:Ljava/lang/String; = "android.permission-group.LOCATION"

.field public static final MICROPHONE:Ljava/lang/String; = "android.permission-group.MICROPHONE"

.field public static final NEARBY_DEVICES:Ljava/lang/String; = "android.permission-group.NEARBY_DEVICES"

.field public static final NOTIFICATIONS:Ljava/lang/String; = "android.permission-group.NOTIFICATIONS"

.field public static final PHONE:Ljava/lang/String; = "android.permission-group.PHONE"

.field public static final READ_MEDIA_AURAL:Ljava/lang/String; = "android.permission-group.READ_MEDIA_AURAL"

.field public static final READ_MEDIA_VISUAL:Ljava/lang/String; = "android.permission-group.READ_MEDIA_VISUAL"

.field public static final SENSORS:Ljava/lang/String; = "android.permission-group.SENSORS"

.field public static final SMS:Ljava/lang/String; = "android.permission-group.SMS"

.field public static final STORAGE:Ljava/lang/String; = "android.permission-group.STORAGE"

.field public static final UNDEFINED:Ljava/lang/String; = "android.permission-group.UNDEFINED"
    .annotation runtime Landroid/annotation/SystemApi;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
