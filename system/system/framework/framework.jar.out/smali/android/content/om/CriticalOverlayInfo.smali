.class public interface abstract Landroid/content/om/CriticalOverlayInfo;
.super Ljava/lang/Object;
.source "CriticalOverlayInfo.java"


# virtual methods
.method public abstract getOverlayIdentifier()Landroid/content/om/OverlayIdentifier;
.end method

.method public abstract getOverlayName()Ljava/lang/String;
.end method

.method public abstract getPackageName()Ljava/lang/String;
.end method

.method public abstract getTargetOverlayableName()Ljava/lang/String;
.end method

.method public abstract getTargetPackageName()Ljava/lang/String;
.end method

.method public abstract isFabricated()Z
.end method
