.class interface abstract Landroid/content/res/IMiuiResource;
.super Ljava/lang/Object;
.source "IMiuiResource.java"


# virtual methods
.method public abstract createResources()Landroid/content/res/Resources;
.end method

.method public abstract createResources(Landroid/content/res/AssetManager;Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;)Landroid/content/res/Resources;
.end method

.method public abstract createResources(Ljava/lang/ClassLoader;)Landroid/content/res/Resources;
.end method

.method public abstract createResourcesImpl(Landroid/content/res/AssetManager;Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;Landroid/view/DisplayAdjustments;)Landroid/content/res/ResourcesImpl;
.end method

.method public abstract createTypedArray(Landroid/content/res/Resources;)Landroid/content/res/TypedArray;
.end method

.method public abstract initMiuiResource(Landroid/content/res/Resources;Ljava/lang/String;)V
.end method
