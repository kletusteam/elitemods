.class final Landroid/content/res/StringBlock$StyleIDs;
.super Ljava/lang/Object;
.source "StringBlock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/content/res/StringBlock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "StyleIDs"
.end annotation


# instance fields
.field private bigId:I

.field private boldId:I

.field private italicId:I

.field private listItemId:I

.field private marqueeId:I

.field private smallId:I

.field private strikeId:I

.field private subId:I

.field private supId:I

.field private ttId:I

.field private underlineId:I


# direct methods
.method static bridge synthetic -$$Nest$fgetbigId(Landroid/content/res/StringBlock$StyleIDs;)I
    .locals 0

    iget p0, p0, Landroid/content/res/StringBlock$StyleIDs;->bigId:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetboldId(Landroid/content/res/StringBlock$StyleIDs;)I
    .locals 0

    iget p0, p0, Landroid/content/res/StringBlock$StyleIDs;->boldId:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetitalicId(Landroid/content/res/StringBlock$StyleIDs;)I
    .locals 0

    iget p0, p0, Landroid/content/res/StringBlock$StyleIDs;->italicId:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetlistItemId(Landroid/content/res/StringBlock$StyleIDs;)I
    .locals 0

    iget p0, p0, Landroid/content/res/StringBlock$StyleIDs;->listItemId:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmarqueeId(Landroid/content/res/StringBlock$StyleIDs;)I
    .locals 0

    iget p0, p0, Landroid/content/res/StringBlock$StyleIDs;->marqueeId:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetsmallId(Landroid/content/res/StringBlock$StyleIDs;)I
    .locals 0

    iget p0, p0, Landroid/content/res/StringBlock$StyleIDs;->smallId:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetstrikeId(Landroid/content/res/StringBlock$StyleIDs;)I
    .locals 0

    iget p0, p0, Landroid/content/res/StringBlock$StyleIDs;->strikeId:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetsubId(Landroid/content/res/StringBlock$StyleIDs;)I
    .locals 0

    iget p0, p0, Landroid/content/res/StringBlock$StyleIDs;->subId:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetsupId(Landroid/content/res/StringBlock$StyleIDs;)I
    .locals 0

    iget p0, p0, Landroid/content/res/StringBlock$StyleIDs;->supId:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetttId(Landroid/content/res/StringBlock$StyleIDs;)I
    .locals 0

    iget p0, p0, Landroid/content/res/StringBlock$StyleIDs;->ttId:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetunderlineId(Landroid/content/res/StringBlock$StyleIDs;)I
    .locals 0

    iget p0, p0, Landroid/content/res/StringBlock$StyleIDs;->underlineId:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fputbigId(Landroid/content/res/StringBlock$StyleIDs;I)V
    .locals 0

    iput p1, p0, Landroid/content/res/StringBlock$StyleIDs;->bigId:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputboldId(Landroid/content/res/StringBlock$StyleIDs;I)V
    .locals 0

    iput p1, p0, Landroid/content/res/StringBlock$StyleIDs;->boldId:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputitalicId(Landroid/content/res/StringBlock$StyleIDs;I)V
    .locals 0

    iput p1, p0, Landroid/content/res/StringBlock$StyleIDs;->italicId:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputlistItemId(Landroid/content/res/StringBlock$StyleIDs;I)V
    .locals 0

    iput p1, p0, Landroid/content/res/StringBlock$StyleIDs;->listItemId:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmarqueeId(Landroid/content/res/StringBlock$StyleIDs;I)V
    .locals 0

    iput p1, p0, Landroid/content/res/StringBlock$StyleIDs;->marqueeId:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputsmallId(Landroid/content/res/StringBlock$StyleIDs;I)V
    .locals 0

    iput p1, p0, Landroid/content/res/StringBlock$StyleIDs;->smallId:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputstrikeId(Landroid/content/res/StringBlock$StyleIDs;I)V
    .locals 0

    iput p1, p0, Landroid/content/res/StringBlock$StyleIDs;->strikeId:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputsubId(Landroid/content/res/StringBlock$StyleIDs;I)V
    .locals 0

    iput p1, p0, Landroid/content/res/StringBlock$StyleIDs;->subId:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputsupId(Landroid/content/res/StringBlock$StyleIDs;I)V
    .locals 0

    iput p1, p0, Landroid/content/res/StringBlock$StyleIDs;->supId:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputttId(Landroid/content/res/StringBlock$StyleIDs;I)V
    .locals 0

    iput p1, p0, Landroid/content/res/StringBlock$StyleIDs;->ttId:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputunderlineId(Landroid/content/res/StringBlock$StyleIDs;I)V
    .locals 0

    iput p1, p0, Landroid/content/res/StringBlock$StyleIDs;->underlineId:I

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Landroid/content/res/StringBlock$StyleIDs;->boldId:I

    iput v0, p0, Landroid/content/res/StringBlock$StyleIDs;->italicId:I

    iput v0, p0, Landroid/content/res/StringBlock$StyleIDs;->underlineId:I

    iput v0, p0, Landroid/content/res/StringBlock$StyleIDs;->ttId:I

    iput v0, p0, Landroid/content/res/StringBlock$StyleIDs;->bigId:I

    iput v0, p0, Landroid/content/res/StringBlock$StyleIDs;->smallId:I

    iput v0, p0, Landroid/content/res/StringBlock$StyleIDs;->subId:I

    iput v0, p0, Landroid/content/res/StringBlock$StyleIDs;->supId:I

    iput v0, p0, Landroid/content/res/StringBlock$StyleIDs;->strikeId:I

    iput v0, p0, Landroid/content/res/StringBlock$StyleIDs;->listItemId:I

    iput v0, p0, Landroid/content/res/StringBlock$StyleIDs;->marqueeId:I

    return-void
.end method
