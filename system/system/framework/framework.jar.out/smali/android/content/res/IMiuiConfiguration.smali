.class public interface abstract Landroid/content/res/IMiuiConfiguration;
.super Ljava/lang/Object;
.source "IMiuiConfiguration.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Landroid/content/res/IMiuiConfiguration;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract diff(Landroid/content/res/IMiuiConfiguration;)I
.end method

.method public abstract readFromParcel(Landroid/os/Parcel;)V
.end method

.method public abstract setTo(Landroid/content/res/IMiuiConfiguration;)V
.end method

.method public abstract setToDefaults()V
.end method

.method public abstract updateFrom(Landroid/content/res/IMiuiConfiguration;)I
.end method

.method public abstract writeToParcel(Landroid/os/Parcel;I)V
.end method
