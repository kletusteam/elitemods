.class abstract Landroid/graphics/drawable/VectorDrawable$VObject;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/graphics/drawable/VectorDrawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "VObject"
.end annotation


# instance fields
.field mTreePtr:Lcom/android/internal/util/VirtualRefBasePtr;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/graphics/drawable/VectorDrawable$VObject;->mTreePtr:Lcom/android/internal/util/VirtualRefBasePtr;

    return-void
.end method


# virtual methods
.method abstract applyTheme(Landroid/content/res/Resources$Theme;)V
.end method

.method abstract canApplyTheme()Z
.end method

.method abstract getNativePtr()J
.end method

.method abstract getNativeSize()I
.end method

.method abstract getProperty(Ljava/lang/String;)Landroid/util/Property;
.end method

.method abstract hasFocusStateSpecified()Z
.end method

.method abstract inflate(Landroid/content/res/Resources;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V
.end method

.method abstract isStateful()Z
.end method

.method isTreeValid()Z
    .locals 4

    goto/32 :goto_1

    nop

    :goto_0
    const/4 v0, 0x1

    goto/32 :goto_3

    nop

    :goto_1
    iget-object v0, p0, Landroid/graphics/drawable/VectorDrawable$VObject;->mTreePtr:Lcom/android/internal/util/VirtualRefBasePtr;

    goto/32 :goto_b

    nop

    :goto_2
    cmp-long v0, v0, v2

    goto/32 :goto_9

    nop

    :goto_3
    goto :goto_8

    :goto_4
    goto/32 :goto_7

    nop

    :goto_5
    invoke-virtual {v0}, Lcom/android/internal/util/VirtualRefBasePtr;->get()J

    move-result-wide v0

    goto/32 :goto_6

    nop

    :goto_6
    const-wide/16 v2, 0x0

    goto/32 :goto_2

    nop

    :goto_7
    const/4 v0, 0x0

    :goto_8
    goto/32 :goto_a

    nop

    :goto_9
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_0

    nop

    :goto_a
    return v0

    :goto_b
    if-nez v0, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_5

    nop
.end method

.method abstract onStateChange([I)Z
.end method

.method setTree(Lcom/android/internal/util/VirtualRefBasePtr;)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iput-object p1, p0, Landroid/graphics/drawable/VectorDrawable$VObject;->mTreePtr:Lcom/android/internal/util/VirtualRefBasePtr;

    goto/32 :goto_0

    nop
.end method
