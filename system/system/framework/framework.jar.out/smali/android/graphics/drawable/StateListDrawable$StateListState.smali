.class Landroid/graphics/drawable/StateListDrawable$StateListState;
.super Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/graphics/drawable/StateListDrawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "StateListState"
.end annotation


# instance fields
.field mStateSets:[[I

.field mThemeAttrs:[I


# direct methods
.method constructor <init>(Landroid/graphics/drawable/StateListDrawable$StateListState;Landroid/graphics/drawable/StateListDrawable;Landroid/content/res/Resources;)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;-><init>(Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;Landroid/graphics/drawable/DrawableContainer;Landroid/content/res/Resources;)V

    if-eqz p1, :cond_0

    iget-object v0, p1, Landroid/graphics/drawable/StateListDrawable$StateListState;->mThemeAttrs:[I

    iput-object v0, p0, Landroid/graphics/drawable/StateListDrawable$StateListState;->mThemeAttrs:[I

    iget-object v0, p1, Landroid/graphics/drawable/StateListDrawable$StateListState;->mStateSets:[[I

    iput-object v0, p0, Landroid/graphics/drawable/StateListDrawable$StateListState;->mStateSets:[[I

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/graphics/drawable/StateListDrawable$StateListState;->mThemeAttrs:[I

    invoke-virtual {p0}, Landroid/graphics/drawable/StateListDrawable$StateListState;->getCapacity()I

    move-result v0

    new-array v0, v0, [[I

    iput-object v0, p0, Landroid/graphics/drawable/StateListDrawable$StateListState;->mStateSets:[[I

    :goto_0
    return-void
.end method


# virtual methods
.method addStateSet([ILandroid/graphics/drawable/Drawable;)I
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    aput-object p1, v1, v0

    goto/32 :goto_3

    nop

    :goto_1
    iget-object v1, p0, Landroid/graphics/drawable/StateListDrawable$StateListState;->mStateSets:[[I

    goto/32 :goto_0

    nop

    :goto_2
    invoke-virtual {p0, p2}, Landroid/graphics/drawable/StateListDrawable$StateListState;->addChild(Landroid/graphics/drawable/Drawable;)I

    move-result v0

    goto/32 :goto_1

    nop

    :goto_3
    return v0
.end method

.method public canApplyTheme()Z
    .locals 1

    iget-object v0, p0, Landroid/graphics/drawable/StateListDrawable$StateListState;->mThemeAttrs:[I

    if-nez v0, :cond_1

    invoke-super {p0}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->canApplyTheme()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public growArray(II)V
    .locals 3

    invoke-super {p0, p1, p2}, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;->growArray(II)V

    new-array v0, p2, [[I

    iget-object v1, p0, Landroid/graphics/drawable/StateListDrawable$StateListState;->mStateSets:[[I

    const/4 v2, 0x0

    invoke-static {v1, v2, v0, v2, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v0, p0, Landroid/graphics/drawable/StateListDrawable$StateListState;->mStateSets:[[I

    return-void
.end method

.method hasFocusStateSpecified()Z
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    invoke-static {v0, v1}, Landroid/util/StateSet;->containsAttribute([[II)Z

    move-result v0

    goto/32 :goto_2

    nop

    :goto_1
    const v1, 0x101009c

    goto/32 :goto_0

    nop

    :goto_2
    return v0

    :goto_3
    iget-object v0, p0, Landroid/graphics/drawable/StateListDrawable$StateListState;->mStateSets:[[I

    goto/32 :goto_1

    nop
.end method

.method indexOfStateSet([I)I
    .locals 4

    goto/32 :goto_e

    nop

    :goto_0
    aget-object v3, v0, v2

    goto/32 :goto_1

    nop

    :goto_1
    invoke-static {v3, p1}, Landroid/util/StateSet;->stateSetMatches([I[I)Z

    move-result v3

    goto/32 :goto_c

    nop

    :goto_2
    return v2

    :goto_3
    invoke-virtual {p0}, Landroid/graphics/drawable/StateListDrawable$StateListState;->getChildCount()I

    move-result v1

    goto/32 :goto_4

    nop

    :goto_4
    const/4 v2, 0x0

    :goto_5
    goto/32 :goto_d

    nop

    :goto_6
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_a

    nop

    :goto_7
    const/4 v2, -0x1

    goto/32 :goto_2

    nop

    :goto_8
    return v2

    :goto_9
    goto/32 :goto_6

    nop

    :goto_a
    goto :goto_5

    :goto_b
    goto/32 :goto_7

    nop

    :goto_c
    if-nez v3, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_8

    nop

    :goto_d
    if-lt v2, v1, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_0

    nop

    :goto_e
    iget-object v0, p0, Landroid/graphics/drawable/StateListDrawable$StateListState;->mStateSets:[[I

    goto/32 :goto_3

    nop
.end method

.method mutate()V
    .locals 4

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Landroid/graphics/drawable/StateListDrawable$StateListState;->mThemeAttrs:[I

    goto/32 :goto_1d

    nop

    :goto_1
    goto :goto_1c

    :goto_2
    goto/32 :goto_1b

    nop

    :goto_3
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_6

    nop

    :goto_4
    check-cast v0, [I

    goto/32 :goto_11

    nop

    :goto_5
    iput-object v0, p0, Landroid/graphics/drawable/StateListDrawable$StateListState;->mThemeAttrs:[I

    goto/32 :goto_10

    nop

    :goto_6
    goto :goto_d

    :goto_7
    goto/32 :goto_14

    nop

    :goto_8
    return-void

    :goto_9
    move-object v0, v1

    :goto_a
    goto/32 :goto_5

    nop

    :goto_b
    array-length v2, v0

    goto/32 :goto_1a

    nop

    :goto_c
    add-int/lit8 v0, v0, -0x1

    :goto_d
    goto/32 :goto_13

    nop

    :goto_e
    check-cast v3, [I

    goto/32 :goto_1

    nop

    :goto_f
    array-length v0, v0

    goto/32 :goto_c

    nop

    :goto_10
    iget-object v0, p0, Landroid/graphics/drawable/StateListDrawable$StateListState;->mStateSets:[[I

    goto/32 :goto_b

    nop

    :goto_11
    goto :goto_a

    :goto_12
    goto/32 :goto_9

    nop

    :goto_13
    if-gez v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_19

    nop

    :goto_14
    iput-object v2, p0, Landroid/graphics/drawable/StateListDrawable$StateListState;->mStateSets:[[I

    goto/32 :goto_8

    nop

    :goto_15
    aget-object v3, v3, v0

    goto/32 :goto_16

    nop

    :goto_16
    if-nez v3, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_1e

    nop

    :goto_17
    if-nez v0, :cond_2

    goto/32 :goto_12

    :cond_2
    goto/32 :goto_1f

    nop

    :goto_18
    aput-object v3, v2, v0

    goto/32 :goto_3

    nop

    :goto_19
    iget-object v3, p0, Landroid/graphics/drawable/StateListDrawable$StateListState;->mStateSets:[[I

    goto/32 :goto_15

    nop

    :goto_1a
    new-array v2, v2, [[I

    goto/32 :goto_f

    nop

    :goto_1b
    move-object v3, v1

    :goto_1c
    goto/32 :goto_18

    nop

    :goto_1d
    const/4 v1, 0x0

    goto/32 :goto_17

    nop

    :goto_1e
    invoke-virtual {v3}, [I->clone()Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_e

    nop

    :goto_1f
    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_4

    nop
.end method

.method public newDrawable()Landroid/graphics/drawable/Drawable;
    .locals 2

    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, v1}, Landroid/graphics/drawable/StateListDrawable;-><init>(Landroid/graphics/drawable/StateListDrawable$StateListState;Landroid/content/res/Resources;Landroid/graphics/drawable/StateListDrawable-IA;)V

    return-object v0
.end method

.method public newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 2

    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Landroid/graphics/drawable/StateListDrawable;-><init>(Landroid/graphics/drawable/StateListDrawable$StateListState;Landroid/content/res/Resources;Landroid/graphics/drawable/StateListDrawable-IA;)V

    return-object v0
.end method
