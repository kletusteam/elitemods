.class Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;
.super Landroid/graphics/drawable/StateListDrawable$StateListState;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/graphics/drawable/AnimatedStateListDrawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AnimatedStateListState"
.end annotation


# static fields
.field private static final REVERSED_BIT:J = 0x100000000L

.field private static final REVERSIBLE_FLAG_BIT:J = 0x200000000L


# instance fields
.field mAnimThemeAttrs:[I

.field mStateIds:Landroid/util/SparseIntArray;

.field mTransitions:Landroid/util/LongSparseLongArray;


# direct methods
.method constructor <init>(Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;Landroid/graphics/drawable/AnimatedStateListDrawable;Landroid/content/res/Resources;)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/graphics/drawable/StateListDrawable$StateListState;-><init>(Landroid/graphics/drawable/StateListDrawable$StateListState;Landroid/graphics/drawable/StateListDrawable;Landroid/content/res/Resources;)V

    if-eqz p1, :cond_0

    iget-object v0, p1, Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;->mAnimThemeAttrs:[I

    iput-object v0, p0, Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;->mAnimThemeAttrs:[I

    iget-object v0, p1, Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;->mTransitions:Landroid/util/LongSparseLongArray;

    iput-object v0, p0, Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;->mTransitions:Landroid/util/LongSparseLongArray;

    iget-object v0, p1, Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;->mStateIds:Landroid/util/SparseIntArray;

    iput-object v0, p0, Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;->mStateIds:Landroid/util/SparseIntArray;

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/util/LongSparseLongArray;

    invoke-direct {v0}, Landroid/util/LongSparseLongArray;-><init>()V

    iput-object v0, p0, Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;->mTransitions:Landroid/util/LongSparseLongArray;

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;->mStateIds:Landroid/util/SparseIntArray;

    :goto_0
    return-void
.end method

.method private static generateTransitionKey(II)J
    .locals 4

    int-to-long v0, p0

    const/16 v2, 0x20

    shl-long/2addr v0, v2

    int-to-long v2, p1

    or-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method addStateSet([ILandroid/graphics/drawable/Drawable;I)I
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    iget-object v1, p0, Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;->mStateIds:Landroid/util/SparseIntArray;

    goto/32 :goto_3

    nop

    :goto_1
    invoke-super {p0, p1, p2}, Landroid/graphics/drawable/StateListDrawable$StateListState;->addStateSet([ILandroid/graphics/drawable/Drawable;)I

    move-result v0

    goto/32 :goto_0

    nop

    :goto_2
    return v0

    :goto_3
    invoke-virtual {v1, v0, p3}, Landroid/util/SparseIntArray;->put(II)V

    goto/32 :goto_2

    nop
.end method

.method addTransition(IILandroid/graphics/drawable/Drawable;Z)I
    .locals 16

    goto/32 :goto_1b

    nop

    :goto_0
    move/from16 v8, p2

    :goto_1
    goto/32 :goto_4

    nop

    :goto_2
    invoke-virtual {v7, v3, v4, v8, v9}, Landroid/util/LongSparseLongArray;->append(JJ)V

    goto/32 :goto_e

    nop

    :goto_3
    int-to-long v8, v2

    goto/32 :goto_10

    nop

    :goto_4
    return v2

    :goto_5
    iget-object v11, v0, Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;->mTransitions:Landroid/util/LongSparseLongArray;

    goto/32 :goto_f

    nop

    :goto_6
    if-nez p4, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_b

    nop

    :goto_7
    move/from16 v7, p1

    goto/32 :goto_18

    nop

    :goto_8
    move/from16 v7, p1

    goto/32 :goto_0

    nop

    :goto_9
    or-long/2addr v12, v14

    goto/32 :goto_19

    nop

    :goto_a
    const-wide v14, 0x100000000L

    goto/32 :goto_9

    nop

    :goto_b
    const-wide v5, 0x200000000L

    :goto_c
    goto/32 :goto_16

    nop

    :goto_d
    invoke-static {v8, v7}, Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;->generateTransitionKey(II)J

    move-result-wide v9

    goto/32 :goto_5

    nop

    :goto_e
    if-nez p4, :cond_1

    goto/32 :goto_15

    :cond_1
    goto/32 :goto_7

    nop

    :goto_f
    int-to-long v12, v2

    goto/32 :goto_a

    nop

    :goto_10
    or-long/2addr v8, v5

    goto/32 :goto_2

    nop

    :goto_11
    move-object/from16 v1, p3

    goto/32 :goto_17

    nop

    :goto_12
    invoke-static/range {p1 .. p2}, Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;->generateTransitionKey(II)J

    move-result-wide v3

    goto/32 :goto_1a

    nop

    :goto_13
    invoke-virtual {v11, v9, v10, v12, v13}, Landroid/util/LongSparseLongArray;->append(JJ)V

    goto/32 :goto_14

    nop

    :goto_14
    goto :goto_1

    :goto_15
    goto/32 :goto_8

    nop

    :goto_16
    iget-object v7, v0, Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;->mTransitions:Landroid/util/LongSparseLongArray;

    goto/32 :goto_3

    nop

    :goto_17
    invoke-super {v0, v1}, Landroid/graphics/drawable/StateListDrawable$StateListState;->addChild(Landroid/graphics/drawable/Drawable;)I

    move-result v2

    goto/32 :goto_12

    nop

    :goto_18
    move/from16 v8, p2

    goto/32 :goto_d

    nop

    :goto_19
    or-long/2addr v12, v5

    goto/32 :goto_13

    nop

    :goto_1a
    const-wide/16 v5, 0x0

    goto/32 :goto_6

    nop

    :goto_1b
    move-object/from16 v0, p0

    goto/32 :goto_11

    nop
.end method

.method public canApplyTheme()Z
    .locals 1

    iget-object v0, p0, Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;->mAnimThemeAttrs:[I

    if-nez v0, :cond_1

    invoke-super {p0}, Landroid/graphics/drawable/StateListDrawable$StateListState;->canApplyTheme()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method getKeyframeIdAt(I)I
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    iget-object v1, p0, Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;->mStateIds:Landroid/util/SparseIntArray;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {v1, p1, v0}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    :goto_2
    goto/32 :goto_4

    nop

    :goto_3
    const/4 v0, 0x0

    goto/32 :goto_5

    nop

    :goto_4
    return v0

    :goto_5
    if-ltz p1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_6

    nop

    :goto_6
    goto :goto_2

    :goto_7
    goto/32 :goto_0

    nop
.end method

.method indexOfKeyframe([I)I
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    invoke-super {p0, p1}, Landroid/graphics/drawable/StateListDrawable$StateListState;->indexOfStateSet([I)I

    move-result v0

    goto/32 :goto_6

    nop

    :goto_1
    return v0

    :goto_2
    goto/32 :goto_3

    nop

    :goto_3
    sget-object v1, Landroid/util/StateSet;->WILD_CARD:[I

    goto/32 :goto_5

    nop

    :goto_4
    return v1

    :goto_5
    invoke-super {p0, v1}, Landroid/graphics/drawable/StateListDrawable$StateListState;->indexOfStateSet([I)I

    move-result v1

    goto/32 :goto_4

    nop

    :goto_6
    if-gez v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop
.end method

.method indexOfTransition(II)I
    .locals 5

    goto/32 :goto_3

    nop

    :goto_0
    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/util/LongSparseLongArray;->get(JJ)J

    move-result-wide v2

    goto/32 :goto_4

    nop

    :goto_1
    return v2

    :goto_2
    iget-object v2, p0, Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;->mTransitions:Landroid/util/LongSparseLongArray;

    goto/32 :goto_5

    nop

    :goto_3
    invoke-static {p1, p2}, Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;->generateTransitionKey(II)J

    move-result-wide v0

    goto/32 :goto_2

    nop

    :goto_4
    long-to-int v2, v2

    goto/32 :goto_1

    nop

    :goto_5
    const-wide/16 v3, -0x1

    goto/32 :goto_0

    nop
.end method

.method isTransitionReversed(II)Z
    .locals 6

    goto/32 :goto_e

    nop

    :goto_0
    const/4 v2, 0x0

    :goto_1
    goto/32 :goto_4

    nop

    :goto_2
    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/util/LongSparseLongArray;->get(JJ)J

    move-result-wide v2

    goto/32 :goto_3

    nop

    :goto_3
    const-wide v4, 0x100000000L

    goto/32 :goto_d

    nop

    :goto_4
    return v2

    :goto_5
    goto :goto_1

    :goto_6
    goto/32 :goto_0

    nop

    :goto_7
    iget-object v2, p0, Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;->mTransitions:Landroid/util/LongSparseLongArray;

    goto/32 :goto_c

    nop

    :goto_8
    if-nez v2, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_9

    nop

    :goto_9
    const/4 v2, 0x1

    goto/32 :goto_5

    nop

    :goto_a
    const-wide/16 v4, 0x0

    goto/32 :goto_b

    nop

    :goto_b
    cmp-long v2, v2, v4

    goto/32 :goto_8

    nop

    :goto_c
    const-wide/16 v3, -0x1

    goto/32 :goto_2

    nop

    :goto_d
    and-long/2addr v2, v4

    goto/32 :goto_a

    nop

    :goto_e
    invoke-static {p1, p2}, Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;->generateTransitionKey(II)J

    move-result-wide v0

    goto/32 :goto_7

    nop
.end method

.method mutate()V
    .locals 1

    goto/32 :goto_5

    nop

    :goto_0
    iget-object v0, p0, Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;->mStateIds:Landroid/util/SparseIntArray;

    goto/32 :goto_3

    nop

    :goto_1
    invoke-virtual {v0}, Landroid/util/LongSparseLongArray;->clone()Landroid/util/LongSparseLongArray;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_2
    iput-object v0, p0, Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;->mStateIds:Landroid/util/SparseIntArray;

    goto/32 :goto_6

    nop

    :goto_3
    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clone()Landroid/util/SparseIntArray;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_4
    iput-object v0, p0, Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;->mTransitions:Landroid/util/LongSparseLongArray;

    goto/32 :goto_0

    nop

    :goto_5
    iget-object v0, p0, Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;->mTransitions:Landroid/util/LongSparseLongArray;

    goto/32 :goto_1

    nop

    :goto_6
    return-void
.end method

.method public newDrawable()Landroid/graphics/drawable/Drawable;
    .locals 2

    new-instance v0, Landroid/graphics/drawable/AnimatedStateListDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, v1}, Landroid/graphics/drawable/AnimatedStateListDrawable;-><init>(Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;Landroid/content/res/Resources;Landroid/graphics/drawable/AnimatedStateListDrawable-IA;)V

    return-object v0
.end method

.method public newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 2

    new-instance v0, Landroid/graphics/drawable/AnimatedStateListDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Landroid/graphics/drawable/AnimatedStateListDrawable;-><init>(Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;Landroid/content/res/Resources;Landroid/graphics/drawable/AnimatedStateListDrawable-IA;)V

    return-object v0
.end method

.method transitionHasReversibleFlag(II)Z
    .locals 6

    goto/32 :goto_7

    nop

    :goto_0
    const-wide/16 v4, 0x0

    goto/32 :goto_2

    nop

    :goto_1
    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/util/LongSparseLongArray;->get(JJ)J

    move-result-wide v2

    goto/32 :goto_3

    nop

    :goto_2
    cmp-long v2, v2, v4

    goto/32 :goto_6

    nop

    :goto_3
    const-wide v4, 0x200000000L

    goto/32 :goto_4

    nop

    :goto_4
    and-long/2addr v2, v4

    goto/32 :goto_0

    nop

    :goto_5
    iget-object v2, p0, Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;->mTransitions:Landroid/util/LongSparseLongArray;

    goto/32 :goto_d

    nop

    :goto_6
    if-nez v2, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_e

    nop

    :goto_7
    invoke-static {p1, p2}, Landroid/graphics/drawable/AnimatedStateListDrawable$AnimatedStateListState;->generateTransitionKey(II)J

    move-result-wide v0

    goto/32 :goto_5

    nop

    :goto_8
    goto :goto_c

    :goto_9
    goto/32 :goto_b

    nop

    :goto_a
    return v2

    :goto_b
    const/4 v2, 0x0

    :goto_c
    goto/32 :goto_a

    nop

    :goto_d
    const-wide/16 v3, -0x1

    goto/32 :goto_1

    nop

    :goto_e
    const/4 v2, 0x1

    goto/32 :goto_8

    nop
.end method
