.class public interface abstract Landroid/companion/AssociationInfo$NonActionableBuilder;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/companion/AssociationInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "NonActionableBuilder"
.end annotation


# virtual methods
.method public abstract setLastTimeConnected(J)Landroid/companion/AssociationInfo$Builder;
.end method

.method public abstract setNotifyOnDeviceNearby(Z)Landroid/companion/AssociationInfo$Builder;
.end method
