.class public Lcom/EliteDevelopment/ext/SearchAppActivity$a;
.super Landroid/widget/ArrayAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/EliteDevelopment/ext/SearchAppActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/EliteDevelopment/ext/SearchAppActivity$a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter<",
        "Lp0/a;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Landroid/view/LayoutInflater;

.field public b:Lp0/a;

.field public final synthetic c:Lcom/EliteDevelopment/ext/SearchAppActivity;


# direct methods
.method public constructor <init>(Lcom/EliteDevelopment/ext/SearchAppActivity;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/EliteDevelopment/ext/SearchAppActivity$a;->c:Lcom/EliteDevelopment/ext/SearchAppActivity;

    const/4 p1, 0x0

    invoke-direct {p0, p2, p1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lcom/EliteDevelopment/ext/SearchAppActivity$a;->a:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public getItemId(I)J
    .locals 2

    invoke-virtual {p0, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lp0/a;

    iget p1, p1, Lp0/a;->a:I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    if-nez p2, :cond_0

    iget-object p2, p0, Lcom/EliteDevelopment/ext/SearchAppActivity$a;->a:Landroid/view/LayoutInflater;

    const v0, 0x7f0d0021

    const/4 v1, 0x0

    invoke-virtual {p2, v0, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance p3, Lcom/EliteDevelopment/ext/SearchAppActivity$a$a;

    invoke-direct {p3, p0, p2}, Lcom/EliteDevelopment/ext/SearchAppActivity$a$a;-><init>(Lcom/EliteDevelopment/ext/SearchAppActivity$a;Landroid/view/View;)V

    new-instance v0, Lo0/d;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p3, v1}, Lo0/d;-><init>(Landroid/widget/ArrayAdapter;Ljava/lang/Object;I)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p2, p3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/EliteDevelopment/ext/SearchAppActivity$a$a;

    :goto_0
    invoke-virtual {p0, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lp0/a;

    iput-object p1, p3, Lcom/EliteDevelopment/ext/SearchAppActivity$a$a;->a:Lp0/a;

    iget-object v0, p3, Lcom/EliteDevelopment/ext/SearchAppActivity$a$a;->b:Landroid/widget/ImageView;

    iget-object v1, p1, Lp0/a;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p3, Lcom/EliteDevelopment/ext/SearchAppActivity$a$a;->c:Landroid/widget/TextView;

    iget-object v1, p1, Lp0/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/EliteDevelopment/ext/SearchAppActivity$a;->b:Lp0/a;

    invoke-virtual {p1, v0}, Lp0/a;->equals(Ljava/lang/Object;)Z

    move-result p1

    move-object v0, p2

    check-cast v0, Landroid/widget/Checkable;

    invoke-interface {v0, p1}, Landroid/widget/Checkable;->setChecked(Z)V

    if-eqz p1, :cond_1

    iget-object p1, p3, Lcom/EliteDevelopment/ext/SearchAppActivity$a$a;->d:Landroid/widget/RadioButton;

    invoke-virtual {p1}, Landroid/widget/RadioButton;->getButtonDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    instance-of p3, p1, Landroid/graphics/drawable/StateListDrawable;

    if-eqz p3, :cond_1

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    instance-of p3, p1, Landroid/graphics/drawable/AnimatedVectorDrawable;

    if-eqz p3, :cond_1

    check-cast p1, Landroid/graphics/drawable/AnimatedVectorDrawable;

    invoke-virtual {p1}, Landroid/graphics/drawable/AnimatedVectorDrawable;->start()V

    :cond_1
    return-object p2
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
