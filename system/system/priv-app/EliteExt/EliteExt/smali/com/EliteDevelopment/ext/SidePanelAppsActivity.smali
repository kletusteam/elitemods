.class public Lcom/EliteDevelopment/ext/SidePanelAppsActivity;
.super Ld1/j;
.source ""

# interfaces
.implements Lq0/d$b;
.implements Lq0/g$a;
.implements Lq0/g$b;
.implements Lq0/g$c;
.implements Lg0/a$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/EliteDevelopment/ext/SidePanelAppsActivity$d;,
        Lcom/EliteDevelopment/ext/SidePanelAppsActivity$b;,
        Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;,
        Lcom/EliteDevelopment/ext/SidePanelAppsActivity$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ld1/j;",
        "Lq0/d$b;",
        "Lq0/g$a;",
        "Lq0/g$b;",
        "Lq0/g$c;",
        "Lg0/a$a<",
        "Ljava/util/List<",
        "Lp0/a;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final synthetic y:I


# instance fields
.field public n:Landroid/view/View;

.field public o:Landroidx/recyclerview/widget/RecyclerView;

.field public p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lp0/a;",
            ">;"
        }
    .end annotation
.end field

.field public q:Lcom/EliteDevelopment/ext/SidePanelAppsActivity$c;

.field public r:Lcom/EliteDevelopment/ext/SidePanelAppsActivity$b;

.field public s:Lq0/b;

.field public t:Lq0/g;

.field public u:Lq0/d;

.field public v:I

.field public w:Lmiuix/view/b;

.field public x:Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ld1/j;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->v:I

    return-void
.end method


# virtual methods
.method public final F()V
    .locals 7

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/EliteDevelopment/ext/LaunchAppPicker;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->p:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [I

    iget-object v2, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->p:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lp0/a;

    add-int/lit8 v6, v4, 0x1

    iget v5, v5, Lp0/a;->a:I

    aput v5, v1, v4

    move v4, v6

    goto :goto_0

    :cond_0
    const-string v2, "excludeIds"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    invoke-virtual {p0, v0, v3}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public final G()V
    .locals 4

    iget-object v0, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->p:Ljava/util/List;

    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lp0/a;

    iget-object v2, v2, Lp0/a;->b:Landroid/content/ComponentName;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->q:Lcom/EliteDevelopment/ext/SidePanelAppsActivity$c;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    :cond_2
    iget-object v0, v0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$c;->p:Landroid/content/ContentResolver;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "lockscreen_side_apps"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    return-void
.end method

.method public b(Landroidx/recyclerview/widget/RecyclerView$a0;Landroid/view/View;)Z
    .locals 2

    iget-object v0, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->w:Lmiuix/view/b;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    invoke-virtual {p2, v1}, Landroid/view/View;->setPressed(Z)V

    iget-object p2, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->x:Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;

    if-nez p2, :cond_0

    new-instance p2, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;

    const/4 v0, 0x0

    invoke-direct {p2, p0, v0}, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;-><init>(Lcom/EliteDevelopment/ext/SidePanelAppsActivity;Lcom/EliteDevelopment/ext/SidePanelAppsActivity$a;)V

    iput-object p2, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->x:Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;

    :cond_0
    iget-object p2, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->x:Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$a0;->e()I

    move-result p1

    iput p1, p2, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->a:I

    iget-object p1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->x:Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;

    invoke-virtual {p0, p1}, Ld1/j;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object p1

    check-cast p1, Lmiuix/view/b;

    iput-object p1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->w:Lmiuix/view/b;

    const/4 p1, 0x1

    return p1

    :cond_1
    return v1
.end method

.method public i(Lh0/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lh0/b<",
            "Ljava/util/List<",
            "Lp0/a;",
            ">;>;)V"
        }
    .end annotation

    return-void
.end method

.method public j(Landroidx/recyclerview/widget/RecyclerView$a0;Landroid/view/View;)V
    .locals 0

    iget-object p2, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->w:Lmiuix/view/b;

    if-nez p2, :cond_0

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$a0;->e()I

    move-result p1

    iput p1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->v:I

    invoke-virtual {p0}, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->F()V

    goto :goto_0

    :cond_0
    check-cast p1, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$d;

    iget-object p1, p1, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$d;->w:Landroid/widget/CheckBox;

    invoke-virtual {p1}, Landroid/widget/CheckBox;->performClick()Z

    :goto_0
    return-void
.end method

.method public k(Lh0/b;Ljava/lang/Object;)V
    .locals 7

    move-object v3, p2

    check-cast v3, Ljava/util/List;

    iput-object v3, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->p:Ljava/util/List;

    iget-object p1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->r:Lcom/EliteDevelopment/ext/SidePanelAppsActivity$b;

    iget-object v1, p1, Landroidx/recyclerview/widget/v;->d:Landroidx/recyclerview/widget/e;

    iget p1, v1, Landroidx/recyclerview/widget/e;->g:I

    add-int/lit8 v4, p1, 0x1

    iput v4, v1, Landroidx/recyclerview/widget/e;->g:I

    iget-object v2, v1, Landroidx/recyclerview/widget/e;->e:Ljava/util/List;

    const/4 p1, 0x0

    if-ne v3, v2, :cond_0

    goto :goto_1

    :cond_0
    iget-object p2, v1, Landroidx/recyclerview/widget/e;->f:Ljava/util/List;

    const/4 v0, 0x0

    if-nez v3, :cond_1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iput-object p1, v1, Landroidx/recyclerview/widget/e;->e:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    iput-object v3, v1, Landroidx/recyclerview/widget/e;->f:Ljava/util/List;

    iget-object v3, v1, Landroidx/recyclerview/widget/e;->a:Lj0/b;

    invoke-interface {v3, v0, v2}, Lj0/b;->b(II)V

    goto :goto_0

    :cond_1
    if-nez v2, :cond_2

    iput-object v3, v1, Landroidx/recyclerview/widget/e;->e:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, v1, Landroidx/recyclerview/widget/e;->f:Ljava/util/List;

    iget-object v2, v1, Landroidx/recyclerview/widget/e;->a:Lj0/b;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {v2, v0, v3}, Lj0/b;->c(II)V

    :goto_0
    invoke-virtual {v1, p2, p1}, Landroidx/recyclerview/widget/e;->a(Ljava/util/List;Ljava/lang/Runnable;)V

    goto :goto_1

    :cond_2
    iget-object p2, v1, Landroidx/recyclerview/widget/e;->b:Landroidx/recyclerview/widget/c;

    iget-object p2, p2, Landroidx/recyclerview/widget/c;->a:Ljava/util/concurrent/Executor;

    new-instance v6, Landroidx/recyclerview/widget/d;

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Landroidx/recyclerview/widget/d;-><init>(Landroidx/recyclerview/widget/e;Ljava/util/List;Ljava/util/List;ILjava/lang/Runnable;)V

    invoke-interface {p2, v6}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :goto_1
    iget-object p2, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->n:Landroid/view/View;

    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object p2, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->s:Lq0/b;

    iget-object v0, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->o:Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p2, Lq0/b;->a:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p2, Lq0/b;->b:Landroidx/recyclerview/widget/RecyclerView$e;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$e;

    move-result-object p1

    if-ne p1, v1, :cond_3

    goto :goto_2

    :cond_3
    if-eqz v1, :cond_4

    iget-object v0, p2, Lq0/b;->d:Landroidx/recyclerview/widget/RecyclerView$g;

    iget-object v1, v1, Landroidx/recyclerview/widget/RecyclerView$e;->a:Landroidx/recyclerview/widget/RecyclerView$f;

    invoke-virtual {v1, v0}, Landroid/database/Observable;->unregisterObserver(Ljava/lang/Object;)V

    :cond_4
    iput-object p1, p2, Lq0/b;->b:Landroidx/recyclerview/widget/RecyclerView$e;

    if-eqz p1, :cond_5

    iget-object v0, p2, Lq0/b;->d:Landroidx/recyclerview/widget/RecyclerView$g;

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$e;->a:Landroidx/recyclerview/widget/RecyclerView$f;

    invoke-virtual {p1, v0}, Landroid/database/Observable;->registerObserver(Ljava/lang/Object;)V

    :cond_5
    invoke-virtual {p2}, Lq0/b;->a()V

    :goto_2
    invoke-virtual {p0}, Ld1/j;->invalidateOptionsMenu()V

    return-void
.end method

.method public m(Landroidx/recyclerview/widget/RecyclerView$a0;Landroid/view/View;)Z
    .locals 3

    iget-object p2, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->u:Lq0/d;

    iget-object v0, p2, Landroidx/recyclerview/widget/s;->m:Landroidx/recyclerview/widget/s$d;

    iget-object v1, p2, Landroidx/recyclerview/widget/s;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, v1, p1}, Landroidx/recyclerview/widget/s$d;->d(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$a0;)I

    move-result v0

    const/high16 v1, 0xff0000

    and-int/2addr v0, v1

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    const-string p1, "Start drag has been called but dragging is not enabled"

    goto :goto_1

    :cond_1
    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$a0;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v2, p2, Landroidx/recyclerview/widget/s;->r:Landroidx/recyclerview/widget/RecyclerView;

    if-eq v0, v2, :cond_2

    const-string p1, "Start drag has been called with a view holder which is not a child of the RecyclerView which is controlled by this ItemTouchHelper."

    :goto_1
    const-string p2, "ItemTouchHelper"

    invoke-static {p2, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_2
    iget-object v0, p2, Landroidx/recyclerview/widget/s;->t:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    :cond_3
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p2, Landroidx/recyclerview/widget/s;->t:Landroid/view/VelocityTracker;

    const/4 v0, 0x0

    iput v0, p2, Landroidx/recyclerview/widget/s;->i:F

    iput v0, p2, Landroidx/recyclerview/widget/s;->h:F

    const/4 v0, 0x2

    invoke-virtual {p2, p1, v0}, Landroidx/recyclerview/widget/s;->r(Landroidx/recyclerview/widget/RecyclerView$a0;I)V

    :goto_2
    return v1
.end method

.method public o(ILandroid/os/Bundle;)Lh0/b;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lh0/b<",
            "Ljava/util/List<",
            "Lp0/a;",
            ">;>;"
        }
    .end annotation

    new-instance p1, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$c;

    invoke-direct {p1, p0}, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$c;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->q:Lcom/EliteDevelopment/ext/SidePanelAppsActivity$c;

    return-object p1
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    const/4 v0, -0x1

    if-ne p2, v0, :cond_3

    if-eqz p3, :cond_3

    iget-object v1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->q:Lcom/EliteDevelopment/ext/SidePanelAppsActivity$c;

    if-eqz v1, :cond_3

    invoke-virtual {p3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.MAIN"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "android.intent.category.LAUNCHER"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    iget-object v1, v1, Lp0/b;->n:Landroid/content/pm/PackageManager;

    const/4 v2, 0x0

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    invoke-static {v1, v2}, Lp0/a;->a(Landroid/content/pm/PackageManager;Landroid/content/pm/ComponentInfo;)Lp0/a;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v3

    :goto_0
    if-eqz v1, :cond_3

    iget-object v2, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->p:Ljava/util/List;

    iget-object v4, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->r:Lcom/EliteDevelopment/ext/SidePanelAppsActivity$b;

    iget v5, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->v:I

    const/4 v6, 0x1

    if-ltz v5, :cond_1

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lp0/a;

    invoke-virtual {v7, v1}, Lp0/a;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-interface {v2, v5, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v4, Landroidx/recyclerview/widget/RecyclerView$e;->a:Landroidx/recyclerview/widget/RecyclerView$f;

    invoke-virtual {v1, v5, v6, v3}, Landroidx/recyclerview/widget/RecyclerView$f;->d(IILjava/lang/Object;)V

    goto :goto_1

    :cond_1
    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, v4, Landroidx/recyclerview/widget/RecyclerView$e;->a:Landroidx/recyclerview/widget/RecyclerView$f;

    invoke-virtual {v1, v3, v6}, Landroidx/recyclerview/widget/RecyclerView$f;->e(II)V

    if-ne v3, v6, :cond_2

    invoke-virtual {v4}, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$b;->c()V

    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->G()V

    :cond_3
    iput v0, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->v:I

    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/i;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9

    iget-object v0, p0, Ld1/j;->m:Ld1/k;

    iget-object v1, v0, Ld1/k;->r:Ld1/d;

    check-cast v1, Ld1/j$b;

    iget-object v1, v1, Ld1/j$b;->a:Ld1/j;

    invoke-static {v1, p1}, Ld1/j;->w(Ld1/j;Landroid/os/Bundle;)V

    invoke-virtual {v0}, Ld1/k;->k()V

    const p1, 0x7f0d001d

    invoke-virtual {p0, p1}, Ld1/j;->setContentView(I)V

    const p1, 0x102000d

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->n:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    new-instance p1, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$b;

    invoke-direct {p1, p0, p0}, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$b;-><init>(Lcom/EliteDevelopment/ext/SidePanelAppsActivity;Landroid/content/Context;)V

    iput-object p1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->r:Lcom/EliteDevelopment/ext/SidePanelAppsActivity$b;

    const p1, 0x102000a

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    iget-object v2, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->r:Lcom/EliteDevelopment/ext/SidePanelAppsActivity$b;

    invoke-virtual {p1, v2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$e;)V

    new-instance v2, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {v2, p0}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$m;)V

    iput-object p1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->o:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v2, Lq0/b;

    const v3, 0x1020004

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-direct {v2, v3}, Lq0/b;-><init>(Landroid/view/View;)V

    iput-object v2, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->s:Lq0/b;

    new-instance v2, Lq0/g;

    invoke-direct {v2}, Lq0/g;-><init>()V

    iput-object v2, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->t:Lq0/g;

    iget-object v3, v2, Lq0/g;->d:Landroid/view/View$OnClickListener;

    if-nez v3, :cond_0

    new-instance v3, Lo0/b;

    invoke-direct {v3, v2, v1}, Lo0/b;-><init>(Ljava/lang/Object;I)V

    iput-object v3, v2, Lq0/g;->d:Landroid/view/View$OnClickListener;

    :cond_0
    iget-object v1, v2, Lq0/g;->g:Landroid/util/SparseArray;

    if-nez v1, :cond_1

    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, v2, Lq0/g;->g:Landroid/util/SparseArray;

    :cond_1
    iget-object v1, v2, Lq0/g;->g:Landroid/util/SparseArray;

    const/4 v3, -0x1

    invoke-virtual {v1, v3, p0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v1, v2, Lq0/a;->b:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    move v5, v0

    :goto_0
    if-ge v5, v4, :cond_3

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_2

    iget-object v7, v2, Lq0/g;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v6, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->t:Lq0/g;

    iget-object v2, v1, Lq0/g;->e:Landroid/view/View$OnLongClickListener;

    if-nez v2, :cond_4

    new-instance v2, Lq0/e;

    invoke-direct {v2, v1}, Lq0/e;-><init>(Lq0/g;)V

    iput-object v2, v1, Lq0/g;->e:Landroid/view/View$OnLongClickListener;

    :cond_4
    iget-object v2, v1, Lq0/g;->h:Landroid/util/SparseArray;

    if-nez v2, :cond_5

    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    iput-object v2, v1, Lq0/g;->h:Landroid/util/SparseArray;

    :cond_5
    iget-object v2, v1, Lq0/g;->h:Landroid/util/SparseArray;

    invoke-virtual {v2, v3, p0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v2, v1, Lq0/a;->b:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    move v5, v0

    :goto_1
    if-ge v5, v4, :cond_7

    invoke-virtual {v2, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_6

    iget-object v7, v1, Lq0/g;->e:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v6, v7}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_7
    iget-object v1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->t:Lq0/g;

    const v2, 0x7f0a0075

    iget-object v4, v1, Lq0/g;->c:Landroid/view/View$OnTouchListener;

    if-nez v4, :cond_8

    new-instance v4, Lq0/f;

    invoke-direct {v4, v1}, Lq0/f;-><init>(Lq0/g;)V

    iput-object v4, v1, Lq0/g;->c:Landroid/view/View$OnTouchListener;

    :cond_8
    iget-object v4, v1, Lq0/g;->f:Landroid/util/SparseArray;

    if-nez v4, :cond_9

    new-instance v4, Landroid/util/SparseArray;

    invoke-direct {v4}, Landroid/util/SparseArray;-><init>()V

    iput-object v4, v1, Lq0/g;->f:Landroid/util/SparseArray;

    :cond_9
    iget-object v4, v1, Lq0/g;->f:Landroid/util/SparseArray;

    invoke-virtual {v4, v2, p0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v4, v1, Lq0/a;->b:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v4, :cond_b

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    move v6, v0

    :goto_2
    if-ge v6, v5, :cond_b

    invoke-virtual {v4, v6}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    if-eqz v7, :cond_a

    iget-object v8, v1, Lq0/g;->c:Landroid/view/View$OnTouchListener;

    invoke-virtual {v7, v8}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_a
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :cond_b
    iget-object v1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->t:Lq0/g;

    invoke-virtual {v1, p1}, Lq0/a;->a(Landroidx/recyclerview/widget/RecyclerView;)V

    new-instance v1, Lq0/d;

    const/4 v2, 0x3

    invoke-direct {v1, v2, v0, p0}, Lq0/d;-><init>(IZLq0/d$b;)V

    iput-object v1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->u:Lq0/d;

    iget-object v2, v1, Landroidx/recyclerview/widget/s;->r:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v4, 0x0

    if-ne v2, p1, :cond_c

    goto/16 :goto_5

    :cond_c
    if-eqz v2, :cond_12

    invoke-virtual {v2, v1}, Landroidx/recyclerview/widget/RecyclerView;->Y(Landroidx/recyclerview/widget/RecyclerView$l;)V

    iget-object v2, v1, Landroidx/recyclerview/widget/s;->r:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v5, v1, Landroidx/recyclerview/widget/s;->z:Landroidx/recyclerview/widget/RecyclerView$q;

    iget-object v6, v2, Landroidx/recyclerview/widget/RecyclerView;->q:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v6, v2, Landroidx/recyclerview/widget/RecyclerView;->r:Landroidx/recyclerview/widget/RecyclerView$q;

    if-ne v6, v5, :cond_d

    iput-object v4, v2, Landroidx/recyclerview/widget/RecyclerView;->r:Landroidx/recyclerview/widget/RecyclerView$q;

    :cond_d
    iget-object v2, v1, Landroidx/recyclerview/widget/s;->r:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v2, v2, Landroidx/recyclerview/widget/RecyclerView;->C:Ljava/util/List;

    if-nez v2, :cond_e

    goto :goto_3

    :cond_e
    invoke-interface {v2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :goto_3
    iget-object v2, v1, Landroidx/recyclerview/widget/s;->p:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v2, v3

    :goto_4
    if-ltz v2, :cond_f

    iget-object v3, v1, Landroidx/recyclerview/widget/s;->p:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroidx/recyclerview/widget/s$f;

    iget-object v5, v3, Landroidx/recyclerview/widget/s$f;->g:Landroid/animation/ValueAnimator;

    invoke-virtual {v5}, Landroid/animation/ValueAnimator;->cancel()V

    iget-object v5, v1, Landroidx/recyclerview/widget/s;->m:Landroidx/recyclerview/widget/s$d;

    iget-object v6, v1, Landroidx/recyclerview/widget/s;->r:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v3, v3, Landroidx/recyclerview/widget/s$f;->e:Landroidx/recyclerview/widget/RecyclerView$a0;

    invoke-virtual {v5, v6, v3}, Landroidx/recyclerview/widget/s$d;->a(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$a0;)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_4

    :cond_f
    iget-object v2, v1, Landroidx/recyclerview/widget/s;->p:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    iput-object v4, v1, Landroidx/recyclerview/widget/s;->w:Landroid/view/View;

    iget-object v2, v1, Landroidx/recyclerview/widget/s;->t:Landroid/view/VelocityTracker;

    if-eqz v2, :cond_10

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->recycle()V

    iput-object v4, v1, Landroidx/recyclerview/widget/s;->t:Landroid/view/VelocityTracker;

    :cond_10
    iget-object v2, v1, Landroidx/recyclerview/widget/s;->y:Landroidx/recyclerview/widget/s$e;

    if-eqz v2, :cond_11

    iput-boolean v0, v2, Landroidx/recyclerview/widget/s$e;->a:Z

    iput-object v4, v1, Landroidx/recyclerview/widget/s;->y:Landroidx/recyclerview/widget/s$e;

    :cond_11
    iget-object v2, v1, Landroidx/recyclerview/widget/s;->x:Lw/e;

    if-eqz v2, :cond_12

    iput-object v4, v1, Landroidx/recyclerview/widget/s;->x:Lw/e;

    :cond_12
    iput-object p1, v1, Landroidx/recyclerview/widget/s;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v2, 0x7f070071

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    iput v2, v1, Landroidx/recyclerview/widget/s;->f:F

    const v2, 0x7f070070

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p1

    iput p1, v1, Landroidx/recyclerview/widget/s;->g:F

    iget-object p1, v1, Landroidx/recyclerview/widget/s;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result p1

    iput p1, v1, Landroidx/recyclerview/widget/s;->q:I

    iget-object p1, v1, Landroidx/recyclerview/widget/s;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p1, v1}, Landroidx/recyclerview/widget/RecyclerView;->f(Landroidx/recyclerview/widget/RecyclerView$l;)V

    iget-object p1, v1, Landroidx/recyclerview/widget/s;->r:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v2, v1, Landroidx/recyclerview/widget/s;->z:Landroidx/recyclerview/widget/RecyclerView$q;

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView;->q:Ljava/util/ArrayList;

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object p1, v1, Landroidx/recyclerview/widget/s;->r:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView;->C:Ljava/util/List;

    if-nez v2, :cond_13

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p1, Landroidx/recyclerview/widget/RecyclerView;->C:Ljava/util/List;

    :cond_13
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView;->C:Ljava/util/List;

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance p1, Landroidx/recyclerview/widget/s$e;

    invoke-direct {p1, v1}, Landroidx/recyclerview/widget/s$e;-><init>(Landroidx/recyclerview/widget/s;)V

    iput-object p1, v1, Landroidx/recyclerview/widget/s;->y:Landroidx/recyclerview/widget/s$e;

    new-instance p1, Lw/e;

    iget-object v2, v1, Landroidx/recyclerview/widget/s;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, v1, Landroidx/recyclerview/widget/s;->y:Landroidx/recyclerview/widget/s$e;

    invoke-direct {p1, v2, v3}, Lw/e;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object p1, v1, Landroidx/recyclerview/widget/s;->x:Lw/e;

    :goto_5
    invoke-static {p0}, Lg0/a;->b(Landroidx/lifecycle/j;)Lg0/a;

    move-result-object p1

    invoke-virtual {p1, v0, v4, p0}, Lg0/a;->c(ILandroid/os/Bundle;Lg0/a$a;)Lh0/b;

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    iget-object v0, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->q:Lcom/EliteDevelopment/ext/SidePanelAppsActivity$c;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, v0, Lp0/c;->l:Ljava/lang/Object;

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Ld1/j;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0e0003

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    return v2

    :cond_1
    return v1
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->q:Lcom/EliteDevelopment/ext/SidePanelAppsActivity$c;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lh0/b;->a()Z

    :cond_0
    invoke-super {p0}, Landroidx/fragment/app/i;->onDestroy()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const/4 v1, 0x1

    const v2, 0x7f0a0043

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->F()V

    return v1

    :cond_0
    const v2, 0x7f0a00c0

    if-ne v0, v2, :cond_1

    new-instance p1, Ld1/i$b;

    invoke-direct {p1, p0}, Ld1/i$b;-><init>(Landroid/content/Context;)V

    const v0, 0x7f120068

    iget-object v2, p1, Ld1/i$b;->a:Ld1/e$b;

    iget-object v3, v2, Ld1/e$b;->b:Landroid/content/Context;

    invoke-virtual {v3, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, v2, Ld1/e$b;->m:Ljava/lang/CharSequence;

    const v0, 0x7f120069

    iget-object v2, p1, Ld1/i$b;->a:Ld1/e$b;

    iget-object v3, v2, Ld1/e$b;->b:Landroid/content/Context;

    invoke-virtual {v3, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, v2, Ld1/e$b;->f:Ljava/lang/CharSequence;

    const v0, 0x104000a

    new-instance v2, Lo0/e;

    invoke-direct {v2, p0}, Lo0/e;-><init>(Lcom/EliteDevelopment/ext/SidePanelAppsActivity;)V

    iget-object v3, p1, Ld1/i$b;->a:Ld1/e$b;

    iget-object v4, v3, Ld1/e$b;->b:Landroid/content/Context;

    invoke-virtual {v4, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, v3, Ld1/e$b;->l:Ljava/lang/CharSequence;

    iget-object v0, p1, Ld1/i$b;->a:Ld1/e$b;

    iput-object v2, v0, Ld1/e$b;->k:Landroid/content/DialogInterface$OnClickListener;

    const/high16 v2, 0x1040000

    sget-object v3, Lo0/f;->a:Lo0/f;

    iget-object v4, v0, Ld1/e$b;->b:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, v0, Ld1/e$b;->h:Ljava/lang/CharSequence;

    iget-object v0, p1, Ld1/i$b;->a:Ld1/e$b;

    iput-object v3, v0, Ld1/e$b;->g:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p1}, Ld1/i$b;->a()Ld1/i;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return v1

    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method
