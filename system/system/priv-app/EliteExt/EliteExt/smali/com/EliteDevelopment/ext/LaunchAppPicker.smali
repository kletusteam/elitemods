.class public Lcom/EliteDevelopment/ext/LaunchAppPicker;
.super Ld1/j;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lg0/a$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/EliteDevelopment/ext/LaunchAppPicker$c;,
        Lcom/EliteDevelopment/ext/LaunchAppPicker$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ld1/j;",
        "Landroid/view/View$OnClickListener;",
        "Lg0/a$a<",
        "Ljava/util/List<",
        "Lp0/a;",
        ">;>;"
    }
.end annotation


# static fields
.field public static final synthetic z:I


# instance fields
.field public n:Landroid/widget/ListView;

.field public o:Landroid/view/View;

.field public p:Landroid/view/View;

.field public q:Landroid/view/View;

.field public r:Lp0/b;

.field public final s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lp0/a;",
            ">;"
        }
    .end annotation
.end field

.field public t:Lcom/EliteDevelopment/ext/LaunchAppPicker$b;

.field public final u:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public v:Landroid/view/View;

.field public w:Landroid/widget/TextView;

.field public x:Landroid/widget/TextView;

.field public final y:Landroid/view/ActionMode$Callback;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ld1/j;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->s:Ljava/util/List;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->u:Ljava/util/Set;

    new-instance v0, Lcom/EliteDevelopment/ext/LaunchAppPicker$c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/EliteDevelopment/ext/LaunchAppPicker$c;-><init>(Lcom/EliteDevelopment/ext/LaunchAppPicker;Lcom/EliteDevelopment/ext/LaunchAppPicker$a;)V

    iput-object v0, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->y:Landroid/view/ActionMode$Callback;

    return-void
.end method


# virtual methods
.method public final F()V
    .locals 2

    iget-object v0, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->t:Lcom/EliteDevelopment/ext/LaunchAppPicker$b;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    iget-object v0, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->t:Lcom/EliteDevelopment/ext/LaunchAppPicker$b;

    iget-object v1, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->s:Ljava/util/List;

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->addAll(Ljava/util/Collection;)V

    iget-object v0, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->x:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->x:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->n:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->q:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    return-void
.end method

.method public i(Lh0/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lh0/b<",
            "Ljava/util/List<",
            "Lp0/a;",
            ">;>;)V"
        }
    .end annotation

    return-void
.end method

.method public k(Lh0/b;Ljava/lang/Object;)V
    .locals 5

    check-cast p2, Ljava/util/List;

    iget-object p1, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->s:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    iget-object p1, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->u:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result p1

    const/4 v0, 0x1

    if-lez p1, :cond_1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v1, v0

    :goto_0
    if-ltz v1, :cond_1

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lp0/a;

    iget-object v3, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->u:Ljava/util/Set;

    iget v2, v2, Lp0/a;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    add-int/lit8 p1, p1, -0x1

    if-nez p1, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    :goto_1
    iget-object p1, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->s:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object p1, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->p:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p1

    iget-object p2, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->v:Landroid/view/View;

    const/4 v1, 0x0

    if-lez p1, :cond_2

    move v2, v0

    goto :goto_2

    :cond_2
    move v2, v1

    :goto_2
    invoke-virtual {p2, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object p2, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->w:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f100012

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {v2, v3, p1, v0}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/EliteDevelopment/ext/LaunchAppPicker;->F()V

    return-void
.end method

.method public o(ILandroid/os/Bundle;)Lh0/b;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lh0/b<",
            "Ljava/util/List<",
            "Lp0/a;",
            ">;>;"
        }
    .end annotation

    new-instance p1, Lp0/b;

    sget-object p2, Lp0/f;->a:Landroid/content/Intent;

    invoke-direct {p1, p0, p2}, Lp0/b;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    iput-object p1, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->r:Lp0/b;

    return-object p1
.end method

.method public onBackPressed()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->v:Landroid/view/View;

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->y:Landroid/view/ActionMode$Callback;

    invoke-virtual {p0, p1}, Ld1/j;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    iget-object v0, p0, Ld1/j;->m:Ld1/k;

    iget-object v1, v0, Ld1/k;->r:Ld1/d;

    check-cast v1, Ld1/j$b;

    iget-object v1, v1, Ld1/j$b;->a:Ld1/j;

    invoke-static {v1, p1}, Ld1/j;->w(Ld1/j;Landroid/os/Bundle;)V

    invoke-virtual {v0}, Ld1/k;->k()V

    const p1, 0x7f0d001f

    invoke-virtual {p0, p1}, Ld1/j;->setContentView(I)V

    const p1, 0x102003f

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->o:Landroid/view/View;

    const p1, 0x102000a

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    iput-object p1, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->n:Landroid/widget/ListView;

    const p1, 0x102000d

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->p:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    const p1, 0x1020004

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->q:Landroid/view/View;

    const p1, 0x7f0a00d6

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->v:Landroid/view/View;

    const v1, 0x1020009

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->w:Landroid/widget/TextView;

    const p1, 0x7f0a00d3

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->x:Landroid/widget/TextView;

    new-instance p1, Lcom/EliteDevelopment/ext/LaunchAppPicker$b;

    invoke-direct {p1, p0, p0}, Lcom/EliteDevelopment/ext/LaunchAppPicker$b;-><init>(Lcom/EliteDevelopment/ext/LaunchAppPicker;Landroid/content/Context;)V

    iput-object p1, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->t:Lcom/EliteDevelopment/ext/LaunchAppPicker$b;

    iget-object v1, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->n:Landroid/widget/ListView;

    invoke-virtual {v1, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object p1, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->v:Landroid/view/View;

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v1, "excludeIds"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object p1

    if-eqz p1, :cond_0

    array-length v1, p1

    move v2, v0

    :goto_0
    if-ge v2, v1, :cond_0

    aget v3, p1, v2

    iget-object v4, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->u:Ljava/util/Set;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-static {p0}, Lg0/a;->b(Landroidx/lifecycle/j;)Lg0/a;

    move-result-object p1

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, p0}, Lg0/a;->c(ILandroid/os/Bundle;Lg0/a$a;)Lh0/b;

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->r:Lp0/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lh0/b;->a()Z

    :cond_0
    invoke-super {p0}, Landroidx/fragment/app/i;->onDestroy()V

    return-void
.end method
