.class public Lcom/EliteDevelopment/ext/widget/CheckableLinearLayout;
.super Landroid/widget/LinearLayout;
.source ""

# interfaces
.implements Landroid/widget/Checkable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/EliteDevelopment/ext/widget/CheckableLinearLayout$b;
    }
.end annotation


# static fields
.field public static final b:Ljava/lang/String;

.field public static final c:[I


# instance fields
.field public a:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    const-class v0, Lcom/EliteDevelopment/ext/widget/CheckableLinearLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/EliteDevelopment/ext/widget/CheckableLinearLayout;->b:Ljava/lang/String;

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a0

    aput v2, v0, v1

    sput-object v0, Lcom/EliteDevelopment/ext/widget/CheckableLinearLayout;->c:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->setLongClickable(Z)V

    return-void
.end method


# virtual methods
.method public drawableStateChanged()V
    .locals 2

    invoke-super {p0}, Landroid/widget/LinearLayout;->drawableStateChanged()V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->invalidate()V

    :cond_0
    return-void
.end method

.method public isChecked()Z
    .locals 1

    iget-boolean v0, p0, Lcom/EliteDevelopment/ext/widget/CheckableLinearLayout;->a:Z

    return v0
.end method

.method public onCreateDrawableState(I)[I
    .locals 1

    add-int/lit8 p1, p1, 0x1

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onCreateDrawableState(I)[I

    move-result-object p1

    invoke-virtual {p0}, Lcom/EliteDevelopment/ext/widget/CheckableLinearLayout;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/EliteDevelopment/ext/widget/CheckableLinearLayout;->c:[I

    invoke-static {p1, v0}, Landroid/widget/LinearLayout;->mergeDrawableStates([I[I)[I

    :cond_0
    return-object p1
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    check-cast p1, Lcom/EliteDevelopment/ext/widget/CheckableLinearLayout$b;

    invoke-virtual {p1}, Landroid/view/View$BaseSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-boolean p1, p1, Lcom/EliteDevelopment/ext/widget/CheckableLinearLayout$b;->a:Z

    invoke-virtual {p0, p1}, Lcom/EliteDevelopment/ext/widget/CheckableLinearLayout;->setChecked(Z)V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->requestLayout()V

    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    new-instance v1, Lcom/EliteDevelopment/ext/widget/CheckableLinearLayout$b;

    invoke-direct {v1, v0}, Lcom/EliteDevelopment/ext/widget/CheckableLinearLayout$b;-><init>(Landroid/os/Parcelable;)V

    invoke-virtual {p0}, Lcom/EliteDevelopment/ext/widget/CheckableLinearLayout;->isChecked()Z

    move-result v0

    iput-boolean v0, v1, Lcom/EliteDevelopment/ext/widget/CheckableLinearLayout$b;->a:Z

    return-object v1
.end method

.method public setChecked(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/EliteDevelopment/ext/widget/CheckableLinearLayout;->a:Z

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->refreshDrawableState()V

    return-void
.end method

.method public toggle()V
    .locals 1

    iget-boolean v0, p0, Lcom/EliteDevelopment/ext/widget/CheckableLinearLayout;->a:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/EliteDevelopment/ext/widget/CheckableLinearLayout;->setChecked(Z)V

    return-void
.end method
