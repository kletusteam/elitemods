.class public Lcom/EliteDevelopment/ext/SearchAppActivity;
.super Ld1/j;
.source ""

# interfaces
.implements Lg0/a$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/EliteDevelopment/ext/SearchAppActivity$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ld1/j;",
        "Lg0/a$a<",
        "Ljava/util/List<",
        "Lp0/a;",
        ">;>;"
    }
.end annotation


# instance fields
.field public n:Landroid/widget/ListView;

.field public o:Landroid/view/View;

.field public p:Landroid/view/View;

.field public q:Lp0/b;

.field public r:Lcom/EliteDevelopment/ext/SearchAppActivity$a;

.field public s:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ld1/j;-><init>()V

    return-void
.end method


# virtual methods
.method public i(Lh0/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lh0/b<",
            "Ljava/util/List<",
            "Lp0/a;",
            ">;>;)V"
        }
    .end annotation

    return-void
.end method

.method public k(Lh0/b;Ljava/lang/Object;)V
    .locals 9

    check-cast p2, Ljava/util/List;

    iget-object p1, p0, Lcom/EliteDevelopment/ext/SearchAppActivity;->r:Lcom/EliteDevelopment/ext/SearchAppActivity$a;

    invoke-virtual {p1}, Landroid/widget/ArrayAdapter;->clear()V

    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    iget-object v0, p0, Lcom/EliteDevelopment/ext/SearchAppActivity;->s:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, "com.android.quicksearchbox"

    if-eqz v0, :cond_0

    move-object p1, v1

    :cond_0
    new-instance v0, Landroid/content/Intent;

    sget-object v2, Lp0/f;->b:Landroid/content/Intent;

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    if-nez v0, :cond_1

    const-string p1, "com.google.android.googlequicksearchbox"

    :cond_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, -0x1

    move v5, v2

    move v6, v5

    move v4, v3

    :goto_0
    if-ge v4, v0, :cond_4

    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lp0/a;

    iget-object v7, v7, Lp0/a;->b:Landroid/content/ComponentName;

    invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    if-ne v5, v2, :cond_2

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    move v5, v4

    :cond_2
    if-ne v6, v2, :cond_3

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    move v6, v4

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_4
    if-lez v5, :cond_6

    invoke-interface {p2, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lp0/a;

    invoke-interface {p2, v3, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    if-ne v6, v5, :cond_5

    goto :goto_1

    :cond_5
    add-int/lit8 v3, v6, 0x1

    goto :goto_1

    :cond_6
    move v3, v6

    :goto_1
    iget-object p1, p0, Lcom/EliteDevelopment/ext/SearchAppActivity;->r:Lcom/EliteDevelopment/ext/SearchAppActivity$a;

    invoke-virtual {p1, p2}, Landroid/widget/ArrayAdapter;->addAll(Ljava/util/Collection;)V

    if-eq v3, v2, :cond_7

    iget-object p1, p0, Lcom/EliteDevelopment/ext/SearchAppActivity;->r:Lcom/EliteDevelopment/ext/SearchAppActivity$a;

    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lp0/a;

    iput-object p2, p1, Lcom/EliteDevelopment/ext/SearchAppActivity$a;->b:Lp0/a;

    invoke-virtual {p1}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    :cond_7
    iget-object p1, p0, Lcom/EliteDevelopment/ext/SearchAppActivity;->o:Landroid/view/View;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/EliteDevelopment/ext/SearchAppActivity;->n:Landroid/widget/ListView;

    iget-object p2, p0, Lcom/EliteDevelopment/ext/SearchAppActivity;->p:Landroid/view/View;

    invoke-virtual {p1, p2}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    return-void
.end method

.method public o(ILandroid/os/Bundle;)Lh0/b;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lh0/b<",
            "Ljava/util/List<",
            "Lp0/a;",
            ">;>;"
        }
    .end annotation

    new-instance p1, Lp0/b;

    sget-object p2, Lp0/f;->b:Landroid/content/Intent;

    invoke-direct {p1, p0, p2}, Lp0/b;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    iput-object p1, p0, Lcom/EliteDevelopment/ext/SearchAppActivity;->q:Lp0/b;

    return-object p1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Ld1/j;->m:Ld1/k;

    iget-object v1, v0, Ld1/k;->r:Ld1/d;

    check-cast v1, Ld1/j$b;

    iget-object v1, v1, Ld1/j$b;->a:Ld1/j;

    invoke-static {v1, p1}, Ld1/j;->w(Ld1/j;Landroid/os/Bundle;)V

    invoke-virtual {v0}, Ld1/k;->k()V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_2

    const-string v0, "com.EliteDevelopment.ext.intent.action.SEARCH_BAR_APP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "com.EliteDevelopment.ext.intent.action.SEARCH_GESTURE_APP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_1

    :cond_0
    const p1, 0x7f1200c7

    invoke-virtual {p0, p1}, Landroid/app/Activity;->setTitle(I)V

    const-string p1, "key_home_screen_search_gesture_provider"

    goto :goto_0

    :cond_1
    const p1, 0x7f1200c6

    invoke-virtual {p0, p1}, Landroid/app/Activity;->setTitle(I)V

    const-string p1, "key_home_screen_search_bar_provider"

    :goto_0
    iput-object p1, p0, Lcom/EliteDevelopment/ext/SearchAppActivity;->s:Ljava/lang/String;

    :cond_2
    :goto_1
    iget-object p1, p0, Lcom/EliteDevelopment/ext/SearchAppActivity;->s:Ljava/lang/String;

    if-nez p1, :cond_3

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_3
    const p1, 0x7f0d001c

    invoke-virtual {p0, p1}, Ld1/j;->setContentView(I)V

    const p1, 0x102000a

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    iput-object p1, p0, Lcom/EliteDevelopment/ext/SearchAppActivity;->n:Landroid/widget/ListView;

    const p1, 0x102000d

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/EliteDevelopment/ext/SearchAppActivity;->o:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    const p1, 0x1020004

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/EliteDevelopment/ext/SearchAppActivity;->p:Landroid/view/View;

    new-instance p1, Lcom/EliteDevelopment/ext/SearchAppActivity$a;

    invoke-direct {p1, p0, p0}, Lcom/EliteDevelopment/ext/SearchAppActivity$a;-><init>(Lcom/EliteDevelopment/ext/SearchAppActivity;Landroid/content/Context;)V

    iput-object p1, p0, Lcom/EliteDevelopment/ext/SearchAppActivity;->r:Lcom/EliteDevelopment/ext/SearchAppActivity$a;

    iget-object v1, p0, Lcom/EliteDevelopment/ext/SearchAppActivity;->n:Landroid/widget/ListView;

    invoke-virtual {v1, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-static {p0}, Lg0/a;->b(Landroidx/lifecycle/j;)Lg0/a;

    move-result-object p1

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, p0}, Lg0/a;->c(ILandroid/os/Bundle;Lg0/a$a;)Lh0/b;

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/EliteDevelopment/ext/SearchAppActivity;->q:Lp0/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lh0/b;->a()Z

    :cond_0
    invoke-super {p0}, Landroidx/fragment/app/i;->onDestroy()V

    return-void
.end method
