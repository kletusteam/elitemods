.class public Lcom/EliteDevelopment/ext/SidePanelAppsActivity$b;
.super Landroidx/recyclerview/widget/v;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/EliteDevelopment/ext/SidePanelAppsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/v<",
        "Lp0/a;",
        "Lcom/EliteDevelopment/ext/SidePanelAppsActivity$d;",
        ">;"
    }
.end annotation


# instance fields
.field public final f:Landroid/view/LayoutInflater;

.field public g:Landroidx/recyclerview/widget/RecyclerView;

.field public final synthetic h:Lcom/EliteDevelopment/ext/SidePanelAppsActivity;


# direct methods
.method public constructor <init>(Lcom/EliteDevelopment/ext/SidePanelAppsActivity;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$b;->h:Lcom/EliteDevelopment/ext/SidePanelAppsActivity;

    sget-object p1, Lp0/a;->e:Landroidx/recyclerview/widget/p$e;

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/v;-><init>(Landroidx/recyclerview/widget/p$e;)V

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$e;->a:Landroidx/recyclerview/widget/RecyclerView$f;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$f;->a()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    iput-boolean p1, p0, Landroidx/recyclerview/widget/RecyclerView$e;->b:Z

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$b;->f:Landroid/view/LayoutInflater;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Cannot change whether this adapter has stable IDs while the adapter has registered observers."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public b(I)J
    .locals 2

    iget-object v0, p0, Landroidx/recyclerview/widget/v;->d:Landroidx/recyclerview/widget/e;

    iget-object v0, v0, Landroidx/recyclerview/widget/e;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lp0/a;

    iget p1, p1, Lp0/a;->a:I

    int-to-long v0, p1

    return-wide v0
.end method

.method public c()V
    .locals 4

    iget-object v0, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$b;->g:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroidx/recyclerview/widget/RecyclerView;->I(Landroid/view/View;)Landroidx/recyclerview/widget/RecyclerView$a0;

    move-result-object v3

    check-cast v3, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$d;

    invoke-virtual {p0, v3}, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$b;->d(Lcom/EliteDevelopment/ext/SidePanelAppsActivity$d;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final d(Lcom/EliteDevelopment/ext/SidePanelAppsActivity$d;)V
    .locals 6

    iget-object v0, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$b;->h:Lcom/EliteDevelopment/ext/SidePanelAppsActivity;

    iget-object v0, v0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->w:Lmiuix/view/b;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    iget-object v3, p1, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$d;->x:Landroid/view/View;

    const/16 v4, 0x8

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroidx/recyclerview/widget/v;->a()I

    move-result v5

    if-le v5, v1, :cond_1

    move v1, v2

    goto :goto_1

    :cond_1
    move v1, v4

    :goto_1
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p1, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$d;->w:Landroid/widget/CheckBox;

    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    move v2, v4

    :goto_2
    invoke-virtual {p1, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    return-void
.end method
