.class public Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ActionMode$Callback;
.implements Lq0/c$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/EliteDevelopment/ext/SidePanelAppsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "e"
.end annotation


# instance fields
.field public a:I

.field public b:Lq0/c;

.field public c:Landroid/view/MenuItem;

.field public final synthetic d:Lcom/EliteDevelopment/ext/SidePanelAppsActivity;


# direct methods
.method public constructor <init>(Lcom/EliteDevelopment/ext/SidePanelAppsActivity;Lcom/EliteDevelopment/ext/SidePanelAppsActivity$a;)V
    .locals 0

    iput-object p1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->d:Lcom/EliteDevelopment/ext/SidePanelAppsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroidx/recyclerview/widget/RecyclerView$a0;Z)V
    .locals 7

    iget-object p1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->d:Lcom/EliteDevelopment/ext/SidePanelAppsActivity;

    iget-object p1, p1, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->w:Lmiuix/view/b;

    if-nez p1, :cond_0

    return-void

    :cond_0
    move-object p2, p1

    check-cast p2, Landroid/view/ActionMode;

    iget-object v0, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->b:Lq0/c;

    invoke-virtual {v0}, Lq0/c;->d()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-lez v0, :cond_1

    iget-object v3, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->d:Lcom/EliteDevelopment/ext/SidePanelAppsActivity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f100013

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Landroid/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    const v3, 0x7f120114

    invoke-virtual {p2, v3}, Landroid/view/ActionMode;->setTitle(I)V

    :goto_0
    iget-object p2, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->c:Landroid/view/MenuItem;

    if-lez v0, :cond_2

    move v1, v2

    :cond_2
    invoke-interface {p2, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    iget-object p2, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->d:Lcom/EliteDevelopment/ext/SidePanelAppsActivity;

    invoke-static {p2}, Landroidx/emoji2/text/l;->I(Landroid/content/Context;)Z

    move-result p2

    const v1, 0x102001a

    if-lez v0, :cond_4

    iget-object v2, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->d:Lcom/EliteDevelopment/ext/SidePanelAppsActivity;

    iget-object v2, v2, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->r:Lcom/EliteDevelopment/ext/SidePanelAppsActivity$b;

    invoke-virtual {v2}, Landroidx/recyclerview/widget/v;->a()I

    move-result v2

    if-ne v0, v2, :cond_4

    if-eqz p2, :cond_3

    const p2, 0x7f080190

    goto :goto_1

    :cond_3
    const p2, 0x7f080193

    goto :goto_1

    :cond_4
    if-eqz p2, :cond_5

    const p2, 0x7f080196

    goto :goto_1

    :cond_5
    const p2, 0x7f080199

    :goto_1
    const-string v0, ""

    invoke-interface {p1, v1, v0, p2}, Lmiuix/view/b;->b(ILjava/lang/CharSequence;I)V

    return-void
.end method

.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 5

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result p2

    const/4 v0, 0x1

    const v1, 0x1020019

    if-ne p2, v1, :cond_1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    goto/16 :goto_4

    :cond_1
    const v1, 0x102001a

    if-ne p2, v1, :cond_4

    iget-object p1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->b:Lq0/c;

    invoke-virtual {p1}, Lq0/c;->d()I

    move-result p1

    if-lez p1, :cond_2

    iget-object p2, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->d:Lcom/EliteDevelopment/ext/SidePanelAppsActivity;

    iget-object p2, p2, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->r:Lcom/EliteDevelopment/ext/SidePanelAppsActivity$b;

    invoke-virtual {p2}, Landroidx/recyclerview/widget/v;->a()I

    move-result p2

    if-ne p1, p2, :cond_2

    iget-object p1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->b:Lq0/c;

    invoke-virtual {p1}, Lq0/c;->e()V

    goto/16 :goto_4

    :cond_2
    iget-object p1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->b:Lq0/c;

    iget-object p2, p1, Lq0/c;->f:Landroidx/recyclerview/widget/RecyclerView$e;

    invoke-virtual {p2}, Landroidx/recyclerview/widget/RecyclerView$e;->a()I

    move-result p2

    iget-object v1, p1, Lq0/c;->d:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-ge v1, p2, :cond_7

    iget-object v1, p1, Lq0/a;->b:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v2, 0x0

    if-nez v1, :cond_3

    :goto_1
    if-ge v2, p2, :cond_7

    iget-object v1, p1, Lq0/c;->d:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    :goto_2
    if-ge v2, p2, :cond_7

    iget-object v1, p1, Lq0/c;->d:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1, v2, v0}, Lq0/c;->f(IZ)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    const v1, 0x7f0a006d

    if-ne p2, v1, :cond_7

    iget-object p2, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->b:Lq0/c;

    invoke-virtual {p2}, Lq0/c;->d()I

    move-result p2

    if-lez p2, :cond_0

    iget-object p2, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->d:Lcom/EliteDevelopment/ext/SidePanelAppsActivity;

    iget-object v1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->b:Lq0/c;

    iget-object v1, v1, Lq0/c;->d:Ljava/util/Set;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    iget-object v2, p2, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->p:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    :cond_5
    :goto_3
    add-int/lit8 v3, v3, -0x1

    if-ltz v3, :cond_6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_3

    :cond_6
    iget-object v1, p2, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->r:Lcom/EliteDevelopment/ext/SidePanelAppsActivity$b;

    iget-object v1, v1, Landroidx/recyclerview/widget/RecyclerView$e;->a:Landroidx/recyclerview/widget/RecyclerView$f;

    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView$f;->b()V

    invoke-virtual {p2}, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->G()V

    goto/16 :goto_0

    :cond_7
    :goto_4
    return v0
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 3

    const v0, 0x7f120114

    invoke-virtual {p1, v0}, Landroid/view/ActionMode;->setTitle(I)V

    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0e0002

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v0, 0x7f0a006d

    invoke-interface {p2, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object p2

    iput-object p2, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->c:Landroid/view/MenuItem;

    check-cast p1, Lmiuix/view/b;

    iget-object p2, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->d:Lcom/EliteDevelopment/ext/SidePanelAppsActivity;

    iput-object p1, p2, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->w:Lmiuix/view/b;

    invoke-static {p2}, Landroidx/emoji2/text/l;->I(Landroid/content/Context;)Z

    move-result p2

    if-eqz p2, :cond_0

    const v0, 0x7f08017e

    goto :goto_0

    :cond_0
    const v0, 0x7f080181

    :goto_0
    const-string v1, ""

    const v2, 0x1020019

    invoke-interface {p1, v2, v1, v0}, Lmiuix/view/b;->b(ILjava/lang/CharSequence;I)V

    const v0, 0x102001a

    if-eqz p2, :cond_1

    const p2, 0x7f080196

    goto :goto_1

    :cond_1
    const p2, 0x7f080199

    :goto_1
    invoke-interface {p1, v0, v1, p2}, Lmiuix/view/b;->b(ILjava/lang/CharSequence;I)V

    iget-object p1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->d:Lcom/EliteDevelopment/ext/SidePanelAppsActivity;

    iget-object p1, p1, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->r:Lcom/EliteDevelopment/ext/SidePanelAppsActivity$b;

    invoke-virtual {p1}, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$b;->c()V

    new-instance p1, Lq0/c;

    const p2, 0x1020001

    iget-object v0, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->d:Lcom/EliteDevelopment/ext/SidePanelAppsActivity;

    iget-object v0, v0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->r:Lcom/EliteDevelopment/ext/SidePanelAppsActivity$b;

    invoke-direct {p1, p2, v0, p0}, Lq0/c;-><init>(ILandroidx/recyclerview/widget/RecyclerView$e;Lq0/c$c;)V

    iput-object p1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->b:Lq0/c;

    iget p2, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->a:I

    const/4 v0, 0x1

    if-ltz p2, :cond_2

    iget-object v1, p1, Lq0/c;->f:Landroidx/recyclerview/widget/RecyclerView$e;

    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView$e;->a()I

    move-result v1

    if-ge p2, v1, :cond_2

    iget-object v1, p1, Lq0/c;->d:Ljava/util/Set;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v1, p1, Lq0/a;->b:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v1, :cond_2

    invoke-virtual {p1, p2, v0}, Lq0/c;->f(IZ)V

    :cond_2
    iget-object p1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->b:Lq0/c;

    iget-object p2, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->d:Lcom/EliteDevelopment/ext/SidePanelAppsActivity;

    iget-object p2, p2, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->o:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p1, p2}, Lq0/c;->a(Landroidx/recyclerview/widget/RecyclerView;)V

    return v0
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 1

    iget-object p1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->d:Lcom/EliteDevelopment/ext/SidePanelAppsActivity;

    const/4 v0, 0x0

    iput-object v0, p1, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->w:Lmiuix/view/b;

    iget-object p1, p1, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->r:Lcom/EliteDevelopment/ext/SidePanelAppsActivity$b;

    invoke-virtual {p1}, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$b;->c()V

    iget-object p1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->b:Lq0/c;

    invoke-virtual {p1}, Lq0/c;->e()V

    iget-object p1, p0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->b:Lq0/c;

    invoke-virtual {p1, v0}, Lq0/c;->a(Landroidx/recyclerview/widget/RecyclerView;)V

    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method
