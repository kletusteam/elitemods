.class public Lcom/EliteDevelopment/ext/WifiRegionActivity;
.super Ld1/j;
.source ""

# interfaces
.implements Lg0/a$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/EliteDevelopment/ext/WifiRegionActivity$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ld1/j;",
        "Lg0/a$a<",
        "Ljava/util/List<",
        "Lp0/g;",
        ">;>;"
    }
.end annotation


# instance fields
.field public n:Landroid/widget/ListView;

.field public o:Landroid/view/View;

.field public p:Lp0/h;

.field public q:Lcom/EliteDevelopment/ext/WifiRegionActivity$a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ld1/j;-><init>()V

    return-void
.end method


# virtual methods
.method public i(Lh0/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lh0/b<",
            "Ljava/util/List<",
            "Lp0/g;",
            ">;>;)V"
        }
    .end annotation

    return-void
.end method

.method public k(Lh0/b;Ljava/lang/Object;)V
    .locals 4

    check-cast p2, Ljava/util/List;

    iget-object p1, p0, Lcom/EliteDevelopment/ext/WifiRegionActivity;->q:Lcom/EliteDevelopment/ext/WifiRegionActivity$a;

    invoke-virtual {p1}, Landroid/widget/ArrayAdapter;->clear()V

    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string v0, "wifi_country_code"

    invoke-static {p1, v0}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    move v2, v1

    :cond_0
    if-ge v2, v0, :cond_1

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lp0/g;

    iget-object v3, v3, Lp0/g;->b:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    add-int/lit8 v2, v2, 0x1

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_1
    move v2, v1

    :goto_0
    const/4 p1, 0x0

    invoke-interface {p2, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object p1, p0, Lcom/EliteDevelopment/ext/WifiRegionActivity;->q:Lcom/EliteDevelopment/ext/WifiRegionActivity$a;

    invoke-virtual {p1, p2}, Landroid/widget/ArrayAdapter;->addAll(Ljava/util/Collection;)V

    iget-object p1, p0, Lcom/EliteDevelopment/ext/WifiRegionActivity;->q:Lcom/EliteDevelopment/ext/WifiRegionActivity$a;

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lp0/g;

    iput-object p2, p1, Lcom/EliteDevelopment/ext/WifiRegionActivity$a;->b:Lp0/g;

    invoke-virtual {p1}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    iget-object p1, p0, Lcom/EliteDevelopment/ext/WifiRegionActivity;->o:Landroid/view/View;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public o(ILandroid/os/Bundle;)Lh0/b;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lh0/b<",
            "Ljava/util/List<",
            "Lp0/g;",
            ">;>;"
        }
    .end annotation

    new-instance p1, Lp0/h;

    invoke-direct {p1, p0}, Lp0/h;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/EliteDevelopment/ext/WifiRegionActivity;->p:Lp0/h;

    return-object p1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Ld1/j;->m:Ld1/k;

    iget-object v1, v0, Ld1/k;->r:Ld1/d;

    check-cast v1, Ld1/j$b;

    iget-object v1, v1, Ld1/j$b;->a:Ld1/j;

    invoke-static {v1, p1}, Ld1/j;->w(Ld1/j;Landroid/os/Bundle;)V

    invoke-virtual {v0}, Ld1/k;->k()V

    const p1, 0x7f0d0076

    invoke-virtual {p0, p1}, Ld1/j;->setContentView(I)V

    const p1, 0x102000a

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    iput-object p1, p0, Lcom/EliteDevelopment/ext/WifiRegionActivity;->n:Landroid/widget/ListView;

    const p1, 0x102000d

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/EliteDevelopment/ext/WifiRegionActivity;->o:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    new-instance p1, Lcom/EliteDevelopment/ext/WifiRegionActivity$a;

    invoke-direct {p1, p0, p0}, Lcom/EliteDevelopment/ext/WifiRegionActivity$a;-><init>(Lcom/EliteDevelopment/ext/WifiRegionActivity;Landroid/content/Context;)V

    iput-object p1, p0, Lcom/EliteDevelopment/ext/WifiRegionActivity;->q:Lcom/EliteDevelopment/ext/WifiRegionActivity$a;

    iget-object v1, p0, Lcom/EliteDevelopment/ext/WifiRegionActivity;->n:Landroid/widget/ListView;

    invoke-virtual {v1, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-static {p0}, Lg0/a;->b(Landroidx/lifecycle/j;)Lg0/a;

    move-result-object p1

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, p0}, Lg0/a;->c(ILandroid/os/Bundle;Lg0/a$a;)Lh0/b;

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/EliteDevelopment/ext/WifiRegionActivity;->p:Lp0/h;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lh0/b;->a()Z

    :cond_0
    invoke-super {p0}, Landroidx/fragment/app/i;->onDestroy()V

    return-void
.end method
