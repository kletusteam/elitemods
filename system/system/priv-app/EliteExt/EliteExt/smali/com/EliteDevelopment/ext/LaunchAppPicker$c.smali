.class public Lcom/EliteDevelopment/ext/LaunchAppPicker$c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lmiuix/view/e$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/EliteDevelopment/ext/LaunchAppPicker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "c"
.end annotation


# instance fields
.field public final a:Landroid/text/TextWatcher;

.field public final synthetic b:Lcom/EliteDevelopment/ext/LaunchAppPicker;


# direct methods
.method public constructor <init>(Lcom/EliteDevelopment/ext/LaunchAppPicker;Lcom/EliteDevelopment/ext/LaunchAppPicker$a;)V
    .locals 0

    iput-object p1, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker$c;->b:Lcom/EliteDevelopment/ext/LaunchAppPicker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance p1, Lcom/EliteDevelopment/ext/LaunchAppPicker$c$a;

    invoke-direct {p1, p0}, Lcom/EliteDevelopment/ext/LaunchAppPicker$c$a;-><init>(Lcom/EliteDevelopment/ext/LaunchAppPicker$c;)V

    iput-object p1, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker$c;->a:Landroid/text/TextWatcher;

    return-void
.end method


# virtual methods
.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 0

    check-cast p1, Lmiuix/view/e;

    iget-object p2, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker$c;->b:Lcom/EliteDevelopment/ext/LaunchAppPicker;

    iget-object p2, p2, Lcom/EliteDevelopment/ext/LaunchAppPicker;->v:Landroid/view/View;

    invoke-interface {p1, p2}, Lmiuix/view/e;->h(Landroid/view/View;)V

    iget-object p2, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker$c;->b:Lcom/EliteDevelopment/ext/LaunchAppPicker;

    iget-object p2, p2, Lcom/EliteDevelopment/ext/LaunchAppPicker;->o:Landroid/view/View;

    invoke-interface {p1, p2}, Lmiuix/view/e;->e(Landroid/view/View;)V

    invoke-interface {p1}, Lmiuix/view/e;->c()Landroid/widget/EditText;

    move-result-object p1

    iget-object p2, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker$c;->a:Landroid/text/TextWatcher;

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    const/4 p1, 0x1

    return p1
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 1

    check-cast p1, Lmiuix/view/e;

    invoke-interface {p1}, Lmiuix/view/e;->c()Landroid/widget/EditText;

    move-result-object p1

    iget-object v0, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker$c;->a:Landroid/text/TextWatcher;

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object p1, p0, Lcom/EliteDevelopment/ext/LaunchAppPicker$c;->b:Lcom/EliteDevelopment/ext/LaunchAppPicker;

    sget v0, Lcom/EliteDevelopment/ext/LaunchAppPicker;->z:I

    invoke-virtual {p1}, Lcom/EliteDevelopment/ext/LaunchAppPicker;->F()V

    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method
