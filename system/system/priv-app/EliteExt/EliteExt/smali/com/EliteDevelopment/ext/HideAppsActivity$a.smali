.class public Lcom/EliteDevelopment/ext/HideAppsActivity$a;
.super Landroid/widget/ArrayAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/EliteDevelopment/ext/HideAppsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/EliteDevelopment/ext/HideAppsActivity$a$b;,
        Lcom/EliteDevelopment/ext/HideAppsActivity$a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Landroid/view/LayoutInflater;

.field public final synthetic b:Lcom/EliteDevelopment/ext/HideAppsActivity;


# direct methods
.method public constructor <init>(Lcom/EliteDevelopment/ext/HideAppsActivity;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/EliteDevelopment/ext/HideAppsActivity$a;->b:Lcom/EliteDevelopment/ext/HideAppsActivity;

    const/4 p1, 0x0

    invoke-direct {p0, p2, p1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lcom/EliteDevelopment/ext/HideAppsActivity$a;->a:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public getItemId(I)J
    .locals 2

    invoke-virtual {p0, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result p1

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 0

    invoke-virtual {p0, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    instance-of p1, p1, Lcom/EliteDevelopment/ext/HideAppsActivity$b;

    return p1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    invoke-virtual {p0, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    instance-of v0, p1, Lcom/EliteDevelopment/ext/HideAppsActivity$b;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    if-nez p2, :cond_0

    iget-object p2, p0, Lcom/EliteDevelopment/ext/HideAppsActivity$a;->a:Landroid/view/LayoutInflater;

    const v0, 0x7f0d0027

    invoke-virtual {p2, v0, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance p3, Lcom/EliteDevelopment/ext/HideAppsActivity$a$b;

    invoke-direct {p3, p0, p2}, Lcom/EliteDevelopment/ext/HideAppsActivity$a$b;-><init>(Lcom/EliteDevelopment/ext/HideAppsActivity$a;Landroid/view/View;)V

    invoke-virtual {p2, p3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/EliteDevelopment/ext/HideAppsActivity$a$b;

    :goto_0
    check-cast p1, Lcom/EliteDevelopment/ext/HideAppsActivity$b;

    iget-object p3, p3, Lcom/EliteDevelopment/ext/HideAppsActivity$a$b;->a:Landroid/widget/TextView;

    iget-object p1, p1, Lcom/EliteDevelopment/ext/HideAppsActivity$b;->a:Ljava/lang/String;

    invoke-virtual {p3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_1
    if-nez p2, :cond_2

    iget-object p2, p0, Lcom/EliteDevelopment/ext/HideAppsActivity$a;->a:Landroid/view/LayoutInflater;

    const v0, 0x7f0d007c

    invoke-virtual {p2, v0, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance p3, Lcom/EliteDevelopment/ext/HideAppsActivity$a$a;

    invoke-direct {p3, p0, p2}, Lcom/EliteDevelopment/ext/HideAppsActivity$a$a;-><init>(Lcom/EliteDevelopment/ext/HideAppsActivity$a;Landroid/view/View;)V

    iget-object v0, p3, Lcom/EliteDevelopment/ext/HideAppsActivity$a$a;->d:Lmiuix/slidingwidget/widget/SlidingButton;

    new-instance v2, Lo0/c;

    invoke-direct {v2, p0, p3}, Lo0/c;-><init>(Lcom/EliteDevelopment/ext/HideAppsActivity$a;Lcom/EliteDevelopment/ext/HideAppsActivity$a$a;)V

    invoke-virtual {v0, v2}, Lmiuix/slidingwidget/widget/SlidingButton;->setOnPerformCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    new-instance v0, Lo0/b;

    invoke-direct {v0, p3, v1}, Lo0/b;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p2, p3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/EliteDevelopment/ext/HideAppsActivity$a$a;

    :goto_1
    check-cast p1, Lp0/a;

    iput-object p1, p3, Lcom/EliteDevelopment/ext/HideAppsActivity$a$a;->a:Lp0/a;

    iget-object v0, p3, Lcom/EliteDevelopment/ext/HideAppsActivity$a$a;->b:Landroid/widget/ImageView;

    iget-object v1, p1, Lp0/a;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p3, Lcom/EliteDevelopment/ext/HideAppsActivity$a$a;->c:Landroid/widget/TextView;

    iget-object v1, p1, Lp0/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p3, p3, Lcom/EliteDevelopment/ext/HideAppsActivity$a$a;->d:Lmiuix/slidingwidget/widget/SlidingButton;

    iget-object v0, p0, Lcom/EliteDevelopment/ext/HideAppsActivity$a;->b:Lcom/EliteDevelopment/ext/HideAppsActivity;

    iget-object v0, v0, Lcom/EliteDevelopment/ext/HideAppsActivity;->u:Ljava/util/Set;

    invoke-virtual {p1}, Lp0/a;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    invoke-virtual {p3, p1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    :goto_2
    return-object p2
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
