.class public Lcom/EliteDevelopment/ext/HideAppsActivity;
.super Ld1/j;
.source ""

# interfaces
.implements Lg0/a$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/EliteDevelopment/ext/HideAppsActivity$a;,
        Lcom/EliteDevelopment/ext/HideAppsActivity$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ld1/j;",
        "Lg0/a$a<",
        "Ljava/util/List<",
        "Lp0/a;",
        ">;>;"
    }
.end annotation


# instance fields
.field public n:Landroid/widget/ListView;

.field public o:Landroid/view/View;

.field public p:Landroid/view/View;

.field public q:Lp0/b;

.field public final r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lp0/a;",
            ">;"
        }
    .end annotation
.end field

.field public final s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lp0/a;",
            ">;"
        }
    .end annotation
.end field

.field public t:Lcom/EliteDevelopment/ext/HideAppsActivity$a;

.field public u:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ld1/j;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/EliteDevelopment/ext/HideAppsActivity;->r:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/EliteDevelopment/ext/HideAppsActivity;->s:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public i(Lh0/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lh0/b<",
            "Ljava/util/List<",
            "Lp0/a;",
            ">;>;)V"
        }
    .end annotation

    return-void
.end method

.method public k(Lh0/b;Ljava/lang/Object;)V
    .locals 2

    check-cast p2, Ljava/util/List;

    iget-object p1, p0, Lcom/EliteDevelopment/ext/HideAppsActivity;->s:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    iget-object p1, p0, Lcom/EliteDevelopment/ext/HideAppsActivity;->r:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lp0/a;

    iget-object v0, p0, Lcom/EliteDevelopment/ext/HideAppsActivity;->u:Ljava/util/Set;

    invoke-virtual {p2}, Lp0/a;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/EliteDevelopment/ext/HideAppsActivity;->s:Ljava/util/List;

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/EliteDevelopment/ext/HideAppsActivity;->r:Ljava/util/List;

    :goto_1
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/EliteDevelopment/ext/HideAppsActivity;->o:Landroid/view/View;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iget-object p2, p0, Lcom/EliteDevelopment/ext/HideAppsActivity;->s:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    new-instance v0, Lcom/EliteDevelopment/ext/HideAppsActivity$b;

    const v1, 0x7f1200c4

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/EliteDevelopment/ext/HideAppsActivity$b;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_2
    iget-object p2, p0, Lcom/EliteDevelopment/ext/HideAppsActivity;->r:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    new-instance v0, Lcom/EliteDevelopment/ext/HideAppsActivity$b;

    const v1, 0x7f1200c3

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/EliteDevelopment/ext/HideAppsActivity$b;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_3
    iget-object p2, p0, Lcom/EliteDevelopment/ext/HideAppsActivity;->t:Lcom/EliteDevelopment/ext/HideAppsActivity$a;

    invoke-virtual {p2}, Landroid/widget/ArrayAdapter;->clear()V

    invoke-virtual {p2, p1}, Landroid/widget/ArrayAdapter;->addAll(Ljava/util/Collection;)V

    iget-object p1, p0, Lcom/EliteDevelopment/ext/HideAppsActivity;->n:Landroid/widget/ListView;

    iget-object p2, p0, Lcom/EliteDevelopment/ext/HideAppsActivity;->p:Landroid/view/View;

    invoke-virtual {p1, p2}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    return-void
.end method

.method public o(ILandroid/os/Bundle;)Lh0/b;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lh0/b<",
            "Ljava/util/List<",
            "Lp0/a;",
            ">;>;"
        }
    .end annotation

    new-instance p1, Lp0/b;

    sget-object p2, Lp0/f;->a:Landroid/content/Intent;

    invoke-direct {p1, p0, p2}, Lp0/b;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    iput-object p1, p0, Lcom/EliteDevelopment/ext/HideAppsActivity;->q:Lp0/b;

    return-object p1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Ld1/j;->m:Ld1/k;

    iget-object v1, v0, Ld1/k;->r:Ld1/d;

    check-cast v1, Ld1/j$b;

    iget-object v1, v1, Ld1/j$b;->a:Ld1/j;

    invoke-static {v1, p1}, Ld1/j;->w(Ld1/j;Landroid/os/Bundle;)V

    invoke-virtual {v0}, Ld1/k;->k()V

    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string v0, "miui_home_hide_apps"

    invoke-static {p1, v0}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Lcom/EliteDevelopment/ext/HideAppsActivity;->u:Ljava/util/Set;

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/util/HashSet;

    const-string v1, ","

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/EliteDevelopment/ext/HideAppsActivity;->u:Ljava/util/Set;

    :goto_0
    const p1, 0x7f0d001e

    invoke-virtual {p0, p1}, Ld1/j;->setContentView(I)V

    const p1, 0x102000a

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    iput-object p1, p0, Lcom/EliteDevelopment/ext/HideAppsActivity;->n:Landroid/widget/ListView;

    const p1, 0x102000d

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/EliteDevelopment/ext/HideAppsActivity;->o:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    const p1, 0x1020004

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/EliteDevelopment/ext/HideAppsActivity;->p:Landroid/view/View;

    new-instance p1, Lcom/EliteDevelopment/ext/HideAppsActivity$a;

    invoke-direct {p1, p0, p0}, Lcom/EliteDevelopment/ext/HideAppsActivity$a;-><init>(Lcom/EliteDevelopment/ext/HideAppsActivity;Landroid/content/Context;)V

    iput-object p1, p0, Lcom/EliteDevelopment/ext/HideAppsActivity;->t:Lcom/EliteDevelopment/ext/HideAppsActivity$a;

    iget-object v1, p0, Lcom/EliteDevelopment/ext/HideAppsActivity;->n:Landroid/widget/ListView;

    invoke-virtual {v1, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-static {p0}, Lg0/a;->b(Landroidx/lifecycle/j;)Lg0/a;

    move-result-object p1

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, p0}, Lg0/a;->c(ILandroid/os/Bundle;Lg0/a$a;)Lh0/b;

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/EliteDevelopment/ext/HideAppsActivity;->q:Lp0/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lh0/b;->a()Z

    :cond_0
    invoke-super {p0}, Landroidx/fragment/app/i;->onDestroy()V

    return-void
.end method
