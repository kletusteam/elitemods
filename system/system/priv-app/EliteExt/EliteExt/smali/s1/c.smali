.class public Ls1/c;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:Ls1/b;

.field public d:Ls1/b;

.field public e:La1/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La1/b<",
            "Ls1/b;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ls1/b;

.field public g:Z

.field public h:Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;

.field public i:Lz0/f;

.field public j:La1/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La1/b<",
            "Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lz0/c$c;

.field public l:Lz0/f;

.field public m:La1/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La1/b<",
            "Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;",
            ">;"
        }
    .end annotation
.end field

.field public n:Lz0/c$c;

.field public o:Lz0/f;

.field public p:Lz0/f;

.field public q:Lz0/f;

.field public r:Lz0/f;

.field public s:F

.field public t:Lz0/f;

.field public u:Lz0/f;

.field public v:Lz0/f;

.field public w:La1/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La1/b<",
            "Ls1/c;",
            ">;"
        }
    .end annotation
.end field

.field public x:Lz0/f;


# direct methods
.method public constructor <init>(Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;ZIIIIIIII)V
    .locals 13

    move-object v0, p0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Ls1/c;->s:F

    new-instance v2, Ls0/b;

    const/4 v3, 0x1

    invoke-direct {v2, p0, v3}, Ls0/b;-><init>(Ljava/lang/Object;I)V

    iput-object v2, v0, Ls1/c;->k:Lz0/c$c;

    new-instance v2, Ls1/c$a;

    invoke-direct {v2, p0}, Ls1/c$a;-><init>(Ls1/c;)V

    iput-object v2, v0, Ls1/c;->n:Lz0/c$c;

    new-instance v2, Ls1/c$b;

    const-string v3, "Scale"

    invoke-direct {v2, p0, v3}, Ls1/c$b;-><init>(Ls1/c;Ljava/lang/String;)V

    iput-object v2, v0, Ls1/c;->m:La1/b;

    new-instance v2, Ls1/c$c;

    const-string v4, "ContentAlpha"

    invoke-direct {v2, p0, v4}, Ls1/c$c;-><init>(Ls1/c;Ljava/lang/String;)V

    iput-object v2, v0, Ls1/c;->j:La1/b;

    new-instance v2, Ls1/c$d;

    invoke-direct {v2, p0, v3}, Ls1/c$d;-><init>(Ls1/c;Ljava/lang/String;)V

    iput-object v2, v0, Ls1/c;->w:La1/b;

    new-instance v2, Ls1/c$e;

    const-string v3, "Alpha"

    invoke-direct {v2, p0, v3}, Ls1/c$e;-><init>(Ls1/c;Ljava/lang/String;)V

    iput-object v2, v0, Ls1/c;->e:La1/b;

    const/4 v2, 0x0

    iput-boolean v2, v0, Ls1/c;->g:Z

    move/from16 v10, p6

    iput v10, v0, Ls1/c;->b:I

    move/from16 v11, p7

    iput v11, v0, Ls1/c;->a:I

    move v3, p2

    iput-boolean v3, v0, Ls1/c;->g:Z

    new-instance v12, Ls1/b;

    move-object v3, v12

    move/from16 v4, p3

    move/from16 v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    move/from16 v8, p9

    move/from16 v9, p10

    invoke-direct/range {v3 .. v9}, Ls1/b;-><init>(IIIIII)V

    iput-object v12, v0, Ls1/c;->f:Ls1/b;

    iget v3, v0, Ls1/c;->b:I

    invoke-virtual {v12, v3}, Ls1/b;->setAlpha(I)V

    new-instance v12, Ls1/b;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v3, v12

    move/from16 v4, p4

    invoke-direct/range {v3 .. v9}, Ls1/b;-><init>(IIIIII)V

    iput-object v12, v0, Ls1/c;->c:Ls1/b;

    invoke-virtual {v12, v2}, Ls1/b;->setAlpha(I)V

    new-instance v2, Ls1/b;

    move-object v3, v2

    move/from16 v4, p5

    invoke-direct/range {v3 .. v9}, Ls1/b;-><init>(IIIIII)V

    iput-object v2, v0, Ls1/c;->d:Ls1/b;

    const/16 v3, 0xff

    invoke-virtual {v2, v3}, Ls1/b;->setAlpha(I)V

    move-object v2, p1

    iput-object v2, v0, Ls1/c;->h:Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;

    new-instance v2, Lz0/f;

    iget-object v3, v0, Ls1/c;->w:La1/b;

    const v4, 0x3f19999a    # 0.6f

    invoke-direct {v2, p0, v3, v4}, Lz0/f;-><init>(Ljava/lang/Object;La1/b;F)V

    iput-object v2, v0, Ls1/c;->r:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    const v3, 0x4476bd71

    invoke-virtual {v2, v3}, Lz0/g;->b(F)Lz0/g;

    iget-object v2, v0, Ls1/c;->r:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    const v5, 0x3f7d70a4    # 0.99f

    invoke-virtual {v2, v5}, Lz0/g;->a(F)Lz0/g;

    iget-object v2, v0, Ls1/c;->r:Lz0/f;

    iget-object v6, v2, Lz0/f;->o:Lz0/g;

    float-to-double v7, v4

    iput-wide v7, v6, Lz0/g;->c:D

    const v6, 0x3b03126f    # 0.002f

    invoke-virtual {v2, v6}, Lz0/c;->f(F)Lz0/c;

    iget-object v2, v0, Ls1/c;->r:Lz0/f;

    iget-object v7, v0, Ls1/c;->n:Lz0/c$c;

    invoke-virtual {v2, v7}, Lz0/c;->b(Lz0/c$c;)Lz0/c;

    new-instance v2, Lz0/f;

    iget-object v7, v0, Ls1/c;->w:La1/b;

    invoke-direct {v2, p0, v7, v1}, Lz0/f;-><init>(Ljava/lang/Object;La1/b;F)V

    iput-object v2, v0, Ls1/c;->x:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v3}, Lz0/g;->b(F)Lz0/g;

    iget-object v2, v0, Ls1/c;->x:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v4}, Lz0/g;->a(F)Lz0/g;

    iget-object v2, v0, Ls1/c;->x:Lz0/f;

    invoke-virtual {v2, v6}, Lz0/c;->f(F)Lz0/c;

    iget-object v2, v0, Ls1/c;->x:Lz0/f;

    new-instance v7, Ls1/d;

    invoke-direct {v7, p0}, Ls1/d;-><init>(Ls1/c;)V

    invoke-virtual {v2, v7}, Lz0/c;->b(Lz0/c$c;)Lz0/c;

    new-instance v2, Lz0/f;

    iget-object v7, v0, Ls1/c;->h:Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;

    iget-object v8, v0, Ls1/c;->j:La1/b;

    const/high16 v9, 0x3f000000    # 0.5f

    invoke-direct {v2, v7, v8, v9}, Lz0/f;-><init>(Ljava/lang/Object;La1/b;F)V

    iput-object v2, v0, Ls1/c;->l:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v3}, Lz0/g;->b(F)Lz0/g;

    iget-object v2, v0, Ls1/c;->l:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v5}, Lz0/g;->a(F)Lz0/g;

    iget-object v2, v0, Ls1/c;->l:Lz0/f;

    const/high16 v7, 0x3b800000    # 0.00390625f

    invoke-virtual {v2, v7}, Lz0/c;->f(F)Lz0/c;

    iget-object v2, v0, Ls1/c;->l:Lz0/f;

    iget-object v8, v0, Ls1/c;->k:Lz0/c$c;

    invoke-virtual {v2, v8}, Lz0/c;->b(Lz0/c$c;)Lz0/c;

    new-instance v2, Lz0/f;

    iget-object v8, v0, Ls1/c;->c:Ls1/b;

    iget-object v9, v0, Ls1/c;->e:La1/b;

    const v10, 0x3dcccccd    # 0.1f

    invoke-direct {v2, v8, v9, v10}, Lz0/f;-><init>(Ljava/lang/Object;La1/b;F)V

    iput-object v2, v0, Ls1/c;->q:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v3}, Lz0/g;->b(F)Lz0/g;

    iget-object v2, v0, Ls1/c;->q:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v5}, Lz0/g;->a(F)Lz0/g;

    iget-object v2, v0, Ls1/c;->q:Lz0/f;

    invoke-virtual {v2, v7}, Lz0/c;->f(F)Lz0/c;

    iget-object v2, v0, Ls1/c;->q:Lz0/f;

    iget-object v8, v0, Ls1/c;->k:Lz0/c$c;

    invoke-virtual {v2, v8}, Lz0/c;->b(Lz0/c$c;)Lz0/c;

    new-instance v2, Lz0/f;

    iget-object v8, v0, Ls1/c;->c:Ls1/b;

    iget-object v9, v0, Ls1/c;->e:La1/b;

    const/4 v10, 0x0

    invoke-direct {v2, v8, v9, v10}, Lz0/f;-><init>(Ljava/lang/Object;La1/b;F)V

    iput-object v2, v0, Ls1/c;->t:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v3}, Lz0/g;->b(F)Lz0/g;

    iget-object v2, v0, Ls1/c;->t:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v5}, Lz0/g;->a(F)Lz0/g;

    iget-object v2, v0, Ls1/c;->t:Lz0/f;

    invoke-virtual {v2, v7}, Lz0/c;->f(F)Lz0/c;

    iget-object v2, v0, Ls1/c;->t:Lz0/f;

    iget-object v8, v0, Ls1/c;->k:Lz0/c$c;

    invoke-virtual {v2, v8}, Lz0/c;->b(Lz0/c$c;)Lz0/c;

    new-instance v2, Lz0/f;

    iget-object v8, v0, Ls1/c;->d:Ls1/b;

    iget-object v9, v0, Ls1/c;->e:La1/b;

    invoke-direct {v2, v8, v9, v1}, Lz0/f;-><init>(Ljava/lang/Object;La1/b;F)V

    iput-object v2, v0, Ls1/c;->v:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v3}, Lz0/g;->b(F)Lz0/g;

    iget-object v2, v0, Ls1/c;->v:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    const v8, 0x3f333333    # 0.7f

    invoke-virtual {v2, v8}, Lz0/g;->a(F)Lz0/g;

    iget-object v2, v0, Ls1/c;->v:Lz0/f;

    invoke-virtual {v2, v7}, Lz0/c;->f(F)Lz0/c;

    iget-object v2, v0, Ls1/c;->v:Lz0/f;

    iget-object v8, v0, Ls1/c;->k:Lz0/c$c;

    invoke-virtual {v2, v8}, Lz0/c;->b(Lz0/c$c;)Lz0/c;

    new-instance v2, Lz0/f;

    iget-object v8, v0, Ls1/c;->h:Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;

    iget-object v9, v0, Ls1/c;->j:La1/b;

    invoke-direct {v2, v8, v9, v1}, Lz0/f;-><init>(Ljava/lang/Object;La1/b;F)V

    iput-object v2, v0, Ls1/c;->p:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    const v8, 0x43db51ec

    invoke-virtual {v2, v8}, Lz0/g;->b(F)Lz0/g;

    iget-object v2, v0, Ls1/c;->p:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v4}, Lz0/g;->a(F)Lz0/g;

    iget-object v2, v0, Ls1/c;->p:Lz0/f;

    invoke-virtual {v2, v7}, Lz0/c;->f(F)Lz0/c;

    iget-object v2, v0, Ls1/c;->p:Lz0/f;

    iget-object v9, v0, Ls1/c;->k:Lz0/c$c;

    invoke-virtual {v2, v9}, Lz0/c;->b(Lz0/c$c;)Lz0/c;

    new-instance v2, Lz0/f;

    iget-object v9, v0, Ls1/c;->d:Ls1/b;

    iget-object v11, v0, Ls1/c;->e:La1/b;

    invoke-direct {v2, v9, v11, v10}, Lz0/f;-><init>(Ljava/lang/Object;La1/b;F)V

    iput-object v2, v0, Ls1/c;->u:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v3}, Lz0/g;->b(F)Lz0/g;

    iget-object v2, v0, Ls1/c;->u:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v5}, Lz0/g;->a(F)Lz0/g;

    iget-object v2, v0, Ls1/c;->u:Lz0/f;

    invoke-virtual {v2, v7}, Lz0/c;->f(F)Lz0/c;

    iget-object v2, v0, Ls1/c;->u:Lz0/f;

    iget-object v7, v0, Ls1/c;->k:Lz0/c$c;

    invoke-virtual {v2, v7}, Lz0/c;->b(Lz0/c$c;)Lz0/c;

    new-instance v2, Lz0/f;

    iget-object v7, v0, Ls1/c;->h:Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;

    iget-object v9, v0, Ls1/c;->m:La1/b;

    invoke-direct {v2, v7, v9, v1}, Lz0/f;-><init>(Ljava/lang/Object;La1/b;F)V

    iput-object v2, v0, Ls1/c;->i:Lz0/f;

    iget-object v1, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v1, v8}, Lz0/g;->b(F)Lz0/g;

    iget-object v1, v0, Ls1/c;->i:Lz0/f;

    iget-object v1, v1, Lz0/f;->o:Lz0/g;

    invoke-virtual {v1, v4}, Lz0/g;->a(F)Lz0/g;

    iget-object v1, v0, Ls1/c;->i:Lz0/f;

    invoke-virtual {v1, v6}, Lz0/c;->f(F)Lz0/c;

    iget-object v1, v0, Ls1/c;->i:Lz0/f;

    iget-object v2, v0, Ls1/c;->k:Lz0/c$c;

    invoke-virtual {v1, v2}, Lz0/c;->b(Lz0/c$c;)Lz0/c;

    iget-boolean v1, v0, Ls1/c;->g:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Ls1/c;->i:Lz0/f;

    const/high16 v2, 0x40a00000    # 5.0f

    goto :goto_0

    :cond_0
    iget-object v1, v0, Ls1/c;->i:Lz0/f;

    const/high16 v2, 0x41200000    # 10.0f

    :goto_0
    iput v2, v1, Lz0/c;->l:F

    new-instance v1, Lz0/f;

    iget-object v2, v0, Ls1/c;->h:Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;

    iget-object v4, v0, Ls1/c;->m:La1/b;

    const v7, 0x3e99999a    # 0.3f

    invoke-direct {v1, v2, v4, v7}, Lz0/f;-><init>(Ljava/lang/Object;La1/b;F)V

    iput-object v1, v0, Ls1/c;->o:Lz0/f;

    iget-object v1, v1, Lz0/f;->o:Lz0/g;

    invoke-virtual {v1, v3}, Lz0/g;->b(F)Lz0/g;

    iget-object v1, v0, Ls1/c;->o:Lz0/f;

    iget-object v1, v1, Lz0/f;->o:Lz0/g;

    invoke-virtual {v1, v5}, Lz0/g;->a(F)Lz0/g;

    iget-object v1, v0, Ls1/c;->o:Lz0/f;

    invoke-virtual {v1, v6}, Lz0/c;->f(F)Lz0/c;

    iget-object v1, v0, Ls1/c;->o:Lz0/f;

    iget-object v2, v0, Ls1/c;->n:Lz0/c$c;

    invoke-virtual {v1, v2}, Lz0/c;->b(Lz0/c$c;)Lz0/c;

    return-void
.end method
