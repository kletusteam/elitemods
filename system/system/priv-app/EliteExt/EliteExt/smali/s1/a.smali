.class public Ls1/a;
.super Landroid/graphics/drawable/AnimatedStateListDrawable;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ls1/a$a;
    }
.end annotation


# instance fields
.field public a:Ls1/a$a;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/graphics/drawable/AnimatedStateListDrawable;-><init>()V

    invoke-virtual {p0}, Ls1/a;->a()Ls1/a$a;

    move-result-object v0

    iput-object v0, p0, Ls1/a;->a:Ls1/a$a;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;Ls1/a$a;)V
    .locals 1

    invoke-direct {p0}, Landroid/graphics/drawable/AnimatedStateListDrawable;-><init>()V

    if-eqz p3, :cond_3

    if-nez p1, :cond_0

    iget-object p1, p3, Ls1/a$a;->f:Landroid/graphics/drawable/Drawable$ConstantState;

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    goto :goto_0

    :cond_0
    if-nez p2, :cond_1

    iget-object p2, p3, Ls1/a$a;->f:Landroid/graphics/drawable/Drawable$ConstantState;

    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    goto :goto_0

    :cond_1
    iget-object v0, p3, Ls1/a$a;->f:Landroid/graphics/drawable/Drawable$ConstantState;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    :goto_0
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object p1

    iput-object p1, p3, Ls1/a$a;->f:Landroid/graphics/drawable/Drawable$ConstantState;

    :cond_2
    iget-object p1, p3, Ls1/a$a;->f:Landroid/graphics/drawable/Drawable$ConstantState;

    check-cast p1, Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;

    invoke-virtual {p0, p1}, Ls1/a;->setConstantState(Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;)V

    invoke-virtual {p0}, Landroid/graphics/drawable/AnimatedStateListDrawable;->getState()[I

    move-result-object p1

    invoke-virtual {p0, p1}, Landroid/graphics/drawable/AnimatedStateListDrawable;->onStateChange([I)Z

    invoke-virtual {p0}, Landroid/graphics/drawable/AnimatedStateListDrawable;->jumpToCurrentState()V

    iget-object p1, p0, Ls1/a;->a:Ls1/a$a;

    iget p2, p3, Ls1/a$a;->e:I

    iput p2, p1, Ls1/a$a;->e:I

    iget p2, p3, Ls1/a$a;->d:I

    iput p2, p1, Ls1/a$a;->d:I

    iget p2, p3, Ls1/a$a;->a:I

    iput p2, p1, Ls1/a$a;->a:I

    iget-boolean p2, p3, Ls1/a$a;->j:Z

    iput-boolean p2, p1, Ls1/a$a;->j:Z

    goto :goto_1

    :cond_3
    const/4 p1, 0x0

    const-string p2, "miuix.internal.view.CheckWidgetAnimatedStateListDrawable"

    const-string p3, "checkWidgetConstantState is null ,but it can\'t be null"

    invoke-static {p2, p3, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_1
    return-void
.end method


# virtual methods
.method public a()Ls1/a$a;
    .locals 1

    new-instance v0, Ls1/a$a;

    invoke-direct {v0}, Ls1/a$a;-><init>()V

    return-object v0
.end method

.method public canApplyTheme()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
    .locals 1

    iget-object v0, p0, Ls1/a;->a:Ls1/a$a;

    return-object v0
.end method

.method public setConstantState(Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/graphics/drawable/AnimatedStateListDrawable;->setConstantState(Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;)V

    iget-object v0, p0, Ls1/a;->a:Ls1/a$a;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ls1/a;->a()Ls1/a$a;

    move-result-object v0

    iput-object v0, p0, Ls1/a;->a:Ls1/a$a;

    :cond_0
    iget-object v0, p0, Ls1/a;->a:Ls1/a$a;

    iput-object p1, v0, Ls1/a$a;->f:Landroid/graphics/drawable/Drawable$ConstantState;

    return-void
.end method
