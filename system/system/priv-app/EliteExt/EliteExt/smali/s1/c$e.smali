.class public Ls1/c$e;
.super La1/b;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ls1/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "La1/b<",
        "Ls1/b;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ls1/c;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p2}, La1/b;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public c(Ljava/lang/Object;)F
    .locals 0

    check-cast p1, Ls1/b;

    invoke-virtual {p1}, Ls1/b;->getAlpha()I

    move-result p1

    div-int/lit16 p1, p1, 0xff

    int-to-float p1, p1

    return p1
.end method

.method public d(Ljava/lang/Object;F)V
    .locals 2

    check-cast p1, Ls1/b;

    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v1, p2, v0

    if-lez v1, :cond_0

    move p2, v0

    :cond_0
    const/4 v0, 0x0

    cmpg-float v1, p2, v0

    if-gez v1, :cond_1

    move p2, v0

    :cond_1
    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr p2, v0

    float-to-int p2, p2

    invoke-virtual {p1, p2}, Ls1/b;->setAlpha(I)V

    return-void
.end method
