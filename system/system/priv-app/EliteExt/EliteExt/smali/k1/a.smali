.class public final Lk1/a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Z

.field public final synthetic c:Ld1/i$c;


# direct methods
.method public constructor <init>(ZLandroid/view/View;Ld1/i$c;)V
    .locals 0

    iput-boolean p1, p0, Lk1/a;->b:Z

    iput-object p2, p0, Lk1/a;->a:Landroid/view/View;

    iput-object p3, p0, Lk1/a;->c:Ld1/i$c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 0

    iget-boolean p2, p0, Lk1/a;->b:Z

    if-nez p2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object p2

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result p2

    if-ne p5, p2, :cond_1

    :cond_0
    invoke-virtual {p1, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    iget-object p2, p0, Lk1/a;->a:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result p3

    int-to-float p3, p3

    invoke-virtual {p2, p3}, Landroid/view/View;->setTranslationY(F)V

    iget-object p2, p0, Lk1/a;->a:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result p3

    iget-boolean p4, p0, Lk1/a;->b:Z

    iget-object p5, p0, Lk1/a;->c:Ld1/i$c;

    const/4 p6, 0x0

    invoke-static {p2, p3, p6, p4, p5}, Lk1/d;->a(Landroid/view/View;IIZLd1/i$c;)V

    invoke-virtual {p1, p6}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void
.end method
