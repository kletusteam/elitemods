.class public final Lk1/b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Ld1/i$c;


# direct methods
.method public constructor <init>(ZLd1/i$c;)V
    .locals 0

    iput-boolean p1, p0, Lk1/b;->a:Z

    iput-object p2, p0, Lk1/b;->b:Ld1/i$c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 0

    sub-int/2addr p5, p3

    const/4 p2, 0x0

    sub-int/2addr p5, p2

    int-to-float p3, p5

    invoke-virtual {p1, p3}, Landroid/view/View;->setTranslationY(F)V

    invoke-virtual {p1, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    iget-boolean p3, p0, Lk1/b;->a:Z

    iget-object p4, p0, Lk1/b;->b:Ld1/i$c;

    invoke-static {p1, p5, p2, p3, p4}, Lk1/d;->a(Landroid/view/View;IIZLd1/i$c;)V

    return-void
.end method
