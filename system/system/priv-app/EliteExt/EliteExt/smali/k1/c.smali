.class public final Lk1/c;
.super Landroid/animation/AnimatorListenerAdapter;
.source ""


# instance fields
.field public final synthetic a:Ld1/i$c;

.field public final synthetic b:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Ld1/i$c;)V
    .locals 0

    iput-object p1, p0, Lk1/c;->b:Landroid/view/View;

    iput-object p2, p0, Lk1/c;->a:Ld1/i$c;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationCancel(Landroid/animation/Animator;)V

    iget-object p1, p0, Lk1/c;->a:Ld1/i$c;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ld1/i$c;->b()V

    :cond_0
    sget-object p1, Lk1/d;->a:Ljava/lang/ref/WeakReference;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->clear()V

    const/4 p1, 0x0

    sput-object p1, Lk1/d;->a:Ljava/lang/ref/WeakReference;

    :cond_1
    iget-object p1, p0, Lk1/c;->b:Landroid/view/View;

    const/4 v0, 0x0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationEnd(Landroid/animation/Animator;)V

    iget-object p1, p0, Lk1/c;->a:Ld1/i$c;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ld1/i$c;->b()V

    :cond_0
    sget-object p1, Lk1/d;->a:Ljava/lang/ref/WeakReference;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->clear()V

    const/4 p1, 0x0

    sput-object p1, Lk1/d;->a:Ljava/lang/ref/WeakReference;

    :cond_1
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;Z)V
    .locals 0

    iget-object p1, p0, Lk1/c;->b:Landroid/view/View;

    const-string p2, "show"

    invoke-virtual {p1, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object p1, p0, Lk1/c;->a:Ld1/i$c;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ld1/i$c;->a()V

    :cond_0
    return-void
.end method
