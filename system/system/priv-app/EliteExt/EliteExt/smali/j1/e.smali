.class public Lj1/e;
.super Lt1/a;
.source ""


# instance fields
.field public A:Landroid/view/MenuItem;

.field public B:Landroid/view/View;

.field public C:F

.field public D:F

.field public v:Lj1/a;

.field public w:Landroid/view/View;

.field public x:I

.field public y:Lh1/g;

.field public z:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lh1/g;Landroid/widget/PopupWindow$OnDismissListener;)V
    .locals 1

    invoke-direct {p0, p1}, Lt1/a;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lj1/e;->y:Lh1/g;

    new-instance p2, Lj1/a;

    iget-object v0, p0, Lj1/e;->y:Lh1/g;

    invoke-direct {p2, p1, v0}, Lj1/a;-><init>(Landroid/content/Context;Landroid/view/Menu;)V

    iput-object p2, p0, Lj1/e;->v:Lj1/a;

    iget-object p2, p2, Lj1/a;->c:Landroid/view/MenuItem;

    iput-object p2, p0, Lj1/e;->A:Landroid/view/MenuItem;

    if-nez p2, :cond_0

    iget-object p2, p0, Lj1/e;->B:Landroid/view/View;

    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lj1/e;->B:Landroid/view/View;

    const v0, 0x1020014

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iget-object v0, p0, Lj1/e;->A:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0400d7

    invoke-static {p1, v0}, Lr1/a;->d(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    iget-object p2, p0, Lj1/e;->B:Landroid/view/View;

    new-instance v0, Lj1/f;

    invoke-direct {v0, p0}, Lj1/f;-><init>(Lj1/e;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p2, p0, Lj1/e;->B:Landroid/view/View;

    invoke-static {p2}, Landroidx/emoji2/text/l;->d(Landroid/view/View;)V

    :goto_0
    iget-object p2, p0, Lj1/e;->v:Lj1/a;

    invoke-virtual {p0, p2}, Lt1/a;->d(Landroid/widget/ListAdapter;)V

    new-instance p2, Lj1/e$a;

    invoke-direct {p2, p0}, Lj1/e$a;-><init>(Lj1/e;)V

    iput-object p2, p0, Lt1/a;->t:Landroid/widget/AdapterView$OnItemClickListener;

    iput-object p3, p0, Lt1/a;->s:Landroid/widget/PopupWindow$OnDismissListener;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0700db

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lj1/e;->x:I

    return-void
.end method


# virtual methods
.method public b(Landroid/content/Context;)V
    .locals 7

    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lj1/e;->z:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d0051

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lj1/e;->B:Landroid/view/View;

    const v0, 0x7f040145

    invoke-static {p1, v0}, Lr1/a;->d(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lt1/a;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    iget-object v1, p0, Lt1/a;->u:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lj1/e;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v4, -0x1

    invoke-direct {v0, v4, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v1, 0x7f0700d8

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    invoke-virtual {v0, v3, p1, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    iget-object p1, p0, Lj1/e;->z:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lt1/a;->u:Landroid/widget/FrameLayout;

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-direct {v5, v4, v3, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {p1, v1, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p1, p0, Lj1/e;->z:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lj1/e;->B:Landroid/view/View;

    invoke-virtual {p1, v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, v2}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lj1/e;->z:Landroid/widget/LinearLayout;

    invoke-virtual {p0, p1}, Lt1/a;->e(Landroid/view/View;)V

    return-void
.end method

.method public final h()I
    .locals 7

    iget-object v0, p0, Lt1/a;->d:Landroid/view/View;

    instance-of v1, v0, Landroid/widget/ListView;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    move v1, v2

    move v3, v1

    :goto_0
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v4

    if-ge v3, v4, :cond_1

    const/4 v4, 0x0

    iget-object v5, p0, Lt1/a;->d:Landroid/view/View;

    check-cast v5, Landroid/widget/ListView;

    invoke-interface {v0, v3, v4, v5}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/view/View;->measure(II)V

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v1, v4

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v0, v1, v3}, Landroid/view/View;->measure(II)V

    iget-object v0, p0, Lt1/a;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/lit8 v1, v0, 0x0

    :cond_1
    return v1
.end method

.method public final i(Landroid/view/View;FF)V
    .locals 8

    const/4 v0, 0x2

    new-array v1, v0, [I

    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationInWindow([I)V

    const/4 v2, 0x0

    aget v3, v1, v2

    float-to-int p2, p2

    const/4 v4, 0x1

    aget v5, v1, v4

    float-to-int p3, p3

    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/view/View;->getLocationInWindow([I)V

    add-int/2addr v3, p2

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result p2

    div-int/2addr p2, v0

    if-gt v3, p2, :cond_0

    goto :goto_0

    :cond_0
    move v4, v2

    :goto_0
    invoke-virtual {p0}, Lj1/e;->h()I

    move-result p2

    add-int/2addr v5, p3

    invoke-virtual {p0}, Lj1/e;->h()I

    move-result p3

    div-int/2addr p3, v0

    sub-int/2addr v5, p3

    int-to-float p3, v5

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3dcccccd    # 0.1f

    mul-float/2addr v0, v1

    cmpg-float v0, p3, v0

    if-gez v0, :cond_1

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result p3

    int-to-float p3, p3

    mul-float/2addr p3, v1

    :cond_1
    iget-object v0, p0, Lj1/e;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lj1/e;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_2

    instance-of v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lj1/e;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    :goto_1
    iget-object v3, p0, Lj1/e;->B:Landroid/view/View;

    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v3, v5, v7}, Landroid/view/View;->measure(II)V

    iget-object v3, p0, Lj1/e;->B:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    goto :goto_2

    :cond_3
    move v3, v2

    :goto_2
    add-int/2addr p2, v3

    int-to-float p2, p2

    add-float v0, p3, p2

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    const v5, 0x3f666666    # 0.9f

    mul-float/2addr v3, v5

    cmpl-float v0, v0, v3

    if-lez v0, :cond_4

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result p3

    int-to-float p3, p3

    mul-float/2addr p3, v5

    sub-float/2addr p3, p2

    :cond_4
    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result p2

    int-to-float p2, p2

    mul-float/2addr p2, v1

    cmpg-float p2, p3, p2

    if-gez p2, :cond_5

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result p2

    int-to-float p2, p2

    mul-float p3, p2, v1

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result p2

    int-to-float p2, p2

    const v0, 0x3f4ccccc    # 0.79999995f

    mul-float/2addr p2, v0

    float-to-int p2, p2

    invoke-virtual {p0, p2}, Landroid/widget/PopupWindow;->setHeight(I)V

    :cond_5
    if-eqz v4, :cond_6

    iget p2, p0, Lj1/e;->x:I

    goto :goto_3

    :cond_6
    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result p2

    iget v0, p0, Lj1/e;->x:I

    sub-int/2addr p2, v0

    invoke-virtual {p0}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v0

    sub-int/2addr p2, v0

    :goto_3
    float-to-int p3, p3

    invoke-virtual {p0, p1, v2, p2, p3}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    return-void
.end method
