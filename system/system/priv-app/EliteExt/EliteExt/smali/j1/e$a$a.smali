.class public Lj1/e$a$a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/PopupWindow$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lj1/e$a;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lj1/e$a;

.field public final synthetic b:Landroid/view/SubMenu;


# direct methods
.method public constructor <init>(Lj1/e$a;Landroid/view/SubMenu;)V
    .locals 0

    iput-object p1, p0, Lj1/e$a$a;->a:Lj1/e$a;

    iput-object p2, p0, Lj1/e$a$a;->b:Landroid/view/SubMenu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss()V
    .locals 6

    iget-object v0, p0, Lj1/e$a$a;->a:Lj1/e$a;

    iget-object v0, v0, Lj1/e$a;->a:Lj1/e;

    const/4 v1, 0x0

    iput-object v1, v0, Lt1/a;->s:Landroid/widget/PopupWindow$OnDismissListener;

    iget-object v1, p0, Lj1/e$a$a;->b:Landroid/view/SubMenu;

    iget-object v0, v0, Lj1/e;->v:Lj1/a;

    iget-object v2, v0, Lt1/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Lt1/c;->a(Landroid/view/Menu;Ljava/util/ArrayList;)V

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    iget-object v0, p0, Lj1/e$a$a;->a:Lj1/e$a;

    iget-object v0, v0, Lj1/e$a;->a:Lj1/e;

    iget-object v1, v0, Lj1/e;->w:Landroid/view/View;

    iget v2, v0, Lj1/e;->C:F

    iget v3, v0, Lj1/e;->D:F

    invoke-virtual {v0}, Lt1/a;->a()I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setWidth(I)V

    const/4 v4, -0x2

    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setHeight(I)V

    iget-object v4, v0, Lj1/e;->B:Landroid/view/View;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v1, v2, v3}, Lj1/e;->i(Landroid/view/View;FF)V

    iget-object v0, v0, Lt1/a;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->forceLayout()V

    return-void
.end method
