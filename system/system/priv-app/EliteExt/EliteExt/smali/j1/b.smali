.class public Lj1/b;
.super Lh1/g;
.source ""

# interfaces
.implements Landroid/view/ContextMenu;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lh1/g;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public setHeaderIcon(I)Landroid/view/ContextMenu;
    .locals 0

    invoke-virtual {p0, p1}, Lh1/g;->t(I)Lh1/g;

    move-object p1, p0

    check-cast p1, Landroid/view/ContextMenu;

    return-object p1
.end method

.method public setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/ContextMenu;
    .locals 0

    invoke-virtual {p0, p1}, Lh1/g;->u(Landroid/graphics/drawable/Drawable;)Lh1/g;

    move-object p1, p0

    check-cast p1, Landroid/view/ContextMenu;

    return-object p1
.end method

.method public setHeaderTitle(I)Landroid/view/ContextMenu;
    .locals 0

    invoke-virtual {p0, p1}, Lh1/g;->w(I)Lh1/g;

    move-object p1, p0

    check-cast p1, Landroid/view/ContextMenu;

    return-object p1
.end method

.method public setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;
    .locals 0

    invoke-virtual {p0, p1}, Lh1/g;->x(Ljava/lang/CharSequence;)Lh1/g;

    move-object p1, p0

    check-cast p1, Landroid/view/ContextMenu;

    return-object p1
.end method

.method public setHeaderView(Landroid/view/View;)Landroid/view/ContextMenu;
    .locals 0

    invoke-virtual {p0, p1}, Lh1/g;->y(Landroid/view/View;)Lh1/g;

    move-object p1, p0

    check-cast p1, Landroid/view/ContextMenu;

    return-object p1
.end method
