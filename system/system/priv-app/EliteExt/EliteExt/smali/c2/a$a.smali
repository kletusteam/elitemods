.class public Lc2/a$a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lc2/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public a:[Landroid/graphics/PointF;

.field public b:[Landroid/graphics/PointF;

.field public c:F

.field public d:Landroid/graphics/RectF;

.field public e:D

.field public f:D

.field public g:F

.field public h:D

.field public i:D


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x4

    new-array v1, v0, [Landroid/graphics/PointF;

    iput-object v1, p0, Lc2/a$a;->a:[Landroid/graphics/PointF;

    new-array v0, v0, [Landroid/graphics/PointF;

    iput-object v0, p0, Lc2/a$a;->b:[Landroid/graphics/PointF;

    return-void
.end method


# virtual methods
.method public a(FLandroid/graphics/RectF;FFDFI)V
    .locals 40

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    move/from16 v10, p7

    move/from16 v4, p1

    move/from16 v11, p8

    iput v4, v0, Lc2/a$a;->c:F

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v12

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v13

    iget v14, v1, Landroid/graphics/RectF;->left:F

    iget v15, v1, Landroid/graphics/RectF;->top:F

    iget v9, v1, Landroid/graphics/RectF;->right:F

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iget v7, v0, Lc2/a$a;->c:F

    move v4, v12

    move v5, v7

    move v6, v7

    move/from16 v16, v7

    move-wide/from16 v7, p5

    move/from16 p1, v1

    move v1, v9

    move/from16 v9, p7

    invoke-static/range {v4 .. v9}, Lc2/a;->c(FFFDF)Z

    move-result v4

    const/4 v9, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/high16 v17, 0x40000000    # 2.0f

    if-eqz v4, :cond_0

    mul-float v4, v16, v17

    div-float/2addr v12, v4

    sub-float/2addr v12, v7

    div-float/2addr v12, v10

    invoke-static {v12, v7}, Ljava/lang/Math;->min(FF)F

    move-result v4

    invoke-static {v4, v9}, Ljava/lang/Math;->max(FF)F

    move-result v4

    float-to-double v4, v4

    goto :goto_0

    :cond_0
    move-wide/from16 v4, p5

    :goto_0
    iput-wide v4, v0, Lc2/a$a;->e:D

    iget v12, v0, Lc2/a$a;->c:F

    move v4, v13

    move v5, v12

    move v6, v12

    move/from16 p2, v1

    move v1, v7

    move-wide/from16 v7, p5

    move/from16 v9, p7

    invoke-static/range {v4 .. v9}, Lc2/a;->b(FFFDF)Z

    move-result v4

    if-eqz v4, :cond_1

    mul-float v12, v12, v17

    div-float/2addr v13, v12

    sub-float/2addr v13, v1

    div-float/2addr v13, v10

    invoke-static {v13, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    const/4 v4, 0x0

    invoke-static {v1, v4}, Ljava/lang/Math;->max(FF)F

    move-result v1

    float-to-double v4, v1

    goto :goto_1

    :cond_1
    move-wide/from16 v4, p5

    :goto_1
    iput-wide v4, v0, Lc2/a$a;->f:D

    iget-wide v6, v0, Lc2/a$a;->e:D

    const-wide v8, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v6, v8

    const-wide/high16 v12, 0x4010000000000000L    # 4.0

    div-double/2addr v6, v12

    iput-wide v6, v0, Lc2/a$a;->h:D

    mul-double/2addr v4, v8

    div-double/2addr v4, v12

    iput-wide v4, v0, Lc2/a$a;->i:D

    const-wide v8, 0x3ff921fb54442d18L    # 1.5707963267948966

    sub-double/2addr v8, v4

    sub-double/2addr v8, v6

    invoke-static {v8, v9}, Lc2/a;->d(D)D

    move-result-wide v4

    double-to-float v1, v4

    iput v1, v0, Lc2/a$a;->g:F

    iget-wide v4, v0, Lc2/a$a;->e:D

    float-to-double v6, v10

    mul-double/2addr v4, v6

    iget-wide v8, v0, Lc2/a$a;->h:D

    const-wide/16 v12, 0x0

    cmpl-double v1, v8, v12

    const-wide/high16 v18, 0x4008000000000000L    # 3.0

    const-wide v20, 0x3fdd70a3e0000000L    # 0.46000000834465027

    const-wide/high16 v22, 0x4000000000000000L    # 2.0

    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    if-nez v1, :cond_2

    move-wide v4, v12

    goto :goto_2

    :cond_2
    div-double v26, v8, v22

    mul-double v4, v4, v20

    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->tan(D)D

    move-result-wide v28

    add-double v28, v28, v4

    mul-double v28, v28, v22

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    add-double v4, v4, v24

    mul-double v4, v4, v28

    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->tan(D)D

    move-result-wide v8

    mul-double v8, v8, v18

    div-double/2addr v4, v8

    sub-double v4, v4, v24

    :goto_2
    iget v1, v0, Lc2/a$a;->c:F

    iget-wide v8, v0, Lc2/a$a;->h:D

    float-to-double v12, v1

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    sub-double v8, v24, v8

    mul-double/2addr v8, v12

    iget v1, v0, Lc2/a$a;->c:F

    iget-wide v12, v0, Lc2/a$a;->h:D

    move-wide/from16 v26, v8

    float-to-double v8, v1

    invoke-static {v12, v13}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    sub-double v12, v24, v12

    mul-double/2addr v12, v8

    iget v1, v0, Lc2/a$a;->c:F

    iget-wide v8, v0, Lc2/a$a;->h:D

    move-wide/from16 v28, v12

    float-to-double v12, v1

    div-double v8, v8, v22

    invoke-static {v8, v9}, Ljava/lang/Math;->tan(D)D

    move-result-wide v8

    sub-double v8, v24, v8

    mul-double/2addr v8, v12

    iget v1, v0, Lc2/a$a;->c:F

    iget-wide v12, v0, Lc2/a$a;->h:D

    move-wide/from16 v30, v8

    float-to-double v8, v1

    const-wide/high16 v32, 0x3ff8000000000000L    # 1.5

    mul-double v8, v8, v32

    div-double v34, v12, v22

    invoke-static/range {v34 .. v35}, Ljava/lang/Math;->tan(D)D

    move-result-wide v34

    mul-double v34, v34, v8

    invoke-static {v12, v13}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    add-double v8, v8, v24

    div-double v34, v34, v8

    mul-double v4, v4, v34

    iget-wide v8, v0, Lc2/a$a;->f:D

    mul-double/2addr v8, v6

    iget-wide v6, v0, Lc2/a$a;->i:D

    const-wide/16 v12, 0x0

    cmpl-double v1, v6, v12

    if-nez v1, :cond_3

    goto :goto_3

    :cond_3
    div-double v12, v6, v22

    mul-double v8, v8, v20

    invoke-static {v12, v13}, Ljava/lang/Math;->tan(D)D

    move-result-wide v20

    add-double v20, v20, v8

    mul-double v20, v20, v22

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    add-double v6, v6, v24

    mul-double v6, v6, v20

    invoke-static {v12, v13}, Ljava/lang/Math;->tan(D)D

    move-result-wide v8

    mul-double v8, v8, v18

    div-double/2addr v6, v8

    sub-double v12, v6, v24

    :goto_3
    iget v1, v0, Lc2/a$a;->c:F

    iget-wide v6, v0, Lc2/a$a;->i:D

    float-to-double v8, v1

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    sub-double v6, v24, v6

    mul-double/2addr v6, v8

    iget v1, v0, Lc2/a$a;->c:F

    iget-wide v8, v0, Lc2/a$a;->i:D

    move-wide/from16 p5, v6

    float-to-double v6, v1

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    sub-double v8, v24, v8

    mul-double/2addr v8, v6

    iget v1, v0, Lc2/a$a;->c:F

    iget-wide v6, v0, Lc2/a$a;->i:D

    move-wide/from16 v18, v8

    float-to-double v8, v1

    div-double v6, v6, v22

    invoke-static {v6, v7}, Ljava/lang/Math;->tan(D)D

    move-result-wide v6

    sub-double v6, v24, v6

    mul-double/2addr v6, v8

    iget v1, v0, Lc2/a$a;->c:F

    iget-wide v8, v0, Lc2/a$a;->i:D

    move-wide/from16 v20, v6

    float-to-double v6, v1

    mul-double v6, v6, v32

    div-double v22, v8, v22

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->tan(D)D

    move-result-wide v22

    mul-double v22, v22, v6

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    add-double v6, v6, v24

    div-double v22, v22, v6

    mul-double v12, v12, v22

    const/4 v1, 0x0

    if-nez v11, :cond_4

    add-float/2addr v14, v2

    add-float/2addr v15, v3

    iget v2, v0, Lc2/a$a;->c:F

    new-instance v3, Landroid/graphics/RectF;

    mul-float v2, v2, v17

    add-float v9, v2, v14

    add-float/2addr v2, v15

    invoke-direct {v3, v14, v15, v9, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v3, v0, Lc2/a$a;->d:Landroid/graphics/RectF;

    iget-object v2, v0, Lc2/a$a;->a:[Landroid/graphics/PointF;

    float-to-double v9, v14

    add-double v6, v26, v9

    double-to-float v3, v6

    float-to-double v6, v15

    new-instance v11, Landroid/graphics/PointF;

    move-wide/from16 v32, v9

    add-double v8, v28, v6

    double-to-float v8, v8

    invoke-direct {v11, v3, v8}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v11, v2, v1

    iget-object v2, v0, Lc2/a$a;->a:[Landroid/graphics/PointF;

    new-instance v3, Landroid/graphics/PointF;

    add-double v8, v30, v32

    double-to-float v8, v8

    invoke-direct {v3, v8, v15}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v8, 0x1

    aput-object v3, v2, v8

    iget-object v2, v0, Lc2/a$a;->a:[Landroid/graphics/PointF;

    add-double v8, v30, v34

    new-instance v3, Landroid/graphics/PointF;

    add-double v10, v8, v32

    double-to-float v10, v10

    invoke-direct {v3, v10, v15}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v10, 0x2

    aput-object v3, v2, v10

    iget-object v2, v0, Lc2/a$a;->a:[Landroid/graphics/PointF;

    new-instance v3, Landroid/graphics/PointF;

    add-double/2addr v8, v4

    add-double v8, v8, v32

    double-to-float v4, v8

    invoke-direct {v3, v4, v15}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v4, 0x3

    aput-object v3, v2, v4

    iget-object v2, v0, Lc2/a$a;->b:[Landroid/graphics/PointF;

    add-double v3, v20, v22

    new-instance v5, Landroid/graphics/PointF;

    add-double/2addr v12, v3

    add-double/2addr v12, v6

    double-to-float v8, v12

    invoke-direct {v5, v14, v8}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v5, v2, v1

    iget-object v1, v0, Lc2/a$a;->b:[Landroid/graphics/PointF;

    new-instance v2, Landroid/graphics/PointF;

    add-double/2addr v3, v6

    double-to-float v3, v3

    invoke-direct {v2, v14, v3}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v3, 0x1

    aput-object v2, v1, v3

    iget-object v1, v0, Lc2/a$a;->b:[Landroid/graphics/PointF;

    new-instance v2, Landroid/graphics/PointF;

    add-double v3, v20, v6

    double-to-float v3, v3

    invoke-direct {v2, v14, v3}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v3, 0x2

    aput-object v2, v1, v3

    iget-object v1, v0, Lc2/a$a;->b:[Landroid/graphics/PointF;

    new-instance v2, Landroid/graphics/PointF;

    move-wide/from16 v8, p5

    add-double v3, v8, v32

    double-to-float v3, v3

    add-double v8, v18, v6

    double-to-float v4, v8

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v3, 0x3

    aput-object v2, v1, v3

    goto/16 :goto_4

    :cond_4
    move-wide/from16 v8, p5

    const/4 v6, 0x1

    if-ne v11, v6, :cond_5

    add-float/2addr v15, v3

    iget v3, v0, Lc2/a$a;->c:F

    sub-float v6, p2, v2

    new-instance v7, Landroid/graphics/RectF;

    mul-float v3, v3, v17

    sub-float v10, p2, v3

    sub-float/2addr v10, v2

    add-float/2addr v3, v15

    invoke-direct {v7, v10, v15, v6, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v7, v0, Lc2/a$a;->d:Landroid/graphics/RectF;

    iget-object v3, v0, Lc2/a$a;->a:[Landroid/graphics/PointF;

    move/from16 v7, p2

    float-to-double v10, v7

    sub-double v30, v10, v30

    sub-double v32, v30, v34

    float-to-double v1, v2

    new-instance v7, Landroid/graphics/PointF;

    sub-double v4, v32, v4

    sub-double/2addr v4, v1

    double-to-float v4, v4

    invoke-direct {v7, v4, v15}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v4, 0x0

    aput-object v7, v3, v4

    iget-object v3, v0, Lc2/a$a;->a:[Landroid/graphics/PointF;

    new-instance v4, Landroid/graphics/PointF;

    move-wide/from16 p5, v12

    sub-double v12, v32, v1

    double-to-float v5, v12

    invoke-direct {v4, v5, v15}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v5, 0x1

    aput-object v4, v3, v5

    iget-object v3, v0, Lc2/a$a;->a:[Landroid/graphics/PointF;

    new-instance v4, Landroid/graphics/PointF;

    sub-double v12, v30, v1

    double-to-float v5, v12

    invoke-direct {v4, v5, v15}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v5, 0x2

    aput-object v4, v3, v5

    iget-object v3, v0, Lc2/a$a;->a:[Landroid/graphics/PointF;

    sub-double v4, v10, v26

    sub-double/2addr v4, v1

    double-to-float v4, v4

    float-to-double v12, v15

    new-instance v5, Landroid/graphics/PointF;

    add-double v14, v28, v12

    double-to-float v7, v14

    invoke-direct {v5, v4, v7}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v4, 0x3

    aput-object v5, v3, v4

    iget-object v3, v0, Lc2/a$a;->b:[Landroid/graphics/PointF;

    new-instance v4, Landroid/graphics/PointF;

    sub-double/2addr v10, v8

    sub-double/2addr v10, v1

    double-to-float v1, v10

    add-double v8, v18, v12

    double-to-float v2, v8

    invoke-direct {v4, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v1, 0x0

    aput-object v4, v3, v1

    iget-object v1, v0, Lc2/a$a;->b:[Landroid/graphics/PointF;

    new-instance v2, Landroid/graphics/PointF;

    add-double v3, v20, v12

    double-to-float v3, v3

    invoke-direct {v2, v6, v3}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v3, 0x1

    aput-object v2, v1, v3

    iget-object v1, v0, Lc2/a$a;->b:[Landroid/graphics/PointF;

    add-double v2, v20, v22

    new-instance v4, Landroid/graphics/PointF;

    add-double v7, v2, v12

    double-to-float v5, v7

    invoke-direct {v4, v6, v5}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v10, 0x2

    aput-object v4, v1, v10

    iget-object v1, v0, Lc2/a$a;->b:[Landroid/graphics/PointF;

    new-instance v4, Landroid/graphics/PointF;

    move-wide/from16 v32, p5

    add-double v2, v2, v32

    add-double/2addr v2, v12

    double-to-float v2, v2

    invoke-direct {v4, v6, v2}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v2, 0x3

    aput-object v4, v1, v2

    goto/16 :goto_4

    :cond_5
    move/from16 v7, p2

    move-wide/from16 v32, v12

    const/4 v10, 0x2

    if-ne v11, v10, :cond_6

    iget v1, v0, Lc2/a$a;->c:F

    sub-float v6, v7, v2

    sub-float v10, p1, v3

    new-instance v11, Landroid/graphics/RectF;

    mul-float v1, v1, v17

    sub-float v12, v7, v1

    sub-float/2addr v12, v2

    sub-float v1, p1, v1

    sub-float/2addr v1, v3

    invoke-direct {v11, v12, v1, v6, v10}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v11, v0, Lc2/a$a;->d:Landroid/graphics/RectF;

    iget-object v1, v0, Lc2/a$a;->a:[Landroid/graphics/PointF;

    float-to-double v11, v7

    float-to-double v13, v2

    sub-double v25, v11, v26

    move-wide/from16 p5, v8

    sub-double v7, v25, v13

    double-to-float v2, v7

    move/from16 v7, p1

    float-to-double v7, v7

    move-wide/from16 v36, v4

    float-to-double v3, v3

    new-instance v5, Landroid/graphics/PointF;

    sub-double v25, v7, v28

    move/from16 p1, v6

    move-wide/from16 v38, v7

    sub-double v6, v25, v3

    double-to-float v6, v6

    invoke-direct {v5, v2, v6}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v2, 0x0

    aput-object v5, v1, v2

    iget-object v1, v0, Lc2/a$a;->a:[Landroid/graphics/PointF;

    sub-double v5, v11, v30

    new-instance v2, Landroid/graphics/PointF;

    sub-double v7, v5, v13

    double-to-float v7, v7

    invoke-direct {v2, v7, v10}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v7, 0x1

    aput-object v2, v1, v7

    iget-object v1, v0, Lc2/a$a;->a:[Landroid/graphics/PointF;

    sub-double v5, v5, v34

    new-instance v2, Landroid/graphics/PointF;

    sub-double v7, v5, v13

    double-to-float v7, v7

    invoke-direct {v2, v7, v10}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v7, 0x2

    aput-object v2, v1, v7

    iget-object v1, v0, Lc2/a$a;->a:[Landroid/graphics/PointF;

    new-instance v2, Landroid/graphics/PointF;

    sub-double v5, v5, v36

    sub-double/2addr v5, v13

    double-to-float v5, v5

    invoke-direct {v2, v5, v10}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v5, 0x3

    aput-object v2, v1, v5

    iget-object v1, v0, Lc2/a$a;->b:[Landroid/graphics/PointF;

    sub-double v7, v38, v20

    sub-double v5, v7, v22

    new-instance v2, Landroid/graphics/PointF;

    sub-double v9, v5, v32

    sub-double/2addr v9, v3

    double-to-float v9, v9

    move/from16 v10, p1

    invoke-direct {v2, v10, v9}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v9, 0x0

    aput-object v2, v1, v9

    iget-object v1, v0, Lc2/a$a;->b:[Landroid/graphics/PointF;

    new-instance v2, Landroid/graphics/PointF;

    sub-double/2addr v5, v3

    double-to-float v5, v5

    invoke-direct {v2, v10, v5}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v5, 0x1

    aput-object v2, v1, v5

    iget-object v1, v0, Lc2/a$a;->b:[Landroid/graphics/PointF;

    new-instance v2, Landroid/graphics/PointF;

    sub-double/2addr v7, v3

    double-to-float v5, v7

    invoke-direct {v2, v10, v5}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v5, 0x2

    aput-object v2, v1, v5

    iget-object v1, v0, Lc2/a$a;->b:[Landroid/graphics/PointF;

    new-instance v2, Landroid/graphics/PointF;

    move-wide/from16 v5, p5

    sub-double/2addr v11, v5

    sub-double/2addr v11, v13

    double-to-float v5, v11

    sub-double v7, v38, v18

    sub-double/2addr v7, v3

    double-to-float v3, v7

    invoke-direct {v2, v5, v3}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v4, 0x3

    aput-object v2, v1, v4

    goto/16 :goto_4

    :cond_6
    move/from16 v7, p1

    move-wide/from16 v36, v4

    move-wide v5, v8

    const/4 v4, 0x3

    if-ne v11, v4, :cond_7

    add-float/2addr v14, v2

    iget v1, v0, Lc2/a$a;->c:F

    sub-float v2, v7, v3

    new-instance v4, Landroid/graphics/RectF;

    mul-float v1, v1, v17

    sub-float v8, v7, v1

    sub-float/2addr v8, v3

    add-float/2addr v1, v14

    invoke-direct {v4, v14, v8, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v4, v0, Lc2/a$a;->d:Landroid/graphics/RectF;

    iget-object v1, v0, Lc2/a$a;->a:[Landroid/graphics/PointF;

    add-double v8, v30, v34

    float-to-double v10, v14

    new-instance v4, Landroid/graphics/PointF;

    add-double v12, v8, v36

    add-double/2addr v12, v10

    double-to-float v12, v12

    invoke-direct {v4, v12, v2}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v12, 0x0

    aput-object v4, v1, v12

    iget-object v1, v0, Lc2/a$a;->a:[Landroid/graphics/PointF;

    new-instance v4, Landroid/graphics/PointF;

    add-double/2addr v8, v10

    double-to-float v8, v8

    invoke-direct {v4, v8, v2}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v8, 0x1

    aput-object v4, v1, v8

    iget-object v1, v0, Lc2/a$a;->a:[Landroid/graphics/PointF;

    new-instance v4, Landroid/graphics/PointF;

    add-double v8, v30, v10

    double-to-float v8, v8

    invoke-direct {v4, v8, v2}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v2, 0x2

    aput-object v4, v1, v2

    iget-object v1, v0, Lc2/a$a;->a:[Landroid/graphics/PointF;

    add-double v8, v26, v10

    double-to-float v2, v8

    float-to-double v7, v7

    float-to-double v3, v3

    new-instance v9, Landroid/graphics/PointF;

    sub-double v12, v7, v28

    sub-double/2addr v12, v3

    double-to-float v12, v12

    invoke-direct {v9, v2, v12}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v2, 0x3

    aput-object v9, v1, v2

    iget-object v1, v0, Lc2/a$a;->b:[Landroid/graphics/PointF;

    new-instance v2, Landroid/graphics/PointF;

    add-double/2addr v5, v10

    double-to-float v5, v5

    sub-double v9, v7, v18

    sub-double/2addr v9, v3

    double-to-float v6, v9

    invoke-direct {v2, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v5, 0x0

    aput-object v2, v1, v5

    iget-object v1, v0, Lc2/a$a;->b:[Landroid/graphics/PointF;

    sub-double v7, v7, v20

    new-instance v2, Landroid/graphics/PointF;

    sub-double v5, v7, v3

    double-to-float v5, v5

    invoke-direct {v2, v14, v5}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v5, 0x1

    aput-object v2, v1, v5

    iget-object v1, v0, Lc2/a$a;->b:[Landroid/graphics/PointF;

    sub-double v7, v7, v22

    new-instance v2, Landroid/graphics/PointF;

    sub-double v5, v7, v3

    double-to-float v5, v5

    invoke-direct {v2, v14, v5}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v5, 0x2

    aput-object v2, v1, v5

    iget-object v1, v0, Lc2/a$a;->b:[Landroid/graphics/PointF;

    new-instance v2, Landroid/graphics/PointF;

    sub-double v7, v7, v32

    sub-double/2addr v7, v3

    double-to-float v3, v7

    invoke-direct {v2, v14, v3}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v3, 0x3

    aput-object v2, v1, v3

    :cond_7
    :goto_4
    return-void
.end method
