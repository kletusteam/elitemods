.class public Lc2/a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lc2/a$a;,
        Lc2/a$b;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(FFFDF)Z
    .locals 4

    float-to-double v0, p0

    add-float/2addr p1, p2

    float-to-double p0, p1

    float-to-double v2, p5

    mul-double/2addr p3, v2

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    add-double/2addr p3, v2

    mul-double/2addr p3, p0

    cmpg-double p0, v0, p3

    if-gtz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static c(FFFDF)Z
    .locals 4

    float-to-double v0, p0

    add-float/2addr p1, p2

    float-to-double p0, p1

    float-to-double v2, p5

    mul-double/2addr p3, v2

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    add-double/2addr p3, v2

    mul-double/2addr p3, p0

    cmpg-double p0, v0, p3

    if-gtz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static d(D)D
    .locals 2

    const-wide v0, 0x4066800000000000L    # 180.0

    mul-double/2addr p0, v0

    const-wide v0, 0x400921fb54442d18L    # Math.PI

    div-double/2addr p0, v0

    return-wide p0
.end method


# virtual methods
.method public a(Landroid/graphics/RectF;[FFF)Lc2/a$b;
    .locals 25

    move-object/from16 v0, p2

    const v1, 0x3f4ccccd    # 0.8f

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/RectF;->height()F

    move-result v3

    float-to-double v5, v1

    new-instance v4, Lc2/a$b;

    const v13, 0x3eeb851f    # 0.46f

    move-object v8, v4

    move v9, v2

    move v10, v3

    move-wide v11, v5

    invoke-direct/range {v8 .. v13}, Lc2/a$b;-><init>(FFDF)V

    const/16 v1, 0x8

    new-array v8, v1, [F

    fill-array-data v8, :array_0

    const/4 v9, 0x0

    move v10, v9

    :goto_0
    array-length v11, v0

    invoke-static {v1, v11}, Ljava/lang/Math;->min(II)I

    move-result v11

    if-ge v10, v11, :cond_1

    aget v11, v0, v10

    invoke-static {v11}, Ljava/lang/Float;->isNaN(F)Z

    move-result v11

    if-nez v11, :cond_0

    aget v11, v0, v10

    aput v11, v8, v10

    :cond_0
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    :cond_1
    aget v0, v8, v9

    const/4 v1, 0x1

    aget v10, v8, v1

    const/4 v11, 0x2

    aget v12, v8, v11

    const/4 v13, 0x3

    aget v14, v8, v13

    const/4 v15, 0x4

    aget v16, v8, v15

    const/16 v17, 0x5

    aget v18, v8, v17

    const/16 v19, 0x6

    aget v20, v8, v19

    const/16 v21, 0x7

    aget v22, v8, v21

    add-float v23, v0, v12

    cmpl-float v23, v23, v2

    if-lez v23, :cond_2

    aget v0, v8, v9

    mul-float/2addr v0, v2

    aget v12, v8, v9

    aget v23, v8, v11

    add-float v12, v12, v23

    div-float/2addr v0, v12

    aget v12, v8, v11

    mul-float/2addr v12, v2

    aget v9, v8, v9

    aget v11, v8, v11

    add-float/2addr v9, v11

    div-float/2addr v12, v9

    :cond_2
    add-float v9, v14, v18

    cmpl-float v9, v9, v3

    if-lez v9, :cond_3

    aget v9, v8, v13

    mul-float/2addr v9, v3

    aget v11, v8, v13

    aget v14, v8, v17

    add-float/2addr v11, v14

    div-float v14, v9, v11

    aget v9, v8, v17

    mul-float/2addr v9, v3

    aget v11, v8, v13

    aget v13, v8, v17

    add-float/2addr v11, v13

    div-float v18, v9, v11

    :cond_3
    move v13, v14

    move/from16 v14, v18

    add-float v9, v16, v20

    cmpl-float v9, v9, v2

    if-lez v9, :cond_4

    aget v9, v8, v15

    mul-float/2addr v9, v2

    aget v11, v8, v15

    aget v16, v8, v19

    add-float v11, v11, v16

    div-float v16, v9, v11

    aget v9, v8, v19

    mul-float/2addr v2, v9

    aget v9, v8, v15

    aget v11, v8, v19

    add-float/2addr v9, v11

    div-float v20, v2, v9

    :cond_4
    move/from16 v2, v16

    move/from16 v15, v20

    add-float v9, v22, v10

    cmpl-float v9, v9, v3

    if-lez v9, :cond_5

    aget v9, v8, v21

    mul-float/2addr v9, v3

    aget v10, v8, v21

    aget v11, v8, v1

    add-float/2addr v10, v11

    div-float v22, v9, v10

    aget v9, v8, v1

    mul-float/2addr v3, v9

    aget v9, v8, v21

    aget v1, v8, v1

    add-float/2addr v9, v1

    div-float v10, v3, v9

    :cond_5
    move/from16 v1, v22

    iget-object v3, v4, Lc2/a$b;->e:Lc2/a$a;

    if-nez v3, :cond_6

    new-instance v3, Lc2/a$a;

    invoke-direct {v3}, Lc2/a$a;-><init>()V

    iput-object v3, v4, Lc2/a$b;->e:Lc2/a$a;

    :cond_6
    iget-object v3, v4, Lc2/a$b;->f:Lc2/a$a;

    if-nez v3, :cond_7

    new-instance v3, Lc2/a$a;

    invoke-direct {v3}, Lc2/a$a;-><init>()V

    iput-object v3, v4, Lc2/a$b;->f:Lc2/a$a;

    :cond_7
    iget-object v3, v4, Lc2/a$b;->b:Lc2/a$a;

    if-nez v3, :cond_8

    new-instance v3, Lc2/a$a;

    invoke-direct {v3}, Lc2/a$a;-><init>()V

    iput-object v3, v4, Lc2/a$b;->b:Lc2/a$a;

    :cond_8
    iget-object v3, v4, Lc2/a$b;->a:Lc2/a$a;

    if-nez v3, :cond_9

    new-instance v3, Lc2/a$a;

    invoke-direct {v3}, Lc2/a$a;-><init>()V

    iput-object v3, v4, Lc2/a$b;->a:Lc2/a$a;

    :cond_9
    iget-object v8, v4, Lc2/a$b;->e:Lc2/a$a;

    invoke-static {v0, v10}, Ljava/lang/Math;->min(FF)F

    move-result v9

    const/16 v16, 0x0

    const v3, 0x3eeb851f    # 0.46f

    move-object/from16 v10, p1

    move/from16 v11, p3

    move v7, v12

    move/from16 v12, p4

    move/from16 v18, v1

    move v0, v13

    move v1, v14

    move-wide v13, v5

    move/from16 v24, v15

    move v15, v3

    invoke-virtual/range {v8 .. v16}, Lc2/a$a;->a(FLandroid/graphics/RectF;FFDFI)V

    iget-object v8, v4, Lc2/a$b;->f:Lc2/a$a;

    invoke-static {v7, v0}, Ljava/lang/Math;->min(FF)F

    move-result v9

    const/16 v16, 0x1

    const v15, 0x3eeb851f    # 0.46f

    invoke-virtual/range {v8 .. v16}, Lc2/a$a;->a(FLandroid/graphics/RectF;FFDFI)V

    iget-object v8, v4, Lc2/a$b;->b:Lc2/a$a;

    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v9

    const/16 v16, 0x2

    const v0, 0x3eeb851f    # 0.46f

    move v15, v0

    invoke-virtual/range {v8 .. v16}, Lc2/a$a;->a(FLandroid/graphics/RectF;FFDFI)V

    iget-object v0, v4, Lc2/a$b;->a:Lc2/a$a;

    move/from16 v2, v18

    move/from16 v1, v24

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    const/4 v8, 0x3

    move-object/from16 v2, p1

    move/from16 v3, p3

    move-object v9, v4

    move/from16 v4, p4

    const v7, 0x3eeb851f    # 0.46f

    invoke-virtual/range {v0 .. v8}, Lc2/a$a;->a(FLandroid/graphics/RectF;FFDFI)V

    return-object v9

    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method
