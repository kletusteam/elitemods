.class public Landroidx/recyclerview/widget/p$d;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/recyclerview/widget/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "d"
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroidx/recyclerview/widget/p$c;",
            ">;"
        }
    .end annotation
.end field

.field public final b:[I

.field public final c:[I

.field public final d:Landroidx/recyclerview/widget/p$b;

.field public final e:I

.field public final f:I

.field public final g:Z


# direct methods
.method public constructor <init>(Landroidx/recyclerview/widget/p$b;Ljava/util/List;[I[IZ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/recyclerview/widget/p$b;",
            "Ljava/util/List<",
            "Landroidx/recyclerview/widget/p$c;",
            ">;[I[IZ)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Landroidx/recyclerview/widget/p$d;->a:Ljava/util/List;

    iput-object p3, p0, Landroidx/recyclerview/widget/p$d;->b:[I

    iput-object p4, p0, Landroidx/recyclerview/widget/p$d;->c:[I

    const/4 v0, 0x0

    invoke-static {p3, v0}, Ljava/util/Arrays;->fill([II)V

    invoke-static {p4, v0}, Ljava/util/Arrays;->fill([II)V

    iput-object p1, p0, Landroidx/recyclerview/widget/p$d;->d:Landroidx/recyclerview/widget/p$b;

    check-cast p1, Landroidx/recyclerview/widget/d$a;

    iget-object p3, p1, Landroidx/recyclerview/widget/d$a;->a:Landroidx/recyclerview/widget/d;

    iget-object p3, p3, Landroidx/recyclerview/widget/d;->a:Ljava/util/List;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p3

    iput p3, p0, Landroidx/recyclerview/widget/p$d;->e:I

    iget-object p1, p1, Landroidx/recyclerview/widget/d$a;->a:Landroidx/recyclerview/widget/d;

    iget-object p1, p1, Landroidx/recyclerview/widget/d;->b:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    iput p1, p0, Landroidx/recyclerview/widget/p$d;->f:I

    iput-boolean p5, p0, Landroidx/recyclerview/widget/p$d;->g:Z

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result p4

    if-eqz p4, :cond_0

    const/4 p4, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Landroidx/recyclerview/widget/p$c;

    :goto_0
    if-eqz p4, :cond_1

    iget p5, p4, Landroidx/recyclerview/widget/p$c;->a:I

    if-nez p5, :cond_1

    iget p4, p4, Landroidx/recyclerview/widget/p$c;->b:I

    if-eqz p4, :cond_2

    :cond_1
    new-instance p4, Landroidx/recyclerview/widget/p$c;

    invoke-direct {p4, v0, v0, v0}, Landroidx/recyclerview/widget/p$c;-><init>(III)V

    invoke-interface {p2, v0, p4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_2
    new-instance p4, Landroidx/recyclerview/widget/p$c;

    invoke-direct {p4, p3, p1, v0}, Landroidx/recyclerview/widget/p$c;-><init>(III)V

    invoke-interface {p2, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroidx/recyclerview/widget/p$c;

    move p3, v0

    :goto_1
    iget p4, p2, Landroidx/recyclerview/widget/p$c;->c:I

    if-ge p3, p4, :cond_3

    iget p4, p2, Landroidx/recyclerview/widget/p$c;->a:I

    add-int/2addr p4, p3

    iget p5, p2, Landroidx/recyclerview/widget/p$c;->b:I

    add-int/2addr p5, p3

    iget-object v1, p0, Landroidx/recyclerview/widget/p$d;->d:Landroidx/recyclerview/widget/p$b;

    invoke-virtual {v1, p4, p5}, Landroidx/recyclerview/widget/p$b;->a(II)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    goto :goto_2

    :cond_4
    const/4 v1, 0x2

    :goto_2
    iget-object v2, p0, Landroidx/recyclerview/widget/p$d;->b:[I

    shl-int/lit8 v3, p5, 0x4

    or-int/2addr v3, v1

    aput v3, v2, p4

    iget-object v2, p0, Landroidx/recyclerview/widget/p$d;->c:[I

    shl-int/lit8 p4, p4, 0x4

    or-int/2addr p4, v1

    aput p4, v2, p5

    add-int/lit8 p3, p3, 0x1

    goto :goto_1

    :cond_5
    iget-boolean p1, p0, Landroidx/recyclerview/widget/p$d;->g:Z

    if-eqz p1, :cond_b

    iget-object p1, p0, Landroidx/recyclerview/widget/p$d;->a:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    move p2, v0

    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_b

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Landroidx/recyclerview/widget/p$c;

    :goto_4
    iget p4, p3, Landroidx/recyclerview/widget/p$c;->a:I

    if-ge p2, p4, :cond_a

    iget-object p4, p0, Landroidx/recyclerview/widget/p$d;->b:[I

    aget p4, p4, p2

    if-nez p4, :cond_9

    iget-object p4, p0, Landroidx/recyclerview/widget/p$d;->a:Ljava/util/List;

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result p4

    move p5, v0

    move v1, p5

    :goto_5
    if-ge p5, p4, :cond_9

    iget-object v2, p0, Landroidx/recyclerview/widget/p$d;->a:Ljava/util/List;

    invoke-interface {v2, p5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroidx/recyclerview/widget/p$c;

    :goto_6
    iget v3, v2, Landroidx/recyclerview/widget/p$c;->b:I

    if-ge v1, v3, :cond_8

    iget-object v3, p0, Landroidx/recyclerview/widget/p$d;->c:[I

    aget v3, v3, v1

    if-nez v3, :cond_7

    iget-object v3, p0, Landroidx/recyclerview/widget/p$d;->d:Landroidx/recyclerview/widget/p$b;

    invoke-virtual {v3, p2, v1}, Landroidx/recyclerview/widget/p$b;->b(II)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object p4, p0, Landroidx/recyclerview/widget/p$d;->d:Landroidx/recyclerview/widget/p$b;

    invoke-virtual {p4, p2, v1}, Landroidx/recyclerview/widget/p$b;->a(II)Z

    move-result p4

    if-eqz p4, :cond_6

    const/16 p4, 0x8

    goto :goto_7

    :cond_6
    const/4 p4, 0x4

    :goto_7
    iget-object p5, p0, Landroidx/recyclerview/widget/p$d;->b:[I

    shl-int/lit8 v2, v1, 0x4

    or-int/2addr v2, p4

    aput v2, p5, p2

    iget-object p5, p0, Landroidx/recyclerview/widget/p$d;->c:[I

    shl-int/lit8 v2, p2, 0x4

    or-int/2addr p4, v2

    aput p4, p5, v1

    goto :goto_8

    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_8
    iget v1, v2, Landroidx/recyclerview/widget/p$c;->c:I

    add-int/2addr v1, v3

    add-int/lit8 p5, p5, 0x1

    goto :goto_5

    :cond_9
    :goto_8
    add-int/lit8 p2, p2, 0x1

    goto :goto_4

    :cond_a
    iget p2, p3, Landroidx/recyclerview/widget/p$c;->c:I

    add-int/2addr p2, p4

    goto :goto_3

    :cond_b
    return-void
.end method

.method public static a(Ljava/util/Collection;IZ)Landroidx/recyclerview/widget/p$f;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Landroidx/recyclerview/widget/p$f;",
            ">;IZ)",
            "Landroidx/recyclerview/widget/p$f;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/p$f;

    iget v1, v0, Landroidx/recyclerview/widget/p$f;->a:I

    if-ne v1, p1, :cond_0

    iget-boolean v1, v0, Landroidx/recyclerview/widget/p$f;->c:Z

    if-ne v1, p2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/p$f;

    iget v1, p1, Landroidx/recyclerview/widget/p$f;->b:I

    if-eqz p2, :cond_2

    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    :goto_1
    iput v1, p1, Landroidx/recyclerview/widget/p$f;->b:I

    goto :goto_0

    :cond_3
    return-object v0
.end method
