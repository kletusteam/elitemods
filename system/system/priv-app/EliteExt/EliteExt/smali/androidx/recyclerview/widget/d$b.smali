.class public Landroidx/recyclerview/widget/d$b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroidx/recyclerview/widget/d;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroidx/recyclerview/widget/p$d;

.field public final synthetic b:Landroidx/recyclerview/widget/d;


# direct methods
.method public constructor <init>(Landroidx/recyclerview/widget/d;Landroidx/recyclerview/widget/p$d;)V
    .locals 0

    iput-object p1, p0, Landroidx/recyclerview/widget/d$b;->b:Landroidx/recyclerview/widget/d;

    iput-object p2, p0, Landroidx/recyclerview/widget/d$b;->a:Landroidx/recyclerview/widget/p$d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Landroidx/recyclerview/widget/d$b;->b:Landroidx/recyclerview/widget/d;

    iget-object v2, v1, Landroidx/recyclerview/widget/d;->e:Landroidx/recyclerview/widget/e;

    iget v3, v2, Landroidx/recyclerview/widget/e;->g:I

    iget v4, v1, Landroidx/recyclerview/widget/d;->c:I

    if-ne v3, v4, :cond_c

    iget-object v3, v1, Landroidx/recyclerview/widget/d;->b:Ljava/util/List;

    iget-object v4, v0, Landroidx/recyclerview/widget/d$b;->a:Landroidx/recyclerview/widget/p$d;

    iget-object v1, v1, Landroidx/recyclerview/widget/d;->d:Ljava/lang/Runnable;

    iget-object v5, v2, Landroidx/recyclerview/widget/e;->f:Ljava/util/List;

    iput-object v3, v2, Landroidx/recyclerview/widget/e;->e:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, v2, Landroidx/recyclerview/widget/e;->f:Ljava/util/List;

    iget-object v3, v2, Landroidx/recyclerview/widget/e;->a:Lj0/b;

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    instance-of v6, v3, Lj0/a;

    if-eqz v6, :cond_0

    check-cast v3, Lj0/a;

    goto :goto_0

    :cond_0
    new-instance v6, Lj0/a;

    invoke-direct {v6, v3}, Lj0/a;-><init>(Lj0/b;)V

    move-object v3, v6

    :goto_0
    iget v6, v4, Landroidx/recyclerview/widget/p$d;->e:I

    new-instance v7, Ljava/util/ArrayDeque;

    invoke-direct {v7}, Ljava/util/ArrayDeque;-><init>()V

    iget v8, v4, Landroidx/recyclerview/widget/p$d;->e:I

    iget v9, v4, Landroidx/recyclerview/widget/p$d;->f:I

    iget-object v10, v4, Landroidx/recyclerview/widget/p$d;->a:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    const/4 v11, 0x1

    sub-int/2addr v10, v11

    :goto_1
    if-ltz v10, :cond_b

    iget-object v12, v4, Landroidx/recyclerview/widget/p$d;->a:Ljava/util/List;

    invoke-interface {v12, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroidx/recyclerview/widget/p$c;

    iget v13, v12, Landroidx/recyclerview/widget/p$c;->a:I

    iget v14, v12, Landroidx/recyclerview/widget/p$c;->c:I

    add-int/2addr v13, v14

    iget v15, v12, Landroidx/recyclerview/widget/p$c;->b:I

    add-int/2addr v15, v14

    :goto_2
    const/4 v14, 0x0

    if-le v8, v13, :cond_4

    add-int/lit8 v8, v8, -0x1

    iget-object v11, v4, Landroidx/recyclerview/widget/p$d;->b:[I

    aget v11, v11, v8

    and-int/lit8 v16, v11, 0xc

    if-eqz v16, :cond_3

    shr-int/lit8 v0, v11, 0x4

    invoke-static {v7, v0, v14}, Landroidx/recyclerview/widget/p$d;->a(Ljava/util/Collection;IZ)Landroidx/recyclerview/widget/p$f;

    move-result-object v14

    if-eqz v14, :cond_1

    iget v14, v14, Landroidx/recyclerview/widget/p$f;->b:I

    sub-int v14, v6, v14

    move/from16 v16, v9

    const/4 v9, 0x1

    sub-int/2addr v14, v9

    invoke-virtual {v3, v8, v14}, Lj0/a;->d(II)V

    and-int/lit8 v11, v11, 0x4

    if-eqz v11, :cond_2

    iget-object v11, v4, Landroidx/recyclerview/widget/p$d;->d:Landroidx/recyclerview/widget/p$b;

    invoke-virtual {v11, v8, v0}, Landroidx/recyclerview/widget/p$b;->c(II)Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-virtual {v3, v14, v9, v0}, Lj0/a;->a(IILjava/lang/Object;)V

    goto :goto_3

    :cond_1
    move/from16 v16, v9

    const/4 v9, 0x1

    new-instance v0, Landroidx/recyclerview/widget/p$f;

    sub-int v11, v6, v8

    sub-int/2addr v11, v9

    invoke-direct {v0, v8, v11, v9}, Landroidx/recyclerview/widget/p$f;-><init>(IIZ)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_3
    move-object/from16 v0, p0

    move v11, v9

    move/from16 v9, v16

    goto :goto_2

    :cond_3
    move/from16 v16, v9

    const/4 v9, 0x1

    invoke-virtual {v3, v8, v9}, Lj0/a;->b(II)V

    add-int/lit8 v6, v6, -0x1

    move-object/from16 v0, p0

    move/from16 v9, v16

    const/4 v11, 0x1

    goto :goto_2

    :cond_4
    move/from16 v16, v9

    :goto_4
    if-le v9, v15, :cond_8

    add-int/lit8 v9, v9, -0x1

    iget-object v0, v4, Landroidx/recyclerview/widget/p$d;->c:[I

    aget v0, v0, v9

    and-int/lit8 v11, v0, 0xc

    if-eqz v11, :cond_7

    shr-int/lit8 v11, v0, 0x4

    const/4 v13, 0x1

    invoke-static {v7, v11, v13}, Landroidx/recyclerview/widget/p$d;->a(Ljava/util/Collection;IZ)Landroidx/recyclerview/widget/p$f;

    move-result-object v14

    if-nez v14, :cond_5

    new-instance v0, Landroidx/recyclerview/widget/p$f;

    sub-int v11, v6, v8

    const/4 v14, 0x0

    invoke-direct {v0, v9, v11, v14}, Landroidx/recyclerview/widget/p$f;-><init>(IIZ)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    move/from16 v16, v14

    goto :goto_5

    :cond_5
    const/16 v16, 0x0

    iget v14, v14, Landroidx/recyclerview/widget/p$f;->b:I

    sub-int v14, v6, v14

    sub-int/2addr v14, v13

    invoke-virtual {v3, v14, v8}, Lj0/a;->d(II)V

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_6

    iget-object v0, v4, Landroidx/recyclerview/widget/p$d;->d:Landroidx/recyclerview/widget/p$b;

    invoke-virtual {v0, v11, v9}, Landroidx/recyclerview/widget/p$b;->c(II)Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-virtual {v3, v8, v13, v0}, Lj0/a;->a(IILjava/lang/Object;)V

    :cond_6
    :goto_5
    move/from16 v14, v16

    goto :goto_4

    :cond_7
    move/from16 v16, v14

    const/4 v13, 0x1

    invoke-virtual {v3, v8, v13}, Lj0/a;->c(II)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :cond_8
    move/from16 v16, v14

    iget v0, v12, Landroidx/recyclerview/widget/p$c;->a:I

    iget v8, v12, Landroidx/recyclerview/widget/p$c;->b:I

    :goto_6
    iget v9, v12, Landroidx/recyclerview/widget/p$c;->c:I

    if-ge v14, v9, :cond_a

    iget-object v9, v4, Landroidx/recyclerview/widget/p$d;->b:[I

    aget v9, v9, v0

    and-int/lit8 v9, v9, 0xf

    const/4 v11, 0x2

    if-ne v9, v11, :cond_9

    iget-object v9, v4, Landroidx/recyclerview/widget/p$d;->d:Landroidx/recyclerview/widget/p$b;

    invoke-virtual {v9, v0, v8}, Landroidx/recyclerview/widget/p$b;->c(II)Ljava/lang/Object;

    const/4 v9, 0x0

    const/4 v11, 0x1

    invoke-virtual {v3, v0, v11, v9}, Lj0/a;->a(IILjava/lang/Object;)V

    goto :goto_7

    :cond_9
    const/4 v9, 0x0

    const/4 v11, 0x1

    :goto_7
    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v8, v8, 0x1

    add-int/lit8 v14, v14, 0x1

    goto :goto_6

    :cond_a
    const/4 v11, 0x1

    iget v8, v12, Landroidx/recyclerview/widget/p$c;->a:I

    iget v9, v12, Landroidx/recyclerview/widget/p$c;->b:I

    add-int/lit8 v10, v10, -0x1

    move-object/from16 v0, p0

    goto/16 :goto_1

    :cond_b
    invoke-virtual {v3}, Lj0/a;->e()V

    invoke-virtual {v2, v5, v1}, Landroidx/recyclerview/widget/e;->a(Ljava/util/List;Ljava/lang/Runnable;)V

    :cond_c
    return-void
.end method
