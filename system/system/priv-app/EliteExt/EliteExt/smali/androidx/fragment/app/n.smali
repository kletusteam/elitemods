.class public abstract Landroidx/fragment/app/n;
.super Landroidx/activity/result/d;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Landroidx/activity/result/d;"
    }
.end annotation


# instance fields
.field public final a:Landroid/app/Activity;

.field public final b:Landroid/content/Context;

.field public final c:Landroid/os/Handler;

.field public final d:Landroidx/fragment/app/q;


# direct methods
.method public constructor <init>(Landroidx/fragment/app/i;)V
    .locals 2

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0}, Landroidx/activity/result/d;-><init>()V

    new-instance v1, Landroidx/fragment/app/r;

    invoke-direct {v1}, Landroidx/fragment/app/r;-><init>()V

    iput-object v1, p0, Landroidx/fragment/app/n;->d:Landroidx/fragment/app/q;

    iput-object p1, p0, Landroidx/fragment/app/n;->a:Landroid/app/Activity;

    const-string v1, "context == null"

    invoke-static {p1, v1}, Landroidx/emoji2/text/l;->l(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Landroidx/fragment/app/n;->b:Landroid/content/Context;

    iput-object v0, p0, Landroidx/fragment/app/n;->c:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public abstract m()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation
.end method

.method public abstract n()Landroid/view/LayoutInflater;
.end method

.method public abstract o()V
.end method
