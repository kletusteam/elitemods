.class public Landroidx/fragment/app/e0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroidx/fragment/app/h0;

.field public final synthetic b:Lk/a;

.field public final synthetic c:Ljava/lang/Object;

.field public final synthetic d:Landroidx/fragment/app/f0$b;

.field public final synthetic e:Ljava/util/ArrayList;

.field public final synthetic f:Landroidx/fragment/app/f;

.field public final synthetic g:Landroidx/fragment/app/f;

.field public final synthetic h:Z

.field public final synthetic i:Ljava/util/ArrayList;

.field public final synthetic j:Ljava/lang/Object;

.field public final synthetic k:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroidx/fragment/app/h0;Lk/a;Ljava/lang/Object;Landroidx/fragment/app/f0$b;Ljava/util/ArrayList;Landroid/view/View;Landroidx/fragment/app/f;Landroidx/fragment/app/f;ZLjava/util/ArrayList;Ljava/lang/Object;Landroid/graphics/Rect;)V
    .locals 0

    iput-object p1, p0, Landroidx/fragment/app/e0;->a:Landroidx/fragment/app/h0;

    iput-object p2, p0, Landroidx/fragment/app/e0;->b:Lk/a;

    iput-object p3, p0, Landroidx/fragment/app/e0;->c:Ljava/lang/Object;

    iput-object p4, p0, Landroidx/fragment/app/e0;->d:Landroidx/fragment/app/f0$b;

    iput-object p5, p0, Landroidx/fragment/app/e0;->e:Ljava/util/ArrayList;

    iput-object p7, p0, Landroidx/fragment/app/e0;->f:Landroidx/fragment/app/f;

    iput-object p8, p0, Landroidx/fragment/app/e0;->g:Landroidx/fragment/app/f;

    iput-boolean p9, p0, Landroidx/fragment/app/e0;->h:Z

    iput-object p10, p0, Landroidx/fragment/app/e0;->i:Ljava/util/ArrayList;

    iput-object p11, p0, Landroidx/fragment/app/e0;->j:Ljava/lang/Object;

    iput-object p12, p0, Landroidx/fragment/app/e0;->k:Landroid/graphics/Rect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v0, p0, Landroidx/fragment/app/e0;->b:Lk/a;

    iget-object v1, p0, Landroidx/fragment/app/e0;->d:Landroidx/fragment/app/f0$b;

    iget-object v1, v1, Landroidx/fragment/app/f0$b;->a:Landroidx/fragment/app/f;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lk/g;->clear()V

    iget-object v0, p0, Landroidx/fragment/app/e0;->f:Landroidx/fragment/app/f;

    iget-object v1, p0, Landroidx/fragment/app/e0;->g:Landroidx/fragment/app/f;

    iget-boolean v2, p0, Landroidx/fragment/app/e0;->h:Z

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroidx/fragment/app/f0;->c(Landroidx/fragment/app/f;Landroidx/fragment/app/f;ZLk/a;Z)V

    iget-object v0, p0, Landroidx/fragment/app/e0;->c:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v1, p0, Landroidx/fragment/app/e0;->a:Landroidx/fragment/app/h0;

    iget-object v2, p0, Landroidx/fragment/app/e0;->i:Ljava/util/ArrayList;

    iget-object v3, p0, Landroidx/fragment/app/e0;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, v2, v3}, Landroidx/fragment/app/h0;->o(Ljava/lang/Object;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    iget-object v0, p0, Landroidx/fragment/app/e0;->d:Landroidx/fragment/app/f0$b;

    iget-object v0, v0, Landroidx/fragment/app/f0$b;->c:Landroidx/fragment/app/a;

    :cond_0
    return-void
.end method
