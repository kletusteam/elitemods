.class public Landroidx/fragment/app/f0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/fragment/app/f0$b;,
        Landroidx/fragment/app/f0$a;
    }
.end annotation


# static fields
.field public static final a:[I

.field public static final b:Landroidx/fragment/app/h0;

.field public static final c:Landroidx/fragment/app/h0;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Landroidx/fragment/app/f0;->a:[I

    new-instance v0, Landroidx/fragment/app/g0;

    invoke-direct {v0}, Landroidx/fragment/app/g0;-><init>()V

    sput-object v0, Landroidx/fragment/app/f0;->b:Landroidx/fragment/app/h0;

    :try_start_0
    const-string v0, "androidx.transition.FragmentTransitionSupport"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Class;

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/fragment/app/h0;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 v0, 0x0

    :goto_0
    sput-object v0, Landroidx/fragment/app/f0;->c:Landroidx/fragment/app/h0;

    return-void

    :array_0
    .array-data 4
        0x0
        0x3
        0x0
        0x1
        0x5
        0x4
        0x7
        0x6
        0x9
        0x8
        0xa
    .end array-data
.end method

.method public static a(Ljava/util/ArrayList;Lk/a;Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;",
            "Lk/a<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget v0, p1, Lk/g;->c:I

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    invoke-virtual {p1, v0}, Lk/g;->k(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    sget-object v2, Lw/v;->a:Ljava/util/WeakHashMap;

    invoke-static {v1}, Lw/v$h;->k(Landroid/view/View;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static b(Landroidx/fragment/app/a;Landroidx/fragment/app/y$a;Landroid/util/SparseArray;ZZ)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/a;",
            "Landroidx/fragment/app/y$a;",
            "Landroid/util/SparseArray<",
            "Landroidx/fragment/app/f0$b;",
            ">;ZZ)V"
        }
    .end annotation

    iget-object v0, p1, Landroidx/fragment/app/y$a;->b:Landroidx/fragment/app/f;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget v1, v0, Landroidx/fragment/app/f;->w:I

    if-nez v1, :cond_1

    return-void

    :cond_1
    if-eqz p3, :cond_2

    sget-object v2, Landroidx/fragment/app/f0;->a:[I

    iget p1, p1, Landroidx/fragment/app/y$a;->a:I

    aget p1, v2, p1

    goto :goto_0

    :cond_2
    iget p1, p1, Landroidx/fragment/app/y$a;->a:I

    :goto_0
    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eq p1, v2, :cond_a

    const/4 v4, 0x3

    if-eq p1, v4, :cond_7

    const/4 v4, 0x4

    if-eq p1, v4, :cond_5

    const/4 v4, 0x5

    if-eq p1, v4, :cond_3

    const/4 v4, 0x6

    if-eq p1, v4, :cond_7

    const/4 v4, 0x7

    if-eq p1, v4, :cond_a

    move p1, v3

    move v2, p1

    move v4, v2

    goto/16 :goto_6

    :cond_3
    if-eqz p4, :cond_4

    iget-boolean p1, v0, Landroidx/fragment/app/f;->H:Z

    if-eqz p1, :cond_b

    iget-boolean p1, v0, Landroidx/fragment/app/f;->y:Z

    if-nez p1, :cond_b

    iget-boolean p1, v0, Landroidx/fragment/app/f;->k:Z

    if-eqz p1, :cond_b

    goto :goto_4

    :cond_4
    iget-boolean p1, v0, Landroidx/fragment/app/f;->y:Z

    goto :goto_5

    :cond_5
    if-eqz p4, :cond_6

    iget-boolean p1, v0, Landroidx/fragment/app/f;->H:Z

    if-eqz p1, :cond_9

    iget-boolean p1, v0, Landroidx/fragment/app/f;->k:Z

    if-eqz p1, :cond_9

    iget-boolean p1, v0, Landroidx/fragment/app/f;->y:Z

    if-eqz p1, :cond_9

    goto :goto_1

    :cond_6
    iget-boolean p1, v0, Landroidx/fragment/app/f;->k:Z

    if-eqz p1, :cond_9

    iget-boolean p1, v0, Landroidx/fragment/app/f;->y:Z

    if-nez p1, :cond_9

    goto :goto_1

    :cond_7
    iget-boolean p1, v0, Landroidx/fragment/app/f;->k:Z

    if-eqz p4, :cond_8

    goto :goto_2

    :cond_8
    if-eqz p1, :cond_9

    iget-boolean p1, v0, Landroidx/fragment/app/f;->y:Z

    if-nez p1, :cond_9

    :goto_1
    move p1, v2

    goto :goto_3

    :cond_9
    :goto_2
    move p1, v3

    :goto_3
    move v4, p1

    move p1, v3

    move v3, v2

    move v2, p1

    goto :goto_6

    :cond_a
    if-eqz p4, :cond_c

    :cond_b
    move p1, v3

    goto :goto_5

    :cond_c
    iget-boolean p1, v0, Landroidx/fragment/app/f;->k:Z

    if-nez p1, :cond_b

    iget-boolean p1, v0, Landroidx/fragment/app/f;->y:Z

    if-nez p1, :cond_b

    :goto_4
    move p1, v2

    :goto_5
    move v4, v3

    :goto_6
    invoke-virtual {p2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroidx/fragment/app/f0$b;

    if-eqz p1, :cond_e

    if-nez v5, :cond_d

    new-instance p1, Landroidx/fragment/app/f0$b;

    invoke-direct {p1}, Landroidx/fragment/app/f0$b;-><init>()V

    invoke-virtual {p2, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move-object v5, p1

    :cond_d
    iput-object v0, v5, Landroidx/fragment/app/f0$b;->a:Landroidx/fragment/app/f;

    iput-boolean p3, v5, Landroidx/fragment/app/f0$b;->b:Z

    iput-object p0, v5, Landroidx/fragment/app/f0$b;->c:Landroidx/fragment/app/a;

    :cond_e
    const/4 p1, 0x0

    if-nez p4, :cond_10

    if-eqz v2, :cond_10

    if-eqz v5, :cond_f

    iget-object v2, v5, Landroidx/fragment/app/f0$b;->d:Landroidx/fragment/app/f;

    if-ne v2, v0, :cond_f

    iput-object p1, v5, Landroidx/fragment/app/f0$b;->d:Landroidx/fragment/app/f;

    :cond_f
    iget-boolean v2, p0, Landroidx/fragment/app/y;->o:Z

    if-nez v2, :cond_10

    iget-object v2, p0, Landroidx/fragment/app/a;->p:Landroidx/fragment/app/q;

    invoke-virtual {v2, v0}, Landroidx/fragment/app/q;->h(Landroidx/fragment/app/f;)Landroidx/fragment/app/w;

    move-result-object v6

    iget-object v7, v2, Landroidx/fragment/app/q;->c:Landroidx/fragment/app/x;

    invoke-virtual {v7, v6}, Landroidx/fragment/app/x;->j(Landroidx/fragment/app/w;)V

    iget v6, v2, Landroidx/fragment/app/q;->p:I

    invoke-virtual {v2, v0, v6}, Landroidx/fragment/app/q;->Q(Landroidx/fragment/app/f;I)V

    :cond_10
    if-eqz v4, :cond_13

    if-eqz v5, :cond_11

    iget-object v2, v5, Landroidx/fragment/app/f0$b;->d:Landroidx/fragment/app/f;

    if-nez v2, :cond_13

    :cond_11
    if-nez v5, :cond_12

    new-instance v2, Landroidx/fragment/app/f0$b;

    invoke-direct {v2}, Landroidx/fragment/app/f0$b;-><init>()V

    invoke-virtual {p2, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move-object v5, v2

    :cond_12
    iput-object v0, v5, Landroidx/fragment/app/f0$b;->d:Landroidx/fragment/app/f;

    iput-boolean p3, v5, Landroidx/fragment/app/f0$b;->e:Z

    iput-object p0, v5, Landroidx/fragment/app/f0$b;->f:Landroidx/fragment/app/a;

    :cond_13
    if-nez p4, :cond_14

    if-eqz v3, :cond_14

    if-eqz v5, :cond_14

    iget-object p0, v5, Landroidx/fragment/app/f0$b;->a:Landroidx/fragment/app/f;

    if-ne p0, v0, :cond_14

    iput-object p1, v5, Landroidx/fragment/app/f0$b;->a:Landroidx/fragment/app/f;

    :cond_14
    return-void
.end method

.method public static c(Landroidx/fragment/app/f;Landroidx/fragment/app/f;ZLk/a;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/f;",
            "Landroidx/fragment/app/f;",
            "Z",
            "Lk/a<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;Z)V"
        }
    .end annotation

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Landroidx/fragment/app/f;->k()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/f;->k()V

    :goto_0
    return-void
.end method

.method public static d(Landroidx/fragment/app/h0;Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/h0;",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v3}, Landroidx/fragment/app/h0;->d(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    return v1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x1

    return p0
.end method

.method public static e(Landroidx/fragment/app/h0;Lk/a;Ljava/lang/Object;Landroidx/fragment/app/f0$b;)Lk/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/h0;",
            "Lk/a<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Object;",
            "Landroidx/fragment/app/f0$b;",
            ")",
            "Lk/a<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Lk/g;->isEmpty()Z

    move-result p0

    const/4 v0, 0x0

    if-nez p0, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    iget-object p0, p3, Landroidx/fragment/app/f0$b;->d:Landroidx/fragment/app/f;

    invoke-virtual {p0}, Landroidx/fragment/app/f;->E()Landroid/view/View;

    throw v0

    :cond_1
    :goto_0
    invoke-virtual {p1}, Lk/g;->clear()V

    return-object v0
.end method

.method public static f(Landroidx/fragment/app/f;Landroidx/fragment/app/f;)Landroidx/fragment/app/h0;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/f;->m()Ljava/lang/Object;

    invoke-virtual {p0}, Landroidx/fragment/app/f;->t()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/f;->v()Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_1

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Landroidx/fragment/app/f;->j()Ljava/lang/Object;

    invoke-virtual {p1}, Landroidx/fragment/app/f;->s()Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_2

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {p1}, Landroidx/fragment/app/f;->u()Ljava/lang/Object;

    :cond_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_4

    const/4 p0, 0x0

    return-object p0

    :cond_4
    sget-object p0, Landroidx/fragment/app/f0;->b:Landroidx/fragment/app/h0;

    invoke-static {p0, v0}, Landroidx/fragment/app/f0;->d(Landroidx/fragment/app/h0;Ljava/util/List;)Z

    move-result p1

    if-eqz p1, :cond_5

    return-object p0

    :cond_5
    sget-object p0, Landroidx/fragment/app/f0;->c:Landroidx/fragment/app/h0;

    if-eqz p0, :cond_6

    invoke-static {p0, v0}, Landroidx/fragment/app/f0;->d(Landroidx/fragment/app/h0;Ljava/util/List;)Z

    move-result p1

    if-eqz p1, :cond_6

    return-object p0

    :cond_6
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Invalid Transition types"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static g(Landroidx/fragment/app/h0;Ljava/lang/Object;Landroidx/fragment/app/f;Ljava/util/ArrayList;Landroid/view/View;)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/h0;",
            "Ljava/lang/Object;",
            "Landroidx/fragment/app/f;",
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;",
            "Landroid/view/View;",
            ")",
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p3, :cond_0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_2

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, p1, v0}, Landroidx/fragment/app/h0;->b(Ljava/lang/Object;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :cond_2
    :goto_0
    return-object v0
.end method

.method public static h(Landroidx/fragment/app/h0;Landroidx/fragment/app/f;Z)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p1}, Landroidx/fragment/app/f;->s()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroidx/fragment/app/f;->j()Ljava/lang/Object;

    :goto_0
    invoke-virtual {p0, v0}, Landroidx/fragment/app/h0;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static i(Landroidx/fragment/app/h0;Landroidx/fragment/app/f;Z)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p1}, Landroidx/fragment/app/f;->t()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroidx/fragment/app/f;->m()Ljava/lang/Object;

    :goto_0
    invoke-virtual {p0, v0}, Landroidx/fragment/app/h0;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static j(Landroidx/fragment/app/h0;Landroidx/fragment/app/f;Landroidx/fragment/app/f;Z)Ljava/lang/Object;
    .locals 0

    if-eqz p3, :cond_0

    invoke-virtual {p2}, Landroidx/fragment/app/f;->v()Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroidx/fragment/app/f;->u()Ljava/lang/Object;

    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p0, p1}, Landroidx/fragment/app/h0;->e(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroidx/fragment/app/h0;->p(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static k(Ljava/util/ArrayList;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;I)V"
        }
    .end annotation

    if-nez p0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static l(Landroid/content/Context;Landroidx/activity/result/d;Ljava/util/ArrayList;Ljava/util/ArrayList;IIZLandroidx/fragment/app/f0$a;)V
    .locals 36
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroidx/activity/result/d;",
            "Ljava/util/ArrayList<",
            "Landroidx/fragment/app/a;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Boolean;",
            ">;IIZ",
            "Landroidx/fragment/app/f0$a;",
            ")V"
        }
    .end annotation

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move/from16 v2, p5

    move/from16 v3, p6

    new-instance v4, Landroid/util/SparseArray;

    invoke-direct {v4}, Landroid/util/SparseArray;-><init>()V

    move/from16 v5, p4

    :goto_0
    const/4 v6, 0x0

    const/4 v7, 0x1

    if-ge v5, v2, :cond_3

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroidx/fragment/app/a;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_1

    iget-object v6, v8, Landroidx/fragment/app/a;->p:Landroidx/fragment/app/q;

    iget-object v6, v6, Landroidx/fragment/app/q;->r:Landroidx/activity/result/d;

    invoke-virtual {v6}, Landroidx/activity/result/d;->h()Z

    move-result v6

    if-nez v6, :cond_0

    goto :goto_3

    :cond_0
    iget-object v6, v8, Landroidx/fragment/app/y;->a:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    sub-int/2addr v6, v7

    :goto_1
    if-ltz v6, :cond_2

    iget-object v9, v8, Landroidx/fragment/app/y;->a:Ljava/util/ArrayList;

    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroidx/fragment/app/y$a;

    invoke-static {v8, v9, v4, v7, v3}, Landroidx/fragment/app/f0;->b(Landroidx/fragment/app/a;Landroidx/fragment/app/y$a;Landroid/util/SparseArray;ZZ)V

    add-int/lit8 v6, v6, -0x1

    goto :goto_1

    :cond_1
    iget-object v7, v8, Landroidx/fragment/app/y;->a:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v9, v6

    :goto_2
    if-ge v9, v7, :cond_2

    iget-object v10, v8, Landroidx/fragment/app/y;->a:Ljava/util/ArrayList;

    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroidx/fragment/app/y$a;

    invoke-static {v8, v10, v4, v6, v3}, Landroidx/fragment/app/f0;->b(Landroidx/fragment/app/a;Landroidx/fragment/app/y$a;Landroid/util/SparseArray;ZZ)V

    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    :cond_2
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v5

    if-eqz v5, :cond_24

    new-instance v5, Landroid/view/View;

    move-object/from16 v8, p0

    invoke-direct {v5, v8}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v15

    move v14, v6

    :goto_4
    if-ge v14, v15, :cond_24

    invoke-virtual {v4, v14}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v8

    new-instance v13, Lk/a;

    invoke-direct {v13}, Lk/a;-><init>()V

    add-int/lit8 v9, v2, -0x1

    move/from16 v12, p4

    :goto_5
    if-lt v9, v12, :cond_8

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroidx/fragment/app/a;

    invoke-virtual {v10, v8}, Landroidx/fragment/app/a;->g(I)Z

    move-result v11

    if-nez v11, :cond_4

    goto :goto_8

    :cond_4
    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Boolean;

    invoke-virtual {v11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    iget-object v6, v10, Landroidx/fragment/app/y;->m:Ljava/util/ArrayList;

    if-eqz v6, :cond_7

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-eqz v11, :cond_5

    iget-object v11, v10, Landroidx/fragment/app/y;->m:Ljava/util/ArrayList;

    iget-object v10, v10, Landroidx/fragment/app/y;->n:Ljava/util/ArrayList;

    const/4 v7, 0x0

    goto :goto_6

    :cond_5
    iget-object v11, v10, Landroidx/fragment/app/y;->m:Ljava/util/ArrayList;

    iget-object v10, v10, Landroidx/fragment/app/y;->n:Ljava/util/ArrayList;

    const/4 v7, 0x0

    move-object/from16 v35, v11

    move-object v11, v10

    move-object/from16 v10, v35

    :goto_6
    if-ge v7, v6, :cond_7

    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    move-object/from16 v0, v16

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v11, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    move-object/from16 v1, v16

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v13, v1}, Lk/g;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    move-object/from16 v2, v16

    check-cast v2, Ljava/lang/String;

    if-eqz v2, :cond_6

    invoke-virtual {v13, v0, v2}, Lk/g;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_7

    :cond_6
    invoke-virtual {v13, v0, v1}, Lk/g;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_7
    add-int/lit8 v7, v7, 0x1

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move/from16 v2, p5

    goto :goto_6

    :cond_7
    :goto_8
    add-int/lit8 v9, v9, -0x1

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move/from16 v2, p5

    const/4 v6, 0x0

    const/4 v7, 0x1

    goto :goto_5

    :cond_8
    invoke-virtual {v4, v14}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/fragment/app/f0$b;

    invoke-virtual/range {p1 .. p1}, Landroidx/activity/result/d;->h()Z

    move-result v1

    if-eqz v1, :cond_22

    move-object/from16 v1, p1

    invoke-virtual {v1, v8}, Landroidx/activity/result/d;->g(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    if-nez v2, :cond_9

    goto/16 :goto_13

    :cond_9
    if-eqz v3, :cond_16

    iget-object v7, v0, Landroidx/fragment/app/f0$b;->a:Landroidx/fragment/app/f;

    iget-object v8, v0, Landroidx/fragment/app/f0$b;->d:Landroidx/fragment/app/f;

    invoke-static {v8, v7}, Landroidx/fragment/app/f0;->f(Landroidx/fragment/app/f;Landroidx/fragment/app/f;)Landroidx/fragment/app/h0;

    move-result-object v9

    if-nez v9, :cond_b

    move-object/from16 v31, v4

    move/from16 v32, v14

    :cond_a
    :goto_9
    const/4 v7, 0x0

    goto/16 :goto_d

    :cond_b
    iget-boolean v10, v0, Landroidx/fragment/app/f0$b;->b:Z

    iget-boolean v11, v0, Landroidx/fragment/app/f0$b;->e:Z

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v9, v7, v10}, Landroidx/fragment/app/f0;->h(Landroidx/fragment/app/h0;Landroidx/fragment/app/f;Z)Ljava/lang/Object;

    move-result-object v10

    invoke-static {v9, v8, v11}, Landroidx/fragment/app/f0;->i(Landroidx/fragment/app/h0;Landroidx/fragment/app/f;Z)Ljava/lang/Object;

    move-result-object v11

    iget-object v3, v0, Landroidx/fragment/app/f0$b;->a:Landroidx/fragment/app/f;

    move-object/from16 v31, v4

    iget-object v4, v0, Landroidx/fragment/app/f0$b;->d:Landroidx/fragment/app/f;

    if-nez v3, :cond_15

    if-eqz v3, :cond_10

    if-nez v4, :cond_c

    goto :goto_b

    :cond_c
    iget-boolean v12, v0, Landroidx/fragment/app/f0$b;->b:Z

    invoke-virtual {v13}, Lk/g;->isEmpty()Z

    move-result v16

    if-eqz v16, :cond_d

    move/from16 v32, v14

    const/4 v14, 0x0

    goto :goto_a

    :cond_d
    invoke-static {v9, v3, v4, v12}, Landroidx/fragment/app/f0;->j(Landroidx/fragment/app/h0;Landroidx/fragment/app/f;Landroidx/fragment/app/f;Z)Ljava/lang/Object;

    move-result-object v16

    move/from16 v32, v14

    move-object/from16 v14, v16

    :goto_a
    invoke-static {v9, v13, v14, v0}, Landroidx/fragment/app/f0;->e(Landroidx/fragment/app/h0;Lk/a;Ljava/lang/Object;Landroidx/fragment/app/f0$b;)Lk/a;

    iget-object v0, v0, Landroidx/fragment/app/f0$b;->a:Landroidx/fragment/app/f;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v13}, Lk/g;->clear()V

    invoke-virtual {v13}, Lk/g;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_f

    if-nez v10, :cond_e

    if-nez v11, :cond_e

    goto :goto_c

    :cond_e
    const/4 v0, 0x1

    const/4 v14, 0x0

    invoke-static {v3, v4, v12, v14, v0}, Landroidx/fragment/app/f0;->c(Landroidx/fragment/app/f;Landroidx/fragment/app/f;ZLk/a;Z)V

    const/16 v30, 0x0

    const/16 v28, 0x0

    new-instance v0, Landroidx/fragment/app/d0;

    const/16 v27, 0x0

    move-object/from16 v23, v0

    move-object/from16 v24, v3

    move-object/from16 v25, v4

    move/from16 v26, v12

    move-object/from16 v29, v9

    invoke-direct/range {v23 .. v30}, Landroidx/fragment/app/d0;-><init>(Landroidx/fragment/app/f;Landroidx/fragment/app/f;ZLk/a;Landroid/view/View;Landroidx/fragment/app/h0;Landroid/graphics/Rect;)V

    invoke-static {v2, v0}, Lw/q;->a(Landroid/view/View;Ljava/lang/Runnable;)Lw/q;

    goto :goto_c

    :cond_f
    invoke-virtual {v13}, Lk/a;->keySet()Ljava/util/Set;

    move-result-object v0

    const/4 v3, 0x0

    invoke-static {v1, v3, v0}, Landroidx/fragment/app/f0;->a(Ljava/util/ArrayList;Lk/a;Ljava/util/Collection;)V

    throw v3

    :cond_10
    :goto_b
    move/from16 v32, v14

    :goto_c
    const/4 v3, 0x0

    if-nez v10, :cond_11

    if-nez v11, :cond_11

    goto :goto_9

    :cond_11
    invoke-static {v9, v11, v8, v1, v5}, Landroidx/fragment/app/f0;->g(Landroidx/fragment/app/h0;Ljava/lang/Object;Landroidx/fragment/app/f;Ljava/util/ArrayList;Landroid/view/View;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v9, v10, v7, v6, v5}, Landroidx/fragment/app/f0;->g(Landroidx/fragment/app/h0;Ljava/lang/Object;Landroidx/fragment/app/f;Ljava/util/ArrayList;Landroid/view/View;)Ljava/util/ArrayList;

    move-result-object v4

    const/4 v7, 0x4

    invoke-static {v4, v7}, Landroidx/fragment/app/f0;->k(Ljava/util/ArrayList;I)V

    invoke-virtual {v9, v11, v10, v3}, Landroidx/fragment/app/h0;->g(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    if-eqz v8, :cond_13

    if-eqz v0, :cond_13

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gtz v3, :cond_12

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_13

    :cond_12
    new-instance v3, Ls/b;

    invoke-direct {v3}, Ls/b;-><init>()V

    move-object/from16 v12, p7

    check-cast v12, Landroidx/fragment/app/q$d;

    invoke-virtual {v12, v8, v3}, Landroidx/fragment/app/q$d;->b(Landroidx/fragment/app/f;Ls/b;)V

    new-instance v14, Landroidx/fragment/app/z;

    invoke-direct {v14, v12, v8, v3}, Landroidx/fragment/app/z;-><init>(Landroidx/fragment/app/f0$a;Landroidx/fragment/app/f;Ls/b;)V

    invoke-virtual {v9, v8, v7, v3, v14}, Landroidx/fragment/app/h0;->m(Landroidx/fragment/app/f;Ljava/lang/Object;Ls/b;Ljava/lang/Runnable;)V

    :cond_13
    if-eqz v7, :cond_a

    if-eqz v8, :cond_14

    if-eqz v11, :cond_14

    iget-boolean v3, v8, Landroidx/fragment/app/f;->k:Z

    if-eqz v3, :cond_14

    iget-boolean v3, v8, Landroidx/fragment/app/f;->y:Z

    if-eqz v3, :cond_14

    iget-boolean v3, v8, Landroidx/fragment/app/f;->H:Z

    if-eqz v3, :cond_14

    const/4 v3, 0x1

    invoke-virtual {v8, v3}, Landroidx/fragment/app/f;->I(Z)V

    const/4 v3, 0x0

    invoke-virtual {v9, v11, v3, v0}, Landroidx/fragment/app/h0;->k(Ljava/lang/Object;Landroid/view/View;Ljava/util/ArrayList;)V

    iget-object v3, v8, Landroidx/fragment/app/f;->D:Landroid/view/ViewGroup;

    new-instance v8, Landroidx/fragment/app/a0;

    invoke-direct {v8, v0}, Landroidx/fragment/app/a0;-><init>(Ljava/util/ArrayList;)V

    invoke-static {v3, v8}, Lw/q;->a(Landroid/view/View;Ljava/lang/Runnable;)Lw/q;

    :cond_14
    invoke-virtual {v9, v6}, Landroidx/fragment/app/h0;->h(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v20

    const/16 v29, 0x0

    move-object/from16 v23, v9

    move-object/from16 v24, v7

    move-object/from16 v25, v10

    move-object/from16 v26, v4

    move-object/from16 v27, v11

    move-object/from16 v28, v0

    move-object/from16 v30, v6

    invoke-virtual/range {v23 .. v30}, Landroidx/fragment/app/h0;->l(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;)V

    invoke-virtual {v9, v2, v7}, Landroidx/fragment/app/h0;->c(Landroid/view/ViewGroup;Ljava/lang/Object;)V

    move-object/from16 v16, v9

    move-object/from16 v17, v2

    move-object/from16 v18, v1

    move-object/from16 v19, v6

    move-object/from16 v21, v13

    invoke-virtual/range {v16 .. v21}, Landroidx/fragment/app/h0;->n(Landroid/view/View;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/Map;)V

    const/4 v7, 0x0

    invoke-static {v4, v7}, Landroidx/fragment/app/f0;->k(Ljava/util/ArrayList;I)V

    const/4 v0, 0x0

    invoke-virtual {v9, v0, v1, v6}, Landroidx/fragment/app/h0;->o(Ljava/lang/Object;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_d

    :cond_15
    const/4 v0, 0x0

    invoke-virtual {v3}, Landroidx/fragment/app/f;->E()Landroid/view/View;

    throw v0

    :cond_16
    move-object/from16 v31, v4

    move/from16 v32, v14

    const/4 v7, 0x0

    iget-object v1, v0, Landroidx/fragment/app/f0$b;->a:Landroidx/fragment/app/f;

    iget-object v3, v0, Landroidx/fragment/app/f0$b;->d:Landroidx/fragment/app/f;

    invoke-static {v3, v1}, Landroidx/fragment/app/f0;->f(Landroidx/fragment/app/f;Landroidx/fragment/app/f;)Landroidx/fragment/app/h0;

    move-result-object v4

    if-nez v4, :cond_17

    :goto_d
    goto/16 :goto_14

    :cond_17
    iget-boolean v6, v0, Landroidx/fragment/app/f0$b;->b:Z

    iget-boolean v8, v0, Landroidx/fragment/app/f0$b;->e:Z

    invoke-static {v4, v1, v6}, Landroidx/fragment/app/f0;->h(Landroidx/fragment/app/h0;Landroidx/fragment/app/f;Z)Ljava/lang/Object;

    move-result-object v6

    invoke-static {v4, v3, v8}, Landroidx/fragment/app/f0;->i(Landroidx/fragment/app/h0;Landroidx/fragment/app/f;Z)Ljava/lang/Object;

    move-result-object v14

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iget-object v10, v0, Landroidx/fragment/app/f0$b;->a:Landroidx/fragment/app/f;

    iget-object v9, v0, Landroidx/fragment/app/f0$b;->d:Landroidx/fragment/app/f;

    if-eqz v10, :cond_1a

    if-nez v9, :cond_18

    goto :goto_f

    :cond_18
    iget-boolean v8, v0, Landroidx/fragment/app/f0$b;->b:Z

    invoke-virtual {v13}, Lk/g;->isEmpty()Z

    move-result v16

    if-eqz v16, :cond_19

    const/4 v7, 0x0

    goto :goto_e

    :cond_19
    invoke-static {v4, v10, v9, v8}, Landroidx/fragment/app/f0;->j(Landroidx/fragment/app/h0;Landroidx/fragment/app/f;Landroidx/fragment/app/f;Z)Ljava/lang/Object;

    move-result-object v16

    move-object/from16 v7, v16

    :goto_e
    invoke-static {v4, v13, v7, v0}, Landroidx/fragment/app/f0;->e(Landroidx/fragment/app/h0;Lk/a;Ljava/lang/Object;Landroidx/fragment/app/f0$b;)Lk/a;

    invoke-virtual {v13}, Lk/g;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1c

    if-nez v6, :cond_1b

    if-nez v14, :cond_1b

    :cond_1a
    :goto_f
    move-object/from16 v22, v11

    move-object/from16 v23, v12

    move-object v0, v13

    move-object/from16 v33, v14

    move/from16 v34, v15

    const/4 v14, 0x0

    goto :goto_10

    :cond_1b
    move-object/from16 v16, v11

    const/4 v7, 0x1

    const/4 v11, 0x0

    invoke-static {v10, v9, v8, v11, v7}, Landroidx/fragment/app/f0;->c(Landroidx/fragment/app/f;Landroidx/fragment/app/f;ZLk/a;Z)V

    const/16 v20, 0x0

    new-instance v11, Landroidx/fragment/app/e0;

    move/from16 v17, v8

    move-object v8, v11

    move-object/from16 v18, v9

    move-object v9, v4

    move-object/from16 v19, v10

    move-object v10, v13

    move-object v7, v11

    move-object/from16 v22, v16

    const/16 v21, 0x0

    move-object/from16 v11, v21

    move-object/from16 v23, v12

    move-object v12, v0

    move-object v0, v13

    move-object/from16 v13, v22

    move-object/from16 v33, v14

    move-object v14, v5

    move/from16 v34, v15

    move-object/from16 v15, v19

    move-object/from16 v16, v18

    move-object/from16 v18, v23

    move-object/from16 v19, v6

    invoke-direct/range {v8 .. v20}, Landroidx/fragment/app/e0;-><init>(Landroidx/fragment/app/h0;Lk/a;Ljava/lang/Object;Landroidx/fragment/app/f0$b;Ljava/util/ArrayList;Landroid/view/View;Landroidx/fragment/app/f;Landroidx/fragment/app/f;ZLjava/util/ArrayList;Ljava/lang/Object;Landroid/graphics/Rect;)V

    invoke-static {v2, v7}, Lw/q;->a(Landroid/view/View;Ljava/lang/Runnable;)Lw/q;

    move-object/from16 v7, v21

    const/4 v14, 0x0

    goto :goto_11

    :cond_1c
    const/4 v14, 0x0

    throw v14

    :goto_10
    move-object v7, v14

    :goto_11
    move-object/from16 v8, v33

    if-nez v6, :cond_1d

    if-nez v8, :cond_1d

    goto/16 :goto_15

    :cond_1d
    move-object/from16 v9, v23

    invoke-static {v4, v8, v3, v9, v5}, Landroidx/fragment/app/f0;->g(Landroidx/fragment/app/h0;Ljava/lang/Object;Landroidx/fragment/app/f;Ljava/util/ArrayList;Landroid/view/View;)Ljava/util/ArrayList;

    move-result-object v15

    if-eqz v15, :cond_1f

    invoke-virtual {v15}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_1e

    goto :goto_12

    :cond_1e
    move-object v14, v8

    :cond_1f
    :goto_12
    invoke-virtual {v4, v6, v5}, Landroidx/fragment/app/h0;->a(Ljava/lang/Object;Landroid/view/View;)V

    invoke-virtual {v4, v14, v6, v7}, Landroidx/fragment/app/h0;->g(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    if-eqz v3, :cond_21

    if-eqz v15, :cond_21

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-gtz v8, :cond_20

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lez v8, :cond_21

    :cond_20
    new-instance v8, Ls/b;

    invoke-direct {v8}, Ls/b;-><init>()V

    move-object/from16 v9, p7

    check-cast v9, Landroidx/fragment/app/q$d;

    invoke-virtual {v9, v3, v8}, Landroidx/fragment/app/q$d;->b(Landroidx/fragment/app/f;Ls/b;)V

    new-instance v10, Landroidx/fragment/app/b0;

    invoke-direct {v10, v9, v3, v8}, Landroidx/fragment/app/b0;-><init>(Landroidx/fragment/app/f0$a;Landroidx/fragment/app/f;Ls/b;)V

    invoke-virtual {v4, v3, v13, v8, v10}, Landroidx/fragment/app/h0;->m(Landroidx/fragment/app/f;Ljava/lang/Object;Ls/b;Ljava/lang/Runnable;)V

    :cond_21
    if-eqz v13, :cond_23

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v23, v4

    move-object/from16 v24, v13

    move-object/from16 v25, v6

    move-object/from16 v26, v3

    move-object/from16 v27, v14

    move-object/from16 v28, v15

    move-object/from16 v29, v7

    move-object/from16 v30, v22

    invoke-virtual/range {v23 .. v30}, Landroidx/fragment/app/h0;->l(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/util/ArrayList;)V

    new-instance v7, Landroidx/fragment/app/c0;

    move-object v8, v7

    move-object v9, v6

    move-object v10, v4

    move-object v11, v5

    move-object v12, v1

    move-object v1, v13

    move-object/from16 v13, v22

    move-object v6, v14

    move-object v14, v3

    move-object/from16 v16, v6

    invoke-direct/range {v8 .. v16}, Landroidx/fragment/app/c0;-><init>(Ljava/lang/Object;Landroidx/fragment/app/h0;Landroid/view/View;Landroidx/fragment/app/f;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/Object;)V

    invoke-static {v2, v7}, Lw/q;->a(Landroid/view/View;Ljava/lang/Runnable;)Lw/q;

    new-instance v3, Landroidx/fragment/app/i0;

    move-object/from16 v6, v22

    invoke-direct {v3, v4, v6, v0}, Landroidx/fragment/app/i0;-><init>(Landroidx/fragment/app/h0;Ljava/util/ArrayList;Ljava/util/Map;)V

    invoke-static {v2, v3}, Lw/q;->a(Landroid/view/View;Ljava/lang/Runnable;)Lw/q;

    invoke-virtual {v4, v2, v1}, Landroidx/fragment/app/h0;->c(Landroid/view/ViewGroup;Ljava/lang/Object;)V

    new-instance v1, Landroidx/fragment/app/j0;

    invoke-direct {v1, v4, v6, v0}, Landroidx/fragment/app/j0;-><init>(Landroidx/fragment/app/h0;Ljava/util/ArrayList;Ljava/util/Map;)V

    invoke-static {v2, v1}, Lw/q;->a(Landroid/view/View;Ljava/lang/Runnable;)Lw/q;

    goto :goto_15

    :cond_22
    :goto_13
    move-object/from16 v31, v4

    move/from16 v32, v14

    :goto_14
    move/from16 v34, v15

    :cond_23
    :goto_15
    add-int/lit8 v14, v32, 0x1

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move/from16 v2, p5

    move/from16 v3, p6

    move-object/from16 v4, v31

    move/from16 v15, v34

    const/4 v6, 0x0

    const/4 v7, 0x1

    goto/16 :goto_4

    :cond_24
    return-void
.end method
