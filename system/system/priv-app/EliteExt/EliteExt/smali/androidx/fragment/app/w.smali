.class public Landroidx/fragment/app/w;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroidx/fragment/app/p;

.field public final b:Landroidx/fragment/app/x;

.field public final c:Landroidx/fragment/app/f;

.field public d:Z

.field public e:I


# direct methods
.method public constructor <init>(Landroidx/fragment/app/p;Landroidx/fragment/app/x;Landroidx/fragment/app/f;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroidx/fragment/app/w;->d:Z

    const/4 v0, -0x1

    iput v0, p0, Landroidx/fragment/app/w;->e:I

    iput-object p1, p0, Landroidx/fragment/app/w;->a:Landroidx/fragment/app/p;

    iput-object p2, p0, Landroidx/fragment/app/w;->b:Landroidx/fragment/app/x;

    iput-object p3, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    return-void
.end method

.method public constructor <init>(Landroidx/fragment/app/p;Landroidx/fragment/app/x;Landroidx/fragment/app/f;Landroidx/fragment/app/v;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroidx/fragment/app/w;->d:Z

    const/4 v1, -0x1

    iput v1, p0, Landroidx/fragment/app/w;->e:I

    iput-object p1, p0, Landroidx/fragment/app/w;->a:Landroidx/fragment/app/p;

    iput-object p2, p0, Landroidx/fragment/app/w;->b:Landroidx/fragment/app/x;

    iput-object p3, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    const/4 p1, 0x0

    iput-object p1, p3, Landroidx/fragment/app/f;->c:Landroid/util/SparseArray;

    iput-object p1, p3, Landroidx/fragment/app/f;->d:Landroid/os/Bundle;

    iput v0, p3, Landroidx/fragment/app/f;->q:I

    iput-boolean v0, p3, Landroidx/fragment/app/f;->n:Z

    iput-boolean v0, p3, Landroidx/fragment/app/f;->k:Z

    iget-object p2, p3, Landroidx/fragment/app/f;->g:Landroidx/fragment/app/f;

    if-eqz p2, :cond_0

    iget-object p2, p2, Landroidx/fragment/app/f;->e:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object p2, p1

    :goto_0
    iput-object p2, p3, Landroidx/fragment/app/f;->h:Ljava/lang/String;

    iput-object p1, p3, Landroidx/fragment/app/f;->g:Landroidx/fragment/app/f;

    iget-object p1, p4, Landroidx/fragment/app/v;->m:Landroid/os/Bundle;

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    :goto_1
    iput-object p1, p3, Landroidx/fragment/app/f;->b:Landroid/os/Bundle;

    return-void
.end method

.method public constructor <init>(Landroidx/fragment/app/p;Landroidx/fragment/app/x;Ljava/lang/ClassLoader;Landroidx/fragment/app/m;Landroidx/fragment/app/v;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroidx/fragment/app/w;->d:Z

    const/4 v0, -0x1

    iput v0, p0, Landroidx/fragment/app/w;->e:I

    iput-object p1, p0, Landroidx/fragment/app/w;->a:Landroidx/fragment/app/p;

    iput-object p2, p0, Landroidx/fragment/app/w;->b:Landroidx/fragment/app/x;

    iget-object p1, p5, Landroidx/fragment/app/v;->a:Ljava/lang/String;

    invoke-virtual {p4, p3, p1}, Landroidx/fragment/app/m;->a(Ljava/lang/ClassLoader;Ljava/lang/String;)Landroidx/fragment/app/f;

    move-result-object p1

    iput-object p1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object p2, p5, Landroidx/fragment/app/v;->j:Landroid/os/Bundle;

    if-eqz p2, :cond_0

    invoke-virtual {p2, p3}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    :cond_0
    iget-object p2, p5, Landroidx/fragment/app/v;->j:Landroid/os/Bundle;

    invoke-virtual {p1, p2}, Landroidx/fragment/app/f;->G(Landroid/os/Bundle;)V

    iget-object p2, p5, Landroidx/fragment/app/v;->b:Ljava/lang/String;

    iput-object p2, p1, Landroidx/fragment/app/f;->e:Ljava/lang/String;

    iget-boolean p2, p5, Landroidx/fragment/app/v;->c:Z

    iput-boolean p2, p1, Landroidx/fragment/app/f;->m:Z

    const/4 p2, 0x1

    iput-boolean p2, p1, Landroidx/fragment/app/f;->o:Z

    iget p2, p5, Landroidx/fragment/app/v;->d:I

    iput p2, p1, Landroidx/fragment/app/f;->v:I

    iget p2, p5, Landroidx/fragment/app/v;->e:I

    iput p2, p1, Landroidx/fragment/app/f;->w:I

    iget-object p2, p5, Landroidx/fragment/app/v;->f:Ljava/lang/String;

    iput-object p2, p1, Landroidx/fragment/app/f;->x:Ljava/lang/String;

    iget-boolean p2, p5, Landroidx/fragment/app/v;->g:Z

    iput-boolean p2, p1, Landroidx/fragment/app/f;->A:Z

    iget-boolean p2, p5, Landroidx/fragment/app/v;->h:Z

    iput-boolean p2, p1, Landroidx/fragment/app/f;->l:Z

    iget-boolean p2, p5, Landroidx/fragment/app/v;->i:Z

    iput-boolean p2, p1, Landroidx/fragment/app/f;->z:Z

    iget-boolean p2, p5, Landroidx/fragment/app/v;->k:Z

    iput-boolean p2, p1, Landroidx/fragment/app/f;->y:Z

    invoke-static {}, Landroidx/lifecycle/f$c;->values()[Landroidx/lifecycle/f$c;

    move-result-object p2

    iget p3, p5, Landroidx/fragment/app/v;->l:I

    aget-object p2, p2, p3

    iput-object p2, p1, Landroidx/fragment/app/f;->J:Landroidx/lifecycle/f$c;

    iget-object p2, p5, Landroidx/fragment/app/v;->m:Landroid/os/Bundle;

    if-eqz p2, :cond_1

    goto :goto_0

    :cond_1
    new-instance p2, Landroid/os/Bundle;

    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    :goto_0
    iput-object p2, p1, Landroidx/fragment/app/f;->b:Landroid/os/Bundle;

    const/4 p2, 0x2

    invoke-static {p2}, Landroidx/fragment/app/q;->K(I)Z

    move-result p2

    if-eqz p2, :cond_2

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Instantiated fragment "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "FragmentManager"

    invoke-static {p2, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    const/4 v0, 0x3

    invoke-static {v0}, Landroidx/fragment/app/q;->K(I)Z

    move-result v1

    const-string v2, "FragmentManager"

    if-eqz v1, :cond_0

    const-string v1, "moveto ACTIVITY_CREATED: "

    invoke-static {v1}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v3, v1, Landroidx/fragment/app/f;->b:Landroid/os/Bundle;

    iget-object v3, v1, Landroidx/fragment/app/f;->t:Landroidx/fragment/app/q;

    invoke-virtual {v3}, Landroidx/fragment/app/q;->R()V

    iput v0, v1, Landroidx/fragment/app/f;->a:I

    const/4 v3, 0x0

    iput-boolean v3, v1, Landroidx/fragment/app/f;->C:Z

    const/4 v4, 0x1

    iput-boolean v4, v1, Landroidx/fragment/app/f;->C:Z

    invoke-static {v0}, Landroidx/fragment/app/q;->K(I)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "moveto RESTORE_VIEW_STATE: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v0, 0x0

    iput-object v0, v1, Landroidx/fragment/app/f;->b:Landroid/os/Bundle;

    iget-object v0, v1, Landroidx/fragment/app/f;->t:Landroidx/fragment/app/q;

    iput-boolean v3, v0, Landroidx/fragment/app/q;->B:Z

    iput-boolean v3, v0, Landroidx/fragment/app/q;->C:Z

    iget-object v1, v0, Landroidx/fragment/app/q;->J:Landroidx/fragment/app/t;

    iput-boolean v3, v1, Landroidx/fragment/app/t;->g:Z

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroidx/fragment/app/q;->w(I)V

    iget-object v0, p0, Landroidx/fragment/app/w;->a:Landroidx/fragment/app/p;

    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v2, v1, Landroidx/fragment/app/f;->b:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2, v3}, Landroidx/fragment/app/p;->a(Landroidx/fragment/app/f;Landroid/os/Bundle;Z)V

    return-void
.end method

.method public b()V
    .locals 7

    const/4 v0, 0x3

    invoke-static {v0}, Landroidx/fragment/app/q;->K(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "moveto ATTACHED: "

    invoke-static {v0}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FragmentManager"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v1, v0, Landroidx/fragment/app/f;->g:Landroidx/fragment/app/f;

    const-string v2, " that does not belong to this FragmentManager!"

    const-string v3, " declared target fragment "

    const-string v4, "Fragment "

    const/4 v5, 0x0

    if-eqz v1, :cond_2

    iget-object v0, p0, Landroidx/fragment/app/w;->b:Landroidx/fragment/app/x;

    iget-object v1, v1, Landroidx/fragment/app/f;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroidx/fragment/app/x;->h(Ljava/lang/String;)Landroidx/fragment/app/w;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v2, v1, Landroidx/fragment/app/f;->g:Landroidx/fragment/app/f;

    iget-object v2, v2, Landroidx/fragment/app/f;->e:Ljava/lang/String;

    iput-object v2, v1, Landroidx/fragment/app/f;->h:Ljava/lang/String;

    iput-object v5, v1, Landroidx/fragment/app/f;->g:Landroidx/fragment/app/f;

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v4}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v3, v3, Landroidx/fragment/app/f;->g:Landroidx/fragment/app/f;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, v0, Landroidx/fragment/app/f;->h:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v1, p0, Landroidx/fragment/app/w;->b:Landroidx/fragment/app/x;

    invoke-virtual {v1, v0}, Landroidx/fragment/app/x;->h(Ljava/lang/String;)Landroidx/fragment/app/w;

    move-result-object v0

    if-eqz v0, :cond_3

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v4}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v3, v3, Landroidx/fragment/app/f;->h:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move-object v0, v5

    :goto_0
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Landroidx/fragment/app/w;->j()V

    :cond_5
    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v1, v0, Landroidx/fragment/app/f;->r:Landroidx/fragment/app/q;

    iget-object v2, v1, Landroidx/fragment/app/q;->q:Landroidx/fragment/app/n;

    iput-object v2, v0, Landroidx/fragment/app/f;->s:Landroidx/fragment/app/n;

    iget-object v1, v1, Landroidx/fragment/app/q;->s:Landroidx/fragment/app/f;

    iput-object v1, v0, Landroidx/fragment/app/f;->u:Landroidx/fragment/app/f;

    iget-object v1, p0, Landroidx/fragment/app/w;->a:Landroidx/fragment/app/p;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroidx/fragment/app/p;->g(Landroidx/fragment/app/f;Z)V

    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v1, v0, Landroidx/fragment/app/f;->O:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroidx/fragment/app/f$c;

    invoke-virtual {v3}, Landroidx/fragment/app/f$c;->a()V

    goto :goto_1

    :cond_6
    iget-object v1, v0, Landroidx/fragment/app/f;->O:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v1, v0, Landroidx/fragment/app/f;->t:Landroidx/fragment/app/q;

    iget-object v3, v0, Landroidx/fragment/app/f;->s:Landroidx/fragment/app/n;

    new-instance v6, Landroidx/fragment/app/e;

    invoke-direct {v6, v0}, Landroidx/fragment/app/e;-><init>(Landroidx/fragment/app/f;)V

    invoke-virtual {v1, v3, v6, v0}, Landroidx/fragment/app/q;->b(Landroidx/fragment/app/n;Landroidx/activity/result/d;Landroidx/fragment/app/f;)V

    iput v2, v0, Landroidx/fragment/app/f;->a:I

    iput-boolean v2, v0, Landroidx/fragment/app/f;->C:Z

    iget-object v1, v0, Landroidx/fragment/app/f;->s:Landroidx/fragment/app/n;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroidx/fragment/app/f;->C:Z

    iget-object v3, v0, Landroidx/fragment/app/f;->s:Landroidx/fragment/app/n;

    if-nez v3, :cond_7

    goto :goto_2

    :cond_7
    iget-object v5, v3, Landroidx/fragment/app/n;->a:Landroid/app/Activity;

    :goto_2
    if-eqz v5, :cond_8

    iput-boolean v2, v0, Landroidx/fragment/app/f;->C:Z

    iput-boolean v1, v0, Landroidx/fragment/app/f;->C:Z

    :cond_8
    iget-boolean v1, v0, Landroidx/fragment/app/f;->C:Z

    if-eqz v1, :cond_a

    iget-object v1, v0, Landroidx/fragment/app/f;->r:Landroidx/fragment/app/q;

    iget-object v3, v1, Landroidx/fragment/app/q;->o:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroidx/fragment/app/u;

    invoke-interface {v4, v1, v0}, Landroidx/fragment/app/u;->b(Landroidx/fragment/app/q;Landroidx/fragment/app/f;)V

    goto :goto_3

    :cond_9
    iget-object v0, v0, Landroidx/fragment/app/f;->t:Landroidx/fragment/app/q;

    iput-boolean v2, v0, Landroidx/fragment/app/q;->B:Z

    iput-boolean v2, v0, Landroidx/fragment/app/q;->C:Z

    iget-object v1, v0, Landroidx/fragment/app/q;->J:Landroidx/fragment/app/t;

    iput-boolean v2, v1, Landroidx/fragment/app/t;->g:Z

    invoke-virtual {v0, v2}, Landroidx/fragment/app/q;->w(I)V

    iget-object v0, p0, Landroidx/fragment/app/w;->a:Landroidx/fragment/app/p;

    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v0, v1, v2}, Landroidx/fragment/app/p;->b(Landroidx/fragment/app/f;Z)V

    return-void

    :cond_a
    new-instance v1, Landroidx/fragment/app/p0;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " did not call through to super.onAttach()"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroidx/fragment/app/p0;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public c()I
    .locals 13

    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v1, v0, Landroidx/fragment/app/f;->r:Landroidx/fragment/app/q;

    if-nez v1, :cond_0

    iget v0, v0, Landroidx/fragment/app/f;->a:I

    return v0

    :cond_0
    iget v1, p0, Landroidx/fragment/app/w;->e:I

    iget-object v0, v0, Landroidx/fragment/app/f;->J:Landroidx/lifecycle/f$c;

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    const/4 v2, -0x1

    const/4 v3, 0x5

    const/4 v4, 0x4

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x0

    if-eq v0, v5, :cond_3

    if-eq v0, v6, :cond_2

    if-eq v0, v7, :cond_1

    if-eq v0, v4, :cond_4

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_0

    :cond_1
    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_0

    :cond_2
    invoke-static {v1, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_0

    :cond_3
    invoke-static {v1, v8}, Ljava/lang/Math;->min(II)I

    move-result v1

    :cond_4
    :goto_0
    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-boolean v9, v0, Landroidx/fragment/app/f;->m:Z

    if-eqz v9, :cond_7

    iget-boolean v9, v0, Landroidx/fragment/app/f;->n:Z

    if-eqz v9, :cond_5

    iget v0, p0, Landroidx/fragment/app/w;->e:I

    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_5
    iget v9, p0, Landroidx/fragment/app/w;->e:I

    if-ge v9, v4, :cond_6

    iget v0, v0, Landroidx/fragment/app/f;->a:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_1

    :cond_6
    invoke-static {v1, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    :cond_7
    :goto_1
    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-boolean v0, v0, Landroidx/fragment/app/f;->k:Z

    if-nez v0, :cond_8

    invoke-static {v1, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    :cond_8
    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v9, v0, Landroidx/fragment/app/f;->D:Landroid/view/ViewGroup;

    const/4 v10, 0x0

    if-eqz v9, :cond_d

    invoke-virtual {v0}, Landroidx/fragment/app/f;->o()Landroidx/fragment/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/q;->I()Landroidx/fragment/app/o0;

    move-result-object v0

    invoke-static {v9, v0}, Landroidx/fragment/app/n0;->e(Landroid/view/ViewGroup;Landroidx/fragment/app/o0;)Landroidx/fragment/app/n0;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v9, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v0, v9}, Landroidx/fragment/app/n0;->c(Landroidx/fragment/app/f;)Landroidx/fragment/app/n0$a;

    move-result-object v9

    if-eqz v9, :cond_9

    iget v8, v9, Landroidx/fragment/app/n0$a;->b:I

    :cond_9
    iget-object v9, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v0, v0, Landroidx/fragment/app/n0;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_b

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroidx/fragment/app/n0$a;

    iget-object v12, v11, Landroidx/fragment/app/n0$a;->c:Landroidx/fragment/app/f;

    invoke-virtual {v12, v9}, Landroidx/fragment/app/f;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    iget-boolean v12, v11, Landroidx/fragment/app/n0$a;->f:Z

    if-nez v12, :cond_a

    move-object v10, v11

    :cond_b
    if-eqz v10, :cond_d

    if-eqz v8, :cond_c

    if-ne v8, v5, :cond_d

    :cond_c
    iget v8, v10, Landroidx/fragment/app/n0$a;->b:I

    :cond_d
    if-ne v8, v6, :cond_e

    const/4 v0, 0x6

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_2

    :cond_e
    if-ne v8, v7, :cond_f

    invoke-static {v1, v7}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_2

    :cond_f
    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-boolean v7, v0, Landroidx/fragment/app/f;->l:Z

    if-eqz v7, :cond_11

    invoke-virtual {v0}, Landroidx/fragment/app/f;->w()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-static {v1, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_2

    :cond_10
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    :cond_11
    :goto_2
    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-boolean v2, v0, Landroidx/fragment/app/f;->E:Z

    if-eqz v2, :cond_12

    iget v0, v0, Landroidx/fragment/app/f;->a:I

    if-ge v0, v3, :cond_12

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    :cond_12
    invoke-static {v6}, Landroidx/fragment/app/q;->K(I)Z

    move-result v0

    if-eqz v0, :cond_13

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "computeExpectedState() of "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " for "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "FragmentManager"

    invoke-static {v2, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_13
    return v1
.end method

.method public d()V
    .locals 7

    const/4 v0, 0x3

    invoke-static {v0}, Landroidx/fragment/app/q;->K(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "moveto CREATED: "

    invoke-static {v0}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FragmentManager"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-boolean v1, v0, Landroidx/fragment/app/f;->I:Z

    const/4 v2, 0x1

    const-string v3, "android:support:fragments"

    if-nez v1, :cond_5

    iget-object v1, p0, Landroidx/fragment/app/w;->a:Landroidx/fragment/app/p;

    iget-object v4, v0, Landroidx/fragment/app/f;->b:Landroid/os/Bundle;

    const/4 v5, 0x0

    invoke-virtual {v1, v0, v4, v5}, Landroidx/fragment/app/p;->h(Landroidx/fragment/app/f;Landroid/os/Bundle;Z)V

    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v1, v0, Landroidx/fragment/app/f;->b:Landroid/os/Bundle;

    iget-object v4, v0, Landroidx/fragment/app/f;->t:Landroidx/fragment/app/q;

    invoke-virtual {v4}, Landroidx/fragment/app/q;->R()V

    iput v2, v0, Landroidx/fragment/app/f;->a:I

    iput-boolean v5, v0, Landroidx/fragment/app/f;->C:Z

    iget-object v4, v0, Landroidx/fragment/app/f;->K:Landroidx/lifecycle/k;

    new-instance v6, Landroidx/fragment/app/Fragment$5;

    invoke-direct {v6, v0}, Landroidx/fragment/app/Fragment$5;-><init>(Landroidx/fragment/app/f;)V

    invoke-virtual {v4, v6}, Landroidx/lifecycle/k;->a(Landroidx/lifecycle/i;)V

    iget-object v4, v0, Landroidx/fragment/app/f;->N:Landroidx/savedstate/b;

    invoke-virtual {v4, v1}, Landroidx/savedstate/b;->a(Landroid/os/Bundle;)V

    iput-boolean v2, v0, Landroidx/fragment/app/f;->C:Z

    if-eqz v1, :cond_1

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v3, v0, Landroidx/fragment/app/f;->t:Landroidx/fragment/app/q;

    invoke-virtual {v3, v1}, Landroidx/fragment/app/q;->V(Landroid/os/Parcelable;)V

    iget-object v1, v0, Landroidx/fragment/app/f;->t:Landroidx/fragment/app/q;

    invoke-virtual {v1}, Landroidx/fragment/app/q;->m()V

    :cond_1
    iget-object v1, v0, Landroidx/fragment/app/f;->t:Landroidx/fragment/app/q;

    iget v3, v1, Landroidx/fragment/app/q;->p:I

    if-lt v3, v2, :cond_2

    move v3, v2

    goto :goto_0

    :cond_2
    move v3, v5

    :goto_0
    if-nez v3, :cond_3

    invoke-virtual {v1}, Landroidx/fragment/app/q;->m()V

    :cond_3
    iput-boolean v2, v0, Landroidx/fragment/app/f;->I:Z

    iget-boolean v1, v0, Landroidx/fragment/app/f;->C:Z

    if-eqz v1, :cond_4

    iget-object v0, v0, Landroidx/fragment/app/f;->K:Landroidx/lifecycle/k;

    sget-object v1, Landroidx/lifecycle/f$b;->ON_CREATE:Landroidx/lifecycle/f$b;

    invoke-virtual {v0, v1}, Landroidx/lifecycle/k;->e(Landroidx/lifecycle/f$b;)V

    iget-object v0, p0, Landroidx/fragment/app/w;->a:Landroidx/fragment/app/p;

    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v2, v1, Landroidx/fragment/app/f;->b:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2, v5}, Landroidx/fragment/app/p;->c(Landroidx/fragment/app/f;Landroid/os/Bundle;Z)V

    goto :goto_1

    :cond_4
    new-instance v1, Landroidx/fragment/app/p0;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Fragment "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " did not call through to super.onCreate()"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroidx/fragment/app/p0;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    iget-object v1, v0, Landroidx/fragment/app/f;->b:Landroid/os/Bundle;

    if-eqz v1, :cond_6

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    if-eqz v1, :cond_6

    iget-object v3, v0, Landroidx/fragment/app/f;->t:Landroidx/fragment/app/q;

    invoke-virtual {v3, v1}, Landroidx/fragment/app/q;->V(Landroid/os/Parcelable;)V

    iget-object v0, v0, Landroidx/fragment/app/f;->t:Landroidx/fragment/app/q;

    invoke-virtual {v0}, Landroidx/fragment/app/q;->m()V

    :cond_6
    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iput v2, v0, Landroidx/fragment/app/f;->a:I

    :goto_1
    return-void
.end method

.method public e()V
    .locals 5

    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-boolean v0, v0, Landroidx/fragment/app/f;->m:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x3

    invoke-static {v0}, Landroidx/fragment/app/q;->K(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "moveto CREATE_VIEW: "

    invoke-static {v0}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FragmentManager"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v1, v0, Landroidx/fragment/app/f;->b:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroidx/fragment/app/f;->B(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v2, v1, Landroidx/fragment/app/f;->D:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    goto/16 :goto_2

    :cond_2
    iget v2, v1, Landroidx/fragment/app/f;->w:I

    if-eqz v2, :cond_6

    const/4 v4, -0x1

    if-eq v2, v4, :cond_5

    iget-object v1, v1, Landroidx/fragment/app/f;->r:Landroidx/fragment/app/q;

    iget-object v1, v1, Landroidx/fragment/app/q;->r:Landroidx/activity/result/d;

    invoke-virtual {v1, v2}, Landroidx/activity/result/d;->g(I)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Landroid/view/ViewGroup;

    if-nez v2, :cond_7

    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-boolean v4, v1, Landroidx/fragment/app/f;->o:Z

    if-nez v4, :cond_7

    :try_start_0
    iget-object v0, v1, Landroidx/fragment/app/f;->s:Landroidx/fragment/app/n;

    if-nez v0, :cond_3

    goto :goto_0

    :cond_3
    iget-object v3, v0, Landroidx/fragment/app/n;->b:Landroid/content/Context;

    :goto_0
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget v1, v1, Landroidx/fragment/app/f;->w:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Fragment "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " not attached to a context."

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const-string v0, "unknown"

    :goto_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "No view found for id 0x"

    invoke-static {v2}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget v3, v3, Landroidx/fragment/app/f;->w:I

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ") for fragment "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot create fragment "

    invoke-static {v1}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " for a container view with no id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    move-object v2, v3

    :cond_7
    :goto_2
    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iput-object v2, v1, Landroidx/fragment/app/f;->D:Landroid/view/ViewGroup;

    iget-object v3, v1, Landroidx/fragment/app/f;->b:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v2, v3}, Landroidx/fragment/app/f;->z(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)V

    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x2

    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iput v0, v1, Landroidx/fragment/app/f;->a:I

    return-void
.end method

.method public f()V
    .locals 7

    const/4 v0, 0x3

    invoke-static {v0}, Landroidx/fragment/app/q;->K(I)Z

    move-result v1

    const-string v2, "FragmentManager"

    if-eqz v1, :cond_0

    const-string v1, "movefrom CREATED: "

    invoke-static {v1}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-boolean v3, v1, Landroidx/fragment/app/f;->l:Z

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Landroidx/fragment/app/f;->w()Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v4

    goto :goto_0

    :cond_1
    move v1, v5

    :goto_0
    if-nez v1, :cond_3

    iget-object v3, p0, Landroidx/fragment/app/w;->b:Landroidx/fragment/app/x;

    iget-object v3, v3, Landroidx/fragment/app/x;->c:Landroidx/fragment/app/t;

    iget-object v6, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v3, v6}, Landroidx/fragment/app/t;->c(Landroidx/fragment/app/f;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    :cond_2
    move v3, v5

    goto :goto_2

    :cond_3
    :goto_1
    move v3, v4

    :goto_2
    if-eqz v3, :cond_d

    iget-object v3, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v3, v3, Landroidx/fragment/app/f;->s:Landroidx/fragment/app/n;

    instance-of v6, v3, Landroidx/lifecycle/y;

    if-eqz v6, :cond_4

    iget-object v3, p0, Landroidx/fragment/app/w;->b:Landroidx/fragment/app/x;

    iget-object v3, v3, Landroidx/fragment/app/x;->c:Landroidx/fragment/app/t;

    iget-boolean v3, v3, Landroidx/fragment/app/t;->f:Z

    goto :goto_3

    :cond_4
    iget-object v3, v3, Landroidx/fragment/app/n;->b:Landroid/content/Context;

    instance-of v6, v3, Landroid/app/Activity;

    if-eqz v6, :cond_5

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v3

    xor-int/2addr v3, v4

    goto :goto_3

    :cond_5
    move v3, v4

    :goto_3
    if-nez v1, :cond_6

    if-eqz v3, :cond_9

    :cond_6
    iget-object v1, p0, Landroidx/fragment/app/w;->b:Landroidx/fragment/app/x;

    iget-object v1, v1, Landroidx/fragment/app/x;->c:Landroidx/fragment/app/t;

    iget-object v3, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Landroidx/fragment/app/q;->K(I)Z

    move-result v0

    if-eqz v0, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Clearing non-config state for "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    iget-object v0, v1, Landroidx/fragment/app/t;->c:Ljava/util/HashMap;

    iget-object v2, v3, Landroidx/fragment/app/f;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/fragment/app/t;

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Landroidx/fragment/app/t;->a()V

    iget-object v0, v1, Landroidx/fragment/app/t;->c:Ljava/util/HashMap;

    iget-object v2, v3, Landroidx/fragment/app/f;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_8
    iget-object v0, v1, Landroidx/fragment/app/t;->d:Ljava/util/HashMap;

    iget-object v2, v3, Landroidx/fragment/app/f;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/lifecycle/x;

    if-eqz v0, :cond_9

    invoke-virtual {v0}, Landroidx/lifecycle/x;->a()V

    iget-object v0, v1, Landroidx/fragment/app/t;->d:Ljava/util/HashMap;

    iget-object v1, v3, Landroidx/fragment/app/f;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_9
    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v1, v0, Landroidx/fragment/app/f;->t:Landroidx/fragment/app/q;

    invoke-virtual {v1}, Landroidx/fragment/app/q;->o()V

    iget-object v1, v0, Landroidx/fragment/app/f;->K:Landroidx/lifecycle/k;

    sget-object v2, Landroidx/lifecycle/f$b;->ON_DESTROY:Landroidx/lifecycle/f$b;

    invoke-virtual {v1, v2}, Landroidx/lifecycle/k;->e(Landroidx/lifecycle/f$b;)V

    iput v5, v0, Landroidx/fragment/app/f;->a:I

    iput-boolean v5, v0, Landroidx/fragment/app/f;->C:Z

    iput-boolean v5, v0, Landroidx/fragment/app/f;->I:Z

    iput-boolean v4, v0, Landroidx/fragment/app/f;->C:Z

    iget-object v0, p0, Landroidx/fragment/app/w;->a:Landroidx/fragment/app/p;

    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v0, v1, v5}, Landroidx/fragment/app/p;->d(Landroidx/fragment/app/f;Z)V

    iget-object v0, p0, Landroidx/fragment/app/w;->b:Landroidx/fragment/app/x;

    invoke-virtual {v0}, Landroidx/fragment/app/x;->f()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_a
    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroidx/fragment/app/w;

    if-eqz v1, :cond_a

    iget-object v1, v1, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v2, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v2, v2, Landroidx/fragment/app/f;->e:Ljava/lang/String;

    iget-object v3, v1, Landroidx/fragment/app/f;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    iget-object v2, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iput-object v2, v1, Landroidx/fragment/app/f;->g:Landroidx/fragment/app/f;

    const/4 v2, 0x0

    iput-object v2, v1, Landroidx/fragment/app/f;->h:Ljava/lang/String;

    goto :goto_4

    :cond_b
    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v1, v0, Landroidx/fragment/app/f;->h:Ljava/lang/String;

    if-eqz v1, :cond_c

    iget-object v2, p0, Landroidx/fragment/app/w;->b:Landroidx/fragment/app/x;

    invoke-virtual {v2, v1}, Landroidx/fragment/app/x;->d(Ljava/lang/String;)Landroidx/fragment/app/f;

    move-result-object v1

    iput-object v1, v0, Landroidx/fragment/app/f;->g:Landroidx/fragment/app/f;

    :cond_c
    iget-object v0, p0, Landroidx/fragment/app/w;->b:Landroidx/fragment/app/x;

    invoke-virtual {v0, p0}, Landroidx/fragment/app/x;->k(Landroidx/fragment/app/w;)V

    goto :goto_5

    :cond_d
    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v0, v0, Landroidx/fragment/app/f;->h:Ljava/lang/String;

    if-eqz v0, :cond_e

    iget-object v1, p0, Landroidx/fragment/app/w;->b:Landroidx/fragment/app/x;

    invoke-virtual {v1, v0}, Landroidx/fragment/app/x;->d(Ljava/lang/String;)Landroidx/fragment/app/f;

    move-result-object v0

    if-eqz v0, :cond_e

    iget-boolean v1, v0, Landroidx/fragment/app/f;->A:Z

    if-eqz v1, :cond_e

    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iput-object v0, v1, Landroidx/fragment/app/f;->g:Landroidx/fragment/app/f;

    :cond_e
    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iput v5, v0, Landroidx/fragment/app/f;->a:I

    :goto_5
    return-void
.end method

.method public g()V
    .locals 3

    const/4 v0, 0x3

    invoke-static {v0}, Landroidx/fragment/app/q;->K(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "movefrom CREATE_VIEW: "

    invoke-static {v0}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FragmentManager"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v1, v0, Landroidx/fragment/app/f;->D:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroidx/fragment/app/f;->A()V

    iget-object v0, p0, Landroidx/fragment/app/w;->a:Landroidx/fragment/app/p;

    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroidx/fragment/app/p;->m(Landroidx/fragment/app/f;Z)V

    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    const/4 v1, 0x0

    iput-object v1, v0, Landroidx/fragment/app/f;->D:Landroid/view/ViewGroup;

    iput-object v1, v0, Landroidx/fragment/app/f;->L:Landroidx/fragment/app/k0;

    iget-object v0, v0, Landroidx/fragment/app/f;->M:Landroidx/lifecycle/n;

    invoke-virtual {v0, v1}, Landroidx/lifecycle/n;->h(Ljava/lang/Object;)V

    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iput-boolean v2, v0, Landroidx/fragment/app/f;->n:Z

    return-void
.end method

.method public h()V
    .locals 9

    const/4 v0, 0x3

    invoke-static {v0}, Landroidx/fragment/app/q;->K(I)Z

    move-result v1

    const-string v2, "FragmentManager"

    if-eqz v1, :cond_0

    const-string v1, "movefrom ATTACHED: "

    invoke-static {v1}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    const/4 v3, -0x1

    iput v3, v1, Landroidx/fragment/app/f;->a:I

    const/4 v4, 0x0

    iput-boolean v4, v1, Landroidx/fragment/app/f;->C:Z

    const/4 v5, 0x1

    iput-boolean v5, v1, Landroidx/fragment/app/f;->C:Z

    const/4 v6, 0x0

    iget-object v7, v1, Landroidx/fragment/app/f;->t:Landroidx/fragment/app/q;

    iget-boolean v8, v7, Landroidx/fragment/app/q;->D:Z

    if-nez v8, :cond_1

    invoke-virtual {v7}, Landroidx/fragment/app/q;->o()V

    new-instance v7, Landroidx/fragment/app/r;

    invoke-direct {v7}, Landroidx/fragment/app/r;-><init>()V

    iput-object v7, v1, Landroidx/fragment/app/f;->t:Landroidx/fragment/app/q;

    :cond_1
    iget-object v1, p0, Landroidx/fragment/app/w;->a:Landroidx/fragment/app/p;

    iget-object v7, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v1, v7, v4}, Landroidx/fragment/app/p;->e(Landroidx/fragment/app/f;Z)V

    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iput v3, v1, Landroidx/fragment/app/f;->a:I

    iput-object v6, v1, Landroidx/fragment/app/f;->s:Landroidx/fragment/app/n;

    iput-object v6, v1, Landroidx/fragment/app/f;->u:Landroidx/fragment/app/f;

    iput-object v6, v1, Landroidx/fragment/app/f;->r:Landroidx/fragment/app/q;

    iget-boolean v3, v1, Landroidx/fragment/app/f;->l:Z

    if-eqz v3, :cond_2

    invoke-virtual {v1}, Landroidx/fragment/app/f;->w()Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    :cond_2
    move v5, v4

    :goto_0
    if-nez v5, :cond_3

    iget-object v1, p0, Landroidx/fragment/app/w;->b:Landroidx/fragment/app/x;

    iget-object v1, v1, Landroidx/fragment/app/x;->c:Landroidx/fragment/app/t;

    iget-object v3, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v1, v3}, Landroidx/fragment/app/t;->c(Landroidx/fragment/app/f;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_3
    invoke-static {v0}, Landroidx/fragment/app/q;->K(I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "initState called for fragment: "

    invoke-static {v0}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Landroidx/lifecycle/k;

    invoke-direct {v1, v0}, Landroidx/lifecycle/k;-><init>(Landroidx/lifecycle/j;)V

    iput-object v1, v0, Landroidx/fragment/app/f;->K:Landroidx/lifecycle/k;

    new-instance v1, Landroidx/savedstate/b;

    invoke-direct {v1, v0}, Landroidx/savedstate/b;-><init>(Landroidx/savedstate/c;)V

    iput-object v1, v0, Landroidx/fragment/app/f;->N:Landroidx/savedstate/b;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroidx/fragment/app/f;->e:Ljava/lang/String;

    iput-boolean v4, v0, Landroidx/fragment/app/f;->k:Z

    iput-boolean v4, v0, Landroidx/fragment/app/f;->l:Z

    iput-boolean v4, v0, Landroidx/fragment/app/f;->m:Z

    iput-boolean v4, v0, Landroidx/fragment/app/f;->n:Z

    iput-boolean v4, v0, Landroidx/fragment/app/f;->o:Z

    iput v4, v0, Landroidx/fragment/app/f;->q:I

    iput-object v6, v0, Landroidx/fragment/app/f;->r:Landroidx/fragment/app/q;

    new-instance v1, Landroidx/fragment/app/r;

    invoke-direct {v1}, Landroidx/fragment/app/r;-><init>()V

    iput-object v1, v0, Landroidx/fragment/app/f;->t:Landroidx/fragment/app/q;

    iput-object v6, v0, Landroidx/fragment/app/f;->s:Landroidx/fragment/app/n;

    iput v4, v0, Landroidx/fragment/app/f;->v:I

    iput v4, v0, Landroidx/fragment/app/f;->w:I

    iput-object v6, v0, Landroidx/fragment/app/f;->x:Ljava/lang/String;

    iput-boolean v4, v0, Landroidx/fragment/app/f;->y:Z

    iput-boolean v4, v0, Landroidx/fragment/app/f;->z:Z

    :cond_5
    return-void
.end method

.method public i()V
    .locals 4

    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-boolean v1, v0, Landroidx/fragment/app/f;->m:Z

    if-eqz v1, :cond_1

    iget-boolean v1, v0, Landroidx/fragment/app/f;->n:Z

    if-eqz v1, :cond_1

    iget-boolean v0, v0, Landroidx/fragment/app/f;->p:Z

    if-nez v0, :cond_1

    const/4 v0, 0x3

    invoke-static {v0}, Landroidx/fragment/app/q;->K(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "moveto CREATE_VIEW: "

    invoke-static {v0}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FragmentManager"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v1, v0, Landroidx/fragment/app/f;->b:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroidx/fragment/app/f;->B(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v3, v3, Landroidx/fragment/app/f;->b:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2, v3}, Landroidx/fragment/app/f;->z(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)V

    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method public j()V
    .locals 7

    iget-boolean v0, p0, Landroidx/fragment/app/w;->d:Z

    const-string v1, "FragmentManager"

    const/4 v2, 0x2

    if-eqz v0, :cond_1

    invoke-static {v2}, Landroidx/fragment/app/q;->K(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Ignoring re-entrant call to moveToExpectedState() for "

    invoke-static {v0}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    const/4 v3, 0x1

    :try_start_0
    iput-boolean v3, p0, Landroidx/fragment/app/w;->d:Z

    :goto_0
    invoke-virtual {p0}, Landroidx/fragment/app/w;->c()I

    move-result v4

    iget-object v5, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget v6, v5, Landroidx/fragment/app/f;->a:I

    if-eq v4, v6, :cond_4

    if-le v4, v6, :cond_2

    add-int/lit8 v6, v6, 0x1

    packed-switch v6, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Landroidx/fragment/app/w;->m()V

    goto :goto_0

    :pswitch_1
    const/4 v4, 0x6

    iput v4, v5, Landroidx/fragment/app/f;->a:I

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Landroidx/fragment/app/w;->n()V

    goto :goto_0

    :pswitch_3
    const/4 v4, 0x4

    iput v4, v5, Landroidx/fragment/app/f;->a:I

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0}, Landroidx/fragment/app/w;->a()V

    goto :goto_0

    :pswitch_5
    invoke-virtual {p0}, Landroidx/fragment/app/w;->i()V

    invoke-virtual {p0}, Landroidx/fragment/app/w;->e()V

    goto :goto_0

    :pswitch_6
    invoke-virtual {p0}, Landroidx/fragment/app/w;->d()V

    goto :goto_0

    :pswitch_7
    invoke-virtual {p0}, Landroidx/fragment/app/w;->b()V

    goto :goto_0

    :cond_2
    add-int/lit8 v6, v6, -0x1

    packed-switch v6, :pswitch_data_1

    goto :goto_0

    :pswitch_8
    invoke-virtual {p0}, Landroidx/fragment/app/w;->k()V

    goto :goto_0

    :pswitch_9
    const/4 v4, 0x5

    iput v4, v5, Landroidx/fragment/app/f;->a:I

    goto :goto_0

    :pswitch_a
    invoke-virtual {p0}, Landroidx/fragment/app/w;->o()V

    goto :goto_0

    :pswitch_b
    const/4 v4, 0x3

    invoke-static {v4}, Landroidx/fragment/app/q;->K(I)Z

    move-result v5

    if-eqz v5, :cond_3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "movefrom ACTIVITY_CREATED: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v5, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v5, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v5, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iput v4, v5, Landroidx/fragment/app/f;->a:I

    goto :goto_0

    :pswitch_c
    iput-boolean v0, v5, Landroidx/fragment/app/f;->n:Z

    iput v2, v5, Landroidx/fragment/app/f;->a:I

    goto :goto_0

    :pswitch_d
    invoke-virtual {p0}, Landroidx/fragment/app/w;->g()V

    iget-object v4, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iput v3, v4, Landroidx/fragment/app/f;->a:I

    goto :goto_0

    :pswitch_e
    invoke-virtual {p0}, Landroidx/fragment/app/w;->f()V

    goto/16 :goto_0

    :pswitch_f
    invoke-virtual {p0}, Landroidx/fragment/app/w;->h()V

    goto/16 :goto_0

    :cond_4
    iget-boolean v1, v5, Landroidx/fragment/app/f;->H:Z

    if-eqz v1, :cond_6

    iget-object v1, v5, Landroidx/fragment/app/f;->r:Landroidx/fragment/app/q;

    if-eqz v1, :cond_5

    iget-boolean v2, v5, Landroidx/fragment/app/f;->k:Z

    if-eqz v2, :cond_5

    invoke-virtual {v1, v5}, Landroidx/fragment/app/q;->L(Landroidx/fragment/app/f;)Z

    move-result v2

    if-eqz v2, :cond_5

    iput-boolean v3, v1, Landroidx/fragment/app/q;->A:Z

    :cond_5
    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iput-boolean v0, v1, Landroidx/fragment/app/f;->H:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_6
    iput-boolean v0, p0, Landroidx/fragment/app/w;->d:Z

    return-void

    :catchall_0
    move-exception v1

    iput-boolean v0, p0, Landroidx/fragment/app/w;->d:Z

    throw v1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
    .end packed-switch
.end method

.method public k()V
    .locals 3

    const/4 v0, 0x3

    invoke-static {v0}, Landroidx/fragment/app/q;->K(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "movefrom RESUMED: "

    invoke-static {v0}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FragmentManager"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v1, v0, Landroidx/fragment/app/f;->t:Landroidx/fragment/app/q;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroidx/fragment/app/q;->w(I)V

    iget-object v1, v0, Landroidx/fragment/app/f;->K:Landroidx/lifecycle/k;

    sget-object v2, Landroidx/lifecycle/f$b;->ON_PAUSE:Landroidx/lifecycle/f$b;

    invoke-virtual {v1, v2}, Landroidx/lifecycle/k;->e(Landroidx/lifecycle/f$b;)V

    const/4 v1, 0x6

    iput v1, v0, Landroidx/fragment/app/f;->a:I

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroidx/fragment/app/f;->C:Z

    const/4 v2, 0x1

    iput-boolean v2, v0, Landroidx/fragment/app/f;->C:Z

    iget-object v0, p0, Landroidx/fragment/app/w;->a:Landroidx/fragment/app/p;

    iget-object v2, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v0, v2, v1}, Landroidx/fragment/app/p;->f(Landroidx/fragment/app/f;Z)V

    return-void
.end method

.method public l(Ljava/lang/ClassLoader;)V
    .locals 3

    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v0, v0, Landroidx/fragment/app/f;->b:Landroid/os/Bundle;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    iget-object p1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v0, p1, Landroidx/fragment/app/f;->b:Landroid/os/Bundle;

    const-string v1, "android:view_state"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v0

    iput-object v0, p1, Landroidx/fragment/app/f;->c:Landroid/util/SparseArray;

    iget-object p1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v0, p1, Landroidx/fragment/app/f;->b:Landroid/os/Bundle;

    const-string v1, "android:view_registry_state"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p1, Landroidx/fragment/app/f;->d:Landroid/os/Bundle;

    iget-object p1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v0, p1, Landroidx/fragment/app/f;->b:Landroid/os/Bundle;

    const-string v1, "android:target_state"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Landroidx/fragment/app/f;->h:Ljava/lang/String;

    iget-object p1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v0, p1, Landroidx/fragment/app/f;->h:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p1, Landroidx/fragment/app/f;->b:Landroid/os/Bundle;

    const/4 v1, 0x0

    const-string v2, "android:target_req_state"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p1, Landroidx/fragment/app/f;->i:I

    :cond_1
    iget-object p1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x1

    iget-object v1, p1, Landroidx/fragment/app/f;->b:Landroid/os/Bundle;

    const-string v2, "android:user_visible_hint"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p1, Landroidx/fragment/app/f;->F:Z

    iget-object p1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-boolean v1, p1, Landroidx/fragment/app/f;->F:Z

    if-nez v1, :cond_2

    iput-boolean v0, p1, Landroidx/fragment/app/f;->E:Z

    :cond_2
    return-void
.end method

.method public m()V
    .locals 6

    const/4 v0, 0x3

    invoke-static {v0}, Landroidx/fragment/app/q;->K(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "moveto RESUMED: "

    invoke-static {v0}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FragmentManager"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v0, v0, Landroidx/fragment/app/f;->G:Landroidx/fragment/app/f$a;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, v0, Landroidx/fragment/app/f$a;->n:Landroid/view/View;

    :goto_0
    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_2

    iget-object v4, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v0, v1}, Landroidx/fragment/app/f;->H(Landroid/view/View;)V

    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v4, v0, Landroidx/fragment/app/f;->t:Landroidx/fragment/app/q;

    invoke-virtual {v4}, Landroidx/fragment/app/q;->R()V

    iget-object v4, v0, Landroidx/fragment/app/f;->t:Landroidx/fragment/app/q;

    invoke-virtual {v4, v2}, Landroidx/fragment/app/q;->B(Z)Z

    const/4 v4, 0x7

    iput v4, v0, Landroidx/fragment/app/f;->a:I

    iput-boolean v3, v0, Landroidx/fragment/app/f;->C:Z

    iput-boolean v2, v0, Landroidx/fragment/app/f;->C:Z

    iget-object v2, v0, Landroidx/fragment/app/f;->K:Landroidx/lifecycle/k;

    sget-object v5, Landroidx/lifecycle/f$b;->ON_RESUME:Landroidx/lifecycle/f$b;

    invoke-virtual {v2, v5}, Landroidx/lifecycle/k;->e(Landroidx/lifecycle/f$b;)V

    iget-object v0, v0, Landroidx/fragment/app/f;->t:Landroidx/fragment/app/q;

    iput-boolean v3, v0, Landroidx/fragment/app/q;->B:Z

    iput-boolean v3, v0, Landroidx/fragment/app/q;->C:Z

    iget-object v2, v0, Landroidx/fragment/app/q;->J:Landroidx/fragment/app/t;

    iput-boolean v3, v2, Landroidx/fragment/app/t;->g:Z

    invoke-virtual {v0, v4}, Landroidx/fragment/app/q;->w(I)V

    iget-object v0, p0, Landroidx/fragment/app/w;->a:Landroidx/fragment/app/p;

    iget-object v2, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v0, v2, v3}, Landroidx/fragment/app/p;->i(Landroidx/fragment/app/f;Z)V

    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iput-object v1, v0, Landroidx/fragment/app/f;->b:Landroid/os/Bundle;

    iput-object v1, v0, Landroidx/fragment/app/f;->c:Landroid/util/SparseArray;

    iput-object v1, v0, Landroidx/fragment/app/f;->d:Landroid/os/Bundle;

    return-void
.end method

.method public n()V
    .locals 5

    const/4 v0, 0x3

    invoke-static {v0}, Landroidx/fragment/app/q;->K(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "moveto STARTED: "

    invoke-static {v0}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FragmentManager"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v1, v0, Landroidx/fragment/app/f;->t:Landroidx/fragment/app/q;

    invoke-virtual {v1}, Landroidx/fragment/app/q;->R()V

    iget-object v1, v0, Landroidx/fragment/app/f;->t:Landroidx/fragment/app/q;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroidx/fragment/app/q;->B(Z)Z

    const/4 v1, 0x5

    iput v1, v0, Landroidx/fragment/app/f;->a:I

    const/4 v3, 0x0

    iput-boolean v3, v0, Landroidx/fragment/app/f;->C:Z

    iput-boolean v2, v0, Landroidx/fragment/app/f;->C:Z

    iget-object v2, v0, Landroidx/fragment/app/f;->K:Landroidx/lifecycle/k;

    sget-object v4, Landroidx/lifecycle/f$b;->ON_START:Landroidx/lifecycle/f$b;

    invoke-virtual {v2, v4}, Landroidx/lifecycle/k;->e(Landroidx/lifecycle/f$b;)V

    iget-object v0, v0, Landroidx/fragment/app/f;->t:Landroidx/fragment/app/q;

    iput-boolean v3, v0, Landroidx/fragment/app/q;->B:Z

    iput-boolean v3, v0, Landroidx/fragment/app/q;->C:Z

    iget-object v2, v0, Landroidx/fragment/app/q;->J:Landroidx/fragment/app/t;

    iput-boolean v3, v2, Landroidx/fragment/app/t;->g:Z

    invoke-virtual {v0, v1}, Landroidx/fragment/app/q;->w(I)V

    iget-object v0, p0, Landroidx/fragment/app/w;->a:Landroidx/fragment/app/p;

    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v0, v1, v3}, Landroidx/fragment/app/p;->k(Landroidx/fragment/app/f;Z)V

    return-void
.end method

.method public o()V
    .locals 5

    const/4 v0, 0x3

    invoke-static {v0}, Landroidx/fragment/app/q;->K(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "movefrom STARTED: "

    invoke-static {v0}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FragmentManager"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    iget-object v1, v0, Landroidx/fragment/app/f;->t:Landroidx/fragment/app/q;

    const/4 v2, 0x1

    iput-boolean v2, v1, Landroidx/fragment/app/q;->C:Z

    iget-object v3, v1, Landroidx/fragment/app/q;->J:Landroidx/fragment/app/t;

    iput-boolean v2, v3, Landroidx/fragment/app/t;->g:Z

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Landroidx/fragment/app/q;->w(I)V

    iget-object v1, v0, Landroidx/fragment/app/f;->K:Landroidx/lifecycle/k;

    sget-object v4, Landroidx/lifecycle/f$b;->ON_STOP:Landroidx/lifecycle/f$b;

    invoke-virtual {v1, v4}, Landroidx/lifecycle/k;->e(Landroidx/lifecycle/f$b;)V

    iput v3, v0, Landroidx/fragment/app/f;->a:I

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroidx/fragment/app/f;->C:Z

    iput-boolean v2, v0, Landroidx/fragment/app/f;->C:Z

    iget-object v0, p0, Landroidx/fragment/app/w;->a:Landroidx/fragment/app/p;

    iget-object v2, p0, Landroidx/fragment/app/w;->c:Landroidx/fragment/app/f;

    invoke-virtual {v0, v2, v1}, Landroidx/fragment/app/p;->l(Landroidx/fragment/app/f;Z)V

    return-void
.end method
