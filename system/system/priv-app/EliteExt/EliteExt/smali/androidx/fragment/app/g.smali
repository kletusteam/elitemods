.class public Landroidx/fragment/app/g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroidx/savedstate/a$b;


# instance fields
.field public final synthetic a:Landroidx/fragment/app/i;


# direct methods
.method public constructor <init>(Landroidx/fragment/app/i;)V
    .locals 0

    iput-object p1, p0, Landroidx/fragment/app/g;->a:Landroidx/fragment/app/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Landroid/os/Bundle;
    .locals 4

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Landroidx/fragment/app/g;->a:Landroidx/fragment/app/i;

    :cond_0
    iget-object v2, v1, Landroidx/fragment/app/i;->h:Landroidx/fragment/app/l;

    iget-object v2, v2, Landroidx/fragment/app/l;->a:Landroidx/fragment/app/n;

    iget-object v2, v2, Landroidx/fragment/app/n;->d:Landroidx/fragment/app/q;

    sget-object v3, Landroidx/lifecycle/f$c;->c:Landroidx/lifecycle/f$c;

    invoke-static {v2, v3}, Landroidx/fragment/app/i;->t(Landroidx/fragment/app/q;Landroidx/lifecycle/f$c;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v1, p0, Landroidx/fragment/app/g;->a:Landroidx/fragment/app/i;

    iget-object v1, v1, Landroidx/fragment/app/i;->i:Landroidx/lifecycle/k;

    sget-object v2, Landroidx/lifecycle/f$b;->ON_STOP:Landroidx/lifecycle/f$b;

    invoke-virtual {v1, v2}, Landroidx/lifecycle/k;->e(Landroidx/lifecycle/f$b;)V

    iget-object v1, p0, Landroidx/fragment/app/g;->a:Landroidx/fragment/app/i;

    iget-object v1, v1, Landroidx/fragment/app/i;->h:Landroidx/fragment/app/l;

    iget-object v1, v1, Landroidx/fragment/app/l;->a:Landroidx/fragment/app/n;

    iget-object v1, v1, Landroidx/fragment/app/n;->d:Landroidx/fragment/app/q;

    invoke-virtual {v1}, Landroidx/fragment/app/q;->W()Landroid/os/Parcelable;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v2, "android:support:fragments"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_1
    return-object v0
.end method
