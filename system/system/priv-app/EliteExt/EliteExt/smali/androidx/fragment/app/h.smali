.class public Landroidx/fragment/app/h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements La/b;


# instance fields
.field public final synthetic a:Landroidx/fragment/app/i;


# direct methods
.method public constructor <init>(Landroidx/fragment/app/i;)V
    .locals 0

    iput-object p1, p0, Landroidx/fragment/app/h;->a:Landroidx/fragment/app/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 2

    iget-object p1, p0, Landroidx/fragment/app/h;->a:Landroidx/fragment/app/i;

    iget-object p1, p1, Landroidx/fragment/app/i;->h:Landroidx/fragment/app/l;

    iget-object p1, p1, Landroidx/fragment/app/l;->a:Landroidx/fragment/app/n;

    iget-object v0, p1, Landroidx/fragment/app/n;->d:Landroidx/fragment/app/q;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p1, v1}, Landroidx/fragment/app/q;->b(Landroidx/fragment/app/n;Landroidx/activity/result/d;Landroidx/fragment/app/f;)V

    iget-object p1, p0, Landroidx/fragment/app/h;->a:Landroidx/fragment/app/i;

    iget-object p1, p1, Landroidx/activity/ComponentActivity;->d:Landroidx/savedstate/b;

    iget-object p1, p1, Landroidx/savedstate/b;->b:Landroidx/savedstate/a;

    const-string v0, "android:support:fragments"

    invoke-virtual {p1, v0}, Landroidx/savedstate/a;->a(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    iget-object v0, p0, Landroidx/fragment/app/h;->a:Landroidx/fragment/app/i;

    iget-object v0, v0, Landroidx/fragment/app/i;->h:Landroidx/fragment/app/l;

    iget-object v0, v0, Landroidx/fragment/app/l;->a:Landroidx/fragment/app/n;

    instance-of v1, v0, Landroidx/lifecycle/y;

    if-eqz v1, :cond_0

    iget-object v0, v0, Landroidx/fragment/app/n;->d:Landroidx/fragment/app/q;

    invoke-virtual {v0, p1}, Landroidx/fragment/app/q;->V(Landroid/os/Parcelable;)V

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Your FragmentHostCallback must implement ViewModelStoreOwner to call restoreSaveState(). Call restoreAllState()  if you\'re still using retainNestedNonConfig()."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    return-void
.end method
