.class public Landroidx/fragment/app/i$a;
.super Landroidx/fragment/app/n;
.source ""

# interfaces
.implements Landroidx/lifecycle/y;
.implements Landroidx/activity/c;
.implements Landroidx/activity/result/f;
.implements Landroidx/fragment/app/u;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/fragment/app/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/fragment/app/n<",
        "Landroidx/fragment/app/i;",
        ">;",
        "Landroidx/lifecycle/y;",
        "Landroidx/activity/c;",
        "Landroidx/activity/result/f;",
        "Landroidx/fragment/app/u;"
    }
.end annotation


# instance fields
.field public final synthetic e:Landroidx/fragment/app/i;


# direct methods
.method public constructor <init>(Landroidx/fragment/app/i;)V
    .locals 0

    iput-object p1, p0, Landroidx/fragment/app/i$a;->e:Landroidx/fragment/app/i;

    invoke-direct {p0, p1}, Landroidx/fragment/app/n;-><init>(Landroidx/fragment/app/i;)V

    return-void
.end method


# virtual methods
.method public a()Landroidx/lifecycle/f;
    .locals 1

    iget-object v0, p0, Landroidx/fragment/app/i$a;->e:Landroidx/fragment/app/i;

    iget-object v0, v0, Landroidx/fragment/app/i;->i:Landroidx/lifecycle/k;

    return-object v0
.end method

.method public b(Landroidx/fragment/app/q;Landroidx/fragment/app/f;)V
    .locals 0

    iget-object p1, p0, Landroidx/fragment/app/i$a;->e:Landroidx/fragment/app/i;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public c()Landroidx/activity/OnBackPressedDispatcher;
    .locals 1

    iget-object v0, p0, Landroidx/fragment/app/i$a;->e:Landroidx/fragment/app/i;

    iget-object v0, v0, Landroidx/activity/ComponentActivity;->f:Landroidx/activity/OnBackPressedDispatcher;

    return-object v0
.end method

.method public e()Landroidx/lifecycle/x;
    .locals 1

    iget-object v0, p0, Landroidx/fragment/app/i$a;->e:Landroidx/fragment/app/i;

    invoke-virtual {v0}, Landroidx/activity/ComponentActivity;->e()Landroidx/lifecycle/x;

    move-result-object v0

    return-object v0
.end method

.method public g(I)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Landroidx/fragment/app/i$a;->e:Landroidx/fragment/app/i;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public h()Z
    .locals 1

    iget-object v0, p0, Landroidx/fragment/app/i$a;->e:Landroidx/fragment/app/i;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/Window;->peekDecorView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public l()Landroidx/activity/result/e;
    .locals 1

    iget-object v0, p0, Landroidx/fragment/app/i$a;->e:Landroidx/fragment/app/i;

    iget-object v0, v0, Landroidx/activity/ComponentActivity;->g:Landroidx/activity/result/e;

    return-object v0
.end method

.method public m()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Landroidx/fragment/app/i$a;->e:Landroidx/fragment/app/i;

    return-object v0
.end method

.method public n()Landroid/view/LayoutInflater;
    .locals 2

    iget-object v0, p0, Landroidx/fragment/app/i$a;->e:Landroidx/fragment/app/i;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Landroidx/fragment/app/i$a;->e:Landroidx/fragment/app/i;

    invoke-virtual {v0, v1}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    return-object v0
.end method

.method public o()V
    .locals 1

    iget-object v0, p0, Landroidx/fragment/app/i$a;->e:Landroidx/fragment/app/i;

    invoke-virtual {v0}, Landroidx/fragment/app/i;->u()V

    return-void
.end method
