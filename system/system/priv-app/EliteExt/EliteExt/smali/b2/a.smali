.class public Lb2/a;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Landroid/graphics/drawable/Drawable;

.field public B:I

.field public C:La1/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La1/b<",
            "Landroid/widget/CompoundButton;",
            ">;"
        }
    .end annotation
.end field

.field public D:I

.field public E:Landroid/graphics/drawable/Drawable;

.field public F:I

.field public G:I

.field public H:I

.field public I:Lz0/f;

.field public J:F

.field public K:La1/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La1/b<",
            "Landroid/widget/CompoundButton;",
            ">;"
        }
    .end annotation
.end field

.field public L:Landroid/graphics/drawable/Drawable;

.field public M:F

.field public N:La1/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La1/b<",
            "Landroid/widget/CompoundButton;",
            ">;"
        }
    .end annotation
.end field

.field public O:Lz0/f;

.field public P:Lz0/f;

.field public Q:Landroid/graphics/drawable/Drawable;

.field public R:Lz0/f;

.field public S:I

.field public T:I

.field public U:Lz0/f;

.field public V:Lz0/f;

.field public W:F

.field public X:La1/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La1/b<",
            "Landroid/widget/CompoundButton;",
            ">;"
        }
    .end annotation
.end field

.field public Y:I

.field public Z:Landroid/graphics/Rect;

.field public a:Z

.field public a0:Z

.field public b:Z

.field public b0:Lz0/f;

.field public c:I

.field public c0:Lz0/f;

.field public d:F

.field public d0:Landroid/widget/CompoundButton;

.field public e:I

.field public e0:I

.field public f:Lz0/c$c;

.field public g:Z

.field public h:I

.field public i:I

.field public j:Lz0/f;

.field public k:Lz0/f;

.field public l:Landroid/graphics/drawable/Drawable;

.field public m:F

.field public n:La1/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La1/b<",
            "Landroid/widget/CompoundButton;",
            ">;"
        }
    .end annotation
.end field

.field public o:F

.field public p:Landroid/graphics/drawable/Drawable;

.field public q:F

.field public r:La1/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La1/b<",
            "Landroid/widget/CompoundButton;",
            ">;"
        }
    .end annotation
.end field

.field public s:Landroid/graphics/drawable/Drawable;

.field public t:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field public u:I

.field public v:Z

.field public w:Landroid/graphics/drawable/StateListDrawable;

.field public x:I

.field public y:Lz0/f;

.field public z:Z


# direct methods
.method public constructor <init>(Landroid/widget/CompoundButton;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lb2/a;->Z:Landroid/graphics/Rect;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb2/a;->g:Z

    new-instance v1, Lb2/a$a;

    const-string v2, "SliderOffset"

    invoke-direct {v1, p0, v2}, Lb2/a$a;-><init>(Lb2/a;Ljava/lang/String;)V

    iput-object v1, p0, Lb2/a;->C:La1/b;

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lb2/a;->J:F

    const/4 v2, 0x0

    iput v2, p0, Lb2/a;->M:F

    const v3, 0x3dcccccd    # 0.1f

    iput v3, p0, Lb2/a;->W:F

    iput v1, p0, Lb2/a;->m:F

    iput v2, p0, Lb2/a;->q:F

    iput-boolean v0, p0, Lb2/a;->v:Z

    const/4 v3, -0x1

    iput v3, p0, Lb2/a;->D:I

    iput v3, p0, Lb2/a;->G:I

    iput-boolean v0, p0, Lb2/a;->b:Z

    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lb2/a;->o:F

    new-instance v0, Lb2/a$b;

    const-string v3, "SliderScale"

    invoke-direct {v0, p0, v3}, Lb2/a$b;-><init>(Lb2/a;Ljava/lang/String;)V

    iput-object v0, p0, Lb2/a;->K:La1/b;

    new-instance v0, Ls0/b;

    const/4 v3, 0x2

    invoke-direct {v0, p0, v3}, Ls0/b;-><init>(Ljava/lang/Object;I)V

    iput-object v0, p0, Lb2/a;->f:Lz0/c$c;

    new-instance v0, Lb2/a$c;

    const-string v3, "SliderShadowAlpha"

    invoke-direct {v0, p0, v3}, Lb2/a$c;-><init>(Lb2/a;Ljava/lang/String;)V

    iput-object v0, p0, Lb2/a;->N:La1/b;

    new-instance v0, Lb2/a$d;

    const-string v3, "StrokeAlpha"

    invoke-direct {v0, p0, v3}, Lb2/a$d;-><init>(Lb2/a;Ljava/lang/String;)V

    iput-object v0, p0, Lb2/a;->X:La1/b;

    new-instance v0, Lb2/a$e;

    const-string v3, "MaskCheckedSlideBarAlpha"

    invoke-direct {v0, p0, v3}, Lb2/a$e;-><init>(Lb2/a;Ljava/lang/String;)V

    iput-object v0, p0, Lb2/a;->n:La1/b;

    new-instance v0, Lb2/a$f;

    const-string v3, "MaskUnCheckedSlideBarAlpha"

    invoke-direct {v0, p0, v3}, Lb2/a$f;-><init>(Lb2/a;Ljava/lang/String;)V

    iput-object v0, p0, Lb2/a;->r:La1/b;

    iput v1, p0, Lb2/a;->d:F

    iput-object p1, p0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result p1

    iput-boolean p1, p0, Lb2/a;->a:Z

    iget-object p1, p0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result p1

    if-nez p1, :cond_0

    iput v2, p0, Lb2/a;->m:F

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 5

    iget-object v0, p0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    const/4 v1, 0x0

    if-eq p1, v0, :cond_9

    iget-object v0, p0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v0, p0, Lb2/a;->y:Lz0/f;

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lz0/c;->f:Z

    if-nez v0, :cond_3

    :cond_0
    iget-boolean v0, p0, Lb2/a;->a:Z

    if-eqz v0, :cond_1

    iget v2, p0, Lb2/a;->H:I

    goto :goto_0

    :cond_1
    move v2, v1

    :goto_0
    iput v2, p0, Lb2/a;->B:I

    if-eqz v0, :cond_2

    const/16 v0, 0xff

    goto :goto_1

    :cond_2
    move v0, v1

    :goto_1
    iput v0, p0, Lb2/a;->F:I

    :cond_3
    iget-boolean v0, p0, Lb2/a;->v:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lb2/a;->D:I

    iput v0, p0, Lb2/a;->B:I

    iget v0, p0, Lb2/a;->G:I

    iput v0, p0, Lb2/a;->F:I

    iget v0, p0, Lb2/a;->o:F

    iput v0, p0, Lb2/a;->m:F

    iget-boolean v0, p0, Lb2/a;->b:Z

    iput-boolean v0, p0, Lb2/a;->a:Z

    iput-boolean v1, p0, Lb2/a;->v:Z

    const/4 v0, -0x1

    iput v0, p0, Lb2/a;->D:I

    iput v0, p0, Lb2/a;->G:I

    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lb2/a;->o:F

    :cond_4
    iget-boolean v0, p0, Lb2/a;->a:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lb2/a;->j:Lz0/f;

    iget-boolean v2, v0, Lz0/c;->f:Z

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lz0/c;->c()V

    :cond_5
    iget-object v0, p0, Lb2/a;->k:Lz0/f;

    iget-boolean v0, v0, Lz0/c;->f:Z

    if-nez v0, :cond_6

    if-nez p1, :cond_6

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lb2/a;->m:F

    :cond_6
    iget-boolean v0, p0, Lb2/a;->a:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lb2/a;->k:Lz0/f;

    iget-boolean v2, v0, Lz0/c;->f:Z

    if-eqz v2, :cond_7

    invoke-virtual {v0}, Lz0/c;->c()V

    :cond_7
    iget-object v0, p0, Lb2/a;->j:Lz0/f;

    iget-boolean v0, v0, Lz0/c;->f:Z

    if-nez v0, :cond_8

    if-eqz p1, :cond_8

    const/4 v0, 0x0

    iput v0, p0, Lb2/a;->m:F

    :cond_8
    iget-object v0, p0, Lb2/a;->t:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    iget-object v2, p0, Lb2/a;->t:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    iget-object v3, p0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-interface {v2, v3, v0}, Landroid/widget/CompoundButton$OnCheckedChangeListener;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    :cond_9
    if-eqz p1, :cond_a

    iget v1, p0, Lb2/a;->H:I

    :cond_a
    new-instance v0, Lb2/a$g;

    invoke-direct {v0, p0}, Lb2/a$g;-><init>(Lb2/a;)V

    iget-object v2, p0, Lb2/a;->y:Lz0/f;

    if-eqz v2, :cond_b

    iget-boolean v3, v2, Lz0/c;->f:Z

    if-eqz v3, :cond_b

    invoke-virtual {v2}, Lz0/c;->c()V

    :cond_b
    iget-object v2, p0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v2

    if-eq p1, v2, :cond_c

    goto :goto_3

    :cond_c
    new-instance v2, Lz0/f;

    iget-object v3, p0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    iget-object v4, p0, Lb2/a;->C:La1/b;

    int-to-float v1, v1

    invoke-direct {v2, v3, v4, v1}, Lz0/f;-><init>(Ljava/lang/Object;La1/b;F)V

    iput-object v2, p0, Lb2/a;->y:Lz0/f;

    iget-object v1, v2, Lz0/f;->o:Lz0/g;

    const v2, 0x4476bd71

    invoke-virtual {v1, v2}, Lz0/g;->b(F)Lz0/g;

    iget-object v1, p0, Lb2/a;->y:Lz0/f;

    iget-object v1, v1, Lz0/f;->o:Lz0/g;

    const v2, 0x3f333333    # 0.7f

    invoke-virtual {v1, v2}, Lz0/g;->a(F)Lz0/g;

    iget-object v1, p0, Lb2/a;->y:Lz0/f;

    iget-object v2, p0, Lb2/a;->f:Lz0/c$c;

    invoke-virtual {v1, v2}, Lz0/c;->b(Lz0/c$c;)Lz0/c;

    iget-object v1, p0, Lb2/a;->y:Lz0/f;

    new-instance v2, Lb2/b;

    invoke-direct {v2, p0, v0}, Lb2/b;-><init>(Lb2/a;Ljava/lang/Runnable;)V

    iget-object v0, v1, Lz0/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, v1, Lz0/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_d
    iget-object v0, p0, Lb2/a;->y:Lz0/f;

    invoke-virtual {v0}, Lz0/f;->i()V

    if-eqz p1, :cond_f

    iget-object p1, p0, Lb2/a;->k:Lz0/f;

    iget-boolean v0, p1, Lz0/c;->f:Z

    if-nez v0, :cond_e

    invoke-virtual {p1}, Lz0/f;->i()V

    :cond_e
    iget-object p1, p0, Lb2/a;->j:Lz0/f;

    iget-boolean v0, p1, Lz0/c;->f:Z

    if-eqz v0, :cond_11

    goto :goto_2

    :cond_f
    iget-object p1, p0, Lb2/a;->j:Lz0/f;

    iget-boolean v0, p1, Lz0/c;->f:Z

    if-nez v0, :cond_10

    invoke-virtual {p1}, Lz0/f;->i()V

    :cond_10
    iget-object p1, p0, Lb2/a;->k:Lz0/f;

    iget-boolean v0, p1, Lz0/c;->f:Z

    if-eqz v0, :cond_11

    :goto_2
    invoke-virtual {p1}, Lz0/c;->c()V

    :cond_11
    :goto_3
    return-void
.end method

.method public final b(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 5

    new-instance v0, Lmiuix/smooth/SmoothContainerDrawable;

    invoke-direct {v0}, Lmiuix/smooth/SmoothContainerDrawable;-><init>()V

    iget-object v1, p0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->getLayerType()I

    move-result v1

    invoke-virtual {v0, v1}, Lmiuix/smooth/SmoothContainerDrawable;->c(I)V

    iget v1, p0, Lb2/a;->c:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lmiuix/smooth/SmoothContainerDrawable;->b(F)V

    iget-object v1, v0, Lmiuix/smooth/SmoothContainerDrawable;->a:Lmiuix/smooth/SmoothContainerDrawable$c;

    if-eqz v1, :cond_0

    new-instance v1, Lmiuix/smooth/SmoothContainerDrawable$b;

    invoke-direct {v1}, Lmiuix/smooth/SmoothContainerDrawable$b;-><init>()V

    iput-object p1, v1, Lmiuix/smooth/SmoothContainerDrawable$b;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iget-object p1, v0, Lmiuix/smooth/SmoothContainerDrawable;->a:Lmiuix/smooth/SmoothContainerDrawable$c;

    iput-object v1, p1, Lmiuix/smooth/SmoothContainerDrawable$c;->a:Lmiuix/smooth/SmoothContainerDrawable$b;

    :cond_0
    iget p1, p0, Lb2/a;->i:I

    new-instance v1, Landroid/graphics/Rect;

    const/4 v2, 0x0

    iget v3, p0, Lb2/a;->e0:I

    iget v4, p0, Lb2/a;->e:I

    sub-int/2addr v4, p1

    invoke-direct {v1, v2, p1, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    return-object v0
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lb2/a;->I:Lz0/f;

    iget-boolean v1, v0, Lz0/c;->f:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lz0/c;->c()V

    :cond_0
    iget-object v0, p0, Lb2/a;->R:Lz0/f;

    iget-boolean v1, v0, Lz0/c;->f:Z

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lz0/f;->i()V

    :cond_1
    iget-object v0, p0, Lb2/a;->P:Lz0/f;

    iget-boolean v1, v0, Lz0/c;->f:Z

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lz0/c;->c()V

    :cond_2
    iget-object v0, p0, Lb2/a;->O:Lz0/f;

    iget-boolean v1, v0, Lz0/c;->f:Z

    if-nez v1, :cond_3

    invoke-virtual {v0}, Lz0/f;->i()V

    :cond_3
    iget-object v0, p0, Lb2/a;->V:Lz0/f;

    iget-boolean v1, v0, Lz0/c;->f:Z

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lz0/c;->c()V

    :cond_4
    iget-object v0, p0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lb2/a;->c0:Lz0/f;

    iget-boolean v1, v0, Lz0/c;->f:Z

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lz0/c;->c()V

    :cond_5
    iget-object v0, p0, Lb2/a;->b0:Lz0/f;

    iget-boolean v1, v0, Lz0/c;->f:Z

    if-nez v1, :cond_6

    invoke-virtual {v0}, Lz0/f;->i()V

    :cond_6
    iget-object v0, p0, Lb2/a;->U:Lz0/f;

    iget-boolean v1, v0, Lz0/c;->f:Z

    if-nez v1, :cond_7

    invoke-virtual {v0}, Lz0/f;->i()V

    :cond_7
    return-void
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lb2/a;->E:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    iget-object v0, p0, Lb2/a;->w:Landroid/graphics/drawable/StateListDrawable;

    iget-object v1, p0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    :cond_0
    return-void
.end method

.method public e(I)V
    .locals 0

    iput p1, p0, Lb2/a;->B:I

    iget-object p1, p0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->invalidate()V

    return-void
.end method
