.class public Lc1/c;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lc1/c$a;,
        Lc1/c$b;,
        Lc1/c$c;
    }
.end annotation


# static fields
.field public static final a:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/Integer;",
            "Landroid/animation/TimeInterpolator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lc1/c;->a:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method public static a(Lc1/c$b;)Landroid/animation/TimeInterpolator;
    .locals 7

    const/4 v0, 0x0

    if-eqz p0, :cond_2

    sget-object v1, Lc1/c;->a:Ljava/util/concurrent/ConcurrentHashMap;

    iget v2, p0, Lc1/c$a;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/animation/TimeInterpolator;

    if-nez v2, :cond_1

    iget v2, p0, Lc1/c$a;->c:I

    iget-object v3, p0, Lc1/c$a;->a:[F

    const/16 v4, 0xb

    const/4 v5, 0x1

    const/4 v6, 0x0

    packed-switch v2, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    new-instance v0, Lf2/a;

    invoke-direct {v0, v5}, Lf2/a;-><init>(I)V

    goto/16 :goto_0

    :pswitch_1
    new-instance v0, Lf2/a;

    const/4 v2, 0x2

    invoke-direct {v0, v2}, Lf2/a;-><init>(I)V

    goto/16 :goto_0

    :pswitch_2
    new-instance v0, Lf2/a;

    invoke-direct {v0, v6}, Lf2/a;-><init>(I)V

    goto/16 :goto_0

    :pswitch_3
    new-instance v0, Landroid/view/animation/BounceInterpolator;

    invoke-direct {v0}, Landroid/view/animation/BounceInterpolator;-><init>()V

    goto/16 :goto_0

    :pswitch_4
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    goto/16 :goto_0

    :pswitch_5
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    goto/16 :goto_0

    :pswitch_6
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    goto/16 :goto_0

    :pswitch_7
    new-instance v0, Lf2/a;

    const/4 v2, 0x7

    invoke-direct {v0, v2}, Lf2/a;-><init>(I)V

    goto/16 :goto_0

    :pswitch_8
    new-instance v0, Lf2/a;

    const/16 v2, 0x8

    invoke-direct {v0, v2}, Lf2/a;-><init>(I)V

    goto/16 :goto_0

    :pswitch_9
    new-instance v0, Lf2/a;

    const/4 v2, 0x6

    invoke-direct {v0, v2}, Lf2/a;-><init>(I)V

    goto/16 :goto_0

    :pswitch_a
    new-instance v0, Lf2/a;

    const/16 v2, 0x12

    invoke-direct {v0, v2}, Lf2/a;-><init>(I)V

    goto/16 :goto_0

    :pswitch_b
    new-instance v0, Lf2/a;

    const/16 v2, 0x13

    invoke-direct {v0, v2}, Lf2/a;-><init>(I)V

    goto/16 :goto_0

    :pswitch_c
    new-instance v0, Lf2/a;

    const/16 v2, 0x11

    invoke-direct {v0, v2}, Lf2/a;-><init>(I)V

    goto/16 :goto_0

    :pswitch_d
    new-instance v0, Lf2/a;

    const/16 v2, 0xf

    invoke-direct {v0, v2}, Lf2/a;-><init>(I)V

    goto/16 :goto_0

    :pswitch_e
    new-instance v0, Lf2/a;

    const/16 v2, 0x10

    invoke-direct {v0, v2}, Lf2/a;-><init>(I)V

    goto :goto_0

    :pswitch_f
    new-instance v0, Lf2/a;

    const/16 v2, 0xe

    invoke-direct {v0, v2}, Lf2/a;-><init>(I)V

    goto :goto_0

    :pswitch_10
    new-instance v0, Lf2/a;

    const/16 v2, 0xd

    invoke-direct {v0, v2}, Lf2/a;-><init>(I)V

    goto :goto_0

    :pswitch_11
    new-instance v0, Lf2/a;

    invoke-direct {v0, v4}, Lf2/a;-><init>(I)V

    goto :goto_0

    :pswitch_12
    new-instance v0, Lf2/a;

    const/16 v2, 0xc

    invoke-direct {v0, v2}, Lf2/a;-><init>(I)V

    goto :goto_0

    :pswitch_13
    new-instance v0, Lf2/a;

    const/4 v2, 0x4

    invoke-direct {v0, v2}, Lf2/a;-><init>(I)V

    goto :goto_0

    :pswitch_14
    new-instance v0, Lf2/a;

    const/4 v2, 0x5

    invoke-direct {v0, v2}, Lf2/a;-><init>(I)V

    goto :goto_0

    :pswitch_15
    new-instance v0, Lf2/a;

    const/4 v2, 0x3

    invoke-direct {v0, v2}, Lf2/a;-><init>(I)V

    goto :goto_0

    :pswitch_16
    new-instance v0, Lf2/a;

    const/16 v2, 0xa

    invoke-direct {v0, v2}, Lf2/a;-><init>(I)V

    goto :goto_0

    :pswitch_17
    new-instance v0, Lf2/a;

    invoke-direct {v0, v4}, Lf2/a;-><init>(I)V

    goto :goto_0

    :pswitch_18
    new-instance v0, Lf2/a;

    const/16 v2, 0x9

    invoke-direct {v0, v2}, Lf2/a;-><init>(I)V

    goto :goto_0

    :pswitch_19
    new-instance v0, Lc1/c$c;

    invoke-direct {v0}, Lc1/c$c;-><init>()V

    aget v2, v3, v6

    iput v2, v0, Lc1/c$c;->c:F

    invoke-virtual {v0}, Lc1/c$c;->a()V

    aget v2, v3, v5

    iput v2, v0, Lc1/c$c;->e:F

    invoke-virtual {v0}, Lc1/c$c;->a()V

    goto :goto_0

    :pswitch_1a
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    :goto_0
    if-eqz v0, :cond_0

    iget p0, p0, Lc1/c$a;->c:I

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v1, p0, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    move-object v2, v0

    :cond_1
    return-object v2

    :cond_2
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1a
        :pswitch_19
        :pswitch_1a
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static varargs b(I[F)Lc1/c$a;
    .locals 3

    const/4 v0, -0x1

    if-lt p0, v0, :cond_2

    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-le v0, v2, :cond_0

    array-length v0, p1

    invoke-static {p1, v2, v0}, Ljava/util/Arrays;->copyOfRange([FII)[F

    move-result-object v0

    goto :goto_0

    :cond_0
    new-array v0, v1, [F

    :goto_0
    new-instance v2, Lc1/c$b;

    invoke-direct {v2, p0, v0}, Lc1/c$b;-><init>(I[F)V

    array-length p0, p1

    if-lez p0, :cond_1

    aget p0, p1, v1

    float-to-int p0, p0

    int-to-long p0, p0

    iput-wide p0, v2, Lc1/c$b;->d:J

    :cond_1
    return-object v2

    :cond_2
    new-instance v0, Lc1/c$a;

    invoke-direct {v0, p0, p1}, Lc1/c$a;-><init>(I[F)V

    return-object v0
.end method
