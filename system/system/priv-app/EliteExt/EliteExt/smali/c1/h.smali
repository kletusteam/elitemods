.class public final Lc1/h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# instance fields
.field public final synthetic a:Lc1/i;

.field public final synthetic b:Ljava/lang/Class;

.field public final synthetic c:[Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lc1/i;[Ljava/lang/Object;Ljava/lang/Class;)V
    .locals 0

    iput-object p1, p0, Lc1/h;->a:Lc1/i;

    iput-object p2, p0, Lc1/h;->c:[Ljava/lang/Object;

    iput-object p3, p0, Lc1/h;->b:Ljava/lang/Class;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9

    iget-object v0, p0, Lc1/h;->a:Lc1/i;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v4, "getState"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p2, p0, Lc1/h;->a:Lc1/i;

    iget-object v0, p0, Lc1/h;->c:[Ljava/lang/Object;

    check-cast p2, Lw0/j$a;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, [Lw0/h;

    array-length p2, v0

    if-lez p2, :cond_1

    array-length p2, p3

    if-lez p2, :cond_1

    aget-object p2, v0, v2

    aget-object p3, p3, v2

    invoke-interface {p2, p3}, Lw0/h;->d(Ljava/lang/Object;)Lw0/a;

    move-result-object v1

    move p2, v3

    :goto_0
    array-length p3, v0

    if-ge p2, p3, :cond_1

    aget-object p3, v0, p2

    invoke-interface {p3, v1}, Lw0/h;->e(Lw0/a;)V

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lc1/h;->c:[Ljava/lang/Object;

    array-length v4, v0

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v0, v2

    :try_start_0
    invoke-virtual {p2, v5, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "failed to invoke "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v8, " for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v6

    const-string v7, "StyleComposer"

    invoke-static {v7, v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    if-eqz v1, :cond_2

    iget-object p2, p0, Lc1/h;->c:[Ljava/lang/Object;

    array-length p3, p2

    sub-int/2addr p3, v3

    aget-object p2, p2, p3

    if-ne v1, p2, :cond_2

    iget-object p2, p0, Lc1/h;->b:Ljava/lang/Class;

    invoke-virtual {p2, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_2
    return-object v1
.end method
