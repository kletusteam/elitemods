.class public Ly1/b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:Lmiuix/pickerwidget/widget/NumberPicker;


# direct methods
.method public constructor <init>(Lmiuix/pickerwidget/widget/NumberPicker;)V
    .locals 0

    iput-object p1, p0, Ly1/b;->a:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 1

    if-eqz p2, :cond_0

    iget-object p1, p0, Ly1/b;->a:Lmiuix/pickerwidget/widget/NumberPicker;

    iget-object p1, p1, Lmiuix/pickerwidget/widget/NumberPicker;->t:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->selectAll()V

    goto :goto_0

    :cond_0
    iget-object p2, p0, Ly1/b;->a:Lmiuix/pickerwidget/widget/NumberPicker;

    iget-object p2, p2, Lmiuix/pickerwidget/widget/NumberPicker;->t:Landroid/widget/EditText;

    const/4 v0, 0x0

    invoke-virtual {p2, v0, v0}, Landroid/widget/EditText;->setSelection(II)V

    iget-object p2, p0, Ly1/b;->a:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lmiuix/pickerwidget/widget/NumberPicker;->r()Z

    goto :goto_0

    :cond_1
    invoke-virtual {p2, p1}, Lmiuix/pickerwidget/widget/NumberPicker;->f(Ljava/lang/String;)I

    move-result p1

    const/4 v0, 0x1

    invoke-virtual {p2, p1, v0}, Lmiuix/pickerwidget/widget/NumberPicker;->p(IZ)V

    :goto_0
    return-void
.end method
