.class public Ly1/a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lmiuix/pickerwidget/widget/NumberPicker$h;


# instance fields
.field public final synthetic a:Lmiuix/pickerwidget/widget/DatePicker;


# direct methods
.method public constructor <init>(Lmiuix/pickerwidget/widget/DatePicker;)V
    .locals 0

    iput-object p1, p0, Ly1/a;->a:Lmiuix/pickerwidget/widget/DatePicker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lmiuix/pickerwidget/widget/NumberPicker;II)V
    .locals 6

    iget-object v0, p0, Ly1/a;->a:Lmiuix/pickerwidget/widget/DatePicker;

    iget-object v1, v0, Lmiuix/pickerwidget/widget/DatePicker;->o:Lv1/a;

    iget-object v0, v0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    iget-wide v2, v0, Lv1/a;->f:J

    iput-wide v2, v1, Lv1/a;->f:J

    invoke-virtual {v1}, Lv1/a;->d()V

    iget-object v0, p0, Ly1/a;->a:Lmiuix/pickerwidget/widget/DatePicker;

    iget-object v1, v0, Lmiuix/pickerwidget/widget/DatePicker;->e:Lmiuix/pickerwidget/widget/NumberPicker;

    const/4 v2, 0x1

    const/4 v3, 0x5

    const/16 v4, 0x9

    if-ne p1, v1, :cond_1

    iget-object v1, v0, Lmiuix/pickerwidget/widget/DatePicker;->o:Lv1/a;

    iget-boolean v0, v0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    if-eqz v0, :cond_0

    const/16 v0, 0xa

    goto :goto_0

    :cond_0
    move v0, v4

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lmiuix/pickerwidget/widget/DatePicker;->j:Lmiuix/pickerwidget/widget/NumberPicker;

    if-ne p1, v1, :cond_3

    iget-object v1, v0, Lmiuix/pickerwidget/widget/DatePicker;->o:Lv1/a;

    iget-boolean v0, v0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x6

    goto :goto_0

    :cond_2
    move v0, v3

    :goto_0
    sub-int/2addr p3, p2

    invoke-virtual {v1, v0, p3}, Lv1/a;->a(II)Lv1/a;

    goto :goto_2

    :cond_3
    iget-object p2, v0, Lmiuix/pickerwidget/widget/DatePicker;->p:Lmiuix/pickerwidget/widget/NumberPicker;

    if-ne p1, p2, :cond_7

    iget-object p2, v0, Lmiuix/pickerwidget/widget/DatePicker;->o:Lv1/a;

    iget-boolean v0, v0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x2

    goto :goto_1

    :cond_4
    move v0, v2

    :goto_1
    invoke-virtual {p2, v0, p3}, Lv1/a;->x(II)Lv1/a;

    :goto_2
    iget-object p2, p0, Ly1/a;->a:Lmiuix/pickerwidget/widget/DatePicker;

    iget-object p3, p2, Lmiuix/pickerwidget/widget/DatePicker;->o:Lv1/a;

    invoke-virtual {p3, v2}, Lv1/a;->l(I)I

    move-result p3

    iget-object v0, p0, Ly1/a;->a:Lmiuix/pickerwidget/widget/DatePicker;

    iget-object v0, v0, Lmiuix/pickerwidget/widget/DatePicker;->o:Lv1/a;

    invoke-virtual {v0, v3}, Lv1/a;->l(I)I

    move-result v0

    iget-object v1, p0, Ly1/a;->a:Lmiuix/pickerwidget/widget/DatePicker;

    iget-object v1, v1, Lmiuix/pickerwidget/widget/DatePicker;->o:Lv1/a;

    invoke-virtual {v1, v4}, Lv1/a;->l(I)I

    move-result v1

    invoke-virtual {p2, p3, v0, v1}, Lmiuix/pickerwidget/widget/DatePicker;->d(III)V

    iget-object p2, p0, Ly1/a;->a:Lmiuix/pickerwidget/widget/DatePicker;

    iget-object p3, p2, Lmiuix/pickerwidget/widget/DatePicker;->p:Lmiuix/pickerwidget/widget/NumberPicker;

    if-ne p1, p3, :cond_5

    invoke-virtual {p2}, Lmiuix/pickerwidget/widget/DatePicker;->c()V

    :cond_5
    iget-object p1, p0, Ly1/a;->a:Lmiuix/pickerwidget/widget/DatePicker;

    invoke-virtual {p1}, Lmiuix/pickerwidget/widget/DatePicker;->g()V

    iget-object v1, p0, Ly1/a;->a:Lmiuix/pickerwidget/widget/DatePicker;

    const/4 p1, 0x4

    invoke-virtual {v1, p1}, Landroid/widget/FrameLayout;->sendAccessibilityEvent(I)V

    iget-object v0, v1, Lmiuix/pickerwidget/widget/DatePicker;->l:Lmiuix/pickerwidget/widget/DatePicker$a;

    if-eqz v0, :cond_6

    invoke-virtual {v1}, Lmiuix/pickerwidget/widget/DatePicker;->getYear()I

    move-result v2

    invoke-virtual {v1}, Lmiuix/pickerwidget/widget/DatePicker;->getMonth()I

    move-result v3

    invoke-virtual {v1}, Lmiuix/pickerwidget/widget/DatePicker;->getDayOfMonth()I

    move-result v4

    iget-boolean v5, v1, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    invoke-interface/range {v0 .. v5}, Lmiuix/pickerwidget/widget/DatePicker$a;->a(Lmiuix/pickerwidget/widget/DatePicker;IIIZ)V

    :cond_6
    return-void

    :cond_7
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1
.end method
