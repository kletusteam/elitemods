.class public Lt/c;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroidx/emoji2/text/l;

.field public final b:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroidx/emoji2/text/l;Landroid/os/Handler;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lt/c;->a:Landroidx/emoji2/text/l;

    iput-object p2, p0, Lt/c;->b:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public a(Lt/j$a;)V
    .locals 3

    iget v0, p1, Lt/j$a;->b:I

    if-nez v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    iget-object p1, p1, Lt/j$a;->a:Landroid/graphics/Typeface;

    iget-object v0, p0, Lt/c;->a:Landroidx/emoji2/text/l;

    iget-object v1, p0, Lt/c;->b:Landroid/os/Handler;

    new-instance v2, Lt/a;

    invoke-direct {v2, p0, v0, p1}, Lt/a;-><init>(Lt/c;Landroidx/emoji2/text/l;Landroid/graphics/Typeface;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lt/c;->a:Landroidx/emoji2/text/l;

    iget-object v1, p0, Lt/c;->b:Landroid/os/Handler;

    new-instance v2, Lt/b;

    invoke-direct {v2, p0, p1, v0}, Lt/b;-><init>(Lt/c;Landroidx/emoji2/text/l;I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_1
    return-void
.end method
