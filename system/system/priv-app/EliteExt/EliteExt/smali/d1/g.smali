.class public Ld1/g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnApplyWindowInsetsListener;


# instance fields
.field public final synthetic a:Ld1/e;


# direct methods
.method public constructor <init>(Ld1/e;)V
    .locals 0

    iput-object p1, p0, Ld1/g;->a:Ld1/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onApplyWindowInsets(Landroid/view/View;Landroid/view/WindowInsets;)Landroid/view/WindowInsets;
    .locals 3

    iget-object p1, p0, Ld1/g;->a:Ld1/e;

    iget-object p1, p1, Ld1/e;->K:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result p1

    const/4 v0, 0x0

    cmpl-float p1, p1, v0

    if-eqz p1, :cond_2

    invoke-static {}, Landroid/view/WindowInsets$Type;->ime()I

    move-result p1

    invoke-virtual {p2, p1}, Landroid/view/WindowInsets;->getInsets(I)Landroid/graphics/Insets;

    move-result-object p1

    iget p2, p1, Landroid/graphics/Insets;->bottom:I

    if-lez p2, :cond_2

    iget-object p2, p0, Ld1/g;->a:Ld1/e;

    iget-boolean v0, p2, Ld1/e;->X:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget v0, p2, Ld1/e;->J:I

    if-nez v0, :cond_0

    invoke-static {p2}, Ld1/e;->b(Ld1/e;)I

    move-result v0

    iget v2, p1, Landroid/graphics/Insets;->bottom:I

    sub-int/2addr v0, v2

    iput v0, p2, Ld1/e;->J:I

    iget-object p2, p0, Ld1/g;->a:Ld1/e;

    iget v0, p2, Ld1/e;->J:I

    if-gez v0, :cond_0

    iput v1, p2, Ld1/e;->J:I

    :cond_0
    iget p1, p1, Landroid/graphics/Insets;->bottom:I

    iget-object p2, p0, Ld1/g;->a:Ld1/e;

    iget-boolean v0, p2, Ld1/e;->X:Z

    if-eqz v0, :cond_1

    iget v1, p2, Ld1/e;->J:I

    :cond_1
    sub-int/2addr p1, v1

    neg-int p1, p1

    invoke-static {p2, p1}, Ld1/e;->a(Ld1/e;I)V

    :cond_2
    sget-object p1, Landroid/view/WindowInsets;->CONSUMED:Landroid/view/WindowInsets;

    return-object p1
.end method
