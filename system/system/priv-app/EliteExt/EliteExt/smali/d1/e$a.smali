.class public Ld1/e$a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ld1/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Ld1/e;


# direct methods
.method public constructor <init>(Ld1/e;)V
    .locals 0

    iput-object p1, p0, Ld1/e$a;->a:Ld1/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    sget v0, Lmiuix/view/c;->n:I

    iget-object v1, p0, Ld1/e$a;->a:Ld1/e;

    iget-object v2, v1, Ld1/e;->j:Landroid/widget/Button;

    if-ne p1, v2, :cond_0

    iget-object v2, v1, Ld1/e;->k:Landroid/os/Message;

    if-eqz v2, :cond_0

    invoke-static {v2}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v0

    sget v1, Lmiuix/view/c;->o:I

    move v3, v1

    move-object v1, v0

    move v0, v3

    goto :goto_0

    :cond_0
    iget-object v2, v1, Ld1/e;->d:Landroid/widget/Button;

    if-ne p1, v2, :cond_1

    iget-object v2, v1, Ld1/e;->e:Landroid/os/Message;

    if-eqz v2, :cond_1

    invoke-static {v2}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v1

    goto :goto_0

    :cond_1
    iget-object v2, v1, Ld1/e;->g:Landroid/widget/Button;

    if-ne p1, v2, :cond_2

    iget-object v1, v1, Ld1/e;->h:Landroid/os/Message;

    if-eqz v1, :cond_2

    invoke-static {v1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    invoke-static {p1, v0}, Lmiuix/view/HapticCompat;->performHapticFeedback(Landroid/view/View;I)Z

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    :cond_3
    iget-object p1, p0, Ld1/e$a;->a:Ld1/e;

    iget-object v0, p1, Ld1/e;->y:Landroid/os/Handler;

    const/4 v1, 0x1

    iget-object p1, p1, Ld1/e;->u:Ld/m;

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method
