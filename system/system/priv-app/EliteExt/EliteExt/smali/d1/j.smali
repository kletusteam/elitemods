.class public Ld1/j;
.super Landroidx/fragment/app/i;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ld1/j$b;
    }
.end annotation


# instance fields
.field public m:Ld1/k;


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Landroidx/fragment/app/i;-><init>()V

    new-instance v0, Ld1/k;

    new-instance v1, Ld1/j$b;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Ld1/j$b;-><init>(Ld1/j;Ld1/j$a;)V

    invoke-direct {v0, p0, v1}, Ld1/k;-><init>(Ld1/j;Ld1/d;)V

    iput-object v0, p0, Ld1/j;->m:Ld1/k;

    return-void
.end method

.method public static synthetic A(Ld1/j;ILandroid/view/MenuItem;)Z
    .locals 0

    invoke-super {p0, p1, p2}, Landroidx/fragment/app/i;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result p0

    return p0
.end method

.method public static synthetic B(Ld1/j;I)Landroid/view/View;
    .locals 0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreatePanelView(I)Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic C(Ld1/j;ILandroid/view/Menu;)Z
    .locals 0

    invoke-super {p0, p1, p2}, Landroidx/fragment/app/i;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result p0

    return p0
.end method

.method public static synthetic D(Ld1/j;ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 0

    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/i;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result p0

    return p0
.end method

.method public static synthetic E(Ld1/j;Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Landroidx/fragment/app/i;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public static synthetic v(Ld1/j;Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Landroidx/activity/ComponentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public static synthetic w(Ld1/j;Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Landroidx/fragment/app/i;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public static synthetic x(Ld1/j;Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public static synthetic y(Ld1/j;)V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/i;->onPostResume()V

    return-void
.end method

.method public static synthetic z(Ld1/j;)V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/i;->onStop()V

    return-void
.end method


# virtual methods
.method public addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    iget-object v0, p0, Ld1/j;->m:Ld1/k;

    iget-boolean v1, v0, Ld1/b;->o:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ld1/k;->k()V

    :cond_0
    iget-object v1, v0, Ld1/k;->t:Landroid/view/ViewGroup;

    if-eqz v1, :cond_1

    invoke-virtual {v1, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    iget-object p1, v0, Ld1/k;->s:Ld1/k$b;

    iget-object p1, p1, Lf/h;->a:Landroid/view/Window$Callback;

    invoke-interface {p1}, Landroid/view/Window$Callback;->onContentChanged()V

    return-void
.end method

.method public getMenuInflater()Landroid/view/MenuInflater;
    .locals 3

    iget-object v0, p0, Ld1/j;->m:Ld1/k;

    iget-object v1, v0, Ld1/b;->l:Landroid/view/MenuInflater;

    if-nez v1, :cond_1

    invoke-virtual {v0}, Ld1/b;->f()Ld1/a;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Landroid/view/MenuInflater;

    invoke-virtual {v1}, Ld/a;->d()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v2, v1}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    iput-object v2, v0, Ld1/b;->l:Landroid/view/MenuInflater;

    goto :goto_0

    :cond_0
    new-instance v1, Landroid/view/MenuInflater;

    iget-object v2, v0, Ld1/b;->d:Ld1/j;

    invoke-direct {v1, v2}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    iput-object v1, v0, Ld1/b;->l:Landroid/view/MenuInflater;

    :cond_1
    :goto_0
    iget-object v0, v0, Ld1/b;->l:Landroid/view/MenuInflater;

    return-object v0
.end method

.method public invalidateOptionsMenu()V
    .locals 1

    iget-object v0, p0, Ld1/j;->m:Ld1/k;

    iget-object v0, v0, Ld1/k;->u:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    return-void
.end method

.method public onActionModeFinished(Landroid/view/ActionMode;)V
    .locals 1

    iget-object p1, p0, Ld1/j;->m:Ld1/k;

    const/4 v0, 0x0

    iput-object v0, p1, Ld1/b;->c:Landroid/view/ActionMode;

    return-void
.end method

.method public onActionModeStarted(Landroid/view/ActionMode;)V
    .locals 1

    iget-object v0, p0, Ld1/j;->m:Ld1/k;

    iput-object p1, v0, Ld1/b;->c:Landroid/view/ActionMode;

    return-void
.end method

.method public onBackPressed()V
    .locals 3

    iget-object v0, p0, Ld1/j;->m:Ld1/k;

    iget-object v1, v0, Ld1/b;->c:Landroid/view/ActionMode;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/ActionMode;->finish()V

    goto :goto_2

    :cond_0
    iget-object v1, v0, Ld1/b;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    if-eqz v1, :cond_3

    iget-object v1, v1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->N:Lmiuix/appcompat/internal/app/widget/ActionBarView$h;

    if-eqz v1, :cond_1

    iget-object v2, v1, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->a:Lh1/i;

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_3

    if-nez v1, :cond_2

    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    iget-object v0, v1, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->a:Lh1/i;

    :goto_1
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lh1/i;->collapseActionView()Z

    goto :goto_2

    :cond_3
    iget-object v0, v0, Ld1/k;->r:Ld1/d;

    check-cast v0, Ld1/j$b;

    iget-object v0, v0, Ld1/j$b;->a:Ld1/j;

    iget-object v0, v0, Landroidx/activity/ComponentActivity;->f:Landroidx/activity/OnBackPressedDispatcher;

    invoke-virtual {v0}, Landroidx/activity/OnBackPressedDispatcher;->b()V

    :cond_4
    :goto_2
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    iget-object v0, p0, Ld1/j;->m:Ld1/k;

    iget-boolean v1, v0, Ld1/b;->f:Z

    if-eqz v1, :cond_0

    iget-boolean v1, v0, Ld1/b;->o:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ld1/b;->f()Ld1/a;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/internal/app/widget/c;

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Lmiuix/appcompat/internal/app/widget/c;->e(Landroid/content/res/Configuration;)V

    :cond_0
    iget-object v0, v0, Ld1/k;->r:Ld1/d;

    check-cast v0, Ld1/j$b;

    iget-object v0, v0, Ld1/j$b;->a:Ld1/j;

    invoke-static {v0, p1}, Ld1/j;->E(Ld1/j;Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Ld1/j;->m:Ld1/k;

    iget-object v1, v0, Ld1/k;->r:Ld1/d;

    check-cast v1, Ld1/j$b;

    iget-object v1, v1, Ld1/j$b;->a:Ld1/j;

    invoke-static {v1, p1}, Ld1/j;->w(Ld1/j;Landroid/os/Bundle;)V

    invoke-virtual {v0}, Ld1/k;->k()V

    return-void
.end method

.method public onCreatePanelMenu(ILandroid/view/Menu;)Z
    .locals 1

    iget-object v0, p0, Ld1/j;->m:Ld1/k;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p1, :cond_0

    iget-object v0, v0, Ld1/k;->r:Ld1/d;

    check-cast v0, Ld1/j$b;

    iget-object v0, v0, Ld1/j$b;->a:Ld1/j;

    invoke-static {v0, p1, p2}, Ld1/j;->C(Ld1/j;ILandroid/view/Menu;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public onCreatePanelView(I)Landroid/view/View;
    .locals 5

    iget-object v0, p0, Ld1/j;->m:Ld1/k;

    if-eqz p1, :cond_0

    iget-object v0, v0, Ld1/k;->r:Ld1/d;

    check-cast v0, Ld1/j$b;

    iget-object v0, v0, Ld1/j$b;->a:Ld1/j;

    invoke-static {v0, p1}, Ld1/j;->B(Ld1/j;I)Landroid/view/View;

    move-result-object p1

    goto :goto_2

    :cond_0
    iget-boolean p1, v0, Ld1/b;->j:Z

    const/4 v1, 0x0

    if-nez p1, :cond_6

    iget-object p1, v0, Ld1/b;->k:Lh1/g;

    iget-object v2, v0, Ld1/b;->c:Landroid/view/ActionMode;

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-nez v2, :cond_2

    if-nez p1, :cond_1

    invoke-virtual {v0}, Ld1/b;->e()Lh1/g;

    move-result-object p1

    invoke-virtual {v0, p1}, Ld1/b;->j(Lh1/g;)V

    invoke-virtual {p1}, Lh1/g;->A()V

    iget-object v2, v0, Ld1/k;->r:Ld1/d;

    check-cast v2, Ld1/j$b;

    invoke-virtual {v2, v4, p1}, Ld1/j$b;->a(ILandroid/view/Menu;)Z

    move-result v3

    :cond_1
    if-eqz v3, :cond_4

    invoke-virtual {p1}, Lh1/g;->A()V

    iget-object v2, v0, Ld1/k;->r:Ld1/d;

    check-cast v2, Ld1/j$b;

    invoke-virtual {v2, v4, v1, p1}, Ld1/j$b;->b(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v3

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    goto :goto_0

    :cond_3
    move v3, v4

    :cond_4
    :goto_0
    if-eqz v3, :cond_5

    invoke-virtual {p1}, Lh1/g;->z()V

    goto :goto_1

    :cond_5
    invoke-virtual {v0, v1}, Ld1/b;->j(Lh1/g;)V

    :cond_6
    :goto_1
    move-object p1, v1

    :goto_2
    return-object p1
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 1

    iget-object v0, p0, Ld1/j;->m:Ld1/k;

    invoke-virtual {v0, p1, p2}, Ld1/k;->g(ILandroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public onPostResume()V
    .locals 2

    iget-object v0, p0, Ld1/j;->m:Ld1/k;

    iget-object v1, v0, Ld1/k;->r:Ld1/d;

    check-cast v1, Ld1/j$b;

    iget-object v1, v1, Ld1/j$b;->a:Ld1/j;

    invoke-static {v1}, Ld1/j;->y(Ld1/j;)V

    invoke-virtual {v0}, Ld1/b;->f()Ld1/a;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/c;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiuix/appcompat/internal/app/widget/c;->h(Z)V

    :cond_0
    return-void
.end method

.method public onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    iget-object v0, p0, Ld1/j;->m:Ld1/k;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p1, :cond_0

    iget-object v0, v0, Ld1/k;->r:Ld1/d;

    check-cast v0, Ld1/j$b;

    iget-object v0, v0, Ld1/j$b;->a:Ld1/j;

    invoke-static {v0, p1, p2, p3}, Ld1/j;->D(Ld1/j;ILandroid/view/View;Landroid/view/Menu;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Ld1/j;->m:Ld1/k;

    iget-object v1, v0, Ld1/k;->r:Ld1/d;

    check-cast v1, Ld1/j$b;

    iget-object v1, v1, Ld1/j$b;->a:Ld1/j;

    invoke-static {v1, p1}, Ld1/j;->x(Ld1/j;Landroid/os/Bundle;)V

    iget-object v1, v0, Ld1/k;->q:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v1, :cond_0

    const-string v1, "miuix:ActionBar"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object v0, v0, Ld1/k;->q:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->restoreHierarchyState(Landroid/util/SparseArray;)V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Ld1/j;->m:Ld1/k;

    iget-object v1, v0, Ld1/k;->r:Ld1/d;

    check-cast v1, Ld1/j$b;

    iget-object v1, v1, Ld1/j$b;->a:Ld1/j;

    invoke-static {v1, p1}, Ld1/j;->v(Ld1/j;Landroid/os/Bundle;)V

    iget-object v1, v0, Ld1/k;->q:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v1, :cond_0

    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iget-object v0, v0, Ld1/k;->q:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->saveHierarchyState(Landroid/util/SparseArray;)V

    const-string v0, "miuix:ActionBar"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    iget-object v0, p0, Ld1/j;->m:Ld1/k;

    iget-object v1, v0, Ld1/k;->r:Ld1/d;

    check-cast v1, Ld1/j$b;

    iget-object v1, v1, Ld1/j$b;->a:Ld1/j;

    invoke-static {v1}, Ld1/j;->z(Ld1/j;)V

    iget-object v1, v0, Ld1/b;->m:Lh1/d;

    if-eqz v1, :cond_0

    check-cast v1, Lh1/e;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V

    :cond_0
    invoke-virtual {v0}, Ld1/b;->f()Ld1/a;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/c;

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiuix/appcompat/internal/app/widget/c;->h(Z)V

    :cond_1
    return-void
.end method

.method public onTitleChanged(Ljava/lang/CharSequence;I)V
    .locals 0

    iget-object p2, p0, Ld1/j;->m:Ld1/k;

    iget-object p2, p2, Ld1/b;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    if-eqz p2, :cond_0

    invoke-virtual {p2, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setWindowTitle(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public onWindowStartingActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .locals 1

    iget-object v0, p0, Ld1/j;->m:Ld1/k;

    invoke-virtual {v0, p1}, Ld1/k;->l(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object p1

    return-object p1
.end method

.method public onWindowStartingActionMode(Landroid/view/ActionMode$Callback;I)Landroid/view/ActionMode;
    .locals 1

    iget-object v0, p0, Ld1/j;->m:Ld1/k;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p2, :cond_0

    invoke-virtual {v0, p1}, Ld1/k;->l(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public setContentView(I)V
    .locals 3

    iget-object v0, p0, Ld1/j;->m:Ld1/k;

    iget-boolean v1, v0, Ld1/b;->o:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ld1/k;->k()V

    :cond_0
    iget-object v1, v0, Ld1/k;->t:Landroid/view/ViewGroup;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v1, v0, Ld1/k;->v:Landroid/view/LayoutInflater;

    iget-object v2, v0, Ld1/k;->t:Landroid/view/ViewGroup;

    invoke-virtual {v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    :cond_1
    iget-object p1, v0, Ld1/k;->s:Ld1/k$b;

    iget-object p1, p1, Lf/h;->a:Landroid/view/Window$Callback;

    invoke-interface {p1}, Landroid/view/Window$Callback;->onContentChanged()V

    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Ld1/j;->m:Ld1/k;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, p1, v1}, Ld1/k;->m(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    iget-object v0, p0, Ld1/j;->m:Ld1/k;

    invoke-virtual {v0, p1, p2}, Ld1/k;->m(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .locals 2

    iget-object v0, p0, Ld1/j;->m:Ld1/k;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    instance-of v1, p1, Lmiuix/view/e$a;

    if-eqz v1, :cond_0

    iget-object v1, v0, Ld1/k;->w:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, v1}, Ld1/b;->a(Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;)V

    :cond_0
    iget-object v0, v0, Ld1/k;->w:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method
