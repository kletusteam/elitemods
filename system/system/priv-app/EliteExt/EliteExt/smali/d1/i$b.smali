.class public Ld1/i$b;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ld1/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field public final a:Ld1/e$b;

.field public final b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    const/4 v0, 0x0

    invoke-static {p1, v0}, Ld1/i;->e(Landroid/content/Context;I)I

    move-result v0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Ld1/e$b;

    new-instance v2, Landroid/view/ContextThemeWrapper;

    invoke-static {p1, v0}, Ld1/i;->e(Landroid/content/Context;I)I

    move-result v3

    invoke-direct {v2, p1, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v1, v2}, Ld1/e$b;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Ld1/i$b;->a:Ld1/e$b;

    iput v0, p0, Ld1/i$b;->b:I

    return-void
.end method


# virtual methods
.method public a()Ld1/i;
    .locals 10

    new-instance v0, Ld1/i;

    iget-object v1, p0, Ld1/i$b;->a:Ld1/e$b;

    iget-object v1, v1, Ld1/e$b;->b:Landroid/content/Context;

    iget v2, p0, Ld1/i$b;->b:I

    invoke-direct {v0, v1, v2}, Ld1/i;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Ld1/i$b;->a:Ld1/e$b;

    iget-object v2, v0, Ld1/i;->c:Ld1/e;

    iget-object v3, v1, Ld1/e$b;->c:Landroid/view/View;

    if-eqz v3, :cond_0

    iput-object v3, v2, Ld1/e;->t:Landroid/view/View;

    goto :goto_0

    :cond_0
    iget-object v3, v1, Ld1/e$b;->m:Ljava/lang/CharSequence;

    if-eqz v3, :cond_1

    iput-object v3, v2, Ld1/e;->V:Ljava/lang/CharSequence;

    iget-object v4, v2, Ld1/e;->W:Landroid/widget/TextView;

    if-eqz v4, :cond_1

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v3, v1, Ld1/e$b;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_2

    iput-object v3, v2, Ld1/e;->z:Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    iput v3, v2, Ld1/e;->A:I

    :cond_2
    :goto_0
    iget-object v3, v1, Ld1/e$b;->f:Ljava/lang/CharSequence;

    if-eqz v3, :cond_3

    iput-object v3, v2, Ld1/e;->H:Ljava/lang/CharSequence;

    iget-object v4, v2, Ld1/e;->I:Landroid/widget/TextView;

    if-eqz v4, :cond_3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, v2, Ld1/e;->I:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Ld1/e;->e(Landroid/widget/TextView;)V

    :cond_3
    iget-object v3, v1, Ld1/e$b;->l:Ljava/lang/CharSequence;

    const/4 v4, -0x1

    const/4 v5, 0x0

    if-eqz v3, :cond_4

    iget-object v6, v1, Ld1/e$b;->k:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v4, v3, v6, v5}, Ld1/e;->n(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    :cond_4
    iget-object v3, v1, Ld1/e$b;->h:Ljava/lang/CharSequence;

    if-eqz v3, :cond_5

    const/4 v6, -0x2

    iget-object v7, v1, Ld1/e$b;->g:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v6, v3, v7, v5}, Ld1/e;->n(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    :cond_5
    iget-object v3, v1, Ld1/e$b;->a:Landroid/widget/ListAdapter;

    if-eqz v3, :cond_8

    iget-object v3, v1, Ld1/e$b;->e:Landroid/view/LayoutInflater;

    iget v6, v2, Ld1/e;->F:I

    invoke-virtual {v3, v6, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    iget v6, v2, Ld1/e;->E:I

    iget-object v7, v1, Ld1/e$b;->a:Landroid/widget/ListAdapter;

    if-eqz v7, :cond_6

    goto :goto_1

    :cond_6
    new-instance v7, Ld1/e$d;

    iget-object v8, v1, Ld1/e$b;->b:Landroid/content/Context;

    const v9, 0x1020014

    invoke-direct {v7, v8, v6, v9, v5}, Ld1/e$d;-><init>(Landroid/content/Context;II[Ljava/lang/CharSequence;)V

    :goto_1
    iput-object v7, v2, Ld1/e;->a:Landroid/widget/ListAdapter;

    iput v4, v2, Ld1/e;->p:I

    iget-object v4, v1, Ld1/e$b;->i:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v4, :cond_7

    new-instance v4, Ld1/h;

    invoke-direct {v4, v1, v2}, Ld1/h;-><init>(Ld1/e$b;Ld1/e;)V

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    :cond_7
    iput-object v3, v2, Ld1/e;->G:Landroid/widget/ListView;

    :cond_8
    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x1

    iput-boolean v1, v2, Ld1/e;->C:Z

    iget-object v2, p0, Ld1/i$b;->a:Ld1/e$b;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ld1/i;->setCancelable(Z)V

    iget-object v2, p0, Ld1/i$b;->a:Ld1/e$b;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ld1/i;->setCanceledOnTouchOutside(Z)V

    iget-object v1, p0, Ld1/i$b;->a:Ld1/e$b;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0, v5}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    iget-object v1, p0, Ld1/i$b;->a:Ld1/e$b;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0, v5}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    iget-object v1, p0, Ld1/i$b;->a:Ld1/e$b;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0, v5}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    iget-object v1, p0, Ld1/i$b;->a:Ld1/e$b;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Ld1/i;->c:Ld1/e;

    iput-object v5, v1, Ld1/e;->U:Ld1/i$c;

    iget-object v1, p0, Ld1/i$b;->a:Ld1/e$b;

    iget-object v1, v1, Ld1/e$b;->j:Landroid/content/DialogInterface$OnKeyListener;

    if-eqz v1, :cond_9

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    :cond_9
    return-object v0
.end method
