.class public Ld1/k;
.super Ld1/b;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ld1/k$b;
    }
.end annotation


# instance fields
.field public q:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

.field public r:Ld1/d;

.field public s:Ld1/k$b;

.field public t:Landroid/view/ViewGroup;

.field public final u:Ljava/lang/Runnable;

.field public v:Landroid/view/LayoutInflater;

.field public w:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

.field public x:Landroid/view/Window;


# direct methods
.method public constructor <init>(Ld1/j;Ld1/d;)V
    .locals 0

    invoke-direct {p0, p1}, Ld1/b;-><init>(Ld1/j;)V

    new-instance p1, Ld1/k$a;

    invoke-direct {p1, p0}, Ld1/k$a;-><init>(Ld1/k;)V

    iput-object p1, p0, Ld1/k;->u:Ljava/lang/Runnable;

    iput-object p2, p0, Ld1/k;->r:Ld1/d;

    return-void
.end method


# virtual methods
.method public d(Lh1/g;Landroid/view/MenuItem;)Z
    .locals 1

    iget-object p1, p0, Ld1/b;->d:Ld1/j;

    const/4 v0, 0x0

    invoke-virtual {p1, v0, p2}, Ld1/j;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public g(ILandroid/view/MenuItem;)Z
    .locals 1

    iget-object v0, p0, Ld1/k;->r:Ld1/d;

    check-cast v0, Ld1/j$b;

    iget-object v0, v0, Ld1/j$b;->a:Ld1/j;

    invoke-static {v0, p1, p2}, Ld1/j;->A(Ld1/j;ILandroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 v0, 0x0

    if-eqz p1, :cond_1

    return v0

    :cond_1
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result p1

    const p2, 0x102002c

    if-ne p1, p2, :cond_3

    invoke-virtual {p0}, Ld1/b;->f()Ld1/a;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Ld1/b;->f()Ld1/a;

    move-result-object p1

    invoke-virtual {p1}, Ld/a;->c()I

    move-result p1

    and-int/lit8 p1, p1, 0x4

    if-eqz p1, :cond_3

    iget-object p1, p0, Ld1/b;->d:Ld1/j;

    invoke-virtual {p1}, Landroid/app/Activity;->getParent()Landroid/app/Activity;

    move-result-object p1

    if-nez p1, :cond_2

    iget-object p1, p0, Ld1/b;->d:Ld1/j;

    invoke-virtual {p1}, Landroid/app/Activity;->onNavigateUp()Z

    move-result p1

    goto :goto_0

    :cond_2
    iget-object p1, p0, Ld1/b;->d:Ld1/j;

    invoke-virtual {p1}, Landroid/app/Activity;->getParent()Landroid/app/Activity;

    move-result-object p1

    iget-object p2, p0, Ld1/b;->d:Ld1/j;

    invoke-virtual {p1, p2}, Landroid/app/Activity;->onNavigateUpFromChild(Landroid/app/Activity;)Z

    move-result p1

    :goto_0
    if-nez p1, :cond_3

    iget-object p1, p0, Ld1/b;->d:Ld1/j;

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    :cond_3
    return v0
.end method

.method public final k()V
    .locals 12

    iget-boolean v0, p0, Ld1/b;->o:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Ld1/k;->x:Landroid/view/Window;

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    if-nez v0, :cond_4

    iget-object v0, p0, Ld1/b;->d:Ld1/j;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    iget-object v1, p0, Ld1/k;->x:Landroid/view/Window;

    const-string v2, "AppCompat has already installed itself into the Window"

    if-nez v1, :cond_3

    invoke-virtual {v0}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v1

    instance-of v3, v1, Ld1/k$b;

    if-nez v3, :cond_2

    new-instance v2, Ld1/k$b;

    invoke-direct {v2, p0, v1}, Ld1/k$b;-><init>(Ld1/k;Landroid/view/Window$Callback;)V

    iput-object v2, p0, Ld1/k;->s:Ld1/k$b;

    invoke-virtual {v0, v2}, Landroid/view/Window;->setCallback(Landroid/view/Window$Callback;)V

    iput-object v0, p0, Ld1/k;->x:Landroid/view/Window;

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    :goto_0
    iget-object v0, p0, Ld1/k;->x:Landroid/view/Window;

    if-eqz v0, :cond_21

    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Ld1/b;->o:Z

    iget-object v1, p0, Ld1/b;->d:Ld1/j;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    iput-object v2, p0, Ld1/k;->v:Landroid/view/LayoutInflater;

    iget-object v2, p0, Ld1/b;->d:Ld1/j;

    sget-object v3, Landroidx/emoji2/text/l;->m0:[I

    invoke-virtual {v2, v3}, Landroid/app/Activity;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v2

    const/16 v3, 0x12

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    if-ne v3, v0, :cond_5

    iget-object v3, p0, Ld1/b;->d:Ld1/j;

    invoke-virtual {v3}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/16 v5, 0x50

    invoke-virtual {v3, v5}, Landroid/view/Window;->setGravity(I)V

    :cond_5
    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v5

    if-eqz v5, :cond_20

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    const/16 v5, 0x8

    if-eqz v3, :cond_6

    invoke-virtual {p0, v5}, Ld1/b;->h(I)Z

    :cond_6
    const/16 v3, 0xc

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0x9

    invoke-virtual {p0, v3}, Ld1/b;->h(I)Z

    :cond_7
    const/16 v3, 0x18

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    iget-object v6, p0, Ld1/b;->d:Ld1/j;

    invoke-virtual {v6}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0018

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    if-ltz v6, :cond_8

    const/4 v7, 0x2

    if-gt v6, v7, :cond_8

    move v3, v6

    :cond_8
    iget v6, p0, Ld1/b;->p:I

    if-eq v6, v3, :cond_9

    iget-object v6, p0, Ld1/b;->d:Ld1/j;

    invoke-virtual {v6}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-static {v6, v3}, Lo1/a;->a(Landroid/view/Window;I)Z

    move-result v6

    if-eqz v6, :cond_9

    iput v3, p0, Ld1/b;->p:I

    :cond_9
    iget-object v3, p0, Ld1/b;->d:Ld1/j;

    invoke-virtual {v1}, Landroid/view/Window;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f040251

    invoke-static {v6, v7, v4}, Lr1/a;->c(Landroid/content/Context;IZ)Z

    move-result v8

    if-eqz v8, :cond_b

    const v8, 0x7f040252

    invoke-static {v6, v8, v4}, Lr1/a;->c(Landroid/content/Context;IZ)Z

    move-result v8

    if-eqz v8, :cond_a

    const v8, 0x7f0d0057

    goto :goto_2

    :cond_a
    const v8, 0x7f0d0054

    goto :goto_2

    :cond_b
    const v8, 0x7f0d005a

    :goto_2
    const v9, 0x7f0401fa

    invoke-static {v6, v9}, Lr1/a;->b(Landroid/content/Context;I)I

    move-result v9

    if-lez v9, :cond_c

    iget-object v10, p0, Ld1/b;->d:Ld1/j;

    invoke-virtual {v10}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v10

    iget-object v10, v10, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v11, "android"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    invoke-static {v6, v7, v0}, Lr1/a;->c(Landroid/content/Context;IZ)Z

    move-result v7

    if-eqz v7, :cond_c

    move v8, v9

    :cond_c
    invoke-virtual {v1}, Landroid/view/Window;->isFloating()Z

    move-result v7

    if-nez v7, :cond_d

    invoke-virtual {v1}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v7

    instance-of v7, v7, Landroid/app/Dialog;

    if-eqz v7, :cond_d

    const v7, 0x7f040265

    invoke-static {v6, v7, v4}, Lr1/a;->e(Landroid/content/Context;II)I

    move-result v6

    invoke-static {v1, v6}, Lo1/a;->a(Landroid/view/Window;I)Z

    :cond_d
    const/4 v6, 0x0

    invoke-static {v3, v8, v6}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    instance-of v7, v3, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    const v8, 0x1020002

    if-eqz v7, :cond_f

    move-object v7, v3

    check-cast v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    iput-object v7, p0, Ld1/k;->w:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {v7, v8}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    invoke-virtual {v1, v8}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/view/ViewGroup;

    if-eqz v9, :cond_f

    :goto_3
    invoke-virtual {v9}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v10

    if-lez v10, :cond_e

    invoke-virtual {v9, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    invoke-virtual {v9, v4}, Landroid/view/ViewGroup;->removeViewAt(I)V

    invoke-virtual {v7, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_3

    :cond_e
    const/4 v10, -0x1

    invoke-virtual {v9, v10}, Landroid/view/ViewGroup;->setId(I)V

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->setId(I)V

    instance-of v7, v9, Landroid/widget/FrameLayout;

    if-eqz v7, :cond_f

    check-cast v9, Landroid/widget/FrameLayout;

    invoke-virtual {v9, v6}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    :cond_f
    invoke-virtual {v1, v3}, Landroid/view/Window;->setContentView(Landroid/view/View;)V

    iget-object v1, p0, Ld1/k;->w:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz v1, :cond_10

    invoke-virtual {v1, v8}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Ld1/k;->t:Landroid/view/ViewGroup;

    :cond_10
    iget-object v1, p0, Ld1/k;->w:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz v1, :cond_11

    iget-object v3, p0, Ld1/b;->d:Ld1/j;

    invoke-virtual {v1, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->setCallback(Landroid/view/Window$Callback;)V

    iget-object v1, p0, Ld1/k;->w:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    iget v3, p0, Ld1/b;->p:I

    invoke-virtual {v1, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->setTranslucentStatus(I)V

    :cond_11
    iget-boolean v1, p0, Ld1/b;->f:Z

    if-eqz v1, :cond_1c

    iget-object v1, p0, Ld1/k;->w:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz v1, :cond_1c

    const v3, 0x7f0a002b

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    iput-object v1, p0, Ld1/k;->q:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    iget-object v1, p0, Ld1/k;->w:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    iget-boolean v7, p0, Ld1/b;->n:Z

    invoke-virtual {v1, v7}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->setOverlayMode(Z)V

    iget-object v1, p0, Ld1/k;->w:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    const v7, 0x7f0a0027

    invoke-virtual {v1, v7}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iput-object v1, p0, Ld1/b;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object v7, p0, Ld1/b;->d:Ld1/j;

    invoke-virtual {v1, v7}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setWindowCallback(Landroid/view/Window$Callback;)V

    iget-boolean v1, p0, Ld1/b;->e:Z

    if-eqz v1, :cond_12

    iget-object v1, p0, Ld1/b;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v7, Landroid/widget/ProgressBar;

    iget-object v8, v1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->D:Landroid/content/Context;

    const v9, 0x7f040012

    invoke-direct {v7, v8, v6, v9}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v7, v1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->b0:Landroid/widget/ProgressBar;

    const v8, 0x7f0a00ba

    invoke-virtual {v7, v8}, Landroid/widget/ProgressBar;->setId(I)V

    iget-object v7, v1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->b0:Landroid/widget/ProgressBar;

    invoke-virtual {v7, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v5, v1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->b0:Landroid/widget/ProgressBar;

    invoke-virtual {v5, v0}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    iget-object v5, v1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->b0:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_12
    const/4 v1, 0x5

    invoke-virtual {v2, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Ld1/b;->h:I

    iget-boolean v5, p0, Ld1/b;->j:Z

    if-eqz v5, :cond_13

    iget-object v5, p0, Ld1/b;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v5, v1, p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->r(ILd1/b;)V

    :cond_13
    iget-object v1, p0, Ld1/b;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getCustomNavigationView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_14

    iget-object v1, p0, Ld1/b;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getDisplayOptions()I

    move-result v5

    or-int/lit8 v5, v5, 0x10

    invoke-virtual {v1, v5}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setDisplayOptions(I)V

    :cond_14
    :try_start_0
    iget-object v1, p0, Ld1/b;->d:Ld1/j;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v5, p0, Ld1/b;->d:Ld1/j;

    invoke-virtual {v5}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v5

    const/16 v7, 0x80

    invoke-virtual {v1, v5, v7}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    if-eqz v1, :cond_15

    const-string v5, "android.support.UI_OPTIONS"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    :catch_0
    const-string v1, "getUiOptionsFromMetadata: Activity \'"

    invoke-static {v1}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v5, p0, Ld1/b;->d:Ld1/j;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "\' not in manifest"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v5, "ActionBarDelegate"

    invoke-static {v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_15
    :goto_4
    const-string v1, "splitActionBarWhenNarrow"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    iget-object v5, p0, Ld1/b;->d:Ld1/j;

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f050004

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v5

    goto :goto_5

    :cond_16
    const/16 v5, 0x17

    invoke-virtual {v2, v5, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v5

    :goto_5
    if-eqz v5, :cond_1b

    iget-object v6, p0, Ld1/k;->w:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    iget-boolean v7, p0, Ld1/b;->g:Z

    if-eqz v7, :cond_17

    goto :goto_8

    :cond_17
    iput-boolean v0, p0, Ld1/b;->g:Z

    const v7, 0x7f0a00e6

    invoke-virtual {v6, v7}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewStub;

    if-eqz v7, :cond_18

    invoke-virtual {v7}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v7

    goto :goto_6

    :cond_18
    const v7, 0x7f0a00e5

    invoke-virtual {v6, v7}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    :goto_6
    check-cast v7, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v7, :cond_19

    iget-object v8, p0, Ld1/b;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v8, v7}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setSplitView(Lmiuix/appcompat/internal/app/widget/ActionBarContainer;)V

    iget-object v8, p0, Ld1/b;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v8, v5}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setSplitActionBar(Z)V

    iget-object v8, p0, Ld1/b;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v8, v1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setSplitWhenNarrow(Z)V

    invoke-virtual {v6, v7}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->setSplitActionBarView(Lmiuix/appcompat/internal/app/widget/ActionBarContainer;)V

    invoke-virtual {p0, v6}, Ld1/b;->a(Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;)V

    :cond_19
    invoke-virtual {v6, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    const v8, 0x7f0a0036

    invoke-virtual {v6, v8}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewStub;

    if-eqz v8, :cond_1a

    invoke-virtual {v8}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v8

    goto :goto_7

    :cond_1a
    const v8, 0x7f0a0035

    invoke-virtual {v6, v8}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    :goto_7
    check-cast v8, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    if-eqz v8, :cond_1b

    invoke-virtual {v3, v8}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setActionBarContextView(Lmiuix/appcompat/internal/app/widget/ActionBarContextView;)V

    invoke-virtual {v6, v8}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->setActionBarContextView(Lmiuix/appcompat/internal/app/widget/ActionBarContextView;)V

    if-eqz v7, :cond_1b

    invoke-virtual {v8, v7}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->setSplitView(Lmiuix/appcompat/internal/app/widget/ActionBarContainer;)V

    invoke-virtual {v8, v5}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->setSplitActionBar(Z)V

    invoke-virtual {v8, v1}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->setSplitWhenNarrow(Z)V

    :cond_1b
    :goto_8
    iget-object v1, p0, Ld1/b;->d:Ld1/j;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    iget-object v3, p0, Ld1/k;->u:Ljava/lang/Runnable;

    invoke-virtual {v1, v3}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    :cond_1c
    const/4 v1, 0x4

    invoke-virtual {v2, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_1f

    iput-boolean v0, p0, Ld1/b;->j:Z

    iget-boolean v1, p0, Ld1/b;->o:Z

    if-eqz v1, :cond_1f

    iget-boolean v1, p0, Ld1/b;->f:Z

    if-eqz v1, :cond_1f

    iget-object v1, p0, Ld1/b;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object v1, v1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->U:Landroid/view/View;

    if-eqz v1, :cond_1d

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_9

    :cond_1d
    move v0, v4

    :goto_9
    if-nez v0, :cond_1e

    iget-object v0, p0, Ld1/b;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget v1, p0, Ld1/b;->h:I

    invoke-virtual {v0, v1, p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->r(ILd1/b;)V

    :cond_1e
    iget-object v0, p0, Ld1/k;->u:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_1f
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :cond_20
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You need to use a miui theme (or descendant) with this activity."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_21
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "We have not been given a Window"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public l(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .locals 7

    invoke-virtual {p0}, Ld1/b;->f()Ld1/a;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_f

    invoke-virtual {p0}, Ld1/b;->f()Ld1/a;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/c;

    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/c;->a:Landroid/view/ActionMode;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/ActionMode;->finish()V

    :cond_0
    instance-of v2, p1, Lmiuix/view/e$a;

    if-eqz v2, :cond_1

    new-instance v3, Lg1/c;

    iget-object v4, v0, Lmiuix/appcompat/internal/app/widget/c;->j:Landroid/content/Context;

    invoke-direct {v3, v4, p1}, Lg1/c;-><init>(Landroid/content/Context;Landroid/view/ActionMode$Callback;)V

    goto :goto_0

    :cond_1
    new-instance v3, Lg1/b;

    iget-object v4, v0, Lmiuix/appcompat/internal/app/widget/c;->j:Landroid/content/Context;

    invoke-direct {v3, v4, p1}, Lg1/b;-><init>(Landroid/content/Context;Landroid/view/ActionMode$Callback;)V

    :goto_0
    iget-object p1, v0, Lmiuix/appcompat/internal/app/widget/c;->c:Lmiuix/appcompat/internal/app/widget/f;

    instance-of v4, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    if-eqz v4, :cond_2

    instance-of v4, v3, Lg1/c;

    if-nez v4, :cond_3

    :cond_2
    instance-of v4, p1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    if-eqz v4, :cond_4

    instance-of v4, v3, Lg1/b;

    if-eqz v4, :cond_4

    :cond_3
    invoke-interface {p1}, Lmiuix/appcompat/internal/app/widget/f;->d()V

    iget-object p1, v0, Lmiuix/appcompat/internal/app/widget/c;->c:Lmiuix/appcompat/internal/app/widget/f;

    invoke-interface {p1}, Lmiuix/appcompat/internal/app/widget/f;->b()V

    :cond_4
    const/4 p1, 0x0

    const-string v4, "not set windowSplitActionBar true in activity style!"

    if-eqz v2, :cond_8

    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/c;->s:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    if-nez v2, :cond_5

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/c;->d()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    iget-object v5, v0, Lmiuix/appcompat/internal/app/widget/c;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    const v6, 0x7f0d005b

    invoke-virtual {v2, v6, v5, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    new-instance v5, Lmiuix/appcompat/internal/app/widget/e;

    invoke-direct {v5, v0}, Lmiuix/appcompat/internal/app/widget/e;-><init>(Lmiuix/appcompat/internal/app/widget/c;)V

    invoke-virtual {v2, v5}, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->setOnBackClickListener(Landroid/view/View$OnClickListener;)V

    iput-object v2, v0, Lmiuix/appcompat/internal/app/widget/c;->s:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    :cond_5
    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/c;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v2}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->getPendingInsets()Landroid/graphics/Rect;

    move-result-object v2

    if-eqz v2, :cond_6

    iget-object v5, v0, Lmiuix/appcompat/internal/app/widget/c;->s:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    invoke-virtual {v5, v2}, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->setStatusBarPaddingTop(I)V

    :cond_6
    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/c;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    iget-object v5, v0, Lmiuix/appcompat/internal/app/widget/c;->s:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    if-eq v2, v5, :cond_7

    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/c;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    iget-object v5, v0, Lmiuix/appcompat/internal/app/widget/c;->s:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    invoke-virtual {v2, v5}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    :cond_7
    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/c;->s:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    goto :goto_1

    :cond_8
    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/c;->l:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    if-eqz v2, :cond_e

    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/c;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v2}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->getPendingInsets()Landroid/graphics/Rect;

    move-result-object v2

    if-eqz v2, :cond_9

    iget-object v5, v0, Lmiuix/appcompat/internal/app/widget/c;->l:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    invoke-virtual {v5, v2}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->setContentInset(I)V

    :cond_9
    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/c;->l:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    :goto_1
    iput-object v2, v0, Lmiuix/appcompat/internal/app/widget/c;->c:Lmiuix/appcompat/internal/app/widget/f;

    if-eqz v2, :cond_d

    invoke-interface {v2, v3}, Lmiuix/appcompat/internal/app/widget/f;->a(Lmiuix/view/a;)V

    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v4, v3, Lg1/a;->b:Ljava/lang/ref/WeakReference;

    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/c;->b:Lg1/a$a;

    iput-object v2, v3, Lg1/a;->a:Lg1/a$a;

    iget-object v2, v3, Lg1/a;->f:Lh1/g;

    invoke-virtual {v2}, Lh1/g;->A()V

    :try_start_0
    iget-object v2, v3, Lg1/a;->c:Landroid/view/ActionMode$Callback;

    iget-object v4, v3, Lg1/a;->f:Lh1/g;

    invoke-interface {v2, v3, v4}, Landroid/view/ActionMode$Callback;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v4, v3, Lg1/a;->f:Lh1/g;

    invoke-virtual {v4}, Lh1/g;->z()V

    if-eqz v2, :cond_c

    invoke-virtual {v3}, Lg1/a;->invalidate()V

    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/c;->c:Lmiuix/appcompat/internal/app/widget/f;

    invoke-interface {v1, v3}, Lmiuix/appcompat/internal/app/widget/f;->c(Landroid/view/ActionMode;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiuix/appcompat/internal/app/widget/c;->k(Z)V

    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/c;->w:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v2, :cond_a

    iget v4, v0, Lmiuix/appcompat/internal/app/widget/c;->k:I

    if-ne v4, v1, :cond_a

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_a

    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/c;->w:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v1, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setVisibility(I)V

    :cond_a
    iget-object p1, v0, Lmiuix/appcompat/internal/app/widget/c;->c:Lmiuix/appcompat/internal/app/widget/f;

    instance-of v1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    if-eqz v1, :cond_b

    check-cast p1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    const/16 v1, 0x20

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->sendAccessibilityEvent(I)V

    :cond_b
    iput-object v3, v0, Lmiuix/appcompat/internal/app/widget/c;->a:Landroid/view/ActionMode;

    move-object v1, v3

    :cond_c
    return-object v1

    :catchall_0
    move-exception p1

    iget-object v0, v3, Lg1/a;->f:Lh1/g;

    invoke-virtual {v0}, Lh1/g;->z()V

    throw p1

    :cond_d
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_e
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_f
    return-object v1
.end method

.method public m(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    iget-boolean v0, p0, Ld1/b;->o:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ld1/k;->k()V

    :cond_0
    iget-object v0, p0, Ld1/k;->t:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v0, p0, Ld1/k;->t:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    iget-object p1, p0, Ld1/k;->s:Ld1/k$b;

    iget-object p1, p1, Lf/h;->a:Landroid/view/Window$Callback;

    invoke-interface {p1}, Landroid/view/Window$Callback;->onContentChanged()V

    return-void
.end method
