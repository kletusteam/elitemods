.class public abstract Ld1/b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lh1/k$a;
.implements Lh1/g$a;


# instance fields
.field public a:Ld1/a;

.field public b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

.field public c:Landroid/view/ActionMode;

.field public final d:Ld1/j;

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:I

.field public i:Lh1/g;

.field public j:Z

.field public k:Lh1/g;

.field public l:Landroid/view/MenuInflater;

.field public m:Lh1/d;

.field public n:Z

.field public o:Z

.field public p:I


# direct methods
.method public constructor <init>(Ld1/j;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Ld1/b;->p:I

    iput-boolean v0, p0, Ld1/b;->g:Z

    iput-object p1, p0, Ld1/b;->d:Ld1/j;

    return-void
.end method


# virtual methods
.method public a(Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;)V
    .locals 1

    if-eqz p1, :cond_1

    const v0, 0x7f0a0061

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_0
    const v0, 0x7f0a0060

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->setContentMask(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method public b(Lh1/g;Z)V
    .locals 0

    iget-object p1, p0, Ld1/b;->d:Ld1/j;

    invoke-virtual {p1}, Landroid/app/Activity;->closeOptionsMenu()V

    return-void
.end method

.method public c(Lh1/g;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public e()Lh1/g;
    .locals 3

    new-instance v0, Lh1/g;

    iget-object v1, p0, Ld1/b;->d:Ld1/j;

    invoke-virtual {p0}, Ld1/b;->f()Ld1/a;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ld/a;->d()Landroid/content/Context;

    move-result-object v1

    :cond_0
    invoke-direct {v0, v1}, Lh1/g;-><init>(Landroid/content/Context;)V

    iput-object p0, v0, Lh1/g;->b:Lh1/g$a;

    return-object v0
.end method

.method public final f()Ld1/a;
    .locals 3

    iget-boolean v0, p0, Ld1/b;->f:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ld1/b;->n:Z

    if-eqz v0, :cond_3

    :cond_0
    iget-object v0, p0, Ld1/b;->a:Ld1/a;

    if-nez v0, :cond_4

    move-object v0, p0

    check-cast v0, Ld1/k;

    iget-boolean v2, v0, Ld1/b;->o:Z

    if-nez v2, :cond_1

    invoke-virtual {v0}, Ld1/k;->k()V

    :cond_1
    iget-object v2, v0, Ld1/k;->w:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-nez v2, :cond_2

    goto :goto_0

    :cond_2
    new-instance v1, Lmiuix/appcompat/internal/app/widget/c;

    iget-object v0, v0, Ld1/b;->d:Ld1/j;

    invoke-direct {v1, v0, v2}, Lmiuix/appcompat/internal/app/widget/c;-><init>(Ld1/j;Landroid/view/ViewGroup;)V

    :cond_3
    :goto_0
    iput-object v1, p0, Ld1/b;->a:Ld1/a;

    :cond_4
    iget-object v0, p0, Ld1/b;->a:Ld1/a;

    return-object v0
.end method

.method public abstract g(ILandroid/view/MenuItem;)Z
.end method

.method public h(I)Z
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x2

    if-eq p1, v1, :cond_3

    const/4 v1, 0x5

    if-eq p1, v1, :cond_2

    const/16 v1, 0x8

    if-eq p1, v1, :cond_1

    const/16 v1, 0x9

    if-eq p1, v1, :cond_0

    iget-object v0, p0, Ld1/b;->d:Ld1/j;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->requestWindowFeature(I)Z

    move-result p1

    return p1

    :cond_0
    iput-boolean v0, p0, Ld1/b;->n:Z

    return v0

    :cond_1
    iput-boolean v0, p0, Ld1/b;->f:Z

    return v0

    :cond_2
    iput-boolean v0, p0, Ld1/b;->e:Z

    :cond_3
    return v0
.end method

.method public i(Lh1/g;)V
    .locals 4

    iget-object v0, p0, Ld1/b;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    if-eqz v0, :cond_3

    iget-object v0, v0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-boolean v3, v0, Li1/b;->u:Z

    if-eqz v3, :cond_0

    move v3, v1

    goto :goto_0

    :cond_0
    move v3, v2

    :goto_0
    if-eqz v3, :cond_3

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Li1/b;->m()Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    move v1, v2

    :goto_1
    if-eqz v1, :cond_2

    iget-object p1, p0, Ld1/b;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    if-eqz p1, :cond_4

    invoke-virtual {p1, v2}, Li1/b;->l(Z)Z

    goto :goto_2

    :cond_2
    iget-object p1, p0, Ld1/b;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result p1

    if-nez p1, :cond_4

    iget-object p1, p0, Ld1/b;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Li1/b;->o()Z

    goto :goto_2

    :cond_3
    invoke-virtual {p1}, Lh1/g;->close()V

    :cond_4
    :goto_2
    return-void
.end method

.method public j(Lh1/g;)V
    .locals 10

    iget-object v0, p0, Ld1/b;->k:Lh1/g;

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    iput-object p1, p0, Ld1/b;->k:Lh1/g;

    iget-object v0, p0, Ld1/b;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    if-eqz v0, :cond_e

    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->o0:Lh1/g;

    if-ne p1, v1, :cond_1

    goto/16 :goto_4

    :cond_1
    iget-boolean v2, v0, Lmiuix/appcompat/internal/app/widget/a;->n:Z

    if-nez v2, :cond_2

    if-nez v1, :cond_2

    goto/16 :goto_4

    :cond_2
    if-eqz v1, :cond_3

    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    invoke-virtual {v1, v2}, Lh1/g;->r(Lh1/k;)V

    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->o0:Lh1/g;

    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->N:Lmiuix/appcompat/internal/app/widget/ActionBarView$h;

    invoke-virtual {v1, v2}, Lh1/g;->r(Lh1/k;)V

    :cond_3
    iput-object p1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->o0:Lh1/g;

    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_4

    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_4
    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    if-nez v1, :cond_7

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    :goto_0
    check-cast v1, Landroid/view/View;

    instance-of v2, v1, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz v2, :cond_5

    new-instance v2, Li1/b;

    iget-object v4, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->D:Landroid/content/Context;

    move-object v5, v1

    check-cast v5, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    const v6, 0x7f0d0037

    const v7, 0x7f0d0036

    const v8, 0x7f0d0029

    const v9, 0x7f0d002c

    move-object v3, v2

    invoke-direct/range {v3 .. v9}, Li1/b;-><init>(Landroid/content/Context;Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;IIII)V

    iput-object p0, v2, Lh1/a;->a:Lh1/k$a;

    iput-object v2, v0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    new-instance v1, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;-><init>(Lmiuix/appcompat/internal/app/widget/ActionBarView;Lmiuix/appcompat/internal/app/widget/ActionBarView$a;)V

    iput-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->N:Lmiuix/appcompat/internal/app/widget/ActionBarView$h;

    goto :goto_1

    :cond_5
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    instance-of v2, v2, Landroid/view/View;

    if-eqz v2, :cond_6

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_0

    :cond_6
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "ActionBarOverlayLayout not found"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_7
    :goto_1
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    iget-boolean v4, v0, Lmiuix/appcompat/internal/app/widget/a;->n:Z

    if-nez v4, :cond_9

    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050001

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->q(Lh1/g;)V

    iget-object p1, v0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    invoke-virtual {p1, v0}, Li1/b;->i(Landroid/view/ViewGroup;)Lh1/l;

    move-result-object p1

    check-cast p1, Li1/c;

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    if-eqz v2, :cond_8

    if-eq v2, v0, :cond_8

    invoke-virtual {v2, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_8
    invoke-virtual {v0, p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_3

    :cond_9
    iget-object v4, v0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, v0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Li1/b;->n(IZ)V

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    invoke-static {}, Lr1/b;->a()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0x11

    goto :goto_2

    :cond_a
    const/16 v2, 0x50

    :goto_2
    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->q(Lh1/g;)V

    iget-object p1, v0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    invoke-virtual {p1, v0}, Li1/b;->i(Landroid/view/ViewGroup;)Lh1/l;

    move-result-object p1

    check-cast p1, Li1/c;

    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/a;->o:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v2, :cond_c

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    if-eqz v2, :cond_b

    iget-object v3, v0, Lmiuix/appcompat/internal/app/widget/a;->o:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eq v2, v3, :cond_b

    invoke-virtual {v2, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_b
    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getAnimatedVisibility()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/a;->o:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v2, p1, v6, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    const v1, 0x7f0a007b

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    goto :goto_3

    :cond_c
    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_d
    :goto_3
    iput-object p1, v0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    :cond_e
    :goto_4
    return-void
.end method
