.class public Ld1/f;
.super Landroid/view/WindowInsetsAnimation$Callback;
.source ""


# instance fields
.field public final synthetic a:Ld1/e;


# direct methods
.method public constructor <init>(Ld1/e;I)V
    .locals 0

    iput-object p1, p0, Ld1/f;->a:Ld1/e;

    invoke-direct {p0, p2}, Landroid/view/WindowInsetsAnimation$Callback;-><init>(I)V

    return-void
.end method


# virtual methods
.method public onPrepare(Landroid/view/WindowInsetsAnimation;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/view/WindowInsetsAnimation$Callback;->onPrepare(Landroid/view/WindowInsetsAnimation;)V

    sget-object p1, Lk1/d;->a:Ljava/lang/ref/WeakReference;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/animation/ValueAnimator;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_0
    return-void
.end method

.method public onProgress(Landroid/view/WindowInsets;Ljava/util/List;)Landroid/view/WindowInsets;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/WindowInsets;",
            "Ljava/util/List<",
            "Landroid/view/WindowInsetsAnimation;",
            ">;)",
            "Landroid/view/WindowInsets;"
        }
    .end annotation

    invoke-static {}, Landroid/view/WindowInsets$Type;->ime()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/view/WindowInsets;->isVisible(I)Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-static {}, Landroid/view/WindowInsets$Type;->ime()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/view/WindowInsets;->getInsets(I)Landroid/graphics/Insets;

    move-result-object p2

    iget p2, p2, Landroid/graphics/Insets;->bottom:I

    iget-object v0, p0, Ld1/f;->a:Ld1/e;

    iget-boolean v1, v0, Ld1/e;->X:Z

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    iget v0, v0, Ld1/e;->J:I

    goto :goto_0

    :cond_0
    iget-object v1, v0, Ld1/e;->K:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingBottom()I

    move-result v1

    iget v0, v0, Ld1/e;->L:I

    sub-int/2addr v1, v0

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    :goto_0
    sub-int/2addr p2, v0

    if-gez p2, :cond_1

    goto :goto_1

    :cond_1
    move v2, p2

    :goto_1
    iget-object p2, p0, Ld1/f;->a:Ld1/e;

    neg-int v0, v2

    invoke-static {p2, v0}, Ld1/e;->a(Ld1/e;I)V

    :cond_2
    return-object p1
.end method

.method public onStart(Landroid/view/WindowInsetsAnimation;Landroid/view/WindowInsetsAnimation$Bounds;)Landroid/view/WindowInsetsAnimation$Bounds;
    .locals 3

    iget-object v0, p0, Ld1/f;->a:Ld1/e;

    iget-boolean v1, v0, Ld1/e;->X:Z

    if-eqz v1, :cond_0

    invoke-static {v0}, Ld1/e;->b(Ld1/e;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Ld1/f;->a:Ld1/e;

    iget-object v2, v2, Ld1/e;->K:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTranslationY()F

    move-result v2

    add-float/2addr v2, v1

    float-to-int v1, v2

    iput v1, v0, Ld1/e;->J:I

    iget-object v0, p0, Ld1/f;->a:Ld1/e;

    iget v1, v0, Ld1/e;->J:I

    if-gtz v1, :cond_0

    const/4 v1, 0x0

    iput v1, v0, Ld1/e;->J:I

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/WindowInsetsAnimation$Callback;->onStart(Landroid/view/WindowInsetsAnimation;Landroid/view/WindowInsetsAnimation$Bounds;)Landroid/view/WindowInsetsAnimation$Bounds;

    move-result-object p1

    return-object p1
.end method
