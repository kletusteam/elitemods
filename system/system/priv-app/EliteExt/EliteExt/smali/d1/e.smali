.class public Ld1/e;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ld1/e$b;,
        Ld1/e$c;,
        Ld1/e$d;,
        Ld1/e$e;,
        Ld1/e$f;
    }
.end annotation


# instance fields
.field public A:I

.field public B:Z

.field public C:Z

.field public D:Ld1/e$f;

.field public E:I

.field public F:I

.field public G:Landroid/widget/ListView;

.field public H:Ljava/lang/CharSequence;

.field public I:Landroid/widget/TextView;

.field public J:I

.field public K:Landroid/view/View;

.field public L:I

.field public M:I

.field public N:I

.field public O:I

.field public P:I

.field public Q:I

.field public R:I

.field public S:Landroid/graphics/Point;

.field public T:Landroidx/core/widget/NestedScrollView;

.field public U:Ld1/i$c;

.field public V:Ljava/lang/CharSequence;

.field public W:Landroid/widget/TextView;

.field public X:Z

.field public Y:Landroid/view/View;

.field public Z:I

.field public a:Landroid/widget/ListAdapter;

.field public final a0:Landroid/view/Window;

.field public b:I

.field public b0:Landroid/view/WindowManager;

.field public final c:Landroid/view/View$OnClickListener;

.field public d:Landroid/widget/Button;

.field public e:Landroid/os/Message;

.field public f:Ljava/lang/CharSequence;

.field public g:Landroid/widget/Button;

.field public h:Landroid/os/Message;

.field public i:Ljava/lang/CharSequence;

.field public j:Landroid/widget/Button;

.field public k:Landroid/os/Message;

.field public l:Ljava/lang/CharSequence;

.field public m:Z

.field public n:Z

.field public o:Ljava/lang/CharSequence;

.field public p:I

.field public q:Ld1/e$e;

.field public final r:Landroid/content/Context;

.field public s:Ljava/lang/Thread;

.field public t:Landroid/view/View;

.field public final u:Ld/m;

.field public v:Landroid/view/View;

.field public w:Landroid/view/View;

.field public x:I

.field public y:Landroid/os/Handler;

.field public z:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ld/m;Landroid/view/Window;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Ld1/e;->A:I

    const/4 v1, -0x1

    iput v1, p0, Ld1/e;->p:I

    const/4 v1, 0x1

    iput-boolean v1, p0, Ld1/e;->m:Z

    iput-boolean v1, p0, Ld1/e;->n:Z

    iput-boolean v1, p0, Ld1/e;->C:Z

    new-instance v2, Ld1/e$a;

    invoke-direct {v2, p0}, Ld1/e$a;-><init>(Ld1/e;)V

    iput-object v2, p0, Ld1/e;->c:Landroid/view/View$OnClickListener;

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    iput-object v2, p0, Ld1/e;->S:Landroid/graphics/Point;

    const-string v2, "window"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    iput-object v2, p0, Ld1/e;->b0:Landroid/view/WindowManager;

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    iget-object v3, p0, Ld1/e;->b0:Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    iget v3, v2, Landroid/graphics/Point;->x:I

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, p0, Ld1/e;->Q:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070064

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Ld1/e;->x:I

    iput-object p1, p0, Ld1/e;->r:Landroid/content/Context;

    iput-object p2, p0, Ld1/e;->u:Ld/m;

    iput-object p3, p0, Ld1/e;->a0:Landroid/view/Window;

    new-instance p3, Ld1/e$c;

    invoke-direct {p3, p2}, Ld1/e$c;-><init>(Landroid/content/DialogInterface;)V

    iput-object p3, p0, Ld1/e;->y:Landroid/os/Handler;

    new-instance p3, Ld1/e$e;

    invoke-direct {p3, p0}, Ld1/e$e;-><init>(Ld1/e;)V

    iput-object p3, p0, Ld1/e;->q:Ld1/e$e;

    new-instance p3, Ld1/e$f;

    invoke-direct {p3, p0}, Ld1/e$f;-><init>(Ld1/e;)V

    iput-object p3, p0, Ld1/e;->D:Ld1/e$f;

    sget-object p3, Landroidx/emoji2/text/l;->W:[I

    const/4 v2, 0x0

    const v3, 0x101005d

    invoke-virtual {p1, v2, p3, v3, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p3

    const/4 v2, 0x4

    invoke-virtual {p3, v2, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Ld1/e;->b:I

    const/4 v2, 0x6

    invoke-virtual {p3, v2, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Ld1/e;->F:I

    const/4 v2, 0x7

    invoke-virtual {p3, v2, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    const/16 v2, 0xa

    invoke-virtual {p3, v2, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    const/4 v2, 0x5

    invoke-virtual {p3, v2, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Ld1/e;->E:I

    invoke-virtual {p3}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p2}, Ld/m;->a()Ld/g;

    move-result-object p2

    invoke-virtual {p2, v1}, Ld/g;->t(I)Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f050008

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result p1

    iput-boolean p1, p0, Ld1/e;->X:Z

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object p1

    iput-object p1, p0, Ld1/e;->s:Ljava/lang/Thread;

    return-void
.end method

.method public static a(Ld1/e;I)V
    .locals 0

    iget-object p0, p0, Ld1/e;->K:Landroid/view/View;

    int-to-float p1, p1

    invoke-virtual {p0, p1}, Landroid/view/View;->setTranslationY(F)V

    return-void
.end method

.method public static b(Ld1/e;)I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [I

    iget-object v1, p0, Ld1/e;->K:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    iget-object v1, p0, Ld1/e;->b0:Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    iget-object v2, p0, Ld1/e;->S:Landroid/graphics/Point;

    invoke-virtual {v1, v2}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    iget-object v1, p0, Ld1/e;->S:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    const/4 v2, 0x1

    aget v0, v0, v2

    iget-object p0, p0, Ld1/e;->K:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result p0

    add-int/2addr p0, v0

    sub-int/2addr v1, p0

    return v1
.end method

.method public static d(Landroid/view/View;)Z
    .locals 4

    invoke-virtual {p0}, Landroid/view/View;->onCheckIsTextEditor()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    instance-of v0, p0, Landroid/view/ViewGroup;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    return v2

    :cond_1
    check-cast p0, Landroid/view/ViewGroup;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    :cond_2
    if-lez v0, :cond_3

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Ld1/e;->d(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_2

    return v1

    :cond_3
    return v2
.end method


# virtual methods
.method public final c()V
    .locals 6

    invoke-virtual {p0}, Ld1/e;->j()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_4

    iget-object v0, p0, Ld1/e;->r:Landroid/content/Context;

    invoke-static {v0}, Landroidx/emoji2/text/l;->F(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ld1/e;->D:Ld1/e$f;

    iget-object v2, v0, Ld1/e$f;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ld1/e;

    iget-object v2, v2, Ld1/e;->b0:Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    iget-object v3, v0, Ld1/e$f;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ld1/e;

    iget-object v3, v3, Ld1/e;->S:Landroid/graphics/Point;

    invoke-virtual {v2, v3}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    iget-object v2, v0, Ld1/e$f;->b:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->left:I

    const/4 v4, 0x1

    if-nez v3, :cond_1

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, v0, Ld1/e$f;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ld1/e;

    iget-object v3, v3, Ld1/e;->S:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    if-ne v2, v3, :cond_1

    iget-object v2, v0, Ld1/e$f;->b:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v0, v0, Ld1/e$f;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ld1/e;

    iget-object v0, v0, Ld1/e;->r:Landroid/content/Context;

    invoke-static {v0}, Landroidx/emoji2/text/l;->w(Landroid/content/Context;)I

    move-result v0

    if-le v2, v0, :cond_0

    goto :goto_0

    :cond_0
    move v4, v1

    :cond_1
    :goto_0
    if-eqz v4, :cond_3

    :cond_2
    iget-object v0, p0, Ld1/e;->r:Landroid/content/Context;

    invoke-static {v0}, Landroidx/emoji2/text/l;->v(Landroid/content/Context;)I

    move-result v1

    :cond_3
    iget-object v0, p0, Ld1/e;->K:Landroid/view/View;

    iget v2, p0, Ld1/e;->N:I

    iget v3, p0, Ld1/e;->O:I

    iget v4, p0, Ld1/e;->M:I

    iget v5, p0, Ld1/e;->L:I

    add-int/2addr v5, v1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/view/View;->setPaddingRelative(IIII)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Ld1/e;->K:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getPaddingBottom()I

    move-result v0

    iget v2, p0, Ld1/e;->L:I

    sub-int/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    if-lez v0, :cond_5

    iget-object v0, p0, Ld1/e;->K:Landroid/view/View;

    iget v1, p0, Ld1/e;->N:I

    iget v2, p0, Ld1/e;->O:I

    iget v3, p0, Ld1/e;->M:I

    iget v4, p0, Ld1/e;->L:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->setPaddingRelative(IIII)V

    :cond_5
    :goto_1
    return-void
.end method

.method public final e(Landroid/widget/TextView;)V
    .locals 1

    iget-object v0, p0, Ld1/e;->Y:Landroid/view/View;

    if-nez v0, :cond_1

    iget-object v0, p0, Ld1/e;->o:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lmiuix/androidbasewidget/widget/SingleCenterTextView;

    if-eqz v0, :cond_1

    check-cast p1, Lmiuix/androidbasewidget/widget/SingleCenterTextView;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lmiuix/androidbasewidget/widget/SingleCenterTextView;->setEnableSingleCenter(Z)V

    :cond_1
    :goto_0
    return-void
.end method

.method public final f(Landroid/view/View;)V
    .locals 3

    instance-of v0, p1, Lmiuix/internal/widget/DialogParentPanel;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setFitsSystemWindows(Z)V

    instance-of v1, p1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    :goto_0
    move-object v1, p1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Ld1/e;->f(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public g(Lk1/d$a;)V
    .locals 6

    iget-object v0, p0, Ld1/e;->K:Landroid/view/View;

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lk1/d$a;->a()V

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Ld1/e;->w:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    const-string v3, "hide"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    :cond_2
    new-instance v2, Lk1/d$b;

    invoke-direct {v2, v0, p1}, Lk1/d$b;-><init>(Landroid/view/View;Lk1/d$a;)V

    const/4 p1, 0x2

    new-array v3, p1, [Landroid/animation/PropertyValuesHolder;

    sget-object v4, La1/h;->b:La1/h;

    new-array v5, p1, [F

    fill-array-data v5, :array_0

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    sget-object v4, La1/h;->k:La1/h;

    new-array v5, p1, [F

    fill-array-data v5, :array_1

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v4

    const/4 v5, 0x1

    aput-object v4, v3, v5

    invoke-static {v0, v3}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v3, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v4, 0x3fc00000    # 1.5f

    invoke-direct {v3, v4}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    new-array p1, p1, [F

    fill-array-data p1, :array_2

    const-string v0, "alpha"

    invoke-static {v1, v0, p1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object p1

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0, v4}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {p1, v0}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const-wide/16 v0, 0xfa

    invoke-virtual {p1, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {p1}, Landroid/animation/ObjectAnimator;->start()V

    :goto_0
    return-void

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x42c80000    # 100.0f
    .end array-data

    :array_2
    .array-data 4
        0x3e99999a    # 0.3f
        0x0
    .end array-data
.end method

.method public final h(Landroid/view/ViewGroup;Z)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz p2, :cond_1

    iget-object p2, p0, Ld1/e;->r:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v0, 0x7f0700f2

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    :goto_0
    iput p2, p1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    return-void
.end method

.method public i()Z
    .locals 2

    iget-boolean v0, p0, Ld1/e;->C:Z

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1d

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final j()Z
    .locals 1

    iget-object v0, p0, Ld1/e;->r:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {p0, v0}, Ld1/e;->k(I)Z

    move-result v0

    return v0
.end method

.method public final k(I)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x2

    if-ne p1, v2, :cond_0

    move p1, v1

    goto :goto_0

    :cond_0
    move p1, v0

    :goto_0
    if-eqz p1, :cond_3

    iget-object v2, p0, Ld1/e;->r:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "synergy_mode"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v1, :cond_1

    move v2, v1

    goto :goto_1

    :cond_1
    move v2, v0

    :goto_1
    if-eqz v2, :cond_3

    iget-object p1, p0, Ld1/e;->b0:Landroid/view/WindowManager;

    invoke-interface {p1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object p1

    iget-object v2, p0, Ld1/e;->S:Landroid/graphics/Point;

    invoke-virtual {p1, v2}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    iget-object p1, p0, Ld1/e;->S:Landroid/graphics/Point;

    iget v2, p1, Landroid/graphics/Point;->x:I

    iget p1, p1, Landroid/graphics/Point;->y:I

    if-le v2, p1, :cond_2

    move p1, v1

    goto :goto_2

    :cond_2
    move p1, v0

    :cond_3
    :goto_2
    if-nez p1, :cond_4

    iget-boolean p1, p0, Ld1/e;->X:Z

    if-eqz p1, :cond_5

    :cond_4
    move v0, v1

    :cond_5
    return v0
.end method

.method public final l()V
    .locals 2

    iget-object v0, p0, Ld1/e;->r:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Ld1/e;->X:Z

    iget-object v0, p0, Ld1/e;->r:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070064

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Ld1/e;->x:I

    return-void
.end method

.method public final m(Lmiuix/appcompat/internal/widget/DialogButtonPanel;)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->removeAllViews()V

    iget-object v0, p0, Ld1/e;->j:Landroid/widget/Button;

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Ld1/e;->g:Landroid/widget/Button;

    if-eqz v0, :cond_1

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :cond_1
    iget-object v0, p0, Ld1/e;->d:Landroid/widget/Button;

    if-eqz v0, :cond_2

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :cond_2
    return-void
.end method

.method public n(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V
    .locals 0

    if-eqz p3, :cond_0

    iget-object p4, p0, Ld1/e;->y:Landroid/os/Handler;

    invoke-virtual {p4, p1, p3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p3

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    const/4 p4, -0x3

    if-eq p1, p4, :cond_3

    const/4 p4, -0x2

    if-eq p1, p4, :cond_2

    const/4 p4, -0x1

    if-ne p1, p4, :cond_1

    iput-object p2, p0, Ld1/e;->l:Ljava/lang/CharSequence;

    iput-object p3, p0, Ld1/e;->k:Landroid/os/Message;

    goto :goto_1

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Button does not exist"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    iput-object p2, p0, Ld1/e;->f:Ljava/lang/CharSequence;

    iput-object p3, p0, Ld1/e;->e:Landroid/os/Message;

    goto :goto_1

    :cond_3
    iput-object p2, p0, Ld1/e;->i:Ljava/lang/CharSequence;

    iput-object p3, p0, Ld1/e;->h:Landroid/os/Message;

    :goto_1
    return-void
.end method

.method public final o(I)V
    .locals 2

    iput p1, p0, Ld1/e;->R:I

    iget-object v0, p0, Ld1/e;->r:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    iput v0, p0, Ld1/e;->P:I

    invoke-virtual {p0, p1}, Ld1/e;->k(I)Z

    move-result p1

    iget-object v0, p0, Ld1/e;->K:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    if-eqz p1, :cond_0

    const/16 v1, 0x11

    goto :goto_0

    :cond_0
    const/16 v1, 0x50

    :goto_0
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    if-eqz p1, :cond_2

    iget-boolean p1, p0, Ld1/e;->X:Z

    if-eqz p1, :cond_1

    iget p1, p0, Ld1/e;->x:I

    goto :goto_1

    :cond_1
    iget p1, p0, Ld1/e;->Q:I

    goto :goto_1

    :cond_2
    const/4 p1, -0x1

    :goto_1
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    const/4 p1, -0x2

    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iget-object p1, p0, Ld1/e;->K:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public final p(I)V
    .locals 2

    iput p1, p0, Ld1/e;->R:I

    iget-object v0, p0, Ld1/e;->r:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    iput v0, p0, Ld1/e;->P:I

    invoke-virtual {p0, p1}, Ld1/e;->k(I)Z

    move-result p1

    iget-object v0, p0, Ld1/e;->a0:Landroid/view/Window;

    if-eqz p1, :cond_0

    const/16 v1, 0x11

    goto :goto_0

    :cond_0
    const/16 v1, 0x50

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/Window;->setGravity(I)V

    iget-object v0, p0, Ld1/e;->a0:Landroid/view/Window;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    iget-object v0, p0, Ld1/e;->a0:Landroid/view/Window;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Landroid/view/Window;->setDimAmount(F)V

    iget-object v0, p0, Ld1/e;->a0:Landroid/view/Window;

    if-eqz p1, :cond_2

    iget-boolean p1, p0, Ld1/e;->X:Z

    if-eqz p1, :cond_1

    iget p1, p0, Ld1/e;->x:I

    goto :goto_1

    :cond_1
    iget p1, p0, Ld1/e;->Q:I

    goto :goto_1

    :cond_2
    const/4 p1, -0x1

    :goto_1
    const/4 v1, -0x2

    invoke-virtual {v0, p1, v1}, Landroid/view/Window;->setLayout(II)V

    iget-object p1, p0, Ld1/e;->a0:Landroid/view/Window;

    const v0, 0x7f060211

    invoke-virtual {p1, v0}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    return-void
.end method
