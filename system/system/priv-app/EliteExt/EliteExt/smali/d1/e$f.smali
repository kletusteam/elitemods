.class public Ld1/e$f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ld1/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "f"
.end annotation


# instance fields
.field public a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Ld1/e;",
            ">;"
        }
    .end annotation
.end field

.field public b:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Ld1/e;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ld1/e$f;->a:Ljava/lang/ref/WeakReference;

    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Ld1/e$f;->b:Landroid/graphics/Rect;

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 0

    iget-object p2, p0, Ld1/e$f;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {p2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ld1/e;

    if-eqz p2, :cond_6

    invoke-virtual {p2}, Ld1/e;->c()V

    iget-object p3, p0, Ld1/e$f;->b:Landroid/graphics/Rect;

    invoke-virtual {p1, p3}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    iget-object p3, p2, Ld1/e;->r:Landroid/content/Context;

    invoke-static {p3}, Landroidx/emoji2/text/l;->F(Landroid/content/Context;)Z

    move-result p3

    const/4 p5, 0x0

    if-eqz p3, :cond_2

    iget-object p3, p0, Ld1/e$f;->b:Landroid/graphics/Rect;

    iget p3, p3, Landroid/graphics/Rect;->left:I

    if-lez p3, :cond_1

    iget-object p3, p2, Ld1/e;->r:Landroid/content/Context;

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    invoke-virtual {p3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p3

    iget p3, p3, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int p3, p4, p3

    iget-object p6, p0, Ld1/e$f;->b:Landroid/graphics/Rect;

    iget p6, p6, Landroid/graphics/Rect;->right:I

    if-ne p6, p4, :cond_0

    iget-object p4, p2, Ld1/e;->v:Landroid/view/View;

    invoke-virtual {p4, p3, p5, p5, p5}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0

    :cond_0
    iget-object p4, p2, Ld1/e;->v:Landroid/view/View;

    invoke-virtual {p4, p5, p5, p3, p5}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0

    :cond_1
    iget-object p3, p2, Ld1/e;->v:Landroid/view/View;

    invoke-virtual {p3, p5, p5, p5, p5}, Landroid/view/View;->setPadding(IIII)V

    :cond_2
    :goto_0
    sget p3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p4, 0x1e

    if-ge p3, p4, :cond_6

    invoke-virtual {p1}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object p3

    if-eqz p3, :cond_5

    iget-object p3, p0, Ld1/e$f;->b:Landroid/graphics/Rect;

    invoke-virtual {p2}, Ld1/e;->j()Z

    move-result p4

    if-nez p4, :cond_3

    iget-object p4, p2, Ld1/e;->r:Landroid/content/Context;

    invoke-static {p4}, Landroidx/emoji2/text/l;->F(Landroid/content/Context;)Z

    move-result p4

    if-eqz p4, :cond_3

    iget-object p4, p2, Ld1/e;->b0:Landroid/view/WindowManager;

    invoke-interface {p4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object p4

    iget-object p6, p2, Ld1/e;->S:Landroid/graphics/Point;

    invoke-virtual {p4, p6}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    iget p4, p3, Landroid/graphics/Rect;->top:I

    if-nez p4, :cond_3

    iget p4, p3, Landroid/graphics/Rect;->left:I

    if-nez p4, :cond_3

    iget p4, p3, Landroid/graphics/Rect;->right:I

    iget-object p6, p2, Ld1/e;->S:Landroid/graphics/Point;

    iget p7, p6, Landroid/graphics/Point;->x:I

    if-ne p4, p7, :cond_3

    iget p3, p3, Landroid/graphics/Rect;->bottom:I

    iget p4, p6, Landroid/graphics/Point;->y:I

    if-ge p3, p4, :cond_3

    const/4 p3, 0x1

    goto :goto_1

    :cond_3
    move p3, p5

    :goto_1
    if-nez p3, :cond_6

    iget-object p3, p0, Ld1/e$f;->b:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    iget-object p4, p2, Ld1/e;->K:Landroid/view/View;

    invoke-virtual {p4}, Landroid/view/View;->getPaddingBottom()I

    move-result p4

    iget p6, p2, Ld1/e;->L:I

    sub-int/2addr p4, p6

    invoke-static {p5, p4}, Ljava/lang/Math;->max(II)I

    move-result p4

    sub-int/2addr p1, p4

    iget p3, p3, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr p1, p3

    if-lez p1, :cond_4

    neg-int p5, p1

    sget-object p1, Lk1/d;->a:Ljava/lang/ref/WeakReference;

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/animation/ValueAnimator;

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_4
    iget-object p1, p2, Ld1/e;->K:Landroid/view/View;

    int-to-float p2, p5

    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_2

    :cond_5
    iget-object p1, p2, Ld1/e;->K:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result p1

    const/4 p3, 0x0

    cmpg-float p1, p1, p3

    if-gez p1, :cond_6

    iget-object p1, p2, Ld1/e;->K:Landroid/view/View;

    int-to-float p2, p5

    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationY(F)V

    :cond_6
    :goto_2
    return-void
.end method
