.class public Ld1/i;
.super Ld/m;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ld1/i$b;,
        Ld1/i$c;
    }
.end annotation


# instance fields
.field public final c:Ld1/e;

.field public d:Lk1/d$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    invoke-static {p1, p2}, Ld1/i;->e(Landroid/content/Context;I)I

    move-result p2

    invoke-direct {p0, p1, p2}, Ld/m;-><init>(Landroid/content/Context;I)V

    new-instance p2, Ld1/n;

    invoke-direct {p2, p0}, Ld1/n;-><init>(Ld1/i;)V

    iput-object p2, p0, Ld1/i;->d:Lk1/d$a;

    new-instance p2, Ld1/e;

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Landroid/view/ContextThemeWrapper;

    if-ne v0, v1, :cond_1

    goto :goto_1

    :cond_1
    :goto_0
    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p1

    :goto_1
    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-direct {p2, p1, p0, v0}, Ld1/e;-><init>(Landroid/content/Context;Ld/m;Landroid/view/Window;)V

    iput-object p2, p0, Ld1/i;->c:Ld1/e;

    return-void
.end method

.method public static e(Landroid/content/Context;I)I
    .locals 2

    ushr-int/lit8 v0, p1, 0x18

    and-int/lit16 v0, v0, 0xff

    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    return p1

    :cond_0
    new-instance p1, Landroid/util/TypedValue;

    invoke-direct {p1}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p0

    const v0, 0x7f040179

    invoke-virtual {p0, v0, p1, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget p0, p1, Landroid/util/TypedValue;->resourceId:I

    return p0
.end method


# virtual methods
.method public c()Landroid/app/Activity;
    .locals 3

    invoke-virtual {p0}, Landroid/app/Dialog;->getOwnerActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v1

    :goto_0
    if-nez v0, :cond_2

    if-eqz v1, :cond_2

    instance-of v2, v1, Landroid/app/Activity;

    if-eqz v2, :cond_0

    move-object v0, v1

    check-cast v0, Landroid/app/Activity;

    goto :goto_0

    :cond_0
    instance-of v2, v1, Landroid/content/ContextWrapper;

    if-eqz v2, :cond_1

    check-cast v1, Landroid/content/ContextWrapper;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public d()V
    .locals 1

    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Ld/m;->dismiss()V

    :cond_0
    return-void
.end method

.method public dismiss()V
    .locals 2

    iget-object v0, p0, Ld1/i;->c:Ld1/e;

    invoke-virtual {v0}, Ld1/e;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Ld1/i;->c()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Ld/m;->dismiss()V

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Ld1/i;->c:Ld1/e;

    iget-object v1, p0, Ld1/i;->d:Lk1/d$a;

    invoke-virtual {v0, v1}, Ld1/e;->g(Lk1/d$a;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    new-instance v1, Ld1/i$a;

    invoke-direct {v1, p0}, Ld1/i$a;-><init>(Ld1/i;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_2
    invoke-super {p0}, Ld/m;->dismiss()V

    :cond_3
    :goto_0
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3

    iget-object v0, p0, Ld1/i;->c:Ld1/e;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x52

    const/4 v2, 0x1

    if-ne v0, v1, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    return v2

    :cond_1
    invoke-super {p0, p1}, Ld/m;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Landroid/app/Dialog;->onAttachedToWindow()V

    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ld1/i;->c:Ld1/e;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 18

    move-object/from16 v0, p0

    invoke-super/range {p0 .. p1}, Ld/m;->onCreate(Landroid/os/Bundle;)V

    iget-object v1, v0, Ld1/i;->c:Ld1/e;

    invoke-virtual {v1}, Ld1/e;->i()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual/range {p0 .. p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/Window;->setWindowAnimations(I)V

    :cond_0
    iget-object v1, v0, Ld1/i;->c:Ld1/e;

    iget-object v3, v1, Ld1/e;->u:Ld/m;

    iget v4, v1, Ld1/e;->b:I

    invoke-virtual {v3, v4}, Ld/m;->setContentView(I)V

    invoke-virtual {v1}, Ld1/e;->i()Z

    move-result v3

    const/4 v4, 0x0

    const/4 v5, -0x1

    const/16 v6, 0x1e

    const/4 v7, 0x1

    if-eqz v3, :cond_1

    iget-object v3, v1, Ld1/e;->a0:Landroid/view/Window;

    invoke-virtual {v3, v5, v5}, Landroid/view/Window;->setLayout(II)V

    iget-object v3, v1, Ld1/e;->a0:Landroid/view/Window;

    const v8, 0x7f060211

    invoke-virtual {v3, v8}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    iget-object v3, v1, Ld1/e;->a0:Landroid/view/Window;

    invoke-virtual {v3, v4}, Landroid/view/Window;->setDimAmount(F)V

    iget-object v3, v1, Ld1/e;->a0:Landroid/view/Window;

    const v8, -0x7ffff700

    invoke-virtual {v3, v8}, Landroid/view/Window;->addFlags(I)V

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    iget-object v8, v1, Ld1/e;->a0:Landroid/view/Window;

    invoke-virtual {v8}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v8

    iput v7, v8, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I

    iget-object v8, v1, Ld1/e;->a0:Landroid/view/Window;

    invoke-virtual {v8}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v8

    invoke-virtual {v1, v8}, Ld1/e;->f(Landroid/view/View;)V

    if-lt v3, v6, :cond_2

    iget-object v3, v1, Ld1/e;->a0:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/WindowManager$LayoutParams;->setFitInsetsSides(I)V

    iget-object v3, v1, Ld1/e;->u:Ld/m;

    check-cast v3, Ld1/i;

    invoke-virtual {v3}, Ld1/i;->c()Landroid/app/Activity;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/16 v8, 0x400

    and-int/2addr v3, v8

    if-nez v3, :cond_2

    iget-object v3, v1, Ld1/e;->a0:Landroid/view/Window;

    invoke-virtual {v3, v8}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0

    :cond_1
    iget-object v3, v1, Ld1/e;->r:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v1, v3}, Ld1/e;->p(I)V

    :cond_2
    :goto_0
    iget-object v3, v1, Ld1/e;->a0:Landroid/view/Window;

    const v8, 0x7f0a0073

    invoke-virtual {v3, v8}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, v1, Ld1/e;->v:Landroid/view/View;

    iget-object v3, v1, Ld1/e;->a0:Landroid/view/Window;

    const v8, 0x7f0a00b7

    invoke-virtual {v3, v8}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, v1, Ld1/e;->K:Landroid/view/View;

    iget-object v3, v1, Ld1/e;->a0:Landroid/view/Window;

    const v8, 0x7f0a0072

    invoke-virtual {v3, v8}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, v1, Ld1/e;->w:Landroid/view/View;

    invoke-virtual {v1}, Ld1/e;->i()Z

    move-result v3

    const/16 v8, 0x8

    if-eqz v3, :cond_3

    iget-object v3, v1, Ld1/e;->w:Landroid/view/View;

    new-instance v9, Ld1/m;

    invoke-direct {v9, v1, v2}, Ld1/m;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v3, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, v1, Ld1/e;->K:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingStart()I

    move-result v3

    iput v3, v1, Ld1/e;->N:I

    iget-object v3, v1, Ld1/e;->K:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingEnd()I

    move-result v3

    iput v3, v1, Ld1/e;->M:I

    iget-object v3, v1, Ld1/e;->K:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    iput v3, v1, Ld1/e;->O:I

    iget-object v3, v1, Ld1/e;->K:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    iput v3, v1, Ld1/e;->L:I

    invoke-virtual {v1}, Ld1/e;->c()V

    iget-object v3, v1, Ld1/e;->r:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v1, v3}, Ld1/e;->o(I)V

    goto :goto_1

    :cond_3
    iget-object v3, v1, Ld1/e;->w:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    iget-object v3, v1, Ld1/e;->K:Landroid/view/View;

    const v9, 0x7f0a010b

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iget-object v9, v1, Ld1/e;->K:Landroid/view/View;

    const v10, 0x7f0a005e

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/view/ViewGroup;

    iget-object v11, v1, Ld1/e;->K:Landroid/view/View;

    const v12, 0x7f0a0050

    invoke-virtual {v11, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    iget-object v13, v1, Ld1/e;->K:Landroid/view/View;

    const v14, 0x7f0a0066

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/view/ViewGroup;

    const v6, 0x102002b

    if-eqz v13, :cond_a

    iget-object v10, v1, Ld1/e;->Y:Landroid/view/View;

    if-eqz v10, :cond_4

    goto :goto_2

    :cond_4
    iget v10, v1, Ld1/e;->Z:I

    if-eqz v10, :cond_5

    iget-object v10, v1, Ld1/e;->r:Landroid/content/Context;

    invoke-static {v10}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v10

    iget v14, v1, Ld1/e;->Z:I

    invoke-virtual {v10, v14, v13, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v10

    goto :goto_2

    :cond_5
    const/4 v10, 0x0

    :goto_2
    if-eqz v10, :cond_6

    move v14, v7

    goto :goto_3

    :cond_6
    move v14, v2

    :goto_3
    if-eqz v14, :cond_7

    invoke-static {v10}, Ld1/e;->d(Landroid/view/View;)Z

    move-result v16

    if-nez v16, :cond_8

    :cond_7
    iget-object v12, v1, Ld1/e;->a0:Landroid/view/Window;

    const/high16 v15, 0x20000

    invoke-virtual {v12, v15, v15}, Landroid/view/Window;->setFlags(II)V

    :cond_8
    if-eqz v14, :cond_9

    iget-object v12, v1, Ld1/e;->a0:Landroid/view/Window;

    invoke-virtual {v12, v6}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/FrameLayout;

    new-instance v14, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v14, v5, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v12, v10, v14}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v10, v1, Ld1/e;->G:Landroid/widget/ListView;

    if-eqz v10, :cond_a

    invoke-virtual {v13}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout$LayoutParams;

    iput v4, v10, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    goto :goto_4

    :cond_9
    invoke-virtual {v13, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_a
    :goto_4
    if-eqz v9, :cond_e

    iget-object v4, v1, Ld1/e;->a0:Landroid/view/Window;

    const v10, 0x7f0a00c6

    invoke-virtual {v4, v10}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroidx/core/widget/NestedScrollView;

    iput-object v4, v1, Ld1/e;->T:Landroidx/core/widget/NestedScrollView;

    invoke-virtual {v4, v2}, Landroid/widget/FrameLayout;->setFocusable(Z)V

    iget-object v4, v1, Ld1/e;->T:Landroidx/core/widget/NestedScrollView;

    invoke-virtual {v4, v2}, Landroidx/core/widget/NestedScrollView;->setNestedScrollingEnabled(Z)V

    const v4, 0x7f0a009d

    invoke-virtual {v9, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Ld1/e;->I:Landroid/widget/TextView;

    if-nez v4, :cond_b

    goto :goto_5

    :cond_b
    iget-object v10, v1, Ld1/e;->H:Ljava/lang/CharSequence;

    if-eqz v10, :cond_c

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, v1, Ld1/e;->I:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Ld1/e;->e(Landroid/widget/TextView;)V

    goto :goto_5

    :cond_c
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v4, v1, Ld1/e;->T:Landroidx/core/widget/NestedScrollView;

    iget-object v10, v1, Ld1/e;->I:Landroid/widget/TextView;

    invoke-virtual {v4, v10}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    iget-object v4, v1, Ld1/e;->G:Landroid/widget/ListView;

    if-eqz v4, :cond_d

    invoke-virtual {v9}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v4, v1, Ld1/e;->G:Landroid/widget/ListView;

    new-instance v10, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v10, v5, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v9, v4, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_5

    :cond_d
    invoke-virtual {v9, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_e
    :goto_5
    if-eqz v11, :cond_1d

    const v4, 0x1020019

    invoke-virtual {v11, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, v1, Ld1/e;->j:Landroid/widget/Button;

    iget-object v10, v1, Ld1/e;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v10}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, v1, Ld1/e;->j:Landroid/widget/Button;

    invoke-static {v4}, Landroidx/emoji2/text/l;->U(Landroid/widget/TextView;)V

    iget-object v4, v1, Ld1/e;->l:Ljava/lang/CharSequence;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_f

    iget-object v4, v1, Ld1/e;->j:Landroid/widget/Button;

    invoke-virtual {v4, v8}, Landroid/widget/Button;->setVisibility(I)V

    move v4, v2

    goto :goto_6

    :cond_f
    iget-object v4, v1, Ld1/e;->j:Landroid/widget/Button;

    iget-object v10, v1, Ld1/e;->l:Ljava/lang/CharSequence;

    invoke-virtual {v4, v10}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, v1, Ld1/e;->j:Landroid/widget/Button;

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v4, v1, Ld1/e;->j:Landroid/widget/Button;

    invoke-static {v4, v2}, Landroidx/emoji2/text/l;->S(Landroid/view/View;Z)V

    iget-object v4, v1, Ld1/e;->j:Landroid/widget/Button;

    invoke-static {v4}, Landroidx/emoji2/text/l;->d(Landroid/view/View;)V

    move v4, v7

    :goto_6
    const v10, 0x102001a

    invoke-virtual {v11, v10}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Button;

    iput-object v10, v1, Ld1/e;->d:Landroid/widget/Button;

    iget-object v12, v1, Ld1/e;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v10, v12}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v10, v1, Ld1/e;->d:Landroid/widget/Button;

    invoke-static {v10}, Landroidx/emoji2/text/l;->U(Landroid/widget/TextView;)V

    iget-object v10, v1, Ld1/e;->f:Ljava/lang/CharSequence;

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_10

    iget-object v10, v1, Ld1/e;->d:Landroid/widget/Button;

    invoke-virtual {v10, v8}, Landroid/widget/Button;->setVisibility(I)V

    move v10, v4

    goto :goto_7

    :cond_10
    iget-object v10, v1, Ld1/e;->d:Landroid/widget/Button;

    iget-object v12, v1, Ld1/e;->f:Ljava/lang/CharSequence;

    invoke-virtual {v10, v12}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v10, v1, Ld1/e;->d:Landroid/widget/Button;

    invoke-virtual {v10, v2}, Landroid/widget/Button;->setVisibility(I)V

    or-int/lit8 v10, v4, 0x2

    add-int/lit8 v4, v4, 0x1

    iget-object v12, v1, Ld1/e;->d:Landroid/widget/Button;

    invoke-static {v12, v2}, Landroidx/emoji2/text/l;->S(Landroid/view/View;Z)V

    iget-object v12, v1, Ld1/e;->d:Landroid/widget/Button;

    invoke-static {v12}, Landroidx/emoji2/text/l;->d(Landroid/view/View;)V

    move/from16 v17, v10

    move v10, v4

    move/from16 v4, v17

    :goto_7
    const v12, 0x102001b

    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/Button;

    iput-object v12, v1, Ld1/e;->g:Landroid/widget/Button;

    iget-object v13, v1, Ld1/e;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v12, v13}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v12, v1, Ld1/e;->g:Landroid/widget/Button;

    invoke-static {v12}, Landroidx/emoji2/text/l;->U(Landroid/widget/TextView;)V

    iget-object v12, v1, Ld1/e;->i:Ljava/lang/CharSequence;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_11

    iget-object v12, v1, Ld1/e;->g:Landroid/widget/Button;

    invoke-virtual {v12, v8}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_8

    :cond_11
    iget-object v12, v1, Ld1/e;->g:Landroid/widget/Button;

    iget-object v13, v1, Ld1/e;->i:Ljava/lang/CharSequence;

    invoke-virtual {v12, v13}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v12, v1, Ld1/e;->g:Landroid/widget/Button;

    invoke-virtual {v12, v2}, Landroid/widget/Button;->setVisibility(I)V

    or-int/lit8 v4, v4, 0x4

    add-int/lit8 v10, v10, 0x1

    iget-object v12, v1, Ld1/e;->g:Landroid/widget/Button;

    invoke-static {v12, v2}, Landroidx/emoji2/text/l;->S(Landroid/view/View;Z)V

    iget-object v12, v1, Ld1/e;->g:Landroid/widget/Button;

    invoke-static {v12}, Landroidx/emoji2/text/l;->d(Landroid/view/View;)V

    :goto_8
    if-eqz v4, :cond_12

    move v4, v7

    goto :goto_9

    :cond_12
    move v4, v2

    :goto_9
    if-nez v4, :cond_13

    invoke-virtual {v11, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_10

    :cond_13
    const v4, 0x7f0a004f

    invoke-virtual {v11, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lmiuix/appcompat/internal/widget/DialogButtonPanel;

    const/4 v12, 0x2

    if-le v10, v12, :cond_14

    goto/16 :goto_f

    :cond_14
    if-ne v10, v7, :cond_17

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v10

    move v12, v2

    :goto_a
    if-ge v12, v10, :cond_16

    invoke-virtual {v4, v12}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v13

    invoke-virtual {v13}, Landroid/view/View;->getVisibility()I

    move-result v14

    if-nez v14, :cond_15

    goto :goto_b

    :cond_15
    add-int/lit8 v12, v12, 0x1

    goto :goto_a

    :cond_16
    const/4 v13, 0x0

    :goto_b
    if-eqz v13, :cond_1d

    invoke-virtual {v13}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    iput v2, v4, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    goto/16 :goto_10

    :cond_17
    iget-object v10, v1, Ld1/e;->K:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    iget v10, v10, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-gtz v10, :cond_18

    iget-object v10, v1, Ld1/e;->r:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v10

    iget v10, v10, Landroid/util/DisplayMetrics;->widthPixels:I

    :cond_18
    invoke-virtual {v11}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    check-cast v13, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v13}, Landroid/view/ViewGroup$MarginLayoutParams;->getMarginStart()I

    move-result v14

    invoke-virtual {v13}, Landroid/view/ViewGroup$MarginLayoutParams;->getMarginEnd()I

    move-result v13

    add-int/2addr v13, v14

    sub-int/2addr v10, v13

    iget-object v13, v1, Ld1/e;->r:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0700e5

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v13

    sub-int/2addr v10, v13

    div-int/2addr v10, v12

    move v12, v2

    move v13, v12

    :goto_c
    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v14

    if-ge v12, v14, :cond_1c

    invoke-virtual {v4, v12}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    invoke-virtual {v14}, Landroid/widget/TextView;->getVisibility()I

    move-result v15

    if-nez v15, :cond_1a

    invoke-virtual {v14}, Landroid/widget/TextView;->getPaddingStart()I

    move-result v13

    invoke-virtual {v14}, Landroid/widget/TextView;->getPaddingEnd()I

    move-result v15

    invoke-virtual {v14}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v6

    invoke-virtual {v14}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v14

    invoke-interface {v14}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v6, v14}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v6

    float-to-int v6, v6

    sub-int v13, v10, v13

    sub-int/2addr v13, v15

    if-le v6, v13, :cond_19

    move v13, v7

    goto :goto_d

    :cond_19
    move v13, v2

    :cond_1a
    :goto_d
    if-eqz v13, :cond_1b

    goto :goto_e

    :cond_1b
    add-int/lit8 v12, v12, 0x1

    const v6, 0x102002b

    goto :goto_c

    :cond_1c
    :goto_e
    if-eqz v13, :cond_1d

    :goto_f
    invoke-virtual {v1, v4}, Ld1/e;->m(Lmiuix/appcompat/internal/widget/DialogButtonPanel;)V

    :cond_1d
    :goto_10
    if-eqz v3, :cond_22

    iget-object v4, v1, Ld1/e;->t:Landroid/view/View;

    const v6, 0x7f0a0045

    if-eqz v4, :cond_1e

    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    const/4 v10, -0x2

    invoke-direct {v4, v5, v10}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    iget-object v10, v1, Ld1/e;->t:Landroid/view/View;

    invoke-virtual {v3, v10, v2, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    iget-object v4, v1, Ld1/e;->a0:Landroid/view/Window;

    invoke-virtual {v4, v6}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_12

    :cond_1e
    iget-object v4, v1, Ld1/e;->V:Ljava/lang/CharSequence;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    xor-int/2addr v4, v7

    if-eqz v4, :cond_21

    iget-object v4, v1, Ld1/e;->a0:Landroid/view/Window;

    invoke-virtual {v4, v6}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Ld1/e;->W:Landroid/widget/TextView;

    iget-object v6, v1, Ld1/e;->V:Ljava/lang/CharSequence;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, v1, Ld1/e;->z:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_1f

    iget-object v6, v1, Ld1/e;->W:Landroid/widget/TextView;

    const/4 v10, 0x0

    invoke-virtual {v6, v4, v10, v10, v10}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_11

    :cond_1f
    const/4 v10, 0x0

    :goto_11
    iget v4, v1, Ld1/e;->A:I

    if-eqz v4, :cond_20

    iget-object v6, v1, Ld1/e;->W:Landroid/widget/TextView;

    invoke-virtual {v6, v4, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    :cond_20
    iget-object v4, v1, Ld1/e;->H:Ljava/lang/CharSequence;

    if-eqz v4, :cond_23

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v4

    if-eq v4, v8, :cond_23

    iget-object v4, v1, Ld1/e;->W:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v6

    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v12

    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v13

    invoke-virtual {v4, v6, v12, v13, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_13

    :cond_21
    const/4 v10, 0x0

    iget-object v4, v1, Ld1/e;->a0:Landroid/view/Window;

    invoke-virtual {v4, v6}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v3, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_13

    :cond_22
    :goto_12
    const/4 v10, 0x0

    :cond_23
    :goto_13
    if-eqz v3, :cond_24

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v4

    if-eq v4, v8, :cond_24

    move v4, v7

    goto :goto_14

    :cond_24
    move v4, v2

    :goto_14
    if-eqz v11, :cond_25

    invoke-virtual {v11}, Landroid/view/ViewGroup;->getVisibility()I

    :cond_25
    if-eqz v4, :cond_2a

    iget-object v4, v1, Ld1/e;->T:Landroidx/core/widget/NestedScrollView;

    if-eqz v4, :cond_26

    invoke-virtual {v4, v7}, Landroid/widget/FrameLayout;->setClipToPadding(Z)V

    :cond_26
    iget-object v4, v1, Ld1/e;->H:Ljava/lang/CharSequence;

    if-nez v4, :cond_28

    iget-object v4, v1, Ld1/e;->G:Landroid/widget/ListView;

    if-eqz v4, :cond_27

    goto :goto_15

    :cond_27
    move-object v15, v10

    goto :goto_16

    :cond_28
    :goto_15
    const v4, 0x7f0a0107

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v15

    :goto_16
    if-eqz v15, :cond_29

    invoke-virtual {v15, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_29
    iget-object v3, v1, Ld1/e;->G:Landroid/widget/ListView;

    if-eqz v3, :cond_2a

    if-eqz v9, :cond_2a

    invoke-virtual {v9}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v3

    invoke-virtual {v9}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v4

    invoke-virtual {v9}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v6

    invoke-virtual {v9, v3, v2, v4, v6}, Landroid/view/ViewGroup;->setPadding(IIII)V

    :cond_2a
    iget-object v3, v1, Ld1/e;->G:Landroid/widget/ListView;

    if-eqz v3, :cond_2b

    iget-object v4, v1, Ld1/e;->a:Landroid/widget/ListAdapter;

    if-eqz v4, :cond_2b

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget v4, v1, Ld1/e;->p:I

    if-le v4, v5, :cond_2b

    invoke-virtual {v3, v4, v7}, Landroid/widget/ListView;->setItemChecked(IZ)V

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setSelection(I)V

    :cond_2b
    iget-object v3, v1, Ld1/e;->K:Landroid/view/View;

    const v4, 0x1020001

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    if-eqz v3, :cond_2d

    iget-object v4, v1, Ld1/e;->o:Ljava/lang/CharSequence;

    if-eqz v4, :cond_2c

    invoke-virtual {v3, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-boolean v4, v1, Ld1/e;->B:Z

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v4, v1, Ld1/e;->o:Ljava/lang/CharSequence;

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    goto :goto_17

    :cond_2c
    invoke-virtual {v3, v8}, Landroid/widget/CheckBox;->setVisibility(I)V

    :cond_2d
    :goto_17
    iget-object v3, v1, Ld1/e;->K:Landroid/view/View;

    const v4, 0x7f0a0050

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iget-object v4, v1, Ld1/e;->K:Landroid/view/View;

    const v5, 0x7f0a0066

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iget-object v5, v1, Ld1/e;->K:Landroid/view/View;

    const v6, 0x7f0a005e

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    if-eqz v3, :cond_2e

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_2e

    move v2, v7

    :cond_2e
    if-eqz v4, :cond_2f

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_2f

    const v3, 0x102002b

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v1, v3, v2}, Ld1/e;->h(Landroid/view/ViewGroup;Z)V

    :cond_2f
    if-eqz v5, :cond_30

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_30

    const v3, 0x7f0a005f

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v1, v3, v2}, Ld1/e;->h(Landroid/view/ViewGroup;Z)V

    :cond_30
    invoke-virtual {v1}, Ld1/e;->i()Z

    move-result v2

    if-eqz v2, :cond_31

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x1e

    if-lt v2, v3, :cond_31

    iget-object v2, v1, Ld1/e;->a0:Landroid/view/Window;

    const/16 v3, 0x30

    invoke-virtual {v2, v3}, Landroid/view/Window;->setSoftInputMode(I)V

    iget-object v2, v1, Ld1/e;->a0:Landroid/view/Window;

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    new-instance v3, Ld1/f;

    invoke-direct {v3, v1, v7}, Ld1/f;-><init>(Ld1/e;I)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setWindowInsetsAnimationCallback(Landroid/view/WindowInsetsAnimation$Callback;)V

    iget-object v2, v1, Ld1/e;->a0:Landroid/view/Window;

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    new-instance v3, Ld1/g;

    invoke-direct {v3, v1}, Ld1/g;-><init>(Ld1/e;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnApplyWindowInsetsListener(Landroid/view/View$OnApplyWindowInsetsListener;)V

    :cond_31
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 4

    invoke-super {p0}, Landroid/app/Dialog;->onDetachedFromWindow()V

    iget-object v0, p0, Ld1/i;->c:Ld1/e;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/view/View;

    iget-object v2, v0, Ld1/e;->K:Landroid/view/View;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v0, v0, Ld1/e;->w:Landroid/view/View;

    const/4 v2, 0x1

    aput-object v0, v1, v2

    invoke-static {v1}, Lu0/a;->b([Ljava/lang/Object;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    iget-object v0, p0, Ld1/i;->c:Ld1/e;

    iget-object v0, v0, Ld1/e;->T:Landroidx/core/widget/NestedScrollView;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Landroidx/core/widget/NestedScrollView;->g(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    return v1

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2

    iget-object v0, p0, Ld1/i;->c:Ld1/e;

    iget-object v0, v0, Ld1/e;->T:Landroidx/core/widget/NestedScrollView;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Landroidx/core/widget/NestedScrollView;->g(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    return v1

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public onStart()V
    .locals 6

    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    iget-object v0, p0, Ld1/i;->c:Ld1/e;

    iget-object v1, v0, Ld1/e;->r:Landroid/content/Context;

    iget-object v2, v0, Ld1/e;->q:Ld1/e$e;

    invoke-virtual {v1, v2}, Landroid/content/Context;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    invoke-virtual {v0}, Ld1/e;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, Ld1/e;->K:Landroid/view/View;

    iget-object v2, v0, Ld1/e;->w:Landroid/view/View;

    invoke-virtual {v0}, Ld1/e;->j()Z

    move-result v3

    iget-object v4, v0, Ld1/e;->U:Ld1/i$c;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v5

    if-lez v5, :cond_0

    new-instance v5, Lk1/a;

    invoke-direct {v5, v3, v1, v4}, Lk1/a;-><init>(ZLandroid/view/View;Ld1/i$c;)V

    invoke-virtual {v1, v5}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v1, v3}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    :cond_0
    new-instance v5, Lk1/b;

    invoke-direct {v5, v3, v4}, Lk1/b;-><init>(ZLd1/i$c;)V

    invoke-virtual {v1, v5}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    :goto_0
    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    const-string v3, "alpha"

    invoke-static {v2, v3, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v3, 0x3fc00000    # 1.5f

    invoke-direct {v2, v3}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    iget-object v1, v0, Ld1/e;->a0:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    iget-object v0, v0, Ld1/e;->D:Ld1/e$f;

    invoke-virtual {v1, v0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    :cond_1
    return-void

    :array_0
    .array-data 4
        0x0
        0x3e99999a    # 0.3f
    .end array-data
.end method

.method public onStop()V
    .locals 3

    invoke-super {p0}, Ld/m;->onStop()V

    iget-object v0, p0, Ld1/i;->c:Ld1/e;

    iget-object v1, v0, Ld1/e;->r:Landroid/content/Context;

    iget-object v2, v0, Ld1/e;->q:Ld1/e$e;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    invoke-virtual {v0}, Ld1/e;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Ld1/e;->a0:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    iget-object v0, v0, Ld1/e;->D:Ld1/e$f;

    invoke-virtual {v1, v0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    :cond_0
    return-void
.end method

.method public setCancelable(Z)V
    .locals 1

    invoke-super {p0, p1}, Landroid/app/Dialog;->setCancelable(Z)V

    iget-object v0, p0, Ld1/i;->c:Ld1/e;

    iput-boolean p1, v0, Ld1/e;->m:Z

    return-void
.end method

.method public setCanceledOnTouchOutside(Z)V
    .locals 1

    invoke-super {p0, p1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Ld1/i;->c:Ld1/e;

    iput-boolean p1, v0, Ld1/e;->n:Z

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    invoke-super {p0, p1}, Ld/m;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Ld1/i;->c:Ld1/e;

    iput-object p1, v0, Ld1/e;->V:Ljava/lang/CharSequence;

    iget-object v0, v0, Ld1/e;->W:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
