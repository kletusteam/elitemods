.class public abstract Lu0/b;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final j:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public final a:Lx0/c;

.field public final b:Lx0/l;

.field public final c:I

.field public d:F

.field public e:J

.field public f:J

.field public g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Lx0/m;

.field public i:Lx0/j;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const v1, 0x7fffffff

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lu0/b;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lx0/l;

    invoke-direct {v0, p0}, Lx0/l;-><init>(Lu0/b;)V

    iput-object v0, p0, Lu0/b;->b:Lx0/l;

    new-instance v0, Lx0/c;

    invoke-direct {v0}, Lx0/c;-><init>()V

    iput-object v0, p0, Lu0/b;->a:Lx0/c;

    new-instance v1, Lx0/j;

    invoke-direct {v1, p0}, Lx0/j;-><init>(Lu0/b;)V

    iput-object v1, p0, Lu0/b;->i:Lx0/j;

    const v1, 0x7f7fffff    # Float.MAX_VALUE

    iput v1, p0, Lu0/b;->d:F

    new-instance v1, Landroid/util/ArrayMap;

    invoke-direct {v1}, Landroid/util/ArrayMap;-><init>()V

    iput-object v1, p0, Lu0/b;->g:Ljava/util/Map;

    sget-object v1, Lu0/b;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v1

    iput v1, p0, Lu0/b;->c:I

    new-instance v1, Lx0/m;

    invoke-direct {v1}, Lx0/m;-><init>()V

    iput-object v1, p0, Lu0/b;->h:Lx0/m;

    iput-object p0, v0, Lx0/c;->d:Lu0/b;

    const v0, 0x3dcccccd    # 0.1f

    const/4 v1, 0x3

    new-array v2, v1, [La1/b;

    sget-object v3, La1/h;->e:La1/h;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    sget-object v3, La1/h;->f:La1/h;

    const/4 v5, 0x1

    aput-object v3, v2, v5

    sget-object v3, La1/h;->g:La1/h;

    const/4 v6, 0x2

    aput-object v3, v2, v6

    invoke-virtual {p0, v0, v2}, Lu0/b;->m(F[La1/b;)Lu0/b;

    const/high16 v0, 0x3b800000    # 0.00390625f

    const/4 v2, 0x4

    new-array v2, v2, [La1/b;

    sget-object v3, La1/h;->b:La1/h;

    aput-object v3, v2, v4

    sget-object v3, La1/h;->c:La1/h;

    aput-object v3, v2, v5

    sget-object v3, La1/i;->b:La1/i$c;

    aput-object v3, v2, v6

    sget-object v3, La1/i;->a:La1/i$b;

    aput-object v3, v2, v1

    invoke-virtual {p0, v0, v2}, Lu0/b;->m(F[La1/b;)Lu0/b;

    const v0, 0x3b03126f    # 0.002f

    new-array v1, v6, [La1/b;

    sget-object v2, La1/h;->h:La1/h;

    aput-object v2, v1, v4

    sget-object v2, La1/h;->i:La1/h;

    aput-object v2, v1, v5

    invoke-virtual {p0, v0, v1}, Lu0/b;->m(F[La1/b;)Lu0/b;

    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public b(Ljava/lang/Runnable;)V
    .locals 0

    invoke-virtual {p0, p1}, Lu0/b;->k(Ljava/lang/Runnable;)V

    return-void
.end method

.method public c()F
    .locals 1

    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public d(La1/c;)I
    .locals 1

    invoke-virtual {p0}, Lu0/b;->g()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1, v0}, La1/c;->b(Ljava/lang/Object;)I

    move-result p1

    return p1

    :cond_0
    const p1, 0x7fffffff

    return p1
.end method

.method public e(Ljava/lang/Object;)F
    .locals 1

    iget-object v0, p0, Lu0/b;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Float;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    return p1

    :cond_0
    iget p1, p0, Lu0/b;->d:F

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_1

    return p1

    :cond_1
    invoke-virtual {p0}, Lu0/b;->c()F

    move-result p1

    return p1
.end method

.method public f()Ly0/a;
    .locals 1

    iget-object v0, p0, Lu0/b;->i:Lx0/j;

    iget-object v0, v0, Lx0/j;->b:Ljava/lang/Object;

    check-cast v0, Ly0/a;

    return-object v0
.end method

.method public abstract g()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public h(La1/b;)F
    .locals 1

    invoke-virtual {p0}, Lu0/b;->g()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, La1/b;->c(Ljava/lang/Object;)F

    move-result p1

    return p1

    :cond_0
    const p1, 0x7f7fffff    # Float.MAX_VALUE

    return p1
.end method

.method public i()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public j()Z
    .locals 4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lu0/b;->f:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public k(Ljava/lang/Runnable;)V
    .locals 4

    iget-object v0, p0, Lu0/b;->b:Lx0/l;

    iget-wide v0, v0, Lx0/l;->c:J

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lu0/b;->b:Lx0/l;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void
.end method

.method public l(La1/c;I)V
    .locals 3

    invoke-virtual {p0}, Lu0/b;->g()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v1

    const v2, 0x7fffffff

    if-eq v1, v2, :cond_0

    invoke-interface {p1, v0, p2}, La1/c;->a(Ljava/lang/Object;I)V

    :cond_0
    return-void
.end method

.method public varargs m(F[La1/b;)Lu0/b;
    .locals 5

    array-length v0, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p2, v1

    iget-object v3, p0, Lu0/b;->g:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public n(La1/b;F)V
    .locals 3

    invoke-virtual {p0}, Lu0/b;->g()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v2, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    invoke-virtual {p1, v0, p2}, La1/b;->d(Ljava/lang/Object;F)V

    :cond_0
    return-void
.end method

.method public o(La1/b;D)V
    .locals 2

    const-wide v0, 0x47efffffe0000000L    # 3.4028234663852886E38

    cmpl-double v0, p2, v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lu0/b;->a:Lx0/c;

    double-to-float p2, p2

    invoke-virtual {v0, p1}, Lx0/c;->c(La1/b;)Ly0/c;

    move-result-object p1

    float-to-double p2, p2

    iput-wide p2, p1, Ly0/c;->f:D

    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "IAnimTarget{"

    invoke-static {v0}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lu0/b;->g()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
