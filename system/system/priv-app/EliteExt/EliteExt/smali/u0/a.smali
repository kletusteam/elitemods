.class public Lu0/a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lu0/a$c;
    }
.end annotation


# static fields
.field public static final a:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Lu0/b;",
            "Lu0/a$c;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Landroid/os/Handler;

.field public static c:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lu0/a$a;

    invoke-direct {v0}, Lu0/a$a;-><init>()V

    sget-object v1, Lx0/n;->b:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lu0/a;->c:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lu0/a;->a:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v0, Lu0/a$b;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lu0/a$b;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lu0/a;->b:Landroid/os/Handler;

    return-void
.end method

.method public static a(Z)V
    .locals 4

    sget-object v0, Lu0/a;->b:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_0
    if-eqz p0, :cond_1

    sget-boolean p0, Lc1/f;->a:Z

    if-eqz p0, :cond_1

    sget-object p0, Lu0/a;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu0/b;

    const-string v2, "exist target:"

    invoke-static {v2}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lu0/b;->g()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lc1/f;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    sget-object p0, Lu0/a;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p0}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result p0

    if-lez p0, :cond_2

    sget-object p0, Lu0/a;->b:Landroid/os/Handler;

    const-wide/16 v2, 0x4e20

    invoke-virtual {p0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    :cond_2
    sget-object p0, Lu0/a;->b:Landroid/os/Handler;

    invoke-virtual {p0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_3
    :goto_1
    return-void
.end method

.method public static varargs b([Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/SafeVarargs;
    .end annotation

    sget-object v0, Lc1/a;->a:[Ljava/lang/Class;

    array-length v0, p0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    sget-object p0, Lu0/a;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu0/b;

    invoke-static {v0}, Lu0/a;->c(Lu0/b;)V

    goto :goto_1

    :cond_1
    array-length v0, p0

    :goto_2
    if-ge v1, v0, :cond_2

    aget-object v2, p0, v1

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lu0/a;->d(Ljava/lang/Object;Lu0/e;)Lu0/b;

    move-result-object v2

    invoke-static {v2}, Lu0/a;->c(Lu0/b;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    return-void
.end method

.method public static c(Lu0/b;)V
    .locals 2

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lu0/b;->a()V

    sget-object v0, Lu0/a;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu0/a$c;

    iget-object p0, p0, Lu0/b;->a:Lx0/c;

    iget-object v1, p0, Lx0/c;->c:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    iget-object v1, p0, Lx0/c;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    iget-object v1, p0, Lx0/c;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    iget-object v1, p0, Lx0/c;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    iget-object p0, p0, Lx0/c;->h:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {p0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    if-eqz v0, :cond_1

    iget-object p0, v0, Lu0/a$c;->c:Lu0/f;

    if-eqz p0, :cond_0

    check-cast p0, Lw0/f;

    invoke-virtual {p0}, Lw0/f;->i()V

    :cond_0
    iget-object p0, v0, Lu0/a$c;->a:Lw0/h;

    if-eqz p0, :cond_1

    invoke-interface {p0}, Lu0/d;->i()V

    :cond_1
    return-void
.end method

.method public static d(Ljava/lang/Object;Lu0/e;)Lu0/b;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lu0/e<",
            "TT;>;)",
            "Lu0/b;"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    instance-of v1, p0, Lu0/b;

    if-eqz v1, :cond_1

    check-cast p0, Lu0/b;

    return-object p0

    :cond_1
    sget-object v1, Lu0/a;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lu0/b;

    invoke-virtual {v2}, Lu0/b;->g()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v3, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    return-object v2

    :cond_3
    if-eqz p1, :cond_4

    invoke-interface {p1, p0}, Lu0/e;->a(Ljava/lang/Object;)Lu0/b;

    move-result-object p0

    if-eqz p0, :cond_4

    invoke-static {p0}, Lu0/a;->e(Lu0/b;)Lu0/c;

    return-object p0

    :cond_4
    return-object v0
.end method

.method public static e(Lu0/b;)Lu0/c;
    .locals 4

    sget-object v0, Lu0/a;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lu0/a$c;

    if-nez v1, :cond_0

    new-instance v1, Lu0/a$c;

    const/4 v2, 0x1

    new-array v2, v2, [Lu0/b;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lu0/a$c;-><init>([Lu0/b;Lu0/a$a;)V

    invoke-virtual {v0, p0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lu0/a$c;

    if-eqz p0, :cond_0

    move-object v1, p0

    :cond_0
    return-object v1
.end method

.method public static varargs f([Landroid/view/View;)Lu0/c;
    .locals 10

    array-length v0, p0

    if-eqz v0, :cond_8

    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    aget-object p0, p0, v1

    sget-object v0, Lmiuix/animation/ViewTarget;->o:Lu0/e;

    invoke-static {p0, v0}, Lu0/a;->d(Ljava/lang/Object;Lu0/e;)Lu0/b;

    move-result-object p0

    invoke-static {p0}, Lu0/a;->e(Lu0/b;)Lu0/c;

    move-result-object p0

    return-object p0

    :cond_0
    array-length v0, p0

    new-array v3, v0, [Lu0/b;

    const/4 v4, 0x0

    move v5, v1

    move v6, v5

    move-object v7, v4

    :goto_0
    array-length v8, p0

    if-ge v5, v8, :cond_3

    aget-object v8, p0, v5

    sget-object v9, Lmiuix/animation/ViewTarget;->o:Lu0/e;

    invoke-static {v8, v9}, Lu0/a;->d(Ljava/lang/Object;Lu0/e;)Lu0/b;

    move-result-object v8

    aput-object v8, v3, v5

    sget-object v8, Lu0/a;->a:Ljava/util/concurrent/ConcurrentHashMap;

    aget-object v9, v3, v5

    invoke-virtual {v8, v9}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lu0/a$c;

    if-nez v7, :cond_1

    move-object v7, v8

    goto :goto_1

    :cond_1
    if-eq v7, v8, :cond_2

    move v6, v2

    :cond_2
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_3
    if-eqz v6, :cond_4

    move-object v7, v4

    :cond_4
    if-nez v7, :cond_7

    new-instance v7, Lu0/a$c;

    invoke-direct {v7, v3, v4}, Lu0/a$c;-><init>([Lu0/b;Lu0/a$a;)V

    :goto_2
    if-ge v1, v0, :cond_7

    aget-object p0, v3, v1

    sget-object v2, Lu0/a;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, p0, v7}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lu0/a$c;

    if-eqz p0, :cond_6

    iget-object v2, p0, Lu0/a$c;->c:Lu0/f;

    if-eqz v2, :cond_5

    check-cast v2, Lw0/f;

    invoke-virtual {v2}, Lw0/f;->i()V

    :cond_5
    iget-object p0, p0, Lu0/a$c;->a:Lw0/h;

    if-eqz p0, :cond_6

    invoke-interface {p0}, Lu0/d;->i()V

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_7
    return-object v7

    :cond_8
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "useAt can not be applied to empty views array"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static varargs g([Ljava/lang/Object;)Lw0/h;
    .locals 2

    array-length v0, p0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    aget-object p0, p0, v0

    sget-object v0, Lu0/h;->l:Lu0/e;

    invoke-static {p0, v0}, Lu0/a;->d(Ljava/lang/Object;Lu0/e;)Lu0/b;

    move-result-object p0

    goto :goto_0

    :cond_0
    new-instance p0, Lu0/h;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lu0/h;-><init>(Ljava/lang/Object;)V

    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lu0/b;->e:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lu0/b;->f:J

    :goto_0
    invoke-static {p0}, Lu0/a;->e(Lu0/b;)Lu0/c;

    move-result-object p0

    check-cast p0, Lu0/a$c;

    invoke-virtual {p0}, Lu0/a$c;->a()Lw0/h;

    move-result-object p0

    return-object p0
.end method
