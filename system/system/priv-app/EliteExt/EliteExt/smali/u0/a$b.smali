.class public final Lu0/a$b;
.super Landroid/os/Handler;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lu0/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# direct methods
.method public constructor <init>(Landroid/os/Looper;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    sget-object p1, Lu0/a;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu0/b;

    invoke-virtual {v0}, Lu0/b;->i()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    const-wide/16 v4, 0x1

    iget-wide v6, v0, Lu0/b;->e:J

    invoke-static {v6, v7, v4, v5}, Lc1/a;->b(JJ)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lu0/b;->a:Lx0/c;

    new-array v4, v3, [La1/b;

    invoke-virtual {v2, v4}, Lx0/c;->d([La1/b;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, Lu0/b;->a:Lx0/c;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v2, Lx0/e;->h:Lx0/k;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lu0/b;->j()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_1
    new-array v2, v1, [Lu0/b;

    aput-object v0, v2, v3

    invoke-static {v2}, Lu0/a;->b([Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-static {v1}, Lu0/a;->a(Z)V

    return-void

    :cond_3
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    return-void
.end method
