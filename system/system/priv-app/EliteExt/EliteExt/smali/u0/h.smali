.class public Lu0/h;
.super Lu0/b;
.source ""


# static fields
.field public static l:Lu0/e;


# instance fields
.field public k:La1/g;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lu0/h$a;

    invoke-direct {v0}, Lu0/h$a;-><init>()V

    sput-object v0, Lu0/h;->l:Lu0/e;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lu0/h;-><init>(Ljava/lang/Object;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 2

    invoke-direct {p0}, Lu0/b;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/16 v1, 0x3e8

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    if-nez p1, :cond_0

    iget p1, p0, Lu0/b;->c:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    :cond_0
    new-instance v0, La1/g;

    invoke-direct {v0, p1}, La1/g;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lu0/h;->k:La1/g;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public c()F
    .locals 1

    const v0, 0x3b03126f    # 0.002f

    return v0
.end method

.method public d(La1/c;)I
    .locals 2

    invoke-virtual {p0, p1}, Lu0/h;->q(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lu0/h;->k:La1/g;

    invoke-interface {p1}, La1/c;->getName()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-virtual {v0, p1, v1}, La1/g;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    if-nez p1, :cond_0

    const p1, 0x7fffffff

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    :goto_0
    return p1

    :cond_1
    iget-object v0, p0, Lu0/h;->k:La1/g;

    invoke-virtual {v0}, La1/g;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0}, La1/c;->b(Ljava/lang/Object;)I

    move-result p1

    return p1
.end method

.method public e(Ljava/lang/Object;)F
    .locals 1

    instance-of v0, p1, La1/c;

    if-eqz v0, :cond_0

    instance-of v0, p1, La1/a;

    if-nez v0, :cond_0

    const/high16 p1, 0x3f800000    # 1.0f

    return p1

    :cond_0
    invoke-super {p0, p1}, Lu0/b;->e(Ljava/lang/Object;)F

    move-result p1

    return p1
.end method

.method public g()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lu0/h;->k:La1/g;

    return-object v0
.end method

.method public h(La1/b;)F
    .locals 2

    invoke-virtual {p0, p1}, Lu0/h;->q(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lu0/h;->k:La1/g;

    invoke-virtual {p1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-virtual {v0, p1, v1}, La1/g;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Float;

    if-nez p1, :cond_0

    const p1, 0x7f7fffff    # Float.MAX_VALUE

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    :goto_0
    return p1

    :cond_1
    iget-object v0, p0, Lu0/h;->k:La1/g;

    invoke-virtual {v0}, La1/g;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, La1/b;->c(Ljava/lang/Object;)F

    move-result p1

    return p1
.end method

.method public i()Z
    .locals 1

    iget-object v0, p0, Lu0/h;->k:La1/g;

    invoke-virtual {v0}, La1/g;->b()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public l(La1/c;I)V
    .locals 2

    invoke-virtual {p0, p1}, Lu0/h;->q(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lu0/h;->k:La1/g;

    invoke-interface {p1}, La1/c;->getName()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v0, p1, v1, p2}, La1/g;->c(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lu0/h;->k:La1/g;

    invoke-virtual {v0}, La1/g;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0, p2}, La1/c;->a(Ljava/lang/Object;I)V

    :goto_0
    return-void
.end method

.method public n(La1/b;F)V
    .locals 2

    invoke-virtual {p0, p1}, Lu0/h;->q(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lu0/h;->k:La1/g;

    invoke-virtual {p1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p2

    invoke-virtual {v0, p1, v1, p2}, La1/g;->c(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lu0/h;->k:La1/g;

    invoke-virtual {v0}, La1/g;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, La1/b;->d(Ljava/lang/Object;F)V

    :goto_0
    return-void
.end method

.method public p(Ljava/lang/String;Ljava/lang/Class;)La1/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "*>;)",
            "La1/b;"
        }
    .end annotation

    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-eq p2, v0, :cond_1

    const-class v0, Ljava/lang/Integer;

    if-ne p2, v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p2, La1/f;

    invoke-direct {p2, p1}, La1/f;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    :goto_0
    new-instance p2, La1/e;

    invoke-direct {p2, p1}, La1/e;-><init>(Ljava/lang/String;)V

    :goto_1
    return-object p2
.end method

.method public final q(Ljava/lang/Object;)Z
    .locals 1

    instance-of v0, p1, La1/f;

    if-nez v0, :cond_1

    instance-of v0, p1, La1/h;

    if-nez v0, :cond_1

    instance-of p1, p1, La1/a;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method
