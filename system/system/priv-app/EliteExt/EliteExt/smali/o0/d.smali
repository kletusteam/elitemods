.class public final synthetic Lo0/d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Ljava/lang/Object;

.field public final synthetic c:Ljava/lang/Object;


# direct methods
.method public synthetic constructor <init>(Landroid/widget/ArrayAdapter;Ljava/lang/Object;I)V
    .locals 0

    iput p3, p0, Lo0/d;->a:I

    iput-object p1, p0, Lo0/d;->b:Ljava/lang/Object;

    iput-object p2, p0, Lo0/d;->c:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    iget p1, p0, Lo0/d;->a:I

    packed-switch p1, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    iget-object p1, p0, Lo0/d;->b:Ljava/lang/Object;

    check-cast p1, Lcom/EliteDevelopment/ext/SearchAppActivity$a;

    iget-object v0, p0, Lo0/d;->c:Ljava/lang/Object;

    check-cast v0, Lcom/EliteDevelopment/ext/SearchAppActivity$a$a;

    iget-object p1, p1, Lcom/EliteDevelopment/ext/SearchAppActivity$a;->c:Lcom/EliteDevelopment/ext/SearchAppActivity;

    iget-object v0, v0, Lcom/EliteDevelopment/ext/SearchAppActivity$a$a;->a:Lp0/a;

    iget-object v1, p1, Lcom/EliteDevelopment/ext/SearchAppActivity;->r:Lcom/EliteDevelopment/ext/SearchAppActivity$a;

    iget-object v1, v1, Lcom/EliteDevelopment/ext/SearchAppActivity$a;->b:Lp0/a;

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p1, Lcom/EliteDevelopment/ext/SearchAppActivity;->s:Ljava/lang/String;

    iget-object v3, v0, Lp0/a;->b:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object p1, p1, Lcom/EliteDevelopment/ext/SearchAppActivity;->r:Lcom/EliteDevelopment/ext/SearchAppActivity$a;

    iput-object v0, p1, Lcom/EliteDevelopment/ext/SearchAppActivity$a;->b:Lp0/a;

    invoke-virtual {p1}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void

    :pswitch_1
    iget-object p1, p0, Lo0/d;->b:Ljava/lang/Object;

    check-cast p1, Lcom/EliteDevelopment/ext/LaunchAppPicker$b;

    iget-object v0, p0, Lo0/d;->c:Ljava/lang/Object;

    check-cast v0, Lcom/EliteDevelopment/ext/LaunchAppPicker$b$a;

    iget-object p1, p1, Lcom/EliteDevelopment/ext/LaunchAppPicker$b;->b:Lcom/EliteDevelopment/ext/LaunchAppPicker;

    iget-object v0, v0, Lcom/EliteDevelopment/ext/LaunchAppPicker$b$a;->a:Lp0/a;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/app/Activity;->setResult(I)V

    goto :goto_0

    :cond_1
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, v0, Lp0/a;->b:Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const/4 v0, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    :goto_0
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    return-void

    :goto_1
    iget-object p1, p0, Lo0/d;->b:Ljava/lang/Object;

    check-cast p1, Lcom/EliteDevelopment/ext/WifiRegionActivity$a;

    iget-object v0, p0, Lo0/d;->c:Ljava/lang/Object;

    check-cast v0, Lcom/EliteDevelopment/ext/WifiRegionActivity$a$a;

    iget-object p1, p1, Lcom/EliteDevelopment/ext/WifiRegionActivity$a;->c:Lcom/EliteDevelopment/ext/WifiRegionActivity;

    iget-object v0, v0, Lcom/EliteDevelopment/ext/WifiRegionActivity$a$a;->a:Lp0/g;

    iget-object v1, p1, Lcom/EliteDevelopment/ext/WifiRegionActivity;->q:Lcom/EliteDevelopment/ext/WifiRegionActivity$a;

    iget-object v1, v1, Lcom/EliteDevelopment/ext/WifiRegionActivity$a;->b:Lp0/g;

    if-eq v0, v1, :cond_3

    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    if-eqz v0, :cond_2

    iget-object v2, v0, Lp0/g;->b:Ljava/lang/String;

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    const-string v3, "wifi_country_code"

    invoke-static {v1, v3, v2}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object p1, p1, Lcom/EliteDevelopment/ext/WifiRegionActivity;->q:Lcom/EliteDevelopment/ext/WifiRegionActivity$a;

    iput-object v0, p1, Lcom/EliteDevelopment/ext/WifiRegionActivity$a;->b:Lp0/g;

    invoke-virtual {p1}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    :cond_3
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
