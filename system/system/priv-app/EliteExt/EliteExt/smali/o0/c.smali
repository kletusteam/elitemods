.class public final synthetic Lo0/c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field public final synthetic a:Lcom/EliteDevelopment/ext/HideAppsActivity$a;

.field public final synthetic b:Lcom/EliteDevelopment/ext/HideAppsActivity$a$a;


# direct methods
.method public synthetic constructor <init>(Lcom/EliteDevelopment/ext/HideAppsActivity$a;Lcom/EliteDevelopment/ext/HideAppsActivity$a$a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lo0/c;->a:Lcom/EliteDevelopment/ext/HideAppsActivity$a;

    iput-object p2, p0, Lo0/c;->b:Lcom/EliteDevelopment/ext/HideAppsActivity$a$a;

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    iget-object p1, p0, Lo0/c;->a:Lcom/EliteDevelopment/ext/HideAppsActivity$a;

    iget-object v0, p0, Lo0/c;->b:Lcom/EliteDevelopment/ext/HideAppsActivity$a$a;

    iget-object p1, p1, Lcom/EliteDevelopment/ext/HideAppsActivity$a;->b:Lcom/EliteDevelopment/ext/HideAppsActivity;

    iget-object v0, v0, Lcom/EliteDevelopment/ext/HideAppsActivity$a$a;->a:Lp0/a;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lp0/a;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/EliteDevelopment/ext/HideAppsActivity;->u:Ljava/util/Set;

    if-eqz p2, :cond_0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :goto_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_1
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    :cond_2
    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v0, "miui_home_hide_apps"

    invoke-static {p1, v0, p2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    return-void
.end method
