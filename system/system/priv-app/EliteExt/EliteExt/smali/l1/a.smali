.class public Ll1/a;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field public a:Lcom/miui/blur/sdk/drawable/BlurDrawable;

.field public b:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    new-instance v0, Lcom/miui/blur/sdk/drawable/BlurDrawable;

    invoke-direct {v0}, Lcom/miui/blur/sdk/drawable/BlurDrawable;-><init>()V

    iput-object v0, p0, Ll1/a;->a:Lcom/miui/blur/sdk/drawable/BlurDrawable;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v0, v0, 0x30

    const/16 v1, 0xa5

    const/16 v2, 0x20

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Ll1/a;->a:Lcom/miui/blur/sdk/drawable/BlurDrawable;

    const/16 v2, 0x13

    const/16 v3, 0x5c

    invoke-static {v1, v3, v3, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    iget-wide v3, v0, Lcom/miui/blur/sdk/drawable/BlurDrawable;->d:J

    invoke-static {v3, v4, v1, v2}, Lcom/miui/blur/sdk/drawable/BlurDrawable;->nSetMixColor(JII)V

    invoke-virtual {v0}, Lcom/miui/blur/sdk/drawable/BlurDrawable;->a()V

    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060213

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Ll1/a;->a:Lcom/miui/blur/sdk/drawable/BlurDrawable;

    const/16 v2, 0x12

    const/16 v3, 0x6b

    invoke-static {v1, v3, v3, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    iget-wide v3, v0, Lcom/miui/blur/sdk/drawable/BlurDrawable;->d:J

    invoke-static {v3, v4, v1, v2}, Lcom/miui/blur/sdk/drawable/BlurDrawable;->nSetMixColor(JII)V

    invoke-virtual {v0}, Lcom/miui/blur/sdk/drawable/BlurDrawable;->a()V

    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060214

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    :goto_0
    iput-object v0, p0, Ll1/a;->b:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Ll1/a;->a:Lcom/miui/blur/sdk/drawable/BlurDrawable;

    const/high16 v1, 0x3f800000    # 1.0f

    iget-wide v2, v0, Lcom/miui/blur/sdk/drawable/BlurDrawable;->d:J

    invoke-static {v2, v3, v1}, Lcom/miui/blur/sdk/drawable/BlurDrawable;->nSetBlurRatio(JF)V

    invoke-virtual {v0}, Lcom/miui/blur/sdk/drawable/BlurDrawable;->a()V

    return-void
.end method

.method public b(Z)Z
    .locals 4

    sget-object v0, Lcom/miui/blur/sdk/drawable/BlurDrawable;->g:Landroid/os/Handler;

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    iget-object p1, p0, Ll1/a;->a:Lcom/miui/blur/sdk/drawable/BlurDrawable;

    const/4 v1, 0x0

    if-nez p1, :cond_0

    :try_start_0
    invoke-virtual {p0}, Ll1/a;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Blur creat fail e:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v2, "Blur"

    invoke-static {v2, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v0, p0, Ll1/a;->a:Lcom/miui/blur/sdk/drawable/BlurDrawable;

    return v1

    :cond_0
    :goto_0
    iget-object p1, p0, Ll1/a;->a:Lcom/miui/blur/sdk/drawable/BlurDrawable;

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result p1

    if-nez p1, :cond_1

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    if-nez p1, :cond_3

    :cond_1
    invoke-virtual {p0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object p1, p0, Ll1/a;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Ll1/a;->a:Lcom/miui/blur/sdk/drawable/BlurDrawable;

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const/high16 p1, 0x3f800000    # 1.0f

    invoke-virtual {p0, p1}, Ll1/a;->setAlpha(F)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result p1

    if-nez p1, :cond_3

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, Ll1/a;->b:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Ll1/a;->a:Lcom/miui/blur/sdk/drawable/BlurDrawable;

    const/16 p1, 0x8

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    :cond_3
    :goto_1
    const/4 p1, 0x1

    return p1
.end method

.method public setAlpha(F)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setAlpha(F)V

    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr p1, v0

    float-to-int p1, p1

    iget-object v0, p0, Ll1/a;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    :cond_0
    iget-object v0, p0, Ll1/a;->a:Lcom/miui/blur/sdk/drawable/BlurDrawable;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lcom/miui/blur/sdk/drawable/BlurDrawable;->setAlpha(I)V

    :cond_1
    return-void
.end method
