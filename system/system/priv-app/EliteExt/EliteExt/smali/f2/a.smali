.class public Lf2/a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field public final synthetic a:I


# direct methods
.method public synthetic constructor <init>(I)V
    .locals 0

    iput p1, p0, Lf2/a;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 17

    move-object/from16 v0, p0

    move/from16 v1, p1

    iget v2, v0, Lf2/a;->a:I

    const-wide v3, 0x3ff921fb54442d18L    # 1.5707963267948966

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    const/high16 v7, -0x3ee00000    # -10.0f

    const/high16 v8, 0x41200000    # 10.0f

    const/4 v9, 0x0

    const/4 v10, 0x2

    const/high16 v11, -0x41000000    # -0.5f

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    const/high16 v14, 0x3f000000    # 0.5f

    const/high16 v15, 0x40000000    # 2.0f

    const/high16 v16, 0x3f800000    # 1.0f

    packed-switch v2, :pswitch_data_0

    goto/16 :goto_b

    :pswitch_0
    float-to-double v1, v1

    const-wide v3, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v1

    sub-double/2addr v1, v5

    double-to-float v1, v1

    mul-float/2addr v1, v11

    return v1

    :pswitch_1
    float-to-double v1, v1

    mul-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v1

    double-to-float v1, v1

    neg-float v1, v1

    add-float v1, v1, v16

    return v1

    :pswitch_2
    sub-float v1, v1, v16

    mul-float v2, v1, v1

    mul-float/2addr v2, v1

    mul-float/2addr v2, v1

    mul-float/2addr v2, v1

    add-float v2, v2, v16

    return v2

    :pswitch_3
    mul-float/2addr v1, v15

    cmpg-float v2, v1, v16

    if-gez v2, :cond_0

    mul-float/2addr v14, v1

    mul-float/2addr v14, v1

    mul-float/2addr v14, v1

    mul-float/2addr v14, v1

    mul-float/2addr v14, v1

    goto :goto_0

    :cond_0
    sub-float/2addr v1, v15

    mul-float v2, v1, v1

    mul-float/2addr v2, v1

    mul-float/2addr v2, v1

    mul-float/2addr v2, v1

    add-float/2addr v2, v15

    mul-float/2addr v14, v2

    :goto_0
    return v14

    :pswitch_4
    mul-float v2, v1, v1

    mul-float/2addr v2, v1

    mul-float/2addr v2, v1

    mul-float/2addr v2, v1

    return v2

    :pswitch_5
    mul-float/2addr v1, v15

    cmpg-float v2, v1, v16

    if-gez v2, :cond_1

    mul-float/2addr v14, v1

    mul-float/2addr v14, v1

    mul-float/2addr v14, v1

    mul-float/2addr v14, v1

    goto :goto_1

    :cond_1
    sub-float/2addr v1, v15

    mul-float v2, v1, v1

    mul-float/2addr v2, v1

    mul-float/2addr v2, v1

    sub-float/2addr v2, v15

    mul-float v14, v2, v11

    :goto_1
    return v14

    :pswitch_6
    mul-float v2, v1, v1

    mul-float/2addr v2, v1

    mul-float/2addr v2, v1

    return v2

    :pswitch_7
    neg-float v2, v1

    sub-float/2addr v1, v15

    mul-float/2addr v1, v2

    return v1

    :pswitch_8
    mul-float/2addr v1, v15

    cmpg-float v2, v1, v16

    if-gez v2, :cond_2

    mul-float/2addr v14, v1

    move v11, v1

    goto :goto_2

    :cond_2
    sub-float v1, v1, v16

    sub-float v2, v1, v15

    mul-float/2addr v2, v1

    sub-float v14, v2, v16

    :goto_2
    mul-float/2addr v14, v11

    return v14

    :pswitch_9
    mul-float/2addr v1, v1

    return v1

    :pswitch_a
    cmpl-float v2, v1, v16

    if-nez v2, :cond_3

    goto :goto_3

    :cond_3
    mul-float/2addr v1, v7

    float-to-double v1, v1

    invoke-static {v12, v13, v1, v2}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    neg-double v1, v1

    add-double/2addr v1, v5

    double-to-float v1, v1

    move/from16 v16, v1

    :goto_3
    return v16

    :pswitch_b
    cmpl-float v2, v1, v9

    if-nez v2, :cond_4

    goto :goto_5

    :cond_4
    cmpl-float v2, v1, v16

    if-nez v2, :cond_5

    move/from16 v9, v16

    goto :goto_5

    :cond_5
    mul-float/2addr v1, v15

    cmpg-float v2, v1, v16

    sub-float v1, v1, v16

    if-gez v2, :cond_6

    mul-float/2addr v1, v8

    float-to-double v1, v1

    invoke-static {v12, v13, v1, v2}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    goto :goto_4

    :cond_6
    mul-float/2addr v1, v7

    float-to-double v1, v1

    invoke-static {v12, v13, v1, v2}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    neg-double v1, v1

    add-double/2addr v1, v12

    :goto_4
    double-to-float v1, v1

    mul-float v9, v1, v14

    :goto_5
    return v9

    :pswitch_c
    cmpl-float v2, v1, v9

    if-nez v2, :cond_7

    goto :goto_6

    :cond_7
    sub-float v1, v1, v16

    mul-float/2addr v1, v8

    float-to-double v1, v1

    invoke-static {v12, v13, v1, v2}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    double-to-float v9, v1

    :goto_6
    return v9

    :pswitch_d
    sub-float v1, v1, v16

    mul-float v2, v1, v1

    mul-float/2addr v2, v1

    add-float v2, v2, v16

    return v2

    :pswitch_e
    mul-float/2addr v1, v15

    cmpg-float v2, v1, v16

    if-gez v2, :cond_8

    mul-float/2addr v14, v1

    mul-float/2addr v14, v1

    mul-float/2addr v14, v1

    goto :goto_7

    :cond_8
    sub-float/2addr v1, v15

    mul-float v2, v1, v1

    mul-float/2addr v2, v1

    add-float/2addr v2, v15

    mul-float/2addr v14, v2

    :goto_7
    return v14

    :pswitch_f
    mul-float v2, v1, v1

    mul-float/2addr v2, v1

    return v2

    :pswitch_10
    float-to-double v2, v1

    const-wide v4, 0x3fd745d1745d1746L    # 0.36363636363636365

    cmpg-double v4, v2, v4

    const/high16 v5, 0x40f20000    # 7.5625f

    if-gez v4, :cond_9

    mul-float v2, v1, v5

    mul-float/2addr v2, v1

    goto :goto_9

    :cond_9
    const-wide v6, 0x3fe745d1745d1746L    # 0.7272727272727273

    cmpg-double v1, v2, v6

    if-gez v1, :cond_a

    const-wide v6, 0x3fe1745d1745d174L    # 0.5454545454545454

    sub-double/2addr v2, v6

    double-to-float v1, v2

    mul-float/2addr v5, v1

    mul-float/2addr v5, v1

    const/high16 v1, 0x3f400000    # 0.75f

    goto :goto_8

    :cond_a
    const-wide v6, 0x3fed1745d1745d17L    # 0.9090909090909091

    cmpg-double v1, v2, v6

    if-gez v1, :cond_b

    const-wide v6, 0x3fea2e8ba2e8ba2fL    # 0.8181818181818182

    sub-double/2addr v2, v6

    double-to-float v1, v2

    mul-float/2addr v5, v1

    mul-float/2addr v5, v1

    const/high16 v1, 0x3f700000    # 0.9375f

    goto :goto_8

    :cond_b
    const-wide v6, 0x3fee8ba2e8ba2e8cL    # 0.9545454545454546

    sub-double/2addr v2, v6

    double-to-float v1, v2

    mul-float/2addr v5, v1

    mul-float/2addr v5, v1

    const/high16 v1, 0x3f7c0000    # 0.984375f

    :goto_8
    add-float v2, v5, v1

    :goto_9
    return v2

    :pswitch_11
    cmpg-float v2, v1, v14

    if-gez v2, :cond_c

    new-instance v2, Lf2/a;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lf2/a;-><init>(I)V

    mul-float/2addr v1, v15

    invoke-virtual {v2, v1}, Lf2/a;->getInterpolation(F)F

    move-result v1

    mul-float/2addr v1, v14

    goto :goto_a

    :cond_c
    new-instance v2, Lf2/a;

    invoke-direct {v2, v10}, Lf2/a;-><init>(I)V

    mul-float/2addr v1, v15

    sub-float v1, v1, v16

    invoke-virtual {v2, v1}, Lf2/a;->getInterpolation(F)F

    move-result v1

    mul-float/2addr v1, v14

    add-float/2addr v1, v14

    :goto_a
    return v1

    :pswitch_12
    new-instance v2, Lf2/a;

    invoke-direct {v2, v10}, Lf2/a;-><init>(I)V

    sub-float v1, v16, v1

    invoke-virtual {v2, v1}, Lf2/a;->getInterpolation(F)F

    move-result v1

    sub-float v16, v16, v1

    return v16

    :goto_b
    float-to-double v1, v1

    mul-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v1

    double-to-float v1, v1

    return v1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
