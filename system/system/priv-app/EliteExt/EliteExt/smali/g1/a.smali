.class public Lg1/a;
.super Landroid/view/ActionMode;
.source ""

# interfaces
.implements Lh1/g$a;
.implements Lmiuix/view/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg1/a$a;
    }
.end annotation


# instance fields
.field public a:Lg1/a$a;

.field public b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lmiuix/appcompat/internal/app/widget/f;",
            ">;"
        }
    .end annotation
.end field

.field public c:Landroid/view/ActionMode$Callback;

.field public d:Landroid/content/Context;

.field public e:Z

.field public f:Lh1/g;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ActionMode$Callback;)V
    .locals 1

    invoke-direct {p0}, Landroid/view/ActionMode;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lg1/a;->e:Z

    iput-object p1, p0, Lg1/a;->d:Landroid/content/Context;

    iput-object p2, p0, Lg1/a;->c:Landroid/view/ActionMode$Callback;

    new-instance p2, Lh1/g;

    invoke-direct {p2, p1}, Lh1/g;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x1

    iput p1, p2, Lh1/g;->e:I

    iput-object p2, p0, Lg1/a;->f:Lh1/g;

    invoke-virtual {p2, p0}, Lh1/g;->s(Lh1/g$a;)V

    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 0

    return-void
.end method

.method public d(Lh1/g;Landroid/view/MenuItem;)Z
    .locals 0

    iget-object p1, p0, Lg1/a;->c:Landroid/view/ActionMode$Callback;

    if-eqz p1, :cond_0

    invoke-interface {p1, p0, p2}, Landroid/view/ActionMode$Callback;->onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public f(ZF)V
    .locals 0

    return-void
.end method

.method public finish()V
    .locals 4

    iget-boolean v0, p0, Lg1/a;->e:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lg1/a;->e:Z

    iget-object v0, p0, Lg1/a;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/f;

    invoke-interface {v0}, Lmiuix/appcompat/internal/app/widget/f;->d()V

    iget-object v0, p0, Lg1/a;->a:Lg1/a$a;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    check-cast v0, Lmiuix/appcompat/internal/app/widget/c$a;

    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/c$a;->a:Lmiuix/appcompat/internal/app/widget/c;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lmiuix/appcompat/internal/app/widget/c;->k(Z)V

    iget-object v0, v0, Lmiuix/appcompat/internal/app/widget/c$a;->a:Lmiuix/appcompat/internal/app/widget/c;

    iput-object v1, v0, Lmiuix/appcompat/internal/app/widget/c;->a:Landroid/view/ActionMode;

    :cond_1
    iget-object v0, p0, Lg1/a;->c:Landroid/view/ActionMode$Callback;

    if-eqz v0, :cond_2

    invoke-interface {v0, p0}, Landroid/view/ActionMode$Callback;->onDestroyActionMode(Landroid/view/ActionMode;)V

    iput-object v1, p0, Lg1/a;->c:Landroid/view/ActionMode$Callback;

    :cond_2
    return-void
.end method

.method public g(Z)V
    .locals 0

    if-nez p1, :cond_0

    iget-object p1, p0, Lg1/a;->c:Landroid/view/ActionMode$Callback;

    if-eqz p1, :cond_0

    invoke-interface {p1, p0}, Landroid/view/ActionMode$Callback;->onDestroyActionMode(Landroid/view/ActionMode;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lg1/a;->c:Landroid/view/ActionMode$Callback;

    :cond_0
    return-void
.end method

.method public getCustomView()Landroid/view/View;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getCustomView not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getMenu()Landroid/view/Menu;
    .locals 1

    iget-object v0, p0, Lg1/a;->f:Lh1/g;

    return-object v0
.end method

.method public getMenuInflater()Landroid/view/MenuInflater;
    .locals 2

    new-instance v0, Landroid/view/MenuInflater;

    iget-object v1, p0, Lg1/a;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public getSubtitle()Ljava/lang/CharSequence;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getSubtitle not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getTitle not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public i(Lh1/g;)V
    .locals 0

    iget-object p1, p0, Lg1/a;->c:Landroid/view/ActionMode$Callback;

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lg1/a;->invalidate()V

    return-void
.end method

.method public invalidate()V
    .locals 2

    iget-object v0, p0, Lg1/a;->f:Lh1/g;

    invoke-virtual {v0}, Lh1/g;->A()V

    :try_start_0
    iget-object v0, p0, Lg1/a;->c:Landroid/view/ActionMode$Callback;

    iget-object v1, p0, Lg1/a;->f:Lh1/g;

    invoke-interface {v0, p0, v1}, Landroid/view/ActionMode$Callback;->onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lg1/a;->f:Lh1/g;

    invoke-virtual {v0}, Lh1/g;->z()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lg1/a;->f:Lh1/g;

    invoke-virtual {v1}, Lh1/g;->z()V

    throw v0
.end method

.method public setCustomView(Landroid/view/View;)V
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "setCustomView not supported"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setSubtitle(I)V
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "setSubTitle not supported"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "setSubTitle not supported"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setTitle(I)V
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "setTitle not supported"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "setTitle not supported"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
