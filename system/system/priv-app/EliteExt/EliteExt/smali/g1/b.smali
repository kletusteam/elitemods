.class public Lg1/b;
.super Lg1/a;
.source ""

# interfaces
.implements Lmiuix/view/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ActionMode$Callback;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lg1/a;-><init>(Landroid/content/Context;Landroid/view/ActionMode$Callback;)V

    return-void
.end method


# virtual methods
.method public b(ILjava/lang/CharSequence;I)V
    .locals 7

    iget-object v0, p0, Lg1/a;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->q()V

    const/4 v1, 0x0

    const v2, 0x102001a

    const v3, 0x1020019

    const/16 v4, 0x8

    if-ne p1, v3, :cond_2

    iget-object v5, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->y:Landroid/widget/Button;

    if-eqz v5, :cond_1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    if-nez p3, :cond_0

    move v1, v4

    :cond_0
    invoke-virtual {v5, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->y:Landroid/widget/Button;

    invoke-virtual {v1, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->y:Landroid/widget/Button;

    invoke-virtual {v1, p3}, Landroid/widget/Button;->setBackgroundResource(I)V

    :cond_1
    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->z:Li1/a;

    goto :goto_0

    :cond_2
    if-ne p1, v2, :cond_5

    iget-object v5, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->A:Landroid/widget/Button;

    if-eqz v5, :cond_4

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    if-nez p3, :cond_3

    move v1, v4

    :cond_3
    invoke-virtual {v5, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->A:Landroid/widget/Button;

    invoke-virtual {v1, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->A:Landroid/widget/Button;

    invoke-virtual {v1, p3}, Landroid/widget/Button;->setBackgroundResource(I)V

    :cond_4
    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->B:Li1/a;

    :goto_0
    iput-object p2, v1, Li1/a;->h:Ljava/lang/CharSequence;

    :cond_5
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_12

    if-eqz p3, :cond_12

    if-ne p1, v3, :cond_6

    iget-object p1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->y:Landroid/widget/Button;

    goto :goto_1

    :cond_6
    if-ne p1, v2, :cond_7

    iget-object p1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->A:Landroid/widget/Button;

    goto :goto_1

    :cond_7
    const/4 p1, 0x0

    :goto_1
    if-nez p1, :cond_8

    goto/16 :goto_7

    :cond_8
    const p2, 0x7f080181

    if-eq p2, p3, :cond_11

    const p2, 0x7f08017e

    if-ne p2, p3, :cond_9

    goto :goto_5

    :cond_9
    const p2, 0x7f080187

    if-eq p2, p3, :cond_10

    const p2, 0x7f080184

    if-ne p2, p3, :cond_a

    goto :goto_4

    :cond_a
    const p2, 0x7f080199

    if-eq p2, p3, :cond_f

    const p2, 0x7f080196

    if-ne p2, p3, :cond_b

    goto :goto_3

    :cond_b
    const p2, 0x7f080193

    if-eq p2, p3, :cond_e

    const p2, 0x7f080190

    if-ne p2, p3, :cond_c

    goto :goto_2

    :cond_c
    const p2, 0x7f08018d

    if-eq p2, p3, :cond_d

    const p2, 0x7f08018a

    if-ne p2, p3, :cond_12

    :cond_d
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f1200d1

    goto :goto_6

    :cond_e
    :goto_2
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f1200d3

    goto :goto_6

    :cond_f
    :goto_3
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f1200d7

    goto :goto_6

    :cond_10
    :goto_4
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f1200d0

    goto :goto_6

    :cond_11
    :goto_5
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f1200cf

    :goto_6
    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_12
    :goto_7
    return-void
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lg1/a;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public setCustomView(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public setSubtitle(I)V
    .locals 0

    return-void
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .locals 0

    return-void
.end method

.method public setTitle(I)V
    .locals 1

    iget-object v0, p0, Lg1/a;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lg1/b;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lg1/a;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method
