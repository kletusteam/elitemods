.class public abstract La1/h;
.super La1/b;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "La1/b<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final b:La1/h;

.field public static final c:La1/h;

.field public static final d:La1/h;

.field public static final e:La1/h;

.field public static final f:La1/h;

.field public static final g:La1/h;

.field public static final h:La1/h;

.field public static final i:La1/h;

.field public static final j:La1/h;

.field public static final k:La1/h;

.field public static final l:La1/h;

.field public static final m:La1/h;

.field public static final n:La1/h;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, La1/h$k;

    const-string v1, "translationX"

    invoke-direct {v0, v1}, La1/h$k;-><init>(Ljava/lang/String;)V

    sput-object v0, La1/h;->j:La1/h;

    new-instance v0, La1/h$l;

    const-string v1, "translationY"

    invoke-direct {v0, v1}, La1/h$l;-><init>(Ljava/lang/String;)V

    sput-object v0, La1/h;->k:La1/h;

    new-instance v0, La1/h$m;

    const-string v1, "translationZ"

    invoke-direct {v0, v1}, La1/h$m;-><init>(Ljava/lang/String;)V

    new-instance v0, La1/h$n;

    const-string v1, "scaleX"

    invoke-direct {v0, v1}, La1/h$n;-><init>(Ljava/lang/String;)V

    sput-object v0, La1/h;->h:La1/h;

    new-instance v0, La1/h$o;

    const-string v1, "scaleY"

    invoke-direct {v0, v1}, La1/h$o;-><init>(Ljava/lang/String;)V

    sput-object v0, La1/h;->i:La1/h;

    new-instance v0, La1/h$p;

    const-string v1, "rotation"

    invoke-direct {v0, v1}, La1/h$p;-><init>(Ljava/lang/String;)V

    sput-object v0, La1/h;->e:La1/h;

    new-instance v0, La1/h$q;

    const-string v1, "rotationX"

    invoke-direct {v0, v1}, La1/h$q;-><init>(Ljava/lang/String;)V

    sput-object v0, La1/h;->f:La1/h;

    new-instance v0, La1/h$r;

    const-string v1, "rotationY"

    invoke-direct {v0, v1}, La1/h$r;-><init>(Ljava/lang/String;)V

    sput-object v0, La1/h;->g:La1/h;

    new-instance v0, La1/h$s;

    const-string v1, "x"

    invoke-direct {v0, v1}, La1/h$s;-><init>(Ljava/lang/String;)V

    sput-object v0, La1/h;->m:La1/h;

    new-instance v0, La1/h$a;

    const-string v1, "y"

    invoke-direct {v0, v1}, La1/h$a;-><init>(Ljava/lang/String;)V

    sput-object v0, La1/h;->n:La1/h;

    new-instance v0, La1/h$b;

    const-string v1, "z"

    invoke-direct {v0, v1}, La1/h$b;-><init>(Ljava/lang/String;)V

    new-instance v0, La1/h$c;

    const-string v1, "height"

    invoke-direct {v0, v1}, La1/h$c;-><init>(Ljava/lang/String;)V

    sput-object v0, La1/h;->d:La1/h;

    new-instance v0, La1/h$d;

    const-string v1, "width"

    invoke-direct {v0, v1}, La1/h$d;-><init>(Ljava/lang/String;)V

    sput-object v0, La1/h;->l:La1/h;

    new-instance v0, La1/h$e;

    const-string v1, "alpha"

    invoke-direct {v0, v1}, La1/h$e;-><init>(Ljava/lang/String;)V

    sput-object v0, La1/h;->b:La1/h;

    new-instance v0, La1/h$f;

    const-string v1, "autoAlpha"

    invoke-direct {v0, v1}, La1/h$f;-><init>(Ljava/lang/String;)V

    sput-object v0, La1/h;->c:La1/h;

    new-instance v0, La1/h$g;

    const-string v1, "scrollX"

    invoke-direct {v0, v1}, La1/h$g;-><init>(Ljava/lang/String;)V

    new-instance v0, La1/h$h;

    const-string v1, "scrollY"

    invoke-direct {v0, v1}, La1/h$h;-><init>(Ljava/lang/String;)V

    new-instance v0, La1/h$i;

    const-string v1, "deprecated_foreground"

    invoke-direct {v0, v1}, La1/h$i;-><init>(Ljava/lang/String;)V

    new-instance v0, La1/h$j;

    const-string v1, "deprecated_background"

    invoke-direct {v0, v1}, La1/h$j;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, La1/b;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "ViewProperty{mPropertyName=\'"

    invoke-static {v0}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, La1/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
