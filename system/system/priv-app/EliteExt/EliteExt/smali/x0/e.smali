.class public Lx0/e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lz0/b$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lx0/e$b;
    }
.end annotation


# static fields
.field public static final g:Landroid/os/Handler;

.field public static final h:Lx0/k;


# instance fields
.field public volatile a:J

.field public b:[J

.field public volatile c:Z

.field public d:J

.field public e:F

.field public f:I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "AnimRunnerThread"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v1, Lx0/k;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Lx0/k;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lx0/e;->h:Lx0/k;

    new-instance v0, Lx0/e$a;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lx0/e$a;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lx0/e;->g:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(Lx0/e$a;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x10

    iput-wide v0, p0, Lx0/e;->a:J

    const/4 p1, 0x5

    new-array p1, p1, [J

    fill-array-data p1, :array_0

    iput-object p1, p0, Lx0/e;->b:[J

    const/4 p1, 0x0

    iput p1, p0, Lx0/e;->f:I

    return-void

    nop

    :array_0
    .array-data 8
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public static b()V
    .locals 4

    sget-object v0, Lx0/e$b;->a:Lx0/e;

    iget-boolean v1, v0, Lx0/e;->c:Z

    if-nez v1, :cond_0

    return-void

    :cond_0
    sget-boolean v1, Lc1/f;->a:Z

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    new-array v1, v2, [Ljava/lang/Object;

    const-string v3, "AnimRunner.endAnimation"

    invoke-static {v3, v1}, Lc1/f;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    iput-boolean v2, v0, Lx0/e;->c:Z

    invoke-static {}, Lz0/b;->b()Lz0/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lz0/b;->c(Lz0/b$b;)V

    return-void
.end method

.method public static c()V
    .locals 4

    sget-object v0, Lx0/e$b;->a:Lx0/e;

    iget-boolean v1, v0, Lx0/e;->c:Z

    if-eqz v1, :cond_0

    return-void

    :cond_0
    sget-boolean v1, Lc1/f;->a:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "AnimRunner.start"

    invoke-static {v2, v1}, Lc1/f;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    sget-object v1, Lu0/a;->c:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, v0, Lx0/e;->e:F

    const/4 v1, 0x1

    iput-boolean v1, v0, Lx0/e;->c:Z

    invoke-static {}, Lz0/b;->b()Lz0/b;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lz0/b;->a(Lz0/b$b;J)V

    return-void
.end method

.method public static d(Ljava/util/Collection;Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lu0/b;",
            ">;Z)V"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lx0/e;->h:Lx0/k;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu0/b;

    iget-object v1, v0, Lu0/b;->a:Lx0/c;

    const/4 v2, 0x0

    new-array v3, v2, [La1/b;

    invoke-virtual {v1, v3}, Lx0/c;->d([La1/b;)Z

    move-result v1

    iget-object v3, v0, Lu0/b;->a:Lx0/c;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v3, Lx0/e;->h:Lx0/k;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v3

    invoke-virtual {v0}, Lu0/b;->j()Z

    move-result v5

    if-eqz v1, :cond_3

    iget-object v0, v0, Lu0/b;->a:Lx0/c;

    if-eqz p1, :cond_2

    iget-object v1, v0, Lx0/c;->d:Lu0/b;

    iget-object v0, v0, Lx0/c;->g:Ljava/lang/Runnable;

    invoke-virtual {v1, v0}, Lu0/b;->k(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v2}, Lx0/c;->f(Z)V

    goto :goto_0

    :cond_3
    if-nez v3, :cond_1

    if-nez v1, :cond_1

    const-wide/16 v6, 0x1

    iget-wide v8, v0, Lu0/b;->e:J

    invoke-static {v8, v9, v6, v7}, Lc1/a;->b(JJ)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v5, :cond_1

    new-array v1, v4, [Lu0/b;

    aput-object v0, v1, v2

    invoke-static {v1}, Lu0/a;->b([Ljava/lang/Object;)V

    goto :goto_0

    :cond_4
    return-void
.end method


# virtual methods
.method public a(J)Z
    .locals 12

    iget-wide v0, p0, Lx0/e;->d:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iput-wide p1, p0, Lx0/e;->d:J

    move-wide v0, v2

    goto :goto_0

    :cond_0
    sub-long v0, p1, v0

    iput-wide p1, p0, Lx0/e;->d:J

    :goto_0
    iget p1, p0, Lx0/e;->f:I

    iget-object p2, p0, Lx0/e;->b:[J

    rem-int/lit8 v4, p1, 0x5

    aput-wide v0, p2, v4

    const/4 v4, 0x1

    add-int/2addr p1, v4

    iput p1, p0, Lx0/e;->f:I

    array-length p1, p2

    const/4 v5, 0x0

    move-wide v8, v2

    move v6, v5

    move v7, v6

    :goto_1
    if-ge v6, p1, :cond_2

    aget-wide v10, p2, v6

    add-long/2addr v8, v10

    cmp-long v10, v10, v2

    if-lez v10, :cond_1

    add-int/lit8 v7, v7, 0x1

    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_2
    if-lez v7, :cond_3

    int-to-long p1, v7

    div-long/2addr v8, p1

    goto :goto_2

    :cond_3
    move-wide v8, v2

    :goto_2
    cmp-long p1, v8, v2

    if-lez p1, :cond_4

    move-wide v0, v8

    :cond_4
    cmp-long p1, v0, v2

    const-wide/16 v2, 0x10

    if-eqz p1, :cond_5

    cmp-long p1, v0, v2

    if-lez p1, :cond_6

    :cond_5
    move-wide v0, v2

    :cond_6
    long-to-float p1, v0

    iget p2, p0, Lx0/e;->e:F

    div-float/2addr p1, p2

    float-to-double p1, p1

    invoke-static {p1, p2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide p1

    double-to-long p1, p1

    iput-wide p1, p0, Lx0/e;->a:J

    iget-boolean p1, p0, Lx0/e;->c:Z

    if-eqz p1, :cond_d

    sget-object p1, Lu0/a;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    move v0, v5

    :cond_7
    :goto_3
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lu0/b;

    iget-object v2, v1, Lu0/b;->a:Lx0/c;

    new-array v3, v5, [La1/b;

    invoke-virtual {v2, v3}, Lx0/c;->d([La1/b;)Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v1, v1, Lu0/b;->a:Lx0/c;

    invoke-virtual {v1}, Lx0/c;->a()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_3

    :cond_8
    const/16 p2, 0x1f4

    if-le v0, p2, :cond_9

    goto :goto_4

    :cond_9
    move v4, v5

    :goto_4
    if-nez v4, :cond_a

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result p2

    if-gtz p2, :cond_b

    :cond_a
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result p2

    if-nez p2, :cond_c

    :cond_b
    invoke-static {p1, v4}, Lx0/e;->d(Ljava/util/Collection;Z)V

    :cond_c
    sget-object p2, Lx0/e;->h:Lx0/k;

    invoke-virtual {p2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x3

    iput v1, v0, Landroid/os/Message;->what:I

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {p2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    if-eqz v4, :cond_d

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result p2

    if-lez p2, :cond_d

    invoke-static {p1, v4}, Lx0/e;->d(Ljava/util/Collection;Z)V

    :cond_d
    iget-boolean p1, p0, Lx0/e;->c:Z

    return p1
.end method
