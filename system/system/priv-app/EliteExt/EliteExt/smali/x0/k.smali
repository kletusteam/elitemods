.class public Lx0/k;
.super Landroid/os/Handler;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lx0/k$b;
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lu0/b;",
            ">;"
        }
    .end annotation
.end field

.field public b:I

.field public c:Z

.field public d:J

.field public final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lu0/b;",
            "Lx0/d;",
            ">;"
        }
    .end annotation
.end field

.field public f:Z

.field public final g:[I

.field public h:Z

.field public final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lx0/i;",
            ">;"
        }
    .end annotation
.end field

.field public j:J

.field public final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lx0/p;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lu0/b;",
            "Lx0/p;",
            ">;"
        }
    .end annotation
.end field

.field public final m:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lu0/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Looper;)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Lx0/k;->m:Ljava/util/Set;

    new-instance p1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {p1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object p1, p0, Lx0/k;->e:Ljava/util/Map;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lx0/k;->l:Ljava/util/Map;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lx0/k;->i:Ljava/util/List;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lx0/k;->a:Ljava/util/List;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lx0/k;->k:Ljava/util/List;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lx0/k;->d:J

    iput-wide v0, p0, Lx0/k;->j:J

    const/4 p1, 0x0

    iput p1, p0, Lx0/k;->b:I

    const/4 p1, 0x2

    new-array p1, p1, [I

    iput-object p1, p0, Lx0/k;->g:[I

    return-void
.end method


# virtual methods
.method public final a(Lu0/b;Lc1/e;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lc1/e;",
            ">(",
            "Lu0/b;",
            "TT;",
            "Ljava/util/Map<",
            "Lu0/b;",
            "TT;>;)V"
        }
    .end annotation

    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lc1/e;

    if-nez v0, :cond_0

    invoke-interface {p3, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_0
    :goto_0
    if-ne v0, p2, :cond_1

    goto :goto_1

    :cond_1
    iget-object p1, v0, Lc1/e;->a:Lc1/e;

    if-nez p1, :cond_2

    iput-object p2, v0, Lc1/e;->a:Lc1/e;

    :goto_1
    return-void

    :cond_2
    move-object v0, p1

    goto :goto_0
.end method

.method public final b()V
    .locals 15

    iget-object v0, p0, Lx0/k;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_e

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lx0/p;

    iget-object v3, p0, Lx0/k;->m:Ljava/util/Set;

    iget-object v4, v1, Lx0/p;->j:Lu0/b;

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v3, v1, Lx0/p;->j:Lu0/b;

    iget-object v3, v3, Lu0/b;->a:Lx0/c;

    iget-object v4, v3, Lx0/c;->b:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v5, v1, Lx0/p;->f:Ljava/lang/Object;

    invoke-virtual {v4, v5, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    iput-wide v4, v1, Lx0/p;->h:J

    iget-object v4, v1, Lx0/p;->d:Lw0/a;

    iget-object v5, v1, Lx0/p;->k:Lw0/a;

    sget-boolean v6, Lc1/f;->a:Z

    const-string v7, "-- doSetup, target = "

    if-eqz v6, :cond_1

    invoke-static {v7}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v1, Lx0/p;->j:Lu0/b;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v9, ", key = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v9, v1, Lx0/p;->f:Ljava/lang/Object;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v9, ", f = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v9, ", t = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v9, "\nconfig = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v9, v1, Lx0/p;->c:Lv0/a;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-array v9, v2, [Ljava/lang/Object;

    invoke-static {v8, v9}, Lc1/f;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v5}, Lw0/a;->f()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v5, v10}, Lw0/a;->d(Ljava/lang/Object;)La1/b;

    move-result-object v10

    invoke-virtual {v3, v10}, Lx0/c;->c(La1/b;)Ly0/c;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v12, v11, Ly0/c;->a:Lx0/b;

    iget-object v13, v1, Lx0/p;->j:Lu0/b;

    invoke-virtual {v5, v13, v10}, Lw0/a;->b(Lu0/b;La1/b;)D

    move-result-wide v13

    iput-wide v13, v12, Lx0/b;->i:D

    iget-object v12, v11, Ly0/c;->a:Lx0/b;

    if-eqz v4, :cond_3

    iget-object v13, v1, Lx0/p;->j:Lu0/b;

    invoke-virtual {v4, v13, v10}, Lw0/a;->b(Lu0/b;La1/b;)D

    move-result-wide v13

    iput-wide v13, v12, Lx0/b;->h:D

    goto :goto_2

    :cond_3
    iget-wide v12, v12, Lx0/b;->h:D

    iget-object v14, v1, Lx0/p;->j:Lu0/b;

    invoke-static {v14, v10, v12, v13}, Landroidx/emoji2/text/l;->y(Lu0/b;La1/b;D)D

    move-result-wide v12

    invoke-static {v12, v13}, Landroidx/emoji2/text/l;->G(D)Z

    move-result v14

    if-nez v14, :cond_4

    iget-object v14, v11, Ly0/c;->a:Lx0/b;

    iput-wide v12, v14, Lx0/b;->h:D

    :cond_4
    :goto_2
    invoke-static {v11}, Landroidx/emoji2/text/l;->z(Ly0/c;)Z

    if-eqz v6, :cond_2

    invoke-static {v7}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v1, Lx0/p;->j:Lu0/b;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v13, ", property = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, ", startValue = "

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v10, v11, Ly0/c;->a:Lx0/b;

    iget-wide v13, v10, Lx0/b;->h:D

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v10, ", targetValue = "

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v10, v11, Ly0/c;->a:Lx0/b;

    iget-wide v13, v10, Lx0/b;->i:D

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v10, ", value = "

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v10, v11, Ly0/c;->a:Lx0/b;

    iget-wide v10, v10, Lx0/b;->k:D

    invoke-virtual {v12, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-array v11, v2, [Ljava/lang/Object;

    invoke-static {v10, v11}, Lc1/f;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_5
    iput-object v8, v1, Lx0/p;->l:Ljava/util/List;

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Lx0/p;->c(Z)V

    iget-object v4, v3, Lx0/c;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    const/4 v6, 0x4

    const/4 v7, 0x0

    if-eqz v5, :cond_c

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lx0/p;

    if-ne v5, v1, :cond_6

    goto :goto_3

    :cond_6
    iget-object v8, v5, Lx0/p;->l:Ljava/util/List;

    iget-object v9, v3, Lx0/c;->e:Ljava/util/List;

    if-nez v9, :cond_7

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, v3, Lx0/c;->e:Ljava/util/List;

    :cond_7
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_8
    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ly0/c;

    iget-object v10, v1, Lx0/p;->k:Lw0/a;

    iget-object v11, v9, Ly0/c;->d:La1/b;

    invoke-virtual {v10, v11}, Lw0/a;->a(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_8

    iget-object v10, v3, Lx0/c;->e:Ljava/util/List;

    invoke-interface {v10, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_9
    iget-object v8, v3, Lx0/c;->e:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_a

    const/4 v7, 0x5

    invoke-virtual {v3, v5, v7, v6}, Lx0/c;->e(Lx0/p;II)V

    goto :goto_3

    :cond_a
    iget-object v6, v3, Lx0/c;->e:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    iget-object v8, v5, Lx0/p;->l:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-eq v6, v8, :cond_b

    iget-object v6, v3, Lx0/c;->e:Ljava/util/List;

    iput-object v6, v5, Lx0/p;->l:Ljava/util/List;

    iput-object v7, v3, Lx0/c;->e:Ljava/util/List;

    invoke-virtual {v5, v2}, Lx0/p;->c(Z)V

    goto :goto_3

    :cond_b
    iget-object v5, v3, Lx0/c;->e:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    goto :goto_3

    :cond_c
    iget-object v3, v1, Lx0/p;->j:Lu0/b;

    iget-object v3, v3, Lu0/b;->a:Lx0/c;

    iget-object v3, v3, Lx0/c;->c:Ljava/util/Set;

    iget-object v4, v1, Lx0/p;->f:Ljava/lang/Object;

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    iget-object v4, v1, Lx0/p;->c:Lv0/a;

    iget-object v4, v4, Lv0/a;->e:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_d

    if-eqz v3, :cond_d

    sget-object v3, Lx0/p;->n:Ljava/util/Map;

    iget v4, v1, Lx0/p;->e:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    check-cast v3, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v4, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, v1, Lx0/p;->j:Lu0/b;

    iget-object v3, v3, Lu0/b;->b:Lx0/l;

    iget v4, v1, Lx0/p;->e:I

    invoke-virtual {v3, v6, v4, v2, v1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    :cond_d
    iget-object v3, v1, Lc1/e;->a:Lc1/e;

    iput-object v7, v1, Lc1/e;->a:Lc1/e;

    move-object v1, v3

    check-cast v1, Lx0/p;

    if-nez v1, :cond_0

    goto/16 :goto_0

    :cond_e
    iget-object v0, p0, Lx0/k;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-boolean v0, p0, Lx0/k;->f:Z

    if-nez v0, :cond_10

    const/4 v0, 0x1

    iput-boolean v0, p0, Lx0/k;->f:Z

    sget-object v0, Lx0/e$b;->a:Lx0/e;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_f

    invoke-static {}, Lx0/e;->c()V

    goto :goto_5

    :cond_f
    sget-object v0, Lx0/e;->g:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_10
    :goto_5
    return-void
.end method

.method public final c(JJZ)V
    .locals 16

    move-object/from16 v0, p0

    iget-object v1, v0, Lx0/k;->m:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual/range {p0 .. p0}, Lx0/k;->d()V

    return-void

    :cond_0
    move-wide/from16 v1, p1

    iput-wide v1, v0, Lx0/k;->d:J

    sget-object v1, Lx0/e$b;->a:Lx0/e;

    iget-wide v1, v1, Lx0/e;->a:J

    iget v3, v0, Lx0/k;->b:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    const-wide/16 v5, 0x2

    mul-long/2addr v5, v1

    cmp-long v5, p3, v5

    if-lez v5, :cond_1

    goto :goto_0

    :cond_1
    move-wide/from16 v1, p3

    :goto_0
    iget-wide v5, v0, Lx0/k;->j:J

    add-long/2addr v5, v1

    iput-wide v5, v0, Lx0/k;->j:J

    add-int/2addr v3, v4

    iput v3, v0, Lx0/k;->b:I

    iget-object v3, v0, Lx0/k;->m:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const/4 v5, 0x0

    move v6, v5

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lu0/b;

    iget-object v7, v7, Lu0/b;->a:Lx0/c;

    invoke-virtual {v7}, Lx0/c;->a()I

    move-result v7

    add-int/2addr v6, v7

    goto :goto_1

    :cond_2
    iget-object v3, v0, Lx0/k;->g:[I

    sget v7, Lx0/n;->a:I

    div-int/lit16 v7, v6, 0xfa0

    invoke-static {v7, v4}, Ljava/lang/Math;->max(II)I

    move-result v7

    sget v8, Lx0/n;->a:I

    if-le v7, v8, :cond_3

    move v7, v8

    :cond_3
    int-to-float v6, v6

    int-to-float v8, v7

    div-float/2addr v6, v8

    float-to-double v8, v6

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v6, v8

    aput v7, v3, v5

    aput v6, v3, v4

    iget-object v3, v0, Lx0/k;->g:[I

    aget v6, v3, v5

    aget v3, v3, v4

    iget-object v7, v0, Lx0/k;->m:Ljava/util/Set;

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lu0/b;

    iget-object v8, v8, Lu0/b;->a:Lx0/c;

    iget-object v9, v0, Lx0/k;->k:Ljava/util/List;

    invoke-virtual {v8, v9}, Lx0/c;->b(Ljava/util/List;)V

    goto :goto_2

    :cond_4
    iget-object v7, v0, Lx0/k;->k:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_5
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_e

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lx0/p;

    iget-object v8, v8, Lx0/p;->b:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lx0/i;

    iget-object v10, v0, Lx0/k;->i:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    const/4 v11, 0x0

    const v12, 0x7fffffff

    :goto_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lx0/i;

    invoke-static {v13}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move v14, v5

    move-object v15, v13

    :goto_5
    if-eqz v15, :cond_6

    iget-object v5, v15, Lx0/i;->b:Lx0/h;

    iget v5, v5, Lx0/h;->a:I

    add-int/2addr v14, v5

    iget-object v5, v15, Lc1/e;->a:Lc1/e;

    move-object v15, v5

    check-cast v15, Lx0/i;

    const/4 v5, 0x0

    goto :goto_5

    :cond_6
    if-ge v14, v12, :cond_7

    move-object v11, v13

    move v12, v14

    :cond_7
    const/4 v5, 0x0

    goto :goto_4

    :cond_8
    if-eqz v11, :cond_d

    iget-object v5, v0, Lx0/k;->i:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v5, v6, :cond_a

    move-object v5, v11

    const/4 v10, 0x0

    :goto_6
    if-eqz v5, :cond_9

    iget-object v12, v5, Lx0/i;->b:Lx0/h;

    iget v12, v12, Lx0/h;->a:I

    add-int/2addr v10, v12

    iget-object v5, v5, Lc1/e;->a:Lc1/e;

    check-cast v5, Lx0/i;

    goto :goto_6

    :cond_9
    iget-object v5, v9, Lx0/i;->b:Lx0/h;

    iget v5, v5, Lx0/h;->a:I

    add-int/2addr v10, v5

    if-le v10, v3, :cond_a

    goto :goto_8

    :cond_a
    :goto_7
    if-ne v11, v9, :cond_b

    goto :goto_9

    :cond_b
    iget-object v5, v11, Lc1/e;->a:Lc1/e;

    if-nez v5, :cond_c

    iput-object v9, v11, Lc1/e;->a:Lc1/e;

    goto :goto_9

    :cond_c
    move-object v11, v5

    goto :goto_7

    :cond_d
    :goto_8
    iget-object v5, v0, Lx0/k;->i:Ljava/util/List;

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_9
    const/4 v5, 0x0

    goto :goto_3

    :cond_e
    iget-object v3, v0, Lx0/k;->i:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    xor-int/2addr v3, v4

    iput-boolean v3, v0, Lx0/k;->c:Z

    sget-object v3, Lx0/i;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v4, v0, Lx0/k;->i:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    iget-object v3, v0, Lx0/k;->i:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lx0/i;

    iget-wide v5, v0, Lx0/k;->j:J

    iput-wide v5, v4, Lx0/i;->g:J

    iput-wide v1, v4, Lx0/i;->c:J

    move/from16 v5, p5

    iput-boolean v5, v4, Lx0/i;->f:Z

    sget-object v6, Lx0/n;->b:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v6, v4}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    goto :goto_a

    :cond_f
    iget-object v1, v0, Lx0/k;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iget-object v1, v0, Lx0/k;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    return-void
.end method

.method public final d()V
    .locals 5

    iget-boolean v0, p0, Lx0/k;->h:Z

    if-eqz v0, :cond_2

    sget-boolean v0, Lc1/f;->a:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    const-string v0, "total time = "

    invoke-static {v0}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v3, p0, Lx0/k;->j:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "frame count = "

    invoke-static {v3}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lx0/k;->b:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v2

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    const-string v0, "RunnerHandler.stopAnimRunner"

    invoke-static {v0, v4}, Lc1/f;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iput-boolean v2, p0, Lx0/k;->h:Z

    iput-boolean v2, p0, Lx0/k;->f:Z

    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lx0/k;->j:J

    iput v2, p0, Lx0/k;->b:I

    sget-object v0, Lx0/e$b;->a:Lx0/e;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-ne v0, v2, :cond_1

    invoke-static {}, Lx0/e;->b()V

    goto :goto_0

    :cond_1
    sget-object v0, Lx0/e;->g:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_2
    :goto_0
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 27

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    iget v0, v7, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2d

    const/4 v2, 0x5

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v8, 0x0

    if-eq v0, v3, :cond_8

    if-eq v0, v4, :cond_5

    if-eq v0, v5, :cond_1

    if-eq v0, v2, :cond_0

    goto/16 :goto_2

    :cond_0
    iget-object v0, v6, Lx0/k;->m:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    invoke-virtual/range {p0 .. p0}, Lx0/k;->d()V

    goto/16 :goto_2

    :cond_1
    iget-object v0, v7, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lx0/k$b;

    iget-object v1, v0, Lx0/k$b;->b:Lu0/b;

    instance-of v1, v1, Lmiuix/animation/ViewTarget;

    iget-object v2, v0, Lx0/k$b;->a:Lw0/a;

    invoke-virtual {v2}, Lw0/a;->f()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, v0, Lx0/k$b;->a:Lw0/a;

    invoke-virtual {v4, v3}, Lw0/a;->d(Ljava/lang/Object;)La1/b;

    move-result-object v3

    iget-object v4, v0, Lx0/k$b;->b:Lu0/b;

    iget-object v4, v4, Lu0/b;->a:Lx0/c;

    iget-object v4, v4, Lx0/c;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ly0/c;

    if-nez v4, :cond_3

    goto :goto_0

    :cond_3
    iget-object v5, v0, Lx0/k$b;->a:Lw0/a;

    iget-object v9, v0, Lx0/k$b;->b:Lu0/b;

    invoke-virtual {v5, v9, v3}, Lw0/a;->b(Lu0/b;La1/b;)D

    move-result-wide v9

    iget-object v3, v4, Ly0/c;->a:Lx0/b;

    iput-wide v9, v3, Lx0/b;->f:D

    if-nez v1, :cond_2

    iget-object v3, v0, Lx0/k$b;->b:Lu0/b;

    invoke-virtual {v4, v3}, Ly0/c;->d(Lu0/b;)V

    goto :goto_0

    :cond_4
    iget-object v1, v0, Lx0/k$b;->b:Lu0/b;

    new-array v2, v8, [La1/b;

    iget-object v1, v1, Lu0/b;->a:Lx0/c;

    invoke-virtual {v1, v2}, Lx0/c;->d([La1/b;)Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v0, v0, Lx0/k$b;->b:Lu0/b;

    iget-object v0, v0, Lu0/b;->a:Lx0/c;

    iget-object v0, v0, Lx0/c;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    goto :goto_2

    :cond_5
    iget-boolean v0, v6, Lx0/k;->f:Z

    if-eqz v0, :cond_7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-object v0, Lx0/e$b;->a:Lx0/e;

    iget-wide v4, v0, Lx0/e;->a:J

    iget-object v0, v7, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    iget-boolean v0, v6, Lx0/k;->h:Z

    if-nez v0, :cond_6

    iput-boolean v1, v6, Lx0/k;->h:Z

    const-wide/16 v0, 0x0

    iput-wide v0, v6, Lx0/k;->j:J

    iput v8, v6, Lx0/k;->b:I

    goto :goto_1

    :cond_6
    iget-boolean v0, v6, Lx0/k;->c:Z

    if-nez v0, :cond_7

    iget-wide v0, v6, Lx0/k;->d:J

    sub-long v0, v2, v0

    move-wide v4, v0

    :goto_1
    move-object/from16 v0, p0

    move-wide v1, v2

    move-wide v3, v4

    move v5, v9

    invoke-virtual/range {v0 .. v5}, Lx0/k;->c(JJZ)V

    :cond_7
    :goto_2
    move-object v0, v6

    move-object v2, v7

    goto/16 :goto_1f

    :cond_8
    iput-boolean v8, v6, Lx0/k;->c:Z

    iget-object v0, v6, Lx0/k;->m:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    move v2, v8

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lu0/b;

    iget-object v4, v6, Lx0/k;->k:Ljava/util/List;

    iget-object v5, v3, Lu0/b;->a:Lx0/c;

    invoke-virtual {v5, v4}, Lx0/c;->b(Ljava/util/List;)V

    iget-object v5, v6, Lx0/k;->e:Ljava/util/Map;

    invoke-interface {v5, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lx0/d;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v10, v2

    move v11, v10

    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_20

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lx0/p;

    iget-object v13, v6, Lx0/k;->l:Ljava/util/Map;

    iget-object v14, v12, Lx0/p;->j:Lu0/b;

    invoke-interface {v13, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    :goto_5
    check-cast v13, Lx0/p;

    if-eqz v13, :cond_a

    if-ne v13, v12, :cond_9

    move v13, v1

    goto :goto_6

    :cond_9
    iget-object v13, v13, Lc1/e;->a:Lc1/e;

    goto :goto_5

    :cond_a
    move v13, v2

    :goto_6
    if-eqz v13, :cond_b

    add-int/lit8 v11, v11, 0x1

    goto :goto_4

    :cond_b
    if-eqz v5, :cond_c

    iget-wide v1, v12, Lx0/p;->h:J

    iget-wide v13, v5, Lx0/d;->c:J

    cmp-long v1, v1, v13

    if-lez v1, :cond_c

    add-int/lit8 v10, v10, 0x1

    const/4 v1, 0x0

    goto :goto_7

    :cond_c
    move-object v1, v5

    :goto_7
    iget-object v2, v12, Lx0/p;->g:Lx0/h;

    invoke-virtual {v2}, Lx0/h;->clear()V

    iget-object v2, v12, Lx0/p;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lx0/i;

    iget-object v14, v12, Lx0/p;->g:Lx0/h;

    iget-object v13, v13, Lx0/i;->b:Lx0/h;

    iget v15, v14, Lx0/h;->a:I

    move-object/from16 v16, v0

    iget v0, v13, Lx0/h;->a:I

    add-int/2addr v15, v0

    iput v15, v14, Lx0/h;->a:I

    iget v0, v14, Lx0/h;->f:I

    iget v15, v13, Lx0/h;->f:I

    add-int/2addr v0, v15

    iput v0, v14, Lx0/h;->f:I

    iget v0, v14, Lx0/h;->e:I

    iget v15, v13, Lx0/h;->e:I

    add-int/2addr v0, v15

    iput v0, v14, Lx0/h;->e:I

    iget v0, v14, Lx0/h;->d:I

    iget v15, v13, Lx0/h;->d:I

    add-int/2addr v0, v15

    iput v0, v14, Lx0/h;->d:I

    iget v0, v14, Lx0/h;->g:I

    iget v15, v13, Lx0/h;->g:I

    add-int/2addr v0, v15

    iput v0, v14, Lx0/h;->g:I

    iget v0, v14, Lx0/h;->b:I

    iget v15, v13, Lx0/h;->b:I

    add-int/2addr v0, v15

    iput v0, v14, Lx0/h;->b:I

    iget v0, v14, Lx0/h;->c:I

    iget v13, v13, Lx0/h;->c:I

    add-int/2addr v0, v13

    iput v0, v14, Lx0/h;->c:I

    move-object/from16 v0, v16

    goto :goto_8

    :cond_d
    move-object/from16 v16, v0

    iget-object v0, v12, Lx0/p;->g:Lx0/h;

    invoke-virtual {v0}, Lx0/h;->b()Z

    move-result v2

    if-eqz v2, :cond_1a

    iget-object v2, v12, Lx0/p;->j:Lu0/b;

    iget-object v2, v2, Lu0/b;->a:Lx0/c;

    iget-object v2, v2, Lx0/c;->c:Ljava/util/Set;

    iget-object v13, v12, Lx0/p;->f:Ljava/lang/Object;

    invoke-interface {v2, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    iget-object v13, v12, Lx0/p;->b:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_9
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_18

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lx0/i;

    iget-object v15, v12, Lx0/p;->l:Ljava/util/List;

    move/from16 v17, v8

    iget v8, v14, Lx0/i;->e:I

    move-object/from16 v18, v9

    iget-object v9, v14, Lx0/i;->b:Lx0/h;

    iget v9, v9, Lx0/h;->a:I

    move/from16 v19, v10

    move-object/from16 v20, v13

    move v10, v8

    :goto_a
    add-int v13, v9, v8

    if-ge v10, v13, :cond_17

    invoke-interface {v15, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ly0/c;

    if-eqz v13, :cond_16

    invoke-static {v13}, Landroidx/emoji2/text/l;->z(Ly0/c;)Z

    move-result v21

    if-nez v21, :cond_e

    const/16 v21, 0x0

    move/from16 v22, v9

    move/from16 v26, v21

    move/from16 v21, v8

    move/from16 v8, v26

    goto :goto_c

    :cond_e
    move/from16 v21, v8

    iget-object v8, v13, Ly0/c;->a:Lx0/b;

    iget-byte v8, v8, Lx0/b;->d:B

    invoke-static {v8}, Lx0/i;->a(B)Z

    move-result v8

    if-eqz v8, :cond_f

    iget-object v8, v14, Lx0/i;->b:Lx0/h;

    move/from16 v22, v9

    iget v9, v8, Lx0/h;->b:I

    add-int/lit8 v9, v9, 0x1

    iput v9, v8, Lx0/h;->b:I

    iget v8, v0, Lx0/h;->b:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v0, Lx0/h;->b:I

    const/4 v8, 0x4

    invoke-virtual {v13, v8}, Ly0/c;->c(B)V

    iget-object v8, v13, Ly0/c;->a:Lx0/b;

    iget-byte v8, v8, Lx0/b;->d:B

    invoke-static {v14, v0, v13, v8}, Lx0/p;->a(Lx0/i;Lx0/h;Ly0/c;B)V

    goto :goto_b

    :cond_f
    move/from16 v22, v9

    :goto_b
    const/4 v8, 0x1

    :goto_c
    if-nez v8, :cond_15

    if-eqz v2, :cond_15

    if-eqz v1, :cond_15

    iget-object v8, v13, Ly0/c;->a:Lx0/b;

    iget-byte v8, v8, Lx0/b;->d:B

    invoke-static {v8}, Lx0/i;->a(B)Z

    move-result v9

    if-eqz v9, :cond_15

    iget-byte v9, v1, Lx0/d;->a:B

    if-eqz v9, :cond_15

    iget-object v9, v1, Lx0/d;->b:Ljava/util/List;

    move-object/from16 v23, v15

    if-eqz v9, :cond_10

    iget-object v15, v13, Ly0/c;->d:La1/b;

    invoke-interface {v9, v15}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_14

    :cond_10
    iget-object v9, v13, Ly0/c;->a:Lx0/b;

    iget-byte v9, v9, Lx0/b;->d:B

    invoke-static {v9}, Lx0/i;->a(B)Z

    move-result v9

    if-eqz v9, :cond_14

    iget v9, v1, Lx0/d;->e:I

    add-int/lit8 v9, v9, 0x1

    iput v9, v1, Lx0/d;->e:I

    iget-byte v9, v1, Lx0/d;->a:B

    const/4 v15, 0x3

    if-ne v9, v15, :cond_12

    iget-object v9, v13, Ly0/c;->a:Lx0/b;

    iget-wide v6, v9, Lx0/b;->i:D

    const-wide v24, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v6, v6, v24

    if-eqz v6, :cond_11

    iget-object v6, v13, Ly0/c;->a:Lx0/b;

    move-object v7, v4

    move-object v15, v5

    iget-wide v4, v6, Lx0/b;->i:D

    iput-wide v4, v6, Lx0/b;->k:D

    goto :goto_d

    :cond_11
    move-object v7, v4

    move-object v15, v5

    :goto_d
    iget-object v4, v14, Lx0/i;->b:Lx0/h;

    iget v5, v4, Lx0/h;->c:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v4, Lx0/h;->c:I

    iget v4, v0, Lx0/h;->c:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v0, Lx0/h;->c:I

    goto :goto_e

    :cond_12
    move-object v7, v4

    move-object v15, v5

    const/4 v4, 0x4

    if-ne v9, v4, :cond_13

    iget-object v4, v14, Lx0/i;->b:Lx0/h;

    iget v5, v4, Lx0/h;->b:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v4, Lx0/h;->b:I

    iget v4, v0, Lx0/h;->b:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v0, Lx0/h;->b:I

    :cond_13
    :goto_e
    iget-byte v4, v1, Lx0/d;->a:B

    invoke-virtual {v13, v4}, Ly0/c;->c(B)V

    invoke-static {v14, v0, v13, v8}, Lx0/p;->a(Lx0/i;Lx0/h;Ly0/c;B)V

    goto :goto_11

    :cond_14
    move-object v7, v4

    :goto_f
    move-object v15, v5

    goto :goto_11

    :cond_15
    move-object v7, v4

    goto :goto_10

    :cond_16
    move-object v7, v4

    move/from16 v21, v8

    move/from16 v22, v9

    :goto_10
    move-object/from16 v23, v15

    goto :goto_f

    :goto_11
    add-int/lit8 v10, v10, 0x1

    move-object/from16 v6, p0

    move-object v4, v7

    move-object v5, v15

    move/from16 v8, v21

    move/from16 v9, v22

    move-object/from16 v15, v23

    move-object/from16 v7, p1

    goto/16 :goto_a

    :cond_17
    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move/from16 v8, v17

    move-object/from16 v9, v18

    move/from16 v10, v19

    move-object/from16 v13, v20

    goto/16 :goto_9

    :cond_18
    move-object v7, v4

    move-object v15, v5

    move/from16 v17, v8

    move-object/from16 v18, v9

    move/from16 v19, v10

    if-nez v2, :cond_19

    iget-object v2, v12, Lx0/p;->j:Lu0/b;

    iget-object v2, v2, Lu0/b;->a:Lx0/c;

    iget-object v2, v2, Lx0/c;->c:Ljava/util/Set;

    iget-object v4, v12, Lx0/p;->f:Ljava/lang/Object;

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_19
    invoke-virtual {v0}, Lx0/h;->a()Z

    move-result v2

    if-eqz v2, :cond_1b

    iget v2, v0, Lx0/h;->g:I

    if-lez v2, :cond_1b

    iget-object v2, v12, Lx0/p;->j:Lu0/b;

    iget-object v2, v2, Lu0/b;->a:Lx0/c;

    iget-object v2, v2, Lx0/c;->a:Ljava/util/Set;

    iget-object v4, v12, Lx0/p;->f:Ljava/lang/Object;

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1b

    sget-object v2, Lx0/p;->n:Ljava/util/Map;

    iget v4, v12, Lx0/p;->e:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    check-cast v2, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v4, v12}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v12, Lx0/p;->j:Lu0/b;

    iget-object v2, v2, Lu0/b;->b:Lx0/l;

    iget v4, v12, Lx0/p;->e:I

    const/4 v5, 0x0

    invoke-virtual {v2, v5, v4, v5}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto :goto_12

    :cond_1a
    move-object v7, v4

    move-object v15, v5

    move/from16 v17, v8

    move-object/from16 v18, v9

    move/from16 v19, v10

    :cond_1b
    :goto_12
    sget-boolean v2, Lc1/f;->a:Z

    if-eqz v2, :cond_1d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "---- updateAnim, target = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, "key = "

    invoke-static {v4}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v12, Lx0/p;->f:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "useOp = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v5, "info.startTime = "

    invoke-static {v5}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v8, v12, Lx0/p;->h:J

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "opInfo.time = "

    invoke-static {v6}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object v8, v15

    if-eqz v15, :cond_1c

    iget-wide v9, v8, Lx0/d;->c:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    goto :goto_13

    :cond_1c
    const/4 v9, 0x0

    :goto_13
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v9, "stats.isRunning = "

    invoke-static {v9}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Lx0/h;->a()Z

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "stats = "

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v13, 0x6

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v4, v13, v14

    const/4 v4, 0x1

    aput-object v1, v13, v4

    const/4 v1, 0x2

    aput-object v5, v13, v1

    const/4 v1, 0x3

    aput-object v6, v13, v1

    const/4 v5, 0x4

    aput-object v9, v13, v5

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x5

    aput-object v6, v13, v9

    invoke-static {v2, v13}, Lc1/f;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v2, v14

    goto :goto_14

    :cond_1d
    move-object v8, v15

    const/4 v1, 0x3

    const/4 v5, 0x4

    const/4 v2, 0x0

    const/4 v4, 0x1

    :goto_14
    invoke-virtual {v0}, Lx0/h;->a()Z

    move-result v6

    if-nez v6, :cond_1f

    iget-object v6, v3, Lu0/b;->a:Lx0/c;

    iget v9, v0, Lx0/h;->b:I

    iget v0, v0, Lx0/h;->c:I

    if-le v9, v0, :cond_1e

    move v1, v5

    :cond_1e
    const/4 v0, 0x2

    invoke-virtual {v6, v12, v0, v1}, Lx0/c;->e(Lx0/p;II)V

    goto :goto_15

    :cond_1f
    add-int/lit8 v11, v11, 0x1

    :goto_15
    move-object/from16 v6, p0

    move v1, v4

    move-object v4, v7

    move-object v5, v8

    move-object/from16 v0, v16

    move/from16 v8, v17

    move-object/from16 v9, v18

    move/from16 v10, v19

    move-object/from16 v7, p1

    goto/16 :goto_4

    :cond_20
    move-object/from16 v16, v0

    move-object v7, v4

    move/from16 v17, v8

    move-object v8, v5

    if-eqz v8, :cond_25

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-eq v10, v0, :cond_24

    iget-object v0, v8, Lx0/d;->b:Ljava/util/List;

    if-nez v0, :cond_21

    move v0, v2

    goto :goto_16

    :cond_21
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_16
    iget v4, v8, Lx0/d;->e:I

    if-nez v0, :cond_22

    if-lez v4, :cond_23

    goto :goto_17

    :cond_22
    if-ne v4, v0, :cond_23

    :goto_17
    move v0, v1

    goto :goto_18

    :cond_23
    move v0, v2

    :goto_18
    if-eqz v0, :cond_25

    :cond_24
    move-object/from16 v0, p0

    iget-object v4, v0, Lx0/k;->e:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_19

    :cond_25
    move-object/from16 v0, p0

    :goto_19
    invoke-interface {v7}, Ljava/util/List;->clear()V

    if-lez v11, :cond_26

    move v4, v1

    goto :goto_1a

    :cond_26
    move v4, v2

    :goto_1a
    if-nez v4, :cond_29

    iget-object v4, v3, Lu0/b;->a:Lx0/c;

    iget-object v4, v4, Lx0/c;->h:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lx0/p;

    if-eqz v4, :cond_27

    iget-object v5, v4, Lx0/p;->j:Lu0/b;

    iget-object v6, v0, Lx0/k;->l:Ljava/util/Map;

    invoke-virtual {v0, v5, v4, v6}, Lx0/k;->a(Lu0/b;Lc1/e;Ljava/util/Map;)V

    move v4, v1

    goto :goto_1b

    :cond_27
    move v4, v2

    :goto_1b
    if-eqz v4, :cond_28

    goto :goto_1c

    :cond_28
    iget-object v4, v0, Lx0/k;->a:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move/from16 v8, v17

    goto :goto_1d

    :cond_29
    :goto_1c
    move v8, v1

    :goto_1d
    iget-object v3, v0, Lx0/k;->k:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    move-object/from16 v7, p1

    move-object v6, v0

    move-object/from16 v0, v16

    goto/16 :goto_3

    :cond_2a
    move-object v0, v6

    move/from16 v17, v8

    iget-object v2, v0, Lx0/k;->m:Ljava/util/Set;

    iget-object v3, v0, Lx0/k;->a:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    iget-object v2, v0, Lx0/k;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    iget-object v2, v0, Lx0/k;->l:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2b

    invoke-virtual/range {p0 .. p0}, Lx0/k;->b()V

    goto :goto_1e

    :cond_2b
    move/from16 v1, v17

    :goto_1e
    if-nez v1, :cond_2c

    invoke-virtual/range {p0 .. p0}, Lx0/k;->d()V

    :cond_2c
    move-object/from16 v2, p1

    goto :goto_1f

    :cond_2d
    move-object v0, v6

    sget-object v1, Lx0/p;->n:Ljava/util/Map;

    move-object/from16 v2, p1

    iget v3, v2, Landroid/os/Message;->arg1:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    check-cast v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v3}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lx0/p;

    if-eqz v1, :cond_2e

    iget-object v3, v1, Lx0/p;->j:Lu0/b;

    iget-object v4, v0, Lx0/k;->l:Ljava/util/Map;

    invoke-virtual {v0, v3, v1, v4}, Lx0/k;->a(Lu0/b;Lc1/e;Ljava/util/Map;)V

    iget-boolean v1, v0, Lx0/k;->c:Z

    if-nez v1, :cond_2e

    invoke-virtual/range {p0 .. p0}, Lx0/k;->b()V

    :cond_2e
    :goto_1f
    const/4 v1, 0x0

    iput-object v1, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    return-void
.end method
