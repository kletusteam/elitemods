.class public final Lx0/l;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final a:Lu0/b;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lx0/p;",
            ">;"
        }
    .end annotation
.end field

.field public final c:J


# direct methods
.method public constructor <init>(Lu0/b;)V
    .locals 2

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lx0/l;->b:Ljava/util/List;

    iput-object p1, p0, Lx0/l;->a:Lu0/b;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Thread;->getId()J

    move-result-wide v0

    iput-wide v0, p0, Lx0/l;->c:J

    return-void
.end method

.method public static a(Lx0/p;Z)V
    .locals 17

    move-object/from16 v1, p0

    invoke-virtual/range {p0 .. p0}, Lx0/p;->b()I

    move-result v0

    const/16 v2, 0xfa0

    if-le v0, v2, :cond_0

    return-void

    :cond_0
    iget-object v0, v1, Lx0/p;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ly0/c;

    iget-object v3, v0, Ly0/c;->d:La1/b;

    sget-object v4, La1/i;->b:La1/i$c;

    if-eq v3, v4, :cond_2

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, v1, Lx0/p;->j:Lu0/b;

    sget-object v6, Lb1/a;->a:Ljava/lang/ThreadLocal;

    const/4 v6, 0x1

    if-eqz p1, :cond_18

    instance-of v7, v5, Lmiuix/animation/ViewTarget;

    if-eqz v7, :cond_3

    check-cast v5, Lmiuix/animation/ViewTarget;

    invoke-virtual {v5}, Lmiuix/animation/ViewTarget;->q()Landroid/view/View;

    move-result-object v5

    goto :goto_1

    :cond_3
    move-object v5, v4

    :goto_1
    if-eqz v5, :cond_4

    move v7, v3

    goto :goto_2

    :cond_4
    move v7, v6

    :goto_2
    if-eqz v7, :cond_5

    goto :goto_0

    :cond_5
    iget-object v0, v0, Ly0/c;->a:Lx0/b;

    iget v7, v0, Lx0/b;->j:I

    invoke-static {v5}, Lb1/b;->c(Landroid/view/View;)Lb1/b;

    move-result-object v0

    if-nez v0, :cond_6

    new-instance v0, Lb1/b;

    invoke-direct {v0}, Lb1/b;-><init>()V

    iput-object v5, v0, Lb1/b;->f:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getForeground()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    iput-object v8, v0, Lb1/b;->c:Landroid/graphics/drawable/Drawable;

    sget-object v8, Lb1/b;->g:Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {v5, v8}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    new-instance v8, Lb1/c;

    invoke-direct {v8, v5, v0}, Lb1/c;-><init>(Landroid/view/View;Lb1/b;)V

    invoke-static {v5, v4}, Lu0/a;->d(Ljava/lang/Object;Lu0/e;)Lu0/b;

    move-result-object v5

    if-eqz v5, :cond_6

    invoke-virtual {v5, v8}, Lu0/b;->k(Ljava/lang/Runnable;)V

    :cond_6
    move-object v5, v0

    sget v0, Lc1/b;->a:I

    const-string v8, "DeviceUtils"

    const-string v9, "getDeviceLevel failed , e:"

    sget v10, Lc1/b;->p:I

    const/4 v11, 0x2

    const/4 v12, -0x1

    const/4 v13, 0x3

    if-ne v10, v0, :cond_7

    sget v10, Lc1/b;->q:I

    if-eq v10, v12, :cond_7

    goto/16 :goto_b

    :cond_7
    sput v0, Lc1/b;->p:I

    :try_start_0
    sget-object v10, Lc1/b;->n:Ljava/lang/reflect/Method;

    sget-object v14, Lc1/b;->r:Ljava/lang/Object;

    new-array v15, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v15, v3

    invoke-virtual {v10, v14, v15}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    invoke-static {v9}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v12

    :goto_3
    invoke-static {v0}, Lc1/b;->f(I)I

    move-result v0

    sput v0, Lc1/b;->q:I

    if-eq v0, v12, :cond_8

    goto :goto_4

    :cond_8
    if-eq v0, v12, :cond_9

    :goto_4
    move v10, v0

    goto/16 :goto_b

    :cond_9
    :try_start_1
    const-string v0, "miui.os.Build"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v10, "IS_MIUI_LITE_VERSION"

    invoke-virtual {v0, v10}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    :catchall_0
    move-exception v0

    const-string v4, "getDeviceLevel failed"

    invoke-static {v8, v4, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v3

    :goto_5
    if-eqz v0, :cond_a

    sput v3, Lc1/b;->q:I

    goto/16 :goto_a

    :cond_a
    new-array v4, v13, [I

    sget v0, Lc1/b;->g:I

    invoke-static {v0}, Lc1/b;->a(I)I

    move-result v0

    aput v0, v4, v3

    sget v0, Lc1/b;->i:I

    invoke-static {v0}, Lc1/b;->a(I)I

    move-result v0

    aput v0, v4, v6

    sget v10, Lc1/b;->a:I

    sget v13, Lc1/b;->h:I

    sget v0, Lc1/b;->g:I

    if-ne v13, v0, :cond_b

    sget v0, Lc1/b;->p:I

    if-ne v0, v10, :cond_c

    sget v0, Lc1/b;->l:I

    if-eq v0, v12, :cond_c

    goto :goto_8

    :cond_b
    sget v0, Lc1/b;->p:I

    if-ne v0, v10, :cond_c

    sget v0, Lc1/b;->o:I

    if-eq v0, v12, :cond_c

    goto :goto_8

    :cond_c
    :try_start_2
    sget-object v0, Lc1/b;->m:Ljava/lang/reflect/Method;

    sget-object v14, Lc1/b;->r:Ljava/lang/Object;

    new-array v15, v11, [Ljava/lang/Object;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v15, v3

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v15, v6

    invoke-virtual {v0, v14, v15}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_6

    :catch_1
    move-exception v0

    invoke-static {v9}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v12

    :goto_6
    invoke-static {v0}, Lc1/b;->f(I)I

    move-result v0

    if-eq v0, v12, :cond_d

    goto :goto_7

    :cond_d
    invoke-static {v13}, Lc1/b;->a(I)I

    move-result v0

    :goto_7
    invoke-static {v10, v0, v13}, Lc1/b;->d(III)I

    move-result v0

    :goto_8
    aput v0, v4, v11

    aget v0, v4, v3

    move v8, v3

    :goto_9
    const/4 v9, 0x3

    if-ge v8, v9, :cond_f

    aget v9, v4, v8

    if-le v9, v12, :cond_e

    if-ge v9, v0, :cond_e

    move v0, v9

    :cond_e
    add-int/lit8 v8, v8, 0x1

    goto :goto_9

    :cond_f
    sput v0, Lc1/b;->q:I

    :goto_a
    sget v0, Lc1/b;->q:I

    goto/16 :goto_4

    :goto_b
    if-nez v10, :cond_10

    if-ne v7, v12, :cond_10

    move v7, v6

    goto :goto_c

    :cond_10
    if-ne v7, v12, :cond_11

    move v7, v3

    :cond_11
    :goto_c
    and-int/lit8 v0, v7, 0x1

    iget-object v4, v5, Lb1/b;->f:Landroid/view/View;

    if-nez v4, :cond_12

    goto/16 :goto_0

    :cond_12
    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    iget-object v7, v5, Lb1/b;->f:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    move-result v7

    if-eqz v4, :cond_17

    if-nez v7, :cond_13

    goto/16 :goto_f

    :cond_13
    iget-object v8, v5, Lb1/b;->a:Landroid/graphics/Bitmap;

    const-string v9, "miuix_anim"

    if-eqz v8, :cond_14

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    if-ne v8, v4, :cond_14

    iget-object v8, v5, Lb1/b;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    iget-object v10, v5, Lb1/b;->f:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getHeight()I

    move-result v10

    if-ne v8, v10, :cond_14

    goto :goto_d

    :cond_14
    invoke-virtual {v5}, Lb1/b;->d()V

    iget-object v8, v5, Lb1/b;->d:Landroid/graphics/Paint;

    invoke-virtual {v8, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    :try_start_3
    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, v5, Lb1/b;->a:Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_d

    :catch_2
    const-string v4, "TintDrawable.createBitmap failed, out of memory"

    invoke-static {v9, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_d
    iget-object v4, v5, Lb1/b;->a:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_16

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-eqz v4, :cond_15

    goto/16 :goto_e

    :cond_15
    :try_start_4
    iget-object v4, v5, Lb1/b;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v4, v3}, Landroid/graphics/Bitmap;->eraseColor(I)V

    new-instance v4, Landroid/graphics/Canvas;

    iget-object v7, v5, Lb1/b;->a:Landroid/graphics/Bitmap;

    invoke-direct {v4, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iget-object v7, v5, Lb1/b;->f:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getScrollX()I

    move-result v7

    iget-object v8, v5, Lb1/b;->f:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getScrollY()I

    move-result v8

    neg-int v7, v7

    int-to-float v7, v7

    neg-int v8, v8

    int-to-float v8, v8

    invoke-virtual {v4, v7, v8}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v7, v5, Lb1/b;->f:Landroid/view/View;

    iget-object v8, v5, Lb1/b;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7, v8}, Landroid/view/View;->setForeground(Landroid/graphics/drawable/Drawable;)V

    iget-object v7, v5, Lb1/b;->f:Landroid/view/View;

    invoke-virtual {v7, v4}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    iget-object v7, v5, Lb1/b;->f:Landroid/view/View;

    invoke-virtual {v7, v5}, Landroid/view/View;->setForeground(Landroid/graphics/drawable/Drawable;)V

    if-nez v0, :cond_1

    new-instance v0, Landroid/graphics/ColorMatrix;

    const/16 v7, 0x14

    new-array v7, v7, [F

    const/high16 v8, 0x3f800000    # 1.0f

    aput v8, v7, v3

    const/4 v3, 0x0

    aput v3, v7, v6

    aput v3, v7, v11

    const/4 v6, 0x3

    aput v3, v7, v6

    const/4 v6, 0x4

    aput v3, v7, v6

    const/4 v6, 0x5

    aput v3, v7, v6

    const/4 v6, 0x6

    aput v8, v7, v6

    const/4 v6, 0x7

    aput v3, v7, v6

    const/16 v6, 0x8

    aput v3, v7, v6

    const/16 v6, 0x9

    aput v3, v7, v6

    const/16 v6, 0xa

    aput v3, v7, v6

    const/16 v6, 0xb

    aput v3, v7, v6

    const/16 v6, 0xc

    aput v8, v7, v6

    const/16 v6, 0xd

    aput v3, v7, v6

    const/16 v6, 0xe

    aput v3, v7, v6

    const/16 v6, 0xf

    aput v3, v7, v6

    const/16 v6, 0x10

    aput v3, v7, v6

    const/16 v6, 0x11

    aput v3, v7, v6

    const/16 v6, 0x12

    const v8, 0x7f7fffff    # Float.MAX_VALUE

    aput v8, v7, v6

    const/16 v6, 0x13

    aput v3, v7, v6

    invoke-direct {v0, v7}, Landroid/graphics/ColorMatrix;-><init>([F)V

    iget-object v6, v5, Lb1/b;->d:Landroid/graphics/Paint;

    new-instance v7, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v7, v0}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    iget-object v0, v5, Lb1/b;->a:Landroid/graphics/Bitmap;

    iget-object v5, v5, Lb1/b;->d:Landroid/graphics/Paint;

    invoke-virtual {v4, v0, v3, v3, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_0

    :catch_3
    move-exception v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "TintDrawable.initBitmap failed, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v9, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_16
    :goto_e
    iget-object v0, v5, Lb1/b;->f:Landroid/view/View;

    iget-object v3, v5, Lb1/b;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v3}, Landroid/view/View;->setForeground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_17
    :goto_f
    invoke-virtual {v5}, Lb1/b;->d()V

    goto/16 :goto_0

    :cond_18
    instance-of v7, v5, Lmiuix/animation/ViewTarget;

    if-eqz v7, :cond_19

    check-cast v5, Lmiuix/animation/ViewTarget;

    invoke-virtual {v5}, Lmiuix/animation/ViewTarget;->q()Landroid/view/View;

    move-result-object v4

    :cond_19
    if-eqz v4, :cond_1a

    goto :goto_10

    :cond_1a
    move v3, v6

    :goto_10
    if-eqz v3, :cond_1b

    goto/16 :goto_0

    :cond_1b
    invoke-static {v4}, Lb1/b;->c(Landroid/view/View;)Lb1/b;

    move-result-object v3

    iget-object v0, v0, Ly0/c;->a:Lx0/b;

    iget-wide v4, v0, Lx0/b;->k:D

    double-to-int v0, v4

    if-eqz v3, :cond_1

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v3}, Lb1/b;->d()V

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    goto/16 :goto_0

    :cond_1c
    return-void
.end method


# virtual methods
.method public final b(ZLx0/p;)V
    .locals 11

    new-instance v4, Ljava/util/ArrayList;

    iget-object v0, p2, Lx0/p;->l:Ljava/util/List;

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ly0/c;

    iget-object v3, v2, Ly0/c;->a:Lx0/b;

    iget-wide v5, v3, Lx0/b;->k:D

    invoke-static {v5, v6}, Landroidx/emoji2/text/l;->G(D)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    :cond_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p2, Lx0/p;->j:Lu0/b;

    iget-object v6, p2, Lx0/p;->f:Ljava/lang/Object;

    iget-object v7, p2, Lx0/p;->i:Ljava/lang/Object;

    if-eqz p1, :cond_3

    instance-of p1, v0, Lmiuix/animation/ViewTarget;

    if-eqz p1, :cond_5

    :cond_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ly0/c;

    iget-object v1, p2, Ly0/c;->a:Lx0/b;

    iget-wide v1, v1, Lx0/b;->k:D

    invoke-static {v1, v2}, Landroidx/emoji2/text/l;->G(D)Z

    move-result v1

    if-eqz v1, :cond_4

    goto :goto_1

    :cond_4
    invoke-virtual {p2, v0}, Ly0/c;->d(Lu0/b;)V

    goto :goto_1

    :cond_5
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result p1

    const p2, 0x9c40

    if-le p1, p2, :cond_6

    invoke-virtual {v0}, Lu0/b;->f()Ly0/a;

    move-result-object v5

    sget-object v8, Ly0/a;->e:Ly0/a$e;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v5 .. v10}, Ly0/a;->b(Ljava/lang/Object;Ljava/lang/Object;Ly0/a$d;Ljava/util/Collection;Ly0/c;)V

    goto :goto_2

    :cond_6
    invoke-virtual {v0}, Lu0/b;->f()Ly0/a;

    move-result-object p1

    invoke-virtual {p1, v6, v7, v4}, Ly0/a;->c(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V

    invoke-virtual {v0}, Lu0/b;->f()Ly0/a;

    move-result-object v0

    sget-object v3, Ly0/a;->h:Ly0/a$h;

    const/4 v5, 0x0

    move-object v1, v6

    move-object v2, v7

    invoke-virtual/range {v0 .. v5}, Ly0/a;->b(Ljava/lang/Object;Ljava/lang/Object;Ly0/a$d;Ljava/util/Collection;Ly0/c;)V

    :cond_7
    :goto_2
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 14

    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0xfa0

    const/4 v2, 0x0

    const-string v3, ", info.key = "

    if-eqz v0, :cond_9

    const/4 v4, 0x2

    const/4 v5, 0x4

    if-eq v0, v4, :cond_2

    const/4 v4, 0x3

    if-eq v0, v4, :cond_1

    if-eq v0, v5, :cond_0

    const/4 v4, 0x5

    if-eq v0, v4, :cond_6

    goto/16 :goto_1

    :cond_0
    sget-object v0, Lx0/p;->n:Ljava/util/Map;

    iget p1, p1, Landroid/os/Message;->arg1:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    check-cast v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lx0/p;

    if-eqz p1, :cond_c

    iget-object v0, p0, Lx0/l;->a:Lu0/b;

    invoke-virtual {v0}, Lu0/b;->f()Ly0/a;

    move-result-object v0

    iget-object v1, p1, Lx0/p;->f:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ly0/a;->d(Ljava/lang/Object;)V

    iget-object v0, p0, Lx0/l;->a:Lu0/b;

    invoke-virtual {v0}, Lu0/b;->f()Ly0/a;

    move-result-object v0

    iget-object v1, p1, Lx0/p;->f:Ljava/lang/Object;

    iget-object p1, p1, Lx0/p;->c:Lv0/a;

    invoke-virtual {v0, v1, p1}, Ly0/a;->a(Ljava/lang/Object;Lv0/a;)Z

    goto/16 :goto_1

    :cond_1
    iget-object p1, p0, Lx0/l;->a:Lu0/b;

    iget-object p1, p1, Lu0/b;->a:Lx0/c;

    iget-object p1, p1, Lx0/c;->h:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    goto/16 :goto_1

    :cond_2
    sget-object v0, Lx0/p;->n:Ljava/util/Map;

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    check-cast v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v4}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx0/p;

    if-nez v0, :cond_3

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lx0/p;

    :cond_3
    if-eqz v0, :cond_6

    iget v4, p1, Landroid/os/Message;->arg2:I

    sget-boolean v6, Lc1/f;->a:Z

    if-eqz v6, :cond_4

    const-string v6, "<<< onEnd, "

    invoke-static {v6}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lx0/l;->a:Lu0/b;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, v0, Lx0/p;->f:Ljava/lang/Object;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v6, v7}, Lc1/f;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    invoke-virtual {p0, v2, v0}, Lx0/l;->b(ZLx0/p;)V

    invoke-static {v0, v2}, Lx0/l;->a(Lx0/p;Z)V

    if-ne v4, v5, :cond_5

    iget-object v4, v0, Lx0/p;->j:Lu0/b;

    invoke-virtual {v4}, Lu0/b;->f()Ly0/a;

    move-result-object v5

    iget-object v6, v0, Lx0/p;->f:Ljava/lang/Object;

    iget-object v7, v0, Lx0/p;->i:Ljava/lang/Object;

    sget-object v8, Ly0/a;->c:Ly0/a$b;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v5 .. v10}, Ly0/a;->b(Ljava/lang/Object;Ljava/lang/Object;Ly0/a$d;Ljava/util/Collection;Ly0/c;)V

    goto :goto_0

    :cond_5
    iget-object v4, v0, Lx0/p;->j:Lu0/b;

    invoke-virtual {v4}, Lu0/b;->f()Ly0/a;

    move-result-object v5

    iget-object v6, v0, Lx0/p;->f:Ljava/lang/Object;

    iget-object v7, v0, Lx0/p;->i:Ljava/lang/Object;

    sget-object v8, Ly0/a;->d:Ly0/a$c;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v5 .. v10}, Ly0/a;->b(Ljava/lang/Object;Ljava/lang/Object;Ly0/a$d;Ljava/util/Collection;Ly0/c;)V

    :goto_0
    iget-object v4, v0, Lx0/p;->j:Lu0/b;

    invoke-virtual {v4}, Lu0/b;->f()Ly0/a;

    move-result-object v4

    iget-object v0, v0, Lx0/p;->f:Ljava/lang/Object;

    invoke-virtual {v4, v0}, Ly0/a;->d(Ljava/lang/Object;)V

    :cond_6
    sget-object v0, Lx0/p;->n:Ljava/util/Map;

    iget p1, p1, Landroid/os/Message;->arg1:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    check-cast v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lx0/p;

    if-eqz p1, :cond_c

    sget-boolean v0, Lc1/f;->a:Z

    if-eqz v0, :cond_7

    const-string v0, "<<< onReplaced, "

    invoke-static {v0}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lx0/l;->a:Lu0/b;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p1, Lx0/p;->f:Ljava/lang/Object;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lc1/f;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_7
    invoke-virtual {p1}, Lx0/p;->b()I

    move-result v0

    if-gt v0, v1, :cond_8

    iget-object v0, p0, Lx0/l;->a:Lu0/b;

    invoke-virtual {v0}, Lu0/b;->f()Ly0/a;

    move-result-object v0

    iget-object v1, p1, Lx0/p;->f:Ljava/lang/Object;

    iget-object v2, p1, Lx0/p;->i:Ljava/lang/Object;

    iget-object v3, p1, Lx0/p;->l:Ljava/util/List;

    invoke-virtual {v0, v1, v2, v3}, Ly0/a;->c(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V

    :cond_8
    iget-object v0, p0, Lx0/l;->a:Lu0/b;

    invoke-virtual {v0}, Lu0/b;->f()Ly0/a;

    move-result-object v1

    iget-object v2, p1, Lx0/p;->f:Ljava/lang/Object;

    iget-object v3, p1, Lx0/p;->i:Ljava/lang/Object;

    sget-object v4, Ly0/a;->c:Ly0/a$b;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Ly0/a;->b(Ljava/lang/Object;Ljava/lang/Object;Ly0/a$d;Ljava/util/Collection;Ly0/c;)V

    iget-object v0, p0, Lx0/l;->a:Lu0/b;

    invoke-virtual {v0}, Lu0/b;->f()Ly0/a;

    move-result-object v0

    iget-object p1, p1, Lx0/p;->f:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ly0/a;->d(Ljava/lang/Object;)V

    goto :goto_1

    :cond_9
    sget-object v0, Lx0/p;->n:Ljava/util/Map;

    iget p1, p1, Landroid/os/Message;->arg1:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    check-cast v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lx0/p;

    if-eqz p1, :cond_c

    sget-boolean v0, Lc1/f;->a:Z

    if-eqz v0, :cond_a

    const-string v0, ">>> onStart, "

    invoke-static {v0}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lx0/l;->a:Lu0/b;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p1, Lx0/p;->f:Ljava/lang/Object;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lc1/f;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_a
    iget-object v0, p1, Lx0/p;->j:Lu0/b;

    invoke-virtual {v0}, Lu0/b;->f()Ly0/a;

    move-result-object v0

    iget-object v2, p1, Lx0/p;->f:Ljava/lang/Object;

    iget-object v3, p1, Lx0/p;->c:Lv0/a;

    invoke-virtual {v0, v2, v3}, Ly0/a;->a(Ljava/lang/Object;Lv0/a;)Z

    iget-object v0, p1, Lx0/p;->j:Lu0/b;

    invoke-virtual {v0}, Lu0/b;->f()Ly0/a;

    move-result-object v2

    iget-object v3, p1, Lx0/p;->f:Ljava/lang/Object;

    iget-object v4, p1, Lx0/p;->i:Ljava/lang/Object;

    sget-object v5, Ly0/a;->b:Ly0/a$a;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Ly0/a;->b(Ljava/lang/Object;Ljava/lang/Object;Ly0/a$d;Ljava/util/Collection;Ly0/c;)V

    iget-object v12, p1, Lx0/p;->l:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v0

    if-gt v0, v1, :cond_b

    iget-object v0, p1, Lx0/p;->j:Lu0/b;

    invoke-virtual {v0}, Lu0/b;->f()Ly0/a;

    move-result-object v8

    iget-object v9, p1, Lx0/p;->f:Ljava/lang/Object;

    iget-object v10, p1, Lx0/p;->i:Ljava/lang/Object;

    sget-object v11, Ly0/a;->f:Ly0/a$f;

    const/4 v13, 0x0

    invoke-virtual/range {v8 .. v13}, Ly0/a;->b(Ljava/lang/Object;Ljava/lang/Object;Ly0/a$d;Ljava/util/Collection;Ly0/c;)V

    :cond_b
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lx0/l;->a(Lx0/p;Z)V

    :cond_c
    :goto_1
    return-void
.end method
