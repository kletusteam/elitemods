.class public Lx0/m$c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lx0/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation


# instance fields
.field public a:Lx0/m$b;

.field public b:La1/b;

.field public c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lu0/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lx0/m$b;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lx0/m$c;->a:Lx0/m$b;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lx0/m$c;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu0/b;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [La1/b;

    const/4 v2, 0x0

    iget-object v3, p0, Lx0/m$c;->b:La1/b;

    aput-object v3, v1, v2

    iget-object v2, v0, Lu0/b;->a:Lx0/c;

    invoke-virtual {v2, v1}, Lx0/c;->d([La1/b;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lx0/m$c;->b:La1/b;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lu0/b;->o(La1/b;D)V

    :cond_0
    iget-object v0, p0, Lx0/m$c;->a:Lx0/m$b;

    iget-object v0, v0, Lx0/m$b;->a:Lc1/j;

    iget-object v1, v0, Lc1/j;->a:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    iget-object v0, v0, Lc1/j;->d:[F

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    :cond_1
    return-void
.end method
