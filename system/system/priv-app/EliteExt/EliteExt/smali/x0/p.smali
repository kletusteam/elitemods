.class public Lx0/p;
.super Lc1/e;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lc1/e<",
        "Lx0/p;",
        ">;"
    }
.end annotation


# static fields
.field public static final m:Ljava/util/concurrent/atomic/AtomicInteger;

.field public static final n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lx0/p;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lx0/i;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:Lv0/a;

.field public volatile d:Lw0/a;

.field public final e:I

.field public volatile f:Ljava/lang/Object;

.field public final g:Lx0/h;

.field public volatile h:J

.field public final i:Ljava/lang/Object;

.field public final j:Lu0/b;

.field public volatile k:Lw0/a;

.field public volatile l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ly0/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lx0/p;->n:Ljava/util/Map;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lx0/p;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(Lu0/b;Lw0/a;Lw0/a;Lv0/b;)V
    .locals 4

    invoke-direct {p0}, Lc1/e;-><init>()V

    sget-object v0, Lx0/p;->m:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    iput v0, p0, Lx0/p;->e:I

    new-instance v1, Lv0/a;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lv0/a;-><init>(Z)V

    iput-object v1, p0, Lx0/p;->c:Lv0/a;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lx0/p;->b:Ljava/util/List;

    new-instance v1, Lx0/h;

    invoke-direct {v1}, Lx0/h;-><init>()V

    iput-object v1, p0, Lx0/p;->g:Lx0/h;

    iput-object p1, p0, Lx0/p;->j:Lu0/b;

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    iget-boolean v3, p2, Lw0/a;->a:Z

    if-eqz v3, :cond_0

    new-instance v3, Lw0/a;

    invoke-direct {v3, v1, v2}, Lw0/a;-><init>(Ljava/lang/Object;Z)V

    invoke-virtual {v3, p2}, Lw0/a;->g(Lw0/a;)V

    move-object p2, v3

    :cond_0
    iput-object p2, p0, Lx0/p;->d:Lw0/a;

    if-eqz p3, :cond_1

    iget-boolean p2, p3, Lw0/a;->a:Z

    if-eqz p2, :cond_1

    new-instance p2, Lw0/a;

    invoke-direct {p2, v1, v2}, Lw0/a;-><init>(Ljava/lang/Object;Z)V

    invoke-virtual {p2, p3}, Lw0/a;->g(Lw0/a;)V

    goto :goto_0

    :cond_1
    move-object p2, p3

    :goto_0
    iput-object p2, p0, Lx0/p;->k:Lw0/a;

    iget-object p2, p0, Lx0/p;->k:Lw0/a;

    iget-object p2, p2, Lw0/a;->d:Ljava/lang/Object;

    iput-object p2, p0, Lx0/p;->i:Ljava/lang/Object;

    iget-boolean v2, p3, Lw0/a;->a:Z

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    :cond_2
    iput-object p2, p0, Lx0/p;->f:Ljava/lang/Object;

    iput-object v1, p0, Lx0/p;->l:Ljava/util/List;

    iget-object p2, p0, Lx0/p;->d:Lw0/a;

    if-nez p2, :cond_3

    goto :goto_2

    :cond_3
    iget-object p2, p0, Lx0/p;->k:Lw0/a;

    invoke-virtual {p2}, Lw0/a;->f()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_4
    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lx0/p;->k:Lw0/a;

    invoke-virtual {v1, v0}, Lw0/a;->e(Ljava/lang/Object;)La1/b;

    move-result-object v0

    instance-of v1, v0, La1/a;

    if-nez v1, :cond_5

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lx0/p;->j:Lu0/b;

    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    invoke-static {v1, v0, v2, v3}, Landroidx/emoji2/text/l;->y(Lu0/b;La1/b;D)D

    move-result-wide v1

    invoke-static {v1, v2}, Landroidx/emoji2/text/l;->G(D)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_1

    :cond_6
    iget-object v1, p0, Lx0/p;->d:Lw0/a;

    iget-object v2, p0, Lx0/p;->j:Lu0/b;

    invoke-virtual {v1, v2, v0}, Lw0/a;->b(Lu0/b;La1/b;)D

    move-result-wide v1

    invoke-static {v1, v2}, Landroidx/emoji2/text/l;->G(D)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lx0/p;->j:Lu0/b;

    check-cast v0, La1/a;

    double-to-int v1, v1

    invoke-virtual {v3, v0, v1}, Lu0/b;->l(La1/c;I)V

    goto :goto_1

    :cond_7
    :goto_2
    iget-object p2, p0, Lx0/p;->c:Lv0/a;

    iget-object p3, p3, Lw0/a;->b:Lv0/a;

    invoke-virtual {p2, p3}, Lv0/a;->b(Lv0/a;)V

    if-eqz p4, :cond_8

    iget-object p2, p0, Lx0/p;->c:Lv0/a;

    invoke-virtual {p4, p2}, Lv0/b;->b(Lv0/a;)V

    :cond_8
    invoke-virtual {p1}, Lu0/b;->f()Ly0/a;

    move-result-object p1

    iget-object p2, p0, Lx0/p;->f:Ljava/lang/Object;

    iget-object p3, p0, Lx0/p;->c:Lv0/a;

    invoke-virtual {p1, p2, p3}, Ly0/a;->a(Ljava/lang/Object;Lv0/a;)Z

    return-void
.end method

.method public static a(Lx0/i;Lx0/h;Ly0/c;B)V
    .locals 3

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    if-ne p3, v0, :cond_0

    iget-object p2, p2, Ly0/c;->a:Lx0/b;

    iget-wide p2, p2, Lx0/b;->a:J

    const-wide/16 v1, 0x0

    cmp-long p2, p2, v1

    if-lez p2, :cond_0

    iget-object p0, p0, Lx0/i;->b:Lx0/h;

    iget p2, p0, Lx0/h;->f:I

    if-lez p2, :cond_0

    sub-int/2addr p2, v0

    iput p2, p0, Lx0/h;->f:I

    iget p0, p1, Lx0/h;->f:I

    sub-int/2addr p0, v0

    iput p0, p1, Lx0/h;->f:I

    :cond_0
    return-void
.end method


# virtual methods
.method public b()I
    .locals 1

    iget-object v0, p0, Lx0/p;->k:Lw0/a;

    invoke-virtual {v0}, Lw0/a;->f()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method

.method public c(Z)V
    .locals 13

    iget-object v0, p0, Lx0/p;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    div-int/lit16 v1, v0, 0xfa0

    const/4 v2, 0x1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    int-to-float v3, v0

    int-to-float v4, v1

    div-float/2addr v3, v4

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-int v3, v3

    iget-object v4, p0, Lx0/p;->b:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-le v4, v1, :cond_0

    iget-object v4, p0, Lx0/p;->b:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    invoke-interface {v4, v1, v5}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    goto :goto_1

    :cond_0
    iget-object v4, p0, Lx0/p;->b:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    :goto_0
    if-ge v4, v1, :cond_1

    iget-object v5, p0, Lx0/p;->b:Ljava/util/List;

    new-instance v6, Lx0/i;

    invoke-direct {v6}, Lx0/i;-><init>()V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    const/4 v1, 0x0

    iget-object v4, p0, Lx0/p;->b:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lx0/i;

    iput-object p0, v5, Lx0/i;->d:Lx0/p;

    add-int v6, v1, v3

    if-le v6, v0, :cond_2

    sub-int v6, v0, v1

    goto :goto_3

    :cond_2
    move v6, v3

    :goto_3
    iget-object v7, v5, Lx0/i;->b:Lx0/h;

    invoke-virtual {v7}, Lx0/h;->clear()V

    iget-object v7, v5, Lx0/i;->b:Lx0/h;

    iput v6, v7, Lx0/h;->a:I

    iput v1, v5, Lx0/i;->e:I

    if-eqz p1, :cond_3

    iget-object v5, v5, Lx0/i;->b:Lx0/h;

    iput v6, v5, Lx0/h;->f:I

    goto :goto_7

    :cond_3
    iget v7, v5, Lx0/i;->e:I

    iget v8, v5, Lx0/i;->e:I

    iget-object v9, v5, Lx0/i;->b:Lx0/h;

    iget v9, v9, Lx0/h;->a:I

    :goto_4
    add-int v10, v8, v9

    if-ge v7, v10, :cond_a

    iget-object v10, v5, Lx0/i;->d:Lx0/p;

    iget-object v10, v10, Lx0/p;->l:Ljava/util/List;

    invoke-interface {v10, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ly0/c;

    if-nez v10, :cond_4

    goto :goto_6

    :cond_4
    iget-object v11, v10, Ly0/c;->a:Lx0/b;

    iget-byte v11, v11, Lx0/b;->d:B

    if-eqz v11, :cond_9

    iget-object v11, v10, Ly0/c;->a:Lx0/b;

    iget-byte v11, v11, Lx0/b;->d:B

    if-ne v11, v2, :cond_5

    goto :goto_5

    :cond_5
    iget-object v11, v5, Lx0/i;->b:Lx0/h;

    iget v12, v11, Lx0/h;->e:I

    add-int/2addr v12, v2

    iput v12, v11, Lx0/h;->e:I

    iget-object v10, v10, Ly0/c;->a:Lx0/b;

    iget-byte v10, v10, Lx0/b;->d:B

    const/4 v11, 0x3

    if-eq v10, v11, :cond_8

    const/4 v11, 0x4

    if-eq v10, v11, :cond_7

    const/4 v11, 0x5

    if-eq v10, v11, :cond_6

    goto :goto_6

    :cond_6
    iget-object v10, v5, Lx0/i;->b:Lx0/h;

    iget v11, v10, Lx0/h;->d:I

    add-int/2addr v11, v2

    iput v11, v10, Lx0/h;->d:I

    goto :goto_6

    :cond_7
    iget-object v10, v5, Lx0/i;->b:Lx0/h;

    iget v11, v10, Lx0/h;->b:I

    add-int/2addr v11, v2

    iput v11, v10, Lx0/h;->b:I

    goto :goto_6

    :cond_8
    iget-object v10, v5, Lx0/i;->b:Lx0/h;

    iget v11, v10, Lx0/h;->c:I

    add-int/2addr v11, v2

    iput v11, v10, Lx0/h;->c:I

    goto :goto_6

    :cond_9
    :goto_5
    iget-object v10, v5, Lx0/i;->b:Lx0/h;

    iget v11, v10, Lx0/h;->f:I

    add-int/2addr v11, v2

    iput v11, v10, Lx0/h;->f:I

    :goto_6
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    :cond_a
    :goto_7
    add-int/2addr v1, v6

    goto/16 :goto_2

    :cond_b
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "TransitionInfo{target = "

    invoke-static {v0}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lx0/p;->j:Lu0/b;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lu0/b;->g()Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", key = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lx0/p;->f:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", propSize = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lx0/p;->k:Lw0/a;

    invoke-virtual {v1}, Lw0/a;->f()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", next = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lc1/e;->a:Lc1/e;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
