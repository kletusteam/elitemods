.class public Lx0/g;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Lx0/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lx0/g;->a:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public static a(Lx0/i;JJZZ)V
    .locals 30

    move-wide/from16 v8, p1

    move-wide/from16 v10, p3

    sget-object v0, Lx0/g;->a:Ljava/lang/ThreadLocal;

    const-class v1, Lx0/a;

    sget-object v2, Lc1/a;->a:[Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    const/4 v3, 0x0

    if-nez v2, :cond_0

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lc1/g;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    :cond_0
    move-object v12, v2

    check-cast v12, Lx0/a;

    sget-boolean v0, Lc1/f;->a:Z

    iput-boolean v0, v12, Lx0/a;->g:Z

    sget-object v0, Lx0/e;->g:Landroid/os/Handler;

    sget-object v0, Lx0/e$b;->a:Lx0/e;

    iget-wide v13, v0, Lx0/e;->a:J

    move-object/from16 v15, p0

    move-wide v0, v10

    :goto_0
    if-eqz v15, :cond_27

    iget-object v2, v15, Lx0/i;->b:Lx0/h;

    iput v3, v2, Lx0/h;->g:I

    invoke-virtual {v2}, Lx0/h;->b()Z

    move-result v16

    iget-object v2, v15, Lx0/i;->d:Lx0/p;

    iget-object v6, v2, Lx0/p;->l:Ljava/util/List;

    iget-object v2, v15, Lx0/i;->d:Lx0/p;

    iget-object v2, v2, Lx0/p;->j:Lu0/b;

    instance-of v7, v2, Lmiuix/animation/ViewTarget;

    iget v2, v15, Lx0/i;->e:I

    iget-object v4, v15, Lx0/i;->b:Lx0/h;

    iget v4, v4, Lx0/h;->a:I

    add-int/2addr v4, v2

    :goto_1
    move v5, v2

    if-ge v5, v4, :cond_26

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ly0/c;

    if-nez v2, :cond_1

    const/4 v2, 0x0

    move v3, v2

    move/from16 v17, v4

    move/from16 v20, v5

    move-object/from16 v18, v6

    move/from16 v19, v7

    move-wide v4, v10

    move-wide/from16 v21, v13

    goto/16 :goto_1a

    :cond_1
    iget-object v3, v15, Lx0/i;->d:Lx0/p;

    iget-object v3, v3, Lx0/p;->c:Lv0/a;

    move/from16 p0, v4

    iget-object v4, v2, Ly0/c;->d:La1/b;

    invoke-virtual {v4}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lv0/a;->c(Ljava/lang/String;)Lv0/c;

    move-result-object v3

    iget-object v4, v15, Lx0/i;->d:Lx0/p;

    iget-object v4, v4, Lx0/p;->c:Lv0/a;

    move/from16 v17, v5

    iget-object v5, v2, Ly0/c;->d:La1/b;

    iput-object v5, v12, Lx0/a;->j:La1/b;

    move-object/from16 v18, v6

    iget-wide v5, v2, Ly0/c;->f:D

    iput-wide v5, v12, Lx0/a;->p:D

    iget v5, v2, Ly0/c;->b:I

    iput v5, v12, Lx0/a;->c:I

    iget-object v5, v2, Ly0/c;->a:Lx0/b;

    iget-byte v5, v5, Lx0/b;->d:B

    int-to-byte v5, v5

    iput-byte v5, v12, Lx0/a;->h:B

    iget-object v5, v2, Ly0/c;->a:Lx0/b;

    iget-wide v5, v5, Lx0/b;->b:J

    iput-wide v5, v12, Lx0/a;->d:J

    iget-object v5, v2, Ly0/c;->a:Lx0/b;

    iget-wide v5, v5, Lx0/b;->g:J

    iput-wide v5, v12, Lx0/a;->k:J

    iget-object v5, v2, Ly0/c;->a:Lx0/b;

    iget-wide v5, v5, Lx0/b;->e:D

    iput-wide v5, v12, Lx0/a;->i:D

    iget-object v5, v2, Ly0/c;->a:Lx0/b;

    iget-wide v5, v5, Lx0/b;->h:D

    iput-wide v5, v12, Lx0/a;->l:D

    iget-object v5, v2, Ly0/c;->a:Lx0/b;

    iget-wide v5, v5, Lx0/b;->i:D

    iput-wide v5, v12, Lx0/a;->m:D

    iget-object v5, v2, Ly0/c;->a:Lx0/b;

    iget-wide v5, v5, Lx0/b;->k:D

    iput-wide v5, v12, Lx0/a;->o:D

    iget-boolean v5, v2, Ly0/c;->c:Z

    iput-boolean v5, v12, Lx0/a;->e:Z

    iget-object v5, v2, Ly0/c;->a:Lx0/b;

    iget-boolean v5, v5, Lx0/b;->c:Z

    iput-boolean v5, v12, Lx0/a;->f:Z

    iget v5, v4, Lv0/a;->i:I

    if-eqz v3, :cond_2

    iget v6, v3, Lv0/a;->i:I

    goto :goto_2

    :cond_2
    const/4 v6, -0x1

    :goto_2
    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, v12, Lx0/a;->n:I

    if-eqz v3, :cond_3

    iget-object v5, v3, Lv0/a;->b:Lc1/c$a;

    if-eqz v5, :cond_3

    sget-object v6, Lv0/a;->j:Lc1/c$a;

    if-eq v5, v6, :cond_3

    goto :goto_3

    :cond_3
    iget-object v5, v4, Lv0/a;->b:Lc1/c$a;

    :goto_3
    if-nez v5, :cond_4

    sget-object v5, Lv0/a;->j:Lc1/c$a;

    :cond_4
    iput-object v5, v12, Lx0/a;->b:Lc1/c$a;

    iget-wide v4, v4, Lv0/a;->a:J

    move/from16 v19, v7

    if-eqz v3, :cond_5

    iget-wide v6, v3, Lv0/a;->a:J

    goto :goto_4

    :cond_5
    const-wide/16 v6, 0x0

    :goto_4
    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    iput-wide v4, v12, Lx0/a;->a:J

    xor-int/lit8 v4, v16, 0x1

    if-eqz v4, :cond_a

    iget-object v4, v15, Lx0/i;->d:Lx0/p;

    iget-wide v5, v12, Lx0/a;->l:D

    invoke-static {v5, v6}, Landroidx/emoji2/text/l;->G(D)Z

    move-result v5

    if-eqz v5, :cond_6

    iget-wide v5, v12, Lx0/a;->l:D

    iget-object v7, v4, Lx0/p;->j:Lu0/b;

    move-object/from16 v20, v2

    iget-object v2, v12, Lx0/a;->j:La1/b;

    invoke-static {v7, v2, v5, v6}, Landroidx/emoji2/text/l;->x(Lu0/b;La1/b;D)D

    move-result-wide v5

    iput-wide v5, v12, Lx0/a;->l:D

    goto :goto_5

    :cond_6
    move-object/from16 v20, v2

    :goto_5
    sub-long v0, v8, v0

    iput-wide v0, v12, Lx0/a;->d:J

    iget-object v2, v15, Lx0/i;->b:Lx0/h;

    iget v5, v2, Lx0/h;->e:I

    const/4 v6, 0x1

    add-int/2addr v5, v6

    iput v5, v2, Lx0/h;->e:I

    iget-byte v5, v12, Lx0/a;->h:B

    const/4 v7, 0x2

    if-ne v5, v7, :cond_8

    iget-wide v5, v12, Lx0/a;->a:J

    move-wide/from16 v21, v13

    const-wide/16 v13, 0x0

    cmp-long v5, v5, v13

    if-lez v5, :cond_7

    const/4 v6, 0x1

    goto :goto_6

    :cond_7
    iput-wide v0, v12, Lx0/a;->k:J

    iput-wide v13, v12, Lx0/a;->a:J

    iget v0, v2, Lx0/h;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, v2, Lx0/h;->f:I

    invoke-static {v15, v12}, Lx0/g;->b(Lx0/i;Lx0/a;)V

    goto :goto_8

    :cond_8
    move-wide/from16 v21, v13

    :goto_6
    invoke-virtual {v12, v6}, Lx0/a;->a(B)V

    iget-object v0, v4, Lx0/p;->c:Lv0/a;

    if-eqz v3, :cond_9

    iget v1, v3, Lv0/a;->d:F

    float-to-double v1, v1

    invoke-static {v1, v2}, Landroidx/emoji2/text/l;->G(D)Z

    move-result v1

    if-nez v1, :cond_9

    iget v0, v3, Lv0/a;->d:F

    goto :goto_7

    :cond_9
    iget v0, v0, Lv0/a;->d:F

    :goto_7
    const v1, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_b

    float-to-double v0, v0

    iput-wide v0, v12, Lx0/a;->p:D

    goto :goto_8

    :cond_a
    move-object/from16 v20, v2

    move-wide/from16 v21, v13

    :cond_b
    :goto_8
    iget-byte v0, v12, Lx0/a;->h:B

    const-string v13, ", velocity = "

    const-string v14, ", target value = "

    const-string v6, ", start value = "

    const-string v7, ", value = "

    const-string v4, ", property = "

    const/4 v1, 0x1

    if-ne v0, v1, :cond_18

    iget-object v0, v15, Lx0/i;->d:Lx0/p;

    iget-wide v1, v12, Lx0/a;->a:J

    const-wide/16 v23, 0x0

    cmp-long v1, v1, v23

    if-lez v1, :cond_e

    iget-boolean v1, v12, Lx0/a;->g:Z

    if-eqz v1, :cond_c

    const-string v1, "StartTask, tag = "

    invoke-static {v1}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v15, Lx0/i;->d:Lx0/p;

    iget-object v2, v2, Lx0/p;->f:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v12, Lx0/a;->j:La1/b;

    invoke-virtual {v2}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", delay = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, v12, Lx0/a;->a:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, ", initTime = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, v12, Lx0/a;->d:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, ", totalT = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lc1/f;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_c
    iget-wide v1, v12, Lx0/a;->d:J

    iget-wide v10, v12, Lx0/a;->a:J

    add-long/2addr v1, v10

    cmp-long v1, v8, v1

    if-gez v1, :cond_d

    const/4 v0, 0x2

    move-wide/from16 v10, p3

    goto/16 :goto_f

    :cond_d
    iget-object v0, v0, Lx0/p;->j:Lu0/b;

    iget-object v1, v12, Lx0/a;->j:La1/b;

    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    invoke-static {v0, v1, v2, v3}, Landroidx/emoji2/text/l;->x(Lu0/b;La1/b;D)D

    move-result-wide v0

    cmpl-double v2, v0, v2

    if-eqz v2, :cond_e

    iput-wide v0, v12, Lx0/a;->l:D

    :cond_e
    iget-object v0, v15, Lx0/i;->b:Lx0/h;

    iget v1, v0, Lx0/h;->f:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lx0/h;->f:I

    iget-wide v0, v12, Lx0/a;->o:D

    invoke-static {v0, v1}, Landroidx/emoji2/text/l;->G(D)Z

    move-result v0

    iget-wide v1, v12, Lx0/a;->l:D

    if-nez v0, :cond_f

    invoke-static {v1, v2}, Landroidx/emoji2/text/l;->G(D)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-wide v0, v12, Lx0/a;->o:D

    iput-wide v0, v12, Lx0/a;->l:D

    goto :goto_9

    :cond_f
    invoke-static {v1, v2}, Landroidx/emoji2/text/l;->G(D)Z

    move-result v0

    if-nez v0, :cond_11

    iget-wide v0, v12, Lx0/a;->l:D

    iput-wide v0, v12, Lx0/a;->o:D

    :cond_10
    :goto_9
    const/4 v0, 0x1

    goto :goto_a

    :cond_11
    const/4 v0, 0x0

    :goto_a
    const-string v1, "miuix_anim"

    if-nez v0, :cond_13

    iget-boolean v0, v12, Lx0/a;->g:Z

    if-eqz v0, :cond_12

    const-string v0, "StartTask, set start value failed, break, tag = "

    invoke-static {v0}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, v15, Lx0/i;->d:Lx0/p;

    iget-object v2, v2, Lx0/p;->f:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v12, Lx0/a;->j:La1/b;

    invoke-virtual {v2}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, v12, Lx0/a;->l:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, v12, Lx0/a;->m:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, v12, Lx0/a;->o:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lc1/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_12
    const/4 v0, 0x0

    goto :goto_c

    :cond_13
    iget-wide v2, v12, Lx0/a;->l:D

    iget-wide v10, v12, Lx0/a;->m:D

    cmpl-double v0, v2, v10

    if-nez v0, :cond_14

    iget-wide v2, v12, Lx0/a;->p:D

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    const-wide v10, 0x4030aaaaa0000000L    # 16.66666603088379

    cmpg-double v0, v2, v10

    if-gez v0, :cond_14

    const/4 v0, 0x1

    goto :goto_b

    :cond_14
    const/4 v0, 0x0

    :goto_b
    if-eqz v0, :cond_16

    iget-boolean v0, v12, Lx0/a;->g:Z

    if-eqz v0, :cond_15

    const-string v0, "StartTask, values invalid, break, tag = "

    invoke-static {v0}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, v15, Lx0/i;->d:Lx0/p;

    iget-object v2, v2, Lx0/p;->f:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v12, Lx0/a;->j:La1/b;

    invoke-virtual {v2}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", startValue = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, v12, Lx0/a;->l:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v2, ", targetValue = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, v12, Lx0/a;->m:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, v12, Lx0/a;->o:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, v12, Lx0/a;->p:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lc1/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_15
    const/4 v0, 0x0

    iput-boolean v0, v12, Lx0/a;->e:Z

    iput v0, v12, Lx0/a;->c:I

    iput-boolean v0, v12, Lx0/a;->f:Z

    :goto_c
    const/4 v1, 0x5

    invoke-virtual {v12, v1}, Lx0/a;->a(B)V

    iget-object v1, v15, Lx0/i;->b:Lx0/h;

    iget v2, v1, Lx0/h;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lx0/h;->d:I

    const/4 v1, 0x2

    move-wide/from16 v10, p3

    goto :goto_d

    :cond_16
    const/4 v0, 0x0

    move-wide/from16 v10, p3

    sub-long v1, v8, v10

    iput-wide v1, v12, Lx0/a;->k:J

    iput v0, v12, Lx0/a;->c:I

    const/4 v0, 0x2

    invoke-virtual {v12, v0}, Lx0/a;->a(B)V

    const/4 v1, 0x1

    move/from16 v29, v1

    move v1, v0

    move/from16 v0, v29

    :goto_d
    if-nez v0, :cond_17

    goto :goto_e

    :cond_17
    invoke-static {v15, v12}, Lx0/g;->b(Lx0/i;Lx0/a;)V

    :goto_e
    move v0, v1

    goto :goto_f

    :cond_18
    const/4 v0, 0x2

    :goto_f
    iget-byte v1, v12, Lx0/a;->h:B

    if-ne v1, v0, :cond_24

    iget-object v0, v15, Lx0/i;->d:Lx0/p;

    iget-object v1, v15, Lx0/i;->b:Lx0/h;

    iget v2, v1, Lx0/h;->g:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lx0/h;->g:I

    iget v1, v12, Lx0/a;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v12, Lx0/a;->c:I

    iget-object v1, v12, Lx0/a;->j:La1/b;

    sget-object v2, La1/i;->b:La1/i$c;

    if-eq v1, v2, :cond_1f

    sget-object v5, La1/i;->a:La1/i$b;

    if-eq v1, v5, :cond_1f

    instance-of v1, v1, La1/a;

    if-eqz v1, :cond_19

    goto/16 :goto_13

    :cond_19
    iget-object v0, v0, Lx0/p;->j:Lu0/b;

    const/4 v2, -0x1

    move-object v1, v12

    move v9, v2

    move-object/from16 v8, v20

    move-wide/from16 v2, p1

    move-object/from16 v25, v4

    move-object/from16 v26, v5

    move/from16 v20, v17

    move/from16 v17, p0

    move-wide/from16 v4, p3

    move-object/from16 v27, v6

    move-object/from16 v28, v7

    move-wide/from16 v6, v21

    invoke-static/range {v0 .. v7}, Lb1/a;->a(Lu0/b;Lx0/a;JJJ)V

    iget-object v0, v12, Lx0/a;->b:Lc1/c$a;

    iget v0, v0, Lc1/c$a;->c:I

    sget-object v1, Lc1/c;->a:Ljava/util/concurrent/ConcurrentHashMap;

    if-ge v0, v9, :cond_1a

    const/4 v0, 0x1

    goto :goto_10

    :cond_1a
    const/4 v0, 0x0

    :goto_10
    if-nez v0, :cond_1e

    iget-wide v0, v12, Lx0/a;->i:D

    double-to-float v0, v0

    iget-object v1, v12, Lx0/a;->j:La1/b;

    move-object/from16 v2, v26

    if-ne v1, v2, :cond_1b

    instance-of v2, v1, La1/a;

    if-eqz v2, :cond_1b

    sget-object v1, Lc1/a;->b:Landroid/animation/ArgbEvaluator;

    goto :goto_11

    :cond_1b
    instance-of v1, v1, La1/c;

    if-eqz v1, :cond_1c

    new-instance v1, Landroid/animation/IntEvaluator;

    invoke-direct {v1}, Landroid/animation/IntEvaluator;-><init>()V

    goto :goto_11

    :cond_1c
    new-instance v1, Landroid/animation/FloatEvaluator;

    invoke-direct {v1}, Landroid/animation/FloatEvaluator;-><init>()V

    :goto_11
    instance-of v2, v1, Landroid/animation/IntEvaluator;

    if-eqz v2, :cond_1d

    check-cast v1, Landroid/animation/IntEvaluator;

    iget-wide v2, v12, Lx0/a;->l:D

    double-to-int v2, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-wide v3, v12, Lx0/a;->m:D

    double-to-int v3, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Landroid/animation/IntEvaluator;->evaluate(FLjava/lang/Integer;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->doubleValue()D

    move-result-wide v0

    goto :goto_12

    :cond_1d
    check-cast v1, Landroid/animation/FloatEvaluator;

    iget-wide v2, v12, Lx0/a;->l:D

    double-to-float v2, v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iget-wide v3, v12, Lx0/a;->m:D

    double-to-float v3, v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Landroid/animation/FloatEvaluator;->evaluate(FLjava/lang/Number;Ljava/lang/Number;)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->doubleValue()D

    move-result-wide v0

    :goto_12
    move-object/from16 v23, v8

    goto :goto_16

    :cond_1e
    move-object/from16 v23, v8

    goto :goto_17

    :cond_1f
    :goto_13
    move-object/from16 v25, v4

    move-object/from16 v27, v6

    move-object/from16 v28, v7

    move-object/from16 v8, v20

    move/from16 v20, v17

    move/from16 v17, p0

    iget-wide v6, v12, Lx0/a;->l:D

    iget-wide v4, v12, Lx0/a;->m:D

    const-wide/16 v1, 0x0

    iput-wide v1, v12, Lx0/a;->l:D

    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    iput-wide v1, v12, Lx0/a;->m:D

    iget-wide v1, v12, Lx0/a;->i:D

    iput-wide v1, v12, Lx0/a;->o:D

    iget-object v0, v0, Lx0/p;->j:Lu0/b;

    move-object v1, v12

    move-wide/from16 v2, p1

    move-object/from16 v23, v8

    move-wide v8, v4

    move-wide/from16 v4, p3

    move-wide v10, v6

    move-wide/from16 v6, v21

    invoke-static/range {v0 .. v7}, Lb1/a;->a(Lu0/b;Lx0/a;JJJ)V

    iget-wide v0, v12, Lx0/a;->o:D

    double-to-float v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v2, v0, v1

    if-lez v2, :cond_20

    :goto_14
    move v0, v1

    goto :goto_15

    :cond_20
    const/4 v1, 0x0

    cmpg-float v2, v0, v1

    if-gez v2, :cond_21

    goto :goto_14

    :cond_21
    :goto_15
    float-to-double v0, v0

    iput-wide v0, v12, Lx0/a;->i:D

    iput-wide v10, v12, Lx0/a;->l:D

    iput-wide v8, v12, Lx0/a;->m:D

    sget-object v2, Lc1/a;->b:Landroid/animation/ArgbEvaluator;

    double-to-float v0, v0

    double-to-int v1, v10

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-wide v3, v12, Lx0/a;->m:D

    double-to-int v3, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v1, v3}, Landroid/animation/ArgbEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->doubleValue()D

    move-result-wide v0

    :goto_16
    iput-wide v0, v12, Lx0/a;->o:D

    :goto_17
    iget-byte v0, v12, Lx0/a;->h:B

    const/4 v1, 0x3

    if-ne v0, v1, :cond_22

    const/4 v0, 0x1

    iput-boolean v0, v12, Lx0/a;->f:Z

    iget-object v1, v15, Lx0/i;->b:Lx0/h;

    iget v2, v1, Lx0/h;->c:I

    add-int/2addr v2, v0

    iput v2, v1, Lx0/h;->c:I

    :cond_22
    iget-boolean v0, v12, Lx0/a;->g:Z

    if-eqz v0, :cond_23

    const-string v0, "----- update anim, target = "

    invoke-static {v0}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v15, Lx0/i;->d:Lx0/p;

    iget-object v1, v1, Lx0/p;->j:Lu0/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tag = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, v15, Lx0/i;->d:Lx0/p;

    iget-object v1, v1, Lx0/p;->f:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, v12, Lx0/a;->j:La1/b;

    invoke-virtual {v1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", op = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-byte v1, v12, Lx0/a;->h:B

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", init time = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, v12, Lx0/a;->d:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", start time = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, v12, Lx0/a;->k:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, v12, Lx0/a;->l:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, v12, Lx0/a;->m:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, v12, Lx0/a;->o:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v1, ", progress = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, v12, Lx0/a;->i:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, v12, Lx0/a;->p:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v1, ", delta = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-wide/from16 v4, p3

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lc1/f;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_19

    :cond_23
    move-wide/from16 v4, p3

    goto :goto_18

    :cond_24
    move-wide v4, v10

    move-object/from16 v23, v20

    move/from16 v20, v17

    move/from16 v17, p0

    :goto_18
    const/4 v1, 0x0

    :goto_19
    iget v0, v12, Lx0/a;->c:I

    move-object/from16 v2, v23

    iput v0, v2, Ly0/c;->b:I

    iget-object v0, v2, Ly0/c;->a:Lx0/b;

    iget-byte v3, v12, Lx0/a;->h:B

    int-to-byte v3, v3

    iput-byte v3, v0, Lx0/b;->d:B

    iget-object v0, v2, Ly0/c;->a:Lx0/b;

    iget-wide v6, v12, Lx0/a;->a:J

    iput-wide v6, v0, Lx0/b;->a:J

    iget-object v0, v2, Ly0/c;->a:Lx0/b;

    iget v3, v12, Lx0/a;->n:I

    iput v3, v0, Lx0/b;->j:I

    iget-object v0, v2, Ly0/c;->a:Lx0/b;

    iget-wide v6, v12, Lx0/a;->d:J

    iput-wide v6, v0, Lx0/b;->b:J

    iget-object v0, v2, Ly0/c;->a:Lx0/b;

    iget-wide v6, v12, Lx0/a;->k:J

    iput-wide v6, v0, Lx0/b;->g:J

    iget-object v0, v2, Ly0/c;->a:Lx0/b;

    iget-wide v6, v12, Lx0/a;->i:D

    iput-wide v6, v0, Lx0/b;->e:D

    iget-object v0, v2, Ly0/c;->a:Lx0/b;

    iget-wide v6, v12, Lx0/a;->l:D

    iput-wide v6, v0, Lx0/b;->h:D

    iget-object v0, v2, Ly0/c;->a:Lx0/b;

    iget-wide v6, v12, Lx0/a;->m:D

    iput-wide v6, v0, Lx0/b;->i:D

    iget-boolean v0, v12, Lx0/a;->e:Z

    iput-boolean v0, v2, Ly0/c;->c:Z

    iget-object v0, v2, Ly0/c;->a:Lx0/b;

    iget-wide v6, v12, Lx0/a;->o:D

    iput-wide v6, v0, Lx0/b;->k:D

    iget-wide v6, v12, Lx0/a;->p:D

    iput-wide v6, v2, Ly0/c;->f:D

    iget-object v0, v2, Ly0/c;->a:Lx0/b;

    iget-boolean v3, v12, Lx0/a;->f:Z

    iput-boolean v3, v0, Lx0/b;->c:Z

    const/4 v0, 0x0

    iput-object v0, v12, Lx0/a;->j:La1/b;

    iput-object v0, v12, Lx0/a;->b:Lc1/c$a;

    if-eqz p5, :cond_25

    if-eqz p6, :cond_25

    if-nez v19, :cond_25

    iget-wide v6, v12, Lx0/a;->o:D

    invoke-static {v6, v7}, Landroidx/emoji2/text/l;->G(D)Z

    move-result v0

    if-nez v0, :cond_25

    iget-object v0, v15, Lx0/i;->d:Lx0/p;

    iget-object v0, v0, Lx0/p;->j:Lu0/b;

    invoke-virtual {v2, v0}, Ly0/c;->d(Lu0/b;)V

    :cond_25
    move v3, v1

    move-wide v0, v4

    :goto_1a
    add-int/lit8 v2, v20, 0x1

    move-wide/from16 v8, p1

    move-wide v10, v4

    move/from16 v4, v17

    move-object/from16 v6, v18

    move/from16 v7, v19

    move-wide/from16 v13, v21

    goto/16 :goto_1

    :cond_26
    move-wide v4, v10

    move-wide/from16 v21, v13

    const/4 v2, 0x0

    iget-object v6, v15, Lc1/e;->a:Lc1/e;

    iput-object v2, v15, Lc1/e;->a:Lc1/e;

    move-object v15, v6

    check-cast v15, Lx0/i;

    move-wide/from16 v8, p1

    goto/16 :goto_0

    :cond_27
    return-void
.end method

.method public static b(Lx0/i;Lx0/a;)V
    .locals 4

    const-wide/16 v0, 0x0

    iput-wide v0, p1, Lx0/a;->i:D

    const/4 v0, 0x0

    iput-boolean v0, p1, Lx0/a;->e:Z

    iput v0, p1, Lx0/a;->c:I

    iput-boolean v0, p1, Lx0/a;->f:Z

    iget-boolean v1, p1, Lx0/a;->g:Z

    if-eqz v1, :cond_0

    const-string v1, "+++++ start anim, target = "

    invoke-static {v1}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lx0/i;->d:Lx0/p;

    iget-object v2, v2, Lx0/p;->j:Lu0/b;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", tag = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, p0, Lx0/i;->d:Lx0/p;

    iget-object p0, p0, Lx0/p;->f:Ljava/lang/Object;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, ", property = "

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, p1, Lx0/a;->j:La1/b;

    invoke-virtual {p0}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ", op = "

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-byte p0, p1, Lx0/a;->h:B

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, ", ease = "

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, p1, Lx0/a;->b:Lc1/c$a;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, ", delay = "

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p1, Lx0/a;->a:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p0, ", start value = "

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p1, Lx0/a;->l:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string p0, ", target value = "

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p1, Lx0/a;->m:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string p0, ", value = "

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p1, Lx0/a;->o:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string p0, ", progress = "

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p1, Lx0/a;->i:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string p0, ", velocity = "

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide p0, p1, Lx0/a;->p:D

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    new-array p1, v0, [Ljava/lang/Object;

    invoke-static {p0, p1}, Lc1/f;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
