.class public Ly/b;
.super Landroid/view/inputmethod/InputConnectionWrapper;
.source ""


# instance fields
.field public final synthetic a:Ly/d;


# direct methods
.method public constructor <init>(Landroid/view/inputmethod/InputConnection;ZLy/d;)V
    .locals 0

    iput-object p3, p0, Ly/b;->a:Ly/d;

    invoke-direct {p0, p1, p2}, Landroid/view/inputmethod/InputConnectionWrapper;-><init>(Landroid/view/inputmethod/InputConnection;Z)V

    return-void
.end method


# virtual methods
.method public commitContent(Landroid/view/inputmethod/InputContentInfo;ILandroid/os/Bundle;)Z
    .locals 3

    iget-object v0, p0, Ly/b;->a:Ly/d;

    if-nez p1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    new-instance v1, Ly/e;

    new-instance v2, Ly/e$a;

    invoke-direct {v2, p1}, Ly/e$a;-><init>(Ljava/lang/Object;)V

    invoke-direct {v1, v2}, Ly/e;-><init>(Ly/e$b;)V

    :goto_0
    check-cast v0, Ly/c;

    invoke-virtual {v0, v1, p2, p3}, Ly/c;->a(Ly/e;ILandroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroid/view/inputmethod/InputConnectionWrapper;->commitContent(Landroid/view/inputmethod/InputContentInfo;ILandroid/os/Bundle;)Z

    move-result p1

    return p1
.end method
