.class public Lv1/c;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Lm1/j$f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lm1/j$f<",
            "Lv1/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lv1/c$a;

    invoke-direct {v0}, Lv1/c$a;-><init>()V

    sget-object v1, Lm1/j;->a:Ljava/util/HashMap;

    new-instance v1, Lm1/j$i;

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, Lm1/j$i;-><init>(Lm1/j$e;I)V

    sput-object v1, Lv1/c;->a:Lm1/j$f;

    return-void
.end method

.method public static a(Landroid/content/Context;JI)Ljava/lang/String;
    .locals 16

    move-object/from16 v0, p0

    sget-object v1, Lm1/j;->c:Lm1/j$f;

    move-object v2, v1

    check-cast v2, Lm1/j$b;

    invoke-virtual {v2}, Lm1/j$b;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/StringBuilder;

    and-int/lit8 v3, p3, 0x10

    const/16 v4, 0x10

    if-nez v3, :cond_1

    and-int/lit8 v3, p3, 0x20

    if-nez v3, :cond_1

    invoke-static/range {p0 .. p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v3, 0x20

    goto :goto_0

    :cond_0
    move v3, v4

    :goto_0
    or-int v3, p3, v3

    goto :goto_1

    :cond_1
    move/from16 v3, p3

    :goto_1
    and-int/lit16 v5, v3, 0x400

    const/16 v6, 0x400

    const/16 v7, 0x800

    if-ne v5, v6, :cond_9

    and-int/lit16 v5, v3, 0x380

    if-eqz v5, :cond_5

    and-int/lit8 v5, v3, 0xf

    and-int/lit16 v6, v3, 0x800

    if-eqz v5, :cond_3

    if-ne v6, v7, :cond_2

    const v5, 0x7f1200af

    goto/16 :goto_2

    :cond_2
    const v5, 0x7f1200ae

    goto/16 :goto_2

    :cond_3
    if-ne v6, v7, :cond_4

    const v5, 0x7f1200b0

    goto :goto_2

    :cond_4
    const v5, 0x7f1200ad

    goto :goto_2

    :cond_5
    and-int/lit8 v5, v3, 0xf

    and-int/lit16 v6, v3, 0x800

    if-eqz v5, :cond_7

    if-ne v6, v7, :cond_6

    const v5, 0x7f1200b4

    goto :goto_2

    :cond_6
    const v5, 0x7f1200b3

    goto :goto_2

    :cond_7
    if-ne v6, v7, :cond_8

    const v5, 0x7f1200b5

    goto :goto_2

    :cond_8
    const v5, 0x7f1200ac

    goto :goto_2

    :cond_9
    and-int/lit16 v5, v3, 0x380

    if-eqz v5, :cond_d

    and-int/lit8 v5, v3, 0xf

    and-int/lit16 v6, v3, 0x800

    if-eqz v5, :cond_b

    if-ne v6, v7, :cond_a

    const v5, 0x7f120094

    goto :goto_2

    :cond_a
    const v5, 0x7f120093

    goto :goto_2

    :cond_b
    if-ne v6, v7, :cond_c

    const v5, 0x7f120095

    goto :goto_2

    :cond_c
    const v5, 0x7f120083

    goto :goto_2

    :cond_d
    and-int/lit8 v5, v3, 0xf

    and-int/lit16 v6, v3, 0x800

    if-eqz v5, :cond_f

    if-ne v6, v7, :cond_e

    const v5, 0x7f1200aa

    goto :goto_2

    :cond_e
    const v5, 0x7f120097

    goto :goto_2

    :cond_f
    if-ne v6, v7, :cond_10

    const v5, 0x7f1200ab

    goto :goto_2

    :cond_10
    const v5, 0x7f12007e

    :goto_2
    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    check-cast v1, Lm1/j$b;

    invoke-virtual {v1}, Lm1/j$b;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/StringBuilder;

    sget-object v6, Lv1/c;->a:Lm1/j$f;

    check-cast v6, Lm1/j$b;

    invoke-virtual {v6}, Lm1/j$b;->a()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lv1/a;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lv1/a;->z(Ljava/util/TimeZone;)Lv1/a;

    move-wide/from16 v8, p1

    iput-wide v8, v6, Lv1/a;->f:J

    invoke-virtual {v6}, Lv1/a;->d()V

    const/4 v8, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    :goto_3
    if-ge v8, v9, :cond_3e

    invoke-virtual {v5, v8}, Ljava/lang/String;->charAt(I)C

    move-result v10

    const/16 v11, 0x44

    const-string v12, "no any time date"

    if-eq v10, v11, :cond_2a

    const/16 v11, 0x54

    if-eq v10, v11, :cond_13

    const/16 v11, 0x57

    if-eq v10, v11, :cond_11

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    :cond_11
    and-int/lit16 v10, v3, 0x2000

    const/16 v11, 0x2000

    if-ne v10, v11, :cond_12

    const v10, 0x7f1200b2

    goto/16 :goto_5

    :cond_12
    const v10, 0x7f1200b1

    goto/16 :goto_5

    :cond_13
    and-int/lit16 v10, v3, 0x4000

    const/16 v11, 0x4000

    const/4 v13, 0x2

    const/4 v14, 0x1

    if-ne v10, v11, :cond_16

    and-int/lit8 v10, v3, 0x1

    if-ne v10, v14, :cond_14

    const/16 v10, 0x16

    invoke-virtual {v6, v10}, Lv1/a;->l(I)I

    move-result v10

    if-nez v10, :cond_16

    :cond_14
    and-int/lit8 v10, v3, 0xe

    if-eqz v10, :cond_16

    and-int/lit8 v10, v3, -0x2

    and-int/lit8 v11, v10, 0x2

    if-ne v11, v13, :cond_15

    const/16 v11, 0x15

    invoke-virtual {v6, v11}, Lv1/a;->l(I)I

    move-result v11

    if-nez v11, :cond_17

    :cond_15
    and-int/lit8 v11, v10, 0xc

    if-eqz v11, :cond_17

    and-int/lit8 v10, v10, -0x3

    const/16 v11, 0x14

    invoke-virtual {v6, v11}, Lv1/a;->l(I)I

    move-result v11

    if-nez v11, :cond_17

    and-int/lit8 v11, v10, 0x8

    if-eqz v11, :cond_17

    and-int/lit8 v10, v10, -0x5

    goto :goto_4

    :cond_16
    move v10, v3

    :cond_17
    :goto_4
    and-int/lit8 v11, v10, 0x8

    const/16 v15, 0x8

    const/4 v7, 0x4

    if-ne v11, v15, :cond_23

    and-int/lit8 v11, v10, 0x10

    if-ne v11, v4, :cond_1f

    and-int/lit8 v11, v10, 0x40

    const/16 v12, 0x40

    if-ne v11, v12, :cond_1b

    and-int/lit8 v11, v10, 0x4

    if-ne v11, v7, :cond_1a

    and-int/lit8 v7, v10, 0x2

    if-ne v7, v13, :cond_19

    and-int/lit8 v7, v10, 0x1

    if-ne v7, v14, :cond_18

    const v10, 0x7f12009c

    goto/16 :goto_5

    :cond_18
    const v10, 0x7f12009b

    goto/16 :goto_5

    :cond_19
    const v10, 0x7f120099

    goto/16 :goto_5

    :cond_1a
    const v10, 0x7f120098

    goto/16 :goto_5

    :cond_1b
    and-int/lit8 v11, v10, 0x4

    if-ne v11, v7, :cond_1e

    and-int/lit8 v7, v10, 0x2

    if-ne v7, v13, :cond_1d

    and-int/lit8 v7, v10, 0x1

    if-ne v7, v14, :cond_1c

    const v10, 0x7f12009d

    goto :goto_5

    :cond_1c
    const v10, 0x7f12009e

    goto :goto_5

    :cond_1d
    const v10, 0x7f12009a

    goto :goto_5

    :cond_1e
    const v10, 0x7f12009f

    goto :goto_5

    :cond_1f
    and-int/lit8 v11, v10, 0x4

    if-ne v11, v7, :cond_22

    and-int/lit8 v7, v10, 0x2

    if-ne v7, v13, :cond_21

    and-int/lit8 v7, v10, 0x1

    if-ne v7, v14, :cond_20

    const v10, 0x7f1200a3

    goto :goto_5

    :cond_20
    const v10, 0x7f1200a2

    goto :goto_5

    :cond_21
    const v10, 0x7f1200a1

    goto :goto_5

    :cond_22
    const v10, 0x7f1200a0

    goto :goto_5

    :cond_23
    and-int/lit8 v11, v10, 0x4

    if-ne v11, v7, :cond_26

    and-int/lit8 v7, v10, 0x2

    if-ne v7, v13, :cond_25

    and-int/lit8 v7, v10, 0x1

    if-ne v7, v14, :cond_24

    const v10, 0x7f1200a7

    goto :goto_5

    :cond_24
    const v10, 0x7f1200a6

    goto :goto_5

    :cond_25
    const v10, 0x7f1200a5

    goto :goto_5

    :cond_26
    and-int/lit8 v7, v10, 0x2

    and-int/lit8 v10, v10, 0x1

    if-ne v7, v13, :cond_28

    if-ne v10, v14, :cond_27

    const v10, 0x7f1200a9

    goto :goto_5

    :cond_27
    const v10, 0x7f1200a8

    goto :goto_5

    :cond_28
    if-ne v10, v14, :cond_29

    const v10, 0x7f1200a4

    :goto_5
    invoke-virtual {v0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    :cond_29
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, v12}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2a
    const v7, 0x8000

    and-int v10, v3, v7

    const v11, 0x7f120084

    const v13, 0x7f120096

    const/16 v14, 0x200

    const/16 v15, 0x100

    const/16 v4, 0x80

    if-ne v10, v7, :cond_31

    and-int/lit16 v7, v3, 0x200

    if-ne v7, v14, :cond_2d

    and-int/lit16 v7, v3, 0x100

    if-ne v7, v15, :cond_2c

    and-int/lit16 v7, v3, 0x80

    if-ne v7, v4, :cond_2b

    const v11, 0x7f12008e

    goto/16 :goto_6

    :cond_2b
    const v11, 0x7f12008d

    goto/16 :goto_6

    :cond_2c
    const v11, 0x7f12008c

    goto/16 :goto_6

    :cond_2d
    and-int/lit16 v7, v3, 0x100

    and-int/lit16 v10, v3, 0x80

    if-ne v7, v15, :cond_2f

    if-ne v10, v4, :cond_2e

    const v11, 0x7f12008b

    goto/16 :goto_6

    :cond_2e
    const v11, 0x7f12008a

    goto/16 :goto_6

    :cond_2f
    if-ne v10, v4, :cond_30

    const v11, 0x7f120089

    goto/16 :goto_6

    :cond_30
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, v12}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_31
    and-int/lit16 v7, v3, 0x1000

    const/16 v10, 0x1000

    if-ne v7, v10, :cond_37

    and-int/lit16 v7, v3, 0x200

    if-ne v7, v14, :cond_33

    and-int/lit16 v7, v3, 0x100

    if-ne v7, v15, :cond_39

    and-int/lit16 v7, v3, 0x80

    if-ne v7, v4, :cond_32

    const v11, 0x7f120092

    goto :goto_6

    :cond_32
    const v11, 0x7f120091

    goto :goto_6

    :cond_33
    and-int/lit16 v7, v3, 0x100

    and-int/lit16 v10, v3, 0x80

    if-ne v7, v15, :cond_35

    if-ne v10, v4, :cond_34

    const v11, 0x7f120090

    goto :goto_6

    :cond_34
    const v11, 0x7f12008f

    goto :goto_6

    :cond_35
    if-ne v10, v4, :cond_36

    goto :goto_6

    :cond_36
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, v12}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_37
    and-int/lit16 v7, v3, 0x200

    if-ne v7, v14, :cond_3a

    and-int/lit16 v7, v3, 0x100

    if-ne v7, v15, :cond_39

    and-int/lit16 v7, v3, 0x80

    if-ne v7, v4, :cond_38

    const v11, 0x7f120088

    goto :goto_6

    :cond_38
    const v11, 0x7f120087

    goto :goto_6

    :cond_39
    move v11, v13

    goto :goto_6

    :cond_3a
    and-int/lit16 v7, v3, 0x100

    and-int/lit16 v10, v3, 0x80

    if-ne v7, v15, :cond_3c

    if-ne v10, v4, :cond_3b

    const v11, 0x7f120086

    goto :goto_6

    :cond_3b
    const v11, 0x7f120085

    goto :goto_6

    :cond_3c
    if-ne v10, v4, :cond_3d

    :goto_6
    invoke-virtual {v0, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_7
    add-int/lit8 v8, v8, 0x1

    const/16 v4, 0x10

    const/4 v7, 0x0

    goto/16 :goto_3

    :cond_3d
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, v12}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3e
    move-object v3, v7

    invoke-virtual {v6, v0, v2, v1, v3}, Lv1/a;->k(Landroid/content/Context;Ljava/lang/StringBuilder;Ljava/lang/CharSequence;Lv1/b;)Ljava/lang/StringBuilder;

    sget-object v0, Lm1/j;->c:Lm1/j$f;

    move-object v3, v0

    check-cast v3, Lm1/j$b;

    invoke-virtual {v3, v1}, Lm1/j$b;->d(Ljava/lang/Object;)V

    sget-object v1, Lv1/c;->a:Lm1/j$f;

    check-cast v1, Lm1/j$b;

    invoke-virtual {v1, v6}, Lm1/j$b;->d(Ljava/lang/Object;)V

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    check-cast v0, Lm1/j$b;

    invoke-virtual {v0, v2}, Lm1/j$b;->d(Ljava/lang/Object;)V

    return-object v1
.end method
