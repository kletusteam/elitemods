.class public Lq0/c;
.super Lq0/a;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lq0/c$c;
    }
.end annotation


# instance fields
.field public final c:Landroid/view/View$OnClickListener;

.field public final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final e:I

.field public final f:Landroidx/recyclerview/widget/RecyclerView$e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/recyclerview/widget/RecyclerView$e<",
            "*>;"
        }
    .end annotation
.end field

.field public final g:Lq0/c$c;


# direct methods
.method public constructor <init>(ILandroidx/recyclerview/widget/RecyclerView$e;Lq0/c$c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroidx/recyclerview/widget/RecyclerView$e<",
            "*>;",
            "Lq0/c$c;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lq0/a;-><init>()V

    new-instance v0, Lq0/c$a;

    invoke-direct {v0, p0}, Lq0/c$a;-><init>(Lq0/c;)V

    new-instance v1, Lq0/c$b;

    invoke-direct {v1, p0}, Lq0/c$b;-><init>(Lq0/c;)V

    iput-object v1, p0, Lq0/c;->c:Landroid/view/View$OnClickListener;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lq0/c;->d:Ljava/util/Set;

    iput p1, p0, Lq0/c;->e:I

    iput-object p2, p0, Lq0/c;->f:Landroidx/recyclerview/widget/RecyclerView$e;

    iput-object p3, p0, Lq0/c;->g:Lq0/c$c;

    iget-object p1, p2, Landroidx/recyclerview/widget/RecyclerView$e;->a:Landroidx/recyclerview/widget/RecyclerView$f;

    invoke-virtual {p1, v0}, Landroid/database/Observable;->registerObserver(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public a(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 5

    iget-object v0, p0, Lq0/a;->b:Landroidx/recyclerview/widget/RecyclerView;

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    move v3, v1

    :goto_0
    if-ge v3, v2, :cond_1

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {p0, v4}, Lq0/c;->c(Landroid/view/View;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Lq0/a;->a(Landroidx/recyclerview/widget/RecyclerView;)V

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    :goto_1
    if-ge v1, v0, :cond_2

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, Lq0/c;->b(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 4

    iget v0, p0, Lq0/c;->e:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    move-object v0, p1

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    instance-of v1, v0, Landroid/widget/Checkable;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lq0/a;->b:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, p1}, Landroidx/recyclerview/widget/RecyclerView;->B(Landroid/view/View;)Landroidx/recyclerview/widget/RecyclerView$a0;

    move-result-object p1

    if-eqz p1, :cond_1

    move-object v1, v0

    check-cast v1, Landroid/widget/Checkable;

    iget-object v2, p0, Lq0/c;->d:Ljava/util/Set;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$a0;->e()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    invoke-interface {v1, v2}, Landroid/widget/Checkable;->setChecked(Z)V

    iget-object v1, p0, Lq0/c;->g:Lq0/c$c;

    check-cast v1, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;

    invoke-virtual {v1, p1, v2}, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->a(Landroidx/recyclerview/widget/RecyclerView$a0;Z)V

    :cond_1
    iget-object p1, p0, Lq0/c;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    return-void
.end method

.method public c(Landroid/view/View;)V
    .locals 2

    iget v0, p0, Lq0/c;->e:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    :goto_0
    instance-of v0, p1, Landroid/widget/Checkable;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    return-void
.end method

.method public d()I
    .locals 1

    iget-object v0, p0, Lq0/c;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method

.method public e()V
    .locals 5

    iget-object v0, p0, Lq0/c;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lq0/a;->b:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lq0/c;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lq0/c;->d:Ljava/util/Set;

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Integer;

    invoke-interface {v0, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    iget-object v2, p0, Lq0/c;->d:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    array-length v2, v0

    move v3, v1

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v0, v3

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p0, v4, v1}, Lq0/c;->f(IZ)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method public final f(IZ)V
    .locals 3

    iget-object v0, p0, Lq0/a;->b:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->F(I)Landroidx/recyclerview/widget/RecyclerView$a0;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$a0;->a:Landroid/view/View;

    iget v1, p0, Lq0/c;->e:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    instance-of v1, v0, Landroid/widget/Checkable;

    if-eqz v1, :cond_1

    check-cast v0, Landroid/widget/Checkable;

    invoke-interface {v0, p2}, Landroid/widget/Checkable;->setChecked(Z)V

    iget-object v0, p0, Lq0/c;->g:Lq0/c$c;

    check-cast v0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;

    invoke-virtual {v0, p1, p2}, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->a(Landroidx/recyclerview/widget/RecyclerView$a0;Z)V

    :cond_1
    return-void
.end method
