.class public Lq0/g;
.super Lq0/a;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lq0/g$b;,
        Lq0/g$a;,
        Lq0/g$c;
    }
.end annotation


# instance fields
.field public c:Landroid/view/View$OnTouchListener;

.field public d:Landroid/view/View$OnClickListener;

.field public e:Landroid/view/View$OnLongClickListener;

.field public f:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lq0/g$c;",
            ">;"
        }
    .end annotation
.end field

.field public g:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lq0/g$a;",
            ">;"
        }
    .end annotation
.end field

.field public h:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lq0/g$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lq0/a;-><init>()V

    return-void
.end method


# virtual methods
.method public b(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lq0/g;->c:Landroid/view/View$OnTouchListener;

    invoke-virtual {p0, p1, v0}, Lq0/g;->f(Landroid/view/View;Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lq0/g;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, p1, v0}, Lq0/g;->d(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lq0/g;->e:Landroid/view/View$OnLongClickListener;

    invoke-virtual {p0, p1, v0}, Lq0/g;->e(Landroid/view/View;Landroid/view/View$OnLongClickListener;)V

    return-void
.end method

.method public c(Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lq0/g;->f(Landroid/view/View;Landroid/view/View$OnTouchListener;)V

    invoke-virtual {p0, p1, v0}, Lq0/g;->d(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, p1, v0}, Lq0/g;->e(Landroid/view/View;Landroid/view/View$OnLongClickListener;)V

    return-void
.end method

.method public final d(Landroid/view/View;Landroid/view/View$OnClickListener;)V
    .locals 5

    iget-object v0, p0, Lq0/g;->g:Landroid/util/SparseArray;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_3

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    move-object v3, p1

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    :goto_1
    if-eqz v3, :cond_2

    invoke-virtual {v3, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method public final e(Landroid/view/View;Landroid/view/View$OnLongClickListener;)V
    .locals 5

    iget-object v0, p0, Lq0/g;->h:Landroid/util/SparseArray;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_3

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    move-object v3, p1

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    :goto_1
    if-eqz v3, :cond_2

    invoke-virtual {v3, p2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method public final f(Landroid/view/View;Landroid/view/View$OnTouchListener;)V
    .locals 5

    iget-object v0, p0, Lq0/g;->f:Landroid/util/SparseArray;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_3

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    move-object v3, p1

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    :goto_1
    if-eqz v3, :cond_2

    invoke-virtual {v3, p2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method
