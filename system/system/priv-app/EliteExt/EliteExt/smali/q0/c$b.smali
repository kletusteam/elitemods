.class public Lq0/c$b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lq0/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lq0/c;


# direct methods
.method public constructor <init>(Lq0/c;)V
    .locals 0

    iput-object p1, p0, Lq0/c$b;->a:Lq0/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    iget-object v0, p0, Lq0/c$b;->a:Lq0/c;

    iget-object v0, v0, Lq0/a;->b:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->B(Landroid/view/View;)Landroidx/recyclerview/widget/RecyclerView$a0;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$a0;->e()I

    move-result v1

    check-cast p1, Landroid/widget/Checkable;

    invoke-interface {p1}, Landroid/widget/Checkable;->isChecked()Z

    move-result p1

    iget-object v2, p0, Lq0/c$b;->a:Lq0/c;

    iget-object v2, v2, Lq0/c;->d:Ljava/util/Set;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eq p1, v2, :cond_1

    iget-object v2, p0, Lq0/c$b;->a:Lq0/c;

    if-eqz p1, :cond_0

    iget-object v2, v2, Lq0/c;->d:Ljava/util/Set;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v2, v2, Lq0/c;->d:Ljava/util/Set;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :goto_0
    iget-object v1, p0, Lq0/c$b;->a:Lq0/c;

    iget-object v1, v1, Lq0/c;->g:Lq0/c$c;

    check-cast v1, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;

    invoke-virtual {v1, v0, p1}, Lcom/EliteDevelopment/ext/SidePanelAppsActivity$e;->a(Landroidx/recyclerview/widget/RecyclerView$a0;Z)V

    :cond_1
    return-void
.end method
