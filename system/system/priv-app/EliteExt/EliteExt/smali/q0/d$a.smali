.class public Lq0/d$a;
.super Landroidx/recyclerview/widget/s$g;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lq0/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public final f:Z

.field public final g:Lq0/d$b;

.field public h:I

.field public i:I


# direct methods
.method public constructor <init>(IZLq0/d$b;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroidx/recyclerview/widget/s$g;-><init>(II)V

    iput-boolean p2, p0, Lq0/d$a;->f:Z

    iput-object p3, p0, Lq0/d$a;->g:Lq0/d$b;

    return-void
.end method


# virtual methods
.method public a(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$a0;)V
    .locals 3

    iget-object p1, p2, Landroidx/recyclerview/widget/RecyclerView$a0;->a:Landroid/view/View;

    const v0, 0x7f0a0093

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Ljava/lang/Float;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    sget-object v2, Lw/v;->a:Ljava/util/WeakHashMap;

    invoke-static {p1, v1}, Lw/v$h;->s(Landroid/view/View;F)V

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    iget-object p1, p2, Landroidx/recyclerview/widget/RecyclerView$a0;->a:Landroid/view/View;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/view/View;->setSelected(Z)V

    iget p1, p0, Lq0/d$a;->h:I

    iget p2, p0, Lq0/d$a;->i:I

    const/4 v0, -0x1

    iput v0, p0, Lq0/d$a;->h:I

    iput v0, p0, Lq0/d$a;->i:I

    if-ltz p2, :cond_1

    iget-object v0, p0, Lq0/d$a;->g:Lq0/d$b;

    check-cast v0, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-eq p1, p2, :cond_1

    invoke-virtual {v0}, Lcom/EliteDevelopment/ext/SidePanelAppsActivity;->G()V

    :cond_1
    return-void
.end method
