.class public Lp0/g$a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lp0/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lp0/g;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/text/Collator;

.field public final b:Z

.field public final c:Lz1/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, Lp0/g$a;->a:Ljava/text/Collator;

    sget-object v0, Ljava/util/Locale;->CHINESE:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lp0/g$a;->b:Z

    sget-object v0, Lz1/a;->b:Lw0/b;

    invoke-virtual {v0, p1}, Lw0/b;->n(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lz1/a;

    iput-object p1, p0, Lp0/g$a;->c:Lz1/a;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 19

    move-object/from16 v0, p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v2, p0

    iget-object v3, v2, Lp0/g$a;->c:Lz1/a;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    goto/16 :goto_10

    :cond_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v7, 0x1

    const/4 v8, 0x0

    move v10, v7

    move v9, v8

    :goto_0
    if-ge v9, v5, :cond_1a

    invoke-virtual {v0, v9}, Ljava/lang/String;->charAt(I)C

    move-result v11

    const/4 v12, 0x4

    const/16 v13, 0x20

    if-ne v11, v13, :cond_2

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v11

    if-lez v11, :cond_1

    invoke-virtual {v3, v6, v4, v10}, Lz1/a;->a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;I)V

    :cond_1
    move v7, v8

    move/from16 v18, v9

    const/4 v8, 0x3

    goto/16 :goto_e

    :cond_2
    const/16 v13, 0x100

    if-ge v11, v13, :cond_7

    if-lez v9, :cond_5

    add-int/lit8 v12, v9, -0x1

    invoke-virtual {v0, v12}, Ljava/lang/String;->charAt(I)C

    move-result v12

    const/16 v13, 0x39

    const/16 v14, 0x30

    if-lt v12, v14, :cond_3

    if-gt v12, v13, :cond_3

    move v12, v7

    goto :goto_1

    :cond_3
    move v12, v8

    :goto_1
    if-lt v11, v14, :cond_4

    if-gt v11, v13, :cond_4

    move v13, v7

    goto :goto_2

    :cond_4
    move v13, v8

    :goto_2
    if-eq v12, v13, :cond_5

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v12

    if-lez v12, :cond_5

    invoke-virtual {v3, v6, v4, v10}, Lz1/a;->a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;I)V

    :cond_5
    if-eq v10, v7, :cond_6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v12

    if-lez v12, :cond_6

    invoke-virtual {v3, v6, v4, v10}, Lz1/a;->a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;I)V

    :cond_6
    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v10, v7

    move v7, v8

    move/from16 v18, v9

    goto/16 :goto_f

    :cond_7
    const/16 v13, 0x3007

    const/4 v15, 0x2

    if-ne v11, v13, :cond_9

    new-instance v11, Lz1/a$c;

    invoke-direct {v11}, Lz1/a$c;-><init>()V

    iput v15, v11, Lz1/a$c;->c:I

    const-string v12, "ling"

    iput-object v12, v11, Lz1/a$c;->b:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v12

    if-lez v12, :cond_8

    invoke-virtual {v3, v6, v4, v10}, Lz1/a;->a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;I)V

    :cond_8
    invoke-virtual {v4, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v18, v9

    move v8, v15

    goto/16 :goto_a

    :cond_9
    const/16 v13, 0x4e00

    if-lt v11, v13, :cond_16

    const v13, 0x9fa5

    if-gt v11, v13, :cond_16

    iget-object v12, v3, Lz1/a;->a:Lz1/a$b;

    iget-object v12, v12, Lz1/a$b;->a:Lm1/i;

    if-nez v12, :cond_a

    move/from16 v18, v9

    :goto_3
    const/4 v13, 0x0

    goto/16 :goto_8

    :cond_a
    add-int/lit16 v7, v11, -0x4e00

    monitor-enter v12

    :try_start_0
    iget-object v13, v12, Lm1/i;->a:Lm1/h;

    if-eqz v13, :cond_15

    iget-object v13, v12, Lm1/i;->c:[Lm1/i$a;

    array-length v15, v13

    if-lez v15, :cond_14

    aget-object v13, v13, v8

    iget-object v13, v13, Lm1/i$a;->a:[Lm1/d;

    array-length v13, v13

    if-lez v13, :cond_13

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    invoke-virtual {v12, v8, v7}, Lm1/i;->c(II)J

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmp-long v7, v14, v16

    if-gez v7, :cond_b

    iget-object v7, v12, Lm1/i;->c:[Lm1/i$a;

    aget-object v7, v7, v8

    iget-object v7, v7, Lm1/i$a;->b:[[Ljava/lang/Object;

    aget-object v7, v7, v8

    aget-object v7, v7, v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_7

    :catchall_0
    move-exception v0

    goto/16 :goto_d

    :cond_b
    :try_start_1
    iget-object v7, v12, Lm1/i;->a:Lm1/h;

    invoke-interface {v7, v14, v15}, Lm1/h;->a(J)V

    move v14, v8

    const/4 v7, 0x0

    :goto_4
    if-gtz v14, :cond_d

    iget-object v15, v12, Lm1/i;->c:[Lm1/i$a;

    aget-object v15, v15, v8

    iget-object v15, v15, Lm1/i$a;->a:[Lm1/d;

    aget-object v15, v15, v14

    iget v15, v15, Lm1/d;->e:I

    invoke-static {v15}, Landroidx/fragment/app/m0;->d(I)I

    move-result v15

    packed-switch v15, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_6

    :pswitch_0
    :try_start_2
    iget-object v15, v12, Lm1/i;->a:Lm1/h;

    iget-object v13, v12, Lm1/i;->c:[Lm1/i$a;

    aget-object v13, v13, v8

    iget-object v13, v13, Lm1/i$a;->a:[Lm1/d;

    aget-object v13, v13, v14

    iget-byte v13, v13, Lm1/d;->a:B

    move/from16 v18, v9

    invoke-static {v15, v13}, Lm1/d;->b(Ljava/io/DataInput;I)J

    move-result-wide v8

    long-to-int v8, v8

    if-nez v14, :cond_c

    const/4 v7, 0x0

    invoke-virtual {v12, v7, v7, v8}, Lm1/i;->d(III)Ljava/lang/Object;

    move-result-object v8
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v7, v8

    goto :goto_5

    :catch_0
    move-exception v0

    :try_start_3
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v3, "File may be corrupt due to invalid data index size"

    invoke-direct {v1, v3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    goto/16 :goto_c

    :pswitch_1
    move/from16 v18, v9

    iget-object v7, v12, Lm1/i;->a:Lm1/h;

    invoke-interface {v7}, Ljava/io/DataInput;->readLong()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    goto :goto_5

    :pswitch_2
    move/from16 v18, v9

    iget-object v7, v12, Lm1/i;->a:Lm1/h;

    invoke-interface {v7}, Ljava/io/DataInput;->readInt()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    goto :goto_5

    :pswitch_3
    move/from16 v18, v9

    iget-object v7, v12, Lm1/i;->a:Lm1/h;

    invoke-interface {v7}, Ljava/io/DataInput;->readShort()S

    move-result v7

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    goto :goto_5

    :pswitch_4
    move/from16 v18, v9

    iget-object v7, v12, Lm1/i;->a:Lm1/h;

    invoke-interface {v7}, Ljava/io/DataInput;->readByte()B

    move-result v7

    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    :cond_c
    :goto_5
    add-int/lit8 v14, v14, 0x1

    move/from16 v9, v18

    const/4 v8, 0x0

    goto :goto_4

    :goto_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown type "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v12, Lm1/i;->c:[Lm1/i$a;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    iget-object v3, v3, Lm1/i$a;->a:[Lm1/d;

    aget-object v3, v3, v14

    iget v3, v3, Lm1/d;->e:I

    invoke-static {v3}, Landroidx/activity/result/a;->h(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_d
    :goto_7
    move/from16 v18, v9

    monitor-exit v12

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_e

    const-string v7, "ChinesePinyinConverter"

    const-string v8, "The ChinesePinyinConverter dictionary is not correct, need rebuild or reset the ROM."

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_e
    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    :goto_8
    new-instance v7, Lz1/a$c;

    invoke-direct {v7}, Lz1/a$c;-><init>()V

    invoke-static {v11}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lz1/a$c;->a:Ljava/lang/String;

    if-nez v13, :cond_f

    const/4 v8, 0x3

    iput v8, v7, Lz1/a$c;->c:I

    invoke-static {v11}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lz1/a$c;->b:Ljava/lang/String;

    const/4 v8, 0x2

    goto :goto_9

    :cond_f
    const/4 v8, 0x2

    iput v8, v7, Lz1/a$c;->c:I

    const/4 v9, 0x0

    aget-object v12, v13, v9

    iput-object v12, v7, Lz1/a$c;->b:Ljava/lang/String;

    :goto_9
    iget v9, v7, Lz1/a$c;->c:I

    if-ne v9, v8, :cond_11

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    if-lez v9, :cond_10

    invoke-virtual {v3, v6, v4, v10}, Lz1/a;->a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;I)V

    :cond_10
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_a
    move v15, v8

    goto :goto_b

    :cond_11
    if-eq v10, v9, :cond_12

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_12

    invoke-virtual {v3, v6, v4, v10}, Lz1/a;->a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;I)V

    :cond_12
    iget v15, v7, Lz1/a$c;->c:I

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_b
    move v10, v15

    const/4 v7, 0x0

    goto/16 :goto_f

    :goto_c
    :try_start_4
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v3, "Seek data from a corrupt file"

    invoke-direct {v1, v3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_13
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DataIndex "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, " out of range[0, "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v12, Lm1/i;->c:[Lm1/i$a;

    aget-object v3, v4, v3

    iget-object v3, v3, Lm1/i$a;->a:[Lm1/d;

    array-length v3, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_14
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Kind "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v7, 0x0

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " out of range[0, "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v12, Lm1/i;->c:[Lm1/i$a;

    array-length v3, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_15
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Get data from a corrupt file"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_d
    monitor-exit v12

    throw v0

    :cond_16
    move v7, v8

    move/from16 v18, v9

    const/16 v8, 0x900

    if-lt v11, v8, :cond_18

    const/16 v8, 0x97f

    if-gt v11, v8, :cond_18

    if-eq v10, v12, :cond_17

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_17

    invoke-virtual {v3, v6, v4, v10}, Lz1/a;->a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;I)V

    :cond_17
    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v10, v12

    goto :goto_f

    :cond_18
    const/4 v8, 0x3

    if-eq v10, v8, :cond_19

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    if-lez v9, :cond_19

    invoke-virtual {v3, v6, v4, v10}, Lz1/a;->a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;I)V

    :cond_19
    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_e
    move v10, v8

    :goto_f
    add-int/lit8 v9, v18, 0x1

    move v8, v7

    const/4 v7, 0x1

    goto/16 :goto_0

    :cond_1a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_1b

    invoke-virtual {v3, v6, v4, v10}, Lz1/a;->a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;I)V

    :cond_1b
    :goto_10
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_11
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1c

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lz1/a$c;

    iget-object v3, v3, Lz1/a$c;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_11

    :cond_1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lp0/g;

    check-cast p2, Lp0/g;

    if-nez p1, :cond_0

    if-nez p2, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    if-nez p1, :cond_1

    const/4 p1, -0x1

    goto :goto_0

    :cond_1
    if-nez p2, :cond_2

    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lp0/g$a;->b:Z

    if-eqz v0, :cond_3

    iget-object p1, p1, Lp0/g;->c:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lp0/g$a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object p2, p2, Lp0/g;->c:Ljava/lang/String;

    invoke-virtual {p0, p2}, Lp0/g$a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lp0/g$a;->a:Ljava/text/Collator;

    iget-object p1, p1, Lp0/g;->c:Ljava/lang/String;

    iget-object p2, p2, Lp0/g;->c:Ljava/lang/String;

    invoke-virtual {v0, p1, p2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    :goto_0
    return p1
.end method
