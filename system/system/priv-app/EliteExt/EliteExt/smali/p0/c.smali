.class public abstract Lp0/c;
.super Lh0/a;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lh0/a<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public l:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lh0/a;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public c(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iput-object p1, p0, Lp0/c;->l:Ljava/lang/Object;

    iget-boolean v0, p0, Lh0/b;->d:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lh0/b;->b:Lh0/b$a;

    if-eqz v0, :cond_3

    check-cast v0, Lg0/b$a;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-ne v1, v2, :cond_0

    invoke-virtual {v0, p1}, Lg0/b$a;->h(Ljava/lang/Object;)V

    goto :goto_1

    :cond_0
    iget-object v1, v0, Landroidx/lifecycle/LiveData;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, Landroidx/lifecycle/LiveData;->f:Ljava/lang/Object;

    sget-object v3, Landroidx/lifecycle/LiveData;->k:Ljava/lang/Object;

    if-ne v2, v3, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    iput-object p1, v0, Landroidx/lifecycle/LiveData;->f:Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_2

    goto :goto_1

    :cond_2
    invoke-static {}, Li/a;->m()Li/a;

    move-result-object p1

    iget-object v0, v0, Landroidx/lifecycle/LiveData;->j:Ljava/lang/Runnable;

    iget-object p1, p1, Li/a;->a:Landroidx/activity/result/d;

    invoke-virtual {p1, v0}, Landroidx/activity/result/d;->j(Ljava/lang/Runnable;)V

    goto :goto_1

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1

    :cond_3
    :goto_1
    return-void
.end method

.method public e()V
    .locals 1

    invoke-virtual {p0}, Lh0/b;->a()Z

    iget-object v0, p0, Lp0/c;->l:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lp0/c;->l:Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public f()V
    .locals 2

    iget-object v0, p0, Lp0/c;->l:Ljava/lang/Object;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lp0/c;->c(Ljava/lang/Object;)V

    :cond_0
    iget-boolean v0, p0, Lh0/b;->g:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lh0/b;->g:Z

    iget-boolean v1, p0, Lh0/b;->h:Z

    or-int/2addr v1, v0

    iput-boolean v1, p0, Lh0/b;->h:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lp0/c;->l:Ljava/lang/Object;

    if-nez v0, :cond_2

    :cond_1
    invoke-virtual {p0}, Lh0/b;->d()V

    :cond_2
    return-void
.end method
