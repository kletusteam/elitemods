.class public Lp0/b;
.super Lp0/c;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lp0/c<",
        "Ljava/util/List<",
        "Lp0/a;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final m:Landroid/content/Intent;

.field public final n:Landroid/content/pm/PackageManager;

.field public final o:Lp0/e;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lp0/c;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lp0/b;->m:Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    iput-object p1, p0, Lp0/b;->n:Landroid/content/pm/PackageManager;

    new-instance p1, Lp0/e;

    invoke-direct {p1, p0}, Lp0/e;-><init>(Lh0/b;)V

    iput-object p1, p0, Lp0/b;->o:Lp0/e;

    return-void
.end method


# virtual methods
.method public e()V
    .locals 2

    iget-object v0, p0, Lp0/b;->o:Lp0/e;

    iget-object v1, v0, Lp0/e;->a:Lh0/b;

    iget-object v1, v1, Lh0/b;->c:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-virtual {p0}, Lh0/b;->a()Z

    iget-object v0, p0, Lp0/c;->l:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lp0/c;->l:Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public f()V
    .locals 3

    invoke-super {p0}, Lp0/c;->f()V

    iget-object v0, p0, Lp0/b;->o:Lp0/e;

    iget-object v1, v0, Lp0/e;->a:Lh0/b;

    iget-object v1, v1, Lh0/b;->c:Landroid/content/Context;

    sget-object v2, Lp0/e;->b:Landroid/content/IntentFilter;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public bridge synthetic i()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lp0/b;->j()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public j()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lp0/a;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lp0/b;->n:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lp0/b;->m:Landroid/content/Intent;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    new-instance v1, Landroid/content/pm/ResolveInfo$DisplayNameComparator;

    iget-object v2, p0, Lp0/b;->n:Landroid/content/pm/PackageManager;

    invoke-direct {v1, v2}, Landroid/content/pm/ResolveInfo$DisplayNameComparator;-><init>(Landroid/content/pm/PackageManager;)V

    invoke-interface {v0, v1}, Ljava/util/List;->sort(Ljava/util/Comparator;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ResolveInfo;

    iget-object v3, p0, Lp0/b;->n:Landroid/content/pm/PackageManager;

    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-static {v3, v2}, Lp0/a;->a(Landroid/content/pm/PackageManager;Landroid/content/pm/ComponentInfo;)Lp0/a;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method
