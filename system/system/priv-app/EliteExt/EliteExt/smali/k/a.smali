.class public Lk/a;
.super Lk/g;
.source ""

# interfaces
.implements Ljava/util/Map;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lk/g<",
        "TK;TV;>;",
        "Ljava/util/Map<",
        "TK;TV;>;"
    }
.end annotation


# instance fields
.field public h:Lk/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lk/f<",
            "TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lk/g;-><init>()V

    return-void
.end method


# virtual methods
.method public entrySet()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/util/Map$Entry<",
            "TK;TV;>;>;"
        }
    .end annotation

    invoke-virtual {p0}, Lk/a;->l()Lk/f;

    move-result-object v0

    iget-object v1, v0, Lk/f;->a:Lk/f$b;

    if-nez v1, :cond_0

    new-instance v1, Lk/f$b;

    invoke-direct {v1, v0}, Lk/f$b;-><init>(Lk/f;)V

    iput-object v1, v0, Lk/f;->a:Lk/f$b;

    :cond_0
    iget-object v0, v0, Lk/f;->a:Lk/f$b;

    return-object v0
.end method

.method public keySet()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "TK;>;"
        }
    .end annotation

    invoke-virtual {p0}, Lk/a;->l()Lk/f;

    move-result-object v0

    iget-object v1, v0, Lk/f;->b:Lk/f$c;

    if-nez v1, :cond_0

    new-instance v1, Lk/f$c;

    invoke-direct {v1, v0}, Lk/f$c;-><init>(Lk/f;)V

    iput-object v1, v0, Lk/f;->b:Lk/f$c;

    :cond_0
    iget-object v0, v0, Lk/f;->b:Lk/f$c;

    return-object v0
.end method

.method public final l()Lk/f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lk/f<",
            "TK;TV;>;"
        }
    .end annotation

    iget-object v0, p0, Lk/a;->h:Lk/f;

    if-nez v0, :cond_0

    new-instance v0, Lk/a$a;

    invoke-direct {v0, p0}, Lk/a$a;-><init>(Lk/a;)V

    iput-object v0, p0, Lk/a;->h:Lk/f;

    :cond_0
    iget-object v0, p0, Lk/a;->h:Lk/f;

    return-object v0
.end method

.method public putAll(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "+TK;+TV;>;)V"
        }
    .end annotation

    iget v0, p0, Lk/g;->c:I

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    add-int/2addr v1, v0

    invoke-virtual {p0, v1}, Lk/g;->b(I)V

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lk/g;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public values()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "TV;>;"
        }
    .end annotation

    invoke-virtual {p0}, Lk/a;->l()Lk/f;

    move-result-object v0

    iget-object v1, v0, Lk/f;->c:Lk/f$e;

    if-nez v1, :cond_0

    new-instance v1, Lk/f$e;

    invoke-direct {v1, v0}, Lk/f$e;-><init>(Lk/f;)V

    iput-object v1, v0, Lk/f;->c:Lk/f$e;

    :cond_0
    iget-object v0, v0, Lk/f;->c:Lk/f$e;

    return-object v0
.end method
