.class public Lm1/j$d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lm1/j$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lm1/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lm1/j$c<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "TT;>;"
        }
    .end annotation
.end field

.field public final b:Ln1/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ln1/a<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Class;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "TT;>;I)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lm1/j$d;->a:Ljava/lang/Class;

    new-instance p1, Ln1/a;

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p1, p2, v0, v1}, Ln1/a;-><init>(IZZ)V

    iput-object p1, p0, Lm1/j$d;->b:Ln1/a;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    iget-object v0, p0, Lm1/j$d;->b:Ln1/a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    iget-object v1, v0, Ln1/a;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, v0, Ln1/a;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v3, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_3

    :cond_0
    iget-object v1, v0, Ln1/a;->c:Ln1/a$b;

    iget-object v2, v0, Ln1/a;->e:Ln1/a$b;

    iget v4, v0, Ln1/a;->a:I

    iget-object v5, v2, Ln1/a$b;->b:Ln1/a$b;

    const/4 v6, 0x1

    if-eq v5, v1, :cond_1

    iput-object p1, v2, Ln1/a$b;->a:Ljava/lang/Object;

    iget-object p1, v5, Ln1/a$b;->b:Ln1/a$b;

    if-eq p1, v1, :cond_2

    if-lez v4, :cond_2

    iput-object p1, v2, Ln1/a$b;->b:Ln1/a$b;

    sub-int/2addr v4, v6

    goto :goto_1

    :cond_1
    if-gez v4, :cond_3

    new-instance v5, Ln1/a$b;

    const/4 v7, 0x0

    invoke-direct {v5, v7}, Ln1/a$b;-><init>(Ln1/a$a;)V

    iput-object v5, v2, Ln1/a$b;->b:Ln1/a$b;

    iput-object v1, v5, Ln1/a$b;->b:Ln1/a$b;

    iput-object p1, v2, Ln1/a$b;->a:Ljava/lang/Object;

    add-int/2addr v4, v6

    :goto_1
    iput v4, v0, Ln1/a;->a:I

    :cond_2
    iget-object p1, v2, Ln1/a$b;->b:Ln1/a$b;

    iput-object p1, v0, Ln1/a;->e:Ln1/a$b;

    goto :goto_2

    :cond_3
    move v6, v3

    :goto_2
    iget-object p1, v0, Ln1/a;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p1, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    return v6

    :cond_4
    :goto_3
    invoke-static {}, Ljava/lang/Thread;->yield()V

    goto :goto_0
.end method

.method public b(I)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lm1/j$d;->b:Ln1/a;

    iget v1, v0, Ln1/a;->a:I

    iget v0, v0, Ln1/a;->b:I

    if-lez v1, :cond_0

    add-int/2addr v0, v1

    :cond_0
    add-int/2addr p1, v0

    if-gtz p1, :cond_1

    sget-object p1, Lm1/j;->a:Ljava/util/HashMap;

    monitor-enter p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lm1/j$d;->a:Ljava/lang/Class;

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0

    :cond_1
    const/4 v0, 0x0

    const/4 v1, -0x1

    if-lez p1, :cond_5

    iget-object v2, p0, Lm1/j$d;->b:Ln1/a;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-gtz p1, :cond_2

    goto :goto_3

    :cond_2
    :goto_0
    iget-object v3, v2, Ln1/a;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, v2, Ln1/a;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3, v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v3

    if-nez v3, :cond_3

    goto :goto_1

    :cond_3
    neg-int v1, p1

    iput v1, v2, Ln1/a;->a:I

    iget v1, v2, Ln1/a;->b:I

    add-int/2addr v1, p1

    iput v1, v2, Ln1/a;->b:I

    iget-object p1, v2, Ln1/a;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    goto :goto_3

    :cond_4
    :goto_1
    invoke-static {}, Ljava/lang/Thread;->yield()V

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lm1/j$d;->b:Ln1/a;

    neg-int p1, p1

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-gtz p1, :cond_6

    goto :goto_3

    :cond_6
    :goto_2
    iget-object v3, v2, Ln1/a;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    if-nez v3, :cond_8

    iget-object v3, v2, Ln1/a;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3, v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v3

    if-nez v3, :cond_7

    goto :goto_4

    :cond_7
    iget v1, v2, Ln1/a;->b:I

    sub-int/2addr v1, p1

    iput v1, v2, Ln1/a;->b:I

    iput p1, v2, Ln1/a;->a:I

    iget-object p1, v2, Ln1/a;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :goto_3
    monitor-exit p0

    return-void

    :cond_8
    :goto_4
    :try_start_4
    invoke-static {}, Ljava/lang/Thread;->yield()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public get()Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v0, p0, Lm1/j$d;->b:Ln1/a;

    :goto_0
    iget-object v1, v0, Ln1/a;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, v0, Ln1/a;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v3, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_2

    :cond_0
    iget-object v1, v0, Ln1/a;->c:Ln1/a$b;

    iget-object v2, v0, Ln1/a;->e:Ln1/a$b;

    const/4 v4, 0x0

    move-object v5, v4

    :goto_1
    if-nez v5, :cond_1

    if-eq v1, v2, :cond_1

    iget-object v5, v1, Ln1/a$b;->a:Ljava/lang/Object;

    iput-object v4, v1, Ln1/a$b;->a:Ljava/lang/Object;

    iget-object v1, v1, Ln1/a$b;->b:Ln1/a$b;

    iget-object v2, v0, Ln1/a;->e:Ln1/a$b;

    goto :goto_1

    :cond_1
    if-eqz v5, :cond_2

    iput-object v1, v0, Ln1/a;->c:Ln1/a$b;

    :cond_2
    iget-object v0, v0, Ln1/a;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    return-object v5

    :cond_3
    :goto_2
    invoke-static {}, Ljava/lang/Thread;->yield()V

    goto :goto_0
.end method
