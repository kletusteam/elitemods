.class public Lm1/c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lm1/h;


# instance fields
.field public a:Ljava/io/InputStream;

.field public b:J


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lm1/c;->a:Ljava/io/InputStream;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/io/InputStream;->mark(I)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lm1/c;->b:J

    return-void
.end method


# virtual methods
.method public a(J)V
    .locals 2

    iget-object v0, p0, Lm1/c;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->reset()V

    iget-object v0, p0, Lm1/c;->a:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    iput-wide p1, p0, Lm1/c;->b:J

    return-void

    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Skip failed"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public b()J
    .locals 2

    iget-wide v0, p0, Lm1/c;->b:J

    return-wide v0
.end method

.method public close()V
    .locals 1

    iget-object v0, p0, Lm1/c;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return-void
.end method

.method public readBoolean()Z
    .locals 4

    iget-wide v0, p0, Lm1/c;->b:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lm1/c;->b:J

    iget-object v0, p0, Lm1/c;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public readByte()B
    .locals 4

    iget-wide v0, p0, Lm1/c;->b:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lm1/c;->b:J

    iget-object v0, p0, Lm1/c;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public readChar()C
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [B

    iget-wide v2, p0, Lm1/c;->b:J

    const-wide/16 v4, 0x2

    add-long/2addr v2, v4

    iput-wide v2, p0, Lm1/c;->b:J

    iget-object v2, p0, Lm1/c;->a:Ljava/io/InputStream;

    invoke-virtual {v2, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v3, 0x0

    if-ne v2, v0, :cond_0

    const/4 v0, 0x1

    aget-byte v0, v1, v0

    and-int/lit16 v0, v0, 0xff

    int-to-char v0, v0

    aget-byte v1, v1, v3

    shl-int/lit8 v1, v1, 0x8

    const v2, 0xff00

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    int-to-char v3, v0

    :cond_0
    return v3
.end method

.method public readDouble()D
    .locals 1

    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0
.end method

.method public readFloat()F
    .locals 1

    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0
.end method

.method public readFully([B)V
    .locals 4

    iget-object v0, p0, Lm1/c;->a:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->read([B)I

    move-result p1

    iget-wide v0, p0, Lm1/c;->b:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lm1/c;->b:J

    return-void
.end method

.method public readFully([BII)V
    .locals 2

    iget-object v0, p0, Lm1/c;->a:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result p1

    iget-wide p2, p0, Lm1/c;->b:J

    int-to-long v0, p1

    add-long/2addr p2, v0

    iput-wide p2, p0, Lm1/c;->b:J

    return-void
.end method

.method public readInt()I
    .locals 6

    const/4 v0, 0x4

    new-array v1, v0, [B

    iget-wide v2, p0, Lm1/c;->b:J

    const-wide/16 v4, 0x4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lm1/c;->b:J

    iget-object v2, p0, Lm1/c;->a:Ljava/io/InputStream;

    invoke-virtual {v2, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v3, 0x0

    if-ne v2, v0, :cond_0

    const/4 v0, 0x3

    aget-byte v0, v1, v0

    and-int/lit16 v0, v0, 0xff

    const/4 v2, 0x2

    aget-byte v2, v1, v2

    shl-int/lit8 v2, v2, 0x8

    const v4, 0xff00

    and-int/2addr v2, v4

    or-int/2addr v0, v2

    const/4 v2, 0x1

    aget-byte v2, v1, v2

    shl-int/lit8 v2, v2, 0x10

    const/high16 v4, 0xff0000

    and-int/2addr v2, v4

    or-int/2addr v0, v2

    aget-byte v1, v1, v3

    shl-int/lit8 v1, v1, 0x18

    const/high16 v2, -0x1000000

    and-int/2addr v1, v2

    or-int v3, v0, v1

    :cond_0
    return v3
.end method

.method public readLine()Ljava/lang/String;
    .locals 1

    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0
.end method

.method public readLong()J
    .locals 19

    move-object/from16 v0, p0

    const/16 v1, 0x8

    new-array v2, v1, [B

    iget-wide v3, v0, Lm1/c;->b:J

    const-wide/16 v5, 0x8

    add-long/2addr v3, v5

    iput-wide v3, v0, Lm1/c;->b:J

    iget-object v3, v0, Lm1/c;->a:Ljava/io/InputStream;

    invoke-virtual {v3, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    if-ne v3, v1, :cond_0

    const/4 v3, 0x7

    aget-byte v3, v2, v3

    and-int/lit16 v3, v3, 0xff

    int-to-long v3, v3

    const/4 v5, 0x6

    aget-byte v5, v2, v5

    shl-int/lit8 v1, v5, 0x8

    int-to-long v5, v1

    const/4 v1, 0x5

    aget-byte v1, v2, v1

    shl-int/lit8 v1, v1, 0x10

    int-to-long v7, v1

    const/4 v1, 0x4

    aget-byte v1, v2, v1

    shl-int/lit8 v1, v1, 0x18

    int-to-long v9, v1

    const/4 v1, 0x3

    aget-byte v1, v2, v1

    int-to-long v11, v1

    const/4 v1, 0x2

    aget-byte v1, v2, v1

    int-to-long v13, v1

    const/4 v1, 0x1

    aget-byte v1, v2, v1

    int-to-long v0, v1

    const/4 v15, 0x0

    aget-byte v2, v2, v15

    move-wide v15, v0

    int-to-long v0, v2

    const/16 v2, 0x38

    shl-long/2addr v0, v2

    const-wide/high16 v17, -0x100000000000000L

    and-long v0, v0, v17

    const-wide/32 v17, 0xff00

    and-long v5, v5, v17

    or-long v2, v3, v5

    const-wide/32 v4, 0xff0000

    and-long/2addr v4, v7

    or-long/2addr v2, v4

    const-wide v4, 0xff000000L

    and-long/2addr v4, v9

    or-long/2addr v2, v4

    const/16 v4, 0x20

    shl-long v4, v11, v4

    const-wide v6, 0xff00000000L

    and-long/2addr v4, v6

    or-long/2addr v2, v4

    const/16 v4, 0x28

    shl-long v4, v13, v4

    const-wide v6, 0xff0000000000L

    and-long/2addr v4, v6

    or-long/2addr v2, v4

    const/16 v4, 0x30

    shl-long v4, v15, v4

    const-wide/high16 v6, 0xff000000000000L

    and-long/2addr v4, v6

    or-long/2addr v2, v4

    or-long/2addr v0, v2

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0
.end method

.method public readShort()S
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [B

    iget-wide v2, p0, Lm1/c;->b:J

    const-wide/16 v4, 0x2

    add-long/2addr v2, v4

    iput-wide v2, p0, Lm1/c;->b:J

    iget-object v2, p0, Lm1/c;->a:Ljava/io/InputStream;

    invoke-virtual {v2, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v3, 0x0

    if-ne v2, v0, :cond_0

    const/4 v0, 0x1

    aget-byte v0, v1, v0

    and-int/lit16 v0, v0, 0xff

    int-to-short v0, v0

    aget-byte v1, v1, v3

    shl-int/lit8 v1, v1, 0x8

    const v2, 0xff00

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    int-to-short v3, v0

    :cond_0
    return v3
.end method

.method public readUTF()Ljava/lang/String;
    .locals 1

    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0
.end method

.method public readUnsignedByte()I
    .locals 4

    iget-wide v0, p0, Lm1/c;->b:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lm1/c;->b:J

    iget-object v0, p0, Lm1/c;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    int-to-byte v0, v0

    return v0
.end method

.method public readUnsignedShort()I
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [B

    iget-wide v2, p0, Lm1/c;->b:J

    const-wide/16 v4, 0x2

    add-long/2addr v2, v4

    iput-wide v2, p0, Lm1/c;->b:J

    iget-object v2, p0, Lm1/c;->a:Ljava/io/InputStream;

    invoke-virtual {v2, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v3, 0x0

    if-ne v2, v0, :cond_0

    const/4 v0, 0x1

    aget-byte v0, v1, v0

    and-int/lit16 v0, v0, 0xff

    int-to-short v0, v0

    aget-byte v1, v1, v3

    shl-int/lit8 v1, v1, 0x8

    const v2, 0xff00

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    int-to-short v3, v0

    :cond_0
    return v3
.end method

.method public skipBytes(I)I
    .locals 4

    iget-object v0, p0, Lm1/c;->a:Ljava/io/InputStream;

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    long-to-int p1, v0

    iget-wide v0, p0, Lm1/c;->b:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lm1/c;->b:J

    return p1
.end method
