.class public Lm1/d;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static f:[B


# instance fields
.field public a:B

.field public b:B

.field public c:J

.field public d:B

.field public e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x400

    new-array v0, v0, [B

    sput-object v0, Lm1/d;->f:[B

    return-void
.end method

.method public constructor <init>(IBBBJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lm1/d;->e:I

    int-to-byte p1, p2

    iput-byte p1, p0, Lm1/d;->a:B

    int-to-byte p1, p3

    iput-byte p1, p0, Lm1/d;->b:B

    int-to-byte p1, p4

    iput-byte p1, p0, Lm1/d;->d:B

    iput-wide p5, p0, Lm1/d;->c:J

    return-void
.end method

.method public static a(Lm1/d;Lm1/h;)[Ljava/lang/Object;
    .locals 3

    iget v0, p0, Lm1/d;->e:I

    invoke-static {v0}, Landroidx/fragment/app/m0;->d(I)I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    goto :goto_0

    :pswitch_0
    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0, p1, v2}, Lm1/d;->c(Lm1/h;I)Ljava/lang/Object;

    move-result-object p0

    aput-object p0, v0, v2

    goto :goto_0

    :pswitch_1
    new-array v0, v1, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/io/DataInput;->readLong()J

    move-result-wide p0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    aput-object p0, v0, v2

    goto :goto_0

    :pswitch_2
    new-array v0, v1, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v0, v2

    goto :goto_0

    :pswitch_3
    new-array v0, v1, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/io/DataInput;->readShort()S

    move-result p0

    invoke-static {p0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object p0

    aput-object p0, v0, v2

    goto :goto_0

    :pswitch_4
    new-array v0, v1, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/io/DataInput;->readByte()B

    move-result p0

    invoke-static {p0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p0

    aput-object p0, v0, v2

    :goto_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static b(Ljava/io/DataInput;I)J
    .locals 1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    const/16 v0, 0x8

    if-ne p1, v0, :cond_0

    invoke-interface {p0}, Ljava/io/DataInput;->readLong()J

    move-result-wide p0

    goto :goto_1

    :cond_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unsuppoert size "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result p0

    goto :goto_0

    :cond_2
    invoke-interface {p0}, Ljava/io/DataInput;->readShort()S

    move-result p0

    goto :goto_0

    :cond_3
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result p0

    :goto_0
    int-to-long p0, p0

    :goto_1
    return-wide p0
.end method


# virtual methods
.method public final c(Lm1/h;I)Ljava/lang/Object;
    .locals 7

    const-class v0, Lm1/d;

    invoke-interface {p1}, Lm1/h;->b()J

    move-result-wide v1

    if-eqz p2, :cond_0

    iget-byte v3, p0, Lm1/d;->d:B

    mul-int/2addr v3, p2

    int-to-long v3, v3

    add-long/2addr v3, v1

    invoke-interface {p1, v3, v4}, Lm1/h;->a(J)V

    :cond_0
    iget-byte p2, p0, Lm1/d;->d:B

    invoke-static {p1, p2}, Lm1/d;->b(Ljava/io/DataInput;I)J

    move-result-wide v3

    add-long/2addr v3, v1

    invoke-interface {p1, v3, v4}, Lm1/h;->a(J)V

    sget-object p2, Lm1/a;->a:[I

    iget v1, p0, Lm1/d;->e:I

    invoke-static {v1}, Landroidx/fragment/app/m0;->d(I)I

    move-result v1

    aget p2, p2, v1

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eq p2, v3, :cond_5

    const/4 v3, 0x2

    if-eq p2, v3, :cond_4

    const/4 v3, 0x3

    if-eq p2, v3, :cond_3

    const/4 v3, 0x4

    if-eq p2, v3, :cond_2

    const/4 v3, 0x5

    if-eq p2, v3, :cond_1

    move-object v3, v1

    goto/16 :goto_4

    :cond_1
    iget-byte p2, p0, Lm1/d;->b:B

    invoke-static {p1, p2}, Lm1/d;->b(Ljava/io/DataInput;I)J

    move-result-wide v3

    long-to-int p2, v3

    new-array v3, p2, [J

    :goto_0
    if-ge v2, p2, :cond_8

    invoke-interface {p1}, Ljava/io/DataInput;->readLong()J

    move-result-wide v4

    aput-wide v4, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    iget-byte p2, p0, Lm1/d;->b:B

    invoke-static {p1, p2}, Lm1/d;->b(Ljava/io/DataInput;I)J

    move-result-wide v3

    long-to-int p2, v3

    new-array v3, p2, [I

    :goto_1
    if-ge v2, p2, :cond_8

    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v4

    aput v4, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    iget-byte p2, p0, Lm1/d;->b:B

    invoke-static {p1, p2}, Lm1/d;->b(Ljava/io/DataInput;I)J

    move-result-wide v3

    long-to-int p2, v3

    new-array v3, p2, [S

    :goto_2
    if-ge v2, p2, :cond_8

    invoke-interface {p1}, Ljava/io/DataInput;->readShort()S

    move-result v4

    aput-short v4, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    iget-byte p2, p0, Lm1/d;->b:B

    invoke-static {p1, p2}, Lm1/d;->b(Ljava/io/DataInput;I)J

    move-result-wide v2

    long-to-int p2, v2

    new-array p2, p2, [B

    invoke-interface {p1, p2}, Ljava/io/DataInput;->readFully([B)V

    move-object v3, p2

    goto :goto_4

    :cond_5
    iget-byte p2, p0, Lm1/d;->b:B

    invoke-static {p1, p2}, Lm1/d;->b(Ljava/io/DataInput;I)J

    move-result-wide v3

    long-to-int p2, v3

    monitor-enter v0

    :try_start_0
    sget-object v3, Lm1/d;->f:[B

    if-eqz v3, :cond_6

    array-length v3, v3

    if-ge v3, p2, :cond_7

    goto :goto_3

    :catchall_0
    move-exception p1

    goto :goto_7

    :cond_6
    :goto_3
    new-array v3, p2, [B

    sput-object v3, Lm1/d;->f:[B

    :cond_7
    sget-object v3, Lm1/d;->f:[B

    sput-object v1, Lm1/d;->f:[B

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {p1, v3, v2, p2}, Ljava/io/DataInput;->readFully([BII)V

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3, v2, p2}, Ljava/lang/String;-><init>([BII)V

    move-object v6, v3

    move-object v3, v1

    move-object v1, v6

    :cond_8
    :goto_4
    monitor-enter v0

    if-eqz v1, :cond_a

    :try_start_1
    sget-object p1, Lm1/d;->f:[B

    if-eqz p1, :cond_9

    array-length p1, p1

    array-length p2, v1

    if-ge p1, p2, :cond_a

    goto :goto_5

    :catchall_1
    move-exception p1

    goto :goto_6

    :cond_9
    :goto_5
    sput-object v1, Lm1/d;->f:[B

    :cond_a
    monitor-exit v0

    return-object v3

    :goto_6
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw p1

    :goto_7
    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1
.end method
