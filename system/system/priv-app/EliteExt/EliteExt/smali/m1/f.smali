.class public Lm1/f;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final b:[B


# instance fields
.field public a:[Lm1/e;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lm1/f;->b:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x49t
        0x44t
        0x46t
        0x20t
    .end array-data
.end method

.method public constructor <init>(II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array p1, p1, [Lm1/e;

    iput-object p1, p0, Lm1/f;->a:[Lm1/e;

    return-void
.end method

.method public static a(Ljava/io/DataInput;)Lm1/f;
    .locals 9

    sget-object v0, Lm1/f;->b:[B

    array-length v0, v0

    new-array v1, v0, [B

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v0, :cond_0

    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v4

    aput-byte v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, Lm1/f;->b:[B

    invoke-static {v1, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v0

    new-instance v1, Lm1/f;

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v3

    invoke-direct {v1, v0, v3}, Lm1/f;-><init>(II)V

    :goto_1
    if-ge v2, v0, :cond_1

    iget-object v3, v1, Lm1/f;->a:[Lm1/e;

    new-instance v4, Lm1/e;

    invoke-interface {p0}, Ljava/io/DataInput;->readLong()J

    move-result-wide v5

    invoke-interface {p0}, Ljava/io/DataInput;->readLong()J

    move-result-wide v7

    invoke-direct {v4, v5, v6, v7, v8}, Lm1/e;-><init>(JJ)V

    aput-object v4, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    return-object v1

    :cond_2
    new-instance p0, Ljava/io/IOException;

    const-string v0, "File version unmatched, please upgrade your reader"

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_3
    new-instance p0, Ljava/io/IOException;

    const-string v0, "File tag unmatched, file may be corrupt"

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
