.class public abstract Lm1/j$b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lm1/j$f;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lm1/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lm1/j$f<",
        "TT;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Object;

.field public b:Lm1/j$c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lm1/j$c<",
            "TT;>;"
        }
    .end annotation
.end field

.field public final c:Lm1/j$e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lm1/j$e<",
            "TT;>;"
        }
    .end annotation
.end field

.field public final d:I


# direct methods
.method public constructor <init>(Lm1/j$e;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lm1/j$e<",
            "TT;>;I)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lm1/j$b$a;

    invoke-direct {v0, p0}, Lm1/j$b$a;-><init>(Lm1/j$b;)V

    iput-object v0, p0, Lm1/j$b;->a:Ljava/lang/Object;

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    if-lt p2, v1, :cond_1

    iput-object p1, p0, Lm1/j$b;->c:Lm1/j$e;

    iput p2, p0, Lm1/j$b;->d:I

    invoke-virtual {p1}, Lm1/j$e;->a()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lm1/j$b;->b(Ljava/lang/Class;I)Lm1/j$c;

    move-result-object p2

    iput-object p2, p0, Lm1/j$b;->b:Lm1/j$c;

    invoke-virtual {p0, p1}, Lm1/j$b;->d(Ljava/lang/Object;)V

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "manager create instance cannot return null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result p1

    iput p1, p0, Lm1/j$b;->d:I

    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "manager cannot be null and size cannot less then 1"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v0, p0, Lm1/j$b;->b:Lm1/j$c;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lm1/j$c;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lm1/j$b;->c:Lm1/j$e;

    invoke-virtual {v0}, Lm1/j$e;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "manager create instance cannot return null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    iget-object v1, p0, Lm1/j$b;->c:Lm1/j$e;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot acquire object after close()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract b(Ljava/lang/Class;I)Lm1/j$c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "TT;>;I)",
            "Lm1/j$c<",
            "TT;>;"
        }
    .end annotation
.end method

.method public abstract c(Lm1/j$c;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lm1/j$c<",
            "TT;>;I)V"
        }
    .end annotation
.end method

.method public final d(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lm1/j$b;->b:Lm1/j$c;

    if-eqz v0, :cond_2

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lm1/j$b;->c:Lm1/j$e;

    invoke-virtual {v0, p1}, Lm1/j$e;->b(Ljava/lang/Object;)V

    iget-object v0, p0, Lm1/j$b;->b:Lm1/j$c;

    invoke-interface {v0, p1}, Lm1/j$c;->a(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lm1/j$b;->c:Lm1/j$e;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void

    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cannot release object after close()"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
