.class public Lm1/j$i;
.super Lm1/j$b;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lm1/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "i"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lm1/j$b<",
        "TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lm1/j$e;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lm1/j$e<",
            "TT;>;I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lm1/j$b;-><init>(Lm1/j$e;I)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Class;I)Lm1/j$c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "TT;>;I)",
            "Lm1/j$c<",
            "TT;>;"
        }
    .end annotation

    sget-object v0, Lm1/j;->b:Ljava/util/HashMap;

    monitor-enter v0

    :try_start_0
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lm1/j$h;

    if-nez v1, :cond_0

    new-instance v1, Lm1/j$h;

    invoke-direct {v1, p1, p2}, Lm1/j$h;-><init>(Ljava/lang/Class;I)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-virtual {v1, p2}, Lm1/j$h;->b(I)V

    :goto_0
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public final c(Lm1/j$c;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lm1/j$c<",
            "TT;>;I)V"
        }
    .end annotation

    check-cast p1, Lm1/j$h;

    sget-object v0, Lm1/j;->b:Ljava/util/HashMap;

    monitor-enter v0

    neg-int p2, p2

    :try_start_0
    invoke-virtual {p1, p2}, Lm1/j$h;->b(I)V

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method
