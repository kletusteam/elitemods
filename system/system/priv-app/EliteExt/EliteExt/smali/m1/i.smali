.class public Lm1/i;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lm1/i$a;
    }
.end annotation


# instance fields
.field public a:Lm1/h;

.field public b:Lm1/f;

.field public c:[Lm1/i$a;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lm1/a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance p2, Lm1/c;

    invoke-direct {p2, p1}, Lm1/c;-><init>(Ljava/io/InputStream;)V

    iput-object p2, p0, Lm1/i;->a:Lm1/h;

    const-string p1, "assets"

    invoke-virtual {p0, p1}, Lm1/i;->b(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lm1/a;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance p2, Lm1/b;

    new-instance v0, Ljava/io/RandomAccessFile;

    const-string v1, "r"

    invoke-direct {v0, p1, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p2, v0}, Lm1/b;-><init>(Ljava/io/RandomAccessFile;)V

    iput-object p2, p0, Lm1/i;->a:Lm1/h;

    invoke-virtual {p0, p1}, Lm1/i;->b(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lm1/i;->a:Lm1/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-interface {v0}, Lm1/h;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catch_0
    :cond_0
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lm1/i;->a:Lm1/h;

    iput-object v0, p0, Lm1/i;->b:Lm1/f;

    iput-object v0, p0, Lm1/i;->c:[Lm1/i$a;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 13

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    :try_start_0
    iget-object p1, p0, Lm1/i;->a:Lm1/h;

    const-wide/16 v0, 0x0

    invoke-interface {p1, v0, v1}, Lm1/h;->a(J)V

    iget-object p1, p0, Lm1/i;->a:Lm1/h;

    invoke-static {p1}, Lm1/f;->a(Ljava/io/DataInput;)Lm1/f;

    move-result-object p1

    iput-object p1, p0, Lm1/i;->b:Lm1/f;

    iget-object p1, p1, Lm1/f;->a:[Lm1/e;

    array-length p1, p1

    new-array p1, p1, [Lm1/i$a;

    iput-object p1, p0, Lm1/i;->c:[Lm1/i$a;

    const/4 p1, 0x0

    move v0, p1

    :goto_0
    iget-object v1, p0, Lm1/i;->b:Lm1/f;

    iget-object v1, v1, Lm1/f;->a:[Lm1/e;

    array-length v2, v1

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Lm1/i;->c:[Lm1/i$a;

    new-instance v3, Lm1/i$a;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lm1/i$a;-><init>(Lm1/a;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    aput-object v3, v2, v0

    :try_start_1
    iget-object v2, p0, Lm1/i;->a:Lm1/h;

    aget-object v1, v1, v0

    iget-wide v3, v1, Lm1/e;->b:J

    invoke-interface {v2, v3, v4}, Lm1/h;->a(J)V

    iget-object v1, p0, Lm1/i;->a:Lm1/h;

    invoke-interface {v1}, Ljava/io/DataInput;->readInt()I

    move-result v1

    iget-object v2, p0, Lm1/i;->c:[Lm1/i$a;

    aget-object v2, v2, v0

    new-array v3, v1, [Lm1/g;

    iput-object v3, v2, Lm1/i$a;->c:[Lm1/g;

    move v2, p1

    :goto_1
    if-ge v2, v1, :cond_0

    iget-object v3, p0, Lm1/i;->c:[Lm1/i$a;

    aget-object v3, v3, v0

    iget-object v3, v3, Lm1/i$a;->c:[Lm1/g;

    iget-object v4, p0, Lm1/i;->a:Lm1/h;

    new-instance v5, Lm1/g;

    invoke-interface {v4}, Ljava/io/DataInput;->readInt()I

    move-result v6

    invoke-interface {v4}, Ljava/io/DataInput;->readInt()I

    move-result v7

    invoke-interface {v4}, Ljava/io/DataInput;->readLong()J

    move-result-wide v8

    invoke-direct {v5, v6, v7, v8, v9}, Lm1/g;-><init>(IIJ)V

    aput-object v5, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lm1/i;->a:Lm1/h;

    iget-object v2, p0, Lm1/i;->b:Lm1/f;

    iget-object v2, v2, Lm1/f;->a:[Lm1/e;

    aget-object v2, v2, v0

    iget-wide v2, v2, Lm1/e;->a:J

    invoke-interface {v1, v2, v3}, Lm1/h;->a(J)V

    iget-object v1, p0, Lm1/i;->a:Lm1/h;

    invoke-interface {v1}, Ljava/io/DataInput;->readInt()I

    move-result v1

    iget-object v2, p0, Lm1/i;->c:[Lm1/i$a;

    aget-object v3, v2, v0

    iput p1, v3, Lm1/i$a;->d:I

    aget-object v2, v2, v0

    new-array v3, v1, [Lm1/d;

    iput-object v3, v2, Lm1/i$a;->a:[Lm1/d;

    move v2, p1

    :goto_2
    if-ge v2, v1, :cond_1

    iget-object v3, p0, Lm1/i;->c:[Lm1/i$a;

    aget-object v3, v3, v0

    iget-object v3, v3, Lm1/i$a;->a:[Lm1/d;

    iget-object v4, p0, Lm1/i;->a:Lm1/h;

    new-instance v12, Lm1/d;

    invoke-static {}, Landroidx/activity/result/a;->a()[I

    move-result-object v5

    invoke-interface {v4}, Ljava/io/DataInput;->readByte()B

    move-result v6

    aget v6, v5, v6

    invoke-interface {v4}, Ljava/io/DataInput;->readByte()B

    move-result v7

    invoke-interface {v4}, Ljava/io/DataInput;->readByte()B

    move-result v8

    invoke-interface {v4}, Ljava/io/DataInput;->readByte()B

    move-result v9

    invoke-interface {v4}, Ljava/io/DataInput;->readLong()J

    move-result-wide v10

    move-object v5, v12

    invoke-direct/range {v5 .. v11}, Lm1/d;-><init>(IBBBJ)V

    aput-object v12, v3, v2

    iget-object v3, p0, Lm1/i;->c:[Lm1/i$a;

    aget-object v4, v3, v0

    iget v5, v4, Lm1/i$a;->d:I

    aget-object v3, v3, v0

    iget-object v3, v3, Lm1/i$a;->a:[Lm1/d;

    aget-object v3, v3, v2

    iget-byte v3, v3, Lm1/d;->a:B

    add-int/2addr v5, v3

    iput v5, v4, Lm1/i$a;->d:I

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_1
    iget-object v2, p0, Lm1/i;->c:[Lm1/i$a;

    aget-object v2, v2, v0

    new-array v3, v1, [[Ljava/lang/Object;

    iput-object v3, v2, Lm1/i$a;->b:[[Ljava/lang/Object;

    move v2, p1

    :goto_3
    if-ge v2, v1, :cond_2

    iget-object v3, p0, Lm1/i;->a:Lm1/h;

    iget-object v4, p0, Lm1/i;->c:[Lm1/i$a;

    aget-object v4, v4, v0

    iget-object v4, v4, Lm1/i$a;->a:[Lm1/d;

    aget-object v4, v4, v2

    iget-wide v4, v4, Lm1/d;->c:J

    invoke-interface {v3, v4, v5}, Lm1/h;->a(J)V

    iget-object v3, p0, Lm1/i;->c:[Lm1/i$a;

    aget-object v4, v3, v0

    iget-object v4, v4, Lm1/i$a;->b:[[Ljava/lang/Object;

    aget-object v3, v3, v0

    iget-object v3, v3, Lm1/i$a;->a:[Lm1/d;

    aget-object v3, v3, v2

    iget-object v5, p0, Lm1/i;->a:Lm1/h;

    invoke-static {v3, v5}, Lm1/d;->a(Lm1/d;Lm1/h;)[Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v4, v2
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_3
    return-void

    :catch_0
    move-exception p1

    invoke-virtual {p0}, Lm1/i;->a()V

    throw p1
.end method

.method public final c(II)J
    .locals 5

    iget-object v0, p0, Lm1/i;->c:[Lm1/i$a;

    aget-object v0, v0, p1

    iget-object v0, v0, Lm1/i$a;->c:[Lm1/g;

    array-length v0, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    add-int v2, v0, v1

    div-int/lit8 v2, v2, 0x2

    iget-object v3, p0, Lm1/i;->c:[Lm1/i$a;

    aget-object v4, v3, p1

    iget-object v4, v4, Lm1/i$a;->c:[Lm1/g;

    aget-object v4, v4, v2

    iget v4, v4, Lm1/g;->b:I

    if-le v4, p2, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    aget-object v1, v3, p1

    iget-object v1, v1, Lm1/i$a;->c:[Lm1/g;

    aget-object v1, v1, v2

    iget v1, v1, Lm1/g;->a:I

    if-gt v1, p2, :cond_1

    add-int/lit8 v1, v2, 0x1

    goto :goto_0

    :cond_1
    aget-object v0, v3, p1

    iget-object v0, v0, Lm1/i$a;->c:[Lm1/g;

    aget-object v0, v0, v2

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    const-wide/16 v1, -0x1

    if-eqz v0, :cond_3

    iget-wide v1, v0, Lm1/g;->c:J

    iget v0, v0, Lm1/g;->b:I

    sub-int/2addr p2, v0

    iget-object v0, p0, Lm1/i;->c:[Lm1/i$a;

    aget-object p1, v0, p1

    iget p1, p1, Lm1/i$a;->d:I

    mul-int/2addr p2, p1

    int-to-long p1, p2

    add-long/2addr v1, p1

    :cond_3
    return-wide v1
.end method

.method public final d(III)Ljava/lang/Object;
    .locals 6

    iget-object v0, p0, Lm1/i;->c:[Lm1/i$a;

    aget-object v1, v0, p1

    iget-object v1, v1, Lm1/i$a;->b:[[Ljava/lang/Object;

    aget-object v1, v1, p2

    aget-object v1, v1, p3

    if-nez v1, :cond_0

    iget-object v1, p0, Lm1/i;->a:Lm1/h;

    aget-object v0, v0, p1

    iget-object v0, v0, Lm1/i$a;->a:[Lm1/d;

    aget-object v0, v0, p2

    iget-wide v2, v0, Lm1/d;->c:J

    const-wide/16 v4, 0x4

    add-long/2addr v2, v4

    invoke-interface {v1, v2, v3}, Lm1/h;->a(J)V

    iget-object v0, p0, Lm1/i;->c:[Lm1/i$a;

    aget-object v1, v0, p1

    iget-object v1, v1, Lm1/i$a;->b:[[Ljava/lang/Object;

    aget-object v1, v1, p2

    aget-object v0, v0, p1

    iget-object v0, v0, Lm1/i$a;->a:[Lm1/d;

    aget-object v0, v0, p2

    iget-object v2, p0, Lm1/i;->a:Lm1/h;

    invoke-virtual {v0, v2, p3}, Lm1/d;->c(Lm1/h;I)Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v1, p3

    :cond_0
    iget-object v0, p0, Lm1/i;->c:[Lm1/i$a;

    aget-object p1, v0, p1

    iget-object p1, p1, Lm1/i$a;->b:[[Ljava/lang/Object;

    aget-object p1, p1, p2

    aget-object p1, p1, p3

    return-object p1
.end method
