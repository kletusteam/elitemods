.class public Ly0/a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ly0/a$a;,
        Ly0/a$b;,
        Ly0/a$c;,
        Ly0/a$d;,
        Ly0/a$e;,
        Ly0/a$f;,
        Ly0/a$g;,
        Ly0/a$h;
    }
.end annotation


# static fields
.field public static final b:Ly0/a$a;

.field public static final c:Ly0/a$b;

.field public static final d:Ly0/a$c;

.field public static final e:Ly0/a$e;

.field public static final f:Ly0/a$f;

.field public static final g:Ly0/a$g;

.field public static final h:Ly0/a$h;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/util/List<",
            "Ly0/b;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ly0/a$a;

    invoke-direct {v0}, Ly0/a$a;-><init>()V

    sput-object v0, Ly0/a;->b:Ly0/a$a;

    new-instance v0, Ly0/a$f;

    invoke-direct {v0}, Ly0/a$f;-><init>()V

    sput-object v0, Ly0/a;->f:Ly0/a$f;

    new-instance v0, Ly0/a$e;

    invoke-direct {v0}, Ly0/a$e;-><init>()V

    sput-object v0, Ly0/a;->e:Ly0/a$e;

    new-instance v0, Ly0/a$h;

    invoke-direct {v0}, Ly0/a$h;-><init>()V

    sput-object v0, Ly0/a;->h:Ly0/a$h;

    new-instance v0, Ly0/a$g;

    invoke-direct {v0}, Ly0/a$g;-><init>()V

    sput-object v0, Ly0/a;->g:Ly0/a$g;

    new-instance v0, Ly0/a$b;

    invoke-direct {v0}, Ly0/a$b;-><init>()V

    sput-object v0, Ly0/a;->c:Ly0/a$b;

    new-instance v0, Ly0/a$c;

    invoke-direct {v0}, Ly0/a$c;-><init>()V

    sput-object v0, Ly0/a;->d:Ly0/a$c;

    return-void
.end method

.method public constructor <init>(Lu0/b;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance p1, Landroid/util/ArrayMap;

    invoke-direct {p1}, Landroid/util/ArrayMap;-><init>()V

    iput-object p1, p0, Ly0/a;->a:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Lv0/a;)Z
    .locals 2

    iget-object v0, p2, Lv0/a;->e:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Ly0/a;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_1

    const-class v0, Ljava/util/ArrayList;

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lc1/g;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iget-object v1, p0, Ly0/a;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget-object p1, p2, Lv0/a;->e:Ljava/util/HashSet;

    sget-object p2, Lc1/a;->a:[Ljava/lang/Class;

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    invoke-interface {v0, p2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-interface {v0, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    const/4 p1, 0x1

    return p1
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;Ly0/a$d;Ljava/util/Collection;Ly0/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ly0/a$d;",
            "Ljava/util/Collection<",
            "Ly0/c;",
            ">;",
            "Ly0/c;",
            ")V"
        }
    .end annotation

    iget-object p5, p0, Ly0/a;->a:Ljava/util/Map;

    invoke-interface {p5, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p5

    if-nez p5, :cond_2

    const-class p5, Ljava/util/HashSet;

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p5, v0}, Lc1/g;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ly0/b;

    invoke-interface {p5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {p3, p2, v0, p4, v1}, Ly0/a$d;->a(Ljava/lang/Object;Ly0/b;Ljava/util/Collection;Ly0/c;)V

    goto :goto_0

    :cond_1
    invoke-static {p5}, Lc1/g;->c(Ljava/lang/Object;)V

    :cond_2
    return-void
.end method

.method public c(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/util/Collection<",
            "Ly0/c;",
            ">;)V"
        }
    .end annotation

    sget-object v3, Ly0/a;->g:Ly0/a$g;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Ly0/a;->b(Ljava/lang/Object;Ljava/lang/Object;Ly0/a$d;Ljava/util/Collection;Ly0/c;)V

    return-void
.end method

.method public d(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Ly0/a;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    invoke-static {p1}, Lc1/g;->c(Ljava/lang/Object;)V

    return-void
.end method
