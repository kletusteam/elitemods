.class public Ly0/a$g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ly0/a$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ly0/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "g"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Ly0/b;Ljava/util/Collection;Ly0/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ly0/b;",
            "Ljava/util/Collection<",
            "Ly0/c;",
            ">;",
            "Ly0/c;",
            ")V"
        }
    .end annotation

    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_0
    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_2

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Ly0/c;

    iget-boolean v0, p4, Ly0/c;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p4, Ly0/c;->a:Lx0/b;

    iget-boolean v0, v0, Lx0/b;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p4, Ly0/c;->a:Lx0/b;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lx0/b;->c:Z

    iget-object v0, p4, Ly0/c;->a:Lx0/b;

    iget-byte v0, v0, Lx0/b;->d:B

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    invoke-virtual {p2, p1, p4}, Ly0/b;->d(Ljava/lang/Object;Ly0/c;)V

    goto :goto_0

    :cond_1
    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    return-void
.end method
