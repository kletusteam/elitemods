.class public Ly0/c;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lx0/b;

.field public volatile b:I

.field public volatile c:Z

.field public final d:La1/b;

.field public final e:Z

.field public volatile f:D


# direct methods
.method public constructor <init>(La1/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lx0/b;

    invoke-direct {v0}, Lx0/b;-><init>()V

    iput-object v0, p0, Ly0/c;->a:Lx0/b;

    iput-object p1, p0, Ly0/c;->d:La1/b;

    instance-of p1, p1, La1/c;

    iput-boolean p1, p0, Ly0/c;->e:Z

    return-void
.end method


# virtual methods
.method public a()F
    .locals 5

    iget-object v0, p0, Ly0/c;->a:Lx0/b;

    iget-wide v0, v0, Lx0/b;->f:D

    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v4, v0, v2

    if-eqz v4, :cond_0

    double-to-float v0, v0

    return v0

    :cond_0
    iget-object v0, p0, Ly0/c;->a:Lx0/b;

    iget-wide v0, v0, Lx0/b;->k:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ly0/c;->a:Lx0/b;

    iget-wide v0, v0, Lx0/b;->k:D

    double-to-float v0, v0

    :goto_0
    return v0
.end method

.method public b()I
    .locals 5

    iget-object v0, p0, Ly0/c;->a:Lx0/b;

    iget-wide v0, v0, Lx0/b;->f:D

    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v4, v0, v2

    if-eqz v4, :cond_0

    double-to-int v0, v0

    return v0

    :cond_0
    iget-object v0, p0, Ly0/c;->a:Lx0/b;

    iget-wide v0, v0, Lx0/b;->k:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    const v0, 0x7fffffff

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ly0/c;->a:Lx0/b;

    iget-wide v0, v0, Lx0/b;->k:D

    double-to-int v0, v0

    :goto_0
    return v0
.end method

.method public c(B)V
    .locals 2

    const/4 v0, 0x1

    if-eqz p1, :cond_1

    const/4 v1, 0x2

    if-le p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v1, v0

    :goto_1
    iput-boolean v1, p0, Ly0/c;->c:Z

    iget-boolean v1, p0, Ly0/c;->c:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Ly0/c;->a:Lx0/b;

    iget-byte v1, v1, Lx0/b;->d:B

    invoke-static {v1}, Lx0/i;->a(B)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Ly0/c;->a:Lx0/b;

    iput-boolean v0, v1, Lx0/b;->c:Z

    :cond_2
    iget-object v0, p0, Ly0/c;->a:Lx0/b;

    int-to-byte p1, p1

    iput-byte p1, v0, Lx0/b;->d:B

    return-void
.end method

.method public d(Lu0/b;)V
    .locals 2

    iget-boolean v0, p0, Ly0/c;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ly0/c;->d:La1/b;

    check-cast v0, La1/c;

    invoke-virtual {p0}, Ly0/c;->b()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lu0/b;->l(La1/c;I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Ly0/c;->d:La1/b;

    invoke-virtual {p0}, Ly0/c;->a()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lu0/b;->n(La1/b;F)V

    :goto_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "UpdateInfo{, property="

    invoke-static {v0}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ly0/c;->d:La1/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", velocity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Ly0/c;->f:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v1, ", value = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Ly0/c;->a:Lx0/b;

    iget-wide v1, v1, Lx0/b;->k:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v1, ", useInt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Ly0/c;->e:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", frameCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Ly0/c;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", isCompleted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Ly0/c;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
