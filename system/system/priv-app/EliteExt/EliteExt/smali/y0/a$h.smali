.class public Ly0/a$h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ly0/a$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ly0/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "h"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Ly0/b;Ljava/util/Collection;Ly0/c;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ly0/b;",
            "Ljava/util/Collection<",
            "Ly0/c;",
            ">;",
            "Ly0/c;",
            ")V"
        }
    .end annotation

    if-eqz p3, :cond_1

    invoke-interface {p3}, Ljava/util/Collection;->size()I

    move-result p4

    const/16 v0, 0xfa0

    if-gt p4, v0, :cond_1

    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p4

    :goto_0
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ly0/c;

    iget-object v1, v0, Ly0/c;->d:La1/b;

    invoke-virtual {v0}, Ly0/c;->a()F

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v1, v0, Ly0/c;->e:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Ly0/c;->d:La1/b;

    move-object v4, v1

    check-cast v4, La1/c;

    invoke-virtual {v0}, Ly0/c;->b()I

    move-result v5

    iget-wide v1, v0, Ly0/c;->f:D

    double-to-float v6, v1

    iget-boolean v7, v0, Ly0/c;->c:Z

    move-object v2, p2

    move-object v3, p1

    invoke-virtual/range {v2 .. v7}, Ly0/b;->e(Ljava/lang/Object;La1/c;IFZ)V

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ly0/c;->a()F

    goto :goto_0

    :cond_1
    invoke-virtual {p2, p1, p3}, Ly0/b;->f(Ljava/lang/Object;Ljava/util/Collection;)V

    return-void
.end method
