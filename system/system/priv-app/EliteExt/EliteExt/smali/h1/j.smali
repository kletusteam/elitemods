.class public Lh1/j;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/widget/PopupWindow$OnDismissListener;
.implements Lh1/k;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lh1/j$a;
    }
.end annotation


# instance fields
.field public a:Lh1/j$a;

.field public b:Landroid/view/View;

.field public c:Landroid/content/Context;

.field public d:Z

.field public e:Landroid/view/LayoutInflater;

.field public f:Lh1/g;

.field public g:I

.field public h:Z

.field public i:Lt1/a;

.field public j:Lh1/k$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lh1/g;Landroid/view/View;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f0d0052

    iput v0, p0, Lh1/j;->g:I

    iput-object p1, p0, Lh1/j;->c:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lh1/j;->e:Landroid/view/LayoutInflater;

    iput-object p2, p0, Lh1/j;->f:Lh1/g;

    iput-boolean p4, p0, Lh1/j;->h:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p4

    iget p4, p4, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 p4, p4, 0x2

    const v0, 0x7f0700d6

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    invoke-static {p4, p1}, Ljava/lang/Math;->max(II)I

    iput-object p3, p0, Lh1/j;->b:Landroid/view/View;

    invoke-virtual {p2, p0}, Lh1/g;->b(Lh1/k;)V

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b(Lh1/g;Z)V
    .locals 1

    iget-object v0, p0, Lh1/j;->f:Lh1/g;

    if-eq p1, v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lh1/j;->d(Z)V

    iget-object v0, p0, Lh1/j;->j:Lh1/k$a;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1, p2}, Lh1/k$a;->b(Lh1/g;Z)V

    :cond_1
    return-void
.end method

.method public c(Z)V
    .locals 0

    iget-object p1, p0, Lh1/j;->a:Lh1/j$a;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lh1/j$a;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public d(Z)V
    .locals 0

    invoke-virtual {p0}, Lh1/j;->isShowing()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lh1/j;->i:Lt1/a;

    invoke-virtual {p1}, Landroid/widget/PopupWindow;->dismiss()V

    :cond_0
    return-void
.end method

.method public e(Landroid/content/Context;Lh1/g;)V
    .locals 0

    return-void
.end method

.method public f(Lh1/g;Lh1/i;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public g()Z
    .locals 4

    new-instance v0, Lt1/a;

    iget-object v1, p0, Lh1/j;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lt1/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lh1/j;->i:Lt1/a;

    iget-object v1, p0, Lh1/j;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07015c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, v0, Lt1/a;->j:I

    iget-object v0, p0, Lh1/j;->i:Lt1/a;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lt1/a;->h:Z

    iput-object p0, v0, Lt1/a;->s:Landroid/widget/PopupWindow$OnDismissListener;

    iput-object p0, v0, Lt1/a;->t:Landroid/widget/AdapterView$OnItemClickListener;

    new-instance v0, Lh1/j$a;

    iget-object v1, p0, Lh1/j;->f:Lh1/g;

    invoke-direct {v0, p0, v1}, Lh1/j$a;-><init>(Lh1/j;Lh1/g;)V

    iput-object v0, p0, Lh1/j;->a:Lh1/j$a;

    iget-object v1, p0, Lh1/j;->i:Lt1/a;

    invoke-virtual {v1, v0}, Lt1/a;->d(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lh1/j;->i:Lt1/a;

    iget v1, v0, Lt1/a;->m:I

    neg-int v1, v1

    iput v1, v0, Lt1/a;->o:I

    const/4 v1, 0x1

    iput-boolean v1, v0, Lt1/a;->p:Z

    iget-object v2, p0, Lh1/j;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/lit16 v2, v2, -0x96

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    iput v2, v0, Lt1/a;->q:I

    iput-boolean v1, v0, Lt1/a;->r:Z

    iget-object v0, p0, Lh1/j;->i:Lt1/a;

    iget-object v2, p0, Lh1/j;->b:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lt1/a;->f(Landroid/view/View;Landroid/view/ViewGroup;)V

    iget-object v0, p0, Lh1/j;->i:Lt1/a;

    iget-object v0, v0, Lt1/a;->i:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return v1
.end method

.method public h(Lh1/m;)Z
    .locals 7

    invoke-virtual {p1}, Lh1/g;->hasVisibleItems()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    new-instance v0, Lh1/j;

    iget-object v2, p0, Lh1/j;->c:Landroid/content/Context;

    iget-object v3, p0, Lh1/j;->b:Landroid/view/View;

    invoke-direct {v0, v2, p1, v3, v1}, Lh1/j;-><init>(Landroid/content/Context;Lh1/g;Landroid/view/View;Z)V

    iget-object v2, p0, Lh1/j;->j:Lh1/k$a;

    iput-object v2, v0, Lh1/j;->j:Lh1/k$a;

    invoke-virtual {p1}, Lh1/g;->size()I

    move-result v2

    move v3, v1

    :goto_0
    const/4 v4, 0x1

    if-ge v3, v2, :cond_1

    invoke-virtual {p1, v3}, Lh1/g;->getItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/MenuItem;->isVisible()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    if-eqz v5, :cond_0

    move v1, v4

    goto :goto_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    iput-boolean v1, v0, Lh1/j;->d:Z

    invoke-virtual {v0}, Lh1/j;->g()Z

    iget-object v0, p0, Lh1/j;->j:Lh1/k$a;

    if-eqz v0, :cond_2

    invoke-interface {v0, p1}, Lh1/k$a;->c(Lh1/g;)Z

    :cond_2
    return v4

    :cond_3
    return v1
.end method

.method public isShowing()Z
    .locals 1

    iget-object v0, p0, Lh1/j;->i:Lt1/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public j(Lh1/g;Lh1/i;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onDismiss()V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lh1/j;->i:Lt1/a;

    iget-object v0, p0, Lh1/j;->f:Lh1/g;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lh1/g;->d(Z)V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object p1, p0, Lh1/j;->a:Lh1/j$a;

    iget-object p2, p1, Lh1/j$a;->a:Lh1/g;

    invoke-virtual {p1, p3}, Lh1/j$a;->b(I)Lh1/i;

    move-result-object p1

    const/4 p3, 0x0

    invoke-virtual {p2, p1, p3}, Lh1/g;->p(Landroid/view/MenuItem;I)Z

    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result p1

    const/4 p3, 0x0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/16 p1, 0x52

    if-ne p2, p1, :cond_0

    invoke-virtual {p0, p3}, Lh1/j;->d(Z)V

    return v0

    :cond_0
    return p3
.end method
