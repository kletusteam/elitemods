.class public Lh1/f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lh1/k;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lh1/f$a;
    }
.end annotation


# instance fields
.field public a:Lh1/f$a;

.field public b:Lh1/k$a;

.field public c:Landroid/content/Context;

.field public d:Landroid/view/LayoutInflater;

.field public e:I

.field public f:Lh1/g;

.field public g:I

.field public h:Lmiuix/appcompat/internal/view/menu/ExpandedMenuView;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lh1/f;->e:I

    const p2, 0x7f0d0044

    iput p2, p0, Lh1/f;->g:I

    iput-object p1, p0, Lh1/f;->c:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lh1/f;->d:Landroid/view/LayoutInflater;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p3, p0, Lh1/f;->e:I

    iput p2, p0, Lh1/f;->g:I

    iput-object p1, p0, Lh1/f;->c:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lh1/f;->d:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b(Lh1/g;Z)V
    .locals 1

    iget-object v0, p0, Lh1/f;->b:Lh1/k$a;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lh1/k$a;->b(Lh1/g;Z)V

    :cond_0
    return-void
.end method

.method public c(Z)V
    .locals 0

    iget-object p1, p0, Lh1/f;->a:Lh1/f$a;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lh1/f$a;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public e(Landroid/content/Context;Lh1/g;)V
    .locals 1

    iget-object v0, p0, Lh1/f;->c:Landroid/content/Context;

    if-eqz v0, :cond_0

    iput-object p1, p0, Lh1/f;->c:Landroid/content/Context;

    iget-object v0, p0, Lh1/f;->d:Landroid/view/LayoutInflater;

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lh1/f;->d:Landroid/view/LayoutInflater;

    :cond_0
    iget-object p1, p0, Lh1/f;->f:Lh1/g;

    if-eqz p1, :cond_1

    invoke-virtual {p1, p0}, Lh1/g;->r(Lh1/k;)V

    :cond_1
    iput-object p2, p0, Lh1/f;->f:Lh1/g;

    iget-object p1, p0, Lh1/f;->a:Lh1/f$a;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lh1/f$a;->notifyDataSetChanged()V

    :cond_2
    return-void
.end method

.method public f(Lh1/g;Lh1/i;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public h(Lh1/m;)Z
    .locals 2

    invoke-virtual {p1}, Lh1/g;->hasVisibleItems()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    new-instance v0, Lh1/h;

    invoke-direct {v0, p1}, Lh1/h;-><init>(Lh1/g;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lh1/h;->d(Landroid/os/IBinder;)V

    iget-object v0, p0, Lh1/f;->b:Lh1/k$a;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Lh1/k$a;->c(Lh1/g;)Z

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method public j(Lh1/g;Lh1/i;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object p1, p0, Lh1/f;->f:Lh1/g;

    iget-object p2, p0, Lh1/f;->a:Lh1/f$a;

    invoke-virtual {p2, p3}, Lh1/f$a;->b(I)Lh1/i;

    move-result-object p2

    const/4 p3, 0x0

    invoke-virtual {p1, p2, p3}, Lh1/g;->p(Landroid/view/MenuItem;I)Z

    return-void
.end method
