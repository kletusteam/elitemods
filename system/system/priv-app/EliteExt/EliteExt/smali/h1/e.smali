.class public Lh1/e;
.super Lt1/a;
.source ""

# interfaces
.implements Lh1/d;


# instance fields
.field public v:Ld1/b;

.field public w:Lh1/c;

.field public x:Landroid/view/View;

.field public y:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Ld1/b;Landroid/view/Menu;)V
    .locals 2

    move-object v0, p1

    check-cast v0, Ld1/k;

    iget-object v1, v0, Ld1/b;->d:Ld1/j;

    invoke-direct {p0, v1}, Lt1/a;-><init>(Landroid/content/Context;)V

    iget-object v0, v0, Ld1/b;->d:Ld1/j;

    iput-object p1, p0, Lh1/e;->v:Ld1/b;

    new-instance p1, Lh1/c;

    invoke-direct {p1, v0, p2}, Lh1/c;-><init>(Landroid/content/Context;Landroid/view/Menu;)V

    iput-object p1, p0, Lh1/e;->w:Lh1/c;

    invoke-virtual {p0, p1}, Lt1/a;->d(Landroid/widget/ListAdapter;)V

    new-instance p1, Lh1/e$a;

    invoke-direct {p1, p0}, Lh1/e$a;-><init>(Lh1/e;)V

    iput-object p1, p0, Lt1/a;->t:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method


# virtual methods
.method public f(Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 6

    iput-object p1, p0, Lh1/e;->x:Landroid/view/View;

    iput-object p2, p0, Lh1/e;->y:Landroid/view/ViewGroup;

    if-nez p2, :cond_0

    const-string v0, "ImmersionMenu"

    const-string v1, "ImmersionMenuPopupWindow offset can\'t be adjusted without parent"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_0
    const/4 v0, 0x2

    new-array v1, v0, [I

    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->getLocationInWindow([I)V

    new-array v0, v0, [I

    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    const/4 v2, 0x1

    aget v3, v0, v2

    aget v4, v1, v2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    sub-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x16

    add-int/2addr v3, v5

    neg-int v3, v3

    iput v3, p0, Lt1/a;->q:I

    iput-boolean v2, p0, Lt1/a;->r:Z

    invoke-virtual {p2}, Landroid/view/View;->getLayoutDirection()I

    move-result v3

    const/4 v4, 0x0

    if-ne v3, v2, :cond_1

    move v3, v2

    goto :goto_0

    :cond_1
    move v3, v4

    :goto_0
    if-eqz v3, :cond_2

    iget v0, p0, Lt1/a;->m:I

    goto :goto_1

    :cond_2
    aget v0, v0, v4

    aget v1, v1, v4

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getWidth()I

    move-result v4

    sub-int/2addr v0, v1

    add-int/2addr v0, v3

    sub-int/2addr v4, v0

    iget v0, p0, Lt1/a;->m:I

    sub-int v0, v4, v0

    :goto_1
    iput v0, p0, Lt1/a;->o:I

    iput-boolean v2, p0, Lt1/a;->p:Z

    :goto_2
    invoke-virtual {p0, p1, p2}, Lt1/a;->c(Landroid/view/View;Landroid/view/ViewGroup;)Z

    move-result p2

    if-eqz p2, :cond_3

    invoke-virtual {p0, p1}, Lt1/a;->g(Landroid/view/View;)V

    :cond_3
    return-void
.end method
