.class public Lh1/e$a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lh1/e;-><init>(Ld1/b;Landroid/view/Menu;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lh1/e;


# direct methods
.method public constructor <init>(Lh1/e;)V
    .locals 0

    iput-object p1, p0, Lh1/e$a;->a:Lh1/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object p1, p0, Lh1/e$a;->a:Lh1/e;

    iget-object p1, p1, Lh1/e;->w:Lh1/c;

    iget-object p1, p1, Lh1/c;->a:Ljava/util/ArrayList;

    invoke-virtual {p1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->hasSubMenu()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object p1

    iget-object p2, p0, Lh1/e$a;->a:Lh1/e;

    new-instance p3, Lh1/e$a$a;

    invoke-direct {p3, p0, p1}, Lh1/e$a$a;-><init>(Lh1/e$a;Landroid/view/SubMenu;)V

    iput-object p3, p2, Lt1/a;->s:Landroid/widget/PopupWindow$OnDismissListener;

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lh1/e$a;->a:Lh1/e;

    iget-object p2, p2, Lh1/e;->v:Ld1/b;

    const/4 p3, 0x0

    invoke-virtual {p2, p3, p1}, Ld1/b;->g(ILandroid/view/MenuItem;)Z

    :goto_0
    iget-object p1, p0, Lh1/e$a;->a:Lh1/e;

    invoke-virtual {p1}, Landroid/widget/PopupWindow;->dismiss()V

    return-void
.end method
