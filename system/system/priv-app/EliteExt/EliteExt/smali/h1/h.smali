.class public Lh1/h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Lh1/k$a;


# instance fields
.field public a:Ld1/i;

.field public b:Lh1/g;

.field public c:Lh1/f;

.field public d:Lh1/k$a;


# direct methods
.method public constructor <init>(Lh1/g;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lh1/h;->b:Lh1/g;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lh1/h;->a:Ld1/i;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ld1/i;->dismiss()V

    :cond_0
    return-void
.end method

.method public b(Lh1/g;Z)V
    .locals 1

    if-nez p2, :cond_0

    iget-object v0, p0, Lh1/h;->b:Lh1/g;

    if-ne p1, v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lh1/h;->a()V

    :cond_1
    iget-object v0, p0, Lh1/h;->d:Lh1/k$a;

    if-eqz v0, :cond_2

    invoke-interface {v0, p1, p2}, Lh1/k$a;->b(Lh1/g;Z)V

    :cond_2
    return-void
.end method

.method public c(Lh1/g;)Z
    .locals 1

    iget-object v0, p0, Lh1/h;->d:Lh1/k$a;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lh1/k$a;->c(Lh1/g;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public d(Landroid/os/IBinder;)V
    .locals 5

    iget-object v0, p0, Lh1/h;->b:Lh1/g;

    new-instance v1, Ld1/i$b;

    iget-object v2, v0, Lh1/g;->c:Landroid/content/Context;

    invoke-direct {v1, v2}, Ld1/i$b;-><init>(Landroid/content/Context;)V

    new-instance v2, Lh1/f;

    iget-object v3, v0, Lh1/g;->c:Landroid/content/Context;

    const v4, 0x7f0d004c

    invoke-direct {v2, v3, v4}, Lh1/f;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lh1/h;->c:Lh1/f;

    iput-object p0, v2, Lh1/f;->b:Lh1/k$a;

    iget-object v3, p0, Lh1/h;->b:Lh1/g;

    invoke-virtual {v3, v2}, Lh1/g;->b(Lh1/k;)V

    iget-object v2, p0, Lh1/h;->c:Lh1/f;

    iget-object v3, v2, Lh1/f;->a:Lh1/f$a;

    if-nez v3, :cond_0

    new-instance v3, Lh1/f$a;

    invoke-direct {v3, v2}, Lh1/f$a;-><init>(Lh1/f;)V

    iput-object v3, v2, Lh1/f;->a:Lh1/f$a;

    :cond_0
    iget-object v2, v2, Lh1/f;->a:Lh1/f$a;

    iget-object v3, v1, Ld1/i$b;->a:Ld1/e$b;

    iput-object v2, v3, Ld1/e$b;->a:Landroid/widget/ListAdapter;

    iput-object p0, v3, Ld1/e$b;->i:Landroid/content/DialogInterface$OnClickListener;

    iget-object v2, v0, Lh1/g;->i:Landroid/view/View;

    if-eqz v2, :cond_1

    iput-object v2, v3, Ld1/e$b;->c:Landroid/view/View;

    goto :goto_0

    :cond_1
    iget-object v2, v0, Lh1/g;->g:Landroid/graphics/drawable/Drawable;

    iput-object v2, v3, Ld1/e$b;->d:Landroid/graphics/drawable/Drawable;

    iget-object v0, v0, Lh1/g;->h:Ljava/lang/CharSequence;

    iput-object v0, v3, Ld1/e$b;->m:Ljava/lang/CharSequence;

    :goto_0
    iput-object p0, v3, Ld1/e$b;->j:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v1}, Ld1/i$b;->a()Ld1/i;

    move-result-object v0

    iput-object v0, p0, Lh1/h;->a:Ld1/i;

    invoke-virtual {v0, p0}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    iget-object v0, p0, Lh1/h;->a:Ld1/i;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    const/16 v1, 0x3eb

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    if-eqz p1, :cond_2

    iput-object p1, v0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    :cond_2
    iget p1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v1, 0x20000

    or-int/2addr p1, v1

    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    iget-object p1, p0, Lh1/h;->a:Ld1/i;

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    iget-object p1, p0, Lh1/h;->b:Lh1/g;

    iget-object v0, p0, Lh1/h;->c:Lh1/f;

    iget-object v1, v0, Lh1/f;->a:Lh1/f$a;

    if-nez v1, :cond_0

    new-instance v1, Lh1/f$a;

    invoke-direct {v1, v0}, Lh1/f$a;-><init>(Lh1/f;)V

    iput-object v1, v0, Lh1/f;->a:Lh1/f$a;

    :cond_0
    iget-object v0, v0, Lh1/f;->a:Lh1/f$a;

    invoke-virtual {v0, p2}, Lh1/f$a;->b(I)Lh1/i;

    move-result-object p2

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Lh1/g;->p(Landroid/view/MenuItem;I)Z

    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2

    iget-object p1, p0, Lh1/h;->c:Lh1/f;

    iget-object v0, p0, Lh1/h;->b:Lh1/g;

    iget-object p1, p1, Lh1/f;->b:Lh1/k$a;

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, Lh1/k$a;->b(Lh1/g;Z)V

    :cond_0
    return-void
.end method

.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 2

    const/16 v0, 0x52

    if-eq p2, v0, :cond_0

    const/4 v0, 0x4

    if-ne p2, v0, :cond_2

    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_1

    iget-object p1, p0, Lh1/h;->a:Ld1/i;

    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1, p3, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    return v1

    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_2

    invoke-virtual {p3}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lh1/h;->a:Ld1/i;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0, p3}, Landroid/view/KeyEvent$DispatcherState;->isTracking(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object p2, p0, Lh1/h;->b:Lh1/g;

    invoke-virtual {p2, v1}, Lh1/g;->d(Z)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return v1

    :cond_2
    iget-object p1, p0, Lh1/h;->b:Lh1/g;

    const/4 v0, 0x0

    invoke-virtual {p1, p2, p3, v0}, Lh1/g;->performShortcut(ILandroid/view/KeyEvent;I)Z

    move-result p1

    return p1
.end method
