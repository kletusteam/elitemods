.class public final Lh1/i;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem;


# instance fields
.field public a:Landroid/view/View;

.field public final b:I

.field public c:Landroid/view/MenuItem$OnMenuItemClickListener;

.field public d:I

.field public final e:I

.field public f:Landroid/graphics/drawable/Drawable;

.field public g:I

.field public final h:I

.field public i:Landroid/content/Intent;

.field public j:Z

.field public k:Lh1/g;

.field public l:Landroid/view/ContextMenu$ContextMenuInfo;

.field public final m:I

.field public n:C

.field public o:C

.field public p:I

.field public q:Lh1/m;

.field public r:Ljava/lang/CharSequence;

.field public s:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lh1/g;IIIILjava/lang/CharSequence;I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lh1/i;->g:I

    const/16 v1, 0x10

    iput v1, p0, Lh1/i;->d:I

    iput v0, p0, Lh1/i;->p:I

    iput-boolean v0, p0, Lh1/i;->j:Z

    iput-object p1, p0, Lh1/i;->k:Lh1/g;

    iput p3, p0, Lh1/i;->h:I

    iput p2, p0, Lh1/i;->e:I

    iput p4, p0, Lh1/i;->b:I

    iput p5, p0, Lh1/i;->m:I

    iput-object p6, p0, Lh1/i;->r:Ljava/lang/CharSequence;

    iput p7, p0, Lh1/i;->p:I

    return-void
.end method


# virtual methods
.method public c()Z
    .locals 1

    iget v0, p0, Lh1/i;->d:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public collapseActionView()Z
    .locals 1

    iget v0, p0, Lh1/i;->p:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_1

    iget-object v0, p0, Lh1/i;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lh1/i;->k:Lh1/g;

    invoke-virtual {v0, p0}, Lh1/g;->e(Lh1/i;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public d(Z)V
    .locals 1

    iput-boolean p1, p0, Lh1/i;->j:Z

    iget-object p1, p0, Lh1/i;->k:Lh1/g;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lh1/g;->o(Z)V

    return-void
.end method

.method public e(Z)V
    .locals 3

    iget v0, p0, Lh1/i;->d:I

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    const/4 p1, 0x2

    goto :goto_0

    :cond_0
    move p1, v1

    :goto_0
    and-int/lit8 v2, v0, -0x3

    or-int/2addr p1, v2

    iput p1, p0, Lh1/i;->d:I

    if-eq v0, p1, :cond_1

    iget-object p1, p0, Lh1/i;->k:Lh1/g;

    invoke-virtual {p1, v1}, Lh1/g;->o(Z)V

    :cond_1
    return-void
.end method

.method public expandActionView()Z
    .locals 1

    iget v0, p0, Lh1/i;->p:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    iget-object v0, p0, Lh1/i;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lh1/i;->k:Lh1/g;

    invoke-virtual {v0, p0}, Lh1/g;->g(Lh1/i;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public f(Z)Z
    .locals 3

    iget v0, p0, Lh1/i;->d:I

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    move p1, v1

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    and-int/lit8 v2, v0, -0x9

    or-int/2addr p1, v2

    iput p1, p0, Lh1/i;->d:I

    if-eq v0, p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public getActionProvider()Landroid/view/ActionProvider;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Implementation should use getSupportActionProvider!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getActionView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lh1/i;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAlphabeticShortcut()C
    .locals 1

    iget-char v0, p0, Lh1/i;->n:C

    return v0
.end method

.method public getGroupId()I
    .locals 1

    iget v0, p0, Lh1/i;->e:I

    return v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 2

    iget-object v0, p0, Lh1/i;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    iget v0, p0, Lh1/i;->g:I

    if-eqz v0, :cond_1

    iget-object v1, p0, Lh1/i;->k:Lh1/g;

    iget-object v1, v1, Lh1/g;->s:Landroid/content/res/Resources;

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, p0, Lh1/i;->g:I

    iput-object v0, p0, Lh1/i;->f:Landroid/graphics/drawable/Drawable;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lh1/i;->i:Landroid/content/Intent;

    return-object v0
.end method

.method public getItemId()I
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    iget v0, p0, Lh1/i;->h:I

    return v0
.end method

.method public getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1

    iget-object v0, p0, Lh1/i;->l:Landroid/view/ContextMenu$ContextMenuInfo;

    return-object v0
.end method

.method public getNumericShortcut()C
    .locals 1

    iget-char v0, p0, Lh1/i;->o:C

    return v0
.end method

.method public getOrder()I
    .locals 1

    iget v0, p0, Lh1/i;->b:I

    return v0
.end method

.method public getSubMenu()Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lh1/i;->q:Lh1/m;

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    iget-object v0, p0, Lh1/i;->r:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTitleCondensed()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lh1/i;->s:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lh1/i;->r:Ljava/lang/CharSequence;

    :goto_0
    return-object v0
.end method

.method public hasSubMenu()Z
    .locals 1

    iget-object v0, p0, Lh1/i;->q:Lh1/m;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isActionViewExpanded()Z
    .locals 1

    iget-boolean v0, p0, Lh1/i;->j:Z

    return v0
.end method

.method public isCheckable()Z
    .locals 2

    iget v0, p0, Lh1/i;->d:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public isChecked()Z
    .locals 2

    iget v0, p0, Lh1/i;->d:I

    const/4 v1, 0x2

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isEnabled()Z
    .locals 1

    iget v0, p0, Lh1/i;->d:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isVisible()Z
    .locals 1

    iget v0, p0, Lh1/i;->d:I

    and-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Implementation should use setSupportActionProvider!"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setActionView(I)Landroid/view/MenuItem;
    .locals 3

    iget-object v0, p0, Lh1/i;->k:Lh1/g;

    iget-object v0, v0, Lh1/g;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    new-instance v2, Landroid/widget/LinearLayout;

    invoke-direct {v2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    invoke-virtual {v1, p1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p0, p1}, Lh1/i;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setActionView(Landroid/view/View;)Landroid/view/MenuItem;
    .locals 2

    iput-object p1, p0, Lh1/i;->a:Landroid/view/View;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lh1/i;->h:I

    if-lez v0, :cond_0

    invoke-virtual {p1, v0}, Landroid/view/View;->setId(I)V

    :cond_0
    iget-object p1, p0, Lh1/i;->k:Lh1/g;

    const/4 v0, 0x1

    iput-boolean v0, p1, Lh1/g;->j:Z

    invoke-virtual {p1, v0}, Lh1/g;->o(Z)V

    return-object p0
.end method

.method public setAlphabeticShortcut(C)Landroid/view/MenuItem;
    .locals 1

    iget-char v0, p0, Lh1/i;->n:C

    if-ne v0, p1, :cond_0

    return-object p0

    :cond_0
    invoke-static {p1}, Ljava/lang/Character;->toLowerCase(C)C

    move-result p1

    iput-char p1, p0, Lh1/i;->n:C

    iget-object p1, p0, Lh1/i;->k:Lh1/g;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lh1/g;->o(Z)V

    return-object p0
.end method

.method public setCheckable(Z)Landroid/view/MenuItem;
    .locals 2

    iget v0, p0, Lh1/i;->d:I

    and-int/lit8 v1, v0, -0x2

    or-int/2addr p1, v1

    iput p1, p0, Lh1/i;->d:I

    if-eq v0, p1, :cond_0

    iget-object p1, p0, Lh1/i;->k:Lh1/g;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lh1/g;->o(Z)V

    :cond_0
    return-object p0
.end method

.method public setChecked(Z)Landroid/view/MenuItem;
    .locals 3

    iget v0, p0, Lh1/i;->d:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    iget-object p1, p0, Lh1/i;->k:Lh1/g;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lh1/i;->getGroupId()I

    move-result v0

    iget-object p1, p1, Lh1/g;->m:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lh1/i;

    iget v2, v1, Lh1/i;->e:I

    if-ne v2, v0, :cond_0

    invoke-virtual {v1}, Lh1/i;->c()Z

    move-result v2

    if-nez v2, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Lh1/i;->isCheckable()Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0

    :cond_2
    if-ne v1, p0, :cond_3

    const/4 v2, 0x1

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v1, v2}, Lh1/i;->e(Z)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0, p1}, Lh1/i;->e(Z)V

    :cond_5
    return-object p0
.end method

.method public setEnabled(Z)Landroid/view/MenuItem;
    .locals 1

    if-eqz p1, :cond_0

    iget p1, p0, Lh1/i;->d:I

    or-int/lit8 p1, p1, 0x10

    goto :goto_0

    :cond_0
    iget p1, p0, Lh1/i;->d:I

    and-int/lit8 p1, p1, -0x11

    :goto_0
    iput p1, p0, Lh1/i;->d:I

    iget-object p1, p0, Lh1/i;->k:Lh1/g;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lh1/g;->o(Z)V

    return-object p0
.end method

.method public setIcon(I)Landroid/view/MenuItem;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lh1/i;->f:Landroid/graphics/drawable/Drawable;

    iput p1, p0, Lh1/i;->g:I

    iget-object p1, p0, Lh1/i;->k:Lh1/g;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lh1/g;->o(Z)V

    return-object p0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lh1/i;->g:I

    iput-object p1, p0, Lh1/i;->f:Landroid/graphics/drawable/Drawable;

    iget-object p1, p0, Lh1/i;->k:Lh1/g;

    invoke-virtual {p1, v0}, Lh1/g;->o(Z)V

    return-object p0
.end method

.method public setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;
    .locals 0

    iput-object p1, p0, Lh1/i;->i:Landroid/content/Intent;

    return-object p0
.end method

.method public setNumericShortcut(C)Landroid/view/MenuItem;
    .locals 1

    iget-char v0, p0, Lh1/i;->o:C

    if-ne v0, p1, :cond_0

    return-object p0

    :cond_0
    int-to-char p1, p1

    iput-char p1, p0, Lh1/i;->o:C

    iget-object p1, p0, Lh1/i;->k:Lh1/g;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lh1/g;->o(Z)V

    return-object p0
.end method

.method public setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Implementation should use setSupportOnActionExpandListener!"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    .locals 0

    iput-object p1, p0, Lh1/i;->c:Landroid/view/MenuItem$OnMenuItemClickListener;

    return-object p0
.end method

.method public setShortcut(CC)Landroid/view/MenuItem;
    .locals 0

    int-to-char p1, p1

    iput-char p1, p0, Lh1/i;->o:C

    invoke-static {p2}, Ljava/lang/Character;->toLowerCase(C)C

    move-result p1

    iput-char p1, p0, Lh1/i;->n:C

    iget-object p1, p0, Lh1/i;->k:Lh1/g;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lh1/g;->o(Z)V

    return-object p0
.end method

.method public setShowAsAction(I)V
    .locals 3

    and-int/lit8 v0, p1, 0x3

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    if-eq v0, v1, :cond_1

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    iput p1, p0, Lh1/i;->p:I

    iget-object p1, p0, Lh1/i;->k:Lh1/g;

    iput-boolean v1, p1, Lh1/g;->j:Z

    invoke-virtual {p1, v1}, Lh1/g;->o(Z)V

    return-void
.end method

.method public setShowAsActionFlags(I)Landroid/view/MenuItem;
    .locals 0

    invoke-virtual {p0, p1}, Lh1/i;->setShowAsAction(I)V

    return-object p0
.end method

.method public setTitle(I)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lh1/i;->k:Lh1/g;

    iget-object v0, v0, Lh1/g;->c:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lh1/i;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 2

    iput-object p1, p0, Lh1/i;->r:Ljava/lang/CharSequence;

    iget-object v0, p0, Lh1/i;->k:Lh1/g;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lh1/g;->o(Z)V

    iget-object v0, p0, Lh1/i;->q:Lh1/m;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lh1/g;->x(Ljava/lang/CharSequence;)Lh1/g;

    :cond_0
    return-object p0
.end method

.method public setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    iput-object p1, p0, Lh1/i;->s:Ljava/lang/CharSequence;

    iget-object p1, p0, Lh1/i;->k:Lh1/g;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lh1/g;->o(Z)V

    return-object p0
.end method

.method public setVisible(Z)Landroid/view/MenuItem;
    .locals 1

    invoke-virtual {p0, p1}, Lh1/i;->f(Z)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lh1/i;->k:Lh1/g;

    const/4 v0, 0x1

    iput-boolean v0, p1, Lh1/g;->l:Z

    invoke-virtual {p1, v0}, Lh1/g;->o(Z)V

    :cond_0
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lh1/i;->r:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
