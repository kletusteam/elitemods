.class public Lh1/m;
.super Lh1/g;
.source ""

# interfaces
.implements Landroid/view/SubMenu;


# instance fields
.field public x:Lh1/i;

.field public y:Lh1/g;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lh1/g;Lh1/i;)V
    .locals 0

    invoke-direct {p0, p1}, Lh1/g;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lh1/m;->y:Lh1/g;

    iput-object p3, p0, Lh1/m;->x:Lh1/i;

    return-void
.end method


# virtual methods
.method public clearHeader()V
    .locals 0

    return-void
.end method

.method public e(Lh1/i;)Z
    .locals 1

    iget-object v0, p0, Lh1/m;->y:Lh1/g;

    invoke-virtual {v0, p1}, Lh1/g;->e(Lh1/i;)Z

    move-result p1

    return p1
.end method

.method public f(Lh1/g;Landroid/view/MenuItem;)Z
    .locals 1

    invoke-super {p0, p1, p2}, Lh1/g;->f(Lh1/g;Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lh1/m;->y:Lh1/g;

    invoke-virtual {v0, p1, p2}, Lh1/g;->f(Lh1/g;Landroid/view/MenuItem;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public g(Lh1/i;)Z
    .locals 1

    iget-object v0, p0, Lh1/m;->y:Lh1/g;

    invoke-virtual {v0, p1}, Lh1/g;->g(Lh1/i;)Z

    move-result p1

    return p1
.end method

.method public getItem()Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lh1/m;->x:Lh1/i;

    return-object v0
.end method

.method public k()Lh1/g;
    .locals 1

    iget-object v0, p0, Lh1/m;->y:Lh1/g;

    return-object v0
.end method

.method public m()Z
    .locals 1

    iget-object v0, p0, Lh1/m;->y:Lh1/g;

    invoke-virtual {v0}, Lh1/g;->m()Z

    move-result v0

    return v0
.end method

.method public n()Z
    .locals 1

    iget-object v0, p0, Lh1/m;->y:Lh1/g;

    invoke-virtual {v0}, Lh1/g;->n()Z

    move-result v0

    return v0
.end method

.method public s(Lh1/g$a;)V
    .locals 1

    iget-object v0, p0, Lh1/m;->y:Lh1/g;

    invoke-virtual {v0, p1}, Lh1/g;->s(Lh1/g$a;)V

    return-void
.end method

.method public setHeaderIcon(I)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lh1/g;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p0, p1}, Lh1/g;->u(Landroid/graphics/drawable/Drawable;)Lh1/g;

    return-object p0
.end method

.method public setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 0

    invoke-virtual {p0, p1}, Lh1/g;->u(Landroid/graphics/drawable/Drawable;)Lh1/g;

    return-object p0
.end method

.method public setHeaderTitle(I)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lh1/g;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lh1/g;->x(Ljava/lang/CharSequence;)Lh1/g;

    return-object p0
.end method

.method public setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 0

    invoke-virtual {p0, p1}, Lh1/g;->x(Ljava/lang/CharSequence;)Lh1/g;

    return-object p0
.end method

.method public setHeaderView(Landroid/view/View;)Landroid/view/SubMenu;
    .locals 0

    invoke-virtual {p0, p1}, Lh1/g;->y(Landroid/view/View;)Lh1/g;

    return-object p0
.end method

.method public setIcon(I)Landroid/view/SubMenu;
    .locals 2

    iget-object v0, p0, Lh1/m;->x:Lh1/i;

    const/4 v1, 0x0

    iput-object v1, v0, Lh1/i;->f:Landroid/graphics/drawable/Drawable;

    iput p1, v0, Lh1/i;->g:I

    iget-object p1, v0, Lh1/i;->k:Lh1/g;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lh1/g;->o(Z)V

    return-object p0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 2

    iget-object v0, p0, Lh1/m;->x:Lh1/i;

    const/4 v1, 0x0

    iput v1, v0, Lh1/i;->g:I

    iput-object p1, v0, Lh1/i;->f:Landroid/graphics/drawable/Drawable;

    iget-object p1, v0, Lh1/i;->k:Lh1/g;

    invoke-virtual {p1, v1}, Lh1/g;->o(Z)V

    return-object p0
.end method

.method public setQwertyMode(Z)V
    .locals 1

    iget-object v0, p0, Lh1/m;->y:Lh1/g;

    invoke-virtual {v0, p1}, Lh1/g;->setQwertyMode(Z)V

    return-void
.end method
