.class public Lv0/b;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final d:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lv0/a;",
            ">;"
        }
    .end annotation
.end field

.field public final b:I

.field public final c:Lv0/a;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lv0/b;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lv0/b;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    iput v0, p0, Lv0/b;->b:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lv0/b;->a:Ljava/util/List;

    new-instance v0, Lv0/a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lv0/a;-><init>(Z)V

    iput-object v0, p0, Lv0/b;->c:Lv0/a;

    return-void
.end method


# virtual methods
.method public varargs a(Lv0/a;[Z)V
    .locals 1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lv0/b;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    array-length v0, p2

    if-lez v0, :cond_0

    const/4 v0, 0x0

    aget-boolean p2, p2, v0

    if-eqz p2, :cond_0

    new-instance p2, Lv0/a;

    invoke-direct {p2, v0}, Lv0/a;-><init>(Z)V

    invoke-virtual {p2, p1}, Lv0/a;->b(Lv0/a;)V

    iget-object p1, p0, Lv0/b;->a:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lv0/b;->a:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    return-void
.end method

.method public b(Lv0/a;)V
    .locals 6

    iget-object v0, p0, Lv0/b;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_3

    iget-object v1, p0, Lv0/b;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lv0/a;

    iget-wide v2, p1, Lv0/a;->a:J

    iget-wide v4, v1, Lv0/a;->a:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p1, Lv0/a;->a:J

    iget-object v2, p1, Lv0/a;->b:Lc1/c$a;

    iget-object v3, v1, Lv0/a;->b:Lc1/c$a;

    if-eqz v3, :cond_0

    sget-object v4, Lv0/a;->j:Lc1/c$a;

    if-eq v3, v4, :cond_0

    move-object v2, v3

    :cond_0
    iput-object v2, p1, Lv0/a;->b:Lc1/c$a;

    iget-object v2, p1, Lv0/a;->e:Ljava/util/HashSet;

    iget-object v3, v1, Lv0/a;->e:Ljava/util/HashSet;

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    iget-wide v2, p1, Lv0/a;->c:J

    iget-wide v4, v1, Lv0/a;->c:J

    or-long/2addr v2, v4

    iput-wide v2, p1, Lv0/a;->c:J

    iget v2, p1, Lv0/a;->d:F

    iget v3, v1, Lv0/a;->d:F

    float-to-double v4, v2

    invoke-static {v4, v5}, Landroidx/emoji2/text/l;->G(D)Z

    move-result v4

    if-eqz v4, :cond_1

    move v2, v3

    goto :goto_1

    :cond_1
    float-to-double v4, v3

    invoke-static {v4, v5}, Landroidx/emoji2/text/l;->G(D)Z

    move-result v4

    if-eqz v4, :cond_2

    goto :goto_1

    :cond_2
    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    :goto_1
    iput v2, p1, Lv0/a;->d:F

    iget-wide v2, p1, Lv0/a;->g:J

    iget-wide v4, v1, Lv0/a;->g:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p1, Lv0/a;->g:J

    iget v2, p1, Lv0/a;->i:I

    iget v3, v1, Lv0/a;->i:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, p1, Lv0/a;->i:I

    iget-object v2, p1, Lv0/a;->f:Ljava/util/Map;

    iget-object v1, v1, Lv0/a;->f:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, "AnimConfigLink{id = "

    invoke-static {v0}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lv0/b;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", configList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lv0/b;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
