.class public Lv0/a;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final j:Lc1/c$a;


# instance fields
.field public a:J

.field public b:Lc1/c$a;

.field public c:J

.field public d:F

.field public final e:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ly0/b;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lv0/c;",
            ">;"
        }
    .end annotation
.end field

.field public g:J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public h:Ljava/lang/Object;

.field public i:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    const/4 v1, -0x2

    invoke-static {v1, v0}, Lc1/c;->b(I[F)Lc1/c$a;

    move-result-object v0

    sput-object v0, Lv0/a;->j:Lc1/c$a;

    return-void

    :array_0
    .array-data 4
        0x3f59999a    # 0.85f
        0x3e99999a    # 0.3f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lv0/a;-><init>(Z)V

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lv0/a;->d:F

    const/4 v0, -0x1

    iput v0, p0, Lv0/a;->i:I

    if-nez p1, :cond_0

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lv0/a;->f:Ljava/util/Map;

    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    iput-object p1, p0, Lv0/a;->f:Ljava/util/Map;

    :goto_0
    iput-object p1, p0, Lv0/a;->e:Ljava/util/HashSet;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lv0/a;->a:J

    const/4 v2, 0x0

    iput-object v2, p0, Lv0/a;->b:Lc1/c$a;

    iget-object v3, p0, Lv0/a;->e:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->clear()V

    iput-object v2, p0, Lv0/a;->h:Ljava/lang/Object;

    iput-wide v0, p0, Lv0/a;->c:J

    const v2, 0x7f7fffff    # Float.MAX_VALUE

    iput v2, p0, Lv0/a;->d:F

    iput-wide v0, p0, Lv0/a;->g:J

    const/4 v0, -0x1

    iput v0, p0, Lv0/a;->i:I

    iget-object v0, p0, Lv0/a;->f:Ljava/util/Map;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    :cond_0
    return-void
.end method

.method public b(Lv0/a;)V
    .locals 2

    if-eqz p1, :cond_0

    if-eq p1, p0, :cond_0

    iget-wide v0, p1, Lv0/a;->a:J

    iput-wide v0, p0, Lv0/a;->a:J

    iget-object v0, p1, Lv0/a;->b:Lc1/c$a;

    iput-object v0, p0, Lv0/a;->b:Lc1/c$a;

    iget-object v0, p0, Lv0/a;->e:Ljava/util/HashSet;

    iget-object v1, p1, Lv0/a;->e:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p1, Lv0/a;->h:Ljava/lang/Object;

    iput-object v0, p0, Lv0/a;->h:Ljava/lang/Object;

    iget-wide v0, p1, Lv0/a;->c:J

    iput-wide v0, p0, Lv0/a;->c:J

    iget v0, p1, Lv0/a;->d:F

    iput v0, p0, Lv0/a;->d:F

    iget-wide v0, p1, Lv0/a;->g:J

    iput-wide v0, p0, Lv0/a;->g:J

    iget v0, p1, Lv0/a;->i:I

    iput v0, p0, Lv0/a;->i:I

    iget-object v0, p0, Lv0/a;->f:Ljava/util/Map;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lv0/a;->f:Ljava/util/Map;

    iget-object p1, p1, Lv0/a;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    :cond_0
    return-void
.end method

.method public c(Ljava/lang/String;)Lv0/c;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lv0/a;->e(Ljava/lang/String;Z)Lv0/c;

    move-result-object p1

    return-object p1
.end method

.method public d(Ljava/lang/String;)Lv0/c;
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lv0/a;->e(Ljava/lang/String;Z)Lv0/c;

    move-result-object p1

    return-object p1
.end method

.method public final e(Ljava/lang/String;Z)Lv0/c;
    .locals 1

    iget-object v0, p0, Lv0/a;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lv0/c;

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    new-instance v0, Lv0/c;

    invoke-direct {v0}, Lv0/c;-><init>()V

    iget-object p2, p0, Lv0/a;->f:Ljava/util/Map;

    invoke-interface {p2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method public varargs f(I[F)Lv0/a;
    .locals 0

    invoke-static {p1, p2}, Lc1/c;->b(I[F)Lc1/c$a;

    move-result-object p1

    iput-object p1, p0, Lv0/a;->b:Lc1/c$a;

    return-object p0
.end method

.method public varargs g(La1/b;J[F)Lv0/a;
    .locals 6

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v3, p2

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lv0/a;->h(La1/b;Lc1/c$a;J[F)Lv0/a;

    move-result-object p1

    return-object p1
.end method

.method public varargs h(La1/b;Lc1/c$a;J[F)Lv0/a;
    .locals 6

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x1

    invoke-virtual {p0, p1, p2}, Lv0/a;->e(Ljava/lang/String;Z)Lv0/c;

    move-result-object p1

    :goto_0
    move-object v1, p1

    const/4 v2, 0x0

    move-object v0, p0

    move-wide v3, p3

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lv0/a;->i(Lv0/c;Lc1/c$a;J[F)V

    return-object p0
.end method

.method public varargs i(Lv0/c;Lc1/c$a;J[F)V
    .locals 2

    if-eqz p2, :cond_0

    iput-object p2, p1, Lv0/a;->b:Lc1/c$a;

    :cond_0
    const-wide/16 v0, 0x0

    cmp-long p2, p3, v0

    if-lez p2, :cond_1

    iput-wide p3, p1, Lv0/a;->a:J

    :cond_1
    array-length p2, p5

    if-lez p2, :cond_2

    const/4 p2, 0x0

    aget p2, p5, p2

    iput p2, p1, Lv0/a;->d:F

    :cond_2
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "AnimConfig{delay="

    invoke-static {v0}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lv0/a;->a:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", minDuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lv0/a;->g:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", ease="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lv0/a;->b:Lc1/c$a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", fromSpeed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lv0/a;->d:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", tintMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lv0/a;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", tag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lv0/a;->h:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", flags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lv0/a;->c:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", listeners="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lv0/a;->e:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", specialNameMap = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lv0/a;->f:Ljava/util/Map;

    const-string v2, "    "

    invoke-static {v1, v2}, Lc1/a;->c(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
