.class public Ld/h$d;
.super Lf/h;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ld/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "d"
.end annotation


# instance fields
.field public final synthetic b:Ld/h;


# direct methods
.method public constructor <init>(Ld/h;Landroid/view/Window$Callback;)V
    .locals 0

    iput-object p1, p0, Ld/h$d;->b:Ld/h;

    invoke-direct {p0, p2}, Lf/h;-><init>(Landroid/view/Window$Callback;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .locals 9

    new-instance v0, Lf/e$a;

    iget-object v1, p0, Ld/h$d;->b:Ld/h;

    iget-object v1, v1, Ld/h;->d:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lf/e$a;-><init>(Landroid/content/Context;Landroid/view/ActionMode$Callback;)V

    iget-object p1, p0, Ld/h$d;->b:Ld/h;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p1, Ld/h;->n:Lf/a;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lf/a;->c()V

    :cond_0
    new-instance v1, Ld/h$c;

    invoke-direct {v1, p1, v0}, Ld/h$c;-><init>(Ld/h;Lf/a$a;)V

    invoke-virtual {p1}, Ld/h;->P()V

    iget-object v2, p1, Ld/h;->h:Ld/a;

    if-eqz v2, :cond_1

    invoke-virtual {v2, v1}, Ld/a;->j(Lf/a$a;)Lf/a;

    move-result-object v2

    iput-object v2, p1, Ld/h;->n:Lf/a;

    if-eqz v2, :cond_1

    iget-object v3, p1, Ld/h;->g:Ld/f;

    if-eqz v3, :cond_1

    invoke-interface {v3, v2}, Ld/f;->h(Lf/a;)V

    :cond_1
    iget-object v2, p1, Ld/h;->n:Lf/a;

    const/4 v3, 0x0

    if-nez v2, :cond_10

    invoke-virtual {p1}, Ld/h;->I()V

    iget-object v2, p1, Ld/h;->n:Lf/a;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lf/a;->c()V

    :cond_2
    iget-object v2, p1, Ld/h;->g:Ld/f;

    if-eqz v2, :cond_3

    iget-boolean v4, p1, Ld/h;->J:Z

    if-nez v4, :cond_3

    :try_start_0
    invoke-interface {v2, v1}, Ld/f;->g(Lf/a$a;)Lf/a;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/AbstractMethodError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    :cond_3
    move-object v2, v3

    :goto_0
    if-eqz v2, :cond_4

    iput-object v2, p1, Ld/h;->n:Lf/a;

    goto/16 :goto_6

    :cond_4
    iget-object v2, p1, Ld/h;->o:Landroidx/appcompat/widget/ActionBarContextView;

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-nez v2, :cond_9

    iget-boolean v2, p1, Ld/h;->B:Z

    if-eqz v2, :cond_6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    iget-object v6, p1, Ld/h;->d:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v6

    const v7, 0x7f040037

    invoke-virtual {v6, v7, v2, v5}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v7, v2, Landroid/util/TypedValue;->resourceId:I

    if-eqz v7, :cond_5

    iget-object v7, p1, Ld/h;->d:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    iget v6, v2, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v7, v6, v5}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    new-instance v6, Lf/c;

    iget-object v8, p1, Ld/h;->d:Landroid/content/Context;

    invoke-direct {v6, v8, v4}, Lf/c;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v6}, Lf/c;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v8

    invoke-virtual {v8, v7}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    goto :goto_1

    :cond_5
    iget-object v6, p1, Ld/h;->d:Landroid/content/Context;

    :goto_1
    new-instance v7, Landroidx/appcompat/widget/ActionBarContextView;

    invoke-direct {v7, v6, v3}, Landroidx/appcompat/widget/ActionBarContextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v7, p1, Ld/h;->o:Landroidx/appcompat/widget/ActionBarContextView;

    new-instance v7, Landroid/widget/PopupWindow;

    const v8, 0x7f040050

    invoke-direct {v7, v6, v3, v8}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v7, p1, Ld/h;->p:Landroid/widget/PopupWindow;

    const/4 v8, 0x2

    invoke-static {v7, v8}, Lz/e;->d(Landroid/widget/PopupWindow;I)V

    iget-object v7, p1, Ld/h;->p:Landroid/widget/PopupWindow;

    iget-object v8, p1, Ld/h;->o:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {v7, v8}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    iget-object v7, p1, Ld/h;->p:Landroid/widget/PopupWindow;

    const/4 v8, -0x1

    invoke-virtual {v7, v8}, Landroid/widget/PopupWindow;->setWidth(I)V

    invoke-virtual {v6}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v7

    const v8, 0x7f040025

    invoke-virtual {v7, v8, v2, v5}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v2, v2, Landroid/util/TypedValue;->data:I

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    move-result v2

    iget-object v6, p1, Ld/h;->o:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {v6, v2}, Landroidx/appcompat/widget/ActionBarContextView;->setContentHeight(I)V

    iget-object v2, p1, Ld/h;->p:Landroid/widget/PopupWindow;

    const/4 v6, -0x2

    invoke-virtual {v2, v6}, Landroid/widget/PopupWindow;->setHeight(I)V

    new-instance v2, Ld/k;

    invoke-direct {v2, p1}, Ld/k;-><init>(Ld/h;)V

    iput-object v2, p1, Ld/h;->q:Ljava/lang/Runnable;

    goto :goto_3

    :cond_6
    iget-object v2, p1, Ld/h;->t:Landroid/view/ViewGroup;

    const v6, 0x7f0a003e

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroidx/appcompat/widget/ViewStubCompat;

    if-eqz v2, :cond_9

    invoke-virtual {p1}, Ld/h;->P()V

    iget-object v6, p1, Ld/h;->h:Ld/a;

    if-eqz v6, :cond_7

    invoke-virtual {v6}, Ld/a;->d()Landroid/content/Context;

    move-result-object v6

    goto :goto_2

    :cond_7
    move-object v6, v3

    :goto_2
    if-nez v6, :cond_8

    iget-object v6, p1, Ld/h;->d:Landroid/content/Context;

    :cond_8
    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroidx/appcompat/widget/ViewStubCompat;->setLayoutInflater(Landroid/view/LayoutInflater;)V

    invoke-virtual {v2}, Landroidx/appcompat/widget/ViewStubCompat;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroidx/appcompat/widget/ActionBarContextView;

    iput-object v2, p1, Ld/h;->o:Landroidx/appcompat/widget/ActionBarContextView;

    :cond_9
    :goto_3
    iget-object v2, p1, Ld/h;->o:Landroidx/appcompat/widget/ActionBarContextView;

    if-eqz v2, :cond_e

    invoke-virtual {p1}, Ld/h;->I()V

    iget-object v2, p1, Ld/h;->o:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {v2}, Landroidx/appcompat/widget/ActionBarContextView;->h()V

    new-instance v2, Lf/d;

    iget-object v6, p1, Ld/h;->o:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p1, Ld/h;->o:Landroidx/appcompat/widget/ActionBarContextView;

    iget-object v8, p1, Ld/h;->p:Landroid/widget/PopupWindow;

    if-nez v8, :cond_a

    goto :goto_4

    :cond_a
    move v5, v4

    :goto_4
    invoke-direct {v2, v6, v7, v1, v5}, Lf/d;-><init>(Landroid/content/Context;Landroidx/appcompat/widget/ActionBarContextView;Lf/a$a;Z)V

    iget-object v5, v2, Lf/d;->h:Landroidx/appcompat/view/menu/e;

    invoke-virtual {v1, v2, v5}, Ld/h$c;->a(Lf/a;Landroid/view/Menu;)Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-virtual {v2}, Lf/d;->i()V

    iget-object v1, p1, Ld/h;->o:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {v1, v2}, Landroidx/appcompat/widget/ActionBarContextView;->f(Lf/a;)V

    iput-object v2, p1, Ld/h;->n:Lf/a;

    invoke-virtual {p1}, Ld/h;->V()Z

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    if-eqz v1, :cond_b

    iget-object v1, p1, Ld/h;->o:Landroidx/appcompat/widget/ActionBarContextView;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setAlpha(F)V

    iget-object v1, p1, Ld/h;->o:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-static {v1}, Lw/v;->a(Landroid/view/View;)Lw/x;

    move-result-object v1

    invoke-virtual {v1, v2}, Lw/x;->a(F)Lw/x;

    iput-object v1, p1, Ld/h;->r:Lw/x;

    new-instance v2, Ld/l;

    invoke-direct {v2, p1}, Ld/l;-><init>(Ld/h;)V

    iget-object v4, v1, Lw/x;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    if-eqz v4, :cond_c

    invoke-virtual {v1, v4, v2}, Lw/x;->e(Landroid/view/View;Lw/y;)V

    goto :goto_5

    :cond_b
    iget-object v1, p1, Ld/h;->o:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setAlpha(F)V

    iget-object v1, p1, Ld/h;->o:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {v1, v4}, Landroidx/appcompat/widget/ActionBarContextView;->setVisibility(I)V

    iget-object v1, p1, Ld/h;->o:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v1, v1, Landroid/view/View;

    if-eqz v1, :cond_c

    iget-object v1, p1, Ld/h;->o:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    sget-object v2, Lw/v;->a:Ljava/util/WeakHashMap;

    invoke-static {v1}, Lw/v$g;->c(Landroid/view/View;)V

    :cond_c
    :goto_5
    iget-object v1, p1, Ld/h;->p:Landroid/widget/PopupWindow;

    if-eqz v1, :cond_e

    iget-object v1, p1, Ld/h;->e:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p1, Ld/h;->q:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_6

    :cond_d
    iput-object v3, p1, Ld/h;->n:Lf/a;

    :cond_e
    :goto_6
    iget-object v1, p1, Ld/h;->n:Lf/a;

    if-eqz v1, :cond_f

    iget-object v2, p1, Ld/h;->g:Ld/f;

    if-eqz v2, :cond_f

    invoke-interface {v2, v1}, Ld/f;->h(Lf/a;)V

    :cond_f
    iget-object v1, p1, Ld/h;->n:Lf/a;

    iput-object v1, p1, Ld/h;->n:Lf/a;

    :cond_10
    iget-object p1, p1, Ld/h;->n:Lf/a;

    if-eqz p1, :cond_11

    invoke-virtual {v0, p1}, Lf/e$a;->e(Lf/a;)Landroid/view/ActionMode;

    move-result-object p1

    return-object p1

    :cond_11
    return-object v3
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Ld/h$d;->b:Ld/h;

    invoke-virtual {v0, p1}, Ld/h;->G(Landroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lf/h;->a:Landroid/view/Window$Callback;

    invoke-interface {v0, p1}, Landroid/view/Window$Callback;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z
    .locals 5

    iget-object v0, p0, Lf/h;->a:Landroid/view/Window$Callback;

    invoke-interface {v0, p1}, Landroid/view/Window$Callback;->dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_4

    iget-object v0, p0, Ld/h$d;->b:Ld/h;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    invoke-virtual {v0}, Ld/h;->P()V

    iget-object v4, v0, Ld/h;->h:Ld/a;

    if-eqz v4, :cond_1

    invoke-virtual {v4, v3, p1}, Ld/a;->f(ILandroid/view/KeyEvent;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    move p1, v2

    goto :goto_1

    :cond_1
    iget-object v3, v0, Ld/h;->F:Ld/h$i;

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    invoke-virtual {v0, v3, v4, p1, v2}, Ld/h;->T(Ld/h$i;ILandroid/view/KeyEvent;I)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object p1, v0, Ld/h;->F:Ld/h$i;

    if-eqz p1, :cond_0

    iput-boolean v2, p1, Ld/h$i;->l:Z

    goto :goto_0

    :cond_2
    iget-object v3, v0, Ld/h;->F:Ld/h$i;

    if-nez v3, :cond_3

    invoke-virtual {v0, v1}, Ld/h;->N(I)Ld/h$i;

    move-result-object v3

    invoke-virtual {v0, v3, p1}, Ld/h;->U(Ld/h$i;Landroid/view/KeyEvent;)Z

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    invoke-virtual {v0, v3, v4, p1, v2}, Ld/h;->T(Ld/h$i;ILandroid/view/KeyEvent;I)Z

    move-result p1

    iput-boolean v1, v3, Ld/h$i;->k:Z

    if-eqz p1, :cond_3

    goto :goto_0

    :cond_3
    move p1, v1

    :goto_1
    if-eqz p1, :cond_5

    :cond_4
    move v1, v2

    :cond_5
    return v1
.end method

.method public onContentChanged()V
    .locals 0

    return-void
.end method

.method public onCreatePanelMenu(ILandroid/view/Menu;)Z
    .locals 1

    if-nez p1, :cond_0

    instance-of v0, p2, Landroidx/appcompat/view/menu/e;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    iget-object v0, p0, Lf/h;->a:Landroid/view/Window$Callback;

    invoke-interface {v0, p1, p2}, Landroid/view/Window$Callback;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method public onCreatePanelView(I)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lf/h;->a:Landroid/view/Window$Callback;

    invoke-interface {v0, p1}, Landroid/view/Window$Callback;->onCreatePanelView(I)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public onMenuOpened(ILandroid/view/Menu;)Z
    .locals 2

    iget-object v0, p0, Lf/h;->a:Landroid/view/Window$Callback;

    invoke-interface {v0, p1, p2}, Landroid/view/Window$Callback;->onMenuOpened(ILandroid/view/Menu;)Z

    iget-object p2, p0, Ld/h$d;->b:Ld/h;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v0, 0x6c

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Ld/h;->P()V

    iget-object p1, p2, Ld/h;->h:Ld/a;

    if-eqz p1, :cond_0

    invoke-virtual {p1, v1}, Ld/a;->b(Z)V

    :cond_0
    return v1
.end method

.method public onPanelClosed(ILandroid/view/Menu;)V
    .locals 2

    iget-object v0, p0, Lf/h;->a:Landroid/view/Window$Callback;

    invoke-interface {v0, p1, p2}, Landroid/view/Window$Callback;->onPanelClosed(ILandroid/view/Menu;)V

    iget-object p2, p0, Ld/h$d;->b:Ld/h;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    const/16 v1, 0x6c

    if-ne p1, v1, :cond_0

    invoke-virtual {p2}, Ld/h;->P()V

    iget-object p1, p2, Ld/h;->h:Ld/a;

    if-eqz p1, :cond_1

    invoke-virtual {p1, v0}, Ld/a;->b(Z)V

    goto :goto_0

    :cond_0
    if-nez p1, :cond_1

    invoke-virtual {p2, p1}, Ld/h;->N(I)Ld/h$i;

    move-result-object p1

    iget-boolean v1, p1, Ld/h$i;->m:Z

    if-eqz v1, :cond_1

    invoke-virtual {p2, p1, v0}, Ld/h;->E(Ld/h$i;Z)V

    :cond_1
    :goto_0
    return-void
.end method

.method public onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 3

    instance-of v0, p3, Landroidx/appcompat/view/menu/e;

    if-eqz v0, :cond_0

    move-object v0, p3

    check-cast v0, Landroidx/appcompat/view/menu/e;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x0

    if-nez p1, :cond_1

    if-nez v0, :cond_1

    return v1

    :cond_1
    if-eqz v0, :cond_2

    const/4 v2, 0x1

    iput-boolean v2, v0, Landroidx/appcompat/view/menu/e;->x:Z

    :cond_2
    iget-object v2, p0, Lf/h;->a:Landroid/view/Window$Callback;

    invoke-interface {v2, p1, p2, p3}, Landroid/view/Window$Callback;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result p1

    if-eqz v0, :cond_3

    iput-boolean v1, v0, Landroidx/appcompat/view/menu/e;->x:Z

    :cond_3
    return p1
.end method

.method public onProvideKeyboardShortcuts(Ljava/util/List;Landroid/view/Menu;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/view/KeyboardShortcutGroup;",
            ">;",
            "Landroid/view/Menu;",
            "I)V"
        }
    .end annotation

    iget-object v0, p0, Ld/h$d;->b:Ld/h;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ld/h;->N(I)Ld/h$i;

    move-result-object v0

    iget-object v0, v0, Ld/h$i;->h:Landroidx/appcompat/view/menu/e;

    if-eqz v0, :cond_0

    iget-object p2, p0, Lf/h;->a:Landroid/view/Window$Callback;

    invoke-interface {p2, p1, v0, p3}, Landroid/view/Window$Callback;->onProvideKeyboardShortcuts(Ljava/util/List;Landroid/view/Menu;I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/h;->a:Landroid/view/Window$Callback;

    invoke-interface {v0, p1, p2, p3}, Landroid/view/Window$Callback;->onProvideKeyboardShortcuts(Ljava/util/List;Landroid/view/Menu;I)V

    :goto_0
    return-void
.end method

.method public onWindowStartingActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public onWindowStartingActionMode(Landroid/view/ActionMode$Callback;I)Landroid/view/ActionMode;
    .locals 1

    iget-object v0, p0, Ld/h$d;->b:Ld/h;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_0

    iget-object v0, p0, Lf/h;->a:Landroid/view/Window$Callback;

    invoke-interface {v0, p1, p2}, Landroid/view/Window$Callback;->onWindowStartingActionMode(Landroid/view/ActionMode$Callback;I)Landroid/view/ActionMode;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-virtual {p0, p1}, Ld/h$d;->a(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object p1

    return-object p1
.end method
