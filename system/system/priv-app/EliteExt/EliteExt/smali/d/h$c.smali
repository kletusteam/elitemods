.class public Ld/h$c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lf/a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ld/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "c"
.end annotation


# instance fields
.field public a:Lf/a$a;

.field public final synthetic b:Ld/h;


# direct methods
.method public constructor <init>(Ld/h;Lf/a$a;)V
    .locals 0

    iput-object p1, p0, Ld/h$c;->b:Ld/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Ld/h$c;->a:Lf/a$a;

    return-void
.end method


# virtual methods
.method public a(Lf/a;Landroid/view/Menu;)Z
    .locals 1

    iget-object v0, p0, Ld/h$c;->a:Lf/a$a;

    invoke-interface {v0, p1, p2}, Lf/a$a;->a(Lf/a;Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method public b(Lf/a;)V
    .locals 2

    iget-object v0, p0, Ld/h$c;->a:Lf/a$a;

    invoke-interface {v0, p1}, Lf/a$a;->b(Lf/a;)V

    iget-object p1, p0, Ld/h$c;->b:Ld/h;

    iget-object v0, p1, Ld/h;->p:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object p1, p1, Ld/h;->e:Landroid/view/Window;

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    iget-object v0, p0, Ld/h$c;->b:Ld/h;

    iget-object v0, v0, Ld/h;->q:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_0
    iget-object p1, p0, Ld/h$c;->b:Ld/h;

    iget-object v0, p1, Ld/h;->o:Landroidx/appcompat/widget/ActionBarContextView;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ld/h;->I()V

    iget-object p1, p0, Ld/h$c;->b:Ld/h;

    iget-object v0, p1, Ld/h;->o:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-static {v0}, Lw/v;->a(Landroid/view/View;)Lw/x;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lw/x;->a(F)Lw/x;

    iput-object v0, p1, Ld/h;->r:Lw/x;

    iget-object p1, p0, Ld/h$c;->b:Ld/h;

    iget-object p1, p1, Ld/h;->r:Lw/x;

    new-instance v0, Ld/h$c$a;

    invoke-direct {v0, p0}, Ld/h$c$a;-><init>(Ld/h$c;)V

    iget-object v1, p1, Lw/x;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v1, :cond_1

    invoke-virtual {p1, v1, v0}, Lw/x;->e(Landroid/view/View;Lw/y;)V

    :cond_1
    iget-object p1, p0, Ld/h$c;->b:Ld/h;

    iget-object v0, p1, Ld/h;->g:Ld/f;

    if-eqz v0, :cond_2

    iget-object p1, p1, Ld/h;->n:Lf/a;

    invoke-interface {v0, p1}, Ld/f;->f(Lf/a;)V

    :cond_2
    iget-object p1, p0, Ld/h$c;->b:Ld/h;

    const/4 v0, 0x0

    iput-object v0, p1, Ld/h;->n:Lf/a;

    iget-object p1, p1, Ld/h;->t:Landroid/view/ViewGroup;

    sget-object v0, Lw/v;->a:Ljava/util/WeakHashMap;

    invoke-static {p1}, Lw/v$g;->c(Landroid/view/View;)V

    return-void
.end method

.method public c(Lf/a;Landroid/view/MenuItem;)Z
    .locals 1

    iget-object v0, p0, Ld/h$c;->a:Lf/a$a;

    invoke-interface {v0, p1, p2}, Lf/a$a;->c(Lf/a;Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public d(Lf/a;Landroid/view/Menu;)Z
    .locals 2

    iget-object v0, p0, Ld/h$c;->b:Ld/h;

    iget-object v0, v0, Ld/h;->t:Landroid/view/ViewGroup;

    sget-object v1, Lw/v;->a:Ljava/util/WeakHashMap;

    invoke-static {v0}, Lw/v$g;->c(Landroid/view/View;)V

    iget-object v0, p0, Ld/h$c;->a:Lf/a$a;

    invoke-interface {v0, p1, p2}, Lf/a$a;->d(Lf/a;Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method
