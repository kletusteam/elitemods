.class public Ld/l;
.super Landroidx/emoji2/text/l;
.source ""


# instance fields
.field public final synthetic v0:Ld/h;


# direct methods
.method public constructor <init>(Ld/h;)V
    .locals 0

    iput-object p1, p0, Ld/l;->v0:Ld/h;

    invoke-direct {p0}, Landroidx/emoji2/text/l;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Ld/l;->v0:Ld/h;

    iget-object p1, p1, Ld/h;->o:Landroidx/appcompat/widget/ActionBarContextView;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setAlpha(F)V

    iget-object p1, p0, Ld/l;->v0:Ld/h;

    iget-object p1, p1, Ld/h;->r:Lw/x;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lw/x;->d(Lw/y;)Lw/x;

    iget-object p1, p0, Ld/l;->v0:Ld/h;

    iput-object v0, p1, Ld/h;->r:Lw/x;

    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Ld/l;->v0:Ld/h;

    iget-object p1, p1, Ld/h;->o:Landroidx/appcompat/widget/ActionBarContextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/ActionBarContextView;->setVisibility(I)V

    iget-object p1, p0, Ld/l;->v0:Ld/h;

    iget-object p1, p1, Ld/h;->o:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    instance-of p1, p1, Landroid/view/View;

    if-eqz p1, :cond_0

    iget-object p1, p0, Ld/l;->v0:Ld/h;

    iget-object p1, p1, Ld/h;->o:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    sget-object v0, Lw/v;->a:Ljava/util/WeakHashMap;

    invoke-static {p1}, Lw/v$g;->c(Landroid/view/View;)V

    :cond_0
    return-void
.end method
