.class public Le2/c;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:D

.field public b:D

.field public c:J

.field public d:D

.field public e:D

.field public f:Z

.field public g:I

.field public h:Z

.field public i:I

.field public j:D

.field public k:D

.field public l:D

.field public m:Le2/b;

.field public n:J

.field public o:D

.field public p:D

.field public q:D


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Le2/c;->f:Z

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 14

    iget-object v0, p0, Le2/c;->m:Le2/b;

    const/4 v1, 0x0

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Le2/c;->f:Z

    if-eqz v0, :cond_0

    goto/16 :goto_3

    :cond_0
    iget v0, p0, Le2/c;->g:I

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    iget v3, p0, Le2/c;->i:I

    if-ne v3, v2, :cond_1

    int-to-double v3, v0

    iput-wide v3, p0, Le2/c;->a:D

    iput-wide v3, p0, Le2/c;->o:D

    goto :goto_0

    :cond_1
    int-to-double v3, v0

    iput-wide v3, p0, Le2/c;->b:D

    iput-wide v3, p0, Le2/c;->p:D

    :goto_0
    iput v1, p0, Le2/c;->g:I

    return v2

    :cond_2
    iget-boolean v0, p0, Le2/c;->h:Z

    if-eqz v0, :cond_3

    iput-boolean v2, p0, Le2/c;->f:Z

    return v2

    :cond_3
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Le2/c;->c:J

    iget-wide v3, p0, Le2/c;->n:J

    sub-long/2addr v0, v3

    long-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    const v1, 0x3c83126f    # 0.016f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const/4 v3, 0x0

    cmpl-float v3, v0, v3

    if-nez v3, :cond_4

    goto :goto_1

    :cond_4
    move v1, v0

    :goto_1
    iget-wide v3, p0, Le2/c;->c:J

    iput-wide v3, p0, Le2/c;->n:J

    iget v0, p0, Le2/c;->i:I

    const/4 v3, 0x2

    iget-object v4, p0, Le2/c;->m:Le2/b;

    iget-wide v5, p0, Le2/c;->q:D

    if-ne v0, v3, :cond_6

    iget-wide v8, p0, Le2/c;->e:D

    iget-wide v10, p0, Le2/c;->p:D

    move v7, v1

    invoke-virtual/range {v4 .. v11}, Le2/b;->a(DFDD)D

    move-result-wide v3

    iget-wide v5, p0, Le2/c;->p:D

    float-to-double v0, v1

    mul-double/2addr v0, v3

    add-double v8, v0, v5

    iput-wide v8, p0, Le2/c;->b:D

    iput-wide v3, p0, Le2/c;->q:D

    iget-wide v10, p0, Le2/c;->k:D

    iget-wide v12, p0, Le2/c;->e:D

    move-object v7, p0

    invoke-virtual/range {v7 .. v13}, Le2/c;->c(DDD)Z

    move-result v0

    if-eqz v0, :cond_5

    iput-boolean v2, p0, Le2/c;->h:Z

    iget-wide v0, p0, Le2/c;->e:D

    iput-wide v0, p0, Le2/c;->b:D

    goto :goto_2

    :cond_5
    iget-wide v0, p0, Le2/c;->b:D

    iput-wide v0, p0, Le2/c;->p:D

    goto :goto_2

    :cond_6
    iget-wide v8, p0, Le2/c;->d:D

    iget-wide v10, p0, Le2/c;->o:D

    move v7, v1

    invoke-virtual/range {v4 .. v11}, Le2/b;->a(DFDD)D

    move-result-wide v3

    iget-wide v5, p0, Le2/c;->o:D

    float-to-double v0, v1

    mul-double/2addr v0, v3

    add-double v8, v0, v5

    iput-wide v8, p0, Le2/c;->a:D

    iput-wide v3, p0, Le2/c;->q:D

    iget-wide v10, p0, Le2/c;->j:D

    iget-wide v12, p0, Le2/c;->d:D

    move-object v7, p0

    invoke-virtual/range {v7 .. v13}, Le2/c;->c(DDD)Z

    move-result v0

    if-eqz v0, :cond_7

    iput-boolean v2, p0, Le2/c;->h:Z

    iget-wide v0, p0, Le2/c;->d:D

    iput-wide v0, p0, Le2/c;->a:D

    goto :goto_2

    :cond_7
    iget-wide v0, p0, Le2/c;->a:D

    iput-wide v0, p0, Le2/c;->o:D

    :goto_2
    return v2

    :cond_8
    :goto_3
    return v1
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Le2/c;->f:Z

    const/4 v0, 0x0

    iput v0, p0, Le2/c;->g:I

    return-void
.end method

.method public c(DDD)Z
    .locals 4

    cmpg-double v0, p3, p5

    const/4 v1, 0x1

    if-gez v0, :cond_0

    cmpl-double v0, p1, p5

    if-lez v0, :cond_0

    return v1

    :cond_0
    cmpl-double p3, p3, p5

    if-lez p3, :cond_1

    cmpg-double p4, p1, p5

    if-gez p4, :cond_1

    return v1

    :cond_1
    if-nez p3, :cond_2

    iget-wide p3, p0, Le2/c;->l:D

    invoke-static {p3, p4}, Ljava/lang/Math;->signum(D)D

    move-result-wide p3

    invoke-static {p1, p2}, Ljava/lang/Math;->signum(D)D

    move-result-wide v2

    cmpl-double p3, p3, v2

    if-eqz p3, :cond_2

    return v1

    :cond_2
    sub-double/2addr p1, p5

    invoke-static {p1, p2}, Ljava/lang/Math;->abs(D)D

    move-result-wide p1

    const-wide/high16 p3, 0x3ff0000000000000L    # 1.0

    cmpg-double p1, p1, p3

    if-gez p1, :cond_3

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    :goto_0
    return v1
.end method
