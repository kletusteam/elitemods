.class public Lc0/a$a;
.super Lx/c;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lc0/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field public final synthetic b:Lc0/a;


# direct methods
.method public constructor <init>(Lc0/a;)V
    .locals 0

    iput-object p1, p0, Lc0/a$a;->b:Lc0/a;

    invoke-direct {p0}, Lx/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Lx/b;
    .locals 1

    iget-object v0, p0, Lc0/a$a;->b:Lc0/a;

    invoke-virtual {v0, p1}, Lc0/a;->l(I)Lx/b;

    move-result-object p1

    iget-object p1, p1, Lx/b;->a:Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-static {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->obtain(Landroid/view/accessibility/AccessibilityNodeInfo;)Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object p1

    new-instance v0, Lx/b;

    invoke-direct {v0, p1}, Lx/b;-><init>(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    return-object v0
.end method

.method public b(I)Lx/b;
    .locals 1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lc0/a$a;->b:Lc0/a;

    iget p1, p1, Lc0/a;->k:I

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lc0/a$a;->b:Lc0/a;

    iget p1, p1, Lc0/a;->l:I

    :goto_0
    const/high16 v0, -0x80000000

    if-ne p1, v0, :cond_1

    const/4 p1, 0x0

    return-object p1

    :cond_1
    iget-object v0, p0, Lc0/a$a;->b:Lc0/a;

    invoke-virtual {v0, p1}, Lc0/a;->l(I)Lx/b;

    move-result-object p1

    iget-object p1, p1, Lx/b;->a:Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-static {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->obtain(Landroid/view/accessibility/AccessibilityNodeInfo;)Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object p1

    new-instance v0, Lx/b;

    invoke-direct {v0, p1}, Lx/b;-><init>(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    return-object v0
.end method

.method public c(IILandroid/os/Bundle;)Z
    .locals 5

    iget-object v0, p0, Lc0/a$a;->b:Lc0/a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, -0x1

    if-eq p1, v1, :cond_c

    const/16 p3, 0x8

    const/high16 v1, -0x80000000

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eq p2, v3, :cond_8

    const/4 v4, 0x2

    if-eq p2, v4, :cond_6

    const/16 p3, 0x40

    if-eq p2, p3, :cond_3

    const/16 p3, 0x80

    if-eq p2, p3, :cond_2

    check-cast v0, Lmiuix/androidbasewidget/widget/ClearableEditText$a;

    if-ne p1, v1, :cond_0

    goto/16 :goto_1

    :cond_0
    const/16 p1, 0x10

    if-eq p2, p1, :cond_1

    goto/16 :goto_1

    :cond_1
    iget-object p1, v0, Lmiuix/androidbasewidget/widget/ClearableEditText$a;->q:Lmiuix/androidbasewidget/widget/ClearableEditText;

    sget-object p2, Lmiuix/androidbasewidget/widget/ClearableEditText;->n:[I

    const-string p2, ""

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    sget p2, Lmiuix/view/c;->n:I

    invoke-static {p1, p2}, Lmiuix/view/HapticCompat;->performHapticFeedback(Landroid/view/View;I)Z

    goto :goto_0

    :cond_2
    invoke-virtual {v0, p1}, Lc0/a;->j(I)Z

    move-result v2

    goto :goto_1

    :cond_3
    iget-object p2, v0, Lc0/a;->h:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result p2

    if-eqz p2, :cond_d

    iget-object p2, v0, Lc0/a;->h:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result p2

    if-nez p2, :cond_4

    goto :goto_1

    :cond_4
    iget p2, v0, Lc0/a;->k:I

    if-eq p2, p1, :cond_d

    if-eq p2, v1, :cond_5

    invoke-virtual {v0, p2}, Lc0/a;->j(I)Z

    :cond_5
    iput p1, v0, Lc0/a;->k:I

    iget-object p2, v0, Lc0/a;->i:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->invalidate()V

    const p2, 0x8000

    invoke-virtual {v0, p1, p2}, Lc0/a;->n(II)Z

    goto :goto_0

    :cond_6
    iget p2, v0, Lc0/a;->l:I

    if-eq p2, p1, :cond_7

    goto :goto_1

    :cond_7
    iput v1, v0, Lc0/a;->l:I

    invoke-virtual {v0, p1, p3}, Lc0/a;->n(II)Z

    goto :goto_0

    :cond_8
    iget-object p2, v0, Lc0/a;->i:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->isFocused()Z

    move-result p2

    if-nez p2, :cond_9

    iget-object p2, v0, Lc0/a;->i:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->requestFocus()Z

    move-result p2

    if-nez p2, :cond_9

    goto :goto_1

    :cond_9
    iget p2, v0, Lc0/a;->l:I

    if-ne p2, p1, :cond_a

    goto :goto_1

    :cond_a
    if-eq p2, v1, :cond_b

    iput v1, v0, Lc0/a;->l:I

    invoke-virtual {v0, p2, p3}, Lc0/a;->n(II)Z

    :cond_b
    iput p1, v0, Lc0/a;->l:I

    invoke-virtual {v0, p1, p3}, Lc0/a;->n(II)Z

    :goto_0
    move v2, v3

    goto :goto_1

    :cond_c
    iget-object p1, v0, Lc0/a;->i:Landroid/view/View;

    sget-object v0, Lw/v;->a:Ljava/util/WeakHashMap;

    invoke-static {p1, p2, p3}, Lw/v$d;->j(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v2

    :cond_d
    :goto_1
    return v2
.end method
