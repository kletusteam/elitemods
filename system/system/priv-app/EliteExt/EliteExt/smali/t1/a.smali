.class public Lt1/a;
.super Landroid/widget/PopupWindow;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lt1/a$c;
    }
.end annotation


# instance fields
.field public a:Landroid/widget/ListAdapter;

.field public final b:Landroid/graphics/Rect;

.field public c:Lt1/a$c;

.field public d:Landroid/view/View;

.field public e:Landroid/content/Context;

.field public f:I

.field public g:I

.field public h:Z

.field public i:Landroid/widget/ListView;

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field public n:Landroid/database/DataSetObserver;

.field public o:I

.field public p:Z

.field public q:I

.field public r:Z

.field public s:Landroid/widget/PopupWindow$OnDismissListener;

.field public t:Landroid/widget/AdapterView$OnItemClickListener;

.field public u:Landroid/widget/FrameLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0, p1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    const v0, 0x800035

    iput v0, p0, Lt1/a;->f:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lt1/a;->h:Z

    new-instance v1, Lt1/a$a;

    invoke-direct {v1, p0}, Lt1/a$a;-><init>(Lt1/a;)V

    iput-object v1, p0, Lt1/a;->n:Landroid/database/DataSetObserver;

    iput-object p1, p0, Lt1/a;->e:Landroid/content/Context;

    const/4 v1, -0x2

    invoke-virtual {p0, v1}, Landroid/widget/PopupWindow;->setHeight(I)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070153

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lt1/a;->k:I

    const v2, 0x7f070154

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lt1/a;->l:I

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x41000000    # 8.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lt1/a;->o:I

    iput v1, p0, Lt1/a;->q:I

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lt1/a;->b:Landroid/graphics/Rect;

    new-instance v1, Lt1/a$c;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lt1/a$c;-><init>(Lt1/a;Lt1/a$a;)V

    iput-object v1, p0, Lt1/a;->c:Lt1/a$c;

    invoke-virtual {p0, v0}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    new-instance v1, Lt1/d;

    invoke-direct {v1, p1}, Lt1/d;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lt1/a;->u:Landroid/widget/FrameLayout;

    new-instance v2, Ld1/m;

    invoke-direct {v2, p0, v0}, Ld1/m;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, p1}, Lt1/a;->b(Landroid/content/Context;)V

    const v0, 0x7f13000e

    invoke-virtual {p0, v0}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    iget-object v0, p0, Lt1/a;->e:Landroid/content/Context;

    sget-object v1, Lr1/a;->a:Landroid/util/TypedValue;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0401b0

    invoke-static {v0, v2}, Lr1/a;->b(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lt1/a;->g:I

    new-instance v0, Lt1/f;

    invoke-direct {v0, p0}, Lt1/f;-><init>(Lt1/a;)V

    invoke-super {p0, v0}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x7f0700db

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lt1/a;->m:I

    return-void
.end method


# virtual methods
.method public a()I
    .locals 15

    iget-object v0, p0, Lt1/a;->c:Lt1/a$c;

    iget-boolean v0, v0, Lt1/a$c;->a:Z

    if-nez v0, :cond_7

    iget-object v0, p0, Lt1/a;->a:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lt1/a;->e:Landroid/content/Context;

    iget v2, p0, Lt1/a;->k:I

    const/4 v3, 0x0

    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v6

    const/4 v7, 0x0

    move v8, v3

    move v9, v8

    move v10, v9

    move-object v11, v7

    move-object v12, v11

    :goto_0
    const/4 v13, 0x1

    if-ge v3, v6, :cond_5

    invoke-interface {v0, v3}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v14

    if-eq v14, v8, :cond_0

    move-object v12, v7

    move v8, v14

    :cond_0
    if-nez v11, :cond_1

    new-instance v11, Landroid/widget/FrameLayout;

    invoke-direct {v11, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    :cond_1
    invoke-interface {v0, v3, v12, v11}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v12

    invoke-virtual {v12, v4, v5}, Landroid/view/View;->measure(II)V

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v14

    add-int/2addr v10, v14

    iget-object v14, p0, Lt1/a;->c:Lt1/a$c;

    iget-boolean v14, v14, Lt1/a$c;->a:Z

    if-eqz v14, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {v12}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    if-lt v14, v2, :cond_3

    iget-object v14, p0, Lt1/a;->c:Lt1/a$c;

    iput v2, v14, Lt1/a$c;->c:I

    iput-boolean v13, v14, Lt1/a$c;->a:Z

    goto :goto_1

    :cond_3
    if-le v14, v9, :cond_4

    move v9, v14

    :cond_4
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lt1/a;->c:Lt1/a$c;

    iget-boolean v1, v0, Lt1/a$c;->a:Z

    if-nez v1, :cond_6

    iput v9, v0, Lt1/a$c;->c:I

    iput-boolean v13, v0, Lt1/a$c;->a:Z

    :cond_6
    iput v10, v0, Lt1/a$c;->b:I

    :cond_7
    iget-object v0, p0, Lt1/a;->c:Lt1/a$c;

    iget v0, v0, Lt1/a$c;->c:I

    iget v1, p0, Lt1/a;->l:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-object v1, p0, Lt1/a;->b:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v2

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    return v0
.end method

.method public b(Landroid/content/Context;)V
    .locals 1

    iget-object p1, p0, Lt1/a;->e:Landroid/content/Context;

    const v0, 0x7f040145

    invoke-static {p1, v0}, Lr1/a;->d(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lt1/a;->b:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    iget-object v0, p0, Lt1/a;->u:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    new-instance p1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v0, 0x0

    invoke-direct {p1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, p1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lt1/a;->u:Landroid/widget/FrameLayout;

    invoke-super {p0, p1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method public c(Landroid/view/View;Landroid/view/ViewGroup;)Z
    .locals 3

    const/4 p2, 0x0

    if-nez p1, :cond_0

    const-string p1, "show: anchor is null"

    goto/16 :goto_0

    :cond_0
    iget-object v0, p0, Lt1/a;->d:Landroid/view/View;

    if-nez v0, :cond_1

    iget-object v0, p0, Lt1/a;->e:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d004e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lt1/a;->d:Landroid/view/View;

    new-instance v1, Lt1/a$b;

    invoke-direct {v1, p0}, Lt1/a$b;-><init>(Lt1/a;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    :cond_1
    iget-object v0, p0, Lt1/a;->u:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lt1/a;->u:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p2}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Lt1/a;->d:Landroid/view/View;

    if-eq v0, v2, :cond_3

    :cond_2
    iget-object v0, p0, Lt1/a;->u:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    iget-object v0, p0, Lt1/a;->u:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lt1/a;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lt1/a;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    const/4 v2, -0x2

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    const/16 v2, 0x10

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    :cond_3
    iget-boolean v0, p0, Lt1/a;->h:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lt1/a;->u:Landroid/widget/FrameLayout;

    iget v2, p0, Lt1/a;->g:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setElevation(F)V

    iget v0, p0, Lt1/a;->g:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Landroid/widget/PopupWindow;->setElevation(F)V

    iget-object v0, p0, Lt1/a;->u:Landroid/widget/FrameLayout;

    new-instance v2, Lt1/b;

    invoke-direct {v2}, Lt1/b;-><init>()V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    :cond_4
    iget-object v0, p0, Lt1/a;->d:Landroid/view/View;

    const v2, 0x102000a

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lt1/a;->i:Landroid/widget/ListView;

    if-nez v0, :cond_5

    const-string p1, "list not found"

    :goto_0
    const-string v0, "ListPopupWindow"

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return p2

    :cond_5
    new-instance v2, Lt1/e;

    invoke-direct {v2, p0}, Lt1/e;-><init>(Lt1/a;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lt1/a;->i:Landroid/widget/ListView;

    iget-object v2, p0, Lt1/a;->a:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lt1/a;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/widget/PopupWindow;->setWidth(I)V

    invoke-static {}, Lr1/b;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    iget v0, p0, Lt1/a;->j:I

    if-lez v0, :cond_6

    iget-object v2, p0, Lt1/a;->c:Lt1/a$c;

    iget v2, v2, Lt1/a$c;->b:I

    if-le v2, v0, :cond_6

    invoke-virtual {p0, v0}, Landroid/widget/PopupWindow;->setHeight(I)V

    :cond_6
    iget-object v0, p0, Lt1/a;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object p1

    invoke-virtual {v0, p1, p2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    return v1
.end method

.method public d(Landroid/widget/ListAdapter;)V
    .locals 2

    iget-object v0, p0, Lt1/a;->a:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lt1/a;->n:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_0
    iput-object p1, p0, Lt1/a;->a:Landroid/widget/ListAdapter;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lt1/a;->n:Landroid/database/DataSetObserver;

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_1
    return-void
.end method

.method public e(Landroid/view/View;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method public f(Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 0

    const/4 p2, 0x0

    invoke-virtual {p0, p1, p2}, Lt1/a;->c(Landroid/view/View;Landroid/view/ViewGroup;)Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-virtual {p0, p1}, Lt1/a;->g(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public final g(Landroid/view/View;)V
    .locals 8

    iget-boolean v0, p0, Lt1/a;->r:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lt1/a;->q:I

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    neg-int v0, v0

    iget-object v1, p0, Lt1/a;->b:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    iget v1, p0, Lt1/a;->q:I

    add-int/2addr v0, v1

    :goto_0
    const/4 v1, 0x2

    new-array v2, v1, [I

    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationInWindow([I)V

    const/4 v3, 0x1

    aget v2, v2, v3

    int-to-float v2, v2

    iget-object v4, p0, Lt1/a;->e:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v5, p0, Lt1/a;->j:I

    if-lez v5, :cond_1

    iget-object v6, p0, Lt1/a;->c:Lt1/a$c;

    iget v6, v6, Lt1/a$c;->b:I

    invoke-static {v6, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    goto :goto_1

    :cond_1
    iget-object v5, p0, Lt1/a;->c:Lt1/a$c;

    iget v5, v5, Lt1/a$c;->b:I

    :goto_1
    const/4 v6, 0x0

    if-ge v5, v4, :cond_3

    int-to-float v7, v0

    add-float/2addr v2, v7

    int-to-float v7, v5

    add-float/2addr v2, v7

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v7

    int-to-float v7, v7

    add-float/2addr v2, v7

    int-to-float v4, v4

    cmpl-float v2, v2, v4

    if-lez v2, :cond_3

    iget-boolean v2, p0, Lt1/a;->r:Z

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    goto :goto_2

    :cond_2
    move v2, v6

    :goto_2
    add-int/2addr v2, v5

    sub-int/2addr v0, v2

    :cond_3
    new-array v2, v1, [I

    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationInWindow([I)V

    invoke-static {p1}, Lh/w0;->b(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_4

    aget v4, v2, v6

    iget v5, p0, Lt1/a;->o:I

    sub-int/2addr v4, v5

    invoke-virtual {p0}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v5

    add-int/2addr v5, v4

    iget v4, p0, Lt1/a;->m:I

    add-int/2addr v5, v4

    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    if-le v5, v4, :cond_5

    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v5

    sub-int/2addr v4, v5

    iget v5, p0, Lt1/a;->m:I

    sub-int/2addr v4, v5

    aget v2, v2, v6

    goto :goto_3

    :cond_4
    aget v4, v2, v6

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v5, v4

    iget v4, p0, Lt1/a;->o:I

    add-int/2addr v5, v4

    invoke-virtual {p0}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v4

    sub-int/2addr v5, v4

    iget v4, p0, Lt1/a;->m:I

    sub-int/2addr v5, v4

    if-gez v5, :cond_5

    invoke-virtual {p0}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v4

    iget v5, p0, Lt1/a;->m:I

    add-int/2addr v4, v5

    aget v2, v2, v6

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v2, v5

    :goto_3
    sub-int/2addr v4, v2

    goto :goto_4

    :cond_5
    move v3, v6

    move v4, v3

    :goto_4
    if-nez v3, :cond_9

    iget-boolean v2, p0, Lt1/a;->p:Z

    if-eqz v2, :cond_6

    iget v6, p0, Lt1/a;->o:I

    :cond_6
    if-eqz v6, :cond_8

    if-nez v2, :cond_8

    invoke-static {p1}, Lh/w0;->b(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lt1/a;->b:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget v3, p0, Lt1/a;->o:I

    sub-int/2addr v2, v3

    sub-int v4, v6, v2

    goto :goto_5

    :cond_7
    iget-object v2, p0, Lt1/a;->b:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget v3, p0, Lt1/a;->o:I

    sub-int/2addr v2, v3

    add-int v4, v2, v6

    goto :goto_5

    :cond_8
    move v4, v6

    :cond_9
    :goto_5
    iget v2, p0, Lt1/a;->f:I

    invoke-virtual {p0, p1, v4, v0, v2}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;III)V

    sget v0, Lmiuix/view/c;->k:I

    invoke-static {p1, v0}, Lmiuix/view/HapticCompat;->performHapticFeedback(Landroid/view/View;I)Z

    iget-object p1, p0, Lt1/a;->u:Landroid/widget/FrameLayout;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getRootView()Landroid/view/View;

    move-result-object p1

    if-nez p1, :cond_a

    goto :goto_6

    :cond_a
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager$LayoutParams;

    if-nez v0, :cond_b

    goto :goto_6

    :cond_b
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const v1, 0x3e99999a    # 0.3f

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    invoke-interface {v1, p1, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :goto_6
    return-void
.end method

.method public setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V
    .locals 0

    iput-object p1, p0, Lt1/a;->s:Landroid/widget/PopupWindow$OnDismissListener;

    return-void
.end method
