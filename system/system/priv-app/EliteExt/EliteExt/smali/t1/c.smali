.class public Lt1/c;
.super Landroid/widget/BaseAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lt1/c$b;
    }
.end annotation


# instance fields
.field public a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/view/MenuItem;",
            ">;"
        }
    .end annotation
.end field

.field public b:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/Menu;)V
    .locals 0

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lt1/c;->b:Landroid/view/LayoutInflater;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lt1/c;->a:Ljava/util/ArrayList;

    if-eqz p2, :cond_0

    invoke-virtual {p0, p2, p1}, Lt1/c;->a(Landroid/view/Menu;Ljava/util/ArrayList;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/Menu;Ljava/util/ArrayList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Ljava/util/ArrayList<",
            "Landroid/view/MenuItem;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    if-eqz p1, :cond_3

    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_3

    invoke-interface {p1, v2}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v3

    move-object v4, p0

    check-cast v4, Lj1/a;

    invoke-interface {v3}, Landroid/view/MenuItem;->isVisible()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Landroid/view/MenuItem;->getOrder()I

    move-result v6

    const/high16 v7, 0x20000

    if-ne v6, v7, :cond_1

    iget-object v5, v4, Lj1/a;->c:Landroid/view/MenuItem;

    if-nez v5, :cond_0

    iput-object v3, v4, Lj1/a;->c:Landroid/view/MenuItem;

    move v5, v1

    goto :goto_1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Only one menu item is allowed to have CATEGORY_SYSTEM order!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_1
    if-eqz v5, :cond_2

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lt1/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lt1/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/MenuItem;

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12

    const v0, 0x7f0a00f5

    const/4 v1, 0x0

    if-nez p2, :cond_0

    iget-object p2, p0, Lt1/c;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f0d0051

    invoke-virtual {p2, v2, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance p3, Lt1/c$b;

    const/4 v2, 0x0

    invoke-direct {p3, v2}, Lt1/c$b;-><init>(Lt1/c$a;)V

    const v2, 0x1020006

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p3, Lt1/c$b;->a:Landroid/widget/ImageView;

    const v2, 0x1020014

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p3, Lt1/c$b;->b:Landroid/widget/TextView;

    invoke-virtual {p2, v0, p3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    invoke-static {p2}, Landroidx/emoji2/text/l;->e(Landroid/view/View;)V

    :cond_0
    invoke-virtual {p0}, Lt1/c;->getCount()I

    move-result p3

    if-nez p3, :cond_1

    goto :goto_4

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    instance-of v3, v2, Landroid/graphics/drawable/StateListDrawable;

    const/4 v4, 0x1

    if-eqz v3, :cond_5

    move-object v3, v2

    check-cast v3, Landroid/graphics/drawable/StateListDrawable;

    sget-object v5, Landroidx/emoji2/text/l;->n0:[I

    sget v6, Lq1/a;->d:I

    invoke-virtual {v3}, Landroid/graphics/drawable/StateListDrawable;->getStateCount()I

    move-result v6

    move v7, v1

    :goto_0
    if-ge v7, v6, :cond_4

    invoke-virtual {v3, v7}, Landroid/graphics/drawable/StateListDrawable;->getStateSet(I)[I

    move-result-object v8

    array-length v9, v8

    move v10, v1

    :goto_1
    if-ge v10, v9, :cond_3

    aget v11, v8, v10

    invoke-static {v5, v11}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v11

    if-ltz v11, :cond_2

    move v3, v4

    goto :goto_2

    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_4
    move v3, v1

    :goto_2
    if-eqz v3, :cond_5

    new-instance v3, Lq1/a;

    invoke-direct {v3, v2}, Lq1/a;-><init>(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p2, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    move-object v2, v3

    :cond_5
    nop

    instance-of v3, v2, Lq1/a;

    if-eqz v3, :cond_9

    check-cast v2, Lq1/a;

    if-ne p3, v4, :cond_6

    sget-object v3, Landroidx/emoji2/text/l;->r0:[I

    goto :goto_3

    :cond_6
    if-nez p1, :cond_7

    sget-object v3, Landroidx/emoji2/text/l;->o0:[I

    goto :goto_3

    :cond_7
    add-int/lit8 v3, p3, -0x1

    if-ne p1, v3, :cond_8

    sget-object v3, Landroidx/emoji2/text/l;->p0:[I

    goto :goto_3

    :cond_8
    sget-object v3, Landroidx/emoji2/text/l;->q0:[I

    :goto_3
    invoke-virtual {v2, v3}, Lq1/a;->b([I)Z

    :cond_9
    :goto_4
    invoke-static {p2, p1, p3}, Landroidx/emoji2/text/l;->T(Landroid/view/View;II)V

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object p3

    if-eqz p3, :cond_b

    check-cast p3, Lt1/c$b;

    iget-object v0, p0, Lt1/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_a

    iget-object v0, p3, Lt1/c$b;->a:Landroid/widget/ImageView;

    invoke-interface {p1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p3, Lt1/c$b;->a:Landroid/widget/ImageView;

    goto :goto_5

    :cond_a
    iget-object v0, p3, Lt1/c$b;->a:Landroid/widget/ImageView;

    const/16 v1, 0x8

    :goto_5
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object p3, p3, Lt1/c$b;->b:Landroid/widget/TextView;

    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_b
    return-object p2
.end method
