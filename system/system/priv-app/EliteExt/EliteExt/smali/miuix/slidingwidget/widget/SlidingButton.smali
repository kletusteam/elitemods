.class public Lmiuix/slidingwidget/widget/SlidingButton;
.super Landroidx/appcompat/widget/AppCompatCheckBox;
.source ""


# instance fields
.field public e:Lb2/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 11

    const v0, 0x7f0401e9

    invoke-direct {p0, p1, p2, v0}, Landroidx/appcompat/widget/AppCompatCheckBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v1, Lb2/a;

    invoke-direct {v1, p0}, Lb2/a;-><init>(Landroid/widget/CompoundButton;)V

    iput-object v1, p0, Lmiuix/slidingwidget/widget/SlidingButton;->e:Lb2/a;

    iget-object v2, v1, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080301

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v1, Lb2/a;->L:Landroid/graphics/drawable/Drawable;

    iget-object v2, v1, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080302

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v1, Lb2/a;->Q:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lmiuix/slidingwidget/widget/SlidingButton;->e:Lb2/a;

    new-instance v2, Lz0/f;

    iget-object v3, v1, Lb2/a;->d0:Landroid/widget/CompoundButton;

    iget-object v4, v1, Lb2/a;->K:La1/b;

    const v5, 0x3fce147b    # 1.61f

    invoke-direct {v2, v3, v4, v5}, Lz0/f;-><init>(Ljava/lang/Object;La1/b;F)V

    iput-object v2, v1, Lb2/a;->I:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    const v3, 0x4476bd71

    invoke-virtual {v2, v3}, Lz0/g;->b(F)Lz0/g;

    iget-object v2, v1, Lb2/a;->I:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    const v4, 0x3f19999a    # 0.6f

    invoke-virtual {v2, v4}, Lz0/g;->a(F)Lz0/g;

    iget-object v2, v1, Lb2/a;->I:Lz0/f;

    const v5, 0x3b03126f    # 0.002f

    invoke-virtual {v2, v5}, Lz0/c;->f(F)Lz0/c;

    iget-object v2, v1, Lb2/a;->I:Lz0/f;

    iget-object v6, v1, Lb2/a;->f:Lz0/c$c;

    invoke-virtual {v2, v6}, Lz0/c;->b(Lz0/c$c;)Lz0/c;

    new-instance v2, Lz0/f;

    iget-object v6, v1, Lb2/a;->d0:Landroid/widget/CompoundButton;

    iget-object v7, v1, Lb2/a;->K:La1/b;

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-direct {v2, v6, v7, v8}, Lz0/f;-><init>(Ljava/lang/Object;La1/b;F)V

    iput-object v2, v1, Lb2/a;->R:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v3}, Lz0/g;->b(F)Lz0/g;

    iget-object v2, v1, Lb2/a;->R:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v4}, Lz0/g;->a(F)Lz0/g;

    iget-object v2, v1, Lb2/a;->R:Lz0/f;

    invoke-virtual {v2, v5}, Lz0/c;->f(F)Lz0/c;

    iget-object v2, v1, Lb2/a;->R:Lz0/f;

    iget-object v4, v1, Lb2/a;->f:Lz0/c$c;

    invoke-virtual {v2, v4}, Lz0/c;->b(Lz0/c$c;)Lz0/c;

    new-instance v2, Lz0/f;

    iget-object v4, v1, Lb2/a;->d0:Landroid/widget/CompoundButton;

    iget-object v5, v1, Lb2/a;->N:La1/b;

    invoke-direct {v2, v4, v5, v8}, Lz0/f;-><init>(Ljava/lang/Object;La1/b;F)V

    iput-object v2, v1, Lb2/a;->P:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v3}, Lz0/g;->b(F)Lz0/g;

    iget-object v2, v1, Lb2/a;->P:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    const v4, 0x3f7d70a4    # 0.99f

    invoke-virtual {v2, v4}, Lz0/g;->a(F)Lz0/g;

    iget-object v2, v1, Lb2/a;->P:Lz0/f;

    const/high16 v5, 0x3b800000    # 0.00390625f

    invoke-virtual {v2, v5}, Lz0/c;->f(F)Lz0/c;

    iget-object v2, v1, Lb2/a;->P:Lz0/f;

    iget-object v6, v1, Lb2/a;->f:Lz0/c$c;

    invoke-virtual {v2, v6}, Lz0/c;->b(Lz0/c$c;)Lz0/c;

    new-instance v2, Lz0/f;

    iget-object v6, v1, Lb2/a;->d0:Landroid/widget/CompoundButton;

    iget-object v7, v1, Lb2/a;->N:La1/b;

    const/4 v9, 0x0

    invoke-direct {v2, v6, v7, v9}, Lz0/f;-><init>(Ljava/lang/Object;La1/b;F)V

    iput-object v2, v1, Lb2/a;->O:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v3}, Lz0/g;->b(F)Lz0/g;

    iget-object v2, v1, Lb2/a;->O:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v4}, Lz0/g;->a(F)Lz0/g;

    iget-object v2, v1, Lb2/a;->O:Lz0/f;

    invoke-virtual {v2, v5}, Lz0/c;->f(F)Lz0/c;

    iget-object v2, v1, Lb2/a;->O:Lz0/f;

    iget-object v6, v1, Lb2/a;->f:Lz0/c$c;

    invoke-virtual {v2, v6}, Lz0/c;->b(Lz0/c$c;)Lz0/c;

    new-instance v2, Lz0/f;

    iget-object v6, v1, Lb2/a;->d0:Landroid/widget/CompoundButton;

    iget-object v7, v1, Lb2/a;->X:La1/b;

    const v10, 0x3e19999a    # 0.15f

    invoke-direct {v2, v6, v7, v10}, Lz0/f;-><init>(Ljava/lang/Object;La1/b;F)V

    iput-object v2, v1, Lb2/a;->V:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v3}, Lz0/g;->b(F)Lz0/g;

    iget-object v2, v1, Lb2/a;->V:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v4}, Lz0/g;->a(F)Lz0/g;

    iget-object v2, v1, Lb2/a;->V:Lz0/f;

    invoke-virtual {v2, v5}, Lz0/c;->f(F)Lz0/c;

    iget-object v2, v1, Lb2/a;->V:Lz0/f;

    iget-object v6, v1, Lb2/a;->f:Lz0/c$c;

    invoke-virtual {v2, v6}, Lz0/c;->b(Lz0/c$c;)Lz0/c;

    new-instance v2, Lz0/f;

    iget-object v6, v1, Lb2/a;->d0:Landroid/widget/CompoundButton;

    iget-object v7, v1, Lb2/a;->X:La1/b;

    const v10, 0x3dcccccd    # 0.1f

    invoke-direct {v2, v6, v7, v10}, Lz0/f;-><init>(Ljava/lang/Object;La1/b;F)V

    iput-object v2, v1, Lb2/a;->U:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v3}, Lz0/g;->b(F)Lz0/g;

    iget-object v2, v1, Lb2/a;->U:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v4}, Lz0/g;->a(F)Lz0/g;

    iget-object v2, v1, Lb2/a;->U:Lz0/f;

    invoke-virtual {v2, v5}, Lz0/c;->f(F)Lz0/c;

    iget-object v2, v1, Lb2/a;->U:Lz0/f;

    iget-object v6, v1, Lb2/a;->f:Lz0/c$c;

    invoke-virtual {v2, v6}, Lz0/c;->b(Lz0/c$c;)Lz0/c;

    new-instance v2, Lz0/f;

    iget-object v6, v1, Lb2/a;->d0:Landroid/widget/CompoundButton;

    iget-object v7, v1, Lb2/a;->n:La1/b;

    invoke-direct {v2, v6, v7, v8}, Lz0/f;-><init>(Ljava/lang/Object;La1/b;F)V

    iput-object v2, v1, Lb2/a;->k:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    const v6, 0x43db51ec

    invoke-virtual {v2, v6}, Lz0/g;->b(F)Lz0/g;

    iget-object v2, v1, Lb2/a;->k:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v4}, Lz0/g;->a(F)Lz0/g;

    iget-object v2, v1, Lb2/a;->k:Lz0/f;

    invoke-virtual {v2, v5}, Lz0/c;->f(F)Lz0/c;

    iget-object v2, v1, Lb2/a;->k:Lz0/f;

    iget-object v6, v1, Lb2/a;->f:Lz0/c$c;

    invoke-virtual {v2, v6}, Lz0/c;->b(Lz0/c$c;)Lz0/c;

    new-instance v2, Lz0/f;

    iget-object v6, v1, Lb2/a;->d0:Landroid/widget/CompoundButton;

    iget-object v7, v1, Lb2/a;->n:La1/b;

    invoke-direct {v2, v6, v7, v9}, Lz0/f;-><init>(Ljava/lang/Object;La1/b;F)V

    iput-object v2, v1, Lb2/a;->j:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v3}, Lz0/g;->b(F)Lz0/g;

    iget-object v2, v1, Lb2/a;->j:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v4}, Lz0/g;->a(F)Lz0/g;

    iget-object v2, v1, Lb2/a;->j:Lz0/f;

    invoke-virtual {v2, v5}, Lz0/c;->f(F)Lz0/c;

    iget-object v2, v1, Lb2/a;->j:Lz0/f;

    iget-object v6, v1, Lb2/a;->f:Lz0/c$c;

    invoke-virtual {v2, v6}, Lz0/c;->b(Lz0/c$c;)Lz0/c;

    new-instance v2, Lz0/f;

    iget-object v6, v1, Lb2/a;->d0:Landroid/widget/CompoundButton;

    iget-object v7, v1, Lb2/a;->r:La1/b;

    const v8, 0x3d4ccccd    # 0.05f

    invoke-direct {v2, v6, v7, v8}, Lz0/f;-><init>(Ljava/lang/Object;La1/b;F)V

    iput-object v2, v1, Lb2/a;->c0:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v3}, Lz0/g;->b(F)Lz0/g;

    iget-object v2, v1, Lb2/a;->c0:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v4}, Lz0/g;->a(F)Lz0/g;

    iget-object v2, v1, Lb2/a;->c0:Lz0/f;

    invoke-virtual {v2, v5}, Lz0/c;->f(F)Lz0/c;

    iget-object v2, v1, Lb2/a;->c0:Lz0/f;

    iget-object v6, v1, Lb2/a;->f:Lz0/c$c;

    invoke-virtual {v2, v6}, Lz0/c;->b(Lz0/c$c;)Lz0/c;

    new-instance v2, Lz0/f;

    iget-object v6, v1, Lb2/a;->d0:Landroid/widget/CompoundButton;

    iget-object v7, v1, Lb2/a;->r:La1/b;

    invoke-direct {v2, v6, v7, v9}, Lz0/f;-><init>(Ljava/lang/Object;La1/b;F)V

    iput-object v2, v1, Lb2/a;->b0:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v3}, Lz0/g;->b(F)Lz0/g;

    iget-object v2, v1, Lb2/a;->b0:Lz0/f;

    iget-object v2, v2, Lz0/f;->o:Lz0/g;

    invoke-virtual {v2, v4}, Lz0/g;->a(F)Lz0/g;

    iget-object v2, v1, Lb2/a;->b0:Lz0/f;

    invoke-virtual {v2, v5}, Lz0/c;->f(F)Lz0/c;

    iget-object v2, v1, Lb2/a;->b0:Lz0/f;

    iget-object v1, v1, Lb2/a;->f:Lz0/c$c;

    invoke-virtual {v2, v1}, Lz0/c;->b(Lz0/c$c;)Lz0/c;

    sget-object v1, Landroidx/emoji2/text/l;->k0:[I

    const v2, 0x7f130280

    invoke-virtual {p1, p2, v1, v0, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    iget-object v0, p0, Lmiuix/slidingwidget/widget/SlidingButton;->e:Lb2/a;

    iget-object v1, v0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07019b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Lb2/a;->c:I

    iget-object v1, v0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07019e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Lb2/a;->i:I

    iget-object v1, v0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setDrawingCacheEnabled(Z)V

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    const/4 v3, 0x2

    div-int/2addr v1, v3

    iput v1, v0, Lb2/a;->Y:I

    const/4 v1, 0x6

    invoke-virtual {p2, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, v0, Lb2/a;->E:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x5

    invoke-virtual {p2, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, v0, Lb2/a;->A:Landroid/graphics/drawable/Drawable;

    iget-object v1, v0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {p2, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/CompoundButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const-string v1, "#FF0D84FF"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    const v1, 0x7f0601ee

    invoke-virtual {p1, v1}, Landroid/content/Context;->getColor(I)I

    move-result p1

    const/4 v1, 0x7

    invoke-virtual {p2, v1, p1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p1

    iput p1, v0, Lb2/a;->T:I

    iget-object p1, v0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v1, 0x7f07019c

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iget-object v1, v0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f07019d

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v4, v0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {v4}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0701a2

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, v0, Lb2/a;->e0:I

    mul-int/2addr p1, v3

    add-int/2addr p1, v1

    iput p1, v0, Lb2/a;->e:I

    iget-object p1, v0, Lb2/a;->E:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result p1

    invoke-static {v4, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    iput p1, v0, Lb2/a;->S:I

    iget p1, v0, Lb2/a;->e:I

    iget-object v1, v0, Lb2/a;->E:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result p1

    iput p1, v0, Lb2/a;->x:I

    iget p1, v0, Lb2/a;->e0:I

    iget v1, v0, Lb2/a;->S:I

    sub-int/2addr p1, v1

    iput p1, v0, Lb2/a;->H:I

    iput v2, v0, Lb2/a;->B:I

    new-instance p1, Landroid/util/TypedValue;

    invoke-direct {p1}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p2, v3, p1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    const/4 v4, 0x3

    invoke-virtual {p2, v4, v1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    invoke-virtual {p2, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {p2, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iget v5, p1, Landroid/util/TypedValue;->type:I

    iget v6, v1, Landroid/util/TypedValue;->type:I

    if-ne v5, v6, :cond_0

    iget v5, p1, Landroid/util/TypedValue;->data:I

    iget v6, v1, Landroid/util/TypedValue;->data:I

    if-ne v5, v6, :cond_0

    iget p1, p1, Landroid/util/TypedValue;->resourceId:I

    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    if-ne p1, v1, :cond_0

    move-object v4, v3

    :cond_0
    if-eqz v4, :cond_1

    if-eqz v3, :cond_1

    iget p1, v0, Lb2/a;->T:I

    invoke-virtual {v4, p1}, Landroid/graphics/drawable/Drawable;->setTint(I)V

    invoke-virtual {v0, v4}, Lb2/a;->b(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {v0, v3}, Lb2/a;->b(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v4}, Lb2/a;->b(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iput-object p1, v0, Lb2/a;->l:Landroid/graphics/drawable/Drawable;

    iput-object v1, v0, Lb2/a;->s:Landroid/graphics/drawable/Drawable;

    iput-object v3, v0, Lb2/a;->p:Landroid/graphics/drawable/Drawable;

    new-instance p1, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {p1}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    iget v1, v0, Lb2/a;->e0:I

    iget v3, v0, Lb2/a;->e:I

    invoke-virtual {p1, v2, v2, v1, v3}, Landroid/graphics/drawable/StateListDrawable;->setBounds(IIII)V

    iget-object v1, v0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {p1, v1}, Landroid/graphics/drawable/StateListDrawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iput-object p1, v0, Lb2/a;->w:Landroid/graphics/drawable/StateListDrawable;

    :cond_1
    invoke-virtual {v0}, Lb2/a;->d()V

    iget-object p1, v0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_2

    iget p1, v0, Lb2/a;->H:I

    invoke-virtual {v0, p1}, Lb2/a;->e(I)V

    :cond_2
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method public drawableStateChanged()V
    .locals 1

    invoke-super {p0}, Landroidx/appcompat/widget/AppCompatCheckBox;->drawableStateChanged()V

    iget-object v0, p0, Lmiuix/slidingwidget/widget/SlidingButton;->e:Lb2/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lb2/a;->d()V

    :cond_0
    return-void
.end method

.method public getAlpha()F
    .locals 1

    iget-object v0, p0, Lmiuix/slidingwidget/widget/SlidingButton;->e:Lb2/a;

    if-eqz v0, :cond_0

    iget v0, v0, Lb2/a;->d:F

    return v0

    :cond_0
    invoke-super {p0}, Landroid/widget/CheckBox;->getAlpha()F

    move-result v0

    return v0
.end method

.method public hasOverlappingRendering()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1

    invoke-super {p0}, Landroid/widget/CheckBox;->jumpDrawablesToCurrentState()V

    iget-object v0, p0, Lmiuix/slidingwidget/widget/SlidingButton;->e:Lb2/a;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lb2/a;->w:Landroid/graphics/drawable/StateListDrawable;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/StateListDrawable;->jumpToCurrentState()V

    :cond_0
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget-object v2, v0, Lmiuix/slidingwidget/widget/SlidingButton;->e:Lb2/a;

    if-nez v2, :cond_0

    invoke-super/range {p0 .. p1}, Landroid/widget/CheckBox;->onDraw(Landroid/graphics/Canvas;)V

    return-void

    :cond_0
    iget-object v3, v2, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {v3}, Landroid/widget/CompoundButton;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    const/16 v3, 0xff

    goto :goto_0

    :cond_1
    const/16 v3, 0x7f

    :goto_0
    int-to-float v3, v3

    iget v4, v2, Lb2/a;->d:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v4, v3

    const/high16 v5, 0x437f0000    # 255.0f

    div-float/2addr v4, v5

    iget v6, v2, Lb2/a;->m:F

    const/high16 v7, 0x3f800000    # 1.0f

    sub-float/2addr v7, v6

    mul-float/2addr v7, v5

    mul-float/2addr v7, v4

    float-to-int v6, v7

    if-lez v6, :cond_2

    iget-object v7, v2, Lb2/a;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7, v6}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v6, v2, Lb2/a;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_2
    iget v6, v2, Lb2/a;->q:F

    mul-float/2addr v6, v5

    mul-float/2addr v6, v4

    float-to-int v6, v6

    if-lez v6, :cond_3

    iget-object v7, v2, Lb2/a;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7, v6}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v6, v2, Lb2/a;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_3
    iget v6, v2, Lb2/a;->m:F

    mul-float/2addr v6, v5

    mul-float/2addr v6, v4

    float-to-int v6, v6

    if-lez v6, :cond_4

    iget-object v7, v2, Lb2/a;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7, v6}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v6, v2, Lb2/a;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_4
    iget-object v6, v2, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-static {v6}, Lh/w0;->b(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_5

    iget v7, v2, Lb2/a;->e0:I

    iget v8, v2, Lb2/a;->B:I

    sub-int/2addr v7, v8

    iget v8, v2, Lb2/a;->S:I

    sub-int/2addr v7, v8

    goto :goto_1

    :cond_5
    iget v7, v2, Lb2/a;->B:I

    :goto_1
    if-eqz v6, :cond_6

    iget v6, v2, Lb2/a;->e0:I

    iget v8, v2, Lb2/a;->B:I

    sub-int/2addr v6, v8

    goto :goto_2

    :cond_6
    iget v6, v2, Lb2/a;->S:I

    iget v8, v2, Lb2/a;->B:I

    add-int/2addr v6, v8

    :goto_2
    iget v8, v2, Lb2/a;->e:I

    iget v9, v2, Lb2/a;->x:I

    sub-int/2addr v8, v9

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v9, v8

    add-int v10, v6, v7

    div-int/lit8 v10, v10, 0x2

    add-int v11, v9, v8

    div-int/lit8 v11, v11, 0x2

    iget v12, v2, Lb2/a;->M:F

    mul-float/2addr v12, v5

    float-to-int v12, v12

    if-nez v12, :cond_7

    goto :goto_4

    :cond_7
    iget-object v13, v2, Lb2/a;->L:Landroid/graphics/drawable/Drawable;

    instance-of v14, v13, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v14, :cond_8

    check-cast v13, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v13}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v13

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    iget-object v14, v2, Lb2/a;->L:Landroid/graphics/drawable/Drawable;

    check-cast v14, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v14}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v14

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    goto :goto_3

    :cond_8
    invoke-virtual {v13}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v13

    iget-object v14, v2, Lb2/a;->L:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v14}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v14

    :goto_3
    iget-object v15, v2, Lb2/a;->L:Landroid/graphics/drawable/Drawable;

    div-int/lit8 v13, v13, 0x2

    div-int/lit8 v14, v14, 0x2

    sub-int v5, v10, v13

    sub-int v0, v11, v14

    add-int/2addr v13, v10

    add-int/2addr v14, v11

    invoke-virtual {v15, v5, v0, v13, v14}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, v2, Lb2/a;->L:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v12}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v0, v2, Lb2/a;->L:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :goto_4
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    iget v0, v2, Lb2/a;->J:F

    int-to-float v5, v10

    int-to-float v10, v11

    invoke-virtual {v1, v0, v0, v5, v10}, Landroid/graphics/Canvas;->scale(FFFF)V

    iget-boolean v0, v2, Lb2/a;->a:Z

    if-eqz v0, :cond_9

    iget-object v0, v2, Lb2/a;->E:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v0, v2, Lb2/a;->E:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v7, v8, v6, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, v2, Lb2/a;->E:Landroid/graphics/drawable/Drawable;

    goto :goto_5

    :cond_9
    iget-object v0, v2, Lb2/a;->A:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v0, v2, Lb2/a;->A:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v7, v8, v6, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, v2, Lb2/a;->A:Landroid/graphics/drawable/Drawable;

    :goto_5
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, v2, Lb2/a;->Q:Landroid/graphics/drawable/Drawable;

    iget v3, v2, Lb2/a;->W:F

    const/high16 v5, 0x437f0000    # 255.0f

    mul-float/2addr v3, v5

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v0, v2, Lb2/a;->Q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v7, v8, v6, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, v2, Lb2/a;->Q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method public onMeasure(II)V
    .locals 0

    iget-object p1, p0, Lmiuix/slidingwidget/widget/SlidingButton;->e:Lb2/a;

    iget p2, p1, Lb2/a;->e0:I

    iget p1, p1, Lb2/a;->e:I

    invoke-virtual {p0, p2, p1}, Landroid/widget/CheckBox;->setMeasuredDimension(II)V

    iget-object p1, p0, Lmiuix/slidingwidget/widget/SlidingButton;->e:Lb2/a;

    iget-object p1, p1, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    if-eqz p1, :cond_0

    instance-of p2, p1, Landroid/view/ViewGroup;

    if-eqz p2, :cond_0

    check-cast p1, Landroid/view/ViewGroup;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    :cond_0
    return-void
.end method

.method public onSetAlpha(I)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    invoke-virtual {p0}, Landroid/widget/CheckBox;->isEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lmiuix/slidingwidget/widget/SlidingButton;->e:Lb2/a;

    const/4 v2, 0x1

    if-eqz v0, :cond_1b

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    float-to-int p1, p1

    iget-object v5, v0, Lb2/a;->Z:Landroid/graphics/Rect;

    iget-object v6, v0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-static {v6}, Lh/w0;->b(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_1

    iget v7, v0, Lb2/a;->e0:I

    iget v8, v0, Lb2/a;->B:I

    sub-int/2addr v7, v8

    iget v8, v0, Lb2/a;->S:I

    sub-int/2addr v7, v8

    goto :goto_0

    :cond_1
    iget v7, v0, Lb2/a;->B:I

    :goto_0
    if-eqz v6, :cond_2

    iget v6, v0, Lb2/a;->e0:I

    iget v8, v0, Lb2/a;->B:I

    sub-int/2addr v6, v8

    goto :goto_1

    :cond_2
    iget v6, v0, Lb2/a;->B:I

    iget v8, v0, Lb2/a;->S:I

    add-int/2addr v6, v8

    :goto_1
    iget v8, v0, Lb2/a;->e:I

    invoke-virtual {v5, v7, v1, v6, v8}, Landroid/graphics/Rect;->set(IIII)V

    if-eqz v3, :cond_11

    const/4 v6, 0x2

    if-eq v3, v2, :cond_c

    if-eq v3, v6, :cond_5

    const/4 p1, 0x3

    if-eq v3, p1, :cond_3

    goto/16 :goto_a

    :cond_3
    invoke-virtual {v0}, Lb2/a;->c()V

    iget-boolean p1, v0, Lb2/a;->a0:Z

    if-eqz p1, :cond_10

    iget p1, v0, Lb2/a;->B:I

    iget v3, v0, Lb2/a;->H:I

    div-int/2addr v3, v6

    if-lt p1, v3, :cond_4

    move p1, v2

    goto :goto_2

    :cond_4
    move p1, v1

    :goto_2
    iput-boolean p1, v0, Lb2/a;->a:Z

    invoke-virtual {v0, p1}, Lb2/a;->a(Z)V

    goto/16 :goto_6

    :cond_5
    iget-boolean p1, v0, Lb2/a;->a0:Z

    if-eqz p1, :cond_1b

    iget p1, v0, Lb2/a;->h:I

    sub-int p1, v4, p1

    iget-object v3, v0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-static {v3}, Lh/w0;->b(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_6

    neg-int p1, p1

    :cond_6
    iget v3, v0, Lb2/a;->B:I

    add-int/2addr v3, p1

    iput v3, v0, Lb2/a;->B:I

    if-gez v3, :cond_7

    iput v1, v0, Lb2/a;->B:I

    goto :goto_3

    :cond_7
    iget p1, v0, Lb2/a;->H:I

    if-le v3, p1, :cond_8

    iput p1, v0, Lb2/a;->B:I

    :cond_8
    :goto_3
    iget p1, v0, Lb2/a;->B:I

    if-eqz p1, :cond_9

    iget v3, v0, Lb2/a;->H:I

    if-ne p1, v3, :cond_a

    :cond_9
    move v1, v2

    :cond_a
    if-eqz v1, :cond_b

    iget-boolean p1, v0, Lb2/a;->g:Z

    if-nez p1, :cond_b

    iget-object p1, v0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    sget v3, Lmiuix/view/c;->m:I

    invoke-static {p1, v3}, Lmiuix/view/HapticCompat;->performHapticFeedback(Landroid/view/View;I)Z

    :cond_b
    iput-boolean v1, v0, Lb2/a;->g:Z

    iget p1, v0, Lb2/a;->B:I

    invoke-virtual {v0, p1}, Lb2/a;->e(I)V

    iput v4, v0, Lb2/a;->h:I

    iget p1, v0, Lb2/a;->u:I

    sub-int/2addr v4, p1

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result p1

    iget v1, v0, Lb2/a;->Y:I

    if-lt p1, v1, :cond_1b

    iput-boolean v2, v0, Lb2/a;->z:Z

    iget-object p1, v0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    invoke-interface {p1, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto/16 :goto_a

    :cond_c
    invoke-virtual {v0}, Lb2/a;->c()V

    iget-boolean v3, v0, Lb2/a;->a0:Z

    if-eqz v3, :cond_f

    iget-boolean v3, v0, Lb2/a;->z:Z

    if-nez v3, :cond_d

    goto :goto_5

    :cond_d
    iget v3, v0, Lb2/a;->B:I

    iget v7, v0, Lb2/a;->H:I

    div-int/2addr v7, v6

    if-lt v3, v7, :cond_e

    move v3, v2

    goto :goto_4

    :cond_e
    move v3, v1

    :goto_4
    iput-boolean v3, v0, Lb2/a;->a:Z

    invoke-virtual {v0, v3}, Lb2/a;->a(Z)V

    invoke-virtual {v5, v4, p1}, Landroid/graphics/Rect;->contains(II)Z

    move-result p1

    if-eqz p1, :cond_10

    iget-object p1, v0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    sget v3, Lmiuix/view/c;->m:I

    invoke-static {p1, v3}, Lmiuix/view/HapticCompat;->performHapticFeedback(Landroid/view/View;I)Z

    goto :goto_6

    :cond_f
    :goto_5
    iget-object p1, v0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result p1

    xor-int/2addr p1, v2

    invoke-virtual {v0, p1}, Lb2/a;->a(Z)V

    iget-object p1, v0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    sget v3, Lmiuix/view/c;->m:I

    invoke-static {p1, v3}, Lmiuix/view/HapticCompat;->performHapticFeedback(Landroid/view/View;I)Z

    :cond_10
    :goto_6
    iput-boolean v1, v0, Lb2/a;->a0:Z

    iput-boolean v1, v0, Lb2/a;->z:Z

    iget-object p1, v0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {p1, v1}, Landroid/widget/CompoundButton;->setPressed(Z)V

    goto :goto_a

    :cond_11
    invoke-virtual {v5, v4, p1}, Landroid/graphics/Rect;->contains(II)Z

    move-result p1

    if-eqz p1, :cond_1a

    iput-boolean v2, v0, Lb2/a;->a0:Z

    iget-object p1, v0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {p1, v2}, Landroid/widget/CompoundButton;->setPressed(Z)V

    iget-object p1, v0, Lb2/a;->R:Lz0/f;

    iget-boolean v3, p1, Lz0/c;->f:Z

    if-eqz v3, :cond_12

    invoke-virtual {p1}, Lz0/c;->c()V

    :cond_12
    iget-object p1, v0, Lb2/a;->I:Lz0/f;

    iget-boolean v3, p1, Lz0/c;->f:Z

    if-nez v3, :cond_13

    invoke-virtual {p1}, Lz0/f;->i()V

    :cond_13
    iget-object p1, v0, Lb2/a;->P:Lz0/f;

    iget-boolean v3, p1, Lz0/c;->f:Z

    if-nez v3, :cond_14

    invoke-virtual {p1}, Lz0/f;->i()V

    :cond_14
    iget-object p1, v0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result p1

    if-nez p1, :cond_17

    iget-object p1, v0, Lb2/a;->b0:Lz0/f;

    iget-boolean v3, p1, Lz0/c;->f:Z

    if-eqz v3, :cond_15

    invoke-virtual {p1}, Lz0/c;->c()V

    :cond_15
    iget-object p1, v0, Lb2/a;->c0:Lz0/f;

    iget-boolean v3, p1, Lz0/c;->f:Z

    if-nez v3, :cond_16

    invoke-virtual {p1}, Lz0/f;->i()V

    :cond_16
    iget-object p1, v0, Lb2/a;->V:Lz0/f;

    iget-boolean v3, p1, Lz0/c;->f:Z

    if-nez v3, :cond_17

    invoke-virtual {p1}, Lz0/f;->i()V

    :cond_17
    iget p1, v0, Lb2/a;->B:I

    if-lez p1, :cond_19

    iget v3, v0, Lb2/a;->H:I

    if-lt p1, v3, :cond_18

    goto :goto_7

    :cond_18
    move p1, v1

    goto :goto_8

    :cond_19
    :goto_7
    move p1, v2

    :goto_8
    iput-boolean p1, v0, Lb2/a;->g:Z

    goto :goto_9

    :cond_1a
    iput-boolean v1, v0, Lb2/a;->a0:Z

    :goto_9
    iput v4, v0, Lb2/a;->h:I

    iput v4, v0, Lb2/a;->u:I

    iput-boolean v1, v0, Lb2/a;->z:Z

    :cond_1b
    :goto_a
    return v2
.end method

.method public performClick()Z
    .locals 3

    invoke-super {p0}, Landroid/widget/CheckBox;->performClick()Z

    iget-object v0, p0, Lmiuix/slidingwidget/widget/SlidingButton;->e:Lb2/a;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lb2/a;->t:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    iget-object v2, v0, Lb2/a;->t:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    iget-object v0, v0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-interface {v2, v0, v1}, Landroid/widget/CompoundButton$OnCheckedChangeListener;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public setAlpha(F)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/CheckBox;->setAlpha(F)V

    iget-object v0, p0, Lmiuix/slidingwidget/widget/SlidingButton;->e:Lb2/a;

    if-eqz v0, :cond_0

    iput p1, v0, Lb2/a;->d:F

    :cond_0
    invoke-virtual {p0}, Landroid/widget/CheckBox;->invalidate()V

    return-void
.end method

.method public setButtonDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    return-void
.end method

.method public setChecked(Z)V
    .locals 3

    invoke-virtual {p0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eq v0, p1, :cond_6

    invoke-super {p0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    invoke-virtual {p0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    iget-object v0, p0, Lmiuix/slidingwidget/widget/SlidingButton;->e:Lb2/a;

    if-eqz v0, :cond_6

    iget v1, v0, Lb2/a;->B:I

    iput v1, v0, Lb2/a;->D:I

    iget v1, v0, Lb2/a;->F:I

    iput v1, v0, Lb2/a;->G:I

    iget v1, v0, Lb2/a;->m:F

    iput v1, v0, Lb2/a;->o:F

    iget-boolean v1, v0, Lb2/a;->a:Z

    iput-boolean v1, v0, Lb2/a;->b:Z

    const/4 v1, 0x1

    iput-boolean v1, v0, Lb2/a;->v:Z

    iput-boolean p1, v0, Lb2/a;->a:Z

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget v2, v0, Lb2/a;->H:I

    goto :goto_0

    :cond_0
    move v2, v1

    :goto_0
    iput v2, v0, Lb2/a;->B:I

    if-eqz p1, :cond_1

    const/16 v1, 0xff

    :cond_1
    iput v1, v0, Lb2/a;->F:I

    if-eqz p1, :cond_2

    const/high16 p1, 0x3f800000    # 1.0f

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    :goto_1
    iput p1, v0, Lb2/a;->m:F

    iget-object p1, v0, Lb2/a;->y:Lz0/f;

    if-eqz p1, :cond_3

    iget-boolean v1, p1, Lz0/c;->f:Z

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lz0/c;->c()V

    :cond_3
    iget-object p1, v0, Lb2/a;->j:Lz0/f;

    iget-boolean v1, p1, Lz0/c;->f:Z

    if-eqz v1, :cond_4

    invoke-virtual {p1}, Lz0/c;->c()V

    :cond_4
    iget-object p1, v0, Lb2/a;->k:Lz0/f;

    iget-boolean v1, p1, Lz0/c;->f:Z

    if-eqz v1, :cond_5

    invoke-virtual {p1}, Lz0/c;->c()V

    :cond_5
    iget-object p1, v0, Lb2/a;->d0:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->invalidate()V

    :cond_6
    return-void
.end method

.method public setLayerType(ILandroid/graphics/Paint;)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/widget/CheckBox;->setLayerType(ILandroid/graphics/Paint;)V

    iget-object p2, p0, Lmiuix/slidingwidget/widget/SlidingButton;->e:Lb2/a;

    if-eqz p2, :cond_2

    iget-object v0, p2, Lb2/a;->l:Landroid/graphics/drawable/Drawable;

    instance-of v1, v0, Lmiuix/smooth/SmoothContainerDrawable;

    if-eqz v1, :cond_0

    check-cast v0, Lmiuix/smooth/SmoothContainerDrawable;

    invoke-virtual {v0, p1}, Lmiuix/smooth/SmoothContainerDrawable;->c(I)V

    :cond_0
    iget-object v0, p2, Lb2/a;->s:Landroid/graphics/drawable/Drawable;

    instance-of v1, v0, Lmiuix/smooth/SmoothContainerDrawable;

    if-eqz v1, :cond_1

    check-cast v0, Lmiuix/smooth/SmoothContainerDrawable;

    invoke-virtual {v0, p1}, Lmiuix/smooth/SmoothContainerDrawable;->c(I)V

    :cond_1
    iget-object p2, p2, Lb2/a;->p:Landroid/graphics/drawable/Drawable;

    instance-of v0, p2, Lmiuix/smooth/SmoothContainerDrawable;

    if-eqz v0, :cond_2

    check-cast p2, Lmiuix/smooth/SmoothContainerDrawable;

    invoke-virtual {p2, p1}, Lmiuix/smooth/SmoothContainerDrawable;->c(I)V

    :cond_2
    return-void
.end method

.method public setOnPerformCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 1

    iget-object v0, p0, Lmiuix/slidingwidget/widget/SlidingButton;->e:Lb2/a;

    if-eqz v0, :cond_0

    iput-object p1, v0, Lb2/a;->t:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    :cond_0
    return-void
.end method

.method public setPressed(Z)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/CheckBox;->setPressed(Z)V

    invoke-virtual {p0}, Landroid/widget/CheckBox;->invalidate()V

    return-void
.end method

.method public verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 3

    invoke-super {p0, p1}, Landroid/widget/CheckBox;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_1

    iget-object v0, p0, Lmiuix/slidingwidget/widget/SlidingButton;->e:Lb2/a;

    if-eqz v0, :cond_2

    iget-object v0, v0, Lb2/a;->w:Landroid/graphics/drawable/StateListDrawable;

    if-ne p1, v0, :cond_0

    move p1, v2

    goto :goto_0

    :cond_0
    move p1, v1

    :goto_0
    if-eqz p1, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    return v1
.end method
