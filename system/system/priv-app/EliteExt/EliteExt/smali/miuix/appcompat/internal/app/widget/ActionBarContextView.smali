.class public Lmiuix/appcompat/internal/app/widget/ActionBarContextView;
.super Lmiuix/appcompat/internal/app/widget/a;
.source ""

# interfaces
.implements Lmiuix/appcompat/internal/app/widget/f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/appcompat/internal/app/widget/ActionBarContextView$d;,
        Lmiuix/appcompat/internal/app/widget/ActionBarContextView$e;
    }
.end annotation


# instance fields
.field public A:Landroid/widget/Button;

.field public B:Li1/a;

.field public C:Lmiuix/appcompat/internal/app/widget/a$b;

.field public D:I

.field public E:I

.field public F:I

.field public G:Landroid/widget/TextView;

.field public H:Landroid/widget/FrameLayout;

.field public I:Lmiuix/appcompat/internal/app/widget/a$b;

.field public J:Z

.field public K:Landroid/view/View$OnClickListener;

.field public L:I

.field public M:Ljava/lang/Runnable;

.field public N:Landroid/widget/Scroller;

.field public O:Z

.field public P:Landroid/graphics/drawable/Drawable;

.field public Q:Z

.field public R:Ljava/lang/CharSequence;

.field public S:Landroid/widget/LinearLayout;

.field public T:Z

.field public U:I

.field public V:Landroid/widget/TextView;

.field public W:Z

.field public a0:Ld/o;

.field public r:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/ActionMode;",
            ">;"
        }
    .end annotation
.end field

.field public s:Landroid/graphics/drawable/Drawable;

.field public t:Z

.field public u:Z

.field public v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmiuix/view/a;",
            ">;"
        }
    .end annotation
.end field

.field public w:I

.field public x:F

.field public y:Landroid/widget/Button;

.field public z:Li1/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 15

    move-object v0, p0

    move-object/from16 v8, p1

    move-object/from16 v1, p2

    const v2, 0x1010394

    invoke-direct {p0, v8, v1, v2}, Lmiuix/appcompat/internal/app/widget/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v9, 0x1

    iput-boolean v9, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->Q:Z

    new-instance v3, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$a;

    invoke-direct {v3, p0}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$a;-><init>(Lmiuix/appcompat/internal/app/widget/ActionBarContextView;)V

    iput-object v3, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->K:Landroid/view/View$OnClickListener;

    new-instance v3, Lmiuix/appcompat/internal/app/widget/a$b;

    invoke-direct {v3}, Lmiuix/appcompat/internal/app/widget/a$b;-><init>()V

    iput-object v3, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->C:Lmiuix/appcompat/internal/app/widget/a$b;

    new-instance v3, Lmiuix/appcompat/internal/app/widget/a$b;

    invoke-direct {v3}, Lmiuix/appcompat/internal/app/widget/a$b;-><init>()V

    iput-object v3, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->I:Lmiuix/appcompat/internal/app/widget/a$b;

    const/4 v3, 0x0

    iput-boolean v3, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->W:Z

    iput-boolean v3, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->J:Z

    new-instance v4, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$c;

    invoke-direct {v4, p0}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$c;-><init>(Lmiuix/appcompat/internal/app/widget/ActionBarContextView;)V

    iput-object v4, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->M:Ljava/lang/Runnable;

    new-instance v4, Landroid/widget/Scroller;

    invoke-direct {v4, v8}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v4, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->N:Landroid/widget/Scroller;

    new-instance v4, Landroid/widget/FrameLayout;

    invoke-direct {v4, v8}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v4, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    const v5, 0x7f0a002d

    invoke-virtual {v4, v5}, Landroid/widget/FrameLayout;->setId(I)V

    iget-object v4, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070096

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v10, 0x7f070098

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f070094

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v10

    invoke-virtual {v4, v5, v7, v6, v10}, Landroid/widget/FrameLayout;->setPaddingRelative(IIII)V

    iget-object v4, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    invoke-virtual {v4, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v4, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->I:Lmiuix/appcompat/internal/app/widget/a$b;

    iget-object v5, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    invoke-virtual {v4, v5}, Lmiuix/appcompat/internal/app/widget/a$b;->b(Landroid/view/View;)V

    sget-object v4, Landroidx/emoji2/text/l;->V:[I

    invoke-virtual {v8, v1, v4, v2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v10

    invoke-virtual {v10, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const/4 v1, 0x2

    invoke-virtual {v10, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->U:I

    const/16 v1, 0x9

    invoke-virtual {v10, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->F:I

    invoke-virtual {v10, v9, v3}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v1

    iput v1, v0, Lmiuix/appcompat/internal/app/widget/a;->e:I

    const/4 v1, 0x4

    invoke-virtual {v10, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->P:Landroid/graphics/drawable/Drawable;

    new-instance v11, Li1/a;

    const/high16 v1, 0x1040000

    invoke-virtual {v8, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v12, 0x0

    const v4, 0x1020019

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, v11

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v7}, Li1/a;-><init>(Landroid/content/Context;IIIILjava/lang/CharSequence;)V

    iput-object v11, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->z:Li1/a;

    new-instance v11, Li1/a;

    const v1, 0x7f1200cc

    invoke-virtual {v8, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    const v4, 0x102001a

    move-object v1, v11

    move v3, v12

    move v5, v13

    move v6, v14

    invoke-direct/range {v1 .. v7}, Li1/a;-><init>(Landroid/content/Context;IIIILjava/lang/CharSequence;)V

    iput-object v11, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->B:Li1/a;

    const/4 v1, 0x5

    invoke-virtual {v10, v1, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->Q:Z

    invoke-virtual {v10}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method public static synthetic m(Lmiuix/appcompat/internal/app/widget/ActionBarContextView;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->setSplitAnimating(Z)V

    return-void
.end method

.method private setSplitAnimating(Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->o:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->setAnimating(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lmiuix/view/a;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->v:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->v:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->v:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public b()V
    .locals 3

    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->v:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->v:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->o:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v0, :cond_1

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    :cond_1
    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->r:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public c(Landroid/view/ActionMode;)V
    .locals 9

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->r:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->n()V

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->b()V

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->q()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->r:Ljava/lang/ref/WeakReference;

    check-cast p1, Lg1/a;

    iget-object p1, p1, Lg1/a;->f:Lh1/g;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Li1/b;->l(Z)Z

    :cond_1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    :goto_0
    check-cast v0, Landroid/view/View;

    instance-of v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz v1, :cond_5

    new-instance v1, Li1/b;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object v4, v0

    check-cast v4, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    const v5, 0x7f0d0037

    const v6, 0x7f0d0038

    const v7, 0x7f0d0029

    const v8, 0x7f0d002c

    move-object v2, v1

    invoke-direct/range {v2 .. v8}, Li1/b;-><init>(Landroid/content/Context;Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;IIII)V

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    const/4 v0, 0x1

    iput-boolean v0, v1, Li1/b;->u:Z

    iput-boolean v0, v1, Li1/b;->v:Z

    const v2, 0x7f04004e

    iput v2, v1, Li1/b;->q:I

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    iget-boolean v4, p0, Lmiuix/appcompat/internal/app/widget/a;->n:Z

    if-nez v4, :cond_2

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    invoke-virtual {p1, v0}, Lh1/g;->b(Lh1/k;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    invoke-virtual {p1, p0}, Li1/b;->i(Landroid/view/ViewGroup;)Lh1/l;

    move-result-object p1

    check-cast p1, Li1/c;

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    invoke-virtual {p0, p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2

    :cond_2
    iget-object v4, p0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    iput-boolean v0, v4, Li1/b;->w:Z

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    invoke-static {}, Lr1/b;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    move v2, v3

    :cond_3
    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    invoke-static {}, Lr1/b;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    const/16 v0, 0x11

    goto :goto_1

    :cond_4
    const/16 v0, 0x50

    :goto_1
    iput v0, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    invoke-virtual {p1, v0}, Lh1/g;->b(Lh1/k;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    invoke-virtual {p1, p0}, Li1/b;->i(Landroid/view/ViewGroup;)Lh1/l;

    move-result-object p1

    check-cast p1, Li1/c;

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->P:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/a;->o:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    invoke-virtual {p1, v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :goto_2
    return-void

    :cond_5
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v1, v1, Landroid/view/View;

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "ActionBarOverlayLayout not found"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public d()V
    .locals 8

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->a0:Ld/o;

    const/4 v1, 0x0

    if-eqz v0, :cond_6

    iget-object v2, v0, Ld/o;->a:Ljava/lang/Object;

    check-cast v2, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, v0, Ld/o;->a:Ljava/lang/Object;

    check-cast v2, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lz0/f;

    if-eqz v3, :cond_0

    iget-object v4, v3, Lz0/f;->o:Lz0/g;

    iget-wide v4, v4, Lz0/g;->b:D

    const-wide/16 v6, 0x0

    cmpl-double v4, v4, v6

    const/4 v5, 0x1

    if-lez v4, :cond_1

    move v4, v5

    goto :goto_1

    :cond_1
    move v4, v1

    :goto_1
    if-eqz v4, :cond_3

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    if-ne v4, v6, :cond_2

    iget-boolean v4, v3, Lz0/c;->f:Z

    if-eqz v4, :cond_0

    iput-boolean v5, v3, Lz0/f;->m:Z

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/util/AndroidRuntimeException;

    const-string v1, "Animations may only be started on the main thread"

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Spring animations can only come to an end when there is damping"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget-object v0, v0, Ld/o;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->a0:Ld/o;

    :cond_6
    invoke-direct {p0, v1}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->setSplitAnimating(Z)V

    const/4 v0, 0x2

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->w:I

    return-void
.end method

.method public e(Z)V
    .locals 2

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->n()V

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->Q:Z

    invoke-direct {p0, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->setSplitAnimating(Z)V

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    iget-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->Q:Z

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->setVisibility(I)V

    iput-boolean v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->O:Z

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v1}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->s(Z)V

    goto :goto_0

    :cond_1
    iget-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->Q:Z

    if-eqz p1, :cond_2

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->t(Z)Ld/o;

    move-result-object p1

    invoke-virtual {p1}, Ld/o;->a()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->s(Z)V

    :goto_0
    return-void
.end method

.method public generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 3

    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public getActionBarStyle()I
    .locals 1

    const v0, 0x1010394

    return v0
.end method

.method public bridge synthetic getActionBarTransitionListener()Ld1/c;
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/internal/app/widget/a;->getActionBarTransitionListener()Ld1/c;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getActionMenuView()Li1/c;
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/internal/app/widget/a;->getActionMenuView()Li1/c;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getAnimatedVisibility()I
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/internal/app/widget/a;->getAnimatedVisibility()I

    move-result v0

    return v0
.end method

.method public getAnimationProgress()F
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->x:F

    return v0
.end method

.method public bridge synthetic getContentHeight()I
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/internal/app/widget/a;->getContentHeight()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getExpandState()I
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/internal/app/widget/a;->getExpandState()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getMenuView()Li1/c;
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/internal/app/widget/a;->getMenuView()Li1/c;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->R:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public h(II)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->L:I

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->N:Landroid/widget/Scroller;

    invoke-virtual {p1}, Landroid/widget/Scroller;->isFinished()Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->N:Landroid/widget/Scroller;

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    :cond_0
    if-eqz p2, :cond_1

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    :cond_1
    if-nez p2, :cond_2

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getHeight()I

    move-result p1

    iget p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->D:I

    sub-int/2addr p1, p2

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->L:I

    :goto_0
    return-void
.end method

.method public l()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Li1/b;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public n()V
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->a0:Ld/o;

    if-eqz v0, :cond_3

    iget-object v1, v0, Ld/o;->a:Ljava/lang/Object;

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, v0, Ld/o;->a:Ljava/lang/Object;

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lz0/f;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lz0/c;->c()V

    goto :goto_0

    :cond_1
    iget-object v0, v0, Ld/o;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->a0:Ld/o;

    :cond_3
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->setSplitAnimating(Z)V

    return-void
.end method

.method public final o(Landroid/view/View;FFF)Lz0/f;
    .locals 2

    new-instance v0, Lz0/f;

    sget-object v1, La1/h;->b:La1/h;

    invoke-direct {v0, p1, v1, p4}, Lz0/f;-><init>(Ljava/lang/Object;La1/b;F)V

    iput p3, v0, Lz0/c;->k:F

    const/4 p1, 0x1

    iput-boolean p1, v0, Lz0/c;->h:Z

    iget-object p1, v0, Lz0/f;->o:Lz0/g;

    invoke-virtual {p1, p2}, Lz0/g;->b(F)Lz0/g;

    iget-object p1, v0, Lz0/f;->o:Lz0/g;

    const p2, 0x3f666666    # 0.9f

    invoke-virtual {p1, p2}, Lz0/g;->a(F)Lz0/g;

    const/high16 p1, 0x3b800000    # 0.00390625f

    invoke-virtual {v0, p1}, Lz0/c;->f(F)Lz0/c;

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/app/widget/a;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x7f070096

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070098

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070094

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-virtual {v0, p1, v1, p1, v2}, Landroid/widget/FrameLayout;->setPaddingRelative(IIII)V

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x7f070082

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070081

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v2

    invoke-virtual {p0, p1, v1, v0, v2}, Landroid/view/ViewGroup;->setPaddingRelative(IIII)V

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 2

    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Li1/b;->l(Z)Z

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    iget-object v0, v0, Li1/b;->i:Li1/b$b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lh1/h;->a()V

    :cond_0
    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 8

    iget p1, p0, Lmiuix/appcompat/internal/app/widget/a;->h:I

    const/4 v0, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne p1, v0, :cond_0

    iget p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->L:I

    goto :goto_0

    :cond_0
    if-ne p1, v2, :cond_1

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result p1

    goto :goto_0

    :cond_1
    move p1, v1

    :goto_0
    sub-int/2addr p5, p3

    sub-int v3, p5, p1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingStart()I

    move-result v4

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v5

    iget v6, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->E:I

    add-int/2addr v5, v6

    sub-int p3, v3, p3

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v6

    sub-int/2addr p3, v6

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v6

    sub-int/2addr p3, v6

    iget v6, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->E:I

    sub-int/2addr p3, v6

    iget-object v6, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->S:Landroid/widget/LinearLayout;

    if-eqz v6, :cond_2

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v6

    const/16 v7, 0x8

    if-eq v6, v7, :cond_2

    iget-object v6, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->S:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v6, v4, v5, p3}, Lmiuix/appcompat/internal/app/widget/a;->i(Landroid/view/View;III)I

    :cond_2
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingEnd()I

    move-result v4

    iget-object v6, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    if-eqz v6, :cond_3

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    if-ne v6, p0, :cond_3

    iget-object v6, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    sub-int v7, p4, p2

    sub-int/2addr v7, v4

    invoke-virtual {p0, v6, v7, v5, p3}, Lmiuix/appcompat/internal/app/widget/a;->j(Landroid/view/View;III)I

    :cond_3
    iget-boolean p3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->O:Z

    if-eqz p3, :cond_4

    iput v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->w:I

    invoke-virtual {p0, v2}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->t(Z)Ld/o;

    move-result-object p3

    invoke-virtual {p3}, Ld/o;->a()V

    iput-boolean v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->O:Z

    :cond_4
    iget-object p3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    if-eqz p3, :cond_7

    invoke-virtual {p3}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result p3

    if-nez p3, :cond_7

    iget p3, p0, Lmiuix/appcompat/internal/app/widget/a;->h:I

    if-eqz p3, :cond_7

    iget-object p3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    invoke-virtual {p3}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v4

    sub-int v4, p5, v4

    invoke-virtual {p3, p2, v4, p4, p5}, Landroid/widget/FrameLayout;->layout(IIII)V

    invoke-virtual {p0}, Landroid/view/View;->getLayoutDirection()I

    move-result p3

    if-ne p3, v2, :cond_5

    move p3, v2

    goto :goto_1

    :cond_5
    move p3, v1

    :goto_1
    if-eqz p3, :cond_6

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    invoke-virtual {p2}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result p2

    sub-int p2, p4, p2

    :cond_6
    new-instance p3, Landroid/graphics/Rect;

    invoke-direct {p3}, Landroid/graphics/Rect;-><init>()V

    iget-object p4, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    invoke-virtual {p4}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result p4

    sub-int/2addr p5, v3

    sub-int/2addr p4, p5

    iget-object p5, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    invoke-virtual {p5}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result p5

    add-int/2addr p5, p2

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {p3, p2, p4, p5, v3}, Landroid/graphics/Rect;->set(IIII)V

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    invoke-virtual {p2, p3}, Landroid/widget/FrameLayout;->setClipBounds(Landroid/graphics/Rect;)V

    :cond_7
    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    invoke-virtual {p2}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result p2

    sub-int/2addr p2, p1

    int-to-float p1, p2

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    invoke-virtual {p2}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result p2

    int-to-float p2, p2

    div-float/2addr p1, p2

    const/high16 p2, 0x40400000    # 3.0f

    mul-float/2addr p1, p2

    const/high16 p2, 0x3f800000    # 1.0f

    invoke-static {p2, p1}, Ljava/lang/Math;->min(FF)F

    move-result p1

    sub-float p1, p2, p1

    iget p3, p0, Lmiuix/appcompat/internal/app/widget/a;->h:I

    const/16 p4, 0x14

    const/4 p5, 0x0

    if-ne p3, v0, :cond_9

    cmpl-float p3, p1, p5

    if-lez p3, :cond_8

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->C:Lmiuix/appcompat/internal/app/widget/a$b;

    iget-object p3, p0, Lmiuix/appcompat/internal/app/widget/a;->c:Lv0/a;

    invoke-virtual {p2, p5, v1, p4, p3}, Lmiuix/appcompat/internal/app/widget/a$b;->a(FIILv0/a;)V

    goto :goto_2

    :cond_8
    iget-object p3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->C:Lmiuix/appcompat/internal/app/widget/a$b;

    iget-object p4, p0, Lmiuix/appcompat/internal/app/widget/a;->d:Lv0/a;

    invoke-virtual {p3, p2, v1, v1, p4}, Lmiuix/appcompat/internal/app/widget/a$b;->a(FIILv0/a;)V

    :goto_2
    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->I:Lmiuix/appcompat/internal/app/widget/a$b;

    iget-object p3, p0, Lmiuix/appcompat/internal/app/widget/a;->k:Lv0/a;

    invoke-virtual {p2, p1, v1, v1, p3}, Lmiuix/appcompat/internal/app/widget/a$b;->a(FIILv0/a;)V

    goto :goto_3

    :cond_9
    if-ne p3, v2, :cond_a

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->C:Lmiuix/appcompat/internal/app/widget/a$b;

    iget-object p3, p0, Lmiuix/appcompat/internal/app/widget/a;->c:Lv0/a;

    invoke-virtual {p1, p5, v1, p4, p3}, Lmiuix/appcompat/internal/app/widget/a$b;->a(FIILv0/a;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->I:Lmiuix/appcompat/internal/app/widget/a$b;

    iget-object p3, p0, Lmiuix/appcompat/internal/app/widget/a;->k:Lv0/a;

    invoke-virtual {p1, p2, v1, v1, p3}, Lmiuix/appcompat/internal/app/widget/a$b;->a(FIILv0/a;)V

    goto :goto_3

    :cond_a
    if-nez p3, :cond_b

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->C:Lmiuix/appcompat/internal/app/widget/a$b;

    iget-object p3, p0, Lmiuix/appcompat/internal/app/widget/a;->d:Lv0/a;

    invoke-virtual {p1, p2, v1, v1, p3}, Lmiuix/appcompat/internal/app/widget/a$b;->a(FIILv0/a;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->I:Lmiuix/appcompat/internal/app/widget/a$b;

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/a;->k:Lv0/a;

    invoke-virtual {p1, p5, v1, v1, p2}, Lmiuix/appcompat/internal/app/widget/a$b;->a(FIILv0/a;)V

    :cond_b
    :goto_3
    return-void
.end method

.method public onMeasure(II)V
    .locals 8

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/a;->e:I

    if-lez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result p2

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v1

    add-int/2addr v1, p2

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result p2

    sub-int p2, p1, p2

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v2

    sub-int/2addr p2, v2

    sub-int/2addr v0, v1

    const/high16 v2, -0x80000000

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    const/4 v4, 0x0

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-ne v3, p0, :cond_1

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    invoke-virtual {p0, v3, p2, v0, v4}, Lmiuix/appcompat/internal/app/widget/a;->f(Landroid/view/View;III)I

    move-result p2

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v4

    goto :goto_1

    :cond_1
    move v3, v4

    :goto_1
    iget-object v5, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->S:Landroid/widget/LinearLayout;

    const/4 v6, 0x1

    if-eqz v5, :cond_6

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v5

    const/16 v7, 0x8

    if-eq v5, v7, :cond_6

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {p2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    iget-object v5, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->S:Landroid/widget/LinearLayout;

    invoke-virtual {v5, p2, v0}, Landroid/widget/LinearLayout;->measure(II)V

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->S:Landroid/widget/LinearLayout;

    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result p2

    add-int/2addr v3, p2

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->V:Landroid/widget/TextView;

    invoke-virtual {p2}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    iget-object v5, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->R:Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    iget-object v5, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->V:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    int-to-float v5, v5

    iget-boolean v7, p0, Lmiuix/appcompat/internal/app/widget/a;->l:Z

    if-nez v7, :cond_2

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->getExpandState()I

    move-result v7

    if-eqz v7, :cond_3

    :cond_2
    cmpg-float v0, v0, v5

    if-gtz v0, :cond_4

    :cond_3
    move v0, v6

    goto :goto_2

    :cond_4
    move v0, v4

    :goto_2
    if-eqz v0, :cond_5

    move v0, v4

    goto :goto_3

    :cond_5
    const/4 v0, 0x4

    :goto_3
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_6
    iget p2, p0, Lmiuix/appcompat/internal/app/widget/a;->e:I

    if-gtz p2, :cond_a

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p2

    move v0, v4

    move v2, v0

    :goto_4
    if-ge v0, p2, :cond_8

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v1

    if-le v3, v2, :cond_7

    move v2, v3

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_8
    if-lez v2, :cond_9

    iget p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->E:I

    add-int v4, v2, p2

    :cond_9
    invoke-virtual {p0, p1, v4}, Landroid/view/ViewGroup;->setMeasuredDimension(II)V

    goto :goto_7

    :cond_a
    if-lez v3, :cond_b

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->E:I

    add-int/2addr p2, v0

    goto :goto_5

    :cond_b
    move p2, v4

    :goto_5
    iput p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->D:I

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    invoke-static {p1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p2, v0, v1}, Landroid/widget/FrameLayout;->measure(II)V

    iget p2, p0, Lmiuix/appcompat/internal/app/widget/a;->h:I

    const/4 v0, 0x2

    if-ne p2, v0, :cond_c

    iget p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->D:I

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->L:I

    add-int/2addr p2, v0

    :goto_6
    invoke-virtual {p0, p1, p2}, Landroid/view/ViewGroup;->setMeasuredDimension(II)V

    goto :goto_7

    :cond_c
    if-ne p2, v6, :cond_d

    iget p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->D:I

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, p2

    invoke-virtual {p0, p1, v0}, Landroid/view/ViewGroup;->setMeasuredDimension(II)V

    goto :goto_7

    :cond_d
    iget p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->D:I

    goto :goto_6

    :goto_7
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3

    check-cast p1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$e;

    invoke-virtual {p1}, Landroid/view/View$BaseSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-object v0, p1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$e;->d:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$e;->a:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->q()V

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->A:Landroid/widget/Button;

    if-eqz v1, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x8

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->A:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->B:Li1/a;

    iput-object v0, v1, Li1/a;->h:Ljava/lang/CharSequence;

    iget-boolean v0, p1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$e;->c:Z

    if-eqz v0, :cond_2

    new-instance v0, Lmiuix/appcompat/internal/app/widget/b;

    invoke-direct {v0, p0}, Lmiuix/appcompat/internal/app/widget/b;-><init>(Lmiuix/appcompat/internal/app/widget/a;)V

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    :cond_2
    iget p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$e;->b:I

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->setExpandState(I)V

    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    new-instance v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$e;

    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$e;-><init>(Landroid/os/Parcelable;)V

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->r()Z

    move-result v1

    iput-boolean v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$e;->c:Z

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$e;->d:Ljava/lang/CharSequence;

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->A:Landroid/widget/Button;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$e;->a:Ljava/lang/CharSequence;

    :cond_0
    iget v1, p0, Lmiuix/appcompat/internal/app/widget/a;->h:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    const/4 v1, 0x0

    :cond_1
    iput v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$e;->b:I

    return-object v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    const/4 p1, 0x1

    return p1
.end method

.method public p()Z
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Li1/b;->l(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public q()V
    .locals 7

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->S:Landroid/widget/LinearLayout;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v4, 0x7f0d0039

    invoke-virtual {v0, v4, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->S:Landroid/widget/LinearLayout;

    const v4, 0x1020019

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->y:Landroid/widget/Button;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->S:Landroid/widget/LinearLayout;

    const v4, 0x102001a

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->A:Landroid/widget/Button;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->y:Landroid/widget/Button;

    const v4, 0x3f19999a    # 0.6f

    if-eqz v0, :cond_0

    iget-object v5, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->K:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-array v0, v2, [Landroid/view/View;

    iget-object v5, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->y:Landroid/widget/Button;

    aput-object v5, v0, v3

    invoke-static {v0}, Lu0/a;->f([Landroid/view/View;)Lu0/c;

    move-result-object v0

    check-cast v0, Lu0/a$c;

    invoke-virtual {v0}, Lu0/a$c;->b()Lu0/f;

    move-result-object v0

    new-array v5, v3, [Lu0/f$a;

    check-cast v0, Lw0/f;

    invoke-virtual {v0, v1, v5}, Lw0/f;->v(F[Lu0/f$a;)Lu0/f;

    new-array v5, v3, [Lu0/f$a;

    invoke-virtual {v0, v4, v5}, Lw0/f;->t(F[Lu0/f$a;)Lu0/f;

    iget-object v5, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->y:Landroid/widget/Button;

    new-array v6, v3, [Lv0/a;

    invoke-virtual {v0, v5, v6}, Lw0/f;->p(Landroid/view/View;[Lv0/a;)V

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->A:Landroid/widget/Button;

    if-eqz v0, :cond_1

    iget-object v5, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->K:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-array v0, v2, [Landroid/view/View;

    iget-object v5, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->A:Landroid/widget/Button;

    aput-object v5, v0, v3

    invoke-static {v0}, Lu0/a;->f([Landroid/view/View;)Lu0/c;

    move-result-object v0

    check-cast v0, Lu0/a$c;

    invoke-virtual {v0}, Lu0/a$c;->b()Lu0/f;

    move-result-object v0

    new-array v5, v3, [Lu0/f$a;

    check-cast v0, Lw0/f;

    invoke-virtual {v0, v1, v5}, Lw0/f;->v(F[Lu0/f$a;)Lu0/f;

    new-array v5, v3, [Lu0/f$a;

    invoke-virtual {v0, v4, v5}, Lw0/f;->t(F[Lu0/f$a;)Lu0/f;

    iget-object v4, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->A:Landroid/widget/Button;

    new-array v5, v3, [Lv0/a;

    invoke-virtual {v0, v4, v5}, Lw0/f;->p(Landroid/view/View;[Lv0/a;)V

    :cond_1
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->S:Landroid/widget/LinearLayout;

    const v4, 0x1020016

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->V:Landroid/widget/TextView;

    iget v4, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->U:I

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    iget v5, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->U:I

    invoke-virtual {v0, v4, v5}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    :cond_2
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->G:Landroid/widget/TextView;

    iget v4, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->F:I

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    iget v5, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->F:I

    invoke-virtual {v0, v4, v5}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    :cond_3
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->V:Landroid/widget/TextView;

    iget-object v4, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->R:Ljava/lang/CharSequence;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->G:Landroid/widget/TextView;

    iget-object v4, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->R:Ljava/lang/CharSequence;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->V:Landroid/widget/TextView;

    iget-object v4, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->C:Lmiuix/appcompat/internal/app/widget/a$b;

    invoke-virtual {v4, v0}, Lmiuix/appcompat/internal/app/widget/a$b;->b(Landroid/view/View;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->R:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/2addr v0, v2

    iget-object v4, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->S:Landroid/widget/LinearLayout;

    const/16 v5, 0x8

    if-eqz v0, :cond_4

    move v6, v3

    goto :goto_0

    :cond_4
    move v6, v5

    :goto_0
    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v4, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->G:Landroid/widget/TextView;

    if-eqz v0, :cond_5

    move v5, v3

    :cond_5
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->S:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->S:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_6
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->G:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    iget-object v4, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->G:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    :cond_7
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_8
    iget v0, p0, Lmiuix/appcompat/internal/app/widget/a;->h:I

    const/4 v4, 0x0

    if-nez v0, :cond_9

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->C:Lmiuix/appcompat/internal/app/widget/a$b;

    invoke-virtual {v0, v1, v3, v3}, Lmiuix/appcompat/internal/app/widget/a$b;->d(FII)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->I:Lmiuix/appcompat/internal/app/widget/a$b;

    invoke-virtual {v0, v4, v3, v3}, Lmiuix/appcompat/internal/app/widget/a$b;->d(FII)V

    goto :goto_1

    :cond_9
    if-ne v0, v2, :cond_a

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->C:Lmiuix/appcompat/internal/app/widget/a$b;

    const/16 v2, 0x14

    invoke-virtual {v0, v4, v3, v2}, Lmiuix/appcompat/internal/app/widget/a$b;->d(FII)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->I:Lmiuix/appcompat/internal/app/widget/a$b;

    invoke-virtual {v0, v1, v3, v3}, Lmiuix/appcompat/internal/app/widget/a$b;->d(FII)V

    :cond_a
    :goto_1
    return-void
.end method

.method public r()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Li1/b;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public s(Z)V
    .locals 6

    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    move v2, v0

    goto :goto_0

    :cond_0
    move v2, v1

    :goto_0
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->setAlpha(F)V

    iget-boolean v2, p0, Lmiuix/appcompat/internal/app/widget/a;->n:Z

    if-nez v2, :cond_1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->w(Z)V

    return-void

    :cond_1
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/a;->o:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    invoke-virtual {v3}, Li1/c;->getCollapsedHeight()I

    move-result v3

    iget-object v4, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    if-eqz p1, :cond_2

    move v5, v1

    goto :goto_1

    :cond_2
    int-to-float v5, v3

    :goto_1
    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setTranslationY(F)V

    if-eqz p1, :cond_3

    goto :goto_2

    :cond_3
    const/4 v3, 0x0

    :goto_2
    invoke-virtual {v2, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->a(I)V

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    if-eqz p1, :cond_4

    goto :goto_3

    :cond_4
    move v0, v1

    :goto_3
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setAlpha(F)V

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->w(Z)V

    return-void
.end method

.method public bridge synthetic setActionBarTransitionListener(Ld1/c;)V
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/app/widget/a;->setActionBarTransitionListener(Ld1/c;)V

    return-void
.end method

.method public setActionModeAnim(Z)V
    .locals 0

    iput-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->Q:Z

    return-void
.end method

.method public setAnimationProgress(F)V
    .locals 1

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->x:F

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->u:Z

    invoke-virtual {p0, v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->v(ZF)V

    return-void
.end method

.method public bridge synthetic setContentHeight(I)V
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/app/widget/a;->setContentHeight(I)V

    return-void
.end method

.method public setContentInset(I)V
    .locals 0

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->E:I

    return-void
.end method

.method public bridge synthetic setExpandState(I)V
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/app/widget/a;->setExpandState(I)V

    return-void
.end method

.method public bridge synthetic setResizable(Z)V
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/app/widget/a;->setResizable(Z)V

    return-void
.end method

.method public setSplitActionBar(Z)V
    .locals 6

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/a;->n:Z

    if-eq v0, p1, :cond_5

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    if-eqz v0, :cond_4

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    if-nez p1, :cond_1

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    invoke-virtual {v1, p0}, Li1/b;->i(Landroid/view/ViewGroup;)Lh1/l;

    move-result-object v1

    check-cast v1, Li1/c;

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    invoke-virtual {p0, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Li1/b;->n(IZ)V

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    invoke-static {}, Lr1/b;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v1, 0x11

    goto :goto_0

    :cond_2
    const/16 v1, 0x50

    :goto_0
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    invoke-virtual {v1, p0}, Li1/b;->i(Landroid/view/ViewGroup;)Lh1/l;

    move-result-object v1

    check-cast v1, Li1/c;

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->P:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_3

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_3
    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/a;->o:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    invoke-virtual {v1, v2, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_4
    :goto_1
    invoke-super {p0, p1}, Lmiuix/appcompat/internal/app/widget/a;->setSplitActionBar(Z)V

    :cond_5
    return-void
.end method

.method public bridge synthetic setSplitView(Lmiuix/appcompat/internal/app/widget/ActionBarContainer;)V
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/app/widget/a;->setSplitView(Lmiuix/appcompat/internal/app/widget/ActionBarContainer;)V

    return-void
.end method

.method public bridge synthetic setSplitWhenNarrow(Z)V
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/app/widget/a;->setSplitWhenNarrow(Z)V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->R:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->q()V

    return-void
.end method

.method public setTitleOptional(Z)V
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->T:Z

    if-eq p1, v0, :cond_0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->requestLayout()V

    :cond_0
    iput-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->T:Z

    return-void
.end method

.method public bridge synthetic setVisibility(I)V
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/app/widget/a;->setVisibility(I)V

    return-void
.end method

.method public t(Z)Ld/o;
    .locals 23

    move-object/from16 v9, p0

    move/from16 v10, p1

    iget-boolean v0, v9, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->u:Z

    const/4 v11, 0x2

    if-ne v10, v0, :cond_0

    iget-object v0, v9, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->a0:Ld/o;

    if-eqz v0, :cond_0

    new-instance v0, Ld/o;

    invoke-direct {v0, v11}, Ld/o;-><init>(I)V

    return-object v0

    :cond_0
    iput-boolean v10, v9, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->u:Z

    iget-object v12, v9, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    const/4 v14, 0x0

    if-nez v12, :cond_1

    move v15, v14

    goto :goto_0

    :cond_1
    invoke-virtual {v12}, Li1/c;->getCollapsedHeight()I

    move-result v0

    move v15, v0

    :goto_0
    const/4 v0, 0x0

    if-nez v12, :cond_2

    move/from16 v16, v0

    goto :goto_1

    :cond_2
    invoke-virtual {v12}, Landroid/widget/LinearLayout;->getTranslationY()F

    move-result v1

    move/from16 v16, v1

    :goto_1
    const/high16 v1, 0x3f800000    # 1.0f

    if-eqz v10, :cond_3

    move v8, v0

    move v7, v1

    move v5, v14

    move v6, v15

    goto :goto_2

    :cond_3
    move v7, v0

    move v8, v1

    move v6, v14

    move v5, v15

    :goto_2
    new-instance v4, Ld/o;

    invoke-direct {v4, v11}, Ld/o;-><init>(I)V

    const v0, 0x4476bd71

    if-eqz v10, :cond_4

    const v1, 0x43a1228f

    goto :goto_3

    :cond_4
    move v1, v0

    :goto_3
    invoke-virtual {v9, v9, v1, v8, v7}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->o(Landroid/view/View;FFF)Lz0/f;

    move-result-object v1

    if-eqz v10, :cond_5

    const-wide/16 v2, 0x32

    goto :goto_4

    :cond_5
    const-wide/16 v2, 0x0

    :goto_4
    invoke-virtual {v1, v2, v3}, Lz0/c;->h(J)V

    iget-object v2, v4, Ld/o;->a:Ljava/lang/Object;

    check-cast v2, Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v9, v8}, Landroid/view/ViewGroup;->setAlpha(F)V

    iget-boolean v2, v9, Lmiuix/appcompat/internal/app/widget/a;->n:Z

    if-nez v2, :cond_7

    new-instance v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$d;

    invoke-direct {v0, v9, v10}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$d;-><init>(Lmiuix/appcompat/internal/app/widget/ActionBarContextView;Z)V

    iget-object v2, v1, Lz0/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v1, v1, Lz0/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    iput-object v4, v9, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->a0:Ld/o;

    return-object v4

    :cond_7
    iput-boolean v14, v9, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->t:Z

    if-eqz v10, :cond_8

    const/16 v1, 0x64

    move v3, v1

    goto :goto_5

    :cond_8
    move v3, v14

    :goto_5
    if-eqz v10, :cond_9

    const v0, 0x43db5333    # 438.65f

    :cond_9
    move v2, v0

    new-instance v1, Lz0/f;

    new-instance v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$b;

    const-string v17, ""

    move-object/from16 v18, v0

    move-object v14, v1

    move-object/from16 v1, p0

    move v11, v2

    move-object/from16 v2, v17

    move v9, v3

    move-object v3, v12

    move-object/from16 v19, v4

    move/from16 v4, v16

    move/from16 v17, v5

    move v5, v15

    move/from16 v20, v6

    move/from16 v6, p1

    move/from16 v21, v7

    move/from16 v7, v20

    move/from16 v22, v8

    move/from16 v8, v17

    invoke-direct/range {v0 .. v8}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$b;-><init>(Lmiuix/appcompat/internal/app/widget/ActionBarContextView;Ljava/lang/String;Li1/c;FIZII)V

    move/from16 v0, v20

    int-to-float v0, v0

    move-object/from16 v1, v18

    invoke-direct {v14, v13, v1, v0}, Lz0/f;-><init>(Ljava/lang/Object;La1/b;F)V

    move/from16 v0, v17

    int-to-float v1, v0

    iput v1, v14, Lz0/c;->k:F

    const/4 v2, 0x1

    iput-boolean v2, v14, Lz0/c;->h:Z

    iget-object v3, v14, Lz0/f;->o:Lz0/g;

    invoke-virtual {v3, v11}, Lz0/g;->b(F)Lz0/g;

    iget-object v3, v14, Lz0/f;->o:Lz0/g;

    const v4, 0x3f666666    # 0.9f

    invoke-virtual {v3, v4}, Lz0/g;->a(F)Lz0/g;

    int-to-long v3, v9

    invoke-virtual {v14, v3, v4}, Lz0/c;->h(J)V

    new-instance v5, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$d;

    move-object/from16 v6, p0

    invoke-direct {v5, v6, v10}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$d;-><init>(Lmiuix/appcompat/internal/app/widget/ActionBarContextView;Z)V

    iget-object v7, v14, Lz0/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a

    iget-object v7, v14, Lz0/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_a
    if-eqz v12, :cond_b

    int-to-float v5, v15

    add-float v16, v16, v5

    sub-float v1, v16, v1

    invoke-virtual {v12, v1}, Landroid/widget/LinearLayout;->setTranslationY(F)V

    :cond_b
    invoke-virtual {v13, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->a(I)V

    if-eqz v12, :cond_e

    move/from16 v1, v21

    move/from16 v0, v22

    invoke-virtual {v6, v12, v11, v0, v1}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->o(Landroid/view/View;FFF)Lz0/f;

    move-result-object v1

    invoke-virtual {v1, v3, v4}, Lz0/c;->h(J)V

    invoke-virtual {v12, v0}, Landroid/widget/LinearLayout;->setAlpha(F)V

    const/4 v0, 0x2

    new-array v3, v0, [Lz0/f;

    const/4 v4, 0x0

    aput-object v14, v3, v4

    aput-object v1, v3, v2

    move v14, v4

    :goto_6
    if-ge v14, v0, :cond_d

    aget-object v1, v3, v14

    move-object/from16 v2, v19

    if-eqz v1, :cond_c

    iget-object v4, v2, Ld/o;->a:Ljava/lang/Object;

    check-cast v4, Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_c
    add-int/lit8 v14, v14, 0x1

    move-object/from16 v19, v2

    goto :goto_6

    :cond_d
    move-object/from16 v2, v19

    goto :goto_7

    :cond_e
    move-object/from16 v2, v19

    iget-object v0, v2, Ld/o;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_7
    iput-object v2, v6, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->a0:Ld/o;

    return-object v2
.end method

.method public u(Z)V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->v:Ljava/util/List;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/view/a;

    invoke-interface {v1, p1}, Lmiuix/view/a;->g(Z)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public v(ZF)V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->v:Ljava/util/List;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/view/a;

    invoke-interface {v1, p1, p2}, Lmiuix/view/a;->f(ZF)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final w(Z)V
    .locals 3

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->u(Z)V

    const/16 v0, 0x8

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    move v2, v1

    goto :goto_0

    :cond_0
    move v2, v0

    :goto_0
    invoke-virtual {p0, v2}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->setVisibility(I)V

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/a;->o:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    if-eqz v2, :cond_2

    if-eqz p1, :cond_1

    move v0, v1

    :cond_1
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_2
    return-void
.end method
