.class public Lmiuix/appcompat/internal/app/widget/ActionBarView$i;
.super Ly0/b;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/appcompat/internal/app/widget/ActionBarView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "i"
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lmiuix/appcompat/internal/app/widget/ActionBarView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lmiuix/appcompat/internal/app/widget/ActionBarView;I)V
    .locals 1

    invoke-direct {p0}, Ly0/b;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$i;->b:Ljava/lang/ref/WeakReference;

    iput p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$i;->a:I

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 1

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$i;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-boolean v0, p1, Lmiuix/appcompat/internal/app/widget/a;->l:Z

    iput-boolean v0, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->z0:Z

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setResizable(Z)V

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setExpandState(I)V

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t:Lmiuix/appcompat/internal/app/widget/a$b;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/a$b;->e(I)V

    return-void
.end method

.method public c(Ljava/lang/Object;)V
    .locals 1

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$i;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$i;->a:I

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setExpandState(I)V

    iget-boolean v0, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->z0:Z

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setResizable(Z)V

    iget-boolean v0, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->W:Z

    if-eqz v0, :cond_1

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t:Lmiuix/appcompat/internal/app/widget/a$b;

    const/4 v0, 0x4

    goto :goto_0

    :cond_1
    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t:Lmiuix/appcompat/internal/app/widget/a$b;

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/a$b;->e(I)V

    return-void
.end method

.method public e(Ljava/lang/Object;La1/c;IFZ)V
    .locals 0

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$i;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;

    if-nez p1, :cond_0

    return-void

    :cond_0
    iput p3, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->p0:I

    invoke-virtual {p1}, Landroid/view/ViewGroup;->requestLayout()V

    return-void
.end method
