.class public abstract Lmiuix/appcompat/internal/app/widget/a;
.super Landroid/view/ViewGroup;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/appcompat/internal/app/widget/a$b;
    }
.end annotation


# instance fields
.field public a:Li1/b;

.field public b:Ly0/b;

.field public c:Lv0/a;

.field public d:Lv0/a;

.field public e:I

.field public f:I

.field public g:Lv0/a;

.field public h:I

.field public i:F

.field public j:Li1/c;

.field public k:Lv0/a;

.field public l:Z

.field public m:Lv0/a;

.field public n:Z

.field public o:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

.field public p:Z

.field public q:Ld1/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lmiuix/appcompat/internal/app/widget/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p3, 0x1

    iput p3, p0, Lmiuix/appcompat/internal/app/widget/a;->h:I

    iput p3, p0, Lmiuix/appcompat/internal/app/widget/a;->f:I

    iput-boolean p3, p0, Lmiuix/appcompat/internal/app/widget/a;->l:Z

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/a;->i:F

    new-instance v0, Lmiuix/appcompat/internal/app/widget/a$a;

    invoke-direct {v0, p0}, Lmiuix/appcompat/internal/app/widget/a$a;-><init>(Lmiuix/appcompat/internal/app/widget/a;)V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->b:Ly0/b;

    new-instance v0, Lv0/a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lv0/a;-><init>(Z)V

    const/4 v2, 0x2

    new-array v3, v2, [F

    fill-array-data v3, :array_0

    const/4 v4, -0x2

    invoke-virtual {v0, v4, v3}, Lv0/a;->f(I[F)Lv0/a;

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->d:Lv0/a;

    new-instance v0, Lv0/a;

    invoke-direct {v0, v1}, Lv0/a;-><init>(Z)V

    new-array v3, v2, [F

    fill-array-data v3, :array_1

    invoke-virtual {v0, v4, v3}, Lv0/a;->f(I[F)Lv0/a;

    new-array v3, p3, [Ly0/b;

    iget-object v5, p0, Lmiuix/appcompat/internal/app/widget/a;->b:Ly0/b;

    aput-object v5, v3, v1

    iget-object v5, v0, Lv0/a;->e:Ljava/util/HashSet;

    invoke-static {v5, v3}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->m:Lv0/a;

    new-instance v0, Lv0/a;

    invoke-direct {v0, v1}, Lv0/a;-><init>(Z)V

    new-array v3, v2, [F

    fill-array-data v3, :array_2

    invoke-virtual {v0, v4, v3}, Lv0/a;->f(I[F)Lv0/a;

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->c:Lv0/a;

    new-instance v0, Lv0/a;

    invoke-direct {v0, v1}, Lv0/a;-><init>(Z)V

    new-array v3, v2, [F

    fill-array-data v3, :array_3

    invoke-virtual {v0, v4, v3}, Lv0/a;->f(I[F)Lv0/a;

    new-array v3, p3, [Ly0/b;

    iget-object v5, p0, Lmiuix/appcompat/internal/app/widget/a;->b:Ly0/b;

    aput-object v5, v3, v1

    iget-object v5, v0, Lv0/a;->e:Ljava/util/HashSet;

    invoke-static {v5, v3}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->g:Lv0/a;

    new-instance v0, Lv0/a;

    invoke-direct {v0, v1}, Lv0/a;-><init>(Z)V

    new-array v3, v2, [F

    fill-array-data v3, :array_4

    invoke-virtual {v0, v4, v3}, Lv0/a;->f(I[F)Lv0/a;

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->k:Lv0/a;

    sget-object v0, Landroidx/emoji2/text/l;->T:[I

    const v3, 0x10102ce

    invoke-virtual {p1, p2, v0, v3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    const/16 p2, 0x21

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    const/16 v0, 0x30

    invoke-virtual {p1, v0, p3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    if-eqz p2, :cond_1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    iget p1, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne p1, v2, :cond_0

    invoke-static {}, Lr1/b;->a()Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    iput p3, p0, Lmiuix/appcompat/internal/app/widget/a;->h:I

    iput p3, p0, Lmiuix/appcompat/internal/app/widget/a;->f:I

    goto :goto_1

    :cond_1
    :goto_0
    iput v1, p0, Lmiuix/appcompat/internal/app/widget/a;->h:I

    iput v1, p0, Lmiuix/appcompat/internal/app/widget/a;->f:I

    :goto_1
    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/a;->l:Z

    return-void

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3e99999a    # 0.3f
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3e99999a    # 0.3f
    .end array-data

    :array_2
    .array-data 4
        0x3f800000    # 1.0f
        0x3e19999a    # 0.15f
    .end array-data

    :array_3
    .array-data 4
        0x3f800000    # 1.0f
        0x3e19999a    # 0.15f
    .end array-data

    :array_4
    .array-data 4
        0x3f800000    # 1.0f
        0x3f19999a    # 0.6f
    .end array-data
.end method


# virtual methods
.method public f(Landroid/view/View;III)I
    .locals 1

    const/high16 v0, -0x80000000

    invoke-static {p2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p1, v0, p3}, Landroid/view/View;->measure(II)V

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result p1

    sub-int/2addr p2, p1

    sub-int/2addr p2, p4

    const/4 p1, 0x0

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result p1

    return p1
.end method

.method public g(II)V
    .locals 0

    return-void
.end method

.method public getActionBarStyle()I
    .locals 1

    const v0, 0x10102ce

    return v0
.end method

.method public getActionBarTransitionListener()Ld1/c;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->q:Ld1/c;

    return-object v0
.end method

.method public getActionMenuView()Li1/c;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    return-object v0
.end method

.method public getAnimatedVisibility()I
    .locals 1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    return v0
.end method

.method public getContentHeight()I
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/a;->e:I

    return v0
.end method

.method public getExpandState()I
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/a;->f:I

    return v0
.end method

.method public getMenuView()Li1/c;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    return-object v0
.end method

.method public h(II)V
    .locals 0

    return-void
.end method

.method public i(Landroid/view/View;III)I
    .locals 9

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    const/4 v2, 0x2

    invoke-static {p4, v1, v2, p3}, Landroidx/activity/result/a;->c(IIII)I

    move-result v6

    add-int v7, p2, v0

    add-int v8, v6, v1

    move-object v3, p0

    move-object v4, p1

    move v5, p2

    invoke-static/range {v3 .. v8}, Landroidx/emoji2/text/l;->J(Landroid/view/ViewGroup;Landroid/view/View;IIII)V

    return v0
.end method

.method public j(Landroid/view/View;III)I
    .locals 9

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    const/4 v2, 0x2

    invoke-static {p4, v1, v2, p3}, Landroidx/activity/result/a;->c(IIII)I

    move-result v6

    sub-int v5, p2, v0

    add-int v8, v6, v1

    move-object v3, p0

    move-object v4, p1

    move v7, p2

    invoke-static/range {v3 .. v8}, Landroidx/emoji2/text/l;->J(Landroid/view/ViewGroup;Landroid/view/View;IIII)V

    return v0
.end method

.method public k(IZ)V
    .locals 2

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    if-eqz p1, :cond_0

    invoke-static {}, Lr1/b;->a()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/a;->l:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/a;->h:I

    if-eq v0, p1, :cond_4

    if-eqz p2, :cond_1

    invoke-virtual {p0, v0, p1}, Lmiuix/appcompat/internal/app/widget/a;->g(II)V

    goto :goto_2

    :cond_1
    iput p1, p0, Lmiuix/appcompat/internal/app/widget/a;->h:I

    if-nez p1, :cond_2

    const/4 p2, 0x0

    :goto_0
    iput p2, p0, Lmiuix/appcompat/internal/app/widget/a;->f:I

    goto :goto_1

    :cond_2
    const/4 p2, 0x1

    if-ne p1, p2, :cond_3

    goto :goto_0

    :cond_3
    :goto_1
    invoke-virtual {p0, v0, p1}, Lmiuix/appcompat/internal/app/widget/a;->h(II)V

    invoke-virtual {p0}, Landroid/view/ViewGroup;->requestLayout()V

    :cond_4
    :goto_2
    return-void
.end method

.method public l()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Li1/b;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    sget-object v0, Landroidx/emoji2/text/l;->T:[I

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/a;->getActionBarStyle()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v0, v1, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/app/widget/a;->setContentHeight(I)V

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    iget-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/a;->p:Z

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x7f050004

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result p1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/a;->setSplitActionBar(Z)V

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    if-eqz p1, :cond_1

    iget-object v0, p1, Lh1/a;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p1, Li1/b;->n:I

    iget-object p1, p1, Lh1/a;->d:Lh1/g;

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lh1/a;->g(Lh1/g;Z)V

    :cond_1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    iget p1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    invoke-static {}, Lr1/b;->a()Z

    move-result p1

    if-nez p1, :cond_2

    invoke-virtual {p0, v3}, Lmiuix/appcompat/internal/app/widget/a;->setExpandState(I)V

    :cond_2
    return-void
.end method

.method public setActionBarTransitionListener(Ld1/c;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/a;->q:Ld1/c;

    return-void
.end method

.method public setContentHeight(I)V
    .locals 0

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/a;->e:I

    invoke-virtual {p0}, Landroid/view/ViewGroup;->requestLayout()V

    return-void
.end method

.method public setExpandState(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lmiuix/appcompat/internal/app/widget/a;->k(IZ)V

    return-void
.end method

.method public setResizable(Z)V
    .locals 0

    iput-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/a;->l:Z

    return-void
.end method

.method public setSplitActionBar(Z)V
    .locals 0

    iput-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/a;->n:Z

    return-void
.end method

.method public setSplitView(Lmiuix/appcompat/internal/app/widget/ActionBarContainer;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/a;->o:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    return-void
.end method

.method public setSplitWhenNarrow(Z)V
    .locals 0

    iput-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/a;->p:Z

    return-void
.end method

.method public setVisibility(I)V
    .locals 1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-eq p1, v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_0
    return-void
.end method
