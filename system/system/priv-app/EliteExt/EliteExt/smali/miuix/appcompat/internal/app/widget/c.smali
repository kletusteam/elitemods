.class public Lmiuix/appcompat/internal/app/widget/c;
.super Ld1/a;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/appcompat/internal/app/widget/c$b;
    }
.end annotation


# instance fields
.field public a:Landroid/view/ActionMode;

.field public b:Lg1/a$a;

.field public c:Lmiuix/appcompat/internal/app/widget/f;

.field public d:Lmiuix/appcompat/internal/app/widget/ActionBarView;

.field public e:Lw0/h;

.field public f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

.field public g:Landroid/view/View;

.field public h:Landroid/view/View$OnClickListener;

.field public i:Landroid/view/View;

.field public j:Landroid/content/Context;

.field public k:I

.field public l:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

.field public m:I

.field public n:I

.field public o:I

.field public p:Z

.field public q:Z

.field public r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

.field public s:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

.field public t:Z

.field public u:Z

.field public v:Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

.field public w:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

.field public x:Lw0/h;

.field public y:Landroid/content/Context;


# direct methods
.method public constructor <init>(Ld1/j;Landroid/view/ViewGroup;)V
    .locals 5

    invoke-direct {p0}, Ld1/a;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/c;->q:Z

    new-instance v1, Lmiuix/appcompat/internal/app/widget/c$a;

    invoke-direct {v1, p0}, Lmiuix/appcompat/internal/app/widget/c$a;-><init>(Lmiuix/appcompat/internal/app/widget/c;)V

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/c;->b:Lg1/a$a;

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/c;->j:Landroid/content/Context;

    iget-object v1, p1, Landroidx/fragment/app/i;->h:Landroidx/fragment/app/l;

    iget-object v1, v1, Landroidx/fragment/app/l;->a:Landroidx/fragment/app/n;

    iget-object v1, v1, Landroidx/fragment/app/n;->d:Landroidx/fragment/app/q;

    move-object v1, p2

    check-cast v1, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/c;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {v1, p0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->setActionBar(Ld1/a;)V

    const v1, 0x7f0a0027

    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/c;->d:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    const v1, 0x7f0a0035

    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/c;->l:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    const v1, 0x7f0a002b

    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/c;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    const v1, 0x7f0a00e5

    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/c;->w:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    const v1, 0x7f0a0060

    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p2

    iput-object p2, p0, Lmiuix/appcompat/internal/app/widget/c;->g:Landroid/view/View;

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/c;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {p2}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->getContentView()Landroid/view/View;

    move-result-object p2

    iput-object p2, p0, Lmiuix/appcompat/internal/app/widget/c;->i:Landroid/view/View;

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/c;->g:Landroid/view/View;

    if-eqz p2, :cond_0

    new-instance p2, Lmiuix/appcompat/internal/app/widget/d;

    invoke-direct {p2, p0}, Lmiuix/appcompat/internal/app/widget/d;-><init>(Lmiuix/appcompat/internal/app/widget/c;)V

    iput-object p2, p0, Lmiuix/appcompat/internal/app/widget/c;->h:Landroid/view/View$OnClickListener;

    :cond_0
    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/c;->d:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    if-nez p2, :cond_2

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/c;->l:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    if-nez v1, :cond_2

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/c;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-class p2, Lmiuix/appcompat/internal/app/widget/c;

    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " can only be used "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "with a compatible window decor layout"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_2
    :goto_0
    iget-boolean v1, p2, Lmiuix/appcompat/internal/app/widget/a;->n:Z

    iput v1, p0, Lmiuix/appcompat/internal/app/widget/c;->k:I

    invoke-virtual {p2}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getDisplayOptions()I

    move-result p2

    and-int/lit8 p2, p2, 0x4

    const/4 v1, 0x0

    if-eqz p2, :cond_3

    move p2, v0

    goto :goto_1

    :cond_3
    move p2, v1

    :goto_1
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/c;->j:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget v3, v3, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v4, 0xe

    if-ge v3, v4, :cond_4

    move v3, v0

    goto :goto_2

    :cond_4
    move v3, v1

    :goto_2
    if-nez v3, :cond_6

    if-eqz p2, :cond_5

    goto :goto_3

    :cond_5
    move v0, v1

    :cond_6
    :goto_3
    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/c;->d:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {p2, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setHomeButtonEnabled(Z)V

    const p2, 0x7f04000c

    invoke-static {v2, p2, v1}, Lr1/a;->c(Landroid/content/Context;IZ)Z

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/c;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setTabContainer(Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;)V

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/c;->d:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iput-boolean v1, p2, Lmiuix/appcompat/internal/app/widget/ActionBarView;->a0:Z

    invoke-virtual {p2}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getNavigationMode()I

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/c;->d:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {p2, v1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setCollapsable(Z)V

    invoke-virtual {p1}, Landroid/app/Activity;->getTitle()Ljava/lang/CharSequence;

    move-result-object p1

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/c;->d:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {p2, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public c()I
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->d:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getDisplayOptions()I

    move-result v0

    return v0
.end method

.method public d()Landroid/content/Context;
    .locals 4

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->y:Landroid/content/Context;

    if-nez v0, :cond_1

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/c;->j:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x1010397

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    if-eqz v0, :cond_0

    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/c;->j:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/c;->y:Landroid/content/Context;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->j:Landroid/content/Context;

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->y:Landroid/content/Context;

    :cond_1
    :goto_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->y:Landroid/content/Context;

    return-object v0
.end method

.method public e(Landroid/content/res/Configuration;)V
    .locals 2

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/c;->j:Landroid/content/Context;

    const v0, 0x7f04000c

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lr1/a;->c(Landroid/content/Context;IZ)Z

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/c;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setTabContainer(Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/c;->d:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iput-boolean v1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->a0:Z

    invoke-virtual {p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getNavigationMode()I

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/c;->d:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {p1, v1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setCollapsable(Z)V

    return-void
.end method

.method public h(Z)V
    .locals 1

    iput-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/c;->t:Z

    if-nez p1, :cond_1

    iget-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/c;->q:Z

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/app/widget/c;->m(Z)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/app/widget/c;->l(Z)V

    :cond_1
    :goto_0
    return-void
.end method

.method public k(Z)V
    .locals 8

    const/4 v0, 0x0

    const/4 v1, 0x1

    const v2, 0x8000

    const/4 v3, 0x0

    if-eqz p1, :cond_7

    iget-boolean v4, p0, Lmiuix/appcompat/internal/app/widget/c;->u:Z

    if-nez v4, :cond_11

    iput-boolean v1, p0, Lmiuix/appcompat/internal/app/widget/c;->u:Z

    iget-boolean v4, p0, Lmiuix/appcompat/internal/app/widget/c;->q:Z

    if-nez v4, :cond_0

    iput-boolean v1, p0, Lmiuix/appcompat/internal/app/widget/c;->q:Z

    invoke-virtual {p0, v3}, Lmiuix/appcompat/internal/app/widget/c;->m(Z)V

    :cond_0
    iget-object v4, p0, Lmiuix/appcompat/internal/app/widget/c;->d:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v4}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getExpandState()I

    move-result v4

    iput v4, p0, Lmiuix/appcompat/internal/app/widget/c;->o:I

    iget-object v5, p0, Lmiuix/appcompat/internal/app/widget/c;->d:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-boolean v6, v5, Lmiuix/appcompat/internal/app/widget/a;->l:Z

    iput-boolean v6, p0, Lmiuix/appcompat/internal/app/widget/c;->p:Z

    iget-object v6, p0, Lmiuix/appcompat/internal/app/widget/c;->c:Lmiuix/appcompat/internal/app/widget/f;

    instance-of v7, v6, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    if-eqz v7, :cond_1

    invoke-virtual {v5, v3, v1}, Lmiuix/appcompat/internal/app/widget/a;->k(IZ)V

    iget-object v4, p0, Lmiuix/appcompat/internal/app/widget/c;->d:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v4, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setResizable(Z)V

    goto :goto_0

    :cond_1
    check-cast v6, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    invoke-virtual {v6, v4}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->setExpandState(I)V

    iget-object v4, p0, Lmiuix/appcompat/internal/app/widget/c;->c:Lmiuix/appcompat/internal/app/widget/f;

    check-cast v4, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    iget-boolean v5, p0, Lmiuix/appcompat/internal/app/widget/c;->p:Z

    invoke-virtual {v4, v5}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->setResizable(Z)V

    :goto_0
    iget-object v4, p0, Lmiuix/appcompat/internal/app/widget/c;->d:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getImportantForAccessibility()I

    move-result v4

    iput v4, p0, Lmiuix/appcompat/internal/app/widget/c;->m:I

    iget-object v4, p0, Lmiuix/appcompat/internal/app/widget/c;->d:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setImportantForAccessibility(I)V

    iget-object v4, p0, Lmiuix/appcompat/internal/app/widget/c;->i:Landroid/view/View;

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Landroid/view/View;->getImportantForAccessibility()I

    move-result v4

    iput v4, p0, Lmiuix/appcompat/internal/app/widget/c;->n:I

    iget-object v4, p0, Lmiuix/appcompat/internal/app/widget/c;->i:Landroid/view/View;

    invoke-virtual {v4, v5}, Landroid/view/View;->setImportantForAccessibility(I)V

    :cond_2
    iget-object v4, p0, Lmiuix/appcompat/internal/app/widget/c;->d:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object v5, p0, Lmiuix/appcompat/internal/app/widget/c;->c:Lmiuix/appcompat/internal/app/widget/f;

    instance-of v5, v5, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/c;->c()I

    move-result v6

    and-int/2addr v2, v6

    if-eqz v2, :cond_3

    move v2, v1

    goto :goto_1

    :cond_3
    move v2, v3

    :goto_1
    iput-boolean v1, v4, Lmiuix/appcompat/internal/app/widget/ActionBarView;->V:Z

    iput-boolean v5, v4, Lmiuix/appcompat/internal/app/widget/ActionBarView;->W:Z

    iget-object v1, v4, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t:Lmiuix/appcompat/internal/app/widget/a$b;

    invoke-virtual {v1, v0}, Lmiuix/appcompat/internal/app/widget/a$b;->c(F)V

    iget-object v1, v4, Lmiuix/appcompat/internal/app/widget/ActionBarView;->j0:Lmiuix/appcompat/internal/app/widget/a$b;

    invoke-virtual {v1, v0}, Lmiuix/appcompat/internal/app/widget/a$b;->c(F)V

    iget-object v1, v4, Lmiuix/appcompat/internal/app/widget/ActionBarView;->w0:Landroid/view/View;

    if-eqz v1, :cond_4

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    :cond_4
    iget-object v1, v4, Lmiuix/appcompat/internal/app/widget/ActionBarView;->H:Landroid/view/View;

    if-eqz v1, :cond_5

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    :cond_5
    iget-object v1, v4, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E0:Landroid/view/View;

    if-eqz v1, :cond_6

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    :cond_6
    if-eqz v2, :cond_11

    iget-object v0, v4, Lmiuix/appcompat/internal/app/widget/ActionBarView;->j0:Lmiuix/appcompat/internal/app/widget/a$b;

    iput-boolean v3, v0, Lmiuix/appcompat/internal/app/widget/a$b;->b:Z

    iget-object v0, v4, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t:Lmiuix/appcompat/internal/app/widget/a$b;

    iput-boolean v3, v0, Lmiuix/appcompat/internal/app/widget/a$b;->b:Z

    goto/16 :goto_5

    :cond_7
    iget-boolean v4, p0, Lmiuix/appcompat/internal/app/widget/c;->u:Z

    if-eqz v4, :cond_11

    iput-boolean v3, p0, Lmiuix/appcompat/internal/app/widget/c;->u:Z

    iget-object v4, p0, Lmiuix/appcompat/internal/app/widget/c;->d:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/c;->c()I

    move-result v5

    and-int/2addr v2, v5

    if-eqz v2, :cond_8

    move v2, v1

    goto :goto_2

    :cond_8
    move v2, v3

    :goto_2
    iput-boolean v3, v4, Lmiuix/appcompat/internal/app/widget/ActionBarView;->V:Z

    iput-boolean v3, v4, Lmiuix/appcompat/internal/app/widget/ActionBarView;->W:Z

    invoke-virtual {v4}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getExpandState()I

    move-result v5

    const/high16 v6, 0x3f800000    # 1.0f

    if-nez v5, :cond_9

    iget-object v5, v4, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t:Lmiuix/appcompat/internal/app/widget/a$b;

    invoke-virtual {v5, v6}, Lmiuix/appcompat/internal/app/widget/a$b;->c(F)V

    iget-object v5, v4, Lmiuix/appcompat/internal/app/widget/ActionBarView;->j0:Lmiuix/appcompat/internal/app/widget/a$b;

    invoke-virtual {v5, v0}, Lmiuix/appcompat/internal/app/widget/a$b;->c(F)V

    goto :goto_3

    :cond_9
    invoke-virtual {v4}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getExpandState()I

    move-result v5

    if-ne v5, v1, :cond_a

    iget-object v5, v4, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t:Lmiuix/appcompat/internal/app/widget/a$b;

    invoke-virtual {v5, v0}, Lmiuix/appcompat/internal/app/widget/a$b;->c(F)V

    iget-object v0, v4, Lmiuix/appcompat/internal/app/widget/ActionBarView;->j0:Lmiuix/appcompat/internal/app/widget/a$b;

    invoke-virtual {v0, v6}, Lmiuix/appcompat/internal/app/widget/a$b;->c(F)V

    :cond_a
    :goto_3
    iget-object v0, v4, Lmiuix/appcompat/internal/app/widget/ActionBarView;->w0:Landroid/view/View;

    if-eqz v0, :cond_b

    invoke-virtual {v0, v6}, Landroid/view/View;->setAlpha(F)V

    :cond_b
    iget-object v0, v4, Lmiuix/appcompat/internal/app/widget/ActionBarView;->H:Landroid/view/View;

    if-eqz v0, :cond_c

    invoke-virtual {v0, v6}, Landroid/view/View;->setAlpha(F)V

    :cond_c
    iget-object v0, v4, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E0:Landroid/view/View;

    if-eqz v0, :cond_d

    invoke-virtual {v0, v6}, Landroid/view/View;->setAlpha(F)V

    :cond_d
    if-eqz v2, :cond_e

    iget-object v0, v4, Lmiuix/appcompat/internal/app/widget/ActionBarView;->j0:Lmiuix/appcompat/internal/app/widget/a$b;

    iput-boolean v1, v0, Lmiuix/appcompat/internal/app/widget/a$b;->b:Z

    iget-object v0, v4, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t:Lmiuix/appcompat/internal/app/widget/a$b;

    iput-boolean v1, v0, Lmiuix/appcompat/internal/app/widget/a$b;->b:Z

    :cond_e
    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/c;->q:Z

    if-nez v0, :cond_f

    iput-boolean v1, p0, Lmiuix/appcompat/internal/app/widget/c;->q:Z

    invoke-virtual {p0, v3}, Lmiuix/appcompat/internal/app/widget/c;->m(Z)V

    :cond_f
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->d:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0, v1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setResizable(Z)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->c:Lmiuix/appcompat/internal/app/widget/f;

    instance-of v2, v0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    if-eqz v2, :cond_10

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/c;->o:I

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/c;->d:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v2, v0, v1}, Lmiuix/appcompat/internal/app/widget/a;->k(IZ)V

    goto :goto_4

    :cond_10
    check-cast v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->getExpandState()I

    move-result v0

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/c;->o:I

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/c;->c:Lmiuix/appcompat/internal/app/widget/f;

    check-cast v1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    iget-boolean v1, v1, Lmiuix/appcompat/internal/app/widget/a;->l:Z

    iput-boolean v1, p0, Lmiuix/appcompat/internal/app/widget/c;->p:Z

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/c;->d:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v1, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setExpandState(I)V

    :goto_4
    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/c;->p:Z

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/c;->d:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v1, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setResizable(Z)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->d:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget v1, p0, Lmiuix/appcompat/internal/app/widget/c;->m:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setImportantForAccessibility(I)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->i:Landroid/view/View;

    if-eqz v0, :cond_11

    iget v1, p0, Lmiuix/appcompat/internal/app/widget/c;->n:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setImportantForAccessibility(I)V

    :cond_11
    :goto_5
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->c:Lmiuix/appcompat/internal/app/widget/f;

    invoke-interface {v0, p1}, Lmiuix/appcompat/internal/app/widget/f;->e(Z)V

    return-void
.end method

.method public l(Z)V
    .locals 6

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->e:Lw0/h;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lw0/h;->g()Lw0/a;

    move-result-object v0

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/c;->e:Lw0/h;

    invoke-interface {v2}, Lu0/d;->cancel()V

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    iget-boolean v2, p0, Lmiuix/appcompat/internal/app/widget/c;->t:Z

    const/4 v3, 0x0

    if-nez v2, :cond_2

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    move p1, v3

    goto :goto_2

    :cond_2
    :goto_1
    const/4 p1, 0x1

    :goto_2
    const/16 v2, 0x8

    const/4 v4, 0x0

    if-eqz p1, :cond_3

    const-string v5, "HideActionBar"

    invoke-virtual {p0, v3, v5, v0}, Lmiuix/appcompat/internal/app/widget/c;->o(ZLjava/lang/String;Lw0/a;)Lw0/h;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->e:Lw0/h;

    goto :goto_3

    :cond_3
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v5

    neg-int v5, v5

    int-to-float v5, v5

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v0, v4}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setAlpha(F)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v0, v2}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setVisibility(I)V

    :goto_3
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->w:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->x:Lw0/h;

    if-eqz v0, :cond_4

    invoke-interface {v0}, Lw0/h;->g()Lw0/a;

    move-result-object v1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->x:Lw0/h;

    invoke-interface {v0}, Lu0/d;->cancel()V

    :cond_4
    if-eqz p1, :cond_5

    const-string p1, "SpliterHide"

    invoke-virtual {p0, v3, p1, v1}, Lmiuix/appcompat/internal/app/widget/c;->p(ZLjava/lang/String;Lw0/a;)Lw0/h;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/c;->x:Lw0/h;

    goto :goto_4

    :cond_5
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/c;->w:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/c;->n()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/c;->w:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p1, v4}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setAlpha(F)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/c;->w:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p1, v2}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setVisibility(I)V

    :goto_4
    invoke-virtual {p0, v3}, Lmiuix/appcompat/internal/app/widget/c;->q(Z)V

    :cond_6
    return-void
.end method

.method public m(Z)V
    .locals 7

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->e:Lw0/h;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lw0/h;->g()Lw0/a;

    move-result-object v0

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/c;->e:Lw0/h;

    invoke-interface {v2}, Lu0/d;->cancel()V

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    iget-boolean v2, p0, Lmiuix/appcompat/internal/app/widget/c;->t:Z

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-nez v2, :cond_2

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    move p1, v3

    goto :goto_2

    :cond_2
    :goto_1
    move p1, v4

    :goto_2
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/c;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v2, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setVisibility(I)V

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    if-eqz p1, :cond_3

    const-string v6, "ShowActionBar"

    invoke-virtual {p0, v4, v6, v0}, Lmiuix/appcompat/internal/app/widget/c;->o(ZLjava/lang/String;Lw0/a;)Lw0/h;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->e:Lw0/h;

    goto :goto_3

    :cond_3
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v0, v2}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setAlpha(F)V

    :goto_3
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->w:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->x:Lw0/h;

    if-eqz v0, :cond_4

    invoke-interface {v0}, Lw0/h;->g()Lw0/a;

    move-result-object v1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->x:Lw0/h;

    invoke-interface {v0}, Lu0/d;->cancel()V

    :cond_4
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->w:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v0, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setVisibility(I)V

    if-eqz p1, :cond_5

    const-string p1, "SpliterShow"

    invoke-virtual {p0, v4, p1, v1}, Lmiuix/appcompat/internal/app/widget/c;->p(ZLjava/lang/String;Lw0/a;)Lw0/h;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/c;->x:Lw0/h;

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/c;->d:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-boolean p1, p1, Lmiuix/appcompat/internal/app/widget/a;->n:Z

    if-eqz p1, :cond_6

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/c;->w:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result p1

    if-lez p1, :cond_6

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/c;->w:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p1, v3}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_6

    instance-of v0, p1, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    if-eqz v0, :cond_6

    move-object v0, p1

    check-cast v0, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;->k()Z

    move-result v0

    xor-int/2addr v0, v4

    if-eqz v0, :cond_6

    check-cast p1, Li1/c;

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->startLayoutAnimation()V

    goto :goto_4

    :cond_5
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/c;->w:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p1, v5}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/c;->w:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p1, v2}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setAlpha(F)V

    :cond_6
    :goto_4
    invoke-virtual {p0, v4}, Lmiuix/appcompat/internal/app/widget/c;->q(Z)V

    :cond_7
    return-void
.end method

.method public final n()I
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->w:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v0

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/c;->w:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/c;->w:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    instance-of v2, v1, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    if-eqz v2, :cond_0

    check-cast v1, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    invoke-virtual {v1}, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;->k()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;->getCollapsedHeight()I

    move-result v0

    :cond_0
    return v0
.end method

.method public final o(ZLjava/lang/String;Lw0/a;)Lw0/h;
    .locals 9

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v0

    const-wide/16 v1, 0x0

    const/4 v3, 0x2

    const/4 v4, -0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eqz p1, :cond_1

    new-instance p1, Lv0/a;

    invoke-direct {p1, v6}, Lv0/a;-><init>(Z)V

    new-array v0, v3, [F

    fill-array-data v0, :array_0

    invoke-static {v4, v0}, Lc1/c;->b(I[F)Lc1/c$a;

    move-result-object v0

    iput-object v0, p1, Lv0/a;->b:Lc1/c$a;

    new-instance v0, Lw0/a;

    invoke-direct {v0, p2, v6}, Lw0/a;-><init>(Ljava/lang/Object;Z)V

    sget-object v3, La1/h;->k:La1/h;

    invoke-virtual {v0, v3, v1, v2}, Lw0/a;->h(Ljava/lang/Object;D)V

    sget-object v1, La1/h;->b:La1/h;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v1, v2, v3}, Lw0/a;->h(Ljava/lang/Object;D)V

    new-array v1, v5, [Landroid/view/View;

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/c;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    aput-object v2, v1, v6

    invoke-static {v1}, Lu0/a;->f([Landroid/view/View;)Lu0/c;

    move-result-object v1

    check-cast v1, Lu0/a$c;

    invoke-virtual {v1}, Lu0/a$c;->a()Lw0/h;

    move-result-object v1

    if-eqz p3, :cond_0

    invoke-virtual {p3, p2}, Lw0/a;->i(Ljava/lang/Object;)V

    invoke-interface {v1, p3}, Lw0/h;->a(Ljava/lang/Object;)Lw0/h;

    move-result-object v1

    :cond_0
    new-array p2, v5, [Lv0/a;

    aput-object p1, p2, v6

    invoke-interface {v1, v0, p2}, Lw0/h;->k(Ljava/lang/Object;[Lv0/a;)Lw0/h;

    move-result-object p1

    goto :goto_0

    :cond_1
    new-instance p1, Lv0/a;

    invoke-direct {p1, v6}, Lv0/a;-><init>(Z)V

    new-array v3, v3, [F

    fill-array-data v3, :array_1

    invoke-static {v4, v3}, Lc1/c;->b(I[F)Lc1/c$a;

    move-result-object v3

    iput-object v3, p1, Lv0/a;->b:Lc1/c$a;

    new-array v3, v5, [Ly0/b;

    new-instance v4, Lmiuix/appcompat/internal/app/widget/c$b;

    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/c;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-direct {v4, v7}, Lmiuix/appcompat/internal/app/widget/c$b;-><init>(Landroid/view/View;)V

    aput-object v4, v3, v6

    iget-object v4, p1, Lv0/a;->e:Ljava/util/HashSet;

    invoke-static {v4, v3}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    new-instance v3, Lw0/a;

    invoke-direct {v3, p2, v6}, Lw0/a;-><init>(Ljava/lang/Object;Z)V

    sget-object v4, La1/h;->k:La1/h;

    neg-int v0, v0

    add-int/lit8 v0, v0, -0x64

    int-to-double v7, v0

    invoke-virtual {v3, v4, v7, v8}, Lw0/a;->h(Ljava/lang/Object;D)V

    sget-object v0, La1/h;->b:La1/h;

    invoke-virtual {v3, v0, v1, v2}, Lw0/a;->h(Ljava/lang/Object;D)V

    new-array v0, v5, [Landroid/view/View;

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/c;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    aput-object v1, v0, v6

    invoke-static {v0}, Lu0/a;->f([Landroid/view/View;)Lu0/c;

    move-result-object v0

    check-cast v0, Lu0/a$c;

    invoke-virtual {v0}, Lu0/a$c;->a()Lw0/h;

    move-result-object v0

    if-eqz p3, :cond_2

    invoke-virtual {p3, p2}, Lw0/a;->i(Ljava/lang/Object;)V

    invoke-interface {v0, p3}, Lw0/h;->a(Ljava/lang/Object;)Lw0/h;

    move-result-object v0

    :cond_2
    new-array p2, v5, [Lv0/a;

    aput-object p1, p2, v6

    invoke-interface {v0, v3, p2}, Lw0/h;->k(Ljava/lang/Object;[Lv0/a;)Lw0/h;

    move-result-object p1

    :goto_0
    return-object p1

    :array_0
    .array-data 4
        0x3f666666    # 0.9f
        0x3e800000    # 0.25f
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3eb33333    # 0.35f
    .end array-data
.end method

.method public final p(ZLjava/lang/String;Lw0/a;)Lw0/h;
    .locals 9

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/c;->n()I

    move-result v0

    const-wide/16 v1, 0x0

    const/4 v3, 0x2

    const/4 v4, -0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eqz p1, :cond_1

    new-instance p1, Lv0/a;

    invoke-direct {p1, v6}, Lv0/a;-><init>(Z)V

    new-array v0, v3, [F

    fill-array-data v0, :array_0

    invoke-static {v4, v0}, Lc1/c;->b(I[F)Lc1/c$a;

    move-result-object v0

    iput-object v0, p1, Lv0/a;->b:Lc1/c$a;

    new-instance v0, Lw0/a;

    invoke-direct {v0, p2, v6}, Lw0/a;-><init>(Ljava/lang/Object;Z)V

    sget-object v3, La1/h;->k:La1/h;

    invoke-virtual {v0, v3, v1, v2}, Lw0/a;->h(Ljava/lang/Object;D)V

    sget-object v1, La1/h;->b:La1/h;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v1, v2, v3}, Lw0/a;->h(Ljava/lang/Object;D)V

    new-array v1, v5, [Landroid/view/View;

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/c;->w:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    aput-object v2, v1, v6

    invoke-static {v1}, Lu0/a;->f([Landroid/view/View;)Lu0/c;

    move-result-object v1

    check-cast v1, Lu0/a$c;

    invoke-virtual {v1}, Lu0/a$c;->a()Lw0/h;

    move-result-object v1

    if-eqz p3, :cond_0

    invoke-virtual {p3, p2}, Lw0/a;->i(Ljava/lang/Object;)V

    invoke-interface {v1, p3}, Lw0/h;->a(Ljava/lang/Object;)Lw0/h;

    move-result-object v1

    :cond_0
    new-array p2, v5, [Lv0/a;

    aput-object p1, p2, v6

    invoke-interface {v1, v0, p2}, Lw0/h;->k(Ljava/lang/Object;[Lv0/a;)Lw0/h;

    move-result-object p1

    goto :goto_0

    :cond_1
    new-instance p1, Lv0/a;

    invoke-direct {p1, v6}, Lv0/a;-><init>(Z)V

    new-array v3, v3, [F

    fill-array-data v3, :array_1

    invoke-static {v4, v3}, Lc1/c;->b(I[F)Lc1/c$a;

    move-result-object v3

    iput-object v3, p1, Lv0/a;->b:Lc1/c$a;

    new-array v3, v5, [Ly0/b;

    new-instance v4, Lmiuix/appcompat/internal/app/widget/c$b;

    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/c;->w:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-direct {v4, v7}, Lmiuix/appcompat/internal/app/widget/c$b;-><init>(Landroid/view/View;)V

    aput-object v4, v3, v6

    iget-object v4, p1, Lv0/a;->e:Ljava/util/HashSet;

    invoke-static {v4, v3}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    new-instance v3, Lw0/a;

    invoke-direct {v3, p2, v6}, Lw0/a;-><init>(Ljava/lang/Object;Z)V

    sget-object v4, La1/h;->k:La1/h;

    add-int/lit8 v0, v0, 0x64

    int-to-double v7, v0

    invoke-virtual {v3, v4, v7, v8}, Lw0/a;->h(Ljava/lang/Object;D)V

    sget-object v0, La1/h;->b:La1/h;

    invoke-virtual {v3, v0, v1, v2}, Lw0/a;->h(Ljava/lang/Object;D)V

    new-array v0, v5, [Landroid/view/View;

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/c;->w:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    aput-object v1, v0, v6

    invoke-static {v0}, Lu0/a;->f([Landroid/view/View;)Lu0/c;

    move-result-object v0

    check-cast v0, Lu0/a$c;

    invoke-virtual {v0}, Lu0/a$c;->a()Lw0/h;

    move-result-object v0

    if-eqz p3, :cond_2

    invoke-virtual {p3, p2}, Lw0/a;->i(Ljava/lang/Object;)V

    invoke-interface {v0, p3}, Lw0/h;->a(Ljava/lang/Object;)Lw0/h;

    move-result-object v0

    :cond_2
    new-array p2, v5, [Lv0/a;

    aput-object p1, p2, v6

    invoke-interface {v0, v3, p2}, Lw0/h;->k(Ljava/lang/Object;[Lv0/a;)Lw0/h;

    move-result-object p1

    :goto_0
    return-object p1

    nop

    :array_0
    .array-data 4
        0x3f666666    # 0.9f
        0x3e800000    # 0.25f
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3eb33333    # 0.35f
    .end array-data
.end method

.method public final q(Z)V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->w:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->w:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->w:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->v:Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->g:Landroid/view/View;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/c;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/c;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->c(Landroid/view/View$OnClickListener;)Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$c;

    move-result-object p1

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$c;->c:Landroid/animation/ObjectAnimator;

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/c;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->c(Landroid/view/View$OnClickListener;)Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$c;

    move-result-object p1

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$c;->a:Landroid/animation/ObjectAnimator;

    :goto_0
    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    :cond_1
    return-void
.end method
