.class public Lmiuix/appcompat/internal/app/widget/SearchActionModeView$b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lmiuix/view/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/appcompat/internal/app/widget/SearchActionModeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation


# instance fields
.field public final synthetic a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;


# direct methods
.method public constructor <init>(Lmiuix/appcompat/internal/app/widget/SearchActionModeView;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$b;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 4

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$b;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    invoke-static {v0}, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->f(Lmiuix/appcompat/internal/app/widget/SearchActionModeView;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$b;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    iput v2, v1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->o:I

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$b;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    invoke-virtual {v0}, Landroid/view/View;->getPaddingBottom()I

    move-result v0

    iput v0, v1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->n:I

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$b;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget-object v0, v0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->f:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    :cond_2
    const/4 v1, 0x1

    if-eqz p1, :cond_5

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$b;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget v2, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->c:I

    const v3, 0x7fffffff

    if-ne v2, v3, :cond_3

    invoke-virtual {p1}, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->getActionBarContainer()Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    move-result-object p1

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$b;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget-object v2, v2, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->w:[I

    invoke-virtual {p1, v2}, Landroid/widget/FrameLayout;->getLocationInWindow([I)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$b;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget-object v2, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->w:[I

    aget v2, v2, v1

    iput v2, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->c:I

    :cond_3
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$b;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    invoke-virtual {p1}, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->getActionBarContainer()Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v2

    iput v2, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->v:I

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$b;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget v2, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->v:I

    neg-int v2, v2

    iput v2, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->u:I

    if-eqz v0, :cond_4

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->w:[I

    invoke-virtual {v0, p1}, Landroid/view/View;->getLocationInWindow([I)V

    :cond_4
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$b;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget-object v0, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->w:[I

    aget v0, v0, v1

    iget v1, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->c:I

    sub-int/2addr v0, v1

    iget v1, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->o:I

    sub-int/2addr v0, v1

    iput v0, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->j:I

    iget v0, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->u:I

    iput v0, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->i:I

    goto :goto_1

    :cond_5
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$b;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->q:Landroid/view/View;

    const/4 v0, 0x0

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    :cond_6
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$b;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget-object v1, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->a:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    iget v1, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->F:I

    invoke-virtual {p1, v1}, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->setContentViewTranslation(I)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$b;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    invoke-virtual {p1, v0, v0}, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->g(II)V

    :goto_1
    return-void
.end method

.method public f(ZF)V
    .locals 2

    if-nez p1, :cond_0

    const/high16 p1, 0x3f800000    # 1.0f

    sub-float p2, p1, p2

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$b;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->h:Ljava/lang/ref/WeakReference;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$b;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget v1, v0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->F:I

    int-to-float v1, v1

    mul-float/2addr v1, p2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->setContentViewTranslation(I)V

    if-eqz p1, :cond_2

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$b;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget v1, v0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->j:I

    int-to-float v1, v1

    iget v0, v0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->i:I

    int-to-float v0, v0

    mul-float/2addr v0, p2

    add-float/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    :cond_2
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$b;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget v0, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->v:I

    int-to-float v0, v0

    iget v1, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->u:I

    int-to-float v1, v1

    mul-float/2addr p2, v1

    add-float/2addr p2, v0

    invoke-virtual {p1, p2}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    return-void
.end method

.method public g(Z)V
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$b;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget-object v1, v1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->h:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationY(F)V

    :cond_1
    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$b;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget-object v1, v1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->f:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    :cond_2
    if-eqz v0, :cond_3

    xor-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    :cond_3
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$b;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget v1, v0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->F:I

    if-lez v1, :cond_5

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->setContentViewTranslation(I)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$b;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    if-eqz p1, :cond_4

    iget v2, v0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->F:I

    goto :goto_1

    :cond_4
    move v2, v1

    :goto_1
    invoke-virtual {v0, v2, v1}, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->g(II)V

    :cond_5
    if-eqz p1, :cond_6

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$b;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->a:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz p1, :cond_6

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    return-void
.end method
