.class public Lmiuix/appcompat/internal/app/widget/a$b;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/appcompat/internal/app/widget/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field public a:F

.field public b:Z

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/a$b;->c:Ljava/util/List;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/a$b;->b:Z

    return-void
.end method


# virtual methods
.method public a(FIILv0/a;)V
    .locals 6

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a$b;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    new-array v1, v2, [Landroid/view/View;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    aput-object v2, v1, v3

    invoke-static {v1}, Lu0/a;->f([Landroid/view/View;)Lu0/c;

    move-result-object v1

    check-cast v1, Lu0/a$c;

    invoke-virtual {v1}, Lu0/a$c;->a()Lw0/h;

    move-result-object v1

    invoke-interface {v1}, Lu0/d;->cancel()V

    goto :goto_0

    :cond_0
    new-instance v0, Lw0/a;

    const-string v1, "to"

    invoke-direct {v0, v1, v3}, Lw0/a;-><init>(Ljava/lang/Object;Z)V

    sget-object v1, La1/h;->b:La1/h;

    iget-boolean v4, p0, Lmiuix/appcompat/internal/app/widget/a$b;->b:Z

    if-eqz v4, :cond_1

    goto :goto_1

    :cond_1
    iget p1, p0, Lmiuix/appcompat/internal/app/widget/a$b;->a:F

    :goto_1
    float-to-double v4, p1

    invoke-virtual {v0, v1, v4, v5}, Lw0/a;->h(Ljava/lang/Object;D)V

    sget-object p1, La1/h;->j:La1/h;

    int-to-double v4, p2

    invoke-virtual {v0, p1, v4, v5}, Lw0/a;->h(Ljava/lang/Object;D)V

    sget-object p1, La1/h;->k:La1/h;

    int-to-double p2, p3

    invoke-virtual {v0, p1, p2, p3}, Lw0/a;->h(Ljava/lang/Object;D)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/a$b;->c:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    new-array p2, v2, [Landroid/view/View;

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Landroid/view/View;

    aput-object p3, p2, v3

    invoke-static {p2}, Lu0/a;->f([Landroid/view/View;)Lu0/c;

    move-result-object p2

    check-cast p2, Lu0/a$c;

    invoke-virtual {p2}, Lu0/a$c;->a()Lw0/h;

    move-result-object p2

    new-array p3, v2, [Lv0/a;

    aput-object p4, p3, v3

    invoke-interface {p2, v0, p3}, Lw0/h;->k(Ljava/lang/Object;[Lv0/a;)Lw0/h;

    goto :goto_2

    :cond_2
    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a$b;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lmiuix/appcompat/internal/app/widget/a$b$a;

    invoke-direct {v0, p0}, Lmiuix/appcompat/internal/app/widget/a$b$a;-><init>(Lmiuix/appcompat/internal/app/widget/a$b;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a$b;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public c(F)V
    .locals 6

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/a$b;->a:F

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a$b;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    new-array v2, v1, [Landroid/view/View;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v2}, Lu0/a;->f([Landroid/view/View;)Lu0/c;

    move-result-object v2

    check-cast v2, Lu0/a$c;

    invoke-virtual {v2}, Lu0/a$c;->a()Lw0/h;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    sget-object v5, La1/h;->b:La1/h;

    aput-object v5, v3, v4

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-interface {v2, v3}, Lw0/h;->c([Ljava/lang/Object;)Lw0/h;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public d(FII)V
    .locals 5

    new-instance v0, Lw0/a;

    const-string v1, "from"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lw0/a;-><init>(Ljava/lang/Object;Z)V

    sget-object v1, La1/h;->b:La1/h;

    iget-boolean v3, p0, Lmiuix/appcompat/internal/app/widget/a$b;->b:Z

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    iget p1, p0, Lmiuix/appcompat/internal/app/widget/a$b;->a:F

    :goto_0
    float-to-double v3, p1

    invoke-virtual {v0, v1, v3, v4}, Lw0/a;->h(Ljava/lang/Object;D)V

    sget-object p1, La1/h;->j:La1/h;

    int-to-double v3, p2

    invoke-virtual {v0, p1, v3, v4}, Lw0/a;->h(Ljava/lang/Object;D)V

    sget-object p1, La1/h;->k:La1/h;

    int-to-double p2, p3

    invoke-virtual {v0, p1, p2, p3}, Lw0/a;->h(Ljava/lang/Object;D)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/a$b;->c:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    const/4 p2, 0x1

    new-array p2, p2, [Landroid/view/View;

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Landroid/view/View;

    aput-object p3, p2, v2

    invoke-static {p2}, Lu0/a;->f([Landroid/view/View;)Lu0/c;

    move-result-object p2

    check-cast p2, Lu0/a$c;

    invoke-virtual {p2}, Lu0/a$c;->a()Lw0/h;

    move-result-object p2

    invoke-interface {p2, v0}, Lw0/h;->a(Ljava/lang/Object;)Lw0/h;

    goto :goto_1

    :cond_1
    return-void
.end method

.method public e(I)V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a$b;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    return-void
.end method
