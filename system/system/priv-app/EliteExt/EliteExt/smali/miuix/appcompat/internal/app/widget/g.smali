.class public final synthetic Lmiuix/appcompat/internal/app/widget/g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lmiuix/appcompat/internal/app/widget/ActionBarView;


# direct methods
.method public synthetic constructor <init>(Lmiuix/appcompat/internal/app/widget/ActionBarView;I)V
    .locals 0

    iput p2, p0, Lmiuix/appcompat/internal/app/widget/g;->a:I

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/g;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/g;->a:I

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/g;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object v0, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    invoke-virtual {v0}, Le1/a;->a()F

    move-result v1

    invoke-virtual {v0, v1}, Le1/a;->b(F)V

    return-void

    :pswitch_1
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/g;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget v1, v0, Lmiuix/appcompat/internal/app/widget/a;->h:I

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    if-nez v1, :cond_0

    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t:Lmiuix/appcompat/internal/app/widget/a$b;

    invoke-virtual {v1, v3, v4, v4}, Lmiuix/appcompat/internal/app/widget/a$b;->d(FII)V

    iget-object v0, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->j0:Lmiuix/appcompat/internal/app/widget/a$b;

    invoke-virtual {v0, v2, v4, v4}, Lmiuix/appcompat/internal/app/widget/a$b;->d(FII)V

    goto :goto_0

    :cond_0
    const/4 v5, 0x1

    if-ne v1, v5, :cond_1

    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t:Lmiuix/appcompat/internal/app/widget/a$b;

    const/16 v5, 0x14

    invoke-virtual {v1, v2, v4, v5}, Lmiuix/appcompat/internal/app/widget/a$b;->d(FII)V

    iget-object v0, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->j0:Lmiuix/appcompat/internal/app/widget/a$b;

    invoke-virtual {v0, v3, v4, v4}, Lmiuix/appcompat/internal/app/widget/a$b;->d(FII)V

    :cond_1
    :goto_0
    return-void

    :pswitch_2
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/g;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object v0, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Le1/a;->a()F

    move-result v1

    invoke-virtual {v0, v1}, Le1/a;->b(F)V

    :cond_2
    return-void

    :pswitch_3
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/g;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    if-eqz v1, :cond_3

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iget-object v1, v1, Le1/a;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getHitRect(Landroid/graphics/Rect;)V

    iget v1, v2, Landroid/graphics/Rect;->left:I

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070082

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sub-int/2addr v1, v3

    iput v1, v2, Landroid/graphics/Rect;->left:I

    new-instance v1, Landroid/view/TouchDelegate;

    iget-object v3, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    iget-object v3, v3, Le1/a;->c:Landroid/widget/LinearLayout;

    invoke-direct {v1, v2, v3}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    :cond_3
    return-void

    :goto_1
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/g;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    if-eqz v1, :cond_4

    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->C0:Landroid/view/View$OnClickListener;

    iget-object v1, v1, Le1/a;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->y0:Ljava/lang/CharSequence;

    if-eqz v1, :cond_4

    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    invoke-virtual {v1}, Le1/a;->a()F

    move-result v2

    invoke-virtual {v1, v2}, Le1/a;->b(F)V

    :cond_4
    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->J:Lx0/j;

    if-eqz v1, :cond_5

    iget-object v0, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->C0:Landroid/view/View$OnClickListener;

    iget-object v1, v1, Lx0/j;->c:Ljava/lang/Object;

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_5
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
