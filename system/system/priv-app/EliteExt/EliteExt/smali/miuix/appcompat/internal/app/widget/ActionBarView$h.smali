.class public Lmiuix/appcompat/internal/app/widget/ActionBarView$h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lh1/k;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/appcompat/internal/app/widget/ActionBarView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "h"
.end annotation


# instance fields
.field public a:Lh1/i;

.field public b:Lh1/g;

.field public final synthetic c:Lmiuix/appcompat/internal/app/widget/ActionBarView;


# direct methods
.method public constructor <init>(Lmiuix/appcompat/internal/app/widget/ActionBarView;Lmiuix/appcompat/internal/app/widget/ActionBarView$a;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b(Lh1/g;Z)V
    .locals 0

    return-void
.end method

.method public c(Z)V
    .locals 4

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->a:Lh1/i;

    if-eqz p1, :cond_2

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->b:Lh1/g;

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lh1/g;->size()I

    move-result p1

    move v1, v0

    :goto_0
    if-ge v1, p1, :cond_1

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->b:Lh1/g;

    invoke-virtual {v2, v1}, Lh1/g;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->a:Lh1/i;

    if-ne v2, v3, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    if-nez v0, :cond_2

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->b:Lh1/g;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->a:Lh1/i;

    invoke-virtual {p0, p1, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->j(Lh1/g;Lh1/i;)Z

    :cond_2
    return-void
.end method

.method public e(Landroid/content/Context;Lh1/g;)V
    .locals 1

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->b:Lh1/g;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->a:Lh1/i;

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Lh1/g;->e(Lh1/i;)Z

    :cond_0
    iput-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->b:Lh1/g;

    return-void
.end method

.method public f(Lh1/g;Lh1/i;)Z
    .locals 3

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {p2}, Lh1/i;->getActionView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->K:Landroid/view/View;

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object v0, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->M:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    iget-object v0, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->D:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v2, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->R:I

    invoke-virtual {v0, v2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    iput-object v0, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->M:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    iget-object v0, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->M:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->L:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object v0, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->M:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    invoke-static {p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->n(Lmiuix/appcompat/internal/app/widget/ActionBarView;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object p1

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    iget-object v0, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iput-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->a:Lh1/i;

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->K:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    if-eq p1, v0, :cond_1

    iget-object p1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->K:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_1
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->M:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    if-eq p1, v0, :cond_2

    iget-object p1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->M:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_2
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    const/16 v0, 0x8

    if-eqz p1, :cond_3

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    :cond_3
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object v2, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    if-eqz v2, :cond_4

    invoke-static {p1, v1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->m(Lmiuix/appcompat/internal/app/widget/ActionBarView;Z)V

    :cond_4
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->z:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz p1, :cond_5

    invoke-virtual {p1, v0}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    :cond_5
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->I:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz p1, :cond_6

    invoke-virtual {p1, v0}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    :cond_6
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz p1, :cond_7

    invoke-virtual {p1, v0}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    :cond_7
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->u0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz p1, :cond_8

    invoke-virtual {p1, v0}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    :cond_8
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E:Landroid/view/View;

    if-eqz p1, :cond_9

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_9
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->requestLayout()V

    const/4 p1, 0x1

    invoke-virtual {p2, p1}, Lh1/i;->d(Z)V

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object p2, p2, Lmiuix/appcompat/internal/app/widget/ActionBarView;->K:Landroid/view/View;

    instance-of v0, p2, Landroid/view/CollapsibleActionView;

    if-eqz v0, :cond_a

    check-cast p2, Landroid/view/CollapsibleActionView;

    invoke-interface {p2}, Landroid/view/CollapsibleActionView;->onActionViewExpanded()V

    :cond_a
    return p1
.end method

.method public h(Lh1/m;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public j(Lh1/g;Lh1/i;)Z
    .locals 5

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->K:Landroid/view/View;

    instance-of v0, p1, Landroid/view/CollapsibleActionView;

    if-eqz v0, :cond_0

    check-cast p1, Landroid/view/CollapsibleActionView;

    invoke-interface {p1}, Landroid/view/CollapsibleActionView;->onActionViewCollapsed()V

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object v0, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->K:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object v0, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->M:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    const/4 v0, 0x0

    iput-object v0, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->K:Landroid/view/View;

    iget v1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->G:I

    const/4 v2, 0x2

    and-int/2addr v1, v2

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    invoke-virtual {p1, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    :cond_1
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget v1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->G:I

    and-int/lit8 v1, v1, 0x8

    const/4 v4, 0x1

    if-eqz v1, :cond_3

    iget-object v1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    if-nez v1, :cond_2

    invoke-virtual {p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s()V

    goto :goto_0

    :cond_2
    invoke-static {p1, v4}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->m(Lmiuix/appcompat/internal/app/widget/ActionBarView;Z)V

    :cond_3
    :goto_0
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object v1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->z:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v1, :cond_4

    iget p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->m0:I

    if-ne p1, v2, :cond_4

    invoke-virtual {v1, v3}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    :cond_4
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object v1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->I:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v1, :cond_5

    iget p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->m0:I

    if-ne p1, v2, :cond_5

    invoke-virtual {v1, v3}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    :cond_5
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object v1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v1, :cond_6

    iget p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->m0:I

    if-ne p1, v2, :cond_6

    invoke-virtual {v1, v3}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    :cond_6
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object v1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->u0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v1, :cond_7

    iget p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->m0:I

    if-ne p1, v2, :cond_7

    invoke-virtual {v1, v3}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    :cond_7
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object v1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E:Landroid/view/View;

    if-eqz v1, :cond_8

    iget p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->G:I

    and-int/lit8 p1, p1, 0x10

    if-eqz p1, :cond_8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_8
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->M:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;->b:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->a:Lh1/i;

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->requestLayout()V

    invoke-virtual {p2, v3}, Lh1/i;->d(Z)V

    return v4
.end method
