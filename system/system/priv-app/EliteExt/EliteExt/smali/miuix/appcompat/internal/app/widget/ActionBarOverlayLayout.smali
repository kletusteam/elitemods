.class public Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements Lw/l;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$b;,
        Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$c;,
        Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$d;,
        Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$e;
    }
.end annotation


# instance fields
.field public A:[I

.field public B:Ld1/l;

.field public C:Z

.field public D:Z

.field public E:Z

.field public F:I

.field public a:Ld1/a;

.field public b:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

.field public c:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

.field public d:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

.field public e:Lmiuix/appcompat/internal/app/widget/ActionBarView;

.field public f:Landroid/view/ActionMode;

.field public g:Z

.field public h:Landroid/graphics/Rect;

.field public i:Landroid/graphics/Rect;

.field public j:Landroid/view/Window$Callback;

.field public k:Z

.field public l:Landroid/graphics/drawable/Drawable;

.field public m:Landroid/graphics/Rect;

.field public n:Landroid/view/View;

.field public o:Landroid/graphics/Rect;

.field public p:Landroid/view/View;

.field public q:Lj1/b;

.field public r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$d;

.field public s:Lh1/h;

.field public t:Landroid/util/TypedValue;

.field public u:Landroid/util/TypedValue;

.field public v:Landroid/util/TypedValue;

.field public w:Landroid/util/TypedValue;

.field public x:Landroid/graphics/Rect;

.field public y:Landroid/graphics/Rect;

.field public z:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->E:Z

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->h:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->y:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->m:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->i:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->z:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->x:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->o:Landroid/graphics/Rect;

    new-instance v2, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$d;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$d;-><init>(Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$a;)V

    iput-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$d;

    const/4 v2, 0x2

    new-array v2, v2, [I

    iput-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->A:[I

    sget-object v2, Landroidx/emoji2/text/l;->m0:[I

    invoke-virtual {p1, p2, v2, v0, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    const/16 p2, 0x10

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    iput-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->v:Landroid/util/TypedValue;

    invoke-virtual {p1, p2, v2}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :cond_0
    const/16 p2, 0x11

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    iput-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->w:Landroid/util/TypedValue;

    invoke-virtual {p1, p2, v2}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :cond_1
    const/16 p2, 0xe

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    iput-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->t:Landroid/util/TypedValue;

    invoke-virtual {p1, p2, v2}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :cond_2
    const/16 p2, 0xf

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    iput-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->u:Landroid/util/TypedValue;

    invoke-virtual {p1, p2, v2}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :cond_3
    invoke-virtual {p1, v0, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->k:Z

    if-eqz p2, :cond_4

    invoke-virtual {p1, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    iput-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->l:Landroid/graphics/drawable/Drawable;

    :cond_4
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 7

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->m:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->top:I

    iput v1, v2, Landroid/graphics/Rect;->top:I

    iput p1, v2, Landroid/graphics/Rect;->bottom:I

    iget p1, v0, Landroid/graphics/Rect;->right:I

    iput p1, v2, Landroid/graphics/Rect;->right:I

    iget p1, v0, Landroid/graphics/Rect;->left:I

    iput p1, v2, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->p:Landroid/view/View;

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->b(Landroid/view/View;Landroid/graphics/Rect;ZZZZ)Z

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->p:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method public final b(Landroid/view/View;Landroid/graphics/Rect;ZZZZ)Z
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v0, 0x1

    if-eqz p3, :cond_0

    iget p3, p1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget v1, p2, Landroid/graphics/Rect;->left:I

    if-eq p3, v1, :cond_0

    iput v1, p1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    move p3, v0

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    if-eqz p4, :cond_1

    iget p4, p1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    iget v1, p2, Landroid/graphics/Rect;->top:I

    if-eq p4, v1, :cond_1

    iput v1, p1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    move p3, v0

    :cond_1
    if-eqz p6, :cond_2

    iget p4, p1, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    iget p6, p2, Landroid/graphics/Rect;->right:I

    if-eq p4, p6, :cond_2

    iput p6, p1, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    move p3, v0

    :cond_2
    if-eqz p5, :cond_3

    iget p4, p1, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    iget p2, p2, Landroid/graphics/Rect;->bottom:I

    if-eq p4, p2, :cond_3

    iput p2, p1, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    goto :goto_1

    :cond_3
    move v0, p3

    :goto_1
    return v0
.end method

.method public c(Landroid/view/View$OnClickListener;)Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$c;
    .locals 2

    new-instance v0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$c;-><init>(Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;Landroid/view/View$OnClickListener;Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$a;)V

    return-object v0
.end method

.method public d()Z
    .locals 5

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getWindowSystemUiVisibility()I

    move-result v0

    and-int/lit16 v1, v0, 0x100

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_0

    move v1, v3

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_0
    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_1

    move v0, v3

    goto :goto_1

    :cond_1
    move v0, v2

    :goto_1
    iget v4, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->F:I

    if-eqz v4, :cond_2

    move v4, v3

    goto :goto_2

    :cond_2
    move v4, v2

    :goto_2
    if-eqz v1, :cond_3

    if-nez v0, :cond_4

    :cond_3
    if-eqz v4, :cond_5

    :cond_4
    move v2, v3

    :cond_5
    return v2
.end method

.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 4

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->l:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getRight()I

    move-result v1

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getLeft()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->h:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 4

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v2, 0x4

    const/4 v3, 0x0

    if-ne v0, v2, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result p1

    if-ne p1, v1, :cond_1

    move p1, v1

    goto :goto_0

    :cond_1
    move p1, v3

    :goto_0
    if-eqz p1, :cond_5

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->f:Landroid/view/ActionMode;

    if-eqz p1, :cond_3

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->c:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->p()Z

    move-result p1

    if-nez p1, :cond_6

    :cond_2
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->f:Landroid/view/ActionMode;

    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    const/4 p1, 0x0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->f:Landroid/view/ActionMode;

    goto :goto_2

    :cond_3
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->e:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    if-eqz p1, :cond_5

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    if-eqz p1, :cond_4

    invoke-virtual {p1, v3}, Li1/b;->l(Z)Z

    move-result p1

    if-eqz p1, :cond_4

    move p1, v1

    goto :goto_1

    :cond_4
    move p1, v3

    :goto_1
    if-eqz p1, :cond_5

    goto :goto_2

    :cond_5
    move v1, v3

    :cond_6
    :goto_2
    return v1
.end method

.method public fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 12

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->B:Ld1/l;

    if-eqz v0, :cond_0

    iget v1, p1, Landroid/graphics/Rect;->top:I

    invoke-interface {v0, v1}, Ld1/l;->a(I)V

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->i:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v2, v0, Landroid/app/Activity;

    if-eqz v2, :cond_1

    check-cast v0, Landroid/app/Activity;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x1

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I

    if-ne v0, v2, :cond_2

    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v1

    :goto_1
    if-nez v0, :cond_4

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const v3, 0x1010586

    invoke-static {v0, v3, v1}, Lr1/a;->e(Landroid/content/Context;II)I

    move-result v0

    if-ne v0, v2, :cond_3

    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v1

    :cond_4
    :goto_2
    if-eqz v0, :cond_5

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->i:Landroid/graphics/Rect;

    iput v1, v0, Landroid/graphics/Rect;->left:I

    iput v1, v0, Landroid/graphics/Rect;->right:I

    :cond_5
    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->E:Z

    if-nez v0, :cond_8

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getWindowSystemUiVisibility()I

    move-result v0

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_6

    move v0, v2

    goto :goto_3

    :cond_6
    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->i:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroidx/emoji2/text/l;->v(Landroid/content/Context;)I

    move-result v3

    if-eq v0, v3, :cond_8

    :cond_7
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->i:Landroid/graphics/Rect;

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    move v0, v2

    goto :goto_4

    :cond_8
    move v0, v1

    :goto_4
    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->d:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v3, :cond_b

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->d()Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->d:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v3, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setPendingInsets(Landroid/graphics/Rect;)V

    :cond_9
    iget-object v5, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->d:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    iget-object v6, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->i:Landroid/graphics/Rect;

    iget-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->E:Z

    if-eqz p1, :cond_a

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->d()Z

    move-result p1

    if-nez p1, :cond_a

    move v8, v2

    goto :goto_5

    :cond_a
    move v8, v1

    :goto_5
    const/4 v7, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x1

    move-object v4, p0

    invoke-virtual/range {v4 .. v10}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->b(Landroid/view/View;Landroid/graphics/Rect;ZZZZ)Z

    move-result p1

    goto :goto_6

    :cond_b
    move p1, v1

    :goto_6
    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->b:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v3, :cond_c

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->o:Landroid/graphics/Rect;

    iget-object v4, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->i:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v6, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->b:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->i:Landroid/graphics/Rect;

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x1

    move-object v5, p0

    invoke-virtual/range {v5 .. v11}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->b(Landroid/view/View;Landroid/graphics/Rect;ZZZZ)Z

    move-result v3

    or-int/2addr p1, v3

    :cond_c
    iget-boolean v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->E:Z

    if-nez v3, :cond_d

    if-nez v0, :cond_d

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->i:Landroid/graphics/Rect;

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    :cond_d
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->i:Landroid/graphics/Rect;

    iget-object v4, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->h:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->d()Z

    move-result v5

    invoke-virtual {v4, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    if-eqz v3, :cond_e

    if-eqz v5, :cond_f

    :cond_e
    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->k:Z

    if-nez v0, :cond_f

    iput v1, v4, Landroid/graphics/Rect;->top:I

    :cond_f
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->y:Landroid/graphics/Rect;

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->h:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->y:Landroid/graphics/Rect;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->h:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_7

    :cond_10
    move v2, p1

    :goto_7
    if-eqz v2, :cond_11

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->requestLayout()V

    :cond_11
    iget-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->E:Z

    return p1
.end method

.method public getActionBar()Ld1/a;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->a:Ld1/a;

    return-object v0
.end method

.method public getActionBarView()Lmiuix/appcompat/internal/app/widget/ActionBarView;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->e:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    return-object v0
.end method

.method public getBottomInset()I
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->b:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->getInsetHeight()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getCallback()Landroid/view/Window$Callback;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->j:Landroid/view/Window$Callback;

    return-object v0
.end method

.method public getContentMask()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->n:Landroid/view/View;

    return-object v0
.end method

.method public getContentView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->p:Landroid/view/View;

    return-object v0
.end method

.method public i(Landroid/view/View;Landroid/view/View;II)V
    .locals 2

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->d:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz p1, :cond_5

    iget-object p2, p1, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->a:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    const/4 p3, 0x2

    const/4 v0, 0x1

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getVisibility()I

    move-result p2

    if-nez p2, :cond_2

    iget-object p2, p1, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->a:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    if-nez p4, :cond_0

    iput-boolean v0, p2, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->W:Z

    goto :goto_0

    :cond_0
    iput-boolean v0, p2, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->J:Z

    :goto_0
    iget-object v1, p2, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->N:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p2, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->N:Landroid/widget/Scroller;

    invoke-virtual {v1, v0}, Landroid/widget/Scroller;->forceFinished(Z)V

    :cond_1
    invoke-virtual {p2, p3}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->setExpandState(I)V

    :cond_2
    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    if-nez p4, :cond_3

    iput-boolean v0, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->F0:Z

    goto :goto_1

    :cond_3
    iput-boolean v0, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->n0:Z

    :goto_1
    iget-object p2, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->r0:Landroid/widget/Scroller;

    invoke-virtual {p2}, Landroid/widget/Scroller;->isFinished()Z

    move-result p2

    if-nez p2, :cond_4

    iget-object p2, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->r0:Landroid/widget/Scroller;

    invoke-virtual {p2, v0}, Landroid/widget/Scroller;->forceFinished(Z)V

    :cond_4
    invoke-virtual {p1, p3}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setExpandState(I)V

    :cond_5
    return-void
.end method

.method public k(Landroid/view/View;IIIII[I)V
    .locals 6

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->A:[I

    const/4 p2, 0x0

    const/4 p3, 0x1

    aput p2, p1, p3

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->d:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz p2, :cond_3

    iget-object p4, p2, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->a:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    if-eqz p4, :cond_1

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getVisibility()I

    move-result p4

    if-nez p4, :cond_1

    iget-object p4, p2, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->a:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    const/4 p6, 0x2

    new-array v0, p6, [I

    fill-array-data v0, :array_0

    new-array p6, p6, [I

    fill-array-data p6, :array_1

    invoke-static {p4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-gez p5, :cond_1

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    iget v2, p4, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->D:I

    iget-object v3, p4, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v2

    if-ge v1, v3, :cond_1

    iget v1, p4, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->L:I

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    sub-int/2addr v2, p5

    iget v3, p4, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->D:I

    iget-object v4, p4, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v3

    if-gt v2, v4, :cond_0

    iget v2, p4, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->L:I

    sub-int/2addr v2, p5

    iput v2, p4, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->L:I

    aget v2, v0, p3

    add-int/2addr v2, p5

    aput v2, v0, p3

    goto :goto_0

    :cond_0
    iget v2, p4, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->D:I

    iget-object v3, p4, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    iget-object v5, p4, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v5

    iput v5, p4, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->L:I

    aget v5, v0, p3

    add-int/2addr v2, v3

    sub-int/2addr v2, v4

    neg-int v2, v2

    add-int/2addr v5, v2

    aput v5, v0, p3

    :goto_0
    iget v0, p4, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->L:I

    if-eq v0, v1, :cond_1

    sub-int/2addr v1, v0

    aput v1, p6, p3

    invoke-virtual {p4}, Landroid/view/ViewGroup;->requestLayout()V

    :cond_1
    iget-object p2, p2, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-gez p5, :cond_3

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getHeight()I

    move-result p4

    iget p6, p2, Lmiuix/appcompat/internal/app/widget/ActionBarView;->v:I

    iget-object v0, p2, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, p6

    if-ge p4, v0, :cond_3

    iget p4, p2, Lmiuix/appcompat/internal/app/widget/ActionBarView;->p0:I

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getHeight()I

    move-result p6

    sub-int/2addr p6, p5

    iget v0, p2, Lmiuix/appcompat/internal/app/widget/ActionBarView;->v:I

    iget-object v1, p2, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v0

    if-gt p6, v1, :cond_2

    iget p6, p2, Lmiuix/appcompat/internal/app/widget/ActionBarView;->p0:I

    sub-int/2addr p6, p5

    iput p6, p2, Lmiuix/appcompat/internal/app/widget/ActionBarView;->p0:I

    aget p6, p7, p3

    add-int/2addr p6, p5

    aput p6, p7, p3

    goto :goto_1

    :cond_2
    iget p5, p2, Lmiuix/appcompat/internal/app/widget/ActionBarView;->v:I

    iget-object p6, p2, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    invoke-virtual {p6}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result p6

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    iget-object v1, p2, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v1

    iget v2, p2, Lmiuix/appcompat/internal/app/widget/ActionBarView;->l0:I

    add-int/2addr v1, v2

    iput v1, p2, Lmiuix/appcompat/internal/app/widget/ActionBarView;->p0:I

    aget v1, p7, p3

    add-int/2addr p5, p6

    sub-int/2addr p5, v0

    neg-int p5, p5

    add-int/2addr v1, p5

    aput v1, p7, p3

    :goto_1
    iget p5, p2, Lmiuix/appcompat/internal/app/widget/ActionBarView;->p0:I

    if-eq p5, p4, :cond_3

    sub-int/2addr p4, p5

    aput p4, p1, p3

    invoke-virtual {p2}, Landroid/view/ViewGroup;->requestLayout()V

    :cond_3
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->p:Landroid/view/View;

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->A:[I

    aget p2, p2, p3

    neg-int p2, p2

    invoke-virtual {p1, p2}, Landroid/view/View;->offsetTopAndBottom(I)V

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public l(Landroid/view/View;IIIII)V
    .locals 0

    return-void
.end method

.method public m(Landroid/view/View;I)V
    .locals 6

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->d:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz p1, :cond_d

    iget-object p2, p1, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->a:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p2, :cond_5

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getVisibility()I

    move-result p2

    if-nez p2, :cond_5

    iget-object p2, p1, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->a:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    iget-boolean v2, p2, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->W:Z

    if-eqz v2, :cond_0

    iput-boolean v1, p2, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->W:Z

    iget-boolean v2, p2, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->J:Z

    if-nez v2, :cond_1

    goto :goto_0

    :cond_0
    iget-boolean v2, p2, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->J:Z

    if-eqz v2, :cond_1

    iput-boolean v1, p2, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->J:Z

    :goto_0
    move v2, v0

    goto :goto_1

    :cond_1
    move v2, v1

    :goto_1
    if-eqz v2, :cond_5

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    iget v3, p2, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->D:I

    if-ne v2, v3, :cond_2

    invoke-virtual {p2, v1}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->setExpandState(I)V

    goto :goto_3

    :cond_2
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    iget v3, p2, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->D:I

    iget-object v4, p2, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v3

    if-ne v2, v4, :cond_3

    iget v2, p2, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->L:I

    iget-object v3, p2, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v3

    if-ne v2, v3, :cond_3

    invoke-virtual {p2, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->setExpandState(I)V

    goto :goto_3

    :cond_3
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    iget v3, p2, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->D:I

    iget-object v4, p2, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v4, v3

    if-le v2, v4, :cond_4

    iget-object v2, p2, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->N:Landroid/widget/Scroller;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    iget v4, p2, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->D:I

    iget-object v5, p2, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->H:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v4

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    sub-int/2addr v5, v4

    invoke-virtual {v2, v1, v3, v1, v5}, Landroid/widget/Scroller;->startScroll(IIII)V

    goto :goto_2

    :cond_4
    iget-object v2, p2, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->N:Landroid/widget/Scroller;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    iget v4, p2, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->D:I

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v2, v1, v3, v1, v4}, Landroid/widget/Scroller;->startScroll(IIII)V

    :goto_2
    iget-object v2, p2, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->M:Ljava/lang/Runnable;

    invoke-virtual {p2, v2}, Landroid/view/ViewGroup;->postOnAnimation(Ljava/lang/Runnable;)V

    :cond_5
    :goto_3
    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object p2, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    invoke-virtual {p2}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result p2

    iget-object v2, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-nez v2, :cond_6

    move v2, v1

    goto :goto_4

    :cond_6
    iget-object v2, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v2

    :goto_4
    iget-boolean v3, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->F0:Z

    if-eqz v3, :cond_7

    iput-boolean v1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->F0:Z

    iget-boolean v3, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->n0:Z

    if-nez v3, :cond_8

    goto :goto_5

    :cond_7
    iget-boolean v3, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->n0:Z

    if-eqz v3, :cond_8

    iput-boolean v1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->n0:Z

    :goto_5
    move v3, v0

    goto :goto_6

    :cond_8
    move v3, v1

    :goto_6
    if-eqz v3, :cond_d

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    iget v4, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->v:I

    if-ne v3, v4, :cond_9

    invoke-virtual {p1, v1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setExpandState(I)V

    goto :goto_9

    :cond_9
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    iget v4, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->v:I

    add-int/2addr v4, p2

    add-int/2addr v4, v2

    if-eq v3, v4, :cond_c

    iget v3, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->p0:I

    add-int/2addr v2, p2

    if-ne v3, v2, :cond_a

    goto :goto_8

    :cond_a
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    iget v2, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->v:I

    iget v3, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->l0:I

    add-int/2addr v3, p2

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v2

    if-le v0, v3, :cond_b

    iget-object v0, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->r0:Landroid/widget/Scroller;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    iget v3, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->v:I

    add-int/2addr v3, p2

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getHeight()I

    move-result p2

    sub-int/2addr v3, p2

    invoke-virtual {v0, v1, v2, v1, v3}, Landroid/widget/Scroller;->startScroll(IIII)V

    goto :goto_7

    :cond_b
    iget-object p2, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->r0:Landroid/widget/Scroller;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    iget v2, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->v:I

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p2, v1, v0, v1, v2}, Landroid/widget/Scroller;->startScroll(IIII)V

    :goto_7
    iget-object p2, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->q0:Ljava/lang/Runnable;

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->postOnAnimation(Ljava/lang/Runnable;)V

    goto :goto_9

    :cond_c
    :goto_8
    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setExpandState(I)V

    :cond_d
    :goto_9
    return-void
.end method

.method public n(Landroid/view/View;II[II)V
    .locals 7

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->A:[I

    const/4 p2, 0x1

    const/4 p5, 0x0

    aput p5, p1, p2

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->d:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v0, :cond_3

    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->a:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->a:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    const/4 v2, 0x2

    new-array v3, v2, [I

    fill-array-data v3, :array_0

    new-array v2, v2, [I

    fill-array-data v2, :array_1

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-lez p3, :cond_1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    iget v5, v1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->D:I

    if-le v4, v5, :cond_1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    iget v5, v1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->L:I

    iget v6, v1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->D:I

    sub-int/2addr v4, p3

    if-lt v4, v6, :cond_0

    sub-int p5, v5, p3

    iput p5, v1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->L:I

    aget p5, v3, p2

    add-int/2addr p5, p3

    aput p5, v3, p2

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    iput p5, v1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->L:I

    aget p5, v3, p2

    sub-int/2addr v6, v4

    neg-int v4, v6

    add-int/2addr p5, v4

    aput p5, v3, p2

    :goto_0
    iget p5, v1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->L:I

    if-eq p5, v5, :cond_1

    sub-int/2addr v5, p5

    aput v5, v2, p2

    invoke-virtual {v1}, Landroid/view/ViewGroup;->requestLayout()V

    :cond_1
    iget-object p5, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-static {p5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-lez p3, :cond_3

    invoke-virtual {p5}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    iget v1, p5, Lmiuix/appcompat/internal/app/widget/ActionBarView;->v:I

    if-le v0, v1, :cond_3

    invoke-virtual {p5}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    iget v1, p5, Lmiuix/appcompat/internal/app/widget/ActionBarView;->p0:I

    sub-int/2addr v0, p3

    iget v2, p5, Lmiuix/appcompat/internal/app/widget/ActionBarView;->v:I

    if-lt v0, v2, :cond_2

    sub-int v0, v1, p3

    iput v0, p5, Lmiuix/appcompat/internal/app/widget/ActionBarView;->p0:I

    aget v0, p4, p2

    add-int/2addr v0, p3

    aput v0, p4, p2

    :cond_2
    iget p3, p5, Lmiuix/appcompat/internal/app/widget/ActionBarView;->p0:I

    if-eq p3, v1, :cond_3

    sub-int/2addr v1, p3

    aput v1, p1, p2

    invoke-virtual {p5}, Landroid/view/ViewGroup;->requestLayout()V

    :cond_3
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->p:Landroid/view/View;

    iget-object p3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->A:[I

    aget p2, p3, p2

    neg-int p2, p2

    invoke-virtual {p1, p2}, Landroid/view/View;->offsetTopAndBottom(I)V

    return-void

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public o(Landroid/view/View;Landroid/view/View;II)Z
    .locals 1

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->d:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    const/4 p2, 0x1

    const/4 p3, 0x0

    if-eqz p1, :cond_3

    iget-object p4, p1, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->a:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    const/4 v0, 0x2

    if-eqz p4, :cond_0

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getVisibility()I

    move-result p4

    if-nez p4, :cond_0

    iget-object p4, p1, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->a:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p4

    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p4

    invoke-virtual {p4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p4

    iget p4, p4, Landroid/content/res/Configuration;->orientation:I

    if-ne p4, v0, :cond_0

    invoke-static {}, Lr1/b;->a()Z

    :cond_0
    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p4

    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p4

    invoke-virtual {p4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p4

    iget p4, p4, Landroid/content/res/Configuration;->orientation:I

    if-ne p4, v0, :cond_1

    invoke-static {}, Lr1/b;->a()Z

    move-result p4

    if-nez p4, :cond_1

    goto :goto_0

    :cond_1
    iget-object p4, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView;->K:Landroid/view/View;

    if-nez p4, :cond_2

    invoke-virtual {p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t()Z

    move-result p4

    if-eqz p4, :cond_2

    iget-boolean p1, p1, Lmiuix/appcompat/internal/app/widget/a;->l:Z

    if-eqz p1, :cond_2

    move p1, p2

    goto :goto_1

    :cond_2
    :goto_0
    move p1, p3

    :goto_1
    if-eqz p1, :cond_3

    goto :goto_2

    :cond_3
    move p2, p3

    :goto_2
    return p2
.end method

.method public onApplyWindowInsets(Landroid/view/WindowInsets;)Landroid/view/WindowInsets;
    .locals 2

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onApplyWindowInsets(Landroid/view/WindowInsets;)Landroid/view/WindowInsets;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/WindowInsets;->isConsumed()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->E:Z

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/WindowInsets;->consumeDisplayCutout()Landroid/view/WindowInsets;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->requestFitSystemWindows()V

    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->p:Landroid/view/View;

    if-nez v0, :cond_0

    const v0, 0x1020002

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->p:Landroid/view/View;

    const v0, 0x7f0a002b

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->d:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v0, :cond_0

    const v1, 0x7f0a0027

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->e:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    :cond_0
    return-void
.end method

.method public onMeasure(II)V
    .locals 18

    move-object/from16 v7, p0

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    const/4 v3, 0x6

    const/4 v4, 0x5

    const/high16 v5, -0x80000000

    const/4 v9, 0x0

    if-ne v0, v5, :cond_4

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v6, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v10, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v6, v10, :cond_0

    const/4 v10, 0x1

    goto :goto_0

    :cond_0
    move v10, v9

    :goto_0
    if-eqz v10, :cond_1

    iget-object v10, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->w:Landroid/util/TypedValue;

    goto :goto_1

    :cond_1
    iget-object v10, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->v:Landroid/util/TypedValue;

    :goto_1
    if-eqz v10, :cond_4

    iget v11, v10, Landroid/util/TypedValue;->type:I

    if-eqz v11, :cond_4

    if-ne v11, v4, :cond_2

    invoke-virtual {v10, v0}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    move-result v0

    goto :goto_2

    :cond_2
    if-ne v11, v3, :cond_3

    int-to-float v0, v6

    invoke-virtual {v10, v0, v0}, Landroid/util/TypedValue;->getFraction(FF)F

    move-result v0

    :goto_2
    float-to-int v0, v0

    goto :goto_3

    :cond_3
    move v0, v9

    :goto_3
    if-lez v0, :cond_4

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    move v10, v0

    goto :goto_4

    :cond_4
    move/from16 v10, p1

    :goto_4
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    if-ne v0, v5, :cond_9

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v5, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v6, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v5, v6, :cond_5

    const/4 v5, 0x1

    goto :goto_5

    :cond_5
    move v5, v9

    :goto_5
    if-eqz v5, :cond_6

    iget-object v5, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->t:Landroid/util/TypedValue;

    goto :goto_6

    :cond_6
    iget-object v5, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->u:Landroid/util/TypedValue;

    :goto_6
    if-eqz v5, :cond_9

    iget v11, v5, Landroid/util/TypedValue;->type:I

    if-eqz v11, :cond_9

    if-ne v11, v4, :cond_7

    invoke-virtual {v5, v0}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    move-result v0

    goto :goto_7

    :cond_7
    if-ne v11, v3, :cond_8

    int-to-float v0, v6

    invoke-virtual {v5, v0, v0}, Landroid/util/TypedValue;->getFraction(FF)F

    move-result v0

    :goto_7
    float-to-int v0, v0

    goto :goto_8

    :cond_8
    move v0, v9

    :goto_8
    if-lez v0, :cond_9

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    move v11, v0

    goto :goto_9

    :cond_9
    move/from16 v11, p2

    :goto_9
    iget-object v12, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->p:Landroid/view/View;

    iget-object v13, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->n:Landroid/view/View;

    move v5, v9

    move v6, v5

    move v14, v6

    move v15, v14

    :goto_a
    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v0

    if-ge v6, v0, :cond_c

    invoke-virtual {v7, v6}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    if-eq v4, v12, :cond_b

    if-eq v4, v13, :cond_b

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_a

    goto :goto_b

    :cond_a
    const/4 v3, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, p0

    move-object v1, v4

    move v2, v10

    move-object/from16 v17, v4

    move v4, v11

    move v8, v5

    move/from16 v5, v16

    invoke-virtual/range {v0 .. v5}, Landroid/widget/FrameLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget v2, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    add-int/2addr v1, v2

    iget v2, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    add-int/2addr v1, v2

    invoke-static {v15, v1}, Ljava/lang/Math;->max(II)I

    move-result v15

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget v2, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v1, v0

    invoke-static {v8, v1}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getMeasuredState()I

    move-result v0

    invoke-static {v14, v0}, Landroid/widget/FrameLayout;->combineMeasuredStates(II)I

    move-result v14

    goto :goto_c

    :cond_b
    :goto_b
    move v8, v5

    move v5, v8

    :goto_c
    add-int/lit8 v6, v6, 0x1

    goto :goto_a

    :cond_c
    move v8, v5

    iget-object v0, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->d:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v0, :cond_d

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->d:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v0

    goto :goto_d

    :cond_d
    move v0, v9

    :goto_d
    iget-object v1, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->e:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    if-eqz v1, :cond_e

    iget-boolean v1, v1, Lmiuix/appcompat/internal/app/widget/a;->n:Z

    if-eqz v1, :cond_e

    invoke-virtual/range {p0 .. p0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->getBottomInset()I

    move-result v1

    goto :goto_e

    :cond_e
    move v1, v9

    :goto_e
    iget-object v2, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->x:Landroid/graphics/Rect;

    iget-object v3, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->i:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v2, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->m:Landroid/graphics/Rect;

    iget-object v3, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->h:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    invoke-virtual/range {p0 .. p0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->d()Z

    move-result v2

    if-eqz v2, :cond_f

    if-lez v0, :cond_f

    iget-object v2, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->m:Landroid/graphics/Rect;

    iput v9, v2, Landroid/graphics/Rect;->top:I

    :cond_f
    iget-boolean v2, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->C:Z

    if-nez v2, :cond_10

    iget-object v2, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->m:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v0

    iput v3, v2, Landroid/graphics/Rect;->top:I

    iget v0, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    iput v0, v2, Landroid/graphics/Rect;->bottom:I

    goto :goto_10

    :cond_10
    invoke-virtual/range {p0 .. p0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->d()Z

    move-result v2

    if-eqz v2, :cond_11

    if-lez v0, :cond_12

    iget-object v2, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->x:Landroid/graphics/Rect;

    iput v0, v2, Landroid/graphics/Rect;->top:I

    goto :goto_f

    :cond_11
    iget-object v2, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->x:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v0

    iput v3, v2, Landroid/graphics/Rect;->top:I

    :cond_12
    :goto_f
    iget-object v0, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->x:Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v1

    iput v2, v0, Landroid/graphics/Rect;->bottom:I

    :goto_10
    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getWindowSystemUiVisibility()I

    move-result v0

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_13

    const/4 v0, 0x1

    goto :goto_11

    :cond_13
    move v0, v9

    :goto_11
    if-eqz v0, :cond_17

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_14

    goto :goto_13

    :cond_14
    iget-object v0, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->m:Landroid/graphics/Rect;

    iput v9, v0, Landroid/graphics/Rect;->right:I

    iput v9, v0, Landroid/graphics/Rect;->left:I

    invoke-static {}, Lr1/b;->a()Z

    move-result v0

    if-nez v0, :cond_16

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "hide_gesture_line"

    invoke-static {v0, v1, v9}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_15

    const/4 v1, 0x1

    goto :goto_12

    :cond_15
    move v1, v9

    :goto_12
    if-nez v1, :cond_17

    :cond_16
    :goto_13
    iget-object v0, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->m:Landroid/graphics/Rect;

    iput v9, v0, Landroid/graphics/Rect;->bottom:I

    :cond_17
    iget-boolean v0, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->g:Z

    if-nez v0, :cond_18

    iget-object v2, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->m:Landroid/graphics/Rect;

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x1

    move-object/from16 v0, p0

    move-object v1, v12

    invoke-virtual/range {v0 .. v6}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->b(Landroid/view/View;Landroid/graphics/Rect;ZZZZ)Z

    :cond_18
    iget-boolean v0, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->C:Z

    if-nez v0, :cond_19

    invoke-virtual {v12}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    invoke-virtual {v12}, Landroid/view/View;->getPaddingRight()I

    move-result v1

    invoke-virtual {v12, v0, v9, v1, v9}, Landroid/view/View;->setPadding(IIII)V

    :cond_19
    iget-object v0, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->z:Landroid/graphics/Rect;

    iget-object v1, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->x:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-boolean v0, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->D:Z

    if-eqz v0, :cond_1b

    :cond_1a
    iget-object v0, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->z:Landroid/graphics/Rect;

    iget-object v1, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->x:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v0, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->x:Landroid/graphics/Rect;

    invoke-super {v7, v0}, Landroid/widget/FrameLayout;->fitSystemWindows(Landroid/graphics/Rect;)Z

    iput-boolean v9, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->D:Z

    :cond_1b
    invoke-virtual/range {p0 .. p0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->d()Z

    move-result v0

    if-eqz v0, :cond_1d

    iget-boolean v0, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->k:Z

    if-eqz v0, :cond_1d

    iget-object v0, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->l:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1c

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getRight()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getLeft()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->h:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0, v9, v9, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_14

    :cond_1c
    const v0, 0x1020002

    invoke-virtual {v7, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1d

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1d

    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    invoke-virtual {v0, v1, v9, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    :cond_1d
    :goto_14
    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object v1, v12

    move v2, v10

    move v4, v11

    invoke-virtual/range {v0 .. v5}, Landroid/widget/FrameLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    invoke-virtual {v12}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget v2, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    add-int/2addr v1, v2

    iget v2, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    add-int/2addr v1, v2

    invoke-static {v15, v1}, Ljava/lang/Math;->max(II)I

    move-result v9

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget v2, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v1, v0

    invoke-static {v8, v1}, Ljava/lang/Math;->max(II)I

    move-result v8

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredState()I

    move-result v0

    invoke-static {v14, v0}, Landroid/widget/FrameLayout;->combineMeasuredStates(II)I

    move-result v12

    if-eqz v13, :cond_1e

    invoke-virtual {v13}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1e

    iget-object v2, v7, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->o:Landroid/graphics/Rect;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x1

    move-object/from16 v0, p0

    move-object v1, v13

    invoke-virtual/range {v0 .. v6}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->b(Landroid/view/View;Landroid/graphics/Rect;ZZZZ)Z

    const/4 v3, 0x0

    const/4 v5, 0x0

    move v2, v10

    move v4, v11

    invoke-virtual/range {v0 .. v5}, Landroid/widget/FrameLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    :cond_1e
    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getPaddingLeft()I

    move-result v0

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getPaddingRight()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getPaddingTop()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getPaddingBottom()I

    move-result v3

    add-int/2addr v3, v2

    add-int/2addr v3, v8

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getSuggestedMinimumHeight()I

    move-result v2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/2addr v0, v1

    add-int/2addr v0, v9

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getSuggestedMinimumWidth()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0, v10, v12}, Landroid/widget/FrameLayout;->resolveSizeAndState(III)I

    move-result v0

    shl-int/lit8 v1, v12, 0x10

    invoke-static {v2, v11, v1}, Landroid/widget/FrameLayout;->resolveSizeAndState(III)I

    move-result v1

    invoke-virtual {v7, v0, v1}, Landroid/widget/FrameLayout;->setMeasuredDimension(II)V

    return-void
.end method

.method public requestFitSystemWindows()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->requestFitSystemWindows()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->D:Z

    return-void
.end method

.method public setActionBar(Ld1/a;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->a:Ld1/a;

    return-void
.end method

.method public setActionBarContextView(Lmiuix/appcompat/internal/app/widget/ActionBarContextView;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->c:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    return-void
.end method

.method public setAnimating(Z)V
    .locals 0

    iput-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->g:Z

    return-void
.end method

.method public setCallback(Landroid/view/Window$Callback;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->j:Landroid/view/Window$Callback;

    return-void
.end method

.method public setContentMask(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->n:Landroid/view/View;

    sget p1, Lr1/b;->a:I

    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->p:Landroid/view/View;

    return-void
.end method

.method public setOnStatusBarChangeListener(Ld1/l;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->B:Ld1/l;

    return-void
.end method

.method public setOverlayMode(Z)V
    .locals 0

    iput-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->C:Z

    return-void
.end method

.method public setRootSubDecor(Z)V
    .locals 0

    iput-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->E:Z

    return-void
.end method

.method public setSplitActionBarView(Lmiuix/appcompat/internal/app/widget/ActionBarContainer;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->b:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    return-void
.end method

.method public setTranslucentStatus(I)V
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->F:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->F:I

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->requestFitSystemWindows()V

    :cond_0
    return-void
.end method

.method public showContextMenuForChild(Landroid/view/View;)Z
    .locals 4

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->q:Lj1/b;

    if-nez v0, :cond_0

    new-instance v0, Lj1/b;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lj1/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->q:Lj1/b;

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$d;

    iput-object v1, v0, Lh1/g;->b:Lh1/g$a;

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lh1/g;->clear()V

    :goto_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->q:Lj1/b;

    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1, v0}, Lj1/c;->a(Landroid/view/View;Lj1/b;)V

    invoke-virtual {v0}, Lh1/g;->l()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-lez v2, :cond_1

    const v2, 0xc351

    invoke-static {v2, v3}, Landroid/util/EventLog;->writeEvent(II)I

    new-instance v2, Lh1/h;

    invoke-direct {v2, v0}, Lh1/h;-><init>(Lh1/g;)V

    invoke-virtual {v2, v1}, Lh1/h;->d(Landroid/os/IBinder;)V

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    iput-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->s:Lh1/h;

    if-eqz v2, :cond_2

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$d;

    iput-object p1, v2, Lh1/h;->d:Lh1/k$a;

    return v3

    :cond_2
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->showContextMenuForChild(Landroid/view/View;)Z

    move-result p1

    return p1
.end method

.method public showContextMenuForChild(Landroid/view/View;FF)Z
    .locals 5

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->q:Lj1/b;

    if-nez v0, :cond_0

    new-instance v0, Lj1/b;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lj1/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->q:Lj1/b;

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$d;

    iput-object v1, v0, Lh1/g;->b:Lh1/g$a;

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lh1/g;->clear()V

    :goto_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->q:Lj1/b;

    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1, v0}, Lj1/c;->a(Landroid/view/View;Lj1/b;)V

    invoke-virtual {v0}, Lh1/g;->l()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-lez v1, :cond_1

    const v1, 0xc351

    invoke-static {v1, v2}, Landroid/util/EventLog;->writeEvent(II)I

    new-instance v1, Lj1/d;

    invoke-direct {v1, v0}, Lj1/d;-><init>(Lh1/g;)V

    new-instance v0, Lj1/e;

    iget-object v3, v1, Lj1/d;->a:Lh1/g;

    iget-object v4, v3, Lh1/g;->c:Landroid/content/Context;

    invoke-direct {v0, v4, v3, v1}, Lj1/e;-><init>(Landroid/content/Context;Lh1/g;Landroid/widget/PopupWindow$OnDismissListener;)V

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iput-object p1, v0, Lj1/e;->w:Landroid/view/View;

    iput p2, v0, Lj1/e;->C:F

    iput p3, v0, Lj1/e;->D:F

    invoke-virtual {v0, p1, v3}, Lt1/a;->c(Landroid/view/View;Landroid/view/ViewGroup;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v0, Lj1/e;->B:Landroid/view/View;

    iget v4, v0, Lt1/a;->g:I

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setElevation(F)V

    iget-object v3, v0, Lj1/e;->B:Landroid/view/View;

    new-instance v4, Lt1/b;

    invoke-direct {v4}, Lt1/b;-><init>()V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    invoke-virtual {v0, p1, p2, p3}, Lj1/e;->i(Landroid/view/View;FF)V

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :cond_2
    :goto_1
    if-eqz v1, :cond_3

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$d;

    iput-object v0, v1, Lj1/d;->b:Lh1/k$a;

    move v0, v2

    goto :goto_2

    :cond_3
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->showContextMenuForChild(Landroid/view/View;)Z

    move-result v0

    :goto_2
    if-eqz v0, :cond_4

    return v2

    :cond_4
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Landroid/view/ViewParent;->showContextMenuForChild(Landroid/view/View;FF)Z

    move-result p1

    if-eqz p1, :cond_5

    goto :goto_3

    :cond_5
    const/4 v2, 0x0

    :goto_3
    return v2
.end method

.method public startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->f:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->f:Landroid/view/ActionMode;

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->getCallback()Landroid/view/Window$Callback;

    move-result-object v1

    if-eqz v1, :cond_2

    instance-of v0, p1, Lmiuix/view/e$a;

    if-eqz v0, :cond_1

    new-instance v0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$e;

    invoke-direct {v0, p0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$e;-><init>(Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;Landroid/view/ActionMode$Callback;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$b;

    invoke-direct {v0, p0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$b;-><init>(Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;Landroid/view/ActionMode$Callback;)V

    :goto_0
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->getCallback()Landroid/view/Window$Callback;

    move-result-object p1

    invoke-interface {p1, v0}, Landroid/view/Window$Callback;->onWindowStartingActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    :cond_2
    if-eqz v0, :cond_3

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->f:Landroid/view/ActionMode;

    :cond_3
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->f:Landroid/view/ActionMode;

    if-eqz p1, :cond_4

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->getCallback()Landroid/view/Window$Callback;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->getCallback()Landroid/view/Window$Callback;

    move-result-object p1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->f:Landroid/view/ActionMode;

    invoke-interface {p1, v0}, Landroid/view/Window$Callback;->onActionModeStarted(Landroid/view/ActionMode;)V

    :cond_4
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->f:Landroid/view/ActionMode;

    return-object p1
.end method

.method public startActionModeForChild(Landroid/view/View;Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .locals 1

    instance-of v0, p1, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->f:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    :cond_0
    instance-of v0, p2, Lmiuix/view/e$a;

    if-eqz v0, :cond_1

    new-instance v0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$e;

    invoke-direct {v0, p0, p2}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$e;-><init>(Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;Landroid/view/ActionMode$Callback;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$b;

    invoke-direct {v0, p0, p2}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$b;-><init>(Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;Landroid/view/ActionMode$Callback;)V

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->f:Landroid/view/ActionMode;

    goto :goto_1

    :cond_2
    invoke-virtual {p0, p2}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object p1

    :goto_1
    return-object p1
.end method
