.class public Lmiuix/appcompat/internal/app/widget/ActionBarView$f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiuix/appcompat/internal/app/widget/ActionBarView;->r(ILd1/b;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lmiuix/appcompat/internal/app/widget/ActionBarView;

.field public final synthetic b:Ld1/b;

.field public final synthetic c:Landroid/view/View;


# direct methods
.method public constructor <init>(Lmiuix/appcompat/internal/app/widget/ActionBarView;Ld1/b;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$f;->a:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iput-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$f;->b:Ld1/b;

    iput-object p3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$f;->c:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$f;->b:Ld1/b;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$f;->c:Landroid/view/View;

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView$f;->a:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-boolean v2, p1, Ld1/b;->j:Z

    if-nez v2, :cond_0

    const-string p1, "ActionBarDelegate"

    const-string v0, "Try to show immersion menu when immersion menu disabled"

    invoke-static {p1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_0
    if-eqz v0, :cond_4

    iget-object v2, p1, Ld1/b;->i:Lh1/g;

    if-nez v2, :cond_1

    invoke-virtual {p1}, Ld1/b;->e()Lh1/g;

    move-result-object v2

    iput-object v2, p1, Ld1/b;->i:Lh1/g;

    move-object v3, p1

    check-cast v3, Ld1/k;

    iget-object v3, v3, Ld1/b;->d:Ld1/j;

    invoke-virtual {v3, v2}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    :cond_1
    iget-object v2, p1, Ld1/b;->i:Lh1/g;

    move-object v3, p1

    check-cast v3, Ld1/k;

    iget-object v3, v3, Ld1/b;->d:Ld1/j;

    invoke-virtual {v3, v2}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p1, Ld1/b;->i:Lh1/g;

    invoke-virtual {v2}, Lh1/g;->hasVisibleItems()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p1, Ld1/b;->m:Lh1/d;

    if-nez v2, :cond_2

    new-instance v2, Lh1/e;

    iget-object v3, p1, Ld1/b;->i:Lh1/g;

    invoke-direct {v2, p1, v3}, Lh1/e;-><init>(Ld1/b;Landroid/view/Menu;)V

    iput-object v2, p1, Ld1/b;->m:Lh1/d;

    goto :goto_0

    :cond_2
    iget-object v3, p1, Ld1/b;->i:Lh1/g;

    check-cast v2, Lh1/e;

    iget-object v2, v2, Lh1/e;->w:Lh1/c;

    iget-object v4, v2, Lh1/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v3, v4}, Lh1/c;->a(Landroid/view/Menu;Ljava/util/ArrayList;)V

    invoke-virtual {v2}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    :goto_0
    iget-object v2, p1, Ld1/b;->m:Lh1/d;

    invoke-interface {v2}, Lh1/d;->isShowing()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object p1, p1, Ld1/b;->m:Lh1/d;

    check-cast p1, Lh1/e;

    invoke-virtual {p1, v0, v1}, Lh1/e;->f(Landroid/view/View;Landroid/view/ViewGroup;)V

    :cond_3
    :goto_1
    return-void

    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "You must specify a valid anchor view"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
