.class public Lmiuix/appcompat/internal/app/widget/ActionBarContextView$b;
.super La1/b;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->t(Z)Ld/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "La1/b<",
        "Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic b:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

.field public final synthetic c:Z

.field public final synthetic d:I

.field public final synthetic e:I

.field public final synthetic f:Li1/c;

.field public final synthetic g:I

.field public final synthetic h:F


# direct methods
.method public constructor <init>(Lmiuix/appcompat/internal/app/widget/ActionBarContextView;Ljava/lang/String;Li1/c;FIZII)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$b;->b:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    iput-object p3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$b;->f:Li1/c;

    iput p4, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$b;->h:F

    iput p5, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$b;->g:I

    iput-boolean p6, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$b;->c:Z

    iput p7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$b;->e:I

    iput p8, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$b;->d:I

    invoke-direct {p0, p2}, La1/b;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic c(Ljava/lang/Object;)F
    .locals 0

    check-cast p1, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    const/4 p1, 0x0

    return p1
.end method

.method public d(Ljava/lang/Object;F)V
    .locals 3

    check-cast p1, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$b;->f:Li1/c;

    if-eqz v0, :cond_0

    iget v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$b;->h:F

    iget v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$b;->g:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    sub-float/2addr v1, p2

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setTranslationY(F)V

    :cond_0
    float-to-int v0, p2

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->a(I)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$b;->b:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    iget-boolean v0, p1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->t:Z

    if-nez v0, :cond_3

    iget-boolean p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$b;->c:Z

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->v:Ljava/util/List;

    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/view/a;

    invoke-interface {v0, p2}, Lmiuix/view/a;->a(Z)V

    goto :goto_0

    :cond_2
    :goto_1
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$b;->b:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    const/4 p2, 0x1

    iput-boolean p2, p1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->t:Z

    goto :goto_3

    :cond_3
    iget v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$b;->e:I

    iget v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$b;->d:I

    if-ne v0, v1, :cond_4

    const/high16 p2, 0x3f800000    # 1.0f

    goto :goto_2

    :cond_4
    int-to-float v2, v1

    sub-float/2addr p2, v2

    sub-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr p2, v0

    :goto_2
    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$b;->c:Z

    invoke-virtual {p1, v0, p2}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->v(ZF)V

    :goto_3
    return-void
.end method
