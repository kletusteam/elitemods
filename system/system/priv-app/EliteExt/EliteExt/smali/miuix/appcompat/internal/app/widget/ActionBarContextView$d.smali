.class public Lmiuix/appcompat/internal/app/widget/ActionBarContextView$d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lz0/c$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/appcompat/internal/app/widget/ActionBarContextView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "d"
.end annotation


# instance fields
.field public a:Z

.field public final synthetic b:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;


# direct methods
.method public constructor <init>(Lmiuix/appcompat/internal/app/widget/ActionBarContextView;Z)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$d;->b:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$d;->a:Z

    return-void
.end method


# virtual methods
.method public a(Lz0/c;ZFF)V
    .locals 0

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$d;->b:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->m(Lmiuix/appcompat/internal/app/widget/ActionBarContextView;Z)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$d;->b:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    iput-boolean p2, p1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->t:Z

    iget-boolean p3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$d;->a:Z

    invoke-virtual {p1, p3}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->u(Z)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$d;->b:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    iget p3, p1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->w:I

    const/4 p4, 0x2

    if-ne p3, p4, :cond_0

    invoke-virtual {p1}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->b()V

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$d;->b:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    iput p2, p1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->w:I

    const/4 p3, 0x0

    iput-object p3, p1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->a0:Ld/o;

    iget-boolean p3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$d;->a:Z

    const/16 p4, 0x8

    if-eqz p3, :cond_1

    move p3, p2

    goto :goto_0

    :cond_1
    move p3, p4

    :goto_0
    invoke-virtual {p1, p3}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->setVisibility(I)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$d;->b:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    iget-object p3, p1, Lmiuix/appcompat/internal/app/widget/a;->o:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz p3, :cond_3

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    if-eqz p1, :cond_3

    iget-boolean p3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView$d;->a:Z

    if-eqz p3, :cond_2

    goto :goto_1

    :cond_2
    move p2, p4

    :goto_1
    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_3
    return-void
.end method
