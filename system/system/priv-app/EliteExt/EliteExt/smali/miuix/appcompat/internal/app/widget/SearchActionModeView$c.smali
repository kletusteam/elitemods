.class public Lmiuix/appcompat/internal/app/widget/SearchActionModeView$c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lmiuix/view/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/appcompat/internal/app/widget/SearchActionModeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "c"
.end annotation


# instance fields
.field public final synthetic a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;


# direct methods
.method public constructor <init>(Lmiuix/appcompat/internal/app/widget/SearchActionModeView;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$c;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$c;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget-object v0, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->q:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$c;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->q:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$c;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->q:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    :cond_0
    return-void
.end method

.method public f(ZF)V
    .locals 2

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/high16 p1, 0x3f800000    # 1.0f

    sub-float p2, p1, p2

    :goto_0
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$c;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->q:Landroid/view/View;

    invoke-virtual {p1, p2}, Landroid/view/View;->setAlpha(F)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$c;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    invoke-virtual {p1}, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->h()Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 p1, 0x0

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$c;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget-object p2, p2, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {p2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/View;

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Landroid/view/View;->getTranslationY()F

    move-result p1

    :cond_1
    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$c;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget-object v0, p2, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->q:Landroid/view/View;

    iget-object v1, p2, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->a:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v1, :cond_2

    iget p2, p2, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->o:I

    goto :goto_1

    :cond_2
    const/4 p2, 0x0

    :goto_1
    int-to-float p2, p2

    add-float/2addr p1, p2

    invoke-virtual {v0, p1}, Landroid/view/View;->setTranslationY(F)V

    :cond_3
    return-void
.end method

.method public g(Z)V
    .locals 1

    const/16 v0, 0x8

    if-eqz p1, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$c;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->t:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result p1

    if-lez p1, :cond_1

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$c;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->q:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$c;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->q:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$c;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->q:Landroid/view/View;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView$c;->a:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->q:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    :cond_1
    :goto_0
    return-void
.end method
