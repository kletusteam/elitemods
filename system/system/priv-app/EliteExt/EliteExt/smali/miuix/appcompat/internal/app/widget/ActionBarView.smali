.class public Lmiuix/appcompat/internal/app/widget/ActionBarView;
.super Lmiuix/appcompat/internal/app/widget/a;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/appcompat/internal/app/widget/ActionBarView$h;,
        Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;,
        Lmiuix/appcompat/internal/app/widget/ActionBarView$i;,
        Lmiuix/appcompat/internal/app/widget/ActionBarView$j;
    }
.end annotation


# instance fields
.field public A:Le1/a;

.field public A0:Ljava/lang/CharSequence;

.field public B:Z

.field public B0:Z

.field public C:I

.field public final C0:Landroid/view/View$OnClickListener;

.field public D:Landroid/content/Context;

.field public D0:Li1/a;

.field public E:Landroid/view/View;

.field public E0:Landroid/view/View;

.field public final F:Landroid/text/TextWatcher;

.field public F0:Z

.field public G:I

.field public G0:I

.field public H:Landroid/view/View;

.field public H0:I

.field public I:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

.field public final I0:Landroid/view/View$OnClickListener;

.field public J:Lx0/j;

.field public J0:Z

.field public K:Landroid/view/View;

.field public K0:Landroid/view/Window$Callback;

.field public final L:Landroid/view/View$OnClickListener;

.field public M:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

.field public N:Lmiuix/appcompat/internal/app/widget/ActionBarView$h;

.field public O:Landroid/graphics/drawable/Drawable;

.field public P:I

.field public Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

.field public final R:I

.field public S:Landroid/graphics/drawable/Drawable;

.field public T:I

.field public U:Landroid/view/View;

.field public V:Z

.field public W:Z

.field public a0:Z

.field public b0:Landroid/widget/ProgressBar;

.field public c0:Z

.field public d0:Z

.field public e0:Z

.field public f0:I

.field public g0:Landroid/graphics/drawable/Drawable;

.field public h0:Li1/a;

.field public i0:Landroid/widget/FrameLayout;

.field public j0:Lmiuix/appcompat/internal/app/widget/a$b;

.field public k0:Lmiuix/springback/view/SpringBackLayout;

.field public l0:I

.field public m0:I

.field public n0:Z

.field public o0:Lh1/g;

.field public p0:I

.field public q0:Ljava/lang/Runnable;

.field public r:Ld/a$c;

.field public r0:Landroid/widget/Scroller;

.field public s:Landroid/widget/FrameLayout;

.field public s0:I

.field public t:Lmiuix/appcompat/internal/app/widget/a$b;

.field public t0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

.field public u:Landroid/widget/FrameLayout;

.field public u0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

.field public v:I

.field public v0:Landroid/widget/SpinnerAdapter;

.field public w:I

.field public w0:Landroid/view/View;

.field public x:Lmiuix/springback/view/SpringBackLayout;

.field public x0:Lw0/h;

.field public y:I

.field public y0:Ljava/lang/CharSequence;

.field public z:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

.field public z0:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 12

    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/internal/app/widget/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, -0x1

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->G:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->B:Z

    new-instance v1, Lmiuix/appcompat/internal/app/widget/ActionBarView$b;

    invoke-direct {v1, p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView$b;-><init>(Lmiuix/appcompat/internal/app/widget/ActionBarView;)V

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->L:Landroid/view/View$OnClickListener;

    new-instance v1, Lmiuix/appcompat/internal/app/widget/ActionBarView$c;

    invoke-direct {v1, p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView$c;-><init>(Lmiuix/appcompat/internal/app/widget/ActionBarView;)V

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->I0:Landroid/view/View$OnClickListener;

    new-instance v1, Lmiuix/appcompat/internal/app/widget/ActionBarView$d;

    invoke-direct {v1, p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView$d;-><init>(Lmiuix/appcompat/internal/app/widget/ActionBarView;)V

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->C0:Landroid/view/View$OnClickListener;

    new-instance v1, Lmiuix/appcompat/internal/app/widget/ActionBarView$e;

    invoke-direct {v1, p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView$e;-><init>(Lmiuix/appcompat/internal/app/widget/ActionBarView;)V

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->F:Landroid/text/TextWatcher;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->d0:Z

    iput-boolean v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->e0:Z

    iput v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->G0:I

    new-instance v2, Lmiuix/appcompat/internal/app/widget/a$b;

    invoke-direct {v2}, Lmiuix/appcompat/internal/app/widget/a$b;-><init>()V

    iput-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t:Lmiuix/appcompat/internal/app/widget/a$b;

    new-instance v2, Lmiuix/appcompat/internal/app/widget/a$b;

    invoke-direct {v2}, Lmiuix/appcompat/internal/app/widget/a$b;-><init>()V

    iput-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->j0:Lmiuix/appcompat/internal/app/widget/a$b;

    iput-boolean v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->F0:Z

    iput-boolean v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->n0:Z

    iput-boolean v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->V:Z

    iput-boolean v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->W:Z

    const/4 v2, 0x0

    iput-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x0:Lw0/h;

    new-instance v3, Lmiuix/appcompat/internal/app/widget/ActionBarView$g;

    invoke-direct {v3, p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView$g;-><init>(Lmiuix/appcompat/internal/app/widget/ActionBarView;)V

    iput-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->q0:Ljava/lang/Runnable;

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->D:Landroid/content/Context;

    new-instance v3, Landroid/widget/Scroller;

    invoke-direct {v3, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->r0:Landroid/widget/Scroller;

    iput-boolean v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->V:Z

    iput-boolean v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->W:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070096

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    iput v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->H0:I

    new-instance v3, Landroid/widget/FrameLayout;

    invoke-direct {v3, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    const v4, 0x7f0a0029

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setId(I)V

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    const/16 v4, 0x11

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setForegroundGravity(I)V

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    iget v4, p0, Lmiuix/appcompat/internal/app/widget/a;->h:I

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    if-nez v4, :cond_0

    move v4, v6

    goto :goto_0

    :cond_0
    move v4, v5

    :goto_0
    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setAlpha(F)V

    new-instance v3, Landroid/widget/FrameLayout;

    invoke-direct {v3, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    const v4, 0x7f0a002d

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setId(I)V

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    iget v4, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->H0:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f070098

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v7

    iget v8, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->H0:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f070094

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v9

    invoke-virtual {v3, v4, v7, v8, v9}, Landroid/widget/FrameLayout;->setPaddingRelative(IIII)V

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    iget v4, p0, Lmiuix/appcompat/internal/app/widget/a;->h:I

    if-nez v4, :cond_1

    goto :goto_1

    :cond_1
    move v5, v6

    :goto_1
    invoke-virtual {v3, v5}, Landroid/widget/FrameLayout;->setAlpha(F)V

    new-instance v3, Lmiuix/springback/view/SpringBackLayout;

    invoke-direct {v3, p1, v2}, Lmiuix/springback/view/SpringBackLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    const v4, 0x7f0a002a

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setId(I)V

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v3, v0}, Lmiuix/springback/view/SpringBackLayout;->setScrollOrientation(I)V

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    new-instance v3, Lmiuix/springback/view/SpringBackLayout;

    invoke-direct {v3, p1, v2}, Lmiuix/springback/view/SpringBackLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    const v2, 0x7f0a002e

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->setId(I)V

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v2, v0}, Lmiuix/springback/view/SpringBackLayout;->setScrollOrientation(I)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t:Lmiuix/appcompat/internal/app/widget/a$b;

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Lmiuix/appcompat/internal/app/widget/a$b;->b(Landroid/view/View;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->j0:Lmiuix/appcompat/internal/app/widget/a$b;

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Lmiuix/appcompat/internal/app/widget/a$b;->b(Landroid/view/View;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t:Lmiuix/appcompat/internal/app/widget/a$b;

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v0, v2}, Lmiuix/appcompat/internal/app/widget/a$b;->b(Landroid/view/View;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->j0:Lmiuix/appcompat/internal/app/widget/a$b;

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v0, v2}, Lmiuix/appcompat/internal/app/widget/a$b;->b(Landroid/view/View;)V

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    sget-object v0, Landroidx/emoji2/text/l;->T:[I

    const v2, 0x10102ce

    invoke-virtual {p1, p2, v0, v2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    const/4 v0, 0x6

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->m0:I

    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A0:Ljava/lang/CharSequence;

    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->y0:Ljava/lang/CharSequence;

    const/16 v0, 0x35

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->B0:Z

    const/4 v0, 0x5

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->g0:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->S:Landroid/graphics/drawable/Drawable;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/16 v2, 0xd

    const v3, 0x7f0d002a

    invoke-virtual {p2, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->R:I

    const/16 v2, 0xa

    invoke-virtual {p2, v2, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->C:I

    const/16 v2, 0xb

    invoke-virtual {p2, v2, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->w:I

    const/16 v2, 0xc

    invoke-virtual {p2, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s0:I

    const/16 v2, 0xe

    invoke-virtual {p2, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->f0:I

    const/4 v2, 0x7

    invoke-virtual {p2, v2, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    invoke-virtual {p0, v2}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setDisplayOptions(I)V

    const/16 v2, 0x9

    invoke-virtual {p2, v2, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, v2, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E:Landroid/view/View;

    iput v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->m0:I

    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v0

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/a;->e:I

    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    new-instance p2, Li1/a;

    const/4 v8, 0x0

    const v4, 0x102002c

    const/4 v9, 0x0

    const/4 v10, 0x0

    iget-object v11, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A0:Ljava/lang/CharSequence;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p2

    move-object v2, p1

    move-object v7, v11

    invoke-direct/range {v1 .. v7}, Li1/a;-><init>(Landroid/content/Context;IIIILjava/lang/CharSequence;)V

    iput-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->h0:Li1/a;

    new-instance p2, Li1/a;

    const v3, 0x1020016

    move-object v0, p2

    move-object v1, p1

    move v2, v8

    move v4, v9

    move v5, v10

    move-object v6, v11

    invoke-direct/range {v0 .. v6}, Li1/a;-><init>(Landroid/content/Context;IIIILjava/lang/CharSequence;)V

    iput-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->D0:Li1/a;

    new-instance p1, Lmiuix/appcompat/internal/app/widget/g;

    const/4 p2, 0x2

    invoke-direct {p1, p0, p2}, Lmiuix/appcompat/internal/app/widget/g;-><init>(Lmiuix/appcompat/internal/app/widget/ActionBarView;I)V

    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private getCircularProgressBar()Landroid/widget/ProgressBar;
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->b0:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_0
    return-object v0
.end method

.method private getHorizontalProgressBar()Landroid/widget/ProgressBar;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method private getIcon()Landroid/graphics/drawable/Drawable;
    .locals 4

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->T:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->D:Landroid/content/Context;

    instance-of v2, v0, Landroid/app/Activity;

    if-eqz v2, :cond_0

    :try_start_0
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->D:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getActivityIcon(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->S:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "ActionBarView"

    const-string v3, "Activity component name not found!"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->S:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->D:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->D:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->S:Landroid/graphics/drawable/Drawable;

    :cond_1
    iget v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->T:I

    or-int/2addr v0, v1

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->T:I

    :cond_2
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->S:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private getLogo()Landroid/graphics/drawable/Drawable;
    .locals 4

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->T:I

    const/4 v1, 0x2

    and-int/2addr v0, v1

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->D:Landroid/content/Context;

    instance-of v2, v0, Landroid/app/Activity;

    if-eqz v2, :cond_0

    :try_start_0
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->D:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getActivityLogo(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->g0:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "ActionBarView"

    const-string v3, "Activity component name not found!"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->g0:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->D:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->D:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/pm/ApplicationInfo;->loadLogo(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->g0:Landroid/graphics/drawable/Drawable;

    :cond_1
    iget v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->T:I

    or-int/2addr v0, v1

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->T:I

    :cond_2
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->g0:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public static synthetic m(Lmiuix/appcompat/internal/app/widget/ActionBarView;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setTitleVisibility(Z)V

    return-void
.end method

.method public static synthetic n(Lmiuix/appcompat/internal/app/widget/ActionBarView;)Landroid/graphics/drawable/Drawable;
    .locals 0

    invoke-direct {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0
.end method

.method private setTitleImpl(Ljava/lang/CharSequence;)V
    .locals 3

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A0:Ljava/lang/CharSequence;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    if-eqz v0, :cond_3

    iget-object v0, v0, Le1/a;->e:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p1, :cond_0

    iget-object v0, v0, Le1/a;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->J:Lx0/j;

    invoke-virtual {v0, p1}, Lx0/j;->c(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->K:Landroid/view/View;

    const/4 v2, 0x1

    if-nez v0, :cond_2

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->G:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A0:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->y0:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    :goto_0
    invoke-direct {p0, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setTitleVisibility(Z)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->y0:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    iget-object v0, v0, Le1/a;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    new-instance v0, Lmiuix/appcompat/internal/app/widget/g;

    invoke-direct {v0, p0, v2}, Lmiuix/appcompat/internal/app/widget/g;-><init>(Lmiuix/appcompat/internal/app/widget/ActionBarView;I)V

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    :cond_3
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->h0:Li1/a;

    if-eqz v0, :cond_4

    iput-object p1, v0, Li1/a;->h:Ljava/lang/CharSequence;

    :cond_4
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->D0:Li1/a;

    if-eqz v0, :cond_5

    iput-object p1, v0, Li1/a;->h:Ljava/lang/CharSequence;

    :cond_5
    return-void
.end method

.method private setTitleVisibility(Z)V
    .locals 5

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    const/16 v1, 0x8

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    move v3, v2

    goto :goto_0

    :cond_0
    move v3, v1

    :goto_0
    iget-object v0, v0, Le1/a;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_1
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->J:Lx0/j;

    if-eqz v0, :cond_3

    if-eqz p1, :cond_2

    move v3, v2

    goto :goto_1

    :cond_2
    move v3, v1

    :goto_1
    iget-object v0, v0, Lx0/j;->c:Ljava/lang/Object;

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_3
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E0:Landroid/view/View;

    if-eqz v0, :cond_8

    if-eqz p1, :cond_7

    iget p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->G:I

    and-int/lit8 v3, p1, 0x4

    const/4 v4, 0x1

    if-eqz v3, :cond_4

    move v3, v4

    goto :goto_2

    :cond_4
    move v3, v2

    :goto_2
    and-int/lit8 p1, p1, 0x2

    if-eqz p1, :cond_5

    goto :goto_3

    :cond_5
    move v4, v2

    :goto_3
    if-nez v4, :cond_7

    if-eqz v3, :cond_6

    move v1, v2

    goto :goto_4

    :cond_6
    const/4 v1, 0x4

    :cond_7
    :goto_4
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_8
    return-void
.end method


# virtual methods
.method public g(II)V
    .locals 7

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x0:Lw0/h;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lu0/d;->cancel()V

    :cond_0
    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, v0, :cond_1

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result p1

    iget v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->l0:I

    add-int/2addr p1, v2

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->p0:I

    goto :goto_0

    :cond_1
    if-nez p1, :cond_2

    iput v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->p0:I

    :cond_2
    :goto_0
    new-instance p1, Lv0/a;

    invoke-direct {p1, v1}, Lv0/a;-><init>(Z)V

    new-array v2, v0, [Ly0/b;

    new-instance v3, Lmiuix/appcompat/internal/app/widget/ActionBarView$i;

    invoke-direct {v3, p0, p2}, Lmiuix/appcompat/internal/app/widget/ActionBarView$i;-><init>(Lmiuix/appcompat/internal/app/widget/ActionBarView;I)V

    aput-object v3, v2, v1

    iget-object v3, p1, Lv0/a;->e:Ljava/util/HashSet;

    invoke-static {v3, v2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    if-ne p2, v0, :cond_3

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v2

    goto :goto_1

    :cond_3
    move v2, v1

    :goto_1
    if-ne p2, v0, :cond_4

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    const/4 v3, 0x4

    invoke-virtual {p2, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {p2, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_2

    :cond_4
    if-nez p2, :cond_5

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {p2, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_5
    :goto_2
    new-array p2, v1, [Ljava/lang/Object;

    invoke-static {p2}, Lu0/a;->g([Ljava/lang/Object;)Lw0/h;

    move-result-object p2

    const-wide/16 v3, 0x1

    invoke-interface {p2, v3, v4}, Lw0/h;->j(J)Lw0/h;

    move-result-object p2

    const/4 v3, 0x2

    new-array v4, v3, [Ljava/lang/Object;

    const-string v5, "actionbar_state_change"

    aput-object v5, v4, v1

    iget v6, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->p0:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v0

    invoke-interface {p2, v4}, Lw0/h;->c([Ljava/lang/Object;)Lw0/h;

    move-result-object p2

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v5, v4, v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v0

    aput-object p1, v4, v3

    invoke-interface {p2, v4}, Lw0/h;->h([Ljava/lang/Object;)Lw0/h;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x0:Lw0/h;

    return-void
.end method

.method public generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    new-instance v0, Ld/a$a;

    const v1, 0x800013

    invoke-direct {v0, v1}, Ld/a$a;-><init>(I)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    new-instance v0, Ld/a$a;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ld/a$a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 0

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public bridge synthetic getActionBarTransitionListener()Ld1/c;
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/internal/app/widget/a;->getActionBarTransitionListener()Ld1/c;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getActionMenuView()Li1/c;
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/internal/app/widget/a;->getActionMenuView()Li1/c;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getAnimatedVisibility()I
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/internal/app/widget/a;->getAnimatedVisibility()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getContentHeight()I
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/internal/app/widget/a;->getContentHeight()I

    move-result v0

    return v0
.end method

.method public getCustomNavigationView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E:Landroid/view/View;

    return-object v0
.end method

.method public getDisplayOptions()I
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->G:I

    return v0
.end method

.method public getDropdownAdapter()Landroid/widget/SpinnerAdapter;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->v0:Landroid/widget/SpinnerAdapter;

    return-object v0
.end method

.method public getDropdownSelectedPosition()I
    .locals 1

    const/4 v0, 0x0

    throw v0
.end method

.method public getEndView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->H:Landroid/view/View;

    return-object v0
.end method

.method public bridge synthetic getExpandState()I
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/internal/app/widget/a;->getExpandState()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getMenuView()Li1/c;
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/internal/app/widget/a;->getMenuView()Li1/c;

    move-result-object v0

    return-object v0
.end method

.method public getNavigationMode()I
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->m0:I

    return v0
.end method

.method public getStartView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->w0:Landroid/view/View;

    return-object v0
.end method

.method public getSubtitle()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->y0:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A0:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public h(II)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->p0:I

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->r0:Landroid/widget/Scroller;

    invoke-virtual {p1}, Landroid/widget/Scroller;->isFinished()Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->r0:Landroid/widget/Scroller;

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    :cond_0
    if-eqz p2, :cond_1

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_1
    if-nez p2, :cond_2

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getHeight()I

    move-result p1

    iget p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->v:I

    sub-int/2addr p1, p2

    iget p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->y:I

    add-int/2addr p1, p2

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->p0:I

    :goto_0
    return-void
.end method

.method public final o(Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;)V
    .locals 2

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->z:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    iput-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->I:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    iput-object p3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    iput-object p4, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->u0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result p1

    const/4 p2, 0x0

    const/4 p3, -0x1

    const/4 p4, -0x2

    if-nez p1, :cond_2

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->z:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz p1, :cond_0

    invoke-virtual {p1, p2}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->z:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->removeAllViews()V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->I:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz p1, :cond_1

    invoke-virtual {p1, p2}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->I:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {p1, p2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    :cond_1
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->removeAllViews()V

    goto/16 :goto_1

    :cond_2
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_9

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->D:Landroid/content/Context;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p2}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f040038

    invoke-static {p1, v1, p2}, Lr1/a;->c(Landroid/content/Context;IZ)Z

    move-result p1

    if-nez p1, :cond_7

    instance-of p1, v0, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz p1, :cond_3

    goto :goto_0

    :cond_3
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz p1, :cond_4

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {p2, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {p1, p2}, Lmiuix/springback/view/SpringBackLayout;->setTarget(Landroid/view/View;)V

    :cond_4
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->u0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz p1, :cond_5

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {p2, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->u0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {p1, p2}, Lmiuix/springback/view/SpringBackLayout;->setTarget(Landroid/view/View;)V

    :cond_5
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    if-nez p1, :cond_6

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    new-instance p2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {p2, p3, p4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_6
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    if-nez p1, :cond_9

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    new-instance p2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {p2, p3, p4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    :cond_7
    :goto_0
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->removeAllViews()V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->z:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz p1, :cond_8

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {p2, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    :cond_8
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->removeAllViews()V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->I:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz p1, :cond_9

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    invoke-virtual {p2, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    :cond_9
    :goto_1
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {p0, p0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x(Landroid/view/ViewGroup;Landroid/view/View;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    invoke-virtual {p0, p0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x(Landroid/view/ViewGroup;Landroid/view/View;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->z:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {p1}, Landroid/widget/HorizontalScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    if-eqz p1, :cond_a

    iput p4, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput p3, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    :cond_a
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->I:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {p1}, Landroid/widget/HorizontalScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    if-eqz p1, :cond_b

    iput p4, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput p4, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    :cond_b
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {p1}, Landroid/widget/HorizontalScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    if-eqz p1, :cond_c

    iput p4, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput p3, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    :cond_c
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->u0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {p1}, Landroid/widget/HorizontalScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    if-eqz p1, :cond_d

    iput p4, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput p3, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    :cond_d
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->z()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/app/widget/a;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getDisplayOptions()I

    move-result v0

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lr1/b;->a()Z

    move-result v1

    if-nez v1, :cond_1

    iget p1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    iget-object p1, v0, Le1/a;->b:Landroid/widget/TextView;

    new-instance v1, Le1/b;

    const/4 v2, 0x3

    invoke-direct {v1, v0, v2}, Le1/b;-><init>(Ljava/lang/Object;I)V

    goto :goto_0

    :cond_0
    iget-object p1, v0, Le1/a;->b:Landroid/widget/TextView;

    new-instance v1, Le1/b;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Le1/b;-><init>(Ljava/lang/Object;I)V

    :goto_0
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    :cond_1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x7f070096

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p1

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->H0:I

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070098

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iget v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->H0:I

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070094

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-virtual {v0, p1, v1, v2, v3}, Landroid/widget/FrameLayout;->setPaddingRelative(IIII)V

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x7f070082

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070081

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v2

    invoke-virtual {p0, p1, v1, v0, v2}, Landroid/view/ViewGroup;->setPaddingRelative(IIII)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->z:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    const/4 v0, -0x1

    const/4 v1, -0x2

    if-eqz p1, :cond_2

    iget-boolean v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->a0:Z

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Landroid/widget/HorizontalScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    if-eqz p1, :cond_2

    iput v1, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v0, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    :cond_2
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->I:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz p1, :cond_3

    iget-boolean v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->a0:Z

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Landroid/widget/HorizontalScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    if-eqz p1, :cond_3

    iput v1, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v1, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    :cond_3
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz p1, :cond_4

    iget-boolean v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->a0:Z

    if-eqz v2, :cond_4

    invoke-virtual {p1}, Landroid/widget/HorizontalScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    if-eqz p1, :cond_4

    iput v1, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v0, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    :cond_4
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->u0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz p1, :cond_5

    iget-boolean v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->a0:Z

    if-eqz v2, :cond_5

    invoke-virtual {p1}, Landroid/widget/HorizontalScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    if-eqz p1, :cond_5

    iput v1, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v0, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    :cond_5
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 2

    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Li1/b;->l(Z)Z

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    iget-object v0, v0, Li1/b;->i:Li1/b$b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lh1/h;->a()V

    :cond_0
    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 21

    move-object/from16 v6, p0

    move/from16 v7, p2

    move/from16 v8, p4

    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v0

    iget-object v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E:Landroid/view/View;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    :cond_0
    move v9, v0

    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v0

    :goto_0
    move v10, v0

    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v0

    iget-object v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_2

    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    iget-object v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v1

    :goto_1
    move v11, v1

    iget v1, v6, Lmiuix/appcompat/internal/app/widget/a;->h:I

    const/4 v2, 0x1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_3

    iget v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->p0:I

    goto :goto_2

    :cond_3
    if-ne v1, v2, :cond_4

    add-int v1, v0, v11

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    :goto_2
    move v12, v1

    sub-int v1, p5, p3

    sub-int v13, v1, v11

    add-int v1, v0, v11

    sub-int/2addr v1, v12

    int-to-float v1, v1

    int-to-float v0, v0

    div-float v14, v1, v0

    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/a;->q:Ld1/c;

    if-eqz v0, :cond_5

    iget v1, v6, Lmiuix/appcompat/internal/app/widget/a;->i:F

    sub-float/2addr v1, v14

    invoke-interface {v0, v1, v14}, Ld1/c;->b(FF)V

    :cond_5
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getPaddingStart()I

    move-result v0

    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v15

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getLayoutDirection()I

    move-result v1

    if-ne v1, v2, :cond_6

    goto :goto_3

    :cond_6
    const/4 v2, 0x0

    :goto_3
    move v5, v2

    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v1

    sub-int v1, v9, v1

    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v2

    sub-int v4, v1, v2

    const/16 v16, 0x0

    if-gtz v4, :cond_7

    move/from16 p1, v11

    move/from16 v18, v12

    move/from16 p3, v14

    goto/16 :goto_1f

    :cond_7
    iget-object v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->K:Landroid/view/View;

    if-eqz v1, :cond_8

    iget-object v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->M:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    goto :goto_4

    :cond_8
    iget-object v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    :goto_4
    const/16 v2, 0x8

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v3

    if-eq v3, v2, :cond_9

    add-int/lit8 v3, v0, 0x0

    invoke-virtual {v6, v1, v3, v15, v4}, Lmiuix/appcompat/internal/app/widget/a;->i(Landroid/view/View;III)I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    goto :goto_5

    :cond_9
    iget-object v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->w0:Landroid/view/View;

    if-eqz v1, :cond_a

    invoke-virtual {v6, v1, v0, v15, v4}, Lmiuix/appcompat/internal/app/widget/a;->i(Landroid/view/View;III)I

    move-result v1

    :goto_5
    add-int/2addr v0, v1

    :cond_a
    iget-object v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->K:Landroid/view/View;

    if-nez v1, :cond_1b

    invoke-virtual/range {p0 .. p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t()Z

    move-result v17

    if-eqz v17, :cond_15

    iget-object v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E0:Landroid/view/View;

    if-eqz v1, :cond_b

    invoke-virtual {v6, v1, v0, v15, v4}, Lmiuix/appcompat/internal/app/widget/a;->i(Landroid/view/View;III)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    move v3, v0

    invoke-virtual/range {p0 .. p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->v()Z

    move-result v0

    if-eqz v0, :cond_14

    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_d

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-eq v0, v2, :cond_d

    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    sub-int/2addr v2, v1

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v1, v0, Landroid/widget/LinearLayout$LayoutParams;

    if-eqz v1, :cond_c

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0}, Landroid/widget/LinearLayout$LayoutParams;->getMarginStart()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    goto :goto_6

    :cond_c
    const/4 v0, 0x0

    :goto_6
    sub-int/2addr v2, v0

    goto :goto_7

    :cond_d
    move v2, v3

    :goto_7
    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    if-eqz v0, :cond_12

    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A0:Ljava/lang/CharSequence;

    if-nez v0, :cond_e

    goto :goto_b

    :cond_e
    iget-boolean v0, v6, Lmiuix/appcompat/internal/app/widget/a;->l:Z

    if-nez v0, :cond_10

    invoke-virtual/range {p0 .. p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getExpandState()I

    move-result v0

    if-eqz v0, :cond_f

    goto :goto_8

    :cond_f
    move/from16 p1, v3

    goto :goto_a

    :cond_10
    :goto_8
    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    iget-object v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A0:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    move/from16 p1, v3

    iget-object v3, v0, Le1/a;->e:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v1

    iget-object v0, v0, Le1/a;->e:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, v1, v0

    if-gtz v0, :cond_11

    const/4 v0, 0x1

    goto :goto_9

    :cond_11
    const/4 v0, 0x0

    :goto_9
    if-eqz v0, :cond_13

    :goto_a
    const/4 v0, 0x1

    goto :goto_c

    :cond_12
    :goto_b
    move/from16 p1, v3

    :cond_13
    const/4 v0, 0x0

    :goto_c
    iput-boolean v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->B:Z

    invoke-virtual/range {p0 .. p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->z()V

    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v0

    iget-object v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v1

    const/4 v3, 0x2

    invoke-static {v4, v1, v3, v15}, Landroidx/activity/result/a;->c(IIII)I

    move-result v3

    move/from16 p3, v4

    iget-object v4, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    add-int v18, v2, v0

    add-int v19, v3, v1

    move-object/from16 v0, p0

    move-object v1, v4

    move/from16 v4, p1

    move/from16 p1, v11

    move/from16 v11, p3

    move/from16 p3, v14

    move v14, v4

    move/from16 v4, v18

    move/from16 v18, v12

    move v12, v5

    move/from16 v5, v19

    invoke-static/range {v0 .. v5}, Landroidx/emoji2/text/l;->J(Landroid/view/ViewGroup;Landroid/view/View;IIII)V

    move v0, v14

    goto :goto_d

    :cond_14
    move/from16 p1, v11

    move/from16 v18, v12

    move/from16 p3, v14

    move v14, v3

    move v11, v4

    move v12, v5

    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {v6, v0, v14, v15, v11}, Lmiuix/appcompat/internal/app/widget/a;->i(Landroid/view/View;III)I

    move-result v0

    add-int/2addr v0, v14

    goto :goto_d

    :cond_15
    move/from16 p1, v11

    move/from16 v18, v12

    move/from16 p3, v14

    move v11, v4

    move v12, v5

    :goto_d
    iget v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->m0:I

    if-eqz v1, :cond_1c

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1c

    const/4 v3, 0x2

    if-eq v1, v3, :cond_16

    goto :goto_11

    :cond_16
    iget-object v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v1

    if-ne v1, v2, :cond_17

    iget-object v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    instance-of v1, v1, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v1, :cond_17

    iget-object v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    goto :goto_e

    :cond_17
    move-object/from16 v1, v16

    :goto_e
    if-eqz v1, :cond_1c

    invoke-virtual {v1}, Landroid/widget/HorizontalScrollView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {v1}, Landroid/widget/HorizontalScrollView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    sub-int/2addr v2, v0

    div-int/lit8 v2, v2, 0x2

    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v3

    add-int/2addr v3, v0

    div-int/lit8 v3, v3, 0x2

    if-eqz v17, :cond_18

    iget v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->f0:I

    add-int/2addr v2, v0

    sub-int/2addr v3, v0

    :cond_18
    move v0, v3

    if-eqz v12, :cond_19

    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v3

    sub-int/2addr v3, v0

    goto :goto_f

    :cond_19
    move v3, v2

    :goto_f
    if-eqz v12, :cond_1a

    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v4

    sub-int/2addr v4, v2

    goto :goto_10

    :cond_1a
    move v4, v0

    :goto_10
    iget-object v2, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    add-int/2addr v1, v15

    invoke-virtual {v2, v3, v15, v4, v1}, Landroid/widget/FrameLayout;->layout(IIII)V

    goto :goto_11

    :cond_1b
    move/from16 p1, v11

    move/from16 v18, v12

    move/from16 p3, v14

    move v11, v4

    move v12, v5

    :cond_1c
    :goto_11
    sub-int v1, v8, v7

    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getPaddingEnd()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, v6, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    if-eqz v2, :cond_1d

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, v6, :cond_1d

    iget-object v2, v6, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    invoke-virtual {v6, v2, v1, v15, v11}, Lmiuix/appcompat/internal/app/widget/a;->j(Landroid/view/View;III)I

    iget-object v2, v6, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    :cond_1d
    iget-object v2, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->H:Landroid/view/View;

    if-eqz v2, :cond_1e

    invoke-virtual {v6, v2, v1, v15, v11}, Lmiuix/appcompat/internal/app/widget/a;->j(Landroid/view/View;III)I

    iget-object v2, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->H:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    :cond_1e
    iget-object v2, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->b0:Landroid/widget/ProgressBar;

    if-eqz v2, :cond_1f

    invoke-virtual {v2}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_1f

    iget-object v2, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->b0:Landroid/widget/ProgressBar;

    iget v3, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s0:I

    sub-int v3, v1, v3

    invoke-virtual {v6, v2, v3, v15, v11}, Lmiuix/appcompat/internal/app/widget/a;->j(Landroid/view/View;III)I

    iget-object v2, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->b0:Landroid/widget/ProgressBar;

    invoke-virtual {v2}, Landroid/widget/ProgressBar;->getMeasuredWidth()I

    move-result v2

    iget v3, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s0:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    sub-int/2addr v1, v2

    :cond_1f
    iget-object v2, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->U:Landroid/view/View;

    if-eqz v2, :cond_20

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_20

    iget-object v2, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->U:Landroid/view/View;

    invoke-virtual {v6, v2, v1, v15, v11}, Lmiuix/appcompat/internal/app/widget/a;->j(Landroid/view/View;III)I

    iget-object v2, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->U:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    :cond_20
    iget-object v2, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->K:Landroid/view/View;

    if-eqz v2, :cond_21

    goto :goto_12

    :cond_21
    iget v2, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->G:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_22

    iget-object v2, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E:Landroid/view/View;

    if-eqz v2, :cond_22

    goto :goto_12

    :cond_22
    move-object/from16 v2, v16

    :goto_12
    if-eqz v2, :cond_32

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_32

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    instance-of v4, v3, Ld/a$a;

    if-eqz v4, :cond_23

    check-cast v3, Ld/a$a;

    goto :goto_13

    :cond_23
    move-object/from16 v3, v16

    :goto_13
    if-eqz v3, :cond_24

    iget v4, v3, Ld/a$a;->a:I

    goto :goto_14

    :cond_24
    const v4, 0x800013

    :goto_14
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    if-eqz v3, :cond_25

    invoke-virtual {v3}, Landroid/view/ViewGroup$MarginLayoutParams;->getMarginStart()I

    move-result v11

    add-int/2addr v0, v11

    invoke-virtual {v3}, Landroid/view/ViewGroup$MarginLayoutParams;->getMarginEnd()I

    move-result v11

    sub-int/2addr v1, v11

    iget v11, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v3, v3, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto :goto_15

    :cond_25
    const/4 v3, 0x0

    const/4 v11, 0x0

    :goto_15
    const v14, 0x800007

    and-int/2addr v14, v4

    const/4 v15, 0x1

    if-ne v14, v15, :cond_27

    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v15

    sub-int/2addr v15, v5

    div-int/lit8 v15, v15, 0x2

    if-ge v15, v0, :cond_26

    goto :goto_16

    :cond_26
    add-int/2addr v15, v5

    if-le v15, v1, :cond_28

    const v14, 0x800005

    goto :goto_17

    :cond_27
    const/4 v15, -0x1

    if-ne v4, v15, :cond_28

    :goto_16
    const v14, 0x800003

    :cond_28
    :goto_17
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getPaddingStart()I

    move-result v15

    invoke-virtual {v6, v14, v12}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->w(IZ)I

    move-result v14

    move/from16 v17, v0

    const/4 v0, 0x1

    if-eq v14, v0, :cond_2b

    const v0, 0x800003

    if-eq v14, v0, :cond_2a

    const v0, 0x800005

    if-eq v14, v0, :cond_29

    move v0, v15

    goto :goto_18

    :cond_29
    sub-int v0, v1, v5

    goto :goto_18

    :cond_2a
    move/from16 v0, v17

    goto :goto_18

    :cond_2b
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    sub-int/2addr v0, v5

    div-int/lit8 v0, v0, 0x2

    :goto_18
    and-int/lit8 v1, v4, 0x70

    const/4 v5, -0x1

    if-ne v4, v5, :cond_2c

    const/16 v1, 0x10

    :cond_2c
    const/16 v4, 0x10

    if-eq v1, v4, :cond_2f

    const/16 v4, 0x30

    if-eq v1, v4, :cond_2e

    const/16 v4, 0x50

    if-eq v1, v4, :cond_2d

    const/4 v1, 0x0

    goto :goto_19

    :cond_2d
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v1, v4

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v1, v4

    sub-int/2addr v1, v3

    goto :goto_19

    :cond_2e
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v1

    add-int/2addr v1, v11

    goto :goto_19

    :cond_2f
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    sub-int/2addr v3, v1

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v3, v1

    div-int/lit8 v1, v3, 0x2

    :goto_19
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    if-eqz v12, :cond_30

    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v4

    sub-int/2addr v4, v0

    sub-int/2addr v4, v3

    goto :goto_1a

    :cond_30
    move v4, v0

    :goto_1a
    if-eqz v12, :cond_31

    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v3

    sub-int/2addr v3, v0

    goto :goto_1b

    :cond_31
    add-int/2addr v3, v0

    :goto_1b
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v1

    invoke-virtual {v2, v4, v1, v3, v0}, Landroid/view/View;->layout(IIII)V

    :cond_32
    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    if-eqz v0, :cond_37

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-eqz v0, :cond_37

    iget v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->H0:I

    add-int v11, v7, v0

    sub-int v12, v8, v0

    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getMeasuredWidth()I

    move-result v14

    iget-object v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    add-int v5, v9, v10

    move-object/from16 v0, p0

    move v2, v11

    move v3, v9

    move v4, v12

    invoke-static/range {v0 .. v5}, Landroidx/emoji2/text/l;->J(Landroid/view/ViewGroup;Landroid/view/View;IIII)V

    sub-int/2addr v12, v11

    sub-int/2addr v12, v14

    div-int/lit8 v12, v12, 0x2

    const/4 v0, 0x0

    invoke-static {v0, v12}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getLayoutDirection()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_33

    move v1, v2

    goto :goto_1c

    :cond_33
    const/4 v1, 0x0

    :goto_1c
    if-eqz v1, :cond_34

    neg-float v0, v0

    :cond_34
    iget-object v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {v1, v0}, Landroid/widget/HorizontalScrollView;->setTranslationX(F)V

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getLayoutDirection()I

    move-result v0

    if-ne v0, v2, :cond_35

    const/4 v0, 0x1

    goto :goto_1d

    :cond_35
    const/4 v0, 0x0

    :goto_1d
    if-eqz v0, :cond_36

    iget v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->H0:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v8, v0

    sub-int v1, v0, v14

    move v14, v0

    goto :goto_1e

    :cond_36
    const/4 v1, 0x0

    :goto_1e
    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getMeasuredHeight()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v14, v2}, Landroid/widget/HorizontalScrollView;->layout(IIII)V

    :cond_37
    :goto_1f
    sub-int v0, v13, v18

    const/high16 v1, 0x40400000    # 3.0f

    mul-float v14, p3, v1

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-static {v9, v14}, Ljava/lang/Math;->min(FF)F

    move-result v1

    sub-float v1, v9, v1

    const/4 v10, 0x0

    cmpg-float v1, v1, v10

    if-gtz v1, :cond_38

    iget v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->y:I

    goto :goto_20

    :cond_38
    const/4 v1, 0x0

    :goto_20
    iget-object v2, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    if-eqz v2, :cond_39

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v2

    if-nez v2, :cond_39

    iget-object v2, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v2

    goto :goto_21

    :cond_39
    const/4 v2, 0x0

    :goto_21
    move v11, v2

    iget-object v2, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    if-eqz v2, :cond_3a

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_3a

    iget-object v2, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v2

    if-nez v2, :cond_3a

    iget-object v2, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v2

    goto :goto_22

    :cond_3a
    const/4 v2, 0x0

    :goto_22
    move v12, v2

    add-int/2addr v0, v11

    add-int/2addr v0, v12

    sub-int/2addr v0, v13

    add-int v15, v0, v1

    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_3f

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3f

    iget v0, v6, Lmiuix/appcompat/internal/app/widget/a;->h:I

    if-eqz v0, :cond_3f

    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    sub-int v1, v13, v11

    invoke-virtual {v0, v7, v1, v8, v13}, Landroid/widget/FrameLayout;->layout(IIII)V

    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3b

    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v0, :cond_3b

    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    goto :goto_23

    :cond_3b
    move-object/from16 v0, v16

    :goto_23
    if-eqz v0, :cond_3e

    iget v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->H0:I

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getLayoutDirection()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3c

    const/4 v2, 0x1

    goto :goto_24

    :cond_3c
    const/4 v2, 0x0

    :goto_24
    if-eqz v2, :cond_3d

    iget v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->H0:I

    sub-int v1, v8, v1

    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    :cond_3d
    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v1

    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getMeasuredHeight()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/widget/HorizontalScrollView;->layout(IIII)V

    :cond_3e
    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    add-int v1, v11, v12

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v2, v7, v15, v8, v1}, Landroid/graphics/Rect;->set(IIII)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setClipBounds(Landroid/graphics/Rect;)V

    :cond_3f
    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    if-eqz v0, :cond_44

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-eqz v0, :cond_44

    iget v0, v6, Lmiuix/appcompat/internal/app/widget/a;->h:I

    if-eqz v0, :cond_44

    iget-object v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    iget v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->H0:I

    add-int v5, v13, p1

    add-int v2, v0, v7

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v0

    sub-int v3, v5, v0

    iget v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->H0:I

    sub-int v4, v8, v0

    move-object/from16 v0, p0

    invoke-static/range {v0 .. v5}, Landroidx/emoji2/text/l;->J(Landroid/view/ViewGroup;Landroid/view/View;IIII)V

    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_40

    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v0, :cond_40

    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    move-object/from16 v16, v0

    check-cast v16, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    :cond_40
    move-object/from16 v0, v16

    if-eqz v0, :cond_43

    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getLayoutDirection()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_41

    const/4 v2, 0x1

    goto :goto_25

    :cond_41
    const/4 v2, 0x0

    :goto_25
    if-eqz v2, :cond_42

    iget v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->H0:I

    mul-int/lit8 v1, v1, 0x2

    sub-int v1, v8, v1

    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->H0:I

    mul-int/lit8 v2, v2, 0x2

    sub-int v2, v8, v2

    goto :goto_26

    :cond_42
    const/4 v2, 0x0

    move/from16 v20, v2

    move v2, v1

    move/from16 v1, v20

    :goto_26
    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getMeasuredHeight()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/widget/HorizontalScrollView;->layout(IIII)V

    :cond_43
    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    sub-int v1, v11, v12

    sub-int/2addr v15, v1

    add-int/2addr v11, v12

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v1, v7, v15, v8, v11}, Landroid/graphics/Rect;->set(IIII)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setClipBounds(Landroid/graphics/Rect;)V

    :cond_44
    invoke-static {v9, v14}, Ljava/lang/Math;->min(FF)F

    move-result v0

    sub-float v0, v9, v0

    iget v1, v6, Lmiuix/appcompat/internal/app/widget/a;->h:I

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/16 v4, 0x14

    const/4 v5, 0x2

    if-ne v1, v5, :cond_4a

    cmpl-float v1, v0, v10

    const/4 v5, 0x3

    const-wide/16 v7, 0x1

    const-string v11, "target"

    if-lez v1, :cond_45

    iget-boolean v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->e0:Z

    if-nez v1, :cond_47

    const/4 v1, 0x1

    iput-boolean v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->e0:Z

    iput-boolean v2, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->d0:Z

    iget-object v12, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t:Lmiuix/appcompat/internal/app/widget/a$b;

    iget-object v13, v6, Lmiuix/appcompat/internal/app/widget/a;->c:Lv0/a;

    invoke-virtual {v12, v10, v2, v4, v13}, Lmiuix/appcompat/internal/app/widget/a$b;->a(FIILv0/a;)V

    iget-object v12, v6, Lmiuix/appcompat/internal/app/widget/a;->q:Ld1/c;

    if-eqz v12, :cond_47

    const/4 v12, 0x2

    new-array v13, v12, [Ljava/lang/Object;

    aput-object v11, v13, v2

    aput-object v3, v13, v1

    invoke-static {v13}, Lu0/a;->g([Ljava/lang/Object;)Lw0/h;

    move-result-object v3

    invoke-interface {v3, v7, v8}, Lw0/h;->j(J)Lw0/h;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v3, v7}, Lw0/h;->b(Ljava/lang/Object;)Lw0/h;

    move-result-object v3

    new-array v7, v12, [Ljava/lang/Object;

    const-string v8, "expand"

    aput-object v8, v7, v2

    iget v11, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->G0:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v7, v1

    invoke-interface {v3, v7}, Lw0/h;->c([Ljava/lang/Object;)Lw0/h;

    move-result-object v3

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v8, v5, v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    iget-object v1, v6, Lmiuix/appcompat/internal/app/widget/a;->g:Lv0/a;

    const/4 v2, 0x2

    aput-object v1, v5, v2

    invoke-interface {v3, v5}, Lw0/h;->h([Ljava/lang/Object;)Lw0/h;

    goto :goto_28

    :cond_45
    const/4 v1, 0x1

    iget-boolean v2, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->d0:Z

    if-nez v2, :cond_47

    iput-boolean v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->d0:Z

    const/4 v1, 0x0

    iput-boolean v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->e0:Z

    iget-object v2, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t:Lmiuix/appcompat/internal/app/widget/a$b;

    iget-boolean v4, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->V:Z

    if-eqz v4, :cond_46

    move v4, v10

    goto :goto_27

    :cond_46
    move v4, v9

    :goto_27
    iget-object v12, v6, Lmiuix/appcompat/internal/app/widget/a;->d:Lv0/a;

    invoke-virtual {v2, v4, v1, v1, v12}, Lmiuix/appcompat/internal/app/widget/a$b;->a(FIILv0/a;)V

    iget-object v2, v6, Lmiuix/appcompat/internal/app/widget/a;->q:Ld1/c;

    if-eqz v2, :cond_47

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/Object;

    aput-object v11, v4, v1

    const/4 v11, 0x1

    aput-object v3, v4, v11

    invoke-static {v4}, Lu0/a;->g([Ljava/lang/Object;)Lw0/h;

    move-result-object v4

    invoke-interface {v4, v7, v8}, Lw0/h;->j(J)Lw0/h;

    move-result-object v4

    invoke-interface {v4, v3}, Lw0/h;->b(Ljava/lang/Object;)Lw0/h;

    move-result-object v4

    new-array v7, v2, [Ljava/lang/Object;

    const-string v8, "collapse"

    aput-object v8, v7, v1

    iget v12, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->G0:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v7, v11

    invoke-interface {v4, v7}, Lw0/h;->c([Ljava/lang/Object;)Lw0/h;

    move-result-object v4

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v8, v5, v1

    aput-object v3, v5, v11

    iget-object v1, v6, Lmiuix/appcompat/internal/app/widget/a;->m:Lv0/a;

    aput-object v1, v5, v2

    invoke-interface {v4, v5}, Lw0/h;->h([Ljava/lang/Object;)Lw0/h;

    :cond_47
    :goto_28
    iget-object v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->j0:Lmiuix/appcompat/internal/app/widget/a$b;

    iget-boolean v2, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->V:Z

    if-eqz v2, :cond_48

    goto :goto_29

    :cond_48
    move v10, v0

    :goto_29
    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/a;->k:Lv0/a;

    const/4 v2, 0x0

    invoke-virtual {v1, v10, v2, v2, v0}, Lmiuix/appcompat/internal/app/widget/a$b;->a(FIILv0/a;)V

    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_4e

    cmpg-float v1, p3, v9

    if-gez v1, :cond_49

    const/4 v1, 0x0

    goto :goto_2a

    :cond_49
    const/4 v1, 0x4

    :goto_2a
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_2b

    :cond_4a
    iget v0, v6, Lmiuix/appcompat/internal/app/widget/a;->h:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4c

    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t:Lmiuix/appcompat/internal/app/widget/a$b;

    iget-object v1, v6, Lmiuix/appcompat/internal/app/widget/a;->c:Lv0/a;

    const/4 v2, 0x0

    invoke-virtual {v0, v10, v2, v4, v1}, Lmiuix/appcompat/internal/app/widget/a$b;->a(FIILv0/a;)V

    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->j0:Lmiuix/appcompat/internal/app/widget/a$b;

    iget-boolean v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->V:Z

    if-eqz v1, :cond_4b

    move v9, v10

    :cond_4b
    iget-object v1, v6, Lmiuix/appcompat/internal/app/widget/a;->k:Lv0/a;

    invoke-virtual {v0, v9, v2, v2, v1}, Lmiuix/appcompat/internal/app/widget/a$b;->a(FIILv0/a;)V

    iput v4, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->G0:I

    goto :goto_2b

    :cond_4c
    const/4 v1, 0x0

    if-nez v0, :cond_4e

    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t:Lmiuix/appcompat/internal/app/widget/a$b;

    iget-boolean v2, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->V:Z

    if-eqz v2, :cond_4d

    move v9, v10

    :cond_4d
    iget-object v2, v6, Lmiuix/appcompat/internal/app/widget/a;->d:Lv0/a;

    invoke-virtual {v0, v9, v1, v1, v2}, Lmiuix/appcompat/internal/app/widget/a$b;->a(FIILv0/a;)V

    iget-object v0, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->j0:Lmiuix/appcompat/internal/app/widget/a$b;

    iget-object v2, v6, Lmiuix/appcompat/internal/app/widget/a;->k:Lv0/a;

    invoke-virtual {v0, v10, v1, v1, v2}, Lmiuix/appcompat/internal/app/widget/a$b;->a(FIILv0/a;)V

    iput v1, v6, Lmiuix/appcompat/internal/app/widget/ActionBarView;->G0:I

    :cond_4e
    :goto_2b
    move/from16 v1, p3

    iput v1, v6, Lmiuix/appcompat/internal/app/widget/a;->i:F

    return-void
.end method

.method public onMeasure(II)V
    .locals 20

    move-object/from16 v0, p0

    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    move v3, v2

    move v4, v3

    :goto_0
    const/16 v5, 0x8

    if-ge v3, v1, :cond_2

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-eq v7, v5, :cond_1

    iget-object v5, v0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    if-ne v6, v5, :cond_0

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    add-int/lit8 v4, v4, 0x1

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x1

    if-nez v4, :cond_3

    invoke-virtual {v0, v2, v2}, Landroid/view/ViewGroup;->setMeasuredDimension(II)V

    iput-boolean v3, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->c0:Z

    return-void

    :cond_3
    iput-boolean v2, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->c0:Z

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    iget v6, v0, Lmiuix/appcompat/internal/app/widget/a;->e:I

    if-lez v6, :cond_4

    goto :goto_1

    :cond_4
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    :goto_1
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v7

    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v8

    add-int/2addr v8, v7

    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getPaddingStart()I

    move-result v7

    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getPaddingEnd()I

    move-result v9

    sub-int v10, v6, v8

    const/high16 v11, 0x40000000    # 2.0f

    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    const/high16 v13, -0x80000000

    invoke-static {v10, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v14

    sub-int v15, v4, v7

    sub-int/2addr v15, v9

    div-int/lit8 v16, v15, 0x2

    iget-object v3, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->w0:Landroid/view/View;

    if-eqz v3, :cond_5

    invoke-virtual {v0, v3, v15, v14, v2}, Lmiuix/appcompat/internal/app/widget/a;->f(Landroid/view/View;III)I

    move-result v15

    iget-object v3, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->w0:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v7, v3

    :cond_5
    iget-object v3, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->H:Landroid/view/View;

    if-eqz v3, :cond_6

    invoke-virtual {v0, v3, v15, v14, v2}, Lmiuix/appcompat/internal/app/widget/a;->f(Landroid/view/View;III)I

    move-result v15

    iget-object v3, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->H:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v9, v3

    :cond_6
    iget-object v3, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->K:Landroid/view/View;

    if-eqz v3, :cond_7

    iget-object v3, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->M:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    goto :goto_2

    :cond_7
    iget-object v3, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    :goto_2
    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->w0:Landroid/view/View;

    if-eqz v2, :cond_8

    if-eqz v3, :cond_8

    invoke-virtual {v3, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    :cond_8
    if-eqz v3, :cond_a

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v2

    if-eq v2, v5, :cond_a

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-gez v2, :cond_9

    invoke-static {v15, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    goto :goto_3

    :cond_9
    invoke-static {v2, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    :goto_3
    invoke-virtual {v3, v2, v12}, Landroid/widget/FrameLayout;->measure(II)V

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v2

    const/4 v3, 0x0

    add-int/2addr v2, v3

    sub-int/2addr v15, v2

    invoke-static {v3, v15}, Ljava/lang/Math;->max(II)I

    move-result v15

    sub-int v12, v15, v2

    invoke-static {v3, v12}, Ljava/lang/Math;->max(II)I

    move-result v12

    add-int/2addr v7, v2

    goto :goto_4

    :cond_a
    const/4 v3, 0x0

    move/from16 v12, v16

    :goto_4
    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    if-eqz v2, :cond_b

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, v0, :cond_b

    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    invoke-virtual {v0, v2, v15, v14, v3}, Lmiuix/appcompat/internal/app/widget/a;->f(Landroid/view/View;III)I

    move-result v15

    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v2

    sub-int v2, v16, v2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v16

    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v9, v2

    :cond_b
    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->b0:Landroid/widget/ProgressBar;

    const/4 v3, 0x2

    if-eqz v2, :cond_c

    invoke-virtual {v2}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v2

    if-eq v2, v5, :cond_c

    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->b0:Landroid/widget/ProgressBar;

    iget v13, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s0:I

    mul-int/2addr v13, v3

    invoke-virtual {v0, v2, v15, v14, v13}, Lmiuix/appcompat/internal/app/widget/a;->f(Landroid/view/View;III)I

    move-result v15

    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->b0:Landroid/widget/ProgressBar;

    invoke-virtual {v2}, Landroid/widget/ProgressBar;->getMeasuredWidth()I

    move-result v2

    sub-int v16, v16, v2

    iget v2, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s0:I

    mul-int/2addr v2, v3

    sub-int v2, v16, v2

    const/4 v13, 0x0

    invoke-static {v13, v2}, Ljava/lang/Math;->max(II)I

    move-result v16

    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->b0:Landroid/widget/ProgressBar;

    invoke-virtual {v2}, Landroid/widget/ProgressBar;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v9, v2

    :cond_c
    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->U:Landroid/view/View;

    if-eqz v2, :cond_d

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-eq v2, v5, :cond_d

    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->U:Landroid/view/View;

    const/4 v13, 0x0

    invoke-virtual {v0, v2, v15, v14, v13}, Lmiuix/appcompat/internal/app/widget/a;->f(Landroid/view/View;III)I

    move-result v15

    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->U:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int v2, v16, v2

    invoke-static {v13, v2}, Ljava/lang/Math;->max(II)I

    move-result v16

    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->U:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v9, v2

    :cond_d
    move/from16 v2, v16

    invoke-virtual/range {p0 .. p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t()Z

    move-result v13

    if-eqz v13, :cond_12

    iget-object v14, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    if-eqz v14, :cond_12

    invoke-virtual/range {p0 .. p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->v()Z

    move-result v16

    iget-object v11, v14, Le1/a;->e:Landroid/widget/TextView;

    invoke-virtual {v11}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    instance-of v3, v11, Landroid/widget/LinearLayout;

    const v17, 0x800003

    if-eqz v3, :cond_f

    check-cast v11, Landroid/widget/LinearLayout;

    if-eqz v16, :cond_e

    const/4 v3, 0x1

    goto :goto_5

    :cond_e
    move/from16 v3, v17

    :goto_5
    or-int/lit8 v3, v3, 0x10

    invoke-virtual {v11, v3}, Landroid/widget/LinearLayout;->setGravity(I)V

    :cond_f
    iget-object v3, v14, Le1/a;->e:Landroid/widget/TextView;

    if-eqz v16, :cond_10

    const/4 v11, 0x1

    goto :goto_6

    :cond_10
    move/from16 v11, v17

    :goto_6
    or-int/lit8 v11, v11, 0x10

    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v3, v14, Le1/a;->e:Landroid/widget/TextView;

    sget-object v11, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v3, v14, Le1/a;->b:Landroid/widget/TextView;

    if-eqz v16, :cond_11

    const/16 v17, 0x1

    :cond_11
    or-int/lit8 v11, v17, 0x10

    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v3, v14, Le1/a;->b:Landroid/widget/TextView;

    sget-object v11, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    :cond_12
    iget-object v3, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->K:Landroid/view/View;

    const/4 v11, 0x0

    if-eqz v3, :cond_13

    goto :goto_7

    :cond_13
    iget v3, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->G:I

    and-int/lit8 v3, v3, 0x10

    if-eqz v3, :cond_14

    iget-object v3, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E:Landroid/view/View;

    if-eqz v3, :cond_14

    goto :goto_7

    :cond_14
    move-object v3, v11

    :goto_7
    iget-object v14, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->w0:Landroid/view/View;

    if-nez v14, :cond_15

    iget-object v14, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->H:Landroid/view/View;

    if-eqz v14, :cond_16

    :cond_15
    if-eqz v3, :cond_16

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_16
    if-eqz v3, :cond_20

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v14

    if-eq v14, v5, :cond_20

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    invoke-virtual {v0, v5}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    instance-of v14, v5, Ld/a$a;

    if-eqz v14, :cond_17

    move-object v11, v5

    check-cast v11, Ld/a$a;

    :cond_17
    if-eqz v11, :cond_18

    iget v14, v11, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v16, v6

    iget v6, v11, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v17, v8

    iget v8, v11, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v6, v14

    iget v14, v11, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v8, v14

    goto :goto_8

    :cond_18
    move/from16 v16, v6

    move/from16 v17, v8

    const/4 v6, 0x0

    const/4 v8, 0x0

    :goto_8
    iget v14, v0, Lmiuix/appcompat/internal/app/widget/a;->e:I

    move/from16 v18, v1

    const/4 v1, -0x2

    if-gtz v14, :cond_19

    goto :goto_9

    :cond_19
    iget v14, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v14, v1, :cond_1a

    const/high16 v14, 0x40000000    # 2.0f

    goto :goto_a

    :cond_1a
    :goto_9
    const/high16 v14, -0x80000000

    :goto_a
    iget v1, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ltz v1, :cond_1b

    invoke-static {v1, v10}, Ljava/lang/Math;->min(II)I

    move-result v10

    :cond_1b
    sub-int/2addr v10, v8

    const/4 v1, 0x0

    invoke-static {v1, v10}, Ljava/lang/Math;->max(II)I

    move-result v8

    iget v10, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v1, -0x2

    if-eq v10, v1, :cond_1c

    const/high16 v1, 0x40000000    # 2.0f

    goto :goto_b

    :cond_1c
    const/high16 v1, -0x80000000

    :goto_b
    if-ltz v10, :cond_1d

    invoke-static {v10, v15}, Ljava/lang/Math;->min(II)I

    move-result v10

    goto :goto_c

    :cond_1d
    move v10, v15

    :goto_c
    sub-int/2addr v10, v6

    move/from16 v19, v4

    const/4 v4, 0x0

    invoke-static {v4, v10}, Ljava/lang/Math;->max(II)I

    move-result v10

    if-eqz v11, :cond_1e

    iget v4, v11, Ld/a$a;->a:I

    goto :goto_d

    :cond_1e
    const v4, 0x800013

    :goto_d
    const v11, 0x800007

    and-int/2addr v4, v11

    const/4 v11, 0x1

    if-ne v4, v11, :cond_1f

    iget v4, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_1f

    invoke-static {v12, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    const/4 v4, 0x2

    mul-int/lit8 v10, v2, 0x2

    :cond_1f
    invoke-static {v10, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v8, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v3, v1, v2}, Landroid/view/View;->measure(II)V

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v6

    sub-int/2addr v15, v1

    goto :goto_e

    :cond_20
    move/from16 v18, v1

    move/from16 v19, v4

    move/from16 v16, v6

    move/from16 v17, v8

    :goto_e
    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->K:Landroid/view/View;

    if-nez v1, :cond_23

    if-eqz v13, :cond_23

    iget v1, v0, Lmiuix/appcompat/internal/app/widget/a;->e:I

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E0:Landroid/view/View;

    if-eqz v2, :cond_21

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v15, v1, v3}, Lmiuix/appcompat/internal/app/widget/a;->f(Landroid/view/View;III)I

    move-result v15

    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E0:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v7, v2

    :cond_21
    invoke-virtual/range {p0 .. p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->v()Z

    move-result v2

    if-eqz v2, :cond_22

    invoke-static {v7, v9}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget-object v3, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    const/4 v4, 0x2

    mul-int/2addr v2, v4

    sub-int v4, v19, v2

    const/4 v2, 0x0

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    const/high16 v5, -0x80000000

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v3, v4, v1}, Landroid/widget/FrameLayout;->measure(II)V

    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    goto :goto_f

    :cond_22
    const/4 v2, 0x0

    iget-object v3, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3, v15, v1, v2}, Lmiuix/appcompat/internal/app/widget/a;->f(Landroid/view/View;III)I

    goto :goto_f

    :cond_23
    const/4 v2, 0x0

    :goto_f
    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    move/from16 v3, v19

    const/high16 v4, -0x80000000

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v1, v5, v4}, Landroid/widget/FrameLayout;->measure(II)V

    iput v2, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->l0:I

    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    if-eqz v1, :cond_24

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v1

    if-nez v1, :cond_24

    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-eqz v1, :cond_24

    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v1, v4, v5}, Landroid/view/ViewGroup;->measure(II)V

    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v1

    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v2

    iput v2, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->l0:I

    goto :goto_10

    :cond_24
    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_10
    const/4 v4, 0x0

    iput v4, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->y:I

    iget-object v5, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    if-eqz v5, :cond_25

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    if-eqz v5, :cond_25

    iget-object v5, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    if-eqz v5, :cond_25

    iget-object v5, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/view/ViewGroup;->measure(II)V

    iget-object v4, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    iget-object v4, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v4

    iput v4, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->y:I

    goto :goto_11

    :cond_25
    const/4 v4, 0x0

    :goto_11
    iget-object v5, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->K:Landroid/view/View;

    if-nez v5, :cond_28

    iget v5, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->m0:I

    const/4 v6, 0x1

    if-eq v5, v6, :cond_28

    const/4 v6, 0x2

    if-eq v5, v6, :cond_26

    goto :goto_12

    :cond_26
    iget-object v5, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v5, :cond_27

    invoke-virtual {v5}, Landroid/widget/HorizontalScrollView;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    if-eqz v5, :cond_27

    iget v5, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->H0:I

    iget-object v7, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    mul-int/2addr v5, v6

    sub-int v5, v3, v5

    const/high16 v6, -0x80000000

    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    const/4 v6, 0x0

    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v7, v5, v8}, Landroid/widget/HorizontalScrollView;->measure(II)V

    :cond_27
    iget-object v5, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->u0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v5, :cond_28

    invoke-virtual {v5}, Landroid/widget/HorizontalScrollView;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    if-eqz v5, :cond_28

    iget v5, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->H0:I

    const/4 v6, 0x2

    mul-int/2addr v5, v6

    sub-int v5, v3, v5

    invoke-static {v1, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    const/4 v5, 0x0

    invoke-static {v5, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget-object v6, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->u0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    const/high16 v7, -0x80000000

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v6, v1, v7}, Landroid/widget/HorizontalScrollView;->measure(II)V

    goto :goto_13

    :cond_28
    :goto_12
    const/4 v5, 0x0

    :goto_13
    iget v1, v0, Lmiuix/appcompat/internal/app/widget/a;->e:I

    if-gtz v1, :cond_2b

    move v2, v5

    move/from16 v1, v18

    :goto_14
    if-ge v2, v1, :cond_2a

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int v4, v4, v17

    if-le v4, v5, :cond_29

    move v5, v4

    :cond_29
    add-int/lit8 v2, v2, 0x1

    goto :goto_14

    :cond_2a
    invoke-virtual {v0, v3, v5}, Landroid/view/ViewGroup;->setMeasuredDimension(II)V

    goto :goto_16

    :cond_2b
    add-int v6, v16, v4

    iput v6, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->v:I

    iget v1, v0, Lmiuix/appcompat/internal/app/widget/a;->h:I

    const/4 v4, 0x2

    if-ne v1, v4, :cond_2c

    iget v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->p0:I

    add-int v1, v16, v1

    invoke-static {v1, v6}, Ljava/lang/Math;->max(II)I

    move-result v1

    :goto_15
    invoke-virtual {v0, v3, v1}, Landroid/view/ViewGroup;->setMeasuredDimension(II)V

    goto :goto_16

    :cond_2c
    const/4 v4, 0x1

    if-ne v1, v4, :cond_2d

    iget-object v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v1

    add-int v1, v1, v16

    add-int/2addr v1, v2

    goto :goto_15

    :cond_2d
    invoke-virtual {v0, v3, v6}, Landroid/view/ViewGroup;->setMeasuredDimension(II)V

    :goto_16
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    check-cast p1, Lmiuix/appcompat/internal/app/widget/ActionBarView$j;

    invoke-virtual {p1}, Landroid/view/View$BaseSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget v0, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView$j;->b:I

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->N:Lmiuix/appcompat/internal/app/widget/ActionBarView$h;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->o0:Lh1/g;

    if-eqz v1, :cond_0

    invoke-virtual {v1, v0}, Lh1/g;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/view/MenuItem;->expandActionView()Z

    :cond_0
    iget-boolean v0, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView$j;->c:Z

    if-eqz v0, :cond_1

    new-instance v0, Lmiuix/appcompat/internal/app/widget/b;

    invoke-direct {v0, p0}, Lmiuix/appcompat/internal/app/widget/b;-><init>(Lmiuix/appcompat/internal/app/widget/a;)V

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    :cond_1
    iget p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView$j;->a:I

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setExpandState(I)V

    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    new-instance v0, Lmiuix/appcompat/internal/app/widget/ActionBarView$j;

    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lmiuix/appcompat/internal/app/widget/ActionBarView$j;-><init>(Landroid/os/Parcelable;)V

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->N:Lmiuix/appcompat/internal/app/widget/ActionBarView$h;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    iget-object v1, v1, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->a:Lh1/i;

    if-eqz v1, :cond_0

    iget v1, v1, Lh1/i;->h:I

    iput v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView$j;->b:I

    goto :goto_0

    :cond_0
    iput v2, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView$j;->b:I

    :goto_0
    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Li1/b;->m()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    move v1, v2

    :goto_1
    iput-boolean v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView$j;->c:Z

    iget v1, p0, Lmiuix/appcompat/internal/app/widget/a;->h:I

    const/4 v3, 0x2

    if-ne v1, v3, :cond_2

    iput v2, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView$j;->a:I

    goto :goto_2

    :cond_2
    iput v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView$j;->a:I

    :goto_2
    return-object v0
.end method

.method public final p()V
    .locals 5

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E:Landroid/view/View;

    const v1, 0x7f0a002c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    const v1, 0x1020016

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_3

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v4}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v2, v2, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->removeAllViews()V

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v2, :cond_1

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {v2, v3}, Lmiuix/springback/view/SpringBackLayout;->setTarget(Landroid/view/View;)V

    :cond_1
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->u0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v2, :cond_2

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->u0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {v2, v3}, Lmiuix/springback/view/SpringBackLayout;->setTarget(Landroid/view/View;)V

    :cond_2
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->removeAllViews()V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->u:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t:Lmiuix/appcompat/internal/app/widget/a$b;

    invoke-virtual {v2, v0}, Lmiuix/appcompat/internal/app/widget/a$b;->b(Landroid/view/View;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->J:Lx0/j;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lx0/j;->c(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->J:Lx0/j;

    iget-object v0, v0, Lx0/j;->d:Ljava/lang/Object;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->J:Lx0/j;

    iget-object v0, v0, Lx0/j;->c:Ljava/lang/Object;

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->J:Lx0/j;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lx0/j;->b(I)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->J:Lx0/j;

    iget-object v2, v2, Lx0/j;->c:Ljava/lang/Object;

    check-cast v2, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->F:Landroid/text/TextWatcher;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_3
    return-void
.end method

.method public final q(Lh1/g;)V
    .locals 3

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    invoke-virtual {p1, v0}, Lh1/g;->b(Lh1/k;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->N:Lmiuix/appcompat/internal/app/widget/ActionBarView$h;

    invoke-virtual {p1, v0}, Lh1/g;->b(Lh1/k;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->D:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Li1/b;->e(Landroid/content/Context;Lh1/g;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->N:Lmiuix/appcompat/internal/app/widget/ActionBarView$h;

    iget-object v0, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->b:Lh1/g;

    if-eqz v0, :cond_1

    iget-object v2, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->a:Lh1/i;

    if-eqz v2, :cond_1

    invoke-virtual {v0, v2}, Lh1/g;->e(Lh1/i;)Z

    :cond_1
    iput-object v1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->b:Lh1/g;

    :goto_0
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Li1/b;->c(Z)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->N:Lmiuix/appcompat/internal/app/widget/ActionBarView$h;

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView$h;->c(Z)V

    return-void
.end method

.method public r(ILd1/b;)V
    .locals 3

    const-string v0, "ActionBarView"

    if-gtz p1, :cond_0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Try to initialize invalid layout for immersion more button: "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->G:I

    and-int/lit8 v2, v1, 0x10

    if-eqz v2, :cond_1

    const-string p1, "Don\'t show immersion menu button for custom action bar"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    if-nez v1, :cond_2

    const-string p1, "Don\'t show immersion menu button for null display option"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_2
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->U:Landroid/view/View;

    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->U:Landroid/view/View;

    const v0, 0x7f0a00aa

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_3

    new-instance v0, Lmiuix/appcompat/internal/app/widget/ActionBarView$f;

    invoke-direct {v0, p0, p2, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView$f;-><init>(Lmiuix/appcompat/internal/app/widget/ActionBarView;Ld1/b;Landroid/view/View;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    return-void
.end method

.method public final s()V
    .locals 14

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E0:Landroid/view/View;

    const/4 v1, 0x0

    const/16 v2, 0x8

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v3, Landroidx/appcompat/widget/AppCompatImageView;

    invoke-direct {v3, v0, v1}, Landroidx/appcompat/widget/AppCompatImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const v4, 0x7f0a0115

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setId(I)V

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    new-instance v4, Lf1/a;

    invoke-direct {v4, v3, v0}, Lf1/a;-><init>(Landroidx/appcompat/widget/AppCompatImageView;Landroid/content/Context;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    iput-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E0:Landroid/view/View;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->I0:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E0:Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    const/4 v3, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-nez v0, :cond_10

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v6, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->C:I

    iget v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->w:I

    new-instance v8, Le1/a;

    invoke-direct {v8, v0, v6, v7}, Le1/a;-><init>(Landroid/content/Context;II)V

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget v6, v6, Landroid/content/res/Configuration;->orientation:I

    if-ne v6, v3, :cond_1

    move v6, v5

    goto :goto_0

    :cond_1
    move v6, v4

    :goto_0
    invoke-static {}, Lr1/b;->a()Z

    move-result v7

    if-nez v7, :cond_2

    if-eqz v6, :cond_2

    move v6, v5

    goto :goto_1

    :cond_2
    move v6, v4

    :goto_1
    const v7, 0x7f0701ba

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    int-to-float v7, v7

    iput v7, v8, Le1/a;->g:F

    new-instance v7, Landroid/widget/LinearLayout;

    iget-object v9, v8, Le1/a;->f:Landroid/content/Context;

    invoke-direct {v7, v9}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v7, v8, Le1/a;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v3}, Landroid/widget/LinearLayout;->setImportantForAccessibility(I)V

    new-instance v7, Landroid/widget/TextView;

    iget-object v9, v8, Le1/a;->f:Landroid/content/Context;

    const v10, 0x7f0400be

    invoke-direct {v7, v9, v1, v10}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v7, v8, Le1/a;->e:Landroid/widget/TextView;

    new-instance v7, Landroid/widget/TextView;

    iget-object v9, v8, Le1/a;->f:Landroid/content/Context;

    const v10, 0x7f0400bd

    invoke-direct {v7, v9, v1, v10}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v7, v8, Le1/a;->b:Landroid/widget/TextView;

    iget-object v7, v8, Le1/a;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    iget-object v7, v8, Le1/a;->c:Landroid/widget/LinearLayout;

    xor-int/lit8 v9, v6, 0x1

    invoke-virtual {v7, v9}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v7, v8, Le1/a;->c:Landroid/widget/LinearLayout;

    new-instance v9, Le1/b;

    invoke-direct {v9, v8, v3}, Le1/b;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v7, v9}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    iget-object v7, v8, Le1/a;->e:Landroid/widget/TextView;

    const v9, 0x7f0a0033

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setId(I)V

    iget-object v7, v8, Le1/a;->c:Landroid/widget/LinearLayout;

    iget-object v10, v8, Le1/a;->e:Landroid/widget/TextView;

    new-instance v11, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v12, -0x2

    invoke-direct {v11, v12, v12}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v7, v10, v11}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v7, v8, Le1/a;->b:Landroid/widget/TextView;

    const v10, 0x7f0a0032

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setId(I)V

    iget-object v7, v8, Le1/a;->b:Landroid/widget/TextView;

    invoke-virtual {v7, v2}, Landroid/widget/TextView;->setVisibility(I)V

    if-eqz v6, :cond_3

    iget-object v7, v8, Le1/a;->b:Landroid/widget/TextView;

    new-instance v11, Le1/b;

    invoke-direct {v11, v8, v5}, Le1/b;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    :cond_3
    iget-object v7, v8, Le1/a;->c:Landroid/widget/LinearLayout;

    iget-object v11, v8, Le1/a;->b:Landroid/widget/TextView;

    new-instance v13, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v13, v12, v12}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v7, v11, v13}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v7, v8, Le1/a;->b:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout$LayoutParams;

    const v11, 0x7f070051

    const v12, 0x7f070052

    if-eqz v6, :cond_4

    const v6, 0x7f070088

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    goto :goto_2

    :cond_4
    invoke-virtual {v0, v12}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    iput v6, v7, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    invoke-virtual {v0, v11}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, v7, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    :goto_2
    iput-object v8, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v6, Lx0/j;

    invoke-direct {v6, v0}, Lx0/j;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v7, v6, Lx0/j;->a:Ljava/lang/Object;

    check-cast v7, Landroid/content/Context;

    invoke-direct {v0, v7}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, v6, Lx0/j;->c:Ljava/lang/Object;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setImportantForAccessibility(I)V

    iget-object v0, v6, Lx0/j;->c:Ljava/lang/Object;

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    iget-object v0, v6, Lx0/j;->c:Ljava/lang/Object;

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v0, v6, Lx0/j;->c:Ljava/lang/Object;

    check-cast v0, Landroid/widget/LinearLayout;

    new-instance v7, Le1/b;

    const/4 v8, 0x4

    invoke-direct {v7, v6, v8}, Le1/b;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    new-instance v0, Landroid/widget/TextView;

    iget-object v7, v6, Lx0/j;->a:Ljava/lang/Object;

    check-cast v7, Landroid/content/Context;

    const v13, 0x7f040115

    invoke-direct {v0, v7, v1, v13}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, v6, Lx0/j;->d:Ljava/lang/Object;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setId(I)V

    iget-object v0, v6, Lx0/j;->c:Ljava/lang/Object;

    check-cast v0, Landroid/widget/LinearLayout;

    iget-object v7, v6, Lx0/j;->d:Ljava/lang/Object;

    check-cast v7, Landroid/widget/TextView;

    invoke-virtual {v6}, Lx0/j;->a()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v9

    invoke-virtual {v0, v7, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/TextView;

    iget-object v7, v6, Lx0/j;->a:Ljava/lang/Object;

    check-cast v7, Landroid/content/Context;

    const v9, 0x7f040113

    invoke-direct {v0, v7, v1, v9}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, v6, Lx0/j;->b:Ljava/lang/Object;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setId(I)V

    iget-object v0, v6, Lx0/j;->b:Ljava/lang/Object;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, v6, Lx0/j;->c:Ljava/lang/Object;

    check-cast v0, Landroid/widget/LinearLayout;

    iget-object v1, v6, Lx0/j;->b:Ljava/lang/Object;

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v6}, Lx0/j;->a()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v7

    invoke-virtual {v0, v1, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, v6, Lx0/j;->a:Ljava/lang/Object;

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, v6, Lx0/j;->b:Ljava/lang/Object;

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v12}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v7

    iput v7, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    invoke-virtual {v0, v11}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    iput-object v6, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->J:Lx0/j;

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->G:I

    and-int/lit8 v1, v0, 0x4

    if-eqz v1, :cond_5

    move v1, v5

    goto :goto_3

    :cond_5
    move v1, v4

    :goto_3
    and-int/2addr v0, v3

    if-eqz v0, :cond_6

    move v0, v5

    goto :goto_4

    :cond_6
    move v0, v4

    :goto_4
    iget-object v6, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E0:Landroid/view/View;

    if-nez v0, :cond_8

    if-eqz v1, :cond_7

    move v2, v4

    goto :goto_5

    :cond_7
    move v2, v8

    :cond_8
    :goto_5
    invoke-virtual {v6, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E0:Landroid/view/View;

    if-eqz v1, :cond_9

    if-nez v0, :cond_9

    move v6, v5

    goto :goto_6

    :cond_9
    move v6, v4

    :goto_6
    invoke-virtual {v2, v6}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    if-eqz v1, :cond_a

    if-nez v0, :cond_a

    move v6, v5

    goto :goto_7

    :cond_a
    move v6, v4

    :goto_7
    iget-object v2, v2, Le1/a;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->J:Lx0/j;

    if-eqz v1, :cond_b

    if-nez v0, :cond_b

    move v0, v5

    goto :goto_8

    :cond_b
    move v0, v4

    :goto_8
    iget-object v1, v2, Lx0/j;->c:Ljava/lang/Object;

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A0:Ljava/lang/CharSequence;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v1, :cond_c

    iget-object v0, v0, Le1/a;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_c
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->y0:Ljava/lang/CharSequence;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v1, :cond_d

    iget-object v0, v0, Le1/a;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_d
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->J:Lx0/j;

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A0:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lx0/j;->c(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->J:Lx0/j;

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->y0:Ljava/lang/CharSequence;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v1, :cond_e

    iget-object v0, v0, Lx0/j;->b:Ljava/lang/Object;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_e
    new-instance v0, Lmiuix/appcompat/internal/app/widget/g;

    invoke-direct {v0, p0, v8}, Lmiuix/appcompat/internal/app/widget/g;-><init>(Lmiuix/appcompat/internal/app/widget/ActionBarView;I)V

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->y0:Ljava/lang/CharSequence;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    iget-object v0, v0, Le1/a;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->J:Lx0/j;

    invoke-virtual {v0, v4}, Lx0/j;->b(I)V

    :cond_f
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->z()V

    :cond_10
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    iget-object v0, v0, Le1/a;->c:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->J:Lx0/j;

    iget-object v1, v1, Lx0/j;->c:Ljava/lang/Object;

    check-cast v1, Landroid/widget/LinearLayout;

    iget v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->m0:I

    if-ne v2, v3, :cond_13

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v2

    if-ne v2, v5, :cond_13

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v4}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v2, v2, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v2, :cond_13

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v2, :cond_11

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {p0, v3, v2}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x(Landroid/view/ViewGroup;Landroid/view/View;)V

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {v2, v3}, Lmiuix/springback/view/SpringBackLayout;->setTarget(Landroid/view/View;)V

    :cond_11
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->u0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v2, :cond_12

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {p0, v3, v2}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x(Landroid/view/ViewGroup;Landroid/view/View;)V

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->u0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {v2, v3}, Lmiuix/springback/view/SpringBackLayout;->setTarget(Landroid/view/View;)V

    :cond_12
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->removeAllViews()V

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->removeAllViews()V

    :cond_13
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v2, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x(Landroid/view/ViewGroup;Landroid/view/View;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0, v1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x(Landroid/view/ViewGroup;Landroid/view/View;)V

    new-instance v0, Lmiuix/appcompat/internal/app/widget/g;

    invoke-direct {v0, p0, v4}, Lmiuix/appcompat/internal/app/widget/g;-><init>(Lmiuix/appcompat/internal/app/widget/ActionBarView;I)V

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->K:Landroid/view/View;

    if-nez v0, :cond_14

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A0:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_15

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->y0:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_15

    :cond_14
    invoke-direct {p0, v4}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setTitleVisibility(Z)V

    :cond_15
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {p0, p0, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x(Landroid/view/ViewGroup;Landroid/view/View;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    invoke-virtual {p0, p0, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x(Landroid/view/ViewGroup;Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic setActionBarTransitionListener(Ld1/c;)V
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/app/widget/a;->setActionBarTransitionListener(Ld1/c;)V

    return-void
.end method

.method public setCallback(Ld/a$c;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->r:Ld/a$c;

    return-void
.end method

.method public setCollapsable(Z)V
    .locals 0

    return-void
.end method

.method public bridge synthetic setContentHeight(I)V
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/app/widget/a;->setContentHeight(I)V

    return-void
.end method

.method public setCustomNavigationView(Landroid/view/View;)V
    .locals 2

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->G:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E:Landroid/view/View;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_1
    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E:Landroid/view/View;

    if-eqz p1, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->p()V

    goto :goto_1

    :cond_2
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t:Lmiuix/appcompat/internal/app/widget/a$b;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/a$b;->b(Landroid/view/View;)V

    :goto_1
    return-void
.end method

.method public setDisplayOptions(I)V
    .locals 10

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->G:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    xor-int v1, p1, v0

    :goto_0
    iput p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->G:I

    and-int/lit8 v0, v1, 0x1f

    const/4 v2, 0x0

    const/4 v3, 0x4

    if-eqz v0, :cond_1e

    and-int/lit8 v0, p1, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eqz v0, :cond_1

    move v0, v5

    goto :goto_1

    :cond_1
    move v0, v4

    :goto_1
    const/16 v6, 0x8

    if-eqz v0, :cond_b

    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    if-nez v7, :cond_4

    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->D:Landroid/content/Context;

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    iget v8, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->R:I

    invoke-virtual {v7, v8, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    iput-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    iget-object v8, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->I0:Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v8}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    invoke-virtual {v7, v5}, Landroid/widget/FrameLayout;->setClickable(Z)V

    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    invoke-virtual {v7, v5}, Landroid/widget/FrameLayout;->setFocusable(Z)V

    iget v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->P:I

    if-eqz v7, :cond_2

    iget-object v8, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    invoke-virtual {v8, v7}, Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;->a(I)V

    iput v4, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->P:I

    :cond_2
    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->O:Landroid/graphics/drawable/Drawable;

    if-eqz v7, :cond_3

    iget-object v8, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    iget-object v9, v8, Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;->d:Landroid/widget/ImageView;

    invoke-virtual {v9, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iput v4, v8, Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;->c:I

    iput-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->O:Landroid/graphics/drawable/Drawable;

    :cond_3
    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    invoke-virtual {p0, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_4
    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    iget-object v8, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->K:Landroid/view/View;

    if-nez v8, :cond_5

    move v8, v4

    goto :goto_2

    :cond_5
    move v8, v6

    :goto_2
    invoke-virtual {v7, v8}, Landroid/widget/FrameLayout;->setVisibility(I)V

    and-int/lit8 v7, v1, 0x4

    if-eqz v7, :cond_8

    and-int/lit8 v7, p1, 0x4

    if-eqz v7, :cond_6

    move v7, v5

    goto :goto_3

    :cond_6
    move v7, v4

    :goto_3
    iget-object v8, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    iget-object v8, v8, Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;->d:Landroid/widget/ImageView;

    if-eqz v7, :cond_7

    move v9, v4

    goto :goto_4

    :cond_7
    move v9, v6

    :goto_4
    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    if-eqz v7, :cond_8

    invoke-virtual {p0, v5}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setHomeButtonEnabled(Z)V

    :cond_8
    and-int/lit8 v7, v1, 0x1

    if-eqz v7, :cond_c

    invoke-direct {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getLogo()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    if-eqz v7, :cond_9

    and-int/lit8 v8, p1, 0x1

    if-eqz v8, :cond_9

    move v8, v5

    goto :goto_5

    :cond_9
    move v8, v4

    :goto_5
    iget-object v9, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    if-eqz v8, :cond_a

    goto :goto_6

    :cond_a
    invoke-direct {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    :goto_6
    iget-object v8, v9, Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;->b:Landroid/widget/ImageView;

    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_7

    :cond_b
    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    if-eqz v7, :cond_c

    invoke-virtual {p0, v7}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_c
    :goto_7
    and-int/lit8 v7, v1, 0x8

    if-eqz v7, :cond_14

    and-int/lit8 v7, p1, 0x8

    const/4 v8, 0x2

    if-eqz v7, :cond_10

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getNavigationMode()I

    move-result v7

    if-ne v7, v8, :cond_f

    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {p0, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {p0, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v7, :cond_d

    iget-object v8, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v8, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    iget-object v8, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {v7, v8}, Lmiuix/springback/view/SpringBackLayout;->setTarget(Landroid/view/View;)V

    :cond_d
    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->u0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v7, :cond_e

    iget-object v8, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v8, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    iget-object v8, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->u0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {v7, v8}, Lmiuix/springback/view/SpringBackLayout;->setTarget(Landroid/view/View;)V

    :cond_e
    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {v7}, Landroid/widget/FrameLayout;->removeAllViews()V

    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    invoke-virtual {v7}, Landroid/widget/FrameLayout;->removeAllViews()V

    :cond_f
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s()V

    goto :goto_8

    :cond_10
    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    if-eqz v7, :cond_11

    iget-object v9, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    iget-object v7, v7, Le1/a;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v9, v7}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    :cond_11
    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->J:Lx0/j;

    if-eqz v7, :cond_12

    iget-object v9, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    iget-object v7, v7, Lx0/j;->c:Ljava/lang/Object;

    check-cast v7, Landroid/widget/LinearLayout;

    invoke-virtual {v9, v7}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    :cond_12
    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E0:Landroid/view/View;

    invoke-virtual {p0, v7}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iput-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    iput-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->J:Lx0/j;

    iput-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E0:Landroid/view/View;

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getNavigationMode()I

    move-result v7

    if-ne v7, v8, :cond_14

    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {p0, v7}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {p0, v7}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->x:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k0:Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->z:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v7, :cond_13

    iget-object v8, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {v8, v7}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    :cond_13
    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->I:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v7, :cond_14

    iget-object v8, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->i0:Landroid/widget/FrameLayout;

    invoke-virtual {v8, v7}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    :cond_14
    :goto_8
    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    if-eqz v7, :cond_1b

    and-int/lit8 v8, v1, 0x6

    if-eqz v8, :cond_1b

    iget v8, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->G:I

    and-int/2addr v8, v3

    if-eqz v8, :cond_15

    move v8, v5

    goto :goto_9

    :cond_15
    move v8, v4

    :goto_9
    iget-object v7, v7, Le1/a;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v7

    if-nez v7, :cond_18

    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E0:Landroid/view/View;

    if-nez v0, :cond_17

    if-eqz v8, :cond_16

    move v6, v4

    goto :goto_a

    :cond_16
    move v6, v3

    :cond_17
    :goto_a
    invoke-virtual {v7, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_18
    iget-object v6, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    if-nez v0, :cond_19

    if-eqz v8, :cond_19

    move v7, v5

    goto :goto_b

    :cond_19
    move v7, v4

    :goto_b
    iget-object v6, v6, Le1/a;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    iget-object v6, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->J:Lx0/j;

    if-nez v0, :cond_1a

    if-eqz v8, :cond_1a

    move v4, v5

    :cond_1a
    iget-object v0, v6, Lx0/j;->c:Ljava/lang/Object;

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    :cond_1b
    and-int/lit8 v0, v1, 0x10

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E:Landroid/view/View;

    if-eqz v0, :cond_1d

    and-int/lit8 v1, p1, 0x10

    if-eqz v1, :cond_1c

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->p()V

    goto :goto_c

    :cond_1c
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_1d
    :goto_c
    invoke-virtual {p0}, Landroid/view/ViewGroup;->requestLayout()V

    goto :goto_d

    :cond_1e
    invoke-virtual {p0}, Landroid/view/ViewGroup;->invalidate()V

    :goto_d
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    if-eqz v0, :cond_21

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1f

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    invoke-virtual {p1, v2}, Landroid/widget/FrameLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_f

    :cond_1f
    and-int/2addr p1, v3

    if-eqz p1, :cond_20

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->D:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f120001

    goto :goto_e

    :cond_20
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->D:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f120000

    :goto_e
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_21
    :goto_f
    return-void
.end method

.method public setDropdownAdapter(Landroid/widget/SpinnerAdapter;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->v0:Landroid/widget/SpinnerAdapter;

    return-void
.end method

.method public setDropdownSelectedPosition(I)V
    .locals 0

    const/4 p1, 0x0

    throw p1
.end method

.method public setEndView(Landroid/view/View;)V
    .locals 4

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->H:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->H:Landroid/view/View;

    if-eqz p1, :cond_1

    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {v0}, Lu0/a;->f([Landroid/view/View;)Lu0/c;

    move-result-object v0

    check-cast v0, Lu0/a$c;

    invoke-virtual {v0}, Lu0/a$c;->b()Lu0/f;

    move-result-object v0

    const/high16 v2, 0x3f800000    # 1.0f

    new-array v3, v1, [Lu0/f$a;

    check-cast v0, Lw0/f;

    invoke-virtual {v0, v2, v3}, Lw0/f;->v(F[Lu0/f$a;)Lu0/f;

    const v2, 0x3f19999a    # 0.6f

    new-array v3, v1, [Lu0/f$a;

    invoke-virtual {v0, v2, v3}, Lw0/f;->t(F[Lu0/f$a;)Lu0/f;

    new-array v1, v1, [Lv0/a;

    invoke-virtual {v0, p1, v1}, Lw0/f;->p(Landroid/view/View;[Lv0/a;)V

    :cond_1
    return-void
.end method

.method public setExpandState(I)V
    .locals 2

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/app/widget/a;->setExpandState(I)V

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    const v1, 0x7f0a0035

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->setExpandState(I)V

    :cond_0
    return-void
.end method

.method public setHomeAsUpIndicator(I)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;->a(I)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->O:Landroid/graphics/drawable/Drawable;

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->P:I

    :goto_0
    return-void
.end method

.method public setHomeAsUpIndicator(Landroid/graphics/drawable/Drawable;)V
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v2, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;->d:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;->a:Landroid/graphics/drawable/Drawable;

    :goto_0
    invoke-virtual {v2, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iput v1, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;->c:I

    goto :goto_1

    :cond_1
    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->O:Landroid/graphics/drawable/Drawable;

    iput v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->P:I

    :goto_1
    return-void
.end method

.method public setHomeButtonEnabled(Z)V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setFocusable(Z)V

    if-nez p1, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_0
    iget p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->G:I

    and-int/lit8 p1, p1, 0x4

    if-eqz p1, :cond_1

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->D:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f120001

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->D:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f120000

    :goto_1
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    :cond_2
    :goto_2
    return-void
.end method

.method public setIcon(I)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->D:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setIcon(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->S:Landroid/graphics/drawable/Drawable;

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->T:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->T:I

    if-eqz p1, :cond_1

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->G:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getLogo()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->K:Landroid/view/View;

    if-eqz p1, :cond_2

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->M:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->S:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;->b:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_2
    return-void
.end method

.method public setLogo(I)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->D:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setLogo(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setLogo(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->g0:Landroid/graphics/drawable/Drawable;

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->T:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->T:I

    if-eqz p1, :cond_0

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->G:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method public setNavigationMode(I)V
    .locals 4

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->m0:I

    if-eq p1, v0, :cond_3

    const/4 v1, 0x1

    if-eq p1, v1, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->z:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->I:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v1, :cond_1

    iget-boolean v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->a0:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->t0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->u0:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {p0, v0, v1, v2, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->o(Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;)V

    :cond_1
    :goto_0
    iput p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->m0:I

    invoke-virtual {p0}, Landroid/view/ViewGroup;->requestLayout()V

    goto :goto_1

    :cond_2
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "MIUIX Deleted"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_1
    return-void
.end method

.method public setProgress(I)V
    .locals 0

    add-int/lit8 p1, p1, 0x0

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->y(I)V

    return-void
.end method

.method public setProgressBarIndeterminate(Z)V
    .locals 0

    if-eqz p1, :cond_0

    const/4 p1, -0x3

    goto :goto_0

    :cond_0
    const/4 p1, -0x4

    :goto_0
    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->y(I)V

    return-void
.end method

.method public setProgressBarIndeterminateVisibility(Z)V
    .locals 0

    if-eqz p1, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    const/4 p1, -0x2

    :goto_0
    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->y(I)V

    return-void
.end method

.method public setProgressBarVisibility(Z)V
    .locals 0

    if-eqz p1, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    const/4 p1, -0x2

    :goto_0
    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->y(I)V

    return-void
.end method

.method public bridge synthetic setResizable(Z)V
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/app/widget/a;->setResizable(Z)V

    return-void
.end method

.method public setSplitActionBar(Z)V
    .locals 3

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/a;->n:Z

    if-eq v0, p1, :cond_8

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    if-eqz p1, :cond_2

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->o:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    :cond_1
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x2

    :goto_0
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Li1/c;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestLayout()V

    :cond_3
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->o:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v0, :cond_5

    if-eqz p1, :cond_4

    const/4 v1, 0x0

    goto :goto_1

    :cond_4
    const/16 v1, 0x8

    :goto_1
    invoke-virtual {v0, v1}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setVisibility(I)V

    :cond_5
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    if-eqz v0, :cond_7

    if-nez p1, :cond_6

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_6
    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->a:Li1/b;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Li1/b;->n(IZ)V

    :cond_7
    :goto_2
    invoke-super {p0, p1}, Lmiuix/appcompat/internal/app/widget/a;->setSplitActionBar(Z)V

    :cond_8
    return-void
.end method

.method public bridge synthetic setSplitView(Lmiuix/appcompat/internal/app/widget/ActionBarContainer;)V
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/app/widget/a;->setSplitView(Lmiuix/appcompat/internal/app/widget/ActionBarContainer;)V

    return-void
.end method

.method public bridge synthetic setSplitWhenNarrow(Z)V
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/app/widget/a;->setSplitWhenNarrow(Z)V

    return-void
.end method

.method public setStartView(Landroid/view/View;)V
    .locals 4

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->w0:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->w0:Landroid/view/View;

    if-eqz p1, :cond_1

    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {v0}, Lu0/a;->f([Landroid/view/View;)Lu0/c;

    move-result-object v0

    check-cast v0, Lu0/a$c;

    invoke-virtual {v0}, Lu0/a$c;->b()Lu0/f;

    move-result-object v0

    const/high16 v2, 0x3f800000    # 1.0f

    new-array v3, v1, [Lu0/f$a;

    check-cast v0, Lw0/f;

    invoke-virtual {v0, v2, v3}, Lw0/f;->v(F[Lu0/f$a;)Lu0/f;

    const v2, 0x3f19999a    # 0.6f

    new-array v3, v1, [Lu0/f$a;

    invoke-virtual {v0, v2, v3}, Lw0/f;->t(F[Lu0/f$a;)Lu0/f;

    new-array v1, v1, [Lv0/a;

    invoke-virtual {v0, p1, v1}, Lw0/f;->p(Landroid/view/View;[Lv0/a;)V

    :cond_1
    return-void
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .locals 4

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->y0:Ljava/lang/CharSequence;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    if-eqz v0, :cond_6

    if-eqz p1, :cond_0

    iget-object v0, v0, Le1/a;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->J:Lx0/j;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p1, :cond_1

    iget-object v0, v0, Lx0/j;->b:Ljava/lang/Object;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    const/16 v1, 0x8

    const/4 v2, 0x0

    if-eqz p1, :cond_2

    move v3, v2

    goto :goto_0

    :cond_2
    move v3, v1

    :goto_0
    iget-object v0, v0, Le1/a;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->J:Lx0/j;

    if-eqz p1, :cond_3

    move p1, v2

    goto :goto_1

    :cond_3
    move p1, v1

    :goto_1
    invoke-virtual {v0, p1}, Lx0/j;->b(I)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->K:Landroid/view/View;

    if-nez p1, :cond_5

    iget p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->G:I

    and-int/2addr p1, v1

    if-eqz p1, :cond_5

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A0:Ljava/lang/CharSequence;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->y0:Ljava/lang/CharSequence;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_5

    :cond_4
    const/4 v2, 0x1

    :cond_5
    invoke-direct {p0, v2}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setTitleVisibility(Z)V

    new-instance p1, Lmiuix/appcompat/internal/app/widget/g;

    const/4 v0, 0x3

    invoke-direct {p1, p0, v0}, Lmiuix/appcompat/internal/app/widget/g;-><init>(Lmiuix/appcompat/internal/app/widget/ActionBarView;I)V

    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    :cond_6
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->J0:Z

    invoke-direct {p0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setTitleImpl(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public bridge synthetic setVisibility(I)V
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/app/widget/a;->setVisibility(I)V

    return-void
.end method

.method public setWindowCallback(Landroid/view/Window$Callback;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->K0:Landroid/view/Window$Callback;

    return-void
.end method

.method public setWindowTitle(Ljava/lang/CharSequence;)V
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->J0:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setTitleImpl(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public shouldDelayChildPressedState()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final t()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v0

    if-gtz v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->u:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public u()Z
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->D:Landroid/content/Context;

    const v1, 0x7f040038

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lr1/a;->c(Landroid/content/Context;IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->a0:Z

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method public final v()Z
    .locals 4

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->B0:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E:Landroid/view/View;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v3, v0, Ld/a$a;

    if-eqz v3, :cond_0

    check-cast v0, Ld/a$a;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    goto :goto_2

    :cond_1
    iget v0, v0, Ld/a$a;->a:I

    invoke-virtual {p0}, Landroid/view/View;->getLayoutDirection()I

    move-result v3

    if-ne v3, v1, :cond_2

    move v3, v1

    goto :goto_1

    :cond_2
    move v3, v2

    :goto_1
    invoke-virtual {p0, v0, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->w(IZ)I

    move-result v0

    const v3, 0x800005

    if-ne v0, v3, :cond_3

    goto :goto_3

    :cond_3
    :goto_2
    move v0, v2

    goto :goto_4

    :cond_4
    :goto_3
    move v0, v1

    :goto_4
    if-eqz v0, :cond_6

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->Q:Lmiuix/appcompat/internal/app/widget/ActionBarView$HomeView;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    const/16 v3, 0x8

    if-ne v0, v3, :cond_6

    :cond_5
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->u()Z

    move-result v0

    if-nez v0, :cond_6

    goto :goto_5

    :cond_6
    move v1, v2

    :goto_5
    return v1
.end method

.method public final w(IZ)I
    .locals 1

    const v0, 0x800007

    and-int/2addr p1, v0

    const/high16 v0, 0x800000

    and-int/2addr v0, p1

    if-nez v0, :cond_3

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_1

    goto :goto_0

    :cond_0
    const/4 v0, 0x5

    if-ne p1, v0, :cond_3

    if-eqz p2, :cond_2

    :cond_1
    const p1, 0x800003

    goto :goto_1

    :cond_2
    :goto_0
    const p1, 0x800005

    :cond_3
    :goto_1
    return p1
.end method

.method public final x(Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 1

    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method public final y(I)V
    .locals 6

    invoke-direct {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getCircularProgressBar()Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-direct {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getHorizontalProgressBar()Landroid/widget/ProgressBar;

    move-result-object v1

    const/4 v2, 0x4

    const/16 v3, 0x2710

    const/4 v4, 0x0

    const/4 v5, -0x1

    if-ne p1, v5, :cond_3

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getProgress()I

    move-result p1

    invoke-virtual {v1}, Landroid/widget/ProgressBar;->isIndeterminate()Z

    move-result v5

    if-nez v5, :cond_0

    if-ge p1, v3, :cond_1

    :cond_0
    move v2, v4

    :cond_1
    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_2
    if-eqz v0, :cond_b

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :cond_3
    const/4 v5, -0x2

    if-ne p1, v5, :cond_5

    const/16 p1, 0x8

    if-eqz v1, :cond_4

    invoke-virtual {v1, p1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_4
    if-eqz v0, :cond_b

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :cond_5
    const/4 v5, -0x3

    if-ne p1, v5, :cond_6

    const/4 p1, 0x1

    invoke-virtual {v1, p1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    goto :goto_0

    :cond_6
    const/4 v5, -0x4

    if-ne p1, v5, :cond_7

    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    goto :goto_0

    :cond_7
    if-ltz p1, :cond_b

    if-gt p1, v3, :cond_b

    add-int/lit8 v5, p1, 0x0

    invoke-virtual {v1, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    if-ge p1, v3, :cond_9

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result p1

    if-ne p1, v2, :cond_8

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_8
    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getProgress()I

    move-result p1

    if-ge p1, v3, :cond_b

    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :cond_9
    if-eqz v0, :cond_a

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result p1

    if-nez p1, :cond_a

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_a
    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result p1

    if-nez p1, :cond_b

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_b
    :goto_0
    return-void
.end method

.method public final z()V
    .locals 5

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->D:Landroid/content/Context;

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A0:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->u()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f040038

    invoke-static {v0, v1, v2}, Lr1/a;->c(Landroid/content/Context;IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    const/16 v1, 0x8

    if-nez v0, :cond_2

    iget-boolean v3, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->B:Z

    if-nez v3, :cond_1

    goto :goto_1

    :cond_1
    move v3, v2

    goto :goto_2

    :cond_2
    :goto_1
    move v3, v1

    :goto_2
    iget-object v4, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    if-eqz v4, :cond_3

    iget-object v4, v4, Le1/a;->e:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_3
    if-nez v0, :cond_4

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->y0:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->B:Z

    if-nez v0, :cond_5

    :cond_4
    move v2, v1

    :cond_5
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ActionBarView;->A:Le1/a;

    if-eqz v0, :cond_6

    iget-object v0, v0, Le1/a;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_6
    return-void
.end method
