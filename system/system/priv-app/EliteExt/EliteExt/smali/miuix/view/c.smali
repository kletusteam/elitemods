.class public Lmiuix/view/c;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:I

.field public static final b:I

.field public static final c:I

.field public static final d:I

.field public static final e:I

.field public static final f:I

.field public static final g:I

.field public static final h:I

.field public static final i:I

.field public static final j:I

.field public static final k:I

.field public static final l:I

.field public static final m:I

.field public static final n:I

.field public static final o:I

.field public static final p:I

.field public static final q:I

.field public static final r:Lk/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lk/h<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 17

    new-instance v0, Lk/h;

    invoke-direct {v0}, Lk/h;-><init>()V

    sput-object v0, Lmiuix/view/c;->r:Lk/h;

    const/high16 v1, 0x10000000

    sput v1, Lmiuix/view/c;->q:I

    const v2, 0x10000001

    sput v2, Lmiuix/view/c;->o:I

    const v3, 0x10000002

    sput v3, Lmiuix/view/c;->n:I

    const v4, 0x10000003

    sput v4, Lmiuix/view/c;->a:I

    const v5, 0x10000004

    sput v5, Lmiuix/view/c;->m:I

    const v6, 0x10000005

    sput v6, Lmiuix/view/c;->f:I

    const v7, 0x10000006

    sput v7, Lmiuix/view/c;->h:I

    const v8, 0x10000007

    sput v8, Lmiuix/view/c;->g:I

    const v9, 0x10000008

    sput v9, Lmiuix/view/c;->e:I

    const v10, 0x10000009

    sput v10, Lmiuix/view/c;->k:I

    const v11, 0x1000000a

    sput v11, Lmiuix/view/c;->j:I

    const v12, 0x1000000b

    sput v12, Lmiuix/view/c;->i:I

    const v13, 0x1000000c

    sput v13, Lmiuix/view/c;->l:I

    const v14, 0x1000000d

    sput v14, Lmiuix/view/c;->p:I

    const v15, 0x1000000e

    sput v15, Lmiuix/view/c;->b:I

    const v15, 0x1000000f

    sput v15, Lmiuix/view/c;->d:I

    const v16, 0x10000010

    sput v16, Lmiuix/view/c;->c:I

    const-string v15, "MIUI_VIRTUAL_RELEASE"

    invoke-virtual {v0, v1, v15}, Lk/h;->a(ILjava/lang/Object;)V

    const-string v1, "MIUI_TAP_NORMAL"

    invoke-virtual {v0, v2, v1}, Lk/h;->a(ILjava/lang/Object;)V

    const-string v1, "MIUI_TAP_LIGHT"

    invoke-virtual {v0, v3, v1}, Lk/h;->a(ILjava/lang/Object;)V

    const-string v1, "MIUI_FLICK"

    invoke-virtual {v0, v4, v1}, Lk/h;->a(ILjava/lang/Object;)V

    const-string v1, "MIUI_SWITCH"

    invoke-virtual {v0, v5, v1}, Lk/h;->a(ILjava/lang/Object;)V

    const-string v1, "MIUI_MESH_HEAVY"

    invoke-virtual {v0, v6, v1}, Lk/h;->a(ILjava/lang/Object;)V

    const-string v1, "MIUI_MESH_NORMAL"

    invoke-virtual {v0, v7, v1}, Lk/h;->a(ILjava/lang/Object;)V

    const-string v1, "MIUI_MESH_LIGHT"

    invoke-virtual {v0, v8, v1}, Lk/h;->a(ILjava/lang/Object;)V

    const-string v1, "MIUI_LONG_PRESS"

    invoke-virtual {v0, v9, v1}, Lk/h;->a(ILjava/lang/Object;)V

    const-string v1, "MIUI_POPUP_NORMAL"

    invoke-virtual {v0, v10, v1}, Lk/h;->a(ILjava/lang/Object;)V

    const-string v1, "MIUI_POPUP_LIGHT"

    invoke-virtual {v0, v11, v1}, Lk/h;->a(ILjava/lang/Object;)V

    const-string v1, "MIUI_PICK_UP"

    invoke-virtual {v0, v12, v1}, Lk/h;->a(ILjava/lang/Object;)V

    const-string v1, "MIUI_SCROLL_EDGE"

    invoke-virtual {v0, v13, v1}, Lk/h;->a(ILjava/lang/Object;)V

    const-string v1, "MIUI_TRIGGER_DRAWER"

    invoke-virtual {v0, v14, v1}, Lk/h;->a(ILjava/lang/Object;)V

    const-string v1, "MIUI_FLICK_LIGHT"

    const v2, 0x1000000e

    invoke-virtual {v0, v2, v1}, Lk/h;->a(ILjava/lang/Object;)V

    const-string v1, "MIUI_HOLD"

    const v2, 0x1000000f

    invoke-virtual {v0, v2, v1}, Lk/h;->a(ILjava/lang/Object;)V

    return-void
.end method
