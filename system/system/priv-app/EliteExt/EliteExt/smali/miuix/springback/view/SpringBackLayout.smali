.class public Lmiuix/springback/view/SpringBackLayout;
.super Landroid/view/ViewGroup;
.source ""

# interfaces
.implements Lw/l;
.implements Lw/i;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/springback/view/SpringBackLayout$a;,
        Lmiuix/springback/view/SpringBackLayout$b;
    }
.end annotation


# instance fields
.field public A:Le2/c;

.field public B:Landroid/view/View;

.field public C:I

.field public D:F

.field public E:F

.field public F:F

.field public G:I

.field public a:I

.field public b:I

.field public c:Le2/a;

.field public d:F

.field public e:F

.field public f:F

.field public g:F

.field public h:Z

.field public i:Z

.field public j:I

.field public k:Z

.field public final l:Lw/j;

.field public final m:Lw/m;

.field public final n:[I

.field public o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmiuix/springback/view/SpringBackLayout$a;",
            ">;"
        }
    .end annotation
.end field

.field public p:Lmiuix/springback/view/SpringBackLayout$b;

.field public q:I

.field public final r:[I

.field public final s:[I

.field public final t:I

.field public final u:I

.field public v:Z

.field public w:I

.field public x:I

.field public y:Z

.field public z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, -0x1

    iput v0, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    const/4 v1, 0x0

    iput v1, p0, Lmiuix/springback/view/SpringBackLayout;->a:I

    const/4 v2, 0x2

    new-array v3, v2, [I

    iput-object v3, p0, Lmiuix/springback/view/SpringBackLayout;->s:[I

    new-array v3, v2, [I

    iput-object v3, p0, Lmiuix/springback/view/SpringBackLayout;->r:[I

    new-array v3, v2, [I

    iput-object v3, p0, Lmiuix/springback/view/SpringBackLayout;->n:[I

    const/4 v3, 0x1

    iput-boolean v3, p0, Lmiuix/springback/view/SpringBackLayout;->y:Z

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lmiuix/springback/view/SpringBackLayout;->o:Ljava/util/List;

    iput v1, p0, Lmiuix/springback/view/SpringBackLayout;->x:I

    new-instance v4, Lw/m;

    invoke-direct {v4}, Lw/m;-><init>()V

    iput-object v4, p0, Lmiuix/springback/view/SpringBackLayout;->m:Lw/m;

    :try_start_0
    const-string v4, "miui.core.view.NestedScrollingParent3"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v4, v3

    goto :goto_0

    :catchall_0
    move v4, v1

    :goto_0
    if-eqz v4, :cond_0

    new-instance v4, Lp1/a;

    invoke-direct {v4, p0}, Lp1/a;-><init>(Landroid/view/View;)V

    goto :goto_1

    :cond_0
    new-instance v4, Lw/j;

    invoke-direct {v4, p0}, Lw/j;-><init>(Landroid/view/View;)V

    :goto_1
    iput-object v4, p0, Lmiuix/springback/view/SpringBackLayout;->l:Lw/j;

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v4

    iput v4, p0, Lmiuix/springback/view/SpringBackLayout;->G:I

    sget-object v4, Landroidx/emoji2/text/l;->l0:[I

    invoke-virtual {p1, p2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p2

    invoke-virtual {p2, v3, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lmiuix/springback/view/SpringBackLayout;->C:I

    invoke-virtual {p2, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lmiuix/springback/view/SpringBackLayout;->q:I

    const/4 v0, 0x3

    invoke-virtual {p2, v2, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lmiuix/springback/view/SpringBackLayout;->z:I

    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    new-instance p2, Le2/c;

    invoke-direct {p2}, Le2/c;-><init>()V

    iput-object p2, p0, Lmiuix/springback/view/SpringBackLayout;->A:Le2/c;

    new-instance p2, Le2/a;

    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->q:I

    invoke-direct {p2, p0, v0}, Le2/a;-><init>(Landroid/view/ViewGroup;I)V

    iput-object p2, p0, Lmiuix/springback/view/SpringBackLayout;->c:Le2/a;

    invoke-virtual {p0, v3}, Lmiuix/springback/view/SpringBackLayout;->setNestedScrollingEnabled(Z)V

    new-instance p2, Landroid/util/DisplayMetrics;

    invoke-direct {p2}, Landroid/util/DisplayMetrics;-><init>()V

    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/WindowManager;

    invoke-interface {p1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget p1, p2, Landroid/util/DisplayMetrics;->widthPixels:I

    iput p1, p0, Lmiuix/springback/view/SpringBackLayout;->u:I

    iget p1, p2, Landroid/util/DisplayMetrics;->heightPixels:I

    iput p1, p0, Lmiuix/springback/view/SpringBackLayout;->t:I

    sget-boolean p1, Lu1/a;->a:Z

    if-eqz p1, :cond_1

    iput-boolean v1, p0, Lmiuix/springback/view/SpringBackLayout;->y:Z

    :cond_1
    return-void
.end method


# virtual methods
.method public final A()Z
    .locals 2

    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->z:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public final a(I)V
    .locals 5

    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    if-ne p1, v3, :cond_3

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getScrollY()I

    move-result v4

    if-eqz v4, :cond_0

    move v4, v2

    goto :goto_0

    :cond_0
    move v4, v1

    :goto_0
    if-eqz v4, :cond_2

    iput-boolean v2, p0, Lmiuix/springback/view/SpringBackLayout;->h:Z

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getScrollY()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0, v0, p1}, Lmiuix/springback/view/SpringBackLayout;->h(FI)F

    move-result p1

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    invoke-virtual {p0, v1, p1, v3}, Lmiuix/springback/view/SpringBackLayout;->q(FFI)F

    move-result p1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getScrollY()I

    move-result v0

    if-gez v0, :cond_1

    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->e:F

    sub-float/2addr v0, p1

    goto :goto_1

    :cond_1
    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->e:F

    add-float/2addr v0, p1

    :goto_1
    iput v0, p0, Lmiuix/springback/view/SpringBackLayout;->e:F

    iput v0, p0, Lmiuix/springback/view/SpringBackLayout;->g:F

    goto :goto_4

    :cond_2
    iput-boolean v1, p0, Lmiuix/springback/view/SpringBackLayout;->h:Z

    goto :goto_4

    :cond_3
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getScrollX()I

    move-result v4

    if-eqz v4, :cond_4

    move v4, v2

    goto :goto_2

    :cond_4
    move v4, v1

    :goto_2
    if-eqz v4, :cond_6

    iput-boolean v2, p0, Lmiuix/springback/view/SpringBackLayout;->h:Z

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getScrollX()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0, v0, p1}, Lmiuix/springback/view/SpringBackLayout;->h(FI)F

    move-result p1

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    invoke-virtual {p0, v1, p1, v3}, Lmiuix/springback/view/SpringBackLayout;->q(FFI)F

    move-result p1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getScrollX()I

    move-result v0

    if-gez v0, :cond_5

    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->d:F

    sub-float/2addr v0, p1

    goto :goto_3

    :cond_5
    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->d:F

    add-float/2addr v0, p1

    :goto_3
    iput v0, p0, Lmiuix/springback/view/SpringBackLayout;->d:F

    iput v0, p0, Lmiuix/springback/view/SpringBackLayout;->f:F

    goto :goto_4

    :cond_6
    iput-boolean v1, p0, Lmiuix/springback/view/SpringBackLayout;->h:Z

    :goto_4
    return-void
.end method

.method public final b(I[II)V
    .locals 1

    const/4 v0, 0x2

    if-ne p3, v0, :cond_0

    const/4 p3, 0x1

    aput p1, p2, p3

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    aput p1, p2, p3

    :goto_0
    return-void
.end method

.method public final c(I)V
    .locals 4

    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->x:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lmiuix/springback/view/SpringBackLayout;->x:I

    iget-object v1, p0, Lmiuix/springback/view/SpringBackLayout;->o:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/springback/view/SpringBackLayout$a;

    iget-object v3, p0, Lmiuix/springback/view/SpringBackLayout;->A:Le2/c;

    iget-boolean v3, v3, Le2/c;->f:Z

    invoke-interface {v2, v0, p1, v3}, Lmiuix/springback/view/SpringBackLayout$a;->b(IIZ)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public computeScroll()V
    .locals 4

    invoke-super {p0}, Landroid/view/ViewGroup;->computeScroll()V

    iget-object v0, p0, Lmiuix/springback/view/SpringBackLayout;->A:Le2/c;

    invoke-virtual {v0}, Le2/c;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/springback/view/SpringBackLayout;->A:Le2/c;

    iget-wide v1, v0, Le2/c;->a:D

    double-to-int v1, v1

    iget-wide v2, v0, Le2/c;->b:D

    double-to-int v0, v2

    invoke-virtual {p0, v1, v0}, Landroid/view/ViewGroup;->scrollTo(II)V

    iget-object v0, p0, Lmiuix/springback/view/SpringBackLayout;->A:Le2/c;

    iget-boolean v0, v0, Le2/c;->f:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->postInvalidateOnAnimation()V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmiuix/springback/view/SpringBackLayout;->c(I)V

    :cond_1
    :goto_0
    return-void
.end method

.method public final d(I)Z
    .locals 1

    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->w:I

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public dispatchNestedFling(FFZ)Z
    .locals 1

    iget-object v0, p0, Lmiuix/springback/view/SpringBackLayout;->l:Lw/j;

    invoke-virtual {v0, p1, p2, p3}, Lw/j;->a(FFZ)Z

    move-result p1

    return p1
.end method

.method public dispatchNestedPreFling(FF)Z
    .locals 1

    iget-object v0, p0, Lmiuix/springback/view/SpringBackLayout;->l:Lw/j;

    invoke-virtual {v0, p1, p2}, Lw/j;->b(FF)Z

    move-result p1

    return p1
.end method

.method public dispatchNestedPreScroll(II[I[I)Z
    .locals 1

    iget-object v0, p0, Lmiuix/springback/view/SpringBackLayout;->l:Lw/j;

    invoke-virtual {v0, p1, p2, p3, p4}, Lw/j;->c(II[I[I)Z

    move-result p1

    return p1
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-nez v0, :cond_1

    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->x:I

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lmiuix/springback/view/SpringBackLayout;->c:Le2/a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v4

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v4

    if-ltz v4, :cond_0

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v5

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    new-array v6, v2, [I

    fill-array-data v6, :array_0

    iget-object v7, v0, Le2/a;->e:Landroid/view/ViewGroup;

    invoke-virtual {v7, v6}, Landroid/view/ViewGroup;->getLocationInWindow([I)V

    aget v7, v6, v1

    aget v6, v6, v3

    iget-object v8, v0, Le2/a;->e:Landroid/view/ViewGroup;

    invoke-virtual {v8}, Landroid/view/ViewGroup;->getHeight()I

    move-result v8

    new-instance v9, Landroid/graphics/Rect;

    iget-object v0, v0, Le2/a;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    add-int/2addr v0, v7

    add-int/2addr v8, v6

    invoke-direct {v9, v7, v6, v0, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    float-to-int v0, v4

    float-to-int v4, v5

    invoke-virtual {v9, v0, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0, v3}, Lmiuix/springback/view/SpringBackLayout;->c(I)V

    :cond_1
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result p1

    if-ne p1, v3, :cond_2

    iget p1, p0, Lmiuix/springback/view/SpringBackLayout;->x:I

    if-eq p1, v2, :cond_2

    invoke-virtual {p0, v1}, Lmiuix/springback/view/SpringBackLayout;->c(I)V

    :cond_2
    return v0

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public final e(I)Z
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    iget-object p1, p0, Lmiuix/springback/view/SpringBackLayout;->B:Landroid/view/View;

    instance-of v1, p1, Landroid/widget/ListView;

    if-eqz v1, :cond_0

    check-cast p1, Landroid/widget/ListView;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->canScrollList(I)Z

    move-result p1

    xor-int/2addr p1, v0

    return p1

    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/View;->canScrollVertically(I)Z

    move-result p1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lmiuix/springback/view/SpringBackLayout;->B:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->canScrollHorizontally(I)Z

    move-result p1

    :goto_0
    xor-int/2addr p1, v0

    return p1
.end method

.method public final f(I)Z
    .locals 2

    const/4 v0, -0x1

    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    iget-object p1, p0, Lmiuix/springback/view/SpringBackLayout;->B:Landroid/view/View;

    instance-of v1, p1, Landroid/widget/ListView;

    if-eqz v1, :cond_0

    check-cast p1, Landroid/widget/ListView;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->canScrollList(I)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1

    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/View;->canScrollVertically(I)Z

    move-result p1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lmiuix/springback/view/SpringBackLayout;->B:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->canScrollHorizontally(I)Z

    move-result p1

    :goto_0
    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public final g(FI)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x2

    neg-float p1, p1

    float-to-int p1, p1

    if-ne p2, v1, :cond_0

    invoke-virtual {p0, v0, p1}, Landroid/view/ViewGroup;->scrollTo(II)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1, v0}, Landroid/view/ViewGroup;->scrollTo(II)V

    :goto_0
    return-void
.end method

.method public getSpringBackMode()I
    .locals 1

    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->z:I

    return v0
.end method

.method public final h(FI)F
    .locals 6

    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    iget p2, p0, Lmiuix/springback/view/SpringBackLayout;->t:I

    goto :goto_0

    :cond_0
    iget p2, p0, Lmiuix/springback/view/SpringBackLayout;->u:I

    :goto_0
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p1, v0}, Ljava/lang/Math;->min(FF)F

    move-result p1

    float-to-double v0, p1

    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    div-double/2addr v4, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    sub-double/2addr v4, v2

    add-double/2addr v4, v0

    double-to-float p1, v4

    int-to-float p2, p2

    mul-float/2addr p1, p2

    return p1
.end method

.method public i(Landroid/view/View;Landroid/view/View;II)V
    .locals 5

    iget-boolean v0, p0, Lmiuix/springback/view/SpringBackLayout;->y:Z

    if-eqz v0, :cond_7

    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->j:I

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v0, v1, :cond_0

    move v0, v3

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move v1, v3

    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getScrollY()I

    move-result v0

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getScrollX()I

    move-result v0

    :goto_2
    int-to-float v0, v0

    const/4 v4, 0x0

    if-eqz p4, :cond_4

    cmpl-float p4, v0, v4

    if-nez p4, :cond_3

    iput v4, p0, Lmiuix/springback/view/SpringBackLayout;->D:F

    goto :goto_3

    :cond_3
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result p4

    invoke-virtual {p0, v1}, Lmiuix/springback/view/SpringBackLayout;->j(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-virtual {p0, p4, v0, v1}, Lmiuix/springback/view/SpringBackLayout;->q(FFI)F

    move-result p4

    iput p4, p0, Lmiuix/springback/view/SpringBackLayout;->D:F

    :goto_3
    iput-boolean v3, p0, Lmiuix/springback/view/SpringBackLayout;->i:Z

    iput v2, p0, Lmiuix/springback/view/SpringBackLayout;->a:I

    goto :goto_6

    :cond_4
    cmpl-float p4, v0, v4

    if-nez p4, :cond_5

    iput v4, p0, Lmiuix/springback/view/SpringBackLayout;->F:F

    goto :goto_4

    :cond_5
    cmpg-float p4, v0, v4

    if-gez p4, :cond_6

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result p4

    invoke-virtual {p0, v1}, Lmiuix/springback/view/SpringBackLayout;->j(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-virtual {p0, p4, v0, v1}, Lmiuix/springback/view/SpringBackLayout;->q(FFI)F

    move-result p4

    iput p4, p0, Lmiuix/springback/view/SpringBackLayout;->F:F

    :goto_4
    iput v4, p0, Lmiuix/springback/view/SpringBackLayout;->E:F

    goto :goto_5

    :cond_6
    iput v4, p0, Lmiuix/springback/view/SpringBackLayout;->F:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result p4

    invoke-virtual {p0, v1}, Lmiuix/springback/view/SpringBackLayout;->j(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-virtual {p0, p4, v0, v1}, Lmiuix/springback/view/SpringBackLayout;->q(FFI)F

    move-result p4

    iput p4, p0, Lmiuix/springback/view/SpringBackLayout;->E:F

    :goto_5
    iput-boolean v3, p0, Lmiuix/springback/view/SpringBackLayout;->k:Z

    :goto_6
    iput-boolean v2, p0, Lmiuix/springback/view/SpringBackLayout;->v:Z

    iget-object p4, p0, Lmiuix/springback/view/SpringBackLayout;->A:Le2/c;

    invoke-virtual {p4}, Le2/c;->b()V

    :cond_7
    invoke-virtual {p0, p1, p2, p3}, Lmiuix/springback/view/SpringBackLayout;->onNestedScrollAccepted(Landroid/view/View;Landroid/view/View;I)V

    return-void
.end method

.method public isNestedScrollingEnabled()Z
    .locals 1

    iget-object v0, p0, Lmiuix/springback/view/SpringBackLayout;->l:Lw/j;

    invoke-virtual {v0}, Lw/j;->k()Z

    move-result v0

    return v0
.end method

.method public final j(I)F
    .locals 1

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0, p1}, Lmiuix/springback/view/SpringBackLayout;->h(FI)F

    move-result p1

    return p1
.end method

.method public k(Landroid/view/View;IIIII[I)V
    .locals 14

    move-object v0, p0

    iget v1, v0, Lmiuix/springback/view/SpringBackLayout;->j:I

    const/4 v9, 0x2

    const/4 v10, 0x0

    const/4 v11, 0x1

    if-ne v1, v9, :cond_0

    move v12, v11

    goto :goto_0

    :cond_0
    move v12, v10

    :goto_0
    if-eqz v12, :cond_1

    aget v1, p7, v11

    goto :goto_1

    :cond_1
    aget v1, p7, v10

    :goto_1
    move v13, v1

    iget-object v6, v0, Lmiuix/springback/view/SpringBackLayout;->r:[I

    iget-object v1, v0, Lmiuix/springback/view/SpringBackLayout;->l:Lw/j;

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    invoke-virtual/range {v1 .. v8}, Lw/j;->e(IIII[II[I)V

    iget-boolean v1, v0, Lmiuix/springback/view/SpringBackLayout;->y:Z

    if-nez v1, :cond_2

    return-void

    :cond_2
    if-eqz v12, :cond_3

    aget v1, p7, v11

    goto :goto_2

    :cond_3
    aget v1, p7, v10

    :goto_2
    sub-int/2addr v1, v13

    if-eqz v12, :cond_4

    sub-int v1, p5, v1

    goto :goto_3

    :cond_4
    sub-int v1, p4, v1

    :goto_3
    if-eqz v1, :cond_5

    move v10, v1

    :cond_5
    if-eqz v12, :cond_6

    move v2, v9

    goto :goto_4

    :cond_6
    move v2, v11

    :goto_4
    const/4 v3, 0x4

    const/4 v4, 0x0

    if-gez v10, :cond_a

    invoke-virtual {p0, v2}, Lmiuix/springback/view/SpringBackLayout;->f(I)Z

    move-result v5

    if-eqz v5, :cond_a

    invoke-virtual {p0}, Lmiuix/springback/view/SpringBackLayout;->A()Z

    move-result v5

    if-eqz v5, :cond_a

    if-eqz p6, :cond_9

    invoke-virtual {p0, v2}, Lmiuix/springback/view/SpringBackLayout;->j(I)F

    move-result v5

    iget v6, v0, Lmiuix/springback/view/SpringBackLayout;->F:F

    cmpl-float v4, v6, v4

    if-eqz v4, :cond_7

    return-void

    :cond_7
    iget v4, v0, Lmiuix/springback/view/SpringBackLayout;->D:F

    sub-float/2addr v5, v4

    iget v4, v0, Lmiuix/springback/view/SpringBackLayout;->a:I

    if-ge v4, v3, :cond_e

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v3

    int-to-float v3, v3

    cmpg-float v3, v5, v3

    if-gtz v3, :cond_8

    iget v1, v0, Lmiuix/springback/view/SpringBackLayout;->D:F

    add-float/2addr v1, v5

    iput v1, v0, Lmiuix/springback/view/SpringBackLayout;->D:F

    aget v1, p7, v11

    int-to-float v1, v1

    add-float/2addr v1, v5

    float-to-int v1, v1

    aput v1, p7, v11

    goto :goto_5

    :cond_8
    iget v3, v0, Lmiuix/springback/view/SpringBackLayout;->D:F

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    iput v3, v0, Lmiuix/springback/view/SpringBackLayout;->D:F

    aget v3, p7, v11

    add-int/2addr v3, v1

    aput v3, p7, v11

    :goto_5
    invoke-virtual {p0, v9}, Lmiuix/springback/view/SpringBackLayout;->c(I)V

    iget v1, v0, Lmiuix/springback/view/SpringBackLayout;->D:F

    invoke-virtual {p0, v1, v2}, Lmiuix/springback/view/SpringBackLayout;->p(FI)F

    move-result v1

    goto/16 :goto_7

    :cond_9
    iget-object v3, v0, Lmiuix/springback/view/SpringBackLayout;->A:Le2/c;

    iget-boolean v3, v3, Le2/c;->f:Z

    if-eqz v3, :cond_e

    iget v3, v0, Lmiuix/springback/view/SpringBackLayout;->F:F

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    iput v3, v0, Lmiuix/springback/view/SpringBackLayout;->F:F

    invoke-virtual {p0, v11}, Lmiuix/springback/view/SpringBackLayout;->c(I)V

    iget v3, v0, Lmiuix/springback/view/SpringBackLayout;->F:F

    invoke-virtual {p0, v3, v2}, Lmiuix/springback/view/SpringBackLayout;->p(FI)F

    move-result v3

    invoke-virtual {p0, v3, v2}, Lmiuix/springback/view/SpringBackLayout;->g(FI)V

    aget v2, p7, v11

    add-int/2addr v2, v1

    aput v2, p7, v11

    goto/16 :goto_8

    :cond_a
    if-lez v10, :cond_e

    invoke-virtual {p0, v2}, Lmiuix/springback/view/SpringBackLayout;->e(I)Z

    move-result v5

    if-eqz v5, :cond_e

    invoke-virtual {p0}, Lmiuix/springback/view/SpringBackLayout;->z()Z

    move-result v5

    if-eqz v5, :cond_e

    if-eqz p6, :cond_d

    invoke-virtual {p0, v2}, Lmiuix/springback/view/SpringBackLayout;->j(I)F

    move-result v5

    iget v6, v0, Lmiuix/springback/view/SpringBackLayout;->E:F

    cmpl-float v4, v6, v4

    if-eqz v4, :cond_b

    return-void

    :cond_b
    iget v4, v0, Lmiuix/springback/view/SpringBackLayout;->D:F

    sub-float/2addr v5, v4

    iget v4, v0, Lmiuix/springback/view/SpringBackLayout;->a:I

    if-ge v4, v3, :cond_e

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v3

    int-to-float v3, v3

    cmpg-float v3, v5, v3

    if-gtz v3, :cond_c

    iget v1, v0, Lmiuix/springback/view/SpringBackLayout;->D:F

    add-float/2addr v1, v5

    iput v1, v0, Lmiuix/springback/view/SpringBackLayout;->D:F

    aget v1, p7, v11

    int-to-float v1, v1

    add-float/2addr v1, v5

    float-to-int v1, v1

    aput v1, p7, v11

    goto :goto_6

    :cond_c
    iget v3, v0, Lmiuix/springback/view/SpringBackLayout;->D:F

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    iput v3, v0, Lmiuix/springback/view/SpringBackLayout;->D:F

    aget v3, p7, v11

    add-int/2addr v3, v1

    aput v3, p7, v11

    :goto_6
    invoke-virtual {p0, v9}, Lmiuix/springback/view/SpringBackLayout;->c(I)V

    iget v1, v0, Lmiuix/springback/view/SpringBackLayout;->D:F

    invoke-virtual {p0, v1, v2}, Lmiuix/springback/view/SpringBackLayout;->p(FI)F

    move-result v1

    neg-float v1, v1

    :goto_7
    invoke-virtual {p0, v1, v2}, Lmiuix/springback/view/SpringBackLayout;->g(FI)V

    iget v1, v0, Lmiuix/springback/view/SpringBackLayout;->a:I

    add-int/2addr v1, v11

    iput v1, v0, Lmiuix/springback/view/SpringBackLayout;->a:I

    goto :goto_8

    :cond_d
    iget-object v3, v0, Lmiuix/springback/view/SpringBackLayout;->A:Le2/c;

    iget-boolean v3, v3, Le2/c;->f:Z

    if-eqz v3, :cond_e

    iget v3, v0, Lmiuix/springback/view/SpringBackLayout;->E:F

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    iput v3, v0, Lmiuix/springback/view/SpringBackLayout;->E:F

    invoke-virtual {p0, v11}, Lmiuix/springback/view/SpringBackLayout;->c(I)V

    iget v3, v0, Lmiuix/springback/view/SpringBackLayout;->E:F

    invoke-virtual {p0, v3, v2}, Lmiuix/springback/view/SpringBackLayout;->p(FI)F

    move-result v3

    neg-float v3, v3

    invoke-virtual {p0, v3, v2}, Lmiuix/springback/view/SpringBackLayout;->g(FI)V

    aget v2, p7, v11

    add-int/2addr v2, v1

    aput v2, p7, v11

    :cond_e
    :goto_8
    return-void
.end method

.method public l(Landroid/view/View;IIIII)V
    .locals 8

    iget-object v7, p0, Lmiuix/springback/view/SpringBackLayout;->n:[I

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v7}, Lmiuix/springback/view/SpringBackLayout;->k(Landroid/view/View;IIIII[I)V

    return-void
.end method

.method public m(Landroid/view/View;I)V
    .locals 4

    iget-object p1, p0, Lmiuix/springback/view/SpringBackLayout;->m:Lw/m;

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p2, v0, :cond_0

    iput v1, p1, Lw/m;->b:I

    goto :goto_0

    :cond_0
    iput v1, p1, Lw/m;->a:I

    :goto_0
    iget-object p1, p0, Lmiuix/springback/view/SpringBackLayout;->l:Lw/j;

    invoke-virtual {p1, p2}, Lw/j;->p(I)V

    iget-boolean p1, p0, Lmiuix/springback/view/SpringBackLayout;->y:Z

    if-nez p1, :cond_1

    return-void

    :cond_1
    iget p1, p0, Lmiuix/springback/view/SpringBackLayout;->j:I

    const/4 p2, 0x2

    if-ne p1, p2, :cond_2

    move p1, v0

    goto :goto_1

    :cond_2
    move p1, v1

    :goto_1
    if-eqz p1, :cond_3

    move v0, p2

    :cond_3
    iget-boolean v2, p0, Lmiuix/springback/view/SpringBackLayout;->k:Z

    const/4 v3, 0x0

    if-eqz v2, :cond_6

    iput-boolean v1, p0, Lmiuix/springback/view/SpringBackLayout;->k:Z

    if-eqz p1, :cond_4

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getScrollY()I

    move-result p1

    goto :goto_2

    :cond_4
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getScrollX()I

    move-result p1

    :goto_2
    int-to-float p1, p1

    iget-boolean v1, p0, Lmiuix/springback/view/SpringBackLayout;->i:Z

    if-nez v1, :cond_5

    cmpl-float v1, p1, v3

    if-eqz v1, :cond_5

    goto :goto_3

    :cond_5
    cmpl-float p1, p1, v3

    if-eqz p1, :cond_9

    invoke-virtual {p0, p2}, Lmiuix/springback/view/SpringBackLayout;->c(I)V

    goto :goto_4

    :cond_6
    iget-boolean p1, p0, Lmiuix/springback/view/SpringBackLayout;->i:Z

    if-eqz p1, :cond_9

    iput-boolean v1, p0, Lmiuix/springback/view/SpringBackLayout;->i:Z

    iget-boolean p1, p0, Lmiuix/springback/view/SpringBackLayout;->v:Z

    if-eqz p1, :cond_8

    iget-object p1, p0, Lmiuix/springback/view/SpringBackLayout;->A:Le2/c;

    iget-boolean p1, p1, Le2/c;->f:Z

    if-eqz p1, :cond_7

    invoke-virtual {p0, v3, v0, v1}, Lmiuix/springback/view/SpringBackLayout;->x(FIZ)V

    :cond_7
    invoke-virtual {p0}, Landroid/view/ViewGroup;->postInvalidateOnAnimation()V

    goto :goto_4

    :cond_8
    :goto_3
    invoke-virtual {p0, v0}, Lmiuix/springback/view/SpringBackLayout;->y(I)V

    :cond_9
    :goto_4
    return-void
.end method

.method public n(Landroid/view/View;II[II)V
    .locals 7

    iget-boolean p1, p0, Lmiuix/springback/view/SpringBackLayout;->y:Z

    if-eqz p1, :cond_1

    iget p1, p0, Lmiuix/springback/view/SpringBackLayout;->j:I

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    invoke-virtual {p0, p3, p4, p5}, Lmiuix/springback/view/SpringBackLayout;->r(I[II)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p2, p4, p5}, Lmiuix/springback/view/SpringBackLayout;->r(I[II)V

    :cond_1
    :goto_0
    iget-object p1, p0, Lmiuix/springback/view/SpringBackLayout;->s:[I

    const/4 v0, 0x0

    aget v1, p4, v0

    sub-int v2, p2, v1

    const/4 p2, 0x1

    aget v1, p4, p2

    sub-int v3, p3, v1

    const/4 v5, 0x0

    iget-object v1, p0, Lmiuix/springback/view/SpringBackLayout;->l:Lw/j;

    move-object v4, p1

    move v6, p5

    invoke-virtual/range {v1 .. v6}, Lw/j;->d(II[I[II)Z

    move-result p3

    if-eqz p3, :cond_2

    aget p3, p4, v0

    aget p5, p1, v0

    add-int/2addr p3, p5

    aput p3, p4, v0

    aget p3, p4, p2

    aget p1, p1, p2

    add-int/2addr p3, p1

    aput p3, p4, p2

    :cond_2
    return-void
.end method

.method public o(Landroid/view/View;Landroid/view/View;II)Z
    .locals 4

    iget-boolean p2, p0, Lmiuix/springback/view/SpringBackLayout;->y:Z

    const/4 v0, 0x1

    if-eqz p2, :cond_5

    iput p3, p0, Lmiuix/springback/view/SpringBackLayout;->j:I

    const/4 p2, 0x2

    const/4 v1, 0x0

    if-ne p3, p2, :cond_0

    move v2, v0

    goto :goto_0

    :cond_0
    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    move p2, v0

    :goto_1
    iget v3, p0, Lmiuix/springback/view/SpringBackLayout;->q:I

    and-int/2addr p2, v3

    if-nez p2, :cond_2

    return v1

    :cond_2
    invoke-virtual {p0, p1, p1, p3}, Lmiuix/springback/view/SpringBackLayout;->onStartNestedScroll(Landroid/view/View;Landroid/view/View;I)Z

    move-result p1

    if-nez p1, :cond_3

    return v1

    :cond_3
    if-eqz v2, :cond_4

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getScrollY()I

    move-result p1

    goto :goto_2

    :cond_4
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getScrollX()I

    move-result p1

    :goto_2
    int-to-float p1, p1

    if-eqz p4, :cond_5

    const/4 p2, 0x0

    cmpl-float p1, p1, p2

    if-eqz p1, :cond_5

    iget-object p1, p0, Lmiuix/springback/view/SpringBackLayout;->B:Landroid/view/View;

    instance-of p1, p1, Landroidx/core/widget/NestedScrollView;

    if-eqz p1, :cond_5

    return v1

    :cond_5
    iget-object p1, p0, Lmiuix/springback/view/SpringBackLayout;->l:Lw/j;

    invoke-virtual {p1, p3, p4}, Lw/j;->n(II)Z

    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11

    iget-boolean v0, p0, Lmiuix/springback/view/SpringBackLayout;->y:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_3e

    iget-boolean v0, p0, Lmiuix/springback/view/SpringBackLayout;->i:Z

    if-nez v0, :cond_3e

    iget-boolean v0, p0, Lmiuix/springback/view/SpringBackLayout;->k:Z

    if-eqz v0, :cond_1

    goto/16 :goto_f

    :cond_1
    iget-object v0, p0, Lmiuix/springback/view/SpringBackLayout;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isNestedScrollingEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    return v1

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    iget-object v2, p0, Lmiuix/springback/view/SpringBackLayout;->A:Le2/c;

    iget-boolean v3, v2, Le2/c;->f:Z

    if-nez v3, :cond_3

    if-nez v0, :cond_3

    invoke-virtual {v2}, Le2/c;->b()V

    :cond_3
    invoke-virtual {p0}, Lmiuix/springback/view/SpringBackLayout;->A()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lmiuix/springback/view/SpringBackLayout;->z()Z

    move-result v0

    if-nez v0, :cond_4

    return v1

    :cond_4
    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->q:I

    and-int/lit8 v2, v0, 0x4

    const/4 v3, 0x6

    const/4 v4, -0x1

    const/4 v5, 0x3

    const/4 v6, 0x2

    const/4 v7, 0x1

    if-eqz v2, :cond_1b

    iget-object v0, p0, Lmiuix/springback/view/SpringBackLayout;->c:Le2/a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    if-eqz v2, :cond_b

    if-eq v2, v7, :cond_a

    if-eq v2, v6, :cond_5

    if-eq v2, v5, :cond_a

    goto :goto_1

    :cond_5
    iget v2, v0, Le2/a;->a:I

    if-ne v2, v4, :cond_6

    goto :goto_1

    :cond_6
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v2

    if-gez v2, :cond_7

    goto :goto_1

    :cond_7
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v8

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    iget v9, v0, Le2/a;->c:F

    sub-float/2addr v8, v9

    iget v9, v0, Le2/a;->b:F

    sub-float/2addr v2, v9

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v9

    iget v10, v0, Le2/a;->f:I

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-gtz v9, :cond_8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v9

    iget v10, v0, Le2/a;->f:I

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-lez v9, :cond_d

    :cond_8
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    cmpl-float v2, v2, v8

    if-lez v2, :cond_9

    move v2, v7

    goto :goto_0

    :cond_9
    move v2, v6

    :goto_0
    iput v2, v0, Le2/a;->d:I

    goto :goto_1

    :cond_a
    iput v1, v0, Le2/a;->d:I

    iget-object v0, v0, Le2/a;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_1

    :cond_b
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v2

    iput v2, v0, Le2/a;->a:I

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v2

    if-gez v2, :cond_c

    goto :goto_1

    :cond_c
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v8

    iput v8, v0, Le2/a;->c:F

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    iput v2, v0, Le2/a;->b:F

    iput v1, v0, Le2/a;->d:I

    :cond_d
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-eqz v0, :cond_13

    if-eq v0, v7, :cond_10

    if-eq v0, v6, :cond_f

    if-eq v0, v5, :cond_10

    if-eq v0, v3, :cond_e

    goto :goto_4

    :cond_e
    invoke-virtual {p0, p1}, Lmiuix/springback/view/SpringBackLayout;->v(Landroid/view/MotionEvent;)V

    goto :goto_4

    :cond_f
    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->w:I

    if-nez v0, :cond_17

    iget-object v0, p0, Lmiuix/springback/view/SpringBackLayout;->c:Le2/a;

    iget v0, v0, Le2/a;->d:I

    if-eqz v0, :cond_17

    iput v0, p0, Lmiuix/springback/view/SpringBackLayout;->w:I

    goto :goto_4

    :cond_10
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_11

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_11
    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->q:I

    and-int/2addr v0, v6

    if-eqz v0, :cond_12

    invoke-virtual {p0, v6}, Lmiuix/springback/view/SpringBackLayout;->y(I)V

    goto :goto_4

    :cond_12
    invoke-virtual {p0, v7}, Lmiuix/springback/view/SpringBackLayout;->y(I)V

    goto :goto_4

    :cond_13
    iget-object v0, p0, Lmiuix/springback/view/SpringBackLayout;->c:Le2/a;

    iget v2, v0, Le2/a;->c:F

    iput v2, p0, Lmiuix/springback/view/SpringBackLayout;->e:F

    iget v2, v0, Le2/a;->b:F

    iput v2, p0, Lmiuix/springback/view/SpringBackLayout;->d:F

    iget v0, v0, Le2/a;->a:I

    iput v0, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getScrollY()I

    move-result v0

    if-eqz v0, :cond_14

    iput v6, p0, Lmiuix/springback/view/SpringBackLayout;->w:I

    goto :goto_2

    :cond_14
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getScrollX()I

    move-result v0

    if-eqz v0, :cond_15

    iput v7, p0, Lmiuix/springback/view/SpringBackLayout;->w:I

    :goto_2
    invoke-virtual {p0, v7}, Lmiuix/springback/view/SpringBackLayout;->w(Z)V

    goto :goto_3

    :cond_15
    iput v1, p0, Lmiuix/springback/view/SpringBackLayout;->w:I

    :goto_3
    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->q:I

    and-int/2addr v0, v6

    if-eqz v0, :cond_16

    invoke-virtual {p0, v6}, Lmiuix/springback/view/SpringBackLayout;->a(I)V

    goto :goto_4

    :cond_16
    invoke-virtual {p0, v7}, Lmiuix/springback/view/SpringBackLayout;->a(I)V

    :cond_17
    :goto_4
    invoke-virtual {p0, v6}, Lmiuix/springback/view/SpringBackLayout;->d(I)Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_18

    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->q:I

    and-int/2addr v0, v7

    if-eqz v0, :cond_18

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getScrollX()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v0, v2

    if-nez v0, :cond_18

    return v1

    :cond_18
    invoke-virtual {p0, v7}, Lmiuix/springback/view/SpringBackLayout;->d(I)Z

    move-result v0

    if-eqz v0, :cond_19

    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->q:I

    and-int/2addr v0, v6

    if-eqz v0, :cond_19

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getScrollY()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v0, v2

    if-nez v0, :cond_19

    return v1

    :cond_19
    invoke-virtual {p0, v6}, Lmiuix/springback/view/SpringBackLayout;->d(I)Z

    move-result v0

    if-nez v0, :cond_1a

    invoke-virtual {p0, v7}, Lmiuix/springback/view/SpringBackLayout;->d(I)Z

    move-result v0

    if-eqz v0, :cond_1c

    :cond_1a
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_1c

    invoke-interface {v0, v7}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_5

    :cond_1b
    iput v0, p0, Lmiuix/springback/view/SpringBackLayout;->w:I

    :cond_1c
    :goto_5
    invoke-virtual {p0, v6}, Lmiuix/springback/view/SpringBackLayout;->d(I)Z

    move-result v0

    const-string v2, "Got ACTION_MOVE event but have an invalid active pointer id."

    const-string v8, "Got ACTION_MOVE event but don\'t have an active pointer id."

    const-string v9, "SpringBackLayout"

    if-eqz v0, :cond_2d

    invoke-virtual {p0, v6}, Lmiuix/springback/view/SpringBackLayout;->f(I)Z

    move-result v0

    if-nez v0, :cond_1d

    invoke-virtual {p0, v6}, Lmiuix/springback/view/SpringBackLayout;->e(I)Z

    move-result v0

    if-nez v0, :cond_1d

    goto/16 :goto_a

    :cond_1d
    invoke-virtual {p0, v6}, Lmiuix/springback/view/SpringBackLayout;->f(I)Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-virtual {p0}, Lmiuix/springback/view/SpringBackLayout;->A()Z

    move-result v0

    if-nez v0, :cond_1e

    goto/16 :goto_a

    :cond_1e
    invoke-virtual {p0, v6}, Lmiuix/springback/view/SpringBackLayout;->e(I)Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-virtual {p0}, Lmiuix/springback/view/SpringBackLayout;->z()Z

    move-result v0

    if-nez v0, :cond_1f

    goto/16 :goto_a

    :cond_1f
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-eqz v0, :cond_29

    if-eq v0, v7, :cond_28

    if-eq v0, v6, :cond_21

    if-eq v0, v5, :cond_28

    if-eq v0, v3, :cond_20

    goto/16 :goto_9

    :cond_20
    invoke-virtual {p0, p1}, Lmiuix/springback/view/SpringBackLayout;->v(Landroid/view/MotionEvent;)V

    goto/16 :goto_9

    :cond_21
    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    if-ne v0, v4, :cond_22

    move-object v2, v8

    goto :goto_6

    :cond_22
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    if-gez v0, :cond_23

    :goto_6
    invoke-static {v9, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_a

    :cond_23
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result p1

    invoke-virtual {p0, v6}, Lmiuix/springback/view/SpringBackLayout;->e(I)Z

    move-result v0

    if-eqz v0, :cond_24

    invoke-virtual {p0, v6}, Lmiuix/springback/view/SpringBackLayout;->f(I)Z

    move-result v0

    if-eqz v0, :cond_24

    move v1, v7

    :cond_24
    if-nez v1, :cond_25

    invoke-virtual {p0, v6}, Lmiuix/springback/view/SpringBackLayout;->f(I)Z

    move-result v0

    if-nez v0, :cond_26

    :cond_25
    if-eqz v1, :cond_27

    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->e:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_27

    :cond_26
    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->e:F

    sub-float v0, p1, v0

    iget v1, p0, Lmiuix/springback/view/SpringBackLayout;->G:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2c

    iget-boolean v0, p0, Lmiuix/springback/view/SpringBackLayout;->h:Z

    if-nez v0, :cond_2c

    goto :goto_7

    :cond_27
    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->e:F

    sub-float/2addr v0, p1

    iget v1, p0, Lmiuix/springback/view/SpringBackLayout;->G:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2c

    iget-boolean v0, p0, Lmiuix/springback/view/SpringBackLayout;->h:Z

    if-nez v0, :cond_2c

    :goto_7
    iput-boolean v7, p0, Lmiuix/springback/view/SpringBackLayout;->h:Z

    invoke-virtual {p0, v7}, Lmiuix/springback/view/SpringBackLayout;->c(I)V

    goto :goto_8

    :cond_28
    iput-boolean v1, p0, Lmiuix/springback/view/SpringBackLayout;->h:Z

    iput v4, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    goto :goto_9

    :cond_29
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    if-gez v0, :cond_2a

    goto :goto_a

    :cond_2a
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result p1

    iput p1, p0, Lmiuix/springback/view/SpringBackLayout;->e:F

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getScrollY()I

    move-result p1

    if-eqz p1, :cond_2b

    iput-boolean v7, p0, Lmiuix/springback/view/SpringBackLayout;->h:Z

    iget p1, p0, Lmiuix/springback/view/SpringBackLayout;->e:F

    :goto_8
    iput p1, p0, Lmiuix/springback/view/SpringBackLayout;->g:F

    goto :goto_9

    :cond_2b
    iput-boolean v1, p0, Lmiuix/springback/view/SpringBackLayout;->h:Z

    :cond_2c
    :goto_9
    iget-boolean v1, p0, Lmiuix/springback/view/SpringBackLayout;->h:Z

    :goto_a
    return v1

    :cond_2d
    invoke-virtual {p0, v7}, Lmiuix/springback/view/SpringBackLayout;->d(I)Z

    move-result v0

    if-eqz v0, :cond_3e

    invoke-virtual {p0, v7}, Lmiuix/springback/view/SpringBackLayout;->f(I)Z

    move-result v0

    if-nez v0, :cond_2e

    invoke-virtual {p0, v7}, Lmiuix/springback/view/SpringBackLayout;->e(I)Z

    move-result v0

    if-nez v0, :cond_2e

    goto/16 :goto_f

    :cond_2e
    invoke-virtual {p0, v7}, Lmiuix/springback/view/SpringBackLayout;->f(I)Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-virtual {p0}, Lmiuix/springback/view/SpringBackLayout;->A()Z

    move-result v0

    if-nez v0, :cond_2f

    goto/16 :goto_f

    :cond_2f
    invoke-virtual {p0, v7}, Lmiuix/springback/view/SpringBackLayout;->e(I)Z

    move-result v0

    if-eqz v0, :cond_30

    invoke-virtual {p0}, Lmiuix/springback/view/SpringBackLayout;->z()Z

    move-result v0

    if-nez v0, :cond_30

    goto/16 :goto_f

    :cond_30
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-eqz v0, :cond_3a

    if-eq v0, v7, :cond_39

    if-eq v0, v6, :cond_32

    if-eq v0, v5, :cond_39

    if-eq v0, v3, :cond_31

    goto/16 :goto_e

    :cond_31
    invoke-virtual {p0, p1}, Lmiuix/springback/view/SpringBackLayout;->v(Landroid/view/MotionEvent;)V

    goto/16 :goto_e

    :cond_32
    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    if-ne v0, v4, :cond_33

    move-object v2, v8

    goto :goto_b

    :cond_33
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    if-gez v0, :cond_34

    :goto_b
    invoke-static {v9, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_f

    :cond_34
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result p1

    invoke-virtual {p0, v7}, Lmiuix/springback/view/SpringBackLayout;->e(I)Z

    move-result v0

    if-eqz v0, :cond_35

    invoke-virtual {p0, v7}, Lmiuix/springback/view/SpringBackLayout;->f(I)Z

    move-result v0

    if-eqz v0, :cond_35

    move v1, v7

    :cond_35
    if-nez v1, :cond_36

    invoke-virtual {p0, v7}, Lmiuix/springback/view/SpringBackLayout;->f(I)Z

    move-result v0

    if-nez v0, :cond_37

    :cond_36
    if-eqz v1, :cond_38

    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->d:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_38

    :cond_37
    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->d:F

    sub-float v0, p1, v0

    iget v1, p0, Lmiuix/springback/view/SpringBackLayout;->G:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3d

    iget-boolean v0, p0, Lmiuix/springback/view/SpringBackLayout;->h:Z

    if-nez v0, :cond_3d

    goto :goto_c

    :cond_38
    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->d:F

    sub-float/2addr v0, p1

    iget v1, p0, Lmiuix/springback/view/SpringBackLayout;->G:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3d

    iget-boolean v0, p0, Lmiuix/springback/view/SpringBackLayout;->h:Z

    if-nez v0, :cond_3d

    :goto_c
    iput-boolean v7, p0, Lmiuix/springback/view/SpringBackLayout;->h:Z

    invoke-virtual {p0, v7}, Lmiuix/springback/view/SpringBackLayout;->c(I)V

    goto :goto_d

    :cond_39
    iput-boolean v1, p0, Lmiuix/springback/view/SpringBackLayout;->h:Z

    iput v4, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    goto :goto_e

    :cond_3a
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    if-gez v0, :cond_3b

    goto :goto_f

    :cond_3b
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result p1

    iput p1, p0, Lmiuix/springback/view/SpringBackLayout;->d:F

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getScrollX()I

    move-result p1

    if-eqz p1, :cond_3c

    iput-boolean v7, p0, Lmiuix/springback/view/SpringBackLayout;->h:Z

    iget p1, p0, Lmiuix/springback/view/SpringBackLayout;->d:F

    :goto_d
    iput p1, p0, Lmiuix/springback/view/SpringBackLayout;->f:F

    goto :goto_e

    :cond_3c
    iput-boolean v1, p0, Lmiuix/springback/view/SpringBackLayout;->h:Z

    :cond_3d
    :goto_e
    iget-boolean v1, p0, Lmiuix/springback/view/SpringBackLayout;->h:Z

    :cond_3e
    :goto_f
    return v1
.end method

.method public onLayout(ZIIII)V
    .locals 4

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result p1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result p2

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result p3

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result p4

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result p5

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v2

    iget-object v3, p0, Lmiuix/springback/view/SpringBackLayout;->B:Landroid/view/View;

    sub-int/2addr p1, p5

    sub-int/2addr p1, v0

    add-int/2addr p1, p3

    sub-int/2addr p2, v1

    sub-int/2addr p2, v2

    add-int/2addr p2, p4

    invoke-virtual {v3, p3, p4, p1, p2}, Landroid/view/View;->layout(IIII)V

    return-void
.end method

.method public onMeasure(II)V
    .locals 5

    iget-object v0, p0, Lmiuix/springback/view/SpringBackLayout;->B:Landroid/view/View;

    if-nez v0, :cond_1

    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->C:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lmiuix/springback/view/SpringBackLayout;->B:Landroid/view/View;

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "invalid target Id"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    iget-object v0, p0, Lmiuix/springback/view/SpringBackLayout;->B:Landroid/view/View;

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Landroid/view/ViewGroup;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmiuix/springback/view/SpringBackLayout;->B:Landroid/view/View;

    instance-of v1, v0, Lw/i;

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->isNestedScrollingEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lmiuix/springback/view/SpringBackLayout;->B:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setNestedScrollingEnabled(Z)V

    :cond_2
    iget-object v0, p0, Lmiuix/springback/view/SpringBackLayout;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getOverScrollMode()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lmiuix/springback/view/SpringBackLayout;->B:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOverScrollMode(I)V

    :cond_3
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    iget-object v4, p0, Lmiuix/springback/view/SpringBackLayout;->B:Landroid/view/View;

    invoke-virtual {p0, v4, p1, p2}, Landroid/view/ViewGroup;->measureChild(Landroid/view/View;II)V

    iget-object p1, p0, Lmiuix/springback/view/SpringBackLayout;->B:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result p1

    if-le v2, p1, :cond_4

    iget-object p1, p0, Lmiuix/springback/view/SpringBackLayout;->B:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    :cond_4
    iget-object p1, p0, Lmiuix/springback/view/SpringBackLayout;->B:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p1

    if-le v3, p1, :cond_5

    iget-object p1, p0, Lmiuix/springback/view/SpringBackLayout;->B:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    :cond_5
    const/high16 p1, 0x40000000    # 2.0f

    if-ne v0, p1, :cond_6

    goto :goto_1

    :cond_6
    iget-object p2, p0, Lmiuix/springback/view/SpringBackLayout;->B:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    :goto_1
    if-ne v1, p1, :cond_7

    goto :goto_2

    :cond_7
    iget-object p1, p0, Lmiuix/springback/view/SpringBackLayout;->B:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    :goto_2
    invoke-virtual {p0, v2, v3}, Landroid/view/ViewGroup;->setMeasuredDimension(II)V

    return-void

    :cond_8
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "fail to get target"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onNestedFling(Landroid/view/View;FFZ)Z
    .locals 0

    invoke-virtual {p0, p2, p3, p4}, Lmiuix/springback/view/SpringBackLayout;->dispatchNestedFling(FFZ)Z

    move-result p1

    return p1
.end method

.method public onNestedPreFling(Landroid/view/View;FF)Z
    .locals 0

    invoke-virtual {p0, p2, p3}, Lmiuix/springback/view/SpringBackLayout;->dispatchNestedPreFling(FF)Z

    move-result p1

    return p1
.end method

.method public onNestedScroll(Landroid/view/View;IIII)V
    .locals 8

    iget-object v7, p0, Lmiuix/springback/view/SpringBackLayout;->n:[I

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v7}, Lmiuix/springback/view/SpringBackLayout;->k(Landroid/view/View;IIIII[I)V

    return-void
.end method

.method public onNestedScrollAccepted(Landroid/view/View;Landroid/view/View;I)V
    .locals 0

    iget-object p1, p0, Lmiuix/springback/view/SpringBackLayout;->m:Lw/m;

    iput p3, p1, Lw/m;->a:I

    and-int/lit8 p1, p3, 0x2

    invoke-virtual {p0, p1}, Lmiuix/springback/view/SpringBackLayout;->startNestedScroll(I)Z

    return-void
.end method

.method public onScrollChanged(IIII)V
    .locals 4

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onScrollChanged(IIII)V

    iget-object v0, p0, Lmiuix/springback/view/SpringBackLayout;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/springback/view/SpringBackLayout$a;

    sub-int v2, p1, p3

    sub-int v3, p2, p4

    invoke-interface {v1, p0, v2, v3}, Lmiuix/springback/view/SpringBackLayout$a;->a(Lmiuix/springback/view/SpringBackLayout;II)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onStartNestedScroll(Landroid/view/View;Landroid/view/View;I)Z
    .locals 0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->isEnabled()Z

    move-result p1

    return p1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->isEnabled()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_a

    iget-boolean v1, p0, Lmiuix/springback/view/SpringBackLayout;->i:Z

    if-nez v1, :cond_a

    iget-boolean v1, p0, Lmiuix/springback/view/SpringBackLayout;->k:Z

    if-eqz v1, :cond_0

    goto/16 :goto_1

    :cond_0
    iget-object v1, p0, Lmiuix/springback/view/SpringBackLayout;->B:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->isNestedScrollingEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    return v2

    :cond_1
    iget-object v1, p0, Lmiuix/springback/view/SpringBackLayout;->A:Le2/c;

    iget-boolean v3, v1, Le2/c;->f:Z

    if-nez v3, :cond_2

    if-nez v0, :cond_2

    invoke-virtual {v1}, Le2/c;->b()V

    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lmiuix/springback/view/SpringBackLayout;->d(I)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    invoke-virtual {p0, v0}, Lmiuix/springback/view/SpringBackLayout;->f(I)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {p0, v0}, Lmiuix/springback/view/SpringBackLayout;->e(I)Z

    move-result v3

    if-nez v3, :cond_3

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v0}, Lmiuix/springback/view/SpringBackLayout;->f(I)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0, v0}, Lmiuix/springback/view/SpringBackLayout;->e(I)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0, p1, v1, v0}, Lmiuix/springback/view/SpringBackLayout;->t(Landroid/view/MotionEvent;II)Z

    move-result v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0, v0}, Lmiuix/springback/view/SpringBackLayout;->e(I)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0, p1, v1, v0}, Lmiuix/springback/view/SpringBackLayout;->u(Landroid/view/MotionEvent;II)Z

    move-result v2

    goto :goto_0

    :cond_5
    invoke-virtual {p0, p1, v1, v0}, Lmiuix/springback/view/SpringBackLayout;->s(Landroid/view/MotionEvent;II)Z

    move-result v2

    :goto_0
    return v2

    :cond_6
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmiuix/springback/view/SpringBackLayout;->d(I)Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    invoke-virtual {p0, v0}, Lmiuix/springback/view/SpringBackLayout;->f(I)Z

    move-result v3

    if-nez v3, :cond_7

    invoke-virtual {p0, v0}, Lmiuix/springback/view/SpringBackLayout;->e(I)Z

    move-result v3

    if-nez v3, :cond_7

    goto :goto_1

    :cond_7
    invoke-virtual {p0, v0}, Lmiuix/springback/view/SpringBackLayout;->f(I)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {p0, v0}, Lmiuix/springback/view/SpringBackLayout;->e(I)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {p0, p1, v1, v0}, Lmiuix/springback/view/SpringBackLayout;->t(Landroid/view/MotionEvent;II)Z

    move-result v2

    goto :goto_1

    :cond_8
    invoke-virtual {p0, v0}, Lmiuix/springback/view/SpringBackLayout;->e(I)Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {p0, p1, v1, v0}, Lmiuix/springback/view/SpringBackLayout;->u(Landroid/view/MotionEvent;II)Z

    move-result v2

    goto :goto_1

    :cond_9
    invoke-virtual {p0, p1, v1, v0}, Lmiuix/springback/view/SpringBackLayout;->s(Landroid/view/MotionEvent;II)Z

    move-result v2

    :cond_a
    :goto_1
    return v2
.end method

.method public final p(FI)F
    .locals 1

    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->t:I

    goto :goto_0

    :cond_0
    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->u:I

    :goto_0
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    int-to-float v0, v0

    div-float/2addr p1, v0

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p1, v0}, Ljava/lang/Math;->min(FF)F

    move-result p1

    invoke-virtual {p0, p1, p2}, Lmiuix/springback/view/SpringBackLayout;->h(FI)F

    move-result p1

    return p1
.end method

.method public final q(FFI)F
    .locals 6

    const/4 v0, 0x2

    if-ne p3, v0, :cond_0

    iget p3, p0, Lmiuix/springback/view/SpringBackLayout;->t:I

    goto :goto_0

    :cond_0
    iget p3, p0, Lmiuix/springback/view/SpringBackLayout;->u:I

    :goto_0
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    goto :goto_1

    :cond_1
    move p1, p2

    :goto_1
    int-to-double v0, p3

    const-wide v2, 0x3fe5555555555555L    # 0.6666666666666666

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    int-to-float p2, p3

    const/high16 p3, 0x40400000    # 3.0f

    mul-float/2addr p1, p3

    sub-float/2addr p2, p1

    float-to-double p1, p2

    const-wide v4, 0x3fd5555555555555L    # 0.3333333333333333

    invoke-static {p1, p2, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide p1

    mul-double/2addr p1, v2

    sub-double/2addr v0, p1

    double-to-float p1, v0

    return p1
.end method

.method public final r(I[II)V
    .locals 5

    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->j:I

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v0, v1, :cond_0

    move v0, v3

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move v1, v3

    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getScrollY()I

    move-result v0

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getScrollX()I

    move-result v0

    :goto_2
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/4 v4, 0x0

    if-nez p3, :cond_6

    if-lez p1, :cond_4

    iget p3, p0, Lmiuix/springback/view/SpringBackLayout;->F:F

    cmpl-float v0, p3, v4

    if-lez v0, :cond_4

    int-to-float v0, p1

    cmpl-float v2, v0, p3

    if-lez v2, :cond_3

    float-to-int p1, p3

    invoke-virtual {p0, p1, p2, v1}, Lmiuix/springback/view/SpringBackLayout;->b(I[II)V

    iput v4, p0, Lmiuix/springback/view/SpringBackLayout;->F:F

    goto :goto_3

    :cond_3
    sub-float/2addr p3, v0

    iput p3, p0, Lmiuix/springback/view/SpringBackLayout;->F:F

    invoke-virtual {p0, p1, p2, v1}, Lmiuix/springback/view/SpringBackLayout;->b(I[II)V

    :goto_3
    invoke-virtual {p0, v3}, Lmiuix/springback/view/SpringBackLayout;->c(I)V

    iget p1, p0, Lmiuix/springback/view/SpringBackLayout;->F:F

    invoke-virtual {p0, p1, v1}, Lmiuix/springback/view/SpringBackLayout;->p(FI)F

    move-result p1

    :goto_4
    invoke-virtual {p0, p1, v1}, Lmiuix/springback/view/SpringBackLayout;->g(FI)V

    goto/16 :goto_7

    :cond_4
    if-gez p1, :cond_e

    iget p3, p0, Lmiuix/springback/view/SpringBackLayout;->E:F

    neg-float v0, p3

    cmpg-float v2, v0, v4

    if-gez v2, :cond_e

    int-to-float v2, p1

    cmpg-float v0, v2, v0

    if-gez v0, :cond_5

    float-to-int p1, p3

    invoke-virtual {p0, p1, p2, v1}, Lmiuix/springback/view/SpringBackLayout;->b(I[II)V

    iput v4, p0, Lmiuix/springback/view/SpringBackLayout;->E:F

    goto :goto_5

    :cond_5
    add-float/2addr p3, v2

    iput p3, p0, Lmiuix/springback/view/SpringBackLayout;->E:F

    invoke-virtual {p0, p1, p2, v1}, Lmiuix/springback/view/SpringBackLayout;->b(I[II)V

    :goto_5
    invoke-virtual {p0, v3}, Lmiuix/springback/view/SpringBackLayout;->c(I)V

    iget p1, p0, Lmiuix/springback/view/SpringBackLayout;->E:F

    invoke-virtual {p0, p1, v1}, Lmiuix/springback/view/SpringBackLayout;->p(FI)F

    move-result p1

    neg-float p1, p1

    goto :goto_4

    :cond_6
    if-lez p1, :cond_9

    iget p3, p0, Lmiuix/springback/view/SpringBackLayout;->F:F

    cmpl-float p3, p3, v4

    if-lez p3, :cond_9

    iget-boolean p3, p0, Lmiuix/springback/view/SpringBackLayout;->v:Z

    if-nez p3, :cond_7

    iput-boolean v3, p0, Lmiuix/springback/view/SpringBackLayout;->v:Z

    invoke-virtual {p0, v4, v1, v2}, Lmiuix/springback/view/SpringBackLayout;->x(FIZ)V

    :cond_7
    iget-object p3, p0, Lmiuix/springback/view/SpringBackLayout;->A:Le2/c;

    invoke-virtual {p3}, Le2/c;->a()Z

    move-result p3

    if-eqz p3, :cond_8

    iget-object p3, p0, Lmiuix/springback/view/SpringBackLayout;->A:Le2/c;

    iget-wide v2, p3, Le2/c;->a:D

    double-to-int v2, v2

    iget-wide v3, p3, Le2/c;->b:D

    double-to-int p3, v3

    invoke-virtual {p0, v2, p3}, Landroid/view/ViewGroup;->scrollTo(II)V

    int-to-float p3, v0

    invoke-virtual {p0, v1}, Lmiuix/springback/view/SpringBackLayout;->j(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-virtual {p0, p3, v0, v1}, Lmiuix/springback/view/SpringBackLayout;->q(FFI)F

    move-result p3

    iput p3, p0, Lmiuix/springback/view/SpringBackLayout;->F:F

    goto :goto_6

    :cond_8
    iput v4, p0, Lmiuix/springback/view/SpringBackLayout;->F:F

    goto :goto_6

    :cond_9
    if-gez p1, :cond_c

    iget p3, p0, Lmiuix/springback/view/SpringBackLayout;->E:F

    neg-float p3, p3

    cmpg-float p3, p3, v4

    if-gez p3, :cond_c

    iget-boolean p3, p0, Lmiuix/springback/view/SpringBackLayout;->v:Z

    if-nez p3, :cond_a

    iput-boolean v3, p0, Lmiuix/springback/view/SpringBackLayout;->v:Z

    invoke-virtual {p0, v4, v1, v2}, Lmiuix/springback/view/SpringBackLayout;->x(FIZ)V

    :cond_a
    iget-object p3, p0, Lmiuix/springback/view/SpringBackLayout;->A:Le2/c;

    invoke-virtual {p3}, Le2/c;->a()Z

    move-result p3

    if-eqz p3, :cond_b

    iget-object p3, p0, Lmiuix/springback/view/SpringBackLayout;->A:Le2/c;

    iget-wide v2, p3, Le2/c;->a:D

    double-to-int v2, v2

    iget-wide v3, p3, Le2/c;->b:D

    double-to-int p3, v3

    invoke-virtual {p0, v2, p3}, Landroid/view/ViewGroup;->scrollTo(II)V

    int-to-float p3, v0

    invoke-virtual {p0, v1}, Lmiuix/springback/view/SpringBackLayout;->j(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-virtual {p0, p3, v0, v1}, Lmiuix/springback/view/SpringBackLayout;->q(FFI)F

    move-result p3

    iput p3, p0, Lmiuix/springback/view/SpringBackLayout;->E:F

    goto :goto_6

    :cond_b
    iput v4, p0, Lmiuix/springback/view/SpringBackLayout;->E:F

    goto :goto_6

    :cond_c
    if-eqz p1, :cond_e

    iget p3, p0, Lmiuix/springback/view/SpringBackLayout;->E:F

    cmpl-float p3, p3, v4

    if-eqz p3, :cond_d

    iget p3, p0, Lmiuix/springback/view/SpringBackLayout;->F:F

    cmpl-float p3, p3, v4

    if-nez p3, :cond_e

    :cond_d
    iget-boolean p3, p0, Lmiuix/springback/view/SpringBackLayout;->v:Z

    if-eqz p3, :cond_e

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getScrollY()I

    move-result p3

    if-nez p3, :cond_e

    :goto_6
    invoke-virtual {p0, p1, p2, v1}, Lmiuix/springback/view/SpringBackLayout;->b(I[II)V

    :cond_e
    :goto_7
    return-void
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lmiuix/springback/view/SpringBackLayout;->y:Z

    if-nez v0, :cond_1

    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    :cond_1
    return-void
.end method

.method public final s(Landroid/view/MotionEvent;II)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p2, :cond_d

    const/4 v2, 0x0

    const-string v3, "SpringBackLayout"

    if-eq p2, v0, :cond_a

    const/4 v4, 0x2

    if-eq p2, v4, :cond_6

    const/4 v5, 0x3

    if-eq p2, v5, :cond_a

    const/4 v2, 0x5

    if-eq p2, v2, :cond_1

    const/4 p3, 0x6

    if-eq p2, p3, :cond_0

    goto/16 :goto_2

    :cond_0
    invoke-virtual {p0, p1}, Lmiuix/springback/view/SpringBackLayout;->v(Landroid/view/MotionEvent;)V

    goto/16 :goto_2

    :cond_1
    iget p2, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result p2

    if-gez p2, :cond_2

    const-string p1, "Got ACTION_POINTER_DOWN event but have an invalid active pointer id."

    invoke-static {v3, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_2
    const-string v2, "Got ACTION_POINTER_DOWN event but have an invalid action index."

    if-ne p3, v4, :cond_4

    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getY(I)F

    move-result p2

    iget p3, p0, Lmiuix/springback/view/SpringBackLayout;->e:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v4

    if-gez v4, :cond_3

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_3
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    sub-float/2addr p2, p3

    sub-float/2addr v1, p2

    iput v1, p0, Lmiuix/springback/view/SpringBackLayout;->e:F

    iput v1, p0, Lmiuix/springback/view/SpringBackLayout;->g:F

    goto :goto_0

    :cond_4
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getX(I)F

    move-result p2

    iget p3, p0, Lmiuix/springback/view/SpringBackLayout;->d:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v4

    if-gez v4, :cond_5

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_5
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    sub-float/2addr p2, p3

    sub-float/2addr v1, p2

    iput v1, p0, Lmiuix/springback/view/SpringBackLayout;->d:F

    iput v1, p0, Lmiuix/springback/view/SpringBackLayout;->f:F

    :goto_0
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result p1

    iput p1, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    goto :goto_2

    :cond_6
    iget p2, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result p2

    if-gez p2, :cond_7

    const-string p1, "Got ACTION_MOVE event but have an invalid active pointer id."

    invoke-static {v3, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_7
    iget-boolean v3, p0, Lmiuix/springback/view/SpringBackLayout;->h:Z

    if-eqz v3, :cond_e

    if-ne p3, v4, :cond_8

    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getY(I)F

    move-result p1

    iget p2, p0, Lmiuix/springback/view/SpringBackLayout;->g:F

    sub-float p2, p1, p2

    invoke-static {p2}, Ljava/lang/Math;->signum(F)F

    move-result p2

    iget v3, p0, Lmiuix/springback/view/SpringBackLayout;->g:F

    goto :goto_1

    :cond_8
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getX(I)F

    move-result p1

    iget p2, p0, Lmiuix/springback/view/SpringBackLayout;->f:F

    sub-float p2, p1, p2

    invoke-static {p2}, Ljava/lang/Math;->signum(F)F

    move-result p2

    iget v3, p0, Lmiuix/springback/view/SpringBackLayout;->f:F

    :goto_1
    sub-float/2addr p1, v3

    invoke-virtual {p0, p1, p3}, Lmiuix/springback/view/SpringBackLayout;->p(FI)F

    move-result p1

    mul-float/2addr p2, p1

    cmpl-float p1, p2, v2

    if-lez p1, :cond_9

    invoke-virtual {p0, v0}, Lmiuix/springback/view/SpringBackLayout;->w(Z)V

    invoke-virtual {p0, p2, p3}, Lmiuix/springback/view/SpringBackLayout;->g(FI)V

    goto :goto_2

    :cond_9
    invoke-virtual {p0, v2, p3}, Lmiuix/springback/view/SpringBackLayout;->g(FI)V

    return v1

    :cond_a
    iget p2, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result p1

    if-gez p1, :cond_b

    const-string p1, "Got ACTION_UP event but don\'t have an active pointer id."

    invoke-static {v3, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_b
    iget-boolean p1, p0, Lmiuix/springback/view/SpringBackLayout;->h:Z

    if-eqz p1, :cond_c

    iput-boolean v1, p0, Lmiuix/springback/view/SpringBackLayout;->h:Z

    invoke-virtual {p0, v2, p3, v0}, Lmiuix/springback/view/SpringBackLayout;->x(FIZ)V

    :cond_c
    const/4 p1, -0x1

    iput p1, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    return v1

    :cond_d
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result p1

    iput p1, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    invoke-virtual {p0, p3}, Lmiuix/springback/view/SpringBackLayout;->a(I)V

    :cond_e
    :goto_2
    return v0
.end method

.method public setEnabled(Z)V
    .locals 2

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setEnabled(Z)V

    iget-object v0, p0, Lmiuix/springback/view/SpringBackLayout;->B:Landroid/view/View;

    if-eqz v0, :cond_0

    instance-of v1, v0, Lw/i;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isNestedScrollingEnabled()Z

    move-result v0

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lmiuix/springback/view/SpringBackLayout;->B:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setNestedScrollingEnabled(Z)V

    :cond_0
    return-void
.end method

.method public setNestedScrollingEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/springback/view/SpringBackLayout;->l:Lw/j;

    invoke-virtual {v0, p1}, Lw/j;->l(Z)V

    return-void
.end method

.method public setOnSpringListener(Lmiuix/springback/view/SpringBackLayout$b;)V
    .locals 0

    iput-object p1, p0, Lmiuix/springback/view/SpringBackLayout;->p:Lmiuix/springback/view/SpringBackLayout$b;

    return-void
.end method

.method public setScrollOrientation(I)V
    .locals 0

    iput p1, p0, Lmiuix/springback/view/SpringBackLayout;->q:I

    iget-object p1, p0, Lmiuix/springback/view/SpringBackLayout;->c:Le2/a;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setSpringBackEnable(Z)V
    .locals 0

    iput-boolean p1, p0, Lmiuix/springback/view/SpringBackLayout;->y:Z

    return-void
.end method

.method public setSpringBackMode(I)V
    .locals 0

    iput p1, p0, Lmiuix/springback/view/SpringBackLayout;->z:I

    return-void
.end method

.method public setTarget(Landroid/view/View;)V
    .locals 1

    iput-object p1, p0, Lmiuix/springback/view/SpringBackLayout;->B:Landroid/view/View;

    instance-of v0, p1, Lw/i;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->isNestedScrollingEnabled()Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lmiuix/springback/view/SpringBackLayout;->B:Landroid/view/View;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setNestedScrollingEnabled(Z)V

    :cond_0
    return-void
.end method

.method public startNestedScroll(I)Z
    .locals 1

    iget-object v0, p0, Lmiuix/springback/view/SpringBackLayout;->l:Lw/j;

    invoke-virtual {v0, p1}, Lw/j;->m(I)Z

    move-result p1

    return p1
.end method

.method public stopNestedScroll()V
    .locals 1

    iget-object v0, p0, Lmiuix/springback/view/SpringBackLayout;->l:Lw/j;

    invoke-virtual {v0}, Lw/j;->o()V

    return-void
.end method

.method public final t(Landroid/view/MotionEvent;II)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p2, :cond_d

    const-string v2, "SpringBackLayout"

    if-eq p2, v0, :cond_a

    const/4 v3, 0x2

    if-eq p2, v3, :cond_7

    const/4 v4, 0x3

    if-eq p2, v4, :cond_6

    const/4 v4, 0x5

    if-eq p2, v4, :cond_1

    const/4 p3, 0x6

    if-eq p2, p3, :cond_0

    goto/16 :goto_2

    :cond_0
    invoke-virtual {p0, p1}, Lmiuix/springback/view/SpringBackLayout;->v(Landroid/view/MotionEvent;)V

    goto/16 :goto_2

    :cond_1
    iget p2, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result p2

    if-gez p2, :cond_2

    const-string p1, "Got ACTION_POINTER_DOWN event but have an invalid active pointer id."

    invoke-static {v2, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_2
    const-string v4, "Got ACTION_POINTER_DOWN event but have an invalid action index."

    if-ne p3, v3, :cond_4

    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getY(I)F

    move-result p2

    iget p3, p0, Lmiuix/springback/view/SpringBackLayout;->e:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v3

    if-gez v3, :cond_3

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_3
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    sub-float/2addr p2, p3

    sub-float/2addr v1, p2

    iput v1, p0, Lmiuix/springback/view/SpringBackLayout;->e:F

    iput v1, p0, Lmiuix/springback/view/SpringBackLayout;->g:F

    goto :goto_0

    :cond_4
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getX(I)F

    move-result p2

    iget p3, p0, Lmiuix/springback/view/SpringBackLayout;->d:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v3

    if-gez v3, :cond_5

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_5
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    sub-float/2addr p2, p3

    sub-float/2addr v1, p2

    iput v1, p0, Lmiuix/springback/view/SpringBackLayout;->d:F

    iput v1, p0, Lmiuix/springback/view/SpringBackLayout;->f:F

    :goto_0
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result p1

    iput p1, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    goto :goto_2

    :cond_6
    return v1

    :cond_7
    iget p2, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result p2

    if-gez p2, :cond_8

    const-string p1, "Got ACTION_MOVE event but have an invalid active pointer id."

    invoke-static {v2, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_8
    iget-boolean v1, p0, Lmiuix/springback/view/SpringBackLayout;->h:Z

    if-eqz v1, :cond_e

    if-ne p3, v3, :cond_9

    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getY(I)F

    move-result p1

    iget p2, p0, Lmiuix/springback/view/SpringBackLayout;->g:F

    sub-float p2, p1, p2

    invoke-static {p2}, Ljava/lang/Math;->signum(F)F

    move-result p2

    iget v1, p0, Lmiuix/springback/view/SpringBackLayout;->g:F

    goto :goto_1

    :cond_9
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getX(I)F

    move-result p1

    iget p2, p0, Lmiuix/springback/view/SpringBackLayout;->f:F

    sub-float p2, p1, p2

    invoke-static {p2}, Ljava/lang/Math;->signum(F)F

    move-result p2

    iget v1, p0, Lmiuix/springback/view/SpringBackLayout;->f:F

    :goto_1
    sub-float/2addr p1, v1

    invoke-virtual {p0, p1, p3}, Lmiuix/springback/view/SpringBackLayout;->p(FI)F

    move-result p1

    invoke-virtual {p0, v0}, Lmiuix/springback/view/SpringBackLayout;->w(Z)V

    mul-float/2addr p2, p1

    invoke-virtual {p0, p2, p3}, Lmiuix/springback/view/SpringBackLayout;->g(FI)V

    goto :goto_2

    :cond_a
    iget p2, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result p1

    if-gez p1, :cond_b

    const-string p1, "Got ACTION_UP event but don\'t have an active pointer id."

    invoke-static {v2, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_b
    iget-boolean p1, p0, Lmiuix/springback/view/SpringBackLayout;->h:Z

    if-eqz p1, :cond_c

    iput-boolean v1, p0, Lmiuix/springback/view/SpringBackLayout;->h:Z

    const/4 p1, 0x0

    invoke-virtual {p0, p1, p3, v0}, Lmiuix/springback/view/SpringBackLayout;->x(FIZ)V

    :cond_c
    const/4 p1, -0x1

    iput p1, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    return v1

    :cond_d
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result p1

    iput p1, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    invoke-virtual {p0, p3}, Lmiuix/springback/view/SpringBackLayout;->a(I)V

    :cond_e
    :goto_2
    return v0
.end method

.method public final u(Landroid/view/MotionEvent;II)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p2, :cond_d

    const/4 v2, 0x0

    const-string v3, "SpringBackLayout"

    if-eq p2, v0, :cond_a

    const/4 v4, 0x2

    if-eq p2, v4, :cond_6

    const/4 v5, 0x3

    if-eq p2, v5, :cond_a

    const/4 v2, 0x5

    if-eq p2, v2, :cond_1

    const/4 p3, 0x6

    if-eq p2, p3, :cond_0

    goto/16 :goto_2

    :cond_0
    invoke-virtual {p0, p1}, Lmiuix/springback/view/SpringBackLayout;->v(Landroid/view/MotionEvent;)V

    goto/16 :goto_2

    :cond_1
    iget p2, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result p2

    if-gez p2, :cond_2

    const-string p1, "Got ACTION_POINTER_DOWN event but have an invalid active pointer id."

    invoke-static {v3, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_2
    const-string v2, "Got ACTION_POINTER_DOWN event but have an invalid action index."

    if-ne p3, v4, :cond_4

    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getY(I)F

    move-result p2

    iget p3, p0, Lmiuix/springback/view/SpringBackLayout;->e:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v4

    if-gez v4, :cond_3

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_3
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    sub-float/2addr p2, p3

    sub-float/2addr v1, p2

    iput v1, p0, Lmiuix/springback/view/SpringBackLayout;->e:F

    iput v1, p0, Lmiuix/springback/view/SpringBackLayout;->g:F

    goto :goto_0

    :cond_4
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getX(I)F

    move-result p2

    iget p3, p0, Lmiuix/springback/view/SpringBackLayout;->d:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v4

    if-gez v4, :cond_5

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_5
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    sub-float/2addr p2, p3

    sub-float/2addr v1, p2

    iput v1, p0, Lmiuix/springback/view/SpringBackLayout;->d:F

    iput v1, p0, Lmiuix/springback/view/SpringBackLayout;->f:F

    :goto_0
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result p1

    iput p1, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    goto :goto_2

    :cond_6
    iget p2, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result p2

    if-gez p2, :cond_7

    const-string p1, "Got ACTION_MOVE event but have an invalid active pointer id."

    invoke-static {v3, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_7
    iget-boolean v3, p0, Lmiuix/springback/view/SpringBackLayout;->h:Z

    if-eqz v3, :cond_e

    if-ne p3, v4, :cond_8

    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getY(I)F

    move-result p1

    iget p2, p0, Lmiuix/springback/view/SpringBackLayout;->g:F

    sub-float/2addr p2, p1

    invoke-static {p2}, Ljava/lang/Math;->signum(F)F

    move-result p2

    iget v3, p0, Lmiuix/springback/view/SpringBackLayout;->g:F

    goto :goto_1

    :cond_8
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getX(I)F

    move-result p1

    iget p2, p0, Lmiuix/springback/view/SpringBackLayout;->f:F

    sub-float/2addr p2, p1

    invoke-static {p2}, Ljava/lang/Math;->signum(F)F

    move-result p2

    iget v3, p0, Lmiuix/springback/view/SpringBackLayout;->f:F

    :goto_1
    sub-float/2addr v3, p1

    invoke-virtual {p0, v3, p3}, Lmiuix/springback/view/SpringBackLayout;->p(FI)F

    move-result p1

    mul-float/2addr p2, p1

    cmpl-float p1, p2, v2

    if-lez p1, :cond_9

    invoke-virtual {p0, v0}, Lmiuix/springback/view/SpringBackLayout;->w(Z)V

    neg-float p1, p2

    invoke-virtual {p0, p1, p3}, Lmiuix/springback/view/SpringBackLayout;->g(FI)V

    goto :goto_2

    :cond_9
    invoke-virtual {p0, v2, p3}, Lmiuix/springback/view/SpringBackLayout;->g(FI)V

    return v1

    :cond_a
    iget p2, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result p1

    if-gez p1, :cond_b

    const-string p1, "Got ACTION_UP event but don\'t have an active pointer id."

    invoke-static {v3, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_b
    iget-boolean p1, p0, Lmiuix/springback/view/SpringBackLayout;->h:Z

    if-eqz p1, :cond_c

    iput-boolean v1, p0, Lmiuix/springback/view/SpringBackLayout;->h:Z

    invoke-virtual {p0, v2, p3, v0}, Lmiuix/springback/view/SpringBackLayout;->x(FIZ)V

    :cond_c
    const/4 p1, -0x1

    iput p1, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    return v1

    :cond_d
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result p1

    iput p1, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    invoke-virtual {p0, p3}, Lmiuix/springback/view/SpringBackLayout;->a(I)V

    :cond_e
    :goto_2
    return v0
.end method

.method public final v(Landroid/view/MotionEvent;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    iget v2, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    if-ne v1, v2, :cond_1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result p1

    iput p1, p0, Lmiuix/springback/view/SpringBackLayout;->b:I

    :cond_1
    return-void
.end method

.method public w(Z)V
    .locals 2

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :goto_0
    if-eqz v0, :cond_1

    instance-of v1, v0, Lmiuix/springback/view/SpringBackLayout;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Lmiuix/springback/view/SpringBackLayout;

    invoke-super {v1, p1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    :cond_0
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final x(FIZ)V
    .locals 10

    iget-object v0, p0, Lmiuix/springback/view/SpringBackLayout;->p:Lmiuix/springback/view/SpringBackLayout$b;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmiuix/springback/view/SpringBackLayout$b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lmiuix/springback/view/SpringBackLayout;->A:Le2/c;

    invoke-virtual {v0}, Le2/c;->b()V

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getScrollY()I

    move-result v1

    iget-object v2, p0, Lmiuix/springback/view/SpringBackLayout;->A:Le2/c;

    int-to-float v3, v0

    int-to-float v4, v1

    const/4 v5, 0x0

    iput-boolean v5, v2, Le2/c;->f:Z

    iput-boolean v5, v2, Le2/c;->h:Z

    float-to-double v6, v3

    iput-wide v6, v2, Le2/c;->o:D

    iput-wide v6, v2, Le2/c;->j:D

    const/4 v3, 0x0

    float-to-double v6, v3

    iput-wide v6, v2, Le2/c;->d:D

    float-to-double v8, v4

    iput-wide v8, v2, Le2/c;->p:D

    iput-wide v8, v2, Le2/c;->k:D

    double-to-int v4, v8

    int-to-double v8, v4

    iput-wide v8, v2, Le2/c;->b:D

    iput-wide v6, v2, Le2/c;->e:D

    float-to-double v6, p1

    iput-wide v6, v2, Le2/c;->l:D

    iput-wide v6, v2, Le2/c;->q:D

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    const-wide v8, 0x40b3880000000000L    # 5000.0

    cmpg-double v4, v6, v8

    const/high16 v6, 0x3f800000    # 1.0f

    if-lez v4, :cond_1

    new-instance v4, Le2/b;

    const v7, 0x3f0ccccd    # 0.55f

    invoke-direct {v4, v6, v7}, Le2/b;-><init>(FF)V

    goto :goto_0

    :cond_1
    new-instance v4, Le2/b;

    const v7, 0x3ecccccd    # 0.4f

    invoke-direct {v4, v6, v7}, Le2/b;-><init>(FF)V

    :goto_0
    iput-object v4, v2, Le2/c;->m:Le2/b;

    iput p2, v2, Le2/c;->i:I

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v6

    iput-wide v6, v2, Le2/c;->n:J

    if-nez v0, :cond_2

    if-nez v1, :cond_2

    cmpl-float p1, p1, v3

    if-nez p1, :cond_2

    invoke-virtual {p0, v5}, Lmiuix/springback/view/SpringBackLayout;->c(I)V

    goto :goto_1

    :cond_2
    const/4 p1, 0x2

    invoke-virtual {p0, p1}, Lmiuix/springback/view/SpringBackLayout;->c(I)V

    :goto_1
    if-eqz p3, :cond_3

    invoke-virtual {p0}, Landroid/view/ViewGroup;->postInvalidateOnAnimation()V

    :cond_3
    return-void
.end method

.method public final y(I)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, p1, v1}, Lmiuix/springback/view/SpringBackLayout;->x(FIZ)V

    return-void
.end method

.method public final z()Z
    .locals 1

    iget v0, p0, Lmiuix/springback/view/SpringBackLayout;->z:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
