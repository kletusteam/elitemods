.class public Lmiuix/pickerwidget/widget/DateTimePicker$e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lmiuix/pickerwidget/widget/NumberPicker$h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/pickerwidget/widget/DateTimePicker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "e"
.end annotation


# instance fields
.field public final synthetic a:Lmiuix/pickerwidget/widget/DateTimePicker;


# direct methods
.method public constructor <init>(Lmiuix/pickerwidget/widget/DateTimePicker;Lmiuix/pickerwidget/widget/DateTimePicker$a;)V
    .locals 0

    iput-object p1, p0, Lmiuix/pickerwidget/widget/DateTimePicker$e;->a:Lmiuix/pickerwidget/widget/DateTimePicker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lmiuix/pickerwidget/widget/NumberPicker;II)V
    .locals 2

    iget-object p2, p0, Lmiuix/pickerwidget/widget/DateTimePicker$e;->a:Lmiuix/pickerwidget/widget/DateTimePicker;

    iget-object p3, p2, Lmiuix/pickerwidget/widget/DateTimePicker;->e:Lmiuix/pickerwidget/widget/NumberPicker;

    if-ne p1, p3, :cond_1

    invoke-virtual {p1}, Lmiuix/pickerwidget/widget/NumberPicker;->getValue()I

    move-result p2

    iget-object p3, p0, Lmiuix/pickerwidget/widget/DateTimePicker$e;->a:Lmiuix/pickerwidget/widget/DateTimePicker;

    iget v0, p3, Lmiuix/pickerwidget/widget/DateTimePicker;->d:I

    sub-int/2addr p2, v0

    add-int/lit8 p2, p2, 0x5

    rem-int/lit8 p2, p2, 0x5

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    :goto_0
    iget-object p2, p3, Lmiuix/pickerwidget/widget/DateTimePicker;->a:Lv1/a;

    const/16 p3, 0xc

    invoke-virtual {p2, p3, v0}, Lv1/a;->a(II)Lv1/a;

    iget-object p2, p0, Lmiuix/pickerwidget/widget/DateTimePicker$e;->a:Lmiuix/pickerwidget/widget/DateTimePicker;

    invoke-virtual {p1}, Lmiuix/pickerwidget/widget/NumberPicker;->getValue()I

    move-result p1

    iput p1, p2, Lmiuix/pickerwidget/widget/DateTimePicker;->d:I

    goto :goto_1

    :cond_1
    iget-object p3, p2, Lmiuix/pickerwidget/widget/DateTimePicker;->f:Lmiuix/pickerwidget/widget/NumberPicker;

    if-ne p1, p3, :cond_2

    iget-object p1, p2, Lmiuix/pickerwidget/widget/DateTimePicker;->a:Lv1/a;

    const/16 p2, 0x12

    invoke-virtual {p3}, Lmiuix/pickerwidget/widget/NumberPicker;->getValue()I

    move-result p3

    invoke-virtual {p1, p2, p3}, Lv1/a;->x(II)Lv1/a;

    goto :goto_1

    :cond_2
    iget-object p3, p2, Lmiuix/pickerwidget/widget/DateTimePicker;->n:Lmiuix/pickerwidget/widget/NumberPicker;

    if-ne p1, p3, :cond_3

    iget-object p1, p2, Lmiuix/pickerwidget/widget/DateTimePicker;->a:Lv1/a;

    const/16 v0, 0x14

    iget p2, p2, Lmiuix/pickerwidget/widget/DateTimePicker;->m:I

    invoke-virtual {p3}, Lmiuix/pickerwidget/widget/NumberPicker;->getValue()I

    move-result p3

    mul-int/2addr p3, p2

    invoke-virtual {p1, v0, p3}, Lv1/a;->x(II)Lv1/a;

    :cond_3
    :goto_1
    iget-object p1, p0, Lmiuix/pickerwidget/widget/DateTimePicker$e;->a:Lmiuix/pickerwidget/widget/DateTimePicker;

    invoke-virtual {p1}, Lmiuix/pickerwidget/widget/DateTimePicker;->b()V

    iget-object p1, p0, Lmiuix/pickerwidget/widget/DateTimePicker$e;->a:Lmiuix/pickerwidget/widget/DateTimePicker;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lmiuix/pickerwidget/widget/DateTimePicker;->f(Z)V

    iget-object p1, p0, Lmiuix/pickerwidget/widget/DateTimePicker$e;->a:Lmiuix/pickerwidget/widget/DateTimePicker;

    invoke-virtual {p1}, Lmiuix/pickerwidget/widget/DateTimePicker;->g()V

    iget-object p1, p0, Lmiuix/pickerwidget/widget/DateTimePicker$e;->a:Lmiuix/pickerwidget/widget/DateTimePicker;

    invoke-virtual {p1}, Lmiuix/pickerwidget/widget/DateTimePicker;->h()V

    iget-object p1, p0, Lmiuix/pickerwidget/widget/DateTimePicker$e;->a:Lmiuix/pickerwidget/widget/DateTimePicker;

    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->sendAccessibilityEvent(I)V

    iget-object p2, p0, Lmiuix/pickerwidget/widget/DateTimePicker$e;->a:Lmiuix/pickerwidget/widget/DateTimePicker;

    iget-object p3, p2, Lmiuix/pickerwidget/widget/DateTimePicker;->h:Lmiuix/pickerwidget/widget/DateTimePicker$d;

    if-eqz p3, :cond_4

    invoke-virtual {p2}, Lmiuix/pickerwidget/widget/DateTimePicker;->getTimeInMillis()J

    move-result-wide v0

    invoke-interface {p3, p1, v0, v1}, Lmiuix/pickerwidget/widget/DateTimePicker$d;->a(Lmiuix/pickerwidget/widget/DateTimePicker;J)V

    :cond_4
    return-void
.end method
