.class public Lmiuix/pickerwidget/widget/NumberPicker$i;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/pickerwidget/widget/NumberPicker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "i"
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public final synthetic c:Lmiuix/pickerwidget/widget/NumberPicker;


# direct methods
.method public constructor <init>(Lmiuix/pickerwidget/widget/NumberPicker;)V
    .locals 0

    iput-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker$i;->c:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/pickerwidget/widget/NumberPicker$i;->b:I

    iput v0, p0, Lmiuix/pickerwidget/widget/NumberPicker$i;->a:I

    iget-object v1, p0, Lmiuix/pickerwidget/widget/NumberPicker$i;->c:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v1, p0}, Landroid/widget/LinearLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v1, p0, Lmiuix/pickerwidget/widget/NumberPicker$i;->c:Lmiuix/pickerwidget/widget/NumberPicker;

    iget-boolean v2, v1, Lmiuix/pickerwidget/widget/NumberPicker;->q:Z

    if-eqz v2, :cond_0

    iput-boolean v0, v1, Lmiuix/pickerwidget/widget/NumberPicker;->q:Z

    iget v2, v1, Lmiuix/pickerwidget/widget/NumberPicker;->f:I

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getRight()I

    move-result v3

    iget-object v4, p0, Lmiuix/pickerwidget/widget/NumberPicker$i;->c:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getBottom()I

    move-result v4

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/widget/LinearLayout;->invalidate(IIII)V

    :cond_0
    iget-object v1, p0, Lmiuix/pickerwidget/widget/NumberPicker$i;->c:Lmiuix/pickerwidget/widget/NumberPicker;

    iget-boolean v2, v1, Lmiuix/pickerwidget/widget/NumberPicker;->j:Z

    if-eqz v2, :cond_1

    iput-boolean v0, v1, Lmiuix/pickerwidget/widget/NumberPicker;->j:Z

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getRight()I

    move-result v2

    iget-object v3, p0, Lmiuix/pickerwidget/widget/NumberPicker$i;->c:Lmiuix/pickerwidget/widget/NumberPicker;

    iget v3, v3, Lmiuix/pickerwidget/widget/NumberPicker;->p0:I

    invoke-virtual {v1, v0, v0, v2, v3}, Landroid/widget/LinearLayout;->invalidate(IIII)V

    :cond_1
    return-void
.end method

.method public run()V
    .locals 6

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker$i;->b:I

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eq v0, v3, :cond_5

    if-eq v0, v1, :cond_0

    goto :goto_2

    :cond_0
    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker$i;->a:I

    if-eq v0, v3, :cond_3

    if-eq v0, v1, :cond_1

    goto :goto_2

    :cond_1
    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker$i;->c:Lmiuix/pickerwidget/widget/NumberPicker;

    iget-boolean v1, v0, Lmiuix/pickerwidget/widget/NumberPicker;->j:Z

    if-nez v1, :cond_2

    invoke-static {}, Landroid/view/ViewConfiguration;->getPressedStateDuration()I

    move-result v1

    int-to-long v4, v1

    invoke-virtual {v0, p0, v4, v5}, Landroid/widget/LinearLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_2
    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker$i;->c:Lmiuix/pickerwidget/widget/NumberPicker;

    iget-boolean v1, v0, Lmiuix/pickerwidget/widget/NumberPicker;->j:Z

    xor-int/2addr v1, v3

    iput-boolean v1, v0, Lmiuix/pickerwidget/widget/NumberPicker;->j:Z

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getRight()I

    move-result v1

    iget-object v3, p0, Lmiuix/pickerwidget/widget/NumberPicker$i;->c:Lmiuix/pickerwidget/widget/NumberPicker;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker$i;->c:Lmiuix/pickerwidget/widget/NumberPicker;

    iget-boolean v1, v0, Lmiuix/pickerwidget/widget/NumberPicker;->q:Z

    if-nez v1, :cond_4

    invoke-static {}, Landroid/view/ViewConfiguration;->getPressedStateDuration()I

    move-result v1

    int-to-long v4, v1

    invoke-virtual {v0, p0, v4, v5}, Landroid/widget/LinearLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_4
    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker$i;->c:Lmiuix/pickerwidget/widget/NumberPicker;

    iget-boolean v1, v0, Lmiuix/pickerwidget/widget/NumberPicker;->q:Z

    xor-int/2addr v1, v3

    iput-boolean v1, v0, Lmiuix/pickerwidget/widget/NumberPicker;->q:Z

    goto :goto_1

    :cond_5
    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker$i;->a:I

    if-eq v0, v3, :cond_7

    if-eq v0, v1, :cond_6

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker$i;->c:Lmiuix/pickerwidget/widget/NumberPicker;

    iput-boolean v3, v0, Lmiuix/pickerwidget/widget/NumberPicker;->j:Z

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getRight()I

    move-result v1

    iget-object v3, p0, Lmiuix/pickerwidget/widget/NumberPicker$i;->c:Lmiuix/pickerwidget/widget/NumberPicker;

    :goto_0
    iget v3, v3, Lmiuix/pickerwidget/widget/NumberPicker;->p0:I

    invoke-virtual {v0, v2, v2, v1, v3}, Landroid/widget/LinearLayout;->invalidate(IIII)V

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker$i;->c:Lmiuix/pickerwidget/widget/NumberPicker;

    iput-boolean v3, v0, Lmiuix/pickerwidget/widget/NumberPicker;->q:Z

    :goto_1
    iget v1, v0, Lmiuix/pickerwidget/widget/NumberPicker;->f:I

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getRight()I

    move-result v3

    iget-object v4, p0, Lmiuix/pickerwidget/widget/NumberPicker$i;->c:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getBottom()I

    move-result v4

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/widget/LinearLayout;->invalidate(IIII)V

    :goto_2
    return-void
.end method
