.class public Lmiuix/pickerwidget/widget/DateTimePicker$c;
.super Lmiuix/pickerwidget/widget/DateTimePicker$b;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/pickerwidget/widget/DateTimePicker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/pickerwidget/widget/DateTimePicker$b;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public a(III)Ljava/lang/String;
    .locals 3

    sget-object v0, Lmiuix/pickerwidget/widget/DateTimePicker;->q:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lv1/a;

    if-nez v0, :cond_0

    new-instance v0, Lv1/a;

    invoke-direct {v0}, Lv1/a;-><init>()V

    sget-object v1, Lmiuix/pickerwidget/widget/DateTimePicker;->q:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lv1/a;->x(II)Lv1/a;

    const/4 p1, 0x5

    invoke-virtual {v0, p1, p2}, Lv1/a;->x(II)Lv1/a;

    const/16 p1, 0x9

    invoke-virtual {v0, p1, p3}, Lv1/a;->x(II)Lv1/a;

    iget-object p1, p0, Lmiuix/pickerwidget/widget/DateTimePicker$b;->a:Landroid/content/Context;

    const p2, 0x7f120082

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    sget-object p3, Lm1/j;->c:Lm1/j$f;

    check-cast p3, Lm1/j$b;

    invoke-virtual {p3}, Lm1/j$b;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, p2, v2}, Lv1/a;->k(Landroid/content/Context;Ljava/lang/StringBuilder;Ljava/lang/CharSequence;Lv1/b;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, v1}, Lm1/j$b;->d(Ljava/lang/Object;)V

    return-object p1
.end method
