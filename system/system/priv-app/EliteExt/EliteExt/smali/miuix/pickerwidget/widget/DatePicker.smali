.class public Lmiuix/pickerwidget/widget/DatePicker;
.super Landroid/widget/FrameLayout;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/pickerwidget/widget/DatePicker$a;,
        Lmiuix/pickerwidget/widget/DatePicker$b;
    }
.end annotation


# static fields
.field public static q:[Ljava/lang/String;

.field public static r:Ljava/lang/String;

.field public static s:[Ljava/lang/String;

.field public static t:[Ljava/lang/String;


# instance fields
.field public a:Lv1/a;

.field public b:Ljava/util/Locale;

.field public final c:Ljava/text/DateFormat;

.field public d:[C

.field public final e:Lmiuix/pickerwidget/widget/NumberPicker;

.field public f:Z

.field public g:Z

.field public h:Lv1/a;

.field public i:Lv1/a;

.field public final j:Lmiuix/pickerwidget/widget/NumberPicker;

.field public k:I

.field public l:Lmiuix/pickerwidget/widget/DatePicker$a;

.field public m:[Ljava/lang/String;

.field public final n:Landroid/widget/LinearLayout;

.field public o:Lv1/a;

.field public final p:Lmiuix/pickerwidget/widget/NumberPicker;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 19

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    const v3, 0x7f0400db

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v5, "MM/dd/yyyy"

    invoke-direct {v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v4, v0, Lmiuix/pickerwidget/widget/DatePicker;->c:Ljava/text/DateFormat;

    const/4 v4, 0x1

    iput-boolean v4, v0, Lmiuix/pickerwidget/widget/DatePicker;->f:Z

    const/4 v5, 0x0

    iput-boolean v5, v0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    sget-object v6, Lmiuix/pickerwidget/widget/DatePicker;->q:[Ljava/lang/String;

    if-nez v6, :cond_0

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lv1/b;->d(Landroid/content/Context;)Lv1/b;

    move-result-object v6

    iget-object v6, v6, Lv1/b;->a:Landroid/content/res/Resources;

    const v7, 0x7f030003

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lmiuix/pickerwidget/widget/DatePicker;->q:[Ljava/lang/String;

    :cond_0
    sget-object v6, Lmiuix/pickerwidget/widget/DatePicker;->t:[Ljava/lang/String;

    if-nez v6, :cond_2

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lv1/b;->d(Landroid/content/Context;)Lv1/b;

    move-result-object v6

    iget-object v6, v6, Lv1/b;->a:Landroid/content/res/Resources;

    const v7, 0x7f030006

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lmiuix/pickerwidget/widget/DatePicker;->t:[Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    move v7, v5

    :goto_0
    sget-object v8, Lmiuix/pickerwidget/widget/DatePicker;->t:[Ljava/lang/String;

    array-length v9, v8

    if-ge v7, v9, :cond_1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lmiuix/pickerwidget/widget/DatePicker;->t:[Ljava/lang/String;

    aget-object v10, v9, v7

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v10, 0x7f12004d

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v9, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_1
    array-length v6, v8

    add-int/2addr v6, v4

    new-array v6, v6, [Ljava/lang/String;

    sput-object v6, Lmiuix/pickerwidget/widget/DatePicker;->s:[Ljava/lang/String;

    :cond_2
    sget-object v6, Lmiuix/pickerwidget/widget/DatePicker;->r:Ljava/lang/String;

    if-nez v6, :cond_3

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lv1/b;->d(Landroid/content/Context;)Lv1/b;

    move-result-object v6

    iget-object v6, v6, Lv1/b;->a:Landroid/content/res/Resources;

    const v7, 0x7f030005

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v4

    sput-object v6, Lmiuix/pickerwidget/widget/DatePicker;->r:Ljava/lang/String;

    :cond_3
    new-instance v6, Lv1/a;

    invoke-direct {v6}, Lv1/a;-><init>()V

    iput-object v6, v0, Lmiuix/pickerwidget/widget/DatePicker;->o:Lv1/a;

    new-instance v6, Lv1/a;

    invoke-direct {v6}, Lv1/a;-><init>()V

    iput-object v6, v0, Lmiuix/pickerwidget/widget/DatePicker;->i:Lv1/a;

    new-instance v6, Lv1/a;

    invoke-direct {v6}, Lv1/a;-><init>()V

    iput-object v6, v0, Lmiuix/pickerwidget/widget/DatePicker;->h:Lv1/a;

    new-instance v6, Lv1/a;

    invoke-direct {v6}, Lv1/a;-><init>()V

    iput-object v6, v0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    sget-object v6, Landroidx/emoji2/text/l;->a0:[I

    const v7, 0x7f130231

    invoke-virtual {v1, v2, v6, v3, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v6

    const/16 v7, 0x76c

    const/16 v8, 0x9

    invoke-virtual {v2, v8, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v10

    const/16 v7, 0x834

    invoke-virtual {v2, v4, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v7

    const/4 v9, 0x4

    invoke-virtual {v2, v9}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v11, 0x3

    invoke-virtual {v2, v11}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v15

    const v11, 0x7f0d003e

    const/4 v12, 0x2

    invoke-virtual {v2, v12, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v12

    iput-boolean v12, v0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    const/4 v12, 0x7

    invoke-virtual {v2, v12, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v12

    const/4 v13, 0x6

    invoke-virtual {v2, v13, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v13

    const/4 v14, 0x5

    invoke-virtual {v2, v14, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v16

    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v2}, Lmiuix/pickerwidget/widget/DatePicker;->setCurrentLocale(Ljava/util/Locale;)V

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    invoke-virtual {v1, v11, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    new-instance v1, Ly1/a;

    invoke-direct {v1, v0}, Ly1/a;-><init>(Lmiuix/pickerwidget/widget/DatePicker;)V

    const v2, 0x7f0a00b8

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, v0, Lmiuix/pickerwidget/widget/DatePicker;->n:Landroid/widget/LinearLayout;

    const v2, 0x7f0a006a

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lmiuix/pickerwidget/widget/NumberPicker;

    iput-object v2, v0, Lmiuix/pickerwidget/widget/DatePicker;->e:Lmiuix/pickerwidget/widget/NumberPicker;

    move-object/from16 p2, v15

    const-wide/16 v14, 0x64

    invoke-virtual {v2, v14, v15}, Lmiuix/pickerwidget/widget/NumberPicker;->setOnLongPressUpdateInterval(J)V

    invoke-virtual {v2, v1}, Lmiuix/pickerwidget/widget/NumberPicker;->setOnValueChangedListener(Lmiuix/pickerwidget/widget/NumberPicker$h;)V

    if-nez v16, :cond_4

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_4
    const v2, 0x7f0a00a9

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lmiuix/pickerwidget/widget/NumberPicker;

    iput-object v2, v0, Lmiuix/pickerwidget/widget/DatePicker;->j:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v2, v5}, Lmiuix/pickerwidget/widget/NumberPicker;->setMinValue(I)V

    iget v5, v0, Lmiuix/pickerwidget/widget/DatePicker;->k:I

    sub-int/2addr v5, v4

    invoke-virtual {v2, v5}, Lmiuix/pickerwidget/widget/NumberPicker;->setMaxValue(I)V

    iget-object v5, v0, Lmiuix/pickerwidget/widget/DatePicker;->m:[Ljava/lang/String;

    invoke-virtual {v2, v5}, Lmiuix/pickerwidget/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    move-object v11, v9

    const-wide/16 v8, 0xc8

    invoke-virtual {v2, v8, v9}, Lmiuix/pickerwidget/widget/NumberPicker;->setOnLongPressUpdateInterval(J)V

    invoke-virtual {v2, v1}, Lmiuix/pickerwidget/widget/NumberPicker;->setOnValueChangedListener(Lmiuix/pickerwidget/widget/NumberPicker$h;)V

    if-nez v13, :cond_5

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_5
    const v2, 0x7f0a0120

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lmiuix/pickerwidget/widget/NumberPicker;

    iput-object v2, v0, Lmiuix/pickerwidget/widget/DatePicker;->p:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v2, v14, v15}, Lmiuix/pickerwidget/widget/NumberPicker;->setOnLongPressUpdateInterval(J)V

    invoke-virtual {v2, v1}, Lmiuix/pickerwidget/widget/NumberPicker;->setOnValueChangedListener(Lmiuix/pickerwidget/widget/NumberPicker$h;)V

    if-nez v12, :cond_6

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_6
    invoke-virtual/range {p0 .. p0}, Lmiuix/pickerwidget/widget/DatePicker;->f()V

    if-nez v6, :cond_7

    invoke-virtual {v0, v4}, Lmiuix/pickerwidget/widget/DatePicker;->setSpinnersShown(Z)V

    goto :goto_1

    :cond_7
    invoke-virtual {v0, v6}, Lmiuix/pickerwidget/widget/DatePicker;->setSpinnersShown(Z)V

    :goto_1
    iget-object v1, v0, Lmiuix/pickerwidget/widget/DatePicker;->o:Lv1/a;

    const-wide/16 v2, 0x0

    iput-wide v2, v1, Lv1/a;->f:J

    invoke-virtual {v1}, Lv1/a;->d()V

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    iget-object v1, v0, Lmiuix/pickerwidget/widget/DatePicker;->o:Lv1/a;

    invoke-virtual {v0, v11, v1}, Lmiuix/pickerwidget/widget/DatePicker;->a(Ljava/lang/String;Lv1/a;)Z

    move-result v1

    if-nez v1, :cond_8

    goto :goto_2

    :cond_8
    move-object/from16 v6, p2

    const/4 v1, 0x5

    goto :goto_3

    :cond_9
    :goto_2
    iget-object v1, v0, Lmiuix/pickerwidget/widget/DatePicker;->o:Lv1/a;

    move-object v9, v1

    const/16 v16, 0x0

    const/4 v15, 0x0

    const/4 v14, 0x0

    const/4 v13, 0x0

    const/4 v12, 0x1

    const/4 v11, 0x0

    const/4 v1, 0x5

    move-object/from16 v6, p2

    invoke-virtual/range {v9 .. v16}, Lv1/a;->y(IIIIIII)Lv1/a;

    :goto_3
    iget-object v8, v0, Lmiuix/pickerwidget/widget/DatePicker;->o:Lv1/a;

    iget-wide v8, v8, Lv1/a;->f:J

    invoke-virtual {v0, v8, v9}, Lmiuix/pickerwidget/widget/DatePicker;->setMinDate(J)V

    iget-object v8, v0, Lmiuix/pickerwidget/widget/DatePicker;->o:Lv1/a;

    iput-wide v2, v8, Lv1/a;->f:J

    invoke-virtual {v8}, Lv1/a;->d()V

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    iget-object v2, v0, Lmiuix/pickerwidget/widget/DatePicker;->o:Lv1/a;

    invoke-virtual {v0, v6, v2}, Lmiuix/pickerwidget/widget/DatePicker;->a(Ljava/lang/String;Lv1/a;)Z

    move-result v2

    if-nez v2, :cond_b

    :cond_a
    iget-object v11, v0, Lmiuix/pickerwidget/widget/DatePicker;->o:Lv1/a;

    const/16 v13, 0xb

    const/16 v14, 0x1f

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move v12, v7

    invoke-virtual/range {v11 .. v18}, Lv1/a;->y(IIIIIII)Lv1/a;

    :cond_b
    iget-object v2, v0, Lmiuix/pickerwidget/widget/DatePicker;->o:Lv1/a;

    iget-wide v2, v2, Lv1/a;->f:J

    invoke-virtual {v0, v2, v3}, Lmiuix/pickerwidget/widget/DatePicker;->setMaxDate(J)V

    iget-object v2, v0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iput-wide v6, v2, Lv1/a;->f:J

    invoke-virtual {v2}, Lv1/a;->d()V

    iget-object v2, v0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    invoke-virtual {v2, v4}, Lv1/a;->l(I)I

    move-result v2

    iget-object v3, v0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    invoke-virtual {v3, v1}, Lv1/a;->l(I)I

    move-result v1

    iget-object v3, v0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    const/16 v5, 0x9

    invoke-virtual {v3, v5}, Lv1/a;->l(I)I

    move-result v3

    const/4 v5, 0x0

    invoke-virtual {v0, v2, v1, v3}, Lmiuix/pickerwidget/widget/DatePicker;->d(III)V

    invoke-virtual/range {p0 .. p0}, Lmiuix/pickerwidget/widget/DatePicker;->g()V

    iput-object v5, v0, Lmiuix/pickerwidget/widget/DatePicker;->l:Lmiuix/pickerwidget/widget/DatePicker$a;

    invoke-virtual/range {p0 .. p0}, Lmiuix/pickerwidget/widget/DatePicker;->b()V

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getImportantForAccessibility()I

    move-result v1

    if-nez v1, :cond_c

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setImportantForAccessibility(I)V

    :cond_c
    return-void
.end method

.method private setCurrentLocale(Ljava/util/Locale;)V
    .locals 1

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->b:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iput-object p1, p0, Lmiuix/pickerwidget/widget/DatePicker;->b:Ljava/util/Locale;

    iget-object p1, p0, Lmiuix/pickerwidget/widget/DatePicker;->o:Lv1/a;

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lv1/a;->m(I)I

    move-result p1

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lmiuix/pickerwidget/widget/DatePicker;->k:I

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/DatePicker;->c()V

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/DatePicker;->f()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lv1/a;)Z
    .locals 2

    :try_start_0
    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->c:Ljava/text/DateFormat;

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iput-wide v0, p2, Lv1/a;->f:J

    invoke-virtual {p2}, Lv1/a;->d()V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Date: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " not in format: "

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "MM/dd/yyyy"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "DatePicker"

    invoke-static {p2, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, 0x0

    return p1
.end method

.method public final b()V
    .locals 5

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->d:[C

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v0

    :cond_0
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_4

    aget-char v3, v0, v2

    const/16 v4, 0x4d

    if-eq v3, v4, :cond_3

    const/16 v4, 0x64

    if-eq v3, v4, :cond_2

    const/16 v4, 0x79

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lmiuix/pickerwidget/widget/DatePicker;->n:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lmiuix/pickerwidget/widget/DatePicker;->p:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v3, p0, Lmiuix/pickerwidget/widget/DatePicker;->p:Lmiuix/pickerwidget/widget/NumberPicker;

    goto :goto_1

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    iget-object v3, p0, Lmiuix/pickerwidget/widget/DatePicker;->n:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lmiuix/pickerwidget/widget/DatePicker;->e:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v3, p0, Lmiuix/pickerwidget/widget/DatePicker;->e:Lmiuix/pickerwidget/widget/NumberPicker;

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lmiuix/pickerwidget/widget/DatePicker;->n:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lmiuix/pickerwidget/widget/DatePicker;->j:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v3, p0, Lmiuix/pickerwidget/widget/DatePicker;->j:Lmiuix/pickerwidget/widget/NumberPicker;

    :goto_1
    invoke-virtual {p0, v3, v1, v2}, Lmiuix/pickerwidget/widget/DatePicker;->e(Lmiuix/pickerwidget/widget/NumberPicker;II)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    return-void
.end method

.method public final c()V
    .locals 5

    iget-boolean v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    invoke-virtual {v0}, Lv1/a;->o()I

    move-result v0

    if-gez v0, :cond_0

    sget-object v0, Lmiuix/pickerwidget/widget/DatePicker;->t:[Ljava/lang/String;

    iput-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->m:[Ljava/lang/String;

    goto :goto_1

    :cond_0
    sget-object v2, Lmiuix/pickerwidget/widget/DatePicker;->s:[Ljava/lang/String;

    iput-object v2, p0, Lmiuix/pickerwidget/widget/DatePicker;->m:[Ljava/lang/String;

    sget-object v3, Lmiuix/pickerwidget/widget/DatePicker;->t:[Ljava/lang/String;

    add-int/lit8 v4, v0, 0x1

    invoke-static {v3, v1, v2, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sget-object v1, Lmiuix/pickerwidget/widget/DatePicker;->t:[Ljava/lang/String;

    iget-object v2, p0, Lmiuix/pickerwidget/widget/DatePicker;->m:[Ljava/lang/String;

    array-length v3, v1

    sub-int/2addr v3, v0

    invoke-static {v1, v0, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->m:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lmiuix/pickerwidget/widget/DatePicker;->r:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lmiuix/pickerwidget/widget/DatePicker;->m:[Ljava/lang/String;

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->b:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v2, "en"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lv1/b;->d(Landroid/content/Context;)Lv1/b;

    move-result-object v0

    iget-object v0, v0, Lv1/b;->a:Landroid/content/res/Resources;

    const v1, 0x7f03000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->m:[Ljava/lang/String;

    goto :goto_1

    :cond_2
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->m:[Ljava/lang/String;

    :goto_0
    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->m:[Ljava/lang/String;

    array-length v2, v0

    if-ge v1, v2, :cond_3

    sget-object v2, Lmiuix/pickerwidget/widget/NumberPicker;->w0:Lmiuix/pickerwidget/widget/NumberPicker$d;

    add-int/lit8 v3, v1, 0x1

    check-cast v2, Lmiuix/pickerwidget/widget/NumberPicker$f;

    invoke-virtual {v2, v3}, Lmiuix/pickerwidget/widget/NumberPicker$f;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    move v1, v3

    goto :goto_0

    :cond_3
    :goto_1
    return-void
.end method

.method public final d(III)V
    .locals 8

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move v1, p1

    move v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v7}, Lv1/a;->y(IIIIIII)Lv1/a;

    iget-object p1, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    iget-object p2, p0, Lmiuix/pickerwidget/widget/DatePicker;->i:Lv1/a;

    iget-wide v0, p1, Lv1/a;->f:J

    iget-wide p2, p2, Lv1/a;->f:J

    cmp-long v2, v0, p2

    const/4 v3, 0x1

    if-gez v2, :cond_0

    move v2, v3

    goto :goto_0

    :cond_0
    move v2, v4

    :goto_0
    if-eqz v2, :cond_1

    goto :goto_2

    :cond_1
    iget-object p2, p0, Lmiuix/pickerwidget/widget/DatePicker;->h:Lv1/a;

    iget-wide p2, p2, Lv1/a;->f:J

    cmp-long v0, v0, p2

    if-lez v0, :cond_2

    goto :goto_1

    :cond_2
    move v3, v4

    :goto_1
    if-eqz v3, :cond_3

    :goto_2
    iput-wide p2, p1, Lv1/a;->f:J

    invoke-virtual {p1}, Lv1/a;->d()V

    :cond_3
    return-void
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 0

    invoke-virtual {p0, p1}, Lmiuix/pickerwidget/widget/DatePicker;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    const/4 p1, 0x1

    return p1
.end method

.method public dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->dispatchThawSelfOnly(Landroid/util/SparseArray;)V

    return-void
.end method

.method public final e(Lmiuix/pickerwidget/widget/NumberPicker;II)V
    .locals 0

    add-int/lit8 p2, p2, -0x1

    if-ge p3, p2, :cond_0

    const/4 p2, 0x5

    goto :goto_0

    :cond_0
    const/4 p2, 0x6

    :goto_0
    const p3, 0x7f0a00b2

    invoke-virtual {p1, p3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setImeOptions(I)V

    return-void
.end method

.method public final f()V
    .locals 2

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->e:Lmiuix/pickerwidget/widget/NumberPicker;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lmiuix/pickerwidget/widget/DatePicker;->p:Lmiuix/pickerwidget/widget/NumberPicker;

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v1, Lmiuix/pickerwidget/widget/NumberPicker;->w0:Lmiuix/pickerwidget/widget/NumberPicker$d;

    invoke-virtual {v0, v1}, Lmiuix/pickerwidget/widget/NumberPicker;->setFormatter(Lmiuix/pickerwidget/widget/NumberPicker$d;)V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->p:Lmiuix/pickerwidget/widget/NumberPicker;

    new-instance v1, Lmiuix/pickerwidget/widget/NumberPicker$f;

    invoke-direct {v1}, Lmiuix/pickerwidget/widget/NumberPicker$f;-><init>()V

    invoke-virtual {v0, v1}, Lmiuix/pickerwidget/widget/NumberPicker;->setFormatter(Lmiuix/pickerwidget/widget/NumberPicker$d;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public final g()V
    .locals 12

    iget-boolean v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->e:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v0, v1}, Lmiuix/pickerwidget/widget/NumberPicker;->setLabel(Ljava/lang/String;)V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->j:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v0, v1}, Lmiuix/pickerwidget/widget/NumberPicker;->setLabel(Ljava/lang/String;)V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->p:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v0, v1}, Lmiuix/pickerwidget/widget/NumberPicker;->setLabel(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->e:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f12006b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmiuix/pickerwidget/widget/NumberPicker;->setLabel(Ljava/lang/String;)V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->j:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f12006c

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmiuix/pickerwidget/widget/NumberPicker;->setLabel(Ljava/lang/String;)V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->p:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f12006d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmiuix/pickerwidget/widget/NumberPicker;->setLabel(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->e:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v0, v1}, Lmiuix/pickerwidget/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->e:Lmiuix/pickerwidget/widget/NumberPicker;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lmiuix/pickerwidget/widget/NumberPicker;->setMinValue(I)V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->e:Lmiuix/pickerwidget/widget/NumberPicker;

    iget-boolean v3, p0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    const/16 v4, 0xa

    const/16 v5, 0x9

    if-eqz v3, :cond_1

    iget-object v3, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    invoke-virtual {v3, v4}, Lv1/a;->m(I)I

    move-result v3

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    invoke-virtual {v3, v5}, Lv1/a;->m(I)I

    move-result v3

    :goto_1
    invoke-virtual {v0, v3}, Lmiuix/pickerwidget/widget/NumberPicker;->setMaxValue(I)V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->e:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v0, v2}, Lmiuix/pickerwidget/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->j:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v0, v1}, Lmiuix/pickerwidget/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->j:Lmiuix/pickerwidget/widget/NumberPicker;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lmiuix/pickerwidget/widget/NumberPicker;->setMinValue(I)V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->j:Lmiuix/pickerwidget/widget/NumberPicker;

    iget-boolean v6, p0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    const/16 v7, 0xb

    if-eqz v6, :cond_2

    iget-object v6, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    invoke-virtual {v6}, Lv1/a;->o()I

    move-result v6

    if-ltz v6, :cond_2

    const/16 v7, 0xc

    :cond_2
    invoke-virtual {v0, v7}, Lmiuix/pickerwidget/widget/NumberPicker;->setMaxValue(I)V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->j:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v0, v2}, Lmiuix/pickerwidget/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    iget-boolean v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    const/4 v6, 0x2

    if-eqz v0, :cond_3

    move v0, v6

    goto :goto_2

    :cond_3
    move v0, v2

    :goto_2
    iget-object v7, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    invoke-virtual {v7, v0}, Lv1/a;->l(I)I

    move-result v7

    iget-object v8, p0, Lmiuix/pickerwidget/widget/DatePicker;->i:Lv1/a;

    invoke-virtual {v8, v0}, Lv1/a;->l(I)I

    move-result v8

    const/4 v9, 0x5

    const/4 v10, 0x6

    if-ne v7, v8, :cond_7

    iget-object v7, p0, Lmiuix/pickerwidget/widget/DatePicker;->j:Lmiuix/pickerwidget/widget/NumberPicker;

    iget-boolean v8, p0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    if-eqz v8, :cond_4

    iget-object v8, p0, Lmiuix/pickerwidget/widget/DatePicker;->i:Lv1/a;

    invoke-virtual {v8, v10}, Lv1/a;->l(I)I

    move-result v8

    goto :goto_3

    :cond_4
    iget-object v8, p0, Lmiuix/pickerwidget/widget/DatePicker;->i:Lv1/a;

    invoke-virtual {v8, v9}, Lv1/a;->l(I)I

    move-result v8

    :goto_3
    invoke-virtual {v7, v8}, Lmiuix/pickerwidget/widget/NumberPicker;->setMinValue(I)V

    iget-object v7, p0, Lmiuix/pickerwidget/widget/DatePicker;->j:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v7, v3}, Lmiuix/pickerwidget/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    iget-boolean v7, p0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    if-eqz v7, :cond_5

    move v7, v10

    goto :goto_4

    :cond_5
    move v7, v9

    :goto_4
    iget-object v8, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    invoke-virtual {v8, v7}, Lv1/a;->l(I)I

    move-result v8

    iget-object v11, p0, Lmiuix/pickerwidget/widget/DatePicker;->i:Lv1/a;

    invoke-virtual {v11, v7}, Lv1/a;->l(I)I

    move-result v7

    if-ne v8, v7, :cond_7

    iget-object v7, p0, Lmiuix/pickerwidget/widget/DatePicker;->e:Lmiuix/pickerwidget/widget/NumberPicker;

    iget-boolean v8, p0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    if-eqz v8, :cond_6

    iget-object v8, p0, Lmiuix/pickerwidget/widget/DatePicker;->i:Lv1/a;

    invoke-virtual {v8, v4}, Lv1/a;->l(I)I

    move-result v8

    goto :goto_5

    :cond_6
    iget-object v8, p0, Lmiuix/pickerwidget/widget/DatePicker;->i:Lv1/a;

    invoke-virtual {v8, v5}, Lv1/a;->l(I)I

    move-result v8

    :goto_5
    invoke-virtual {v7, v8}, Lmiuix/pickerwidget/widget/NumberPicker;->setMinValue(I)V

    iget-object v7, p0, Lmiuix/pickerwidget/widget/DatePicker;->e:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v7, v3}, Lmiuix/pickerwidget/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    :cond_7
    iget-object v7, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    invoke-virtual {v7, v0}, Lv1/a;->l(I)I

    move-result v7

    iget-object v8, p0, Lmiuix/pickerwidget/widget/DatePicker;->h:Lv1/a;

    invoke-virtual {v8, v0}, Lv1/a;->l(I)I

    move-result v0

    if-ne v7, v0, :cond_b

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->j:Lmiuix/pickerwidget/widget/NumberPicker;

    iget-boolean v7, p0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    if-eqz v7, :cond_8

    iget-object v7, p0, Lmiuix/pickerwidget/widget/DatePicker;->i:Lv1/a;

    invoke-virtual {v7, v10}, Lv1/a;->l(I)I

    move-result v7

    goto :goto_6

    :cond_8
    iget-object v7, p0, Lmiuix/pickerwidget/widget/DatePicker;->h:Lv1/a;

    invoke-virtual {v7, v9}, Lv1/a;->l(I)I

    move-result v7

    :goto_6
    invoke-virtual {v0, v7}, Lmiuix/pickerwidget/widget/NumberPicker;->setMaxValue(I)V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->j:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v0, v3}, Lmiuix/pickerwidget/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->j:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v0, v1}, Lmiuix/pickerwidget/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    iget-boolean v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    if-eqz v0, :cond_9

    move v0, v10

    goto :goto_7

    :cond_9
    move v0, v9

    :goto_7
    iget-object v1, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    invoke-virtual {v1, v0}, Lv1/a;->l(I)I

    move-result v1

    iget-object v7, p0, Lmiuix/pickerwidget/widget/DatePicker;->h:Lv1/a;

    invoke-virtual {v7, v0}, Lv1/a;->l(I)I

    move-result v0

    if-ne v1, v0, :cond_b

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->e:Lmiuix/pickerwidget/widget/NumberPicker;

    iget-boolean v1, p0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    if-eqz v1, :cond_a

    iget-object v1, p0, Lmiuix/pickerwidget/widget/DatePicker;->h:Lv1/a;

    invoke-virtual {v1, v4}, Lv1/a;->l(I)I

    move-result v1

    goto :goto_8

    :cond_a
    iget-object v1, p0, Lmiuix/pickerwidget/widget/DatePicker;->h:Lv1/a;

    invoke-virtual {v1, v5}, Lv1/a;->l(I)I

    move-result v1

    :goto_8
    invoke-virtual {v0, v1}, Lmiuix/pickerwidget/widget/NumberPicker;->setMaxValue(I)V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->e:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v0, v3}, Lmiuix/pickerwidget/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    :cond_b
    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->m:[Ljava/lang/String;

    iget-object v1, p0, Lmiuix/pickerwidget/widget/DatePicker;->j:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v1}, Lmiuix/pickerwidget/widget/NumberPicker;->getMinValue()I

    move-result v1

    iget-object v7, p0, Lmiuix/pickerwidget/widget/DatePicker;->m:[Ljava/lang/String;

    array-length v7, v7

    invoke-static {v0, v1, v7}, Ljava/util/Arrays;->copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iget-object v1, p0, Lmiuix/pickerwidget/widget/DatePicker;->j:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v1, v0}, Lmiuix/pickerwidget/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    iget-boolean v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    if-eqz v0, :cond_c

    sget-object v0, Lmiuix/pickerwidget/widget/DatePicker;->q:[Ljava/lang/String;

    iget-object v1, p0, Lmiuix/pickerwidget/widget/DatePicker;->e:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v1}, Lmiuix/pickerwidget/widget/NumberPicker;->getMinValue()I

    move-result v1

    sub-int/2addr v1, v2

    sget-object v7, Lmiuix/pickerwidget/widget/DatePicker;->q:[Ljava/lang/String;

    array-length v7, v7

    invoke-static {v0, v1, v7}, Ljava/util/Arrays;->copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iget-object v1, p0, Lmiuix/pickerwidget/widget/DatePicker;->e:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v1, v0}, Lmiuix/pickerwidget/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    :cond_c
    iget-boolean v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    if-eqz v0, :cond_d

    move v0, v6

    goto :goto_9

    :cond_d
    move v0, v2

    :goto_9
    iget-object v1, p0, Lmiuix/pickerwidget/widget/DatePicker;->p:Lmiuix/pickerwidget/widget/NumberPicker;

    iget-object v7, p0, Lmiuix/pickerwidget/widget/DatePicker;->i:Lv1/a;

    invoke-virtual {v7, v0}, Lv1/a;->l(I)I

    move-result v7

    invoke-virtual {v1, v7}, Lmiuix/pickerwidget/widget/NumberPicker;->setMinValue(I)V

    iget-object v1, p0, Lmiuix/pickerwidget/widget/DatePicker;->p:Lmiuix/pickerwidget/widget/NumberPicker;

    iget-object v7, p0, Lmiuix/pickerwidget/widget/DatePicker;->h:Lv1/a;

    invoke-virtual {v7, v0}, Lv1/a;->l(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lmiuix/pickerwidget/widget/NumberPicker;->setMaxValue(I)V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->p:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v0, v3}, Lmiuix/pickerwidget/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    invoke-virtual {v0}, Lv1/a;->o()I

    move-result v0

    if-ltz v0, :cond_f

    iget-object v1, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    invoke-virtual {v1}, Lv1/a;->p()Z

    move-result v1

    if-nez v1, :cond_e

    iget-object v1, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    invoke-virtual {v1, v10}, Lv1/a;->l(I)I

    move-result v1

    if-le v1, v0, :cond_f

    :cond_e
    move v3, v2

    :cond_f
    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->p:Lmiuix/pickerwidget/widget/NumberPicker;

    iget-boolean v1, p0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    if-eqz v1, :cond_10

    iget-object v1, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    invoke-virtual {v1, v6}, Lv1/a;->l(I)I

    move-result v1

    goto :goto_a

    :cond_10
    iget-object v1, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    invoke-virtual {v1, v2}, Lv1/a;->l(I)I

    move-result v1

    :goto_a
    invoke-virtual {v0, v1}, Lmiuix/pickerwidget/widget/NumberPicker;->setValue(I)V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->j:Lmiuix/pickerwidget/widget/NumberPicker;

    iget-boolean v1, p0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    if-eqz v1, :cond_11

    iget-object v1, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    invoke-virtual {v1, v10}, Lv1/a;->l(I)I

    move-result v1

    if-eqz v3, :cond_12

    add-int/2addr v1, v2

    goto :goto_b

    :cond_11
    iget-object v1, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    invoke-virtual {v1, v9}, Lv1/a;->l(I)I

    move-result v1

    :cond_12
    :goto_b
    invoke-virtual {v0, v1}, Lmiuix/pickerwidget/widget/NumberPicker;->setValue(I)V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->e:Lmiuix/pickerwidget/widget/NumberPicker;

    iget-boolean v1, p0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    if-eqz v1, :cond_13

    iget-object v1, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    invoke-virtual {v1, v4}, Lv1/a;->l(I)I

    move-result v1

    goto :goto_c

    :cond_13
    iget-object v1, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    invoke-virtual {v1, v5}, Lv1/a;->l(I)I

    move-result v1

    :goto_c
    invoke-virtual {v0, v1}, Lmiuix/pickerwidget/widget/NumberPicker;->setValue(I)V

    return-void
.end method

.method public getDayOfMonth()I
    .locals 2

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    iget-boolean v1, p0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    if-eqz v1, :cond_0

    const/16 v1, 0xa

    goto :goto_0

    :cond_0
    const/16 v1, 0x9

    :goto_0
    invoke-virtual {v0, v1}, Lv1/a;->l(I)I

    move-result v0

    return v0
.end method

.method public getMaxDate()J
    .locals 2

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->h:Lv1/a;

    iget-wide v0, v0, Lv1/a;->f:J

    return-wide v0
.end method

.method public getMinDate()J
    .locals 2

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->i:Lv1/a;

    iget-wide v0, v0, Lv1/a;->f:J

    return-wide v0
.end method

.method public getMonth()I
    .locals 2

    iget-boolean v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    invoke-virtual {v0}, Lv1/a;->p()Z

    move-result v0

    const/4 v1, 0x6

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    invoke-virtual {v0, v1}, Lv1/a;->l(I)I

    move-result v0

    add-int/lit8 v0, v0, 0xc

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    const/4 v1, 0x5

    :goto_0
    invoke-virtual {v0, v1}, Lv1/a;->l(I)I

    move-result v0

    :goto_1
    return v0
.end method

.method public getSpinnersShown()Z
    .locals 1

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isShown()Z

    move-result v0

    return v0
.end method

.method public getYear()I
    .locals 2

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    iget-boolean v1, p0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Lv1/a;->l(I)I

    move-result v0

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->f:Z

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object p1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-direct {p0, p1}, Lmiuix/pickerwidget/widget/DatePicker;->setCurrentLocale(Ljava/util/Locale;)V

    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    const-class v0, Lmiuix/pickerwidget/widget/DatePicker;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    const-class v0, Lmiuix/pickerwidget/widget/DatePicker;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    iget-wide v1, v1, Lv1/a;->f:J

    const/16 v3, 0x380

    invoke-static {v0, v1, v2, v3}, Lv1/c;->a(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3

    check-cast p1, Lmiuix/pickerwidget/widget/DatePicker$b;

    invoke-virtual {p1}, Landroid/view/View$BaseSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget v0, p1, Lmiuix/pickerwidget/widget/DatePicker$b;->d:I

    iget v1, p1, Lmiuix/pickerwidget/widget/DatePicker$b;->c:I

    iget v2, p1, Lmiuix/pickerwidget/widget/DatePicker$b;->a:I

    invoke-virtual {p0, v0, v1, v2}, Lmiuix/pickerwidget/widget/DatePicker;->d(III)V

    iget-boolean p1, p1, Lmiuix/pickerwidget/widget/DatePicker$b;->b:Z

    iput-boolean p1, p0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/DatePicker;->g()V

    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 8

    new-instance v7, Lmiuix/pickerwidget/widget/DatePicker$b;

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lv1/a;->l(I)I

    move-result v2

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Lv1/a;->l(I)I

    move-result v3

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    const/16 v4, 0x9

    invoke-virtual {v0, v4}, Lv1/a;->l(I)I

    move-result v4

    iget-boolean v5, p0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    const/4 v6, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lmiuix/pickerwidget/widget/DatePicker$b;-><init>(Landroid/os/Parcelable;IIIZLy1/a;)V

    return-object v7
.end method

.method public setDateFormatOrder([C)V
    .locals 0

    iput-object p1, p0, Lmiuix/pickerwidget/widget/DatePicker;->d:[C

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/DatePicker;->b()V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    iget-boolean v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->f:Z

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->e:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->j:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->p:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    iput-boolean p1, p0, Lmiuix/pickerwidget/widget/DatePicker;->f:Z

    return-void
.end method

.method public setLunarMode(Z)V
    .locals 1

    iget-boolean v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lmiuix/pickerwidget/widget/DatePicker;->g:Z

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/DatePicker;->c()V

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/DatePicker;->g()V

    :cond_0
    return-void
.end method

.method public setMaxDate(J)V
    .locals 6

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->o:Lv1/a;

    iput-wide p1, v0, Lv1/a;->f:J

    invoke-virtual {v0}, Lv1/a;->d()V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->o:Lv1/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lv1/a;->l(I)I

    move-result v0

    iget-object v2, p0, Lmiuix/pickerwidget/widget/DatePicker;->h:Lv1/a;

    invoke-virtual {v2, v1}, Lv1/a;->l(I)I

    move-result v2

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->o:Lv1/a;

    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Lv1/a;->l(I)I

    move-result v0

    iget-object v3, p0, Lmiuix/pickerwidget/widget/DatePicker;->h:Lv1/a;

    invoke-virtual {v3, v2}, Lv1/a;->l(I)I

    move-result v2

    if-eq v0, v2, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->h:Lv1/a;

    iput-wide p1, v0, Lv1/a;->f:J

    invoke-virtual {v0}, Lv1/a;->d()V

    iget-object p1, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    iget-object p2, p0, Lmiuix/pickerwidget/widget/DatePicker;->h:Lv1/a;

    iget-wide v2, p1, Lv1/a;->f:J

    iget-wide v4, p2, Lv1/a;->f:J

    cmp-long p2, v2, v4

    if-lez p2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    iput-wide v4, p1, Lv1/a;->f:J

    invoke-virtual {p1}, Lv1/a;->d()V

    :cond_2
    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/DatePicker;->g()V

    return-void
.end method

.method public setMinDate(J)V
    .locals 6

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->o:Lv1/a;

    iput-wide p1, v0, Lv1/a;->f:J

    invoke-virtual {v0}, Lv1/a;->d()V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->o:Lv1/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lv1/a;->l(I)I

    move-result v0

    iget-object v2, p0, Lmiuix/pickerwidget/widget/DatePicker;->i:Lv1/a;

    invoke-virtual {v2, v1}, Lv1/a;->l(I)I

    move-result v2

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->o:Lv1/a;

    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Lv1/a;->l(I)I

    move-result v0

    iget-object v3, p0, Lmiuix/pickerwidget/widget/DatePicker;->i:Lv1/a;

    invoke-virtual {v3, v2}, Lv1/a;->l(I)I

    move-result v2

    if-eq v0, v2, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->i:Lv1/a;

    iput-wide p1, v0, Lv1/a;->f:J

    invoke-virtual {v0}, Lv1/a;->d()V

    iget-object p1, p0, Lmiuix/pickerwidget/widget/DatePicker;->a:Lv1/a;

    iget-object p2, p0, Lmiuix/pickerwidget/widget/DatePicker;->i:Lv1/a;

    iget-wide v2, p1, Lv1/a;->f:J

    iget-wide v4, p2, Lv1/a;->f:J

    cmp-long p2, v2, v4

    if-gez p2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    iput-wide v4, p1, Lv1/a;->f:J

    invoke-virtual {p1}, Lv1/a;->d()V

    :cond_2
    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/DatePicker;->g()V

    return-void
.end method

.method public setSpinnersShown(Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/pickerwidget/widget/DatePicker;->n:Landroid/widget/LinearLayout;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void
.end method
