.class public Lmiuix/pickerwidget/widget/NumberPicker$k;
.super Landroid/os/Handler;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/pickerwidget/widget/NumberPicker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "k"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/pickerwidget/widget/NumberPicker$k$a;
    }
.end annotation


# static fields
.field public static final a:Lmiuix/pickerwidget/widget/NumberPicker$k$a;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lmiuix/pickerwidget/widget/NumberPicker$k$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lmiuix/pickerwidget/widget/NumberPicker$k$a;-><init>(Ly1/b;)V

    sput-object v0, Lmiuix/pickerwidget/widget/NumberPicker$k;->a:Lmiuix/pickerwidget/widget/NumberPicker$k$a;

    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lmiuix/pickerwidget/widget/NumberPicker$k;->a:Lmiuix/pickerwidget/widget/NumberPicker$k$a;

    iget p1, p1, Landroid/os/Message;->arg1:I

    iget-object v1, v0, Lmiuix/pickerwidget/widget/NumberPicker$k$a;->b:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, v0, Lmiuix/pickerwidget/widget/NumberPicker$k$a;->b:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, v0, Lmiuix/pickerwidget/widget/NumberPicker$k$a;->d:Landroid/media/SoundPool;

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Landroid/media/SoundPool;->release()V

    const/4 p1, 0x0

    iput-object p1, v0, Lmiuix/pickerwidget/widget/NumberPicker$k$a;->d:Landroid/media/SoundPool;

    goto :goto_0

    :cond_1
    sget-object p1, Lmiuix/pickerwidget/widget/NumberPicker$k;->a:Lmiuix/pickerwidget/widget/NumberPicker$k$a;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p1, Lmiuix/pickerwidget/widget/NumberPicker$k$a;->d:Landroid/media/SoundPool;

    if-eqz v2, :cond_4

    iget-wide v3, p1, Lmiuix/pickerwidget/widget/NumberPicker$k$a;->a:J

    sub-long v3, v0, v3

    const-wide/16 v5, 0x32

    cmp-long v3, v3, v5

    if-lez v3, :cond_4

    iget v3, p1, Lmiuix/pickerwidget/widget/NumberPicker$k$a;->c:I

    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual/range {v2 .. v8}, Landroid/media/SoundPool;->play(IFFIIF)I

    iput-wide v0, p1, Lmiuix/pickerwidget/widget/NumberPicker$k$a;->a:J

    goto :goto_0

    :cond_2
    sget-object v0, Lmiuix/pickerwidget/widget/NumberPicker$k;->a:Lmiuix/pickerwidget/widget/NumberPicker$k$a;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/content/Context;

    iget p1, p1, Landroid/os/Message;->arg1:I

    iget-object v3, v0, Lmiuix/pickerwidget/widget/NumberPicker$k$a;->d:Landroid/media/SoundPool;

    if-nez v3, :cond_3

    new-instance v3, Landroid/media/SoundPool;

    const/4 v4, 0x0

    invoke-direct {v3, v1, v1, v4}, Landroid/media/SoundPool;-><init>(III)V

    iput-object v3, v0, Lmiuix/pickerwidget/widget/NumberPicker$k$a;->d:Landroid/media/SoundPool;

    const v4, 0x7f110001

    invoke-virtual {v3, v2, v4, v1}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v1

    iput v1, v0, Lmiuix/pickerwidget/widget/NumberPicker$k$a;->c:I

    :cond_3
    iget-object v0, v0, Lmiuix/pickerwidget/widget/NumberPicker$k$a;->b:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_4
    :goto_0
    return-void
.end method
