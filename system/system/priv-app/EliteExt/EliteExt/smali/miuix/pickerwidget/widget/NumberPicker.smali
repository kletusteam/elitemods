.class public Lmiuix/pickerwidget/widget/NumberPicker;
.super Landroid/widget/LinearLayout;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/pickerwidget/widget/NumberPicker$a;,
        Lmiuix/pickerwidget/widget/NumberPicker$b;,
        Lmiuix/pickerwidget/widget/NumberPicker$c;,
        Lmiuix/pickerwidget/widget/NumberPicker$CustomEditText;,
        Lmiuix/pickerwidget/widget/NumberPicker$d;,
        Lmiuix/pickerwidget/widget/NumberPicker$e;,
        Lmiuix/pickerwidget/widget/NumberPicker$f;,
        Lmiuix/pickerwidget/widget/NumberPicker$g;,
        Lmiuix/pickerwidget/widget/NumberPicker$h;,
        Lmiuix/pickerwidget/widget/NumberPicker$i;,
        Lmiuix/pickerwidget/widget/NumberPicker$j;,
        Lmiuix/pickerwidget/widget/NumberPicker$k;,
        Lmiuix/pickerwidget/widget/NumberPicker$k$a;
    }
.end annotation


# static fields
.field public static final v0:[C

.field public static final w0:Lmiuix/pickerwidget/widget/NumberPicker$d;

.field public static final x0:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public A:J

.field public B:F

.field public C:F

.field public D:I

.field public E:I

.field public F:J

.field public G:F

.field public final H:I

.field public I:I

.field public J:I

.field public K:I

.field public final L:I

.field public M:I

.field public final N:I

.field public O:I

.field public P:Ljava/lang/String;

.field public Q:Lmiuix/pickerwidget/widget/NumberPicker$g;

.field public R:Lmiuix/pickerwidget/widget/NumberPicker$h;

.field public S:I

.field public final T:Lmiuix/pickerwidget/widget/NumberPicker$i;

.field public U:I

.field public V:I

.field public final W:I

.field public a:I

.field public final a0:I

.field public b:I

.field public b0:I

.field public c:Lmiuix/pickerwidget/widget/NumberPicker$a;

.field public final c0:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Landroid/widget/Scroller;

.field public final d0:[I

.field public e:Lmiuix/pickerwidget/widget/NumberPicker$b;

.field public e0:I

.field public f:I

.field public final f0:Landroid/graphics/Paint;

.field public g:Lmiuix/pickerwidget/widget/NumberPicker$c;

.field public g0:Lmiuix/pickerwidget/widget/NumberPicker$j;

.field public final h:Z

.field public h0:Z

.field public i:I

.field public i0:Lmiuix/pickerwidget/widget/NumberPicker$k;

.field public j:Z

.field public j0:I

.field public k:F

.field public k0:I

.field public l:[Ljava/lang/String;

.field public l0:I

.field public final m:Landroid/widget/Scroller;

.field public final m0:I

.field public n:Lmiuix/pickerwidget/widget/NumberPicker$d;

.field public n0:I

.field public final o:Z

.field public o0:I

.field public final p:I

.field public p0:I

.field public q:Z

.field public q0:I

.field public r:Z

.field public r0:Ljava/lang/String;

.field public s:I

.field public s0:I

.field public final t:Landroid/widget/EditText;

.field public t0:Landroid/view/VelocityTracker;

.field public u:Ljava/lang/CharSequence;

.field public u0:Z

.field public v:Landroid/graphics/Paint;

.field public w:I

.field public x:I

.field public y:F

.field public z:F


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lmiuix/pickerwidget/widget/NumberPicker;->x0:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Lmiuix/pickerwidget/widget/NumberPicker$f;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lmiuix/pickerwidget/widget/NumberPicker$f;-><init>(I)V

    sput-object v0, Lmiuix/pickerwidget/widget/NumberPicker;->w0:Lmiuix/pickerwidget/widget/NumberPicker$d;

    const/16 v0, 0xa

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lmiuix/pickerwidget/widget/NumberPicker;->v0:[C

    return-void

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 11

    const v0, 0x7f0401a0

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v1, Lmiuix/pickerwidget/widget/NumberPicker;->x0:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    iput v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->p:I

    const/4 v1, 0x1

    iput v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->a:I

    const/4 v2, 0x2

    iput v2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->b:I

    const/16 v3, 0x190

    iput v3, p0, Lmiuix/pickerwidget/widget/NumberPicker;->J:I

    const-wide/16 v3, 0x12c

    iput-wide v3, p0, Lmiuix/pickerwidget/widget/NumberPicker;->F:J

    new-instance v3, Landroid/util/SparseArray;

    invoke-direct {v3}, Landroid/util/SparseArray;-><init>()V

    iput-object v3, p0, Lmiuix/pickerwidget/widget/NumberPicker;->c0:Landroid/util/SparseArray;

    const/4 v3, 0x3

    new-array v4, v3, [I

    iput-object v4, p0, Lmiuix/pickerwidget/widget/NumberPicker;->d0:[I

    const/high16 v4, -0x80000000

    iput v4, p0, Lmiuix/pickerwidget/widget/NumberPicker;->s:I

    const/4 v4, 0x0

    iput v4, p0, Lmiuix/pickerwidget/widget/NumberPicker;->V:I

    const/4 v5, -0x1

    iput v5, p0, Lmiuix/pickerwidget/widget/NumberPicker;->D:I

    const/high16 v6, 0x41400000    # 12.0f

    iput v6, p0, Lmiuix/pickerwidget/widget/NumberPicker;->y:F

    const v6, 0x3f4ccccd    # 0.8f

    iput v6, p0, Lmiuix/pickerwidget/widget/NumberPicker;->z:F

    const/high16 v6, 0x3f800000    # 1.0f

    iput v6, p0, Lmiuix/pickerwidget/widget/NumberPicker;->G:F

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v6, v6, Landroid/util/DisplayMetrics;->density:F

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f07016a

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v7

    iput v7, p0, Lmiuix/pickerwidget/widget/NumberPicker;->a:I

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f07016b

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v7

    iput v7, p0, Lmiuix/pickerwidget/widget/NumberPicker;->b:I

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v8

    sget-object v9, Landroidx/emoji2/text/l;->h0:[I

    const v10, 0x7f130265

    invoke-virtual {v8, p2, v9, v0, v10}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    invoke-virtual {p2, v2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->u:Ljava/lang/CharSequence;

    const v0, 0x7f070170

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    const/4 v8, 0x6

    invoke-virtual {p2, v8, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->n0:I

    const v0, 0x7f070173

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    const/4 v9, 0x7

    invoke-virtual {p2, v9, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->o0:I

    const v0, 0x7f07016d

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {p2, v3, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->x:I

    const v0, 0x7f0601c0

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p2, v4, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->j0:I

    const v0, 0x7f0601c4

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p2, v1, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->k0:I

    const v0, 0x7f0601c6

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    const/4 v3, 0x5

    invoke-virtual {p2, v3, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->w:I

    const v0, 0x7f07016c

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    const/4 v3, 0x4

    invoke-virtual {p2, v3, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->l0:I

    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    iget p2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->x:I

    iput p2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->S:I

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->h()V

    iput-boolean v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->o:Z

    const/high16 p2, 0x40000000    # 2.0f

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v1, p2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result p2

    float-to-int p2, p2

    iput p2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->W:I

    const/high16 p2, 0x42340000    # 45.0f

    mul-float/2addr p2, v6

    float-to-int p2, p2

    iput p2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->a0:I

    iput v5, p0, Lmiuix/pickerwidget/widget/NumberPicker;->L:I

    const/high16 p2, 0x434a0000    # 202.0f

    mul-float/2addr v6, p2

    float-to-int p2, v6

    iput p2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->H:I

    iput v5, p0, Lmiuix/pickerwidget/widget/NumberPicker;->N:I

    iput v5, p0, Lmiuix/pickerwidget/widget/NumberPicker;->J:I

    iput-boolean v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->h:Z

    new-instance p2, Lmiuix/pickerwidget/widget/NumberPicker$i;

    invoke-direct {p2, p0}, Lmiuix/pickerwidget/widget/NumberPicker$i;-><init>(Lmiuix/pickerwidget/widget/NumberPicker;)V

    iput-object p2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->T:Lmiuix/pickerwidget/widget/NumberPicker$i;

    invoke-virtual {p0, v4}, Landroid/widget/LinearLayout;->setWillNotDraw(Z)V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p2

    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/LayoutInflater;

    const v0, 0x7f0d004f

    invoke-virtual {p2, v0, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    const p2, 0x7f0a00b2

    invoke-virtual {p0, p2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/EditText;

    iput-object p2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->t:Landroid/widget/EditText;

    new-instance v0, Ly1/b;

    invoke-direct {v0, p0}, Ly1/b;-><init>(Lmiuix/pickerwidget/widget/NumberPicker;)V

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    new-array v0, v1, [Landroid/text/InputFilter;

    new-instance v6, Lmiuix/pickerwidget/widget/NumberPicker$e;

    invoke-direct {v6, p0}, Lmiuix/pickerwidget/widget/NumberPicker$e;-><init>(Lmiuix/pickerwidget/widget/NumberPicker;)V

    aput-object v6, v0, v4

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    invoke-virtual {p2, v2}, Landroid/widget/EditText;->setRawInputType(I)V

    invoke-virtual {p2, v8}, Landroid/widget/EditText;->setImeOptions(I)V

    invoke-virtual {p2, v3}, Landroid/widget/EditText;->setVisibility(I)V

    const v0, 0x800003

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setGravity(I)V

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setScaleX(F)V

    invoke-virtual {p2, v4}, Landroid/widget/EditText;->setSaveEnabled(Z)V

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->l0:I

    invoke-virtual {p2}, Landroid/widget/EditText;->getPaddingTop()I

    move-result v2

    iget v3, p0, Lmiuix/pickerwidget/widget/NumberPicker;->l0:I

    invoke-virtual {p2}, Landroid/widget/EditText;->getPaddingRight()I

    move-result v4

    invoke-virtual {p2, v0, v2, v3, v4}, Landroid/widget/EditText;->setPadding(IIII)V

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->q0:I

    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v0

    iput v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->O:I

    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result p1

    div-int/lit8 p1, p1, 0x8

    iput p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->K:I

    invoke-virtual {p2}, Landroid/widget/EditText;->getTextSize()F

    move-result p1

    float-to-int p1, p1

    iput p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->m0:I

    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v0, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->n0:I

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    invoke-virtual {p2}, Landroid/widget/EditText;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    invoke-virtual {p2}, Landroid/widget/EditText;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object p2

    sget-object v0, Landroid/widget/LinearLayout;->ENABLED_STATE_SET:[I

    invoke-virtual {p2, v0, v5}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    iput-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->f0:Landroid/graphics/Paint;

    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->v:Landroid/graphics/Paint;

    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->v:Landroid/graphics/Paint;

    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    iget-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->v:Landroid/graphics/Paint;

    iget p2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->w:I

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->v:Landroid/graphics/Paint;

    iget p2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->x:I

    int-to-float p2, p2

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setTextSize(F)V

    new-instance p1, Landroid/widget/Scroller;

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p2

    const/4 v0, 0x0

    invoke-direct {p1, p2, v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;Z)V

    iput-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->m:Landroid/widget/Scroller;

    new-instance p1, Landroid/widget/Scroller;

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p2

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x40200000    # 2.5f

    invoke-direct {v0, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-direct {p1, p2, v0}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->d:Landroid/widget/Scroller;

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->r()Z

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getImportantForAccessibility()I

    move-result p1

    if-nez p1, :cond_0

    invoke-virtual {p0, v1}, Landroid/widget/LinearLayout;->setImportantForAccessibility(I)V

    :cond_0
    return-void
.end method

.method private getLabelWidth()F
    .locals 2

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->u:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->j()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->v:Landroid/graphics/Paint;

    iget-object v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->u:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 13

    iget-boolean v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->o:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->t:Landroid/widget/EditText;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->m:Landroid/widget/Scroller;

    invoke-virtual {p0, v0}, Lmiuix/pickerwidget/widget/NumberPicker;->l(Landroid/widget/Scroller;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->d:Landroid/widget/Scroller;

    invoke-virtual {p0, v0}, Lmiuix/pickerwidget/widget/NumberPicker;->l(Landroid/widget/Scroller;)Z

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->U:I

    if-eqz p1, :cond_1

    iget-object v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->m:Landroid/widget/Scroller;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->b0:I

    neg-int v5, p1

    const/16 v6, 0x12c

    invoke-virtual/range {v1 .. v6}, Landroid/widget/Scroller;->startScroll(IIIII)V

    goto :goto_0

    :cond_1
    iget-object v7, p0, Lmiuix/pickerwidget/widget/NumberPicker;->m:Landroid/widget/Scroller;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    iget v11, p0, Lmiuix/pickerwidget/widget/NumberPicker;->b0:I

    const/16 v12, 0x12c

    invoke-virtual/range {v7 .. v12}, Landroid/widget/Scroller;->startScroll(IIIII)V

    :goto_0
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->invalidate()V

    goto :goto_2

    :cond_2
    const/4 v0, 0x1

    if-eqz p1, :cond_3

    iget p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->s0:I

    add-int/2addr p1, v0

    goto :goto_1

    :cond_3
    iget p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->s0:I

    sub-int/2addr p1, v0

    :goto_1
    invoke-virtual {p0, p1, v0}, Lmiuix/pickerwidget/widget/NumberPicker;->p(IZ)V

    :goto_2
    return-void
.end method

.method public final b(I)V
    .locals 3

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->c0:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v1, :cond_0

    return-void

    :cond_0
    iget v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->M:I

    if-lt p1, v1, :cond_3

    iget v2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->I:I

    if-le p1, v2, :cond_1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->l:[Ljava/lang/String;

    if-eqz v2, :cond_2

    sub-int v1, p1, v1

    aget-object v1, v2, v1

    goto :goto_1

    :cond_2
    invoke-virtual {p0, p1}, Lmiuix/pickerwidget/widget/NumberPicker;->d(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_3
    :goto_0
    const-string v1, ""

    :goto_1
    invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    return-void
.end method

.method public final c()Z
    .locals 7

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->s:I

    iget v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->i:I

    sub-int/2addr v0, v1

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    iput v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->U:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v1

    iget v2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->b0:I

    div-int/lit8 v3, v2, 0x2

    if-le v1, v3, :cond_1

    if-lez v0, :cond_0

    neg-int v2, v2

    :cond_0
    add-int/2addr v0, v2

    :cond_1
    move v5, v0

    iget-object v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->d:Landroid/widget/Scroller;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0x320

    invoke-virtual/range {v1 .. v6}, Landroid/widget/Scroller;->startScroll(IIIII)V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->invalidate()V

    const/4 v0, 0x1

    return v0

    :cond_2
    return v1
.end method

.method public computeScroll()V
    .locals 4

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->m:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->d:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    move-result v1

    iget v2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->U:I

    if-nez v2, :cond_1

    invoke-virtual {v0}, Landroid/widget/Scroller;->getStartY()I

    move-result v2

    iput v2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->U:I

    :cond_1
    iget v2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->U:I

    sub-int v2, v1, v2

    const/4 v3, 0x0

    invoke-virtual {p0, v3, v2}, Lmiuix/pickerwidget/widget/NumberPicker;->scrollBy(II)V

    iput v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->U:I

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->m:Landroid/widget/Scroller;

    if-ne v0, v1, :cond_3

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->c()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->r()Z

    :cond_2
    invoke-virtual {p0, v3}, Lmiuix/pickerwidget/widget/NumberPicker;->m(I)V

    goto :goto_0

    :cond_3
    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->V:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_5

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->r()Z

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->invalidate()V

    :cond_5
    :goto_0
    return-void
.end method

.method public final d(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->n:Lmiuix/pickerwidget/widget/NumberPicker$d;

    if-eqz v0, :cond_0

    check-cast v0, Lmiuix/pickerwidget/widget/NumberPicker$f;

    iget v0, v0, Lmiuix/pickerwidget/widget/NumberPicker$f;->a:I

    invoke-static {v0, p1}, Lw1/a;->a(II)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    invoke-static {v0, p1}, Lw1/a;->a(II)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    iget-boolean v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->o:Z

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1

    :cond_0
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iget v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->p0:I

    if-ge v0, v1, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    iget v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->f:I

    if-le v0, v1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result p1

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    move-result-object v1

    check-cast v1, Lmiuix/pickerwidget/widget/NumberPicker$a;

    const/4 v2, 0x7

    const/16 v3, 0x100

    const/4 v4, 0x0

    const/16 v5, 0x40

    const/16 v6, 0x80

    const/4 v7, -0x1

    if-eq p1, v2, :cond_5

    const/16 v2, 0x9

    if-eq p1, v2, :cond_4

    const/16 v2, 0xa

    if-eq p1, v2, :cond_3

    goto :goto_2

    :cond_3
    invoke-virtual {v1, v0, v3}, Lmiuix/pickerwidget/widget/NumberPicker$a;->h(II)V

    iput v7, p0, Lmiuix/pickerwidget/widget/NumberPicker;->E:I

    goto :goto_2

    :cond_4
    :goto_1
    invoke-virtual {v1, v0, v6}, Lmiuix/pickerwidget/widget/NumberPicker$a;->h(II)V

    iput v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->E:I

    invoke-virtual {v1, v0, v5, v4}, Lmiuix/pickerwidget/widget/NumberPicker$a;->performAction(IILandroid/os/Bundle;)Z

    goto :goto_2

    :cond_5
    iget p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->E:I

    if-eq p1, v0, :cond_6

    if-eq p1, v7, :cond_6

    invoke-virtual {v1, p1, v3}, Lmiuix/pickerwidget/widget/NumberPicker$a;->h(II)V

    goto :goto_1

    :cond_6
    :goto_2
    const/4 p1, 0x0

    return p1
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 5

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x14

    const/16 v2, 0x13

    if-eq v0, v2, :cond_1

    if-eq v0, v1, :cond_1

    const/16 v1, 0x17

    if-eq v0, v1, :cond_0

    const/16 v1, 0x42

    if-eq v0, v1, :cond_0

    goto :goto_3

    :cond_0
    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->o()V

    goto :goto_3

    :cond_1
    iget-boolean v2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->o:Z

    if-nez v2, :cond_2

    goto :goto_3

    :cond_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_4

    if-eq v2, v3, :cond_3

    goto :goto_3

    :cond_3
    iget v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->D:I

    if-ne v1, v0, :cond_9

    const/4 p1, -0x1

    iput p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->D:I

    return v3

    :cond_4
    iget-boolean v2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->u0:Z

    if-nez v2, :cond_6

    if-ne v0, v1, :cond_5

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->getValue()I

    move-result v2

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->getMinValue()I

    move-result v4

    if-le v2, v4, :cond_9

    goto :goto_1

    :cond_6
    :goto_0
    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->getValue()I

    move-result v2

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->getMaxValue()I

    move-result v4

    if-ge v2, v4, :cond_9

    :goto_1
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->requestFocus()Z

    iput v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->D:I

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->o()V

    iget-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->m:Landroid/widget/Scroller;

    invoke-virtual {p1}, Landroid/widget/Scroller;->isFinished()Z

    move-result p1

    if-eqz p1, :cond_8

    if-ne v0, v1, :cond_7

    move p1, v3

    goto :goto_2

    :cond_7
    const/4 p1, 0x0

    :goto_2
    invoke-virtual {p0, p1}, Lmiuix/pickerwidget/widget/NumberPicker;->a(Z)V

    :cond_8
    return v3

    :cond_9
    :goto_3
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 0

    invoke-virtual {p0, p1}, Lmiuix/pickerwidget/widget/NumberPicker;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    const/4 p1, 0x1

    return p1
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->o()V

    :goto_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public dispatchTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->o()V

    :goto_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchTrackballEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public drawableStateChanged()V
    .locals 0

    invoke-super {p0}, Landroid/widget/LinearLayout;->drawableStateChanged()V

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->q()V

    return-void
.end method

.method public final e(FIZ)I
    .locals 1

    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    return p2

    :cond_0
    if-eqz p3, :cond_1

    neg-float p1, p1

    invoke-static {p2}, Landroid/graphics/Color;->alpha(I)I

    move-result p3

    int-to-float p3, p3

    mul-float/2addr p1, p3

    invoke-static {p2}, Landroid/graphics/Color;->alpha(I)I

    move-result p3

    int-to-float p3, p3

    add-float/2addr p1, p3

    goto :goto_0

    :cond_1
    invoke-static {p2}, Landroid/graphics/Color;->alpha(I)I

    move-result p3

    int-to-float p3, p3

    mul-float/2addr p1, p3

    :goto_0
    float-to-int p1, p1

    shl-int/lit8 p1, p1, 0x18

    const p3, 0xffffff

    and-int/2addr p2, p3

    or-int/2addr p1, p2

    return p1
.end method

.method public final f(Ljava/lang/String;)I
    .locals 2

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->l:[Ljava/lang/String;

    if-nez v0, :cond_0

    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->l:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->l:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->M:I

    add-int/2addr p1, v0

    return p1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    :try_start_1
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    return p1

    :catch_0
    iget p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->M:I

    return p1
.end method

.method public final g(I)I
    .locals 2

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->I:I

    iget v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->M:I

    if-le p1, v0, :cond_0

    sub-int/2addr p1, v0

    sub-int/2addr v0, v1

    rem-int/2addr p1, v0

    add-int/2addr p1, v1

    add-int/lit8 p1, p1, -0x1

    return p1

    :cond_0
    if-ge p1, v1, :cond_1

    sub-int p1, v1, p1

    sub-int v1, v0, v1

    rem-int/2addr p1, v1

    sub-int/2addr v0, p1

    add-int/lit8 v0, v0, 0x1

    return v0

    :cond_1
    return p1
.end method

.method public getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;
    .locals 1

    iget-boolean v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->o:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/widget/LinearLayout;->getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->c:Lmiuix/pickerwidget/widget/NumberPicker$a;

    if-nez v0, :cond_1

    new-instance v0, Lmiuix/pickerwidget/widget/NumberPicker$a;

    invoke-direct {v0, p0}, Lmiuix/pickerwidget/widget/NumberPicker$a;-><init>(Lmiuix/pickerwidget/widget/NumberPicker;)V

    iput-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->c:Lmiuix/pickerwidget/widget/NumberPicker$a;

    :cond_1
    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->c:Lmiuix/pickerwidget/widget/NumberPicker$a;

    return-object v0
.end method

.method public getBottomFadingEdgeStrength()F
    .locals 1

    const v0, 0x3f666666    # 0.9f

    return v0
.end method

.method public getDisplayedValues()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->l:[Ljava/lang/String;

    return-object v0
.end method

.method public getMaxValue()I
    .locals 1

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->I:I

    return v0
.end method

.method public getMinValue()I
    .locals 1

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->M:I

    return v0
.end method

.method public getTopFadingEdgeStrength()F
    .locals 1

    const v0, 0x3f666666    # 0.9f

    return v0
.end method

.method public getValue()I
    .locals 1

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->s0:I

    return v0
.end method

.method public getWrapSelectorWheel()Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->u0:Z

    return v0
.end method

.method public final h()V
    .locals 5

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->i0:Lmiuix/pickerwidget/widget/NumberPicker$k;

    if-nez v0, :cond_1

    new-instance v0, Lmiuix/pickerwidget/widget/NumberPicker$k;

    const-string v1, "NumberPicker_sound_play"

    sget-object v2, Lx1/a;->a:Ljava/util/Map;

    const-class v2, Lx1/a;

    monitor-enter v2

    :try_start_0
    sget-object v3, Lx1/a;->a:Ljava/util/Map;

    move-object v4, v3

    check-cast v4, Landroid/util/ArrayMap;

    invoke-virtual {v4, v1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lx1/a$a;

    if-nez v4, :cond_0

    new-instance v4, Lx1/a$a;

    invoke-direct {v4, v1}, Lx1/a$a;-><init>(Ljava/lang/String;)V

    check-cast v3, Landroid/util/ArrayMap;

    invoke-virtual {v3, v1, v4}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget v1, v4, Lx1/a$a;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v4, Lx1/a$a;->a:I

    :goto_0
    iget-object v1, v4, Lx1/a$a;->b:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    invoke-direct {v0, v1}, Lmiuix/pickerwidget/widget/NumberPicker$k;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->i0:Lmiuix/pickerwidget/widget/NumberPicker$k;

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->p:I

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v2, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    iput-object v1, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    :goto_1
    return-void
.end method

.method public final i()V
    .locals 5

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->c0:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->d0:[I

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->getValue()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p0, Lmiuix/pickerwidget/widget/NumberPicker;->d0:[I

    array-length v3, v3

    if-ge v2, v3, :cond_1

    add-int/lit8 v3, v2, -0x1

    add-int/2addr v3, v1

    iget-boolean v4, p0, Lmiuix/pickerwidget/widget/NumberPicker;->u0:Z

    if-eqz v4, :cond_0

    invoke-virtual {p0, v3}, Lmiuix/pickerwidget/widget/NumberPicker;->g(I)I

    move-result v3

    :cond_0
    aput v3, v0, v2

    aget v3, v0, v2

    invoke-virtual {p0, v3}, Lmiuix/pickerwidget/widget/NumberPicker;->b(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final j()Z
    .locals 7

    const-class v0, Ljava/lang/String;

    iget-object v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->P:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string v1, "android.os.SystemProperties"

    invoke-static {v1}, Landroidx/emoji2/text/l;->q(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const/4 v2, 0x2

    new-array v3, v2, [Ljava/lang/Class;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v5, 0x1

    aput-object v0, v3, v5

    new-array v2, v2, [Ljava/lang/Object;

    const-string v6, "ro.product.mod_device"

    aput-object v6, v2, v4

    const-string v4, ""

    aput-object v4, v2, v5

    const-string v4, "get"

    invoke-static {v1, v0, v4, v3, v2}, Landroidx/emoji2/text/l;->h(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->P:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->P:Ljava/lang/String;

    const-string v1, "_global"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final k(II)I
    .locals 4

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    return p1

    :cond_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    const/high16 v2, -0x80000000

    const/high16 v3, 0x40000000    # 2.0f

    if-eq v1, v2, :cond_3

    if-eqz v1, :cond_2

    if-ne v1, v3, :cond_1

    return p1

    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Unknown measure mode: "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_2
    invoke-static {p2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    return p1

    :cond_3
    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result p1

    invoke-static {p1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    return p1
.end method

.method public final l(Landroid/widget/Scroller;)Z
    .locals 6

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/Scroller;->forceFinished(Z)V

    invoke-virtual {p1}, Landroid/widget/Scroller;->getFinalY()I

    move-result v1

    invoke-virtual {p1}, Landroid/widget/Scroller;->getCurrY()I

    move-result p1

    sub-int/2addr v1, p1

    iget p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->i:I

    iget v2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->b0:I

    iget v3, p0, Lmiuix/pickerwidget/widget/NumberPicker;->s:I

    add-int/2addr p1, v1

    rem-int/2addr p1, v2

    sub-int/2addr v3, p1

    const/4 p1, 0x0

    if-eqz v3, :cond_2

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v2

    iget v4, p0, Lmiuix/pickerwidget/widget/NumberPicker;->b0:I

    div-int/lit8 v5, v4, 0x2

    if-le v2, v5, :cond_1

    if-lez v3, :cond_0

    sub-int/2addr v3, v4

    goto :goto_0

    :cond_0
    add-int/2addr v3, v4

    :cond_1
    :goto_0
    add-int/2addr v1, v3

    invoke-virtual {p0, p1, v1}, Lmiuix/pickerwidget/widget/NumberPicker;->scrollBy(II)V

    return v0

    :cond_2
    return p1
.end method

.method public final m(I)V
    .locals 2

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->V:I

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_2

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->r0:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->t:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->t:Landroid/widget/EditText;

    iget-object v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->r0:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->r0:Ljava/lang/String;

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->i0:Lmiuix/pickerwidget/widget/NumberPicker$k;

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_2
    iput p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->V:I

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->Q:Lmiuix/pickerwidget/widget/NumberPicker$g;

    if-eqz v0, :cond_3

    invoke-interface {v0, p0, p1}, Lmiuix/pickerwidget/widget/NumberPicker$g;->a(Lmiuix/pickerwidget/widget/NumberPicker;I)V

    :cond_3
    return-void
.end method

.method public final n(ZJ)V
    .locals 1

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->g:Lmiuix/pickerwidget/widget/NumberPicker$c;

    if-nez v0, :cond_0

    new-instance v0, Lmiuix/pickerwidget/widget/NumberPicker$c;

    invoke-direct {v0, p0}, Lmiuix/pickerwidget/widget/NumberPicker$c;-><init>(Lmiuix/pickerwidget/widget/NumberPicker;)V

    iput-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->g:Lmiuix/pickerwidget/widget/NumberPicker$c;

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    :goto_0
    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->g:Lmiuix/pickerwidget/widget/NumberPicker$c;

    iput-boolean p1, v0, Lmiuix/pickerwidget/widget/NumberPicker$c;->a:Z

    invoke-virtual {p0, v0, p2, p3}, Landroid/widget/LinearLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public final o()V
    .locals 1

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->g:Lmiuix/pickerwidget/widget/NumberPicker$c;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_0
    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->g0:Lmiuix/pickerwidget/widget/NumberPicker$j;

    if-eqz v0, :cond_1

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_1
    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->e:Lmiuix/pickerwidget/widget/NumberPicker$b;

    if-eqz v0, :cond_2

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_2
    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->T:Lmiuix/pickerwidget/widget/NumberPicker$i;

    invoke-virtual {v0}, Lmiuix/pickerwidget/widget/NumberPicker$i;->a()V

    return-void
.end method

.method public onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->h()V

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 5

    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->i0:Lmiuix/pickerwidget/widget/NumberPicker$k;

    if-eqz v0, :cond_0

    iget v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->p:I

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->i0:Lmiuix/pickerwidget/widget/NumberPicker$k;

    :cond_0
    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->o()V

    const-string v0, "NumberPicker_sound_play"

    sget-object v1, Lx1/a;->a:Ljava/util/Map;

    const-class v1, Lx1/a;

    monitor-enter v1

    :try_start_0
    sget-object v2, Lx1/a;->a:Ljava/util/Map;

    move-object v3, v2

    check-cast v3, Landroid/util/ArrayMap;

    invoke-virtual {v3, v0}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lx1/a$a;

    if-eqz v3, :cond_1

    iget v4, v3, Lx1/a$a;->a:I

    add-int/lit8 v4, v4, -0x1

    iput v4, v3, Lx1/a$a;->a:I

    if-nez v4, :cond_1

    check-cast v2, Landroid/util/ArrayMap;

    invoke-virtual {v2, v0}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v3, Lx1/a$a;->b:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quitSafely()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 14

    iget-boolean v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->o:Z

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v1

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getRight()I

    move-result v2

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getLeft()I

    move-result v3

    sub-int/2addr v2, v3

    add-int/2addr v2, v0

    sub-int/2addr v2, v1

    div-int/lit8 v2, v2, 0x2

    int-to-float v0, v2

    iget v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->s:I

    iget v2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->b0:I

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v2, v1

    int-to-float v1, v2

    iget v2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->i:I

    int-to-float v2, v2

    iget-object v3, p0, Lmiuix/pickerwidget/widget/NumberPicker;->c0:Landroid/util/SparseArray;

    iget-object v4, p0, Lmiuix/pickerwidget/widget/NumberPicker;->d0:[I

    const/4 v5, 0x0

    move v6, v5

    :goto_0
    array-length v7, v4

    const/high16 v8, 0x40000000    # 2.0f

    if-ge v6, v7, :cond_3

    aget v7, v4, v6

    invoke-virtual {v3, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    sub-float v9, v1, v2

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    iget v10, p0, Lmiuix/pickerwidget/widget/NumberPicker;->b0:I

    int-to-float v10, v10

    div-float/2addr v9, v10

    iget v10, p0, Lmiuix/pickerwidget/widget/NumberPicker;->n0:I

    iget v11, p0, Lmiuix/pickerwidget/widget/NumberPicker;->o0:I

    const/high16 v12, 0x3f800000    # 1.0f

    cmpl-float v13, v9, v12

    if-ltz v13, :cond_1

    int-to-float v10, v11

    goto :goto_1

    :cond_1
    sub-int/2addr v11, v10

    int-to-float v11, v11

    mul-float/2addr v11, v9

    int-to-float v10, v10

    add-float/2addr v10, v11

    :goto_1
    iget-object v11, p0, Lmiuix/pickerwidget/widget/NumberPicker;->f0:Landroid/graphics/Paint;

    invoke-virtual {v11, v10}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v11, p0, Lmiuix/pickerwidget/widget/NumberPicker;->f0:Landroid/graphics/Paint;

    iget v13, p0, Lmiuix/pickerwidget/widget/NumberPicker;->k0:I

    invoke-virtual {p0, v9, v13, v5}, Lmiuix/pickerwidget/widget/NumberPicker;->e(FIZ)I

    move-result v13

    invoke-virtual {v11, v13}, Landroid/graphics/Paint;->setColor(I)V

    iget v11, p0, Lmiuix/pickerwidget/widget/NumberPicker;->o0:I

    int-to-float v11, v11

    invoke-static {v10, v11, v8, v2}, Landroidx/activity/result/a;->b(FFFF)F

    move-result v11

    iget-object v13, p0, Lmiuix/pickerwidget/widget/NumberPicker;->f0:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v0, v11, v13}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    cmpg-float v11, v9, v12

    if-gez v11, :cond_2

    iget-object v11, p0, Lmiuix/pickerwidget/widget/NumberPicker;->f0:Landroid/graphics/Paint;

    iget v12, p0, Lmiuix/pickerwidget/widget/NumberPicker;->j0:I

    const/4 v13, 0x1

    invoke-virtual {p0, v9, v12, v13}, Lmiuix/pickerwidget/widget/NumberPicker;->e(FIZ)I

    move-result v9

    invoke-virtual {v11, v9}, Landroid/graphics/Paint;->setColor(I)V

    iget v9, p0, Lmiuix/pickerwidget/widget/NumberPicker;->o0:I

    int-to-float v9, v9

    invoke-static {v10, v9, v8, v2}, Landroidx/activity/result/a;->b(FFFF)F

    move-result v8

    iget-object v9, p0, Lmiuix/pickerwidget/widget/NumberPicker;->f0:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v0, v8, v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_2
    iget v7, p0, Lmiuix/pickerwidget/widget/NumberPicker;->b0:I

    int-to-float v7, v7

    add-float/2addr v2, v7

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->u:Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->j()Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->v:Landroid/graphics/Paint;

    iget-object v3, p0, Lmiuix/pickerwidget/widget/NumberPicker;->u:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v2

    invoke-static {p0}, Lh/w0;->b(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget v3, p0, Lmiuix/pickerwidget/widget/NumberPicker;->k:F

    div-float/2addr v3, v8

    sub-float/2addr v0, v3

    iget v3, p0, Lmiuix/pickerwidget/widget/NumberPicker;->a:I

    int-to-float v3, v3

    sub-float/2addr v0, v3

    sub-float/2addr v0, v2

    const/4 v2, 0x0

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    goto :goto_2

    :cond_4
    iget v3, p0, Lmiuix/pickerwidget/widget/NumberPicker;->k:F

    div-float/2addr v3, v8

    add-float/2addr v3, v0

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->a:I

    int-to-float v0, v0

    add-float/2addr v3, v0

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, v2

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    :goto_2
    iget v2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->n0:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget v3, p0, Lmiuix/pickerwidget/widget/NumberPicker;->x:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget v4, p0, Lmiuix/pickerwidget/widget/NumberPicker;->b:I

    int-to-float v4, v4

    iget-object v5, p0, Lmiuix/pickerwidget/widget/NumberPicker;->u:Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    sub-float/2addr v1, v2

    add-float/2addr v1, v3

    add-float/2addr v1, v4

    iget-object v2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->v:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v0, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_5
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    const-class v0, Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setScrollable(Z)V

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->M:I

    iget v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->s0:I

    add-int/2addr v0, v1

    iget v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->b0:I

    mul-int/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setScrollY(I)V

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->I:I

    iget v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->M:I

    sub-int/2addr v0, v1

    iget v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->b0:I

    mul-int/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setMaxScrollY(I)V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    iget-boolean v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->o:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_3

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-eqz v0, :cond_1

    return v1

    :cond_1
    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->o()V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->t:Landroid/widget/EditText;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->B:F

    iput v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->C:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    iput-wide v2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->A:J

    iput-boolean v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->r:Z

    iput-boolean v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->h0:Z

    iget p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->B:F

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->p0:I

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    const/4 v2, 0x1

    if-gez v0, :cond_2

    iget p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->V:I

    if-nez p1, :cond_3

    iget-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->T:Lmiuix/pickerwidget/widget/NumberPicker$i;

    const/4 v0, 0x2

    invoke-virtual {p1}, Lmiuix/pickerwidget/widget/NumberPicker$i;->a()V

    iput v2, p1, Lmiuix/pickerwidget/widget/NumberPicker$i;->b:I

    iput v0, p1, Lmiuix/pickerwidget/widget/NumberPicker$i;->a:I

    goto :goto_0

    :cond_2
    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->f:I

    int-to-float v0, v0

    cmpl-float p1, p1, v0

    if-lez p1, :cond_3

    iget p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->V:I

    if-nez p1, :cond_3

    iget-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->T:Lmiuix/pickerwidget/widget/NumberPicker$i;

    invoke-virtual {p1}, Lmiuix/pickerwidget/widget/NumberPicker$i;->a()V

    iput v2, p1, Lmiuix/pickerwidget/widget/NumberPicker$i;->b:I

    iput v2, p1, Lmiuix/pickerwidget/widget/NumberPicker$i;->a:I

    :goto_0
    iget-object v0, p1, Lmiuix/pickerwidget/widget/NumberPicker$i;->c:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v0, p1, v3, v4}, Landroid/widget/LinearLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_3
    iget-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->m:Landroid/widget/Scroller;

    invoke-virtual {p1}, Landroid/widget/Scroller;->isFinished()Z

    move-result p1

    if-nez p1, :cond_4

    iget-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->m:Landroid/widget/Scroller;

    invoke-virtual {p1, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    iget-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->d:Landroid/widget/Scroller;

    invoke-virtual {p1, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    invoke-virtual {p0, v1}, Lmiuix/pickerwidget/widget/NumberPicker;->m(I)V

    goto :goto_2

    :cond_4
    iget-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->d:Landroid/widget/Scroller;

    invoke-virtual {p1}, Landroid/widget/Scroller;->isFinished()Z

    move-result p1

    if-nez p1, :cond_5

    iget-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->m:Landroid/widget/Scroller;

    invoke-virtual {p1, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    iget-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->d:Landroid/widget/Scroller;

    invoke-virtual {p1, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    goto :goto_2

    :cond_5
    iget p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->B:F

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->p0:I

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_6

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result p1

    int-to-long v3, p1

    invoke-virtual {p0, v1, v3, v4}, Lmiuix/pickerwidget/widget/NumberPicker;->n(ZJ)V

    goto :goto_2

    :cond_6
    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->f:I

    int-to-float v0, v0

    cmpl-float p1, p1, v0

    if-lez p1, :cond_7

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result p1

    int-to-long v0, p1

    invoke-virtual {p0, v2, v0, v1}, Lmiuix/pickerwidget/widget/NumberPicker;->n(ZJ)V

    goto :goto_2

    :cond_7
    iput-boolean v2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->h0:Z

    iget-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->e:Lmiuix/pickerwidget/widget/NumberPicker$b;

    if-nez p1, :cond_8

    new-instance p1, Lmiuix/pickerwidget/widget/NumberPicker$b;

    invoke-direct {p1, p0}, Lmiuix/pickerwidget/widget/NumberPicker$b;-><init>(Lmiuix/pickerwidget/widget/NumberPicker;)V

    iput-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->e:Lmiuix/pickerwidget/widget/NumberPicker$b;

    goto :goto_1

    :cond_8
    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    :goto_1
    iget-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->e:Lmiuix/pickerwidget/widget/NumberPicker$b;

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p0, p1, v0, v1}, Landroid/widget/LinearLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_2
    return v2

    :cond_9
    :goto_3
    return v1
.end method

.method public onLayout(ZIIII)V
    .locals 1

    iget-boolean v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->o:Z

    if-nez v0, :cond_0

    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result p2

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result p3

    iget-object p4, p0, Lmiuix/pickerwidget/widget/NumberPicker;->t:Landroid/widget/EditText;

    invoke-virtual {p4}, Landroid/widget/EditText;->getMeasuredWidth()I

    move-result p4

    iget-object p5, p0, Lmiuix/pickerwidget/widget/NumberPicker;->t:Landroid/widget/EditText;

    invoke-virtual {p5}, Landroid/widget/EditText;->getMeasuredHeight()I

    move-result p5

    sub-int/2addr p2, p4

    div-int/lit8 p2, p2, 0x2

    sub-int/2addr p3, p5

    div-int/lit8 p3, p3, 0x2

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->t:Landroid/widget/EditText;

    add-int/2addr p4, p2

    add-int/2addr p5, p3

    invoke-virtual {v0, p2, p3, p4, p5}, Landroid/widget/EditText;->layout(IIII)V

    const/4 p2, 0x0

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->i()V

    iget-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->d0:[I

    array-length p3, p1

    iget p4, p0, Lmiuix/pickerwidget/widget/NumberPicker;->m0:I

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getBottom()I

    move-result p5

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getTop()I

    move-result v0

    sub-int/2addr p5, v0

    mul-int/2addr p3, p4

    sub-int/2addr p5, p3

    int-to-float p3, p5

    cmpg-float p4, p3, p2

    if-gez p4, :cond_1

    move p3, p2

    :cond_1
    array-length p1, p1

    int-to-float p1, p1

    div-float/2addr p3, p1

    const/high16 p1, 0x3f000000    # 0.5f

    add-float/2addr p3, p1

    float-to-int p1, p3

    iput p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->e0:I

    iget p3, p0, Lmiuix/pickerwidget/widget/NumberPicker;->m0:I

    add-int/2addr p3, p1

    iput p3, p0, Lmiuix/pickerwidget/widget/NumberPicker;->b0:I

    iget-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->t:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getBaseline()I

    move-result p1

    iget-object p3, p0, Lmiuix/pickerwidget/widget/NumberPicker;->t:Landroid/widget/EditText;

    invoke-virtual {p3}, Landroid/widget/EditText;->getTop()I

    move-result p3

    add-int/2addr p3, p1

    iget p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->b0:I

    const/4 p4, 0x1

    mul-int/2addr p1, p4

    sub-int/2addr p3, p1

    iput p3, p0, Lmiuix/pickerwidget/widget/NumberPicker;->s:I

    iput p3, p0, Lmiuix/pickerwidget/widget/NumberPicker;->i:I

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->r()Z

    invoke-virtual {p0, p4}, Landroid/widget/LinearLayout;->setVerticalFadingEdgeEnabled(Z)V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getBottom()I

    move-result p1

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getTop()I

    move-result p3

    sub-int/2addr p1, p3

    iget p3, p0, Lmiuix/pickerwidget/widget/NumberPicker;->m0:I

    sub-int/2addr p1, p3

    div-int/lit8 p1, p1, 0x2

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->setFadingEdgeLength(I)V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result p1

    iget p3, p0, Lmiuix/pickerwidget/widget/NumberPicker;->a0:I

    sub-int/2addr p1, p3

    div-int/lit8 p1, p1, 0x2

    iget p4, p0, Lmiuix/pickerwidget/widget/NumberPicker;->W:I

    sub-int/2addr p1, p4

    iput p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->p0:I

    mul-int/lit8 p4, p4, 0x2

    add-int/2addr p4, p1

    add-int/2addr p4, p3

    iput p4, p0, Lmiuix/pickerwidget/widget/NumberPicker;->f:I

    :cond_2
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getRight()I

    move-result p1

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getLeft()I

    move-result p3

    sub-int/2addr p1, p3

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result p3

    add-int/2addr p3, p1

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result p1

    sub-int/2addr p3, p1

    div-int/lit8 p3, p3, 0x2

    int-to-float p1, p3

    invoke-direct {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->getLabelWidth()F

    move-result p3

    cmpl-float p2, p3, p2

    if-lez p2, :cond_3

    iget p2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->S:I

    :goto_0
    iput p2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->x:I

    iget-object p3, p0, Lmiuix/pickerwidget/widget/NumberPicker;->v:Landroid/graphics/Paint;

    int-to-float p2, p2

    invoke-virtual {p3, p2}, Landroid/graphics/Paint;->setTextSize(F)V

    iget p2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->k:F

    const/high16 p3, 0x40000000    # 2.0f

    div-float/2addr p2, p3

    add-float/2addr p2, p1

    iget p3, p0, Lmiuix/pickerwidget/widget/NumberPicker;->a:I

    int-to-float p3, p3

    add-float/2addr p2, p3

    invoke-direct {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->getLabelWidth()F

    move-result p3

    add-float/2addr p2, p3

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getWidth()I

    move-result p3

    int-to-float p3, p3

    cmpl-float p2, p2, p3

    if-lez p2, :cond_3

    iget p2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->x:I

    int-to-float p2, p2

    iget p3, p0, Lmiuix/pickerwidget/widget/NumberPicker;->y:F

    cmpl-float p3, p2, p3

    if-lez p3, :cond_3

    iget p3, p0, Lmiuix/pickerwidget/widget/NumberPicker;->z:F

    mul-float/2addr p2, p3

    float-to-int p2, p2

    goto :goto_0

    :cond_3
    return-void
.end method

.method public onMeasure(II)V
    .locals 4

    iget-boolean v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->o:Z

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    return-void

    :cond_0
    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->J:I

    invoke-virtual {p0, p1, v0}, Lmiuix/pickerwidget/widget/NumberPicker;->k(II)I

    move-result v0

    iget v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->H:I

    invoke-virtual {p0, p2, v1}, Lmiuix/pickerwidget/widget/NumberPicker;->k(II)I

    move-result v1

    invoke-super {p0, v0, v1}, Landroid/widget/LinearLayout;->onMeasure(II)V

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->N:I

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0, p1, v2}, Landroid/widget/LinearLayout;->resolveSizeAndState(III)I

    move-result v1

    :cond_1
    iget p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->L:I

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v0

    if-eq p1, v3, :cond_2

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    invoke-static {p1, p2, v2}, Landroid/widget/LinearLayout;->resolveSizeAndState(III)I

    move-result v0

    :cond_2
    invoke-virtual {p0, v1, v0}, Landroid/widget/LinearLayout;->setMeasuredDimension(II)V

    return-void
.end method

.method public onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->V:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->r0:Ljava/lang/String;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->t:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->u:Ljava/lang/CharSequence;

    if-nez v1, :cond_1

    const-string v1, ""

    :cond_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->isEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_10

    iget-boolean v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->o:Z

    if-nez v0, :cond_0

    goto/16 :goto_6

    :cond_0
    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->t0:Landroid/view/VelocityTracker;

    if-nez v0, :cond_1

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->t0:Landroid/view/VelocityTracker;

    :cond_1
    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->t0:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eq v0, v3, :cond_6

    if-eq v0, v2, :cond_2

    goto/16 :goto_5

    :cond_2
    iget-boolean v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->r:Z

    if-eqz v0, :cond_3

    goto/16 :goto_5

    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->V:I

    if-eq v0, v3, :cond_4

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->B:F

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-int v0, v0

    iget v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->q0:I

    if-le v0, v1, :cond_5

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->o()V

    invoke-virtual {p0, v3}, Lmiuix/pickerwidget/widget/NumberPicker;->m(I)V

    goto :goto_0

    :cond_4
    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->C:F

    sub-float v0, p1, v0

    float-to-int v0, v0

    invoke-virtual {p0, v1, v0}, Lmiuix/pickerwidget/widget/NumberPicker;->scrollBy(II)V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->invalidate()V

    :cond_5
    :goto_0
    iput p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->C:F

    goto/16 :goto_5

    :cond_6
    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->e:Lmiuix/pickerwidget/widget/NumberPicker$b;

    if-eqz v0, :cond_7

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_7
    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->g:Lmiuix/pickerwidget/widget/NumberPicker$c;

    if-eqz v0, :cond_8

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_8
    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->T:Lmiuix/pickerwidget/widget/NumberPicker$i;

    invoke-virtual {v0}, Lmiuix/pickerwidget/widget/NumberPicker$i;->a()V

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->t0:Landroid/view/VelocityTracker;

    const/16 v4, 0x3e8

    iget v5, p0, Lmiuix/pickerwidget/widget/NumberPicker;->K:I

    int-to-float v5, v5

    invoke-virtual {v0, v4, v5}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v4

    iget v5, p0, Lmiuix/pickerwidget/widget/NumberPicker;->K:I

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    if-lt v4, v5, :cond_9

    int-to-float v0, v0

    iget v4, p0, Lmiuix/pickerwidget/widget/NumberPicker;->G:F

    mul-float/2addr v0, v4

    float-to-int v0, v0

    :cond_9
    move v8, v0

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v4, p0, Lmiuix/pickerwidget/widget/NumberPicker;->O:I

    if-le v0, v4, :cond_b

    iput v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->U:I

    iget-object v4, p0, Lmiuix/pickerwidget/widget/NumberPicker;->m:Landroid/widget/Scroller;

    const/4 v5, 0x0

    if-lez v8, :cond_a

    goto :goto_1

    :cond_a
    const v1, 0x7fffffff

    :goto_1
    move v6, v1

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const v12, 0x7fffffff

    invoke-virtual/range {v4 .. v12}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->invalidate()V

    invoke-virtual {p0, v2}, Lmiuix/pickerwidget/widget/NumberPicker;->m(I)V

    goto :goto_4

    :cond_b
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    int-to-float v4, v0

    iget v5, p0, Lmiuix/pickerwidget/widget/NumberPicker;->B:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    iget-wide v7, p0, Lmiuix/pickerwidget/widget/NumberPicker;->A:J

    iget p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->q0:I

    if-gt v4, p1, :cond_e

    sub-long/2addr v5, v7

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result p1

    int-to-long v7, p1

    cmp-long p1, v5, v7

    if-gez p1, :cond_e

    iget-boolean p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->h0:Z

    if-eqz p1, :cond_c

    iput-boolean v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->h0:Z

    goto :goto_3

    :cond_c
    iget p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->b0:I

    div-int/2addr v0, p1

    sub-int/2addr v0, v3

    if-lez v0, :cond_d

    invoke-virtual {p0, v3}, Lmiuix/pickerwidget/widget/NumberPicker;->a(Z)V

    iget-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->T:Lmiuix/pickerwidget/widget/NumberPicker$i;

    invoke-virtual {p1}, Lmiuix/pickerwidget/widget/NumberPicker$i;->a()V

    iput v2, p1, Lmiuix/pickerwidget/widget/NumberPicker$i;->b:I

    iput v3, p1, Lmiuix/pickerwidget/widget/NumberPicker$i;->a:I

    goto :goto_2

    :cond_d
    if-gez v0, :cond_f

    invoke-virtual {p0, v1}, Lmiuix/pickerwidget/widget/NumberPicker;->a(Z)V

    iget-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->T:Lmiuix/pickerwidget/widget/NumberPicker$i;

    invoke-virtual {p1}, Lmiuix/pickerwidget/widget/NumberPicker$i;->a()V

    iput v2, p1, Lmiuix/pickerwidget/widget/NumberPicker$i;->b:I

    iput v2, p1, Lmiuix/pickerwidget/widget/NumberPicker$i;->a:I

    :goto_2
    iget-object v0, p1, Lmiuix/pickerwidget/widget/NumberPicker$i;->c:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    goto :goto_3

    :cond_e
    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->c()Z

    :cond_f
    :goto_3
    invoke-virtual {p0, v1}, Lmiuix/pickerwidget/widget/NumberPicker;->m(I)V

    :goto_4
    iget-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->t0:Landroid/view/VelocityTracker;

    invoke-virtual {p1}, Landroid/view/VelocityTracker;->recycle()V

    const/4 p1, 0x0

    iput-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->t0:Landroid/view/VelocityTracker;

    :goto_5
    return v3

    :cond_10
    :goto_6
    return v1
.end method

.method public final p(IZ)V
    .locals 1

    iget-boolean v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->u0:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lmiuix/pickerwidget/widget/NumberPicker;->g(I)I

    move-result p1

    goto :goto_0

    :cond_0
    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->M:I

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->I:I

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    :goto_0
    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->s0:I

    if-ne v0, p1, :cond_1

    return-void

    :cond_1
    iput p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->s0:I

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->r()Z

    if-eqz p2, :cond_3

    const/4 p1, 0x4

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->sendAccessibilityEvent(I)V

    iget-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->i0:Lmiuix/pickerwidget/widget/NumberPicker$k;

    if-eqz p1, :cond_2

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_2
    sget p1, Lmiuix/view/c;->h:I

    invoke-static {p0, p1}, Lmiuix/view/HapticCompat;->performHapticFeedback(Landroid/view/View;I)Z

    iget-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->R:Lmiuix/pickerwidget/widget/NumberPicker$h;

    if-eqz p1, :cond_3

    iget p2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->s0:I

    invoke-interface {p1, p0, v0, p2}, Lmiuix/pickerwidget/widget/NumberPicker$h;->a(Lmiuix/pickerwidget/widget/NumberPicker;II)V

    :cond_3
    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->i()V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->invalidate()V

    return-void
.end method

.method public final q()V
    .locals 5

    iget-boolean v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->h:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/high16 v0, -0x40800000    # -1.0f

    iget-object v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->f0:Landroid/graphics/Paint;

    iget v2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->n0:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->l:[Ljava/lang/String;

    const/4 v2, 0x0

    if-nez v1, :cond_3

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x9

    if-ge v2, v1, :cond_2

    iget-object v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->f0:Landroid/graphics/Paint;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    cmpl-float v3, v1, v0

    if-lez v3, :cond_1

    move v0, v1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    iget v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->I:I

    invoke-virtual {p0, v1}, Lmiuix/pickerwidget/widget/NumberPicker;->d(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    float-to-int v0, v1

    int-to-float v0, v0

    goto :goto_2

    :cond_3
    array-length v1, v1

    :goto_1
    if-ge v2, v1, :cond_5

    iget-object v3, p0, Lmiuix/pickerwidget/widget/NumberPicker;->l:[Ljava/lang/String;

    aget-object v3, v3, v2

    iget-object v4, p0, Lmiuix/pickerwidget/widget/NumberPicker;->f0:Landroid/graphics/Paint;

    invoke-virtual {v4, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v3

    cmpl-float v4, v3, v0

    if-lez v4, :cond_4

    move v0, v3

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    :goto_2
    iput v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->k:F

    iget-object v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->t:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->t:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getPaddingRight()I

    move-result v2

    add-int/2addr v2, v1

    int-to-float v1, v2

    add-float/2addr v0, v1

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    iget v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->J:I

    int-to-float v1, v1

    cmpl-float v1, v1, v0

    if-eqz v1, :cond_7

    iget v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->N:I

    int-to-float v2, v1

    cmpl-float v2, v0, v2

    if-lez v2, :cond_6

    float-to-int v0, v0

    iput v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->J:I

    goto :goto_3

    :cond_6
    iput v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->J:I

    :cond_7
    :goto_3
    return-void
.end method

.method public final r()Z
    .locals 3

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->l:[Ljava/lang/String;

    if-nez v0, :cond_0

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->s0:I

    invoke-virtual {p0, v0}, Lmiuix/pickerwidget/widget/NumberPicker;->d(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->s0:I

    iget v2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->M:I

    sub-int/2addr v1, v2

    aget-object v0, v0, v1

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    iget v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->V:I

    if-eqz v1, :cond_1

    iput-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->r0:Ljava/lang/String;

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->t:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->t:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_1
    const/4 v0, 0x1

    return v0

    :cond_3
    const/4 v0, 0x0

    return v0
.end method

.method public scrollBy(II)V
    .locals 4

    iget-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->d0:[I

    iget-boolean v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->u0:Z

    const/4 v1, 0x1

    if-nez v0, :cond_0

    if-lez p2, :cond_0

    aget v2, p1, v1

    iget v3, p0, Lmiuix/pickerwidget/widget/NumberPicker;->M:I

    if-gt v2, v3, :cond_0

    :goto_0
    iget p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->s:I

    iput p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->i:I

    return-void

    :cond_0
    if-nez v0, :cond_1

    if-gez p2, :cond_1

    aget v0, p1, v1

    iget v2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->I:I

    if-lt v0, v2, :cond_1

    goto :goto_0

    :cond_1
    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->i:I

    add-int/2addr v0, p2

    iput v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->i:I

    :cond_2
    :goto_1
    iget p2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->i:I

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->s:I

    sub-int v0, p2, v0

    iget v2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->e0:I

    const/4 v3, 0x0

    if-le v0, v2, :cond_5

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->b0:I

    sub-int/2addr p2, v0

    iput p2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->i:I

    array-length p2, p1

    sub-int/2addr p2, v1

    :goto_2
    if-lez p2, :cond_3

    add-int/lit8 v0, p2, -0x1

    aget v2, p1, v0

    aput v2, p1, p2

    move p2, v0

    goto :goto_2

    :cond_3
    aget p2, p1, v1

    sub-int/2addr p2, v1

    iget-boolean v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->u0:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->M:I

    if-ge p2, v0, :cond_4

    iget p2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->I:I

    :cond_4
    aput p2, p1, v3

    invoke-virtual {p0, p2}, Lmiuix/pickerwidget/widget/NumberPicker;->b(I)V

    aget p2, p1, v1

    invoke-virtual {p0, p2, v1}, Lmiuix/pickerwidget/widget/NumberPicker;->p(IZ)V

    iget-boolean p2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->u0:Z

    if-nez p2, :cond_2

    aget p2, p1, v1

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->M:I

    if-gt p2, v0, :cond_2

    iget p2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->s:I

    iput p2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->i:I

    goto :goto_1

    :cond_5
    :goto_3
    iget p2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->i:I

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->s:I

    sub-int v0, p2, v0

    iget v2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->e0:I

    neg-int v2, v2

    if-ge v0, v2, :cond_8

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->b0:I

    add-int/2addr p2, v0

    iput p2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->i:I

    move p2, v3

    :goto_4
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    if-ge p2, v0, :cond_6

    add-int/lit8 v0, p2, 0x1

    aget v2, p1, v0

    aput v2, p1, p2

    move p2, v0

    goto :goto_4

    :cond_6
    array-length p2, p1

    add-int/lit8 p2, p2, -0x2

    aget p2, p1, p2

    add-int/2addr p2, v1

    iget-boolean v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->u0:Z

    if-eqz v0, :cond_7

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->I:I

    if-le p2, v0, :cond_7

    iget p2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->M:I

    :cond_7
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aput p2, p1, v0

    invoke-virtual {p0, p2}, Lmiuix/pickerwidget/widget/NumberPicker;->b(I)V

    aget p2, p1, v1

    invoke-virtual {p0, p2, v1}, Lmiuix/pickerwidget/widget/NumberPicker;->p(IZ)V

    iget-boolean p2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->u0:Z

    if-nez p2, :cond_5

    aget p2, p1, v1

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->I:I

    if-lt p2, v0, :cond_5

    iget p2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->s:I

    iput p2, p0, Lmiuix/pickerwidget/widget/NumberPicker;->i:I

    goto :goto_3

    :cond_8
    return-void
.end method

.method public setDisplayedValues([Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->l:[Ljava/lang/String;

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    iput-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->l:[Ljava/lang/String;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->t:Landroid/widget/EditText;

    const v0, 0x80001

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->t:Landroid/widget/EditText;

    const/4 v0, 0x2

    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setRawInputType(I)V

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->r()Z

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->i()V

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->q()V

    return-void
.end method

.method public setFormatter(Lmiuix/pickerwidget/widget/NumberPicker$d;)V
    .locals 1

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->n:Lmiuix/pickerwidget/widget/NumberPicker$d;

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    iput-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->n:Lmiuix/pickerwidget/widget/NumberPicker$d;

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->i()V

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->r()Z

    return-void
.end method

.method public setLabel(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->u:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iput-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->u:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->invalidate()V

    :cond_2
    return-void
.end method

.method public setLabelTextSizeThreshold(F)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    move-result p1

    iput p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->y:F

    return-void
.end method

.method public setLabelTextSizeTrimFactor(F)V
    .locals 1

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    iput p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->z:F

    :cond_0
    return-void
.end method

.method public setMaxFlingSpeedFactor(F)V
    .locals 1

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    iput p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->G:F

    :cond_0
    return-void
.end method

.method public setMaxValue(I)V
    .locals 1

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->I:I

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    if-ltz p1, :cond_3

    iput p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->I:I

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->s0:I

    if-ge p1, v0, :cond_1

    iput p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->s0:I

    :cond_1
    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->M:I

    sub-int/2addr p1, v0

    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->d0:[I

    array-length v0, v0

    if-le p1, v0, :cond_2

    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p0, p1}, Lmiuix/pickerwidget/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->i()V

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->r()Z

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->q()V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->invalidate()V

    return-void

    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "maxValue must be >= 0"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setMinValue(I)V
    .locals 1

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->M:I

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    if-ltz p1, :cond_3

    iput p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->M:I

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->s0:I

    if-le p1, v0, :cond_1

    iput p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->s0:I

    :cond_1
    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->I:I

    sub-int/2addr v0, p1

    iget-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->d0:[I

    array-length p1, p1

    if-le v0, p1, :cond_2

    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p0, p1}, Lmiuix/pickerwidget/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->i()V

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->r()Z

    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->q()V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->invalidate()V

    return-void

    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "minValue must be >= 0"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setOnLongPressUpdateInterval(J)V
    .locals 0

    iput-wide p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->F:J

    return-void
.end method

.method public setOnScrollListener(Lmiuix/pickerwidget/widget/NumberPicker$g;)V
    .locals 0

    iput-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->Q:Lmiuix/pickerwidget/widget/NumberPicker$g;

    return-void
.end method

.method public setOnValueChangedListener(Lmiuix/pickerwidget/widget/NumberPicker$h;)V
    .locals 0

    iput-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->R:Lmiuix/pickerwidget/widget/NumberPicker$h;

    return-void
.end method

.method public setValue(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lmiuix/pickerwidget/widget/NumberPicker;->p(IZ)V

    return-void
.end method

.method public setWrapSelectorWheel(Z)V
    .locals 2

    iget v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->I:I

    iget v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->M:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->d0:[I

    array-length v1, v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz p1, :cond_1

    if-eqz v0, :cond_2

    :cond_1
    iget-boolean v0, p0, Lmiuix/pickerwidget/widget/NumberPicker;->u0:Z

    if-eq p1, v0, :cond_2

    iput-boolean p1, p0, Lmiuix/pickerwidget/widget/NumberPicker;->u0:Z

    :cond_2
    invoke-virtual {p0}, Lmiuix/pickerwidget/widget/NumberPicker;->i()V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->invalidate()V

    return-void
.end method
