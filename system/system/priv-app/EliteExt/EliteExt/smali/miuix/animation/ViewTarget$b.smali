.class public Lmiuix/animation/ViewTarget$b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiuix/animation/ViewTarget;->b(Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lmiuix/animation/ViewTarget;

.field public final synthetic b:Ljava/lang/Runnable;

.field public final synthetic c:Landroid/view/View;


# direct methods
.method public constructor <init>(Lmiuix/animation/ViewTarget;Landroid/view/View;Ljava/lang/Runnable;)V
    .locals 0

    iput-object p1, p0, Lmiuix/animation/ViewTarget$b;->a:Lmiuix/animation/ViewTarget;

    iput-object p2, p0, Lmiuix/animation/ViewTarget$b;->c:Landroid/view/View;

    iput-object p3, p0, Lmiuix/animation/ViewTarget$b;->b:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    iget-object v0, p0, Lmiuix/animation/ViewTarget$b;->a:Lmiuix/animation/ViewTarget;

    iget-object v1, p0, Lmiuix/animation/ViewTarget$b;->c:Landroid/view/View;

    iget-object v2, p0, Lmiuix/animation/ViewTarget$b;->b:Ljava/lang/Runnable;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v3, v0, Landroid/view/ViewGroup;

    if-eqz v3, :cond_1

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const v4, 0x7f0a00a3

    invoke-virtual {v1, v4, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLeft()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTop()I

    move-result v5

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v6

    const/16 v7, 0x8

    if-ne v6, v7, :cond_0

    const/4 v7, 0x4

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v7

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v8

    invoke-virtual {v0, v7, v8}, Landroid/view/ViewGroup;->measure(II)V

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v7

    add-int/2addr v7, v3

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v8

    add-int/2addr v8, v5

    invoke-virtual {v0, v3, v5, v7, v8}, Landroid/view/ViewGroup;->layout(IIII)V

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    const/4 v0, 0x0

    invoke-virtual {v1, v4, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    :cond_1
    return-void
.end method
