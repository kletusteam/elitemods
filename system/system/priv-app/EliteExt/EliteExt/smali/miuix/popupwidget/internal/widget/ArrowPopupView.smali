.class public Lmiuix/popupwidget/internal/widget/ArrowPopupView;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/popupwidget/internal/widget/ArrowPopupView$a;
    }
.end annotation


# static fields
.field public static final synthetic H:I


# instance fields
.field public A:I

.field public B:Landroid/graphics/drawable/Drawable;

.field public C:Landroid/widget/LinearLayout;

.field public D:Landroidx/appcompat/widget/AppCompatTextView;

.field public E:Landroid/graphics/Rect;

.field public F:Landroid/view/View$OnTouchListener;

.field public G:I

.field public a:Landroid/view/View;

.field public b:Landroidx/appcompat/widget/AppCompatImageView;

.field public c:Landroid/graphics/drawable/Drawable;

.field public d:Landroid/graphics/drawable/Drawable;

.field public e:Landroid/graphics/drawable/Drawable;

.field public f:Landroid/graphics/drawable/Drawable;

.field public g:I

.field public h:Landroid/graphics/drawable/Drawable;

.field public i:I

.field public j:I

.field public k:Landroid/graphics/drawable/Drawable;

.field public l:Landroid/graphics/drawable/Drawable;

.field public m:Landroid/graphics/drawable/Drawable;

.field public n:Landroid/graphics/drawable/Drawable;

.field public o:Z

.field public p:Landroid/graphics/drawable/Drawable;

.field public q:Landroid/graphics/drawable/Drawable;

.field public r:Landroid/graphics/drawable/Drawable;

.field public s:Landroid/widget/FrameLayout;

.field public t:Landroid/widget/LinearLayout;

.field public u:I

.field public v:Landroidx/appcompat/widget/AppCompatButton;

.field public w:Lmiuix/popupwidget/internal/widget/ArrowPopupView$a;

.field public x:I

.field public y:Landroidx/appcompat/widget/AppCompatButton;

.field public z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    const v0, 0x7f040069

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->E:Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->o:Z

    const/4 v2, 0x0

    iput v2, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->g:I

    invoke-static {p0, v2}, Landroidx/emoji2/text/l;->S(Landroid/view/View;Z)V

    iput-boolean v1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->o:Z

    sget-object v3, Landroidx/emoji2/text/l;->X:[I

    const v4, 0x7f130212

    invoke-virtual {p1, p2, v3, v0, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    const/4 v0, 0x6

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->q:Landroid/graphics/drawable/Drawable;

    const/4 v0, 0x2

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->r:Landroid/graphics/drawable/Drawable;

    const/16 v0, 0x9

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->B:Landroid/graphics/drawable/Drawable;

    const/16 v0, 0xa

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->k:Landroid/graphics/drawable/Drawable;

    const/16 v0, 0xb

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->n:Landroid/graphics/drawable/Drawable;

    const/4 v0, 0x3

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->c:Landroid/graphics/drawable/Drawable;

    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->h:Landroid/graphics/drawable/Drawable;

    const/4 v0, 0x7

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->f:Landroid/graphics/drawable/Drawable;

    const/16 v0, 0xc

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->l:Landroid/graphics/drawable/Drawable;

    const/16 v0, 0xd

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->m:Landroid/graphics/drawable/Drawable;

    const/4 v0, 0x5

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->e:Landroid/graphics/drawable/Drawable;

    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0700c6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p2, v2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0700c8

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p1

    iput p1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->u:I

    return-void
.end method

.method private getArrowHeight()I
    .locals 2

    iget v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->g:I

    const/16 v1, 0x9

    if-eq v0, v1, :cond_1

    const/16 v1, 0xa

    if-eq v0, v1, :cond_1

    const/16 v1, 0x11

    if-eq v0, v1, :cond_0

    const/16 v1, 0x12

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->b:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->b:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->c:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->k:Landroid/graphics/drawable/Drawable;

    :goto_0
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    :cond_2
    return v0
.end method

.method private getArrowWidth()I
    .locals 1

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->b:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->b:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    :cond_0
    return v0
.end method


# virtual methods
.method public final a(I)Z
    .locals 1

    iget v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->g:I

    and-int/2addr v0, p1

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final b()Z
    .locals 1

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->a(I)Z

    move-result v0

    return v0
.end method

.method public final c(I)V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/16 v2, 0x20

    if-eq p1, v2, :cond_d

    const/16 v2, 0x40

    if-eq p1, v2, :cond_9

    packed-switch p1, :pswitch_data_0

    packed-switch p1, :pswitch_data_1

    goto/16 :goto_4

    :pswitch_0
    iget-object p1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->b:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {p0}, Landroid/view/View;->getLayoutDirection()I

    move-result v2

    if-ne v2, v1, :cond_0

    move v0, v1

    :cond_0
    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->l:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_3

    :cond_2
    :goto_0
    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->m:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_3

    :pswitch_1
    iget-object p1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->b:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {p0}, Landroid/view/View;->getLayoutDirection()I

    move-result v2

    if-ne v2, v1, :cond_3

    move v0, v1

    :cond_3
    if-eqz v0, :cond_1

    goto :goto_0

    :pswitch_2
    iget-object p1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->b:Landroidx/appcompat/widget/AppCompatImageView;

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->C:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->n:Landroid/graphics/drawable/Drawable;

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->k:Landroid/graphics/drawable/Drawable;

    goto :goto_3

    :pswitch_3
    iget-object p1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->b:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {p0}, Landroid/view/View;->getLayoutDirection()I

    move-result v2

    if-ne v2, v1, :cond_5

    move v0, v1

    :cond_5
    if-eqz v0, :cond_7

    :cond_6
    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->e:Landroid/graphics/drawable/Drawable;

    goto :goto_3

    :cond_7
    :goto_1
    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->d:Landroid/graphics/drawable/Drawable;

    goto :goto_3

    :pswitch_4
    iget-object p1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->b:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {p0}, Landroid/view/View;->getLayoutDirection()I

    move-result v2

    if-ne v2, v1, :cond_8

    move v0, v1

    :cond_8
    if-eqz v0, :cond_6

    goto :goto_1

    :pswitch_5
    iget-object p1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->b:Landroidx/appcompat/widget/AppCompatImageView;

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->c:Landroid/graphics/drawable/Drawable;

    goto :goto_3

    :cond_9
    iget-object p1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->b:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {p0}, Landroid/view/View;->getLayoutDirection()I

    move-result v2

    if-ne v2, v1, :cond_a

    move v0, v1

    :cond_a
    if-eqz v0, :cond_c

    :cond_b
    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->f:Landroid/graphics/drawable/Drawable;

    goto :goto_3

    :cond_c
    :goto_2
    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->h:Landroid/graphics/drawable/Drawable;

    goto :goto_3

    :cond_d
    iget-object p1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->b:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {p0}, Landroid/view/View;->getLayoutDirection()I

    move-result v2

    if-ne v2, v1, :cond_e

    move v0, v1

    :cond_e
    if-eqz v0, :cond_b

    goto :goto_2

    :goto_3
    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_4
    return-void

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x10
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public getArrowMode()I
    .locals 1

    iget v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->g:I

    return v0
.end method

.method public getContentView()Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->s:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getNegativeButton()Landroidx/appcompat/widget/AppCompatButton;
    .locals 1

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->v:Landroidx/appcompat/widget/AppCompatButton;

    return-object v0
.end method

.method public getPositiveButton()Landroidx/appcompat/widget/AppCompatButton;
    .locals 1

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->y:Landroidx/appcompat/widget/AppCompatButton;

    return-object v0
.end method

.method public getRollingPercent()F
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 14

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->p:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->z:I

    iget-object v1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v0

    iget v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->A:I

    iget-object v2, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v0

    iget v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->g:I

    const/16 v3, 0x40

    const/16 v4, 0x10

    const/16 v5, 0x8

    const/16 v6, 0x20

    const/4 v7, 0x0

    const/4 v8, 0x0

    if-eq v0, v5, :cond_4

    if-eq v0, v4, :cond_3

    if-eq v0, v6, :cond_2

    if-eq v0, v3, :cond_1

    move v0, v7

    move v9, v8

    move v11, v9

    goto :goto_1

    :cond_1
    const/high16 v0, 0x42b40000    # 90.0f

    iget v9, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->j:I

    iget-object v10, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->b:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {v10}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    add-int/2addr v10, v9

    iget v9, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->A:I

    sub-int v9, v10, v9

    iget-object v11, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v11}, Landroid/widget/LinearLayout;->getBottom()I

    move-result v11

    sub-int/2addr v11, v10

    goto :goto_1

    :cond_2
    const/high16 v0, -0x3d4c0000    # -90.0f

    iget v9, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->j:I

    iget-object v10, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->b:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {v10}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    add-int/2addr v10, v9

    iget-object v9, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getBottom()I

    move-result v9

    sub-int/2addr v9, v10

    iget v11, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->A:I

    goto :goto_0

    :cond_3
    const/high16 v0, 0x43340000    # 180.0f

    iget v9, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->i:I

    iget-object v10, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->b:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {v10}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    add-int/2addr v10, v9

    iget-object v9, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getRight()I

    move-result v9

    sub-int/2addr v9, v10

    iget v11, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->z:I

    :goto_0
    sub-int v11, v10, v11

    goto :goto_1

    :cond_4
    iget v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->i:I

    iget-object v9, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->b:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    add-int/2addr v9, v0

    iget v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->z:I

    iget-object v10, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getRight()I

    move-result v10

    sub-int v0, v9, v0

    sub-int v11, v10, v9

    move v9, v0

    move v0, v7

    :goto_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v10

    int-to-float v12, v1

    int-to-float v13, v2

    invoke-virtual {p1, v0, v12, v13}, Landroid/graphics/Canvas;->rotate(FFF)V

    iget v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->g:I

    if-eq v0, v5, :cond_7

    if-eq v0, v4, :cond_7

    if-eq v0, v6, :cond_5

    if-eq v0, v3, :cond_5

    goto/16 :goto_5

    :cond_5
    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    sub-int/2addr v1, v0

    int-to-float v0, v1

    iget-object v1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v2, v1

    int-to-float v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->q:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v1

    invoke-virtual {v0, v8, v8, v9, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {p0, v6}, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->a(I)Z

    move-result v0

    if-eqz v0, :cond_6

    iget v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->G:I

    goto :goto_2

    :cond_6
    iget v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->G:I

    neg-int v0, v0

    :goto_2
    int-to-float v0, v0

    invoke-virtual {p1, v7, v0}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    int-to-float v0, v9

    invoke-virtual {p1, v0, v7}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->r:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v1

    goto :goto_4

    :cond_7
    iget v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->z:I

    int-to-float v0, v0

    iget v1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->A:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->q:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v1

    invoke-virtual {v0, v8, v8, v9, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {p0}, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->b()Z

    move-result v0

    if-eqz v0, :cond_8

    iget v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->G:I

    goto :goto_3

    :cond_8
    iget v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->G:I

    neg-int v0, v0

    :goto_3
    int-to-float v0, v0

    invoke-virtual {p1, v7, v0}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    int-to-float v0, v9

    invoke-virtual {p1, v0, v7}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->r:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v1

    :goto_4
    invoke-virtual {v0, v8, v8, v11, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->r:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :goto_5
    invoke-virtual {p1, v10}, Landroid/graphics/Canvas;->restoreToCount(I)V

    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const v0, 0x7f0a00b9

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/AppCompatImageView;

    iput-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->b:Landroidx/appcompat/widget/AppCompatImageView;

    const v0, 0x1020002

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->s:Landroid/widget/FrameLayout;

    const v0, 0x7f0a0062

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0700c0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iget-object v1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setMinimumHeight(I)V

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->r:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iget-object v1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    iget-object v1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    :cond_0
    const v0, 0x7f0a0108

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->C:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->B:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const v0, 0x1020016

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/AppCompatTextView;

    iput-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->D:Landroidx/appcompat/widget/AppCompatTextView;

    const v0, 0x102001a

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/AppCompatButton;

    iput-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->y:Landroidx/appcompat/widget/AppCompatButton;

    const v0, 0x1020019

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/AppCompatButton;

    iput-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->v:Landroidx/appcompat/widget/AppCompatButton;

    new-instance v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView$a;

    invoke-direct {v0, p0}, Lmiuix/popupwidget/internal/widget/ArrowPopupView$a;-><init>(Lmiuix/popupwidget/internal/widget/ArrowPopupView;)V

    new-instance v1, Lmiuix/popupwidget/internal/widget/ArrowPopupView$a;

    invoke-direct {v1, p0}, Lmiuix/popupwidget/internal/widget/ArrowPopupView$a;-><init>(Lmiuix/popupwidget/internal/widget/ArrowPopupView;)V

    iput-object v1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->w:Lmiuix/popupwidget/internal/widget/ArrowPopupView$a;

    iget-object v1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->y:Landroidx/appcompat/widget/AppCompatButton;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->v:Landroidx/appcompat/widget/AppCompatButton;

    iget-object v1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->w:Lmiuix/popupwidget/internal/widget/ArrowPopupView$a;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 18

    move-object/from16 v0, p0

    iget-object v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v1

    if-eqz v1, :cond_2f

    iget v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->g:I

    const/16 v2, 0x20

    const/16 v3, 0x40

    const/16 v4, 0x10

    const/4 v5, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x1

    if-nez v1, :cond_3

    new-array v1, v6, [I

    iget-object v8, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->a:Landroid/view/View;

    invoke-virtual {v8, v1}, Landroid/view/View;->getLocationInWindow([I)V

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v8

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v9

    iget-object v10, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v10

    iget-object v11, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v11}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v11

    iget-object v12, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->a:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getHeight()I

    move-result v12

    iget-object v13, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->a:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getWidth()I

    move-result v13

    new-instance v14, Landroid/util/SparseIntArray;

    const/4 v15, 0x4

    invoke-direct {v14, v15}, Landroid/util/SparseIntArray;-><init>(I)V

    aget v15, v1, v7

    sub-int/2addr v15, v11

    invoke-virtual {v14, v4, v15}, Landroid/util/SparseIntArray;->put(II)V

    aget v15, v1, v7

    sub-int/2addr v9, v15

    sub-int/2addr v9, v12

    sub-int/2addr v9, v11

    const/16 v11, 0x8

    invoke-virtual {v14, v11, v9}, Landroid/util/SparseIntArray;->put(II)V

    aget v9, v1, v5

    sub-int/2addr v9, v10

    invoke-virtual {v14, v3, v9}, Landroid/util/SparseIntArray;->put(II)V

    aget v1, v1, v5

    sub-int/2addr v8, v1

    sub-int/2addr v8, v13

    sub-int/2addr v8, v10

    invoke-virtual {v14, v2, v8}, Landroid/util/SparseIntArray;->put(II)V

    const/high16 v1, -0x80000000

    move v8, v4

    move v2, v5

    :goto_0
    invoke-virtual {v14}, Landroid/util/SparseIntArray;->size()I

    move-result v9

    if-ge v2, v9, :cond_2

    invoke-virtual {v14, v2}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v9

    invoke-virtual {v14, v9}, Landroid/util/SparseIntArray;->get(I)I

    move-result v10

    iget v11, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->u:I

    if-lt v10, v11, :cond_0

    move v8, v9

    goto :goto_1

    :cond_0
    invoke-virtual {v14, v9}, Landroid/util/SparseIntArray;->get(I)I

    move-result v10

    if-le v10, v1, :cond_1

    invoke-virtual {v14, v9}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    move v8, v9

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    invoke-virtual {v0, v8}, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->setArrowMode(I)V

    :cond_3
    iget v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->g:I

    invoke-virtual {v0, v1}, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->c(I)V

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getLayoutDirection()I

    move-result v1

    if-ne v1, v7, :cond_4

    move v1, v7

    goto :goto_2

    :cond_4
    move v1, v5

    :goto_2
    if-eqz v1, :cond_5

    iget v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->x:I

    neg-int v1, v1

    goto :goto_3

    :cond_5
    iget v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->x:I

    :goto_3
    iput v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->x:I

    invoke-virtual/range {p0 .. p0}, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->b()Z

    move-result v1

    if-nez v1, :cond_7

    invoke-virtual {v0, v4}, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->a(I)Z

    move-result v1

    if-eqz v1, :cond_6

    goto :goto_4

    :cond_6
    move v1, v5

    goto :goto_5

    :cond_7
    :goto_4
    move v1, v7

    :goto_5
    if-eqz v1, :cond_1d

    iget-object v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    iget-object v2, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v8

    iget-object v9, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v9

    iget-object v10, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getMinimumWidth()I

    move-result v10

    if-le v9, v10, :cond_8

    iget-object v9, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v9

    goto :goto_6

    :cond_8
    iget-object v9, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getMinimumWidth()I

    move-result v9

    :goto_6
    iget-object v10, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v10

    iget-object v11, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v11}, Landroid/widget/LinearLayout;->getMinimumHeight()I

    move-result v11

    if-le v10, v11, :cond_9

    iget-object v10, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v10

    goto :goto_7

    :cond_9
    iget-object v10, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getMinimumHeight()I

    move-result v10

    :goto_7
    invoke-direct/range {p0 .. p0}, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->getArrowWidth()I

    move-result v11

    invoke-direct/range {p0 .. p0}, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->getArrowHeight()I

    move-result v12

    new-array v13, v6, [I

    iget-object v14, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->a:Landroid/view/View;

    invoke-virtual {v14, v13}, Landroid/view/View;->getLocationOnScreen([I)V

    aget v14, v13, v5

    aget v7, v13, v7

    invoke-virtual {v0, v13}, Landroid/widget/FrameLayout;->getLocationOnScreen([I)V

    div-int/lit8 v15, v1, 0x2

    add-int/2addr v15, v14

    aget v16, v13, v5

    sub-int v15, v15, v16

    iput v15, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->z:I

    sub-int v15, v3, v15

    invoke-static {v1, v11, v6, v14}, Landroidx/activity/result/a;->c(IIII)I

    move-result v1

    aget v6, v13, v5

    sub-int/2addr v1, v6

    iput v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->i:I

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getTop()I

    move-result v1

    add-int/2addr v1, v5

    iput v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->A:I

    invoke-virtual {v0, v4}, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->a(I)Z

    move-result v1

    if-eqz v1, :cond_a

    iget v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->A:I

    const/4 v2, 0x1

    aget v4, v13, v2

    sub-int v4, v7, v4

    sub-int/2addr v4, v10

    iget-object v6, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v6

    sub-int/2addr v6, v12

    add-int/2addr v6, v4

    add-int/2addr v6, v1

    iput v6, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->A:I

    aget v1, v13, v2

    sub-int/2addr v7, v1

    sub-int/2addr v7, v12

    add-int/2addr v7, v5

    sub-int/2addr v7, v2

    goto :goto_8

    :cond_a
    const/4 v1, 0x1

    invoke-virtual/range {p0 .. p0}, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->b()Z

    move-result v4

    if-eqz v4, :cond_b

    iget v4, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->A:I

    add-int/2addr v7, v2

    aget v1, v13, v1

    sub-int/2addr v7, v1

    iget-object v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getPaddingTop()I

    move-result v1

    sub-int/2addr v7, v1

    add-int/2addr v7, v12

    add-int/2addr v7, v4

    iput v7, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->A:I

    iget-object v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getPaddingTop()I

    move-result v1

    sub-int/2addr v1, v12

    add-int/2addr v1, v7

    add-int/lit8 v7, v1, 0x1

    goto :goto_8

    :cond_b
    move v7, v5

    :goto_8
    div-int/lit8 v1, v9, 0x2

    sub-int v2, v9, v1

    iget v4, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->z:I

    if-lt v4, v1, :cond_c

    if-lt v15, v2, :cond_c

    sub-int/2addr v4, v1

    iput v4, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->z:I

    goto :goto_9

    :cond_c
    if-ge v15, v2, :cond_d

    sub-int v1, v3, v9

    iput v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->z:I

    goto :goto_9

    :cond_d
    if-ge v4, v1, :cond_e

    iput v5, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->z:I

    :cond_e
    :goto_9
    iget v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->z:I

    iget v2, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->x:I

    add-int/2addr v1, v2

    iput v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->z:I

    iget v4, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->i:I

    add-int/2addr v4, v2

    iput v4, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->i:I

    if-gez v4, :cond_f

    iput v5, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->i:I

    goto :goto_a

    :cond_f
    add-int v2, v4, v11

    if-le v2, v3, :cond_10

    sub-int/2addr v2, v3

    sub-int/2addr v4, v2

    iput v4, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->i:I

    :cond_10
    :goto_a
    iget-object v2, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-static {v1, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget v4, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->A:I

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    iget v6, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->z:I

    add-int/2addr v6, v9

    invoke-static {v6, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    iget v6, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->A:I

    add-int/2addr v6, v10

    invoke-static {v6, v8}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-virtual {v2, v1, v4, v3, v6}, Landroid/widget/LinearLayout;->layout(IIII)V

    iget v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->g:I

    const/16 v2, 0x9

    if-eq v1, v2, :cond_1a

    const/16 v2, 0xa

    if-eq v1, v2, :cond_17

    const/16 v2, 0x11

    if-eq v1, v2, :cond_14

    const/16 v2, 0x12

    if-eq v1, v2, :cond_11

    iget v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->i:I

    goto/16 :goto_10

    :cond_11
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getLayoutDirection()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_12

    const/4 v5, 0x1

    :cond_12
    iget-object v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    if-eqz v5, :cond_13

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getRight()I

    move-result v1

    iget-object v2, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getPaddingEnd()I

    move-result v2

    sub-int/2addr v1, v2

    sub-int/2addr v1, v11

    goto :goto_b

    :cond_13
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLeft()I

    move-result v1

    iget-object v2, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getPaddingStart()I

    move-result v2

    add-int/2addr v1, v2

    :goto_b
    iget-object v2, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getBottom()I

    move-result v2

    iget-object v3, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->b:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v3

    sub-int/2addr v3, v12

    sub-int/2addr v2, v3

    iget-object v3, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->b:Landroidx/appcompat/widget/AppCompatImageView;

    add-int v4, v1, v11

    invoke-virtual {v3}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v2

    invoke-virtual {v3, v1, v2, v4, v5}, Landroid/widget/ImageView;->layout(IIII)V

    goto :goto_d

    :cond_14
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getLayoutDirection()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_15

    const/4 v5, 0x1

    :cond_15
    iget-object v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    if-eqz v5, :cond_16

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLeft()I

    move-result v1

    iget-object v2, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getPaddingStart()I

    move-result v2

    add-int/2addr v2, v1

    goto :goto_c

    :cond_16
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getRight()I

    move-result v1

    iget-object v2, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getPaddingEnd()I

    move-result v2

    sub-int/2addr v1, v2

    sub-int v2, v1, v11

    :goto_c
    iget-object v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getBottom()I

    move-result v1

    iget-object v3, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v1, v3

    iget-object v3, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->b:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v3

    sub-int/2addr v3, v12

    sub-int/2addr v1, v3

    move/from16 v17, v2

    move v2, v1

    move/from16 v1, v17

    :goto_d
    add-int/lit8 v7, v2, -0x5

    goto :goto_10

    :cond_17
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getLayoutDirection()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_18

    move v5, v2

    :cond_18
    iget-object v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    if-eqz v5, :cond_19

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLeft()I

    move-result v1

    iget-object v3, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getPaddingStart()I

    move-result v3

    add-int/2addr v3, v1

    sub-int/2addr v3, v2

    goto :goto_e

    :cond_19
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getRight()I

    move-result v1

    iget-object v3, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getPaddingEnd()I

    move-result v3

    sub-int/2addr v1, v3

    sub-int/2addr v1, v11

    add-int/2addr v1, v2

    goto :goto_f

    :cond_1a
    const/4 v1, 0x1

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getLayoutDirection()I

    move-result v2

    if-ne v2, v1, :cond_1b

    const/4 v5, 0x1

    :cond_1b
    iget-object v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    if-eqz v5, :cond_1c

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getRight()I

    move-result v1

    iget-object v2, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getPaddingStart()I

    move-result v2

    sub-int/2addr v1, v2

    sub-int/2addr v1, v11

    goto :goto_f

    :cond_1c
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLeft()I

    move-result v1

    iget-object v2, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getPaddingStart()I

    move-result v2

    add-int/2addr v2, v1

    add-int/lit8 v3, v2, -0x1

    :goto_e
    move v1, v3

    :goto_f
    iget-object v2, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getPaddingTop()I

    move-result v2

    add-int/2addr v2, v7

    sub-int v7, v2, v12

    iget-object v2, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->b:Landroidx/appcompat/widget/AppCompatImageView;

    add-int v3, v1, v11

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v7

    invoke-virtual {v2, v1, v7, v3, v4}, Landroid/widget/ImageView;->layout(IIII)V

    :goto_10
    iget-object v2, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->b:Landroidx/appcompat/widget/AppCompatImageView;

    add-int/2addr v11, v1

    invoke-virtual {v2}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    add-int/2addr v3, v7

    invoke-virtual {v2, v1, v7, v11, v3}, Landroid/widget/ImageView;->layout(IIII)V

    goto/16 :goto_1b

    :cond_1d
    new-array v1, v6, [I

    iget-object v2, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->a:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    aget v2, v1, v5

    const/4 v4, 0x1

    aget v4, v1, v4

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getLocationOnScreen([I)V

    iget-object v5, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->a:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v5

    iget-object v7, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->a:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    move-result v7

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v8

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v9

    iget-object v10, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v10

    iget-object v11, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v11}, Landroid/widget/LinearLayout;->getMinimumWidth()I

    move-result v11

    if-le v10, v11, :cond_1e

    iget-object v10, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v10

    goto :goto_11

    :cond_1e
    iget-object v10, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getMinimumWidth()I

    move-result v10

    :goto_11
    iget-object v11, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v11}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v11

    iget-object v12, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v12}, Landroid/widget/LinearLayout;->getMinimumHeight()I

    move-result v12

    if-le v11, v12, :cond_1f

    iget-object v11, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v11}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v11

    goto :goto_12

    :cond_1f
    iget-object v11, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v11}, Landroid/widget/LinearLayout;->getMinimumHeight()I

    move-result v11

    :goto_12
    invoke-direct/range {p0 .. p0}, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->getArrowWidth()I

    move-result v12

    invoke-direct/range {p0 .. p0}, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->getArrowHeight()I

    move-result v13

    div-int/lit8 v14, v7, 0x2

    add-int/2addr v14, v4

    const/4 v15, 0x1

    aget v16, v1, v15

    sub-int v14, v14, v16

    iput v14, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->A:I

    sub-int v14, v9, v14

    invoke-static {v7, v13, v6, v4}, Landroidx/activity/result/a;->c(IIII)I

    move-result v4

    aget v7, v1, v15

    sub-int/2addr v4, v7

    iget-object v7, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getPaddingTop()I

    move-result v7

    iget-object v15, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v15}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v15

    sub-int/2addr v7, v15

    div-int/2addr v7, v6

    add-int/2addr v7, v4

    iput v7, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->j:I

    div-int/lit8 v4, v11, 0x2

    sub-int v6, v11, v4

    invoke-virtual/range {p0 .. p0}, Landroid/widget/FrameLayout;->getLeft()I

    move-result v7

    iget v15, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->x:I

    add-int/2addr v7, v15

    iput v7, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->z:I

    invoke-virtual {v0, v3}, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->a(I)Z

    move-result v3

    if-eqz v3, :cond_22

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getLayoutDirection()I

    move-result v3

    const/4 v7, 0x1

    if-ne v3, v7, :cond_20

    const/4 v3, 0x1

    goto :goto_13

    :cond_20
    const/4 v3, 0x0

    :goto_13
    if-eqz v3, :cond_21

    iget v3, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->z:I

    add-int/2addr v2, v5

    iget-object v5, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v5

    sub-int/2addr v2, v5

    add-int/2addr v2, v12

    const/4 v5, 0x0

    aget v1, v1, v5

    goto :goto_16

    :cond_21
    const/4 v3, 0x0

    iget v5, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->z:I

    sub-int v7, v2, v10

    iget-object v15, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v15}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v15

    add-int/2addr v15, v7

    sub-int/2addr v15, v12

    aget v7, v1, v3

    sub-int/2addr v15, v7

    add-int/2addr v15, v5

    iput v15, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->z:I

    sub-int/2addr v2, v12

    aget v1, v1, v3

    goto :goto_15

    :cond_22
    const/16 v3, 0x20

    invoke-virtual {v0, v3}, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->a(I)Z

    move-result v3

    if-eqz v3, :cond_25

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getLayoutDirection()I

    move-result v3

    const/4 v7, 0x1

    if-ne v3, v7, :cond_23

    const/4 v3, 0x1

    goto :goto_14

    :cond_23
    const/4 v3, 0x0

    :goto_14
    if-eqz v3, :cond_24

    iget v3, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->z:I

    sub-int v5, v2, v10

    iget-object v7, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v7

    add-int/2addr v7, v5

    sub-int/2addr v7, v12

    const/4 v5, 0x0

    aget v15, v1, v5

    sub-int/2addr v7, v15

    add-int/lit8 v7, v7, 0x1

    add-int/2addr v7, v3

    iput v7, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->z:I

    sub-int/2addr v2, v12

    aget v1, v1, v5

    :goto_15
    sub-int/2addr v2, v1

    iget v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->x:I

    add-int/2addr v2, v1

    add-int/lit8 v2, v2, -0x1

    goto :goto_17

    :cond_24
    iget v3, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->z:I

    add-int/2addr v2, v5

    iget-object v5, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v5

    sub-int/2addr v2, v5

    add-int/2addr v2, v12

    const/4 v5, 0x0

    aget v1, v1, v5

    :goto_16
    sub-int/2addr v2, v1

    add-int/2addr v2, v3

    iput v2, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->z:I

    iget-object v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v1, v12

    add-int/2addr v1, v2

    add-int/lit8 v2, v1, 0x1

    goto :goto_17

    :cond_25
    const/4 v2, 0x0

    :goto_17
    iget v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->A:I

    if-lt v1, v4, :cond_26

    if-lt v14, v6, :cond_26

    sub-int/2addr v1, v4

    goto :goto_18

    :cond_26
    if-ge v14, v6, :cond_27

    sub-int v1, v9, v11

    :goto_18
    const/4 v3, 0x0

    add-int/2addr v1, v3

    iput v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->A:I

    goto :goto_19

    :cond_27
    const/4 v3, 0x0

    if-ge v1, v4, :cond_28

    iput v3, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->A:I

    :cond_28
    :goto_19
    iget v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->j:I

    add-int/2addr v1, v3

    iput v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->j:I

    if-gez v1, :cond_29

    iput v3, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->j:I

    goto :goto_1a

    :cond_29
    add-int v4, v1, v13

    if-le v4, v9, :cond_2a

    sub-int/2addr v4, v9

    sub-int/2addr v1, v4

    iput v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->j:I

    :cond_2a
    :goto_1a
    iget-object v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    iget v4, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->z:I

    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v4

    iget v5, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->A:I

    invoke-static {v5, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget v5, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->z:I

    add-int/2addr v5, v10

    invoke-static {v5, v8}, Ljava/lang/Math;->min(II)I

    move-result v5

    iget v6, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->A:I

    add-int/2addr v6, v11

    invoke-static {v6, v9}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-virtual {v1, v4, v3, v5, v6}, Landroid/widget/LinearLayout;->layout(IIII)V

    iget-object v1, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->b:Landroidx/appcompat/widget/AppCompatImageView;

    iget v3, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->j:I

    add-int/2addr v12, v2

    add-int/2addr v13, v3

    invoke-virtual {v1, v2, v3, v12, v13}, Landroid/widget/ImageView;->layout(IIII)V

    :goto_1b
    invoke-virtual/range {p0 .. p0}, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->getContentView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_2e

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    iget-object v4, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v4

    iget-object v5, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->C:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v5

    sub-int/2addr v4, v5

    if-le v3, v4, :cond_2b

    iget-object v3, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v3

    iget-object v4, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->C:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_1c

    :cond_2b
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    iget-object v4, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v4

    if-le v3, v4, :cond_2c

    iget-object v3, v0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v3

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    :goto_1c
    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2c
    iget v1, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-lez v1, :cond_2d

    iget v1, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-gtz v1, :cond_2e

    :cond_2d
    const-string v1, "ArrowPopupView"

    const-string v2, "Invalid LayoutPrams of content view, please check the anchor view"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2e
    return-void

    :cond_2f
    const/4 v1, 0x0

    throw v1
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->E:Landroid/graphics/Rect;

    iget-object v3, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->getHitRect(Landroid/graphics/Rect;)V

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    throw p1

    :cond_1
    :goto_0
    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->F:Landroid/view/View$OnTouchListener;

    if-eqz v0, :cond_2

    invoke-interface {v0, p1, p2}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    :goto_1
    return p1
.end method

.method public setAnchor(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->a:Landroid/view/View;

    return-void
.end method

.method public setArrowMode(I)V
    .locals 0

    iput p1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->g:I

    invoke-virtual {p0, p1}, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->c(I)V

    return-void
.end method

.method public setArrowPopupWindow(La2/a;)V
    .locals 0

    return-void
.end method

.method public setAutoDismiss(Z)V
    .locals 0

    iput-boolean p1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->o:Z

    return-void
.end method

.method public setContentView(I)V
    .locals 2

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p0, p1}, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 2

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->removeAllViews()V

    if-eqz p1, :cond_0

    iget-object v1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->s:Landroid/widget/FrameLayout;

    invoke-virtual {v1, p1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void
.end method

.method public setRollingPercent(F)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 2

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->C:Landroid/widget/LinearLayout;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x8

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->D:Landroidx/appcompat/widget/AppCompatTextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTouchInterceptor(Landroid/view/View$OnTouchListener;)V
    .locals 0

    iput-object p1, p0, Lmiuix/popupwidget/internal/widget/ArrowPopupView;->F:Landroid/view/View$OnTouchListener;

    return-void
.end method
