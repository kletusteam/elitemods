.class public Lmiuix/popupwidget/internal/widget/GuidePopupView;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field public static final synthetic p:I


# instance fields
.field public a:Landroid/view/View;

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:F

.field public j:I

.field public final k:Landroid/graphics/Paint;

.field public l:F

.field public m:F

.field public n:Landroid/widget/LinearLayout;

.field public o:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const v0, 0x7f040134

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->o:Z

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->k:Landroid/graphics/Paint;

    new-instance v3, Lmiuix/popupwidget/internal/widget/GuidePopupView$a;

    invoke-direct {v3, p0}, Lmiuix/popupwidget/internal/widget/GuidePopupView$a;-><init>(Lmiuix/popupwidget/internal/widget/GuidePopupView;)V

    new-instance v3, Lmiuix/popupwidget/internal/widget/GuidePopupView$b;

    invoke-direct {v3, p0}, Lmiuix/popupwidget/internal/widget/GuidePopupView$b;-><init>(Lmiuix/popupwidget/internal/widget/GuidePopupView;)V

    const/4 v3, -0x1

    iput v3, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->f:I

    sget-object v4, Landroidx/emoji2/text/l;->d0:[I

    const v5, 0x7f13024b

    invoke-virtual {p1, p2, v4, v0, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    const/4 p2, 0x5

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result p2

    iput p2, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->l:F

    const/4 p2, 0x3

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result p2

    iput p2, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->i:F

    const/4 p2, 0x6

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result p2

    iput p2, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->m:F

    const/4 p2, 0x0

    invoke-virtual {p1, p2, p2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p2

    iput p2, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->g:I

    const/4 p2, 0x4

    invoke-virtual {p1, p2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p2

    invoke-virtual {v2, p2}, Landroid/graphics/Paint;->setColor(I)V

    const/16 p2, 0xf

    invoke-virtual {p1, v1, p2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    const/4 p2, 0x2

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    iget p1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->i:F

    iget p2, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->m:F

    const/high16 v0, 0x40200000    # 2.5f

    mul-float/2addr p2, v0

    add-float/2addr p2, p1

    float-to-int p1, p2

    iput p1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->j:I

    return-void
.end method

.method private getMirroredMode()I
    .locals 2

    iget v0, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->f:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    return v1

    :cond_0
    rem-int/lit8 v1, v0, 0x2

    if-nez v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    return v0

    :cond_1
    add-int/lit8 v0, v0, -0x1

    return v0
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;III)V
    .locals 8

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->k:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->k:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget v0, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->c:I

    iget v1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->e:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v0

    add-int/2addr v1, p3

    int-to-float p3, v1

    iget v0, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->d:I

    iget v1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->b:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v0

    add-int/2addr v1, p4

    int-to-float p4, v1

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    packed-switch p2, :pswitch_data_0

    move p2, v0

    goto :goto_0

    :pswitch_0
    const/high16 p2, -0x3cf90000    # -135.0f

    goto :goto_0

    :pswitch_1
    const/high16 p2, 0x42340000    # 45.0f

    goto :goto_0

    :pswitch_2
    const/high16 p2, 0x43070000    # 135.0f

    goto :goto_0

    :pswitch_3
    const/high16 p2, -0x3dcc0000    # -45.0f

    goto :goto_0

    :pswitch_4
    const/high16 p2, -0x3d4c0000    # -90.0f

    goto :goto_0

    :pswitch_5
    const/high16 p2, 0x42b40000    # 90.0f

    goto :goto_0

    :cond_0
    const/high16 p2, 0x43340000    # 180.0f

    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {p1, p2, p3, p4}, Landroid/graphics/Canvas;->rotate(FFF)V

    iget p2, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->h:I

    int-to-float p2, p2

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result p2

    const/high16 v0, 0x40000000    # 2.0f

    sub-float v3, p3, v0

    add-float v5, p3, v0

    iget v0, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->l:F

    add-float v6, p4, v0

    sget-object v7, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    move-object v2, p1

    move v4, p4

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->clipRect(FFFFLandroid/graphics/Region$Op;)Z

    iget v0, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->l:F

    iget-object v1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->k:Landroid/graphics/Paint;

    invoke-virtual {p1, p3, p4, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    invoke-virtual {p1, p2}, Landroid/graphics/Canvas;->restoreToCount(I)V

    iget-object p2, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->k:Landroid/graphics/Paint;

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object p2, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->k:Landroid/graphics/Paint;

    const/high16 v0, 0x40800000    # 4.0f

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget p2, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->i:F

    add-float v6, p4, p2

    iget-object v7, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->k:Landroid/graphics/Paint;

    move v3, p3

    move v5, p3

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget p2, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->i:F

    iget v1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->m:F

    iget-object v2, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->k:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v2, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->k:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    add-float/2addr p4, p2

    add-float/2addr p4, v1

    iget p2, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->m:F

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->k:Landroid/graphics/Paint;

    invoke-virtual {p1, p3, p4, p2, v0}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b(ILandroid/widget/LinearLayout;II)V
    .locals 8

    iget v0, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->h:I

    int-to-float v0, v0

    iget v1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->i:F

    add-float/2addr v0, v1

    iget v1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->m:F

    add-float/2addr v0, v1

    iget v1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->c:I

    iget v2, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->e:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v1

    iget v1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->d:I

    iget v3, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->b:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v1

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    move v2, v1

    goto :goto_2

    :pswitch_0
    int-to-float v1, v2

    add-float/2addr v1, v0

    goto :goto_0

    :pswitch_1
    int-to-float v1, v2

    sub-float/2addr v1, v0

    :goto_0
    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v3, v2

    goto :goto_2

    :pswitch_2
    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v2, v1

    int-to-float v1, v3

    add-float/2addr v1, v0

    goto :goto_1

    :pswitch_3
    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v2, v1

    int-to-float v1, v3

    sub-float/2addr v1, v0

    :goto_1
    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v1, v3

    float-to-int v1, v1

    move v7, v2

    move v2, v1

    move v1, v7

    :goto_2
    float-to-double v3, v0

    const-wide v5, 0x3fe921fb54442d18L    # 0.7853981633974483

    invoke-static {v5, v6}, Ljava/lang/Math;->sin(D)D

    move-result-wide v5

    mul-double/2addr v5, v3

    double-to-int v3, v5

    int-to-float v4, v3

    sub-float/2addr v0, v4

    float-to-int v0, v0

    const/4 v4, 0x4

    if-eq p1, v4, :cond_3

    const/4 v4, 0x5

    if-eq p1, v4, :cond_2

    const/4 v4, 0x6

    if-eq p1, v4, :cond_1

    const/4 v4, 0x7

    if-eq p1, v4, :cond_0

    goto :goto_5

    :cond_0
    add-int/2addr v1, v3

    goto :goto_3

    :cond_1
    sub-int/2addr v1, v3

    goto :goto_4

    :cond_2
    sub-int/2addr v1, v3

    :goto_3
    add-int/2addr v2, v0

    goto :goto_5

    :cond_3
    add-int/2addr v1, v3

    :goto_4
    sub-int/2addr v2, v0

    :goto_5
    add-int/2addr v1, p3

    add-int/2addr v2, p4

    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result p1

    add-int/2addr p1, v1

    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result p3

    add-int/2addr p3, v2

    invoke-virtual {p2, v1, v2, p1, p3}, Landroid/widget/LinearLayout;->layout(IIII)V

    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget v0, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->c:I

    int-to-float v0, v0

    iget v1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->d:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->a:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->buildDrawingCache()V

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    iget v0, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->f:I

    invoke-virtual {p0, p1, v0, v1, v1}, Lmiuix/popupwidget/internal/widget/GuidePopupView;->a(Landroid/graphics/Canvas;III)V

    return-void
.end method

.method public getArrowMode()I
    .locals 1

    iget v0, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->f:I

    return v0
.end method

.method public getColorBackground()I
    .locals 1

    iget v0, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->g:I

    return v0
.end method

.method public onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const v0, 0x7f0a0102

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->n:Landroid/widget/LinearLayout;

    const v0, 0x7f0a00a1

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 9

    iget p1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->e:I

    if-eqz p1, :cond_0

    iget p1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->b:I

    if-nez p1, :cond_1

    :cond_0
    iget-object p1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->a:Landroid/view/View;

    invoke-virtual {p0, p1}, Lmiuix/popupwidget/internal/widget/GuidePopupView;->setAnchor(Landroid/view/View;)V

    :cond_1
    iget-object p1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->n:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result p1

    iget-object p2, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->n:Landroid/widget/LinearLayout;

    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result p2

    int-to-double p3, p1

    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    invoke-static {p3, p4, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide p3

    int-to-double p1, p2

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide p1

    add-double/2addr p1, p3

    invoke-static {p1, p2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide p1

    div-double/2addr p1, v0

    iget p3, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->m:F

    float-to-double p3, p3

    invoke-static {p1, p2, p3, p4}, Ljava/lang/Math;->max(DD)D

    move-result-wide p1

    double-to-float p1, p1

    iput p1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->m:F

    iget p1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->f:I

    const/4 p2, -0x1

    const/4 p3, 0x3

    const/4 p4, 0x2

    const/4 p5, 0x0

    const/4 v2, 0x1

    if-ne p1, p2, :cond_e

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getWidth()I

    move-result p1

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getHeight()I

    move-result p2

    const/4 v0, 0x4

    new-array v1, v0, [I

    iget v3, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->d:I

    aput v3, v1, p5

    iget v4, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->b:I

    sub-int v5, p2, v3

    sub-int/2addr v5, v4

    aput v5, v1, v2

    iget v5, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->c:I

    aput v5, v1, p4

    iget v6, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->e:I

    sub-int v7, p1, v5

    sub-int/2addr v7, v6

    aput v7, v1, p3

    div-int/2addr v6, p4

    add-int/2addr v6, v5

    div-int/2addr v4, p4

    add-int/2addr v4, v3

    const/high16 v3, -0x80000000

    move v5, v3

    move v3, p5

    :goto_0
    if-ge p5, v0, :cond_4

    aget v7, v1, p5

    iget v8, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->j:I

    if-lt v7, v8, :cond_2

    goto :goto_1

    :cond_2
    aget v7, v1, p5

    if-le v7, v5, :cond_3

    aget v3, v1, p5

    move v5, v3

    move v3, p5

    :cond_3
    add-int/lit8 p5, p5, 0x1

    goto :goto_0

    :cond_4
    move p5, v3

    :goto_1
    if-eqz p5, :cond_b

    if-eq p5, v2, :cond_9

    if-eq p5, p4, :cond_7

    if-eq p5, p3, :cond_5

    goto :goto_5

    :cond_5
    int-to-float p1, v4

    iget p3, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->m:F

    cmpg-float p1, p1, p3

    if-gez p1, :cond_6

    goto :goto_6

    :cond_6
    sub-int/2addr p2, v4

    int-to-float p1, p2

    cmpg-float p1, p1, p3

    if-gez p1, :cond_d

    goto :goto_3

    :cond_7
    int-to-float p1, v4

    iget p3, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->m:F

    cmpg-float p1, p1, p3

    if-gez p1, :cond_8

    goto :goto_2

    :cond_8
    sub-int/2addr p2, v4

    int-to-float p1, p2

    cmpg-float p1, p1, p3

    if-gez p1, :cond_d

    goto :goto_4

    :cond_9
    int-to-float p2, v6

    iget p3, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->m:F

    cmpg-float p2, p2, p3

    if-gez p2, :cond_a

    goto :goto_6

    :cond_a
    sub-int/2addr p1, v6

    int-to-float p1, p1

    cmpg-float p1, p1, p3

    if-gez p1, :cond_d

    :goto_2
    const/4 v0, 0x6

    goto :goto_6

    :cond_b
    int-to-float p2, v6

    iget p3, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->m:F

    cmpg-float p2, p2, p3

    if-gez p2, :cond_c

    :goto_3
    const/4 v0, 0x7

    goto :goto_6

    :cond_c
    sub-int/2addr p1, v6

    int-to-float p1, p1

    cmpg-float p1, p1, p3

    if-gez p1, :cond_d

    :goto_4
    const/4 v0, 0x5

    goto :goto_6

    :cond_d
    :goto_5
    move v0, p5

    :goto_6
    invoke-virtual {p0, v0}, Lmiuix/popupwidget/internal/widget/GuidePopupView;->setArrowMode(I)V

    goto :goto_8

    :cond_e
    iget-boolean p1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->o:Z

    if-nez p1, :cond_f

    iput p5, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->h:I

    goto :goto_7

    :cond_f
    iget p1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->e:I

    div-int/2addr p1, p4

    iget p2, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->b:I

    div-int/2addr p2, p4

    int-to-double v3, p1

    invoke-static {v3, v4, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    int-to-double v5, p2

    invoke-static {v5, v6, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    add-double/2addr v0, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-int v0, v0

    iget v1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->f:I

    if-eqz v1, :cond_11

    if-eq v1, v2, :cond_11

    if-eq v1, p4, :cond_10

    if-eq v1, p3, :cond_10

    iput v0, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->h:I

    goto :goto_7

    :cond_10
    iput p1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->h:I

    goto :goto_7

    :cond_11
    iput p2, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->h:I

    :goto_7
    iget p1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->f:I

    iget-object p2, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->n:Landroid/widget/LinearLayout;

    invoke-virtual {p0, p1, p2, p5, p5}, Lmiuix/popupwidget/internal/widget/GuidePopupView;->b(ILandroid/widget/LinearLayout;II)V

    :goto_8
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result p1

    float-to-int p1, p1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iget v1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->c:I

    new-instance v2, Landroid/graphics/Rect;

    iget v3, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->d:I

    iget-object v4, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->a:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v4, v1

    iget v5, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->d:I

    iget-object v6, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->a:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    add-int/2addr v6, v5

    invoke-direct {v2, v1, v3, v4, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result p2

    if-nez p2, :cond_0

    invoke-virtual {v2, p1, v0}, Landroid/graphics/Rect;->contains(II)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->a:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->callOnClick()Z

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    throw p1
.end method

.method public setAnchor(Landroid/view/View;)V
    .locals 1

    iput-object p1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->a:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result p1

    iput p1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->e:I

    iget-object p1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->a:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    iput p1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->b:I

    const/4 p1, 0x2

    new-array p1, p1, [I

    iget-object v0, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->getLocationInWindow([I)V

    const/4 v0, 0x0

    aget v0, p1, v0

    iput v0, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->c:I

    const/4 v0, 0x1

    aget p1, p1, v0

    iput p1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->d:I

    return-void
.end method

.method public setArrowMode(I)V
    .locals 0

    iput p1, p0, Lmiuix/popupwidget/internal/widget/GuidePopupView;->f:I

    return-void
.end method

.method public setGuidePopupWindow(La2/b;)V
    .locals 0

    return-void
.end method

.method public setTouchInterceptor(Landroid/view/View$OnTouchListener;)V
    .locals 0

    return-void
.end method
