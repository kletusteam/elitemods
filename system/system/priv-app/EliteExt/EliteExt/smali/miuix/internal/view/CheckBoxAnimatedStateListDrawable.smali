.class public Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;
.super Ls1/a;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable$a;
    }
.end annotation


# instance fields
.field public b:Ls1/c;

.field public c:F

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:F


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ls1/a;-><init>()V

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->g:F

    iput v0, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->c:F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->f:Z

    iput-boolean v0, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->e:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;Ls1/a$a;)V
    .locals 11

    invoke-direct {p0, p1, p2, p3}, Ls1/a;-><init>(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;Ls1/a$a;)V

    const/high16 p1, 0x3f800000    # 1.0f

    iput p1, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->g:F

    iput p1, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->c:F

    const/4 p1, 0x0

    iput-boolean p1, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->f:Z

    iput-boolean p1, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->e:Z

    new-instance p1, Ls1/c;

    instance-of v2, p0, Lmiuix/internal/view/RadioButtonAnimatedStateListDrawable;

    iget v3, p3, Ls1/a$a;->e:I

    iget v4, p3, Ls1/a$a;->d:I

    iget v5, p3, Ls1/a$a;->a:I

    iget v6, p3, Ls1/a$a;->c:I

    iget v7, p3, Ls1/a$a;->b:I

    iget v8, p3, Ls1/a$a;->g:I

    iget v9, p3, Ls1/a$a;->i:I

    iget v10, p3, Ls1/a$a;->h:I

    move-object v0, p1

    move-object v1, p0

    invoke-direct/range {v0 .. v10}, Ls1/c;-><init>(Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;ZIIIIIIII)V

    iput-object p1, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->b:Ls1/c;

    return-void
.end method


# virtual methods
.method public a()Ls1/a$a;
    .locals 1

    new-instance v0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable$a;

    invoke-direct {v0}, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable$a;-><init>()V

    return-object v0
.end method

.method public applyTheme(Landroid/content/res/Resources$Theme;)V
    .locals 13

    invoke-super {p0, p1}, Landroid/graphics/drawable/AnimatedStateListDrawable;->applyTheme(Landroid/content/res/Resources$Theme;)V

    invoke-virtual {p0}, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->b()I

    move-result v0

    sget-object v1, Landroidx/emoji2/text/l;->Y:[I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object p1

    iget-object v0, p0, Ls1/a;->a:Ls1/a$a;

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, v0, Ls1/a$a;->e:I

    iget-object v0, p0, Ls1/a;->a:Ls1/a$a;

    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, v0, Ls1/a$a;->d:I

    iget-object v0, p0, Ls1/a;->a:Ls1/a$a;

    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, v0, Ls1/a$a;->a:I

    iget-object v0, p0, Ls1/a;->a:Ls1/a$a;

    const/4 v1, 0x6

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, v0, Ls1/a$a;->g:I

    iget-object v0, p0, Ls1/a;->a:Ls1/a$a;

    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, v0, Ls1/a$a;->c:I

    iget-object v0, p0, Ls1/a;->a:Ls1/a$a;

    invoke-virtual {p1, v2, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, v0, Ls1/a$a;->b:I

    iget-object v0, p0, Ls1/a;->a:Ls1/a$a;

    const/16 v1, 0x8

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, v0, Ls1/a$a;->i:I

    iget-object v0, p0, Ls1/a;->a:Ls1/a$a;

    const/4 v1, 0x7

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, v0, Ls1/a$a;->h:I

    iget-object v0, p0, Ls1/a;->a:Ls1/a$a;

    const/16 v1, 0x9

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, v0, Ls1/a$a;->j:Z

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    new-instance p1, Ls1/c;

    instance-of v4, p0, Lmiuix/internal/view/RadioButtonAnimatedStateListDrawable;

    iget-object v0, p0, Ls1/a;->a:Ls1/a$a;

    iget v5, v0, Ls1/a$a;->e:I

    iget v6, v0, Ls1/a$a;->d:I

    iget v7, v0, Ls1/a$a;->a:I

    iget v8, v0, Ls1/a$a;->c:I

    iget v9, v0, Ls1/a$a;->b:I

    iget v10, v0, Ls1/a$a;->g:I

    iget v11, v0, Ls1/a$a;->i:I

    iget v12, v0, Ls1/a$a;->h:I

    move-object v2, p1

    move-object v3, p0

    invoke-direct/range {v2 .. v12}, Ls1/c;-><init>(Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;ZIIIIIIII)V

    iput-object p1, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->b:Ls1/c;

    return-void
.end method

.method public b()I
    .locals 1

    const v0, 0x7f1300ac

    return v0
.end method

.method public c(IIII)V
    .locals 2

    iget-object v0, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->b:Ls1/c;

    if-eqz v0, :cond_0

    iget-object v1, v0, Ls1/c;->f:Ls1/b;

    invoke-virtual {v1, p1, p2, p3, p4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v1, v0, Ls1/c;->c:Ls1/b;

    invoke-virtual {v1, p1, p2, p3, p4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, v0, Ls1/c;->d:Ls1/b;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :cond_0
    return-void
.end method

.method public d(Landroid/graphics/Rect;)V
    .locals 2

    iget-object v0, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->b:Ls1/c;

    if-eqz v0, :cond_0

    iget-object v1, v0, Ls1/c;->f:Ls1/b;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v1, v0, Ls1/c;->c:Ls1/b;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v0, v0, Ls1/c;->d:Ls1/b;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    :cond_0
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 5

    invoke-virtual {p0}, Landroid/graphics/drawable/AnimatedStateListDrawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v0, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/graphics/drawable/AnimatedStateListDrawable;->draw(Landroid/graphics/Canvas;)V

    return-void

    :cond_0
    iget-object v0, p0, Ls1/a;->a:Ls1/a$a;

    iget-boolean v0, v0, Ls1/a$a;->j:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->b:Ls1/c;

    if-eqz v0, :cond_1

    iget-object v1, v0, Ls1/c;->f:Ls1/b;

    invoke-virtual {v1, p1}, Ls1/b;->draw(Landroid/graphics/Canvas;)V

    iget-object v1, v0, Ls1/c;->c:Ls1/b;

    invoke-virtual {v1, p1}, Ls1/b;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, v0, Ls1/c;->d:Ls1/b;

    invoke-virtual {v0, p1}, Ls1/b;->draw(Landroid/graphics/Canvas;)V

    :cond_1
    invoke-super {p0, p1}, Landroid/graphics/drawable/AnimatedStateListDrawable;->draw(Landroid/graphics/Canvas;)V

    return-void

    :cond_2
    new-instance v0, Landroid/graphics/PaintFlagsDrawFilter;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Landroid/graphics/PaintFlagsDrawFilter;-><init>(II)V

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->setDrawFilter(Landroid/graphics/DrawFilter;)V

    iget-boolean v0, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->d:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->b:Ls1/c;

    if-eqz v0, :cond_3

    iget-object v1, v0, Ls1/c;->f:Ls1/b;

    invoke-virtual {v1, p1}, Ls1/b;->draw(Landroid/graphics/Canvas;)V

    iget-object v1, v0, Ls1/c;->c:Ls1/b;

    invoke-virtual {v1, p1}, Ls1/b;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, v0, Ls1/c;->d:Ls1/b;

    invoke-virtual {v0, p1}, Ls1/b;->draw(Landroid/graphics/Canvas;)V

    :cond_3
    iget v0, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->c:F

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0

    :cond_4
    const/16 v0, 0x7f

    :goto_0
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/AnimatedStateListDrawable;->setAlpha(I)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {p0}, Landroid/graphics/drawable/AnimatedStateListDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    iget v1, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->g:F

    iget v3, v0, Landroid/graphics/Rect;->left:I

    iget v4, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v4

    div-int/2addr v3, v2

    int-to-float v3, v3

    iget v4, v0, Landroid/graphics/Rect;->top:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v4, v0

    div-int/2addr v4, v2

    int-to-float v0, v4

    invoke-virtual {p1, v1, v1, v3, v0}, Landroid/graphics/Canvas;->scale(FFFF)V

    invoke-super {p0, p1}, Landroid/graphics/drawable/AnimatedStateListDrawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method public onStateChange([I)Z
    .locals 9

    invoke-super {p0, p1}, Landroid/graphics/drawable/AnimatedStateListDrawable;->onStateChange([I)Z

    move-result v0

    iget-object v1, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->b:Ls1/c;

    if-nez v1, :cond_0

    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/graphics/drawable/AnimatedStateListDrawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_1

    instance-of v1, v1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_1

    invoke-super {p0, p1}, Landroid/graphics/drawable/AnimatedStateListDrawable;->onStateChange([I)Z

    move-result p1

    return p1

    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->d:Z

    array-length v2, p1

    move v3, v1

    move v4, v3

    move v5, v4

    :goto_0
    if-ge v3, v2, :cond_5

    aget v6, p1, v3

    const v7, 0x10100a7

    const/4 v8, 0x1

    if-ne v6, v7, :cond_2

    move v4, v8

    goto :goto_1

    :cond_2
    const v7, 0x10100a0

    if-ne v6, v7, :cond_3

    move v5, v8

    goto :goto_1

    :cond_3
    const v7, 0x101009e

    if-ne v6, v7, :cond_4

    iput-boolean v8, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->d:Z

    :cond_4
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_5
    if-eqz v4, :cond_10

    iget-object p1, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->b:Ls1/c;

    if-eqz p1, :cond_10

    iget-object v2, p0, Ls1/a;->a:Ls1/a$a;

    iget-boolean v2, v2, Ls1/a$a;->j:Z

    if-eqz v2, :cond_10

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v3

    if-eq v2, v3, :cond_6

    goto :goto_2

    :cond_6
    iget-object v2, p1, Ls1/c;->r:Lz0/f;

    iget-boolean v3, v2, Lz0/c;->f:Z

    if-nez v3, :cond_7

    invoke-virtual {v2}, Lz0/f;->i()V

    :cond_7
    iget-object v2, p1, Ls1/c;->l:Lz0/f;

    iget-boolean v3, v2, Lz0/c;->f:Z

    if-nez v3, :cond_8

    invoke-virtual {v2}, Lz0/f;->i()V

    :cond_8
    if-nez v5, :cond_9

    iget-object v2, p1, Ls1/c;->q:Lz0/f;

    iget-boolean v3, v2, Lz0/c;->f:Z

    if-nez v3, :cond_9

    invoke-virtual {v2}, Lz0/f;->i()V

    :cond_9
    iget-object v2, p1, Ls1/c;->t:Lz0/f;

    iget-boolean v3, v2, Lz0/c;->f:Z

    if-eqz v3, :cond_a

    invoke-virtual {v2}, Lz0/c;->c()V

    :cond_a
    iget-object v2, p1, Ls1/c;->x:Lz0/f;

    iget-boolean v3, v2, Lz0/c;->f:Z

    if-eqz v3, :cond_b

    invoke-virtual {v2}, Lz0/c;->c()V

    :cond_b
    iget-object v2, p1, Ls1/c;->p:Lz0/f;

    iget-boolean v3, v2, Lz0/c;->f:Z

    if-eqz v3, :cond_c

    invoke-virtual {v2}, Lz0/c;->c()V

    :cond_c
    iget-object v2, p1, Ls1/c;->i:Lz0/f;

    iget-boolean v3, v2, Lz0/c;->f:Z

    if-eqz v3, :cond_d

    invoke-virtual {v2}, Lz0/c;->c()V

    :cond_d
    iget-object v2, p1, Ls1/c;->o:Lz0/f;

    iget-boolean v3, v2, Lz0/c;->f:Z

    if-eqz v3, :cond_e

    invoke-virtual {v2}, Lz0/c;->c()V

    :cond_e
    iget-object v2, p1, Ls1/c;->u:Lz0/f;

    iget-boolean v3, v2, Lz0/c;->f:Z

    if-eqz v3, :cond_f

    invoke-virtual {v2}, Lz0/c;->c()V

    :cond_f
    iget-object p1, p1, Ls1/c;->v:Lz0/f;

    iget-boolean v2, p1, Lz0/c;->f:Z

    if-eqz v2, :cond_10

    invoke-virtual {p1}, Lz0/c;->c()V

    :cond_10
    :goto_2
    iget-boolean p1, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->f:Z

    if-nez p1, :cond_13

    if-nez v4, :cond_13

    iget-boolean p1, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->d:Z

    iget-object v2, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->b:Ls1/c;

    if-eqz v2, :cond_13

    if-eqz p1, :cond_12

    if-eqz v5, :cond_11

    iget-object p1, v2, Ls1/c;->d:Ls1/b;

    const/16 v1, 0xff

    invoke-virtual {p1, v1}, Ls1/b;->setAlpha(I)V

    iget-object p1, v2, Ls1/c;->c:Ls1/b;

    const/16 v1, 0x19

    goto :goto_3

    :cond_11
    iget-object p1, v2, Ls1/c;->d:Ls1/b;

    invoke-virtual {p1, v1}, Ls1/b;->setAlpha(I)V

    iget-object p1, v2, Ls1/c;->c:Ls1/b;

    :goto_3
    invoke-virtual {p1, v1}, Ls1/b;->setAlpha(I)V

    iget-object p1, v2, Ls1/c;->f:Ls1/b;

    iget v1, v2, Ls1/c;->b:I

    goto :goto_4

    :cond_12
    iget-object p1, v2, Ls1/c;->d:Ls1/b;

    invoke-virtual {p1, v1}, Ls1/b;->setAlpha(I)V

    iget-object p1, v2, Ls1/c;->c:Ls1/b;

    invoke-virtual {p1, v1}, Ls1/b;->setAlpha(I)V

    iget-object p1, v2, Ls1/c;->f:Ls1/b;

    iget v1, v2, Ls1/c;->a:I

    :goto_4
    invoke-virtual {p1, v1}, Ls1/b;->setAlpha(I)V

    invoke-virtual {p0}, Landroid/graphics/drawable/AnimatedStateListDrawable;->invalidateSelf()V

    :cond_13
    if-nez v4, :cond_23

    iget-boolean p1, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->f:Z

    if-nez p1, :cond_14

    iget-boolean p1, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->e:Z

    if-eq v5, p1, :cond_23

    :cond_14
    iget-object p1, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->b:Ls1/c;

    if-eqz p1, :cond_23

    iget-object v1, p0, Ls1/a;->a:Ls1/a$a;

    iget-boolean v1, v1, Ls1/a$a;->j:Z

    if-eqz v1, :cond_21

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v2

    if-eq v1, v2, :cond_15

    goto/16 :goto_7

    :cond_15
    iget-object v1, p1, Ls1/c;->r:Lz0/f;

    iget-boolean v2, v1, Lz0/c;->f:Z

    if-eqz v2, :cond_16

    invoke-virtual {v1}, Lz0/c;->c()V

    :cond_16
    iget-object v1, p1, Ls1/c;->l:Lz0/f;

    iget-boolean v2, v1, Lz0/c;->f:Z

    if-eqz v2, :cond_17

    invoke-virtual {v1}, Lz0/c;->c()V

    :cond_17
    iget-object v1, p1, Ls1/c;->q:Lz0/f;

    iget-boolean v2, v1, Lz0/c;->f:Z

    if-eqz v2, :cond_18

    invoke-virtual {v1}, Lz0/c;->c()V

    :cond_18
    iget-object v1, p1, Ls1/c;->t:Lz0/f;

    iget-boolean v2, v1, Lz0/c;->f:Z

    if-nez v2, :cond_19

    invoke-virtual {v1}, Lz0/f;->i()V

    :cond_19
    if-eqz v5, :cond_1d

    iget-object v1, p1, Ls1/c;->u:Lz0/f;

    iget-boolean v2, v1, Lz0/c;->f:Z

    if-eqz v2, :cond_1a

    invoke-virtual {v1}, Lz0/c;->c()V

    :cond_1a
    iget-object v1, p1, Ls1/c;->v:Lz0/f;

    iget-boolean v2, v1, Lz0/c;->f:Z

    if-nez v2, :cond_1b

    invoke-virtual {v1}, Lz0/f;->i()V

    :cond_1b
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Ls1/e;

    invoke-direct {v2, p1}, Ls1/e;-><init>(Ls1/c;)V

    const-wide/16 v6, 0x32

    invoke-virtual {v1, v2, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-boolean v1, p1, Ls1/c;->g:Z

    if-eqz v1, :cond_1c

    iget-object v1, p1, Ls1/c;->x:Lz0/f;

    const/high16 v2, 0x41200000    # 10.0f

    goto :goto_5

    :cond_1c
    iget-object v1, p1, Ls1/c;->x:Lz0/f;

    const/high16 v2, 0x40a00000    # 5.0f

    :goto_5
    iput v2, v1, Lz0/c;->l:F

    goto :goto_6

    :cond_1d
    iget-object v1, p1, Ls1/c;->v:Lz0/f;

    iget-boolean v2, v1, Lz0/c;->f:Z

    if-eqz v2, :cond_1e

    invoke-virtual {v1}, Lz0/c;->c()V

    :cond_1e
    iget-object v1, p1, Ls1/c;->u:Lz0/f;

    iget-boolean v2, v1, Lz0/c;->f:Z

    if-nez v2, :cond_1f

    invoke-virtual {v1}, Lz0/f;->i()V

    :cond_1f
    iget-object v1, p1, Ls1/c;->o:Lz0/f;

    iget-boolean v2, v1, Lz0/c;->f:Z

    if-nez v2, :cond_20

    invoke-virtual {v1}, Lz0/f;->i()V

    :cond_20
    :goto_6
    iget-object p1, p1, Ls1/c;->x:Lz0/f;

    invoke-virtual {p1}, Lz0/f;->i()V

    goto :goto_9

    :cond_21
    :goto_7
    const/high16 v1, 0x437f0000    # 255.0f

    iget-object v2, p1, Ls1/c;->d:Ls1/b;

    if-eqz v5, :cond_22

    iget-object p1, p1, Ls1/c;->v:Lz0/f;

    goto :goto_8

    :cond_22
    iget-object p1, p1, Ls1/c;->u:Lz0/f;

    :goto_8
    iget-object p1, p1, Lz0/f;->o:Lz0/g;

    iget-wide v6, p1, Lz0/g;->c:D

    double-to-float p1, v6

    mul-float/2addr p1, v1

    float-to-int p1, p1

    invoke-virtual {v2, p1}, Ls1/b;->setAlpha(I)V

    :cond_23
    :goto_9
    iput-boolean v4, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->f:Z

    iput-boolean v5, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->e:Z

    return v0
.end method

.method public setBounds(IIII)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/graphics/drawable/AnimatedStateListDrawable;->setBounds(IIII)V

    invoke-virtual {p0, p1, p2, p3, p4}, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->c(IIII)V

    return-void
.end method

.method public setBounds(Landroid/graphics/Rect;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/graphics/drawable/AnimatedStateListDrawable;->setBounds(Landroid/graphics/Rect;)V

    invoke-virtual {p0, p1}, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->d(Landroid/graphics/Rect;)V

    return-void
.end method
