.class public Lmiuix/internal/view/RadioButtonAnimatedStateListDrawable;
.super Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/internal/view/RadioButtonAnimatedStateListDrawable$a;
    }
.end annotation


# instance fields
.field public h:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;-><init>()V

    const/16 v0, 0x13

    iput v0, p0, Lmiuix/internal/view/RadioButtonAnimatedStateListDrawable;->h:I

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;Ls1/a$a;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;-><init>(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;Ls1/a$a;)V

    const/16 p2, 0x13

    iput p2, p0, Lmiuix/internal/view/RadioButtonAnimatedStateListDrawable;->h:I

    if-eqz p1, :cond_0

    const p2, 0x7f070181

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lmiuix/internal/view/RadioButtonAnimatedStateListDrawable;->h:I

    :cond_0
    return-void
.end method


# virtual methods
.method public a()Ls1/a$a;
    .locals 1

    new-instance v0, Lmiuix/internal/view/RadioButtonAnimatedStateListDrawable$a;

    invoke-direct {v0}, Lmiuix/internal/view/RadioButtonAnimatedStateListDrawable$a;-><init>()V

    return-object v0
.end method

.method public b()I
    .locals 1

    const v0, 0x7f1300ad

    return v0
.end method

.method public c(IIII)V
    .locals 2

    iget v0, p0, Lmiuix/internal/view/RadioButtonAnimatedStateListDrawable;->h:I

    add-int/2addr p1, v0

    add-int/2addr p2, v0

    sub-int/2addr p3, v0

    sub-int/2addr p4, v0

    iget-object v0, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->b:Ls1/c;

    if-eqz v0, :cond_0

    iget-object v1, v0, Ls1/c;->f:Ls1/b;

    invoke-virtual {v1, p1, p2, p3, p4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v1, v0, Ls1/c;->c:Ls1/b;

    invoke-virtual {v1, p1, p2, p3, p4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, v0, Ls1/c;->d:Ls1/b;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :cond_0
    return-void
.end method

.method public d(Landroid/graphics/Rect;)V
    .locals 2

    iget v0, p0, Lmiuix/internal/view/RadioButtonAnimatedStateListDrawable;->h:I

    invoke-virtual {p1, v0, v0}, Landroid/graphics/Rect;->inset(II)V

    iget-object v0, p0, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->b:Ls1/c;

    if-eqz v0, :cond_0

    iget-object v1, v0, Ls1/c;->f:Ls1/b;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v1, v0, Ls1/c;->c:Ls1/b;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v0, v0, Ls1/c;->d:Ls1/b;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    :cond_0
    return-void
.end method
