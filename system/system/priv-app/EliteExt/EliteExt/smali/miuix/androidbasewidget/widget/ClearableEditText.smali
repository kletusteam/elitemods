.class public Lmiuix/androidbasewidget/widget/ClearableEditText;
.super Lt0/a;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/androidbasewidget/widget/ClearableEditText$a;,
        Lmiuix/androidbasewidget/widget/ClearableEditText$b;
    }
.end annotation


# static fields
.field public static final n:[I


# instance fields
.field public i:Lmiuix/androidbasewidget/widget/ClearableEditText$a;

.field public j:Landroid/graphics/drawable/Drawable;

.field public k:Z

.field public l:Z

.field public m:Lmiuix/androidbasewidget/widget/ClearableEditText$b;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a9

    aput v2, v0, v1

    sput-object v0, Lmiuix/androidbasewidget/widget/ClearableEditText;->n:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const v0, 0x7f0400b6

    invoke-direct {p0, p1, p2, v0}, Lt0/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance p1, Lmiuix/androidbasewidget/widget/ClearableEditText$b;

    invoke-direct {p1, p0}, Lmiuix/androidbasewidget/widget/ClearableEditText$b;-><init>(Lmiuix/androidbasewidget/widget/ClearableEditText;)V

    iput-object p1, p0, Lmiuix/androidbasewidget/widget/ClearableEditText;->m:Lmiuix/androidbasewidget/widget/ClearableEditText$b;

    invoke-virtual {p0}, Landroid/widget/EditText;->getCompoundDrawablesRelative()[Landroid/graphics/drawable/Drawable;

    move-result-object p1

    const/4 p2, 0x2

    aget-object p1, p1, p2

    iput-object p1, p0, Lmiuix/androidbasewidget/widget/ClearableEditText;->j:Landroid/graphics/drawable/Drawable;

    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    new-instance p2, Lmiuix/androidbasewidget/widget/ClearableEditText$a;

    invoke-direct {p2, p0, p0}, Lmiuix/androidbasewidget/widget/ClearableEditText$a;-><init>(Lmiuix/androidbasewidget/widget/ClearableEditText;Landroid/view/View;)V

    iput-object p2, p0, Lmiuix/androidbasewidget/widget/ClearableEditText;->i:Lmiuix/androidbasewidget/widget/ClearableEditText$a;

    invoke-static {p0, p2}, Lw/v;->k(Landroid/view/View;Lw/a;)V

    const/16 p2, 0x1d

    if-lt p1, p2, :cond_0

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroid/widget/EditText;->setForceDarkAllowed(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    iget-object v0, p0, Lmiuix/androidbasewidget/widget/ClearableEditText;->i:Lmiuix/androidbasewidget/widget/ClearableEditText$a;

    if-eqz v0, :cond_a

    iget-boolean v1, p0, Lmiuix/androidbasewidget/widget/ClearableEditText;->l:Z

    if-eqz v1, :cond_a

    iget-object v1, v0, Lc0/a;->h:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_9

    iget-object v1, v0, Lc0/a;->h:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    goto/16 :goto_6

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v4, 0x7

    const/16 v5, 0x100

    const/16 v6, 0x80

    const/high16 v7, -0x80000000

    if-eq v1, v4, :cond_3

    const/16 v4, 0x9

    if-eq v1, v4, :cond_3

    const/16 v4, 0xa

    if-eq v1, v4, :cond_1

    goto/16 :goto_6

    :cond_1
    iget v1, v0, Lc0/a;->m:I

    if-eq v1, v7, :cond_9

    if-ne v1, v7, :cond_2

    goto/16 :goto_5

    :cond_2
    iput v7, v0, Lc0/a;->m:I

    invoke-virtual {v0, v7, v6}, Lc0/a;->n(II)Z

    invoke-virtual {v0, v1, v5}, Lc0/a;->n(II)Z

    goto :goto_5

    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    iget-object v4, v0, Lmiuix/androidbasewidget/widget/ClearableEditText$a;->q:Lmiuix/androidbasewidget/widget/ClearableEditText;

    iget-boolean v8, v4, Lmiuix/androidbasewidget/widget/ClearableEditText;->l:Z

    if-eqz v8, :cond_7

    iget-object v4, v4, Lmiuix/androidbasewidget/widget/ClearableEditText;->j:Landroid/graphics/drawable/Drawable;

    if-nez v4, :cond_4

    move v4, v2

    goto :goto_0

    :cond_4
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    :goto_0
    iget-object v8, v0, Lmiuix/androidbasewidget/widget/ClearableEditText$a;->q:Lmiuix/androidbasewidget/widget/ClearableEditText;

    invoke-static {v8}, Lh/w0;->b(Landroid/view/View;)Z

    move-result v8

    if-eqz v8, :cond_5

    iget-object v8, v0, Lmiuix/androidbasewidget/widget/ClearableEditText$a;->q:Lmiuix/androidbasewidget/widget/ClearableEditText;

    invoke-virtual {v8}, Landroid/widget/EditText;->getPaddingLeft()I

    move-result v8

    mul-int/lit8 v8, v8, 0x2

    add-int/2addr v8, v4

    int-to-float v4, v8

    cmpg-float v1, v1, v4

    if-gez v1, :cond_6

    goto :goto_1

    :cond_5
    iget-object v8, v0, Lmiuix/androidbasewidget/widget/ClearableEditText$a;->q:Lmiuix/androidbasewidget/widget/ClearableEditText;

    invoke-virtual {v8}, Landroid/widget/EditText;->getWidth()I

    move-result v8

    iget-object v9, v0, Lmiuix/androidbasewidget/widget/ClearableEditText$a;->q:Lmiuix/androidbasewidget/widget/ClearableEditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getPaddingRight()I

    move-result v9

    mul-int/lit8 v9, v9, 0x2

    sub-int/2addr v8, v9

    sub-int/2addr v8, v4

    int-to-float v4, v8

    cmpl-float v1, v1, v4

    if-lez v1, :cond_6

    :goto_1
    move v1, v3

    goto :goto_2

    :cond_6
    move v1, v2

    :goto_2
    if-eqz v1, :cond_7

    move v1, v2

    goto :goto_3

    :cond_7
    move v1, v7

    :goto_3
    iget v4, v0, Lc0/a;->m:I

    if-ne v4, v1, :cond_8

    goto :goto_4

    :cond_8
    iput v1, v0, Lc0/a;->m:I

    invoke-virtual {v0, v1, v6}, Lc0/a;->n(II)Z

    invoke-virtual {v0, v4, v5}, Lc0/a;->n(II)Z

    :goto_4
    if-eq v1, v7, :cond_9

    :goto_5
    move v2, v3

    :cond_9
    :goto_6
    if-eqz v2, :cond_a

    return v3

    :cond_a
    invoke-super {p0, p1}, Landroid/widget/EditText;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    iget-boolean v0, p0, Lmiuix/androidbasewidget/widget/ClearableEditText;->l:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmiuix/androidbasewidget/widget/ClearableEditText;->j:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    :goto_0
    invoke-static {p0}, Lh/w0;->b(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p0}, Landroid/widget/EditText;->getPaddingLeft()I

    move-result v4

    add-int/2addr v4, v0

    int-to-float v0, v4

    cmpg-float v0, v3, v0

    if-gez v0, :cond_2

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p0}, Landroid/widget/EditText;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Landroid/widget/EditText;->getPaddingRight()I

    move-result v5

    sub-int/2addr v4, v5

    sub-int/2addr v4, v0

    int-to-float v0, v4

    cmpl-float v0, v3, v0

    if-lez v0, :cond_2

    :goto_1
    move v0, v1

    goto :goto_2

    :cond_2
    move v0, v2

    :goto_2
    if-eqz v0, :cond_7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result p1

    if-eqz p1, :cond_5

    if-eq p1, v1, :cond_4

    const/4 v0, 0x3

    if-eq p1, v0, :cond_3

    goto :goto_3

    :cond_3
    iget-boolean p1, p0, Lmiuix/androidbasewidget/widget/ClearableEditText;->k:Z

    if-eqz p1, :cond_6

    iput-boolean v2, p0, Lmiuix/androidbasewidget/widget/ClearableEditText;->k:Z

    goto :goto_3

    :cond_4
    invoke-virtual {p0}, Landroid/widget/EditText;->isEnabled()Z

    move-result p1

    if-eqz p1, :cond_6

    iget-boolean p1, p0, Lmiuix/androidbasewidget/widget/ClearableEditText;->k:Z

    if-eqz p1, :cond_6

    const-string p1, ""

    invoke-virtual {p0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    sget p1, Lmiuix/view/c;->n:I

    invoke-static {p0, p1}, Lmiuix/view/HapticCompat;->performHapticFeedback(Landroid/view/View;I)Z

    goto :goto_3

    :cond_5
    invoke-virtual {p0}, Landroid/widget/EditText;->isEnabled()Z

    move-result p1

    if-eqz p1, :cond_6

    iget-boolean p1, p0, Lmiuix/androidbasewidget/widget/ClearableEditText;->l:Z

    if-eqz p1, :cond_6

    iput-boolean v1, p0, Lmiuix/androidbasewidget/widget/ClearableEditText;->k:Z

    :cond_6
    :goto_3
    return v1

    :cond_7
    invoke-super {p0, p1}, Landroid/widget/EditText;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public drawableStateChanged()V
    .locals 2

    invoke-super {p0}, Lh/i;->drawableStateChanged()V

    iget-object v0, p0, Lmiuix/androidbasewidget/widget/ClearableEditText;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/widget/EditText;->getDrawableState()[I

    move-result-object v0

    iget-object v1, p0, Lmiuix/androidbasewidget/widget/ClearableEditText;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    invoke-virtual {p0}, Landroid/widget/EditText;->invalidate()V

    :cond_0
    return-void
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1

    invoke-super {p0}, Landroid/widget/EditText;->jumpDrawablesToCurrentState()V

    iget-object v0, p0, Lmiuix/androidbasewidget/widget/ClearableEditText;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    :cond_0
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/EditText;->onAttachedToWindow()V

    iget-object v0, p0, Lmiuix/androidbasewidget/widget/ClearableEditText;->m:Lmiuix/androidbasewidget/widget/ClearableEditText$b;

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public onCreateDrawableState(I)[I
    .locals 1

    add-int/lit8 p1, p1, 0x1

    invoke-super {p0, p1}, Landroid/widget/EditText;->onCreateDrawableState(I)[I

    move-result-object p1

    iget-boolean v0, p0, Lmiuix/androidbasewidget/widget/ClearableEditText;->l:Z

    if-nez v0, :cond_0

    sget-object v0, Lmiuix/androidbasewidget/widget/ClearableEditText;->n:[I

    invoke-static {p1, v0}, Landroid/widget/EditText;->mergeDrawableStates([I[I)[I

    :cond_0
    return-object p1
.end method

.method public onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/EditText;->onDetachedFromWindow()V

    iget-object v0, p0, Lmiuix/androidbasewidget/widget/ClearableEditText;->m:Lmiuix/androidbasewidget/widget/ClearableEditText$b;

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    if-nez p1, :cond_0

    if-nez p2, :cond_0

    if-nez p3, :cond_0

    if-nez p4, :cond_0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/EditText;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "ClearableEditText can only set drawables by setCompoundDrawablesRelative()"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    iput-object p3, p0, Lmiuix/androidbasewidget/widget/ClearableEditText;->j:Landroid/graphics/drawable/Drawable;

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/EditText;->setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/EditText;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lmiuix/androidbasewidget/widget/ClearableEditText;->j:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method
