.class public Lb1/a;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/ThreadLocal;

.field public static final b:Lz0/a;

.field public static final c:Lz0/a;

.field public static final d:Lz0/h;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lz0/h;

    invoke-direct {v0}, Lz0/h;-><init>()V

    sput-object v0, Lb1/a;->d:Lz0/h;

    new-instance v0, Lz0/a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lz0/a;-><init>(I)V

    sput-object v0, Lb1/a;->b:Lz0/a;

    new-instance v0, Lz0/a;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lz0/a;-><init>(I)V

    sput-object v0, Lb1/a;->c:Lz0/a;

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lb1/a;->a:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public static a(Lu0/b;Lx0/a;JJJ)V
    .locals 21

    move-object/from16 v0, p1

    move-wide/from16 v1, p4

    move-wide/from16 v3, p6

    iget-wide v5, v0, Lx0/a;->k:J

    sub-long v5, p2, v5

    iget-object v7, v0, Lx0/a;->b:Lc1/c$a;

    iget v8, v7, Lc1/c$a;->c:I

    sget-object v9, Lc1/c;->a:Ljava/util/concurrent/ConcurrentHashMap;

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, -0x1

    if-ge v8, v11, :cond_0

    move v8, v10

    goto :goto_0

    :cond_0
    move v8, v9

    :goto_0
    if-eqz v8, :cond_f

    cmp-long v7, v1, v3

    if-lez v7, :cond_1

    long-to-float v1, v1

    long-to-float v2, v3

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    goto :goto_1

    :cond_1
    move v1, v10

    :goto_1
    long-to-double v2, v3

    const-wide v7, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v7

    sget-object v4, Lb1/a;->a:Ljava/lang/ThreadLocal;

    const-class v7, Lz0/d;

    sget-object v8, Lc1/a;->a:[Ljava/lang/Class;

    invoke-virtual {v4}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v8

    if-nez v8, :cond_2

    new-array v8, v9, [Ljava/lang/Object;

    invoke-static {v7, v8}, Lc1/g;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    :cond_2
    check-cast v8, Lz0/d;

    iget-object v4, v0, Lx0/a;->j:La1/b;

    iget-wide v11, v0, Lx0/a;->m:D

    invoke-static {v8}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v7, p0

    invoke-virtual {v7, v4}, Lu0/b;->e(Ljava/lang/Object;)F

    move-result v4

    const/high16 v7, 0x3f400000    # 0.75f

    mul-float/2addr v4, v7

    iput v4, v8, Lz0/d;->b:F

    const v7, 0x41855555

    mul-float/2addr v4, v7

    iput v4, v8, Lz0/d;->c:F

    iput-wide v11, v8, Lz0/d;->a:D

    move v4, v9

    :goto_2
    if-ge v9, v1, :cond_11

    iget-wide v12, v0, Lx0/a;->p:D

    iget-object v7, v0, Lx0/a;->b:Lc1/c$a;

    iget v7, v7, Lc1/c$a;->c:I

    invoke-static {v7}, Lb1/a;->b(I)Lz0/e;

    move-result-object v11

    if-eqz v11, :cond_4

    instance-of v7, v11, Lz0/h;

    if-eqz v7, :cond_3

    iget-wide v14, v0, Lx0/a;->m:D

    invoke-static {v14, v15}, Landroidx/emoji2/text/l;->G(D)Z

    move-result v7

    if-eqz v7, :cond_3

    goto :goto_3

    :cond_3
    iget-object v7, v0, Lx0/a;->b:Lc1/c$a;

    iget-object v7, v7, Lc1/c$a;->b:[D

    aget-wide v14, v7, v4

    aget-wide v16, v7, v10

    const/4 v7, 0x2

    new-array v7, v7, [D

    move-wide/from16 p2, v5

    iget-wide v5, v0, Lx0/a;->m:D

    aput-wide v5, v7, v4

    iget-wide v4, v0, Lx0/a;->o:D

    aput-wide v4, v7, v10

    move-wide/from16 v18, v2

    move-object/from16 v20, v7

    invoke-interface/range {v11 .. v20}, Lz0/e;->a(DDDD[D)D

    move-result-wide v4

    iget-wide v6, v0, Lx0/a;->o:D

    mul-double v10, v2, v4

    add-double/2addr v10, v6

    iput-wide v10, v0, Lx0/a;->o:D

    goto :goto_4

    :cond_4
    :goto_3
    move-wide/from16 p2, v5

    iget-wide v4, v0, Lx0/a;->m:D

    iput-wide v4, v0, Lx0/a;->o:D

    const-wide/16 v4, 0x0

    :goto_4
    iput-wide v4, v0, Lx0/a;->p:D

    iget-object v6, v0, Lx0/a;->j:La1/b;

    iget-object v7, v0, Lx0/a;->b:Lc1/c$a;

    iget v7, v7, Lc1/c$a;->c:I

    iget-wide v10, v0, Lx0/a;->o:D

    const/4 v12, -0x2

    if-ne v7, v12, :cond_7

    iget-wide v12, v8, Lz0/d;->a:D

    invoke-static {v12, v13}, Ljava/lang/Math;->abs(D)D

    move-result-wide v14

    const-wide v16, 0x47efffffe0000000L    # 3.4028234663852886E38

    cmpl-double v14, v14, v16

    if-eqz v14, :cond_6

    sub-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->abs(D)D

    move-result-wide v10

    iget v12, v8, Lz0/d;->b:F

    float-to-double v12, v12

    cmpg-double v10, v10, v12

    if-gez v10, :cond_5

    goto :goto_5

    :cond_5
    const/4 v10, 0x0

    goto :goto_6

    :cond_6
    :goto_5
    const/4 v10, 0x1

    :goto_6
    if-eqz v10, :cond_8

    :cond_7
    const/4 v10, -0x3

    if-eq v7, v10, :cond_8

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    iget v7, v8, Lz0/d;->c:F

    float-to-double v10, v7

    cmpg-double v4, v4, v10

    if-gez v4, :cond_8

    const/4 v4, 0x1

    const/4 v5, 0x1

    goto :goto_7

    :cond_8
    const/4 v4, 0x1

    const/4 v5, 0x0

    :goto_7
    move v10, v4

    xor-int/lit8 v4, v5, 0x1

    if-eqz v4, :cond_a

    const-wide/16 v11, 0x2710

    move-wide/from16 v13, p2

    cmp-long v5, v13, v11

    if-lez v5, :cond_b

    sget-boolean v4, Lc1/f;->a:Z

    if-eqz v4, :cond_9

    const-string v4, "animation for "

    invoke-static {v4}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v6}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " stopped for running time too long"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ", totalTime = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    new-array v6, v5, [Ljava/lang/Object;

    invoke-static {v4, v6}, Lc1/f;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v4, v5

    goto :goto_8

    :cond_9
    const/4 v4, 0x0

    :goto_8
    move v5, v4

    goto :goto_9

    :cond_a
    move-wide/from16 v13, p2

    :cond_b
    const/4 v5, 0x0

    :goto_9
    if-nez v4, :cond_e

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lx0/a;->a(B)V

    iget-object v1, v0, Lx0/a;->b:Lc1/c$a;

    iget v1, v1, Lc1/c$a;->c:I

    const/4 v2, -0x2

    if-ne v1, v2, :cond_c

    goto :goto_a

    :cond_c
    move v10, v5

    :goto_a
    if-nez v10, :cond_d

    goto :goto_c

    :cond_d
    iget-wide v1, v0, Lx0/a;->m:D

    iput-wide v1, v0, Lx0/a;->o:D

    goto :goto_c

    :cond_e
    add-int/lit8 v9, v9, 0x1

    move v4, v5

    move-wide v5, v13

    goto/16 :goto_2

    :cond_f
    move-wide v13, v5

    check-cast v7, Lc1/c$b;

    invoke-static {v7}, Lc1/c;->a(Lc1/c$b;)Landroid/animation/TimeInterpolator;

    move-result-object v1

    iget-wide v2, v7, Lc1/c$b;->d:J

    cmp-long v4, v13, v2

    if-gez v4, :cond_10

    long-to-float v4, v13

    long-to-float v2, v2

    div-float/2addr v4, v2

    invoke-interface {v1, v4}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v1

    float-to-double v1, v1

    goto :goto_b

    :cond_10
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lx0/a;->a(B)V

    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    :goto_b
    iput-wide v1, v0, Lx0/a;->i:D

    iput-wide v1, v0, Lx0/a;->o:D

    :cond_11
    :goto_c
    return-void
.end method

.method public static b(I)Lz0/e;
    .locals 1

    const/4 v0, -0x4

    if-eq p0, v0, :cond_2

    const/4 v0, -0x3

    if-eq p0, v0, :cond_1

    const/4 v0, -0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    sget-object p0, Lb1/a;->d:Lz0/h;

    return-object p0

    :cond_1
    sget-object p0, Lb1/a;->b:Lz0/a;

    return-object p0

    :cond_2
    sget-object p0, Lb1/a;->c:Lz0/a;

    return-object p0
.end method
