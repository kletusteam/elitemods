.class public Lw/v$h$a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnApplyWindowInsetsListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lw/v$h;->u(Landroid/view/View;Lw/n;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public a:Lw/a0;

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:Lw/n;


# direct methods
.method public constructor <init>(Landroid/view/View;Lw/n;)V
    .locals 0

    iput-object p1, p0, Lw/v$h$a;->b:Landroid/view/View;

    iput-object p2, p0, Lw/v$h$a;->c:Lw/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    iput-object p1, p0, Lw/v$h$a;->a:Lw/a0;

    return-void
.end method


# virtual methods
.method public onApplyWindowInsets(Landroid/view/View;Landroid/view/WindowInsets;)Landroid/view/WindowInsets;
    .locals 4

    invoke-static {p2, p1}, Lw/a0;->h(Landroid/view/WindowInsets;Landroid/view/View;)Lw/a0;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1e

    if-ge v1, v2, :cond_0

    iget-object v3, p0, Lw/v$h$a;->b:Landroid/view/View;

    invoke-static {p2, v3}, Lw/v$h;->a(Landroid/view/WindowInsets;Landroid/view/View;)V

    iget-object p2, p0, Lw/v$h$a;->a:Lw/a0;

    invoke-virtual {v0, p2}, Lw/a0;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    iget-object p2, p0, Lw/v$h$a;->c:Lw/n;

    check-cast p2, Ld/i;

    invoke-virtual {p2, p1, v0}, Ld/i;->a(Landroid/view/View;Lw/a0;)Lw/a0;

    move-result-object p1

    invoke-virtual {p1}, Lw/a0;->f()Landroid/view/WindowInsets;

    move-result-object p1

    return-object p1

    :cond_0
    iput-object v0, p0, Lw/v$h$a;->a:Lw/a0;

    iget-object p2, p0, Lw/v$h$a;->c:Lw/n;

    check-cast p2, Ld/i;

    invoke-virtual {p2, p1, v0}, Ld/i;->a(Landroid/view/View;Lw/a0;)Lw/a0;

    move-result-object p2

    if-lt v1, v2, :cond_1

    invoke-virtual {p2}, Lw/a0;->f()Landroid/view/WindowInsets;

    move-result-object p1

    return-object p1

    :cond_1
    invoke-static {p1}, Lw/v$g;->c(Landroid/view/View;)V

    invoke-virtual {p2}, Lw/a0;->f()Landroid/view/WindowInsets;

    move-result-object p1

    return-object p1
.end method
