.class public final Lw/v$o;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/OnReceiveContentListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lw/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "o"
.end annotation


# instance fields
.field public final a:Lw/o;


# direct methods
.method public constructor <init>(Lw/o;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lw/v$o;->a:Lw/o;

    return-void
.end method


# virtual methods
.method public onReceiveContent(Landroid/view/View;Landroid/view/ContentInfo;)Landroid/view/ContentInfo;
    .locals 2

    new-instance v0, Lw/c;

    new-instance v1, Lw/c$d;

    invoke-direct {v1, p2}, Lw/c$d;-><init>(Landroid/view/ContentInfo;)V

    invoke-direct {v0, v1}, Lw/c;-><init>(Lw/c$e;)V

    iget-object v1, p0, Lw/v$o;->a:Lw/o;

    invoke-interface {v1, p1, v0}, Lw/o;->a(Landroid/view/View;Lw/c;)Lw/c;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    if-ne p1, v0, :cond_1

    return-object p2

    :cond_1
    iget-object p1, p1, Lw/c;->a:Lw/c$e;

    invoke-interface {p1}, Lw/c$e;->d()Landroid/view/ContentInfo;

    move-result-object p1

    return-object p1
.end method
