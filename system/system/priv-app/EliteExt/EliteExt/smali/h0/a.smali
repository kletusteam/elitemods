.class public abstract Lh0/a;
.super Lh0/b;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lh0/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D:",
        "Ljava/lang/Object;",
        ">",
        "Lh0/b<",
        "TD;>;"
    }
.end annotation


# instance fields
.field public i:Ljava/util/concurrent/Executor;

.field public volatile j:Lh0/a$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lh0/a<",
            "TD;>.a;"
        }
    .end annotation
.end field

.field public volatile k:Lh0/a$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lh0/a<",
            "TD;>.a;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lh0/b;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public g(Lh0/a$a;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lh0/a<",
            "TD;>.a;TD;)V"
        }
    .end annotation

    iget-object p2, p0, Lh0/a;->k:Lh0/a$a;

    if-ne p2, p1, :cond_2

    iget-boolean p1, p0, Lh0/b;->h:Z

    if-eqz p1, :cond_1

    iget-boolean p1, p0, Lh0/b;->d:Z

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lh0/b;->d()V

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    iput-boolean p1, p0, Lh0/b;->g:Z

    :cond_1
    :goto_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    const/4 p1, 0x0

    iput-object p1, p0, Lh0/a;->k:Lh0/a$a;

    invoke-virtual {p0}, Lh0/a;->h()V

    :cond_2
    return-void
.end method

.method public h()V
    .locals 5

    iget-object v0, p0, Lh0/a;->k:Lh0/a$a;

    if-nez v0, :cond_4

    iget-object v0, p0, Lh0/a;->j:Lh0/a$a;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lh0/a;->j:Lh0/a$a;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lh0/a;->i:Ljava/util/concurrent/Executor;

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lh0/a;->i:Ljava/util/concurrent/Executor;

    :cond_0
    iget-object v0, p0, Lh0/a;->j:Lh0/a$a;

    iget-object v1, p0, Lh0/a;->i:Ljava/util/concurrent/Executor;

    iget v2, v0, Lh0/c;->b:I

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-eq v2, v4, :cond_3

    iget v0, v0, Lh0/c;->b:I

    invoke-static {v0}, Landroidx/fragment/app/m0;->d(I)I

    move-result v0

    if-eq v0, v4, :cond_2

    if-eq v0, v3, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "We should never reach this state"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot execute task: the task has already been executed (a task can be executed only once)"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot execute task: the task is already running."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iput v3, v0, Lh0/c;->b:I

    iget-object v0, v0, Lh0/c;->a:Ljava/util/concurrent/FutureTask;

    invoke-interface {v1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :cond_4
    return-void
.end method

.method public abstract i()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TD;"
        }
    .end annotation
.end method
