.class public Lq1/a;
.super Le/a;
.source ""


# static fields
.field public static final synthetic d:I


# instance fields
.field public b:[I

.field public c:[I


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getState()[I

    move-result-object v0

    invoke-direct {p0, p1}, Le/a;-><init>(Landroid/graphics/drawable/Drawable;)V

    const/4 p1, 0x0

    new-array v1, p1, [I

    iput-object v1, p0, Lq1/a;->c:[I

    new-array p1, p1, [I

    iput-object p1, p0, Lq1/a;->b:[I

    invoke-virtual {p0, v0}, Lq1/a;->b([I)Z

    return-void
.end method

.method public static a([I[I)[I
    .locals 3

    array-length v0, p1

    array-length v1, p0

    add-int/2addr v0, v1

    new-array v0, v0, [I

    array-length v1, p0

    const/4 v2, 0x0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length p0, p0

    array-length v1, p1

    invoke-static {p1, v2, v0, p0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method


# virtual methods
.method public b([I)Z
    .locals 1

    iget-object v0, p0, Lq1/a;->c:[I

    invoke-static {p1, v0}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lq1/a;->c:[I

    iget-object v0, p0, Lq1/a;->b:[I

    invoke-static {p1, v0}, Lq1/a;->a([I[I)[I

    move-result-object p1

    iget-object v0, p0, Le/a;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public setState([I)Z
    .locals 1

    iget-object v0, p0, Lq1/a;->b:[I

    invoke-static {p1, v0}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lq1/a;->b:[I

    iget-object v0, p0, Lq1/a;->c:[I

    invoke-static {v0, p1}, Lq1/a;->a([I[I)[I

    move-result-object p1

    iget-object v0, p0, Le/a;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
