.class public Lz0/a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lz0/e;


# instance fields
.field public final synthetic a:I


# direct methods
.method public synthetic constructor <init>(I)V
    .locals 0

    iput p1, p0, Lz0/a;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public varargs a(DDDD[D)D
    .locals 0

    iget p5, p0, Lz0/a;->a:I

    packed-switch p5, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    mul-double/2addr p3, p7

    add-double/2addr p3, p1

    return-wide p3

    :goto_0
    const-wide/high16 p5, 0x3ff0000000000000L    # 1.0

    sub-double/2addr p5, p3

    invoke-static {p5, p6, p7, p8}, Ljava/lang/Math;->pow(DD)D

    move-result-wide p3

    mul-double/2addr p3, p1

    return-wide p3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public b([F[D)V
    .locals 6

    iget v0, p0, Lz0/a;->a:I

    const/4 v1, 0x0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    aget p1, p1, v1

    float-to-double v2, p1

    const-wide v4, 0x408f400000000000L    # 1000.0

    mul-double/2addr v2, v4

    aput-wide v2, p2, v1

    return-void

    :goto_0
    aget p1, p1, v1

    float-to-double v2, p1

    const-wide v4, -0x3fef333340000000L    # -4.199999809265137

    mul-double/2addr v2, v4

    const-wide v4, 0x4005bf0a8b145769L    # Math.E

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v4, v2

    aput-wide v4, p2, v1

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
