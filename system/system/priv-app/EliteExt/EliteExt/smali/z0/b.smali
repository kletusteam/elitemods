.class public Lz0/b;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lz0/b$a;,
        Lz0/b$b;,
        Lz0/b$c;,
        Lz0/b$d;
    }
.end annotation


# static fields
.field public static final g:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Lz0/b;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lz0/b$b;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lz0/b$a;

.field public c:J

.field public final d:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Lz0/b$b;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public e:Z

.field public f:Lz0/b$c;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lz0/b;->g:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lz0/b;->d:Landroid/util/ArrayMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lz0/b;->a:Ljava/util/ArrayList;

    new-instance v0, Lz0/b$a;

    invoke-direct {v0, p0}, Lz0/b$a;-><init>(Lz0/b;)V

    iput-object v0, p0, Lz0/b;->b:Lz0/b$a;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lz0/b;->c:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lz0/b;->e:Z

    return-void
.end method

.method public static b()Lz0/b;
    .locals 2

    sget-object v0, Lz0/b;->g:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v1, Lz0/b;

    invoke-direct {v1}, Lz0/b;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lz0/b;

    return-object v0
.end method


# virtual methods
.method public a(Lz0/b$b;J)V
    .locals 3

    iget-object v0, p0, Lz0/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lz0/b;->f:Lz0/b$c;

    if-nez v0, :cond_0

    new-instance v0, Lz0/b$d;

    iget-object v1, p0, Lz0/b;->b:Lz0/b$a;

    invoke-direct {v0, v1}, Lz0/b$d;-><init>(Lz0/b$a;)V

    iput-object v0, p0, Lz0/b;->f:Lz0/b$c;

    :cond_0
    iget-object v0, p0, Lz0/b;->f:Lz0/b$c;

    check-cast v0, Lz0/b$d;

    iget-object v1, v0, Lz0/b$d;->b:Landroid/view/Choreographer;

    iget-object v0, v0, Lz0/b$d;->c:Landroid/view/Choreographer$FrameCallback;

    invoke-virtual {v1, v0}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    :cond_1
    iget-object v0, p0, Lz0/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lz0/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lz0/b;->d:Landroid/util/ArrayMap;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    add-long/2addr v1, p2

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    return-void
.end method

.method public c(Lz0/b$b;)V
    .locals 2

    iget-object v0, p0, Lz0/b;->d:Landroid/util/ArrayMap;

    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lz0/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result p1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lz0/b;->a:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lz0/b;->e:Z

    :cond_0
    return-void
.end method
