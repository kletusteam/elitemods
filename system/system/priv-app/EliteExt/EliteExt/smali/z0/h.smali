.class public Lz0/h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lz0/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public varargs a(DDDD[D)D
    .locals 2

    mul-double/2addr p5, p7

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v0, p5

    mul-double/2addr v0, p1

    const/4 p1, 0x0

    aget-wide p1, p9, p1

    const/4 p5, 0x1

    aget-wide p5, p9, p5

    sub-double/2addr p1, p5

    mul-double/2addr p1, p3

    mul-double/2addr p1, p7

    double-to-float p1, p1

    float-to-double p1, p1

    add-double/2addr v0, p1

    return-wide v0
.end method

.method public b([F[D)V
    .locals 10

    const/4 v0, 0x0

    aget v1, p1, v0

    float-to-double v1, v1

    const/4 v3, 0x1

    aget p1, p1, v3

    float-to-double v4, p1

    const-wide v6, 0x401921fb54442d18L    # 6.283185307179586

    div-double/2addr v6, v4

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    aput-wide v6, p2, v0

    const-wide v6, 0x402921fb54442d18L    # 12.566370614359172

    mul-double/2addr v1, v6

    div-double/2addr v1, v4

    aput-wide v1, p2, v3

    return-void
.end method
