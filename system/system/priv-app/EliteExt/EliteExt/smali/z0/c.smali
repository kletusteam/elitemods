.class public abstract Lz0/c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lz0/b$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lz0/c$a;,
        Lz0/c$b;,
        Lz0/c$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lz0/c<",
        "TT;>;>",
        "Ljava/lang/Object;",
        "Lz0/b$b;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lz0/c$b;",
            ">;"
        }
    .end annotation
.end field

.field public b:J

.field public c:F

.field public d:F

.field public final e:La1/b;

.field public f:Z

.field public g:J

.field public h:Z

.field public final i:Ljava/lang/Object;

.field public final j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lz0/c$c;",
            ">;"
        }
    .end annotation
.end field

.field public k:F

.field public l:F


# direct methods
.method public constructor <init>(Ljava/lang/Object;La1/b;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            ">(TK;",
            "La1/b<",
            "TK;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lz0/c;->l:F

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lz0/c;->k:F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lz0/c;->h:Z

    iput-boolean v0, p0, Lz0/c;->f:Z

    const v0, -0x800001

    iput v0, p0, Lz0/c;->c:F

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lz0/c;->b:J

    iput-wide v0, p0, Lz0/c;->g:J

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lz0/c;->a:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lz0/c;->j:Ljava/util/ArrayList;

    iput-object p1, p0, Lz0/c;->i:Ljava/lang/Object;

    iput-object p2, p0, Lz0/c;->e:La1/b;

    sget-object p1, La1/h;->e:La1/h;

    if-eq p2, p1, :cond_4

    sget-object p1, La1/h;->f:La1/h;

    if-eq p2, p1, :cond_4

    sget-object p1, La1/h;->g:La1/h;

    if-ne p2, p1, :cond_0

    goto :goto_1

    :cond_0
    sget-object p1, La1/h;->b:La1/h;

    if-ne p2, p1, :cond_1

    const/high16 p1, 0x3b800000    # 0.00390625f

    goto :goto_2

    :cond_1
    sget-object p1, La1/h;->h:La1/h;

    if-eq p2, p1, :cond_3

    sget-object p1, La1/h;->i:La1/h;

    if-ne p2, p1, :cond_2

    goto :goto_0

    :cond_2
    const/high16 p1, 0x3f800000    # 1.0f

    goto :goto_2

    :cond_3
    :goto_0
    const p1, 0x3b03126f    # 0.002f

    goto :goto_2

    :cond_4
    :goto_1
    const p1, 0x3dcccccd    # 0.1f

    :goto_2
    iput p1, p0, Lz0/c;->d:F

    return-void
.end method

.method public static e(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/ArrayList<",
            "TT;>;)V"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public a(J)Z
    .locals 21

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    iget-wide v3, v0, Lz0/c;->b:J

    const-wide/16 v5, 0x0

    cmp-long v5, v3, v5

    const/4 v6, 0x0

    iput-wide v1, v0, Lz0/c;->b:J

    if-nez v5, :cond_0

    iget v1, v0, Lz0/c;->k:F

    invoke-virtual {v0, v1}, Lz0/c;->g(F)V

    return v6

    :cond_0
    sub-long/2addr v1, v3

    move-object v3, v0

    check-cast v3, Lz0/f;

    iget-boolean v4, v3, Lz0/f;->m:Z

    const/4 v5, 0x0

    const/4 v7, 0x1

    const v8, 0x7f7fffff    # Float.MAX_VALUE

    if-eqz v4, :cond_2

    iget v1, v3, Lz0/f;->n:F

    cmpl-float v2, v1, v8

    if-eqz v2, :cond_1

    iget-object v2, v3, Lz0/f;->o:Lz0/g;

    float-to-double v9, v1

    iput-wide v9, v2, Lz0/g;->c:D

    iput v8, v3, Lz0/f;->n:F

    :cond_1
    iget-object v1, v3, Lz0/f;->o:Lz0/g;

    iget-wide v1, v1, Lz0/g;->c:D

    double-to-float v1, v1

    iput v1, v3, Lz0/c;->k:F

    iput v5, v3, Lz0/c;->l:F

    iput-boolean v6, v3, Lz0/f;->m:Z

    goto/16 :goto_2

    :cond_2
    iget v4, v3, Lz0/f;->n:F

    cmpl-float v4, v4, v8

    if-eqz v4, :cond_3

    iget-object v4, v3, Lz0/f;->o:Lz0/g;

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v9, v3, Lz0/f;->o:Lz0/g;

    iget v4, v3, Lz0/c;->k:F

    float-to-double v10, v4

    iget v4, v3, Lz0/c;->l:F

    float-to-double v12, v4

    const-wide/16 v14, 0x2

    div-long/2addr v1, v14

    move-wide v14, v1

    invoke-virtual/range {v9 .. v15}, Lz0/g;->c(DDJ)Lz0/c$a;

    move-result-object v4

    iget-object v9, v3, Lz0/f;->o:Lz0/g;

    iget v10, v3, Lz0/f;->n:F

    float-to-double v10, v10

    iput-wide v10, v9, Lz0/g;->c:D

    iput v8, v3, Lz0/f;->n:F

    iget v10, v4, Lz0/c$a;->a:F

    float-to-double v10, v10

    iget v4, v4, Lz0/c$a;->b:F

    goto :goto_0

    :cond_3
    iget-object v9, v3, Lz0/f;->o:Lz0/g;

    iget v4, v3, Lz0/c;->k:F

    float-to-double v10, v4

    iget v4, v3, Lz0/c;->l:F

    :goto_0
    float-to-double v12, v4

    move-wide/from16 v19, v1

    move-object v14, v9

    move-wide v15, v10

    move-wide/from16 v17, v12

    invoke-virtual/range {v14 .. v20}, Lz0/g;->c(DDJ)Lz0/c$a;

    move-result-object v1

    iget v2, v1, Lz0/c$a;->a:F

    iput v2, v3, Lz0/c;->k:F

    iget v1, v1, Lz0/c$a;->b:F

    iput v1, v3, Lz0/c;->l:F

    iget v1, v3, Lz0/c;->c:F

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iput v1, v3, Lz0/c;->k:F

    invoke-static {v1, v8}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iput v1, v3, Lz0/c;->k:F

    iget v2, v3, Lz0/c;->l:F

    iget-object v4, v3, Lz0/f;->o:Lz0/g;

    invoke-static {v4}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-double v9, v2

    iget-wide v11, v4, Lz0/g;->j:D

    cmpg-double v2, v9, v11

    if-gez v2, :cond_4

    iget-wide v9, v4, Lz0/g;->c:D

    double-to-float v2, v9

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    float-to-double v1, v1

    iget-wide v9, v4, Lz0/g;->i:D

    cmpg-double v1, v1, v9

    if-gez v1, :cond_4

    move v1, v7

    goto :goto_1

    :cond_4
    move v1, v6

    :goto_1
    if-eqz v1, :cond_5

    iget-object v1, v3, Lz0/f;->o:Lz0/g;

    iget-wide v1, v1, Lz0/g;->c:D

    double-to-float v1, v1

    iput v1, v3, Lz0/c;->k:F

    iput v5, v3, Lz0/c;->l:F

    goto :goto_2

    :cond_5
    move v7, v6

    :goto_2
    iget v1, v0, Lz0/c;->k:F

    invoke-static {v1, v8}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iput v1, v0, Lz0/c;->k:F

    iget v2, v0, Lz0/c;->c:F

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iput v1, v0, Lz0/c;->k:F

    invoke-virtual {v0, v1}, Lz0/c;->g(F)V

    if-eqz v7, :cond_6

    invoke-virtual {v0, v6}, Lz0/c;->d(Z)V

    :cond_6
    return v7
.end method

.method public b(Lz0/c$c;)Lz0/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lz0/c$c;",
            ")TT;"
        }
    .end annotation

    iget-boolean v0, p0, Lz0/c;->f:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lz0/c;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lz0/c;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object p0

    :cond_1
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Error: Update listeners must be added beforethe miuix.animation."

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public c()V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lz0/c;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lz0/c;->d(Z)V

    :cond_0
    return-void

    :cond_1
    new-instance v0, Landroid/util/AndroidRuntimeException;

    const-string v1, "Animations may only be canceled on the main thread"

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final d(Z)V
    .locals 4

    const/4 v0, 0x0

    iput-boolean v0, p0, Lz0/c;->f:Z

    invoke-static {}, Lz0/b;->b()Lz0/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lz0/b;->c(Lz0/b$b;)V

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lz0/c;->b:J

    iput-boolean v0, p0, Lz0/c;->h:Z

    :goto_0
    iget-object v1, p0, Lz0/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lz0/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lz0/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lz0/c$b;

    iget v2, p0, Lz0/c;->k:F

    iget v3, p0, Lz0/c;->l:F

    invoke-interface {v1, p0, p1, v2, v3}, Lz0/c$b;->a(Lz0/c;ZFF)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lz0/c;->a:Ljava/util/ArrayList;

    invoke-static {p1}, Lz0/c;->e(Ljava/util/ArrayList;)V

    return-void
.end method

.method public f(F)Lz0/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)TT;"
        }
    .end annotation

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-lez v0, :cond_0

    iput p1, p0, Lz0/c;->d:F

    return-object p0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Minimum visible change must be positive."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public g(F)V
    .locals 3

    iget-object v0, p0, Lz0/c;->e:La1/b;

    iget-object v1, p0, Lz0/c;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, La1/b;->d(Ljava/lang/Object;F)V

    const/4 p1, 0x0

    :goto_0
    iget-object v0, p0, Lz0/c;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    iget-object v0, p0, Lz0/c;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lz0/c;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lz0/c$c;

    iget v1, p0, Lz0/c;->k:F

    iget v2, p0, Lz0/c;->l:F

    invoke-interface {v0, p0, v1, v2}, Lz0/c$c;->a(Lz0/c;FF)V

    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lz0/c;->j:Ljava/util/ArrayList;

    invoke-static {p1}, Lz0/c;->e(Ljava/util/ArrayList;)V

    return-void
.end method

.method public h(J)V
    .locals 3

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-gez v2, :cond_0

    move-wide p1, v0

    :cond_0
    iput-wide p1, p0, Lz0/c;->g:J

    return-void
.end method
