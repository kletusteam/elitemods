.class public final Lz0/g;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:D

.field public b:D

.field public c:D

.field public d:D

.field public e:D

.field public f:Z

.field public final g:Lz0/c$a;

.field public h:D

.field public i:D

.field public j:D


# direct methods
.method public constructor <init>(F)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide v0, 0x4097700000000000L    # 1500.0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    iput-wide v0, p0, Lz0/g;->h:D

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    iput-wide v0, p0, Lz0/g;->b:D

    const/4 v0, 0x0

    iput-boolean v0, p0, Lz0/g;->f:Z

    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lz0/g;->c:D

    new-instance v0, Lz0/c$a;

    invoke-direct {v0}, Lz0/c$a;-><init>()V

    iput-object v0, p0, Lz0/g;->g:Lz0/c$a;

    float-to-double v0, p1

    iput-wide v0, p0, Lz0/g;->c:D

    return-void
.end method


# virtual methods
.method public a(F)Lz0/g;
    .locals 2

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-ltz v0, :cond_0

    float-to-double v0, p1

    iput-wide v0, p0, Lz0/g;->b:D

    const/4 p1, 0x0

    iput-boolean p1, p0, Lz0/g;->f:Z

    return-object p0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Damping ratio must be non-negative"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public b(F)Lz0/g;
    .locals 2

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-lez v0, :cond_0

    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    iput-wide v0, p0, Lz0/g;->h:D

    const/4 p1, 0x0

    iput-boolean p1, p0, Lz0/g;->f:Z

    return-object p0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Spring stiffness constant must be positive."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public c(DDJ)Lz0/c$a;
    .locals 16

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lz0/g;->f:Z

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    iget-wide v4, v0, Lz0/g;->c:D

    const-wide v6, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v1, v4, v6

    if-eqz v1, :cond_5

    iget-wide v4, v0, Lz0/g;->b:D

    cmpl-double v1, v4, v2

    if-lez v1, :cond_1

    neg-double v6, v4

    iget-wide v8, v0, Lz0/g;->h:D

    mul-double/2addr v6, v8

    mul-double/2addr v4, v4

    sub-double/2addr v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    mul-double/2addr v4, v8

    add-double/2addr v4, v6

    iput-wide v4, v0, Lz0/g;->e:D

    iget-wide v4, v0, Lz0/g;->b:D

    neg-double v6, v4

    iget-wide v8, v0, Lz0/g;->h:D

    mul-double/2addr v6, v8

    mul-double/2addr v4, v4

    sub-double/2addr v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    mul-double/2addr v4, v8

    sub-double/2addr v6, v4

    iput-wide v6, v0, Lz0/g;->d:D

    goto :goto_0

    :cond_1
    const-wide/16 v6, 0x0

    cmpl-double v1, v4, v6

    if-ltz v1, :cond_2

    cmpg-double v1, v4, v2

    if-gez v1, :cond_2

    iget-wide v6, v0, Lz0/g;->h:D

    mul-double/2addr v4, v4

    sub-double v4, v2, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    mul-double/2addr v4, v6

    iput-wide v4, v0, Lz0/g;->a:D

    :cond_2
    :goto_0
    const/4 v1, 0x1

    iput-boolean v1, v0, Lz0/g;->f:Z

    :goto_1
    move-wide/from16 v4, p5

    long-to-double v4, v4

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    iget-wide v6, v0, Lz0/g;->c:D

    sub-double v6, p1, v6

    iget-wide v8, v0, Lz0/g;->b:D

    cmpl-double v1, v8, v2

    const-wide v10, 0x4005bf0a8b145769L    # Math.E

    if-lez v1, :cond_3

    iget-wide v1, v0, Lz0/g;->d:D

    iget-wide v8, v0, Lz0/g;->e:D

    mul-double v12, v1, v6

    sub-double v12, v12, p3

    sub-double v8, v1, v8

    div-double/2addr v12, v8

    sub-double/2addr v6, v12

    mul-double/2addr v1, v4

    invoke-static {v10, v11, v1, v2}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    mul-double/2addr v1, v6

    iget-wide v8, v0, Lz0/g;->e:D

    mul-double/2addr v8, v4

    invoke-static {v10, v11, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    mul-double/2addr v8, v12

    add-double/2addr v8, v1

    iget-wide v1, v0, Lz0/g;->d:D

    mul-double v14, v1, v4

    invoke-static {v10, v11, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v14

    iget-wide v10, v0, Lz0/g;->e:D

    mul-double/2addr v6, v1

    mul-double/2addr v6, v14

    mul-double/2addr v12, v10

    mul-double/2addr v10, v4

    const-wide v1, 0x4005bf0a8b145769L    # Math.E

    invoke-static {v1, v2, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    mul-double/2addr v1, v12

    add-double/2addr v1, v6

    goto/16 :goto_2

    :cond_3
    if-nez v1, :cond_4

    iget-wide v1, v0, Lz0/g;->h:D

    mul-double v8, v1, v6

    add-double v8, v8, p3

    mul-double v10, v8, v4

    add-double/2addr v10, v6

    neg-double v1, v1

    mul-double/2addr v1, v4

    const-wide v6, 0x4005bf0a8b145769L    # Math.E

    invoke-static {v6, v7, v1, v2}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    mul-double/2addr v1, v10

    iget-wide v12, v0, Lz0/g;->h:D

    neg-double v12, v12

    mul-double/2addr v12, v4

    invoke-static {v6, v7, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    iget-wide v14, v0, Lz0/g;->h:D

    neg-double v14, v14

    mul-double/2addr v4, v14

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    mul-double/2addr v3, v8

    mul-double/2addr v10, v12

    mul-double/2addr v10, v14

    add-double/2addr v3, v10

    move-wide v8, v1

    move-wide v1, v3

    goto :goto_2

    :cond_4
    iget-wide v10, v0, Lz0/g;->a:D

    div-double/2addr v2, v10

    iget-wide v10, v0, Lz0/g;->h:D

    mul-double v12, v8, v10

    mul-double/2addr v12, v6

    add-double v12, v12, p3

    mul-double/2addr v12, v2

    neg-double v1, v8

    mul-double/2addr v1, v10

    mul-double/2addr v1, v4

    const-wide v8, 0x4005bf0a8b145769L    # Math.E

    invoke-static {v8, v9, v1, v2}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    iget-wide v8, v0, Lz0/g;->a:D

    mul-double/2addr v8, v4

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    mul-double/2addr v8, v6

    iget-wide v10, v0, Lz0/g;->a:D

    mul-double/2addr v10, v4

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v10, v12

    add-double/2addr v10, v8

    mul-double v8, v10, v1

    iget-wide v1, v0, Lz0/g;->h:D

    neg-double v10, v1

    iget-wide v14, v0, Lz0/g;->b:D

    move-wide/from16 p3, v12

    neg-double v12, v14

    mul-double/2addr v12, v1

    mul-double/2addr v12, v4

    const-wide v1, 0x4005bf0a8b145769L    # Math.E

    invoke-static {v1, v2, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    iget-wide v12, v0, Lz0/g;->a:D

    move-wide/from16 p1, v1

    neg-double v1, v12

    mul-double/2addr v12, v4

    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    move-wide/from16 p5, v4

    iget-wide v3, v0, Lz0/g;->a:D

    mul-double/2addr v10, v8

    mul-double/2addr v10, v14

    mul-double/2addr v1, v6

    mul-double/2addr v1, v12

    move-wide/from16 v12, p3

    mul-double/2addr v12, v3

    move-wide/from16 v5, p5

    mul-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->cos(D)D

    move-result-wide v3

    mul-double/2addr v3, v12

    add-double/2addr v3, v1

    move-wide/from16 v1, p1

    mul-double/2addr v3, v1

    add-double v1, v3, v10

    :goto_2
    iget-object v3, v0, Lz0/g;->g:Lz0/c$a;

    iget-wide v4, v0, Lz0/g;->c:D

    add-double/2addr v8, v4

    double-to-float v4, v8

    iput v4, v3, Lz0/c$a;->a:F

    double-to-float v1, v1

    iput v1, v3, Lz0/c$a;->b:F

    return-object v3

    :cond_5
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Error: Final position of the spring must be set before the miuix.animation starts"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
