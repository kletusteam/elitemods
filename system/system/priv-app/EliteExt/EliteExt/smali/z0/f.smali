.class public final Lz0/f;
.super Lz0/c;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lz0/c<",
        "Lz0/f;",
        ">;"
    }
.end annotation


# instance fields
.field public m:Z

.field public n:F

.field public o:Lz0/g;


# direct methods
.method public constructor <init>(Ljava/lang/Object;La1/b;F)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            ">(TK;",
            "La1/b<",
            "TK;>;F)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lz0/c;-><init>(Ljava/lang/Object;La1/b;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lz0/f;->o:Lz0/g;

    const p1, 0x7f7fffff    # Float.MAX_VALUE

    iput p1, p0, Lz0/f;->n:F

    const/4 p1, 0x0

    iput-boolean p1, p0, Lz0/f;->m:Z

    new-instance p1, Lz0/g;

    invoke-direct {p1, p3}, Lz0/g;-><init>(F)V

    iput-object p1, p0, Lz0/f;->o:Lz0/g;

    return-void
.end method


# virtual methods
.method public i()V
    .locals 6

    iget-object v0, p0, Lz0/f;->o:Lz0/g;

    if-eqz v0, :cond_6

    iget-wide v1, v0, Lz0/g;->c:D

    double-to-float v1, v1

    float-to-double v1, v1

    const v3, 0x7f7fffff    # Float.MAX_VALUE

    float-to-double v4, v3

    cmpl-double v4, v1, v4

    if-gtz v4, :cond_5

    iget v4, p0, Lz0/c;->c:F

    float-to-double v4, v4

    cmpg-double v1, v1, v4

    if-ltz v1, :cond_4

    iget v1, p0, Lz0/c;->d:F

    const/high16 v2, 0x3f400000    # 0.75f

    mul-float/2addr v1, v2

    float-to-double v1, v1

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/Math;->abs(D)D

    move-result-wide v1

    iput-wide v1, v0, Lz0/g;->i:D

    const-wide v4, 0x404f400000000000L    # 62.5

    mul-double/2addr v1, v4

    iput-wide v1, v0, Lz0/g;->j:D

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_3

    iget-boolean v0, p0, Lz0/c;->f:Z

    if-nez v0, :cond_2

    if-nez v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lz0/c;->f:Z

    iget-boolean v0, p0, Lz0/c;->h:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lz0/c;->e:La1/b;

    iget-object v1, p0, Lz0/c;->i:Ljava/lang/Object;

    invoke-virtual {v0, v1}, La1/b;->c(Ljava/lang/Object;)F

    move-result v0

    iput v0, p0, Lz0/c;->k:F

    :cond_0
    iget v0, p0, Lz0/c;->k:F

    cmpl-float v1, v0, v3

    if-gtz v1, :cond_1

    iget v1, p0, Lz0/c;->c:F

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_1

    invoke-static {}, Lz0/b;->b()Lz0/b;

    move-result-object v0

    iget-wide v1, p0, Lz0/c;->g:J

    invoke-virtual {v0, p0, v1, v2}, Lz0/b;->a(Lz0/b$b;J)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Starting value need to be in between min value and max value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    :goto_0
    return-void

    :cond_3
    new-instance v0, Landroid/util/AndroidRuntimeException;

    const-string v1, "Animations may only be started on the main thread"

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Final position of the spring cannot be less than the min value."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Final position of the spring cannot be greater than the max value."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Incomplete SpringAnimation: Either final position or a spring force needs to be set."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
