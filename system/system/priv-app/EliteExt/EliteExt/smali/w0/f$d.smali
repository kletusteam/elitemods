.class public final Lw0/f$d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lw0/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "d"
.end annotation


# instance fields
.field public a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lw0/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lw0/f$a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lw0/f;)V
    .locals 1

    iget-object p1, p1, Lw0/b;->a:Ljava/lang/Object;

    check-cast p1, Lw0/h;

    invoke-interface {p1}, Lw0/h;->f()Lu0/b;

    move-result-object p1

    instance-of v0, p1, Lmiuix/animation/ViewTarget;

    if-eqz v0, :cond_0

    check-cast p1, Lmiuix/animation/ViewTarget;

    invoke-virtual {p1}, Lmiuix/animation/ViewTarget;->q()Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1, p0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public run()V
    .locals 3

    iget-object v0, p0, Lw0/f$d;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lw0/f;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lw0/b;->a:Ljava/lang/Object;

    check-cast v1, Lw0/h;

    invoke-interface {v1}, Lw0/h;->f()Lu0/b;

    move-result-object v1

    instance-of v2, v1, Lmiuix/animation/ViewTarget;

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lu0/b;->g()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v2, v0, Lw0/f;->n:Landroid/view/View$OnLongClickListener;

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->performLongClick()Z

    iget-boolean v2, v0, Lw0/f;->m:Z

    if-nez v2, :cond_0

    const/4 v2, 0x1

    iput-boolean v2, v0, Lw0/f;->m:Z

    iget-object v0, v0, Lw0/f;->n:Landroid/view/View$OnLongClickListener;

    invoke-interface {v0, v1}, Landroid/view/View$OnLongClickListener;->onLongClick(Landroid/view/View;)Z

    :cond_0
    return-void
.end method
