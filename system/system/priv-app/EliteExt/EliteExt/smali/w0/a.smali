.class public Lw0/a;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final g:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public final a:Z

.field public final b:Lv0/a;

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field public volatile d:Ljava/lang/Object;

.field public e:La1/e;

.field public f:La1/f;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lw0/a;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lw0/a;-><init>(Ljava/lang/Object;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, La1/f;

    const-string v1, ""

    invoke-direct {v0, v1}, La1/f;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lw0/a;->f:La1/f;

    new-instance v0, La1/e;

    invoke-direct {v0, v1}, La1/e;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lw0/a;->e:La1/e;

    new-instance v0, Lv0/a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lv0/a;-><init>(Z)V

    iput-object v0, p0, Lw0/a;->b:Lv0/a;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lw0/a;->c:Ljava/util/Map;

    invoke-virtual {p0, p1}, Lw0/a;->i(Ljava/lang/Object;)V

    iput-boolean p2, p0, Lw0/a;->a:Z

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    iget-object v1, p0, Lw0/a;->c:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_1
    instance-of v1, p1, La1/b;

    if-eqz v1, :cond_2

    iget-object v0, p0, Lw0/a;->c:Ljava/util/Map;

    check-cast p1, La1/b;

    invoke-virtual {p1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_2
    return v0
.end method

.method public b(Lu0/b;La1/b;)D
    .locals 9

    iget-object v0, p0, Lw0/a;->c:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    if-nez v0, :cond_0

    instance-of v1, p2, La1/b;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lw0/a;->c:Ljava/util/Map;

    invoke-virtual {p2}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    :cond_0
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p0, p2}, Lw0/a;->c(Ljava/lang/Object;)J

    move-result-wide v2

    const-wide/16 v4, 0x1

    invoke-static {v2, v3, v4, v5}, Lc1/a;->b(JJ)Z

    move-result v4

    if-nez v4, :cond_1

    const-wide v5, 0x412e848000000000L    # 1000000.0

    cmpl-double v5, v0, v5

    if-eqz v5, :cond_1

    const-wide v5, 0x412e854800000000L    # 1000100.0

    cmpl-double v5, v0, v5

    if-eqz v5, :cond_1

    instance-of v5, p2, La1/d;

    if-eqz v5, :cond_4

    :cond_1
    invoke-static {p1, p2, v0, v1}, Landroidx/emoji2/text/l;->x(Lu0/b;La1/b;D)D

    move-result-wide v5

    if-eqz v4, :cond_3

    invoke-static {v0, v1}, Landroidx/emoji2/text/l;->G(D)Z

    move-result p1

    if-nez p1, :cond_3

    const-wide/16 v7, -0x2

    and-long/2addr v2, v7

    instance-of p1, p2, La1/b;

    if-eqz p1, :cond_2

    invoke-virtual {p2}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_2
    move-object p1, p2

    check-cast p1, Ljava/lang/String;

    :goto_0
    iget-object v4, p0, Lw0/a;->b:Lv0/a;

    invoke-virtual {v4, p1}, Lv0/a;->d(Ljava/lang/String;)Lv0/c;

    move-result-object p1

    iput-wide v2, p1, Lv0/a;->c:J

    add-double/2addr v5, v0

    invoke-virtual {p0, p2, v5, v6}, Lw0/a;->h(Ljava/lang/Object;D)V

    :cond_3
    move-wide v0, v5

    :cond_4
    return-wide v0

    :cond_5
    const-wide p1, 0x7fefffffffffffffL    # Double.MAX_VALUE

    return-wide p1
.end method

.method public c(Ljava/lang/Object;)J
    .locals 2

    instance-of v0, p1, La1/b;

    if-eqz v0, :cond_0

    check-cast p1, La1/b;

    invoke-virtual {p1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    check-cast p1, Ljava/lang/String;

    :goto_0
    iget-object v0, p0, Lw0/a;->b:Lv0/a;

    invoke-virtual {v0, p1}, Lv0/a;->c(Ljava/lang/String;)Lv0/c;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-wide v0, p1, Lv0/a;->c:J

    goto :goto_1

    :cond_1
    const-wide/16 v0, 0x0

    :goto_1
    return-wide v0
.end method

.method public d(Ljava/lang/Object;)La1/b;
    .locals 4

    instance-of v0, p1, La1/b;

    if-eqz v0, :cond_0

    check-cast p1, La1/b;

    return-object p1

    :cond_0
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lw0/a;->c(Ljava/lang/Object;)J

    move-result-wide v0

    const-wide/16 v2, 0x4

    invoke-static {v0, v1, v2, v3}, Lc1/a;->b(JJ)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, La1/e;

    invoke-direct {v0, p1}, La1/e;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-instance v0, La1/f;

    invoke-direct {v0, p1}, La1/f;-><init>(Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method public e(Ljava/lang/Object;)La1/b;
    .locals 4

    instance-of v0, p1, La1/b;

    if-eqz v0, :cond_0

    check-cast p1, La1/b;

    return-object p1

    :cond_0
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lw0/a;->c(Ljava/lang/Object;)J

    move-result-wide v0

    const-wide/16 v2, 0x4

    invoke-static {v0, v1, v2, v3}, Lc1/a;->b(JJ)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lw0/a;->e:La1/e;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lw0/a;->f:La1/f;

    :goto_0
    iput-object p1, v0, La1/f;->b:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lw0/a;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public g(Lw0/a;)V
    .locals 2

    iget-object v0, p1, Lw0/a;->d:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lw0/a;->i(Ljava/lang/Object;)V

    iget-object v0, p0, Lw0/a;->b:Lv0/a;

    iget-object v1, p1, Lw0/a;->b:Lv0/a;

    invoke-virtual {v0, v1}, Lv0/a;->b(Lv0/a;)V

    iget-object v0, p0, Lw0/a;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lw0/a;->c:Ljava/util/Map;

    iget-object p1, p1, Lw0/a;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    return-void
.end method

.method public final h(Ljava/lang/Object;D)V
    .locals 3

    instance-of v0, p1, La1/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lw0/a;->c:Ljava/util/Map;

    move-object v1, p1

    check-cast v1, La1/b;

    invoke-virtual {v1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lw0/a;->c:Ljava/util/Map;

    invoke-virtual {v1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, p3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p2

    invoke-interface {p1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lw0/a;->c:Ljava/util/Map;

    invoke-static {p2, p3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void
.end method

.method public final i(Ljava/lang/Object;)V
    .locals 1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, "TAG_"

    invoke-static {p1}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    sget-object v0, Lw0/a;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lw0/a;->d:Ljava/lang/Object;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, "\nAnimState{mTag=\'"

    invoke-static {v0}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lw0/a;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", flags:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", mMaps="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lw0/a;->c:Ljava/util/Map;

    const-string v2, "    "

    invoke-static {v1, v2}, Lc1/a;->c(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
