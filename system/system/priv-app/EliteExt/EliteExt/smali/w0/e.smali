.class public Lw0/e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lw0/h;


# instance fields
.field public a:Lv0/b;

.field public b:Z

.field public c:Lw0/l;

.field public d:Lu0/b;


# direct methods
.method public constructor <init>(Lu0/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lw0/l;

    invoke-direct {v0}, Lw0/l;-><init>()V

    iput-object v0, p0, Lw0/e;->c:Lw0/l;

    new-instance v0, Lv0/b;

    invoke-direct {v0}, Lv0/b;-><init>()V

    iput-object v0, p0, Lw0/e;->a:Lv0/b;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lw0/e;->b:Z

    iput-object p1, p0, Lw0/e;->d:Lu0/b;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Lw0/h;
    .locals 1

    new-instance v0, Lv0/b;

    invoke-direct {v0}, Lv0/b;-><init>()V

    invoke-virtual {p0, p1, v0}, Lw0/e;->m(Ljava/lang/Object;Lv0/b;)Lw0/h;

    move-result-object p1

    return-object p1
.end method

.method public b(Ljava/lang/Object;)Lw0/h;
    .locals 1

    iget-object v0, p0, Lw0/e;->c:Lw0/l;

    invoke-virtual {v0, p1}, Lw0/l;->e(Ljava/lang/Object;)Lw0/a;

    return-object p0
.end method

.method public varargs c([Ljava/lang/Object;)Lw0/h;
    .locals 4

    iget-object v0, p0, Lw0/e;->a:Lv0/b;

    iget-object v1, p0, Lw0/e;->c:Lw0/l;

    iget-object v2, p0, Lw0/e;->d:Lu0/b;

    iget-object v3, v1, Lw0/l;->b:Lw0/a;

    invoke-virtual {v1, v3, p1}, Lw0/l;->c(Ljava/lang/Object;[Ljava/lang/Object;)Lw0/a;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0, p1}, Lw0/l;->d(Lu0/b;Lw0/a;Lv0/b;[Ljava/lang/Object;)V

    invoke-virtual {p0, v3, v0}, Lw0/e;->m(Ljava/lang/Object;Lv0/b;)Lw0/h;

    return-object p0
.end method

.method public cancel()V
    .locals 5

    sget-object v0, Lx0/e;->g:Landroid/os/Handler;

    sget-object v0, Lx0/e$b;->a:Lx0/e;

    iget-object v1, p0, Lw0/e;->d:Lu0/b;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lx0/e;->h:Lx0/k;

    new-instance v2, Lx0/d;

    const/4 v3, 0x4

    const/4 v4, 0x0

    invoke-direct {v2, v1, v3, v4, v4}, Lx0/d;-><init>(Lu0/b;B[Ljava/lang/String;[La1/b;)V

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v3, 0x0

    new-array v3, v3, [La1/b;

    iget-object v4, v1, Lu0/b;->a:Lx0/c;

    invoke-virtual {v4, v3}, Lx0/c;->d([La1/b;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v3

    iput-wide v3, v2, Lx0/d;->c:J

    iget-object v0, v0, Lx0/k;->e:Ljava/util/Map;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public d(Ljava/lang/Object;)Lw0/a;
    .locals 2

    iget-object v0, p0, Lw0/e;->c:Lw0/l;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lw0/l;->b(Ljava/lang/Object;Z)Lw0/a;

    move-result-object p1

    return-object p1
.end method

.method public e(Lw0/a;)V
    .locals 2

    iget-object v0, p0, Lw0/e;->c:Lw0/l;

    iget-object v0, v0, Lw0/l;->d:Ljava/util/Map;

    iget-object v1, p1, Lw0/a;->d:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public f()Lu0/b;
    .locals 1

    iget-object v0, p0, Lw0/e;->d:Lu0/b;

    return-object v0
.end method

.method public g()Lw0/a;
    .locals 2

    iget-object v0, p0, Lw0/e;->c:Lw0/l;

    iget-object v1, v0, Lw0/l;->a:Ljava/lang/Object;

    if-nez v1, :cond_0

    iget-object v1, v0, Lw0/l;->e:Lw0/a;

    iput-object v1, v0, Lw0/l;->a:Ljava/lang/Object;

    :cond_0
    iget-object v1, v0, Lw0/l;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lw0/l;->a(Ljava/lang/Object;)Lw0/a;

    move-result-object v0

    return-object v0
.end method

.method public varargs h([Ljava/lang/Object;)Lw0/h;
    .locals 4

    iget-object v0, p0, Lw0/e;->c:Lw0/l;

    iget-object v1, p0, Lw0/e;->d:Lu0/b;

    iget-object v2, p0, Lw0/e;->a:Lv0/b;

    iget-object v3, v0, Lw0/l;->a:Ljava/lang/Object;

    if-nez v3, :cond_0

    iget-object v3, v0, Lw0/l;->e:Lw0/a;

    iput-object v3, v0, Lw0/l;->a:Ljava/lang/Object;

    :cond_0
    iget-object v3, v0, Lw0/l;->a:Ljava/lang/Object;

    invoke-virtual {v0, v3}, Lw0/l;->a(Ljava/lang/Object;)Lw0/a;

    move-result-object v3

    invoke-virtual {v0, v3, p1}, Lw0/l;->c(Ljava/lang/Object;[Ljava/lang/Object;)Lw0/a;

    move-result-object v3

    invoke-virtual {v0, v1, v3, v2, p1}, Lw0/l;->d(Lu0/b;Lw0/a;Lv0/b;[Ljava/lang/Object;)V

    const/4 p1, 0x0

    new-array p1, p1, [Lv0/a;

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v3, p1}, Lw0/e;->l(Ljava/lang/Object;Ljava/lang/Object;[Lv0/a;)Lw0/h;

    return-object p0
.end method

.method public i()V
    .locals 0

    invoke-virtual {p0}, Lw0/e;->cancel()V

    return-void
.end method

.method public j(J)Lw0/h;
    .locals 1

    iget-object v0, p0, Lw0/e;->d:Lu0/b;

    iput-wide p1, v0, Lu0/b;->e:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide p1

    iput-wide p1, v0, Lu0/b;->f:J

    return-object p0
.end method

.method public varargs k(Ljava/lang/Object;[Lv0/a;)Lw0/h;
    .locals 3

    instance-of v0, p1, Lw0/a;

    const/4 v1, 0x1

    if-nez v0, :cond_2

    iget-object v0, p0, Lw0/e;->c:Lw0/l;

    iget-object v0, v0, Lw0/l;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    invoke-static {p1}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v0

    array-length v1, p2

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length p1, p2

    invoke-static {p2, v2, v1, v0, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {p0, v1}, Lw0/e;->h([Ljava/lang/Object;)Lw0/h;

    return-object p0

    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    aput-object p2, v0, v1

    invoke-virtual {p0, v0}, Lw0/e;->h([Ljava/lang/Object;)Lw0/h;

    return-object p0

    :cond_2
    :goto_0
    const/4 v0, 0x0

    iget-object v2, p0, Lw0/e;->c:Lw0/l;

    invoke-virtual {v2, p1, v1}, Lw0/l;->b(Ljava/lang/Object;Z)Lw0/a;

    move-result-object p1

    invoke-virtual {p0, v0, p1, p2}, Lw0/e;->l(Ljava/lang/Object;Ljava/lang/Object;[Lv0/a;)Lw0/h;

    return-object p0
.end method

.method public varargs l(Ljava/lang/Object;Ljava/lang/Object;[Lv0/a;)Lw0/h;
    .locals 5

    iget-object p1, p0, Lw0/e;->a:Lv0/b;

    array-length v0, p3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_0

    aget-object v3, p3, v2

    new-array v4, v1, [Z

    invoke-virtual {p1, v3, v4}, Lv0/b;->a(Lv0/a;[Z)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iget-boolean p3, p0, Lw0/e;->b:Z

    if-eqz p3, :cond_4

    iget-object p3, p0, Lw0/e;->c:Lw0/l;

    invoke-virtual {p3, p2}, Lw0/l;->e(Ljava/lang/Object;)Lw0/a;

    iget-object p3, p0, Lw0/e;->c:Lw0/l;

    const/4 v0, 0x1

    invoke-virtual {p3, p2, v0}, Lw0/l;->b(Ljava/lang/Object;Z)Lw0/a;

    move-result-object p3

    iget-object v2, p0, Lw0/e;->c:Lw0/l;

    iget-object v2, v2, Lw0/l;->e:Lw0/a;

    if-eq p3, v2, :cond_1

    iget-object v2, v2, Lw0/a;->b:Lv0/a;

    new-array v1, v1, [Z

    invoke-virtual {p1, v2, v1}, Lv0/b;->a(Lv0/a;[Z)V

    :cond_1
    sget-object v1, Lx0/e;->g:Landroid/os/Handler;

    sget-object v1, Lx0/e$b;->a:Lx0/e;

    iget-object v2, p0, Lw0/e;->d:Lu0/b;

    iget-object v3, p0, Lw0/e;->c:Lw0/l;

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v0}, Lw0/l;->b(Ljava/lang/Object;Z)Lw0/a;

    move-result-object v3

    iget-object v4, p0, Lw0/e;->c:Lw0/l;

    invoke-virtual {v4, p2, v0}, Lw0/l;->b(Ljava/lang/Object;Z)Lw0/a;

    move-result-object p2

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lx0/p;

    invoke-direct {v0, v2, v3, p2, p1}, Lx0/p;-><init>(Lu0/b;Lw0/a;Lw0/a;Lv0/b;)V

    iget-object p2, v0, Lx0/p;->j:Lu0/b;

    new-instance v2, Lx0/f;

    invoke-direct {v2, v1, v0}, Lx0/f;-><init>(Lx0/e;Lx0/p;)V

    invoke-virtual {p2, v2}, Lu0/b;->b(Ljava/lang/Runnable;)V

    iget-object p2, p0, Lw0/e;->c:Lw0/l;

    iget-object v0, p2, Lw0/l;->e:Lw0/a;

    if-eq p3, v0, :cond_2

    iget-object p2, p2, Lw0/l;->b:Lw0/a;

    if-ne p3, p2, :cond_3

    :cond_2
    iget-object p2, p3, Lw0/a;->b:Lv0/a;

    invoke-virtual {p2}, Lv0/a;->a()V

    iget-object p2, p3, Lw0/a;->c:Ljava/util/Map;

    invoke-interface {p2}, Ljava/util/Map;->clear()V

    :cond_3
    iget-object p2, p1, Lv0/b;->a:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->clear()V

    iget-object p2, p1, Lv0/b;->c:Lv0/a;

    invoke-virtual {p2}, Lv0/a;->a()V

    iget-object p2, p1, Lv0/b;->a:Ljava/util/List;

    iget-object p1, p1, Lv0/b;->c:Lv0/a;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    return-object p0
.end method

.method public final m(Ljava/lang/Object;Lv0/b;)Lw0/h;
    .locals 2

    iget-object v0, p0, Lw0/e;->d:Lu0/b;

    if-nez v0, :cond_0

    return-object p0

    :cond_0
    instance-of v1, p1, Ljava/lang/Integer;

    if-nez v1, :cond_2

    instance-of v1, p1, Ljava/lang/Float;

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    new-instance v1, Lw0/d;

    invoke-direct {v1, p0, p1, p2}, Lw0/d;-><init>(Lw0/e;Ljava/lang/Object;Lv0/b;)V

    invoke-virtual {v0, v1}, Lu0/b;->b(Ljava/lang/Runnable;)V

    return-object p0

    :cond_2
    :goto_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    invoke-virtual {p0, v0}, Lw0/e;->c([Ljava/lang/Object;)Lw0/h;

    return-object p0
.end method
