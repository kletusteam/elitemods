.class public Lw0/f$a;
.super Ly0/b;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lw0/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lw0/f;


# direct methods
.method public constructor <init>(Lw0/f;)V
    .locals 0

    iput-object p1, p0, Lw0/f$a;->a:Lw0/f;

    invoke-direct {p0}, Ly0/b;-><init>()V

    return-void
.end method


# virtual methods
.method public b(Ljava/lang/Object;Ljava/util/Collection;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/Collection<",
            "Ly0/c;",
            ">;)V"
        }
    .end annotation

    sget-object v0, Lu0/f$a;->b:Lu0/f$a;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_b

    iget-object p1, p0, Lw0/f$a;->a:Lw0/f;

    iget-object p1, p1, Lw0/b;->a:Ljava/lang/Object;

    check-cast p1, Lw0/h;

    sget-object v0, Lu0/f$a;->c:Lu0/f$a;

    invoke-interface {p1, v0}, Lw0/h;->d(Ljava/lang/Object;)Lw0/a;

    move-result-object p1

    sget-object v0, Lw0/a;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ly0/c;

    iget-object v2, v1, Ly0/c;->d:La1/b;

    invoke-virtual {p1, v2}, Lw0/a;->a(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, v1, Ly0/c;->e:Z

    if-eqz v2, :cond_1

    iget-object v2, v1, Ly0/c;->d:La1/b;

    iget-object v1, v1, Ly0/c;->a:Lx0/b;

    iget-wide v3, v1, Lx0/b;->h:D

    double-to-int v1, v3

    int-to-double v3, v1

    goto :goto_1

    :cond_1
    iget-object v2, v1, Ly0/c;->d:La1/b;

    iget-object v1, v1, Ly0/c;->a:Lx0/b;

    iget-wide v3, v1, Lx0/b;->h:D

    double-to-float v1, v3

    float-to-double v3, v1

    :goto_1
    invoke-virtual {p1, v2, v3, v4}, Lw0/a;->h(Ljava/lang/Object;D)V

    goto :goto_0

    :cond_2
    const-class v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lc1/g;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {p1}, Lw0/a;->f()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    instance-of v3, v2, La1/b;

    const/4 v4, 0x0

    if-eqz v3, :cond_5

    move-object v3, v2

    check-cast v3, La1/b;

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ly0/c;

    iget-object v7, v6, Ly0/c;->d:La1/b;

    invoke-virtual {v7, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    :goto_3
    move-object v4, v6

    goto :goto_4

    :cond_5
    move-object v3, v2

    check-cast v3, Ljava/lang/String;

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ly0/c;

    iget-object v7, v6, Ly0/c;->d:La1/b;

    invoke-virtual {v7}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    goto :goto_3

    :cond_7
    :goto_4
    if-nez v4, :cond_3

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_8
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_9
    :goto_5
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p1, Lw0/a;->c:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    instance-of v2, v1, La1/b;

    if-eqz v2, :cond_9

    iget-object v2, p1, Lw0/a;->c:Ljava/util/Map;

    check-cast v1, La1/b;

    invoke-virtual {v1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    :cond_a
    invoke-static {v0}, Lc1/g;->c(Ljava/lang/Object;)V

    :cond_b
    return-void
.end method
