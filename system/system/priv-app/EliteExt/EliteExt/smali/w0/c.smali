.class public Lw0/c;
.super Lw0/b;
.source ""

# interfaces
.implements Lu0/g;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lw0/c$a;
    }
.end annotation


# instance fields
.field public b:Lv0/a;

.field public c:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v0, 0x0

    new-array v1, v0, [Lu0/b;

    invoke-direct {p0, v1}, Lw0/b;-><init>([Lu0/b;)V

    new-instance v1, Lv0/a;

    invoke-direct {v1, v0}, Lv0/a;-><init>(Z)V

    iput-object v1, p0, Lw0/c;->b:Lv0/a;

    const/4 v2, 0x3

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v2}, Lc1/c;->b(I[F)Lc1/c$a;

    move-result-object v0

    iput-object v0, v1, Lv0/a;->b:Lc1/c$a;

    return-void

    :array_0
    .array-data 4
        0x43af0000    # 350.0f
        0x3f666666    # 0.9f
        0x3f5c28f6    # 0.86f
    .end array-data
.end method


# virtual methods
.method public i()V
    .locals 1

    invoke-super {p0}, Lw0/b;->i()V

    const/4 v0, 0x0

    iput-object v0, p0, Lw0/b;->a:Ljava/lang/Object;

    return-void
.end method

.method public varargs o(I[Lv0/a;)Lu0/g;
    .locals 5

    sget-object v0, Lw0/c$a;->b:Lw0/c$a;

    iget-object v1, p0, Lw0/b;->a:Ljava/lang/Object;

    move-object v2, v1

    check-cast v2, Lw0/h;

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lw0/c;->c:Z

    const/4 v3, 0x1

    if-nez v2, :cond_0

    iput-boolean v3, p0, Lw0/c;->c:Z

    check-cast v1, Lw0/h;

    invoke-interface {v1, v0}, Lw0/h;->a(Ljava/lang/Object;)Lw0/h;

    :cond_0
    new-array v1, v3, [Lv0/a;

    const/4 v2, 0x0

    iget-object v3, p0, Lw0/c;->b:Lv0/a;

    aput-object v3, v1, v2

    invoke-static {p2, v1}, Lc1/a;->d([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p2

    check-cast p2, [Lv0/a;

    if-nez p1, :cond_1

    iget-object p1, p0, Lw0/b;->a:Ljava/lang/Object;

    check-cast p1, Lw0/h;

    invoke-interface {p1, v0, p2}, Lw0/h;->k(Ljava/lang/Object;[Lv0/a;)Lw0/h;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lw0/b;->a:Ljava/lang/Object;

    check-cast v0, Lw0/h;

    sget-object v1, Lw0/c$a;->c:Lw0/c$a;

    invoke-interface {v0, v1}, Lw0/h;->d(Ljava/lang/Object;)Lw0/a;

    move-result-object v0

    const/4 v2, 0x0

    int-to-double v3, p1

    invoke-virtual {v0, v2, v3, v4}, Lw0/a;->h(Ljava/lang/Object;D)V

    iget-object p1, p0, Lw0/b;->a:Ljava/lang/Object;

    check-cast p1, Lw0/h;

    invoke-interface {p1, v1, p2}, Lw0/h;->k(Ljava/lang/Object;[Lv0/a;)Lw0/h;

    :cond_2
    :goto_0
    return-object p0
.end method
