.class public Lw0/g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lw0/f;

.field public final synthetic b:Z

.field public final synthetic c:[Lv0/a;

.field public final synthetic d:Z

.field public final synthetic e:Landroid/view/View;


# direct methods
.method public constructor <init>(Lw0/f;ZLandroid/view/View;[Lv0/a;Z)V
    .locals 0

    iput-object p1, p0, Lw0/g;->a:Lw0/f;

    iput-boolean p2, p0, Lw0/g;->b:Z

    iput-object p3, p0, Lw0/g;->e:Landroid/view/View;

    iput-object p4, p0, Lw0/g;->c:[Lv0/a;

    iput-boolean p5, p0, Lw0/g;->d:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    iget-boolean v0, p0, Lw0/g;->b:Z

    if-nez v0, :cond_7

    iget-object v0, p0, Lw0/g;->a:Lw0/f;

    iget-object v1, p0, Lw0/g;->e:Landroid/view/View;

    iget-object v2, p0, Lw0/g;->c:[Lv0/a;

    iget-object v3, v0, Lw0/b;->a:Ljava/lang/Object;

    check-cast v3, Lw0/h;

    invoke-interface {v3}, Lw0/h;->f()Lu0/b;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    if-eqz v3, :cond_6

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    :goto_0
    if-eqz v3, :cond_2

    instance-of v6, v3, Landroid/widget/AbsListView;

    if-eqz v6, :cond_0

    check-cast v3, Landroid/widget/AbsListView;

    goto :goto_1

    :cond_0
    instance-of v6, v3, Landroid/view/View;

    if-eqz v6, :cond_1

    move-object v6, v3

    check-cast v6, Landroid/view/View;

    :cond_1
    invoke-interface {v3}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    goto :goto_0

    :cond_2
    move-object v3, v5

    :goto_1
    if-eqz v3, :cond_3

    new-instance v6, Ljava/lang/ref/WeakReference;

    invoke-direct {v6, v5}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v6, v0, Lw0/f;->k:Ljava/lang/ref/WeakReference;

    goto :goto_2

    :cond_3
    move-object v3, v5

    :goto_2
    if-eqz v3, :cond_6

    sget-boolean v6, Lc1/f;->a:Z

    if-eqz v6, :cond_4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "handleListViewTouch for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v6, v4}, Lc1/f;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    const v4, 0x7f0a00a8

    invoke-virtual {v3, v4}, Landroid/widget/AbsListView;->getTag(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lw0/i;

    if-nez v6, :cond_5

    new-instance v6, Lw0/i;

    invoke-direct {v6, v3}, Lw0/i;-><init>(Landroid/widget/AbsListView;)V

    invoke-virtual {v3, v4, v6}, Landroid/widget/AbsListView;->setTag(ILjava/lang/Object;)V

    :cond_5
    invoke-virtual {v3, v6}, Landroid/widget/AbsListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    new-instance v3, Lw0/f$b;

    invoke-direct {v3, v0, v2}, Lw0/f$b;-><init>(Lw0/f;[Lv0/a;)V

    iget-object v0, v6, Lw0/i;->c:Ljava/util/WeakHashMap;

    invoke-virtual {v0, v1, v3}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v4, 0x1

    :cond_6
    if-eqz v4, :cond_7

    iget-object v0, p0, Lw0/g;->a:Lw0/f;

    iget-object v1, p0, Lw0/g;->e:Landroid/view/View;

    iget-boolean v2, p0, Lw0/g;->d:Z

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1, v2}, Landroid/view/View;->setClickable(Z)V

    invoke-virtual {v1, v5}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_7
    return-void
.end method
