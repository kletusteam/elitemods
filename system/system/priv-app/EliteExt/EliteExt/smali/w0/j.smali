.class public Lw0/j;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Lc1/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lc1/i<",
            "Lw0/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lw0/j$a;

    invoke-direct {v0}, Lw0/j$a;-><init>()V

    sput-object v0, Lw0/j;->a:Lc1/i;

    return-void
.end method

.method public static varargs a([Lu0/b;)Lw0/h;
    .locals 7

    const/4 v0, 0x0

    if-eqz p0, :cond_4

    array-length v1, p0

    if-nez v1, :cond_0

    goto :goto_1

    :cond_0
    array-length v1, p0

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v1, v2, :cond_1

    new-instance v0, Lw0/e;

    aget-object p0, p0, v3

    invoke-direct {v0, p0}, Lw0/e;-><init>(Lu0/b;)V

    return-object v0

    :cond_1
    array-length v1, p0

    new-array v1, v1, [Lw0/e;

    move v4, v3

    :goto_0
    array-length v5, p0

    if-ge v4, v5, :cond_2

    new-instance v5, Lw0/e;

    aget-object v6, p0, v4

    invoke-direct {v5, v6}, Lw0/e;-><init>(Lu0/b;)V

    aput-object v5, v1, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    const-class p0, Lw0/h;

    sget-object v4, Lw0/j;->a:Lc1/i;

    new-instance v5, Lc1/h;

    invoke-direct {v5, v4, v1, p0}, Lc1/h;-><init>(Lc1/i;[Ljava/lang/Object;Ljava/lang/Class;)V

    invoke-virtual {p0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Class;

    aput-object p0, v2, v3

    invoke-static {v1, v2, v5}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :cond_3
    check-cast v0, Lw0/h;

    :cond_4
    :goto_1
    return-object v0
.end method
