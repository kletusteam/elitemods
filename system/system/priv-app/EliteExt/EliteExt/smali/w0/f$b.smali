.class public Lw0/f$b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lw0/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field public a:[Lv0/a;

.field public b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lw0/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>(Lw0/f;[Lv0/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lw0/f$b;->b:Ljava/lang/ref/WeakReference;

    iput-object p2, p0, Lw0/f$b;->a:[Lv0/a;

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    iget-object v0, p0, Lw0/f$b;->b:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lw0/f;

    :goto_0
    if-eqz v0, :cond_2

    if-nez p2, :cond_1

    iget-object p1, p0, Lw0/f$b;->a:[Lv0/a;

    invoke-virtual {v0, p1}, Lw0/f;->r([Lv0/a;)V

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lw0/f$b;->a:[Lv0/a;

    invoke-static {v0, p1, p2, v1}, Lw0/f;->o(Lw0/f;Landroid/view/View;Landroid/view/MotionEvent;[Lv0/a;)V

    :cond_2
    :goto_1
    const/4 p1, 0x0

    return p1
.end method
