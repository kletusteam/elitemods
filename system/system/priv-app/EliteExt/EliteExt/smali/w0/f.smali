.class public Lw0/f;
.super Lw0/b;
.source ""

# interfaces
.implements Lu0/f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lw0/f$b;,
        Lw0/f$c;,
        Lw0/f$d;
    }
.end annotation


# static fields
.field public static v:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap<",
            "Landroid/view/View;",
            "Lw0/f$c;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Z

.field public c:Z

.field public d:Landroid/view/View$OnClickListener;

.field public e:Ly0/b;

.field public f:Lv0/a;

.field public g:F

.field public h:F

.field public i:Lw0/c;

.field public j:Z

.field public k:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public l:[I

.field public m:Z

.field public n:Landroid/view/View$OnLongClickListener;

.field public o:Lw0/f$d;

.field public p:F

.field public q:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lu0/f$a;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public r:Z

.field public s:I

.field public t:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public u:Lv0/a;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lw0/f;->v:Ljava/util/WeakHashMap;

    return-void
.end method

.method public varargs constructor <init>([Lu0/b;)V
    .locals 7

    invoke-direct {p0, p1}, Lw0/b;-><init>([Lu0/b;)V

    const/4 v0, 0x2

    new-array v1, v0, [I

    iput-object v1, p0, Lw0/f;->l:[I

    new-instance v1, Landroid/util/ArrayMap;

    invoke-direct {v1}, Landroid/util/ArrayMap;-><init>()V

    iput-object v1, p0, Lw0/f;->q:Ljava/util/Map;

    new-instance v1, Lv0/a;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lv0/a;-><init>(Z)V

    iput-object v1, p0, Lw0/f;->f:Lv0/a;

    new-instance v1, Lv0/a;

    invoke-direct {v1, v2}, Lv0/a;-><init>(Z)V

    iput-object v1, p0, Lw0/f;->u:Lv0/a;

    iput-boolean v2, p0, Lw0/f;->b:Z

    new-instance v1, Lw0/f$a;

    invoke-direct {v1, p0}, Lw0/f$a;-><init>(Lw0/f;)V

    iput-object v1, p0, Lw0/f;->e:Ly0/b;

    array-length v1, p1

    const/4 v3, 0x0

    if-lez v1, :cond_0

    aget-object p1, p1, v2

    goto :goto_0

    :cond_0
    move-object p1, v3

    :goto_0
    instance-of v1, p1, Lmiuix/animation/ViewTarget;

    if-eqz v1, :cond_1

    check-cast p1, Lmiuix/animation/ViewTarget;

    invoke-virtual {p1}, Lmiuix/animation/ViewTarget;->q()Landroid/view/View;

    move-result-object v3

    :cond_1
    const/4 p1, 0x1

    if-eqz v3, :cond_2

    const/high16 v1, 0x41200000    # 10.0f

    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {p1, v1, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    iput v1, p0, Lw0/f;->p:F

    :cond_2
    sget-object v1, La1/h;->h:La1/h;

    sget-object v3, La1/h;->i:La1/h;

    iget-object v4, p0, Lw0/b;->a:Ljava/lang/Object;

    check-cast v4, Lw0/h;

    sget-object v5, Lu0/f$a;->c:Lu0/f$a;

    invoke-interface {v4, v5}, Lw0/h;->d(Ljava/lang/Object;)Lw0/a;

    move-result-object v4

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v4, v1, v5, v6}, Lw0/a;->h(Ljava/lang/Object;D)V

    invoke-virtual {v4, v3, v5, v6}, Lw0/a;->h(Ljava/lang/Object;D)V

    invoke-virtual {p0}, Lw0/f;->w()V

    iget-object v1, p0, Lw0/f;->f:Lv0/a;

    new-array v3, v0, [F

    fill-array-data v3, :array_0

    const/4 v4, -0x2

    invoke-static {v4, v3}, Lc1/c;->b(I[F)Lc1/c$a;

    move-result-object v3

    iput-object v3, v1, Lv0/a;->b:Lc1/c$a;

    iget-object v1, p0, Lw0/f;->f:Lv0/a;

    new-array p1, p1, [Ly0/b;

    iget-object v3, p0, Lw0/f;->e:Ly0/b;

    aput-object v3, p1, v2

    iget-object v1, v1, Lv0/a;->e:Ljava/util/HashSet;

    invoke-static {v1, p1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    iget-object p1, p0, Lw0/f;->u:Lv0/a;

    new-array v1, v0, [F

    fill-array-data v1, :array_1

    invoke-virtual {p1, v4, v1}, Lv0/a;->f(I[F)Lv0/a;

    sget-object v1, La1/h;->b:La1/h;

    const-wide/16 v2, -0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_2

    invoke-virtual {p1, v1, v2, v3, v0}, Lv0/a;->g(La1/b;J[F)Lv0/a;

    return-void

    nop

    :array_0
    .array-data 4
        0x3f7d70a4    # 0.99f
        0x3e19999a    # 0.15f
    .end array-data

    :array_1
    .array-data 4
        0x3f7d70a4    # 0.99f
        0x3e99999a    # 0.3f
    .end array-data

    :array_2
    .array-data 4
        0x3f666666    # 0.9f
        0x3e4ccccd    # 0.2f
    .end array-data
.end method

.method public static o(Lw0/f;Landroid/view/View;Landroid/view/MotionEvent;[Lv0/a;)V
    .locals 7

    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_6

    if-eq v0, v2, :cond_4

    const/4 v3, 0x2

    if-eq v0, v3, :cond_0

    goto/16 :goto_0

    :cond_0
    iget-boolean v0, p0, Lw0/f;->j:Z

    if-eqz v0, :cond_e

    iget-object v0, p0, Lw0/f;->l:[I

    if-eqz p1, :cond_1

    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    float-to-int v4, v4

    aget v5, v0, v1

    if-lt v3, v5, :cond_2

    aget v5, v0, v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v6

    add-int/2addr v6, v5

    if-gt v3, v6, :cond_2

    aget v3, v0, v2

    if-lt v4, v3, :cond_2

    aget v0, v0, v2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    add-int/2addr v3, v0

    if-gt v4, v3, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    if-nez v1, :cond_3

    invoke-virtual {p0, p3}, Lw0/f;->x([Lv0/a;)V

    invoke-virtual {p0}, Lw0/f;->s()V

    goto/16 :goto_2

    :cond_3
    iget-object p3, p0, Lw0/f;->o:Lw0/f$d;

    if-eqz p3, :cond_e

    invoke-virtual {p0, p1, p2}, Lw0/f;->q(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result p1

    if-nez p1, :cond_e

    iget-object p1, p0, Lw0/f;->o:Lw0/f$d;

    invoke-virtual {p1, p0}, Lw0/f$d;->a(Lw0/f;)V

    goto/16 :goto_2

    :cond_4
    iget-boolean v0, p0, Lw0/f;->j:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lw0/f;->d:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_5

    iget v0, p0, Lw0/f;->s:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v1

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lw0/b;->a:Ljava/lang/Object;

    check-cast v0, Lw0/h;

    invoke-interface {v0}, Lw0/h;->f()Lu0/b;

    move-result-object v0

    instance-of v1, v0, Lmiuix/animation/ViewTarget;

    if-eqz v1, :cond_5

    invoke-virtual {p0, p1, p2}, Lw0/f;->q(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result p1

    if-eqz p1, :cond_5

    check-cast v0, Lmiuix/animation/ViewTarget;

    invoke-virtual {v0}, Lmiuix/animation/ViewTarget;->q()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->performClick()Z

    iget-boolean p2, p0, Lw0/f;->c:Z

    if-nez p2, :cond_5

    iget-boolean p2, p0, Lw0/f;->m:Z

    if-nez p2, :cond_5

    iput-boolean v2, p0, Lw0/f;->c:Z

    iget-object p2, p0, Lw0/f;->d:Landroid/view/View$OnClickListener;

    invoke-interface {p2, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_5
    :goto_0
    invoke-virtual {p0, p3}, Lw0/f;->r([Lv0/a;)V

    goto/16 :goto_2

    :cond_6
    iget-object p1, p0, Lw0/f;->d:Landroid/view/View$OnClickListener;

    if-nez p1, :cond_7

    iget-object p1, p0, Lw0/f;->n:Landroid/view/View$OnLongClickListener;

    if-eqz p1, :cond_a

    :cond_7
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result p1

    iput p1, p0, Lw0/f;->s:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result p1

    iput p1, p0, Lw0/f;->g:F

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result p1

    iput p1, p0, Lw0/f;->h:F

    iput-boolean v1, p0, Lw0/f;->c:Z

    iput-boolean v1, p0, Lw0/f;->m:Z

    iget-object p1, p0, Lw0/f;->n:Landroid/view/View$OnLongClickListener;

    if-nez p1, :cond_8

    goto :goto_1

    :cond_8
    iget-object p1, p0, Lw0/f;->o:Lw0/f$d;

    if-nez p1, :cond_9

    new-instance p1, Lw0/f$d;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Lw0/f$d;-><init>(Lw0/f$a;)V

    iput-object p1, p0, Lw0/f;->o:Lw0/f$d;

    :cond_9
    iget-object p1, p0, Lw0/f;->o:Lw0/f$d;

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p2, p0, Lw0/b;->a:Ljava/lang/Object;

    check-cast p2, Lw0/h;

    invoke-interface {p2}, Lw0/h;->f()Lu0/b;

    move-result-object p2

    instance-of v0, p2, Lmiuix/animation/ViewTarget;

    if-eqz v0, :cond_a

    check-cast p2, Lmiuix/animation/ViewTarget;

    invoke-virtual {p2}, Lmiuix/animation/ViewTarget;->q()Landroid/view/View;

    move-result-object p2

    if-eqz p2, :cond_a

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p1, Lw0/f$d;->a:Ljava/lang/ref/WeakReference;

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v0

    int-to-long v3, v0

    invoke-virtual {p2, p1, v3, v4}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_a
    :goto_1
    sget-boolean p1, Lc1/f;->a:Z

    if-eqz p1, :cond_b

    new-array p1, v1, [Ljava/lang/Object;

    const-string p2, "onEventDown, touchDown"

    invoke-static {p2, p1}, Lc1/f;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_b
    iput-boolean v2, p0, Lw0/f;->j:Z

    invoke-virtual {p0}, Lw0/f;->w()V

    new-array p1, v2, [Lv0/a;

    iget-object p2, p0, Lw0/f;->f:Lv0/a;

    aput-object p2, p1, v1

    invoke-static {p3, p1}, Lc1/a;->d([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lv0/a;

    iget-object p2, p0, Lw0/f;->i:Lw0/c;

    if-eqz p2, :cond_c

    invoke-virtual {p2, v1, p1}, Lw0/c;->o(I[Lv0/a;)Lu0/g;

    :cond_c
    iget-object p2, p0, Lw0/b;->a:Ljava/lang/Object;

    check-cast p2, Lw0/h;

    sget-object p3, Lu0/f$a;->b:Lu0/f$a;

    invoke-interface {p2, p3}, Lw0/h;->d(Ljava/lang/Object;)Lw0/a;

    move-result-object p2

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p0, Lw0/f;->q:Ljava/util/Map;

    invoke-interface {v1, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    invoke-virtual {v0, p3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_d

    iget-object p3, p0, Lw0/b;->a:Ljava/lang/Object;

    check-cast p3, Lw0/h;

    invoke-interface {p3}, Lw0/h;->f()Lu0/b;

    move-result-object p3

    sget-object v0, La1/h;->l:La1/h;

    invoke-virtual {p3, v0}, Lu0/b;->h(La1/b;)F

    move-result v0

    sget-object v1, La1/h;->d:La1/h;

    invoke-virtual {p3, v1}, Lu0/b;->h(La1/b;)F

    move-result p3

    invoke-static {v0, p3}, Ljava/lang/Math;->max(FF)F

    move-result p3

    iget v0, p0, Lw0/f;->p:F

    sub-float v0, p3, v0

    div-float/2addr v0, p3

    const p3, 0x3f666666    # 0.9f

    invoke-static {v0, p3}, Ljava/lang/Math;->max(FF)F

    move-result p3

    sget-object v0, La1/h;->h:La1/h;

    float-to-double v1, p3

    invoke-virtual {p2, v0, v1, v2}, Lw0/a;->h(Ljava/lang/Object;D)V

    sget-object p3, La1/h;->i:La1/h;

    invoke-virtual {p2, p3, v1, v2}, Lw0/a;->h(Ljava/lang/Object;D)V

    :cond_d
    iget-object p0, p0, Lw0/b;->a:Ljava/lang/Object;

    check-cast p0, Lw0/h;

    invoke-interface {p0, p2, p1}, Lw0/h;->k(Ljava/lang/Object;[Lv0/a;)Lw0/h;

    :cond_e
    :goto_2
    return-void
.end method


# virtual methods
.method public i()V
    .locals 3

    invoke-super {p0}, Lw0/b;->i()V

    iget-object v0, p0, Lw0/f;->i:Lw0/c;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lw0/c;->i()V

    :cond_0
    iget-object v0, p0, Lw0/f;->q:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lw0/f;->t:Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_1
    iput-object v1, p0, Lw0/f;->t:Ljava/lang/ref/WeakReference;

    :cond_2
    iget-object v0, p0, Lw0/f;->k:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_3

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_3
    if-eqz v0, :cond_4

    const v2, 0x7f0a00a8

    invoke-virtual {v0, v2, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    :cond_4
    iput-object v1, p0, Lw0/f;->k:Ljava/lang/ref/WeakReference;

    :cond_5
    invoke-virtual {p0}, Lw0/f;->s()V

    return-void
.end method

.method public varargs p(Landroid/view/View;[Lv0/a;)V
    .locals 9

    iget-object v0, p0, Lw0/b;->a:Ljava/lang/Object;

    check-cast v0, Lw0/h;

    invoke-interface {v0}, Lw0/h;->f()Lu0/b;

    move-result-object v0

    instance-of v1, v0, Lmiuix/animation/ViewTarget;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    check-cast v0, Lmiuix/animation/ViewTarget;

    invoke-virtual {v0}, Lmiuix/animation/ViewTarget;->q()Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v2

    :goto_0
    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lw0/f;->d:Landroid/view/View$OnClickListener;

    if-eqz v1, :cond_2

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    iput-object v2, p0, Lw0/f;->d:Landroid/view/View$OnClickListener;

    iget-object v1, p0, Lw0/f;->n:Landroid/view/View$OnLongClickListener;

    if-eqz v1, :cond_3

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :cond_3
    iput-object v2, p0, Lw0/f;->n:Landroid/view/View$OnLongClickListener;

    :goto_1
    sget-object v0, Lw0/f;->v:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lw0/f$c;

    if-nez v0, :cond_4

    new-instance v0, Lw0/f$c;

    invoke-direct {v0, v2}, Lw0/f$c;-><init>(Lw0/f$a;)V

    sget-object v1, Lw0/f;->v:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, v0, Lw0/f$c;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p0, p2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lw0/f;->t:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/view/View;

    :cond_5
    const/4 v0, 0x0

    const/4 v1, 0x1

    if-ne v2, p1, :cond_6

    move v2, v0

    goto :goto_2

    :cond_6
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, p0, Lw0/f;->t:Ljava/lang/ref/WeakReference;

    move v2, v1

    :goto_2
    if-eqz v2, :cond_8

    sget-boolean v2, Lc1/f;->a:Z

    if-eqz v2, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleViewTouch for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v0}, Lc1/f;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_7
    invoke-virtual {p1}, Landroid/view/View;->isClickable()Z

    move-result v8

    invoke-virtual {p1, v1}, Landroid/view/View;->setClickable(Z)V

    new-instance v0, Lw0/g;

    const/4 v5, 0x0

    move-object v3, v0

    move-object v4, p0

    move-object v6, p1

    move-object v7, p2

    invoke-direct/range {v3 .. v8}, Lw0/g;-><init>(Lw0/f;ZLandroid/view/View;[Lv0/a;Z)V

    sget-object p2, Lc1/a;->a:[Ljava/lang/Class;

    new-instance p2, Lc1/a$a;

    invoke-direct {p2, v0}, Lc1/a$a;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p2, Lc1/a$a;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0, p2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    :cond_8
    return-void
.end method

.method public final q(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result p2

    iget v1, p0, Lw0/f;->g:F

    iget v2, p0, Lw0/f;->h:F

    sget-object v3, Lc1/a;->a:[Ljava/lang/Class;

    sub-float/2addr v0, v1

    float-to-double v0, v0

    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    sub-float/2addr p2, v2

    float-to-double v5, p2

    invoke-static {v5, v6, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    add-double/2addr v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    sget p2, Lc1/a;->c:F

    const/4 v2, 0x0

    cmpl-float p2, p2, v2

    if-nez p2, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result p1

    int-to-float p1, p1

    sput p1, Lc1/a;->c:F

    :cond_0
    sget p1, Lc1/a;->c:F

    float-to-double p1, p1

    cmpg-double p1, v0, p1

    if-gez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final varargs r([Lv0/a;)V
    .locals 2

    iget-boolean v0, p0, Lw0/f;->j:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lc1/f;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onEventUp, touchUp"

    invoke-static {v1, v0}, Lc1/f;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p0, p1}, Lw0/f;->x([Lv0/a;)V

    invoke-virtual {p0}, Lw0/f;->s()V

    :cond_1
    return-void
.end method

.method public final s()V
    .locals 1

    iget-object v0, p0, Lw0/f;->o:Lw0/f$d;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lw0/f$d;->a(Lw0/f;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lw0/f;->j:Z

    iput v0, p0, Lw0/f;->s:I

    const/4 v0, 0x0

    iput v0, p0, Lw0/f;->g:F

    iput v0, p0, Lw0/f;->h:F

    return-void
.end method

.method public varargs t(F[Lu0/f$a;)Lu0/f;
    .locals 3

    iget-object v0, p0, Lw0/b;->a:Ljava/lang/Object;

    check-cast v0, Lw0/h;

    array-length v1, p2

    if-lez v1, :cond_0

    const/4 v1, 0x0

    aget-object p2, p2, v1

    goto :goto_0

    :cond_0
    sget-object p2, Lu0/f$a;->b:Lu0/f$a;

    :goto_0
    invoke-interface {v0, p2}, Lw0/h;->d(Ljava/lang/Object;)Lw0/a;

    move-result-object p2

    sget-object v0, La1/h;->b:La1/h;

    float-to-double v1, p1

    invoke-virtual {p2, v0, v1, v2}, Lw0/a;->h(Ljava/lang/Object;D)V

    return-object p0
.end method

.method public u(FFFF)Lu0/f;
    .locals 2

    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr p1, v0

    float-to-int p1, p1

    mul-float/2addr p2, v0

    float-to-int p2, p2

    mul-float/2addr p3, v0

    float-to-int p3, p3

    mul-float/2addr p4, v0

    float-to-int p4, p4

    invoke-static {p1, p2, p3, p4}, Landroid/graphics/Color;->argb(IIII)I

    move-result p1

    sget-object p2, La1/i;->a:La1/i$b;

    iget-object p3, p0, Lw0/b;->a:Ljava/lang/Object;

    check-cast p3, Lw0/h;

    sget-object p4, Lu0/f$a;->b:Lu0/f$a;

    invoke-interface {p3, p4}, Lw0/h;->d(Ljava/lang/Object;)Lw0/a;

    move-result-object p3

    int-to-double v0, p1

    invoke-virtual {p3, p2, v0, v1}, Lw0/a;->h(Ljava/lang/Object;D)V

    iget-object p1, p0, Lw0/b;->a:Ljava/lang/Object;

    check-cast p1, Lw0/h;

    sget-object p3, Lu0/f$a;->c:Lu0/f$a;

    invoke-interface {p1, p3}, Lw0/h;->d(Ljava/lang/Object;)Lw0/a;

    move-result-object p1

    iget-object p3, p0, Lw0/b;->a:Ljava/lang/Object;

    check-cast p3, Lw0/h;

    invoke-interface {p3}, Lw0/h;->f()Lu0/b;

    move-result-object p3

    const-wide/16 v0, 0x0

    invoke-static {p3, p2, v0, v1}, Landroidx/emoji2/text/l;->y(Lu0/b;La1/b;D)D

    move-result-wide p3

    double-to-int p3, p3

    int-to-double p3, p3

    invoke-virtual {p1, p2, p3, p4}, Lw0/a;->h(Ljava/lang/Object;D)V

    return-object p0
.end method

.method public varargs v(F[Lu0/f$a;)Lu0/f;
    .locals 3

    array-length v0, p2

    if-lez v0, :cond_0

    const/4 v0, 0x0

    aget-object p2, p2, v0

    goto :goto_0

    :cond_0
    sget-object p2, Lu0/f$a;->b:Lu0/f$a;

    :goto_0
    iget-object v0, p0, Lw0/f;->q:Ljava/util/Map;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lw0/b;->a:Ljava/lang/Object;

    check-cast v0, Lw0/h;

    invoke-interface {v0, p2}, Lw0/h;->d(Ljava/lang/Object;)Lw0/a;

    move-result-object p2

    sget-object v0, La1/h;->h:La1/h;

    float-to-double v1, p1

    invoke-virtual {p2, v0, v1, v2}, Lw0/a;->h(Ljava/lang/Object;D)V

    sget-object p1, La1/h;->i:La1/h;

    invoke-virtual {p2, p1, v1, v2}, Lw0/a;->h(Ljava/lang/Object;D)V

    return-object p0
.end method

.method public final w()V
    .locals 5

    iget-boolean v0, p0, Lw0/f;->r:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lw0/f;->b:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/16 v0, 0x14

    const/4 v1, 0x0

    invoke-static {v0, v1, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    iget-object v1, p0, Lw0/b;->a:Ljava/lang/Object;

    check-cast v1, Lw0/h;

    invoke-interface {v1}, Lw0/h;->f()Lu0/b;

    move-result-object v1

    invoke-virtual {v1}, Lu0/b;->g()Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Landroid/view/View;

    if-eqz v2, :cond_2

    check-cast v1, Landroid/view/View;

    const v0, 0x7f060215

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "uimode"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/UiModeManager;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/app/UiModeManager;->getNightMode()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    const v0, 0x7f060216

    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :cond_2
    sget-object v1, La1/i;->b:La1/i$c;

    iget-object v2, p0, Lw0/b;->a:Ljava/lang/Object;

    check-cast v2, Lw0/h;

    sget-object v3, Lu0/f$a;->b:Lu0/f$a;

    invoke-interface {v2, v3}, Lw0/h;->d(Ljava/lang/Object;)Lw0/a;

    move-result-object v2

    int-to-double v3, v0

    invoke-virtual {v2, v1, v3, v4}, Lw0/a;->h(Ljava/lang/Object;D)V

    iget-object v0, p0, Lw0/b;->a:Ljava/lang/Object;

    check-cast v0, Lw0/h;

    sget-object v2, Lu0/f$a;->c:Lu0/f$a;

    invoke-interface {v0, v2}, Lw0/h;->d(Ljava/lang/Object;)Lw0/a;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lw0/a;->h(Ljava/lang/Object;D)V

    :cond_3
    :goto_0
    return-void
.end method

.method public varargs x([Lv0/a;)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lv0/a;

    iget-object v1, p0, Lw0/f;->u:Lv0/a;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Lc1/a;->d([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lv0/a;

    iget-object v0, p0, Lw0/f;->i:Lw0/c;

    if-eqz v0, :cond_0

    invoke-virtual {v0, v2, p1}, Lw0/c;->o(I[Lv0/a;)Lu0/g;

    :cond_0
    iget-object v0, p0, Lw0/b;->a:Ljava/lang/Object;

    move-object v1, v0

    check-cast v1, Lw0/h;

    check-cast v0, Lw0/h;

    sget-object v2, Lu0/f$a;->c:Lu0/f$a;

    invoke-interface {v0, v2}, Lw0/h;->d(Ljava/lang/Object;)Lw0/a;

    move-result-object v0

    invoke-interface {v1, v0, p1}, Lw0/h;->k(Ljava/lang/Object;[Lv0/a;)Lw0/h;

    return-void
.end method
