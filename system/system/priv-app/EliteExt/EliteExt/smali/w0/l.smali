.class public Lw0/l;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/Object;

.field public final b:Lw0/a;

.field public c:Lw0/k;

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Lw0/a;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lw0/a;


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lw0/l;->d:Ljava/util/Map;

    new-instance v0, Lw0/a;

    const-string v1, "defaultTo"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lw0/a;-><init>(Ljava/lang/Object;Z)V

    iput-object v0, p0, Lw0/l;->e:Lw0/a;

    new-instance v0, Lw0/a;

    const-string v1, "defaultSetTo"

    invoke-direct {v0, v1, v2}, Lw0/a;-><init>(Ljava/lang/Object;Z)V

    iput-object v0, p0, Lw0/l;->b:Lw0/a;

    new-instance v0, Lw0/a;

    const-string v1, "autoSetTo"

    invoke-direct {v0, v1, v2}, Lw0/a;-><init>(Ljava/lang/Object;Z)V

    new-instance v0, Lw0/k;

    invoke-direct {v0}, Lw0/k;-><init>()V

    iput-object v0, p0, Lw0/l;->c:Lw0/k;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Lw0/a;
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lw0/l;->b(Ljava/lang/Object;Z)Lw0/a;

    move-result-object p1

    return-object p1
.end method

.method public final b(Ljava/lang/Object;Z)Lw0/a;
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    instance-of v0, p1, Lw0/a;

    if-eqz v0, :cond_1

    check-cast p1, Lw0/a;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lw0/l;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lw0/a;

    if-nez v0, :cond_2

    if-eqz p2, :cond_2

    new-instance p2, Lw0/a;

    const/4 v0, 0x0

    invoke-direct {p2, p1, v0}, Lw0/a;-><init>(Ljava/lang/Object;Z)V

    iget-object p1, p0, Lw0/l;->d:Ljava/util/Map;

    iget-object v0, p2, Lw0/a;->d:Ljava/lang/Object;

    invoke-interface {p1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object p1, p2

    goto :goto_0

    :cond_2
    move-object p1, v0

    :goto_0
    return-object p1
.end method

.method public final varargs c(Ljava/lang/Object;[Ljava/lang/Object;)Lw0/a;
    .locals 4

    array-length v0, p2

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-lez v0, :cond_2

    const/4 v0, 0x0

    aget-object v3, p2, v0

    invoke-virtual {p0, v3, v0}, Lw0/l;->b(Ljava/lang/Object;Z)Lw0/a;

    move-result-object v3

    if-nez v3, :cond_1

    aget-object v0, p2, v0

    array-length v3, p2

    if-le v3, v1, :cond_0

    aget-object p2, p2, v1

    goto :goto_0

    :cond_0
    move-object p2, v2

    :goto_0
    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_2

    instance-of p2, p2, Ljava/lang/String;

    if-eqz p2, :cond_2

    invoke-virtual {p0, v0, v1}, Lw0/l;->b(Ljava/lang/Object;Z)Lw0/a;

    move-result-object v2

    goto :goto_1

    :cond_1
    move-object v2, v3

    :cond_2
    :goto_1
    if-nez v2, :cond_3

    invoke-virtual {p0, p1, v1}, Lw0/l;->b(Ljava/lang/Object;Z)Lw0/a;

    move-result-object v2

    :cond_3
    return-object v2
.end method

.method public final varargs d(Lu0/b;Lw0/a;Lv0/b;[Ljava/lang/Object;)V
    .locals 17

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p0

    move-object/from16 v4, p4

    iget-object v5, v3, Lw0/l;->c:Lw0/k;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    array-length v6, v4

    if-nez v6, :cond_0

    goto/16 :goto_16

    :cond_0
    const/4 v6, 0x0

    aget-object v7, v4, v6

    iget-object v8, v1, Lw0/a;->d:Ljava/lang/Object;

    invoke-virtual {v7, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    :goto_0
    array-length v8, v4

    if-ge v7, v8, :cond_1e

    aget-object v8, v4, v7

    add-int/lit8 v9, v7, 0x1

    array-length v10, v4

    if-ge v9, v10, :cond_1

    aget-object v10, v4, v9

    goto :goto_1

    :cond_1
    const/4 v10, 0x0

    :goto_1
    instance-of v12, v8, Ljava/lang/String;

    if-eqz v12, :cond_2

    instance-of v13, v10, Ljava/lang/String;

    if-eqz v13, :cond_2

    move v7, v9

    goto :goto_0

    :cond_2
    instance-of v13, v8, Ly0/b;

    const/4 v14, 0x1

    if-nez v13, :cond_7

    instance-of v15, v8, Lc1/c$a;

    if-eqz v15, :cond_3

    goto :goto_5

    :cond_3
    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Class;->isArray()Z

    move-result v13

    if-eqz v13, :cond_6

    invoke-static {v8}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v13

    move v15, v6

    move/from16 v16, v15

    :goto_2
    if-ge v15, v13, :cond_b

    invoke-static {v8, v15}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v5, v2, v11}, Lw0/k;->a(Lv0/b;Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_5

    if-eqz v16, :cond_4

    goto :goto_3

    :cond_4
    move/from16 v16, v6

    goto :goto_4

    :cond_5
    :goto_3
    move/from16 v16, v14

    :goto_4
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    :cond_6
    invoke-virtual {v5, v2, v8}, Lw0/k;->a(Lv0/b;Ljava/lang/Object;)Z

    move-result v16

    goto :goto_7

    :cond_7
    :goto_5
    iget-object v11, v2, Lv0/b;->a:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_8

    iget-object v11, v2, Lv0/b;->a:Ljava/util/List;

    iget-object v15, v2, Lv0/b;->c:Lv0/a;

    invoke-interface {v11, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_8
    iget-object v11, v2, Lv0/b;->a:Ljava/util/List;

    invoke-interface {v11, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lv0/a;

    if-eqz v13, :cond_9

    new-array v13, v14, [Ly0/b;

    move-object v15, v8

    check-cast v15, Ly0/b;

    aput-object v15, v13, v6

    iget-object v11, v11, Lv0/a;->e:Ljava/util/HashSet;

    invoke-static {v11, v13}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    goto :goto_6

    :cond_9
    instance-of v13, v8, Lc1/c$a;

    if-eqz v13, :cond_a

    move-object v13, v8

    check-cast v13, Lc1/c$a;

    iput-object v13, v11, Lv0/a;->b:Lc1/c$a;

    :cond_a
    :goto_6
    move/from16 v16, v14

    :cond_b
    :goto_7
    if-nez v16, :cond_1b

    instance-of v11, v8, La1/b;

    if-eqz v11, :cond_c

    check-cast v8, La1/b;

    goto :goto_9

    :cond_c
    if-eqz v12, :cond_e

    instance-of v11, v0, Lu0/h;

    if-eqz v11, :cond_e

    if-eqz v10, :cond_d

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    goto :goto_8

    :cond_d
    const/4 v10, 0x0

    :goto_8
    move-object v11, v0

    check-cast v11, Lu0/h;

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v11, v8, v10}, Lu0/h;->p(Ljava/lang/String;Ljava/lang/Class;)La1/b;

    move-result-object v8

    goto :goto_9

    :cond_e
    instance-of v8, v8, Ljava/lang/Float;

    if-eqz v8, :cond_f

    sget-object v8, Lw0/k;->b:La1/f;

    goto :goto_9

    :cond_f
    const/4 v8, 0x0

    :goto_9
    if-eqz v8, :cond_1b

    sget-object v10, Lw0/k;->b:La1/f;

    if-eq v8, v10, :cond_11

    sget-object v10, Lw0/k;->a:La1/e;

    if-ne v8, v10, :cond_10

    goto :goto_a

    :cond_10
    move v10, v6

    goto :goto_b

    :cond_11
    :goto_a
    move v10, v14

    :goto_b
    if-eqz v10, :cond_12

    goto :goto_c

    :cond_12
    move v7, v9

    :goto_c
    array-length v9, v4

    if-ge v7, v9, :cond_13

    aget-object v11, v4, v7

    goto :goto_d

    :cond_13
    const/4 v11, 0x0

    :goto_d
    if-eqz v11, :cond_1b

    instance-of v9, v11, Ljava/lang/Integer;

    if-nez v9, :cond_15

    instance-of v10, v11, Ljava/lang/Float;

    if-nez v10, :cond_15

    instance-of v10, v11, Ljava/lang/Double;

    if-eqz v10, :cond_14

    goto :goto_e

    :cond_14
    move v9, v6

    goto :goto_12

    :cond_15
    :goto_e
    instance-of v10, v8, La1/c;

    if-eqz v10, :cond_17

    if-eqz v9, :cond_16

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v9

    goto :goto_f

    :cond_16
    check-cast v11, Ljava/lang/Float;

    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v9

    float-to-int v9, v9

    :goto_f
    int-to-double v9, v9

    goto :goto_11

    :cond_17
    if-eqz v9, :cond_18

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v9

    int-to-float v9, v9

    goto :goto_10

    :cond_18
    check-cast v11, Ljava/lang/Float;

    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v9

    :goto_10
    float-to-double v9, v9

    :goto_11
    invoke-virtual {v1, v8, v9, v10}, Lw0/a;->h(Ljava/lang/Object;D)V

    move v9, v14

    :goto_12
    if-eqz v9, :cond_1b

    add-int/lit8 v9, v7, 0x1

    array-length v10, v4

    if-lt v9, v10, :cond_19

    goto :goto_13

    :cond_19
    aget-object v9, v4, v9

    instance-of v10, v9, Ljava/lang/Float;

    if-eqz v10, :cond_1a

    check-cast v9, Ljava/lang/Float;

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v9

    float-to-double v9, v9

    invoke-virtual {v0, v8, v9, v10}, Lu0/b;->o(La1/b;D)V

    move v8, v14

    goto :goto_14

    :cond_1a
    :goto_13
    move v8, v6

    :goto_14
    if-eqz v8, :cond_1c

    const/4 v14, 0x2

    goto :goto_15

    :cond_1b
    move v14, v6

    :cond_1c
    :goto_15
    if-lez v14, :cond_1d

    add-int/2addr v7, v14

    goto/16 :goto_0

    :cond_1d
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    :cond_1e
    :goto_16
    return-void
.end method

.method public e(Ljava/lang/Object;)Lw0/a;
    .locals 2

    instance-of v0, p1, Lw0/a;

    if-eqz v0, :cond_0

    check-cast p1, Lw0/a;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lw0/l;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lw0/a;

    if-nez v0, :cond_1

    new-instance v0, Lw0/a;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lw0/a;-><init>(Ljava/lang/Object;Z)V

    iget-object p1, p0, Lw0/l;->d:Ljava/util/Map;

    iget-object v1, v0, Lw0/a;->d:Ljava/lang/Object;

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    move-object p1, v0

    :goto_0
    iput-object p1, p0, Lw0/l;->a:Ljava/lang/Object;

    return-object p1
.end method
