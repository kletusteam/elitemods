.class public Lw0/d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lw0/e;

.field public final synthetic b:Lv0/b;

.field public final synthetic c:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lw0/e;Ljava/lang/Object;Lv0/b;)V
    .locals 0

    iput-object p1, p0, Lw0/d;->a:Lw0/e;

    iput-object p2, p0, Lw0/d;->c:Ljava/lang/Object;

    iput-object p3, p0, Lw0/d;->b:Lv0/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 30

    move-object/from16 v0, p0

    iget-object v1, v0, Lw0/d;->a:Lw0/e;

    iget-object v2, v0, Lw0/d;->c:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lw0/e;->d(Ljava/lang/Object;)Lw0/a;

    move-result-object v1

    iget-object v2, v0, Lw0/d;->a:Lw0/e;

    iget-object v2, v2, Lw0/e;->d:Lu0/b;

    sget-boolean v3, Lc1/f;->a:Z

    const/4 v4, 0x0

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FolmeState.setTo, state = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v5, v4, [Ljava/lang/Object;

    invoke-static {v3, v5}, Lc1/f;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iget-object v2, v2, Lu0/b;->a:Lx0/c;

    iget-object v3, v0, Lw0/d;->b:Lv0/b;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    sget-boolean v5, Lc1/f;->a:Z

    const/4 v6, 0x1

    if-eqz v5, :cond_1

    const-string v5, "setTo, target = "

    invoke-static {v5}, Landroidx/activity/result/a;->g(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, v2, Lx0/c;->d:Lu0/b;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "to = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    new-array v8, v6, [Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v8, v4

    invoke-static {v5, v8}, Lc1/f;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    invoke-virtual {v1}, Lw0/a;->f()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    const/16 v7, 0x96

    const/4 v8, 0x0

    if-le v5, v7, :cond_3

    sget-object v3, Lx0/e;->h:Lx0/k;

    iget-object v2, v2, Lx0/c;->d:Lu0/b;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v5, Lx0/k$b;

    invoke-direct {v5, v8}, Lx0/k$b;-><init>(Lx0/k$a;)V

    iput-object v2, v5, Lx0/k$b;->b:Lu0/b;

    iget-boolean v2, v1, Lw0/a;->a:Z

    if-eqz v2, :cond_2

    new-instance v2, Lw0/a;

    invoke-direct {v2, v8, v4}, Lw0/a;-><init>(Ljava/lang/Object;Z)V

    iput-object v2, v5, Lx0/k$b;->a:Lw0/a;

    invoke-virtual {v2, v1}, Lw0/a;->g(Lw0/a;)V

    goto :goto_0

    :cond_2
    iput-object v1, v5, Lx0/k$b;->a:Lw0/a;

    :goto_0
    const/4 v2, 0x4

    invoke-virtual {v3, v2, v5}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_b

    :cond_3
    invoke-virtual {v1}, Lw0/a;->f()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_18

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v1, v7}, Lw0/a;->e(Ljava/lang/Object;)La1/b;

    move-result-object v7

    iget-object v9, v2, Lx0/c;->d:Lu0/b;

    invoke-virtual {v1, v9, v7}, Lw0/a;->b(Lu0/b;La1/b;)D

    move-result-wide v9

    iget-object v11, v2, Lx0/c;->d:Lu0/b;

    iget-object v11, v11, Lu0/b;->a:Lx0/c;

    iget-object v11, v11, Lx0/c;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v11, v7}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ly0/c;

    if-eqz v11, :cond_4

    iget-object v11, v11, Ly0/c;->a:Lx0/b;

    iput-wide v9, v11, Lx0/b;->f:D

    :cond_4
    instance-of v11, v7, La1/c;

    if-eqz v11, :cond_5

    iget-object v11, v2, Lx0/c;->d:Lu0/b;

    move-object v12, v7

    check-cast v12, La1/c;

    double-to-int v13, v9

    invoke-virtual {v11, v12, v13}, Lu0/b;->l(La1/c;I)V

    goto :goto_2

    :cond_5
    iget-object v11, v2, Lx0/c;->d:Lu0/b;

    double-to-float v12, v9

    invoke-virtual {v11, v7, v12}, Lu0/b;->n(La1/b;F)V

    :goto_2
    iget-object v11, v2, Lx0/c;->d:Lu0/b;

    iget-object v12, v11, Lu0/b;->h:Lx0/m;

    iget-object v13, v12, Lx0/m;->a:Ljava/util/Map;

    invoke-interface {v13, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lx0/m$b;

    if-nez v13, :cond_6

    new-instance v13, Lx0/m$b;

    invoke-direct {v13, v8}, Lx0/m$b;-><init>(Lx0/m$a;)V

    iget-object v12, v12, Lx0/m;->a:Ljava/util/Map;

    invoke-interface {v12, v7, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    iget-object v12, v13, Lx0/m$b;->a:Lc1/j;

    new-array v6, v6, [D

    aput-wide v9, v6, v4

    invoke-static {v12}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v9, Lc1/j$b;

    invoke-direct {v9, v8}, Lc1/j$b;-><init>(Lc1/j$a;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v14

    iput-wide v14, v9, Lc1/j$b;->a:J

    iput-object v6, v9, Lc1/j$b;->b:[D

    iget-object v6, v12, Lc1/j;->a:Ljava/util/LinkedList;

    invoke-virtual {v6, v9}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object v6, v12, Lc1/j;->a:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    move-result v6

    const/16 v8, 0xa

    if-le v6, v8, :cond_7

    iget-object v6, v12, Lc1/j;->a:Ljava/util/LinkedList;

    invoke-virtual {v6, v4}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    :cond_7
    iget-object v6, v12, Lc1/j;->a:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    move-result v6

    const/4 v8, 0x2

    if-lt v6, v8, :cond_11

    iget-object v8, v12, Lc1/j;->a:Ljava/util/LinkedList;

    invoke-virtual {v8}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lc1/j$b;

    iget-object v9, v12, Lc1/j;->a:Ljava/util/LinkedList;

    add-int/lit8 v6, v6, -0x2

    invoke-virtual {v9, v6}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lc1/j$b;

    iget-object v9, v12, Lc1/j;->d:[F

    if-eqz v9, :cond_8

    array-length v9, v9

    iget-object v10, v8, Lc1/j$b;->b:[D

    array-length v10, v10

    if-ge v9, v10, :cond_9

    :cond_8
    iget-object v9, v8, Lc1/j$b;->b:[D

    array-length v9, v9

    new-array v9, v9, [F

    iput-object v9, v12, Lc1/j;->d:[F

    :cond_9
    :goto_3
    iget-object v9, v8, Lc1/j$b;->b:[D

    array-length v10, v9

    if-ge v4, v10, :cond_10

    iget-object v10, v12, Lc1/j;->d:[F

    aget-wide v21, v9, v4

    iget-wide v14, v8, Lc1/j$b;->a:J

    iget-object v9, v6, Lc1/j$b;->b:[D

    aget-wide v17, v9, v4

    move-object/from16 v23, v8

    iget-wide v8, v6, Lc1/j$b;->a:J

    sub-long v19, v14, v8

    move-wide v8, v14

    move-object v14, v12

    move-wide/from16 v15, v21

    invoke-virtual/range {v14 .. v20}, Lc1/j;->a(DDJ)F

    move-result v14

    float-to-double v14, v14

    move-object/from16 v24, v5

    iget-object v5, v12, Lc1/j;->a:Ljava/util/LinkedList;

    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x2

    const/16 v16, 0x0

    :goto_4
    const v25, 0x7f7fffff    # Float.MAX_VALUE

    if-ltz v5, :cond_c

    move-object/from16 v26, v6

    iget-object v6, v12, Lc1/j;->a:Ljava/util/LinkedList;

    invoke-virtual {v6, v5}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lc1/j$b;

    move-wide/from16 v17, v14

    iget-wide v14, v6, Lc1/j$b;->a:J

    sub-long v19, v8, v14

    iget-object v14, v12, Lc1/j;->c:Ljava/lang/Long;

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    cmp-long v14, v19, v14

    if-lez v14, :cond_b

    iget-object v14, v12, Lc1/j;->b:Ljava/lang/Long;

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    cmp-long v14, v19, v14

    if-gez v14, :cond_b

    iget-object v5, v6, Lc1/j$b;->b:[D

    aget-wide v27, v5, v4

    move-object/from16 v29, v1

    move-wide/from16 v0, v17

    move-object v14, v12

    move-wide/from16 v15, v21

    move-wide/from16 v17, v27

    invoke-virtual/range {v14 .. v20}, Lc1/j;->a(DDJ)F

    move-result v5

    float-to-double v14, v5

    mul-double v16, v0, v14

    const-wide/16 v18, 0x0

    cmpl-double v16, v16, v18

    if-lez v16, :cond_d

    const/16 v16, 0x0

    cmpl-float v5, v5, v16

    if-lez v5, :cond_a

    invoke-static {v0, v1, v14, v15}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    goto :goto_5

    :cond_a
    invoke-static {v0, v1, v14, v15}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    :goto_5
    double-to-float v5, v0

    goto :goto_6

    :cond_b
    move-object/from16 v29, v1

    move-wide/from16 v0, v17

    add-int/lit8 v5, v5, -0x1

    move-wide v14, v0

    move-object/from16 v16, v6

    move-object/from16 v6, v26

    move-object/from16 v1, v29

    move-object/from16 v0, p0

    goto :goto_4

    :cond_c
    move-object/from16 v29, v1

    move-object/from16 v26, v6

    move-object/from16 v6, v16

    move/from16 v5, v25

    :cond_d
    :goto_6
    cmpl-float v0, v5, v25

    if-nez v0, :cond_e

    if-eqz v6, :cond_e

    iget-wide v0, v6, Lc1/j$b;->a:J

    sub-long v19, v8, v0

    iget-object v0, v12, Lc1/j;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v19, v0

    if-lez v0, :cond_e

    iget-object v0, v12, Lc1/j;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v19, v0

    if-gez v0, :cond_e

    iget-object v0, v6, Lc1/j$b;->b:[D

    aget-wide v17, v0, v4

    move-object v14, v12

    move-wide/from16 v15, v21

    invoke-virtual/range {v14 .. v20}, Lc1/j;->a(DDJ)F

    move-result v5

    :cond_e
    cmpl-float v0, v5, v25

    if-nez v0, :cond_f

    const/4 v5, 0x0

    :cond_f
    aput v5, v10, v4

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v8, v23

    move-object/from16 v5, v24

    move-object/from16 v6, v26

    move-object/from16 v1, v29

    goto/16 :goto_3

    :cond_10
    move-object/from16 v29, v1

    move-object/from16 v24, v5

    goto :goto_7

    :cond_11
    move-object/from16 v29, v1

    move-object/from16 v24, v5

    iget-object v0, v12, Lc1/j;->d:[F

    if-eqz v0, :cond_12

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    :cond_12
    :goto_7
    iget-object v0, v13, Lx0/m$b;->a:Lc1/j;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-object v1, v0, Lc1/j;->a:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-lez v1, :cond_13

    iget-object v1, v0, Lc1/j;->a:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lc1/j$b;

    iget-wide v8, v1, Lc1/j$b;->a:J

    sub-long/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    const-wide/16 v8, 0x32

    cmp-long v1, v4, v8

    if-lez v1, :cond_13

    goto :goto_8

    :cond_13
    iget-object v0, v0, Lc1/j;->d:[F

    if-eqz v0, :cond_14

    array-length v1, v0

    if-lez v1, :cond_14

    const/4 v1, 0x0

    aget v0, v0, v1

    move v4, v1

    goto :goto_9

    :cond_14
    :goto_8
    const/4 v0, 0x0

    const/4 v1, 0x0

    move v4, v0

    move v0, v1

    :goto_9
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_17

    iget-object v1, v13, Lx0/m$b;->b:Lx0/m$c;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v5, v11, Lu0/b;->b:Lx0/l;

    invoke-virtual {v5, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v5, v1, Lx0/m$c;->c:Ljava/lang/ref/WeakReference;

    if-eqz v5, :cond_15

    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    if-eq v5, v11, :cond_16

    :cond_15
    new-instance v5, Ljava/lang/ref/WeakReference;

    invoke-direct {v5, v11}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v5, v1, Lx0/m$c;->c:Ljava/lang/ref/WeakReference;

    :cond_16
    iput-object v7, v1, Lx0/m$c;->b:La1/b;

    iget-object v5, v11, Lu0/b;->b:Lx0/l;

    const-wide/16 v8, 0x258

    invoke-virtual {v5, v1, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    float-to-double v0, v0

    invoke-virtual {v11, v7, v0, v1}, Lu0/b;->o(La1/b;D)V

    :cond_17
    const/4 v6, 0x1

    const/4 v8, 0x0

    move-object/from16 v0, p0

    move-object/from16 v5, v24

    move-object/from16 v1, v29

    goto/16 :goto_1

    :cond_18
    move-object/from16 v29, v1

    iget-object v0, v2, Lx0/c;->d:Lu0/b;

    iget-object v0, v0, Lu0/b;->i:Lx0/j;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez v3, :cond_19

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    goto/16 :goto_b

    :cond_19
    move-object/from16 v1, v29

    iget-object v2, v1, Lw0/a;->d:Ljava/lang/Object;

    iget-object v4, v0, Lx0/j;->a:Ljava/lang/Object;

    check-cast v4, Lv0/a;

    iget-object v5, v1, Lw0/a;->b:Lv0/a;

    invoke-virtual {v4, v5}, Lv0/a;->b(Lv0/a;)V

    iget-object v4, v0, Lx0/j;->a:Ljava/lang/Object;

    check-cast v4, Lv0/a;

    invoke-virtual {v3, v4}, Lv0/b;->b(Lv0/a;)V

    iget-object v3, v0, Lx0/j;->c:Ljava/lang/Object;

    check-cast v3, Ly0/a;

    iget-object v4, v0, Lx0/j;->a:Ljava/lang/Object;

    check-cast v4, Lv0/a;

    invoke-virtual {v3, v2, v4}, Ly0/a;->a(Ljava/lang/Object;Lv0/a;)Z

    move-result v3

    if-nez v3, :cond_1a

    goto :goto_a

    :cond_1a
    iget-object v3, v0, Lx0/j;->c:Ljava/lang/Object;

    move-object v4, v3

    check-cast v4, Ly0/a;

    sget-object v7, Ly0/a;->b:Ly0/a$a;

    const/4 v8, 0x0

    const/4 v3, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v9, 0x0

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v4 .. v9}, Ly0/a;->b(Ljava/lang/Object;Ljava/lang/Object;Ly0/a$d;Ljava/util/Collection;Ly0/c;)V

    iget-object v4, v0, Lx0/j;->d:Ljava/lang/Object;

    check-cast v4, Lu0/b;

    iget-object v4, v4, Lu0/b;->a:Lx0/c;

    iget-object v4, v4, Lx0/c;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v12

    iget-object v4, v0, Lx0/j;->c:Ljava/lang/Object;

    check-cast v4, Ly0/a;

    sget-object v7, Ly0/a;->f:Ly0/a$f;

    move-object v8, v12

    move-object v9, v11

    invoke-virtual/range {v4 .. v9}, Ly0/a;->b(Ljava/lang/Object;Ljava/lang/Object;Ly0/a$d;Ljava/util/Collection;Ly0/c;)V

    iget-object v4, v0, Lx0/j;->c:Ljava/lang/Object;

    check-cast v4, Ly0/a;

    sget-object v7, Ly0/a;->h:Ly0/a$h;

    move-object v9, v10

    invoke-virtual/range {v4 .. v9}, Ly0/a;->b(Ljava/lang/Object;Ljava/lang/Object;Ly0/a$d;Ljava/util/Collection;Ly0/c;)V

    iget-object v4, v0, Lx0/j;->c:Ljava/lang/Object;

    check-cast v4, Ly0/a;

    invoke-virtual {v4, v2, v2, v12}, Ly0/a;->c(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V

    iget-object v4, v0, Lx0/j;->c:Ljava/lang/Object;

    check-cast v4, Ly0/a;

    sget-object v7, Ly0/a;->d:Ly0/a$c;

    const/4 v8, 0x0

    move-object v9, v3

    invoke-virtual/range {v4 .. v9}, Ly0/a;->b(Ljava/lang/Object;Ljava/lang/Object;Ly0/a$d;Ljava/util/Collection;Ly0/c;)V

    iget-object v3, v0, Lx0/j;->c:Ljava/lang/Object;

    check-cast v3, Ly0/a;

    invoke-virtual {v3, v2}, Ly0/a;->d(Ljava/lang/Object;)V

    :goto_a
    iget-object v0, v0, Lx0/j;->a:Ljava/lang/Object;

    check-cast v0, Lv0/a;

    invoke-virtual {v0}, Lv0/a;->a()V

    move-object/from16 v0, p0

    :goto_b
    iget-object v2, v0, Lw0/d;->a:Lw0/e;

    iget-object v2, v2, Lw0/e;->c:Lw0/l;

    iget-object v3, v2, Lw0/l;->e:Lw0/a;

    if-eq v1, v3, :cond_1b

    iget-object v2, v2, Lw0/l;->b:Lw0/a;

    if-ne v1, v2, :cond_1c

    :cond_1b
    iget-object v2, v1, Lw0/a;->b:Lv0/a;

    invoke-virtual {v2}, Lv0/a;->a()V

    iget-object v1, v1, Lw0/a;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    :cond_1c
    return-void
.end method
