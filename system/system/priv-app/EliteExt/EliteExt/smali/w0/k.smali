.class public Lw0/k;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:La1/e;

.field public static final b:La1/f;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, La1/f;

    const-string v1, "defaultProperty"

    invoke-direct {v0, v1}, La1/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lw0/k;->b:La1/f;

    new-instance v0, La1/e;

    const-string v1, "defaultIntProperty"

    invoke-direct {v0, v1}, La1/e;-><init>(Ljava/lang/String;)V

    sput-object v0, Lw0/k;->a:La1/e;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lv0/b;Ljava/lang/Object;)Z
    .locals 3

    instance-of v0, p2, Lv0/a;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    check-cast p2, Lv0/a;

    new-array v0, v1, [Z

    invoke-virtual {p1, p2, v0}, Lv0/b;->a(Lv0/a;[Z)V

    const/4 p1, 0x1

    return p1

    :cond_0
    instance-of v0, p2, Lv0/b;

    if-eqz v0, :cond_2

    check-cast p2, Lv0/b;

    new-array v0, v1, [Z

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p2, :cond_1

    goto :goto_1

    :cond_1
    iget-object p2, p2, Lv0/b;->a:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lv0/a;

    invoke-virtual {p1, v2, v0}, Lv0/b;->a(Lv0/a;[Z)V

    goto :goto_0

    :cond_2
    :goto_1
    return v1
.end method
