.class public Lz1/b;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final n:Lw0/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw0/b;"
        }
    .end annotation
.end field

.field public static o:Lm1/j$g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lm1/j$g<",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public static p:Lm1/j$g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lm1/j$g<",
            "Ljava/lang/StringBuilder;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:[Ljava/lang/String;

.field public b:[Ljava/lang/String;

.field public c:[Ljava/lang/String;

.field public d:[Ljava/lang/String;

.field public e:[Ljava/lang/String;

.field public f:[Ljava/lang/String;

.field public g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:[Ljava/lang/String;

.field public i:[Ljava/lang/String;

.field public j:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public l:[Ljava/lang/String;

.field public m:[Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lz1/b$a;

    invoke-direct {v0}, Lz1/b$a;-><init>()V

    sput-object v0, Lz1/b;->n:Lw0/b;

    new-instance v0, Lz1/b$b;

    invoke-direct {v0}, Lz1/b$b;-><init>()V

    sget-object v1, Lm1/j;->a:Ljava/util/HashMap;

    new-instance v1, Lm1/j$g;

    const/4 v2, 0x4

    invoke-direct {v1, v0, v2}, Lm1/j$g;-><init>(Lm1/j$e;I)V

    sput-object v1, Lz1/b;->p:Lm1/j$g;

    new-instance v0, Lz1/b$c;

    invoke-direct {v0}, Lz1/b$c;-><init>()V

    new-instance v1, Lm1/j$g;

    invoke-direct {v1, v0, v2}, Lm1/j$g;-><init>(Lm1/j$e;I)V

    sput-object v1, Lz1/b;->o:Lm1/j$g;

    return-void
.end method

.method public constructor <init>()V
    .locals 32

    move-object/from16 v0, p0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const/16 v1, 0xc

    new-array v2, v1, [Ljava/lang/String;

    iput-object v2, v0, Lz1/b;->i:[Ljava/lang/String;

    new-array v2, v1, [Ljava/lang/String;

    iput-object v2, v0, Lz1/b;->h:[Ljava/lang/String;

    const/16 v2, 0xf

    new-array v3, v2, [Ljava/lang/String;

    iput-object v3, v0, Lz1/b;->f:[Ljava/lang/String;

    new-array v3, v2, [Ljava/lang/String;

    iput-object v3, v0, Lz1/b;->e:[Ljava/lang/String;

    const/16 v3, 0x23

    new-array v4, v3, [Ljava/lang/String;

    iput-object v4, v0, Lz1/b;->d:[Ljava/lang/String;

    new-array v4, v3, [Ljava/lang/String;

    iput-object v4, v0, Lz1/b;->c:[Ljava/lang/String;

    const/16 v4, 0x8

    new-array v5, v4, [Ljava/lang/String;

    iput-object v5, v0, Lz1/b;->b:[Ljava/lang/String;

    new-array v5, v4, [Ljava/lang/String;

    iput-object v5, v0, Lz1/b;->a:[Ljava/lang/String;

    const/4 v5, 0x3

    new-array v6, v5, [Ljava/lang/String;

    iput-object v6, v0, Lz1/b;->m:[Ljava/lang/String;

    new-array v6, v5, [Ljava/lang/String;

    iput-object v6, v0, Lz1/b;->l:[Ljava/lang/String;

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, v0, Lz1/b;->j:Ljava/util/HashMap;

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, v0, Lz1/b;->g:Ljava/util/HashMap;

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, v0, Lz1/b;->k:Ljava/util/HashMap;

    iget-object v6, v0, Lz1/b;->i:[Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "\u0905"

    aput-object v8, v6, v7

    const/4 v8, 0x1

    const-string v9, "\u0906"

    aput-object v9, v6, v8

    const/4 v9, 0x2

    const-string v10, "\u0907"

    aput-object v10, v6, v9

    const-string v10, "\u0908"

    aput-object v10, v6, v5

    const/4 v10, 0x4

    const-string v11, "\u0909"

    aput-object v11, v6, v10

    const/4 v11, 0x5

    const-string v12, "\u090a"

    aput-object v12, v6, v11

    const/4 v12, 0x6

    const-string v13, "\u090b"

    aput-object v13, v6, v12

    const/4 v13, 0x7

    const-string v14, "\u090f"

    aput-object v14, v6, v13

    const-string v14, "\u0910"

    aput-object v14, v6, v4

    const/16 v14, 0x9

    const-string v15, "\u0911"

    aput-object v15, v6, v14

    const/16 v15, 0xa

    const-string v16, "\u0913"

    aput-object v16, v6, v15

    const/16 v16, 0xb

    const-string v17, "\u0914"

    aput-object v17, v6, v16

    iget-object v6, v0, Lz1/b;->h:[Ljava/lang/String;

    const-string v3, "a"

    aput-object v3, v6, v7

    const-string v18, "aa"

    aput-object v18, v6, v8

    const-string v19, "i"

    aput-object v19, v6, v9

    const-string v20, "ee"

    aput-object v20, v6, v5

    const-string v21, "u"

    aput-object v21, v6, v10

    const-string v22, "oo"

    aput-object v22, v6, v11

    const-string v23, "r"

    aput-object v23, v6, v12

    const-string v24, "e"

    aput-object v24, v6, v13

    const-string v25, "ai"

    aput-object v25, v6, v4

    const-string v26, "o"

    aput-object v26, v6, v14

    aput-object v26, v6, v15

    const-string v27, "au"

    aput-object v27, v6, v16

    iget-object v6, v0, Lz1/b;->f:[Ljava/lang/String;

    const-string v28, "\u093e"

    aput-object v28, v6, v7

    const-string v28, "\u093f"

    aput-object v28, v6, v8

    const-string v28, "\u0940"

    aput-object v28, v6, v9

    const-string v28, "\u0941"

    aput-object v28, v6, v5

    const-string v28, "\u0942"

    aput-object v28, v6, v10

    const-string v28, "\u0943"

    aput-object v28, v6, v11

    const-string v28, "\u0944"

    aput-object v28, v6, v12

    const-string v28, "\u0945"

    aput-object v28, v6, v13

    const-string v28, "\u0947"

    aput-object v28, v6, v4

    const-string v28, "\u0948"

    aput-object v28, v6, v14

    const-string v28, "\u0949"

    aput-object v28, v6, v15

    const-string v28, "\u094b"

    aput-object v28, v6, v16

    const-string v28, "\u094c"

    aput-object v28, v6, v1

    const/16 v28, 0xd

    const-string v29, "\u094e"

    aput-object v29, v6, v28

    const/16 v29, 0xe

    const-string v30, "\u094f"

    aput-object v30, v6, v29

    iget-object v6, v0, Lz1/b;->e:[Ljava/lang/String;

    aput-object v18, v6, v7

    aput-object v19, v6, v8

    aput-object v20, v6, v9

    aput-object v21, v6, v5

    aput-object v22, v6, v10

    aput-object v23, v6, v11

    const-string v18, "R"

    aput-object v18, v6, v12

    aput-object v24, v6, v13

    aput-object v24, v6, v4

    aput-object v25, v6, v14

    aput-object v26, v6, v15

    aput-object v26, v6, v16

    aput-object v27, v6, v1

    aput-object v24, v6, v28

    const-string v19, "aw"

    aput-object v19, v6, v29

    iget-object v6, v0, Lz1/b;->d:[Ljava/lang/String;

    const-string v19, "\u0915"

    aput-object v19, v6, v7

    const-string v19, "\u0916"

    aput-object v19, v6, v8

    const-string v19, "\u0917"

    aput-object v19, v6, v9

    const-string v19, "\u0918"

    aput-object v19, v6, v5

    const-string v19, "\u0919"

    aput-object v19, v6, v10

    const-string v19, "\u091a"

    aput-object v19, v6, v11

    const-string v19, "\u091b"

    aput-object v19, v6, v12

    const-string v19, "\u091c"

    aput-object v19, v6, v13

    const-string v19, "\u091d"

    aput-object v19, v6, v4

    const-string v19, "\u091e"

    aput-object v19, v6, v14

    const-string v19, "\u091f"

    aput-object v19, v6, v15

    const-string v19, "\u0920"

    aput-object v19, v6, v16

    const-string v19, "\u0921"

    aput-object v19, v6, v1

    const-string v19, "\u0922"

    aput-object v19, v6, v28

    const-string v19, "\u0923"

    aput-object v19, v6, v29

    const-string v19, "\u0924"

    aput-object v19, v6, v2

    const-string v19, "\u0925"

    const/16 v20, 0x10

    aput-object v19, v6, v20

    const-string v19, "\u0926"

    const/16 v21, 0x11

    aput-object v19, v6, v21

    const-string v19, "\u0927"

    const/16 v22, 0x12

    aput-object v19, v6, v22

    const-string v19, "\u0928"

    const/16 v24, 0x13

    aput-object v19, v6, v24

    const-string v19, "\u0929"

    const/16 v25, 0x14

    aput-object v19, v6, v25

    const-string v19, "\u092a"

    const/16 v26, 0x15

    aput-object v19, v6, v26

    const-string v19, "\u092b"

    const/16 v27, 0x16

    aput-object v19, v6, v27

    const-string v19, "\u092c"

    const/16 v30, 0x17

    aput-object v19, v6, v30

    const/16 v19, 0x18

    const-string v31, "\u092d"

    aput-object v31, v6, v19

    const/16 v19, 0x19

    const-string v31, "\u092e"

    aput-object v31, v6, v19

    const/16 v19, 0x1a

    const-string v31, "\u092f"

    aput-object v31, v6, v19

    const/16 v19, 0x1b

    const-string v31, "\u0930"

    aput-object v31, v6, v19

    const/16 v19, 0x1c

    const-string v31, "\u0931"

    aput-object v31, v6, v19

    const/16 v19, 0x1d

    const-string v31, "\u0932"

    aput-object v31, v6, v19

    const/16 v19, 0x1e

    const-string v31, "\u0935"

    aput-object v31, v6, v19

    const/16 v19, 0x1f

    const-string v31, "\u0936"

    aput-object v31, v6, v19

    const/16 v19, 0x20

    const-string v31, "\u0937"

    aput-object v31, v6, v19

    const/16 v19, 0x21

    const-string v31, "\u0938"

    aput-object v31, v6, v19

    const/16 v19, 0x22

    const-string v31, "\u0939"

    aput-object v31, v6, v19

    iget-object v6, v0, Lz1/b;->c:[Ljava/lang/String;

    const-string v19, "k"

    aput-object v19, v6, v7

    const-string v19, "kh"

    aput-object v19, v6, v8

    const-string v19, "g"

    aput-object v19, v6, v9

    const-string v19, "gh"

    aput-object v19, v6, v5

    const-string v19, "ng"

    aput-object v19, v6, v10

    const-string v19, "c"

    aput-object v19, v6, v11

    const-string v19, "ch"

    aput-object v19, v6, v12

    const-string v19, "j"

    aput-object v19, v6, v13

    const-string v19, "jh"

    aput-object v19, v6, v4

    const-string v19, "ny"

    aput-object v19, v6, v14

    const-string v14, "T"

    aput-object v14, v6, v15

    const-string v14, "Th"

    aput-object v14, v6, v16

    const-string v14, "D"

    aput-object v14, v6, v1

    const-string v14, "Dh"

    aput-object v14, v6, v28

    const-string v14, "N"

    aput-object v14, v6, v29

    const-string v14, "t"

    aput-object v14, v6, v2

    const-string v14, "th"

    aput-object v14, v6, v20

    const-string v14, "d"

    aput-object v14, v6, v21

    const-string v14, "dh"

    aput-object v14, v6, v22

    const-string v14, "n"

    aput-object v14, v6, v24

    const-string v14, "Nn"

    aput-object v14, v6, v25

    const-string v14, "p"

    aput-object v14, v6, v26

    const-string v14, "ph"

    aput-object v14, v6, v27

    const-string v14, "b"

    aput-object v14, v6, v30

    const/16 v14, 0x18

    const-string v15, "bh"

    aput-object v15, v6, v14

    const/16 v14, 0x19

    const-string v15, "m"

    aput-object v15, v6, v14

    const/16 v14, 0x1a

    const-string v15, "y"

    aput-object v15, v6, v14

    const/16 v14, 0x1b

    aput-object v23, v6, v14

    const/16 v14, 0x1c

    aput-object v18, v6, v14

    const/16 v14, 0x1d

    const-string v15, "l"

    aput-object v15, v6, v14

    const/16 v14, 0x1e

    const-string v15, "v"

    aput-object v15, v6, v14

    const/16 v14, 0x1f

    const-string v15, "sh"

    aput-object v15, v6, v14

    const/16 v14, 0x20

    const-string v15, "S"

    aput-object v15, v6, v14

    const/16 v14, 0x21

    const-string v15, "s"

    aput-object v15, v6, v14

    const/16 v14, 0x22

    const-string v15, "h"

    aput-object v15, v6, v14

    iget-object v6, v0, Lz1/b;->b:[Ljava/lang/String;

    const-string v14, "\u0958"

    aput-object v14, v6, v7

    const-string v14, "\u0959"

    aput-object v14, v6, v8

    const-string v14, "\u095a"

    aput-object v14, v6, v9

    const-string v14, "\u095b"

    aput-object v14, v6, v5

    const-string v14, "\u095c"

    aput-object v14, v6, v10

    const-string v14, "\u095d"

    aput-object v14, v6, v11

    const-string v14, "\u095e"

    aput-object v14, v6, v12

    const-string v14, "\u095f"

    aput-object v14, v6, v13

    iget-object v6, v0, Lz1/b;->a:[Ljava/lang/String;

    const-string v14, "q"

    aput-object v14, v6, v7

    const-string v14, "khh"

    aput-object v14, v6, v8

    const-string v14, "ghh"

    aput-object v14, v6, v9

    const-string v14, "z"

    aput-object v14, v6, v5

    const-string v14, "Ddh"

    aput-object v14, v6, v10

    const-string v10, "rh"

    aput-object v10, v6, v11

    const-string v10, "f"

    aput-object v10, v6, v12

    const-string v10, "Y"

    aput-object v10, v6, v13

    iget-object v6, v0, Lz1/b;->m:[Ljava/lang/String;

    const-string v10, "\u0901"

    aput-object v10, v6, v7

    const-string v10, "\u0902"

    aput-object v10, v6, v8

    const-string v10, "\u0903"

    aput-object v10, v6, v9

    iget-object v6, v0, Lz1/b;->l:[Ljava/lang/String;

    const-string v10, "an"

    aput-object v10, v6, v7

    const-string v10, "an"

    aput-object v10, v6, v8

    const-string v10, "ah"

    aput-object v10, v6, v9

    move v6, v7

    :goto_0
    const/16 v9, 0x23

    if-ge v6, v9, :cond_0

    iget-object v9, v0, Lz1/b;->d:[Ljava/lang/String;

    aget-object v9, v9, v6

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, v0, Lz1/b;->c:[Ljava/lang/String;

    aget-object v11, v11, v6

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iget-object v11, v0, Lz1/b;->j:Ljava/util/HashMap;

    invoke-virtual {v11, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_0
    move v3, v7

    :goto_1
    if-ge v3, v1, :cond_1

    iget-object v6, v0, Lz1/b;->i:[Ljava/lang/String;

    aget-object v6, v6, v3

    iget-object v9, v0, Lz1/b;->h:[Ljava/lang/String;

    aget-object v9, v9, v3

    iget-object v10, v0, Lz1/b;->j:Ljava/util/HashMap;

    invoke-virtual {v10, v6, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    move v3, v7

    :goto_2
    if-ge v3, v4, :cond_2

    iget-object v6, v0, Lz1/b;->b:[Ljava/lang/String;

    aget-object v6, v6, v3

    iget-object v9, v0, Lz1/b;->a:[Ljava/lang/String;

    aget-object v9, v9, v3

    iget-object v10, v0, Lz1/b;->j:Ljava/util/HashMap;

    invoke-virtual {v10, v6, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    move v3, v7

    :goto_3
    const/16 v6, 0x23

    if-ge v3, v6, :cond_5

    move v6, v7

    :goto_4
    if-ge v6, v2, :cond_3

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, v0, Lz1/b;->d:[Ljava/lang/String;

    aget-object v10, v10, v3

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v10, v0, Lz1/b;->f:[Ljava/lang/String;

    aget-object v10, v10, v6

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, v0, Lz1/b;->c:[Ljava/lang/String;

    aget-object v11, v11, v3

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v11, v0, Lz1/b;->e:[Ljava/lang/String;

    aget-object v11, v11, v6

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iget-object v11, v0, Lz1/b;->g:Ljava/util/HashMap;

    invoke-virtual {v11, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :cond_3
    move v6, v7

    :goto_5
    if-ge v6, v5, :cond_4

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, v0, Lz1/b;->d:[Ljava/lang/String;

    aget-object v10, v10, v3

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v10, v0, Lz1/b;->m:[Ljava/lang/String;

    aget-object v10, v10, v6

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, v0, Lz1/b;->c:[Ljava/lang/String;

    aget-object v11, v11, v3

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v11, v0, Lz1/b;->l:[Ljava/lang/String;

    aget-object v11, v11, v6

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iget-object v11, v0, Lz1/b;->g:Ljava/util/HashMap;

    invoke-virtual {v11, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_5
    move v3, v7

    :goto_6
    if-ge v3, v4, :cond_8

    move v6, v7

    :goto_7
    if-ge v6, v2, :cond_6

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, v0, Lz1/b;->b:[Ljava/lang/String;

    aget-object v10, v10, v3

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v10, v0, Lz1/b;->f:[Ljava/lang/String;

    aget-object v10, v10, v6

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, v0, Lz1/b;->a:[Ljava/lang/String;

    aget-object v11, v11, v3

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v11, v0, Lz1/b;->e:[Ljava/lang/String;

    aget-object v11, v11, v6

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iget-object v11, v0, Lz1/b;->g:Ljava/util/HashMap;

    invoke-virtual {v11, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v6, v6, 0x1

    goto :goto_7

    :cond_6
    move v6, v7

    :goto_8
    if-ge v6, v5, :cond_7

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, v0, Lz1/b;->d:[Ljava/lang/String;

    aget-object v10, v10, v3

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v10, v0, Lz1/b;->m:[Ljava/lang/String;

    aget-object v10, v10, v6

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, v0, Lz1/b;->c:[Ljava/lang/String;

    aget-object v11, v11, v3

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v11, v0, Lz1/b;->l:[Ljava/lang/String;

    aget-object v11, v11, v6

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iget-object v11, v0, Lz1/b;->g:Ljava/util/HashMap;

    invoke-virtual {v11, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v6, v6, 0x1

    goto :goto_8

    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_8
    move v3, v7

    :goto_9
    const/16 v4, 0x23

    if-ge v3, v4, :cond_9

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, v0, Lz1/b;->d:[Ljava/lang/String;

    aget-object v6, v6, v3

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\u094d"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v6, v0, Lz1/b;->c:[Ljava/lang/String;

    aget-object v6, v6, v3

    iget-object v9, v0, Lz1/b;->g:Ljava/util/HashMap;

    invoke-virtual {v9, v4, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :cond_9
    move v3, v7

    :goto_a
    if-ge v3, v1, :cond_a

    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    :cond_a
    move v1, v7

    const/16 v3, 0x23

    :goto_b
    if-ge v1, v3, :cond_d

    move v4, v7

    :goto_c
    if-ge v4, v2, :cond_c

    move v6, v7

    :goto_d
    if-ge v6, v5, :cond_b

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, v0, Lz1/b;->d:[Ljava/lang/String;

    aget-object v10, v10, v1

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v10, v0, Lz1/b;->f:[Ljava/lang/String;

    aget-object v10, v10, v4

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v10, v0, Lz1/b;->m:[Ljava/lang/String;

    aget-object v10, v10, v6

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, v0, Lz1/b;->c:[Ljava/lang/String;

    aget-object v11, v11, v1

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v11, v0, Lz1/b;->e:[Ljava/lang/String;

    aget-object v11, v11, v4

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v11, v0, Lz1/b;->l:[Ljava/lang/String;

    aget-object v11, v11, v6

    invoke-virtual {v11, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iget-object v11, v0, Lz1/b;->k:Ljava/util/HashMap;

    invoke-virtual {v11, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v6, v6, 0x1

    goto :goto_d

    :cond_b
    add-int/lit8 v4, v4, 0x1

    goto :goto_c

    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    :cond_d
    iget-object v1, v0, Lz1/b;->g:Ljava/util/HashMap;

    const-string v2, "\u0905\u0902"

    const-string v3, "am"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lz1/b$a;)V
    .locals 42

    move-object/from16 v0, p0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const/16 v1, 0xc

    new-array v2, v1, [Ljava/lang/String;

    iput-object v2, v0, Lz1/b;->i:[Ljava/lang/String;

    new-array v2, v1, [Ljava/lang/String;

    iput-object v2, v0, Lz1/b;->h:[Ljava/lang/String;

    const/16 v2, 0xf

    new-array v3, v2, [Ljava/lang/String;

    iput-object v3, v0, Lz1/b;->f:[Ljava/lang/String;

    new-array v3, v2, [Ljava/lang/String;

    iput-object v3, v0, Lz1/b;->e:[Ljava/lang/String;

    const/16 v3, 0x23

    new-array v4, v3, [Ljava/lang/String;

    iput-object v4, v0, Lz1/b;->d:[Ljava/lang/String;

    new-array v4, v3, [Ljava/lang/String;

    iput-object v4, v0, Lz1/b;->c:[Ljava/lang/String;

    const/16 v4, 0x8

    new-array v5, v4, [Ljava/lang/String;

    iput-object v5, v0, Lz1/b;->b:[Ljava/lang/String;

    new-array v5, v4, [Ljava/lang/String;

    iput-object v5, v0, Lz1/b;->a:[Ljava/lang/String;

    const/4 v5, 0x3

    new-array v6, v5, [Ljava/lang/String;

    iput-object v6, v0, Lz1/b;->m:[Ljava/lang/String;

    new-array v6, v5, [Ljava/lang/String;

    iput-object v6, v0, Lz1/b;->l:[Ljava/lang/String;

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, v0, Lz1/b;->j:Ljava/util/HashMap;

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, v0, Lz1/b;->g:Ljava/util/HashMap;

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, v0, Lz1/b;->k:Ljava/util/HashMap;

    iget-object v6, v0, Lz1/b;->i:[Ljava/lang/String;

    const-string v7, "\u0905"

    const/4 v8, 0x0

    aput-object v7, v6, v8

    const/4 v7, 0x1

    const-string v9, "\u0906"

    aput-object v9, v6, v7

    const-string v9, "\u0907"

    const/4 v10, 0x2

    aput-object v9, v6, v10

    const-string v9, "\u0908"

    aput-object v9, v6, v5

    const-string v9, "\u0909"

    const/4 v11, 0x4

    aput-object v9, v6, v11

    const-string v9, "\u090a"

    const/4 v12, 0x5

    aput-object v9, v6, v12

    const-string v9, "\u090b"

    const/4 v13, 0x6

    aput-object v9, v6, v13

    const-string v9, "\u090f"

    const/4 v14, 0x7

    aput-object v9, v6, v14

    const-string v9, "\u0910"

    aput-object v9, v6, v4

    const-string v9, "\u0911"

    const/16 v15, 0x9

    aput-object v9, v6, v15

    const-string v9, "\u0913"

    const/16 v16, 0xa

    aput-object v9, v6, v16

    const-string v9, "\u0914"

    const/16 v17, 0xb

    aput-object v9, v6, v17

    iget-object v6, v0, Lz1/b;->h:[Ljava/lang/String;

    const-string v9, "a"

    aput-object v9, v6, v8

    const-string v18, "aa"

    aput-object v18, v6, v7

    const-string v19, "i"

    aput-object v19, v6, v10

    const-string v20, "ee"

    aput-object v20, v6, v5

    const-string v21, "u"

    aput-object v21, v6, v11

    const-string v22, "oo"

    aput-object v22, v6, v12

    const-string v23, "r"

    aput-object v23, v6, v13

    const-string v24, "e"

    aput-object v24, v6, v14

    const-string v25, "ai"

    aput-object v25, v6, v4

    const-string v26, "o"

    aput-object v26, v6, v15

    aput-object v26, v6, v16

    const-string v27, "au"

    aput-object v27, v6, v17

    iget-object v6, v0, Lz1/b;->f:[Ljava/lang/String;

    const-string v28, "\u093e"

    aput-object v28, v6, v8

    const-string v28, "\u093f"

    aput-object v28, v6, v7

    const-string v28, "\u0940"

    aput-object v28, v6, v10

    const-string v28, "\u0941"

    aput-object v28, v6, v5

    const-string v28, "\u0942"

    aput-object v28, v6, v11

    const-string v28, "\u0943"

    aput-object v28, v6, v12

    const-string v28, "\u0944"

    aput-object v28, v6, v13

    const-string v28, "\u0945"

    aput-object v28, v6, v14

    const-string v28, "\u0947"

    aput-object v28, v6, v4

    const-string v28, "\u0948"

    aput-object v28, v6, v15

    const-string v28, "\u0949"

    aput-object v28, v6, v16

    const-string v28, "\u094b"

    aput-object v28, v6, v17

    const-string v28, "\u094c"

    aput-object v28, v6, v1

    const-string v28, "\u094e"

    const/16 v29, 0xd

    aput-object v28, v6, v29

    const-string v28, "\u094f"

    const/16 v30, 0xe

    aput-object v28, v6, v30

    iget-object v6, v0, Lz1/b;->e:[Ljava/lang/String;

    aput-object v18, v6, v8

    aput-object v19, v6, v7

    aput-object v20, v6, v10

    aput-object v21, v6, v5

    aput-object v22, v6, v11

    aput-object v23, v6, v12

    const-string v18, "R"

    aput-object v18, v6, v13

    aput-object v24, v6, v14

    aput-object v24, v6, v4

    aput-object v25, v6, v15

    aput-object v26, v6, v16

    aput-object v26, v6, v17

    aput-object v27, v6, v1

    aput-object v24, v6, v29

    const-string v19, "aw"

    aput-object v19, v6, v30

    iget-object v6, v0, Lz1/b;->d:[Ljava/lang/String;

    const-string v19, "\u0915"

    aput-object v19, v6, v8

    const-string v19, "\u0916"

    aput-object v19, v6, v7

    const-string v19, "\u0917"

    aput-object v19, v6, v10

    const-string v19, "\u0918"

    aput-object v19, v6, v5

    const-string v19, "\u0919"

    aput-object v19, v6, v11

    const-string v19, "\u091a"

    aput-object v19, v6, v12

    const-string v19, "\u091b"

    aput-object v19, v6, v13

    const-string v19, "\u091c"

    aput-object v19, v6, v14

    const-string v19, "\u091d"

    aput-object v19, v6, v4

    const-string v19, "\u091e"

    aput-object v19, v6, v15

    const-string v19, "\u091f"

    aput-object v19, v6, v16

    const-string v19, "\u0920"

    aput-object v19, v6, v17

    const-string v19, "\u0921"

    aput-object v19, v6, v1

    const-string v19, "\u0922"

    aput-object v19, v6, v29

    const-string v19, "\u0923"

    aput-object v19, v6, v30

    const-string v19, "\u0924"

    aput-object v19, v6, v2

    const-string v19, "\u0925"

    const/16 v20, 0x10

    aput-object v19, v6, v20

    const-string v19, "\u0926"

    const/16 v21, 0x11

    aput-object v19, v6, v21

    const-string v19, "\u0927"

    const/16 v22, 0x12

    aput-object v19, v6, v22

    const-string v19, "\u0928"

    const/16 v24, 0x13

    aput-object v19, v6, v24

    const-string v19, "\u0929"

    const/16 v25, 0x14

    aput-object v19, v6, v25

    const-string v19, "\u092a"

    const/16 v26, 0x15

    aput-object v19, v6, v26

    const-string v19, "\u092b"

    const/16 v27, 0x16

    aput-object v19, v6, v27

    const-string v19, "\u092c"

    const/16 v28, 0x17

    aput-object v19, v6, v28

    const/16 v19, 0x18

    const-string v31, "\u092d"

    aput-object v31, v6, v19

    const/16 v31, 0x19

    const-string v32, "\u092e"

    aput-object v32, v6, v31

    const/16 v32, 0x1a

    const-string v33, "\u092f"

    aput-object v33, v6, v32

    const/16 v33, 0x1b

    const-string v34, "\u0930"

    aput-object v34, v6, v33

    const/16 v34, 0x1c

    const-string v35, "\u0931"

    aput-object v35, v6, v34

    const/16 v35, 0x1d

    const-string v36, "\u0932"

    aput-object v36, v6, v35

    const/16 v36, 0x1e

    const-string v37, "\u0935"

    aput-object v37, v6, v36

    const/16 v37, 0x1f

    const-string v38, "\u0936"

    aput-object v38, v6, v37

    const/16 v38, 0x20

    const-string v39, "\u0937"

    aput-object v39, v6, v38

    const/16 v39, 0x21

    const-string v40, "\u0938"

    aput-object v40, v6, v39

    const/16 v40, 0x22

    const-string v41, "\u0939"

    aput-object v41, v6, v40

    iget-object v6, v0, Lz1/b;->c:[Ljava/lang/String;

    const-string v41, "k"

    aput-object v41, v6, v8

    const-string v41, "kh"

    aput-object v41, v6, v7

    const-string v41, "g"

    aput-object v41, v6, v10

    const-string v41, "gh"

    aput-object v41, v6, v5

    const-string v41, "ng"

    aput-object v41, v6, v11

    const-string v41, "c"

    aput-object v41, v6, v12

    const-string v41, "ch"

    aput-object v41, v6, v13

    const-string v41, "j"

    aput-object v41, v6, v14

    const-string v41, "jh"

    aput-object v41, v6, v4

    const-string v41, "ny"

    aput-object v41, v6, v15

    const-string v15, "T"

    aput-object v15, v6, v16

    const-string v15, "Th"

    aput-object v15, v6, v17

    const-string v15, "D"

    aput-object v15, v6, v1

    const-string v15, "Dh"

    aput-object v15, v6, v29

    const-string v15, "N"

    aput-object v15, v6, v30

    const-string v15, "t"

    aput-object v15, v6, v2

    const-string v15, "th"

    aput-object v15, v6, v20

    const-string v15, "d"

    aput-object v15, v6, v21

    const-string v15, "dh"

    aput-object v15, v6, v22

    const-string v15, "n"

    aput-object v15, v6, v24

    const-string v15, "Nn"

    aput-object v15, v6, v25

    const-string v15, "p"

    aput-object v15, v6, v26

    const-string v15, "ph"

    aput-object v15, v6, v27

    const-string v15, "b"

    aput-object v15, v6, v28

    const-string v15, "bh"

    aput-object v15, v6, v19

    const-string v15, "m"

    aput-object v15, v6, v31

    const-string v15, "y"

    aput-object v15, v6, v32

    aput-object v23, v6, v33

    aput-object v18, v6, v34

    const-string v15, "l"

    aput-object v15, v6, v35

    const-string v15, "v"

    aput-object v15, v6, v36

    const-string v15, "sh"

    aput-object v15, v6, v37

    const-string v15, "S"

    aput-object v15, v6, v38

    const-string v15, "s"

    aput-object v15, v6, v39

    const-string v15, "h"

    aput-object v15, v6, v40

    iget-object v6, v0, Lz1/b;->b:[Ljava/lang/String;

    const-string v15, "\u0958"

    aput-object v15, v6, v8

    const-string v15, "\u0959"

    aput-object v15, v6, v7

    const-string v15, "\u095a"

    aput-object v15, v6, v10

    const-string v15, "\u095b"

    aput-object v15, v6, v5

    const-string v15, "\u095c"

    aput-object v15, v6, v11

    const-string v15, "\u095d"

    aput-object v15, v6, v12

    const-string v15, "\u095e"

    aput-object v15, v6, v13

    const-string v15, "\u095f"

    aput-object v15, v6, v14

    iget-object v6, v0, Lz1/b;->a:[Ljava/lang/String;

    const-string v15, "q"

    aput-object v15, v6, v8

    const-string v15, "khh"

    aput-object v15, v6, v7

    const-string v15, "ghh"

    aput-object v15, v6, v10

    const-string v15, "z"

    aput-object v15, v6, v5

    const-string v15, "Ddh"

    aput-object v15, v6, v11

    const-string v11, "rh"

    aput-object v11, v6, v12

    const-string v11, "f"

    aput-object v11, v6, v13

    const-string v11, "Y"

    aput-object v11, v6, v14

    iget-object v6, v0, Lz1/b;->m:[Ljava/lang/String;

    const-string v11, "\u0901"

    aput-object v11, v6, v8

    const-string v11, "\u0902"

    aput-object v11, v6, v7

    const-string v11, "\u0903"

    aput-object v11, v6, v10

    iget-object v6, v0, Lz1/b;->l:[Ljava/lang/String;

    const-string v11, "an"

    aput-object v11, v6, v8

    aput-object v11, v6, v7

    const-string v11, "ah"

    aput-object v11, v6, v10

    move v6, v8

    :goto_0
    if-ge v6, v3, :cond_0

    iget-object v10, v0, Lz1/b;->d:[Ljava/lang/String;

    aget-object v10, v10, v6

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, v0, Lz1/b;->c:[Ljava/lang/String;

    aget-object v12, v12, v6

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    iget-object v12, v0, Lz1/b;->j:Ljava/util/HashMap;

    invoke-virtual {v12, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_0
    move v6, v8

    :goto_1
    if-ge v6, v1, :cond_1

    iget-object v9, v0, Lz1/b;->i:[Ljava/lang/String;

    aget-object v9, v9, v6

    iget-object v10, v0, Lz1/b;->h:[Ljava/lang/String;

    aget-object v10, v10, v6

    iget-object v11, v0, Lz1/b;->j:Ljava/util/HashMap;

    invoke-virtual {v11, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_1
    move v6, v8

    :goto_2
    if-ge v6, v4, :cond_2

    iget-object v9, v0, Lz1/b;->b:[Ljava/lang/String;

    aget-object v9, v9, v6

    iget-object v10, v0, Lz1/b;->a:[Ljava/lang/String;

    aget-object v10, v10, v6

    iget-object v11, v0, Lz1/b;->j:Ljava/util/HashMap;

    invoke-virtual {v11, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :cond_2
    move v6, v8

    :goto_3
    if-ge v6, v3, :cond_5

    move v9, v8

    :goto_4
    if-ge v9, v2, :cond_3

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, v0, Lz1/b;->d:[Ljava/lang/String;

    aget-object v11, v11, v6

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v11, v0, Lz1/b;->f:[Ljava/lang/String;

    aget-object v11, v11, v9

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, v0, Lz1/b;->c:[Ljava/lang/String;

    aget-object v12, v12, v6

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v12, v0, Lz1/b;->e:[Ljava/lang/String;

    aget-object v12, v12, v9

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    iget-object v12, v0, Lz1/b;->g:Ljava/util/HashMap;

    invoke-virtual {v12, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    :cond_3
    move v9, v8

    :goto_5
    if-ge v9, v5, :cond_4

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, v0, Lz1/b;->d:[Ljava/lang/String;

    aget-object v11, v11, v6

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v11, v0, Lz1/b;->m:[Ljava/lang/String;

    aget-object v11, v11, v9

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, v0, Lz1/b;->c:[Ljava/lang/String;

    aget-object v12, v12, v6

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v12, v0, Lz1/b;->l:[Ljava/lang/String;

    aget-object v12, v12, v9

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    iget-object v12, v0, Lz1/b;->g:Ljava/util/HashMap;

    invoke-virtual {v12, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v9, v9, 0x1

    goto :goto_5

    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_5
    move v6, v8

    :goto_6
    if-ge v6, v4, :cond_8

    move v9, v8

    :goto_7
    if-ge v9, v2, :cond_6

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, v0, Lz1/b;->b:[Ljava/lang/String;

    aget-object v11, v11, v6

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v11, v0, Lz1/b;->f:[Ljava/lang/String;

    aget-object v11, v11, v9

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, v0, Lz1/b;->a:[Ljava/lang/String;

    aget-object v12, v12, v6

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v12, v0, Lz1/b;->e:[Ljava/lang/String;

    aget-object v12, v12, v9

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    iget-object v12, v0, Lz1/b;->g:Ljava/util/HashMap;

    invoke-virtual {v12, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v9, v9, 0x1

    goto :goto_7

    :cond_6
    move v9, v8

    :goto_8
    if-ge v9, v5, :cond_7

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, v0, Lz1/b;->d:[Ljava/lang/String;

    aget-object v11, v11, v6

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v11, v0, Lz1/b;->m:[Ljava/lang/String;

    aget-object v11, v11, v9

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, v0, Lz1/b;->c:[Ljava/lang/String;

    aget-object v12, v12, v6

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v12, v0, Lz1/b;->l:[Ljava/lang/String;

    aget-object v12, v12, v9

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    iget-object v12, v0, Lz1/b;->g:Ljava/util/HashMap;

    invoke-virtual {v12, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v9, v9, 0x1

    goto :goto_8

    :cond_7
    add-int/lit8 v6, v6, 0x1

    goto :goto_6

    :cond_8
    move v4, v8

    :goto_9
    if-ge v4, v3, :cond_9

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, v0, Lz1/b;->d:[Ljava/lang/String;

    aget-object v9, v9, v4

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\u094d"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v9, v0, Lz1/b;->c:[Ljava/lang/String;

    aget-object v9, v9, v4

    iget-object v10, v0, Lz1/b;->g:Ljava/util/HashMap;

    invoke-virtual {v10, v6, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v4, v4, 0x1

    goto :goto_9

    :cond_9
    move v4, v8

    :goto_a
    if-ge v4, v1, :cond_a

    add-int/lit8 v4, v4, 0x1

    goto :goto_a

    :cond_a
    move v1, v8

    :goto_b
    if-ge v1, v3, :cond_d

    move v4, v8

    :goto_c
    if-ge v4, v2, :cond_c

    move v6, v8

    :goto_d
    if-ge v6, v5, :cond_b

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, v0, Lz1/b;->d:[Ljava/lang/String;

    aget-object v10, v10, v1

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v10, v0, Lz1/b;->f:[Ljava/lang/String;

    aget-object v10, v10, v4

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v10, v0, Lz1/b;->m:[Ljava/lang/String;

    aget-object v10, v10, v6

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, v0, Lz1/b;->c:[Ljava/lang/String;

    aget-object v11, v11, v1

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v11, v0, Lz1/b;->e:[Ljava/lang/String;

    aget-object v11, v11, v4

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v11, v0, Lz1/b;->l:[Ljava/lang/String;

    aget-object v11, v11, v6

    invoke-virtual {v11, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iget-object v11, v0, Lz1/b;->k:Ljava/util/HashMap;

    invoke-virtual {v11, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v6, v6, 0x1

    goto :goto_d

    :cond_b
    add-int/lit8 v4, v4, 0x1

    goto :goto_c

    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    :cond_d
    iget-object v1, v0, Lz1/b;->g:Ljava/util/HashMap;

    const-string v2, "\u0905\u0902"

    const-string v3, "am"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static varargs a([Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    sget-object v0, Lz1/b;->p:Lm1/j$g;

    invoke-virtual {v0}, Lm1/j$b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/StringBuilder;

    array-length v1, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p0, v2

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    sget-object v1, Lz1/b;->p:Lm1/j$g;

    invoke-virtual {v1, v0}, Lm1/j$b;->d(Ljava/lang/Object;)V

    return-object p0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    sget-object v0, Lz1/b;->p:Lm1/j$g;

    invoke-virtual {v0}, Lm1/j$b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_2

    invoke-static {p0, v3}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->charCount(I)I

    move-result v5

    const/4 v6, 0x1

    if-le v5, v6, :cond_0

    add-int/lit8 v5, v5, -0x1

    add-int/2addr v3, v5

    :cond_0
    const/16 v5, 0x80

    if-ge v4, v5, :cond_1

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_1
    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v2

    const-string v4, "\\u%04x"

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    add-int/2addr v3, v6

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    sget-object v1, Lz1/b;->p:Lm1/j$g;

    invoke-virtual {v1, v0}, Lm1/j$b;->d(Ljava/lang/Object;)V

    return-object p0
.end method
