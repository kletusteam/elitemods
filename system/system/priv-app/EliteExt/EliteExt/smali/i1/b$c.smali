.class public Li1/b$c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Li1/b$e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Li1/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "c"
.end annotation


# instance fields
.field public a:Lh1/f;

.field public final synthetic b:Li1/b;


# direct methods
.method public constructor <init>(Li1/b;Li1/b$a;)V
    .locals 0

    iput-object p1, p0, Li1/b$c;->b:Li1/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public d(Z)V
    .locals 1

    iget-object p1, p0, Li1/b$c;->b:Li1/b;

    iget-object v0, p1, Lh1/a;->f:Lh1/l;

    check-cast v0, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    iget-object p1, p1, Li1/b;->j:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;->i(Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;)Z

    return-void
.end method

.method public g()Z
    .locals 2

    iget-object v0, p0, Li1/b$c;->b:Li1/b;

    iget-object v1, v0, Lh1/a;->f:Lh1/l;

    check-cast v1, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    iget-object v0, v0, Li1/b;->j:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {v1, v0}, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;->m(Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;)Z

    move-result v0

    return v0
.end method

.method public i(Lh1/g;)V
    .locals 6

    iget-object v0, p0, Li1/b$c;->b:Li1/b;

    iget-object v0, v0, Lh1/a;->f:Lh1/l;

    check-cast v0, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    const/4 v1, 0x0

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lh1/g;->j()V

    iget-object v2, p1, Lh1/g;->o:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_3

    iget-object v2, p0, Li1/b$c;->a:Lh1/f;

    if-nez v2, :cond_0

    new-instance v2, Lh1/f;

    iget-object v3, p0, Li1/b$c;->b:Li1/b;

    iget-object v4, v3, Lh1/a;->b:Landroid/content/Context;

    iget v5, v3, Li1/b;->l:I

    iget v3, v3, Li1/b;->k:I

    invoke-direct {v2, v4, v5, v3}, Lh1/f;-><init>(Landroid/content/Context;II)V

    iput-object v2, p0, Li1/b$c;->a:Lh1/f;

    :cond_0
    iget-object v2, p0, Li1/b$c;->a:Lh1/f;

    invoke-virtual {p1, v2}, Lh1/g;->b(Lh1/k;)V

    iget-object p1, p0, Li1/b$c;->a:Lh1/f;

    iget-object v2, p0, Li1/b$c;->b:Li1/b;

    iget-object v2, v2, Lh1/a;->f:Lh1/l;

    check-cast v2, Landroid/view/ViewGroup;

    iget-object v3, p1, Lh1/f;->a:Lh1/f$a;

    if-nez v3, :cond_1

    new-instance v3, Lh1/f$a;

    invoke-direct {v3, p1}, Lh1/f$a;-><init>(Lh1/f;)V

    iput-object v3, p1, Lh1/f;->a:Lh1/f$a;

    :cond_1
    iget-object v3, p1, Lh1/f;->a:Lh1/f$a;

    invoke-virtual {v3}, Landroid/widget/BaseAdapter;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v1, p1, Lh1/f;->h:Lmiuix/appcompat/internal/view/menu/ExpandedMenuView;

    if-nez v1, :cond_2

    iget-object v1, p1, Lh1/f;->d:Landroid/view/LayoutInflater;

    iget v3, p1, Lh1/f;->g:I

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/internal/view/menu/ExpandedMenuView;

    iput-object v1, p1, Lh1/f;->h:Lmiuix/appcompat/internal/view/menu/ExpandedMenuView;

    iget-object v2, p1, Lh1/f;->a:Lh1/f$a;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p1, Lh1/f;->h:Lmiuix/appcompat/internal/view/menu/ExpandedMenuView;

    invoke-virtual {v1, p1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    :cond_2
    iget-object v1, p1, Lh1/f;->h:Lmiuix/appcompat/internal/view/menu/ExpandedMenuView;

    :cond_3
    invoke-virtual {v0, v1}, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;->setOverflowMenuView(Landroid/view/View;)V

    return-void
.end method

.method public isShowing()Z
    .locals 1

    iget-object v0, p0, Li1/b$c;->b:Li1/b;

    iget-object v0, v0, Lh1/a;->f:Lh1/l;

    check-cast v0, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;->k()Z

    move-result v0

    return v0
.end method
