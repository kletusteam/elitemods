.class public Li1/b;
.super Lh1/a;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Li1/b$b;,
        Li1/b$c;,
        Li1/b$d;,
        Li1/b$e;,
        Li1/b$f;,
        Li1/b$g;
    }
.end annotation


# instance fields
.field public i:Li1/b$b;

.field public j:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

.field public k:I

.field public l:I

.field public m:Li1/b$e;

.field public n:I

.field public o:Landroid/view/View;

.field public p:Li1/b$e;

.field public q:I

.field public r:Lh1/i;

.field public final s:Li1/b$g;

.field public t:Li1/b$d;

.field public u:Z

.field public v:Z

.field public w:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;IIII)V
    .locals 0

    invoke-direct {p0, p1, p3, p4}, Lh1/a;-><init>(Landroid/content/Context;II)V

    const p1, 0x10102f6

    iput p1, p0, Li1/b;->q:I

    new-instance p1, Landroid/util/SparseBooleanArray;

    invoke-direct {p1}, Landroid/util/SparseBooleanArray;-><init>()V

    new-instance p1, Li1/b$g;

    const/4 p3, 0x0

    invoke-direct {p1, p0, p3}, Li1/b$g;-><init>(Li1/b;Li1/b$a;)V

    iput-object p1, p0, Li1/b;->s:Li1/b$g;

    iput p5, p0, Li1/b;->l:I

    iput p6, p0, Li1/b;->k:I

    iput-object p2, p0, Li1/b;->j:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 9

    iget-object v0, p0, Lh1/a;->d:Lh1/g;

    invoke-virtual {v0}, Lh1/g;->l()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, p0, Li1/b;->n:I

    if-ge v2, v1, :cond_0

    add-int/lit8 v2, v2, -0x1

    :cond_0
    const/4 v3, 0x0

    move v4, v3

    :goto_0
    const/4 v5, 0x1

    if-ge v4, v1, :cond_7

    if-lez v2, :cond_7

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lh1/i;

    iget v7, v6, Lh1/i;->p:I

    and-int/lit8 v8, v7, 0x1

    if-ne v8, v5, :cond_1

    move v8, v5

    goto :goto_1

    :cond_1
    move v8, v3

    :goto_1
    if-nez v8, :cond_4

    and-int/lit8 v7, v7, 0x2

    const/4 v8, 0x2

    if-ne v7, v8, :cond_2

    move v7, v5

    goto :goto_2

    :cond_2
    move v7, v3

    :goto_2
    if-eqz v7, :cond_3

    goto :goto_3

    :cond_3
    move v5, v3

    :cond_4
    :goto_3
    iget v7, v6, Lh1/i;->d:I

    if-eqz v5, :cond_5

    or-int/lit8 v7, v7, 0x20

    goto :goto_4

    :cond_5
    and-int/lit8 v7, v7, -0x21

    :goto_4
    iput v7, v6, Lh1/i;->d:I

    if-eqz v5, :cond_6

    add-int/lit8 v2, v2, -0x1

    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_7
    :goto_5
    if-ge v4, v1, :cond_8

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lh1/i;

    iget v3, v2, Lh1/i;->d:I

    and-int/lit8 v3, v3, -0x21

    iput v3, v2, Lh1/i;->d:I

    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_8
    return v5
.end method

.method public b(Lh1/g;Z)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Li1/b;->l(Z)Z

    iget-object v0, p0, Lh1/a;->a:Lh1/k$a;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lh1/k$a;->b(Lh1/g;Z)V

    :cond_0
    return-void
.end method

.method public c(Z)V
    .locals 12

    iget-object p1, p0, Lh1/a;->f:Lh1/l;

    move-object v0, p1

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-nez v0, :cond_0

    goto/16 :goto_8

    :cond_0
    invoke-interface {p1}, Lh1/l;->e()Z

    move-result p1

    iget-object v4, p0, Lh1/a;->f:Lh1/l;

    invoke-interface {v4}, Lh1/l;->c()Z

    move-result v4

    if-eqz v4, :cond_1

    add-int/lit8 p1, p1, 0x1

    :cond_1
    iget-object v4, p0, Lh1/a;->d:Lh1/g;

    if-eqz v4, :cond_f

    invoke-virtual {v4}, Lh1/g;->j()V

    iget-object v4, p0, Lh1/a;->d:Lh1/g;

    invoke-virtual {v4}, Lh1/g;->l()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lh1/i;

    iget v6, v5, Lh1/i;->d:I

    const/16 v7, 0x20

    and-int/2addr v6, v7

    if-ne v6, v7, :cond_3

    move v6, v1

    goto :goto_1

    :cond_3
    move v6, v3

    :goto_1
    if-eqz v6, :cond_2

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    instance-of v7, v6, Lh1/l$a;

    if-eqz v7, :cond_4

    move-object v7, v6

    check-cast v7, Lh1/l$a;

    invoke-interface {v7}, Lh1/l$a;->getItemData()Lh1/i;

    move-result-object v7

    goto :goto_2

    :cond_4
    move-object v7, v2

    :goto_2
    invoke-virtual {v5}, Lh1/i;->getActionView()Landroid/view/View;

    move-result-object v8

    const/16 v9, 0x8

    if-eqz v8, :cond_6

    iget v10, v5, Lh1/i;->p:I

    and-int/2addr v10, v9

    if-eqz v10, :cond_5

    iget-object v10, v5, Lh1/i;->a:Landroid/view/View;

    if-eqz v10, :cond_5

    move v10, v1

    goto :goto_3

    :cond_5
    move v10, v3

    :goto_3
    if-eqz v10, :cond_9

    :cond_6
    instance-of v8, v6, Lmiuix/appcompat/internal/view/menu/action/ActionMenuItemView;

    if-nez v8, :cond_7

    move-object v8, v2

    goto :goto_4

    :cond_7
    move-object v8, v6

    :goto_4
    instance-of v10, v8, Lh1/l$a;

    if-eqz v10, :cond_8

    check-cast v8, Lh1/l$a;

    goto :goto_5

    :cond_8
    iget-object v8, p0, Lh1/a;->h:Landroid/view/LayoutInflater;

    iget v10, p0, Lh1/a;->c:I

    invoke-virtual {v8, v10, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lh1/l$a;

    :goto_5
    invoke-interface {v8, v5, v3}, Lh1/l$a;->a(Lh1/i;I)V

    iget-object v10, p0, Lh1/a;->f:Lh1/l;

    check-cast v10, Lh1/g$b;

    invoke-interface {v8, v10}, Lh1/l$a;->setItemInvoker(Lh1/g$b;)V

    check-cast v8, Landroid/view/View;

    :cond_9
    iget-boolean v10, v5, Lh1/i;->j:Z

    if-eqz v10, :cond_a

    goto :goto_6

    :cond_a
    move v9, v3

    :goto_6
    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    move-object v9, v0

    check-cast v9, Li1/c;

    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    invoke-virtual {v9, v10}, Li1/c;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v11

    if-nez v11, :cond_b

    invoke-virtual {v9, v10}, Li1/c;->g(Landroid/view/ViewGroup$LayoutParams;)Li1/c$a;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_b
    if-eq v5, v7, :cond_c

    invoke-virtual {v8, v3}, Landroid/view/View;->setPressed(Z)V

    :cond_c
    if-eq v8, v6, :cond_e

    invoke-virtual {v8}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    if-eqz v5, :cond_d

    invoke-virtual {v5, v8}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_d
    iget-object v5, p0, Lh1/a;->f:Lh1/l;

    check-cast v5, Landroid/view/ViewGroup;

    invoke-virtual {v5, v8, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    :cond_e
    add-int/lit8 p1, p1, 0x1

    goto/16 :goto_0

    :cond_f
    :goto_7
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    if-ge p1, v4, :cond_10

    iget-object v4, p0, Lh1/a;->f:Lh1/l;

    invoke-interface {v4, p1}, Lh1/l;->a(I)Z

    move-result v4

    if-nez v4, :cond_f

    add-int/lit8 p1, p1, 0x1

    goto :goto_7

    :cond_10
    :goto_8
    iget-object p1, p0, Lh1/a;->f:Lh1/l;

    if-nez p1, :cond_11

    return-void

    :cond_11
    iget-object p1, p0, Lh1/a;->d:Lh1/g;

    if-eqz p1, :cond_12

    invoke-virtual {p1}, Lh1/g;->j()V

    iget-object p1, p1, Lh1/g;->o:Ljava/util/ArrayList;

    goto :goto_9

    :cond_12
    move-object p1, v2

    :goto_9
    iget-boolean v0, p0, Li1/b;->u:Z

    if-eqz v0, :cond_14

    if-eqz p1, :cond_14

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v1, :cond_13

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lh1/i;

    iget-boolean p1, p1, Lh1/i;->j:Z

    xor-int/lit8 v3, p1, 0x1

    goto :goto_a

    :cond_13
    if-lez v0, :cond_14

    move v3, v1

    :cond_14
    :goto_a
    iget-object p1, p0, Li1/b;->o:Landroid/view/View;

    if-eqz v3, :cond_17

    if-nez p1, :cond_15

    iget-object p1, p0, Lh1/a;->g:Landroid/content/Context;

    new-instance v0, Li1/d;

    iget v3, p0, Li1/b;->q:I

    invoke-direct {v0, p1, v2, v3}, Li1/d;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance p1, Ld/o;

    invoke-direct {p1, p0}, Ld/o;-><init>(Ljava/lang/Object;)V

    iput-object p1, v0, Li1/d;->b:Li1/d$a;

    iput-object v0, p0, Li1/b;->o:Landroid/view/View;

    goto :goto_b

    :cond_15
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    :goto_b
    iget-object p1, p0, Li1/b;->o:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iget-object v0, p0, Lh1/a;->f:Lh1/l;

    if-eq p1, v0, :cond_18

    if-eqz p1, :cond_16

    iget-object v0, p0, Li1/b;->o:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_16
    iget-object p1, p0, Lh1/a;->f:Lh1/l;

    check-cast p1, Li1/c;

    iget-object v0, p0, Li1/b;->o:Landroid/view/View;

    invoke-virtual {p1}, Li1/c;->f()Li1/c$a;

    move-result-object v2

    iput-boolean v1, v2, Li1/c$a;->a:Z

    invoke-virtual {p1, v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_c

    :cond_17
    if-eqz p1, :cond_18

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    iget-object v0, p0, Lh1/a;->f:Lh1/l;

    if-ne p1, v0, :cond_18

    check-cast v0, Landroid/view/ViewGroup;

    iget-object p1, p0, Li1/b;->o:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_18
    :goto_c
    iget-object p1, p0, Lh1/a;->f:Lh1/l;

    check-cast p1, Li1/c;

    iget-boolean v0, p0, Li1/b;->u:Z

    invoke-virtual {p1, v0}, Li1/c;->setOverflowReserved(Z)V

    invoke-static {}, Lr1/b;->a()Z

    move-result p1

    if-nez p1, :cond_19

    invoke-virtual {p0}, Li1/b;->k()Li1/b$e;

    move-result-object p1

    iget-object v0, p0, Lh1/a;->d:Lh1/g;

    invoke-interface {p1, v0}, Li1/b$e;->i(Lh1/g;)V

    :cond_19
    return-void
.end method

.method public e(Landroid/content/Context;Lh1/g;)V
    .locals 2

    iput-object p1, p0, Lh1/a;->b:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    iput-object p2, p0, Lh1/a;->d:Lh1/g;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    iget-boolean p2, p0, Li1/b;->v:Z

    if-nez p2, :cond_0

    const/4 p2, 0x1

    iput-boolean p2, p0, Li1/b;->u:Z

    :cond_0
    iget-boolean p2, p0, Li1/b;->w:Z

    if-nez p2, :cond_1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p2

    iget p2, p2, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 p2, p2, 0x2

    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0b0002

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    iput p1, p0, Li1/b;->n:I

    iget-boolean p1, p0, Li1/b;->u:Z

    const/4 p2, 0x0

    if-eqz p1, :cond_3

    iget-object p1, p0, Li1/b;->o:Landroid/view/View;

    if-nez p1, :cond_2

    iget-object p1, p0, Lh1/a;->g:Landroid/content/Context;

    new-instance v0, Li1/d;

    iget v1, p0, Li1/b;->q:I

    invoke-direct {v0, p1, p2, v1}, Li1/d;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance p1, Ld/o;

    invoke-direct {p1, p0}, Ld/o;-><init>(Ljava/lang/Object;)V

    iput-object p1, v0, Li1/d;->b:Li1/d$a;

    iput-object v0, p0, Li1/b;->o:Landroid/view/View;

    const/4 p1, 0x0

    invoke-static {p1, p1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    iget-object p2, p0, Li1/b;->o:Landroid/view/View;

    invoke-virtual {p2, p1, p1}, Landroid/view/View;->measure(II)V

    :cond_2
    iget-object p1, p0, Li1/b;->o:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    goto :goto_0

    :cond_3
    iput-object p2, p0, Li1/b;->o:Landroid/view/View;

    :goto_0
    return-void
.end method

.method public h(Lh1/m;)Z
    .locals 8

    invoke-virtual {p1}, Lh1/g;->hasVisibleItems()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    move-object v0, p1

    :goto_0
    iget-object v2, v0, Lh1/m;->y:Lh1/g;

    iget-object v3, p0, Lh1/a;->d:Lh1/g;

    if-eq v2, v3, :cond_1

    move-object v0, v2

    check-cast v0, Lh1/m;

    goto :goto_0

    :cond_1
    iget-object v0, v0, Lh1/m;->x:Lh1/i;

    iget-object v2, p0, Lh1/a;->f:Lh1/l;

    check-cast v2, Landroid/view/ViewGroup;

    const/4 v3, 0x0

    if-nez v2, :cond_2

    goto :goto_2

    :cond_2
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    move v5, v1

    :goto_1
    if-ge v5, v4, :cond_4

    invoke-virtual {v2, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    instance-of v7, v6, Lh1/l$a;

    if-eqz v7, :cond_3

    move-object v7, v6

    check-cast v7, Lh1/l$a;

    invoke-interface {v7}, Lh1/l$a;->getItemData()Lh1/i;

    move-result-object v7

    if-ne v7, v0, :cond_3

    goto :goto_3

    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_4
    :goto_2
    move-object v6, v3

    :goto_3
    if-nez v6, :cond_5

    iget-object v0, p0, Li1/b;->o:Landroid/view/View;

    if-nez v0, :cond_5

    return v1

    :cond_5
    iget-object v0, p1, Lh1/m;->x:Lh1/i;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Li1/b$b;

    invoke-direct {v0, p0, p1}, Li1/b$b;-><init>(Li1/b;Lh1/m;)V

    iput-object v0, p0, Li1/b;->i:Li1/b$b;

    invoke-virtual {v0, v3}, Lh1/h;->d(Landroid/os/IBinder;)V

    iget-object v0, p0, Lh1/a;->a:Lh1/k$a;

    if-eqz v0, :cond_6

    invoke-interface {v0, p1}, Lh1/k$a;->c(Lh1/g;)Z

    :cond_6
    const/4 p1, 0x1

    return p1
.end method

.method public i(Landroid/view/ViewGroup;)Lh1/l;
    .locals 3

    iget-object v0, p0, Lh1/a;->f:Lh1/l;

    if-nez v0, :cond_0

    iget-object v0, p0, Lh1/a;->h:Landroid/view/LayoutInflater;

    iget v1, p0, Lh1/a;->e:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lh1/l;

    iput-object p1, p0, Lh1/a;->f:Lh1/l;

    iget-object v0, p0, Lh1/a;->d:Lh1/g;

    invoke-interface {p1, v0}, Lh1/l;->b(Lh1/g;)V

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Li1/b;->c(Z)V

    :cond_0
    iget-object p1, p0, Lh1/a;->f:Lh1/l;

    move-object v0, p1

    check-cast v0, Li1/c;

    invoke-virtual {v0, p0}, Li1/c;->setPresenter(Li1/b;)V

    return-object p1
.end method

.method public final k()Li1/b$e;
    .locals 7

    invoke-static {}, Lr1/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Li1/b$f;

    iget-object v3, p0, Lh1/a;->b:Landroid/content/Context;

    iget-object v4, p0, Lh1/a;->d:Lh1/g;

    iget-object v5, p0, Li1/b;->o:Landroid/view/View;

    const/4 v6, 0x1

    move-object v1, v0

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Li1/b$f;-><init>(Li1/b;Landroid/content/Context;Lh1/g;Landroid/view/View;Z)V

    return-object v0

    :cond_0
    iget-object v0, p0, Li1/b;->m:Li1/b$e;

    if-nez v0, :cond_1

    new-instance v0, Li1/b$c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Li1/b$c;-><init>(Li1/b;Li1/b$a;)V

    iput-object v0, p0, Li1/b;->m:Li1/b$e;

    :cond_1
    iget-object v0, p0, Li1/b;->m:Li1/b$e;

    return-object v0
.end method

.method public l(Z)Z
    .locals 3

    iget-object v0, p0, Li1/b;->t:Li1/b$d;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lh1/a;->f:Lh1/l;

    if-eqz v0, :cond_0

    iget-object p1, p0, Li1/b;->o:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setSelected(Z)V

    iget-object p1, p0, Lh1/a;->f:Lh1/l;

    check-cast p1, Landroid/view/View;

    iget-object v0, p0, Li1/b;->t:Li1/b$d;

    invoke-virtual {p1, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    const/4 p1, 0x0

    iput-object p1, p0, Li1/b;->t:Li1/b$d;

    const/4 p1, 0x1

    return p1

    :cond_0
    iget-object v0, p0, Li1/b;->p:Li1/b$e;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Li1/b$e;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Li1/b;->o:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setSelected(Z)V

    :cond_1
    iget-object v1, p0, Li1/b;->p:Li1/b$e;

    invoke-interface {v1, p1}, Li1/b$e;->d(Z)V

    return v0

    :cond_2
    return v1
.end method

.method public m()Z
    .locals 1

    iget-object v0, p0, Li1/b;->p:Li1/b$e;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Li1/b$e;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public n(IZ)V
    .locals 0

    const/4 p1, 0x1

    iput-boolean p1, p0, Li1/b;->w:Z

    return-void
.end method

.method public o()Z
    .locals 2

    iget-boolean v0, p0, Li1/b;->u:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Li1/b;->m()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lh1/a;->d:Lh1/g;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lh1/a;->f:Lh1/l;

    if-eqz v0, :cond_1

    iget-object v0, p0, Li1/b;->t:Li1/b$d;

    if-nez v0, :cond_1

    new-instance v0, Li1/b$d;

    invoke-virtual {p0}, Li1/b;->k()Li1/b$e;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Li1/b$d;-><init>(Li1/b;Li1/b$e;)V

    iput-object v0, p0, Li1/b;->t:Li1/b$d;

    iget-object v1, p0, Lh1/a;->f:Lh1/l;

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x0

    iget-object v1, p0, Lh1/a;->a:Lh1/k$a;

    if-eqz v1, :cond_0

    invoke-interface {v1, v0}, Lh1/k$a;->c(Lh1/g;)Z

    :cond_0
    iget-object v0, p0, Li1/b;->o:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    return v1

    :cond_1
    const/4 v0, 0x0

    return v0
.end method
