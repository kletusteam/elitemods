.class public Li1/b$f;
.super Lh1/j;
.source ""

# interfaces
.implements Li1/b$e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Li1/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "f"
.end annotation


# instance fields
.field public final synthetic k:Li1/b;


# direct methods
.method public constructor <init>(Li1/b;Landroid/content/Context;Lh1/g;Landroid/view/View;Z)V
    .locals 0

    iput-object p1, p0, Li1/b$f;->k:Li1/b;

    invoke-direct {p0, p2, p3, p4, p5}, Lh1/j;-><init>(Landroid/content/Context;Lh1/g;Landroid/view/View;Z)V

    iget-object p1, p1, Li1/b;->s:Li1/b$g;

    iput-object p1, p0, Lh1/j;->j:Lh1/k$a;

    const p1, 0x7f0d0050

    iput p1, p0, Lh1/j;->g:I

    return-void
.end method


# virtual methods
.method public d(Z)V
    .locals 1

    invoke-virtual {p0}, Lh1/j;->isShowing()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lh1/j;->i:Lt1/a;

    invoke-virtual {p1}, Landroid/widget/PopupWindow;->dismiss()V

    :cond_0
    iget-object p1, p0, Li1/b$f;->k:Li1/b;

    iget-object p1, p1, Li1/b;->o:Landroid/view/View;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setSelected(Z)V

    :cond_1
    return-void
.end method

.method public i(Lh1/g;)V
    .locals 0

    return-void
.end method

.method public onDismiss()V
    .locals 2

    invoke-super {p0}, Lh1/j;->onDismiss()V

    iget-object v0, p0, Li1/b$f;->k:Li1/b;

    iget-object v0, v0, Lh1/a;->d:Lh1/g;

    invoke-virtual {v0}, Lh1/g;->close()V

    iget-object v0, p0, Li1/b$f;->k:Li1/b;

    const/4 v1, 0x0

    iput-object v1, v0, Li1/b;->p:Li1/b$e;

    return-void
.end method
