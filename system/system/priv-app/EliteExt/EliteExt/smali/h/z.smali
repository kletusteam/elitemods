.class public Lh/z;
.super Landroid/widget/ToggleButton;
.source ""


# instance fields
.field public final a:Lh/d;

.field public final b:Lh/w;

.field public c:Lh/k;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const v0, 0x101004b

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/ToggleButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p0}, Landroid/widget/ToggleButton;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p0, p1}, Lh/o0;->a(Landroid/view/View;Landroid/content/Context;)V

    new-instance p1, Lh/d;

    invoke-direct {p1, p0}, Lh/d;-><init>(Landroid/view/View;)V

    iput-object p1, p0, Lh/z;->a:Lh/d;

    invoke-virtual {p1, p2, v0}, Lh/d;->d(Landroid/util/AttributeSet;I)V

    new-instance p1, Lh/w;

    invoke-direct {p1, p0}, Lh/w;-><init>(Landroid/widget/TextView;)V

    iput-object p1, p0, Lh/z;->b:Lh/w;

    invoke-virtual {p1, p2, v0}, Lh/w;->d(Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lh/z;->getEmojiTextViewHelper()Lh/k;

    move-result-object p1

    invoke-virtual {p1, p2, v0}, Lh/k;->a(Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private getEmojiTextViewHelper()Lh/k;
    .locals 1

    iget-object v0, p0, Lh/z;->c:Lh/k;

    if-nez v0, :cond_0

    new-instance v0, Lh/k;

    invoke-direct {v0, p0}, Lh/k;-><init>(Landroid/widget/TextView;)V

    iput-object v0, p0, Lh/z;->c:Lh/k;

    :cond_0
    iget-object v0, p0, Lh/z;->c:Lh/k;

    return-object v0
.end method


# virtual methods
.method public drawableStateChanged()V
    .locals 1

    invoke-super {p0}, Landroid/widget/ToggleButton;->drawableStateChanged()V

    iget-object v0, p0, Lh/z;->a:Lh/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lh/d;->a()V

    :cond_0
    iget-object v0, p0, Lh/z;->b:Lh/w;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lh/w;->b()V

    :cond_1
    return-void
.end method

.method public getSupportBackgroundTintList()Landroid/content/res/ColorStateList;
    .locals 1

    iget-object v0, p0, Lh/z;->a:Lh/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lh/d;->b()Landroid/content/res/ColorStateList;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getSupportBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;
    .locals 1

    iget-object v0, p0, Lh/z;->a:Lh/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lh/d;->c()Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public setAllCaps(Z)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/ToggleButton;->setAllCaps(Z)V

    invoke-direct {p0}, Lh/z;->getEmojiTextViewHelper()Lh/k;

    move-result-object v0

    iget-object v0, v0, Lh/k;->b:Le0/f;

    iget-object v0, v0, Le0/f;->a:Le0/f$b;

    invoke-virtual {v0, p1}, Le0/f$b;->b(Z)V

    return-void
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/ToggleButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lh/z;->a:Lh/d;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lh/d;->e()V

    :cond_0
    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/ToggleButton;->setBackgroundResource(I)V

    iget-object v0, p0, Lh/z;->a:Lh/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lh/d;->f(I)V

    :cond_0
    return-void
.end method

.method public setEmojiCompatEnabled(Z)V
    .locals 1

    invoke-direct {p0}, Lh/z;->getEmojiTextViewHelper()Lh/k;

    move-result-object v0

    iget-object v0, v0, Lh/k;->b:Le0/f;

    iget-object v0, v0, Le0/f;->a:Le0/f$b;

    invoke-virtual {v0, p1}, Le0/f$b;->c(Z)V

    return-void
.end method

.method public setFilters([Landroid/text/InputFilter;)V
    .locals 1

    invoke-direct {p0}, Lh/z;->getEmojiTextViewHelper()Lh/k;

    move-result-object v0

    iget-object v0, v0, Lh/k;->b:Le0/f;

    iget-object v0, v0, Le0/f;->a:Le0/f$b;

    invoke-virtual {v0, p1}, Le0/f$b;->a([Landroid/text/InputFilter;)[Landroid/text/InputFilter;

    move-result-object p1

    invoke-super {p0, p1}, Landroid/widget/ToggleButton;->setFilters([Landroid/text/InputFilter;)V

    return-void
.end method

.method public setSupportBackgroundTintList(Landroid/content/res/ColorStateList;)V
    .locals 1

    iget-object v0, p0, Lh/z;->a:Lh/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lh/d;->h(Landroid/content/res/ColorStateList;)V

    :cond_0
    return-void
.end method

.method public setSupportBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    iget-object v0, p0, Lh/z;->a:Lh/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lh/d;->i(Landroid/graphics/PorterDuff$Mode;)V

    :cond_0
    return-void
.end method
