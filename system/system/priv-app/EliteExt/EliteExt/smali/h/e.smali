.class public Lh/e;
.super Landroid/widget/CheckedTextView;
.source ""


# instance fields
.field public final a:Lh/f;

.field public final b:Lh/d;

.field public final c:Lh/w;

.field public d:Lh/k;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const v0, 0x7f0400a9

    invoke-direct {p0, p1, p2, v0}, Lh/e;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9

    invoke-static {p1}, Lh/q0;->a(Landroid/content/Context;)Landroid/content/Context;

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/CheckedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p0}, Landroid/widget/CheckedTextView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p0, p1}, Lh/o0;->a(Landroid/view/View;Landroid/content/Context;)V

    new-instance p1, Lh/w;

    invoke-direct {p1, p0}, Lh/w;-><init>(Landroid/widget/TextView;)V

    iput-object p1, p0, Lh/e;->c:Lh/w;

    invoke-virtual {p1, p2, p3}, Lh/w;->d(Landroid/util/AttributeSet;I)V

    invoke-virtual {p1}, Lh/w;->b()V

    new-instance p1, Lh/d;

    invoke-direct {p1, p0}, Lh/d;-><init>(Landroid/view/View;)V

    iput-object p1, p0, Lh/e;->b:Lh/d;

    invoke-virtual {p1, p2, p3}, Lh/d;->d(Landroid/util/AttributeSet;I)V

    new-instance p1, Lh/f;

    invoke-direct {p1, p0}, Lh/f;-><init>(Landroid/widget/CheckedTextView;)V

    iput-object p1, p0, Lh/e;->a:Lh/f;

    invoke-virtual {p0}, Landroid/widget/CheckedTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v3, Landroidx/emoji2/text/l;->l:[I

    const/4 v8, 0x0

    invoke-static {v0, p2, v3, p3, v8}, Lh/t0;->o(Landroid/content/Context;Landroid/util/AttributeSet;[III)Lh/t0;

    move-result-object v0

    invoke-virtual {p0}, Landroid/widget/CheckedTextView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v5, v0, Lh/t0;->b:Landroid/content/res/TypedArray;

    const/4 v7, 0x0

    move-object v1, p0

    move-object v4, p2

    move v6, p3

    invoke-static/range {v1 .. v7}, Lw/v;->j(Landroid/view/View;Landroid/content/Context;[ILandroid/util/AttributeSet;Landroid/content/res/TypedArray;II)V

    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {v0, v1}, Lh/t0;->m(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1, v8}, Lh/t0;->j(II)I

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_0

    :try_start_1
    invoke-virtual {p0}, Landroid/widget/CheckedTextView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2}, Landroidx/emoji2/text/l;->t(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p0, v2}, Landroid/widget/CheckedTextView;->setCheckMarkDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    :cond_0
    move v1, v8

    :goto_0
    if-nez v1, :cond_1

    :try_start_2
    invoke-virtual {v0, v8}, Lh/t0;->m(I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0, v8, v8}, Lh/t0;->j(II)I

    move-result v1

    if-eqz v1, :cond_1

    iget-object v2, p1, Lh/f;->a:Landroid/widget/CheckedTextView;

    invoke-virtual {v2}, Landroid/widget/CheckedTextView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v1}, Landroidx/emoji2/text/l;->t(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/CheckedTextView;->setCheckMarkDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lh/t0;->m(I)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p1, Lh/f;->a:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v1}, Lh/t0;->b(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/CheckedTextView;->setCheckMarkTintList(Landroid/content/res/ColorStateList;)V

    :cond_2
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lh/t0;->m(I)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object p1, p1, Lh/f;->a:Landroid/widget/CheckedTextView;

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Lh/t0;->h(II)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lh/c0;->d(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuff$Mode;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/CheckedTextView;->setCheckMarkTintMode(Landroid/graphics/PorterDuff$Mode;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    iget-object p1, v0, Lh/t0;->b:Landroid/content/res/TypedArray;

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    invoke-direct {p0}, Lh/e;->getEmojiTextViewHelper()Lh/k;

    move-result-object p1

    invoke-virtual {p1, p2, p3}, Lh/k;->a(Landroid/util/AttributeSet;I)V

    return-void

    :catchall_0
    move-exception p1

    iget-object p2, v0, Lh/t0;->b:Landroid/content/res/TypedArray;

    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    throw p1
.end method

.method private getEmojiTextViewHelper()Lh/k;
    .locals 1

    iget-object v0, p0, Lh/e;->d:Lh/k;

    if-nez v0, :cond_0

    new-instance v0, Lh/k;

    invoke-direct {v0, p0}, Lh/k;-><init>(Landroid/widget/TextView;)V

    iput-object v0, p0, Lh/e;->d:Lh/k;

    :cond_0
    iget-object v0, p0, Lh/e;->d:Lh/k;

    return-object v0
.end method


# virtual methods
.method public drawableStateChanged()V
    .locals 1

    invoke-super {p0}, Landroid/widget/CheckedTextView;->drawableStateChanged()V

    iget-object v0, p0, Lh/e;->c:Lh/w;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lh/w;->b()V

    :cond_0
    iget-object v0, p0, Lh/e;->b:Lh/d;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lh/d;->a()V

    :cond_1
    iget-object v0, p0, Lh/e;->a:Lh/f;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lh/f;->a()V

    :cond_2
    return-void
.end method

.method public getCustomSelectionActionModeCallback()Landroid/view/ActionMode$Callback;
    .locals 1

    invoke-super {p0}, Landroid/widget/CheckedTextView;->getCustomSelectionActionModeCallback()Landroid/view/ActionMode$Callback;

    move-result-object v0

    invoke-static {v0}, Lz/f;->d(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode$Callback;

    move-result-object v0

    return-object v0
.end method

.method public getSupportBackgroundTintList()Landroid/content/res/ColorStateList;
    .locals 1

    iget-object v0, p0, Lh/e;->b:Lh/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lh/d;->b()Landroid/content/res/ColorStateList;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getSupportBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;
    .locals 1

    iget-object v0, p0, Lh/e;->b:Lh/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lh/d;->c()Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getSupportCheckMarkTintList()Landroid/content/res/ColorStateList;
    .locals 1

    iget-object v0, p0, Lh/e;->a:Lh/f;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lh/f;->b:Landroid/content/res/ColorStateList;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getSupportCheckMarkTintMode()Landroid/graphics/PorterDuff$Mode;
    .locals 1

    iget-object v0, p0, Lh/e;->a:Lh/f;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lh/f;->c:Landroid/graphics/PorterDuff$Mode;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/CheckedTextView;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    invoke-static {v0, p1, p0}, Landroidx/emoji2/text/l;->L(Landroid/view/inputmethod/InputConnection;Landroid/view/inputmethod/EditorInfo;Landroid/view/View;)Landroid/view/inputmethod/InputConnection;

    return-object v0
.end method

.method public setAllCaps(Z)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/CheckedTextView;->setAllCaps(Z)V

    invoke-direct {p0}, Lh/e;->getEmojiTextViewHelper()Lh/k;

    move-result-object v0

    iget-object v0, v0, Lh/k;->b:Le0/f;

    iget-object v0, v0, Le0/f;->a:Le0/f$b;

    invoke-virtual {v0, p1}, Le0/f$b;->b(Z)V

    return-void
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/CheckedTextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lh/e;->b:Lh/d;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lh/d;->e()V

    :cond_0
    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/CheckedTextView;->setBackgroundResource(I)V

    iget-object v0, p0, Lh/e;->b:Lh/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lh/d;->f(I)V

    :cond_0
    return-void
.end method

.method public setCheckMarkDrawable(I)V
    .locals 1

    invoke-virtual {p0}, Landroid/widget/CheckedTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Landroidx/emoji2/text/l;->t(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p0, p1}, Lh/e;->setCheckMarkDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setCheckMarkDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/CheckedTextView;->setCheckMarkDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lh/e;->a:Lh/f;

    if-eqz p1, :cond_1

    iget-boolean v0, p1, Lh/f;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p1, Lh/f;->f:Z

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p1, Lh/f;->f:Z

    invoke-virtual {p1}, Lh/f;->a()V

    :cond_1
    :goto_0
    return-void
.end method

.method public setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/CheckedTextView;->setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V

    return-void
.end method

.method public setEmojiCompatEnabled(Z)V
    .locals 1

    invoke-direct {p0}, Lh/e;->getEmojiTextViewHelper()Lh/k;

    move-result-object v0

    iget-object v0, v0, Lh/k;->b:Le0/f;

    iget-object v0, v0, Le0/f;->a:Le0/f$b;

    invoke-virtual {v0, p1}, Le0/f$b;->c(Z)V

    return-void
.end method

.method public setSupportBackgroundTintList(Landroid/content/res/ColorStateList;)V
    .locals 1

    iget-object v0, p0, Lh/e;->b:Lh/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lh/d;->h(Landroid/content/res/ColorStateList;)V

    :cond_0
    return-void
.end method

.method public setSupportBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    iget-object v0, p0, Lh/e;->b:Lh/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lh/d;->i(Landroid/graphics/PorterDuff$Mode;)V

    :cond_0
    return-void
.end method

.method public setSupportCheckMarkTintList(Landroid/content/res/ColorStateList;)V
    .locals 1

    iget-object v0, p0, Lh/e;->a:Lh/f;

    if-eqz v0, :cond_0

    iput-object p1, v0, Lh/f;->b:Landroid/content/res/ColorStateList;

    const/4 p1, 0x1

    iput-boolean p1, v0, Lh/f;->d:Z

    invoke-virtual {v0}, Lh/f;->a()V

    :cond_0
    return-void
.end method

.method public setSupportCheckMarkTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    iget-object v0, p0, Lh/e;->a:Lh/f;

    if-eqz v0, :cond_0

    iput-object p1, v0, Lh/f;->c:Landroid/graphics/PorterDuff$Mode;

    const/4 p1, 0x1

    iput-boolean p1, v0, Lh/f;->e:Z

    invoke-virtual {v0}, Lh/f;->a()V

    :cond_0
    return-void
.end method

.method public setTextAppearance(Landroid/content/Context;I)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/widget/CheckedTextView;->setTextAppearance(Landroid/content/Context;I)V

    iget-object v0, p0, Lh/e;->c:Lh/w;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lh/w;->e(Landroid/content/Context;I)V

    :cond_0
    return-void
.end method
