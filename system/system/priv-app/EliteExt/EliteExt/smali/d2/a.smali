.class public Ld2/a;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/graphics/Paint;

.field public b:Landroid/graphics/Path;

.field public c:Landroid/graphics/RectF;

.field public d:Landroid/graphics/Path;

.field public e:Lc2/a;

.field public f:[F

.field public g:F

.field public h:I

.field public i:Landroid/graphics/Paint;

.field public j:I


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Ld2/a;->j:I

    iput v0, p0, Ld2/a;->h:I

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Ld2/a;->a:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Ld2/a;->i:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Ld2/a;->d:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Ld2/a;->b:Landroid/graphics/Path;

    new-instance v0, Lc2/a;

    invoke-direct {v0}, Lc2/a;-><init>()V

    iput-object v0, p0, Ld2/a;->e:Lc2/a;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Ld2/a;->c:Landroid/graphics/RectF;

    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Canvas;)V
    .locals 2

    iget v0, p0, Ld2/a;->j:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Ld2/a;->i:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Ld2/a;->h:I

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v0, p0, Ld2/a;->i:Landroid/graphics/Paint;

    iget v1, p0, Ld2/a;->j:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Ld2/a;->i:Landroid/graphics/Paint;

    iget v1, p0, Ld2/a;->h:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Ld2/a;->d:Landroid/graphics/Path;

    iget-object v1, p0, Ld2/a;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_1
    return-void
.end method

.method public b(Landroid/graphics/Rect;)Landroid/graphics/Path;
    .locals 9

    iget v0, p0, Ld2/a;->j:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Ld2/a;->i:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Ld2/a;->h:I

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/high16 v1, 0x3f000000    # 0.5f

    if-eqz v0, :cond_1

    iget v0, p0, Ld2/a;->j:I

    int-to-float v0, v0

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    add-float/2addr v1, v0

    :cond_1
    move v8, v1

    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, p1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    iget-object v5, p0, Ld2/a;->f:[F

    iget v6, p0, Ld2/a;->g:F

    move-object v2, p0

    move v7, v8

    invoke-virtual/range {v2 .. v8}, Ld2/a;->c(Landroid/graphics/Path;Landroid/graphics/RectF;[FFFF)Landroid/graphics/Path;

    move-result-object p1

    return-object p1
.end method

.method public final c(Landroid/graphics/Path;Landroid/graphics/RectF;[FFFF)Landroid/graphics/Path;
    .locals 18

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move/from16 v3, p5

    move/from16 v4, p6

    const/4 v5, 0x3

    const/4 v6, 0x2

    const/4 v7, 0x1

    const/4 v8, 0x0

    if-nez v2, :cond_0

    iget-object v2, v0, Ld2/a;->e:Lc2/a;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v9, 0x8

    new-array v9, v9, [F

    aput p4, v9, v8

    aput p4, v9, v7

    aput p4, v9, v6

    aput p4, v9, v5

    const/4 v10, 0x4

    aput p4, v9, v10

    const/4 v10, 0x5

    aput p4, v9, v10

    const/4 v10, 0x6

    aput p4, v9, v10

    const/4 v10, 0x7

    aput p4, v9, v10

    invoke-virtual {v2, v1, v9, v3, v4}, Lc2/a;->a(Landroid/graphics/RectF;[FFF)Lc2/a$b;

    move-result-object v1

    goto :goto_0

    :cond_0
    iget-object v9, v0, Ld2/a;->e:Lc2/a;

    invoke-virtual {v9, v1, v2, v3, v4}, Lc2/a;->a(Landroid/graphics/RectF;[FFF)Lc2/a$b;

    move-result-object v1

    :goto_0
    iget-object v2, v0, Ld2/a;->e:Lc2/a;

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p1, :cond_1

    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    goto :goto_1

    :cond_1
    move-object/from16 v2, p1

    :goto_1
    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    if-nez v1, :cond_2

    goto/16 :goto_6

    :cond_2
    iget-object v3, v1, Lc2/a$b;->e:Lc2/a$a;

    if-eqz v3, :cond_4

    iget-object v4, v1, Lc2/a$b;->f:Lc2/a$a;

    if-eqz v4, :cond_4

    iget-object v4, v1, Lc2/a$b;->b:Lc2/a$a;

    if-eqz v4, :cond_4

    iget-object v4, v1, Lc2/a$b;->a:Lc2/a$a;

    if-nez v4, :cond_3

    goto :goto_2

    :cond_3
    move v4, v8

    goto :goto_3

    :cond_4
    :goto_2
    move v4, v7

    :goto_3
    const/4 v15, 0x0

    if-eqz v4, :cond_5

    new-instance v3, Landroid/graphics/RectF;

    iget v4, v1, Lc2/a$b;->g:F

    iget v1, v1, Lc2/a$b;->c:F

    invoke-direct {v3, v15, v15, v4, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    sget-object v1, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    goto/16 :goto_6

    :cond_5
    iget v4, v3, Lc2/a$a;->g:F

    cmpl-float v4, v4, v15

    if-eqz v4, :cond_6

    iget-object v4, v3, Lc2/a$a;->d:Landroid/graphics/RectF;

    iget-wide v9, v3, Lc2/a$a;->i:D

    const-wide v11, 0x400921fb54442d18L    # Math.PI

    add-double/2addr v9, v11

    invoke-static {v9, v10}, Lc2/a;->d(D)D

    move-result-wide v9

    double-to-float v3, v9

    iget-object v9, v1, Lc2/a$b;->e:Lc2/a$a;

    iget v9, v9, Lc2/a$a;->g:F

    invoke-virtual {v2, v4, v3, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    goto :goto_4

    :cond_6
    iget-object v3, v3, Lc2/a$a;->a:[Landroid/graphics/PointF;

    aget-object v4, v3, v8

    iget v4, v4, Landroid/graphics/PointF;->x:F

    aget-object v3, v3, v8

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v4, v3}, Landroid/graphics/Path;->moveTo(FF)V

    :goto_4
    iget-object v3, v1, Lc2/a$b;->e:Lc2/a$a;

    iget-wide v9, v3, Lc2/a$a;->e:D

    const-wide/16 v16, 0x0

    cmpl-double v4, v9, v16

    if-eqz v4, :cond_7

    iget-object v3, v3, Lc2/a$a;->a:[Landroid/graphics/PointF;

    aget-object v4, v3, v7

    iget v10, v4, Landroid/graphics/PointF;->x:F

    aget-object v4, v3, v7

    iget v11, v4, Landroid/graphics/PointF;->y:F

    aget-object v4, v3, v6

    iget v12, v4, Landroid/graphics/PointF;->x:F

    aget-object v4, v3, v6

    iget v13, v4, Landroid/graphics/PointF;->y:F

    aget-object v4, v3, v5

    iget v14, v4, Landroid/graphics/PointF;->x:F

    aget-object v3, v3, v5

    iget v3, v3, Landroid/graphics/PointF;->y:F

    move-object v9, v2

    move v4, v15

    move v15, v3

    invoke-virtual/range {v9 .. v15}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    goto :goto_5

    :cond_7
    move v4, v15

    :goto_5
    iget v3, v1, Lc2/a$b;->g:F

    iget-object v9, v1, Lc2/a$b;->e:Lc2/a$a;

    iget v9, v9, Lc2/a$a;->c:F

    iget-object v10, v1, Lc2/a$b;->f:Lc2/a$a;

    iget v10, v10, Lc2/a$a;->c:F

    iget-wide v11, v1, Lc2/a$b;->d:D

    const v13, 0x3eeb851f    # 0.46f

    move/from16 p1, v3

    move/from16 p2, v9

    move/from16 p3, v10

    move-wide/from16 p4, v11

    move/from16 p6, v13

    invoke-static/range {p1 .. p6}, Lc2/a;->c(FFFDF)Z

    move-result v3

    if-nez v3, :cond_8

    iget-object v3, v1, Lc2/a$b;->f:Lc2/a$a;

    iget-object v3, v3, Lc2/a$a;->a:[Landroid/graphics/PointF;

    aget-object v9, v3, v8

    iget v9, v9, Landroid/graphics/PointF;->x:F

    aget-object v3, v3, v8

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v9, v3}, Landroid/graphics/Path;->lineTo(FF)V

    :cond_8
    iget-object v3, v1, Lc2/a$b;->f:Lc2/a$a;

    iget-wide v9, v3, Lc2/a$a;->e:D

    cmpl-double v9, v9, v16

    if-eqz v9, :cond_9

    iget-object v3, v3, Lc2/a$a;->a:[Landroid/graphics/PointF;

    aget-object v9, v3, v7

    iget v10, v9, Landroid/graphics/PointF;->x:F

    aget-object v9, v3, v7

    iget v11, v9, Landroid/graphics/PointF;->y:F

    aget-object v9, v3, v6

    iget v12, v9, Landroid/graphics/PointF;->x:F

    aget-object v9, v3, v6

    iget v13, v9, Landroid/graphics/PointF;->y:F

    aget-object v9, v3, v5

    iget v14, v9, Landroid/graphics/PointF;->x:F

    aget-object v3, v3, v5

    iget v15, v3, Landroid/graphics/PointF;->y:F

    move-object v9, v2

    invoke-virtual/range {v9 .. v15}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    :cond_9
    iget-object v3, v1, Lc2/a$b;->f:Lc2/a$a;

    iget v9, v3, Lc2/a$a;->g:F

    cmpl-float v9, v9, v4

    if-eqz v9, :cond_a

    iget-object v9, v3, Lc2/a$a;->d:Landroid/graphics/RectF;

    iget-wide v10, v3, Lc2/a$a;->h:D

    const-wide v12, 0x4012d97c7f3321d2L    # 4.71238898038469

    add-double/2addr v10, v12

    invoke-static {v10, v11}, Lc2/a;->d(D)D

    move-result-wide v10

    double-to-float v3, v10

    iget-object v10, v1, Lc2/a$b;->f:Lc2/a$a;

    iget v10, v10, Lc2/a$a;->g:F

    invoke-virtual {v2, v9, v3, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    :cond_a
    iget-object v3, v1, Lc2/a$b;->f:Lc2/a$a;

    iget-wide v9, v3, Lc2/a$a;->f:D

    cmpl-double v9, v9, v16

    if-eqz v9, :cond_b

    iget-object v3, v3, Lc2/a$a;->b:[Landroid/graphics/PointF;

    aget-object v9, v3, v7

    iget v10, v9, Landroid/graphics/PointF;->x:F

    aget-object v9, v3, v7

    iget v11, v9, Landroid/graphics/PointF;->y:F

    aget-object v9, v3, v6

    iget v12, v9, Landroid/graphics/PointF;->x:F

    aget-object v9, v3, v6

    iget v13, v9, Landroid/graphics/PointF;->y:F

    aget-object v9, v3, v5

    iget v14, v9, Landroid/graphics/PointF;->x:F

    aget-object v3, v3, v5

    iget v15, v3, Landroid/graphics/PointF;->y:F

    move-object v9, v2

    invoke-virtual/range {v9 .. v15}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    :cond_b
    iget v3, v1, Lc2/a$b;->c:F

    iget-object v9, v1, Lc2/a$b;->f:Lc2/a$a;

    iget v9, v9, Lc2/a$a;->c:F

    iget-object v10, v1, Lc2/a$b;->b:Lc2/a$a;

    iget v10, v10, Lc2/a$a;->c:F

    iget-wide v11, v1, Lc2/a$b;->d:D

    const v13, 0x3eeb851f    # 0.46f

    move/from16 p1, v3

    move/from16 p2, v9

    move/from16 p3, v10

    move-wide/from16 p4, v11

    move/from16 p6, v13

    invoke-static/range {p1 .. p6}, Lc2/a;->b(FFFDF)Z

    move-result v3

    if-nez v3, :cond_c

    iget-object v3, v1, Lc2/a$b;->b:Lc2/a$a;

    iget-object v3, v3, Lc2/a$a;->b:[Landroid/graphics/PointF;

    aget-object v9, v3, v8

    iget v9, v9, Landroid/graphics/PointF;->x:F

    aget-object v3, v3, v8

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v9, v3}, Landroid/graphics/Path;->lineTo(FF)V

    :cond_c
    iget-object v3, v1, Lc2/a$b;->b:Lc2/a$a;

    iget-wide v9, v3, Lc2/a$a;->f:D

    cmpl-double v9, v9, v16

    if-eqz v9, :cond_d

    iget-object v3, v3, Lc2/a$a;->b:[Landroid/graphics/PointF;

    aget-object v9, v3, v7

    iget v10, v9, Landroid/graphics/PointF;->x:F

    aget-object v9, v3, v7

    iget v11, v9, Landroid/graphics/PointF;->y:F

    aget-object v9, v3, v6

    iget v12, v9, Landroid/graphics/PointF;->x:F

    aget-object v9, v3, v6

    iget v13, v9, Landroid/graphics/PointF;->y:F

    aget-object v9, v3, v5

    iget v14, v9, Landroid/graphics/PointF;->x:F

    aget-object v3, v3, v5

    iget v15, v3, Landroid/graphics/PointF;->y:F

    move-object v9, v2

    invoke-virtual/range {v9 .. v15}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    :cond_d
    iget-object v3, v1, Lc2/a$b;->b:Lc2/a$a;

    iget v9, v3, Lc2/a$a;->g:F

    cmpl-float v9, v9, v4

    if-eqz v9, :cond_e

    iget-object v9, v3, Lc2/a$a;->d:Landroid/graphics/RectF;

    iget-wide v10, v3, Lc2/a$a;->i:D

    invoke-static {v10, v11}, Lc2/a;->d(D)D

    move-result-wide v10

    double-to-float v3, v10

    iget-object v10, v1, Lc2/a$b;->b:Lc2/a$a;

    iget v10, v10, Lc2/a$a;->g:F

    invoke-virtual {v2, v9, v3, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    :cond_e
    iget-object v3, v1, Lc2/a$b;->b:Lc2/a$a;

    iget-wide v9, v3, Lc2/a$a;->e:D

    cmpl-double v9, v9, v16

    if-eqz v9, :cond_f

    iget-object v3, v3, Lc2/a$a;->a:[Landroid/graphics/PointF;

    aget-object v9, v3, v7

    iget v10, v9, Landroid/graphics/PointF;->x:F

    aget-object v9, v3, v7

    iget v11, v9, Landroid/graphics/PointF;->y:F

    aget-object v9, v3, v6

    iget v12, v9, Landroid/graphics/PointF;->x:F

    aget-object v9, v3, v6

    iget v13, v9, Landroid/graphics/PointF;->y:F

    aget-object v9, v3, v5

    iget v14, v9, Landroid/graphics/PointF;->x:F

    aget-object v3, v3, v5

    iget v15, v3, Landroid/graphics/PointF;->y:F

    move-object v9, v2

    invoke-virtual/range {v9 .. v15}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    :cond_f
    iget v3, v1, Lc2/a$b;->g:F

    iget-object v9, v1, Lc2/a$b;->b:Lc2/a$a;

    iget v9, v9, Lc2/a$a;->c:F

    iget-object v10, v1, Lc2/a$b;->a:Lc2/a$a;

    iget v10, v10, Lc2/a$a;->c:F

    iget-wide v11, v1, Lc2/a$b;->d:D

    const v13, 0x3eeb851f    # 0.46f

    move/from16 p1, v3

    move/from16 p2, v9

    move/from16 p3, v10

    move-wide/from16 p4, v11

    move/from16 p6, v13

    invoke-static/range {p1 .. p6}, Lc2/a;->c(FFFDF)Z

    move-result v3

    if-nez v3, :cond_10

    iget-object v3, v1, Lc2/a$b;->a:Lc2/a$a;

    iget-object v3, v3, Lc2/a$a;->a:[Landroid/graphics/PointF;

    aget-object v9, v3, v8

    iget v9, v9, Landroid/graphics/PointF;->x:F

    aget-object v3, v3, v8

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v9, v3}, Landroid/graphics/Path;->lineTo(FF)V

    :cond_10
    iget-object v3, v1, Lc2/a$b;->a:Lc2/a$a;

    iget-wide v9, v3, Lc2/a$a;->e:D

    cmpl-double v9, v9, v16

    if-eqz v9, :cond_11

    iget-object v3, v3, Lc2/a$a;->a:[Landroid/graphics/PointF;

    aget-object v9, v3, v7

    iget v10, v9, Landroid/graphics/PointF;->x:F

    aget-object v9, v3, v7

    iget v11, v9, Landroid/graphics/PointF;->y:F

    aget-object v9, v3, v6

    iget v12, v9, Landroid/graphics/PointF;->x:F

    aget-object v9, v3, v6

    iget v13, v9, Landroid/graphics/PointF;->y:F

    aget-object v9, v3, v5

    iget v14, v9, Landroid/graphics/PointF;->x:F

    aget-object v3, v3, v5

    iget v15, v3, Landroid/graphics/PointF;->y:F

    move-object v9, v2

    invoke-virtual/range {v9 .. v15}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    :cond_11
    iget-object v3, v1, Lc2/a$b;->a:Lc2/a$a;

    iget v9, v3, Lc2/a$a;->g:F

    cmpl-float v4, v9, v4

    if-eqz v4, :cond_12

    iget-object v4, v3, Lc2/a$a;->d:Landroid/graphics/RectF;

    iget-wide v9, v3, Lc2/a$a;->h:D

    const-wide v11, 0x3ff921fb54442d18L    # 1.5707963267948966

    add-double/2addr v9, v11

    invoke-static {v9, v10}, Lc2/a;->d(D)D

    move-result-wide v9

    double-to-float v3, v9

    iget-object v9, v1, Lc2/a$b;->a:Lc2/a$a;

    iget v9, v9, Lc2/a$a;->g:F

    invoke-virtual {v2, v4, v3, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    :cond_12
    iget-object v3, v1, Lc2/a$b;->a:Lc2/a$a;

    iget-wide v9, v3, Lc2/a$a;->f:D

    cmpl-double v4, v9, v16

    if-eqz v4, :cond_13

    iget-object v3, v3, Lc2/a$a;->b:[Landroid/graphics/PointF;

    aget-object v4, v3, v7

    iget v10, v4, Landroid/graphics/PointF;->x:F

    aget-object v4, v3, v7

    iget v11, v4, Landroid/graphics/PointF;->y:F

    aget-object v4, v3, v6

    iget v12, v4, Landroid/graphics/PointF;->x:F

    aget-object v4, v3, v6

    iget v13, v4, Landroid/graphics/PointF;->y:F

    aget-object v4, v3, v5

    iget v14, v4, Landroid/graphics/PointF;->x:F

    aget-object v3, v3, v5

    iget v15, v3, Landroid/graphics/PointF;->y:F

    move-object v9, v2

    invoke-virtual/range {v9 .. v15}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    :cond_13
    iget v3, v1, Lc2/a$b;->c:F

    iget-object v4, v1, Lc2/a$b;->a:Lc2/a$a;

    iget v4, v4, Lc2/a$a;->c:F

    iget-object v9, v1, Lc2/a$b;->e:Lc2/a$a;

    iget v9, v9, Lc2/a$a;->c:F

    iget-wide v10, v1, Lc2/a$b;->d:D

    const v12, 0x3eeb851f    # 0.46f

    move/from16 p1, v3

    move/from16 p2, v4

    move/from16 p3, v9

    move-wide/from16 p4, v10

    move/from16 p6, v12

    invoke-static/range {p1 .. p6}, Lc2/a;->b(FFFDF)Z

    move-result v3

    if-nez v3, :cond_14

    iget-object v3, v1, Lc2/a$b;->e:Lc2/a$a;

    iget-object v3, v3, Lc2/a$a;->b:[Landroid/graphics/PointF;

    aget-object v4, v3, v8

    iget v4, v4, Landroid/graphics/PointF;->x:F

    aget-object v3, v3, v8

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v4, v3}, Landroid/graphics/Path;->lineTo(FF)V

    :cond_14
    iget-object v1, v1, Lc2/a$b;->e:Lc2/a$a;

    iget-wide v3, v1, Lc2/a$a;->f:D

    cmpl-double v3, v3, v16

    if-eqz v3, :cond_15

    iget-object v1, v1, Lc2/a$a;->b:[Landroid/graphics/PointF;

    aget-object v3, v1, v7

    iget v10, v3, Landroid/graphics/PointF;->x:F

    aget-object v3, v1, v7

    iget v11, v3, Landroid/graphics/PointF;->y:F

    aget-object v3, v1, v6

    iget v12, v3, Landroid/graphics/PointF;->x:F

    aget-object v3, v1, v6

    iget v13, v3, Landroid/graphics/PointF;->y:F

    aget-object v3, v1, v5

    iget v14, v3, Landroid/graphics/PointF;->x:F

    aget-object v1, v1, v5

    iget v15, v1, Landroid/graphics/PointF;->y:F

    move-object v9, v2

    invoke-virtual/range {v9 .. v15}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    :cond_15
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    :goto_6
    return-object v2
.end method

.method public d(Landroid/graphics/Rect;)V
    .locals 10

    iget-object v0, p0, Ld2/a;->c:Landroid/graphics/RectF;

    iget v1, p1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    const/high16 v2, 0x3f000000    # 0.5f

    sub-float/2addr v1, v2

    iget v3, p1, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    sub-float/2addr v3, v2

    iget v4, p1, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    add-float/2addr v4, v2

    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    int-to-float p1, p1

    add-float/2addr p1, v2

    invoke-virtual {v0, v1, v3, v4, p1}, Landroid/graphics/RectF;->set(FFFF)V

    iget p1, p0, Ld2/a;->j:I

    if-eqz p1, :cond_0

    iget-object p1, p0, Ld2/a;->i:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/graphics/Paint;->getAlpha()I

    move-result p1

    if-eqz p1, :cond_0

    iget p1, p0, Ld2/a;->h:I

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    iget p1, p0, Ld2/a;->j:I

    int-to-float p1, p1

    const/high16 v0, 0x40000000    # 2.0f

    div-float/2addr p1, v0

    add-float/2addr v2, p1

    :cond_1
    move v9, v2

    iget-object v4, p0, Ld2/a;->d:Landroid/graphics/Path;

    iget-object v5, p0, Ld2/a;->c:Landroid/graphics/RectF;

    iget-object v6, p0, Ld2/a;->f:[F

    iget v7, p0, Ld2/a;->g:F

    move-object v3, p0

    move v8, v9

    invoke-virtual/range {v3 .. v9}, Ld2/a;->c(Landroid/graphics/Path;Landroid/graphics/RectF;[FFFF)Landroid/graphics/Path;

    move-result-object p1

    iput-object p1, p0, Ld2/a;->d:Landroid/graphics/Path;

    iget-object p1, p0, Ld2/a;->b:Landroid/graphics/Path;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/graphics/Path;->reset()V

    goto :goto_1

    :cond_2
    new-instance p1, Landroid/graphics/Path;

    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    iput-object p1, p0, Ld2/a;->b:Landroid/graphics/Path;

    :goto_1
    iget-object p1, p0, Ld2/a;->b:Landroid/graphics/Path;

    iget-object v0, p0, Ld2/a;->c:Landroid/graphics/RectF;

    sget-object v1, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    iget-object p1, p0, Ld2/a;->b:Landroid/graphics/Path;

    iget-object v0, p0, Ld2/a;->d:Landroid/graphics/Path;

    sget-object v1, Landroid/graphics/Path$Op;->DIFFERENCE:Landroid/graphics/Path$Op;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->op(Landroid/graphics/Path;Landroid/graphics/Path$Op;)Z

    return-void
.end method
